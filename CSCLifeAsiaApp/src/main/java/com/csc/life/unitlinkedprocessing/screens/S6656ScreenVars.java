package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6656
 * @version 1.0 generated on 30/08/09 06:56
 * @author Quipoz
 */
public class S6656ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1228);
	public FixedLengthStringData dataFields = new FixedLengthStringData(316).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData divisor = DD.divisor.copyToZonedDecimal().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,7);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,15);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,23);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData svpFactors = new FixedLengthStringData(250).isAPartOf(dataFields, 61);
	public ZonedDecimalData[] svpFactor = ZDArrayPartOfStructure(50, 5, 0, svpFactors, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(250).isAPartOf(svpFactors, 0, FILLER_REDEFINE);
	public ZonedDecimalData svpFactor01 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData svpFactor02 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,5);
	public ZonedDecimalData svpFactor03 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,10);
	public ZonedDecimalData svpFactor04 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData svpFactor05 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,20);
	public ZonedDecimalData svpFactor06 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,25);
	public ZonedDecimalData svpFactor07 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData svpFactor08 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,35);
	public ZonedDecimalData svpFactor09 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,40);
	public ZonedDecimalData svpFactor10 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,45);
	public ZonedDecimalData svpFactor11 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,50);
	public ZonedDecimalData svpFactor12 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,55);
	public ZonedDecimalData svpFactor13 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,60);
	public ZonedDecimalData svpFactor14 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,65);
	public ZonedDecimalData svpFactor15 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,70);
	public ZonedDecimalData svpFactor16 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,75);
	public ZonedDecimalData svpFactor17 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,80);
	public ZonedDecimalData svpFactor18 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,85);
	public ZonedDecimalData svpFactor19 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,90);
	public ZonedDecimalData svpFactor20 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,95);
	public ZonedDecimalData svpFactor21 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,100);
	public ZonedDecimalData svpFactor22 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,105);
	public ZonedDecimalData svpFactor23 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,110);
	public ZonedDecimalData svpFactor24 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,115);
	public ZonedDecimalData svpFactor25 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,120);
	public ZonedDecimalData svpFactor26 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,125);
	public ZonedDecimalData svpFactor27 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,130);
	public ZonedDecimalData svpFactor28 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,135);
	public ZonedDecimalData svpFactor29 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,140);
	public ZonedDecimalData svpFactor30 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,145);
	public ZonedDecimalData svpFactor31 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,150);
	public ZonedDecimalData svpFactor32 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,155);
	public ZonedDecimalData svpFactor33 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,160);
	public ZonedDecimalData svpFactor34 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,165);
	public ZonedDecimalData svpFactor35 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,170);
	public ZonedDecimalData svpFactor36 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,175);
	public ZonedDecimalData svpFactor37 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,180);
	public ZonedDecimalData svpFactor38 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,185);
	public ZonedDecimalData svpFactor39 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,190);
	public ZonedDecimalData svpFactor40 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,195);
	public ZonedDecimalData svpFactor41 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,200);
	public ZonedDecimalData svpFactor42 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,205);
	public ZonedDecimalData svpFactor43 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,210);
	public ZonedDecimalData svpFactor44 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,215);
	public ZonedDecimalData svpFactor45 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,220);
	public ZonedDecimalData svpFactor46 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,225);
	public ZonedDecimalData svpFactor47 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,230);
	public ZonedDecimalData svpFactor48 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,235);
	public ZonedDecimalData svpFactor49 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,240);
	public ZonedDecimalData svpFactor50 = DD.svpfact.copyToZonedDecimal().isAPartOf(filler,245);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,311);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 316);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData divisorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData svpfactsErr = new FixedLengthStringData(200).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] svpfactErr = FLSArrayPartOfStructure(50, 4, svpfactsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(200).isAPartOf(svpfactsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData svpfact01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData svpfact02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData svpfact03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData svpfact04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData svpfact05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData svpfact06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData svpfact07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData svpfact08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData svpfact09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData svpfact10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData svpfact11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData svpfact12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData svpfact13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData svpfact14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData svpfact15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData svpfact16Err = new FixedLengthStringData(4).isAPartOf(filler1, 60);
	public FixedLengthStringData svpfact17Err = new FixedLengthStringData(4).isAPartOf(filler1, 64);
	public FixedLengthStringData svpfact18Err = new FixedLengthStringData(4).isAPartOf(filler1, 68);
	public FixedLengthStringData svpfact19Err = new FixedLengthStringData(4).isAPartOf(filler1, 72);
	public FixedLengthStringData svpfact20Err = new FixedLengthStringData(4).isAPartOf(filler1, 76);
	public FixedLengthStringData svpfact21Err = new FixedLengthStringData(4).isAPartOf(filler1, 80);
	public FixedLengthStringData svpfact22Err = new FixedLengthStringData(4).isAPartOf(filler1, 84);
	public FixedLengthStringData svpfact23Err = new FixedLengthStringData(4).isAPartOf(filler1, 88);
	public FixedLengthStringData svpfact24Err = new FixedLengthStringData(4).isAPartOf(filler1, 92);
	public FixedLengthStringData svpfact25Err = new FixedLengthStringData(4).isAPartOf(filler1, 96);
	public FixedLengthStringData svpfact26Err = new FixedLengthStringData(4).isAPartOf(filler1, 100);
	public FixedLengthStringData svpfact27Err = new FixedLengthStringData(4).isAPartOf(filler1, 104);
	public FixedLengthStringData svpfact28Err = new FixedLengthStringData(4).isAPartOf(filler1, 108);
	public FixedLengthStringData svpfact29Err = new FixedLengthStringData(4).isAPartOf(filler1, 112);
	public FixedLengthStringData svpfact30Err = new FixedLengthStringData(4).isAPartOf(filler1, 116);
	public FixedLengthStringData svpfact31Err = new FixedLengthStringData(4).isAPartOf(filler1, 120);
	public FixedLengthStringData svpfact32Err = new FixedLengthStringData(4).isAPartOf(filler1, 124);
	public FixedLengthStringData svpfact33Err = new FixedLengthStringData(4).isAPartOf(filler1, 128);
	public FixedLengthStringData svpfact34Err = new FixedLengthStringData(4).isAPartOf(filler1, 132);
	public FixedLengthStringData svpfact35Err = new FixedLengthStringData(4).isAPartOf(filler1, 136);
	public FixedLengthStringData svpfact36Err = new FixedLengthStringData(4).isAPartOf(filler1, 140);
	public FixedLengthStringData svpfact37Err = new FixedLengthStringData(4).isAPartOf(filler1, 144);
	public FixedLengthStringData svpfact38Err = new FixedLengthStringData(4).isAPartOf(filler1, 148);
	public FixedLengthStringData svpfact39Err = new FixedLengthStringData(4).isAPartOf(filler1, 152);
	public FixedLengthStringData svpfact40Err = new FixedLengthStringData(4).isAPartOf(filler1, 156);
	public FixedLengthStringData svpfact41Err = new FixedLengthStringData(4).isAPartOf(filler1, 160);
	public FixedLengthStringData svpfact42Err = new FixedLengthStringData(4).isAPartOf(filler1, 164);
	public FixedLengthStringData svpfact43Err = new FixedLengthStringData(4).isAPartOf(filler1, 168);
	public FixedLengthStringData svpfact44Err = new FixedLengthStringData(4).isAPartOf(filler1, 172);
	public FixedLengthStringData svpfact45Err = new FixedLengthStringData(4).isAPartOf(filler1, 176);
	public FixedLengthStringData svpfact46Err = new FixedLengthStringData(4).isAPartOf(filler1, 180);
	public FixedLengthStringData svpfact47Err = new FixedLengthStringData(4).isAPartOf(filler1, 184);
	public FixedLengthStringData svpfact48Err = new FixedLengthStringData(4).isAPartOf(filler1, 188);
	public FixedLengthStringData svpfact49Err = new FixedLengthStringData(4).isAPartOf(filler1, 192);
	public FixedLengthStringData svpfact50Err = new FixedLengthStringData(4).isAPartOf(filler1, 196);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(684).isAPartOf(dataArea, 544);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] divisorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData svpfactsOut = new FixedLengthStringData(600).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] svpfactOut = FLSArrayPartOfStructure(50, 12, svpfactsOut, 0);
	public FixedLengthStringData[][] svpfactO = FLSDArrayPartOfArrayStructure(12, 1, svpfactOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(600).isAPartOf(svpfactsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] svpfact01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] svpfact02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] svpfact03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] svpfact04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] svpfact05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] svpfact06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] svpfact07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] svpfact08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] svpfact09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] svpfact10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] svpfact11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] svpfact12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] svpfact13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] svpfact14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] svpfact15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public FixedLengthStringData[] svpfact16Out = FLSArrayPartOfStructure(12, 1, filler2, 180);
	public FixedLengthStringData[] svpfact17Out = FLSArrayPartOfStructure(12, 1, filler2, 192);
	public FixedLengthStringData[] svpfact18Out = FLSArrayPartOfStructure(12, 1, filler2, 204);
	public FixedLengthStringData[] svpfact19Out = FLSArrayPartOfStructure(12, 1, filler2, 216);
	public FixedLengthStringData[] svpfact20Out = FLSArrayPartOfStructure(12, 1, filler2, 228);
	public FixedLengthStringData[] svpfact21Out = FLSArrayPartOfStructure(12, 1, filler2, 240);
	public FixedLengthStringData[] svpfact22Out = FLSArrayPartOfStructure(12, 1, filler2, 252);
	public FixedLengthStringData[] svpfact23Out = FLSArrayPartOfStructure(12, 1, filler2, 264);
	public FixedLengthStringData[] svpfact24Out = FLSArrayPartOfStructure(12, 1, filler2, 276);
	public FixedLengthStringData[] svpfact25Out = FLSArrayPartOfStructure(12, 1, filler2, 288);
	public FixedLengthStringData[] svpfact26Out = FLSArrayPartOfStructure(12, 1, filler2, 300);
	public FixedLengthStringData[] svpfact27Out = FLSArrayPartOfStructure(12, 1, filler2, 312);
	public FixedLengthStringData[] svpfact28Out = FLSArrayPartOfStructure(12, 1, filler2, 324);
	public FixedLengthStringData[] svpfact29Out = FLSArrayPartOfStructure(12, 1, filler2, 336);
	public FixedLengthStringData[] svpfact30Out = FLSArrayPartOfStructure(12, 1, filler2, 348);
	public FixedLengthStringData[] svpfact31Out = FLSArrayPartOfStructure(12, 1, filler2, 360);
	public FixedLengthStringData[] svpfact32Out = FLSArrayPartOfStructure(12, 1, filler2, 372);
	public FixedLengthStringData[] svpfact33Out = FLSArrayPartOfStructure(12, 1, filler2, 384);
	public FixedLengthStringData[] svpfact34Out = FLSArrayPartOfStructure(12, 1, filler2, 396);
	public FixedLengthStringData[] svpfact35Out = FLSArrayPartOfStructure(12, 1, filler2, 408);
	public FixedLengthStringData[] svpfact36Out = FLSArrayPartOfStructure(12, 1, filler2, 420);
	public FixedLengthStringData[] svpfact37Out = FLSArrayPartOfStructure(12, 1, filler2, 432);
	public FixedLengthStringData[] svpfact38Out = FLSArrayPartOfStructure(12, 1, filler2, 444);
	public FixedLengthStringData[] svpfact39Out = FLSArrayPartOfStructure(12, 1, filler2, 456);
	public FixedLengthStringData[] svpfact40Out = FLSArrayPartOfStructure(12, 1, filler2, 468);
	public FixedLengthStringData[] svpfact41Out = FLSArrayPartOfStructure(12, 1, filler2, 480);
	public FixedLengthStringData[] svpfact42Out = FLSArrayPartOfStructure(12, 1, filler2, 492);
	public FixedLengthStringData[] svpfact43Out = FLSArrayPartOfStructure(12, 1, filler2, 504);
	public FixedLengthStringData[] svpfact44Out = FLSArrayPartOfStructure(12, 1, filler2, 516);
	public FixedLengthStringData[] svpfact45Out = FLSArrayPartOfStructure(12, 1, filler2, 528);
	public FixedLengthStringData[] svpfact46Out = FLSArrayPartOfStructure(12, 1, filler2, 540);
	public FixedLengthStringData[] svpfact47Out = FLSArrayPartOfStructure(12, 1, filler2, 552);
	public FixedLengthStringData[] svpfact48Out = FLSArrayPartOfStructure(12, 1, filler2, 564);
	public FixedLengthStringData[] svpfact49Out = FLSArrayPartOfStructure(12, 1, filler2, 576);
	public FixedLengthStringData[] svpfact50Out = FLSArrayPartOfStructure(12, 1, filler2, 588);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6656screenWritten = new LongData(0);
	public LongData S6656protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6656ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, svpFactor01, svpFactor02, svpFactor03, svpFactor04, svpFactor05, svpFactor06, svpFactor07, svpFactor08, svpFactor09, svpFactor10, svpFactor11, svpFactor12, svpFactor13, svpFactor14, svpFactor15, svpFactor16, svpFactor17, svpFactor18, svpFactor19, svpFactor20, svpFactor21, svpFactor22, svpFactor23, svpFactor24, svpFactor25, svpFactor26, svpFactor27, svpFactor28, svpFactor29, svpFactor30, svpFactor31, svpFactor32, svpFactor33, svpFactor34, svpFactor35, svpFactor36, svpFactor37, svpFactor38, svpFactor39, svpFactor40, svpFactor41, svpFactor42, svpFactor43, svpFactor44, svpFactor45, svpFactor46, svpFactor47, svpFactor48, svpFactor49, svpFactor50, divisor};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, svpfact01Out, svpfact02Out, svpfact03Out, svpfact04Out, svpfact05Out, svpfact06Out, svpfact07Out, svpfact08Out, svpfact09Out, svpfact10Out, svpfact11Out, svpfact12Out, svpfact13Out, svpfact14Out, svpfact15Out, svpfact16Out, svpfact17Out, svpfact18Out, svpfact19Out, svpfact20Out, svpfact21Out, svpfact22Out, svpfact23Out, svpfact24Out, svpfact25Out, svpfact26Out, svpfact27Out, svpfact28Out, svpfact29Out, svpfact30Out, svpfact31Out, svpfact32Out, svpfact33Out, svpfact34Out, svpfact35Out, svpfact36Out, svpfact37Out, svpfact38Out, svpfact39Out, svpfact40Out, svpfact41Out, svpfact42Out, svpfact43Out, svpfact44Out, svpfact45Out, svpfact46Out, svpfact47Out, svpfact48Out, svpfact49Out, svpfact50Out, divisorOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, svpfact01Err, svpfact02Err, svpfact03Err, svpfact04Err, svpfact05Err, svpfact06Err, svpfact07Err, svpfact08Err, svpfact09Err, svpfact10Err, svpfact11Err, svpfact12Err, svpfact13Err, svpfact14Err, svpfact15Err, svpfact16Err, svpfact17Err, svpfact18Err, svpfact19Err, svpfact20Err, svpfact21Err, svpfact22Err, svpfact23Err, svpfact24Err, svpfact25Err, svpfact26Err, svpfact27Err, svpfact28Err, svpfact29Err, svpfact30Err, svpfact31Err, svpfact32Err, svpfact33Err, svpfact34Err, svpfact35Err, svpfact36Err, svpfact37Err, svpfact38Err, svpfact39Err, svpfact40Err, svpfact41Err, svpfact42Err, svpfact43Err, svpfact44Err, svpfact45Err, svpfact46Err, svpfact47Err, svpfact48Err, svpfact49Err, svpfact50Err, divisorErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6656screen.class;
		protectRecord = S6656protect.class;
	}

}
