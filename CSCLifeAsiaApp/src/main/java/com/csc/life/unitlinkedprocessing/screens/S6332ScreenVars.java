package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6332
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6332ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(364);
	public FixedLengthStringData dataFields = new FixedLengthStringData(108).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData liencds = new FixedLengthStringData(12).isAPartOf(dataFields, 25);
	public FixedLengthStringData[] liencd = FLSArrayPartOfStructure(6, 2, liencds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(liencds, 0, FILLER_REDEFINE);
	public FixedLengthStringData liencd01 = DD.liencd.copy().isAPartOf(filler,0);
	public FixedLengthStringData liencd02 = DD.liencd.copy().isAPartOf(filler,2);
	public FixedLengthStringData liencd03 = DD.liencd.copy().isAPartOf(filler,4);
	public FixedLengthStringData liencd04 = DD.liencd.copy().isAPartOf(filler,6);
	public FixedLengthStringData liencd05 = DD.liencd.copy().isAPartOf(filler,8);
	public FixedLengthStringData liencd06 = DD.liencd.copy().isAPartOf(filler,10);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,37);
	public ZonedDecimalData premmult = DD.premmult.copyToZonedDecimal().isAPartOf(dataFields,67);
	public FixedLengthStringData rsvflg = DD.rsvflg.copy().isAPartOf(dataFields,72);
	public ZonedDecimalData sumInsMax = DD.suminsmax.copyToZonedDecimal().isAPartOf(dataFields,73);
	public ZonedDecimalData sumInsMin = DD.suminsmin.copyToZonedDecimal().isAPartOf(dataFields,88);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 108);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData liencdsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] liencdErr = FLSArrayPartOfStructure(6, 4, liencdsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(liencdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData liencd01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData liencd02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData liencd03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData liencd04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData liencd05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData liencd06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData premmultErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData rsvflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData suminsmaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData suminsminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 172);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData liencdsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(6, 12, liencdsOut, 0);
	public FixedLengthStringData[][] liencdO = FLSDArrayPartOfArrayStructure(12, 1, liencdOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(72).isAPartOf(liencdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] liencd01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] liencd02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] liencd03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] liencd04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] liencd05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] liencd06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] premmultOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] rsvflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] suminsmaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] suminsminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6332screenWritten = new LongData(0);
	public LongData S6332protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6332ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itmfrmOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itmtoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsminOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premmultOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsmaxOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rsvflgOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencd01Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencd02Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencd03Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencd04Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencd05Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencd06Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, sumInsMin, premmult, sumInsMax, rsvflg, liencd01, liencd02, liencd03, liencd04, liencd05, liencd06};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, suminsminOut, premmultOut, suminsmaxOut, rsvflgOut, liencd01Out, liencd02Out, liencd03Out, liencd04Out, liencd05Out, liencd06Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, suminsminErr, premmultErr, suminsmaxErr, rsvflgErr, liencd01Err, liencd02Err, liencd03Err, liencd04Err, liencd05Err, liencd06Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6332screen.class;
		protectRecord = S6332protect.class;
	}

}
