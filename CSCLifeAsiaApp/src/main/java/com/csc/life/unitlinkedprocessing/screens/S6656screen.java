package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6656screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6656ScreenVars sv = (S6656ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6656screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6656ScreenVars screenVars = (S6656ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.svpFactor01.setClassString("");
		screenVars.svpFactor02.setClassString("");
		screenVars.svpFactor03.setClassString("");
		screenVars.svpFactor04.setClassString("");
		screenVars.svpFactor05.setClassString("");
		screenVars.svpFactor06.setClassString("");
		screenVars.svpFactor07.setClassString("");
		screenVars.svpFactor08.setClassString("");
		screenVars.svpFactor09.setClassString("");
		screenVars.svpFactor10.setClassString("");
		screenVars.svpFactor11.setClassString("");
		screenVars.svpFactor12.setClassString("");
		screenVars.svpFactor13.setClassString("");
		screenVars.svpFactor14.setClassString("");
		screenVars.svpFactor15.setClassString("");
		screenVars.svpFactor16.setClassString("");
		screenVars.svpFactor17.setClassString("");
		screenVars.svpFactor18.setClassString("");
		screenVars.svpFactor19.setClassString("");
		screenVars.svpFactor20.setClassString("");
		screenVars.svpFactor21.setClassString("");
		screenVars.svpFactor22.setClassString("");
		screenVars.svpFactor23.setClassString("");
		screenVars.svpFactor24.setClassString("");
		screenVars.svpFactor25.setClassString("");
		screenVars.svpFactor26.setClassString("");
		screenVars.svpFactor27.setClassString("");
		screenVars.svpFactor28.setClassString("");
		screenVars.svpFactor29.setClassString("");
		screenVars.svpFactor30.setClassString("");
		screenVars.svpFactor31.setClassString("");
		screenVars.svpFactor32.setClassString("");
		screenVars.svpFactor33.setClassString("");
		screenVars.svpFactor34.setClassString("");
		screenVars.svpFactor35.setClassString("");
		screenVars.svpFactor36.setClassString("");
		screenVars.svpFactor37.setClassString("");
		screenVars.svpFactor38.setClassString("");
		screenVars.svpFactor39.setClassString("");
		screenVars.svpFactor40.setClassString("");
		screenVars.svpFactor41.setClassString("");
		screenVars.svpFactor42.setClassString("");
		screenVars.svpFactor43.setClassString("");
		screenVars.svpFactor44.setClassString("");
		screenVars.svpFactor45.setClassString("");
		screenVars.svpFactor46.setClassString("");
		screenVars.svpFactor47.setClassString("");
		screenVars.svpFactor48.setClassString("");
		screenVars.svpFactor49.setClassString("");
		screenVars.svpFactor50.setClassString("");
		screenVars.divisor.setClassString("");
	}

/**
 * Clear all the variables in S6656screen
 */
	public static void clear(VarModel pv) {
		S6656ScreenVars screenVars = (S6656ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.svpFactor01.clear();
		screenVars.svpFactor02.clear();
		screenVars.svpFactor03.clear();
		screenVars.svpFactor04.clear();
		screenVars.svpFactor05.clear();
		screenVars.svpFactor06.clear();
		screenVars.svpFactor07.clear();
		screenVars.svpFactor08.clear();
		screenVars.svpFactor09.clear();
		screenVars.svpFactor10.clear();
		screenVars.svpFactor11.clear();
		screenVars.svpFactor12.clear();
		screenVars.svpFactor13.clear();
		screenVars.svpFactor14.clear();
		screenVars.svpFactor15.clear();
		screenVars.svpFactor16.clear();
		screenVars.svpFactor17.clear();
		screenVars.svpFactor18.clear();
		screenVars.svpFactor19.clear();
		screenVars.svpFactor20.clear();
		screenVars.svpFactor21.clear();
		screenVars.svpFactor22.clear();
		screenVars.svpFactor23.clear();
		screenVars.svpFactor24.clear();
		screenVars.svpFactor25.clear();
		screenVars.svpFactor26.clear();
		screenVars.svpFactor27.clear();
		screenVars.svpFactor28.clear();
		screenVars.svpFactor29.clear();
		screenVars.svpFactor30.clear();
		screenVars.svpFactor31.clear();
		screenVars.svpFactor32.clear();
		screenVars.svpFactor33.clear();
		screenVars.svpFactor34.clear();
		screenVars.svpFactor35.clear();
		screenVars.svpFactor36.clear();
		screenVars.svpFactor37.clear();
		screenVars.svpFactor38.clear();
		screenVars.svpFactor39.clear();
		screenVars.svpFactor40.clear();
		screenVars.svpFactor41.clear();
		screenVars.svpFactor42.clear();
		screenVars.svpFactor43.clear();
		screenVars.svpFactor44.clear();
		screenVars.svpFactor45.clear();
		screenVars.svpFactor46.clear();
		screenVars.svpFactor47.clear();
		screenVars.svpFactor48.clear();
		screenVars.svpFactor49.clear();
		screenVars.svpFactor50.clear();
		screenVars.divisor.clear();
	}
}
