package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UswhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:54
 * Class transformed from USWHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UswhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 16;
	public FixedLengthStringData uswhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData uswhpfRecord = uswhrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(uswhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(uswhrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(uswhrec);
	public FixedLengthStringData switchRule = DD.swtchrule.copy().isAPartOf(uswhrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UswhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UswhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UswhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UswhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UswhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UswhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UswhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("USWHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"SWTCHRULE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     switchRule,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		switchRule.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUswhrec() {
  		return uswhrec;
	}

	public FixedLengthStringData getUswhpfRecord() {
  		return uswhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUswhrec(what);
	}

	public void setUswhrec(Object what) {
  		this.uswhrec.set(what);
	}

	public void setUswhpfRecord(Object what) {
  		this.uswhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(uswhrec.getLength());
		result.set(uswhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}