package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:20
 * Description:
 * Copybook name: UDELPRCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Udelprckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData udelprcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData udelprcKey = new FixedLengthStringData(64).isAPartOf(udelprcFileKey, 0, REDEFINE);
  	public FixedLengthStringData udelprcChdrcoy = new FixedLengthStringData(1).isAPartOf(udelprcKey, 0);
  	public FixedLengthStringData udelprcChdrnum = new FixedLengthStringData(8).isAPartOf(udelprcKey, 1);
  	public PackedDecimalData udelprcProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(udelprcKey, 9);
  	public FixedLengthStringData udelprcUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(udelprcKey, 11);
  	public FixedLengthStringData udelprcUnitType = new FixedLengthStringData(1).isAPartOf(udelprcKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(udelprcKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(udelprcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		udelprcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}