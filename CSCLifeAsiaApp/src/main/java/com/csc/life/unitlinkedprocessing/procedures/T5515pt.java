/*
 * File: T5515pt.java
 * Date: 30 August 2009 2:21:19
 * Author: Quipoz Limited
 * 
 * Class transformed from T5515PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5515.
*
*
*****************************************************************
* </pre>
*/
public class T5515pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Non-Traditional Fund - Create                  S5515");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine003, 9, FILLER).init("From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 17);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 27, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 32, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(33);
	private FixedLengthStringData filler11 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 9, FILLER).init("Fund Type (D,U):");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 32);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(73);
	private FixedLengthStringData filler13 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine005, 9, FILLER).init("Fund Currency:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 32);
	private FixedLengthStringData filler15 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine005, 35, FILLER).init("    Management Charge (%):");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler16 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine006, 9, FILLER).init("Unit Types: (I,A,B):");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 32);
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 33, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine006, 39, FILLER).init("Price change tolerance (%):");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(4, 2).isAPartOf(wsaaPrtLine006, 68).setPattern("ZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(64);
	private FixedLengthStringData filler20 = new FixedLengthStringData(39).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler21 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine007, 39, FILLER).init("Initial      Accumulation");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(61);
	private FixedLengthStringData filler22 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine008, 9, FILLER).init("Rounding Of Bid/Offer:");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 2).isAPartOf(wsaaPrtLine008, 41).setPattern("Z.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 45, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 2).isAPartOf(wsaaPrtLine008, 57).setPattern("Z.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(64);
	private FixedLengthStringData filler25 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine009, 9, FILLER).init("Bid/Offer Differential:");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(8, 5).isAPartOf(wsaaPrtLine009, 39).setPattern("ZZZ.ZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(8, 5).isAPartOf(wsaaPrtLine009, 55).setPattern("ZZZ.ZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(70);
	private FixedLengthStringData filler28 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(60).isAPartOf(wsaaPrtLine010, 9, FILLER).init("Initial and Accumulation Units to have same details Y/N ?:");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 69);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(55);
	private FixedLengthStringData filler30 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler31 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine011, 9, FILLER).init("Fund Switch Basis - Bid to Bid:");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 54);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(55);
	private FixedLengthStringData filler32 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine012, 29, FILLER).init("Bid to Offer:");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 54);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(75);
	private FixedLengthStringData filler34 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine013, 29, FILLER).init("Bid to Discounted Offer:");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 54);
	private FixedLengthStringData filler36 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 55, FILLER).init("  Discount %:");
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 69).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(55);
	private FixedLengthStringData filler37 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine014, 29, FILLER).init("Offer to Offer:");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 54);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5515rec t5515rec = new T5515rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5515pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5515rec.t5515Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo008.set(t5515rec.currcode);
		fieldNo009.set(t5515rec.managementCharge);
		fieldNo010.set(t5515rec.unitType);
		fieldNo011.set(t5515rec.tolerance);
		fieldNo012.set(t5515rec.initialRounding);
		fieldNo013.set(t5515rec.accumRounding);
		fieldNo014.set(t5515rec.initBidOffer);
		fieldNo015.set(t5515rec.acumbof);
		fieldNo016.set(t5515rec.initAccumSame);
		fieldNo017.set(t5515rec.btobid);
		fieldNo018.set(t5515rec.btooff);
		fieldNo019.set(t5515rec.btodisc);
		fieldNo021.set(t5515rec.otoff);
		fieldNo020.set(t5515rec.discountOfferPercent);
		fieldNo007.set(t5515rec.zfundtyp);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
