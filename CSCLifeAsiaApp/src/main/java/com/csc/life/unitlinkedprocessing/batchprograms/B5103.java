/*
 * File: B5103.java
 * Date: 29 August 2009 20:55:04
 * Author: Quipoz Limited
 * 
 * Class transformed from B5103.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.CovupfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UderpfDAO;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This program is the Coverage Debt Recovery Splitter program
* to be run before Coverage Debt Recovery (B5104).
*
* An SQL Select is used to extract the full coverage key from
* COVRPF where the company is equal to the process company, the
* currto is greater or equal to the effdate of the schedule, the
* coverage statcode is not equal to the T5679 statcode, the
* coverage validflag = '1', the contract number is within the
* parameter range and finally - there are no undealt UTRNs for
* the coverage which could change the coverage fund holding.
*
* The extracted COVRs are sorted into contract sequence and
* all COVRs for a contract are written to one temporary file
* member so that B5104, which can be run in multiple threads,
* does not get softlock conflicts while locking the contract.
*
* Control totals used in this program:
*
*    01  -  No. of COVR extracted records
*    02  -  No. of thread members
*
*
*****************************************************************
* </pre>
*/
public class B5103 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlcovrCursorrs = null;
	private java.sql.PreparedStatement sqlcovrCursorps = null;
	private java.sql.Connection sqlcovrCursorconn = null;
	private DiskFileDAM covu01 = new DiskFileDAM("COVU01");
	private DiskFileDAM covu02 = new DiskFileDAM("COVU02");
	private DiskFileDAM covu03 = new DiskFileDAM("COVU03");
	private DiskFileDAM covu04 = new DiskFileDAM("COVU04");
	private DiskFileDAM covu05 = new DiskFileDAM("COVU05");
	private DiskFileDAM covu06 = new DiskFileDAM("COVU06");
	private DiskFileDAM covu07 = new DiskFileDAM("COVU07");
	private DiskFileDAM covu08 = new DiskFileDAM("COVU08");
	private DiskFileDAM covu09 = new DiskFileDAM("COVU09");
	private DiskFileDAM covu10 = new DiskFileDAM("COVU10");
	private DiskFileDAM covu11 = new DiskFileDAM("COVU11");
	private DiskFileDAM covu12 = new DiskFileDAM("COVU12");
	private DiskFileDAM covu13 = new DiskFileDAM("COVU13");
	private DiskFileDAM covu14 = new DiskFileDAM("COVU14");
	private DiskFileDAM covu15 = new DiskFileDAM("COVU15");
	private DiskFileDAM covu16 = new DiskFileDAM("COVU16");
	private DiskFileDAM covu17 = new DiskFileDAM("COVU17");
	private DiskFileDAM covu18 = new DiskFileDAM("COVU18");
	private DiskFileDAM covu19 = new DiskFileDAM("COVU19");
	private DiskFileDAM covu20 = new DiskFileDAM("COVU20");
	private CovupfTableDAM covupfData = new CovupfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5103");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

		/*  COVU member parametres.*/
	private FixedLengthStringData wsaaCovuFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCovuFn, 0, FILLER).init("COVU");
	private FixedLengthStringData wsaaCovuRunid = new FixedLengthStringData(2).isAPartOf(wsaaCovuFn, 4);
	private ZonedDecimalData wsaaCovuJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCovuFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
		/*  Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

		/*  SQL error message formatting.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/
	private PackedDecimalData wsaaRowsInBlock = new PackedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaCovrData = FLSInittedArray (1000, 18);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaCovrData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 9);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 11);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 13);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaCovrData, 15);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaCovrInd = FLSInittedArray (1000, 12);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(6, 4, 0, wsaaCovrInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaT5679RiskStat = new FixedLengthStringData(2).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private int ct01Value = 0;
	private int ct02Value = 0;
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String itemrec = "ITEMREC   ";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private P6671par p6671par = new P6671par();
	private T5679rec t5679rec = new T5679rec();
	private static final String  FMC_FEATURE_ID = "BTPRO026";
	private boolean fmcOnFlag = false;
	private UderpfDAO uderpfDAO = getApplicationContext().getBean("uderpfDAO", UderpfDAO.class);
	private static final String FMC = "FMC";
	private ZonedDecimalData wsaaNextdate = new ZonedDecimalData(8, 0).init(0);
	private Datcon4rec datcon4rec = new Datcon4rec();
	
	public B5103() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaCovuRunid.set(bprdIO.getSystemParam04());
		wsaaCovuJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		fmcOnFlag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString().trim(), FMC_FEATURE_ID, appVars, "IT");
		/*if(fmcOnFlag && isNE(bprdIO.getSystemParam05(),FMC)){
			return;
		}*/
		/*    Log the number of threads being used*/
		ct01Value += bprdIO.getThreadsSubsqntProc().toInt();
		/*    Prepare the host effective date from the parameter screen*/
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaCompany.set(bsprIO.getCompany());
		iy.set(1);
		/*    Read table T5679 to get the component status.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		/*  MOVE BPRD-AUTH-CODE         TO ITEM-ITEMITEM.                */
		itemIO.setItemitem(bprdIO.getSystemParam02());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaT5679RiskStat.set(t5679rec.setCovRiskStat);
		if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){	
			datcon4rec.frequency.set("12");
			datcon4rec.freqFactor.set(1);
			datcon4rec.intDate1.set(bsscIO.getEffectiveDate());
			datcon4rec.intDate2.set(0);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaNextdate.set(datcon4rec.intDate2.toInt());
		}
		StringBuilder sqlcovrCursor = new StringBuilder(" SELECT  C.CHDRCOY, C.CHDRNUM, C.LIFE, C.COVERAGE, C.RIDER, C.PLNSFX FROM  "); 
		sqlcovrCursor.append(getAppVars().getTableNameOverriden("COVRPF"));
		sqlcovrCursor.append(" C ");
		if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
			sqlcovrCursor.append(" INNER JOIN ");
			sqlcovrCursor.append(getAppVars().getTableNameOverriden("ZFMCPF Z ON  Z.CHDRNUM=C.CHDRNUM INNER JOIN PAYRPF P ON Z.CHDRNUM=P.CHDRNUM"));
		}
		sqlcovrCursor.append(" WHERE C.CHDRCOY = ?");
		sqlcovrCursor.append(" AND C.CURRTO >= ?");
		sqlcovrCursor.append(" AND C.STATCODE <> ?");
		sqlcovrCursor.append(" AND C.VALIDFLAG = ?");
		sqlcovrCursor.append(" AND C.CRDEBT <> 0");
		sqlcovrCursor.append(" AND C.CHDRNUM >= ?");
		sqlcovrCursor.append(" AND C.CHDRNUM <= ?");
		sqlcovrCursor.append(" AND C.CRRCD <= ?");
		if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
			sqlcovrCursor.append(" AND Z.CHDRCOY = ? AND Z.ZFMCDAT <> 0 AND Z.VALIDFLAG = ? AND Z.ZFMCDAT <= ? AND P.BTDATE=P.PTDATE AND P.VALIDFLAG = ?");
		}
		sqlcovrCursor.append(" ORDER BY C.CHDRCOY, C.CHDRNUM, C.LIFE, C.COVERAGE, C.RIDER, C.PLNSFX");
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlcovrCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlcovrCursorps = getAppVars().prepareStatementEmbeded(sqlcovrCursorconn, sqlcovrCursor.toString(), "COVRPF");
			getAppVars().setDBString(sqlcovrCursorps, 1, wsaaCompany);
			getAppVars().setDBNumber(sqlcovrCursorps, 2, wsaaEffdate);
			getAppVars().setDBString(sqlcovrCursorps, 3, wsaaT5679RiskStat);
			getAppVars().setDBString(sqlcovrCursorps, 4, wsaa1);
			getAppVars().setDBString(sqlcovrCursorps, 5, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlcovrCursorps, 6, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqlcovrCursorps, 7, wsaaEffdate);
			if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
				getAppVars().setDBString(sqlcovrCursorps, 8, wsaaCompany);
				getAppVars().setDBString(sqlcovrCursorps, 9, wsaa1);
				getAppVars().setDBNumber(sqlcovrCursorps, 10, wsaaNextdate);
				getAppVars().setDBString(sqlcovrCursorps, 11, wsaa1);
			}
			sqlcovrCursorrs = getAppVars().executeQuery(sqlcovrCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}


protected void openThreadMember1100()
	{
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaCovuFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaCovuFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(COVU");
		stringVariable2.addExpression(iz);
		stringVariable2.addExpression(") TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaCovuFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.addExpression(" SEQONLY(*YES 1000)");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			covu01.openOutput();
		}
		if (isEQ(iz, 2)) {
			covu02.openOutput();
		}
		if (isEQ(iz, 3)) {
			covu03.openOutput();
		}
		if (isEQ(iz, 4)) {
			covu04.openOutput();
		}
		if (isEQ(iz, 5)) {
			covu05.openOutput();
		}
		if (isEQ(iz, 6)) {
			covu06.openOutput();
		}
		if (isEQ(iz, 7)) {
			covu07.openOutput();
		}
		if (isEQ(iz, 8)) {
			covu08.openOutput();
		}
		if (isEQ(iz, 9)) {
			covu09.openOutput();
		}
		if (isEQ(iz, 10)) {
			covu10.openOutput();
		}
		if (isEQ(iz, 11)) {
			covu11.openOutput();
		}
		if (isEQ(iz, 12)) {
			covu12.openOutput();
		}
		if (isEQ(iz, 13)) {
			covu13.openOutput();
		}
		if (isEQ(iz, 14)) {
			covu14.openOutput();
		}
		if (isEQ(iz, 15)) {
			covu15.openOutput();
		}
		if (isEQ(iz, 16)) {
			covu16.openOutput();
		}
		if (isEQ(iz, 17)) {
			covu17.openOutput();
		}
		if (isEQ(iz, 18)) {
			covu18.openOutput();
		}
		if (isEQ(iz, 19)) {
			covu19.openOutput();
		}
		if (isEQ(iz, 20)) {
			covu20.openOutput();
		}
	}


protected void initialiseArray1500()
	{
		/*START*/
		wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaRider[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (int covrCursorLoopIndex = 1; isLTE(covrCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlcovrCursorrs); covrCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlcovrCursorrs, 1, wsaaChdrcoy[covrCursorLoopIndex], wsaaNullInd[covrCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlcovrCursorrs, 2, wsaaChdrnum[covrCursorLoopIndex], wsaaNullInd[covrCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlcovrCursorrs, 3, wsaaLife[covrCursorLoopIndex], wsaaNullInd[covrCursorLoopIndex][3]);
				getAppVars().getDBObject(sqlcovrCursorrs, 4, wsaaCoverage[covrCursorLoopIndex], wsaaNullInd[covrCursorLoopIndex][4]);
				getAppVars().getDBObject(sqlcovrCursorrs, 5, wsaaRider[covrCursorLoopIndex], wsaaNullInd[covrCursorLoopIndex][5]);
				getAppVars().getDBObject(sqlcovrCursorrs, 6, wsaaPlnsfx[covrCursorLoopIndex], wsaaNullInd[covrCursorLoopIndex][6]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  An SQLCODE = +100 is returned when :-*/
		/*      a) no rows are returned on the first fetch*/
		/*      b) the last row of the cursor is either in the block or is*/
		/*         the last row of the block.*/
		/*  We must continue processing to the 3000- section for case b)*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}  else if (isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			/*  On the first entry to the program we must set up the*/
			/*  WSAA-PREV-CHDRNUM = present chdrnum.*/
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}

	}


protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		/*  Load threads until end of array or an incomplete block is*/
		/*  detected.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the CHDRNUM being processed is not equal to the previous*/
		/*  CHDRNUM then we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last CHDRNUM*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next COVU member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			iy.add(1);
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		}
		/*  Load from storage all COVU data for the same contract until*/
		/*  the CHDRNUM on COVU has changed.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		covupfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		covupfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		covupfData.life.set(wsaaLife[wsaaInd.toInt()]);
		covupfData.coverage.set(wsaaCoverage[wsaaInd.toInt()]);
		covupfData.rider.set(wsaaRider[wsaaInd.toInt()]);
		covupfData.planSuffix.set(wsaaPlnsfx[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy, 1)) {
			covu01.write(covupfData);
		}
		if (isEQ(iy, 2)) {
			covu02.write(covupfData);
		}
		if (isEQ(iy, 3)) {
			covu03.write(covupfData);
		}
		if (isEQ(iy, 4)) {
			covu04.write(covupfData);
		}
		if (isEQ(iy, 5)) {
			covu05.write(covupfData);
		}
		if (isEQ(iy, 6)) {
			covu06.write(covupfData);
		}
		if (isEQ(iy, 7)) {
			covu07.write(covupfData);
		}
		if (isEQ(iy, 8)) {
			covu08.write(covupfData);
		}
		if (isEQ(iy, 9)) {
			covu09.write(covupfData);
		}
		if (isEQ(iy, 10)) {
			covu10.write(covupfData);
		}
		if (isEQ(iy, 11)) {
			covu11.write(covupfData);
		}
		if (isEQ(iy, 12)) {
			covu12.write(covupfData);
		}
		if (isEQ(iy, 13)) {
			covu13.write(covupfData);
		}
		if (isEQ(iy, 14)) {
			covu14.write(covupfData);
		}
		if (isEQ(iy, 15)) {
			covu15.write(covupfData);
		}
		if (isEQ(iy, 16)) {
			covu16.write(covupfData);
		}
		if (isEQ(iy, 17)) {
			covu17.write(covupfData);
		}
		if (isEQ(iy, 18)) {
			covu18.write(covupfData);
		}
		if (isEQ(iy, 19)) {
			covu19.write(covupfData);
		}
		if (isEQ(iy, 20)) {
			covu20.write(covupfData);
		}
		/*    Log the number of extracted records.*/
		ct02Value++;
		/*  Set up the array for the next block of records.*/
		/*  Otherwise process the next record in the array.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/
		//ILIFE-1337 - bpham7
		if (isLTE(wsaaInd, wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsaaEofInBlock.set("Y");
		}

	}


protected void commit3500()
	{
		/*COMMIT*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callProgram(Contot.class, contotrec.contotRec);		
		ct01Value = 0;
	
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct02Value = 0;
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		clearUdErrorFile4010();
		closeFiles4010();
	}

protected void clearUdErrorFile4010()
	{
		/*    Clear the Unit Dealing Error file of all previous errors.*/
		uderpfDAO.deleteUderpfRecordByRC("COVR");
	}

protected void closeFiles4010()
	{
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlcovrCursorconn, sqlcovrCursorps, sqlcovrCursorrs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		if (isEQ(iz, 1)) {
			covu01.close();
		}
		if (isEQ(iz, 2)) {
			covu02.close();
		}
		if (isEQ(iz, 3)) {
			covu03.close();
		}
		if (isEQ(iz, 4)) {
			covu04.close();
		}
		if (isEQ(iz, 5)) {
			covu05.close();
		}
		if (isEQ(iz, 6)) {
			covu06.close();
		}
		if (isEQ(iz, 7)) {
			covu07.close();
		}
		if (isEQ(iz, 8)) {
			covu08.close();
		}
		if (isEQ(iz, 9)) {
			covu09.close();
		}
		if (isEQ(iz, 10)) {
			covu10.close();
		}
		if (isEQ(iz, 11)) {
			covu11.close();
		}
		if (isEQ(iz, 12)) {
			covu12.close();
		}
		if (isEQ(iz, 13)) {
			covu13.close();
		}
		if (isEQ(iz, 14)) {
			covu14.close();
		}
		if (isEQ(iz, 15)) {
			covu15.close();
		}
		if (isEQ(iz, 16)) {
			covu16.close();
		}
		if (isEQ(iz, 17)) {
			covu17.close();
		}
		if (isEQ(iz, 18)) {
			covu18.close();
		}
		if (isEQ(iz, 19)) {
			covu19.close();
		}
		if (isEQ(iz, 20)) {
			covu20.close();
		}
	}


protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}