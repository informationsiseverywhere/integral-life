package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:08
 * Description:
 * Copybook name: VPRCUPRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprcuprkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprcuprFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprcuprKey = new FixedLengthStringData(256).isAPartOf(vprcuprFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprcuprCompany = new FixedLengthStringData(1).isAPartOf(vprcuprKey, 0);
  	public FixedLengthStringData vprcuprUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprcuprKey, 1);
  	public FixedLengthStringData vprcuprUnitType = new FixedLengthStringData(1).isAPartOf(vprcuprKey, 5);
  	public PackedDecimalData vprcuprEffdate = new PackedDecimalData(8, 0).isAPartOf(vprcuprKey, 6);
  	public PackedDecimalData vprcuprJobno = new PackedDecimalData(8, 0).isAPartOf(vprcuprKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(vprcuprKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprcuprFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprcuprFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}