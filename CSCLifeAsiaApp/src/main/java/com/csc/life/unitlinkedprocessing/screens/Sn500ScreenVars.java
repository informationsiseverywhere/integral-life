package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SN500
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sn500ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1914);
	public FixedLengthStringData dataFields = new FixedLengthStringData(698).isAPartOf(dataArea, 0);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData currcys = new FixedLengthStringData(30).isAPartOf(dataFields, 64);
	public FixedLengthStringData[] currcy = FLSArrayPartOfStructure(10, 3, currcys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(currcys, 0, FILLER_REDEFINE);
	public FixedLengthStringData currcy01 = DD.currcy.copy().isAPartOf(filler,0);
	public FixedLengthStringData currcy02 = DD.currcy.copy().isAPartOf(filler,3);
	public FixedLengthStringData currcy03 = DD.currcy.copy().isAPartOf(filler,6);
	public FixedLengthStringData currcy04 = DD.currcy.copy().isAPartOf(filler,9);
	public FixedLengthStringData currcy05 = DD.currcy.copy().isAPartOf(filler,12);
	public FixedLengthStringData currcy06 = DD.currcy.copy().isAPartOf(filler,15);
	public FixedLengthStringData currcy07 = DD.currcy.copy().isAPartOf(filler,18);
	public FixedLengthStringData currcy08 = DD.currcy.copy().isAPartOf(filler,21);
	public FixedLengthStringData currcy09 = DD.currcy.copy().isAPartOf(filler,24);
	public FixedLengthStringData currcy10 = DD.currcy.copy().isAPartOf(filler,27);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,94);
	public ZonedDecimalData exrat = DD.exrat.copyToZonedDecimal().isAPartOf(dataFields,102);
	public FixedLengthStringData virtFundSplitMethod = DD.fndspl.copy().isAPartOf(dataFields,120);
	public ZonedDecimalData instprem = DD.instprem.copyToZonedDecimal().isAPartOf(dataFields,124);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,141);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,245);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,253);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,257);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(dataFields,258);
	public FixedLengthStringData percentAmountInd = DD.pcamtind.copy().isAPartOf(dataFields,261);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,262);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,266);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,276);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,279);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,281);
	public ZonedDecimalData susamt = DD.susamt.copyToZonedDecimal().isAPartOf(dataFields,298);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,315);
	public ZonedDecimalData totlprem = DD.totlprem.copyToZonedDecimal().isAPartOf(dataFields,332);
	public FixedLengthStringData unitAllocPercAmts = new FixedLengthStringData(170).isAPartOf(dataFields, 349);
	public ZonedDecimalData[] unitAllocPercAmt = ZDArrayPartOfStructure(10, 17, 2, unitAllocPercAmts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(170).isAPartOf(unitAllocPercAmts, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitAllocPercAmt01 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData unitAllocPercAmt02 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData unitAllocPercAmt03 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData unitAllocPercAmt04 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData unitAllocPercAmt05 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData unitAllocPercAmt06 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData unitAllocPercAmt07 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData unitAllocPercAmt08 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData unitAllocPercAmt09 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,136);
	public ZonedDecimalData unitAllocPercAmt10 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,153);
	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(40).isAPartOf(dataFields, 519);
	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(10, 4, unitVirtualFunds, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
	public FixedLengthStringData unitVirtualFund01 = DD.vrtfnd.copy().isAPartOf(filler2,0);
	public FixedLengthStringData unitVirtualFund02 = DD.vrtfnd.copy().isAPartOf(filler2,4);
	public FixedLengthStringData unitVirtualFund03 = DD.vrtfnd.copy().isAPartOf(filler2,8);
	public FixedLengthStringData unitVirtualFund04 = DD.vrtfnd.copy().isAPartOf(filler2,12);
	public FixedLengthStringData unitVirtualFund05 = DD.vrtfnd.copy().isAPartOf(filler2,16);
	public FixedLengthStringData unitVirtualFund06 = DD.vrtfnd.copy().isAPartOf(filler2,20);
	public FixedLengthStringData unitVirtualFund07 = DD.vrtfnd.copy().isAPartOf(filler2,24);
	public FixedLengthStringData unitVirtualFund08 = DD.vrtfnd.copy().isAPartOf(filler2,28);
	public FixedLengthStringData unitVirtualFund09 = DD.vrtfnd.copy().isAPartOf(filler2,32);
	public FixedLengthStringData unitVirtualFund10 = DD.vrtfnd.copy().isAPartOf(filler2,36);
	public ZonedDecimalData zrtopusum = DD.zrtopusum.copyToZonedDecimal().isAPartOf(dataFields,559);
	public ZonedDecimalData zrtotsumin = DD.zrtotsumin.copyToZonedDecimal().isAPartOf(dataFields,576);
	public FixedLengthStringData reserveUnitsInd = DD.rsunin.copy().isAPartOf(dataFields,593); // ILIFE-5450
	public ZonedDecimalData reserveUnitsDate = DD.rundte.copyToZonedDecimal().isAPartOf(dataFields,594); // ILIFE-5450	
	//ILIFE-8164- STARTS
	public FixedLengthStringData newFundLists = new FixedLengthStringData(48).isAPartOf(dataFields, 650);
	public FixedLengthStringData[] newFundList = FLSArrayPartOfStructure(12, 4, newFundLists, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(48).isAPartOf(newFundLists, 0, FILLER_REDEFINE);
	public FixedLengthStringData newFundList01 = DD.newFundList.copy().isAPartOf(filler12,0);
	public FixedLengthStringData newFundList02 = DD.newFundList.copy().isAPartOf(filler12,4);
	public FixedLengthStringData newFundList03 = DD.newFundList.copy().isAPartOf(filler12,8);
	public FixedLengthStringData newFundList04 = DD.newFundList.copy().isAPartOf(filler12,12);
	public FixedLengthStringData newFundList05 = DD.newFundList.copy().isAPartOf(filler12,16);
	public FixedLengthStringData newFundList06 = DD.newFundList.copy().isAPartOf(filler12,20);
	public FixedLengthStringData newFundList07 = DD.newFundList.copy().isAPartOf(filler12,24);
	public FixedLengthStringData newFundList08 = DD.newFundList.copy().isAPartOf(filler12,28);
	public FixedLengthStringData newFundList09 = DD.newFundList.copy().isAPartOf(filler12,32);
	public FixedLengthStringData newFundList10 = DD.newFundList.copy().isAPartOf(filler12,36);
	public FixedLengthStringData newFundList11 = DD.newFundList.copy().isAPartOf(filler12,40);
	public FixedLengthStringData newFundList12 = DD.newFundList.copy().isAPartOf(filler12,44);
	//ILIFE-8164- ENDS
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(304).isAPartOf(dataArea, 698);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData currcysErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] currcyErr = FLSArrayPartOfStructure(10, 4, currcysErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(currcysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData currcy01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData currcy02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData currcy03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData currcy04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData currcy05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData currcy06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData currcy07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData currcy08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData currcy09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData currcy10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData exratErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData fndsplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData instpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData paycurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData pcamtindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData susamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData totlpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData ualprcsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData[] ualprcErr = FLSArrayPartOfStructure(10, 4, ualprcsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(ualprcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ualprc01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData ualprc02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData ualprc03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData ualprc04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData ualprc05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData ualprc06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData ualprc07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData ualprc08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData ualprc09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData ualprc10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData vrtfndsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData[] vrtfndErr = FLSArrayPartOfStructure(10, 4, vrtfndsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(vrtfndsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData vrtfnd01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData vrtfnd02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData vrtfnd03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData vrtfnd04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData vrtfnd05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData vrtfnd06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData vrtfnd07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData vrtfnd08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData vrtfnd09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData vrtfnd10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData zrtopusumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData zrtotsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData rsuninErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	public FixedLengthStringData rundteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);
	//ILIFE-8164- STARTS
	public FixedLengthStringData newFundListErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 256);
	public FixedLengthStringData[] newFundListsErr = FLSArrayPartOfStructure(12, 4, newFundListErr, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(48).isAPartOf(newFundListErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData newFundList01Err = new FixedLengthStringData(4).isAPartOf(filler13, 0);
	public FixedLengthStringData newFundList02Err = new FixedLengthStringData(4).isAPartOf(filler13, 4);
	public FixedLengthStringData newFundList03Err = new FixedLengthStringData(4).isAPartOf(filler13, 8);
	public FixedLengthStringData newFundList04Err = new FixedLengthStringData(4).isAPartOf(filler13, 12);
	public FixedLengthStringData newFundList05Err = new FixedLengthStringData(4).isAPartOf(filler13, 16);
	public FixedLengthStringData newFundList06Err = new FixedLengthStringData(4).isAPartOf(filler13, 20);
	public FixedLengthStringData newFundList07Err = new FixedLengthStringData(4).isAPartOf(filler13, 24);
	public FixedLengthStringData newFundList08Err = new FixedLengthStringData(4).isAPartOf(filler13, 28);
	public FixedLengthStringData newFundList09Err = new FixedLengthStringData(4).isAPartOf(filler13, 32);
	public FixedLengthStringData newFundList10Err = new FixedLengthStringData(4).isAPartOf(filler13, 36);
	public FixedLengthStringData newFundList11Err = new FixedLengthStringData(4).isAPartOf(filler13, 40);
	public FixedLengthStringData newFundList12Err = new FixedLengthStringData(4).isAPartOf(filler13, 44);
	//ILIFE-8164- ENDS
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(912).isAPartOf(dataArea, 1002);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData currcysOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] currcyOut = FLSArrayPartOfStructure(10, 12, currcysOut, 0);
	public FixedLengthStringData[][] currcyO = FLSDArrayPartOfArrayStructure(12, 1, currcyOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(currcysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] currcy01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] currcy02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] currcy03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] currcy04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] currcy05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] currcy06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] currcy07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] currcy08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] currcy09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] currcy10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] exratOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] fndsplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] paycurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] pcamtindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] susamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] totlpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData ualprcsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 480);
	public FixedLengthStringData[] ualprcOut = FLSArrayPartOfStructure(10, 12, ualprcsOut, 0);
	public FixedLengthStringData[][] ualprcO = FLSDArrayPartOfArrayStructure(12, 1, ualprcOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(ualprcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ualprc01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] ualprc02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] ualprc03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] ualprc04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] ualprc05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] ualprc06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] ualprc07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] ualprc08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] ualprc09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] ualprc10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData vrtfndsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 600);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(10, 12, vrtfndsOut, 0);
	public FixedLengthStringData[][] vrtfndO = FLSDArrayPartOfArrayStructure(12, 1, vrtfndOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(vrtfndsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] vrtfnd01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] vrtfnd02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] vrtfnd03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] vrtfnd04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] vrtfnd05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] vrtfnd06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] vrtfnd07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] vrtfnd08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] vrtfnd09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] vrtfnd10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] zrtopusumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	public FixedLengthStringData[] zrtotsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] rsuninOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	public FixedLengthStringData[] rundteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);
	//ILIFE-8164- STARTS
	public FixedLengthStringData newFundListsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 768);
	public FixedLengthStringData[] newFundListOut = FLSArrayPartOfStructure(12, 12, newFundListsOut, 0);
	public FixedLengthStringData[][] newFundLisO = FLSDArrayPartOfArrayStructure(12, 1, newFundListOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(144).isAPartOf(newFundListsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] newFundList01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] newFundList02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] newFundList03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] newFundList04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] newFundList05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] newFundList06Out = FLSArrayPartOfStructure(12, 1, filler14, 60);
	public FixedLengthStringData[] newFundList07Out = FLSArrayPartOfStructure(12, 1, filler14, 72);
	public FixedLengthStringData[] newFundList08Out = FLSArrayPartOfStructure(12, 1, filler14, 84);
	public FixedLengthStringData[] newFundList09Out = FLSArrayPartOfStructure(12, 1, filler14, 96);
	public FixedLengthStringData[] newFundList10Out = FLSArrayPartOfStructure(12, 1, filler14, 108);
	public FixedLengthStringData[] newFundList11Out = FLSArrayPartOfStructure(12, 1, filler14, 120);
	public FixedLengthStringData[] newFundList12Out = FLSArrayPartOfStructure(12, 1, filler14, 132);
	//ILIFE-8164- ENDS
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sn500screenWritten = new LongData(0);
	public LongData Sn500protectWritten = new LongData(0);
	
	public FixedLengthStringData reserveUnitsDateDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return false;
	}


	public Sn500ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paycurrOut,new String[] {"18","19","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcamtindOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fndsplOut,new String[] {"13","60","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instpremOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd01Out,new String[] {"31","21","-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc01Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd02Out,new String[] {"32","22","-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc02Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd03Out,new String[] {"33","23","-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc03Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd04Out,new String[] {"34","24","-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc04Out,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd05Out,new String[] {"35","25","-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc05Out,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd06Out,new String[] {"36","26","-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc06Out,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd07Out,new String[] {"37","27","-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc07Out,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd08Out,new String[] {"38","28","-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc08Out,new String[] {"48",null, "-48",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd09Out,new String[] {"39","29","-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc09Out,new String[] {"49",null, "-49",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"60",null, "-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd10Out,new String[] {"40","30","-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc10Out,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrtotsuminOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrtopusumOut,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "51",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rsuninOut,new String[] {"70","31","-70","59",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rundteOut,new String[] {"71","32","-71","69",null, null, null, null, null, null, null, null});
		//ILIFE-8164 -STARTS 
		fieldIndMap.put(newFundList01Out,new String[] {"50","51","-50", "101", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList02Out,new String[] {"54","55","-54", "102", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList03Out,new String[] {"58","59","-58", "103", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList04Out,new String[] {"62","63","-62", "104", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList05Out,new String[] {"66","67","-66", "105", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList06Out,new String[] {"70","71","-70", "106", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList07Out,new String[] {"74","75","-74", "107", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList08Out,new String[] {"78","79","-78", "108", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList09Out,new String[] {"82","83","-82", "109", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList10Out,new String[] {"86","87","-86", "110", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList11Out,new String[] {"90","91","-90", "111", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList12Out,new String[] {"94","95","-94", "112", null, null, null, null, null, null, null, null});
		//ILIFE-8164 - ENDS
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, planSuffix, numpols, billcurr, life, coverage, rider, crtable, paycurr, exrat, effdate, percentAmountInd, virtFundSplitMethod, instprem, totlprem, susamt, unitVirtualFund01, currcy01, unitAllocPercAmt01, unitVirtualFund02, currcy02, unitAllocPercAmt02, unitVirtualFund03, currcy03, unitAllocPercAmt03, unitVirtualFund04, currcy04, unitAllocPercAmt04, unitVirtualFund05, currcy05, unitAllocPercAmt05, unitVirtualFund06, currcy06, unitAllocPercAmt06, unitVirtualFund07, currcy07, unitAllocPercAmt07, unitVirtualFund08, currcy08, unitAllocPercAmt08, unitVirtualFund09, currcy09, unitAllocPercAmt09, comind, unitVirtualFund10, currcy10, unitAllocPercAmt10, zrtotsumin, zrtopusum, optextind, sumins, taxamt, reserveUnitsInd, reserveUnitsDate, newFundList01, newFundList02, newFundList03, newFundList04, newFundList05, newFundList06, newFundList07, newFundList08, newFundList09, newFundList10, newFundList11, newFundList12};//ILIFE-8164
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, plnsfxOut, numpolsOut, billcurrOut, lifeOut, coverageOut, riderOut, crtableOut, paycurrOut, exratOut, effdateOut, pcamtindOut, fndsplOut, instpremOut, totlpremOut, susamtOut, vrtfnd01Out, currcy01Out, ualprc01Out, vrtfnd02Out, currcy02Out, ualprc02Out, vrtfnd03Out, currcy03Out, ualprc03Out, vrtfnd04Out, currcy04Out, ualprc04Out, vrtfnd05Out, currcy05Out, ualprc05Out, vrtfnd06Out, currcy06Out, ualprc06Out, vrtfnd07Out, currcy07Out, ualprc07Out, vrtfnd08Out, currcy08Out, ualprc08Out, vrtfnd09Out, currcy09Out, ualprc09Out, comindOut, vrtfnd10Out, currcy10Out, ualprc10Out, zrtotsuminOut, zrtopusumOut, optextindOut, suminsOut, taxamtOut, rsuninOut, rundteOut, newFundList01Out, newFundList02Out,newFundList03Out, newFundList04Out, newFundList05Out, newFundList06Out, newFundList07Out, newFundList08Out, newFundList09Out, newFundList10Out, newFundList11Out, newFundList12Out};//ILIFE-8164
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, plnsfxErr, numpolsErr, billcurrErr, lifeErr, coverageErr, riderErr, crtableErr, paycurrErr, exratErr, effdateErr, pcamtindErr, fndsplErr, instpremErr, totlpremErr, susamtErr, vrtfnd01Err, currcy01Err, ualprc01Err, vrtfnd02Err, currcy02Err, ualprc02Err, vrtfnd03Err, currcy03Err, ualprc03Err, vrtfnd04Err, currcy04Err, ualprc04Err, vrtfnd05Err, currcy05Err, ualprc05Err, vrtfnd06Err, currcy06Err, ualprc06Err, vrtfnd07Err, currcy07Err, ualprc07Err, vrtfnd08Err, currcy08Err, ualprc08Err, vrtfnd09Err, currcy09Err, ualprc09Err, comindErr, vrtfnd10Err, currcy10Err, ualprc10Err, zrtotsuminErr, zrtopusumErr, optextindErr, suminsErr, taxamtErr, rsuninErr, rundteErr, newFundList01Err, newFundList02Err, newFundList03Err, newFundList04Err, newFundList05Err, newFundList06Err, newFundList07Err, newFundList08Err, newFundList09Err, newFundList10Err, newFundList11Err, newFundList12Err};//ILIFE-8164
		screenDateFields = new BaseData[] {effdate, reserveUnitsDate};
		screenDateErrFields = new BaseData[] {effdateErr, rundteErr};
		screenDateDispFields = new BaseData[] {effdateDisp, reserveUnitsDateDisp};		

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sn500screen.class;
		protectRecord = Sn500protect.class;
	}

}
