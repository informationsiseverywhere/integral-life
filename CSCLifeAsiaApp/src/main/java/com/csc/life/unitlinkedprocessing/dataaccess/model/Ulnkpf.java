package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

import com.quipoz.framework.datatype.PackedDecimalData;

public class Ulnkpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private String validflag;
	private int currfrom;
	private int tranno;
	private String unitAllocFund01;
	private String unitAllocFund02;
	private String unitAllocFund03;
	private String unitAllocFund04;
	private String unitAllocFund05;
	private String unitAllocFund06;
	private String unitAllocFund07;
	private String unitAllocFund08;
	private String unitAllocFund09;
	private String unitAllocFund10;
	private BigDecimal unitAllocPercAmt01;
	private BigDecimal unitAllocPercAmt02;
	private BigDecimal unitAllocPercAmt03;
	private BigDecimal unitAllocPercAmt04;
	private BigDecimal unitAllocPercAmt05;
	private BigDecimal unitAllocPercAmt06;
	private BigDecimal unitAllocPercAmt07;
	private BigDecimal unitAllocPercAmt08;
	private BigDecimal unitAllocPercAmt09;
	private BigDecimal unitAllocPercAmt10;
	private String unitDeallocFund01;
	private String unitDeallocFund02;
	private String unitDeallocFund03;
	private String unitDeallocFund04;
	private String unitDeallocFund05;
	private String unitDeallocFund06;
	private String unitDeallocFund07;
	private String unitDeallocFund08;
	private String unitDeallocFund09;
	private String unitDeallocFund10;
	private BigDecimal unitDeallocPercAmt01;
	private BigDecimal unitDeallocPercAmt02;
	private BigDecimal unitDeallocPercAmt03;
	private BigDecimal unitDeallocPercAmt04;
	private BigDecimal unitDeallocPercAmt05;
	private BigDecimal unitDeallocPercAmt06;
	private BigDecimal unitDeallocPercAmt07;
	private BigDecimal unitDeallocPercAmt08;
	private BigDecimal unitDeallocPercAmt09;
	private BigDecimal unitDeallocPercAmt10;
	private BigDecimal unitSpecPrice01;
	private BigDecimal unitSpecPrice02;
	private BigDecimal unitSpecPrice03;
	private BigDecimal unitSpecPrice04;
	private BigDecimal unitSpecPrice05;
	private BigDecimal unitSpecPrice06;
	private BigDecimal unitSpecPrice07;
	private BigDecimal unitSpecPrice08;
	private BigDecimal unitSpecPrice09;
	private BigDecimal unitSpecPrice10;
	
	private String fundpool01;
	private String fundpool02;
	private String fundpool03;
	private String fundpool04;
	private String fundpool05;
	private String fundpool06;
	private String fundpool07;
	private String fundpool08;
	private String fundpool09;
	private String fundpool10;	
	
	private String percOrAmntInd;
	private int currto;
	private String premTopupInd;
	private int seqnbr;
	private int planSuffix;
	private String userProfile;
	private String jobName;
	private String datime;
	public String getUalfnd(int indx) {

		switch (indx) {
			case 1 : return unitAllocFund01;
			case 2 : return unitAllocFund02;
			case 3 : return unitAllocFund03;
			case 4 : return unitAllocFund04;
			case 5 : return unitAllocFund05;
			case 6 : return unitAllocFund06;
			case 7 : return unitAllocFund07;
			case 8 : return unitAllocFund08;
			case 9 : return unitAllocFund09;
			case 10 : return unitAllocFund10;
			default: return null; 
		}
	
	}
	public BigDecimal getUalprc(int indx) {

		switch (indx) {
			case 1 : return unitAllocPercAmt01;
			case 2 : return unitAllocPercAmt02;
			case 3 : return unitAllocPercAmt03;
			case 4 : return unitAllocPercAmt04;
			case 5 : return unitAllocPercAmt05;
			case 6 : return unitAllocPercAmt06;
			case 7 : return unitAllocPercAmt07;
			case 8 : return unitAllocPercAmt08;
			case 9 : return unitAllocPercAmt09;
			case 10 : return unitAllocPercAmt10;
			default: return null; 
		}
	
	}
	public BigDecimal getUspcpr(int indx) {

		switch (indx) {
			case 1 : return unitSpecPrice01;
			case 2 : return unitSpecPrice02;
			case 3 : return unitSpecPrice03;
			case 4 : return unitSpecPrice04;
			case 5 : return unitSpecPrice05;
			case 6 : return unitSpecPrice06;
			case 7 : return unitSpecPrice07;
			case 8 : return unitSpecPrice08;
			case 9 : return unitSpecPrice09;
			case 10 : return unitSpecPrice10;
			default: return null;
		}
	
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getCurrfrom() {
		return currfrom;
	}

	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getUnitAllocFund01() {
		return unitAllocFund01;
	}

	public void setUnitAllocFund01(String unitAllocFund01) {
		this.unitAllocFund01 = unitAllocFund01;
	}

	public String getUnitAllocFund02() {
		return unitAllocFund02;
	}

	public void setUnitAllocFund02(String unitAllocFund02) {
		this.unitAllocFund02 = unitAllocFund02;
	}

	public String getUnitAllocFund03() {
		return unitAllocFund03;
	}

	public void setUnitAllocFund03(String unitAllocFund03) {
		this.unitAllocFund03 = unitAllocFund03;
	}

	public String getUnitAllocFund04() {
		return unitAllocFund04;
	}

	public void setUnitAllocFund04(String unitAllocFund04) {
		this.unitAllocFund04 = unitAllocFund04;
	}

	public String getUnitAllocFund05() {
		return unitAllocFund05;
	}

	public void setUnitAllocFund05(String unitAllocFund05) {
		this.unitAllocFund05 = unitAllocFund05;
	}

	public String getUnitAllocFund06() {
		return unitAllocFund06;
	}

	public void setUnitAllocFund06(String unitAllocFund06) {
		this.unitAllocFund06 = unitAllocFund06;
	}

	public String getUnitAllocFund07() {
		return unitAllocFund07;
	}

	public void setUnitAllocFund07(String unitAllocFund07) {
		this.unitAllocFund07 = unitAllocFund07;
	}

	public String getUnitAllocFund08() {
		return unitAllocFund08;
	}

	public void setUnitAllocFund08(String unitAllocFund08) {
		this.unitAllocFund08 = unitAllocFund08;
	}

	public String getUnitAllocFund09() {
		return unitAllocFund09;
	}

	public void setUnitAllocFund09(String unitAllocFund09) {
		this.unitAllocFund09 = unitAllocFund09;
	}

	public String getUnitAllocFund10() {
		return unitAllocFund10;
	}

	public void setUnitAllocFund10(String unitAllocFund10) {
		this.unitAllocFund10 = unitAllocFund10;
	}

	public BigDecimal getUnitAllocPercAmt01() {
		return unitAllocPercAmt01;
	}

	public void setUnitAllocPercAmt01(BigDecimal unitAllocPercAmt01) {
		this.unitAllocPercAmt01 = unitAllocPercAmt01;
	}

	public BigDecimal getUnitAllocPercAmt02() {
		return unitAllocPercAmt02;
	}

	public void setUnitAllocPercAmt02(BigDecimal unitAllocPercAmt02) {
		this.unitAllocPercAmt02 = unitAllocPercAmt02;
	}

	public BigDecimal getUnitAllocPercAmt03() {
		return unitAllocPercAmt03;
	}

	public void setUnitAllocPercAmt03(BigDecimal unitAllocPercAmt03) {
		this.unitAllocPercAmt03 = unitAllocPercAmt03;
	}

	public BigDecimal getUnitAllocPercAmt04() {
		return unitAllocPercAmt04;
	}

	public void setUnitAllocPercAmt04(BigDecimal unitAllocPercAmt04) {
		this.unitAllocPercAmt04 = unitAllocPercAmt04;
	}

	public BigDecimal getUnitAllocPercAmt05() {
		return unitAllocPercAmt05;
	}

	public void setUnitAllocPercAmt05(BigDecimal unitAllocPercAmt05) {
		this.unitAllocPercAmt05 = unitAllocPercAmt05;
	}

	public BigDecimal getUnitAllocPercAmt06() {
		return unitAllocPercAmt06;
	}

	public void setUnitAllocPercAmt06(BigDecimal unitAllocPercAmt06) {
		this.unitAllocPercAmt06 = unitAllocPercAmt06;
	}

	public BigDecimal getUnitAllocPercAmt07() {
		return unitAllocPercAmt07;
	}

	public void setUnitAllocPercAmt07(BigDecimal unitAllocPercAmt07) {
		this.unitAllocPercAmt07 = unitAllocPercAmt07;
	}

	public BigDecimal getUnitAllocPercAmt08() {
		return unitAllocPercAmt08;
	}

	public void setUnitAllocPercAmt08(BigDecimal unitAllocPercAmt08) {
		this.unitAllocPercAmt08 = unitAllocPercAmt08;
	}

	public BigDecimal getUnitAllocPercAmt09() {
		return unitAllocPercAmt09;
	}

	public void setUnitAllocPercAmt09(BigDecimal unitAllocPercAmt09) {
		this.unitAllocPercAmt09 = unitAllocPercAmt09;
	}

	public BigDecimal getUnitAllocPercAmt10() {
		return unitAllocPercAmt10;
	}

	public void setUnitAllocPercAmt10(BigDecimal unitAllocPercAmt10) {
		this.unitAllocPercAmt10 = unitAllocPercAmt10;
	}

	public String getUnitDeallocFund01() {
		return unitDeallocFund01;
	}

	public void setUnitDeallocFund01(String unitDeallocFund01) {
		this.unitDeallocFund01 = unitDeallocFund01;
	}

	public String getUnitDeallocFund02() {
		return unitDeallocFund02;
	}

	public void setUnitDeallocFund02(String unitDeallocFund02) {
		this.unitDeallocFund02 = unitDeallocFund02;
	}

	public String getUnitDeallocFund03() {
		return unitDeallocFund03;
	}

	public void setUnitDeallocFund03(String unitDeallocFund03) {
		this.unitDeallocFund03 = unitDeallocFund03;
	}

	public String getUnitDeallocFund04() {
		return unitDeallocFund04;
	}

	public void setUnitDeallocFund04(String unitDeallocFund04) {
		this.unitDeallocFund04 = unitDeallocFund04;
	}

	public String getUnitDeallocFund05() {
		return unitDeallocFund05;
	}

	public void setUnitDeallocFund05(String unitDeallocFund05) {
		this.unitDeallocFund05 = unitDeallocFund05;
	}

	public String getUnitDeallocFund06() {
		return unitDeallocFund06;
	}

	public void setUnitDeallocFund06(String unitDeallocFund06) {
		this.unitDeallocFund06 = unitDeallocFund06;
	}

	public String getUnitDeallocFund07() {
		return unitDeallocFund07;
	}

	public void setUnitDeallocFund07(String unitDeallocFund07) {
		this.unitDeallocFund07 = unitDeallocFund07;
	}

	public String getUnitDeallocFund08() {
		return unitDeallocFund08;
	}

	public void setUnitDeallocFund08(String unitDeallocFund08) {
		this.unitDeallocFund08 = unitDeallocFund08;
	}

	public String getUnitDeallocFund09() {
		return unitDeallocFund09;
	}

	public void setUnitDeallocFund09(String unitDeallocFund09) {
		this.unitDeallocFund09 = unitDeallocFund09;
	}

	public String getUnitDeallocFund10() {
		return unitDeallocFund10;
	}

	public void setUnitDeallocFund10(String unitDeallocFund10) {
		this.unitDeallocFund10 = unitDeallocFund10;
	}

	public BigDecimal getUnitDeallocPercAmt01() {
		return unitDeallocPercAmt01;
	}

	public void setUnitDeallocPercAmt01(BigDecimal unitDeallocPercAmt01) {
		this.unitDeallocPercAmt01 = unitDeallocPercAmt01;
	}

	public BigDecimal getUnitDeallocPercAmt02() {
		return unitDeallocPercAmt02;
	}

	public void setUnitDeallocPercAmt02(BigDecimal unitDeallocPercAmt02) {
		this.unitDeallocPercAmt02 = unitDeallocPercAmt02;
	}

	public BigDecimal getUnitDeallocPercAmt03() {
		return unitDeallocPercAmt03;
	}

	public void setUnitDeallocPercAmt03(BigDecimal unitDeallocPercAmt03) {
		this.unitDeallocPercAmt03 = unitDeallocPercAmt03;
	}

	public BigDecimal getUnitDeallocPercAmt04() {
		return unitDeallocPercAmt04;
	}

	public void setUnitDeallocPercAmt04(BigDecimal unitDeallocPercAmt04) {
		this.unitDeallocPercAmt04 = unitDeallocPercAmt04;
	}

	public BigDecimal getUnitDeallocPercAmt05() {
		return unitDeallocPercAmt05;
	}

	public void setUnitDeallocPercAmt05(BigDecimal unitDeallocPercAmt05) {
		this.unitDeallocPercAmt05 = unitDeallocPercAmt05;
	}

	public BigDecimal getUnitDeallocPercAmt06() {
		return unitDeallocPercAmt06;
	}

	public void setUnitDeallocPercAmt06(BigDecimal unitDeallocPercAmt06) {
		this.unitDeallocPercAmt06 = unitDeallocPercAmt06;
	}

	public BigDecimal getUnitDeallocPercAmt07() {
		return unitDeallocPercAmt07;
	}

	public void setUnitDeallocPercAmt07(BigDecimal unitDeallocPercAmt07) {
		this.unitDeallocPercAmt07 = unitDeallocPercAmt07;
	}

	public BigDecimal getUnitDeallocPercAmt08() {
		return unitDeallocPercAmt08;
	}

	public void setUnitDeallocPercAmt08(BigDecimal unitDeallocPercAmt08) {
		this.unitDeallocPercAmt08 = unitDeallocPercAmt08;
	}

	public BigDecimal getUnitDeallocPercAmt09() {
		return unitDeallocPercAmt09;
	}

	public void setUnitDeallocPercAmt09(BigDecimal unitDeallocPercAmt09) {
		this.unitDeallocPercAmt09 = unitDeallocPercAmt09;
	}

	public BigDecimal getUnitDeallocPercAmt10() {
		return unitDeallocPercAmt10;
	}

	public void setUnitDeallocPercAmt10(BigDecimal unitDeallocPercAmt10) {
		this.unitDeallocPercAmt10 = unitDeallocPercAmt10;
	}

	public BigDecimal getUnitSpecPrice01() {
		return unitSpecPrice01;
	}

	public void setUnitSpecPrice01(BigDecimal unitSpecPrice01) {
		this.unitSpecPrice01 = unitSpecPrice01;
	}

	public BigDecimal getUnitSpecPrice02() {
		return unitSpecPrice02;
	}

	public void setUnitSpecPrice02(BigDecimal unitSpecPrice02) {
		this.unitSpecPrice02 = unitSpecPrice02;
	}

	public BigDecimal getUnitSpecPrice03() {
		return unitSpecPrice03;
	}

	public void setUnitSpecPrice03(BigDecimal unitSpecPrice03) {
		this.unitSpecPrice03 = unitSpecPrice03;
	}

	public BigDecimal getUnitSpecPrice04() {
		return unitSpecPrice04;
	}

	public void setUnitSpecPrice04(BigDecimal unitSpecPrice04) {
		this.unitSpecPrice04 = unitSpecPrice04;
	}

	public BigDecimal getUnitSpecPrice05() {
		return unitSpecPrice05;
	}

	public void setUnitSpecPrice05(BigDecimal unitSpecPrice05) {
		this.unitSpecPrice05 = unitSpecPrice05;
	}

	public BigDecimal getUnitSpecPrice06() {
		return unitSpecPrice06;
	}

	public void setUnitSpecPrice06(BigDecimal unitSpecPrice06) {
		this.unitSpecPrice06 = unitSpecPrice06;
	}

	public BigDecimal getUnitSpecPrice07() {
		return unitSpecPrice07;
	}

	public void setUnitSpecPrice07(BigDecimal unitSpecPrice07) {
		this.unitSpecPrice07 = unitSpecPrice07;
	}

	public BigDecimal getUnitSpecPrice08() {
		return unitSpecPrice08;
	}

	public void setUnitSpecPrice08(BigDecimal unitSpecPrice08) {
		this.unitSpecPrice08 = unitSpecPrice08;
	}

	public BigDecimal getUnitSpecPrice09() {
		return unitSpecPrice09;
	}

	public void setUnitSpecPrice09(BigDecimal unitSpecPrice09) {
		this.unitSpecPrice09 = unitSpecPrice09;
	}

	public BigDecimal getUnitSpecPrice10() {
		return unitSpecPrice10;
	}

	public void setUnitSpecPrice10(BigDecimal unitSpecPrice10) {
		this.unitSpecPrice10 = unitSpecPrice10;
	}

	public String getPercOrAmntInd() {
		return percOrAmntInd;
	}

	public void setPercOrAmntInd(String percOrAmntInd) {
		this.percOrAmntInd = percOrAmntInd;
	}

	public int getCurrto() {
		return currto;
	}

	public void setCurrto(int currto) {
		this.currto = currto;
	}

	public String getPremTopupInd() {
		return premTopupInd;
	}

	public void setPremTopupInd(String premTopupInd) {
		this.premTopupInd = premTopupInd;
	}

	public int getSeqnbr() {
		return seqnbr;
	}

	public void setSeqnbr(int seqnbr) {
		this.seqnbr = seqnbr;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
   //san	
	public String getFundPool01() {
		return fundpool01;
	}

	public void setFundPool01(String fundPool) {
		this.fundpool01 = fundPool;
	}
	
	public String getFundPool02() {
		return fundpool02;
	}

	public void setFundPool02(String fundPool) {
		this.fundpool02 = fundPool;
	}
	
	public String getFundPool03() {
		return fundpool03;
	}

	public void setFundPool03(String fundPool) {
		this.fundpool03 = fundPool;
	}
	
	public String getFundPool04() {
		return fundpool04;
	}

	public void setFundPool04(String fundPool) {
		this.fundpool04 = fundPool;
	}
	
	public String getFundPool05() {
		return fundpool05;
	}

	public void setFundPool05(String fundPool) {
		this.fundpool05 = fundPool;
	}
	
	
	public String getFundPool06() {
		return fundpool06;
	}

	public void setFundPool06(String fundPool) {
		this.fundpool06 = fundPool;
	}
	
	public String getFundPool07() {
		return fundpool07;
	}

	public void setFundPool07(String fundPool) {
		this.fundpool07 = fundPool;
	}
	
	public String getFundPool08() {
		return fundpool08;
	}

	public void setFundPool08(String fundPool) {
		this.fundpool08 = fundPool;
	}
	
	public String getFundPool09() {
		return fundpool09;
	}

	public void setFundPool09(String fundPool) {
		this.fundpool09 = fundPool;
	}
	
	public String getFundPool10() {
		return fundpool10;
	}

	public void setFundPool10(String fundPool) {
		this.fundpool10 = fundPool;
	}	
	
	public String getFundPool(int indx) {

		switch (indx) {
			case 1 : return fundpool01;
			case 2 : return fundpool02;
			case 3 : return fundpool03;
			case 4 : return fundpool04;
			case 5 : return fundpool05;
			case 6 : return fundpool06;
			case 7 : return fundpool07;
			case 8 : return fundpool08;
			case 9 : return fundpool09;
			case 10 : return fundpool10;
			default: return null; 
		}
	
	}
	//san
	
	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}
}