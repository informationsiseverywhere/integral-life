package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:26
 * Description:
 * Copybook name: ULUVALUREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Uluvalurec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData uluvaluRec = new FixedLengthStringData(196);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(uluvaluRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(uluvaluRec, 5);
  	public FixedLengthStringData ridrkey = new FixedLengthStringData(17).isAPartOf(uluvaluRec, 9);
  	public FixedLengthStringData chdrkey = new FixedLengthStringData(9).isAPartOf(ridrkey, 0);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrkey, 0);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(chdrkey, 1);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(ridrkey, 9);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(ridrkey, 11);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(ridrkey, 13);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(ridrkey, 15);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(uluvaluRec, 26);
  	public FixedLengthStringData ndfind = new FixedLengthStringData(1).isAPartOf(uluvaluRec, 29);
  	public ZonedDecimalData priceEfd = new ZonedDecimalData(8, 0).isAPartOf(uluvaluRec, 30).setUnsigned();
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(uluvaluRec, 38);
  	public FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(uluvaluRec, 41);
  	public FixedLengthStringData unitType = new FixedLengthStringData(2).isAPartOf(uluvaluRec, 45);
  	public ZonedDecimalData value = new ZonedDecimalData(17, 5).isAPartOf(uluvaluRec, 47);
  	public ZonedDecimalData realValue = new ZonedDecimalData(17, 5).isAPartOf(uluvaluRec, 64);
  	public ZonedDecimalData nofUnits = new ZonedDecimalData(16, 5).isAPartOf(uluvaluRec, 81);
  	public ZonedDecimalData nofDunits = new ZonedDecimalData(16, 5).isAPartOf(uluvaluRec, 97);
  	public ZonedDecimalData nofFunds = new ZonedDecimalData(5, 0).isAPartOf(uluvaluRec, 113);
  	public FixedLengthStringData filler = new FixedLengthStringData(78).isAPartOf(uluvaluRec, 118, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(uluvaluRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		uluvaluRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}