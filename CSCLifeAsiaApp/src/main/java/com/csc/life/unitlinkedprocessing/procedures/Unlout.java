/*
 * File: Unlout.java
 * Date: 30 August 2009 2:51:03
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLOUT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.Utrnbk1TableDAM;
import com.csc.life.contractservicing.recordstructures.Genoutrec;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.IncibrkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkbrkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfbrkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnbrkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsbrkTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  GENERIC SUB-ROUTINE  (BREAKOUT)
*
*
* This Unit Linked generic subroutine is called by the Breakout
* subroutine (which in turn  was  called by an "AT" module), in
* order to breakout the  summarised  "INCI", "ULNK", UTRS, UTRN
* and USTF records.
*
* BREAKOUT processing occurs when the policy selected from the
* calling program belongs to the summerised policy record which
* is denoted by PLAN-SUFFIX '00'. For whatever reason this policy
* must become a record in its own right for further processing.
*
* In this program, records are 'broken out' in one of two possible
* ways depending on the file. If the file holds the lowest possible
* values in the system then use processing 'A'. If the file is
* built from values on another file use processing 'B' (UTRS are
* built from the values on the UTRNs).
*
* PROCESSING A
* ------------
* Get the summerised record (plan suffix 00).
* Store the values of the summerised record in an array
* (WSAA-AMOUNT-IN).
* Write a new record until the last policy that requires to be
* broken out is reached. The values on the new record are
* calculated and stored in an array (WSAA-AMOUNT-OUT) as follows -
*
*    COMPUTE WSAA-AMOUNT-OUT (WSAA-SUB) ROUNDED =
*                 WSAA-AMOUNT-IN (WSAA-SUB) / BRK-OLD-SUMMARY.
*
* These new values are totaled for later use in an array;
*
*    COMPUTE WSAD-AMOUNT-TOT (WSAA-SUB) =
*                 WSAD-AMOUNT-TOT (WSAA-SUB) +
*                 WSAA-AMOUNT-OUT (WSAA-SUB).
*
* When the last policy that requires to be broken out is reached
* calculate the values on the record as the following example for
* UTRNS;
*
*    COMPUTE UTRNBRK-FUND-AMOUNT =
*                 WSAA-AMOUNT-IN (4) - WSAD-AMOUNT-TOT (4).
*
* These values are either written as a new record or rewritten on
* an old record depending on the following;
*
*  1.The policy to be broken out is the last policy of the plan.
*    In this case it is necessary to delete the summerised record
*    (plan-suffix 00) and write a new recordusing the calculation
*    above, with a plan suffix of 1.
*
*  2.The policy to be broken out is not the last policy on the
*    plan so calculate the new values as above and rewrite the
*    summerised record (plan-suffix 00).
*
* PROCESSING B
* ------------
* This processing is applied to master files that hold the totals
* of values held on a transaction file. eg. UTRNS hold the total
* of UTRSs.
*
* The transaction record must be broken out before the master
* file processing.
*
* Get the summerised record of the master file.
* eg UTRS-PLAN-SUFFIX 00.
*
* There are two methods of processing depending on whether the
* last policy of the plan is to be broken out or not.
*
* If the policy to be broken out is the last policy of the plan,
* write a new record until all the policies nave been broken out.
* The values on each new record is calculated by the accumulation
* of the values on the transaction records for that policy
* (UTRN-PLAN-SUFFIX).
*
* If the policy to be broken out is not the last policy of the
* plan, write a new record until the last policy that requires to
* be broken out is reached. The values on each new record is
* calculated by the accumulation of the values on the transaction
* records for that policy (UTRN-PLAN-SUFFIX).
*
* When the last policy required to be broken out is reached and
* as its not the last policy in the plan, rewrite the summerised
* record (plan-suffix 00) and calculate the values as the
* following example for UTRS;
*
*    COMPUTE UTRSBRK-CURRENT-UNIT-BAL =
*                         UTRSBRK-CURRENT-UNIT-BAL -
*                         WSAB-TOT-CURR-UNIT-BAL.
*
* NOTE
* ----
* The files that use PROCESSING A are INCI, UNLK, UTRN, USTF.
* The files that use PROCESSING B are UTRS.
*
*****************************************************************
* </pre>
*/
public class Unlout extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLOUT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaX = new ZonedDecimalData(3, 0).init(0).setUnsigned();

		/* WSAA-AMOUNT-TABLE */
	private FixedLengthStringData[] wsaaAmountsIn = FLSInittedArray (20, 17);
	private ZonedDecimalData[] wsaaAmountIn = ZDArrayPartOfArrayStructure(17, 2, wsaaAmountsIn, 0);

	private FixedLengthStringData[] wsaaAmountsOut = FLSInittedArray (20, 17);
	private ZonedDecimalData[] wsaaAmountOut = ZDArrayPartOfArrayStructure(17, 2, wsaaAmountsOut, 0);

		/* WSAA-AMOUNT-TABLE-5 */
	private FixedLengthStringData[] wsaaAmountsIn5 = FLSInittedArray (20, 16);
	private ZonedDecimalData[] wsaaAmountIn5 = ZDArrayPartOfArrayStructure(16, 5, wsaaAmountsIn5, 0);

	private FixedLengthStringData[] wsaaAmountsOut5 = FLSInittedArray (20, 16);
	private ZonedDecimalData[] wsaaAmountOut5 = ZDArrayPartOfArrayStructure(16, 5, wsaaAmountsOut5, 0);
		/* WSAD-AMOUNT-TOT-TABLE */
	private ZonedDecimalData[] wsadAmountTot = ZDInittedArray(20, 17, 2);
		/* WSAD-AMOUNT-TOT-TABLE-5 */
	private ZonedDecimalData[] wsadAmountTot5 = ZDInittedArray(20, 16, 5);
	private ZonedDecimalData wsabCurrentUnitBal = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsabCurrentDunitBal = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsabTotCurrUnitBal = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsabTotCurrDunitBal = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsabBrkPlnSuffix = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsaaInciNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaUstfNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaUlnkNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaUtrsNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaUtrnNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(ZERO);
		/* WSAA-PERIODS */
	private BinaryData wsaaPeriod1 = new BinaryData(18, 0).init(ZERO);
	private BinaryData wsaaPeriod2 = new BinaryData(18, 0).init(ZERO);
	private BinaryData wsaaPeriod3 = new BinaryData(18, 0).init(ZERO);
	private BinaryData wsaaPeriod4 = new BinaryData(18, 0).init(ZERO);
		/* WSAA-PREMS-TO-PAY */
	private BinaryData wsaaPremToPay1 = new BinaryData(18, 0).init(ZERO);
	private BinaryData wsaaPremToPay2 = new BinaryData(18, 0).init(ZERO);
	private BinaryData wsaaPremToPay3 = new BinaryData(18, 0).init(ZERO);
	private BinaryData wsaaPremToPay4 = new BinaryData(18, 0).init(ZERO);
	private InciTableDAM inciIO = new InciTableDAM();
	private IncibrkTableDAM incibrkIO = new IncibrkTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UlnkbrkTableDAM ulnkbrkIO = new UlnkbrkTableDAM();
	private UstfTableDAM ustfIO = new UstfTableDAM();
	private UstfbrkTableDAM ustfbrkIO = new UstfbrkTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Utrnbk1TableDAM utrnbk1IO = new Utrnbk1TableDAM();
	private UtrnbrkTableDAM utrnbrkIO = new UtrnbrkTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private UtrsbrkTableDAM utrsbrkIO = new UtrsbrkTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Genoutrec genoutrec = new Genoutrec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr202, 
		exit209, 
		nextr302, 
		exit309, 
		nextr402, 
		exit409, 
		nextr502, 
		exit509, 
		nextr602, 
		exit609
	}

	public Unlout() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		genoutrec.outRec = convertAndSetParam(genoutrec.outRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		initialize100();
		/*  process the INCI file*/
		while ( !(isEQ(incibrkIO.getStatuz(),varcom.endp))) {
			mainProcessingIncibrk200();
		}
		
		/*  process the ULNK file*/
		while ( !(isEQ(ulnkbrkIO.getStatuz(),varcom.endp))) {
			mainProcessingUlnk300();
		}
		
		/*  process the UTRN file*/
		while ( !(isEQ(utrnbrkIO.getStatuz(),varcom.endp))) {
			mainProcessingUtrn400();
		}
		
		/*  Process the UTRS after the UTRN file. The UTRS holds the balanc*/
		/*  of the UTRN values.*/
		while ( !(isEQ(utrsbrkIO.getStatuz(),varcom.endp))) {
			mainProcessingUtrs500();
		}
		
		/*  process the USTF file*/
		while ( !(isEQ(ustfbrkIO.getStatuz(),varcom.endp))) {
			mainProcessingUstf600();
		}
		
		/*EXIT*/
		exitProgram();
	}

protected void initialize100()
	{
		para101();
	}

protected void para101()
	{
		genoutrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaInciNoRecs.set(ZERO);
		wsaaUstfNoRecs.set(ZERO);
		wsaaUlnkNoRecs.set(ZERO);
		wsaaUtrsNoRecs.set(ZERO);
		wsaaUtrnNoRecs.set(ZERO);
		/*  read the first record from all of the files to be processed*/
		/*   FIRST read of the INCIBRK file*/
		incibrkIO.setDataArea(SPACES);
		incibrkIO.setChdrnum(genoutrec.chdrnum);
		incibrkIO.setChdrcoy(genoutrec.chdrcoy);
		incibrkIO.setPlanSuffix(ZERO);
		incibrkIO.setInciNum(ZERO);
		incibrkIO.setSeqno(ZERO);
		incibrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, incibrkIO);
		if (isNE(incibrkIO.getStatuz(),varcom.oK)
		&& isNE(incibrkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incibrkIO.getParams());
			fatalError9000();
		}
		if (isNE(incibrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(incibrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(incibrkIO.getPlanSuffix(),ZERO)
		|| isEQ(incibrkIO.getStatuz(),varcom.endp)) {
			wsaaInciNoRecs.set(1);
		}
		/*   FIRST read of the ULNKBRK file*/
		ulnkbrkIO.setDataArea(SPACES);
		ulnkbrkIO.setChdrnum(genoutrec.chdrnum);
		ulnkbrkIO.setChdrcoy(genoutrec.chdrcoy);
		ulnkbrkIO.setPlanSuffix(ZERO);
		ulnkbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, ulnkbrkIO);
		if (isNE(ulnkbrkIO.getStatuz(),varcom.oK)
		&& isNE(ulnkbrkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkbrkIO.getParams());
			fatalError9000();
		}
		if (isNE(ulnkbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(ulnkbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(ulnkbrkIO.getPlanSuffix(),ZERO)
		|| isEQ(ulnkbrkIO.getStatuz(),varcom.endp)) {
			wsaaUlnkNoRecs.set(1);
		}
		/*   FIRST read of the UTRSBRK file*/
		utrsbrkIO.setDataArea(SPACES);
		utrsbrkIO.setChdrnum(genoutrec.chdrnum);
		utrsbrkIO.setChdrcoy(genoutrec.chdrcoy);
		utrsbrkIO.setPlanSuffix(ZERO);
		utrsbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, utrsbrkIO);
		if (isNE(utrsbrkIO.getStatuz(),varcom.oK)
		&& isNE(utrsbrkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsbrkIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(utrsbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(utrsbrkIO.getPlanSuffix(),ZERO)
		|| isEQ(utrsbrkIO.getStatuz(),varcom.endp)) {
			wsaaUtrsNoRecs.set(1);
		}
		/*   FIRST read of the UTRNBRK file*/
		utrnbrkIO.setDataArea(SPACES);
		utrnbrkIO.setChdrnum(genoutrec.chdrnum);
		utrnbrkIO.setChdrcoy(genoutrec.chdrcoy);
		utrnbrkIO.setPlanSuffix(ZERO);
		utrnbrkIO.setTranno(ZERO);
		utrnbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, utrnbrkIO);
		if (isNE(utrnbrkIO.getStatuz(),varcom.oK)
		&& isNE(utrnbrkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnbrkIO.getParams());
			fatalError9000();
		}
		if (isNE(utrnbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(utrnbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(utrnbrkIO.getPlanSuffix(),ZERO)
		|| isEQ(utrnbrkIO.getStatuz(),varcom.endp)) {
			wsaaUtrnNoRecs.set(1);
		}
		/*   FIRST read of the USTFBRK file*/
		ustfbrkIO.setDataArea(SPACES);
		ustfbrkIO.setChdrnum(genoutrec.chdrnum);
		ustfbrkIO.setChdrcoy(genoutrec.chdrcoy);
		ustfbrkIO.setPlanSuffix(ZERO);
		ustfbrkIO.setUstmno(ZERO);
		ustfbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, ustfbrkIO);
		if (isNE(ustfbrkIO.getStatuz(),varcom.oK)
		&& isNE(ustfbrkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ustfbrkIO.getParams());
			fatalError9000();
		}
		if (isNE(ustfbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(ustfbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(ustfbrkIO.getPlanSuffix(),ZERO)
		|| isEQ(ustfbrkIO.getStatuz(),varcom.endp)) {
			wsaaUstfNoRecs.set(1);
		}
		/*   initialize the  working storage table used in the*/
		/*   breakout calculation*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			initializeTable150();
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			initializeTable5160();
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			wsadAmountTot[wsaaSub.toInt()].set(ZERO);
			wsadAmountTot5[wsaaSub.toInt()].set(ZERO);
		}
	}

protected void initializeTable150()
	{
		/*PARA*/
		wsaaAmountIn[wsaaSub.toInt()].set(ZERO);
		wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void initializeTable5160()
	{
		/*PARA*/
		wsaaAmountIn5[wsaaSub.toInt()].set(ZERO);
		wsaaAmountOut5[wsaaSub.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void mainProcessingIncibrk200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para201();
				case nextr202: 
					nextr202();
				case exit209: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para201()
	{
		if (isEQ(wsaaInciNoRecs,1)) {
			incibrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit209);
		}
		/*  move all the numeric fields from the INCI summary record in*/
		/*  order to loop thru them and unsummarize each one*/
		moveInciValsToTable210();
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			wsadAmountTot[wsaaSub.toInt()].set(ZERO);
			wsadAmountTot5[wsaaSub.toInt()].set(ZERO);
		}
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix,genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			inciCalcs220();
			moveInciValsFromTable230();
			writeBrkInci240();
		}
		/* calculate the new summary record - passed to us in linkage*/
		calcNewSummaryRecInci250();
	}

	/**
	* <pre>
	*  read the next INCI record and process as above until EOF
	* </pre>
	*/
protected void nextr202()
	{
		incibrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incibrkIO);
		if (isNE(incibrkIO.getStatuz(),varcom.oK)
		&& isNE(incibrkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incibrkIO.getParams());
			fatalError9000();
		}
		if (isEQ(incibrkIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(incibrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(incibrkIO.getChdrcoy(),genoutrec.chdrcoy)) {
			incibrkIO.setFunction(varcom.rewrt);
			incibrkIO.setFormat(formatsInner.incibrkrec);
			SmartFileCode.execute(appVars, incibrkIO);
			if (isNE(incibrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(incibrkIO.getParams());
				fatalError9000();
			}
			else {
				incibrkIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isNE(incibrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(incibrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(incibrkIO.getPlanSuffix(),ZERO)) {
			incibrkIO.setFunction(varcom.rewrt);
			incibrkIO.setFormat(formatsInner.incibrkrec);
			SmartFileCode.execute(appVars, incibrkIO);
			if (isNE(incibrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(incibrkIO.getParams());
				fatalError9000();
			}
			else {
				goTo(GotoLabel.nextr202);
			}
		}
	}

protected void moveInciValsToTable210()
	{
		/*READ*/
		wsaaAmountIn[1].set(incibrkIO.getPremst(1));
		wsaaAmountIn[2].set(incibrkIO.getPremst(2));
		wsaaAmountIn[3].set(incibrkIO.getPremst(3));
		wsaaAmountIn[4].set(incibrkIO.getPremst(4));
		wsaaAmountIn[5].set(incibrkIO.getPremcurr(1));
		wsaaAmountIn[6].set(incibrkIO.getPremcurr(2));
		wsaaAmountIn[7].set(incibrkIO.getPremcurr(3));
		wsaaAmountIn[8].set(incibrkIO.getPremcurr(4));
		wsaaAmountIn[9].set(incibrkIO.getOrigPrem());
		wsaaAmountIn[10].set(incibrkIO.getCurrPrem());
		/*EXIT*/
	}

protected void inciCalcs220()
	{
		/*READ*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			calcInciRemainBreakout8000();
		}
		/*EXIT*/
	}

protected void moveInciValsFromTable230()
	{
		/*GO*/
		incibrkIO.setPremst(1, wsaaAmountOut[1]);
		incibrkIO.setPremst(2, wsaaAmountOut[2]);
		incibrkIO.setPremst(3, wsaaAmountOut[3]);
		incibrkIO.setPremst(4, wsaaAmountOut[4]);
		incibrkIO.setPremcurr(1, wsaaAmountOut[5]);
		incibrkIO.setPremcurr(2, wsaaAmountOut[6]);
		incibrkIO.setPremcurr(3, wsaaAmountOut[7]);
		incibrkIO.setPremcurr(4, wsaaAmountOut[8]);
		incibrkIO.setOrigPrem(wsaaAmountOut[9]);
		incibrkIO.setCurrPrem(wsaaAmountOut[10]);
		/*EXIT*/
	}

protected void writeBrkInci240()
	{
		go241();
	}

protected void go241()
	{
		inciIO.setChdrcoy(incibrkIO.getChdrcoy());
		inciIO.setChdrnum(incibrkIO.getChdrnum());
		inciIO.setLife(incibrkIO.getLife());
		inciIO.setCoverage(incibrkIO.getCoverage());
		inciIO.setRider(incibrkIO.getRider());
		inciIO.setPlanSuffix(incibrkIO.getPlanSuffix());
		inciIO.setInciNum(incibrkIO.getInciNum());
		inciIO.setSeqno(incibrkIO.getSeqno());
		inciIO.setPlanSuffix(wsaaPlanSuffix);
		inciIO.setNonKey(incibrkIO.getNonKey());
		inciIO.setFormat(formatsInner.incirec);
		inciIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecInci250()
	{
			go251();
		}

protected void go251()
	{
		if (isLT(genoutrec.newSummary,2)) {
			deleteSummaryInci260();
		}
		setPrecision(incibrkIO.getPremst(1), 2);
		incibrkIO.setPremst(1, sub(wsaaAmountIn[1],wsadAmountTot[1]));
		setPrecision(incibrkIO.getPremst(2), 2);
		incibrkIO.setPremst(2, sub(wsaaAmountIn[2],wsadAmountTot[2]));
		setPrecision(incibrkIO.getPremst(3), 2);
		incibrkIO.setPremst(3, sub(wsaaAmountIn[3],wsadAmountTot[3]));
		setPrecision(incibrkIO.getPremst(4), 2);
		incibrkIO.setPremst(4, sub(wsaaAmountIn[4],wsadAmountTot[4]));
		setPrecision(incibrkIO.getPremcurr(1), 2);
		incibrkIO.setPremcurr(1, sub(wsaaAmountIn[5],wsadAmountTot[5]));
		setPrecision(incibrkIO.getPremcurr(2), 2);
		incibrkIO.setPremcurr(2, sub(wsaaAmountIn[6],wsadAmountTot[6]));
		setPrecision(incibrkIO.getPremcurr(3), 2);
		incibrkIO.setPremcurr(3, sub(wsaaAmountIn[7],wsadAmountTot[7]));
		setPrecision(incibrkIO.getPremcurr(4), 2);
		incibrkIO.setPremcurr(4, sub(wsaaAmountIn[8],wsadAmountTot[8]));
		setPrecision(incibrkIO.getOrigPrem(), 2);
		incibrkIO.setOrigPrem(sub(wsaaAmountIn[9],wsadAmountTot[9]));
		setPrecision(incibrkIO.getCurrPrem(), 2);
		incibrkIO.setCurrPrem(sub(wsaaAmountIn[10],wsadAmountTot[10]));
		if (isLT(genoutrec.newSummary,2)) {
			wsaaPlanSuffix.set(1);
			writeBrkInci240();
			return ;
		}
		/* rewrite the new summarised record*/
		incibrkIO.setFormat(formatsInner.incibrkrec);
		incibrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, incibrkIO);
		if (isNE(incibrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incibrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryInci260()
	{
		/*GO*/
		incibrkIO.setFunction(varcom.delet);
		incibrkIO.setFormat(formatsInner.incibrkrec);
		SmartFileCode.execute(appVars, incibrkIO);
		if (isNE(incibrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incibrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void mainProcessingUlnk300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para301();
				}
				case nextr302: {
					nextr302();
				}
				case exit309: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para301()
	{
		if (isEQ(wsaaUlnkNoRecs,1)) {
			ulnkbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit309);
		}
		moveUlnkValsToTable310();
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			wsadAmountTot[wsaaSub.toInt()].set(ZERO);
			wsadAmountTot5[wsaaSub.toInt()].set(ZERO);
		}
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix,genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			unlkCalcs320();
			if (isNE(ulnkbrkIO.getPercOrAmntInd(),"P")) {
				moveUlnkValsFromTable330();
			}
			writeBrkUlnk340();
		}
		calcNewSummaryRecUlnk350();
	}

protected void nextr302()
	{
		ulnkbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ulnkbrkIO);
		if ((isNE(ulnkbrkIO.getStatuz(),varcom.oK))
		&& (isNE(ulnkbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ulnkbrkIO.getParams());
			fatalError9000();
		}
		if (isEQ(ulnkbrkIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(ulnkbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(ulnkbrkIO.getChdrcoy(),genoutrec.chdrcoy)) {
			ulnkbrkIO.setFunction(varcom.rewrt);
			ulnkbrkIO.setFormat(formatsInner.ulnkbrkrec);
			SmartFileCode.execute(appVars, ulnkbrkIO);
			if (isNE(ulnkbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(ulnkbrkIO.getParams());
				fatalError9000();
			}
			else {
				ulnkbrkIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isNE(ulnkbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(ulnkbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(ulnkbrkIO.getPlanSuffix(),ZERO)) {
			ulnkbrkIO.setFunction(varcom.rewrt);
			ulnkbrkIO.setFormat(formatsInner.ulnkbrkrec);
			SmartFileCode.execute(appVars, ulnkbrkIO);
			if (isNE(ulnkbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(ulnkbrkIO.getParams());
				fatalError9000();
			}
			else {
				goTo(GotoLabel.nextr302);
			}
		}
	}

protected void moveUlnkValsToTable310()
	{
		/*READ*/
		wsaaAmountIn[1].set(ulnkbrkIO.getUalprc(1));
		wsaaAmountIn[2].set(ulnkbrkIO.getUalprc(2));
		wsaaAmountIn[3].set(ulnkbrkIO.getUalprc(3));
		wsaaAmountIn[4].set(ulnkbrkIO.getUalprc(4));
		wsaaAmountIn[5].set(ulnkbrkIO.getUalprc(5));
		wsaaAmountIn[6].set(ulnkbrkIO.getUalprc(6));
		wsaaAmountIn[7].set(ulnkbrkIO.getUalprc(7));
		wsaaAmountIn[8].set(ulnkbrkIO.getUalprc(8));
		wsaaAmountIn[9].set(ulnkbrkIO.getUalprc(9));
		wsaaAmountIn[10].set(ulnkbrkIO.getUalprc(10));
		/*EXIT*/
	}

protected void unlkCalcs320()
	{
		/*READ*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			calcRemainBreakout7000();
		}
		/*EXIT*/
	}

protected void moveUlnkValsFromTable330()
	{
		/*GO*/
		ulnkbrkIO.setUalprc(1, wsaaAmountOut[1]);
		ulnkbrkIO.setUalprc(2, wsaaAmountOut[2]);
		ulnkbrkIO.setUalprc(3, wsaaAmountOut[3]);
		ulnkbrkIO.setUalprc(4, wsaaAmountOut[4]);
		ulnkbrkIO.setUalprc(5, wsaaAmountOut[5]);
		ulnkbrkIO.setUalprc(6, wsaaAmountOut[6]);
		ulnkbrkIO.setUalprc(7, wsaaAmountOut[7]);
		ulnkbrkIO.setUalprc(8, wsaaAmountOut[8]);
		ulnkbrkIO.setUalprc(9, wsaaAmountOut[9]);
		ulnkbrkIO.setUalprc(10, wsaaAmountOut[10]);
		/*EXIT*/
	}

protected void writeBrkUlnk340()
	{
		go341();
	}

protected void go341()
	{
		ulnkIO.setChdrcoy(ulnkbrkIO.getChdrcoy());
		ulnkIO.setChdrnum(ulnkbrkIO.getChdrnum());
		ulnkIO.setLife(ulnkbrkIO.getLife());
		ulnkIO.setJlife(ulnkbrkIO.getJlife());
		ulnkIO.setCoverage(ulnkbrkIO.getCoverage());
		ulnkIO.setRider(ulnkbrkIO.getRider());
		ulnkIO.setTranno(ulnkbrkIO.getTranno());
		ulnkIO.setPlanSuffix(wsaaPlanSuffix);
		ulnkIO.setNonKey(ulnkbrkIO.getNonKey());
		ulnkIO.setFormat(formatsInner.ulnkrec);
		ulnkIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecUlnk350()
	{
					read351();
					writeOrRewriteUnlk358();
				}

protected void read351()
	{
		if (isLT(genoutrec.newSummary,2)) {
			deleteSummaryUlnk360();
		}
		if (isEQ(ulnkbrkIO.getPercOrAmntInd(),"P")) {
			return ;
		}
		setPrecision(ulnkbrkIO.getUalprc(1), 2);
		ulnkbrkIO.setUalprc(1, sub(wsaaAmountIn[1],wsadAmountTot[1]));
		setPrecision(ulnkbrkIO.getUalprc(2), 2);
		ulnkbrkIO.setUalprc(2, sub(wsaaAmountIn[2],wsadAmountTot[2]));
		setPrecision(ulnkbrkIO.getUalprc(3), 2);
		ulnkbrkIO.setUalprc(3, sub(wsaaAmountIn[3],wsadAmountTot[3]));
		setPrecision(ulnkbrkIO.getUalprc(4), 2);
		ulnkbrkIO.setUalprc(4, sub(wsaaAmountIn[4],wsadAmountTot[4]));
		setPrecision(ulnkbrkIO.getUalprc(5), 2);
		ulnkbrkIO.setUalprc(5, sub(wsaaAmountIn[5],wsadAmountTot[5]));
		setPrecision(ulnkbrkIO.getUalprc(6), 2);
		ulnkbrkIO.setUalprc(6, sub(wsaaAmountIn[6],wsadAmountTot[6]));
		setPrecision(ulnkbrkIO.getUalprc(7), 2);
		ulnkbrkIO.setUalprc(7, sub(wsaaAmountIn[7],wsadAmountTot[7]));
		setPrecision(ulnkbrkIO.getUalprc(8), 2);
		ulnkbrkIO.setUalprc(8, sub(wsaaAmountIn[8],wsadAmountTot[8]));
		setPrecision(ulnkbrkIO.getUalprc(9), 2);
		ulnkbrkIO.setUalprc(9, sub(wsaaAmountIn[9],wsadAmountTot[9]));
		setPrecision(ulnkbrkIO.getUalprc(10), 2);
		ulnkbrkIO.setUalprc(10, sub(wsaaAmountIn[10],wsadAmountTot[10]));
	}

protected void writeOrRewriteUnlk358()
	{
		if (isLT(genoutrec.newSummary,2)) {
			wsaaPlanSuffix.set(1);
			writeBrkUlnk340();
			return ;
		}
		ulnkbrkIO.setFormat(formatsInner.ulnkbrkrec);
		ulnkbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ulnkbrkIO);
		if (isNE(ulnkbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkbrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryUlnk360()
	{
		/*READ*/
		ulnkbrkIO.setFunction(varcom.delet);
		ulnkbrkIO.setFormat(formatsInner.ulnkbrkrec);
		SmartFileCode.execute(appVars, ulnkbrkIO);
		if (isNE(ulnkbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkbrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void mainProcessingUtrn400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para401();
				}
				case nextr402: {
					nextr402();
				}
				case exit409: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para401()
	{
		if (isEQ(wsaaUtrnNoRecs,1)) {
			utrnbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit409);
		}
		moveUtrnValsToTable410();
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			wsadAmountTot[wsaaSub.toInt()].set(ZERO);
			wsadAmountTot5[wsaaSub.toInt()].set(ZERO);
		}
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix,genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			utrnCalcs420();
			moveUtrnValsFromTable430();
			writeBrkUtrn440();
		}
		calcNewSummaryRecUtrn450();
	}

protected void nextr402()
	{
		utrnbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnbrkIO);
		if ((isNE(utrnbrkIO.getStatuz(),varcom.oK))
		&& (isNE(utrnbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrnbrkIO.getParams());
			fatalError9000();
		}
		if (isEQ(utrnbrkIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(utrnbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(utrnbrkIO.getChdrcoy(),genoutrec.chdrcoy)) {
			utrnbrkIO.setFunction(varcom.rewrt);
			utrnbrkIO.setFormat(formatsInner.utrnbrkrec);
			SmartFileCode.execute(appVars, utrnbrkIO);
			if (isNE(utrnbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(utrnbrkIO.getParams());
				fatalError9000();
			}
			else {
				utrnbrkIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isNE(utrnbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(utrnbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(utrnbrkIO.getPlanSuffix(),ZERO)) {
			utrnbrkIO.setFunction(varcom.rewrt);
			utrnbrkIO.setFormat(formatsInner.utrnbrkrec);
			SmartFileCode.execute(appVars, utrnbrkIO);
			if (isNE(utrnbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(utrnbrkIO.getParams());
				fatalError9000();
			}
			else {
				goTo(GotoLabel.nextr402);
			}
		}
	}

protected void moveUtrnValsToTable410()
	{
		/*READ*/
		wsaaAmountIn[1].set(utrnbrkIO.getInciprm(1));
		wsaaAmountIn[2].set(utrnbrkIO.getInciprm(2));
		wsaaAmountIn[3].set(utrnbrkIO.getContractAmount());
		wsaaAmountIn[4].set(utrnbrkIO.getFundAmount());
		wsaaAmountIn5[5].set(utrnbrkIO.getNofUnits());
		wsaaAmountIn5[6].set(utrnbrkIO.getNofDunits());
		/*EXIT*/
	}

protected void utrnCalcs420()
	{
		/*ENTRY*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			calcRemainBreakout7000();
		}
		for (wsaaSub.set(5); !(isGT(wsaaSub,6)); wsaaSub.add(1)){
			calcRemainBreakout7500();
		}
		/*EXIT*/
	}

protected void moveUtrnValsFromTable430()
	{
		/*GO*/
		utrnbrkIO.setInciprm(1, wsaaAmountOut[1]);
		utrnbrkIO.setInciprm(2, wsaaAmountOut[2]);
		utrnbrkIO.setContractAmount(wsaaAmountOut[3]);
		utrnbrkIO.setFundAmount(wsaaAmountOut[4]);
		utrnbrkIO.setNofUnits(wsaaAmountOut5[5]);
		utrnbrkIO.setNofDunits(wsaaAmountOut5[6]);
		/*EXIT*/
	}

protected void writeBrkUtrn440()
	{
		go441();
	}

protected void go441()
	{
		utrnIO.setChdrcoy(utrnbrkIO.getChdrcoy());
		utrnIO.setChdrnum(utrnbrkIO.getChdrnum());
		utrnIO.setLife(utrnbrkIO.getLife());
		utrnIO.setCoverage(utrnbrkIO.getCoverage());
		utrnIO.setRider(utrnbrkIO.getRider());
		utrnIO.setUnitVirtualFund(utrnbrkIO.getUnitVirtualFund());
		utrnIO.setUnitType(utrnbrkIO.getUnitType());
		utrnIO.setTranno(utrnbrkIO.getTranno());
		utrnIO.setPlanSuffix(wsaaPlanSuffix);
		utrnIO.setNonKey(utrnbrkIO.getNonKey());
		utrnIO.setFormat(formatsInner.utrnrec);
		utrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecUtrn450()
	{
			read451();
		}

protected void read451()
	{
		if (isLT(genoutrec.newSummary,2)) {
			deleteSummaryUtrn460();
		}
		setPrecision(utrnbrkIO.getInciprm(1), 2);
		utrnbrkIO.setInciprm(1, sub(wsaaAmountIn[1],wsadAmountTot[1]));
		setPrecision(utrnbrkIO.getInciprm(2), 2);
		utrnbrkIO.setInciprm(2, sub(wsaaAmountIn[2],wsadAmountTot[2]));
		setPrecision(utrnbrkIO.getContractAmount(), 2);
		utrnbrkIO.setContractAmount(sub(wsaaAmountIn[3],wsadAmountTot[3]));
		setPrecision(utrnbrkIO.getFundAmount(), 2);
		utrnbrkIO.setFundAmount(sub(wsaaAmountIn[4],wsadAmountTot[4]));
		setPrecision(utrnbrkIO.getNofUnits(), 5);
		utrnbrkIO.setNofUnits(sub(wsaaAmountIn5[5],wsadAmountTot5[5]));
		setPrecision(utrnbrkIO.getNofDunits(), 5);
		utrnbrkIO.setNofDunits(sub(wsaaAmountIn5[6],wsadAmountTot5[6]));
		if (isLT(genoutrec.newSummary,2)) {
			wsaaPlanSuffix.set(1);
			writeBrkUtrn440();
			return ;
		}
		utrnbrkIO.setFormat(formatsInner.utrnbrkrec);
		utrnbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, utrnbrkIO);
		if (isNE(utrnbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnbrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryUtrn460()
	{
		/*ENTRY*/
		utrnbrkIO.setFunction(varcom.delet);
		utrnbrkIO.setFormat(formatsInner.utrnbrkrec);
		SmartFileCode.execute(appVars, utrnbrkIO);
		if (isNE(utrnbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnbrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void mainProcessingUtrs500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para501();
				}
				case nextr502: {
					nextr502();
				}
				case exit509: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para501()
	{
		if (isEQ(wsaaUtrsNoRecs,1)) {
			utrsbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit509);
		}
		wsabTotCurrUnitBal.set(ZERO);
		wsabTotCurrDunitBal.set(ZERO);
		compute(wsabBrkPlnSuffix, 5).set(add(genoutrec.newSummary,1));
		for (wsaaPlanSuffix.set(wsabBrkPlnSuffix); !(isGT(wsaaPlanSuffix,genoutrec.oldSummary)); wsaaPlanSuffix.add(1)){
			calcWriteUtrs510();
		}
		calcNewSummaryRecUtrs520();
	}

protected void nextr502()
	{
		utrsbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsbrkIO);
		if ((isNE(utrsbrkIO.getStatuz(),varcom.oK))
		&& (isNE(utrsbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrsbrkIO.getParams());
			fatalError9000();
		}
		if (isEQ(utrsbrkIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(utrsbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(utrsbrkIO.getChdrnum(),genoutrec.chdrnum)) {
			utrsbrkIO.setFunction(varcom.rewrt);
			utrsbrkIO.setFormat(formatsInner.utrsbrkrec);
			SmartFileCode.execute(appVars, utrsbrkIO);
			if (isNE(utrsbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(utrsbrkIO.getParams());
				fatalError9000();
			}
			else {
				utrsbrkIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isNE(utrsbrkIO.getPlanSuffix(),ZERO)) {
			utrsbrkIO.setFunction(varcom.rewrt);
			utrsbrkIO.setFormat(formatsInner.utrsbrkrec);
			SmartFileCode.execute(appVars, utrsbrkIO);
			if (isNE(utrsbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(utrsbrkIO.getParams());
				fatalError9000();
			}
			else {
				goTo(GotoLabel.nextr502);
			}
		}
	}

protected void calcWriteUtrs510()
	{
			entry511();
		}

protected void entry511()
	{
		utrnbk1IO.setDataKey(SPACES);
		utrnbk1IO.setChdrcoy(utrsbrkIO.getChdrcoy());
		utrnbk1IO.setChdrnum(utrsbrkIO.getChdrnum());
		utrnbk1IO.setLife(utrsbrkIO.getLife());
		utrnbk1IO.setCoverage(utrsbrkIO.getCoverage());
		utrnbk1IO.setRider(utrsbrkIO.getRider());
		utrnbk1IO.setUnitVirtualFund(utrsbrkIO.getUnitVirtualFund());
		utrnbk1IO.setUnitType(utrsbrkIO.getUnitType());
		utrnbk1IO.setPlanSuffix(wsaaPlanSuffix);
		utrnbk1IO.setTranno(ZERO);
		utrnbk1IO.setFormat(formatsInner.utrnbk1rec);
		utrnbk1IO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrnbk1IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnbk1IO.setFitKeysSearch("CHDRCOY","CHDRNUM","PLNSFX","COVERAGE","VRTFND");
        
		SmartFileCode.execute(appVars, utrnbk1IO);
		if (isNE(utrnbk1IO.getStatuz(),varcom.oK)
		&& isNE(utrnbk1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnbk1IO.getParams());
			syserrrec.statuz.set(utrnbk1IO.getStatuz());
			fatalError9000();
		}
		if (isEQ(utrnbk1IO.getStatuz(),varcom.endp)
		|| isNE(utrnbk1IO.getChdrcoy(),utrsbrkIO.getChdrcoy())
		|| isNE(utrnbk1IO.getChdrnum(),utrsbrkIO.getChdrnum())
		|| isNE(utrnbk1IO.getCoverage(),utrsbrkIO.getCoverage())
		|| isNE(utrnbk1IO.getRider(),utrsbrkIO.getRider())
		|| isNE(utrnbk1IO.getPlanSuffix(),wsaaPlanSuffix)) {
			return ;
		}
		wsabCurrentUnitBal.set(ZERO);
		wsabCurrentDunitBal.set(ZERO);
		while ( !(isEQ(utrnbk1IO.getStatuz(),varcom.endp)
		|| isNE(utrnbk1IO.getChdrcoy(),utrsbrkIO.getChdrcoy())
		|| isNE(utrnbk1IO.getChdrnum(),utrsbrkIO.getChdrnum())
		|| isNE(utrnbk1IO.getLife(),utrsbrkIO.getLife())
		|| isNE(utrnbk1IO.getCoverage(),utrsbrkIO.getCoverage())
		|| isNE(utrnbk1IO.getRider(),utrsbrkIO.getRider())
		|| isNE(utrnbk1IO.getPlanSuffix(),wsaaPlanSuffix)
		|| isNE(utrnbk1IO.getUnitVirtualFund(),utrsbrkIO.getUnitVirtualFund())
		|| isNE(utrnbk1IO.getFeedbackInd(),"Y")
		|| isNE(utrnbk1IO.getUnitType(),utrsbrkIO.getUnitType()))) {
			calcUnits540();
		}
		
		utrsBrkoutRec550();
	}

protected void calcNewSummaryRecUtrs520()
	{
			read521();
		}

protected void read521()
	{
		if (isLT(genoutrec.newSummary,2)) {
			deleteSummaryUtrs530();
			wsaaPlanSuffix.set(1);
			calcWriteUtrs510();
			return ;
		}
		if (isNE(wsabTotCurrUnitBal,ZERO)
		&& isNE(wsabTotCurrDunitBal,ZERO)) {
			setPrecision(utrsbrkIO.getCurrentUnitBal(), 5);
			utrsbrkIO.setCurrentUnitBal(sub(utrsbrkIO.getCurrentUnitBal(),wsabTotCurrUnitBal));
			setPrecision(utrsbrkIO.getCurrentDunitBal(), 5);
			utrsbrkIO.setCurrentDunitBal(sub(utrsbrkIO.getCurrentDunitBal(),wsabTotCurrDunitBal));
		}
		utrsbrkIO.setFormat(formatsInner.utrsbrkrec);
		utrsbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, utrsbrkIO);
		if (isNE(utrsbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsbrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryUtrs530()
	{
		/*READ*/
		utrsbrkIO.setFunction(varcom.delet);
		utrsbrkIO.setFormat(formatsInner.utrsbrkrec);
		SmartFileCode.execute(appVars, utrsbrkIO);
		if (isNE(utrsbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsbrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void calcUnits540()
	{
		/*ENTRY*/
		compute(wsabCurrentUnitBal, 5).set(add(wsabCurrentUnitBal,utrnbk1IO.getNofUnits()));
		compute(wsabCurrentDunitBal, 5).set(add(wsabCurrentDunitBal,utrnbk1IO.getNofDunits()));
		utrnbk1IO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnbk1IO);
		if (isNE(utrnbk1IO.getStatuz(),varcom.oK)
		&& isNE(utrnbk1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnbk1IO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void utrsBrkoutRec550()
	{
			entry551();
		}

protected void entry551()
	{
		if (isEQ(wsabCurrentUnitBal,ZERO)
		&& isEQ(wsabCurrentDunitBal,ZERO)) {
			return ;
		}
		utrsIO.setCurrentUnitBal(wsabCurrentUnitBal);
		utrsIO.setCurrentDunitBal(wsabCurrentDunitBal);
		compute(wsabTotCurrUnitBal, 5).set(add(wsabTotCurrUnitBal,utrsIO.getCurrentUnitBal()));
		compute(wsabTotCurrDunitBal, 5).set(add(wsabTotCurrDunitBal,utrsIO.getCurrentDunitBal()));
		utrsIO.setChdrcoy(utrsbrkIO.getChdrcoy());
		utrsIO.setChdrnum(utrsbrkIO.getChdrnum());
		utrsIO.setLife(utrsbrkIO.getLife());
		utrsIO.setCoverage(utrsbrkIO.getCoverage());
		utrsIO.setRider(utrsbrkIO.getRider());
		utrsIO.setUnitVirtualFund(utrsbrkIO.getUnitVirtualFund());
		utrsIO.setUnitType(utrsbrkIO.getUnitType());
		utrsIO.setPlanSuffix(wsaaPlanSuffix);
		utrsIO.setFormat(formatsInner.utrsrec);
		utrsIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError9000();
		}
	}

protected void mainProcessingUstf600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para601();
				}
				case nextr602: {
					nextr602();
				}
				case exit609: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para601()
	{
		if (isEQ(wsaaUstfNoRecs,1)) {
			ustfbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit609);
		}
		moveUstfValsToTable610();
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			wsadAmountTot[wsaaSub.toInt()].set(ZERO);
			wsadAmountTot5[wsaaSub.toInt()].set(ZERO);
		}
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix,genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			ustfCalcs620();
			moveUstfValsFromTable630();
			writeBrkUstf640();
		}
		calcNewSummaryRecUstf650();
	}

protected void nextr602()
	{
		ustfbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustfbrkIO);
		if ((isNE(ustfbrkIO.getStatuz(),varcom.oK))
		&& (isNE(ustfbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ustfbrkIO.getParams());
			fatalError9000();
		}
		if (isEQ(ustfbrkIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(ustfbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(ustfbrkIO.getChdrcoy(),genoutrec.chdrcoy)) {
			ustfbrkIO.setFunction(varcom.rewrt);
			ustfbrkIO.setFormat(formatsInner.ustfbrkrec);
			SmartFileCode.execute(appVars, ustfbrkIO);
			if (isNE(ustfbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(ustfbrkIO.getParams());
				fatalError9000();
			}
			else {
				ustfbrkIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isNE(ustfbrkIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(ustfbrkIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(ustfbrkIO.getPlanSuffix(),ZERO)) {
			ustfbrkIO.setFunction(varcom.rewrt);
			ustfbrkIO.setFormat(formatsInner.ustfbrkrec);
			SmartFileCode.execute(appVars, ustfbrkIO);
			if (isNE(ustfbrkIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(ustfbrkIO.getParams());
				fatalError9000();
			}
			else {
				goTo(GotoLabel.nextr602);
			}
		}
	}

protected void moveUstfValsToTable610()
	{
		read611();
	}

protected void read611()
	{
		wsaaAmountIn5[1].set(ustfbrkIO.getStmtOpUnits());
		wsaaAmountIn5[2].set(ustfbrkIO.getStmtOpDunits());
		wsaaAmountIn5[3].set(ustfbrkIO.getStmtClUnits());
		wsaaAmountIn5[4].set(ustfbrkIO.getStmtClDunits());
		wsaaAmountIn5[5].set(ustfbrkIO.getStmtTrnUnits());
		wsaaAmountIn5[6].set(ustfbrkIO.getStmtTrnDunits());
		wsaaAmountIn[7].set(ustfbrkIO.getStmtOpFundCash());
		wsaaAmountIn[8].set(ustfbrkIO.getStmtOpContCash());
		wsaaAmountIn[9].set(ustfbrkIO.getStmtClFundCash());
		wsaaAmountIn[10].set(ustfbrkIO.getStmtClContCash());
		wsaaAmountIn[11].set(ustfbrkIO.getStmtTrFundCash());
		wsaaAmountIn[12].set(ustfbrkIO.getStmtTrContCash());
	}

protected void ustfCalcs620()
	{
		/*READ*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,6)); wsaaSub.add(1)){
			calcRemainBreakout7500();
		}
		for (wsaaSub.set(7); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			calcRemainBreakout7000();
		}
		/*EXIT*/
	}

protected void moveUstfValsFromTable630()
	{
		go631();
	}

protected void go631()
	{
		ustfbrkIO.setStmtOpUnits(wsaaAmountOut5[1]);
		ustfbrkIO.setStmtOpDunits(wsaaAmountOut5[2]);
		ustfbrkIO.setStmtClUnits(wsaaAmountOut5[3]);
		ustfbrkIO.setStmtClDunits(wsaaAmountOut5[4]);
		ustfbrkIO.setStmtTrnUnits(wsaaAmountOut5[5]);
		ustfbrkIO.setStmtTrnDunits(wsaaAmountOut5[6]);
		ustfbrkIO.setStmtOpFundCash(wsaaAmountOut[7]);
		ustfbrkIO.setStmtOpContCash(wsaaAmountOut[8]);
		ustfbrkIO.setStmtClFundCash(wsaaAmountOut[9]);
		ustfbrkIO.setStmtClContCash(wsaaAmountOut[10]);
		ustfbrkIO.setStmtTrFundCash(wsaaAmountOut[11]);
		ustfbrkIO.setStmtTrContCash(wsaaAmountOut[12]);
	}

protected void writeBrkUstf640()
	{
		go661();
	}

protected void go661()
	{
		ustfIO.setChdrcoy(ustfbrkIO.getChdrcoy());
		ustfIO.setChdrnum(ustfbrkIO.getChdrnum());
		ustfIO.setPlanSuffix(ustfbrkIO.getPlanSuffix());
		ustfIO.setLife(ustfbrkIO.getLife());
		ustfIO.setCoverage(ustfbrkIO.getCoverage());
		ustfIO.setUstmno(ustfbrkIO.getUstmno());
		ustfIO.setRider(ustfbrkIO.getRider());
		ustfIO.setUnitVirtualFund(ustfbrkIO.getUnitVirtualFund());
		ustfIO.setUnitType(ustfbrkIO.getUnitType());
		ustfIO.setFormat(formatsInner.ustfrec);
		ustfIO.setPlanSuffix(wsaaPlanSuffix);
		ustfIO.setNonKey(ustfbrkIO.getNonKey());
		ustfIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ustfIO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecUstf650()
	{
			read651();
		}

protected void read651()
	{
		if (isLT(genoutrec.newSummary,2)) {
			deleteSummaryUstf660();
		}
		setPrecision(ustfbrkIO.getStmtOpUnits(), 5);
		ustfbrkIO.setStmtOpUnits(sub(wsaaAmountIn5[1],wsadAmountTot5[1]));
		setPrecision(ustfbrkIO.getStmtOpDunits(), 5);
		ustfbrkIO.setStmtOpDunits(sub(wsaaAmountIn5[2],wsadAmountTot5[2]));
		setPrecision(ustfbrkIO.getStmtClUnits(), 5);
		ustfbrkIO.setStmtClUnits(sub(wsaaAmountIn5[3],wsadAmountTot5[3]));
		setPrecision(ustfbrkIO.getStmtClDunits(), 5);
		ustfbrkIO.setStmtClDunits(sub(wsaaAmountIn5[4],wsadAmountTot5[4]));
		setPrecision(ustfbrkIO.getStmtTrnUnits(), 5);
		ustfbrkIO.setStmtTrnUnits(sub(wsaaAmountIn5[5],wsadAmountTot5[5]));
		setPrecision(ustfbrkIO.getStmtTrnDunits(), 5);
		ustfbrkIO.setStmtTrnDunits(sub(wsaaAmountIn5[6],wsadAmountTot5[6]));
		setPrecision(ustfbrkIO.getStmtOpFundCash(), 2);
		ustfbrkIO.setStmtOpFundCash(sub(wsaaAmountIn[7],wsadAmountTot[7]));
		setPrecision(ustfbrkIO.getStmtOpContCash(), 2);
		ustfbrkIO.setStmtOpContCash(sub(wsaaAmountIn[8],wsadAmountTot[8]));
		setPrecision(ustfbrkIO.getStmtClFundCash(), 2);
		ustfbrkIO.setStmtClFundCash(sub(wsaaAmountIn[9],wsadAmountTot[9]));
		setPrecision(ustfbrkIO.getStmtClContCash(), 2);
		ustfbrkIO.setStmtClContCash(sub(wsaaAmountIn[10],wsadAmountTot[10]));
		setPrecision(ustfbrkIO.getStmtTrFundCash(), 2);
		ustfbrkIO.setStmtTrFundCash(sub(wsaaAmountIn[11],wsadAmountTot[11]));
		setPrecision(ustfbrkIO.getStmtTrContCash(), 2);
		ustfbrkIO.setStmtTrContCash(sub(wsaaAmountIn[12],wsadAmountTot[12]));
		if (isLT(genoutrec.newSummary,2)) {
			wsaaPlanSuffix.set(1);
			writeBrkUstf640();
			return ;
		}
		ustfbrkIO.setFormat(formatsInner.ustfbrkrec);
		ustfbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ustfbrkIO);
		if (isNE(ustfbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ustfbrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryUstf660()
	{
		/*READ*/
		ustfbrkIO.setFunction(varcom.delet);
		ustfbrkIO.setFormat(formatsInner.ustfbrkrec);
		SmartFileCode.execute(appVars, ustfbrkIO);
		if (isNE(ustfbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ustfbrkIO.getParams());
			fatalError9000();
		}
		/*EXIT1*/
	}

protected void calcRemainBreakout7000()
	{
		/*GO*/
		if (isEQ(wsaaAmountIn[wsaaSub.toInt()],ZERO)) {
			wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
			return ;
		}
		compute(wsaaAmountOut[wsaaSub.toInt()], 3).setRounded(div(wsaaAmountIn[wsaaSub.toInt()],genoutrec.oldSummary));
		zrdecplrec.amountIn.set(wsaaAmountOut[wsaaSub.toInt()]);
		a000CallRounding();
		wsaaAmountOut[wsaaSub.toInt()].set(zrdecplrec.amountOut);
		compute(wsadAmountTot[wsaaSub.toInt()], 2).set(add(wsadAmountTot[wsaaSub.toInt()],wsaaAmountOut[wsaaSub.toInt()]));
		/*EXIT*/
	}

protected void calcRemainBreakout7500()
	{
		/*GO*/
		if (isEQ(wsaaAmountIn5[wsaaSub.toInt()],ZERO)) {
			wsaaAmountOut5[wsaaSub.toInt()].set(ZERO);
			return ;
		}
		compute(wsaaAmountOut5[wsaaSub.toInt()], 6).setRounded(div(wsaaAmountIn5[wsaaSub.toInt()],genoutrec.oldSummary));
		zrdecplrec.amountIn.set(wsaaAmountOut5[wsaaSub.toInt()]);
		a000CallRounding();
		wsaaAmountOut5[wsaaSub.toInt()].set(zrdecplrec.amountOut);
		compute(wsadAmountTot5[wsaaSub.toInt()], 5).set(add(wsadAmountTot5[wsaaSub.toInt()],wsaaAmountOut5[wsaaSub.toInt()]));
		/*EXIT*/
	}

protected void calcInciRemainBreakout8000()
	{
			go8001();
		}

protected void go8001()
	{
		if (isEQ(wsaaAmountIn[wsaaSub.toInt()],ZERO)) {
			wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
			return ;
		}
		compute(wsaaPeriod1, 2).set(div(wsaaAmountIn[1],wsaaAmountIn[9]));
		compute(wsaaPeriod2, 2).set(div(wsaaAmountIn[2],wsaaAmountIn[9]));
		compute(wsaaPeriod3, 2).set(div(wsaaAmountIn[3],wsaaAmountIn[9]));
		compute(wsaaPeriod4, 2).set(div(wsaaAmountIn[4],wsaaAmountIn[9]));
		compute(wsaaPremToPay1, 2).set(div(wsaaAmountIn[5],wsaaAmountIn[9]));
		compute(wsaaPremToPay2, 2).set(div(wsaaAmountIn[6],wsaaAmountIn[9]));
		compute(wsaaPremToPay3, 2).set(div(wsaaAmountIn[7],wsaaAmountIn[9]));
		compute(wsaaPremToPay4, 2).set(div(wsaaAmountIn[8],wsaaAmountIn[9]));
		compute(wsaaAmountOut[9], 3).setRounded(div(wsaaAmountIn[9],genoutrec.oldSummary));
		compute(wsaaAmountOut[10], 3).setRounded(div(wsaaAmountIn[10],genoutrec.oldSummary));
		compute(wsaaAmountOut[1], 2).set(mult(wsaaPeriod1,wsaaAmountOut[9]));
		compute(wsaaAmountOut[2], 2).set(mult(wsaaPeriod2,wsaaAmountOut[9]));
		compute(wsaaAmountOut[3], 2).set(mult(wsaaPeriod3,wsaaAmountOut[9]));
		compute(wsaaAmountOut[4], 2).set(mult(wsaaPeriod4,wsaaAmountOut[9]));
		compute(wsaaAmountOut[5], 2).set(mult(wsaaPremToPay1,wsaaAmountOut[9]));
		compute(wsaaAmountOut[6], 2).set(mult(wsaaPremToPay2,wsaaAmountOut[9]));
		compute(wsaaAmountOut[7], 2).set(mult(wsaaPremToPay3,wsaaAmountOut[9]));
		compute(wsaaAmountOut[8], 2).set(mult(wsaaPremToPay4,wsaaAmountOut[9]));
		for (wsaaX.set(1); !(isGT(wsaaX, 10)); wsaaX.add(1)){
			zrdecplrec.amountIn.set(wsaaAmountOut[wsaaX.toInt()]);
			a000CallRounding();
			wsaaAmountOut[wsaaX.toInt()].set(zrdecplrec.amountOut);
		}
		/* Totals are calculated to be used later in the calculation       */
		/* of the new summary record.                                      */
		compute(wsadAmountTot[wsaaSub.toInt()], 2).set(add(wsadAmountTot[wsaaSub.toInt()],wsaaAmountOut[wsaaSub.toInt()]));
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		genoutrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(genoutrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(genoutrec.cntcurr);
		zrdecplrec.batctrcde.set(genoutrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData incibrkrec = new FixedLengthStringData(10).init("INCIBRKREC");
	private FixedLengthStringData ulnkbrkrec = new FixedLengthStringData(10).init("ULNKBRKREC");
	private FixedLengthStringData utrsbrkrec = new FixedLengthStringData(10).init("UTRSBRKREC");
	private FixedLengthStringData utrnbrkrec = new FixedLengthStringData(10).init("UTRNBRKREC");
	private FixedLengthStringData ustfbrkrec = new FixedLengthStringData(10).init("USTFBRKREC");
	private FixedLengthStringData incirec = new FixedLengthStringData(10).init("INCIREC");
	private FixedLengthStringData ulnkrec = new FixedLengthStringData(10).init("ULNKREC");
	private FixedLengthStringData utrsrec = new FixedLengthStringData(10).init("UTRSREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData ustfrec = new FixedLengthStringData(10).init("USTFREC");
	private FixedLengthStringData utrnbk1rec = new FixedLengthStringData(10).init("UTRNBK1REC");
}
}
