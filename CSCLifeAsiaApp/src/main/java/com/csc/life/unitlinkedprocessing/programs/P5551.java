/*
 * File: P5551.java
 * Date: 30 August 2009 0:30:49
 * Author: Quipoz Limited
 * 
 * Class transformed from P5551.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.procedures.T5551pt;
import com.csc.life.unitlinkedprocessing.screens.S5551ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
****************************************************************** ****
* </pre>
*/
public class P5551 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5551");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5551rec t5551rec = new T5551rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5551ScreenVars sv = ScreenProgram.getScreenVars( S5551ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public P5551() {
		super();
		screenVars = sv;
		new ScreenModel("S5551", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5551rec.t5551Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5551rec.ageIssageFrm01.set(ZERO);
		t5551rec.ageIssageFrm02.set(ZERO);
		t5551rec.ageIssageFrm03.set(ZERO);
		t5551rec.ageIssageFrm04.set(ZERO);
		t5551rec.ageIssageFrm05.set(ZERO);
		t5551rec.ageIssageFrm06.set(ZERO);
		t5551rec.ageIssageFrm07.set(ZERO);
		t5551rec.ageIssageFrm08.set(ZERO);
		t5551rec.ageIssageTo01.set(ZERO);
		t5551rec.ageIssageTo02.set(ZERO);
		t5551rec.ageIssageTo03.set(ZERO);
		t5551rec.ageIssageTo04.set(ZERO);
		t5551rec.ageIssageTo05.set(ZERO);
		t5551rec.ageIssageTo06.set(ZERO);
		t5551rec.ageIssageTo07.set(ZERO);
		t5551rec.ageIssageTo08.set(ZERO);
		t5551rec.maxLumpSum.set(ZERO);
		t5551rec.minLumpSum.set(ZERO);
		t5551rec.maturityAgeTo01.set(ZERO);
		t5551rec.maturityAgeTo02.set(ZERO);
		t5551rec.maturityAgeTo03.set(ZERO);
		t5551rec.maturityAgeTo04.set(ZERO);
		t5551rec.maturityAgeTo05.set(ZERO);
		t5551rec.maturityAgeTo06.set(ZERO);
		t5551rec.maturityAgeTo07.set(ZERO);
		t5551rec.maturityAgeTo08.set(ZERO);
		t5551rec.maturityAgeFrom01.set(ZERO);
		t5551rec.maturityAgeFrom02.set(ZERO);
		t5551rec.maturityAgeFrom03.set(ZERO);
		t5551rec.maturityAgeFrom04.set(ZERO);
		t5551rec.maturityAgeFrom05.set(ZERO);
		t5551rec.maturityAgeFrom06.set(ZERO);
		t5551rec.maturityAgeFrom07.set(ZERO);
		t5551rec.maturityAgeFrom08.set(ZERO);
		t5551rec.maturityTermFrom01.set(ZERO);
		t5551rec.maturityTermFrom02.set(ZERO);
		t5551rec.maturityTermFrom03.set(ZERO);
		t5551rec.maturityTermFrom04.set(ZERO);
		t5551rec.maturityTermFrom05.set(ZERO);
		t5551rec.maturityTermFrom06.set(ZERO);
		t5551rec.maturityTermFrom07.set(ZERO);
		t5551rec.maturityTermFrom08.set(ZERO);
		t5551rec.maturityTermTo01.set(ZERO);
		t5551rec.maturityTermTo02.set(ZERO);
		t5551rec.maturityTermTo03.set(ZERO);
		t5551rec.maturityTermTo04.set(ZERO);
		t5551rec.maturityTermTo05.set(ZERO);
		t5551rec.maturityTermTo06.set(ZERO);
		t5551rec.maturityTermTo07.set(ZERO);
		t5551rec.maturityTermTo08.set(ZERO);
		t5551rec.premCessageFrom01.set(ZERO);
		t5551rec.premCessageFrom02.set(ZERO);
		t5551rec.premCessageFrom03.set(ZERO);
		t5551rec.premCessageFrom04.set(ZERO);
		t5551rec.premCessageFrom05.set(ZERO);
		t5551rec.premCessageFrom06.set(ZERO);
		t5551rec.premCessageFrom07.set(ZERO);
		t5551rec.premCessageFrom08.set(ZERO);
		t5551rec.premCessageTo01.set(ZERO);
		t5551rec.premCessageTo02.set(ZERO);
		t5551rec.premCessageTo03.set(ZERO);
		t5551rec.premCessageTo04.set(ZERO);
		t5551rec.premCessageTo05.set(ZERO);
		t5551rec.premCessageTo06.set(ZERO);
		t5551rec.premCessageTo07.set(ZERO);
		t5551rec.premCessageTo08.set(ZERO);
		t5551rec.premCesstermFrom01.set(ZERO);
		t5551rec.premCesstermFrom02.set(ZERO);
		t5551rec.premCesstermFrom03.set(ZERO);
		t5551rec.premCesstermFrom04.set(ZERO);
		t5551rec.premCesstermFrom05.set(ZERO);
		t5551rec.premCesstermFrom06.set(ZERO);
		t5551rec.premCesstermFrom07.set(ZERO);
		t5551rec.premCesstermFrom08.set(ZERO);
		t5551rec.premCesstermTo01.set(ZERO);
		t5551rec.premCesstermTo02.set(ZERO);
		t5551rec.premCesstermTo03.set(ZERO);
		t5551rec.premCesstermTo04.set(ZERO);
		t5551rec.premCesstermTo05.set(ZERO);
		t5551rec.premCesstermTo06.set(ZERO);
		t5551rec.premCesstermTo07.set(ZERO);
		t5551rec.premCesstermTo08.set(ZERO);
		t5551rec.sumInsMax.set(ZERO);
		t5551rec.sumInsMin.set(ZERO);
		t5551rec.termIssageFrm01.set(ZERO);
		t5551rec.termIssageFrm02.set(ZERO);
		t5551rec.termIssageFrm03.set(ZERO);
		t5551rec.termIssageFrm04.set(ZERO);
		t5551rec.termIssageFrm05.set(ZERO);
		t5551rec.termIssageFrm06.set(ZERO);
		t5551rec.termIssageFrm07.set(ZERO);
		t5551rec.termIssageFrm08.set(ZERO);
		t5551rec.termIssageTo01.set(ZERO);
		t5551rec.termIssageTo02.set(ZERO);
		t5551rec.termIssageTo03.set(ZERO);
		t5551rec.termIssageTo04.set(ZERO);
		t5551rec.termIssageTo05.set(ZERO);
		t5551rec.termIssageTo06.set(ZERO);
		t5551rec.termIssageTo07.set(ZERO);
		t5551rec.termIssageTo08.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.ageIssageFrms.set(t5551rec.ageIssageFrms);
		sv.ageIssageTos.set(t5551rec.ageIssageTos);
		sv.alfnds.set(t5551rec.alfnds);
		sv.eaage.set(t5551rec.eaage);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.liencds.set(t5551rec.liencds);
		sv.maxLumpSum.set(t5551rec.maxLumpSum);
		sv.minLumpSum.set(t5551rec.minLumpSum);
		sv.maturityAgeTos.set(t5551rec.maturityAgeTos);
		sv.maturityAgeFroms.set(t5551rec.maturityAgeFroms);
		sv.maturityTermFroms.set(t5551rec.maturityTermFroms);
		sv.maturityTermTos.set(t5551rec.maturityTermTos);
		sv.mortclss.set(t5551rec.mortclss);
		sv.premCessageFroms.set(t5551rec.premCessageFroms);
		sv.premCessageTos.set(t5551rec.premCessageTos);
		sv.premCesstermFroms.set(t5551rec.premCesstermFroms);
		sv.premCesstermTos.set(t5551rec.premCesstermTos);
		sv.rsvflg.set(t5551rec.rsvflg);
		sv.specind.set(t5551rec.specind);
		sv.sumInsMax.set(t5551rec.sumInsMax);
		sv.sumInsMin.set(t5551rec.sumInsMin);
		sv.svcmeth.set(t5551rec.svcmeth);
		sv.termIssageFrms.set(t5551rec.termIssageFrms);
		sv.termIssageTos.set(t5551rec.termIssageTos);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5551rec.t5551Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.ageIssageFrms, t5551rec.ageIssageFrms)) {
			t5551rec.ageIssageFrms.set(sv.ageIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ageIssageTos, t5551rec.ageIssageTos)) {
			t5551rec.ageIssageTos.set(sv.ageIssageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.alfnds, t5551rec.alfnds)) {
			t5551rec.alfnds.set(sv.alfnds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.eaage, t5551rec.eaage)) {
			t5551rec.eaage.set(sv.eaage);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.liencds, t5551rec.liencds)) {
			t5551rec.liencds.set(sv.liencds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxLumpSum, t5551rec.maxLumpSum)) {
			t5551rec.maxLumpSum.set(sv.maxLumpSum);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.minLumpSum, t5551rec.minLumpSum)) {
			t5551rec.minLumpSum.set(sv.minLumpSum);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maturityAgeTos, t5551rec.maturityAgeTos)) {
			t5551rec.maturityAgeTos.set(sv.maturityAgeTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maturityAgeFroms, t5551rec.maturityAgeFroms)) {
			t5551rec.maturityAgeFroms.set(sv.maturityAgeFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maturityTermFroms, t5551rec.maturityTermFroms)) {
			t5551rec.maturityTermFroms.set(sv.maturityTermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maturityTermTos, t5551rec.maturityTermTos)) {
			t5551rec.maturityTermTos.set(sv.maturityTermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mortclss, t5551rec.mortclss)) {
			t5551rec.mortclss.set(sv.mortclss);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageFroms, t5551rec.premCessageFroms)) {
			t5551rec.premCessageFroms.set(sv.premCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageTos, t5551rec.premCessageTos)) {
			t5551rec.premCessageTos.set(sv.premCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermFroms, t5551rec.premCesstermFroms)) {
			t5551rec.premCesstermFroms.set(sv.premCesstermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermTos, t5551rec.premCesstermTos)) {
			t5551rec.premCesstermTos.set(sv.premCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rsvflg, t5551rec.rsvflg)) {
			t5551rec.rsvflg.set(sv.rsvflg);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.specind, t5551rec.specind)) {
			t5551rec.specind.set(sv.specind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMax, t5551rec.sumInsMax)) {
			t5551rec.sumInsMax.set(sv.sumInsMax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMin, t5551rec.sumInsMin)) {
			t5551rec.sumInsMin.set(sv.sumInsMin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.svcmeth, t5551rec.svcmeth)) {
			t5551rec.svcmeth.set(sv.svcmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageFrms, t5551rec.termIssageFrms)) {
			t5551rec.termIssageFrms.set(sv.termIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageTos, t5551rec.termIssageTos)) {
			t5551rec.termIssageTos.set(sv.termIssageTos);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5551pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
