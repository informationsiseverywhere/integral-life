/*
 * File: Br607.java
 * Date: 29 August 2009 22:22:53
 * Author: Quipoz Limited
 * 
 * Class transformed from BR607.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br607TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br607DTO;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1959 - 2005.
*    All rights reserved. CSC Confidential.
*
*                    UFNXPF SPLITTER PROGRAM
*                   -------------------------
*
*
* This program is the Splitter program which will split the UFNS
* file containing all pending updates for any deferred Unit Fundsce
* holdings updates.
*
* All records from UFNSPF will be read via SQL and ordered
* by COMPANY, VRTFUND and UNITYP.
*
* Control totals used in this program:
*
*    01  -  No. of UFNS extracted records.
*    02  -  No. of thread members.
*
* The temporary file for this splitter program is UFNXPF with
* many UFNX members. Extracted records will be written to as many
* output members as threads specified in the 'No. of subsequent
* threads' parameter on the process definition for this program.
* Each UFNS record is written to a different UFNX member, unless
* there are several UFNS records for the same bank, in which
* case, these are written to the same UFNX member.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Br607 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
//	private int sqlcode = 0;
//	private boolean sqlerrorflag;
//	private SQLException sqlca = new SQLException();
//	private java.sql.ResultSet sqlufnsCursorrs = null;
//	private java.sql.PreparedStatement sqlufnsCursorps = null;
//	private java.sql.Connection sqlufnsCursorconn = null;
//	private String sqlufnsCursor = "";
//	private int ufnsCursorLoopIndex = 0;
//	private UfnxpfTableDAM ufnxpf = new UfnxpfTableDAM();
//	private DiskFileDAM ufnx01 = new DiskFileDAM("UFNX01");
//	private DiskFileDAM ufnx02 = new DiskFileDAM("UFNX02");
//	private DiskFileDAM ufnx03 = new DiskFileDAM("UFNX03");
//	private DiskFileDAM ufnx04 = new DiskFileDAM("UFNX04");
//	private DiskFileDAM ufnx05 = new DiskFileDAM("UFNX05");
//	private DiskFileDAM ufnx06 = new DiskFileDAM("UFNX06");
//	private DiskFileDAM ufnx07 = new DiskFileDAM("UFNX07");
//	private DiskFileDAM ufnx08 = new DiskFileDAM("UFNX08");
//	private DiskFileDAM ufnx09 = new DiskFileDAM("UFNX09");
//	private DiskFileDAM ufnx10 = new DiskFileDAM("UFNX10");
//	private DiskFileDAM ufnx11 = new DiskFileDAM("UFNX11");
//	private DiskFileDAM ufnx12 = new DiskFileDAM("UFNX12");
//	private DiskFileDAM ufnx13 = new DiskFileDAM("UFNX13");
//	private DiskFileDAM ufnx14 = new DiskFileDAM("UFNX14");
//	private DiskFileDAM ufnx15 = new DiskFileDAM("UFNX15");
//	private DiskFileDAM ufnx16 = new DiskFileDAM("UFNX16");
//	private DiskFileDAM ufnx17 = new DiskFileDAM("UFNX17");
//	private DiskFileDAM ufnx18 = new DiskFileDAM("UFNX18");
//	private DiskFileDAM ufnx19 = new DiskFileDAM("UFNX19");
//	private DiskFileDAM ufnx20 = new DiskFileDAM("UFNX20");
//	private FixedLengthStringData ufnx01Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx02Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx03Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx04Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx05Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx06Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx07Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx08Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx09Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx10Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx11Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx12Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx13Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx14Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx15Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx16Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx17Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx18Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx19Rec = new FixedLengthStringData(15);
//	private FixedLengthStringData ufnx20Rec = new FixedLengthStringData(15);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR607");
//	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaUfnsCompany = new FixedLengthStringData(1);
	private PackedDecimalData wsaaUfnsJobno = new PackedDecimalData(8, 0);
		/* ERRORS */
//	private String ivrm = "IVRM";
	private String esql = "ESQL";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

//	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
//	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

//	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
//	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaUfnxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaUfnxFn, 0, FILLER).init("UFNX");
	private FixedLengthStringData wsaaUfnxRunid = new FixedLengthStringData(2).isAPartOf(wsaaUfnxFn, 4);
	private ZonedDecimalData wsaaUfnxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUfnxFn, 6).setUnsigned();
//	private PackedDecimalData iy = new PackedDecimalData(5, 0);
//	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

//	private FixedLengthStringData wsaaIndArray = new FixedLengthStringData(12000);
//	private FixedLengthStringData[] wsaaUfnsInd = FLSArrayPartOfStructure(1000, 12, wsaaIndArray, 0);
//	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(6, 4, 0, wsaaUfnsInd, 0);
		/* WSAA-HOST-VARIABLES */
//	private FixedLengthStringData wsaaPrevCompany = new FixedLengthStringData(1).init(SPACES);
//	private FixedLengthStringData wsaaPrevVrtfund = new FixedLengthStringData(4).init(SPACES);
//	private FixedLengthStringData wsaaPrevUnittype = new FixedLengthStringData(1).init(SPACES);

//	private FixedLengthStringData wsaaFetchArray = new FixedLengthStringData(15000);
//	private FixedLengthStringData[] wsaaUfnsData = FLSArrayPartOfStructure(1000, 15, wsaaFetchArray, 0);
//	private FixedLengthStringData[] wsaaCompany = FLSDArrayPartOfArrayStructure(1, wsaaUfnsData, 0);
//	private FixedLengthStringData[] wsaaVrtfund = FLSDArrayPartOfArrayStructure(4, wsaaUfnsData, 1);
//	private FixedLengthStringData[] wsaaUnittype = FLSDArrayPartOfArrayStructure(1, wsaaUfnsData, 5);
//	private PackedDecimalData[] wsaaNofunts = PDArrayPartOfArrayStructure(16, 5, wsaaUfnsData, 6);
//	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
//	private IntegerData wsaaInd = new IntegerData();
		/*DEFERRED FUND BALANCE UPDATE FILE*/
//	private UfnspfTableDAM ufnspfData = new UfnspfTableDAM();
		/*TEMP FILE FOR TOTAL UNIT HOLDING*/
//	private UfnxpfTableDAM ufnxpfData = new UfnxpfTableDAM();

    private int ctrCT02;
    
    private Br607TempDAO tempDAO = getApplicationContext().getBean("br607TempDAO", Br607TempDAO.class);
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public Br607() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

	protected void initialise1000() {
		//wsaaUfnxRunid.set(bprdIO.getSystemParam04());
		wsaaUfnsCompany.set(bsprIO.getCompany());
		wsaaUfnsJobno.set(bsscIO.getScheduleNumber());
		//wsaaUfnxJobno.set(bsscIO.getScheduleNumber());
		//wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		Br607DTO dto = new Br607DTO();
		dto.setTableName("UFNXPF");
		dto.setCompany(wsaaUfnsCompany.toString());
		dto.setJobNo(wsaaUfnsJobno.toInt());
		ctrCT02 = tempDAO.buildTempData(dto);
	}

//protected void initialise1010()
//	{
////		if (isNE(bprdIO.getRestartMethod(),"1")) {
////			syserrrec.statuz.set(ivrm);
////			fatalError600();
////		}
////		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
////			bprdIO.setThreadsSubsqntProc(20);
////		}
//
//		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
//			openThreadMember1100();
//		}
//		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
//		contotrec.totno.set(ct01);
//		callContot001();
//		iy.set(1);
//		sqlufnsCursor = " SELECT  COMPANY, VRTFUND, UNITYP, NOFUNTS, JOBNO" +
//" FROM   " + appVars.getTableNameOverriden("UFNSPF") + " " +
//" WHERE COMPANY = ?" +
//" AND JOBNO = ?" +
//" ORDER BY COMPANY, VRTFUND, UNITYP";
//		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
//			initialiseArray1500();
//		}
//		wsaaInd.set(1);
//		sqlerrorflag = false;
//		try {
//			sqlufnsCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.enquiries.dataaccess.UfnspfTableDAM());
//			sqlufnsCursorps = appVars.prepareStatementEmbeded(sqlufnsCursorconn, sqlufnsCursor, "UFNSPF");
//			appVars.setDBString(sqlufnsCursorps, 1, wsaaUfnsCompany.toString());
//			appVars.setDBDouble(sqlufnsCursorps, 2, wsaaUfnsJobno.toDouble());
//			sqlufnsCursorrs = appVars.executeQuery(sqlufnsCursorps);
//		}
//		catch (SQLException ex){
//			sqlca = ex;
//			sqlerrorflag = true;
//		}
//		if (sqlerrorflag) {
//			sqlError500();
//		}
//	}

//protected void openThreadMember1100()
//	{
//		openThreadMember1110();
//	}

//protected void openThreadMember1110()
//	{
//		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
//		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaUfnxFn, wsaaThreadMember, wsaaStatuz);
//		if (isNE(wsaaStatuz,varcom.oK)) {
//			StringBuilder stringVariable1 = new StringBuilder();
//			stringVariable1.append("CLRTMPF");
//			stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
//			stringVariable1.append(SPACES);
//			stringVariable1.append(wsaaUfnxFn.toString());
//			stringVariable1.append(SPACES);
//			stringVariable1.append(wsaaThreadMember.toString());
//			wsaaClrtmpfError.setLeft(stringVariable1.toString());
//			syserrrec.statuz.set(wsaaStatuz);
//			syserrrec.params.set(wsaaClrtmpfError);
//			fatalError600();
//		}
//		StringBuilder stringVariable2 = new StringBuilder();
//		stringVariable2.append("OVRDBF FILE(UFNX");
//		stringVariable2.append(iz.toString());
//		stringVariable2.append(") TOFILE(");
//		stringVariable2.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
//		stringVariable2.append("/");
//		stringVariable2.append(wsaaUfnxFn.toString());
//		stringVariable2.append(") MBR(");
//		stringVariable2.append(wsaaThreadMember.toString());
//		stringVariable2.append(")");
//		stringVariable2.append(" SEQONLY(*YES 1000)");
//		wsaaQcmdexc.setLeft(stringVariable2.toString());
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//		if (isEQ(iz,1)) {
//			ufnx01.openOutput();
//		}
//		if (isEQ(iz,2)) {
//			ufnx02.openOutput();
//		}
//		if (isEQ(iz,3)) {
//			ufnx03.openOutput();
//		}
//		if (isEQ(iz,4)) {
//			ufnx04.openOutput();
//		}
//		if (isEQ(iz,5)) {
//			ufnx05.openOutput();
//		}
//		if (isEQ(iz,6)) {
//			ufnx06.openOutput();
//		}
//		if (isEQ(iz,7)) {
//			ufnx07.openOutput();
//		}
//		if (isEQ(iz,8)) {
//			ufnx08.openOutput();
//		}
//		if (isEQ(iz,9)) {
//			ufnx09.openOutput();
//		}
//		if (isEQ(iz,10)) {
//			ufnx10.openOutput();
//		}
//		if (isEQ(iz,11)) {
//			ufnx11.openOutput();
//		}
//		if (isEQ(iz,12)) {
//			ufnx12.openOutput();
//		}
//		if (isEQ(iz,13)) {
//			ufnx13.openOutput();
//		}
//		if (isEQ(iz,14)) {
//			ufnx14.openOutput();
//		}
//		if (isEQ(iz,15)) {
//			ufnx15.openOutput();
//		}
//		if (isEQ(iz,16)) {
//			ufnx16.openOutput();
//		}
//		if (isEQ(iz,17)) {
//			ufnx17.openOutput();
//		}
//		if (isEQ(iz,18)) {
//			ufnx18.openOutput();
//		}
//		if (isEQ(iz,19)) {
//			ufnx19.openOutput();
//		}
//		if (isEQ(iz,20)) {
//			ufnx20.openOutput();
//		}
//	}

//protected void initialiseArray1500()
//	{
//		/*START*/
//		wsaaNofunts[wsaaInd.toInt()].set(ZERO);
//		wsaaCompany[wsaaInd.toInt()].set(SPACES);
//		wsaaVrtfund[wsaaInd.toInt()].set(SPACES);
//		wsaaUnittype[wsaaInd.toInt()].set(SPACES);
//		/*EXIT*/
//	}

	protected void readFile2000() {
		wsspEdterror.set(varcom.endp);
	}

//protected void readFiles2010()
//	{
//		if (eofInBlock.isTrue()) {
//			wsspEdterror.set(varcom.endp);
//			goTo(GotoLabel.exit2090);
//		}
//		if (isNE(wsaaInd,1)) {
//			goTo(GotoLabel.exit2090);
//		}
//		sqlerrorflag = false;
//		try {
//			for (ufnsCursorLoopIndex = 1; isLTE(ufnsCursorLoopIndex,wsaaRowsInBlock.toInt())
//			&& sqlufnsCursorrs.next(); ufnsCursorLoopIndex++ ){
//				// fix bug961
////				appVars.getDBObject(sqlufnsCursorrs, 1, wsaaUfnsData[ufnsCursorLoopIndex]);
////				appVars.getDBObject(sqlufnsCursorrs, 2, wsaaUfnsInd[ufnsCursorLoopIndex]);
//				appVars.getDBObject(sqlufnsCursorrs, 1, wsaaCompany[ufnsCursorLoopIndex]);
//				appVars.getDBObject(sqlufnsCursorrs, 2, wsaaVrtfund[ufnsCursorLoopIndex]);
//				appVars.getDBObject(sqlufnsCursorrs, 3, wsaaUnittype[ufnsCursorLoopIndex]);
//				appVars.getDBObject(sqlufnsCursorrs, 4, wsaaNofunts[ufnsCursorLoopIndex]);
//			}
//		}
//		catch (SQLException ex){
//			sqlca = ex;
//			sqlerrorflag = true;
//		}
//		if (sqlerrorflag) {
//			sqlError500();
//		}
//		if (isEQ(sqlca.getErrorCode(),100)) {
//			wsspEdterror.set(varcom.endp);
//		} else if (isEQ(wsaaCompany[wsaaInd.toInt()], SPACES)) {
//			wsspEdterror.set(varcom.endp);
//		} else {
//			if (firstTime.isTrue()) {
//				wsaaPrevCompany.set(wsaaCompany[wsaaInd.toInt()]);
//				wsaaPrevVrtfund.set(wsaaVrtfund[wsaaInd.toInt()]);
//				wsaaPrevUnittype.set(wsaaUnittype[wsaaInd.toInt()]);
//				wsaaFirstTime.set("N");
//			}
//		}
//	}

	protected void edit2500() {
		/* READ */
		// wsspEdterror.set(varcom.oK);
		/* EXIT */
	}

	protected void update3000() {

	}

//protected void loadThreads3100()
//	{
//		/*START*/
//		if (isNE(wsaaCompany[wsaaInd.toInt()],wsaaPrevCompany)
//		|| isNE(wsaaVrtfund[wsaaInd.toInt()],wsaaPrevVrtfund)
//		|| isNE(wsaaUnittype[wsaaInd.toInt()],wsaaPrevUnittype)) {
//			iy.add(1);
//			wsaaPrevCompany.set(SPACES);
//			wsaaPrevVrtfund.set(SPACES);
//			wsaaPrevUnittype.set(SPACES);
//			wsaaPrevCompany.set(wsaaCompany[wsaaInd.toInt()]);
//			wsaaPrevVrtfund.set(wsaaVrtfund[wsaaInd.toInt()]);
//			wsaaPrevUnittype.set(wsaaUnittype[wsaaInd.toInt()]);
//		}
//		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
//		|| isNE(wsaaCompany[wsaaInd.toInt()],wsaaPrevCompany)
//		|| isNE(wsaaVrtfund[wsaaInd.toInt()],wsaaPrevVrtfund)
//		|| isNE(wsaaUnittype[wsaaInd.toInt()],wsaaPrevUnittype)
//		|| eofInBlock.isTrue())) {
//			loadSameContracts3200();
//		}
//		
//		/*EXIT*/
//	}

//protected void loadSameContracts3200()
//	{
//		start3200();
//	}

//protected void start3200()
//	{
//		ufnxpfData.company.set(wsaaCompany[wsaaInd.toInt()]);
//		ufnxpfData.virtualFund.set(wsaaVrtfund[wsaaInd.toInt()]);
//		ufnxpfData.unitType.set(wsaaUnittype[wsaaInd.toInt()]);
//		ufnxpfData.nofunts.set(wsaaNofunts[wsaaInd.toInt()]);
//		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
//			iy.set(1);
//		}
//		if (isEQ(iy,1)) {
//			ufnx01.write(ufnxpfData);
//		}
//		if (isEQ(iy,2)) {
//			ufnx02.write(ufnxpfData);
//		}
//		if (isEQ(iy,3)) {
//			ufnx03.write(ufnxpfData);
//		}
//		if (isEQ(iy,4)) {
//			ufnx04.write(ufnxpfData);
//		}
//		if (isEQ(iy,5)) {
//			ufnx05.write(ufnxpfData);
//		}
//		if (isEQ(iy,6)) {
//			ufnx06.write(ufnxpfData);
//		}
//		if (isEQ(iy,7)) {
//			ufnx07.write(ufnxpfData);
//		}
//		if (isEQ(iy,8)) {
//			ufnx08.write(ufnxpfData);
//		}
//		if (isEQ(iy,9)) {
//			ufnx09.write(ufnxpfData);
//		}
//		if (isEQ(iy,10)) {
//			ufnx10.write(ufnxpfData);
//		}
//		if (isEQ(iy,11)) {
//			ufnx11.write(ufnxpfData);
//		}
//		if (isEQ(iy,12)) {
//			ufnx12.write(ufnxpfData);
//		}
//		if (isEQ(iy,13)) {
//			ufnx13.write(ufnxpfData);
//		}
//		if (isEQ(iy,14)) {
//			ufnx14.write(ufnxpfData);
//		}
//		if (isEQ(iy,15)) {
//			ufnx15.write(ufnxpfData);
//		}
//		if (isEQ(iy,16)) {
//			ufnx16.write(ufnxpfData);
//		}
//		if (isEQ(iy,17)) {
//			ufnx17.write(ufnxpfData);
//		}
//		if (isEQ(iy,18)) {
//			ufnx18.write(ufnxpfData);
//		}
//		if (isEQ(iy,19)) {
//			ufnx19.write(ufnxpfData);
//		}
//		if (isEQ(iy,20)) {
//			ufnx20.write(ufnxpfData);
//		}
//		contotrec.totval.set(1);
//		contotrec.totno.set(ct02);
//		callContot001();
//		wsaaInd.add(1);
//		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaCompany[wsaaInd.toInt()],SPACES)) {
//			wsaaEofInBlock.set("Y");
//		}
//	}

	protected void commit3500() {
		commitControlTotals();
	}

	protected void commitControlTotals() {
		/* Log the number of threads being used */
		contotrec.totno.set(ct01);
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		callContot001();

		/* Log the number of extracted records. */
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callContot001();
		ctrCT02 = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

	protected void close4000() {
	}


//protected void sqlError500()
//	{
//		/*CALL-SYSTEM-ERROR*/
//		sqlErrorCode.set(sqlca.getErrorCode());
//		StringBuilder stringVariable1 = new StringBuilder();
//		stringVariable1.append(sqlSign.toString());
//		stringVariable1.append(sqlStatuz.toString());
//		wsaaSqlcode.setLeft(stringVariable1.toString());
//		wsaaSqlmessage.set(sqlca.getMessage());
//		syserrrec.statuz.set(esql);
//		syserrrec.params.set(wsaaSqlError);
//		fatalError600();
//	}
}
