package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6220
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6220ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(3);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(dataFields, 0, FILLER);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 1);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(errorIndicators, 0, FILLER);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 2);
	public FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(outputIndicators, 0, FILLER);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(120);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(38).isAPartOf(subfileArea, 0);
	public FixedLengthStringData change = DD.change.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public ZonedDecimalData jobno = DD.jobno.copyToZonedDecimal().isAPartOf(subfileFields,9);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData validtype = DD.validtype.copy().isAPartOf(subfileFields,18);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 38);
	public FixedLengthStringData changeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData jobnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData validtypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 58);
	public FixedLengthStringData[] changeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] jobnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] validtypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 118);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S6220screensflWritten = new LongData(0);
	public LongData S6220screenctlWritten = new LongData(0);
	public LongData S6220screenWritten = new LongData(0);
	public LongData S6220protectWritten = new LongData(0);
	public GeneralTable s6220screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6220screensfl;
	}

	public S6220ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {select, jobno, change, effdate, validtype};
		screenSflOutFields = new BaseData[][] {selectOut, jobnoOut, changeOut, effdateOut, validtypeOut};
		screenSflErrFields = new BaseData[] {selectErr, jobnoErr, changeErr, effdateErr, validtypeErr};
		screenSflDateFields = new BaseData[] {effdate};
		screenSflDateErrFields = new BaseData[] {effdateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp};

		screenFields = new BaseData[] {};
		screenOutFields = new BaseData[][] {};
		screenErrFields = new BaseData[] {};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6220screen.class;
		screenSflRecord = S6220screensfl.class;
		screenCtlRecord = S6220screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6220protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6220screenctl.lrec.pageSubfile);
	}
}
