package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:25
 * Description:
 * Copybook name: ZSTAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zstakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zstaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zstaKey = new FixedLengthStringData(64).isAPartOf(zstaFileKey, 0, REDEFINE);
  	public FixedLengthStringData zstaChdrcoy = new FixedLengthStringData(1).isAPartOf(zstaKey, 0);
  	public FixedLengthStringData zstaChdrnum = new FixedLengthStringData(8).isAPartOf(zstaKey, 1);
  	public PackedDecimalData zstaUstmno = new PackedDecimalData(5, 0).isAPartOf(zstaKey, 9);
  	public PackedDecimalData zstaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(zstaKey, 12);
  	public FixedLengthStringData zstaLife = new FixedLengthStringData(2).isAPartOf(zstaKey, 15);
  	public FixedLengthStringData zstaCoverage = new FixedLengthStringData(2).isAPartOf(zstaKey, 17);
  	public FixedLengthStringData zstaRider = new FixedLengthStringData(2).isAPartOf(zstaKey, 19);
  	public FixedLengthStringData zstaUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(zstaKey, 21);
  	public FixedLengthStringData zstaUnitType = new FixedLengthStringData(1).isAPartOf(zstaKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(zstaKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zstaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zstaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}