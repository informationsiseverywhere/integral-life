package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UtrnrgwTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:16
 * Class transformed from UTRNRGW.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UtrnrgwTableDAM extends UtrnpfTableDAM {

	public UtrnrgwTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UTRNRGW");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VRTFND, " +
		            "UNITYP, " +
		            "TRANNO, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "FUNDRATE, " +
		            "UNITSA, " +
		            "STRPDATE, " +
		            "NDFIND, " +
		            "NOFUNT, " +
		            "NOFDUNT, " +
		            "MONIESDT, " +
		            "PRICEDT, " +
		            "PRICEUSED, " +
		            "UBREPR, " +
		            "CRTABLE, " +
		            "CNTCURR, " +
		            "FDBKIND, " +
		            "USTMNO, " +
		            "CNTAMNT, " +
		            "FNDCURR, " +
		            "FUNDAMNT, " +
		            "SACSCODE, " +
		            "SACSTYP, " +
		            "GENLCDE, " +
		            "CONTYP, " +
		            "TRIGER, " +
		            "TRIGKY, " +
		            "PRCSEQ, " +
		            "SVP, " +
		            "DISCFA, " +
		            "PERSUR, " +
		            "CRCDTE, " +
		            "CIUIND, " +
		            "SWCHIND, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               unitVirtualFund,
                               unitType,
                               tranno,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               fundRate,
                               unitSubAccount,
                               strpdate,
                               nowDeferInd,
                               nofUnits,
                               nofDunits,
                               moniesDate,
                               priceDateUsed,
                               priceUsed,
                               unitBarePrice,
                               crtable,
                               cntcurr,
                               feedbackInd,
                               ustmno,
                               contractAmount,
                               fundCurrency,
                               fundAmount,
                               sacscode,
                               sacstyp,
                               genlcde,
                               contractType,
                               triggerModule,
                               triggerKey,
                               procSeqNo,
                               svp,
                               discountFactor,
                               surrenderPercent,
                               crComDate,
                               canInitUnitInd,
                               switchIndicator,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(52);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller90.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(250);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getFundRate().toInternal()
					+ getUnitSubAccount().toInternal()
					+ getStrpdate().toInternal()
					+ getNowDeferInd().toInternal()
					+ getNofUnits().toInternal()
					+ getNofDunits().toInternal()
					+ getMoniesDate().toInternal()
					+ getPriceDateUsed().toInternal()
					+ getPriceUsed().toInternal()
					+ getUnitBarePrice().toInternal()
					+ getCrtable().toInternal()
					+ getCntcurr().toInternal()
					+ getFeedbackInd().toInternal()
					+ getUstmno().toInternal()
					+ getContractAmount().toInternal()
					+ getFundCurrency().toInternal()
					+ getFundAmount().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getGenlcde().toInternal()
					+ getContractType().toInternal()
					+ getTriggerModule().toInternal()
					+ getTriggerKey().toInternal()
					+ getProcSeqNo().toInternal()
					+ getSvp().toInternal()
					+ getDiscountFactor().toInternal()
					+ getSurrenderPercent().toInternal()
					+ getCrComDate().toInternal()
					+ getCanInitUnitInd().toInternal()
					+ getSwitchIndicator().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, fundRate);
			what = ExternalData.chop(what, unitSubAccount);
			what = ExternalData.chop(what, strpdate);
			what = ExternalData.chop(what, nowDeferInd);
			what = ExternalData.chop(what, nofUnits);
			what = ExternalData.chop(what, nofDunits);
			what = ExternalData.chop(what, moniesDate);
			what = ExternalData.chop(what, priceDateUsed);
			what = ExternalData.chop(what, priceUsed);
			what = ExternalData.chop(what, unitBarePrice);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, feedbackInd);
			what = ExternalData.chop(what, ustmno);
			what = ExternalData.chop(what, contractAmount);
			what = ExternalData.chop(what, fundCurrency);
			what = ExternalData.chop(what, fundAmount);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, genlcde);
			what = ExternalData.chop(what, contractType);
			what = ExternalData.chop(what, triggerModule);
			what = ExternalData.chop(what, triggerKey);
			what = ExternalData.chop(what, procSeqNo);
			what = ExternalData.chop(what, svp);
			what = ExternalData.chop(what, discountFactor);
			what = ExternalData.chop(what, surrenderPercent);
			what = ExternalData.chop(what, crComDate);
			what = ExternalData.chop(what, canInitUnitInd);
			what = ExternalData.chop(what, switchIndicator);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}	
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public PackedDecimalData getFundRate() {
		return fundRate;
	}
	public void setFundRate(Object what) {
		setFundRate(what, false);
	}
	public void setFundRate(Object what, boolean rounded) {
		if (rounded)
			fundRate.setRounded(what);
		else
			fundRate.set(what);
	}	
	public FixedLengthStringData getUnitSubAccount() {
		return unitSubAccount;
	}
	public void setUnitSubAccount(Object what) {
		unitSubAccount.set(what);
	}	
	public PackedDecimalData getStrpdate() {
		return strpdate;
	}
	public void setStrpdate(Object what) {
		setStrpdate(what, false);
	}
	public void setStrpdate(Object what, boolean rounded) {
		if (rounded)
			strpdate.setRounded(what);
		else
			strpdate.set(what);
	}	
	public FixedLengthStringData getNowDeferInd() {
		return nowDeferInd;
	}
	public void setNowDeferInd(Object what) {
		nowDeferInd.set(what);
	}	
	public PackedDecimalData getNofUnits() {
		return nofUnits;
	}
	public void setNofUnits(Object what) {
		setNofUnits(what, false);
	}
	public void setNofUnits(Object what, boolean rounded) {
		if (rounded)
			nofUnits.setRounded(what);
		else
			nofUnits.set(what);
	}	
	public PackedDecimalData getNofDunits() {
		return nofDunits;
	}
	public void setNofDunits(Object what) {
		setNofDunits(what, false);
	}
	public void setNofDunits(Object what, boolean rounded) {
		if (rounded)
			nofDunits.setRounded(what);
		else
			nofDunits.set(what);
	}	
	public PackedDecimalData getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(Object what) {
		setMoniesDate(what, false);
	}
	public void setMoniesDate(Object what, boolean rounded) {
		if (rounded)
			moniesDate.setRounded(what);
		else
			moniesDate.set(what);
	}	
	public PackedDecimalData getPriceDateUsed() {
		return priceDateUsed;
	}
	public void setPriceDateUsed(Object what) {
		setPriceDateUsed(what, false);
	}
	public void setPriceDateUsed(Object what, boolean rounded) {
		if (rounded)
			priceDateUsed.setRounded(what);
		else
			priceDateUsed.set(what);
	}	
	public PackedDecimalData getPriceUsed() {
		return priceUsed;
	}
	public void setPriceUsed(Object what) {
		setPriceUsed(what, false);
	}
	public void setPriceUsed(Object what, boolean rounded) {
		if (rounded)
			priceUsed.setRounded(what);
		else
			priceUsed.set(what);
	}	
	public PackedDecimalData getUnitBarePrice() {
		return unitBarePrice;
	}
	public void setUnitBarePrice(Object what) {
		setUnitBarePrice(what, false);
	}
	public void setUnitBarePrice(Object what, boolean rounded) {
		if (rounded)
			unitBarePrice.setRounded(what);
		else
			unitBarePrice.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(Object what) {
		feedbackInd.set(what);
	}	
	public PackedDecimalData getUstmno() {
		return ustmno;
	}
	public void setUstmno(Object what) {
		setUstmno(what, false);
	}
	public void setUstmno(Object what, boolean rounded) {
		if (rounded)
			ustmno.setRounded(what);
		else
			ustmno.set(what);
	}	
	public PackedDecimalData getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(Object what) {
		setContractAmount(what, false);
	}
	public void setContractAmount(Object what, boolean rounded) {
		if (rounded)
			contractAmount.setRounded(what);
		else
			contractAmount.set(what);
	}	
	public FixedLengthStringData getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(Object what) {
		fundCurrency.set(what);
	}	
	public PackedDecimalData getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(Object what) {
		setFundAmount(what, false);
	}
	public void setFundAmount(Object what, boolean rounded) {
		if (rounded)
			fundAmount.setRounded(what);
		else
			fundAmount.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}	
	public FixedLengthStringData getGenlcde() {
		return genlcde;
	}
	public void setGenlcde(Object what) {
		genlcde.set(what);
	}	
	public FixedLengthStringData getContractType() {
		return contractType;
	}
	public void setContractType(Object what) {
		contractType.set(what);
	}	
	public FixedLengthStringData getTriggerModule() {
		return triggerModule;
	}
	public void setTriggerModule(Object what) {
		triggerModule.set(what);
	}	
	public FixedLengthStringData getTriggerKey() {
		return triggerKey;
	}
	public void setTriggerKey(Object what) {
		triggerKey.set(what);
	}	
	public PackedDecimalData getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(Object what) {
		setProcSeqNo(what, false);
	}
	public void setProcSeqNo(Object what, boolean rounded) {
		if (rounded)
			procSeqNo.setRounded(what);
		else
			procSeqNo.set(what);
	}	
	public PackedDecimalData getSvp() {
		return svp;
	}
	public void setSvp(Object what) {
		setSvp(what, false);
	}
	public void setSvp(Object what, boolean rounded) {
		if (rounded)
			svp.setRounded(what);
		else
			svp.set(what);
	}	
	public PackedDecimalData getDiscountFactor() {
		return discountFactor;
	}
	public void setDiscountFactor(Object what) {
		setDiscountFactor(what, false);
	}
	public void setDiscountFactor(Object what, boolean rounded) {
		if (rounded)
			discountFactor.setRounded(what);
		else
			discountFactor.set(what);
	}	
	public PackedDecimalData getSurrenderPercent() {
		return surrenderPercent;
	}
	public void setSurrenderPercent(Object what) {
		setSurrenderPercent(what, false);
	}
	public void setSurrenderPercent(Object what, boolean rounded) {
		if (rounded)
			surrenderPercent.setRounded(what);
		else
			surrenderPercent.set(what);
	}	
	public PackedDecimalData getCrComDate() {
		return crComDate;
	}
	public void setCrComDate(Object what) {
		setCrComDate(what, false);
	}
	public void setCrComDate(Object what, boolean rounded) {
		if (rounded)
			crComDate.setRounded(what);
		else
			crComDate.set(what);
	}	
	public FixedLengthStringData getCanInitUnitInd() {
		return canInitUnitInd;
	}
	public void setCanInitUnitInd(Object what) {
		canInitUnitInd.set(what);
	}	
	public FixedLengthStringData getSwitchIndicator() {
		return switchIndicator;
	}
	public void setSwitchIndicator(Object what) {
		switchIndicator.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		unitVirtualFund.clear();
		unitType.clear();
		nonKeyFiller90.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		fundRate.clear();
		unitSubAccount.clear();
		strpdate.clear();
		nowDeferInd.clear();
		nofUnits.clear();
		nofDunits.clear();
		moniesDate.clear();
		priceDateUsed.clear();
		priceUsed.clear();
		unitBarePrice.clear();
		crtable.clear();
		cntcurr.clear();
		feedbackInd.clear();
		ustmno.clear();
		contractAmount.clear();
		fundCurrency.clear();
		fundAmount.clear();
		sacscode.clear();
		sacstyp.clear();
		genlcde.clear();
		contractType.clear();
		triggerModule.clear();
		triggerKey.clear();
		procSeqNo.clear();
		svp.clear();
		discountFactor.clear();
		surrenderPercent.clear();
		crComDate.clear();
		canInitUnitInd.clear();
		switchIndicator.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}