package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:54
 * Description:
 * Copybook name: UTRSSWHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsswhkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsswhFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData utrsswhKey = new FixedLengthStringData(256).isAPartOf(utrsswhFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsswhChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsswhKey, 0);
  	public FixedLengthStringData utrsswhChdrnum = new FixedLengthStringData(8).isAPartOf(utrsswhKey, 1);
  	public PackedDecimalData utrsswhPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsswhKey, 9);
  	public FixedLengthStringData utrsswhLife = new FixedLengthStringData(2).isAPartOf(utrsswhKey, 12);
  	public FixedLengthStringData utrsswhCoverage = new FixedLengthStringData(2).isAPartOf(utrsswhKey, 14);
  	public FixedLengthStringData utrsswhRider = new FixedLengthStringData(2).isAPartOf(utrsswhKey, 16);
  	public FixedLengthStringData utrsswhUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrsswhKey, 18);
  	public FixedLengthStringData utrsswhUnitType = new FixedLengthStringData(1).isAPartOf(utrsswhKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(utrsswhKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsswhFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsswhFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}