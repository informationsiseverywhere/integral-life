package com.csc.life.unitlinkedprocessing.recordstructures;

import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:09
 * Description:
 * Copybook name: CBNDEFIND
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cbndefind extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData nowDeferInd = new FixedLengthStringData(1);
  	public Validator now = new Validator(nowDeferInd, "N");
  	public Validator defer = new Validator(nowDeferInd, "D");
  	public FixedLengthStringData nowVal = new FixedLengthStringData(1).init("N");
  	public FixedLengthStringData deferVal = new FixedLengthStringData(1).init("D");


	public void initialize() {
		COBOLFunctions.initialize(nowDeferInd);
		COBOLFunctions.initialize(nowVal);
		COBOLFunctions.initialize(deferVal);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		nowDeferInd.isAPartOf(baseString, true);
    		nowVal.isAPartOf(baseString, true);
    		deferVal.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}