package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Dry5102DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UtrnpfDAOImpl extends BaseDAOImpl<Utrnpf> implements UtrnpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UtrnpfDAOImpl.class);
	private String QUERY_SQL = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, VRTFND, UNITYP, TRANNO, BATCTRCDE, UNITSA, NDFIND, NOFUNT, NOFDUNT, MONIESDT, CNTCURR, FDBKIND, COVDBTIND, CNTAMNT, FUNDAMNT, CONTYP, PRCSEQ, PERSUR, SWCHIND, FUNDRATE,FUNDPOOL  ";
	private String QUERY_SQL_ORDER = " ORDER BY CHDRCOY, CHDRNUM, PRCSEQ, VRTFND, UNITYP ";
	public List<Utrnpf> searchUtrnRecord(Utrnpf utrnpfData) {
		StringBuilder sqlUtrnSelect1 = new StringBuilder(
				"SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, FDBKIND");
		sqlUtrnSelect1
		.append(" FROM VM1DTA.UTRNPF WHERE CHDRCOY= ? AND CHDRNUM = ?");

		sqlUtrnSelect1
		.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psUtrnSelect = getPrepareStatement(sqlUtrnSelect1.toString());
		ResultSet sqlUtrnpf1rs = null;
		List<Utrnpf> utrnpfList = new ArrayList<>();
		try {
			psUtrnSelect.setString(1, utrnpfData.getChdrcoy());
			psUtrnSelect.setString(2, utrnpfData.getChdrnum());
			sqlUtrnpf1rs = executeQuery(psUtrnSelect);
			while (sqlUtrnpf1rs.next()) {
				Utrnpf utrnpf = new Utrnpf();
				utrnpf.setChdrcoy(sqlUtrnpf1rs.getString(1));
				utrnpf.setChdrnum(sqlUtrnpf1rs.getString(2));
				utrnpf.setLife(sqlUtrnpf1rs.getString(3));
				utrnpf.setCoverage(sqlUtrnpf1rs.getString(4));
				utrnpf.setRider(sqlUtrnpf1rs.getString(5));
				utrnpf.setFeedbackInd(sqlUtrnpf1rs.getString(6));
				utrnpfList.add(utrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchUtrnRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psUtrnSelect, sqlUtrnpf1rs);
		}
		return utrnpfList;

	}

	@Override
	public List<Utrnpf> readUtrnpfForUnitDealProcess(Utrnpf utrnpf) {
		//ILIFE-3704 Start by dpuhawan
		/*
		StringBuilder sql = new StringBuilder("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, CONTYP, "
				+ "TRANNO, VRTFND, UNITYP, MONIESDT, NDFIND, PRCSEQ FROM UTRNPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND FDBKIND=?");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, PRCSEQ ASC, VRTFND ASC, UNITYP ASC, UNIQUE_NUMBER ASC");
		 */
		StringBuilder sql = new StringBuilder(" ");
		sql.append("SELECT UTRN.CHDRCOY, UTRN.CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, CONTYP");
		sql.append(", UTRN.TRANNO, VRTFND, UNITYP, MONIESDT, NDFIND, PRCSEQ");
		sql.append(", CHDR.POLSUM, PAYR.PTDATE,UTRN.FUNDPOOL"); //ILIFE-8110
		sql.append(" FROM UTRNPF UTRN");
		sql.append(" INNER JOIN CHDRPF CHDR on CHDR.CHDRCOY = UTRN.CHDRCOY AND CHDR.CHDRNUM = UTRN.CHDRNUM AND CHDR.SERVUNIT = 'LP' AND CHDR.VALIDFLAG = '1' ");
		sql.append(" INNER JOIN PAYRPF PAYR on PAYR.CHDRCOY = UTRN.CHDRCOY AND PAYR.CHDRNUM = UTRN.CHDRNUM AND PAYR.VALIDFLAG = '1' AND PAYR.PAYRSEQNO = 1");
		sql.append(" WHERE UTRN.CHDRCOY=? AND UTRN.CHDRNUM=? AND FDBKIND=?");
		sql.append(" ORDER BY UTRN.CHDRCOY ASC, UTRN.CHDRNUM ASC, PRCSEQ ASC, VRTFND ASC, UNITYP ASC, UTRN.UNIQUE_NUMBER ASC");
		//ILIFE-3704 End
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Utrnpf> utrnpfList = new ArrayList<>();
		try{
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, utrnpf.getChdrcoy());
			ps.setString(2, utrnpf.getChdrnum());
			ps.setString(3, " ");

			rs = ps.executeQuery();
			while(rs.next()){
				Utrnpf rsUtrnpf = new Utrnpf();
				Dry5102DTO dry5102Dto = new Dry5102DTO();
				rsUtrnpf.setChdrcoy(rs.getString(1));
				rsUtrnpf.setChdrnum(rs.getString(2));
				rsUtrnpf.setLife(rs.getString(3));
				rsUtrnpf.setCoverage(rs.getString(4));
				rsUtrnpf.setRider(rs.getString(5));
				rsUtrnpf.setPlanSuffix(rs.getInt(6));
				rsUtrnpf.setCnttyp(rs.getString(7));
				rsUtrnpf.setTranno(rs.getInt(8));
				rsUtrnpf.setUnitVirtualFund(rs.getString(9));
				rsUtrnpf.setUnitType(rs.getString(10));
				rsUtrnpf.setMoniesDate(rs.getLong(11));
				rsUtrnpf.setNowDeferInd(rs.getString(12));
				rsUtrnpf.setProcSeqNo(rs.getInt(13));
				dry5102Dto.setPolsum(rs.getInt(14));
				dry5102Dto.setPtdate(rs.getInt(15));
				rsUtrnpf.setFundPool(rs.getString(16)); //ILIFE-8110
				rsUtrnpf.setDry5101Dto(dry5102Dto);
				utrnpfList.add(rsUtrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("readUtrnpf2()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,rs);			
		}	

		return utrnpfList;

	} 

	@Override
	public List<Utrnpf> readUtrnpf(Utrnpf utrnpf) {
		StringBuilder sql = new StringBuilder("SELECT FDBKIND, MONIESDT, COVDBTIND, SWCHIND,UNIQUE_NUMBER FROM UTRNPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND FDBKIND=?");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, MONIESDT ASC, UNIQUE_NUMBER DESC");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Utrnpf> utrnpfList = new ArrayList<>();
		try{
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, utrnpf.getChdrcoy());
			ps.setString(2, utrnpf.getChdrnum());
			ps.setString(3, " ");

			rs = ps.executeQuery();
			while(rs.next()){
				Utrnpf rsUtrnpf = new Utrnpf();
				rsUtrnpf.setFeedbackInd(rs.getString(1));			      
				rsUtrnpf.setMoniesDate(rs.getLong(2));
				rsUtrnpf.setCovdbtind(rs.getString(3));
				rsUtrnpf.setSwitchIndicator(rs.getString(4));
				rsUtrnpf.setUniqueNumber(rs.getLong(5));
				utrnpfList.add(rsUtrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("readUtrnpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,rs);			
		}	

		return utrnpfList;

	}

	public Map<String, List<Utrnpf>> checkUtrnRecordByChdrnum(List<String> chdrnumList){

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT CHDRCOY, CHDRNUM ");
		sqlSelect1.append("FROM UTRNUTM WHERE ");
		sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		Map<String,List<Utrnpf>> utrnpfMap = new HashMap<>();

		try {
			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrnpf u = new Utrnpf();
				u.setChdrcoy(sqlrs.getString(1));
				u.setChdrnum(sqlrs.getString(2));
				if(utrnpfMap.containsKey(u.getChdrnum())){
					utrnpfMap.get(u.getChdrnum()).add(u);
				}else{
					List<Utrnpf> utrnpfList=new ArrayList<>();
					utrnpfList.add(u);
					utrnpfMap.put(u.getChdrnum(), utrnpfList);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("checkUtrnRecordByChdrnum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrnpfMap;
	}

	public Map<String, List<Utrnpf>> searchUtrnRecordByFdbkind(List<String> chdrnumList){

		if(chdrnumList == null || chdrnumList.isEmpty()){
			return null;
		}
		StringBuilder sqlSelect1 = new StringBuilder();
    	sqlSelect1.append("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,JCTLJNUMPR,VRTFND,UNITSA,STRPDATE,NDFIND,NOFUNT,UNITYP,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,PRICEDT,MONIESDT,PRICEUSED,CRTABLE,CNTCURR,NOFDUNT,FDBKIND,COVDBTIND,UBREPR,INCINUM,INCIPERD01,INCIPERD02,INCIPRM01,INCIPRM02,USTMNO,CNTAMNT,FNDCURR,FUNDAMNT,FUNDRATE,SACSCODE,SACSTYP,GENLCDE,CONTYP,TRIGER,TRIGKY,PRCSEQ,SVP,DISCFA,PERSUR,CRCDTE,CIUIND,SWCHIND,BSCHEDNAM,BSCHEDNUM,FUNDPOOL,UNIQUE_NUMBER ");
		sqlSelect1.append(" FROM UTRNPF WHERE FDBKIND = ' ' AND ");
		sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, VRTFND ASC, UNITYP ASC, PRCSEQ ASC, TRANNO ASC, MONIESDT ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		Map<String,List<Utrnpf>> utrnpfMap = new HashMap<>();

		try {
			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrnpf u = new Utrnpf();
				u.setChdrcoy(sqlrs.getString(1));
				u.setChdrnum(sqlrs.getString(2));
				u.setLife(sqlrs.getString(3));
				u.setCoverage(sqlrs.getString(4));
				u.setRider(sqlrs.getString(5));
				u.setPlanSuffix(sqlrs.getInt(6));
				u.setTranno(sqlrs.getInt(7));
				u.setTermid(sqlrs.getString(8));
				u.setTransactionDate(sqlrs.getInt(9));
				u.setTransactionTime(sqlrs.getInt(10));
				u.setUser(sqlrs.getInt(11));
				u.setJobnoPrice(sqlrs.getBigDecimal(12));
				u.setUnitVirtualFund(sqlrs.getString(13));
				u.setUnitSubAccount(sqlrs.getString(14));
				u.setStrpdate(sqlrs.getLong(15));
				u.setNowDeferInd(sqlrs.getString(16));
				u.setNofUnits(sqlrs.getBigDecimal(17));
				u.setUnitType(sqlrs.getString(18));
				u.setBatccoy(sqlrs.getString(19));
				u.setBatcbrn(sqlrs.getString(20));
				u.setBatcactyr(sqlrs.getInt(21));
				u.setBatcactmn(sqlrs.getInt(22));
				u.setBatctrcde(sqlrs.getString(23));
				u.setBatcbatch(sqlrs.getString(24));
				u.setPriceDateUsed(sqlrs.getLong(25));
				u.setMoniesDate(sqlrs.getLong(26));
				u.setPriceUsed(sqlrs.getBigDecimal(27));
				u.setCrtable(sqlrs.getString(28));
				u.setCntcurr(sqlrs.getString(29));
				u.setNofDunits(sqlrs.getBigDecimal(30));
				u.setFeedbackInd(sqlrs.getString(31));
				u.setCovdbtind(sqlrs.getString(32));
				u.setUnitBarePrice(sqlrs.getBigDecimal(33));
				u.setInciNum(sqlrs.getInt(34));
				u.setInciPerd01(sqlrs.getInt(35));
				u.setInciPerd02(sqlrs.getInt(36));
				u.setInciprm01(sqlrs.getBigDecimal(37));
				u.setInciprm02(sqlrs.getBigDecimal(38));
				u.setUstmno(sqlrs.getInt(39));
				u.setContractAmount(sqlrs.getBigDecimal(40));
				u.setFundCurrency(sqlrs.getString(41));
				u.setFundAmount(sqlrs.getBigDecimal(42));
				u.setFundRate(sqlrs.getBigDecimal(43));
				u.setSacscode(sqlrs.getString(44));
				u.setSacstyp(sqlrs.getString(45));
				u.setGenlcde(sqlrs.getString(46));
				u.setCnttyp(sqlrs.getString(47));
				u.setTriggerModule(sqlrs.getString(48));
				u.setTriggerKey(sqlrs.getString(49));
				u.setProcSeqNo(sqlrs.getInt(50));
				u.setSvp(sqlrs.getBigDecimal(51));
				u.setDiscountFactor(sqlrs.getBigDecimal(52));
				u.setSurrenderPercent(sqlrs.getBigDecimal(53));
				u.setCrComDate(sqlrs.getLong(54));
				u.setCanInitUnitInd(sqlrs.getString(55));
				u.setSwitchIndicator(sqlrs.getString(56)!= null ? (sqlrs.getString(56).trim()) : "");
				u.setScheduleName(sqlrs.getString(57));
            	u.setScheduleNumber(sqlrs.getInt(58));
            	u.setFundPool(sqlrs.getString(59));
            	u.setUniqueNumber(sqlrs.getLong(60));

				if(utrnpfMap.containsKey(u.getChdrnum())){
					utrnpfMap.get(u.getChdrnum()).add(u);
				}else{
					List<Utrnpf> utrnpfList=new ArrayList<>();
					utrnpfList.add(u);
					utrnpfMap.put(u.getChdrnum(), utrnpfList);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchUtrnRecordByFdbkind()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrnpfMap;
	}

	public void updateUtrnpfRecord(List<Utrnpf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		String sql = "UPDATE UTRNPF SET FDBKIND=?, NOFDUNT=?, NOFUNT=?, FUNDAMNT=?, FUNDRATE=?, CNTAMNT=?, PRICEUSED=?, PRICEDT=?, UBREPR=?, DISCFA=?,BSCHEDNAM=?,BSCHEDNUM=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);

		try {
			for (Utrnpf u : urList) {
				int i = 1;
				ps.setString(i++, u.getFeedbackInd());
				ps.setBigDecimal(i++, u.getNofDunits());
				ps.setBigDecimal(i++, u.getNofUnits());
				ps.setBigDecimal(i++, u.getFundAmount());
				ps.setBigDecimal(i++, u.getFundRate());
				ps.setBigDecimal(i++, u.getContractAmount());
				ps.setBigDecimal(i++, u.getPriceUsed());
				ps.setLong(i++, u.getPriceDateUsed());
				ps.setBigDecimal(i++, u.getUnitBarePrice());
				ps.setBigDecimal(i++, u.getDiscountFactor());
				ps.setString(i++, u.getScheduleName());
				ps.setInt(i++, u.getScheduleNumber());
				ps.setString(i++, getJobnm());
				ps.setString(i++, getUsrprf());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.setLong(i++, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateUtrnpfRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	public Map<String, List<Utrnpf>> searchUtrnrevRecord(List<String> chdrnumList){

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT UNIQUE_NUMBER, CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,TRANNO,COVDBTIND");
		sqlSelect1.append(" FROM UTRNREV WHERE ");
		sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");

		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		Map<String,List<Utrnpf>> utrnpfMap = new HashMap<>();

		try {
			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrnpf u = new Utrnpf();
				u.setUniqueNumber(sqlrs.getLong("UNIQUE_NUMBER"));
				u.setChdrcoy(sqlrs.getString("chdrcoy"));
				u.setChdrnum(sqlrs.getString("chdrnum"));
				u.setLife(sqlrs.getString("life"));
				u.setCoverage(sqlrs.getString("coverage"));
				u.setRider(sqlrs.getString("rider"));
				u.setPlanSuffix(sqlrs.getInt("plnsfx"));
				u.setTranno(sqlrs.getInt("tranno"));
				u.setCovdbtind(sqlrs.getString("covdbtind"));

				if(utrnpfMap.containsKey(u.getChdrnum())){
					utrnpfMap.get(u.getChdrnum()).add(u);
				}else{
					List<Utrnpf> utrnpfList=new ArrayList<>();
					utrnpfList.add(u);
					utrnpfMap.put(u.getChdrnum(), utrnpfList);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchUtrnrevRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrnpfMap;
	}

	public Map<String, List<Utrnpf>> searchUtrnuddRecord(List<String> chdrnumList){

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT UNIQUE_NUMBER, CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,TRANNO");
		sqlSelect1.append(" FROM UTRNUDD WHERE ");
		sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");

		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		Map<String,List<Utrnpf>> utrnpfMap = new HashMap<>();

		try {
			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrnpf u = new Utrnpf();
				u.setUniqueNumber(sqlrs.getLong("UNIQUE_NUMBER"));
				u.setChdrcoy(sqlrs.getString("chdrcoy"));
				u.setChdrnum(sqlrs.getString("chdrnum"));
				u.setLife(sqlrs.getString("life"));
				u.setCoverage(sqlrs.getString("coverage"));
				u.setRider(sqlrs.getString("rider"));
				u.setPlanSuffix(sqlrs.getInt("plnsfx"));
				u.setTranno(sqlrs.getInt("tranno"));

				if(utrnpfMap.containsKey(u.getChdrnum())){
					utrnpfMap.get(u.getChdrnum()).add(u);
				}else{
					List<Utrnpf> utrnpfList=new ArrayList<>();
					utrnpfList.add(u);
					utrnpfMap.put(u.getChdrnum(), utrnpfList);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchUtrnuddRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrnpfMap;
	}
	public void deleteUtrnrevRecord(List<Utrnpf> utrnList) {
		String sql = "DELETE FROM UTRNREV WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Utrnpf u:utrnList){
				ps.setLong(1, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteUtrnrevRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	public void insertUtrnRecoed(List<Utrnpf> utrnBulkOpList) {
		if (utrnBulkOpList != null && utrnBulkOpList.size() > 0) {

            String SQL_Utrn_INSERT = " INSERT INTO UtrnPF(chdrcoy,chdrnum,life,coverage,rider,plnsfx,tranno,termid,trdt,trtm,user_t,jctljnumpr,vrtfnd,unitsa,strpdate,ndfind,nofunt,unityp,batccoy,batcbrn,batcactyr,batcactmn,batctrcde,batcbatch,pricedt,moniesdt,priceused,crtable,cntcurr,nofdunt,fdbkind,covdbtind,ubrepr,incinum,inciperd01,inciperd02,inciprm01,inciprm02,ustmno,cntamnt,fndcurr,fundamnt,fundrate,sacscode,sacstyp,genlcde,contyp,triger,trigky,prcseq,svp,discfa,persur,crcdte,ciuind,swchind,bschednam,bschednum,JOBNM,USRPRF,DATIME,FUNDPOOL) "
            		+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			PreparedStatement psUtrnInsert = getPrepareStatement(SQL_Utrn_INSERT);
			try {
				for (Utrnpf u : utrnBulkOpList) {
					int i = 1;
					psUtrnInsert.setString(i++, u.getChdrcoy());
					psUtrnInsert.setString(i++, u.getChdrnum());
					psUtrnInsert.setString(i++, u.getLife());
					psUtrnInsert.setString(i++, u.getCoverage());
					psUtrnInsert.setString(i++, u.getRider());
					psUtrnInsert.setInt(i++, u.getPlanSuffix());
					psUtrnInsert.setInt(i++, u.getTranno());
					psUtrnInsert.setString(i++, u.getTermid());
					psUtrnInsert.setInt(i++, u.getTransactionDate());
					psUtrnInsert.setInt(i++, u.getTransactionTime());
					psUtrnInsert.setInt(i++, u.getUser());
					psUtrnInsert.setBigDecimal(i++, u.getJobnoPrice());
					psUtrnInsert.setString(i++, u.getUnitVirtualFund());
					psUtrnInsert.setString(i++, u.getUnitSubAccount());
					psUtrnInsert.setLong(i++, u.getStrpdate());
					psUtrnInsert.setString(i++, u.getNowDeferInd());
					psUtrnInsert.setBigDecimal(i++, u.getNofUnits());
					psUtrnInsert.setString(i++, u.getUnitType());
					psUtrnInsert.setString(i++, u.getBatccoy());
					psUtrnInsert.setString(i++, u.getBatcbrn());
					psUtrnInsert.setInt(i++, u.getBatcactyr());
					psUtrnInsert.setInt(i++, u.getBatcactmn());
					psUtrnInsert.setString(i++, u.getBatctrcde());
					psUtrnInsert.setString(i++, u.getBatcbatch());
					psUtrnInsert.setLong(i++, u.getPriceDateUsed());
					psUtrnInsert.setLong(i++, u.getMoniesDate());
					psUtrnInsert.setBigDecimal(i++, u.getPriceUsed());
					psUtrnInsert.setString(i++, u.getCrtable());
					psUtrnInsert.setString(i++, u.getCntcurr());
					psUtrnInsert.setBigDecimal(i++, u.getNofDunits());
					psUtrnInsert.setString(i++, u.getFeedbackInd());
					psUtrnInsert.setString(i++, u.getCovdbtind());
					psUtrnInsert.setBigDecimal(i++, u.getUnitBarePrice());
					psUtrnInsert.setInt(i++, u.getInciNum());
					psUtrnInsert.setInt(i++, u.getInciPerd01());
					psUtrnInsert.setInt(i++, u.getInciPerd02());
					psUtrnInsert.setBigDecimal(i++, u.getInciprm01());
					psUtrnInsert.setBigDecimal(i++, u.getInciprm02());
					psUtrnInsert.setInt(i++, u.getUstmno());
					psUtrnInsert.setBigDecimal(i++, u.getContractAmount());
                	psUtrnInsert.setString(i++, u.getFundCurrency().trim());
					psUtrnInsert.setBigDecimal(i++, u.getFundAmount());
					psUtrnInsert.setBigDecimal(i++, u.getFundRate());
					psUtrnInsert.setString(i++, u.getSacscode());
					psUtrnInsert.setString(i++, u.getSacstyp());
					psUtrnInsert.setString(i++, u.getGenlcde());
					psUtrnInsert.setString(i++, u.getCnttyp());
					psUtrnInsert.setString(i++, u.getTriggerModule());
					psUtrnInsert.setString(i++, u.getTriggerKey());
					psUtrnInsert.setInt(i++, u.getProcSeqNo());
					psUtrnInsert.setBigDecimal(i++, u.getSvp());
					psUtrnInsert.setBigDecimal(i++, u.getDiscountFactor());
					psUtrnInsert.setBigDecimal(i++, u.getSurrenderPercent());
					psUtrnInsert.setLong(i++, u.getCrComDate());
					psUtrnInsert.setString(i++, u.getCanInitUnitInd());
					psUtrnInsert.setString(i++, u.getSwitchIndicator());
					psUtrnInsert.setString(i++, u.getScheduleName());
					psUtrnInsert.setInt(i++, u.getScheduleNumber());
					psUtrnInsert.setString(i++, getJobnm());
					psUtrnInsert.setString(i++, getUsrprf());
					psUtrnInsert.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
					psUtrnInsert.setString(i++,u.getFundPool());
					psUtrnInsert.addBatch();
				}
				psUtrnInsert.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("insertUtrnRecoed()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psUtrnInsert, null);
			}
		}
	}

	public List<String> checkUtrnrnlRecordByChdrnum(String chdrcoy, List<String> chdrnumList) {

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT  CHDRNUM ");
		sqlSelect1.append("FROM UTRNRNL WHERE CHDRCOY=? AND ");
		sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC");

		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<String> utrnpfList = new ArrayList<>();
		try {

			psSelect.setString(1, chdrcoy);
			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				utrnpfList.add(sqlrs.getString(1));
			}

		} catch (SQLException e) {
			LOGGER.error("checkUtrnrnlRecordByChdrnum()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrnpfList;
	}    

	private String getChunkSQL(String tableId, String orderBy, String whereSql) {
		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append(" SELECT * FROM ( ");
		sqlSelect.append(QUERY_SQL);
		sqlSelect.append(" ,FLOOR((ROW_NUMBER()OVER( ");
		sqlSelect.append(orderBy);
		sqlSelect.append(" )-1)/?) BATCHNUM FROM ");
		sqlSelect.append(tableId);
		sqlSelect.append(whereSql);
		sqlSelect.append(" ) MAIN WHERE BATCHNUM = ? ");
		return sqlSelect.toString();
	}

	public List<Utrnpf> sqlDeal1200ChunkRead(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String chdrnumFrom, String chdrnumTo){
		String sqldealCursor = getChunkSQL(tableId, QUERY_SQL_ORDER, " WHERE CHDRCOY = ? AND FDBKIND = ? AND CHDRNUM >= ? AND CHDRNUM <= ?");

		PreparedStatement psUtrnSelect = getPrepareStatement(sqldealCursor);
		ResultSet sqlutrnpf1rs = null;
		List<Utrnpf> utrnpfList = new ArrayList<>();
		try {
			psUtrnSelect.setInt(1, batchExtractSize);
			psUtrnSelect.setString(2, chdrcoy);
			psUtrnSelect.setString(3, fdbkind);
			psUtrnSelect.setString(4, chdrnumFrom);
			psUtrnSelect.setString(5, chdrnumTo);
			psUtrnSelect.setInt(6, batchID);

			sqlutrnpf1rs = executeQuery(psUtrnSelect);
			while (sqlutrnpf1rs.next()) {
				Utrnpf utrnpf = new Utrnpf();
				utrnpf.setChdrcoy(sqlutrnpf1rs.getString("chdrcoy"));
				utrnpf.setChdrnum(sqlutrnpf1rs.getString("chdrnum"));
				utrnpf.setLife(sqlutrnpf1rs.getString("life"));
				utrnpf.setCoverage(sqlutrnpf1rs.getString("coverage"));
				utrnpf.setRider(sqlutrnpf1rs.getString("rider"));
				utrnpf.setPlanSuffix(sqlutrnpf1rs.getInt("plnsfx"));
				utrnpf.setUnitVirtualFund(sqlutrnpf1rs.getString("vrtfnd"));
				utrnpf.setUnitType(sqlutrnpf1rs.getString("unityp"));
				utrnpf.setTranno(sqlutrnpf1rs.getInt("tranno"));
				utrnpf.setBatctrcde(sqlutrnpf1rs.getString("batctrcde"));
				utrnpf.setUnitSubAccount(sqlutrnpf1rs.getString("unitsa"));
				utrnpf.setNowDeferInd(sqlutrnpf1rs.getString("ndfind"));
				utrnpf.setNofUnits(sqlutrnpf1rs.getBigDecimal("nofunt"));
				utrnpf.setMoniesDate(sqlutrnpf1rs.getLong("moniesdt"));
				utrnpf.setCntcurr(sqlutrnpf1rs.getString("cntcurr"));
				utrnpf.setNofDunits(sqlutrnpf1rs.getBigDecimal("nofdunt"));
				utrnpf.setFeedbackInd(sqlutrnpf1rs.getString("fdbkind"));
				utrnpf.setContractAmount(sqlutrnpf1rs.getBigDecimal("cntamnt"));
				utrnpf.setFundAmount(sqlutrnpf1rs.getBigDecimal("fundamnt"));
				utrnpf.setCnttyp(sqlutrnpf1rs.getString("contyp"));
				utrnpf.setProcSeqNo(sqlutrnpf1rs.getInt("prcseq"));
				utrnpf.setSurrenderPercent(sqlutrnpf1rs.getBigDecimal("persur"));
				utrnpf.setFundRate(sqlutrnpf1rs.getBigDecimal("fundrate"));
				utrnpf.setCovdbtind(sqlutrnpf1rs.getString("covdbtind"));
				utrnpf.setSwitchIndicator(sqlutrnpf1rs.getString("swchind"));
				utrnpf.setFundPool(sqlutrnpf1rs.getString("fundpool"));
				utrnpfList.add(utrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("sqlDeal1200ChunkRead()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psUtrnSelect, sqlutrnpf1rs);
		}
		return utrnpfList;
	}

	public List<Utrnpf> sqlDebt1300ChunkRead(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String covdbtind, String chdrnumFrom, String chdrnumTo){
		String sqldealCursor = getChunkSQL(tableId, QUERY_SQL_ORDER, " WHERE CHDRCOY = ? AND FDBKIND = ? AND COVDBTIND = ? AND CHDRNUM >= ? AND CHDRNUM <= ? ");

		PreparedStatement psUtrnSelect = getPrepareStatement(sqldealCursor);
		ResultSet sqlutrnpf1rs = null;
		List<Utrnpf> utrnpfList = new ArrayList<>();
		try {
			psUtrnSelect.setInt(1, batchExtractSize);
			psUtrnSelect.setString(2, chdrcoy);
			psUtrnSelect.setString(3, fdbkind);
			psUtrnSelect.setString(4, covdbtind);
			psUtrnSelect.setString(5, chdrnumFrom);
			psUtrnSelect.setString(6, chdrnumTo);
			psUtrnSelect.setInt(7, batchID);

			sqlutrnpf1rs = executeQuery(psUtrnSelect);
			while (sqlutrnpf1rs.next()) {
				Utrnpf utrnpf = new Utrnpf();
				utrnpf.setChdrcoy(sqlutrnpf1rs.getString("chdrcoy"));
				utrnpf.setChdrnum(sqlutrnpf1rs.getString("chdrnum"));
				utrnpf.setLife(sqlutrnpf1rs.getString("life"));
				utrnpf.setCoverage(sqlutrnpf1rs.getString("coverage"));
				utrnpf.setRider(sqlutrnpf1rs.getString("rider"));
				utrnpf.setPlanSuffix(sqlutrnpf1rs.getInt("plnsfx"));
				utrnpf.setUnitVirtualFund(sqlutrnpf1rs.getString("vrtfnd"));
				utrnpf.setUnitType(sqlutrnpf1rs.getString("unityp"));
				utrnpf.setTranno(sqlutrnpf1rs.getInt("tranno"));
				utrnpf.setBatctrcde(sqlutrnpf1rs.getString("batctrcde"));
				utrnpf.setUnitSubAccount(sqlutrnpf1rs.getString("unitsa"));
				utrnpf.setNowDeferInd(sqlutrnpf1rs.getString("ndfind"));
				utrnpf.setNofUnits(sqlutrnpf1rs.getBigDecimal("nofunt"));
				utrnpf.setMoniesDate(sqlutrnpf1rs.getLong("moniesdt"));
				utrnpf.setCntcurr(sqlutrnpf1rs.getString("cntcurr"));
				utrnpf.setNofDunits(sqlutrnpf1rs.getBigDecimal("nofdunt"));
				utrnpf.setFeedbackInd(sqlutrnpf1rs.getString("fdbkind"));
				utrnpf.setContractAmount(sqlutrnpf1rs.getBigDecimal("cntamnt"));
				utrnpf.setFundAmount(sqlutrnpf1rs.getBigDecimal("fundamnt"));
				utrnpf.setCnttyp(sqlutrnpf1rs.getString("contyp"));
				utrnpf.setProcSeqNo(sqlutrnpf1rs.getInt("prcseq"));
				utrnpf.setSurrenderPercent(sqlutrnpf1rs.getBigDecimal("persur"));
				utrnpf.setFundRate(sqlutrnpf1rs.getBigDecimal("fundrate"));
				utrnpf.setCovdbtind(sqlutrnpf1rs.getString("covdbtind"));
				utrnpf.setSwitchIndicator(sqlutrnpf1rs.getString("swchind"));
				utrnpf.setFundPool(sqlutrnpf1rs.getString("fundpool"));
				utrnpfList.add(utrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("sqlDebt1300ChunkRead()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psUtrnSelect, sqlutrnpf1rs);
		}
		return utrnpfList;
	}

	public List<Utrnpf> sqlSwch1400ChunkRead(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String swchind, String chdrnumFrom, String chdrnumTo){
		String sqldealCursor = getChunkSQL(tableId, QUERY_SQL_ORDER, " WHERE CHDRCOY = ? AND FDBKIND = ? AND SWCHIND = ? AND CHDRNUM >= ? AND CHDRNUM <= ? ");

		PreparedStatement psUtrnSelect = getPrepareStatement(sqldealCursor);
		ResultSet sqlutrnpf1rs = null;
		List<Utrnpf> utrnpfList = new ArrayList<>();
		try {
			psUtrnSelect.setInt(1, batchExtractSize);
			psUtrnSelect.setString(2, chdrcoy);
			psUtrnSelect.setString(3, fdbkind);
			psUtrnSelect.setString(4, swchind);
			psUtrnSelect.setString(5, chdrnumFrom);
			psUtrnSelect.setString(6, chdrnumTo);
			psUtrnSelect.setInt(7, batchID);

			sqlutrnpf1rs = executeQuery(psUtrnSelect);
			while (sqlutrnpf1rs.next()) {
				Utrnpf utrnpf = new Utrnpf();
				utrnpf.setChdrcoy(sqlutrnpf1rs.getString("chdrcoy"));
				utrnpf.setChdrnum(sqlutrnpf1rs.getString("chdrnum"));
				utrnpf.setLife(sqlutrnpf1rs.getString("life"));
				utrnpf.setCoverage(sqlutrnpf1rs.getString("coverage"));
				utrnpf.setRider(sqlutrnpf1rs.getString("rider"));
				utrnpf.setPlanSuffix(sqlutrnpf1rs.getInt("plnsfx"));
				utrnpf.setUnitVirtualFund(sqlutrnpf1rs.getString("vrtfnd"));
				utrnpf.setUnitType(sqlutrnpf1rs.getString("unityp"));
				utrnpf.setTranno(sqlutrnpf1rs.getInt("tranno"));
				utrnpf.setBatctrcde(sqlutrnpf1rs.getString("batctrcde"));
				utrnpf.setUnitSubAccount(sqlutrnpf1rs.getString("unitsa"));
				utrnpf.setNowDeferInd(sqlutrnpf1rs.getString("ndfind"));
				utrnpf.setNofUnits(sqlutrnpf1rs.getBigDecimal("nofunt"));
				utrnpf.setMoniesDate(sqlutrnpf1rs.getLong("moniesdt"));
				utrnpf.setCntcurr(sqlutrnpf1rs.getString("cntcurr"));
				utrnpf.setNofDunits(sqlutrnpf1rs.getBigDecimal("nofdunt"));
				utrnpf.setFeedbackInd(sqlutrnpf1rs.getString("fdbkind"));
				utrnpf.setContractAmount(sqlutrnpf1rs.getBigDecimal("cntamnt"));
				utrnpf.setFundAmount(sqlutrnpf1rs.getBigDecimal("fundamnt"));
				utrnpf.setCnttyp(sqlutrnpf1rs.getString("contyp"));
				utrnpf.setProcSeqNo(sqlutrnpf1rs.getInt("prcseq"));
				utrnpf.setSurrenderPercent(sqlutrnpf1rs.getBigDecimal("persur"));
				utrnpf.setFundRate(sqlutrnpf1rs.getBigDecimal("fundrate"));
				utrnpf.setCovdbtind(sqlutrnpf1rs.getString("covdbtind"));
				utrnpf.setSwitchIndicator(sqlutrnpf1rs.getString("swchind"));
				utrnpf.setFundPool(sqlutrnpf1rs.getString("fundpool"));
				utrnpfList.add(utrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("sqlSwch1400ChunkRead()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psUtrnSelect, sqlutrnpf1rs);
		}
		return utrnpfList;
	}
	//ILIFE-4959:remove the costly query on UTRNPF from P5284
	public int getUnprocessedUtrnCount(String chdrCoy, String chdrNum ){
		int intRetVal = 0;
		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("SELECT  COUNT(*) ");
		sqlSelect.append("FROM UTRNPF WHERE CHDRCOY=? AND ");
		sqlSelect.append("CHDRNUM=? AND FDBKIND  IN( 'N', '')");  //ILIFE-6130
	
		PreparedStatement psSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlrs = null;

		try {
			psSelect.setString(1, chdrCoy);

			psSelect.setString(2, chdrNum);

			sqlrs = executeQuery(psSelect);
			while (sqlrs.next()) {
				intRetVal = sqlrs.getInt(1);
			}
		}catch (SQLException e) {
			LOGGER.error("getUnprocessedUtrnCount()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return intRetVal;
	}
	public Utrnpf findUtrnpf(Utrnpf utrnpf) {
		StringBuilder sql = new StringBuilder("SELECT * FROM UTRNPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND FDBKIND=' '"); //ILIFE-8704
		PreparedStatement ps = null;
		ResultSet rs = null;
		Utrnpf rsUtrnpf = null;
		try{
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, utrnpf.getChdrcoy());
			ps.setString(2, utrnpf.getChdrnum());
			rs = ps.executeQuery();
			while(rs.next()){
				rsUtrnpf = new Utrnpf();
				rsUtrnpf.setFeedbackInd(rs.getString(1));			      
				rsUtrnpf.setMoniesDate(rs.getLong(2));
				rsUtrnpf.setCovdbtind(rs.getString(3));
				rsUtrnpf.setSwitchIndicator(rs.getString(4));
			}
		} catch (SQLException e) {
			LOGGER.error("readUtrnpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,rs);			
		}	

		return rsUtrnpf;

	}

	public List<Utrnpf> searchUtrnextRecord(String chdrcoy, String chdrnum) {
		StringBuilder sql = new StringBuilder(
				"SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,VRTFND,UNITYP FROM UTRNEXT");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, PRCSEQ ASC, VRTFND ASC, UNITYP ASC, UNIQUE_NUMBER ASC ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Utrnpf> utrnpfList = new LinkedList<>();
		try {
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);

			rs = ps.executeQuery();
			while (rs.next()) {
				Utrnpf rsUtrnpf = new Utrnpf();
				rsUtrnpf.setChdrcoy(rs.getString("CHDRCOY"));
				rsUtrnpf.setChdrnum(rs.getString("CHDRNUM"));
				rsUtrnpf.setLife(rs.getString("LIFE"));
				rsUtrnpf.setCoverage(rs.getString("COVERAGE"));
				rsUtrnpf.setRider(rs.getString("RIDER"));
				rsUtrnpf.setPlanSuffix(rs.getInt("PLNSFX"));
				rsUtrnpf.setUnitVirtualFund(rs.getString("VRTFND"));
				rsUtrnpf.setUnitType(rs.getString("UNITYP"));
				utrnpfList.add(rsUtrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchUtrnextRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return utrnpfList;

	}

	public List<Utrnpf> searchUtrnRecord(String chdrcoy, String chdrnum){

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT * FROM UTRN ");
		sqlSelect1.append(" WHERE CHDRNUM = ? AND CHDRCOY = ? ");
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, VRTFND ASC, UNITYP ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<Utrnpf> utrnpfList=new LinkedList<>();

		try {
			psSelect.setString(1, chdrnum);
			psSelect.setString(2, chdrcoy);

			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrnpf u = new Utrnpf();
				u.setChdrcoy(sqlrs.getString("Chdrcoy"));
				u.setChdrnum(sqlrs.getString("Chdrnum"));
				u.setLife(sqlrs.getString("Life"));
				u.setCoverage(sqlrs.getString("Coverage"));
				u.setRider(sqlrs.getString("Rider"));
				u.setPlanSuffix(sqlrs.getInt("PLNSFX"));
				u.setTranno(sqlrs.getInt("Tranno"));
				u.setTermid(sqlrs.getString("Termid"));
				u.setTransactionDate(sqlrs.getInt("TRDT"));
				u.setTransactionTime(sqlrs.getInt("TRTM"));
				u.setJobnoPrice(sqlrs.getBigDecimal("JCTLJNUMPR"));
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));
				u.setUnitSubAccount(sqlrs.getString("UNITSA"));
				u.setStrpdate(sqlrs.getLong("Strpdate"));
				u.setNowDeferInd(sqlrs.getString("ndfind"));
				u.setNofUnits(sqlrs.getBigDecimal("nofunt"));
				u.setUnitType(sqlrs.getString("UNITYP"));
				u.setBatccoy(sqlrs.getString("Batccoy"));
				u.setBatcbrn(sqlrs.getString("Batcbrn"));
				u.setBatcactyr(sqlrs.getInt("Batcactyr"));
				u.setBatcactmn(sqlrs.getInt("Batcactmn"));
				u.setBatctrcde(sqlrs.getString("Batctrcde"));
				u.setBatcbatch(sqlrs.getString("Batcbatch"));
				u.setPriceDateUsed(sqlrs.getLong("pricedt"));
				u.setMoniesDate(sqlrs.getLong("MONIESDT"));
				u.setPriceUsed(sqlrs.getBigDecimal("PriceUsed"));
				u.setCrtable(sqlrs.getString("Crtable"));
				u.setCntcurr(sqlrs.getString("Cntcurr"));
				u.setNofDunits(sqlrs.getBigDecimal("nofdunt"));
				u.setFeedbackInd(sqlrs.getString("FDBKIND"));
				u.setUnitBarePrice(sqlrs.getBigDecimal("ubrepr"));
				u.setInciNum(sqlrs.getInt("InciNum"));
				u.setInciPerd01(sqlrs.getInt("InciPerd01"));
				u.setInciPerd02(sqlrs.getInt("InciPerd02"));
				u.setInciprm01(sqlrs.getBigDecimal("Inciprm01"));
				u.setInciprm02(sqlrs.getBigDecimal("Inciprm02"));
				u.setUstmno(sqlrs.getInt("Ustmno"));
				u.setContractAmount(sqlrs.getBigDecimal("cntamnt"));
				u.setFundCurrency(sqlrs.getString("FNDCURR"));
				u.setFundAmount(sqlrs.getBigDecimal("FUNDAMNT"));
				u.setFundRate(sqlrs.getBigDecimal("FundRate"));
				u.setSacscode(sqlrs.getString("Sacscode"));
				u.setSacstyp(sqlrs.getString("Sacstyp"));
				u.setGenlcde(sqlrs.getString("Genlcde"));
				u.setCnttyp(sqlrs.getString("CONTYP"));
				u.setProcSeqNo(sqlrs.getInt("prcseq"));
				u.setSvp(sqlrs.getBigDecimal("Svp"));
				u.setDiscountFactor(sqlrs.getBigDecimal("DISCFA"));
				u.setSurrenderPercent(sqlrs.getBigDecimal("persur"));
				u.setCrComDate(sqlrs.getLong("CRCDTE"));
				u.setCanInitUnitInd(sqlrs.getString("ciuind"));
				u.setSwitchIndicator(sqlrs.getString("SWCHIND")!= null ? (sqlrs.getString("SWCHIND").trim()) : "");
				u.setUniqueNumber(sqlrs.getLong("Unique_Number"));
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));
				utrnpfList.add(u);
			}

		} catch (SQLException e) {
			LOGGER.error("searchUtrnRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrnpfList;

	}


	public void insertUstnRecord(List<Utrnpf> ustnUpdateList) {
		if (ustnUpdateList != null && ustnUpdateList.size() > 0) {

			String SQL_Utrn_INSERT = " INSERT INTO UTRNPF(chdrcoy,chdrnum,life,coverage,rider,plnsfx,tranno,termid,trdt,trtm,user_t,jctljnumpr,vrtfnd,unitsa,"
					+ "strpdate,ndfind,nofunt,unityp,batccoy,batcbrn,batcactyr,batcactmn,batctrcde,batcbatch,pricedt,moniesdt,priceused,crtable,cntcurr,nofdunt,"
					+ "fdbkind,covdbtind,ubrepr,incinum,inciperd01,inciperd02,inciprm01,inciprm02,ustmno,cntamnt,fndcurr,fundamnt,fundrate,sacscode,sacstyp,genlcde,"
					+ "contyp,triger,trigky,prcseq,svp,discfa,persur,crcdte,ciuind,swchind,bschednam,bschednum,JOBNM,USRPRF,DATIME) "
					+ " SELECT chdrcoy,chdrnum,life,coverage,rider,plnsfx,tranno,termid,trdt,trtm,user_t,jctljnumpr,vrtfnd,unitsa,"
					+ " ? ,ndfind,nofunt,unityp,batccoy,batcbrn,batcactyr,batcactmn,batctrcde,batcbatch,pricedt,moniesdt,priceused,crtable,cntcurr,nofdunt,"
					+ "fdbkind,covdbtind,ubrepr,incinum,inciperd01,inciperd02,inciprm01,inciprm02,?,cntamnt,fndcurr,fundamnt,fundrate,sacscode,sacstyp,genlcde,"
					+ "contyp,triger,trigky,prcseq,svp,discfa,persur,crcdte,ciuind,swchind,bschednam,bschednum,?,?,? FROM UTRNPF WHERE UNIQUE_NUMBER=?";

			PreparedStatement psUtrnInsert = getPrepareStatement(SQL_Utrn_INSERT);
			try {
				for (Utrnpf u : ustnUpdateList) {
					int i = 1;
					psUtrnInsert.setLong(i++, u.getStrpdate());
					psUtrnInsert.setInt(i++, u.getUstmno());
					psUtrnInsert.setString(i++, getJobnm());
					psUtrnInsert.setString(i++, getUsrprf());
					psUtrnInsert.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
					psUtrnInsert.setLong(i++, u.getUniqueNumber());                
					psUtrnInsert.addBatch();
				}
				psUtrnInsert.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("insertUtrnRcds()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psUtrnInsert, null);
			}
		}

	}
 //ILIFE-8786 start
	@Override
	public List<Utrnpf> searchUtrnpfRecord(Utrnpf utrnpf) {
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT * FROM UTRN ");
		sqlSelect1.append(" WHERE  CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND PLNSFX = ? AND TRANNO=? AND PRCSEQ=?");
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, VRTFND ASC, UNITYP ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<Utrnpf> utrnpfList=new LinkedList<>();

		try {
			psSelect.setString(1, utrnpf.getChdrcoy());
			psSelect.setString(2, utrnpf.getChdrnum());
			psSelect.setString(3, utrnpf.getLife());
			psSelect.setString(4, utrnpf.getCoverage());
			psSelect.setInt(5, utrnpf.getPlanSuffix());
			psSelect.setInt(6, utrnpf.getTranno());
			psSelect.setInt(7, utrnpf.getProcSeqNo());
			

			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Utrnpf u = new Utrnpf();
				u.setChdrcoy(sqlrs.getString("Chdrcoy"));
				u.setChdrnum(sqlrs.getString("Chdrnum"));
				u.setLife(sqlrs.getString("Life"));
				u.setCoverage(sqlrs.getString("Coverage"));
				u.setRider(sqlrs.getString("Rider"));
				u.setPlanSuffix(sqlrs.getInt("PLNSFX"));
				u.setTranno(sqlrs.getInt("Tranno"));
				u.setTermid(sqlrs.getString("Termid"));
				u.setJobnoPrice(sqlrs.getBigDecimal("JCTLJNUMPR"));
				u.setTransactionDate(sqlrs.getInt("TRDT"));
				u.setTransactionTime(sqlrs.getInt("TRTM"));
				u.setUser(sqlrs.getInt("USER_T"));
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));
				u.setUnitSubAccount(sqlrs.getString("UNITSA"));
				u.setStrpdate(sqlrs.getLong("Strpdate"));
				u.setNowDeferInd(sqlrs.getString("ndfind"));
				u.setNofUnits(sqlrs.getBigDecimal("nofunt"));
				u.setUnitType(sqlrs.getString("UNITYP"));
				u.setBatccoy(sqlrs.getString("Batccoy"));
				u.setBatcbrn(sqlrs.getString("Batcbrn"));
				u.setBatcactyr(sqlrs.getInt("Batcactyr"));
				u.setBatcactmn(sqlrs.getInt("Batcactmn"));
				u.setBatctrcde(sqlrs.getString("Batctrcde"));
				u.setBatcbatch(sqlrs.getString("Batcbatch"));
				u.setPriceDateUsed(sqlrs.getLong("pricedt"));
				u.setMoniesDate(sqlrs.getLong("MONIESDT"));
				u.setPriceUsed(sqlrs.getBigDecimal("PriceUsed"));
				u.setCntcurr(sqlrs.getString("Cntcurr"));
				u.setCrtable(sqlrs.getString("Crtable"));
				u.setNofDunits(sqlrs.getBigDecimal("nofdunt"));
				u.setFeedbackInd(sqlrs.getString("FDBKIND"));
				u.setUnitBarePrice(sqlrs.getBigDecimal("ubrepr"));
				u.setInciNum(sqlrs.getInt("InciNum"));
				u.setInciPerd01(sqlrs.getInt("InciPerd01"));
				u.setInciPerd02(sqlrs.getInt("InciPerd02"));
				u.setInciprm01(sqlrs.getBigDecimal("Inciprm01"));
				u.setInciprm02(sqlrs.getBigDecimal("Inciprm02"));
				u.setUstmno(sqlrs.getInt("Ustmno"));
				u.setContractAmount(sqlrs.getBigDecimal("cntamnt"));
				u.setFundCurrency(sqlrs.getString("FNDCURR"));
				u.setFundAmount(sqlrs.getBigDecimal("FUNDAMNT"));
				u.setFundRate(sqlrs.getBigDecimal("FundRate"));
				u.setSacscode(sqlrs.getString("Sacscode"));
				u.setSacstyp(sqlrs.getString("Sacstyp"));
				u.setGenlcde(sqlrs.getString("Genlcde"));
				u.setTriggerModule(sqlrs.getString("Triger"));
				u.setTriggerKey(sqlrs.getString("Trigky"));
				u.setCnttyp(sqlrs.getString("CONTYP"));
				u.setProcSeqNo(sqlrs.getInt("prcseq"));
				u.setSurrenderPercent(sqlrs.getBigDecimal("persur"));
				u.setSvp(sqlrs.getBigDecimal("Svp"));
				u.setDiscountFactor(sqlrs.getBigDecimal("DISCFA"));
				u.setCrComDate(sqlrs.getLong("CRCDTE"));
				u.setCanInitUnitInd(sqlrs.getString("ciuind"));
				u.setSwitchIndicator(sqlrs.getString("SWCHIND")!= null ? (sqlrs.getString("SWCHIND").trim()) : "");
				u.setFundPool(sqlrs.getString("FUNDPOOL"));
				u.setUniqueNumber(sqlrs.getLong("Unique_Number"));
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));
				utrnpfList.add(u);
			}

		} catch (SQLException e) {
			LOGGER.error("searchUtrnRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return utrnpfList;
	}

	@Override
	public void updatedUtrnpfRecord(List<Utrnpf> utrnList) {
		if (utrnList == null || utrnList.isEmpty()) {
			return;
		}
		String sql = "UPDATE UTRNPF SET CHDRCOY=?, CHDRNUM=?, LIFE=?, COVERAGE=?, RIDER=?, TRANNO=?, TERMID=?, TRDT=?, TRTM=?, USER_T=?, BATCCOY=?, BATCBRN=?, BATCACTYR=?, BATCACTMN=?, "
				+ "BATCTRCDE=?, BATCBATCH=?, NDFIND=?, PRCSEQ=?, MONIESDT=?, CRTABLE=?, PLNSFX=?, CNTCURR=?, CONTYP=?, CRCDTE=?, VRTFND=?, FNDCURR=?, SACSCODE=?, SACSTYP=?, GENCLDE=?, "
				+ "UNITSA=?, CNTAMNT=?, UNITYP=?, INCIPRM01=?, INCIPRM02, NOFUNT=?, NOFDUNT=?, FUNDRATE=?, STRPDATE=?, USTMNO=?, PRICEDT=?, PRICEUSED=?, UBREPR=?, INCINUM=?, INCIPERD01=?, INCIPERD02=?, "
				+ "FUNDAMNT=?, JCTLJNUMPR=?, DISCFA=?, PERSUR=?, SVP=?, SWCHIND=?, FDBKIND=?, TRIGER=?, TRIGKY=?, FUNDPOOL=?, JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);

		try {
			for (Utrnpf u : utrnList) {
				int i = 1;
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getLife());
				ps.setString(i++, u.getCoverage());
				ps.setString(i++, u.getRider());
				ps.setInt(i++, u.getTranno());
				ps.setString(i++, u.getTermid());
				ps.setLong(i++, u.getTransactionDate());
				ps.setInt(i++, u.getTransactionTime());
				ps.setInt(i++, u.getUser());
				ps.setString(i++, u.getBatccoy());
				ps.setString(i++, u.getBatcbrn());
				ps.setInt(i++, u.getBatcactyr());
				ps.setInt(i++, u.getBatcactmn());
				ps.setString(i++, u.getBatctrcde());
				ps.setString(i++, u.getBatcbatch());
				ps.setString(i++, u.getNowDeferInd());
				ps.setInt(i++, u.getProcSeqNo());
				ps.setLong(i++, u.getMoniesDate());
				ps.setString(i++, u.getCrtable());
				ps.setInt(i++, u.getPlanSuffix());
				ps.setString(i++, u.getCntcurr());
				ps.setString(i++, u.getCnttyp());
				ps.setLong(i++, u.getCrComDate());
				ps.setString(i++, u.getUnitVirtualFund());
				ps.setString(i++, u.getFundCurrency());
				ps.setString(i++, u.getSacscode());
				ps.setString(i++, u.getSacstyp());
				ps.setString(i++, u.getGenlcde());
				ps.setString(i++, u.getUnitSubAccount());
				ps.setBigDecimal(i++, u.getContractAmount());
				ps.setString(i++, u.getUnitType());
				ps.setBigDecimal(i++, u.getInciprm01());
				ps.setBigDecimal(i++, u.getInciprm02());
				ps.setBigDecimal(i++, u.getNofUnits());
				ps.setBigDecimal(i++, u.getNofDunits());
				ps.setBigDecimal(i++, u.getFundRate());
				ps.setLong(i++, u.getStrpdate());
				ps.setInt(i++, u.getUstmno());
				ps.setLong(i++, u.getPriceDateUsed());
				ps.setBigDecimal(i++, u.getPriceUsed());
				ps.setBigDecimal(i++, u.getUnitBarePrice());
				ps.setInt(i++, u.getInciNum());
				ps.setInt(i++, u.getInciPerd01());
				ps.setInt(i++, u.getInciPerd02());
				ps.setBigDecimal(i++, u.getFundAmount());
				ps.setBigDecimal(i++, u.getJobnoPrice());
				ps.setBigDecimal(i++, u.getDiscountFactor());
				ps.setBigDecimal(i++, u.getSurrenderPercent());
				ps.setBigDecimal(i++, u.getSvp());
				ps.setString(i++, u.getSwitchIndicator());
				ps.setString(i++, u.getFeedbackInd());
				ps.setString(i++, u.getTriggerModule());
				ps.setString(i++, u.getTriggerKey());
				ps.setString(i++, u.getFundPool());
				ps.setString(i++, getJobnm());
				ps.setString(i++, getUsrprf());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.setLong(i++, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updatedUtrnpfRecord()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	
	public void deleteUtrnRecord(List<Utrnpf> utrnList) {
		String sql = "DELETE FROM UTRNPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Utrnpf u:utrnList){
				ps.setLong(1, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteUtrnRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
 //ILIFE-8786 end
	public Map<String, List<Utrnpf>> searchUstsByChdrnum(List<String> chdrnumList, String chdrcoy) {
		StringBuilder sqlUtrnSelect1 = new StringBuilder(
				"SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, USTMNO, VRTFND, UNITYP, TRANNO, SACSCODE, "
				+ "SACSTYP, BATCTRCDE, UNITSA, NDFIND, NOFUNT, NOFDUNT, MONIESDT, CNTAMNT, PRICEDT, FUNDAMNT, UNIQUE_NUMBER ");
		sqlUtrnSelect1.append("FROM UTRNPF WHERE CHDRCOY=? AND USTMNO != '0' AND ");
		sqlUtrnSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlUtrnSelect1
		.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,PLNSFX ASC,USTMNO ASC, LIFE ASC, COVERAGE ASC, RIDER ASC,"
				+ "VRTFND ASC, UNITYP ASC, MONIESDT ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psUtrnSelect = getPrepareStatement(sqlUtrnSelect1.toString());
		ResultSet sqlutrnpf1rs = null;
		Map<String, List<Utrnpf>> resultMap = new HashMap<>();
		try {
			psUtrnSelect.setString(1, chdrcoy);
			sqlutrnpf1rs = executeQuery(psUtrnSelect);
			while (sqlutrnpf1rs.next()) {
				Utrnpf u = new Utrnpf();
				u.setChdrcoy(sqlutrnpf1rs.getString("Chdrcoy"));
				u.setChdrnum(sqlutrnpf1rs.getString("Chdrnum"));
				u.setLife(sqlutrnpf1rs.getString("Life"));
				u.setCoverage(sqlutrnpf1rs.getString("Coverage"));
				u.setRider(sqlutrnpf1rs.getString("Rider"));
				u.setPlanSuffix(sqlutrnpf1rs.getInt("PLNSFX"));
				u.setTranno(sqlutrnpf1rs.getInt("Tranno"));
				u.setUnitVirtualFund(sqlutrnpf1rs.getString("VRTFND"));
				u.setUnitSubAccount(sqlutrnpf1rs.getString("UNITSA"));
				u.setNowDeferInd(sqlutrnpf1rs.getString("ndfind"));
				u.setNofUnits(sqlutrnpf1rs.getBigDecimal("nofunt"));
				u.setUnitType(sqlutrnpf1rs.getString("UNITYP"));
				u.setBatctrcde(sqlutrnpf1rs.getString("Batctrcde"));
				u.setPriceDateUsed(sqlutrnpf1rs.getLong("pricedt"));
				u.setMoniesDate(sqlutrnpf1rs.getLong("MONIESDT"));
				u.setNofDunits(sqlutrnpf1rs.getBigDecimal("nofdunt"));
				u.setUstmno(sqlutrnpf1rs.getInt("Ustmno"));
				u.setContractAmount(sqlutrnpf1rs.getBigDecimal("cntamnt"));
				u.setFundAmount(sqlutrnpf1rs.getBigDecimal("FUNDAMNT"));
				u.setSacscode(sqlutrnpf1rs.getString("Sacscode"));
				u.setSacstyp(sqlutrnpf1rs.getString("Sacstyp"));
				u.setUniqueNumber(sqlutrnpf1rs.getLong("Unique_Number"));
				u.setUnitVirtualFund(sqlutrnpf1rs.getString("VRTFND"));
				if (resultMap.containsKey(u.getChdrnum())) {
					resultMap.get(u.getChdrnum()).add(u);
				} else {
					List<Utrnpf> utrnpfList = new ArrayList<>();
					utrnpfList.add(u);
					resultMap.put(u.getChdrnum(), utrnpfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchZrstRecordByChdrnum()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psUtrnSelect, sqlutrnpf1rs);
		}
		return resultMap;
	}
	
	public List<Utrnpf> sqlDebt1300ChunkReadUtrnZfmcPayr(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String covdbtind, String chdrnumFrom, String chdrnumTo, int wsaaNextdate){
		String sqldealCursor = "SELECT * FROM ( SELECT  U.CHDRCOY, U.CHDRNUM, U.LIFE, U.COVERAGE, U.RIDER, U.PLNSFX, U.VRTFND, U.UNITYP, "
				+ " U.TRANNO, U.BATCTRCDE, U.UNITSA, U.NDFIND, U.NOFUNT, U.NOFDUNT, U.MONIESDT, U.CNTCURR, U.FDBKIND, U.COVDBTIND, "
				+ " U.CNTAMNT, U.FUNDAMNT, U.CONTYP, U.PRCSEQ, U.PERSUR, U.SWCHIND, U.FUNDRATE,U.FUNDPOOL ,"
				+ " FLOOR((ROW_NUMBER()OVER(  ORDER BY U.CHDRCOY, U.CHDRNUM, PRCSEQ, VRTFND, UNITYP  )-1)/?) BATCHNUM FROM "
				+ tableId
				+ " WHERE U.CHDRCOY = ? AND U.FDBKIND = ? AND U.COVDBTIND = ? AND U.CHDRNUM >= ? "
				+ " AND U.CHDRNUM <= ? AND Z.CHDRCOY = ? AND Z.ZFMCDAT <> 0 AND Z.VALIDFLAG = '1' AND Z.ZFMCDAT <= ? "
				+ " AND P.BTDATE=P.PTDATE AND P.VALIDFLAG = '1' ) MAIN WHERE BATCHNUM = ? ";

		PreparedStatement psUtrnSelect = getPrepareStatement(sqldealCursor);
		ResultSet sqlutrnpf1rs = null;
		List<Utrnpf> utrnpfList = new ArrayList<>();
		try {
			psUtrnSelect.setInt(1, batchExtractSize);
			psUtrnSelect.setString(2, chdrcoy);
			psUtrnSelect.setString(3, fdbkind);
			psUtrnSelect.setString(4, covdbtind);
			psUtrnSelect.setString(5, chdrnumFrom);
			psUtrnSelect.setString(6, chdrnumTo);
			psUtrnSelect.setString(7, chdrcoy);
			psUtrnSelect.setInt(8, wsaaNextdate);
			psUtrnSelect.setInt(9, batchID);

			sqlutrnpf1rs = executeQuery(psUtrnSelect);
			while (sqlutrnpf1rs.next()) {
				Utrnpf utrnpf = new Utrnpf();
				utrnpf.setChdrcoy(sqlutrnpf1rs.getString("chdrcoy"));
				utrnpf.setChdrnum(sqlutrnpf1rs.getString("chdrnum"));
				utrnpf.setLife(sqlutrnpf1rs.getString("life"));
				utrnpf.setCoverage(sqlutrnpf1rs.getString("coverage"));
				utrnpf.setRider(sqlutrnpf1rs.getString("rider"));
				utrnpf.setPlanSuffix(sqlutrnpf1rs.getInt("plnsfx"));
				utrnpf.setUnitVirtualFund(sqlutrnpf1rs.getString("vrtfnd"));
				utrnpf.setUnitType(sqlutrnpf1rs.getString("unityp"));
				utrnpf.setTranno(sqlutrnpf1rs.getInt("tranno"));
				utrnpf.setBatctrcde(sqlutrnpf1rs.getString("batctrcde"));
				utrnpf.setUnitSubAccount(sqlutrnpf1rs.getString("unitsa"));
				utrnpf.setNowDeferInd(sqlutrnpf1rs.getString("ndfind"));
				utrnpf.setNofUnits(sqlutrnpf1rs.getBigDecimal("nofunt"));
				utrnpf.setMoniesDate(sqlutrnpf1rs.getLong("moniesdt"));
				utrnpf.setCntcurr(sqlutrnpf1rs.getString("cntcurr"));
				utrnpf.setNofDunits(sqlutrnpf1rs.getBigDecimal("nofdunt"));
				utrnpf.setFeedbackInd(sqlutrnpf1rs.getString("fdbkind"));
				utrnpf.setContractAmount(sqlutrnpf1rs.getBigDecimal("cntamnt"));
				utrnpf.setFundAmount(sqlutrnpf1rs.getBigDecimal("fundamnt"));
				utrnpf.setCnttyp(sqlutrnpf1rs.getString("contyp"));
				utrnpf.setProcSeqNo(sqlutrnpf1rs.getInt("prcseq"));
				utrnpf.setSurrenderPercent(sqlutrnpf1rs.getBigDecimal("persur"));
				utrnpf.setFundRate(sqlutrnpf1rs.getBigDecimal("fundrate"));
				utrnpf.setCovdbtind(sqlutrnpf1rs.getString("covdbtind"));
				utrnpf.setSwitchIndicator(sqlutrnpf1rs.getString("swchind"));
				utrnpf.setFundPool(sqlutrnpf1rs.getString("fundpool"));
				utrnpfList.add(utrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("sqlDebt1300ChunkReadUtrnZfmcPayr()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(psUtrnSelect, sqlutrnpf1rs);
		}
		return utrnpfList;
	}
	
	public List<Utrnpf> searchUtrnRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int tranno){
		StringBuilder sql = new StringBuilder("SELECT * FROM UTRN WHERE CHDRNUM=? AND CHDRCOY=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND TRANNO=? ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, VRTFND ASC, UNITYP ASC, TRANNO ASC, UNIQUE_NUMBER DESC");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet sqlrs = null;
		List<Utrnpf> list = new ArrayList<Utrnpf>();
		Utrnpf u;
		try {
			ps.setString(1, chdrnum);
			ps.setString(2, chdrcoy);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			ps.setInt(6, tranno);
			sqlrs = executeQuery(ps);
			while(sqlrs.next()) {
				u = new Utrnpf();
				u.setChdrcoy(sqlrs.getString("Chdrcoy"));
				u.setChdrnum(sqlrs.getString("Chdrnum"));
				u.setLife(sqlrs.getString("Life"));
				u.setCoverage(sqlrs.getString("Coverage"));
				u.setRider(sqlrs.getString("Rider"));
				u.setPlanSuffix(sqlrs.getInt("PLNSFX"));
				u.setTranno(sqlrs.getInt("Tranno"));
				u.setTermid(sqlrs.getString("Termid"));
				u.setJobnoPrice(sqlrs.getBigDecimal("JCTLJNUMPR"));
				u.setTransactionDate(sqlrs.getInt("TRDT"));
				u.setTransactionTime(sqlrs.getInt("TRTM"));
				u.setUser(sqlrs.getInt("USER_T"));
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));
				u.setUnitSubAccount(sqlrs.getString("UNITSA"));
				u.setStrpdate(sqlrs.getLong("Strpdate"));
				u.setNowDeferInd(sqlrs.getString("ndfind"));
				u.setNofUnits(sqlrs.getBigDecimal("nofunt"));
				u.setUnitType(sqlrs.getString("UNITYP"));
				u.setBatccoy(sqlrs.getString("Batccoy"));
				u.setBatcbrn(sqlrs.getString("Batcbrn"));
				u.setBatcactyr(sqlrs.getInt("Batcactyr"));
				u.setBatcactmn(sqlrs.getInt("Batcactmn"));
				u.setBatctrcde(sqlrs.getString("Batctrcde"));
				u.setBatcbatch(sqlrs.getString("Batcbatch"));
				u.setPriceDateUsed(sqlrs.getLong("pricedt"));
				u.setMoniesDate(sqlrs.getLong("MONIESDT"));
				u.setPriceUsed(sqlrs.getBigDecimal("PriceUsed"));
				u.setCntcurr(sqlrs.getString("Cntcurr"));
				u.setCrtable(sqlrs.getString("Crtable"));
				u.setNofDunits(sqlrs.getBigDecimal("nofdunt"));
				u.setFeedbackInd(sqlrs.getString("FDBKIND"));
				u.setUnitBarePrice(sqlrs.getBigDecimal("ubrepr"));
				u.setInciNum(sqlrs.getInt("InciNum"));
				u.setInciPerd01(sqlrs.getInt("InciPerd01"));
				u.setInciPerd02(sqlrs.getInt("InciPerd02"));
				u.setInciprm01(sqlrs.getBigDecimal("Inciprm01"));
				u.setInciprm02(sqlrs.getBigDecimal("Inciprm02"));
				u.setUstmno(sqlrs.getInt("Ustmno"));
				u.setContractAmount(sqlrs.getBigDecimal("cntamnt"));
				u.setFundCurrency(sqlrs.getString("FNDCURR"));
				u.setFundAmount(sqlrs.getBigDecimal("FUNDAMNT"));
				u.setFundRate(sqlrs.getBigDecimal("FundRate"));
				u.setSacscode(sqlrs.getString("Sacscode"));
				u.setSacstyp(sqlrs.getString("Sacstyp"));
				u.setGenlcde(sqlrs.getString("Genlcde"));
				u.setTriggerModule(sqlrs.getString("Triger"));
				u.setTriggerKey(sqlrs.getString("Trigky"));
				u.setCnttyp(sqlrs.getString("CONTYP"));
				u.setProcSeqNo(sqlrs.getInt("prcseq"));
				u.setSurrenderPercent(sqlrs.getBigDecimal("persur"));
				u.setSvp(sqlrs.getBigDecimal("Svp"));
				u.setDiscountFactor(sqlrs.getBigDecimal("DISCFA"));
				u.setCrComDate(sqlrs.getLong("CRCDTE"));
				u.setCanInitUnitInd(sqlrs.getString("ciuind"));
				u.setSwitchIndicator(sqlrs.getString("SWCHIND")!= null ? (sqlrs.getString("SWCHIND").trim()) : "");
				u.setFundPool(sqlrs.getString("FUNDPOOL"));
				u.setUniqueNumber(sqlrs.getLong("Unique_Number"));
				u.setUnitVirtualFund(sqlrs.getString("VRTFND"));
				list.add(u);
			}
		}catch (SQLException e) {
			LOGGER.error("searchUtrnRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, sqlrs);
		}
		return list;
	}
}