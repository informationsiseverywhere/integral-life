package com.csc.life.unitlinkedprocessing.batchservice;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.integral.batch.model.FileFormat;
import com.csc.integral.batch.model.FileProcessingRequest;
import com.csc.integral.batch.service.FileProcessor;
import com.csc.integral.batch.service.FileService;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;

public class NAVFileService extends FileService {
	
	private String[] headers = {
			"Fund Code",
			"Fund Description",
			"Effective Date",
			"Bid Price",
			"Offer Price"
			};
	
	private String headerError = "";
	
	private List<ValidatedData> validatedDataList;
	
	private VprcpfDAO vprcpfDAO = IntegralApplicationContext.getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class);
	
	private String pricePattern = "[0-9]*\\.+([0-9][0-9][0-9][0-9][0-9])[0-9]*";
	private String decimalPattern = "[0-9]*\\.*[0-9]*";
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	
	private int currentDate= Integer.parseInt(dateFormat.format(new Date()));
	
	private DescpfDAO descDao = null;
	
	private int jobNo = -1;
	
	@Override
	public FileProcessor getFileParser(FileProcessingRequest request) {
		String formatString = request.getUploadedFilePath().substring(request.getUploadedFilePath().lastIndexOf(".") + 1);
		FileFormat fileFormat = FileFormat.valueOf(formatString == null ? "" :formatString.trim());
		FileProcessor fileProcessor = null;
		switch (fileFormat) {
		case csv:
			fileProcessor = new CSVFileProcessor(request.getUploadedFilePath());
			break;
		case txt: 
			// TODO Text and other File Formats can be support by creating FileProcessor and configuring here
			fileProcessor = null;
			break;
		default:
			break;
		}
		return fileProcessor;
	}

	@Override
	public List<Map<String, String>>  updateDatabase() {
		List<Vprcpf> vprcpfList = getVprcpfListForInsert();
		List<Integer> insertResult = vprcpfDAO.insert(vprcpfList);
		updateInsertResult(vprcpfList, insertResult);
		return getUpdatedDataForOutput();
	}
	
	private List<Map<String, String>> getUpdatedDataForOutput(){
		List<Map<String, String>> outputData = new ArrayList<>();
		for(ValidatedData data : validatedDataList){
			Map<String,String> outputValues = data.recFromFile;
			if(data.errorMessage.equalsIgnoreCase("") && data.dbInsertStatus){
				outputValues.put("Status", "Succeed: JobNo is "+Integer.toString(getJobNumber()));
			}else{
				outputValues.put("Status", "Failed: "+data.errorMessage);
			}
			outputData.add(outputValues);
		}
		return outputData;
	}
	
	private void updateInsertResult(List<Vprcpf> vprcpfList, List<Integer> insertResult){
		for(int index = 0; index < insertResult.size(); index++){
			if(insertResult.get(index) < 0){
				for(ValidatedData data : validatedDataList){
					if(data.daoObj != null && data.daoObj.equals(vprcpfList.get(index))){
						data.errorMessage = "Database insertion failed";
						break;
					}
				}
			}else{
				for(ValidatedData data : validatedDataList){
					if(data.daoObj != null && data.daoObj.equals(vprcpfList.get(index))){
						data.dbInsertStatus = true;
						break;
					}
				}
			}
		}
	}
	
	private List<Vprcpf> getVprcpfListForInsert(){
		List<Vprcpf> vprcpfList = new ArrayList<>();
		for(ValidatedData data : validatedDataList){
			if(data.errorMessage.equalsIgnoreCase("") && data.daoObj != null){
				vprcpfList.add(data.daoObj);
			}
		}
		return vprcpfList;
	}
	
	private Map<String, String> getFundCodeAndDesc(){
		Map<String, String> fundDescMap = new HashMap<>();
		descDao = DAOFactory.getDescpfDAO();
		List<Descpf> descItems = descDao.getAllDescTblItemByLang("IT", "T5515", "E");
		for(Descpf descpf : descItems){
			String fundCode = descpf.getDescitem().trim();
			String longDesc = descpf.getLongdesc().trim();
			if(fundCode != null && longDesc != null){
				fundDescMap.put(fundCode, longDesc);
			}
		}
		return fundDescMap;
	}
	
	
	@Override
	protected boolean validateData(List<String> headers, List<Map<String,String>> fileData) {
		if(validateHeaders(headers)){
			
			Map<String, String> fundValueDescMap = getFundCodeAndDesc();
			
			validatedDataList = new ArrayList<>();
			int recordSize = fileData.size();
			for(int recordIndex = 0; recordIndex < recordSize ; recordIndex++){
				String errorMsg = "";
				Vprcpf vprcpf = new Vprcpf();
				for(String header : headers){
					switch (header) {
					case "Fund Code":
						String fundCode = fileData.get(recordIndex).get(header);
						ValidatedData validatedData = null;
						String fundDescFromFile = fileData.get(recordIndex).get("Fund Description");
						String fundDescFromDB = null;
						if(fundCode == null || fundCode.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Fund Code is mandatory;";
						}else if((fundDescFromDB = fundValueDescMap.get(fundCode = fundCode.trim())) == null){
							errorMsg = errorMsg + "Fund Code did not match;";
						}else if(!fundDescFromDB.equalsIgnoreCase(fundDescFromFile)
								&& (!fundDescFromFile.equalsIgnoreCase(""))){
							errorMsg = errorMsg + "Fund Code and Description did not match;";
						}else if((validatedData = getFundCodeData(fundCode)) != null){
							String duplicateMsg = "Duplicate Fund Code;";
							errorMsg = errorMsg + duplicateMsg;
							validatedData.errorMessage = validatedData.errorMessage + duplicateMsg;
							validatedData.daoObj = null;
						}else{
							vprcpf.setUnitVirtualFund(fundCode);
						}
						break;
						
					case "Fund Description":
						String fundDesc = fileData.get(recordIndex).get(header);
						if(fundDesc == null || fundDesc.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Fund Description is mandatory;";
						}else if(!fundValueDescMap.containsValue(fundDesc)){
							errorMsg = errorMsg + "Fund Description did not match;";
						}
						break;
						
					case "Effective Date":
						String effDate = fileData.get(recordIndex).get(header);
						int validDateInInt = -1;
						if(effDate == null || effDate.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Effective Date is mandatory;";
						}else if((validDateInInt = getValidDateInInt(effDate)) == -1){
							errorMsg = errorMsg + "Invalid Effective Date;";
						}else{
							vprcpf.setEffdate(validDateInInt);
						}
						break;
						
					case "Bid Price":
						String bidPrice = fileData.get(recordIndex).get(header);
						if(bidPrice == null || bidPrice.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Bid Price is mandatory;";
						}else if(!bidPrice.matches(decimalPattern)){
							errorMsg = errorMsg + "Bid Price is invalid;";
						}else if(!(bidPrice = bidPrice.trim()).matches(pricePattern)){
							errorMsg = errorMsg + "Bid Price should be valid number with minimum five decimal digits;";
						}else{
							vprcpf.setUnitBidPrice(new BigDecimal(bidPrice));
						}
						break;
						
					case "Offer Price":
						String offerPrice = fileData.get(recordIndex).get(header);
						if(offerPrice == null || offerPrice.trim().equalsIgnoreCase("")){
							vprcpf.setUnitOfferPrice(null);
						}else if(!offerPrice.matches(decimalPattern)){
							errorMsg = errorMsg + "Offer Price is invalid;";
						}else if(!(offerPrice = offerPrice.trim()).matches(pricePattern)){
							errorMsg = errorMsg + "Offer Price should be valid number with minimum five decimal digits;";
						}else{
							vprcpf.setUnitOfferPrice(new BigDecimal(offerPrice));
						}
						break;
						
					default:
						break;
					}
				}
				
				if(errorMsg.equalsIgnoreCase("")){
					updateCommonAttribute(vprcpf);
					validatedDataList.add(new ValidatedData(fileData.get(recordIndex), errorMsg, vprcpf));
				}else{
					validatedDataList.add(new ValidatedData(fileData.get(recordIndex), errorMsg, null));
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	
	private int getJobNumber(){
		if(jobNo == -1){
			jobNo = vprcpfDAO.getMaxJobNumber() + 1;
		}
		return jobNo;
	}
	
	private void updateCommonAttribute(Vprcpf vprcpf){
		vprcpf.setCompany("2");
		vprcpf.setValidflag("3");
		vprcpf.setUnitType("A");
		vprcpf.setTranno(1);
		vprcpf.setJobno(getJobNumber());
		vprcpf.setProcflag("Y");
		//UpdateDate should be business date of the user who is uploading the file
		//since user is not available current date is used, this needs to be changed in future 
		vprcpf.setUpdateDate(currentDate);
		vprcpf.setTransactionTime(0);
	}
	
	private int getValidDateInInt(String dateStr){
		int validDateInInt = -1;
		dateFormat.setLenient(false);
		try{
			Date validDate = dateFormat.parse(dateStr);
			if(validDate != null){
				validDateInInt = Integer.parseInt(dateStr.trim());
			}
		}catch(ParseException e){
			validDateInInt = -1;
		}catch(NumberFormatException e){
			validDateInInt = -1;
		}
		return validDateInInt;
	}
	
	private ValidatedData getFundCodeData(String fundCode){
		for(ValidatedData validatedData : validatedDataList){
			if(validatedData.daoObj != null && validatedData.daoObj.getUnitVirtualFund() != null &&
					validatedData.daoObj.getUnitVirtualFund().equalsIgnoreCase(fundCode)){
				return validatedData;
			}
		}
		return null;
	}
	
	private boolean validateHeaders(List<String> headers){
		boolean isValidHeaders = true;
		List<String> headersInFile = headers;
		if(headersInFile == null || headersInFile.size() <= 0){
			headerError = "Header not available in the file";
			isValidHeaders = false;
		}else{
			for(String header : headers){
				if(!headersInFile.contains(header)){
					isValidHeaders = false;
					headerError = headerError  + header +" "; 
				}
			}
			if(!isValidHeaders){
				headerError = "Header(s) " + headerError + "not available";
			}
		}
		return isValidHeaders;
	}


	@Override
	public String[] getHeaders() {
		return Arrays.copyOf(headers, headers.length);//IJTI-316
	}


	@Override
	protected String getDataValidationError() {
		return headerError;
	}
	
	private class ValidatedData{
		
		private Map<String, String> recFromFile;
		
		private String errorMessage;
		
		private Vprcpf daoObj;
		
		private boolean dbInsertStatus = false;
		
		private ValidatedData(Map<String, String> recFromFile, String errorMessage, Vprcpf daoObj){
			this.recFromFile = recFromFile;
			this.errorMessage = errorMessage;
			this.daoObj = daoObj;
		}
	}
}
