/*
 * File: Unlsc01.java
 * Date: 30 August 2009 2:51:52
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLSC01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.terminationclaims.dataaccess.CovrpenTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6656rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Full Surrender Charge/Penalty Calculation Method #1.
* ----------------------------------------------------
*
* PROCESSING.
* ----------
*   Look-up the  "Surrender Value Penalty (SVP)"  factor table  T6656,
*   keyed on  surrender charge  method plus  component premium status.
*   We use the 'Nth' factor on  table T6656  which has  50 occurences.
*   The occurence we use is the no. of years that the premium has been
*   paid.  The  factor obtained  is then divided  by the  value in the
*   divisor field to give a calculated factor. This new factor is used
*   in the charge/penalty calculations.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Unlsc01 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLSC01";

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(1).isAPartOf(wsaaT5551Item, 4);
	private FixedLengthStringData wsaaT5551Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);
	private PackedDecimalData wsaaEstimateTotAmt = new PackedDecimalData(18, 5).init(0);

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, 1);
	private Validator partPlan = new Validator(wsaaPlanSwitch, 2);
	private Validator summaryPart = new Validator(wsaaPlanSwitch, 3);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private PackedDecimalData wsaaItmfrm = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaSurcFreq = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPrem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(13, 9);
	private ZonedDecimalData wsaaStoreYears = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaPtYears1 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaSvpDiff = new ZonedDecimalData(14, 9);
	private ZonedDecimalData wsaaSvpFactor = new ZonedDecimalData(14, 9);
		/* WSAA-YEAR-MNTH */
	private ZonedDecimalData wsaaPtYears = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaPtMnths = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaNoT6656Entry = new FixedLengthStringData(1);
	private Validator noT6656Entry = new Validator(wsaaNoT6656Entry, "Y");

	private FixedLengthStringData wsaaT6656Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6656Meth = new FixedLengthStringData(4).isAPartOf(wsaaT6656Key, 0);
	private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(4).isAPartOf(wsaaT6656Key, 4);
		/* TABLES */
	private static final String t5551 = "T5551";
	private static final String t6656 = "T6656";
	private CovrpenTableDAM covrpenIO = new CovrpenTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5551rec t5551rec = new T5551rec();
	private T6656rec t6656rec = new T6656rec();
	private Srcalcpy srcalcpy = new Srcalcpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readT5551400
	}

	public Unlsc01() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		initialzation100();
		if (isEQ(srcalcpy.planSuffix,ZERO)
		&& !summaryPart.isTrue()) {
			wholePlan200();
		}
		else {
			component300();
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialzation100()
	{
		/*INIT*/
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaPlanSwitch.set(srcalcpy.type);
		wsaaEstimateTotAmt.set(srcalcpy.estimatedVal);
		wsaaPrem.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaSvpDiff.set(ZERO);
		wsaaSvpFactor.set(ZERO);
		wsaaSurcFreq.set(ZERO);
		wsaaPtYears1.set(ZERO);
		wsaaPtYears.set(ZERO);
		wsaaPtMnths.set(ZERO);
		wsaaSurcFreq.set(srcalcpy.billfreq);
		/*EXIT*/
	}

protected void wholePlan200()
	{
		para200();
	}

protected void para200()
	{
		covrpenIO.setParams(SPACES);
		covrpenIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		covrpenIO.setChdrnum(srcalcpy.chdrChdrnum);
		covrpenIO.setLife(srcalcpy.lifeLife);
		covrpenIO.setCoverage(srcalcpy.covrCoverage);
		covrpenIO.setRider(srcalcpy.covrRider);
		covrpenIO.setFunction(varcom.begn);
		while ( !(isEQ(covrpenIO.getStatuz(),varcom.endp))) {
			accumAnnprem210();
		}
		
		if (isLT(wsaaEstimateTotAmt,wsaaAnnprem)) {
			wsaaAnnprem.set(wsaaEstimateTotAmt);
		}
		srcalcpy.actualVal.set(wsaaAnnprem);
	}

protected void accumAnnprem210()
	{
			para210();
		}

protected void para210()
	{
		SmartFileCode.execute(appVars, covrpenIO);
		if (isNE(covrpenIO.getStatuz(),varcom.oK)
		&& isNE(covrpenIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrpenIO.getStatuz());
			syserrrec.params.set(covrpenIO.getParams());
			fatalError9000();
		}
		if (isNE(covrpenIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrpenIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrpenIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrpenIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrpenIO.getRider(),srcalcpy.covrRider)
		|| isNE(covrpenIO.getStatuz(),varcom.oK)) {
			covrpenIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(covrpenIO.getValidflag(),"2")) {
			covrpenIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaItmfrm.set(covrpenIO.getCrrcd());
		readTableT5551400();
		if (isEQ(t5551rec.svcmeth,SPACES)) {
			covrpenIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaT6656Meth.set(t5551rec.svcmeth);
		wsaaPremStat.set(covrpenIO.getPstatcode());
		wsaaItemitem.set(wsaaT6656Key);
		wsaaItmfrm.set(covrpenIO.getCrrcd());
		readTableT6656500();
		if (noT6656Entry.isTrue()) {
			covrpenIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaPrem.set(covrpenIO.getInstprem());
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrpenIO.getCrrcd());
		findSvpFactor600();
		wsaaAnnprem.add(wsaaPrem);
		covrpenIO.setFunction(varcom.nextr);
	}

protected void component300()
	{
			para300();
		}

protected void para300()
	{
		wsaaItmfrm.set(srcalcpy.crrcd);
		readTableT5551400();
		wsaaT6656Meth.set(t5551rec.svcmeth);
		wsaaPremStat.set(srcalcpy.pstatcode);
		wsaaItemitem.set(wsaaT6656Key);
		wsaaItmfrm.set(srcalcpy.crrcd);
		readTableT6656500();
		if (noT6656Entry.isTrue()) {
			srcalcpy.actualVal.set(ZERO);
			return ;
		}
		wsaaPrem.set(srcalcpy.singp);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		findSvpFactor600();
		if (isLT(wsaaEstimateTotAmt,wsaaPrem)) {
			srcalcpy.actualVal.set(wsaaEstimateTotAmt);
		}
		else {
			srcalcpy.actualVal.set(wsaaPrem);
		}
	}

protected void readTableT5551400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initLang400();
				case readT5551400: 
					readT5551400();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initLang400()
	{
		wsaaT5551Key.set(srcalcpy.language);
	}

protected void readT5551400()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5551);
		wsaaT5551Crtable.set(srcalcpy.crtable);
		wsaaT5551Currcode.set(srcalcpy.currcode);
		itdmIO.setItemitem(wsaaT5551Item);
		itdmIO.setItmfrm(wsaaItmfrm);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemitem(),wsaaT5551Item)
		&& isEQ(itdmIO.getStatuz(),varcom.oK)) {
			wsaaT5551Key.set("*");
			goTo(GotoLabel.readT5551400);
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5551)
		|| isNE(itdmIO.getItemitem(),wsaaT5551Item)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableT6656500()
	{
		para500();
	}

protected void para500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6656);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(wsaaItmfrm);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
 
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6656)
		|| isNE(itdmIO.getItemitem(),wsaaItemitem)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			wsaaNoT6656Entry.set("Y");
		}
		else {
			wsaaNoT6656Entry.set(SPACES);
			t6656rec.t6656Rec.set(itdmIO.getGenarea());
		}
	}

protected void findSvpFactor600()
	{
		para600();
	}

protected void para600()
	{
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		compute(wsaaPtYears, 5).setDivide(datcon3rec.freqFactor, (12));
		wsaaPtMnths.setRemainder(wsaaPtYears);
		compute(wsaaPrem, 2).set(mult(wsaaSurcFreq,wsaaPrem));
		wsaaPtYears.add(1);
		/* IF  WSAA-PT-YEARS           > 50                             */
		if (isGT(wsaaPtYears, 49)) {
			wsaaPtMnths.set(ZERO);
			wsaaPtYears.set(50);
		}
		/*  Only need to check if not on a full year boundary ie. that*/
		/*  MNTHS NOT = 0. Doesnt matter how many years.*/
		if (isNE(wsaaPtMnths,0)) {
			svpCalc610();
		}
		else {
			wsaaSvpFactor.set(t6656rec.svpFactor[wsaaPtYears.toInt()]);
		}
		compute(wsaaFactor, 9).set(div(wsaaSvpFactor,t6656rec.divisor));
		compute(wsaaPrem, 10).setRounded(mult(wsaaPrem,wsaaFactor));
	}

protected void svpCalc610()
	{
		/*PARA*/
		wsaaPtYears1.set(wsaaPtYears);
		wsaaPtYears1.add(1);
		/*  Given :*/
		/*      FACTOR  is the factor at  YEARS*/
		/*      FACTOR1 is the factor at  YEARS1*/
		/*  Calculate :*/
		/*      A factor F at a general time T between YEARS and YEARS1*/
		/*  Using the relationship :*/
		/*            T  -  YEARS             F - FACTOR*/
		/*          YEARS1 - YEARS         FACTOR1 - FACTOR*/
		/*  Get the formula, F = :*/
		/*  (((T-YEARS) * (FACTOR1-FACTOR)) / (YEARS1-YEARS))   +   FACTOR*/
		/*  Where :*/
		/*      YEARS   = WSAA-PT-YEARS (expressed in months)*/
		/*      YEARS1  = WSAA-PT-YEARS1 (expressed in months)*/
		/*      FACTOR  = T6656-SVP-FACTOR(WSAA-PT-YEARS)*/
		/*      FACTOR1 = T6656-SVP-FACTOR(WSAA-PT-YEARS1)*/
		/*  Note :*/
		/*      T       = WSAA-PT-YEARS + WSAA-PT-MNTHS*/
		/*       ( hence, T - YEARS = WSAA-PT-MONTHS )*/
		/*      YEARS1 - YEARS always gives 12 months*/
		/*  Store value of WSAA-PT-YEARS before converting to months*/
		wsaaStoreYears.set(wsaaPtYears);
		/*  Find the difference between the two relevant factors.*/
		compute(wsaaSvpDiff, 9).set(sub(t6656rec.svpFactor[wsaaPtYears1.toInt()],t6656rec.svpFactor[wsaaPtYears.toInt()]));
		/*  Convert figures to months.*/
		compute(wsaaPtYears, 0).set(mult(wsaaPtYears,12));
		compute(wsaaPtYears1, 0).set(mult(wsaaPtYears1,12));
		compute(wsaaSvpFactor, 9).set(add((div(mult(wsaaSvpDiff,wsaaPtMnths),12)),t6656rec.svpFactor[wsaaStoreYears.toInt()]));
		/*EXIT*/
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		srcalcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
