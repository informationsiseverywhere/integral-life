package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: VprcpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:56
 * Class transformed from VPRCPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class VprcpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 92;//ILIFE-2745
	public FixedLengthStringData vprcrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData vprcpfRecord = vprcrec;
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(vprcrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(vprcrec);
	public FixedLengthStringData unitType = DD.ultype.copy().isAPartOf(vprcrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(vprcrec);
	public PackedDecimalData jobno = DD.jobno.copy().isAPartOf(vprcrec);
	public PackedDecimalData unitBarePrice = DD.ubrepr.copy().isAPartOf(vprcrec);
	public PackedDecimalData unitBidPrice = DD.ubidpr.copy().isAPartOf(vprcrec);
	public PackedDecimalData unitOfferPrice = DD.uoffpr.copy().isAPartOf(vprcrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(vprcrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(vprcrec);
	public PackedDecimalData updateDate = DD.upddte.copy().isAPartOf(vprcrec);
	public FixedLengthStringData procflag = DD.procflag.copy().isAPartOf(vprcrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(vprcrec);
	//ILIFE-2745-STARTS
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(vprcrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(vprcrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(vprcrec);
	//ILIFE-2745-ENDS

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public VprcpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for VprcpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public VprcpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for VprcpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public VprcpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for VprcpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public VprcpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("VPRCPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"COMPANY, " +
							"VRTFND, " +
							"ULTYPE, " +
							"EFFDATE, " +
							"JOBNO, " +
							"UBREPR, " +
							"UBIDPR, " +
							"UOFFPR, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"UPDDTE, " +
							"PROCFLAG, " +
							"TRTM, " +
							//ILIFE-2745-STARTS
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							//ILIFE-2745-ENDS
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     company,
                                     unitVirtualFund,
                                     unitType,
                                     effdate,
                                     jobno,
                                     unitBarePrice,
                                     unitBidPrice,
                                     unitOfferPrice,
                                     validflag,
                                     tranno,
                                     updateDate,
                                     procflag,
                                     transactionTime,
                                     //ILIFE-2745-STARTS
                                     userProfile,
                                     jobName,
                                     datime,
                                     //ILIFE-2745-ENDS
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		company.clear();
  		unitVirtualFund.clear();
  		unitType.clear();
  		effdate.clear();
  		jobno.clear();
  		unitBarePrice.clear();
  		unitBidPrice.clear();
  		unitOfferPrice.clear();
  		validflag.clear();
  		tranno.clear();
  		updateDate.clear();
  		procflag.clear();
  		transactionTime.clear();
  		//ILIFE-2745-STARTS
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		//ILIFE-2745-ENDS
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getVprcrec() {
  		return vprcrec;
	}

	public FixedLengthStringData getVprcpfRecord() {
  		return vprcpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setVprcrec(what);
	}

	public void setVprcrec(Object what) {
  		this.vprcrec.set(what);
	}

	public void setVprcpfRecord(Object what) {
  		this.vprcpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(vprcrec.getLength());
		result.set(vprcrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}