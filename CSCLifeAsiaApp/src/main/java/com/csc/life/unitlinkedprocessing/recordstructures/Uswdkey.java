package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:46
 * Description:
 * Copybook name: USWDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Uswdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData uswdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData uswdKey = new FixedLengthStringData(64).isAPartOf(uswdFileKey, 0, REDEFINE);
  	public FixedLengthStringData uswdChdrcoy = new FixedLengthStringData(1).isAPartOf(uswdKey, 0);
  	public FixedLengthStringData uswdChdrnum = new FixedLengthStringData(8).isAPartOf(uswdKey, 1);
  	public PackedDecimalData uswdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(uswdKey, 9);
  	public FixedLengthStringData uswdLife = new FixedLengthStringData(2).isAPartOf(uswdKey, 12);
  	public FixedLengthStringData uswdCoverage = new FixedLengthStringData(2).isAPartOf(uswdKey, 14);
  	public FixedLengthStringData uswdRider = new FixedLengthStringData(2).isAPartOf(uswdKey, 16);
  	public PackedDecimalData uswdTranno = new PackedDecimalData(5, 0).isAPartOf(uswdKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(uswdKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(uswdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		uswdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}