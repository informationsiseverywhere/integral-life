package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5539screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5539ScreenVars sv = (S5539ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5539screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5539ScreenVars screenVars = (S5539ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.dfact01.setClassString("");
		screenVars.dfact02.setClassString("");
		screenVars.dfact03.setClassString("");
		screenVars.dfact04.setClassString("");
		screenVars.dfact05.setClassString("");
		screenVars.dfact06.setClassString("");
		screenVars.dfact07.setClassString("");
		screenVars.dfact08.setClassString("");
		screenVars.dfact09.setClassString("");
		screenVars.dfact10.setClassString("");
		screenVars.dfact11.setClassString("");
		screenVars.dfact12.setClassString("");
		screenVars.dfact13.setClassString("");
		screenVars.dfact14.setClassString("");
		screenVars.dfact15.setClassString("");
		screenVars.dfact16.setClassString("");
		screenVars.dfact17.setClassString("");
		screenVars.dfact18.setClassString("");
		screenVars.dfact19.setClassString("");
		screenVars.dfact20.setClassString("");
		screenVars.dfact21.setClassString("");
		screenVars.dfact22.setClassString("");
		screenVars.dfact23.setClassString("");
		screenVars.dfact24.setClassString("");
		screenVars.dfact25.setClassString("");
		screenVars.dfact26.setClassString("");
		screenVars.dfact27.setClassString("");
		screenVars.dfact28.setClassString("");
		screenVars.dfact29.setClassString("");
		screenVars.dfact30.setClassString("");
		screenVars.dfact31.setClassString("");
		screenVars.dfact32.setClassString("");
		screenVars.dfact33.setClassString("");
		screenVars.dfact34.setClassString("");
		screenVars.dfact35.setClassString("");
		screenVars.dfact36.setClassString("");
		screenVars.dfact37.setClassString("");
		screenVars.dfact38.setClassString("");
		screenVars.dfact39.setClassString("");
		screenVars.dfact40.setClassString("");
		screenVars.dfact41.setClassString("");
		screenVars.dfact42.setClassString("");
		screenVars.dfact43.setClassString("");
		screenVars.dfact44.setClassString("");
		screenVars.dfact45.setClassString("");
		screenVars.dfact46.setClassString("");
		screenVars.dfact47.setClassString("");
		screenVars.dfact48.setClassString("");
		screenVars.dfact49.setClassString("");
		screenVars.dfact50.setClassString("");
		screenVars.dfact51.setClassString("");
		screenVars.dfact52.setClassString("");
		screenVars.dfact53.setClassString("");
		screenVars.dfact54.setClassString("");
		screenVars.dfact55.setClassString("");
		screenVars.dfact56.setClassString("");
		screenVars.dfact57.setClassString("");
		screenVars.dfact58.setClassString("");
		screenVars.dfact59.setClassString("");
		screenVars.dfact60.setClassString("");
		screenVars.dfact61.setClassString("");
		screenVars.dfact62.setClassString("");
		screenVars.dfact63.setClassString("");
		screenVars.dfact64.setClassString("");
		screenVars.dfact65.setClassString("");
		screenVars.dfact66.setClassString("");
		screenVars.dfact67.setClassString("");
		screenVars.dfact68.setClassString("");
		screenVars.dfact69.setClassString("");
		screenVars.dfact70.setClassString("");
		screenVars.dfact71.setClassString("");
		screenVars.dfact72.setClassString("");
		screenVars.dfact73.setClassString("");
		screenVars.dfact74.setClassString("");
		screenVars.dfact75.setClassString("");
		screenVars.dfact76.setClassString("");
		screenVars.dfact77.setClassString("");
		screenVars.dfact78.setClassString("");
		screenVars.dfact79.setClassString("");
		screenVars.dfact80.setClassString("");
		screenVars.dfact81.setClassString("");
		screenVars.dfact82.setClassString("");
		screenVars.dfact83.setClassString("");
		screenVars.dfact84.setClassString("");
		screenVars.dfact85.setClassString("");
		screenVars.dfact86.setClassString("");
		screenVars.dfact87.setClassString("");
		screenVars.dfact88.setClassString("");
		screenVars.dfact89.setClassString("");
		screenVars.dfact90.setClassString("");
		screenVars.dfact91.setClassString("");
		screenVars.dfact92.setClassString("");
		screenVars.dfact93.setClassString("");
		screenVars.dfact94.setClassString("");
		screenVars.dfact95.setClassString("");
		screenVars.dfact96.setClassString("");
		screenVars.dfact97.setClassString("");
		screenVars.dfact98.setClassString("");
		screenVars.dfact99.setClassString("");
		screenVars.fact01.setClassString("");
		screenVars.fact02.setClassString("");
		screenVars.fact03.setClassString("");
		screenVars.fact04.setClassString("");
		screenVars.fact05.setClassString("");
		screenVars.fact06.setClassString("");
		screenVars.fact07.setClassString("");
		screenVars.fact08.setClassString("");
		screenVars.fact09.setClassString("");
		screenVars.fact10.setClassString("");
		screenVars.fact11.setClassString("");
	}

/**
 * Clear all the variables in S5539screen
 */
	public static void clear(VarModel pv) {
		S5539ScreenVars screenVars = (S5539ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.dfact01.clear();
		screenVars.dfact02.clear();
		screenVars.dfact03.clear();
		screenVars.dfact04.clear();
		screenVars.dfact05.clear();
		screenVars.dfact06.clear();
		screenVars.dfact07.clear();
		screenVars.dfact08.clear();
		screenVars.dfact09.clear();
		screenVars.dfact10.clear();
		screenVars.dfact11.clear();
		screenVars.dfact12.clear();
		screenVars.dfact13.clear();
		screenVars.dfact14.clear();
		screenVars.dfact15.clear();
		screenVars.dfact16.clear();
		screenVars.dfact17.clear();
		screenVars.dfact18.clear();
		screenVars.dfact19.clear();
		screenVars.dfact20.clear();
		screenVars.dfact21.clear();
		screenVars.dfact22.clear();
		screenVars.dfact23.clear();
		screenVars.dfact24.clear();
		screenVars.dfact25.clear();
		screenVars.dfact26.clear();
		screenVars.dfact27.clear();
		screenVars.dfact28.clear();
		screenVars.dfact29.clear();
		screenVars.dfact30.clear();
		screenVars.dfact31.clear();
		screenVars.dfact32.clear();
		screenVars.dfact33.clear();
		screenVars.dfact34.clear();
		screenVars.dfact35.clear();
		screenVars.dfact36.clear();
		screenVars.dfact37.clear();
		screenVars.dfact38.clear();
		screenVars.dfact39.clear();
		screenVars.dfact40.clear();
		screenVars.dfact41.clear();
		screenVars.dfact42.clear();
		screenVars.dfact43.clear();
		screenVars.dfact44.clear();
		screenVars.dfact45.clear();
		screenVars.dfact46.clear();
		screenVars.dfact47.clear();
		screenVars.dfact48.clear();
		screenVars.dfact49.clear();
		screenVars.dfact50.clear();
		screenVars.dfact51.clear();
		screenVars.dfact52.clear();
		screenVars.dfact53.clear();
		screenVars.dfact54.clear();
		screenVars.dfact55.clear();
		screenVars.dfact56.clear();
		screenVars.dfact57.clear();
		screenVars.dfact58.clear();
		screenVars.dfact59.clear();
		screenVars.dfact60.clear();
		screenVars.dfact61.clear();
		screenVars.dfact62.clear();
		screenVars.dfact63.clear();
		screenVars.dfact64.clear();
		screenVars.dfact65.clear();
		screenVars.dfact66.clear();
		screenVars.dfact67.clear();
		screenVars.dfact68.clear();
		screenVars.dfact69.clear();
		screenVars.dfact70.clear();
		screenVars.dfact71.clear();
		screenVars.dfact72.clear();
		screenVars.dfact73.clear();
		screenVars.dfact74.clear();
		screenVars.dfact75.clear();
		screenVars.dfact76.clear();
		screenVars.dfact77.clear();
		screenVars.dfact78.clear();
		screenVars.dfact79.clear();
		screenVars.dfact80.clear();
		screenVars.dfact81.clear();
		screenVars.dfact82.clear();
		screenVars.dfact83.clear();
		screenVars.dfact84.clear();
		screenVars.dfact85.clear();
		screenVars.dfact86.clear();
		screenVars.dfact87.clear();
		screenVars.dfact88.clear();
		screenVars.dfact89.clear();
		screenVars.dfact90.clear();
		screenVars.dfact91.clear();
		screenVars.dfact92.clear();
		screenVars.dfact93.clear();
		screenVars.dfact94.clear();
		screenVars.dfact95.clear();
		screenVars.dfact96.clear();
		screenVars.dfact97.clear();
		screenVars.dfact98.clear();
		screenVars.dfact99.clear();
		screenVars.fact01.clear();
		screenVars.fact02.clear();
		screenVars.fact03.clear();
		screenVars.fact04.clear();
		screenVars.fact05.clear();
		screenVars.fact06.clear();
		screenVars.fact07.clear();
		screenVars.fact08.clear();
		screenVars.fact09.clear();
		screenVars.fact10.clear();
		screenVars.fact11.clear();
	}
}
