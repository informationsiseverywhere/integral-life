package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Ufnspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UfnspfDAO extends BaseDAO<Ufnspf> {
	public void insertUfnspfRecord(List<Ufnspf> urList);
}
