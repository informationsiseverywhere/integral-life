package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrxpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UtrxpfDAOImpl extends BaseDAOImpl<Utrxpf> implements UtrxpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UtrxpfDAOImpl.class);

	public List<Utrxpf> searchUtrxpfRecord(String tableId, String memName, int batchExtractSize, int batchID) {
		StringBuilder sqlUtrxSelect = new StringBuilder();
		sqlUtrxSelect.append(" SELECT * FROM ( ");
		sqlUtrxSelect.append(" SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,VRTFND,UNITYP,TRANNO,BATCTRCDE,UNITSA,NDFIND,NOFUNT,MONIESDT,CNTCURR,NOFDUNT,FDBKIND,CNTAMNT,FUNDAMNT,CONTYP,PRCSEQ,PERSUR,FUNDRATE,FUNDPOOL ");
		sqlUtrxSelect.append(" ,FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC)-1)/?) BATCHNUM ");
		sqlUtrxSelect.append("  FROM ");
		sqlUtrxSelect.append(tableId);
		sqlUtrxSelect.append("   WHERE MEMBER_NAME = ? ");
		sqlUtrxSelect.append(" ) MAIN WHERE BATCHNUM = ? ");

		PreparedStatement psUtrxSelect = getPrepareStatement(sqlUtrxSelect.toString());
		ResultSet sqlutrxpf1rs = null;
		List<Utrxpf> utrxpfList = new ArrayList<>();
		try {
			psUtrxSelect.setInt(1, batchExtractSize);
			psUtrxSelect.setString(2, memName);
			psUtrxSelect.setInt(3, batchID);

			sqlutrxpf1rs = executeQuery(psUtrxSelect);
			while (sqlutrxpf1rs.next()) {
				Utrxpf utrxpf = new Utrxpf();
				utrxpf.setChdrcoy(sqlutrxpf1rs.getString("chdrcoy"));
				utrxpf.setChdrnum(sqlutrxpf1rs.getString("chdrnum"));
				utrxpf.setLife(sqlutrxpf1rs.getString("life"));
				utrxpf.setCoverage(sqlutrxpf1rs.getString("coverage"));
				utrxpf.setRider(sqlutrxpf1rs.getString("rider"));
				utrxpf.setPlanSuffix(sqlutrxpf1rs.getInt("plnsfx"));
				utrxpf.setUnitVirtualFund(sqlutrxpf1rs.getString("vrtfnd"));
				utrxpf.setUnitType(sqlutrxpf1rs.getString("unityp"));
				utrxpf.setTranno(sqlutrxpf1rs.getInt("tranno"));
				utrxpf.setBatctrcde(sqlutrxpf1rs.getString("batctrcde"));
				utrxpf.setUnitSubAccount(sqlutrxpf1rs.getString("unitsa"));
				utrxpf.setNowDeferInd(sqlutrxpf1rs.getString("ndfind"));
				utrxpf.setNofUnits(sqlutrxpf1rs.getBigDecimal("nofunt"));
				utrxpf.setMoniesDate(sqlutrxpf1rs.getInt("moniesdt"));
				utrxpf.setCntcurr(sqlutrxpf1rs.getString("cntcurr"));
				utrxpf.setNofDunits(sqlutrxpf1rs.getBigDecimal("nofdunt"));
				utrxpf.setFeedbackInd(sqlutrxpf1rs.getString("fdbkind"));
				utrxpf.setContractAmount(sqlutrxpf1rs.getBigDecimal("cntamnt"));
				utrxpf.setFundAmount(sqlutrxpf1rs.getBigDecimal("fundamnt"));
				utrxpf.setContractType(sqlutrxpf1rs.getString("contyp"));
				utrxpf.setProcSeqNo(sqlutrxpf1rs.getInt("prcseq"));
				utrxpf.setSurrenderPercent(sqlutrxpf1rs.getBigDecimal("persur"));
				utrxpf.setFundRate(sqlutrxpf1rs.getBigDecimal("fundrate"));
				utrxpf.setFundPool(sqlutrxpf1rs.getString("fundpool"));
				utrxpf.setUniqueNumber(sqlutrxpf1rs.getLong("UNIQUE_NUMBER"));
				utrxpfList.add(utrxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchUtrxpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psUtrxSelect, sqlutrxpf1rs);
		}
		return utrxpfList;
	}
	
	   public void insertUtrxRecoed(List<Utrxpf> utrxBulkOpList, String tableId) {
	        if (utrxBulkOpList != null && utrxBulkOpList.size() > 0) {
	    		StringBuilder sqlUtrxSelect = new StringBuilder();
	    		sqlUtrxSelect.append(" INSERT INTO ");
	    		sqlUtrxSelect.append(tableId);
	    		sqlUtrxSelect.append(" (CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,VRTFND,UNITYP,TRANNO,BATCTRCDE,UNITSA,NDFIND,NOFUNT,NOFDUNT,MONIESDT,CNTCURR,FDBKIND,CNTAMNT,FUNDAMNT,CONTYP,PRCSEQ,PERSUR,FUNDRATE,MEMBER_NAME,FUNDPOOL) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
	       
	            PreparedStatement psUtrxInsert = getPrepareStatement(sqlUtrxSelect.toString());
	            try {
	                for (Utrxpf u : utrxBulkOpList) {
	                	int i = 1;
	                	psUtrxInsert.setString(i++, u.getChdrcoy());
	                	psUtrxInsert.setString(i++, u.getChdrnum());
	                	psUtrxInsert.setString(i++, u.getLife());
	                	psUtrxInsert.setString(i++, u.getCoverage());
	                	psUtrxInsert.setString(i++, u.getRider());
	                	psUtrxInsert.setInt(i++, u.getPlanSuffix());
	                	psUtrxInsert.setString(i++, u.getUnitVirtualFund());
	                	psUtrxInsert.setString(i++, u.getUnitType());
	                	psUtrxInsert.setInt(i++, u.getTranno());
	                	psUtrxInsert.setString(i++, u.getBatctrcde());
	                	psUtrxInsert.setString(i++, u.getUnitSubAccount());
	                	psUtrxInsert.setString(i++, u.getNowDeferInd());
	                	psUtrxInsert.setBigDecimal(i++, u.getNofUnits());
	                	psUtrxInsert.setBigDecimal(i++, u.getNofDunits());
	                	psUtrxInsert.setInt(i++, u.getMoniesDate());
	                	psUtrxInsert.setString(i++, u.getCntcurr());
	                	psUtrxInsert.setString(i++, u.getFeedbackInd());
	                	psUtrxInsert.setBigDecimal(i++, u.getContractAmount());
	                	psUtrxInsert.setBigDecimal(i++, u.getFundAmount());
	                	psUtrxInsert.setString(i++, u.getContractType());
	                	psUtrxInsert.setInt(i++, u.getProcSeqNo());
	                	psUtrxInsert.setBigDecimal(i++, u.getSurrenderPercent());
	                	psUtrxInsert.setBigDecimal(i++, u.getFundRate());
	                	psUtrxInsert.setString(i++, u.getMemberName());
	                	psUtrxInsert.setString(i++, u.getFundPool());
	                    psUtrxInsert.addBatch();
	                }
	                psUtrxInsert.executeBatch();
	            } catch (SQLException e) {
	                LOGGER.error("insertUtrxRecoed()", e);//IJTI-1561
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(psUtrxInsert, null);
	            }
	        }
	    }
}
