package com.csc.life.unitlinkedprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:26
 * Description:
 * Copybook name: ULPARAMS
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ulparams extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(17);
  	public ZonedDecimalData stmEffdate = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 0);
  	public ZonedDecimalData stmJobno = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 8);
  	public FixedLengthStringData stmType = new FixedLengthStringData(1).isAPartOf(parmRecord, 16);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}