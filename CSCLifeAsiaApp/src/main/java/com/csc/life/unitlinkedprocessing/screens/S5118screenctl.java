package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5118screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S5118screensfl";
		lrec.subfileClass = S5118screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 12;
		lrec.pageSubfile = 11;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5118ScreenVars sv = (S5118ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5118screenctlWritten, sv.S5118screensflWritten, av, sv.s5118screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5118ScreenVars screenVars = (S5118ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.currds.setClassString("");
		screenVars.polinc.setClassString("");
		screenVars.crtable.setClassString("");
	}

/**
 * Clear all the variables in S5118screenctl
 */
	public static void clear(VarModel pv) {
		S5118ScreenVars screenVars = (S5118ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.currcd.clear();
		screenVars.currds.clear();
		screenVars.polinc.clear();
		screenVars.crtable.clear();
	}
}
