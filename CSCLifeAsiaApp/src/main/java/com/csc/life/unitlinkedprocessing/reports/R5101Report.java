package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5101.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5101Report extends SMARTReportLayout { 

	private FixedLengthStringData batctrcde = new FixedLengthStringData(4);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private ZonedDecimalData convrate = new ZonedDecimalData(13, 4);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData estflag = new FixedLengthStringData(1);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(1);
	private ZonedDecimalData fundvalb = new ZonedDecimalData(18, 5);
	private ZonedDecimalData fundvalc = new ZonedDecimalData(18, 5);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private FixedLengthStringData ndfind = new FixedLengthStringData(1);
	private ZonedDecimalData nofunits = new ZonedDecimalData(16, 5);
	private ZonedDecimalData nofuntsb = new ZonedDecimalData(16, 5);
	private ZonedDecimalData nofuntsc = new ZonedDecimalData(16, 5);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private ZonedDecimalData prcntdiff = new ZonedDecimalData(6, 3);
	private ZonedDecimalData prcseq = new ZonedDecimalData(3, 0);
	private FixedLengthStringData pricedt = new FixedLengthStringData(10);
	private ZonedDecimalData pricenow = new ZonedDecimalData(9, 5);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totfundval = new ZonedDecimalData(18, 5);
	private ZonedDecimalData totnofunts = new ZonedDecimalData(16, 5);
	private ZonedDecimalData tranval = new ZonedDecimalData(18, 5);
	private FixedLengthStringData unitsa = new FixedLengthStringData(4);
	private FixedLengthStringData unityp = new FixedLengthStringData(1);
	private ZonedDecimalData untsdiff = new ZonedDecimalData(16, 5);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5101Report() {
		super();
	}


	/**
	 * Print the XML for R5101dd1
	 */
	public void printR5101dd1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(10).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 15, 4));
		batctrcde.setFieldName("batctrcde");
		batctrcde.setInternal(subString(recordData, 19, 4));
		prcseq.setFieldName("prcseq");
		prcseq.setInternal(subString(recordData, 23, 3));
		pricedt.setFieldName("pricedt");
		pricedt.setInternal(subString(recordData, 26, 10));
		ndfind.setFieldName("ndfind");
		ndfind.setInternal(subString(recordData, 36, 1));
		unitsa.setFieldName("unitsa");
		unitsa.setInternal(subString(recordData, 37, 4));
		tranval.setFieldName("tranval");
		tranval.setInternal(subString(recordData, 41, 18));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 59, 3));
		convrate.setFieldName("convrate");
		convrate.setInternal(subString(recordData, 62, 13));
		nofunits.setFieldName("nofunits");
		nofunits.setInternal(subString(recordData, 75, 16));
		estflag.setFieldName("estflag");
		estflag.setInternal(subString(recordData, 91, 1));
		printLayout("R5101dd1",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				plnsfx,
				batctrcde,
				prcseq,
				pricedt,
				ndfind,
				unitsa,
				tranval,
				cntcurr,
				convrate,
				nofunits,
				estflag
			}
			, new Object[] {			// indicators
				new Object[]{"ind10", indicArea.charAt(10)}
			}
		);

	}

	/**
	 * Print the XML for R5101dh0
	 */
	public void printR5101dh0(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5101dh0",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(7);
	}

	/**
	 * Print the XML for R5101dh1
	 */
	public void printR5101dh1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		vrtfnd.setFieldName("vrtfnd");
		vrtfnd.setInternal(subString(recordData, 1, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 5, 30));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 35, 3));
		curdesc.setFieldName("curdesc");
		curdesc.setInternal(subString(recordData, 38, 30));
		fundtyp.setFieldName("fundtyp");
		fundtyp.setInternal(subString(recordData, 68, 1));
		printLayout("R5101dh1",			// Record name
			new BaseData[]{			// Fields:
				vrtfnd,
				longdesc,
				fndcurr,
				curdesc,
				fundtyp
			}
		);

	}

	/**
	 * Print the XML for R5101dt1
	 */
	public void printR5101dt1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		fundvalb.setFieldName("fundvalb");
		fundvalb.setInternal(subString(recordData, 1, 18));
		nofuntsb.setFieldName("nofuntsb");
		nofuntsb.setInternal(subString(recordData, 19, 16));
		fundvalc.setFieldName("fundvalc");
		fundvalc.setInternal(subString(recordData, 35, 18));
		nofuntsc.setFieldName("nofuntsc");
		nofuntsc.setInternal(subString(recordData, 53, 16));
		untsdiff.setFieldName("untsdiff");
		untsdiff.setInternal(subString(recordData, 69, 16));
		prcntdiff.setFieldName("prcntdiff");
		prcntdiff.setInternal(subString(recordData, 85, 6));
		printLayout("R5101dt1",			// Record name
			new BaseData[]{			// Fields:
				fundvalb,
				nofuntsb,
				fundvalc,
				nofuntsc,
				untsdiff,
				prcntdiff
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5101e01
	 */
	public void printR5101e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R5101e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5101e02
	 */
	public void printR5101e02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R5101e02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5101sd1
	 */
	public void printR5101sd1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		vrtfnd.setFieldName("vrtfnd");
		vrtfnd.setInternal(subString(recordData, 1, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 5, 30));
		unityp.setFieldName("unityp");
		unityp.setInternal(subString(recordData, 35, 1));
		totfundval.setFieldName("totfundval");
		totfundval.setInternal(subString(recordData, 36, 18));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 54, 3));
		pricenow.setFieldName("pricenow");
		pricenow.setInternal(subString(recordData, 57, 9));
		pricedt.setFieldName("pricedt");
		pricedt.setInternal(subString(recordData, 66, 10));
		totnofunts.setFieldName("totnofunts");
		totnofunts.setInternal(subString(recordData, 76, 16));
		prcntdiff.setFieldName("prcntdiff");
		prcntdiff.setInternal(subString(recordData, 92, 6));
		printLayout("R5101sd1",			// Record name
			new BaseData[]{			// Fields:
				vrtfnd,
				longdesc,
				unityp,
				totfundval,
				fndcurr,
				pricenow,
				pricedt,
				totnofunts,
				prcntdiff
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5101sh0
	 */
	public void printR5101sh0(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5101sh0",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(9);
	}

	/**
	 * Print the XML for R5101sh1
	 */
	public void printR5101sh1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("R5101sh1",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}


}
