package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh608screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh608ScreenVars sv = (Sh608ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh608screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh608ScreenVars screenVars = (Sh608ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.daywh01.setClassString("");
		screenVars.daywh02.setClassString("");
		screenVars.daywh03.setClassString("");
		screenVars.daywh04.setClassString("");
		screenVars.daywh05.setClassString("");
		screenVars.daywh06.setClassString("");
		screenVars.daywh07.setClassString("");
		screenVars.daywh08.setClassString("");
		screenVars.daywh09.setClassString("");
		screenVars.daywh10.setClassString("");
		screenVars.daywh11.setClassString("");
		screenVars.daywh12.setClassString("");
	}

/**
 * Clear all the variables in Sh608screen
 */
	public static void clear(VarModel pv) {
		Sh608ScreenVars screenVars = (Sh608ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.daywh01.clear();
		screenVars.daywh02.clear();
		screenVars.daywh03.clear();
		screenVars.daywh04.clear();
		screenVars.daywh05.clear();
		screenVars.daywh06.clear();
		screenVars.daywh07.clear();
		screenVars.daywh08.clear();
		screenVars.daywh09.clear();
		screenVars.daywh10.clear();
		screenVars.daywh11.clear();
		screenVars.daywh12.clear();
	}
}
