/*
 * File: Ulstmt.java
 * Date: 30 August 2009 2:48:55
 * Author: Quipoz Limited
 * 
 * Class transformed from ULSTMT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*    This is the  unit linked unit statement triggering
* subroutine.
*    The calling program  will  pass  parameters in the copybook
* ANNPROCREC. All that is  required  is  to  create  a 'trigger'
* record on the unit  statement  file  (USTMPF). This is done by
* writing a new record  to  the  database using the logical view
* USTM with the following parameters:
*
*         - key from linkage area
*         - effective date from linkage area
*         - batch key from linkage area
*
*
* ========================================================
*        Asia Base Changes.
* ========================================================
*
*  This Subroutine now prints a Detail Unit Statement instead
*  of Summary Report as default
*
****************************************************************
* </pre>
*/
public class Ulstmt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected String wsaaSubr = "ULSTMT";
		/* FORMATS */
	protected String ustmrec = "USTMREC   ";
	protected String ustfrec = "USTFREC   ";
	protected Annprocrec annprocrec = new Annprocrec();
	protected Syserrrec syserrrec = new Syserrrec();
		/*Unit Statement Fund Header Details.*/
	protected UstfTableDAM ustfIO = new UstfTableDAM();
		/*Unit Statement Trigger Records no Statem*/
	protected UstmTableDAM ustmIO = new UstmTableDAM();
	protected Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit579, 
		exit589
	}

	public Ulstmt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		annprocrec.annpllRec = convertAndSetParam(annprocrec.annpllRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		para100();
		exit190();
	}

protected void para100()
	{
		syserrrec.subrname.set(wsaaSubr);
		annprocrec.statuz.set(varcom.oK);
		ustmIO.setParams(SPACES);
		ustfIO.setParams(SPACES);
		ustfIO.setDataArea(SPACES);
		ustfIO.setChdrcoy(annprocrec.company);
		ustfIO.setChdrnum(annprocrec.chdrnum);
		ustfIO.setUstmno(99999);
		ustfIO.setPlanSuffix(ZERO);
		ustfIO.setLife("01");
		ustfIO.setCoverage("01");
		ustfIO.setRider("00");
		ustfIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ustfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ustfIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
        
		ustfIO.setFormat(ustfrec);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)
		&& isNE(ustfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ustfIO.getParams());
			databaseError580();
		}
		if (isEQ(ustfIO.getStatuz(),varcom.endp)
		|| isNE(ustfIO.getChdrcoy(),annprocrec.company)
		|| isNE(ustfIO.getChdrnum(),annprocrec.chdrnum)) {
			ustfIO.setUstmno(ZERO);
			ustfIO.setStmtClDate(ZERO);
		}
		ustmIO.setUstmno(ustfIO.getUstmno());
		ustmIO.setStrpdate(ustfIO.getStmtClDate());
		ustmIO.setChdrcoy(annprocrec.company);
		ustmIO.setChdrnum(annprocrec.chdrnum);
		ustmIO.setEffdate(annprocrec.effdate);
		ustmIO.setBatctrcde(annprocrec.batctrcde);
		ustmIO.setTranno(0);
		ustmIO.setJobnoStmt(0);
		ustmIO.setBatcactyr(annprocrec.batcactyr);
		ustmIO.setBatcactmn(annprocrec.batcactmn);
		ustmIO.setUnitStmtFlag("D");
		ustmIO.setStmtType("P");
		ustmIO.setFormat(ustmrec);
		ustmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ustmIO);
		if (isNE(ustmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ustmIO.getStatuz());
			syserrrec.params.set(ustmIO.getParams());
			databaseError580();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void stop199()
	{
		stopRun();
	}

protected void systemError570()
	{
		try {
			para570();
		}
		catch (GOTOException e){
		}
		finally{
			exit579();
		}
	}

protected void para570()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit579);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		annprocrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError580()
	{
		try {
			para580();
		}
		catch (GOTOException e){
		}
		finally{
			exit589();
		}
	}

protected void para580()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit589);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		annprocrec.statuz.set(varcom.bomb);
		exit190();
	}
}
