package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UstmpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:51
 * Class transformed from USTMPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UstmpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 122;
	public FixedLengthStringData ustmrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ustmpfRecord = ustmrec;
	
	public FixedLengthStringData chdrpfx = DD.chdrpfx.copy().isAPartOf(ustmrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ustmrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ustmrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ustmrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(ustmrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ustmrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ustmrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(ustmrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ustmrec);
	public FixedLengthStringData batcpfx = DD.batcpfx.copy().isAPartOf(ustmrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(ustmrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(ustmrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(ustmrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(ustmrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(ustmrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(ustmrec);
	public PackedDecimalData strpdate = DD.strpdate.copy().isAPartOf(ustmrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(ustmrec);
	public PackedDecimalData jobnoStmt = DD.jctljnumst.copy().isAPartOf(ustmrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(ustmrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(ustmrec);
	public FixedLengthStringData clntpfx = DD.clntpfx.copy().isAPartOf(ustmrec);
	public FixedLengthStringData clntcoy = DD.clntcoy.copy().isAPartOf(ustmrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(ustmrec);
	public FixedLengthStringData stmtType = DD.ustmtyp.copy().isAPartOf(ustmrec);
	public FixedLengthStringData unitStmtFlag = DD.ustmtflag.copy().isAPartOf(ustmrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ustmrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ustmrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ustmrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UstmpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UstmpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UstmpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UstmpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UstmpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UstmpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UstmpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("USTMPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRPFX, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"EFFDATE, " +
							"TRANNO, " +
							"BATCPFX, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"STRPDATE, " +
							"USTMNO, " +
							"JCTLJNUMST, " +
							"VALIDFLAG, " +
							"CNTCURR, " +
							"CLNTPFX, " +
							"CLNTCOY, " +
							"CLNTNUM, " +
							"USTMTYP, " +
							"USTMTFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrpfx,
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     effdate,
                                     tranno,
                                     batcpfx,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     strpdate,
                                     ustmno,
                                     jobnoStmt,
                                     validflag,
                                     cntcurr,
                                     clntpfx,
                                     clntcoy,
                                     clntnum,
                                     stmtType,
                                     unitStmtFlag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrpfx.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		effdate.clear();
  		tranno.clear();
  		batcpfx.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		strpdate.clear();
  		ustmno.clear();
  		jobnoStmt.clear();
  		validflag.clear();
  		cntcurr.clear();
  		clntpfx.clear();
  		clntcoy.clear();
  		clntnum.clear();
  		stmtType.clear();
  		unitStmtFlag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUstmrec() {
  		return ustmrec;
	}

	public FixedLengthStringData getUstmpfRecord() {
  		return ustmpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUstmrec(what);
	}

	public void setUstmrec(Object what) {
  		this.ustmrec.set(what);
	}

	public void setUstmpfRecord(Object what) {
  		this.ustmpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ustmrec.getLength());
		result.set(ustmrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}