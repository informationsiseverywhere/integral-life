/*
 * File: Lbonrev.java
 * Date: 06 March 2018
 * Author: eparachuru
 * 
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.List;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.dao.LbonpfDAO;
import com.csc.life.enquiries.dataaccess.model.Lbonpf;
import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      -----------------------------------
*           LOYALTY BONUS REVERSAL
*      -----------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  The routine is used to reverse a loyalty bonusr.
*
*  All UTRNs and ACMVs that were posted in the forward transaction are
*  reversed.
*  The details of which contract to process originate from
*  the Policy Transaction File (PTRN) and are passed through
*  in the linkage.
*
*  The routine is driven by :
*    - The Company to which this Contract belongs.
*    - The Contract Header Number.
*    - The Transaction Number used in the original
*      reversal transaction.
*
*  PROCESSING
*  ----------
*
*  - Get Today's date.
*
*  - Read the Contract Header details.
*
*  - Get the Transaction Code description from T1688 using
*    Transaction Code passed in through Linkage.
*
*  - BEGN on the ACMV file. This will get to the first
*    accounting record which has this Transaction Number.
*
*  - Now find the first record under this number which has the
*    appropriate Transaction Code (the code of the original
*    surrender transaction).
*
*  MAIN PROCESSING LOOP.
*  --------------------
*
*  PROCESS UNTIL ACMV-STATUZ = ENDP
*
*  - Post a new ACMV with :
*
*              ORIGAMT = previous value * -1.
*              TRANSACTION-CODE = Tran Code of the Reversing
*                                 transaction (passed in through
*                                 Linkage).
*              TRANDESC = Description of the reversing Tran Code.
*              TRANSACTION NUMBER = Previous Contract Header
*                                   Tran Number + 1.
*
*  - Find the next ACMV to process by reading the file
*    sequentially until a record of the same transaction code
*    is read.
*
*    If a new Company, Contract Header Number or Transaction
*    Number is encountered, move ENDP to ACMV-STATUZ and
*    effectively exit the processing loop.
*
*  END OF MAIN PROCESSING LOOP.
*
*****************************************************************
* </pre>
*/
public class Lbonrev extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();
 
	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private FixedLengthStringData wsaaAcmvFlag = new FixedLengthStringData(1);
	private Validator nextAcmvFound = new Validator(wsaaAcmvFlag, "Y");
	private Validator nextAcmvNotFound = new Validator(wsaaAcmvFlag, "N");
		private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Rnlallrec rnlallrec = new Rnlallrec();
	private LbonpfDAO lbonpfDAO = getApplicationContext().getBean("lbonpfDAO", LbonpfDAO.class);
	private final String wsaaSubr = "LBONREV";
	private AcmvpfDAO acmvpfDAO =  getApplicationContext().getBean("acmvDAO", AcmvpfDAO.class);
	private Acmvpf acmvpf = null;
	private static final String acmvrevrec = "ACMVREVREC";
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf = null;
	private Reverserec reverserec = new Reverserec();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30);
	private static final String arcmrec = "ARCMREC";
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		feedback4040, 
		nextHitr4050, 
		exit4090
	}
	private static final String hitrrevrec = "HITRREVREC";
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	public Lbonrev() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*START*/
		initialise1000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
		processAcmvs3000();
		processHitrs4000();
	}
 
protected void processAcmvs3000()
{
	start3010();
	processAcmvOptical3050();
}
protected void start3010()
{
	acmvrevIO.setParams(SPACES);
	acmvrevIO.setRldgcoy(reverserec.company);
	acmvrevIO.setRdocnum(reverserec.chdrnum);
	acmvrevIO.setTranno(reverserec.tranno);
 
	acmvrevIO.setFunction(varcom.begn);
	//performance improvement -- Anjali
	acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	acmvrevIO.setFormat(acmvrevrec);
	SmartFileCode.execute(appVars, acmvrevIO);
	if (isNE(acmvrevIO.getStatuz(),varcom.oK)
	&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(acmvrevIO.getParams());
		dbError580();
	}
	if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
	|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
	|| isNE(reverserec.tranno,acmvrevIO.getTranno())
	|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
		acmvrevIO.setStatuz(varcom.endp);
	}
	while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
		reverseAcmvs3100();
	}
	
}

protected void processAcmvOptical3050()
{
	/* Find the latest accounting period archived.*/
	arcmIO.setParams(SPACES);
	arcmIO.setFileName("ACMV");
	arcmIO.setFunction(varcom.readr);
	arcmIO.setFormat(arcmrec);
	SmartFileCode.execute(appVars, arcmIO);
	if (isNE(arcmIO.getStatuz(), varcom.oK)
	&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(arcmIO.getParams());
		dbError580();
			}
	if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
		wsbbPeriod.set(ZERO);
	}
	else {
		wsbbActyr.set(arcmIO.getAcctyr());
		wsbbActmn.set(arcmIO.getAcctmnth());
	}
	wsaaActyr.set(reverserec.ptrnBatcactyr);
	wsaaActmn.set(reverserec.ptrnBatcactmn);
	/* If the PTRN being processed has an Accounting Period greater*/
	/* than the last period archived then there is no need to attempt*/
	/* to read the Optical Device.*/
	if (isGT(wsaaPeriod, wsbbPeriod)) {
		return ;
	}
	/* If PTRN Accounting Period <= ARCM Accouting Period, call*/
	/* COLDPOINT to determine if ACMVs on Optical are to be*/
	/* reversed.*/
	acmvrevIO.setParams(SPACES);
	acmvrevIO.setRldgcoy(reverserec.company);
	acmvrevIO.setRdocnum(reverserec.chdrnum);
	acmvrevIO.setTranno(reverserec.tranno);
	acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
	acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
	acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
	 
	acmvrevIO.setFunction(varcom.begn);
	acmvrevIO.setFormat(acmvrevrec);
	FixedLengthStringData groupTEMP = acmvrevIO.getParams();
	callProgram(Acmvrevcp.class, groupTEMP);
	acmvrevIO.setParams(groupTEMP);
	if (isNE(acmvrevIO.getStatuz(), varcom.oK)
	&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(acmvrevIO.getParams());
		dbError580();
			}
	if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
	|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
	|| isNE(reverserec.tranno, acmvrevIO.getTranno())
	|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
		acmvrevIO.setStatuz(varcom.endp);
			}
	while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
		reverseAcmvs3100();
			}
	
	/* If the next policy is found then we must call the COLD API*/
	/* again with a function of CLOSE.*/
	if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
	|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
	|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
		acmvrevIO.setFunction("CLOSE");
		FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP2);
		acmvrevIO.setParams(groupTEMP2);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			dbError580();
		}
		}
	}

protected void reverseAcmvs3100()
{
	start3110();
	readNextAcmv3180();
}

protected void start3110()
{
	/* Check If ACMV just read has same trancode as transaction*/
	/* we are trying to reverse*/
	if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
		return ;
	}
	/* Post equal & opposite amount to ACMV just read*/
	lifacmvrec.lifacmvRec.set(SPACES);
	lifacmvrec.rcamt.set(ZERO);
	lifacmvrec.contot.set(ZERO);
	lifacmvrec.frcdate.set(ZERO);
	lifacmvrec.transactionDate.set(ZERO);
	lifacmvrec.transactionTime.set(ZERO);
	lifacmvrec.user.set(ZERO);
	lifacmvrec.function.set("PSTW");
	lifacmvrec.batckey.set(reverserec.batchkey);
	lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
	lifacmvrec.tranno.set(reverserec.newTranno);
	lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
	lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
	lifacmvrec.glcode.set(acmvrevIO.getGlcode());
	lifacmvrec.glsign.set(acmvrevIO.getGlsign());
	lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
	lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
	lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
	lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
	lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
	compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
	compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
	lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
	lifacmvrec.crate.set(acmvrevIO.getCrate());
	lifacmvrec.postyear.set(SPACES);
	lifacmvrec.postmonth.set(SPACES);
	lifacmvrec.tranref.set(acmvrevIO.getTranref());
	lifacmvrec.trandesc.set(wsaaTransDesc);
	lifacmvrec.effdate.set(datcon1rec.intDate);
	lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec.termid.set(reverserec.termid);
	lifacmvrec.user.set(reverserec.user);
	lifacmvrec.transactionTime.set(reverserec.transTime);
	lifacmvrec.transactionDate.set(reverserec.transDate);
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz,varcom.oK)) {
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec.statuz);
		syserr570();
	}
}

protected void readNextAcmv3180()
{
	/*  Read the next ACMV record.*/
	acmvrevIO.setFunction(varcom.nextr);
	 
		SmartFileCode.execute(appVars, acmvrevIO);
	 
	if (isNE(acmvrevIO.getStatuz(),varcom.oK)
	&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(acmvrevIO.getParams());
		dbError580();
	}
	if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
	|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
	|| isNE(reverserec.tranno,acmvrevIO.getTranno())
	|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
		acmvrevIO.setStatuz(varcom.endp);
	}
	/*EXIT*/
}

protected void processHitrs4000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				hitr4010();
			case feedback4040: 
				feedback4040();
			case nextHitr4050: 
				nextHitr4050();
			case exit4090: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void hitr4010()
{
	hitrrevIO.setParams(SPACES);
	hitrrevIO.setChdrcoy(reverserec.company);
	hitrrevIO.setChdrnum(reverserec.chdrnum);
	hitrrevIO.setTranno(reverserec.tranno);
	hitrrevIO.setFormat(hitrrevrec);
	hitrrevIO.setFunction(varcom.begn);
	//performance improvement -- Anjali
	hitrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	hitrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
	SmartFileCode.execute(appVars, hitrrevIO);
	if (isNE(hitrrevIO.getStatuz(),varcom.oK)
	&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(hitrrevIO.getParams());
		dbError580();
	}
	if (isNE(hitrrevIO.getChdrcoy(),reverserec.company)
	|| isNE(hitrrevIO.getChdrnum(),reverserec.chdrnum)
	|| isNE(hitrrevIO.getTranno(),reverserec.tranno)
	|| isEQ(hitrrevIO.getStatuz(),varcom.endp)) {
		goTo(GotoLabel.exit4090);
	}
}

protected void feedback4040()
{
	if (isEQ(hitrrevIO.getFeedbackInd(),SPACES)) {
		deleteHitr4100();
		goTo(GotoLabel.nextHitr4050);
	}
	if (isEQ(hitrrevIO.getFeedbackInd(),"Y")) {
		createHitr4200();
	}
}

protected void nextHitr4050()
{
	hitrrevIO.setFunction(varcom.nextr);
	SmartFileCode.execute(appVars, hitrrevIO);
	if (isNE(hitrrevIO.getStatuz(),varcom.oK)
	&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(hitrrevIO.getParams());
		dbError580();
	}
	if (isNE(hitrrevIO.getChdrcoy(),reverserec.company)
	|| isNE(hitrrevIO.getChdrnum(),reverserec.chdrnum)
	|| isNE(hitrrevIO.getTranno(),reverserec.tranno)
	|| isEQ(hitrrevIO.getStatuz(),varcom.endp)) {
		return ;
	}
	goTo(GotoLabel.feedback4040);
}

protected void deleteHitr4100()
{
	/*DELETE*/
	hitrrevIO.setFormat(hitrrevrec);
	hitrrevIO.setFunction(varcom.deltd);
	SmartFileCode.execute(appVars, hitrrevIO);
	if (isNE(hitrrevIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(hitrrevIO.getParams());
		dbError580();
	}
	/*EXIT*/
}

protected void createHitr4200()
{
	format4210();
}

protected void format4210()
{
	hitrrevIO.setTranno(reverserec.newTranno);
	setPrecision(hitrrevIO.getContractAmount(), 2);
	hitrrevIO.setContractAmount(mult(hitrrevIO.getContractAmount(),-1));
	setPrecision(hitrrevIO.getFundAmount(), 2);
	hitrrevIO.setFundAmount(mult(hitrrevIO.getFundAmount(),-1));
	setPrecision(hitrrevIO.getProcSeqNo(), 0);
	hitrrevIO.setProcSeqNo(mult(hitrrevIO.getProcSeqNo(),-1));
	hitrrevIO.setFormat(hitrrevrec);
	hitrrevIO.setSurrenderPercent(ZERO);
	hitrrevIO.setFeedbackInd(SPACES);
	hitrrevIO.setSwitchIndicator(SPACES);
	hitrrevIO.setTriggerModule(SPACES);
	hitrrevIO.setTriggerKey(SPACES);
	hitrrevIO.setBatctrcde(reverserec.batctrcde);
	/* MOVE SPACE                  TO HITRREV-ZINTAPPIND.           */
	hitrrevIO.setFunction(varcom.writr);
	hitrrevIO.setUstmno(ZERO);
	SmartFileCode.execute(appVars, hitrrevIO);
	if (isNE(hitrrevIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(hitrrevIO.getParams());
		dbError580();
	}
}


protected void start1000()
	{
	reverserec.statuz.set(varcom.oK);
		Lbonpf loyBonus = lbonpfDAO.getLbonpfRecord(reverserec.chdrnum.toString(), reverserec.tranno.toInt(), reverserec.company.toString());
		
		if (loyBonus!=null)
		{
			  
				loyBonus.setRevflag("1");			
				BigDecimal amt;
				amt=loyBonus.getUalprc();
				loyBonus.setUalprc(amt);
				lbonpfDAO.updateLbonpfRecord(loyBonus);
			 
		}
}
 

protected void fatalError9000()
	{
					fatalErrors9000();
					errorProg9000();
				}

protected void fatalErrors9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9000()
	{
	reverserec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void syserr570()
{
	/*PARA*/
	syserrrec.subrname.set(wsaaSubr);
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT*/
	reverserec.statuz.set(varcom.bomb);
	exitProgram();
}

protected void dbError580()
{
	/*PARA*/
	syserrrec.subrname.set(wsaaSubr);
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("1");
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT*/
	reverserec.statuz.set(varcom.bomb);
	exitProgram();
}
}
