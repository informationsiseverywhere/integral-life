package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UtrspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:55
 * Class transformed from UTRSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UtrspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 42;
	public FixedLengthStringData utrsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData utrspfRecord = utrsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(utrsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(utrsrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(utrsrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(utrsrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(utrsrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(utrsrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(utrsrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(utrsrec);
	public PackedDecimalData currentUnitBal = DD.curuntbal.copy().isAPartOf(utrsrec);
	public PackedDecimalData currentDunitBal = DD.curduntbal.copy().isAPartOf(utrsrec);
	public FixedLengthStringData fundPool = DD.fundpool.copy().isAPartOf(utrsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UtrspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UtrspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UtrspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UtrspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UtrspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UtrspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UtrspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UTRSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"VRTFND, " +
							"PLNSFX, " +
							"UNITYP, " +
							"CURUNTBAL, " +
							"CURDUNTBAL, " +
							"UNIQUE_NUMBER "+
							"FUNDPOOL";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     unitVirtualFund,
                                     planSuffix,
                                     unitType,
                                     currentUnitBal,
                                     currentDunitBal,
                                     unique_number,
                                     fundPool
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		unitVirtualFund.clear();
  		planSuffix.clear();
  		unitType.clear();
  		currentUnitBal.clear();
  		currentDunitBal.clear();
  		fundPool.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUtrsrec() {
  		return utrsrec;
	}

	public FixedLengthStringData getUtrspfRecord() {
  		return utrspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUtrsrec(what);
	}

	public void setUtrsrec(Object what) {
  		this.utrsrec.set(what);
	}

	public void setUtrspfRecord(Object what) {
  		this.utrspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(utrsrec.getLength());
		result.set(utrsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}