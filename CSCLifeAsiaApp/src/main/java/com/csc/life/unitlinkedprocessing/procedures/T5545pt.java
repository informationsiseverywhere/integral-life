/*
 * File: T5545pt.java
 * Date: 30 August 2009 2:22:32
 * Author: Quipoz Limited
 * 
 * Class transformed from T5545PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5545.
*
*
*****************************************************************
* </pre>
*/
public class T5545pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Enhanced Unit Allocation                  S5545");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 25, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler9 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Freq      Prem     %       Freq      Prem      %      Freq      Prem      %");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(69);
	private FixedLengthStringData filler10 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(58).isAPartOf(wsaaPrtLine005, 11, FILLER).init("From                       From                       From");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(57);
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 1);
	private FixedLengthStringData filler13 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine006, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 28);
	private FixedLengthStringData filler14 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 55);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(80);
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(80);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(80);
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(80);
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(80);
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(57);
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 1);
	private FixedLengthStringData filler46 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine012, 3, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 28);
	private FixedLengthStringData filler47 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 55);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(80);
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(80);
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(80);
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(80);
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(80);
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine017, 1).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 20).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine017, 28).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 47).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine017, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 73, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 74).setPattern("ZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5545rec t5545rec = new T5545rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5545pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5545rec.t5545Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5545rec.billfreq01);
		fieldNo010.set(t5545rec.enhanaPrm01);
		fieldNo011.set(t5545rec.enhanaPc01);
		fieldNo016.set(t5545rec.enhanaPrm02);
		fieldNo017.set(t5545rec.enhanaPc02);
		fieldNo022.set(t5545rec.enhanaPrm03);
		fieldNo023.set(t5545rec.enhanaPc03);
		fieldNo028.set(t5545rec.enhanaPrm04);
		fieldNo029.set(t5545rec.enhanaPc04);
		fieldNo034.set(t5545rec.enhanaPrm05);
		fieldNo035.set(t5545rec.enhanaPc05);
		fieldNo008.set(t5545rec.billfreq02);
		fieldNo012.set(t5545rec.enhanbPrm01);
		fieldNo013.set(t5545rec.enhanbPc01);
		fieldNo018.set(t5545rec.enhanbPrm02);
		fieldNo019.set(t5545rec.enhanbPc02);
		fieldNo024.set(t5545rec.enhanbPrm03);
		fieldNo025.set(t5545rec.enhanbPc03);
		fieldNo030.set(t5545rec.enhanbPrm04);
		fieldNo031.set(t5545rec.enhanbPc04);
		fieldNo036.set(t5545rec.enhanbPrm05);
		fieldNo037.set(t5545rec.enhanbPc05);
		fieldNo009.set(t5545rec.billfreq03);
		fieldNo014.set(t5545rec.enhancPrm01);
		fieldNo015.set(t5545rec.enhancPc01);
		fieldNo020.set(t5545rec.enhancPrm02);
		fieldNo021.set(t5545rec.enhancPc02);
		fieldNo026.set(t5545rec.enhancPrm03);
		fieldNo027.set(t5545rec.enhancPc03);
		fieldNo032.set(t5545rec.enhancPrm04);
		fieldNo033.set(t5545rec.enhancPc04);
		fieldNo038.set(t5545rec.enhancPrm05);
		fieldNo039.set(t5545rec.enhancPc05);
		fieldNo040.set(t5545rec.billfreq04);
		fieldNo043.set(t5545rec.enhandPrm01);
		fieldNo044.set(t5545rec.enhandPc01);
		fieldNo049.set(t5545rec.enhandPrm02);
		fieldNo050.set(t5545rec.enhandPc02);
		fieldNo055.set(t5545rec.enhandPrm03);
		fieldNo056.set(t5545rec.enhandPc03);
		fieldNo061.set(t5545rec.enhandPrm04);
		fieldNo062.set(t5545rec.enhandPc04);
		fieldNo067.set(t5545rec.enhandPrm05);
		fieldNo068.set(t5545rec.enhandPc05);
		fieldNo041.set(t5545rec.billfreq05);
		fieldNo045.set(t5545rec.enhanePrm01);
		fieldNo046.set(t5545rec.enhanePc01);
		fieldNo051.set(t5545rec.enhanePrm02);
		fieldNo052.set(t5545rec.enhanePc02);
		fieldNo057.set(t5545rec.enhanePrm03);
		fieldNo058.set(t5545rec.enhanePc03);
		fieldNo063.set(t5545rec.enhanePrm04);
		fieldNo064.set(t5545rec.enhanePc04);
		fieldNo069.set(t5545rec.enhanePrm05);
		fieldNo070.set(t5545rec.enhanePc05);
		fieldNo042.set(t5545rec.billfreq06);
		fieldNo047.set(t5545rec.enhanfPrm01);
		fieldNo048.set(t5545rec.enhanfPc01);
		fieldNo053.set(t5545rec.enhanfPrm02);
		fieldNo054.set(t5545rec.enhanfPc02);
		fieldNo059.set(t5545rec.enhanfPrm03);
		fieldNo060.set(t5545rec.enhanfPc03);
		fieldNo065.set(t5545rec.enhanfPrm04);
		fieldNo066.set(t5545rec.enhanfPc04);
		fieldNo071.set(t5545rec.enhanfPrm05);
		fieldNo072.set(t5545rec.enhanfPc05);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
