/*
 * File: Bidoff.java
 * Date: 29 August 2009 21:39:48
 * Author: Quipoz Limited
 * 
 * Class transformed from BIDOFF.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.newbusiness.recordstructures.Bidoffrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*AUTHOR.            PAXUS FINANCIAL SYSTEMS PTY. LTD.
*REMARKS.
*              CALCULATE BID/OFFER PRICES
*
*    This subroutine receives paramters from wherever it is
* called, and calculates the bid/offer prices accordingly.
*
* The subroutine accepts the fund, bare price, initial or
* accumulation type of units.
*            - Fund,
*            - Bare Price,
*            - Unit Type
*
* It outputs:
*            - Statuz,
*            - Bid Price,
*            - Offer Price
*
* LINKAGE AREA:
*       Function             PIC X(05)
*       Statuz               PIC X(04)
*       Unit type            PIC X(01)
*       Fund                 PIC X(04)
*       Bare Price           PIC S9(04)V9(05)
*       Bid Price            PIC S9(04)V9(05)
*       Offer Price          PIC S9(04)V9(05)
*
*
*****************************************************
* </pre>
*/
public class Bidoff extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(6).init("BIDOFF");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaAcumrnd = new ZonedDecimalData(3, 2).setUnsigned();
	private ZonedDecimalData wsaaInitrnd = new ZonedDecimalData(3, 2).setUnsigned();
	private ZonedDecimalData wsaaOut = new ZonedDecimalData(9, 5);
	private ZonedDecimalData wsaaAcumPerc = new ZonedDecimalData(8, 5);
	private ZonedDecimalData wsaaInitPerc = new ZonedDecimalData(8, 5);
	private ZonedDecimalData wsaaAcumOffer = new ZonedDecimalData(9, 5);
	private ZonedDecimalData wsaaInitOffer = new ZonedDecimalData(9, 5);
	private ZonedDecimalData wsaaInitBid = new ZonedDecimalData(9, 5);
	private ZonedDecimalData wsaaAcumBid = new ZonedDecimalData(9, 5);
	private ZonedDecimalData wsaaBarePrice = new ZonedDecimalData(9, 5);
		/* TABLES */
	private String t5515 = "T5515";
	private Bidoffrec bidoffrec = new Bidoffrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2900, 
		exit3900, 
		acumOfferPrice6200, 
		initOfferPrice6300, 
		exit6900
	}

	public Bidoff() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bidoffrec.bidoffRec = convertAndSetParam(bidoffrec.bidoffRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAINLINE*/
		bidoffrec.statuz.set(varcom.oK);
		getBarePrice2000();
		/*EXIT-GETDIST*/
		exitProgram();
	}

protected void stopRun000()
	{
		stopRun();
	}

protected void getBarePrice2000()
	{
		try {
			para2000();
		}
		catch (GOTOException e){
		}
	}

protected void para2000()
	{
		wsaaAcumrnd.set(ZERO);
		wsaaInitrnd.set(ZERO);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemcoy(bidoffrec.company);
		itdmIO.setItemitem(bidoffrec.fund);
		itdmIO.setItmfrm(bidoffrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			bidoffrec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit2900);
		}
		if (isNE(itdmIO.getItemcoy(),bidoffrec.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),bidoffrec.fund)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			bidoffrec.statuz.set(varcom.nogo);
			goTo(GotoLabel.exit2900);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5515rec.initialRounding,ZERO)) {
			wsaaInitrnd.set(ZERO);
		}
		else {
			wsaaInitrnd.set(t5515rec.initialRounding);
		}
		if (isEQ(t5515rec.accumRounding,ZERO)) {
			wsaaAcumrnd.set(ZERO);
		}
		else {
			wsaaAcumrnd.set(t5515rec.accumRounding);
		}
		if (isEQ(bidoffrec.unitType,"A")) {
			accumulationDetails3000();
		}
		if (isEQ(bidoffrec.unitType,"I")) {
			initialDetails5000();
		}
		obtainOfferPrice6000();
	}

protected void accumulationDetails3000()
	{
		try {
			init3000();
			send3000();
		}
		catch (GOTOException e){
		}
	}

protected void init3000()
	{
		wsaaOut.set(ZERO);
		wsaaBarePrice.set(ZERO);
		wsaaAcumPerc.set(ZERO);
		wsaaAcumBid.set(ZERO);
	}

protected void send3000()
	{
		wsaaBarePrice.set(bidoffrec.barePrice);
		if (isEQ(t5515rec.acumbof,ZERO)) {
			wsaaAcumPerc.set(ZERO);
		}
		else {
			wsaaAcumPerc.set(t5515rec.acumbof);
		}
		compute(wsaaOut, 6).setRounded(mult(wsaaBarePrice,wsaaAcumrnd));
		bidoffrec.bidPrice.set(wsaaOut);
		wsaaAcumBid.set(wsaaOut);
		goTo(GotoLabel.exit3900);
	}

protected void initialDetails5000()
	{
		init5000();
		send5000();
	}

protected void init5000()
	{
		wsaaOut.set(ZERO);
		wsaaBarePrice.set(ZERO);
		wsaaInitPerc.set(ZERO);
		wsaaInitBid.set(ZERO);
	}

protected void send5000()
	{
		wsaaBarePrice.set(bidoffrec.barePrice);
		if (isEQ(t5515rec.initBidOffer,ZERO)) {
			wsaaInitPerc.set(ZERO);
		}
		else {
			wsaaInitPerc.set(t5515rec.initBidOffer);
		}
		compute(wsaaOut, 6).setRounded(mult(wsaaBarePrice,wsaaInitrnd));
		bidoffrec.bidPrice.set(wsaaOut);
		wsaaInitBid.set(wsaaOut);
		/*EXIT*/
	}

protected void obtainOfferPrice6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para6000();
				}
				case acumOfferPrice6200: {
					acumOfferPrice6200();
				}
				case initOfferPrice6300: {
					initOfferPrice6300();
				}
				case exit6900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6000()
	{
		wsaaAcumOffer.set(ZERO);
		wsaaInitOffer.set(ZERO);
		if (isEQ(bidoffrec.unitType,"A")) {
			goTo(GotoLabel.acumOfferPrice6200);
		}
		if (isEQ(bidoffrec.unitType,"I")) {
			goTo(GotoLabel.initOfferPrice6300);
		}
	}

protected void acumOfferPrice6200()
	{
		if (isNE(wsaaAcumBid,ZERO)) {
			compute(wsaaAcumOffer, 6).setRounded(div((mult(wsaaAcumBid,100)),(sub(100,wsaaAcumPerc))));
		}
		bidoffrec.offerPrice.set(wsaaAcumOffer);
		goTo(GotoLabel.exit6900);
	}

protected void initOfferPrice6300()
	{
		if (isNE(wsaaInitBid,ZERO)) {
			compute(wsaaInitOffer, 6).setRounded(div((mult(wsaaInitBid,100)),(sub(100,wsaaInitPerc))));
		}
		bidoffrec.offerPrice.set(wsaaInitOffer);
	}
}
