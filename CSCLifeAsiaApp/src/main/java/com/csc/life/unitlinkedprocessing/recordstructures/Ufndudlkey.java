package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:23
 * Description:
 * Copybook name: UFNDUDLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ufndudlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ufndudlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData ufndudlKey = new FixedLengthStringData(256).isAPartOf(ufndudlFileKey, 0, REDEFINE);
  	public FixedLengthStringData ufndudlCompany = new FixedLengthStringData(1).isAPartOf(ufndudlKey, 0);
  	public FixedLengthStringData ufndudlVirtualFund = new FixedLengthStringData(4).isAPartOf(ufndudlKey, 1);
  	public FixedLengthStringData ufndudlUnitType = new FixedLengthStringData(1).isAPartOf(ufndudlKey, 5);
  	public FixedLengthStringData filler = new FixedLengthStringData(250).isAPartOf(ufndudlKey, 6, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ufndudlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ufndudlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}