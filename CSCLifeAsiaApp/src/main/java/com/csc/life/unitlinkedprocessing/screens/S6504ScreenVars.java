package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6504
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6504ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(180);
	public FixedLengthStringData dataFields = new FixedLengthStringData(52).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,6);
	public FixedLengthStringData jobname = DD.jobname.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData runid = DD.runid.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData runlib = DD.runlib.copy().isAPartOf(dataFields,34);
	public ZonedDecimalData stmEffdte = DD.stmeffd.copyToZonedDecimal().isAPartOf(dataFields,44);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 52);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jobnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData runidErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData runlibErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData stmeffdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 84);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] jobnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] runidOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] runlibOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] stmeffdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData stmEffdteDisp = new FixedLengthStringData(10);

	public LongData S6504screenWritten = new LongData(0);
	public LongData S6504protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6504ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(stmeffdOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, acctyear, jobname, runid, jobq, runlib, stmEffdte};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, acctyearOut, jobnameOut, runidOut, jobqOut, runlibOut, stmeffdOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, acctyearErr, jobnameErr, runidErr, jobqErr, runlibErr, stmeffdErr};
		screenDateFields = new BaseData[] {effdate, stmEffdte};
		screenDateErrFields = new BaseData[] {effdateErr, stmeffdErr};
		screenDateDispFields = new BaseData[] {effdateDisp, stmEffdteDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6504screen.class;
		protectRecord = S6504protect.class;
	}

}
