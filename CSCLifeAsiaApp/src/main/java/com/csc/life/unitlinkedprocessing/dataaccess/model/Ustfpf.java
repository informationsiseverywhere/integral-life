package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Ustfpf {
	private String chdrpfx;
	private String chdrcoy;// 1
	private String chdrnum;
	private int plnsfx;
	private String life;
	private String coverage;
	private String rider;
	private int ustmno;
	private String ustmtflag;
	private String vrtfnd;
	private String unityp;
	private int stopdt;
	private BigDecimal stopun;
	private BigDecimal stopdun;
	private BigDecimal stofuca;
	private BigDecimal stopcoca;
	private long stcldt;
	private BigDecimal stclun;
	private BigDecimal stcldun;
	private int strpdate;
	private BigDecimal stclfuca;
	private BigDecimal stclcoca;
	private BigDecimal sttrun;
	private BigDecimal sttrdun;
	private BigDecimal sttrfuca;
	private BigDecimal sttrcoca;
	private long unique_number;

	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getUstmno() {
		return ustmno;
	}
	public void setUstmno(int ustmno) {
		this.ustmno = ustmno;
	}
	public String getUstmtflag() {
		return ustmtflag;
	}
	public void setUstmtflag(String ustmtflag) {
		this.ustmtflag = ustmtflag;
	}
	public String getVrtfnd() {
		return vrtfnd;
	}
	public void setVrtfnd(String vrtfnd) {
		this.vrtfnd = vrtfnd;
	}
	public String getUnityp() {
		return unityp;
	}
	public void setUnityp(String unityp) {
		this.unityp = unityp;
	}
	public int getStopdt() {
		return stopdt;
	}
	public void setStopdt(int stopdt) {
		this.stopdt = stopdt;
	}
	public BigDecimal getStopun() {
		return stopun;
	}
	public void setStopun(BigDecimal stopun) {
		this.stopun = stopun;
	}
	public BigDecimal getStopdun() {
		return stopdun;
	}
	public void setStopdun(BigDecimal stopdun) {
		this.stopdun = stopdun;
	}
	public BigDecimal getStofuca() {
		return stofuca;
	}
	public void setStofuca(BigDecimal stofuca) {
		this.stofuca = stofuca;
	}
	public BigDecimal getStopcoca() {
		return stopcoca;
	}
	public void setStopcoca(BigDecimal stopcoca) {
		this.stopcoca = stopcoca;
	}
	public long getStcldt() {
		return stcldt;
	}
	public void setStcldt(long stcldt) {
		this.stcldt = stcldt;
	}
	public BigDecimal getStclun() {
		return stclun;
	}
	public void setStclun(BigDecimal stclun) {
		this.stclun = stclun;
	}
	public BigDecimal getStcldun() {
		return stcldun;
	}
	public void setStcldun(BigDecimal stcldun) {
		this.stcldun = stcldun;
	}
	public int getStrpdate() {
		return strpdate;
	}
	public void setStrpdate(int strpdate) {
		this.strpdate = strpdate;
	}
	public BigDecimal getStclfuca() {
		return stclfuca;
	}
	public void setStclfuca(BigDecimal stclfuca) {
		this.stclfuca = stclfuca;
	}
	public BigDecimal getStclcoca() {
		return stclcoca;
	}
	public void setStclcoca(BigDecimal stclcoca) {
		this.stclcoca = stclcoca;
	}
	public BigDecimal getSttrun() {
		return sttrun;
	}
	public void setSttrun(BigDecimal sttrun) {
		this.sttrun = sttrun;
	}
	public BigDecimal getSttrdun() {
		return sttrdun;
	}
	public void setSttrdun(BigDecimal sttrdun) {
		this.sttrdun = sttrdun;
	}
	public BigDecimal getSttrfuca() {
		return sttrfuca;
	}
	public void setSttrfuca(BigDecimal sttrfuca) {
		this.sttrfuca = sttrfuca;
	}
	public BigDecimal getSttrcoca() {
		return sttrcoca;
	}
	public void setSttrcoca(BigDecimal sttrcoca) {
		this.sttrcoca = sttrcoca;
	}
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
}