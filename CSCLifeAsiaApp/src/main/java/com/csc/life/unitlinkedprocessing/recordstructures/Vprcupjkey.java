package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:07
 * Description:
 * Copybook name: VPRCUPJKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprcupjkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprcupjFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprcupjKey = new FixedLengthStringData(256).isAPartOf(vprcupjFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprcupjCompany = new FixedLengthStringData(1).isAPartOf(vprcupjKey, 0);
  	public PackedDecimalData vprcupjEffdate = new PackedDecimalData(8, 0).isAPartOf(vprcupjKey, 1);
  	public PackedDecimalData vprcupjJobno = new PackedDecimalData(8, 0).isAPartOf(vprcupjKey, 6);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(vprcupjKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprcupjFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprcupjFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}