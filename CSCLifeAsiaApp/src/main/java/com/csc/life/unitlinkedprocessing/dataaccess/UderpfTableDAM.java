package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UderpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:39
 * Class transformed from UDERPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UderpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 132;
	public FixedLengthStringData uderrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData uderpfRecord = uderrec;
	
	public PackedDecimalData age = DD.age.copy().isAPartOf(uderrec);
	public FixedLengthStringData recordtype = DD.recordtype.copy().isAPartOf(uderrec);
	public FixedLengthStringData lapind = DD.lapind.copy().isAPartOf(uderrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(uderrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(uderrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(uderrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(uderrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(uderrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(uderrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(uderrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(uderrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(uderrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(uderrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(uderrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(uderrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(uderrec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(uderrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(uderrec);
	public FixedLengthStringData triggerModule = DD.triger.copy().isAPartOf(uderrec);
	public PackedDecimalData relativeRecordNo = DD.relrecno.copy().isAPartOf(uderrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(uderrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(uderrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(uderrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UderpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UderpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UderpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UderpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UderpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UderpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UderpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UDERPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGE, " +
							"RECORDTYPE, " +
							"LAPIND, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VRTFND, " +
							"UNITYP, " +
							"TRANNO, " +
							"BATCTRCDE, " +
							"CNTAMNT, " +
							"CNTCURR, " +
							"FUNDAMNT, " +
							"FNDCURR, " +
							"FUNDRATE, " +
							"TRIGER, " +
							"RELRECNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     age,
                                     recordtype,
                                     lapind,
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     unitVirtualFund,
                                     unitType,
                                     tranno,
                                     batctrcde,
                                     contractAmount,
                                     cntcurr,
                                     fundAmount,
                                     fundCurrency,
                                     fundRate,
                                     triggerModule,
                                     relativeRecordNo,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		age.clear();
  		recordtype.clear();
  		lapind.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		unitVirtualFund.clear();
  		unitType.clear();
  		tranno.clear();
  		batctrcde.clear();
  		contractAmount.clear();
  		cntcurr.clear();
  		fundAmount.clear();
  		fundCurrency.clear();
  		fundRate.clear();
  		triggerModule.clear();
  		relativeRecordNo.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUderrec() {
  		return uderrec;
	}

	public FixedLengthStringData getUderpfRecord() {
  		return uderpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUderrec(what);
	}

	public void setUderrec(Object what) {
  		this.uderrec.set(what);
	}

	public void setUderpfRecord(Object what) {
  		this.uderpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(uderrec.getLength());
		result.set(uderrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}