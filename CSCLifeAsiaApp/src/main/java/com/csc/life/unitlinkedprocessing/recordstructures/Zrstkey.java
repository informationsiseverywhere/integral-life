package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:22
 * Description:
 * Copybook name: ZRSTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrstkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrstFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrstKey = new FixedLengthStringData(64).isAPartOf(zrstFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrstChdrcoy = new FixedLengthStringData(1).isAPartOf(zrstKey, 0);
  	public FixedLengthStringData zrstChdrnum = new FixedLengthStringData(8).isAPartOf(zrstKey, 1);
  	public FixedLengthStringData zrstLife = new FixedLengthStringData(2).isAPartOf(zrstKey, 9);
  	public FixedLengthStringData zrstCoverage = new FixedLengthStringData(2).isAPartOf(zrstKey, 11);
  	public FixedLengthStringData zrstRider = new FixedLengthStringData(2).isAPartOf(zrstKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(zrstKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrstFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrstFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}