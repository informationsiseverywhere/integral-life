package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:51
 * Description:
 * Copybook name: UTRNUTMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnutmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnutmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnutmKey = new FixedLengthStringData(64).isAPartOf(utrnutmFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnutmChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnutmKey, 0);
  	public FixedLengthStringData utrnutmChdrnum = new FixedLengthStringData(8).isAPartOf(utrnutmKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(utrnutmKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnutmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnutmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}