/*
 * File: P6517.java
 * Date: 30 August 2009 0:46:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P6517.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.unitlinkedprocessing.dataaccess.ChdrulsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstmTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S6517ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            UNIT STATEMENT REPRINT REQUEST
*
* This program lists details of unit statements for the
* contract, one per line.
* Initialise
* ----------
*
*
*     The details of the contract being dislayed  will be stored in
*     the CHDRULS I/O module.  Retrieve the  details,  display  the
*     contract number and contract owner and look up the owner name
*     on CLTS.
*
*     Perform  a  BEGN  on  USTF  using  the  Contract  Company and
*     Contract  Number  from CHDRULS and a Unit Statement Number of
*     all  9's.  This  should provide the first USTF record for the
*     given  contract.  If  no record is found matching the Company
*     and  Contract Number then inform the user that no details are
*     available.
*
*     If a matching record is found then display the statement date
*     and  number  on the screen along with the Unit Statement Flag
*     which  inidicates  whether  the statement was at a summary or
*     detail level.
*
*     Continue to load the subfile by subtracting  1  from the Unit
*     Statement Number in the USTF key and performing another BEGN.
*     This  should  return the next previous USTF  record  for  the
*     contract.  Proceed in this manner until 15  lines  have  been
*     written or the contract company/contract  number  changes  or
*     end of file is reached.
*
*
*
* Validation
* ----------
*
*     If  the  'KILL'  function  key  was  pressed   skip  all  the
*     validation.
*
*     If 'Rollup' was requested then continue to  load  the subfile
*     as described above.
*
*     No other processing is required in this section.
*
* Updating
* --------
*
*     If  the  'KILL'  function  key  was  pressed   skip  all  the
*     processing for this section.
*
*     Use  the Read Next Changed Subfile Record  function  to  read
*     through the subfile dropping all the records  where  a  space
*     has  been  entered in the Select field.  For  every  selected
*     record create a corresponding trigger record  on USTM showing
*     that this is a request for a reprint.
*
*
* Next Program
* ------------
*
*     Add 1 to the current program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P6517 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6517");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* WSAA-MISCELLANEOUS */
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaLinecount = new PackedDecimalData(3, 0);
	private String ustfrec = "USTFREC";
	private String ustmrec = "USTMREC";
		/* ERRORS */
	private String e040 = "E040";

	private FixedLengthStringData wsaaStmtType = new FixedLengthStringData(1);
	private Validator wsaaNewStmt = new Validator(wsaaStmtType, "P");
	private Validator wsaaReprint = new Validator(wsaaStmtType, "R");
		/*Contract Header Information for Unit Sta*/
	private ChdrulsTableDAM chdrulsIO = new ChdrulsTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Unit Statement Fund Header Details.*/
	private UstfTableDAM ustfIO = new UstfTableDAM();
		/*Unit Statement Trigger Records no Statem*/
	private UstmTableDAM ustmIO = new UstmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6517ScreenVars sv = ScreenProgram.getScreenVars( S6517ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		para1100, 
		setMoreSign1150, 
		preExit, 
		exit2090, 
		readNextChanged3010, 
		exit3090
	}

	public P6517() {
		super();
		screenVars = sv;
		new ScreenModel("S6517", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			para1000();
			loadSubfile1200();
		}
		catch (GOTOException e){
		}
	}

protected void para1000()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		wsaaToday.set(0);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		ustfIO.setFormat(ustfrec);
		chdrulsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrulsIO);
		if (isNE(chdrulsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrulsIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrulsIO.getChdrnum());
		sv.cownnum.set(chdrulsIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		sv.stmtClDate.set(ZERO);
		sv.ustmno.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6517", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void loadSubfile1200()
	{
		ustfIO.setChdrcoy(chdrulsIO.getChdrcoy());
		ustfIO.setChdrnum(chdrulsIO.getChdrnum());
		ustfIO.setUstmno(99999);
		ustfIO.setPlanSuffix(ZERO);
		ustfIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		ustfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ustfIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)
		&& isNE(ustfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ustfIO.getParams());
			syserrrec.statuz.set(ustfIO.getStatuz());
			fatalError600();
		}
		if (isNE(ustfIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(e040);
			goTo(GotoLabel.exit1090);
		}
		wsaaLinecount.set(ZERO);
		ustfIO.setFunction(varcom.nextr); //ILIFE-1651		 
		while ( !(isNE(ustfIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLinecount,15))) {
			writeSubfile1100();
		}
		
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void writeSubfile1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case para1100: {
					para1100();
				}
				case setMoreSign1150: {
					setMoreSign1150();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1100()
	{
		sv.subfileArea.set(SPACES);
		sv.ustmno.set(ustfIO.getUstmno());
		sv.stmtClDate.set(ustfIO.getStmtClDate());
		sv.stmtlevel.set(ustfIO.getUnitStmtFlag());
		scrnparams.function.set(varcom.sadd);
		processScreen("S6517", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		wsaaLinecount.add(1);
		ustfIO.setStatuz(varcom.endp); //ILIFE-1651 START by nnazeer
		/*setPrecision(ustfIO.getUstmno(), 0);
		ustfIO.setUstmno(sub(ustfIO.getUstmno(),1));
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)
		&& isNE(ustfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ustfIO.getParams());
			syserrrec.statuz.set(ustfIO.getStatuz());
			fatalError600();
		}
		if (isNE(ustfIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLinecount,15)) {
			goTo(GotoLabel.setMoreSign1150); 
		}
		goTo(GotoLabel.para1100);*/  //ILIFE-1651 END
	}

protected void setMoreSign1150()
	{
		if (isNE(ustfIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			rollUp2100();
			goTo(GotoLabel.exit2090);
		}
	}

protected void rollUp2100()
	{
		/*PARA*/
		wsaaLinecount.set(ZERO);
		while ( !(isNE(ustfIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLinecount,15))) {
			writeSubfile1100();
		}
		
		wsspcomn.edterror.set(SPACES);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para3000();
				}
				case readNextChanged3010: {
					readNextChanged3010();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3000()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void readNextChanged3010()
	{
		scrnparams.function.set(varcom.srnch);
		sv.subfileArea.set(SPACES);
		processScreen("S6517", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.readNextChanged3010);
		}
		writeTrigger3100();
		goTo(GotoLabel.readNextChanged3010);
	}

protected void writeTrigger3100()
	{
		para3100();
	}

protected void para3100()
	{
		ustmIO.setDataArea(SPACES);
		ustmIO.setChdrcoy(chdrulsIO.getChdrcoy());
		ustmIO.setChdrnum(chdrulsIO.getChdrnum());
		ustmIO.setEffdate(sv.stmtClDate);
		ustmIO.setStrpdate(wsaaToday);
		wsaaBatckey.set(wsspcomn.batchkey);
		ustmIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ustmIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ustmIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ustmIO.setTranno(ZERO);
		ustmIO.setUstmno(sv.ustmno);
		ustmIO.setJobnoStmt(ZERO);
		ustmIO.setStmtType("R");
		ustmIO.setUnitStmtFlag(ustfIO.getUnitStmtFlag());
		ustmIO.setFormat(ustmrec);
		ustmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ustmIO);
		if (isNE(ustmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ustmIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*PARA*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
