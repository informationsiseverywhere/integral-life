package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:54
 * Description:
 * Copybook name: UTRSSURKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrssurkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrssurFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrssurKey = new FixedLengthStringData(64).isAPartOf(utrssurFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrssurChdrcoy = new FixedLengthStringData(1).isAPartOf(utrssurKey, 0);
  	public FixedLengthStringData utrssurChdrnum = new FixedLengthStringData(8).isAPartOf(utrssurKey, 1);
  	public FixedLengthStringData utrssurLife = new FixedLengthStringData(2).isAPartOf(utrssurKey, 9);
  	public FixedLengthStringData utrssurCoverage = new FixedLengthStringData(2).isAPartOf(utrssurKey, 11);
  	public FixedLengthStringData utrssurRider = new FixedLengthStringData(2).isAPartOf(utrssurKey, 13);
  	public FixedLengthStringData utrssurUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrssurKey, 15);
  	public FixedLengthStringData utrssurUnitType = new FixedLengthStringData(1).isAPartOf(utrssurKey, 19);
  	public PackedDecimalData utrssurPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrssurKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(utrssurKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrssurFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrssurFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}