/*
 * File: P6211.java
 * Date: 30 August 2009 0:36:32
 * Author: Quipoz Limited
 * 
 * Class transformed from P6211.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupbTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S6211ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P6211 - U.L. Activated Fund Price Enquiry.
*
* This program allow the user to inquire the activated fund
* prices over a given period.(From Effective Date to To Date)
*
* The user can enter either one fund or space for all the funds.
* Subsequently the program will display the prices of one fund or
* all the funds between the two given dates.
*
* It is called from the Unit Linked Entry submenu(P5412) by
* means of selecting the appropriate option.
*
*****************************************************************
* </pre>
*/
public class P6211 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6211");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaLineCount = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaVrtfnd = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaShortdesc = new FixedLengthStringData(10).init(SPACES);
		/* ERRORS */
	private String e040 = "E040";
		/* TABLES */
	private String t5515 = "T5515";
		/* FORMATS */
	private String vprcuparec = "VPRCUPAREC";
	private String vprcupbrec = "VPRCUPBREC";
	private String descrec = "DESCREC";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Vflagcpy vflagcpy = new Vflagcpy();
		/*Unit Price file (Inquiry)*/
	private VprcupaTableDAM vprcupaIO = new VprcupaTableDAM();
		/*Virtual Price Logical View*/
	private VprcupbTableDAM vprcupbIO = new VprcupbTableDAM();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S6211ScreenVars sv = ScreenProgram.getScreenVars( S6211ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		emptyFund1060, 
		exit1090, 
		preExit, 
		emptyFund2060, 
		exit2090, 
		nextRead5020, 
		exit5090, 
		exit5590, 
		exit6090, 
		exit6590
	}

	public P6211() {
		super();
		screenVars = sv;
		new ScreenModel("S6211", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					loadSubfile1030();
				}
				case emptyFund1060: {
					emptyFund1060();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6211", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void loadSubfile1030()
	{
		if (isEQ(wsspuprc.uprcVrtfnd,SPACES)) {
			goTo(GotoLabel.emptyFund1060);
		}
		vprcupaIO.setParams(SPACES);
		vprcupaIO.setUnitVirtualFund(wsspuprc.uprcVrtfnd);
		vprcupaIO.setCompany(wsspcomn.company);
		vprcupaIO.setEffdate(wsspcomn.currto);
		vprcupaIO.setJobno(99999999);
		vprcupaIO.setUnitType(SPACES);
		vprcupaIO.setFormat(vprcuparec);
		vprcupaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprcupbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcupbIO.setFitKeysSearch("COMPANY");
		SmartFileCode.execute(appVars, vprcupaIO);
		if (isNE(vprcupaIO.getStatuz(),varcom.oK)
		&& isNE(vprcupaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupaIO.getParams());
			fatalError600();
		}
		wsaaLineCount.set(0);
		while ( !(isEQ(vprcupaIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLineCount,17))) {
			loadSubfile5000();
		}
		
		if (isEQ(wsaaLineCount,ZERO)) {
			scrnparams.errorCode.set(e040);
		}
		if (isEQ(vprcupaIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		goTo(GotoLabel.exit1090);
	}

protected void emptyFund1060()
	{
		vprcupbIO.setParams(SPACES);
		vprcupbIO.setUnitVirtualFund(wsspuprc.uprcVrtfnd);
		vprcupbIO.setCompany(wsspcomn.company);
		vprcupbIO.setEffdate(wsspcomn.currto);
		vprcupbIO.setJobno(99999999);
		vprcupbIO.setUnitType(SPACES);
		vprcupbIO.setFormat(vprcupbrec);
		vprcupbIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprcupbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcupbIO.setFitKeysSearch("COMPANY");
		SmartFileCode.execute(appVars, vprcupbIO);
		if (isNE(vprcupbIO.getStatuz(),varcom.oK)
		&& isNE(vprcupbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupbIO.getParams());
			fatalError600();
		}
		wsaaLineCount.set(0);
		while ( !(isEQ(vprcupbIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLineCount,17))) {
			loadSubfile5500();
		}
		
		if (isEQ(wsaaLineCount,ZERO)) {
			scrnparams.errorCode.set(e040);
		}
		if (isEQ(vprcupbIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					rollUp2020();
				}
				case emptyFund2060: {
					emptyFund2060();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void rollUp2020()
	{
		if (isNE(scrnparams.statuz,varcom.rolu)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspuprc.uprcVrtfnd,SPACES)) {
			goTo(GotoLabel.emptyFund2060);
		}
		wsaaLineCount.set(0);
		while ( !(isEQ(vprcupaIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLineCount,17))) {
			loadSubfile5000();
		}
		
		if (isEQ(vprcupaIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void emptyFund2060()
	{
		wsaaLineCount.set(0);
		while ( !(isEQ(vprcupbIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLineCount,17))) {
			loadSubfile5500();
		}
		
		if (isEQ(vprcupbIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		wsspcomn.edterror.set("Y");
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadSubfile5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextRecord5010();
				}
				case nextRead5020: {
					nextRead5020();
				}
				case exit5090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextRecord5010()
	{
		if (isNE(vprcupaIO.getCompany(),wsspcomn.company)) {
			vprcupaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5090);
		}
		if (isNE(wsspuprc.uprcVrtfnd,SPACES)) {
			if (isNE(vprcupaIO.getUnitVirtualFund(),wsspuprc.uprcVrtfnd)) {
				vprcupaIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit5090);
			}
		}
		if (isGT(vprcupaIO.getEffdate(),wsspcomn.currto)) {
			goTo(GotoLabel.nextRead5020);
		}
		if (isNE(wsspcomn.currfrom,varcom.vrcmMaxDate)) {
			if (isLT(vprcupaIO.getEffdate(),wsspcomn.currfrom)) {
				goTo(GotoLabel.nextRead5020);
			}
		}
		if (isNE(vprcupaIO.getValidflag(),vflagcpy.inForceVal)) {
			goTo(GotoLabel.nextRead5020);
		}
		sv.unitVirtualFund.set(vprcupaIO.getUnitVirtualFund());
		sv.unitType.set(vprcupaIO.getUnitType());
		sv.effdate.set(vprcupaIO.getEffdate());
		sv.unitBarePrice.set(vprcupaIO.getUnitBarePrice());
		sv.unitBidPrice.set(vprcupaIO.getUnitBidPrice());
		sv.unitOfferPrice.set(vprcupaIO.getUnitOfferPrice());
		sv.updateDate.set(vprcupaIO.getUpdateDate());
		if (isNE(vprcupaIO.getUnitVirtualFund(),wsaaVrtfnd)) {
			getDescription6000();
		}
		sv.shortdesc.set(wsaaShortdesc);
		wsaaLineCount.add(1);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6211", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextRead5020()
	{
		vprcupaIO.setFunction(varcom.nextr);
		vprcupaIO.setFormat(vprcuparec);
		SmartFileCode.execute(appVars, vprcupaIO);
		if (isNE(vprcupaIO.getStatuz(),varcom.oK)
		&& isNE(vprcupaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupaIO.getParams());
			fatalError600();
		}
	}

protected void loadSubfile5500()
	{
		try {
			nextRecord5510();
			nextRead5520();
		}
		catch (GOTOException e){
		}
	}

protected void nextRecord5510()
	{
		if (isNE(vprcupbIO.getCompany(),wsspcomn.company)) {
			vprcupbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5590);
		}
		if (isNE(wsspcomn.currfrom,varcom.vrcmMaxDate)) {
			if (isLT(vprcupbIO.getEffdate(),wsspcomn.currfrom)) {
				vprcupbIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit5590);
			}
		}
		sv.unitVirtualFund.set(vprcupbIO.getUnitVirtualFund());
		sv.unitType.set(vprcupbIO.getUnitType());
		sv.effdate.set(vprcupbIO.getEffdate());
		sv.unitBarePrice.set(vprcupbIO.getUnitBarePrice());
		sv.unitBidPrice.set(vprcupbIO.getUnitBidPrice());
		sv.unitOfferPrice.set(vprcupbIO.getUnitOfferPrice());
		sv.updateDate.set(vprcupbIO.getUpdateDate());
		if (isNE(vprcupbIO.getUnitVirtualFund(),wsaaVrtfnd)) {
			getDescription6500();
		}
		sv.shortdesc.set(wsaaShortdesc);
		wsaaLineCount.add(1);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6211", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextRead5520()
	{
		vprcupbIO.setFunction(varcom.nextr);
		vprcupbIO.setFormat(vprcupbrec);
		SmartFileCode.execute(appVars, vprcupbIO);
		if (isNE(vprcupbIO.getStatuz(),varcom.oK)
		&& isNE(vprcupbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupbIO.getParams());
			fatalError600();
		}
	}

protected void getDescription6000()
	{
		try {
			readDesc6010();
		}
		catch (GOTOException e){
		}
	}

protected void readDesc6010()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(vprcupaIO.getUnitVirtualFund());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsaaShortdesc.fill("?");
			goTo(GotoLabel.exit6090);
		}
		wsaaShortdesc.set(descIO.getShortdesc());
		wsaaVrtfnd.set(vprcupaIO.getUnitVirtualFund());
	}

protected void getDescription6500()
	{
		try {
			readDesc6510();
		}
		catch (GOTOException e){
		}
	}

protected void readDesc6510()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(vprcupbIO.getUnitVirtualFund());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsaaShortdesc.fill("?");
			goTo(GotoLabel.exit6590);
		}
		wsaaShortdesc.set(descIO.getShortdesc());
		wsaaVrtfnd.set(vprcupbIO.getUnitVirtualFund());
	}
}
