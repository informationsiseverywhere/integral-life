package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:49
 * Description:
 * Copybook name: UTRNEXTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnextkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnextFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnextKey = new FixedLengthStringData(64).isAPartOf(utrnextFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnextChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnextKey, 0);
  	public FixedLengthStringData utrnextChdrnum = new FixedLengthStringData(8).isAPartOf(utrnextKey, 1);
  	public PackedDecimalData utrnextProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(utrnextKey, 9);
  	public FixedLengthStringData utrnextUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnextKey, 11);
  	public FixedLengthStringData utrnextUnitType = new FixedLengthStringData(1).isAPartOf(utrnextKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(utrnextKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnextFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnextFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}