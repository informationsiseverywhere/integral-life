package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5542
 * @version 1.0 generated on 30/08/09 06:43
 * @author Quipoz
 */
public class S5542ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1692);
	public FixedLengthStringData dataFields = new FixedLengthStringData(732).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreqs = new FixedLengthStringData(12).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(6, 2, billfreqs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01 = DD.billfreq.copy().isAPartOf(filler,0);
	public FixedLengthStringData billfreq02 = DD.billfreq.copy().isAPartOf(filler,2);
	public FixedLengthStringData billfreq03 = DD.billfreq.copy().isAPartOf(filler,4);
	public FixedLengthStringData billfreq04 = DD.billfreq.copy().isAPartOf(filler,6);
	public FixedLengthStringData billfreq05 = DD.billfreq.copy().isAPartOf(filler,8);
	public FixedLengthStringData billfreq06 = DD.billfreq.copy().isAPartOf(filler,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData feemaxs = new FixedLengthStringData(102).isAPartOf(dataFields, 13);
	public ZonedDecimalData[] feemax = ZDArrayPartOfStructure(6, 17, 2, feemaxs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(102).isAPartOf(feemaxs, 0, FILLER_REDEFINE);
	public ZonedDecimalData feemax01 = DD.feemax.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData feemax02 = DD.feemax.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData feemax03 = DD.feemax.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData feemax04 = DD.feemax.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData feemax05 = DD.feemax.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData feemax06 = DD.feemax.copyToZonedDecimal().isAPartOf(filler1,85);
	public FixedLengthStringData feemins = new FixedLengthStringData(102).isAPartOf(dataFields, 115);
	public ZonedDecimalData[] feemin = ZDArrayPartOfStructure(6, 17, 2, feemins, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(102).isAPartOf(feemins, 0, FILLER_REDEFINE);
	public ZonedDecimalData feemin01 = DD.feemin.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData feemin02 = DD.feemin.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData feemin03 = DD.feemin.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData feemin04 = DD.feemin.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData feemin05 = DD.feemin.copyToZonedDecimal().isAPartOf(filler2,68);
	public ZonedDecimalData feemin06 = DD.feemin.copyToZonedDecimal().isAPartOf(filler2,85);
	public FixedLengthStringData feepcs = new FixedLengthStringData(30).isAPartOf(dataFields, 217);
	public ZonedDecimalData[] feepc = ZDArrayPartOfStructure(6, 5, 2, feepcs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(feepcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData feepc01 = DD.feepc.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData feepc02 = DD.feepc.copyToZonedDecimal().isAPartOf(filler3,5);
	public ZonedDecimalData feepc03 = DD.feepc.copyToZonedDecimal().isAPartOf(filler3,10);
	public ZonedDecimalData feepc04 = DD.feepc.copyToZonedDecimal().isAPartOf(filler3,15);
	public ZonedDecimalData feepc05 = DD.feepc.copyToZonedDecimal().isAPartOf(filler3,20);
	public ZonedDecimalData feepc06 = DD.feepc.copyToZonedDecimal().isAPartOf(filler3,25);
	public FixedLengthStringData ffamts = new FixedLengthStringData(102).isAPartOf(dataFields, 247);
	public ZonedDecimalData[] ffamt = ZDArrayPartOfStructure(6, 17, 2, ffamts, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(102).isAPartOf(ffamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData ffamt01 = DD.ffamt.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData ffamt02 = DD.ffamt.copyToZonedDecimal().isAPartOf(filler4,17);
	public ZonedDecimalData ffamt03 = DD.ffamt.copyToZonedDecimal().isAPartOf(filler4,34);
	public ZonedDecimalData ffamt04 = DD.ffamt.copyToZonedDecimal().isAPartOf(filler4,51);
	public ZonedDecimalData ffamt05 = DD.ffamt.copyToZonedDecimal().isAPartOf(filler4,68);
	public ZonedDecimalData ffamt06 = DD.ffamt.copyToZonedDecimal().isAPartOf(filler4,85);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,349);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,357);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,365);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,373);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,403);
	public FixedLengthStringData wdlAmounts = new FixedLengthStringData(102).isAPartOf(dataFields, 408);
	public ZonedDecimalData[] wdlAmount = ZDArrayPartOfStructure(6, 17, 2, wdlAmounts, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(102).isAPartOf(wdlAmounts, 0, FILLER_REDEFINE);
	public ZonedDecimalData wdlAmount01 = DD.wdamnt.copyToZonedDecimal().isAPartOf(filler5,0);
	public ZonedDecimalData wdlAmount02 = DD.wdamnt.copyToZonedDecimal().isAPartOf(filler5,17);
	public ZonedDecimalData wdlAmount03 = DD.wdamnt.copyToZonedDecimal().isAPartOf(filler5,34);
	public ZonedDecimalData wdlAmount04 = DD.wdamnt.copyToZonedDecimal().isAPartOf(filler5,51);
	public ZonedDecimalData wdlAmount05 = DD.wdamnt.copyToZonedDecimal().isAPartOf(filler5,68);
	public ZonedDecimalData wdlAmount06 = DD.wdamnt.copyToZonedDecimal().isAPartOf(filler5,85);
	public FixedLengthStringData wdlFreqs = new FixedLengthStringData(18).isAPartOf(dataFields, 510);
	public ZonedDecimalData[] wdlFreq = ZDArrayPartOfStructure(6, 3, 0, wdlFreqs, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(18).isAPartOf(wdlFreqs, 0, FILLER_REDEFINE);
	public ZonedDecimalData wdlFreq01 = DD.wdlfrq.copyToZonedDecimal().isAPartOf(filler6,0);
	public ZonedDecimalData wdlFreq02 = DD.wdlfrq.copyToZonedDecimal().isAPartOf(filler6,3);
	public ZonedDecimalData wdlFreq03 = DD.wdlfrq.copyToZonedDecimal().isAPartOf(filler6,6);
	public ZonedDecimalData wdlFreq04 = DD.wdlfrq.copyToZonedDecimal().isAPartOf(filler6,9);
	public ZonedDecimalData wdlFreq05 = DD.wdlfrq.copyToZonedDecimal().isAPartOf(filler6,12);
	public ZonedDecimalData wdlFreq06 = DD.wdlfrq.copyToZonedDecimal().isAPartOf(filler6,15);
	public FixedLengthStringData wdrems = new FixedLengthStringData(102).isAPartOf(dataFields, 528);
	public ZonedDecimalData[] wdrem = ZDArrayPartOfStructure(6, 17, 2, wdrems, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(102).isAPartOf(wdrems, 0, FILLER_REDEFINE);
	public ZonedDecimalData wdrem01 = DD.wdrem.copyToZonedDecimal().isAPartOf(filler7,0);
	public ZonedDecimalData wdrem02 = DD.wdrem.copyToZonedDecimal().isAPartOf(filler7,17);
	public ZonedDecimalData wdrem03 = DD.wdrem.copyToZonedDecimal().isAPartOf(filler7,34);
	public ZonedDecimalData wdrem04 = DD.wdrem.copyToZonedDecimal().isAPartOf(filler7,51);
	public ZonedDecimalData wdrem05 = DD.wdrem.copyToZonedDecimal().isAPartOf(filler7,68);
	public ZonedDecimalData wdrem06 = DD.wdrem.copyToZonedDecimal().isAPartOf(filler7,85);
	//ILIFE-7956 -START
	public FixedLengthStringData maxwiths = new FixedLengthStringData(102).isAPartOf(dataFields, 630);
	public ZonedDecimalData[] maxwith = ZDArrayPartOfStructure(6, 17, 2, maxwiths, 0);
	public FixedLengthStringData filler24 = new FixedLengthStringData(102).isAPartOf(maxwiths, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxwith01 = DD.maxwith.copyToZonedDecimal().isAPartOf(filler24,0);
	public ZonedDecimalData maxwith02 = DD.maxwith.copyToZonedDecimal().isAPartOf(filler24,17);
	public ZonedDecimalData maxwith03 = DD.maxwith.copyToZonedDecimal().isAPartOf(filler24,34);
	public ZonedDecimalData maxwith04 = DD.maxwith.copyToZonedDecimal().isAPartOf(filler24,51);
	public ZonedDecimalData maxwith05 = DD.maxwith.copyToZonedDecimal().isAPartOf(filler24,68);
	public ZonedDecimalData maxwith06 = DD.maxwith.copyToZonedDecimal().isAPartOf(filler24,85);
	//ILIFE-7956 -END
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 732);
	public FixedLengthStringData billfreqsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] billfreqErr = FLSArrayPartOfStructure(6, 4, billfreqsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(billfreqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData billfreq02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData billfreq03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData billfreq04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData billfreq05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData billfreq06Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData feemaxsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] feemaxErr = FLSArrayPartOfStructure(6, 4, feemaxsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(feemaxsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData feemax01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData feemax02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData feemax03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData feemax04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData feemax05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData feemax06Err = new FixedLengthStringData(4).isAPartOf(filler9, 20);
	public FixedLengthStringData feeminsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] feeminErr = FLSArrayPartOfStructure(6, 4, feeminsErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(feeminsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData feemin01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData feemin02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData feemin03Err = new FixedLengthStringData(4).isAPartOf(filler10, 8);
	public FixedLengthStringData feemin04Err = new FixedLengthStringData(4).isAPartOf(filler10, 12);
	public FixedLengthStringData feemin05Err = new FixedLengthStringData(4).isAPartOf(filler10, 16);
	public FixedLengthStringData feemin06Err = new FixedLengthStringData(4).isAPartOf(filler10, 20);
	public FixedLengthStringData feepcsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 76);	
	public FixedLengthStringData[] feepcErr = FLSArrayPartOfStructure(6, 4, feepcsErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(24).isAPartOf(feepcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData feepc01Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData feepc02Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData feepc03Err = new FixedLengthStringData(4).isAPartOf(filler11, 8);
	public FixedLengthStringData feepc04Err = new FixedLengthStringData(4).isAPartOf(filler11, 12);
	public FixedLengthStringData feepc05Err = new FixedLengthStringData(4).isAPartOf(filler11, 16);
	public FixedLengthStringData feepc06Err = new FixedLengthStringData(4).isAPartOf(filler11, 20);
	public FixedLengthStringData ffamtsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] ffamtErr = FLSArrayPartOfStructure(6, 4, ffamtsErr, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(ffamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ffamt01Err = new FixedLengthStringData(4).isAPartOf(filler12, 0);
	public FixedLengthStringData ffamt02Err = new FixedLengthStringData(4).isAPartOf(filler12, 4);
	public FixedLengthStringData ffamt03Err = new FixedLengthStringData(4).isAPartOf(filler12, 8);
	public FixedLengthStringData ffamt04Err = new FixedLengthStringData(4).isAPartOf(filler12, 12);
	public FixedLengthStringData ffamt05Err = new FixedLengthStringData(4).isAPartOf(filler12, 16);
	public FixedLengthStringData ffamt06Err = new FixedLengthStringData(4).isAPartOf(filler12, 20);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData wdamntsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData[] wdamntErr = FLSArrayPartOfStructure(6, 4, wdamntsErr, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(24).isAPartOf(wdamntsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData wdamnt01Err = new FixedLengthStringData(4).isAPartOf(filler13, 0);
	public FixedLengthStringData wdamnt02Err = new FixedLengthStringData(4).isAPartOf(filler13, 4);
	public FixedLengthStringData wdamnt03Err = new FixedLengthStringData(4).isAPartOf(filler13, 8);
	public FixedLengthStringData wdamnt04Err = new FixedLengthStringData(4).isAPartOf(filler13, 12);
	public FixedLengthStringData wdamnt05Err = new FixedLengthStringData(4).isAPartOf(filler13, 16);
	public FixedLengthStringData wdamnt06Err = new FixedLengthStringData(4).isAPartOf(filler13, 20);
	public FixedLengthStringData wdlfrqsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData[] wdlfrqErr = FLSArrayPartOfStructure(6, 4, wdlfrqsErr, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(24).isAPartOf(wdlfrqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData wdlfrq01Err = new FixedLengthStringData(4).isAPartOf(filler14, 0);  
	public FixedLengthStringData wdlfrq02Err = new FixedLengthStringData(4).isAPartOf(filler14, 4);
	public FixedLengthStringData wdlfrq03Err = new FixedLengthStringData(4).isAPartOf(filler14, 8);
	public FixedLengthStringData wdlfrq04Err = new FixedLengthStringData(4).isAPartOf(filler14, 12);
	public FixedLengthStringData wdlfrq05Err = new FixedLengthStringData(4).isAPartOf(filler14, 16);
	public FixedLengthStringData wdlfrq06Err = new FixedLengthStringData(4).isAPartOf(filler14, 20);
	public FixedLengthStringData wdremsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData[] wdremErr = FLSArrayPartOfStructure(6, 4, wdremsErr, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(24).isAPartOf(wdremsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData wdrem01Err = new FixedLengthStringData(4).isAPartOf(filler15, 0);
	public FixedLengthStringData wdrem02Err = new FixedLengthStringData(4).isAPartOf(filler15, 4);
	public FixedLengthStringData wdrem03Err = new FixedLengthStringData(4).isAPartOf(filler15, 8);
	public FixedLengthStringData wdrem04Err = new FixedLengthStringData(4).isAPartOf(filler15, 12);
	public FixedLengthStringData wdrem05Err = new FixedLengthStringData(4).isAPartOf(filler15, 16);
	public FixedLengthStringData wdrem06Err = new FixedLengthStringData(4).isAPartOf(filler15, 20);
	//ILIFE-7956 START
	public FixedLengthStringData maxwithsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData[] maxwithErr = FLSArrayPartOfStructure(6, 4, maxwithsErr, 0);
	public FixedLengthStringData filler25 = new FixedLengthStringData(24).isAPartOf(maxwithsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxwith01Err = new FixedLengthStringData(4).isAPartOf(filler25, 0);
	public FixedLengthStringData maxwith02Err = new FixedLengthStringData(4).isAPartOf(filler25, 4);
	public FixedLengthStringData maxwith03Err = new FixedLengthStringData(4).isAPartOf(filler25, 8);
	public FixedLengthStringData maxwith04Err = new FixedLengthStringData(4).isAPartOf(filler25, 12);
	public FixedLengthStringData maxwith05Err = new FixedLengthStringData(4).isAPartOf(filler25, 16);
	public FixedLengthStringData maxwith06Err = new FixedLengthStringData(4).isAPartOf(filler25, 20);
	//ILIFE-7956 -END
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(720).isAPartOf(dataArea, 972);
	public FixedLengthStringData billfreqsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(6, 12, billfreqsOut, 0);
	public FixedLengthStringData[][] billfreqO = FLSDArrayPartOfArrayStructure(12, 1, billfreqOut, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(72).isAPartOf(billfreqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] billfreq01Out = FLSArrayPartOfStructure(12, 1, filler16, 0);
	public FixedLengthStringData[] billfreq02Out = FLSArrayPartOfStructure(12, 1, filler16, 12);
	public FixedLengthStringData[] billfreq03Out = FLSArrayPartOfStructure(12, 1, filler16, 24);
	public FixedLengthStringData[] billfreq04Out = FLSArrayPartOfStructure(12, 1, filler16, 36);
	public FixedLengthStringData[] billfreq05Out = FLSArrayPartOfStructure(12, 1, filler16, 48);
	public FixedLengthStringData[] billfreq06Out = FLSArrayPartOfStructure(12, 1, filler16, 60);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData feemaxsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] feemaxOut = FLSArrayPartOfStructure(6, 12, feemaxsOut, 0);
	public FixedLengthStringData[][] feemaxO = FLSDArrayPartOfArrayStructure(12, 1, feemaxOut, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(72).isAPartOf(feemaxsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] feemax01Out = FLSArrayPartOfStructure(12, 1, filler17, 0);
	public FixedLengthStringData[] feemax02Out = FLSArrayPartOfStructure(12, 1, filler17, 12);
	public FixedLengthStringData[] feemax03Out = FLSArrayPartOfStructure(12, 1, filler17, 24);
	public FixedLengthStringData[] feemax04Out = FLSArrayPartOfStructure(12, 1, filler17, 36);
	public FixedLengthStringData[] feemax05Out = FLSArrayPartOfStructure(12, 1, filler17, 48);
	public FixedLengthStringData[] feemax06Out = FLSArrayPartOfStructure(12, 1, filler17, 60);
	public FixedLengthStringData feeminsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] feeminOut = FLSArrayPartOfStructure(6, 12, feeminsOut, 0);
	public FixedLengthStringData[][] feeminO = FLSDArrayPartOfArrayStructure(12, 1, feeminOut, 0);
	public FixedLengthStringData filler18 = new FixedLengthStringData(72).isAPartOf(feeminsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] feemin01Out = FLSArrayPartOfStructure(12, 1, filler18, 0);
	public FixedLengthStringData[] feemin02Out = FLSArrayPartOfStructure(12, 1, filler18, 12);
	public FixedLengthStringData[] feemin03Out = FLSArrayPartOfStructure(12, 1, filler18, 24);
	public FixedLengthStringData[] feemin04Out = FLSArrayPartOfStructure(12, 1, filler18, 36);
	public FixedLengthStringData[] feemin05Out = FLSArrayPartOfStructure(12, 1, filler18, 48);
	public FixedLengthStringData[] feemin06Out = FLSArrayPartOfStructure(12, 1, filler18, 60);
	public FixedLengthStringData feepcsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 228);
	public FixedLengthStringData[] feepcOut = FLSArrayPartOfStructure(6, 12, feepcsOut, 0);
	public FixedLengthStringData[][] feepcO = FLSDArrayPartOfArrayStructure(12, 1, feepcOut, 0);
	public FixedLengthStringData filler19 = new FixedLengthStringData(72).isAPartOf(feepcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] feepc01Out = FLSArrayPartOfStructure(12, 1, filler19, 0);
	public FixedLengthStringData[] feepc02Out = FLSArrayPartOfStructure(12, 1, filler19, 12);
	public FixedLengthStringData[] feepc03Out = FLSArrayPartOfStructure(12, 1, filler19, 24);
	public FixedLengthStringData[] feepc04Out = FLSArrayPartOfStructure(12, 1, filler19, 36);
	public FixedLengthStringData[] feepc05Out = FLSArrayPartOfStructure(12, 1, filler19, 48);
	public FixedLengthStringData[] feepc06Out = FLSArrayPartOfStructure(12, 1, filler19, 60);
	public FixedLengthStringData ffamtsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 300);
	public FixedLengthStringData[] ffamtOut = FLSArrayPartOfStructure(6, 12, ffamtsOut, 0);
	public FixedLengthStringData[][] ffamtO = FLSDArrayPartOfArrayStructure(12, 1, ffamtOut, 0);
	public FixedLengthStringData filler20 = new FixedLengthStringData(72).isAPartOf(ffamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ffamt01Out = FLSArrayPartOfStructure(12, 1, filler20, 0);
	public FixedLengthStringData[] ffamt02Out = FLSArrayPartOfStructure(12, 1, filler20, 12);
	public FixedLengthStringData[] ffamt03Out = FLSArrayPartOfStructure(12, 1, filler20, 24);
	public FixedLengthStringData[] ffamt04Out = FLSArrayPartOfStructure(12, 1, filler20, 36);
	public FixedLengthStringData[] ffamt05Out = FLSArrayPartOfStructure(12, 1, filler20, 48);
	public FixedLengthStringData[] ffamt06Out = FLSArrayPartOfStructure(12, 1, filler20, 60);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData wdamntsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 432);
	public FixedLengthStringData[] wdamntOut = FLSArrayPartOfStructure(6, 12, wdamntsOut, 0);
	public FixedLengthStringData[][] wdamntO = FLSDArrayPartOfArrayStructure(12, 1, wdamntOut, 0);
	public FixedLengthStringData filler21 = new FixedLengthStringData(72).isAPartOf(wdamntsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] wdamnt01Out = FLSArrayPartOfStructure(12, 1, filler21, 0);
	public FixedLengthStringData[] wdamnt02Out = FLSArrayPartOfStructure(12, 1, filler21, 12);
	public FixedLengthStringData[] wdamnt03Out = FLSArrayPartOfStructure(12, 1, filler21, 24);
	public FixedLengthStringData[] wdamnt04Out = FLSArrayPartOfStructure(12, 1, filler21, 36);
	public FixedLengthStringData[] wdamnt05Out = FLSArrayPartOfStructure(12, 1, filler21, 48);
	public FixedLengthStringData[] wdamnt06Out = FLSArrayPartOfStructure(12, 1, filler21, 60);
	public FixedLengthStringData wdlfrqsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 504);
	public FixedLengthStringData[] wdlfrqOut = FLSArrayPartOfStructure(6, 12, wdlfrqsOut, 0);
	public FixedLengthStringData[][] wdlfrqO = FLSDArrayPartOfArrayStructure(12, 1, wdlfrqOut, 0);
	public FixedLengthStringData filler22 = new FixedLengthStringData(72).isAPartOf(wdlfrqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] wdlfrq01Out = FLSArrayPartOfStructure(12, 1, filler22, 0);
	public FixedLengthStringData[] wdlfrq02Out = FLSArrayPartOfStructure(12, 1, filler22, 12);
	public FixedLengthStringData[] wdlfrq03Out = FLSArrayPartOfStructure(12, 1, filler22, 24);
	public FixedLengthStringData[] wdlfrq04Out = FLSArrayPartOfStructure(12, 1, filler22, 36);
	public FixedLengthStringData[] wdlfrq05Out = FLSArrayPartOfStructure(12, 1, filler22, 48);
	public FixedLengthStringData[] wdlfrq06Out = FLSArrayPartOfStructure(12, 1, filler22, 60);
	public FixedLengthStringData wdremsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 576);
	public FixedLengthStringData[] wdremOut = FLSArrayPartOfStructure(6, 12, wdremsOut, 0);
	public FixedLengthStringData[][] wdremO = FLSDArrayPartOfArrayStructure(12, 1, wdremOut, 0);
	public FixedLengthStringData filler23 = new FixedLengthStringData(72).isAPartOf(wdremsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] wdrem01Out = FLSArrayPartOfStructure(12, 1, filler23, 0);
	public FixedLengthStringData[] wdrem02Out = FLSArrayPartOfStructure(12, 1, filler23, 12);
	public FixedLengthStringData[] wdrem03Out = FLSArrayPartOfStructure(12, 1, filler23, 24);
	public FixedLengthStringData[] wdrem04Out = FLSArrayPartOfStructure(12, 1, filler23, 36);
	public FixedLengthStringData[] wdrem05Out = FLSArrayPartOfStructure(12, 1, filler23, 48);
	public FixedLengthStringData[] wdrem06Out = FLSArrayPartOfStructure(12, 1, filler23, 60);
	//ILIFE-7956 START
	public FixedLengthStringData maxwithsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 648);
	public FixedLengthStringData[] maxwithOut = FLSArrayPartOfStructure(6, 12, maxwithsOut, 0);
	public FixedLengthStringData[][] maxwithO = FLSDArrayPartOfArrayStructure(12, 1, maxwithOut, 0);
	public FixedLengthStringData filler26 = new FixedLengthStringData(72).isAPartOf(maxwithsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxwith01Out = FLSArrayPartOfStructure(12, 1, filler26, 0);
	public FixedLengthStringData[] maxwith02Out = FLSArrayPartOfStructure(12, 1, filler26, 12);
	public FixedLengthStringData[] maxwith03Out = FLSArrayPartOfStructure(12, 1, filler26, 24);
	public FixedLengthStringData[] maxwith04Out = FLSArrayPartOfStructure(12, 1, filler26, 36);
	public FixedLengthStringData[] maxwith05Out = FLSArrayPartOfStructure(12, 1, filler26, 48);
	public FixedLengthStringData[] maxwith06Out = FLSArrayPartOfStructure(12, 1, filler26, 60);
	//ILIFE-7956 -END
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5542screenWritten = new LongData(0);
	public LongData S5542protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5542ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(billfreq01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxwith01Out,new String[] {null,null, null,"10", null, null, null, null, null, null, null, null});//ILIFE-7956
		fieldIndMap.put(maxwith02Out,new String[] {null,null, null,"11", null, null, null, null, null, null, null, null});//ILIFE-7956
		fieldIndMap.put(maxwith03Out,new String[] {null,null, null,"12", null, null, null, null, null, null, null, null});//ILIFE-7956
		fieldIndMap.put(maxwith04Out,new String[] {null,null, null,"13", null, null, null, null, null, null, null, null});//ILIFE-7956
		fieldIndMap.put(maxwith05Out,new String[] {null,null, null,"14", null, null, null, null, null, null, null, null});//ILIFE-7956
		fieldIndMap.put(maxwith06Out,new String[] {null,null, null,"15", null, null, null, null, null, null, null, null});//ILIFE-7956
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, billfreq01, wdlFreq01, wdlAmount01, wdrem01, ffamt01, feepc01, feemin01, feemax01, billfreq02, wdlFreq02, wdlAmount02, wdrem02, ffamt02, feepc02, feemin02, feemax02, billfreq03, wdlFreq03, wdlAmount03, wdrem03, ffamt03, feepc03, feemin03, feemax03, billfreq04, wdlFreq04, wdlAmount04, wdrem04, ffamt04, feepc04, feemin04, feemax04, billfreq05, wdlFreq05, wdlAmount05, wdrem05, ffamt05, feepc05, feemin05, feemax05, billfreq06, wdlFreq06, wdlAmount06, wdrem06, ffamt06, feepc06, feemin06, feemax06, maxwith01, maxwith02, maxwith03, maxwith04, maxwith05, maxwith06};//ILIFE-7956
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, billfreq01Out, wdlfrq01Out, wdamnt01Out, wdrem01Out, ffamt01Out, feepc01Out, feemin01Out, feemax01Out, billfreq02Out, wdlfrq02Out, wdamnt02Out, wdrem02Out, ffamt02Out, feepc02Out, feemin02Out, feemax02Out, billfreq03Out, wdlfrq03Out, wdamnt03Out, wdrem03Out, ffamt03Out, feepc03Out, feemin03Out, feemax03Out, billfreq04Out, wdlfrq04Out, wdamnt04Out, wdrem04Out, ffamt04Out, feepc04Out, feemin04Out, feemax04Out, billfreq05Out, wdlfrq05Out, wdamnt05Out, wdrem05Out, ffamt05Out, feepc05Out, feemin05Out, feemax05Out, billfreq06Out, wdlfrq06Out, wdamnt06Out, wdrem06Out, ffamt06Out, feepc06Out, feemin06Out, feemax06Out, maxwith01Out, maxwith01Out, maxwith02Out, maxwith03Out, maxwith04Out, maxwith05Out, maxwith06Out};//ILIFE-7956
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, billfreq01Err, wdlfrq01Err, wdamnt01Err, wdrem01Err, ffamt01Err, feepc01Err, feemin01Err, feemax01Err, billfreq02Err, wdlfrq02Err, wdamnt02Err, wdrem02Err, ffamt02Err, feepc02Err, feemin02Err, feemax02Err, billfreq03Err, wdlfrq03Err, wdamnt03Err, wdrem03Err, ffamt03Err, feepc03Err, feemin03Err, feemax03Err, billfreq04Err, wdlfrq04Err, wdamnt04Err, wdrem04Err, ffamt04Err, feepc04Err, feemin04Err, feemax04Err, billfreq05Err, wdlfrq05Err, wdamnt05Err, wdrem05Err, ffamt05Err, feepc05Err, feemin05Err, feemax05Err, billfreq06Err, wdlfrq06Err, wdamnt06Err, wdrem06Err, ffamt06Err, feepc06Err, feemin06Err, feemax06Err, maxwith01Err, maxwith02Err, maxwith03Err, maxwith04Err, maxwith05Err, maxwith06Err};//ILIFE-7956
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5542screen.class;
		protectRecord = S5542protect.class;
	}

}
