package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UdivpfTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:47
 * Class transformed from UDIVPF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UdivpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 58;
	public FixedLengthStringData udivrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData udivpfRecord = udivrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(udivrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(udivrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(udivrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(udivrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(udivrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(udivrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UdivpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UdivpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UdivpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UdivpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UdivpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UdivpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UdivpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UDIVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUdivrec() {
  		return udivrec;
	}

	public FixedLengthStringData getUdivpfRecord() {
  		return udivpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUdivrec(what);
	}

	public void setUdivrec(Object what) {
  		this.udivrec.set(what);
	}

	public void setUdivpfRecord(Object what) {
  		this.udivpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(udivrec.getLength());
		result.set(udivrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}