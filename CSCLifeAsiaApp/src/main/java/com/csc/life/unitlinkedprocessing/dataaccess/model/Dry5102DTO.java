package com.csc.life.unitlinkedprocessing.dataaccess.model;

public class Dry5102DTO {
	private int polsum;
	private int ptdate;
	public int getPolsum() {
		return polsum;
	}
	public void setPolsum(int polsum) {
		this.polsum = polsum;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
}
