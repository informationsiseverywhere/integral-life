/*
 * File: P5536.java
 * Date: 30 August 2009 0:29:57
 * Author: Quipoz Limited
 * 
 * Class transformed from P5536.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.procedures.T5536pt;
import com.csc.life.unitlinkedprocessing.screens.S5536ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5536 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5536");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5536rec t5536rec = new T5536rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5536ScreenVars sv = ScreenProgram.getScreenVars( S5536ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5536() {
		super();
		screenVars = sv;
		new ScreenModel("S5536", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5536rec.t5536Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5536rec.maxPerioda01.set(ZERO);
		t5536rec.maxPerioda02.set(ZERO);
		t5536rec.maxPerioda03.set(ZERO);
		t5536rec.maxPerioda04.set(ZERO);
		t5536rec.maxPerioda05.set(ZERO);
		t5536rec.maxPerioda06.set(ZERO);
		t5536rec.maxPerioda07.set(ZERO);
		
		
		
		t5536rec.maxPeriodb01.set(ZERO);
		t5536rec.maxPeriodb02.set(ZERO);
		t5536rec.maxPeriodb03.set(ZERO);
		t5536rec.maxPeriodb04.set(ZERO);
		t5536rec.maxPeriodb05.set(ZERO);
		t5536rec.maxPeriodb06.set(ZERO);
		t5536rec.maxPeriodb07.set(ZERO);
		
		
		
		t5536rec.maxPeriodc01.set(ZERO);
		t5536rec.maxPeriodc02.set(ZERO);
		t5536rec.maxPeriodc03.set(ZERO);
		t5536rec.maxPeriodc04.set(ZERO);
		t5536rec.maxPeriodc05.set(ZERO);
		t5536rec.maxPeriodc06.set(ZERO);
		t5536rec.maxPeriodc07.set(ZERO);
		
		
		t5536rec.maxPeriodd01.set(ZERO);
		t5536rec.maxPeriodd02.set(ZERO);
		t5536rec.maxPeriodd03.set(ZERO);		
		t5536rec.maxPeriodd04.set(ZERO);
		t5536rec.maxPeriodd05.set(ZERO);
		t5536rec.maxPeriodd06.set(ZERO);		
		t5536rec.maxPeriodd07.set(ZERO);
		
		
		
		t5536rec.maxPeriode01.set(ZERO);
		t5536rec.maxPeriode02.set(ZERO);
		t5536rec.maxPeriode03.set(ZERO);
		t5536rec.maxPeriode04.set(ZERO);
		t5536rec.maxPeriode05.set(ZERO);
		t5536rec.maxPeriode06.set(ZERO);
		t5536rec.maxPeriode07.set(ZERO);
		
		
		
		t5536rec.maxPeriodf01.set(ZERO);
		t5536rec.maxPeriodf02.set(ZERO);
		t5536rec.maxPeriodf03.set(ZERO);
		t5536rec.maxPeriodf04.set(ZERO);
		t5536rec.maxPeriodf05.set(ZERO);
		t5536rec.maxPeriodf06.set(ZERO);
		t5536rec.maxPeriodf07.set(ZERO);
		
		
		t5536rec.pcInitUnitsa01.set(ZERO);
		t5536rec.pcInitUnitsa02.set(ZERO);
		t5536rec.pcInitUnitsa03.set(ZERO);
		t5536rec.pcInitUnitsa04.set(ZERO);
		t5536rec.pcInitUnitsa05.set(ZERO);
		t5536rec.pcInitUnitsa06.set(ZERO);
		t5536rec.pcInitUnitsa07.set(ZERO);
		
		
		
		t5536rec.pcInitUnitsb01.set(ZERO);
		t5536rec.pcInitUnitsb02.set(ZERO);
		t5536rec.pcInitUnitsb03.set(ZERO);
		t5536rec.pcInitUnitsb04.set(ZERO);
		t5536rec.pcInitUnitsb05.set(ZERO);
		t5536rec.pcInitUnitsb06.set(ZERO);
		t5536rec.pcInitUnitsb07.set(ZERO);
		
		
		
		t5536rec.pcInitUnitsc01.set(ZERO);
		t5536rec.pcInitUnitsc02.set(ZERO);
		t5536rec.pcInitUnitsc03.set(ZERO);
		t5536rec.pcInitUnitsc04.set(ZERO);
		t5536rec.pcInitUnitsc05.set(ZERO);
		t5536rec.pcInitUnitsc06.set(ZERO);
		t5536rec.pcInitUnitsc07.set(ZERO);
		
		
		
		t5536rec.pcInitUnitsd01.set(ZERO);
		t5536rec.pcInitUnitsd02.set(ZERO);
		t5536rec.pcInitUnitsd03.set(ZERO);
		t5536rec.pcInitUnitsd04.set(ZERO);
		t5536rec.pcInitUnitsd05.set(ZERO);
		t5536rec.pcInitUnitsd06.set(ZERO);
		t5536rec.pcInitUnitsd07.set(ZERO);
		
		
		t5536rec.pcInitUnitse01.set(ZERO);
		t5536rec.pcInitUnitse02.set(ZERO);
		t5536rec.pcInitUnitse03.set(ZERO);
		t5536rec.pcInitUnitse04.set(ZERO);
		t5536rec.pcInitUnitse05.set(ZERO);
		t5536rec.pcInitUnitse06.set(ZERO);
		t5536rec.pcInitUnitse07.set(ZERO);
		
		
		t5536rec.pcInitUnitsf01.set(ZERO);
		t5536rec.pcInitUnitsf02.set(ZERO);
		t5536rec.pcInitUnitsf03.set(ZERO);
		t5536rec.pcInitUnitsf04.set(ZERO);
		t5536rec.pcInitUnitsf05.set(ZERO);
		t5536rec.pcInitUnitsf06.set(ZERO);
		t5536rec.pcInitUnitsf07.set(ZERO);
		
		
		t5536rec.pcUnitsa01.set(ZERO);
		t5536rec.pcUnitsa02.set(ZERO);
		t5536rec.pcUnitsa03.set(ZERO);
		t5536rec.pcUnitsa04.set(ZERO);
		t5536rec.pcUnitsa05.set(ZERO);
		t5536rec.pcUnitsa06.set(ZERO);
		t5536rec.pcUnitsa07.set(ZERO);
		
		
		
		t5536rec.pcUnitsb01.set(ZERO);
		t5536rec.pcUnitsb02.set(ZERO);
		t5536rec.pcUnitsb03.set(ZERO);
		t5536rec.pcUnitsb04.set(ZERO);
		t5536rec.pcUnitsb05.set(ZERO);
		t5536rec.pcUnitsb06.set(ZERO);
		t5536rec.pcUnitsb07.set(ZERO);
		
		
		t5536rec.pcUnitsc01.set(ZERO);
		t5536rec.pcUnitsc02.set(ZERO);
		t5536rec.pcUnitsc03.set(ZERO);
		t5536rec.pcUnitsc04.set(ZERO);
		t5536rec.pcUnitsc05.set(ZERO);
		t5536rec.pcUnitsc06.set(ZERO);
		t5536rec.pcUnitsc07.set(ZERO);
		
		
		t5536rec.pcUnitsd01.set(ZERO);
		t5536rec.pcUnitsd02.set(ZERO);
		t5536rec.pcUnitsd03.set(ZERO);
		t5536rec.pcUnitsd04.set(ZERO);
		t5536rec.pcUnitsd05.set(ZERO);
		t5536rec.pcUnitsd06.set(ZERO);
		t5536rec.pcUnitsd07.set(ZERO);
		
		
		t5536rec.pcUnitse01.set(ZERO);
		t5536rec.pcUnitse02.set(ZERO);
		t5536rec.pcUnitse03.set(ZERO);
		t5536rec.pcUnitse04.set(ZERO);
		t5536rec.pcUnitse05.set(ZERO);
		t5536rec.pcUnitse06.set(ZERO);
		t5536rec.pcUnitse07.set(ZERO);
		
		
		t5536rec.pcUnitsf01.set(ZERO);
		t5536rec.pcUnitsf02.set(ZERO);
		t5536rec.pcUnitsf03.set(ZERO);
		t5536rec.pcUnitsf04.set(ZERO);
		t5536rec.pcUnitsf05.set(ZERO);
		t5536rec.pcUnitsf06.set(ZERO);
		t5536rec.pcUnitsf07.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.billfreqs.set(t5536rec.billfreqs);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.maxPeriodas.set(t5536rec.maxPeriodas);
		sv.maxPeriodbs.set(t5536rec.maxPeriodbs);
		sv.maxPeriodcs.set(t5536rec.maxPeriodcs);
		sv.maxPeriodds.set(t5536rec.maxPeriodds);
		sv.maxPeriodes.set(t5536rec.maxPeriodes);
		sv.maxPeriodfs.set(t5536rec.maxPeriodfs);
		sv.pcInitUnitsas.set(t5536rec.pcInitUnitsas);
		sv.pcInitUnitsbs.set(t5536rec.pcInitUnitsbs);
		sv.pcInitUnitscs.set(t5536rec.pcInitUnitscs);
		sv.pcInitUnitsds.set(t5536rec.pcInitUnitsds);
		sv.pcInitUnitses.set(t5536rec.pcInitUnitses);
		sv.pcInitUnitsfs.set(t5536rec.pcInitUnitsfs);
		sv.pcUnitsas.set(t5536rec.pcUnitsas);
		sv.pcUnitsbs.set(t5536rec.pcUnitsbs);
		sv.pcUnitscs.set(t5536rec.pcUnitscs);
		sv.pcUnitsds.set(t5536rec.pcUnitsds);
		sv.pcUnitses.set(t5536rec.pcUnitses);
		sv.pcUnitsfs.set(t5536rec.pcUnitsfs);
		sv.yotalmth.set(t5536rec.yotalmth);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5536rec.t5536Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.billfreqs,t5536rec.billfreqs)) {
			t5536rec.billfreqs.set(sv.billfreqs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxPeriodas,t5536rec.maxPeriodas)) {
			t5536rec.maxPeriodas.set(sv.maxPeriodas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxPeriodbs,t5536rec.maxPeriodbs)) {
			t5536rec.maxPeriodbs.set(sv.maxPeriodbs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxPeriodcs,t5536rec.maxPeriodcs)) {
			t5536rec.maxPeriodcs.set(sv.maxPeriodcs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxPeriodds,t5536rec.maxPeriodds)) {
			t5536rec.maxPeriodds.set(sv.maxPeriodds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxPeriodes,t5536rec.maxPeriodes)) {
			t5536rec.maxPeriodes.set(sv.maxPeriodes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.maxPeriodfs,t5536rec.maxPeriodfs)) {
			t5536rec.maxPeriodfs.set(sv.maxPeriodfs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcInitUnitsas,t5536rec.pcInitUnitsas)) {
			t5536rec.pcInitUnitsas.set(sv.pcInitUnitsas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcInitUnitsbs,t5536rec.pcInitUnitsbs)) {
			t5536rec.pcInitUnitsbs.set(sv.pcInitUnitsbs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcInitUnitscs,t5536rec.pcInitUnitscs)) {
			t5536rec.pcInitUnitscs.set(sv.pcInitUnitscs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcInitUnitsds,t5536rec.pcInitUnitsds)) {
			t5536rec.pcInitUnitsds.set(sv.pcInitUnitsds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcInitUnitses,t5536rec.pcInitUnitses)) {
			t5536rec.pcInitUnitses.set(sv.pcInitUnitses);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcInitUnitsfs,t5536rec.pcInitUnitsfs)) {
			t5536rec.pcInitUnitsfs.set(sv.pcInitUnitsfs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcUnitsas,t5536rec.pcUnitsas)) {
			t5536rec.pcUnitsas.set(sv.pcUnitsas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcUnitsbs,t5536rec.pcUnitsbs)) {
			t5536rec.pcUnitsbs.set(sv.pcUnitsbs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcUnitscs,t5536rec.pcUnitscs)) {
			t5536rec.pcUnitscs.set(sv.pcUnitscs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcUnitsds,t5536rec.pcUnitsds)) {
			t5536rec.pcUnitsds.set(sv.pcUnitsds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcUnitses,t5536rec.pcUnitses)) {
			t5536rec.pcUnitses.set(sv.pcUnitses);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pcUnitsfs,t5536rec.pcUnitsfs)) {
			t5536rec.pcUnitsfs.set(sv.pcUnitsfs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.yotalmth,t5536rec.yotalmth)) {
			t5536rec.yotalmth.set(sv.yotalmth);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5536pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
