/*
 * File: T5544pt.java
 * Date: 30 August 2009 2:22:28
 * Author: Quipoz Limited
 * 
 * Class transformed from T5544PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5544.
*
*
*****************************************************************
* </pre>
*/
public class T5544pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine001, 33, FILLER).init("Unit Switch Methods                   S5544");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 25, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(52);
	private FixedLengthStringData filler9 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(42).isAPartOf(wsaaPrtLine004, 9, FILLER).init("Switch Basis:   Bid To Bid:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 51);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(52);
	private FixedLengthStringData filler11 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine005, 25, FILLER).init("Bid To Offer:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 51);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(71);
	private FixedLengthStringData filler13 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine006, 25, FILLER).init("Bid To Discounted Offer:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 51);
	private FixedLengthStringData filler15 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 52, FILLER).init("  Discount %");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 65).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(52);
	private FixedLengthStringData filler16 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine007, 25, FILLER).init("Offer To Offer:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 51);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(52);
	private FixedLengthStringData filler18 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine008, 25, FILLER).init("Bid To Fund:");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 51);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(26);
	private FixedLengthStringData filler20 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler21 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 9, FILLER).init("Now/Deferred:");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 25);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(78);
	private FixedLengthStringData filler22 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine010, 9, FILLER).init("Number of Free Switches:");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 34).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine010, 37, FILLER).init("  Per Policy Year(s):");
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine010, 59).setPattern("Z");
	private FixedLengthStringData filler25 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine010, 60, FILLER).init("  Carry Forward:");
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine010, 77).setPattern("Z");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(40);
	private FixedLengthStringData filler26 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler27 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 9, FILLER).init("Fee:   Flat:");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 22).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(80);
	private FixedLengthStringData filler28 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 19, FILLER).init("%:");
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 22).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 28, FILLER).init("   Min:");
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 36).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 54, FILLER).init("   Max:");
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 62).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5544rec t5544rec = new T5544rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5544pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5544rec.t5544Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5544rec.btobid);
		fieldNo008.set(t5544rec.btooff);
		fieldNo009.set(t5544rec.btodisc);
		fieldNo011.set(t5544rec.otooff);
		fieldNo013.set(t5544rec.nowDeferInd);
		fieldNo014.set(t5544rec.noOfFreeSwitches);
		fieldNo015.set(t5544rec.ppyears);
		fieldNo016.set(t5544rec.cfrwd);
		fieldNo017.set(t5544rec.ffamt);
		fieldNo018.set(t5544rec.feepc);
		fieldNo019.set(t5544rec.feemin);
		fieldNo020.set(t5544rec.feemax);
		fieldNo012.set(t5544rec.bidToFund);
		fieldNo010.set(t5544rec.discountOfferPercent);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
