/*
 * File: P5426.java
 * Date: 30 August 2009 0:24:54
 * Author: Quipoz Limited
 * 
 * Class transformed from P5426.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.unitlinkedprocessing.dataaccess.VprcupdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupeTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S5426ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*   P5426 - Modify Unit Bare Prices.
*
*   This program allows the user to modify the unit bare prices
*   that entered from P5413.
*   These prices are identified by Job Number and Effective Date.
*
*   For each fund there must be a non-zero entry of accumulation
*   unit. Matching also available by entering 1, 2 ,3  or 4
*   characters. Consequently the program will display the fund
*   that matches these characters as the first fund in the
*   subfile.
*
*   It is called from the Unit Linked Entry submenu(P5412) by
*   means of selecting the appropriate option.
*
*   The main program flow is as follows:-
*
*        Initialisation.
*
*        Read VPRCUPD and select records, then load to the subfile.
*        This is to be repeated until subfile size equals to the
*        screen size or end of file.
*
*        Check screen status.
*        If ROLU
*            Display next page of the subfile by reading VPRCUPD and
*            loading as above.
*        If not ROLU
*            Validate the screen fields until there is no error.
*
*        Once the prices are captured, write two records(Initial and
*        Accumulation) for each fund that has prices.
*
*        Return control to calling program.
*
*
*
*****************************************************************
* </pre>
*/
public class P5426 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5426");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaNormError = "N";
	private FixedLengthStringData wsaaLastVprc = new FixedLengthStringData(64);
	private ZonedDecimalData wsaaPage = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaPartFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPartF1 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 0);
	private FixedLengthStringData wsaaPartF2 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 1);
	private FixedLengthStringData wsaaPartF3 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 2);
	private FixedLengthStringData wsaaPartF4 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 3);

	private FixedLengthStringData wsaaItemFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaItemF1 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 0);
	private FixedLengthStringData wsaaItemF2 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 1);
	private FixedLengthStringData wsaaItemF3 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 2);
	private FixedLengthStringData wsaaItemF4 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 3);
	private FixedLengthStringData wsaaSearchFund = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevVirtualFund = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaRecObtainedFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaRecObtained = new Validator(wsaaRecObtainedFlag, "Y");

	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Validator wsaaRolu = new Validator(wsaaFunctionKey, "ROLU");
	private Validator wsaaCalc = new Validator(wsaaFunctionKey, "CALC");
	private ZonedDecimalData wsaaTolerance = new ZonedDecimalData(8, 5).setUnsigned();
	private String wsaaToleranceFlag = "";
	private ZonedDecimalData wsaaTdayIntDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaTdayExtDate = new FixedLengthStringData(10);
		/* ERRORS */
	private String f351 = "F351";
	private String g037 = "G037";
	private String h025 = "H025";
	private String h094 = "H094";
	private String h095 = "H095";
		/* TABLES */
	private String t5515 = "T5515";
		/* FORMATS */
	private String vprcupdrec = "VPRCUPDREC";
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Getdescrec getdescrec = new Getdescrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5515rec t5515rec = new T5515rec();
		/*Unit Price file*/
	private VprcupdTableDAM vprcupdIO = new VprcupdTableDAM();
		/*Unit Price File (For checking tolerance)*/
	private VprcupeTableDAM vprcupeIO = new VprcupeTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S5426ScreenVars sv = ScreenProgram.getScreenVars( S5426ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		writeToSubfile1300, 
		nextRecord1400, 
		setMoreSign1400, 
		preExit, 
		validateSubfile2100, 
		rollUp2600, 
		writeToSubfile2700, 
		nextRecord2800, 
		setMoreSign2700, 
		exit2090, 
		moreUpdateRec3200, 
		exit3900, 
		callVprcupd5120, 
		exit5190, 
		nextRecord6050, 
		exit6090, 
		init7200, 
		exit7900, 
		exit8900, 
		checkInitut14200, 
		exit14900
	}

	public P5426() {
		super();
		screenVars = sv;
		new ScreenModel("S5426", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1100();
					loadSubfile1200();
				}
				case writeToSubfile1300: {
					writeToSubfile1300();
				}
				case nextRecord1400: {
					nextRecord1400();
				}
				case setMoreSign1400: {
					setMoreSign1400();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.initut.set(ZERO);
		sv.acumut.set(ZERO);
		sv.tranno.set(ZERO);
		wsaaToleranceFlag = "N";
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTdayIntDate.set(datcon1rec.intDate);
		sv.company.set(wsspcomn.company);
		sv.effdate.set(wsspuprc.uprcEffdate);
		sv.jobno.set(wsspuprc.uprcJobno);
		if (isNE(wsspuprc.uprcVrtfnd,SPACES)) {
			wsaaPartFund.set(wsspuprc.uprcVrtfnd);
			wsaaSearchFund.set("Y");
		}
		else {
			wsaaSearchFund.set("N");
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S5426", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void loadSubfile1200()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		//performance improvement -- < Niharika Modi >
		//vprcupdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//vprcupdIO.setFitKeysSearch( "COMPANY","EFFDATE","JOBNO");
		
		getNextVprcupdRec5100();
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1400);
		}
		if (isEQ(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.nextRecord1400);
		}
		if (isEQ(wsaaSearchFund,"Y")) {
			searchFund8000();
			if (isEQ(wsaaSearchFund,"Y")) {
				goTo(GotoLabel.nextRecord1400);
			}
		}
	}

protected void writeToSubfile1300()
	{
		wsaaRecObtainedFlag.set("N");
		moveFieldsToSubfile6000();
		getDescription6100();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5426", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void nextRecord1400()
	{
		if (!wsaaRecObtained.isTrue()) {
			vprcupdIO.setFunction(varcom.nextr);
			getNextVprcupdRec5100();
		}
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1400);
		}
		if (isEQ(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.nextRecord1400);
		}
		if (isEQ(wsaaSearchFund,"Y")) {
			searchFund8000();
			if (isEQ(wsaaSearchFund,"Y")) {
				goTo(GotoLabel.nextRecord1400);
			}
		}
		if (isEQ(scrnparams.subfileRrn,sv.subfilePage)) {
			goTo(GotoLabel.setMoreSign1400);
		}
		goTo(GotoLabel.writeToSubfile1300);
	}

protected void setMoreSign1400()
	{
		if (isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if ((isEQ(wsaaNormError,"N"))
		&& (isEQ(wsspcomn.edterror,"Y"))
		&& (!wsaaRolu.isTrue())
		&& (isEQ(wsaaToleranceFlag,"N"))
		&& (!wsaaCalc.isTrue())) {
			wsaaToleranceFlag = "Y";
			scrnparams.errorCode.set(h094);
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case validateSubfile2100: {
					validateSubfile2100();
				}
				case rollUp2600: {
					rollUp2600();
				}
				case writeToSubfile2700: {
					writeToSubfile2700();
				}
				case nextRecord2800: {
					nextRecord2800();
				}
				case setMoreSign2700: {
					setMoreSign2700();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaNormError = "N";
		if (isNE(scrnparams.function,varcom.prot)) {
			wsaaFunctionKey.set(scrnparams.statuz);
		}
		if (wsaaCalc.isTrue()) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5426", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.rollUp2600);
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(sv.unitVirtualFund,itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.unitType,"I")) {
			if (isNE(sv.acumut,ZERO)) {
				sv.acumutErr.set(h025);
				sv.initutErr.set(SPACES);
				wsaaNormError = "Y";
				wsspcomn.edterror.set("Y");
			}
		}
		else {
			if (isEQ(t5515rec.unitType,"A")) {
				if (isNE(sv.initut,ZERO)) {
					sv.initutErr.set(h025);
					sv.acumutErr.set(SPACES);
					wsaaNormError = "Y";
					wsspcomn.edterror.set("Y");
				}
			}
			else {
				if (isNE(sv.initut,ZERO)
				&& isEQ(sv.acumut,ZERO)) {
					sv.acumutErr.set(f351);
					sv.initutErr.set(SPACES);
					wsspcomn.edterror.set("Y");
					wsaaNormError = "Y";
				}
			}
		}
		if (isEQ(wsaaToleranceFlag,"N")) {
			if (isNE(wsaaNormError,"Y")) {
				checkTolerances14000();
			}
		}
		sv.updateDate.set(wsaaTdayIntDate);
		sv.tranno.add(1);
		scrnparams.function.set(varcom.supd);
		processScreen("S5426", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsaaNormError = "Y";
		}
		goTo(GotoLabel.validateSubfile2100);
	}

protected void rollUp2600()
	{
		if (!wsaaRolu.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
		wsspcomn.edterror.set("Y");
	}

protected void writeToSubfile2700()
	{
		moveFieldsToSubfile6000();
		getDescription6100();
		scrnparams.function.set(varcom.sadd);
		callScreenIo5000();
	}

protected void nextRecord2800()
	{
		getNextVprcupdRec5100();
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign2700);
		}
		if (isEQ(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.nextRecord2800);
		}
		compute(wsaaPage, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		wsaaRem.setRemainder(wsaaPage);
		if (isEQ(wsaaRem,ZERO)) {
			goTo(GotoLabel.setMoreSign2700);
		}
		goTo(GotoLabel.writeToSubfile2700);
	}

protected void setMoreSign2700()
	{
		if (isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		wsaaNormError = "Y";
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateDatabase3100();
				}
				case moreUpdateRec3200: {
					moreUpdateRec3200();
				}
				case exit3900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3100()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void moreUpdateRec3200()
	{
		processScreen("S5426", sv);
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		writeToVprcupdFile7000();
		goTo(GotoLabel.moreUpdateRec3200);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callScreenIo5000()
	{
		/*PARA*/
		processScreen("S5426", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void getNextVprcupdRec5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case callVprcupd5120: {
					callVprcupd5120();
				}
				case exit5190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callVprcupd5120()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit5190);
		}
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getCompany(),wsspcomn.company)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)) {
			vprcupdIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callVprcupd5120);
		}
	}

protected void moveFieldsToSubfile6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para6000();
				}
				case nextRecord6050: {
					nextRecord6050();
				}
				case exit6090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6000()
	{
		if (isNE(vprcupdIO.getUnitType(),"A")
		&& isNE(vprcupdIO.getUnitType(),"I")) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(vprcupdIO.getUnitVirtualFund());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(vprcupdIO.getUnitVirtualFund(),itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.unitType,"A")) {
			sv.initutOut[varcom.pr.toInt()].set("Y");
			sv.acumutOut[varcom.pr.toInt()].set(SPACES);
		}
		if (isEQ(t5515rec.unitType,"I")) {
			sv.acumutOut[varcom.pr.toInt()].set("Y");
			sv.initutOut[varcom.pr.toInt()].set(SPACES);
		}
		if (isEQ(t5515rec.unitType,"B")) {
			sv.acumutOut[varcom.pr.toInt()].set(SPACES);
			sv.initutOut[varcom.pr.toInt()].set(SPACES);
		}
		wsaaPrevVirtualFund.set(vprcupdIO.getUnitVirtualFund());
		sv.unitVirtualFund.set(vprcupdIO.getUnitVirtualFund());
		sv.updateDate.set(vprcupdIO.getUpdateDate());
		sv.tranno.set(vprcupdIO.getTranno());
		if (isEQ(vprcupdIO.getUnitType(),"I")) {
			sv.acumut.set(ZERO);
			sv.initut.set(vprcupdIO.getUnitBarePrice());
			goTo(GotoLabel.exit6090);
		}
		else {
			if (isEQ(vprcupdIO.getUnitType(),"A")) {
				sv.acumut.set(vprcupdIO.getUnitBarePrice());
			}
		}
	}

protected void nextRecord6050()
	{
		vprcupdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit6090);
		}
		if (isEQ(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.nextRecord6050);
		}
		if (isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getCompany(),wsspcomn.company)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)) {
			goTo(GotoLabel.exit6090);
		}
		if (isNE(vprcupdIO.getUnitType(),"A")
		&& isNE(vprcupdIO.getUnitType(),"I")) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getUnitVirtualFund(),wsaaPrevVirtualFund)) {
			sv.initut.set(ZERO);
			wsaaRecObtainedFlag.set("Y");
		}
		else {
			if (isEQ(vprcupdIO.getUnitType(),"I")) {
				sv.initut.set(vprcupdIO.getUnitBarePrice());
			}
			else {
				if (isEQ(vprcupdIO.getUnitType(),"A")) {
					sv.initut.set(ZERO);
					wsaaRecObtainedFlag.set("Y");
				}
			}
		}
	}

protected void getDescription6100()
	{
		para6100();
	}

protected void para6100()
	{
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsspcomn.company);
		wsaaItemkey.itemItemtabl.set(t5515);
		wsaaItemkey.itemItemitem.set(wsaaPrevVirtualFund);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(wsspcomn.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isEQ(getdescrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(getdescrec.statuz);
			fatalError600();
		}
		if (isNE(getdescrec.statuz,varcom.oK)) {
			sv.ffnddsc.set(SPACES);
		}
		else {
			sv.ffnddsc.set(getdescrec.longdesc);
		}
	}

protected void writeToVprcupdFile7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					accum7100();
				}
				case init7200: {
					init7200();
				}
				case exit7900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void accum7100()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.readh);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupdIO.setUnitType("A");
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(vprcupdIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.init7200);
			}
			else {
				syserrrec.statuz.set(vprcupdIO.getStatuz());
				syserrrec.params.set(vprcupdIO.getParams());
				fatalError600();
			}
		}
		if (isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getCompany(),wsspcomn.company)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| isNE(vprcupdIO.getUnitType(),"A")
		|| isNE(vprcupdIO.getUnitVirtualFund(),sv.unitVirtualFund)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.acumut,0)) {
			vprcupdIO.setFunction(varcom.delet);
		}
		else {
			vprcupdIO.setFunction(varcom.rewrt);
		}
		vprcupdIO.setUnitBarePrice(sv.acumut);
		vprcupdIO.setUpdateDate(sv.updateDate);
		vprcupdIO.setTranno(sv.tranno);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			fatalError600();
		}
	}

protected void init7200()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.readh);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupdIO.setUnitType("I");
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(vprcupdIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.exit7900);
			}
			else {
				syserrrec.statuz.set(vprcupdIO.getStatuz());
				syserrrec.params.set(vprcupdIO.getParams());
				fatalError600();
			}
		}
		if (isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getCompany(),wsspcomn.company)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| isNE(vprcupdIO.getUnitType(),"I")
		|| isNE(vprcupdIO.getUnitVirtualFund(),sv.unitVirtualFund)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.initut,0)) {
			vprcupdIO.setFunction(varcom.delet);
		}
		else {
			vprcupdIO.setFunction(varcom.rewrt);
		}
		vprcupdIO.setUnitBarePrice(sv.initut);
		vprcupdIO.setUpdateDate(sv.updateDate);
		vprcupdIO.setTranno(sv.tranno);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			fatalError600();
		}
	}

protected void searchFund8000()
	{
		try {
			para8010();
		}
		catch (GOTOException e){
		}
	}

protected void para8010()
	{
		wsaaItemFund.set(vprcupdIO.getUnitVirtualFund());
		if (isNE(wsaaPartF1,wsaaItemF1)) {
			if (isLT(wsaaPartF1,wsaaItemF1)) {
				/*NEXT_SENTENCE*/
			}
			else {
				goTo(GotoLabel.exit8900);
			}
		}
		else {
			if (isNE(wsaaPartF2,SPACES)) {
				if (isNE(wsaaPartF2,wsaaItemF2)) {
					if (isLT(wsaaPartF2,wsaaItemF2)) {
						/*NEXT_SENTENCE*/
					}
					else {
						goTo(GotoLabel.exit8900);
					}
				}
				else {
					if (isNE(wsaaPartF3,SPACES)) {
						if (isNE(wsaaPartF3,wsaaItemF3)) {
							if (isLT(wsaaPartF3,wsaaItemF3)) {
								/*NEXT_SENTENCE*/
							}
							else {
								goTo(GotoLabel.exit8900);
							}
						}
						else {
							if (isNE(wsaaPartF4,SPACES)) {
								if (isNE(wsaaPartF4,wsaaItemF4)) {
									if (isLT(wsaaPartF4,wsaaItemF4)) {
										/*NEXT_SENTENCE*/
									}
									else {
										goTo(GotoLabel.exit8900);
									}
								}
							}
						}
					}
				}
			}
		}
		wsaaSearchFund.set("N");
	}

protected void checkTolerances14000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start14100();
					checkAccumut14100();
				}
				case checkInitut14200: {
					checkInitut14200();
				}
				case exit14900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start14100()
	{
		if (isEQ(t5515rec.tolerance,0)) {
			goTo(GotoLabel.exit14900);
		}
		vprcupeIO.setCompany(wsspcomn.company);
		vprcupeIO.setValidflag("1");
		vprcupeIO.setEffdate(varcom.vrcmMaxDate);
		vprcupeIO.setJobno(99999999);
		vprcupeIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupeIO.setUnitType("A");
		vprcupeIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.checkInitut14200);
		}
	}

protected void checkAccumut14100()
	{
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBarePrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.acumut, 5)
		&& isGT(sv.acumut,(add(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))
		|| (setPrecision(sv.acumut, 5)
		&& isLT(sv.acumut,(sub(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))) {
			sv.acumutErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
		vprcupeIO.setUnitType("I");
		vprcupeIO.setFunction(varcom.begn);
	}

protected void checkInitut14200()
	{
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit14900);
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBarePrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.initut, 5)
		&& isGT(sv.initut,(add(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))
		|| (setPrecision(sv.initut, 5)
		&& isLT(sv.initut,(sub(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))) {
			sv.initutErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
	}
}
