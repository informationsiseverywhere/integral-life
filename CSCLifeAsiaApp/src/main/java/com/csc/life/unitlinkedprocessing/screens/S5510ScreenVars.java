package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5510
 * @version 1.0 generated on 30/08/09 06:41
 * @author Quipoz
 */
public class S5510ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(566);
	public FixedLengthStringData dataFields = new FixedLengthStringData(150).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData unitPremPercents = new FixedLengthStringData(50).isAPartOf(dataFields, 60);
	public ZonedDecimalData[] unitPremPercent = ZDArrayPartOfStructure(10, 5, 2, unitPremPercents, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(unitPremPercents, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitPremPercent01 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData unitPremPercent02 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,5);
	public ZonedDecimalData unitPremPercent03 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,10);
	public ZonedDecimalData unitPremPercent04 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData unitPremPercent05 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,20);
	public ZonedDecimalData unitPremPercent06 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,25);
	public ZonedDecimalData unitPremPercent07 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData unitPremPercent08 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,35);
	public ZonedDecimalData unitPremPercent09 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,40);
	public ZonedDecimalData unitPremPercent10 = DD.uprmpc.copyToZonedDecimal().isAPartOf(filler,45);
	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(40).isAPartOf(dataFields, 110);
	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(10, 4, unitVirtualFunds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
	public FixedLengthStringData unitVirtualFund01 = DD.vrtfnd.copy().isAPartOf(filler1,0);
	public FixedLengthStringData unitVirtualFund02 = DD.vrtfnd.copy().isAPartOf(filler1,4);
	public FixedLengthStringData unitVirtualFund03 = DD.vrtfnd.copy().isAPartOf(filler1,8);
	public FixedLengthStringData unitVirtualFund04 = DD.vrtfnd.copy().isAPartOf(filler1,12);
	public FixedLengthStringData unitVirtualFund05 = DD.vrtfnd.copy().isAPartOf(filler1,16);
	public FixedLengthStringData unitVirtualFund06 = DD.vrtfnd.copy().isAPartOf(filler1,20);
	public FixedLengthStringData unitVirtualFund07 = DD.vrtfnd.copy().isAPartOf(filler1,24);
	public FixedLengthStringData unitVirtualFund08 = DD.vrtfnd.copy().isAPartOf(filler1,28);
	public FixedLengthStringData unitVirtualFund09 = DD.vrtfnd.copy().isAPartOf(filler1,32);
	public FixedLengthStringData unitVirtualFund10 = DD.vrtfnd.copy().isAPartOf(filler1,36);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 150);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData uprmpcsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] uprmpcErr = FLSArrayPartOfStructure(10, 4, uprmpcsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(uprmpcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData uprmpc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData uprmpc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData uprmpc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData uprmpc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData uprmpc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData uprmpc06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData uprmpc07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData uprmpc08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData uprmpc09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData uprmpc10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData vrtfndsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] vrtfndErr = FLSArrayPartOfStructure(10, 4, vrtfndsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(vrtfndsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData vrtfnd01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData vrtfnd02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData vrtfnd03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData vrtfnd04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData vrtfnd05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData vrtfnd06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData vrtfnd07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData vrtfnd08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData vrtfnd09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData vrtfnd10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 254);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData uprmpcsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] uprmpcOut = FLSArrayPartOfStructure(10, 12, uprmpcsOut, 0);
	public FixedLengthStringData[][] uprmpcO = FLSDArrayPartOfArrayStructure(12, 1, uprmpcOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(uprmpcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] uprmpc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] uprmpc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] uprmpc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] uprmpc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] uprmpc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] uprmpc06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] uprmpc07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] uprmpc08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] uprmpc09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] uprmpc10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData vrtfndsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(10, 12, vrtfndsOut, 0);
	public FixedLengthStringData[][] vrtfndO = FLSDArrayPartOfArrayStructure(12, 1, vrtfndOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(vrtfndsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] vrtfnd01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] vrtfnd02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] vrtfnd03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] vrtfnd04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] vrtfnd05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] vrtfnd06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] vrtfnd07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] vrtfnd08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] vrtfnd09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] vrtfnd10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5510screenWritten = new LongData(0);
	public LongData S5510protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5510ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itmfrmOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itmtoOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc02Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc03Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc06Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc07Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc08Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc09Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uprmpc10Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, unitVirtualFund01, unitVirtualFund02, unitVirtualFund03, unitVirtualFund04, unitVirtualFund05, unitVirtualFund06, unitVirtualFund07, unitVirtualFund08, unitVirtualFund09, unitVirtualFund10, unitPremPercent01, unitPremPercent02, unitPremPercent03, unitPremPercent04, unitPremPercent05, unitPremPercent06, unitPremPercent07, unitPremPercent08, unitPremPercent09, unitPremPercent10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, vrtfnd01Out, vrtfnd02Out, vrtfnd03Out, vrtfnd04Out, vrtfnd05Out, vrtfnd06Out, vrtfnd07Out, vrtfnd08Out, vrtfnd09Out, vrtfnd10Out, uprmpc01Out, uprmpc02Out, uprmpc03Out, uprmpc04Out, uprmpc05Out, uprmpc06Out, uprmpc07Out, uprmpc08Out, uprmpc09Out, uprmpc10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, vrtfnd01Err, vrtfnd02Err, vrtfnd03Err, vrtfnd04Err, vrtfnd05Err, vrtfnd06Err, vrtfnd07Err, vrtfnd08Err, vrtfnd09Err, vrtfnd10Err, uprmpc01Err, uprmpc02Err, uprmpc03Err, uprmpc04Err, uprmpc05Err, uprmpc06Err, uprmpc07Err, uprmpc08Err, uprmpc09Err, uprmpc10Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5510screen.class;
		protectRecord = S5510protect.class;
	}

}
