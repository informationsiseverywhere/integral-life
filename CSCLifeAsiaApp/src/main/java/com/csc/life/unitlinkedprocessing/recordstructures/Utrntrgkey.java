package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:51
 * Description:
 * Copybook name: UTRNTRGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrntrgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrntrgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrntrgKey = new FixedLengthStringData(64).isAPartOf(utrntrgFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrntrgChdrcoy = new FixedLengthStringData(1).isAPartOf(utrntrgKey, 0);
  	public FixedLengthStringData utrntrgChdrnum = new FixedLengthStringData(8).isAPartOf(utrntrgKey, 1);
  	public PackedDecimalData utrntrgTranno = new PackedDecimalData(5, 0).isAPartOf(utrntrgKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(utrntrgKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrntrgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrntrgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}