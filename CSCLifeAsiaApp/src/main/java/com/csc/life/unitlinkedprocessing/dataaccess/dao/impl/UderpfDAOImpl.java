package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UderpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Uderpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UderpfDAOImpl extends BaseDAOImpl<Uderpf> implements UderpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UderpfDAOImpl.class);

	public void deleteUderpfUTRNRecord() {
		String sql = "DELETE FROM UDER WHERE RECORDTYPE='UTRN' ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteUderpfUTRNRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public void insertUderpfRecord(List<Uderpf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO UDERPF(AGE,RECORDTYPE,LAPIND,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,VRTFND,UNITYP,TRANNO,BATCTRCDE,CNTAMNT,CNTCURR,FUNDAMNT,FNDCURR,FUNDRATE,TRIGER,RELRECNO,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		
		try {
			for (Uderpf u : urList) {
				int i = 1;
				ps.setInt(i++, u.getAge());
				ps.setString(i++, u.getRecordtype());
				ps.setString(i++, u.getLapind());
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getLife());
				ps.setString(i++, u.getCoverage());
				ps.setString(i++, u.getRider());
				ps.setInt(i++, u.getPlanSuffix());
				ps.setString(i++, u.getUnitVirtualFund());
				ps.setString(i++, u.getUnitType());
				ps.setInt(i++, u.getTranno());
				ps.setString(i++, u.getBatctrcde());
				ps.setBigDecimal(i++, u.getContractAmount());
				ps.setString(i++, u.getCntcurr());
				ps.setBigDecimal(i++, u.getFundAmount());
				ps.setString(i++, u.getFundCurrency());
				ps.setBigDecimal(i++, u.getFundRate());
				ps.setString(i++, u.getTriggerModule());
				ps.setLong(i++, u.getRelativeRecordNo());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertUderpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public void deleteUderpfRecordByRC(String recordtype) {
		String sql = "DELETE FROM UDER WHERE RECORDTYPE=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			ps.setString(1, recordtype);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteUderpfRecordByRC()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
}