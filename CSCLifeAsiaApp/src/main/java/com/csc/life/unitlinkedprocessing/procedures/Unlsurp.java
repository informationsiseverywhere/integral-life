/*
 * File: Unlsurp.java
 * Date: 30 August 2009 2:52:33
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLSURP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Full Surrender Processing Method.
* ---------------------------------
*
* This  program  is  an  item  entry  on  T6598,  the  Surrender  claim
* subroutine  method  table. This method is used in order to update the
* Unit Transactions.
*
* The following is the linkage information passed to this subroutine:
*
*            - company
*            - contract header number
*            - suffix
*            - life number
*            - joint-life number
*            - coverage
*            - rider
*            - crtable
*            - effective date
*            - estimated value
*            - actual value
*            - currency
*            - element code
*            - type code
*            - status
*            - batch key
*
* PROCESSING.
* ----------
*
* For each Unit Summary record (UTRS) for  this  contract  perform  the
* following:-
*   If initial units
*
*     look-up  discount  table  T6655 for svp factor, divide the factor
*     found by the value in the divisor field .  Write  a  UTRN  record
*     with the following data for the (pending surrender sub-account):
*
*       Processing Seq No. -  from T6647
*       Surrender %        -  100
*       Amount (signed)    -  0
*       Effective date     -  Effective-date of the surrender
*       Bid/Offer Ind      -  'B'
*       Now/Deferred Ind   -  T6647 entry (deallocation field)
*       Price              -  0
*       Fund               -  linkage (element code)
*       Number of Units    -  0
*       Contract No.       -  linkage
*       Contract Type      -  linkage
*       Sub-Account Code   -  T5645 entry for this sub-account
*       Sub-Account Type   -  ditto
*       G/L key            -  ditto
*       Trigger module     -  T5687/T6598 entry
*       Trigger key        -  Contract no./Suffix/
*                             Life/Coverage/Rider
*       Surr Value Penalty -  as calculated from table
* If accumulation units
*     Write  a  UTRN  record  with  the following data for the (pending
*     surrender sub-account):-
*
*       Processing Seq No. -  from T6647
*       Surrender %        -  100
*       Amount (signed)    -  0
*       Effective date     -  Effective-date of the surrender
*       Bid/Offer Ind      -  'B'
*       Now/Deferred Ind   -  T6647 entry (deallocation field)
*       Price              -  0
*       Fund               -  linkage (element code)
*       Number of Units    -  0
*       Contract No.       -  linkage
*       Contract Type      -  linkage
*       Sub-Account Code   -  T5645 entry for this sub-account
*       Sub-Account Type   -  ditto
*       G/L key            -  ditto
*       Trigger module     -  T5687/T6598 entry
*       Trigger key        -  Contract no./Suffix/
*                             Life/Coverage/Rider
*       Surr Value Penalty -  1
*****************************************************************
* </pre>
*/
public class Unlsurp extends COBOLConvCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Unlsurp.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected String wsaaSubr = "UNLSURP";
	private String e308 = "E308";
	private String f294 = "F294";
	private String h115 = "H115";
	private String g029 = "G029";
		/* TABLES */
	private String t6647 = "T6647";
	private String t5515 = "T5515";
	private String t5645 = "T5645";
	private String t5687 = "T5687";
	private String t6598 = "T6598";
	private String t5688 = "T5688";
	private String t6659 = "T6659";
	private String itdmrec = "ITEMREC";
	private String utrnrec = "UTRNREC";
	private String hitrrec = "HITRREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaTrigger = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaTriggerChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 0);
	private FixedLengthStringData wsaaTriggerChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTrigger, 1);
	private PackedDecimalData wsaaTriggerSuffix = new PackedDecimalData(5, 0).isAPartOf(wsaaTrigger, 9);
	private PackedDecimalData wsaaTriggerTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTrigger, 12);
	private FixedLengthStringData wsaaTriggerCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 15);
	private FixedLengthStringData wsaaTriggerRider = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 17);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 19, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);

	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();
	private Annprocrec annprocrec = new Annprocrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	protected DescTableDAM descIO = new DescTableDAM();
		/*Interest Bearing Transaction Details*/
	private HitrTableDAM hitrIO = new HitrTableDAM();
		/*Interest Bearing Transaction Summary*/
	private HitsTableDAM hitsIO = new HitsTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected Surpcpy surpcpy = new Surpcpy();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5515rec t5515rec = new T5515rec();
	protected T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6598rec t6598rec = new T6598rec();
	private T6647rec t6647rec = new T6647rec();
	private T6659rec t6659rec = new T6659rec();
		/*UTRN LOGICAL VIEW.*/
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
		/*Unit Transactions for Claims*/
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
		/*United linked details - surrender*/
	protected Varcom varcom = new Varcom();
	protected Batckey wsaaBatckey = new Batckey();
	private Zutrpf zutrpf = new Zutrpf();//ILIFE-5452
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);//ILIFE-5452
	private List<Zutrpf> zutrpfList = null; //ILIFE-5452
	//ILB-1097 start
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private Utrspf utrssur = new Utrspf();
	//ILB-1097 end

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit220, 
		nextRecord253, 
		a190Exit, 
		a290Exit, 
		exit9090
	}

	public Unlsurp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		initialize100();
		if (isEQ(surpcpy.type,"A")
		|| isEQ(surpcpy.type,"I")) {
			mainProcessing200();
		}
		if (isEQ(surpcpy.type,"D")) {
			a200InterestBearing();
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialize100()
	{
		go101();
	}

protected void go101()
	{
		surpcpy.status.set("****");
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(surpcpy.batckey);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTime.set(getCobolTime());
		if (isEQ(surpcpy.status,"****")) {
			readTabT6647120();
		}
		if (isEQ(surpcpy.status,"****")) {
			readTabT5645140();
		}
		if (isEQ(surpcpy.status,"****")) {
			readTabT5688145();
		}
	}

protected void readTabT6647120()
	{
		read121();
	}

protected void read121()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaTabBatckey.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(surpcpy.cnttype);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT6647Key,itdmIO.getItemitem())
		|| isNE(surpcpy.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t6647)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			surpcpy.status.set(h115);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItemcoy(surpcpy.chdrcoy);
			itdmIO.setItemtabl(t6647);
			fatalError9000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTabT5645140()
	{
		read141();
	}

protected void read141()
	{
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(surpcpy.chdrcoy);
		descIO.setLanguage(surpcpy.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),"****")
		&& isNE(descIO.getStatuz(),"MRNF")) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
	}

protected void readTabT5688145()
	{
		read146();
	}

protected void read146()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(surpcpy.cnttype);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN ");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),surpcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemitem(surpcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void mainProcessing200()
	{
		try {
			para201();
		}
		catch (GOTOException e){
		}
	}

protected void para201()
	{
		if (isEQ(surpcpy.estimatedVal,0)) {
			goTo(GotoLabel.exit220);
		}
		
		if (isNE(t6647rec.unitStatMethod,SPACES)) {
			a100ReadT6659();
		}
		if (isEQ(surpcpy.planSuffix,ZERO)) {
			writeUtrnWholePlan280();
			goTo(GotoLabel.exit220);
		}
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setPlanSuffix(surpcpy.planSuffix);
		utrsclmIO.setLife(surpcpy.life);
		utrsclmIO.setUnitVirtualFund(surpcpy.fund);
		utrsclmIO.setUnitType(surpcpy.type);
		utrsclmIO.setChdrcoy(surpcpy.chdrcoy);
		utrsclmIO.setChdrnum(surpcpy.chdrnum);
		utrsclmIO.setCoverage(surpcpy.coverage);
		utrsclmIO.setRider(surpcpy.rider);
		utrsclmIO.setFunction("READR");
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(),"****")) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		getTrigger500();
		setupUtrns600();
	}

protected void writeUtrnWholePlan280()
	{
		para1201();
	}

protected void para1201()
	{
		//ILB-1097 start
	
		utrssur = new Utrspf();
		utrssur.setLife(surpcpy.life.toString());
		utrssur.setUnitVirtualFund(surpcpy.fund.toString());
		utrssur.setUnitType(surpcpy.type.toString());
		utrssur.setChdrcoy(surpcpy.chdrcoy.toString());
		utrssur.setChdrnum(surpcpy.chdrnum.toString());
		utrssur.setLife(surpcpy.life.toString());
		utrssur.setCoverage(surpcpy.coverage.toString());
		utrssur.setRider(surpcpy.rider.toString());
		List<Utrspf> utrssurList = utrspfDAO.searchUtrssurRecord(utrssur);
		if(!utrssurList.isEmpty()) {
			for(Utrspf utrs:utrssurList) {
				utrssur = utrs;
				if (isEQ(utrssur.getCurrentDunitBal(),0)) {
					continue;
				}
				getTrigger500();
				setupUtrns600();
			}
		}
		//ILB-1097 end
	}


protected void getTrigger500()
	{
		read531();
	}

protected void read531()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(surpcpy.crtable);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),surpcpy.crtable)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			surpcpy.status.set(f294);
			itdmIO.setItemcoy(surpcpy.chdrcoy);
			itdmIO.setItemtabl(t5687);
			itdmIO.setItemitem(surpcpy.crtable);
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setItempfx("IT");
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void setupUtrns600()
	{
		go601();
	}

protected void go601()
	{
		utrnIO.setParams(SPACES);
		utrnIO.setTranno(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setProcSeqNo(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setSvp(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setCrComDate(ZERO);
		utrnIO.setChdrcoy(surpcpy.chdrcoy);
		wsaaTriggerChdrcoy.set(surpcpy.chdrcoy);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		wsaaTriggerTranno.set(surpcpy.tranno);
		utrnIO.setChdrnum(surpcpy.chdrnum);
		wsaaTriggerChdrnum.set(surpcpy.chdrnum);
		if (isEQ(surpcpy.planSuffix,ZERO)) {
			utrnIO.setPlanSuffix(utrssur.getPlanSuffix());
			wsaaTriggerSuffix.set(utrssur.getPlanSuffix());
		}
		else {
			utrnIO.setPlanSuffix(surpcpy.planSuffix);
			wsaaTriggerSuffix.set(surpcpy.planSuffix);
		}
		utrnIO.setLife(surpcpy.life);
		utrnIO.setCoverage(surpcpy.coverage);
		wsaaTriggerCoverage.set(surpcpy.coverage);
		utrnIO.setRider(surpcpy.rider);
		wsaaTriggerRider.set(surpcpy.rider);
		utrnIO.setCrtable(surpcpy.crtable);
		utrnIO.setMoniesDate(surpcpy.effdate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		if (isEQ(surpcpy.type,"P")) {
			utrnIO.setSurrenderPercent(surpcpy.percreqd);
		}
		else {
			utrnIO.setSurrenderPercent(100);
		}
		utrnIO.setFundAmount(0);
		if (isEQ(surpcpy.element,SPACES)) {
			utrnIO.setUnitVirtualFund(surpcpy.fund);
		}
		else {
			utrnIO.setUnitVirtualFund(surpcpy.element);
		}
		utrnIO.setContractType(surpcpy.cnttype);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setNowDeferInd(t6647rec.dealin);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		utrnIO.setTriggerModule(t6598rec.addprocess);
		utrnIO.setTriggerKey(wsaaTrigger);
		utrnIO.setNofUnits(0);
		utrnIO.setTransactionDate(surpcpy.date_var);
		utrnIO.setTransactionTime(surpcpy.time);
		utrnIO.setTranno(surpcpy.tranno);
		utrnIO.setUser(surpcpy.user);
		if (isEQ(surpcpy.planSuffix,0)) {
			utrnIO.setUnitType(utrssur.getUnitType());
			if (isEQ(utrssur.getUnitType(),"A")) {
				utrnIO.setSvp(1);
			}
		}
		if (isNE(surpcpy.planSuffix,ZERO)) {
			utrnIO.setUnitType(utrsclmIO.getUnitType());
			if (isEQ(utrsclmIO.getUnitType(),"A")) {
				utrnIO.setSvp(1);
			}
		}
		utrnIO.setSvp(1);
		if (isEQ(utrnIO.getUnitType(),"A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		else {
			utrnIO.setUnitSubAccount("INIT");
		}
		utrnIO.setTermid(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setItmfrm(utrnIO.getMoniesDate());
		itemIO.setFormat(itdmrec);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemcoy(surpcpy.chdrcoy);
			itdmIO.setItemtabl(t5515);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemcoy(surpcpy.chdrcoy);
			itdmIO.setItemtabl(t5515);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isNE(utrnIO.getUnitType(),t5515rec.unitType)
		&& isNE(t5515rec.unitType,"B")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		utrnIO.setFundCurrency(t5515rec.currcode);
		utrnIO.setCntcurr(surpcpy.cntcurr);
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction("WRITR");
		
		readReservePricing();		
		
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),"****")) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void a100ReadT6659()
	{
		try {
			a100Read();
			a1010CheckT6659Details();
		}
		catch (GOTOException e){
		}
	}

protected void a100Read()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(t6647rec.unitStatMethod,itdmIO.getItemitem())
		|| isNE(surpcpy.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t6659)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g029);
			fatalError9000();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void a1010CheckT6659Details()
	{
		if (isEQ(t6659rec.subprog,SPACES)
		|| isNE(t6659rec.annOrPayInd,"P")
		|| isNE(t6659rec.osUtrnInd,"Y")) {
			goTo(GotoLabel.a190Exit);
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(surpcpy.chdrcoy);
		annprocrec.chdrnum.set(surpcpy.chdrnum);
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(surpcpy.effdate);
		annprocrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(surpcpy.user);
		annprocrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		annprocrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		annprocrec.batccoy.set(wsaaBatckey.batcBatccoy);
		annprocrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		annprocrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		annprocrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz,"****")) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError9000();
		}
	}

protected void a200InterestBearing()
	{
		try {
			a210Para();
		}
		catch (GOTOException e){
		}
	}

protected void a210Para()
	{
		if (isEQ(surpcpy.estimatedVal,0)) {
			goTo(GotoLabel.a290Exit);
		}
		hitsIO.setParams(SPACES);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setLife(surpcpy.life);
		hitsIO.setZintbfnd(surpcpy.fund);
		hitsIO.setChdrcoy(surpcpy.chdrcoy);
		hitsIO.setChdrnum(surpcpy.chdrnum);
		hitsIO.setLife(surpcpy.life);
		hitsIO.setCoverage(surpcpy.coverage);
		hitsIO.setRider(surpcpy.rider);
		hitsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","ZINTBFND");
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isEQ(hitsIO.getStatuz(),varcom.endp)
		|| isNE(surpcpy.chdrnum,hitsIO.getChdrnum())
		|| isNE(surpcpy.chdrcoy,hitsIO.getChdrcoy())
		|| isNE(surpcpy.life,hitsIO.getLife())
		|| isNE(surpcpy.coverage,hitsIO.getCoverage())
		|| isNE(surpcpy.rider,hitsIO.getRider())
		|| isNE(surpcpy.fund,hitsIO.getZintbfnd())) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		while ( !(isEQ(hitsIO.getStatuz(),varcom.endp))) {
			a300ReadHitsWriteHitr();
		}
	}

protected void a300ReadHitsWriteHitr()
	{
		/*A310-READ*/
		if (isNE(hitsIO.getZcurprmbal(),0)) {
			getTrigger500();
			a400SetupHitrs();
		}
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(surpcpy.chdrnum,hitsIO.getChdrnum())
		|| isNE(surpcpy.chdrcoy,hitsIO.getChdrcoy())
		|| isNE(surpcpy.life,hitsIO.getLife())
		|| isNE(surpcpy.coverage,hitsIO.getCoverage())
		|| isNE(surpcpy.rider,hitsIO.getRider())
		|| isNE(surpcpy.fund,hitsIO.getZintbfnd())) {
			hitsIO.setStatuz(varcom.endp);
		}
		/*A390-EXIT*/
	}

protected void a400SetupHitrs()
	{
		a410Go();
	}

protected void a410Go()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setTranno(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setPlanSuffix(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setContractAmount(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setProcSeqNo(ZERO);
		hitrIO.setSvp(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setChdrcoy(surpcpy.chdrcoy);
		wsaaTriggerChdrcoy.set(surpcpy.chdrcoy);
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		wsaaTriggerTranno.set(surpcpy.tranno);
		hitrIO.setChdrnum(surpcpy.chdrnum);
		wsaaTriggerChdrnum.set(surpcpy.chdrnum);
		hitrIO.setPlanSuffix(surpcpy.planSuffix);
		wsaaTriggerSuffix.set(surpcpy.planSuffix);
		hitrIO.setLife(surpcpy.life);
		hitrIO.setCoverage(surpcpy.coverage);
		wsaaTriggerCoverage.set(surpcpy.coverage);
		hitrIO.setRider(surpcpy.rider);
		wsaaTriggerRider.set(surpcpy.rider);
		hitrIO.setCrtable(surpcpy.crtable);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setSurrenderPercent(100);
		hitrIO.setFundAmount(0);
		if (isEQ(surpcpy.element,SPACES)) {
			hitrIO.setZintbfnd(surpcpy.fund);
		}
		else {
			hitrIO.setZintbfnd(surpcpy.element);
		}
		hitrIO.setCnttyp(surpcpy.cnttype);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			hitrIO.setSacscode(t5645rec.sacscode02);
			hitrIO.setSacstyp(t5645rec.sacstype02);
			hitrIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode01);
			hitrIO.setSacstyp(t5645rec.sacstype01);
			hitrIO.setGenlcde(t5645rec.glmap01);
		}
		hitrIO.setTriggerModule(t6598rec.addprocess);
		hitrIO.setTriggerKey(wsaaTrigger);
		hitrIO.setTranno(surpcpy.tranno);
		hitrIO.setSvp(1);
		hitrIO.setZrectyp("P");
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hitrIO.getChdrcoy());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(hitrIO.getZintbfnd());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setCntcurr(surpcpy.cntcurr);
		hitrIO.setEffdate(surpcpy.effdate);
		hitrIO.setZintalloc("N");
		hitrIO.setZintrate(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			fatalError9000();
		}
	}
//ILIFE-5459 Start
protected void readReservePricing(){
	
	zutrpf.setChdrcoy(surpcpy.chdrcoy.toString().trim());
	zutrpf.setChdrnum(surpcpy.chdrnum.toString().trim());
	zutrpf.setTranno(surpcpy.tranno.toInt());
	
	try {
		zutrpfList = zutrpfDAO.readReservePricing(zutrpf);
	} catch (Exception e) {
		LOGGER.error("Exception occured in readReservePricing()",e);
		fatalError9000();
	}
	
	if (isGT(zutrpfList.size(), 0)){
		
		for(Zutrpf zutrpf1 : zutrpfList){
			utrnIO.setMoniesDate(zutrpf1.getReserveUnitsDate()); 
			utrnIO.setNowDeferInd('N');
		}
	}	//ILIFE-5459 End
	
}

protected void fatalError9000()
	{
		try {
			error9010();
		}
		catch (GOTOException e){
		}
		finally{
			exit9090();
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9090()
	{
		exitProgram();
	}
}
