package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:27
 * Description:
 * Copybook name: COVRUDDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covruddkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covruddFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covruddKey = new FixedLengthStringData(64).isAPartOf(covruddFileKey, 0, REDEFINE);
  	public FixedLengthStringData covruddChdrcoy = new FixedLengthStringData(1).isAPartOf(covruddKey, 0);
  	public FixedLengthStringData covruddChdrnum = new FixedLengthStringData(8).isAPartOf(covruddKey, 1);
  	public FixedLengthStringData covruddLife = new FixedLengthStringData(2).isAPartOf(covruddKey, 9);
  	public FixedLengthStringData covruddCoverage = new FixedLengthStringData(2).isAPartOf(covruddKey, 11);
  	public FixedLengthStringData covruddRider = new FixedLengthStringData(2).isAPartOf(covruddKey, 13);
  	public PackedDecimalData covruddPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covruddKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covruddKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covruddFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covruddFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}