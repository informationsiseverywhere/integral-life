package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:39
 * Description:
 * Copybook name: UREPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Urepkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData urepFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData urepKey = new FixedLengthStringData(64).isAPartOf(urepFileKey, 0, REDEFINE);
  	public FixedLengthStringData urepChdrcoy = new FixedLengthStringData(1).isAPartOf(urepKey, 0);
  	public FixedLengthStringData urepChdrnum = new FixedLengthStringData(8).isAPartOf(urepKey, 1);
  	public PackedDecimalData urepSeqnum = new PackedDecimalData(7, 0).isAPartOf(urepKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(urepKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(urepFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		urepFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}