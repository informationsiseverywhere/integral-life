package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Uderpf {
	
	public int age;
	public String recordtype;
	public String lapind;
	public String chdrcoy;
	public String chdrnum;
	public String life;
	public String coverage;
	public String rider;
	public int planSuffix;
	public String unitVirtualFund;
	public String unitType;
	public int tranno;
	public String batctrcde;
	public BigDecimal contractAmount;
	public String cntcurr;
	public BigDecimal fundAmount;
	public String fundCurrency;
	public BigDecimal fundRate;
	public String triggerModule;
	public long relativeRecordNo;
	public String userProfile;
	public String jobName;
	public String datime;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getRecordtype() {
		return recordtype;
	}
	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}
	public String getLapind() {
		return lapind;
	}
	public void setLapind(String lapind) {
		this.lapind = lapind;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(String unitVirtualFund) {
		this.unitVirtualFund = unitVirtualFund;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public BigDecimal getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(BigDecimal contractAmount) {
		this.contractAmount = contractAmount;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public BigDecimal getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(BigDecimal fundAmount) {
		this.fundAmount = fundAmount;
	}
	public String getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(String fundCurrency) {
		this.fundCurrency = fundCurrency;
	}
	public BigDecimal getFundRate() {
		return fundRate;
	}
	public void setFundRate(BigDecimal fundRate) {
		this.fundRate = fundRate;
	}
	public String getTriggerModule() {
		return triggerModule;
	}
	public void setTriggerModule(String triggerModule) {
		this.triggerModule = triggerModule;
	}
	public long getRelativeRecordNo() {
		return relativeRecordNo;
	}
	public void setRelativeRecordNo(long relativeRecordNo) {
		this.relativeRecordNo = relativeRecordNo;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
}