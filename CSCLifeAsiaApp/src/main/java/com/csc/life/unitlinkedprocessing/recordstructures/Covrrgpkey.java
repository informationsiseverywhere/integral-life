package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:25
 * Description:
 * Copybook name: COVRRGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrrgpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrrgpFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrrgpKey = new FixedLengthStringData(64).isAPartOf(covrrgpFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrrgpChdrcoy = new FixedLengthStringData(1).isAPartOf(covrrgpKey, 0);
  	public FixedLengthStringData covrrgpChdrnum = new FixedLengthStringData(8).isAPartOf(covrrgpKey, 1);
  	public FixedLengthStringData covrrgpLife = new FixedLengthStringData(2).isAPartOf(covrrgpKey, 9);
  	public FixedLengthStringData covrrgpCoverage = new FixedLengthStringData(2).isAPartOf(covrrgpKey, 11);
  	public FixedLengthStringData covrrgpRider = new FixedLengthStringData(2).isAPartOf(covrrgpKey, 13);
  	public PackedDecimalData covrrgpPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrrgpKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrrgpKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrrgpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrrgpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}