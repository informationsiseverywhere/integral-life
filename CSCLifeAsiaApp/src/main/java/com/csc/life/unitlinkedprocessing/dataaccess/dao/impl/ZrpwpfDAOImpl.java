/******************************************************************************
 * File Name 		: ZrpwpfDAOImpl.java
 * Author			: CSC
 * Creation Date	: 08 May 2017
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for ZRPWPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			| *											| *				  |
 * ***************************************************************************/
package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrpwpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrpwpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * The Class Zrpwpf which contains the DAO implementation methods for the table ZRPWPF.
 */
public class ZrpwpfDAOImpl extends BaseDAOImpl<Zrpwpf> implements ZrpwpfDAO {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ZrpwpfDAOImpl.class);

	/**
	 * Fetches records from ZRPWPF for the given Criteria.
	 */
	@Override
	public List<Zrpwpf> searchZrpwpfRecord(Zrpwpf zrpwpfObj) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, TRANNO, LIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, USRPRF, JOBNM, DATIME FROM ZRPWPF ");
		
		StringBuilder whereClause = new StringBuilder();
		if(zrpwpfObj.getChdrcoy() != null){
			whereClause.append("CHDRCOY = " + zrpwpfObj.getChdrcoy() + " ");
		}
		if(whereClause.length() > 0){
			whereClause.append("AND ");
		}
		if(zrpwpfObj.getChdrnum() != null){
			whereClause.append("CHDRNUM = ? ");
		}
		if(whereClause.length() > 0){
			whereClause.append("AND ");
		}
		whereClause.append("TRANNO >= " + zrpwpfObj.getTranno() + " ");
		if(whereClause.length() > 0){
			sqlSelect.append("WHERE " + whereClause.toString());
		}
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psZrpwpfSelect = null;
		ResultSet sqlZrpwpfRs = null;
		List<Zrpwpf> outputList = new ArrayList<Zrpwpf>();
		try {
			psZrpwpfSelect = getPrepareStatement(sqlSelect.toString());
			if(zrpwpfObj.getChdrnum() != null) {
				psZrpwpfSelect.setString(1, zrpwpfObj.getChdrnum());
			}
			sqlZrpwpfRs = executeQuery(psZrpwpfSelect);
			while (sqlZrpwpfRs.next()) {
				Zrpwpf zrpwpf = new Zrpwpf();
				zrpwpf.setUniqueNumber(sqlZrpwpfRs.getInt("UNIQUE_NUMBER"));
				zrpwpf.setChdrcoy(sqlZrpwpfRs.getString("CHDRCOY"));
				zrpwpf.setChdrnum(sqlZrpwpfRs.getString("CHDRNUM"));
				zrpwpf.setTranno(sqlZrpwpfRs.getInt("TRANNO"));
				zrpwpf.setLife(sqlZrpwpfRs.getString("LIFE"));
				zrpwpf.setCoverage(sqlZrpwpfRs.getString("COVERAGE"));
				zrpwpf.setRider(sqlZrpwpfRs.getString("RIDER"));
				zrpwpf.setPlnsfx(sqlZrpwpfRs.getBigDecimal("PLNSFX"));
				zrpwpf.setUsrprf(sqlZrpwpfRs.getString("USRPRF"));
				zrpwpf.setJobnm(sqlZrpwpfRs.getString("JOBNM"));
				zrpwpf.setDatime(sqlZrpwpfRs.getString("DATIME"));
				outputList.add(zrpwpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchZrpwpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psZrpwpfSelect, sqlZrpwpfRs);
		}

		return outputList;
	}

	/**
	 * Deletes Record from the Table ZRPWPF.
	 */
	@Override
	public int deleteZrpwpf(Zrpwpf zrpwpf) throws SQLRuntimeException{
		int deleteCount;
		StringBuilder sqlDelete = new StringBuilder();
		sqlDelete.append("DELETE FROM ZRPWPF WHERE UNIQUE_NUMBER = ?");
		PreparedStatement psZrpwpfDelete = getPrepareStatement(sqlDelete.toString());
		try {
			psZrpwpfDelete.setLong(1, zrpwpf.getUniqueNumber());
			deleteCount = executeUpdate(psZrpwpfDelete);
			LOGGER.debug("deleteZrpwpf {} rows deleted.", deleteCount);//IJTI-1561
		} catch (SQLException e) {
			LOGGER.error("deleteZrpwpf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psZrpwpfDelete, null);
		}
		return deleteCount;
	}
}
