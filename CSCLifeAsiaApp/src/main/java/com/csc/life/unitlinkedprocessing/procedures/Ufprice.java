/*
 * File: Ufprice.java
 * Date: 30 August 2009 2:48:47
 * Author: Quipoz Limited
 * 
 * Class transformed from UFPRICE.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.unitlinkedprocessing.dataaccess.VprdudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Unit Fund Pricing Subroutine.
*
* This program will retrieve the prices and pricing date when
* supplied with a fund.
*
* Inputs:
*
*   Function  1. PRICE: The price correct at the eff date (status
*                       will be O-K) or the next date to the eff
*                       date (status will be ESTM).
*             2. DEALP: The dealing price (status O-K)
*             3. FLUCT: The dealing price (status O-K)
*
*   Company, fund, fund type, now/defferred in, effdate
*
*   (NOW-DEFER indicator defaults to NOW if none specified).
*
* Outputs:
*
*   Status    O-K : A suitable price was found
*             ESTM: Estimated price (PRICE function only)
*             ENDP: No price found
*   pricing date
*   prices: bare, bid, offer
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Ufprice extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UFPRICE";
	private PackedDecimalData wsaaCount = new PackedDecimalData(9, 0);

	private FixedLengthStringData wsaaGotOne = new FixedLengthStringData(1);
	private Validator gotOne = new Validator(wsaaGotOne, "Y");
		/* WSAA-HIT-RATES */
	private PackedDecimalData[] wsaaHitRate = PDInittedArray(50, 9, 0, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData saveEffdate = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData saveEffdate2 = new PackedDecimalData(8, 0).init(ZERO);
	private IntegerData ix = new IntegerData();
	private IntegerData iy = new IntegerData();
	private VprdudlTableDAM vprdudlIO = new VprdudlTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private LastUfpriceArrayInner lastUfpriceArrayInner = new LastUfpriceArrayInner();
	private VprcpfDAO vprcpfDao = getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont2020, 
		readVprdudl2030, 
		getEstimatePrice2050, 
		exit2090
	}

	public Ufprice() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ufpricerec.ufpriceRec = convertAndSetParam(ufpricerec.ufpriceRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		/*MAINLINE*/
		/*    Check that we are not being asked to do the same thing as*/
		/*    the previous call. If we are simply return the previous*/
		/*    values.*/
		/*    comment out the test that will perfrom buffering logic       */
		/*    processing                                                   */
		/*    IF  UFPR-MODE               = 'BATCH'                        */
		/*        MOVE 'N'                TO WSAA-GOT-ONE                  */
		/*        PERFORM 100-BUFFER-CHECK                                 */
		/*        IF  GOT-ONE                                              */
		/*            GO TO 090-EXIT                                       */
		/*        END-IF                                                   */
		/*    END-IF.                                                      */
		syserrrec.subrname.set(wsaaSubr);
		ufpricerec.statuz.set(varcom.oK);
		if (isNE(ufpricerec.function,"PRICE")
		&& isNE(ufpricerec.function,"DEALP")
		&& isNE(ufpricerec.function,"FLUCT")) {
			syserrrec.statuz.set("FUNC");
			ufpricerec.statuz.set("FUNC");
			fatalError600();
		}
		if (isEQ(ufpricerec.nowDeferInd,"D")) {
			getDeferredPrice2000();
		}
		else {
			getNowPrice1000();
		}
		/*EXIT*/
		exitProgram();
	}

protected void bufferCheck100()
	{
		bufferCheck101();
	}

protected void bufferCheck101()
	{
		/*    Check that we are not being asked to do the same thing as*/
		/*    the previous call. If we are, simply return the previous*/
		/*    values.*/
		wsaaCount.set(999999999);
		for (ix.set(1); !(isGT(ix, 50)
		|| isEQ(lastUfpriceArrayInner.lastCompany[ix.toInt()], SPACES)
		|| (isEQ(lastUfpriceArrayInner.lastCompany[ix.toInt()], ufpricerec.company)
		&& isEQ(lastUfpriceArrayInner.lastUnitVirtualFund[ix.toInt()], ufpricerec.unitVirtualFund)
		&& isEQ(lastUfpriceArrayInner.lastUnitType[ix.toInt()], ufpricerec.unitType)
		&& isEQ(lastUfpriceArrayInner.lastFunction[ix.toInt()], ufpricerec.function))); ix.add(1)){
			iz.set(ix);
			if (isLT(wsaaHitRate[iz.toInt()], wsaaCount)) {
				wsaaCount.set(wsaaHitRate[iz.toInt()]);
				iy.set(ix);
			}
			/****      IF WSAA-HIT-RATE (IX)         < WSAA-COUNT               */
			/****          MOVE WSAA-HIT-RATE (IX)   TO WSAA-COUNT              */
			/****          SET IY                    TO IX                      */
			/****      END-IF                                                   */
		}
		if (isGT(ix, 50)) {
			ix.set(iy);
			return ;
		}
		iz.set(ix);
		if (isEQ(ufpricerec.function, lastUfpriceArrayInner.lastFunction[ix.toInt()])
		&& isEQ(ufpricerec.company, lastUfpriceArrayInner.lastCompany[ix.toInt()])
		&& isEQ(ufpricerec.unitVirtualFund, lastUfpriceArrayInner.lastUnitVirtualFund[ix.toInt()])
		&& isEQ(ufpricerec.unitType, lastUfpriceArrayInner.lastUnitType[ix.toInt()])
		&& isEQ(ufpricerec.effdate, lastUfpriceArrayInner.lastEffdate[ix.toInt()])
		&& isEQ(ufpricerec.nowDeferInd, lastUfpriceArrayInner.lastNowDeferInd[ix.toInt()])) {
			wsaaGotOne.set("Y");
			/*      ADD  1                    TO WSAA-HIT-RATE       (IX)    */
			wsaaHitRate[iz.toInt()].add(1);
			ufpricerec.ufpriceRec.set(lastUfpriceArrayInner.lastUfpriceRec[ix.toInt()]);
		}
		else {
			/*      MOVE ZERO                 TO WSAA-HIT-RATE       (IX)    */
			wsaaHitRate[iz.toInt()].set(ZERO);
		}
	}

protected void fatalError600()
	{
		/*FATAL*/
		callProgram(Syserr.class, syserrrec.syserrRec);
		ufpricerec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void getNowPrice1000()
	{
		getNowPrice1010();
	}

protected void getNowPrice1010()
	{
		/* To find the "now" price we read vprc using the logical file*/
		/* VPRNUDL which has the eff date in descending sequence. Thus*/
		/* we will find the first price with an eff date <= the param*/
		/* eff date.*/
		// Start ILIFE-3407 vdivisala
	
		/*vprnudlIO.setCompany(ufpricerec.company);
		vprnudlIO.setUnitVirtualFund(ufpricerec.unitVirtualFund);
		vprnudlIO.setUnitType(ufpricerec.unitType);
		vprnudlIO.setEffdate(ufpricerec.effdate);
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError600();
		}
		if (isEQ(vprnudlIO.getStatuz(),varcom.endp)
		|| isNE(vprnudlIO.getCompany(),ufpricerec.company)
		|| isNE(vprnudlIO.getUnitVirtualFund(),ufpricerec.unitVirtualFund)
		|| isNE(vprnudlIO.getUnitType(),ufpricerec.unitType)) {
			vprnudlIO.setStatuz(varcom.endp);
		}
		if (isEQ(vprnudlIO.getStatuz(),varcom.oK)) {
			ufpricerec.priceDate.set(vprnudlIO.getEffdate());
			ufpricerec.barePrice.set(vprnudlIO.getUnitBarePrice());
			ufpricerec.bidPrice.set(vprnudlIO.getUnitBidPrice());
			ufpricerec.offerPrice.set(vprnudlIO.getUnitOfferPrice());
		}*/
		Vprcpf priceResult = vprcpfDao.getNowPrice(ufpricerec.company.toString(), ufpricerec.unitVirtualFund.toString(),
				ufpricerec.unitType.toString(), ufpricerec.effdate.toInt(), "1");
		if(priceResult!=null){
			ufpricerec.priceDate.set(priceResult.getEffdate());
			ufpricerec.barePrice.set(priceResult.getUnitBarePrice());
			ufpricerec.bidPrice.set(priceResult.getUnitBidPrice());
			ufpricerec.offerPrice.set(priceResult.getUnitOfferPrice());
		}
		else {
			ufpricerec.priceDate.set(ZERO);
			ufpricerec.barePrice.set(ZERO);
			ufpricerec.bidPrice.set(ZERO);
			ufpricerec.offerPrice.set(ZERO);
			ufpricerec.statuz.set(varcom.endp);
		}
	} // End ILIFE-3407

protected void getDeferredPrice2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					getDeferredPrice2010();
				case cont2020: 
					cont2020();
				case readVprdudl2030: 
					readVprdudl2030();
				case getEstimatePrice2050: 
					getEstimatePrice2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getDeferredPrice2010()
	{
		vprdudlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		vprdudlIO.setCompany(ufpricerec.company);
		vprdudlIO.setUnitVirtualFund(ufpricerec.unitVirtualFund);
		vprdudlIO.setUnitType(ufpricerec.unitType);
		if (isEQ(ufpricerec.function,"FLUCT")) {
			vprdudlIO.setEffdate(ufpricerec.effdate);
			datcon2rec.intDate2.set(ufpricerec.effdate);
			goTo(GotoLabel.readVprdudl2030);
		}
		if (isEQ(ufpricerec.effdate,saveEffdate)) {
			datcon2rec.intDate2.set(saveEffdate2);
			goTo(GotoLabel.cont2020);
		}else if(isEQ(ufpricerec.bsscIODate,ufpricerec.effdate)){
			datcon2rec.intDate2.set(ufpricerec.effdate);
			goTo(GotoLabel.cont2020);
		}
		saveEffdate.set(ufpricerec.effdate);
		datcon2rec.intDate1.set(ufpricerec.effdate);
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		saveEffdate2.set(datcon2rec.intDate2);
	}

protected void cont2020()
	{
		vprdudlIO.setEffdate(datcon2rec.intDate2);
	}

protected void readVprdudl2030()
	{
		SmartFileCode.execute(appVars, vprdudlIO);
		if (isNE(vprdudlIO.getStatuz(),varcom.oK)
		&& isNE(vprdudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprdudlIO.getParams());
			fatalError600();
		}
		if (isEQ(vprdudlIO.getStatuz(),varcom.endp)
		|| isNE(vprdudlIO.getCompany(),ufpricerec.company)
		|| isNE(vprdudlIO.getUnitVirtualFund(),ufpricerec.unitVirtualFund)
		|| isNE(vprdudlIO.getUnitType(),ufpricerec.unitType)) {
			vprdudlIO.setStatuz(varcom.endp);
		}
		if (isEQ(vprdudlIO.getStatuz(),varcom.oK)) {
			ufpricerec.priceDate.set(vprdudlIO.getEffdate());
			ufpricerec.barePrice.set(vprdudlIO.getUnitBarePrice());
			ufpricerec.bidPrice.set(vprdudlIO.getUnitBidPrice());
			ufpricerec.offerPrice.set(vprdudlIO.getUnitOfferPrice());
			if (isEQ(ufpricerec.function,"PRICE")
			&& isEQ(vprdudlIO.getEffdate(),datcon2rec.intDate2)) {
				ufpricerec.statuz.set(varcom.oK);
			}
			if (isEQ(ufpricerec.function,"PRICE")
			&& isGT(vprdudlIO.getEffdate(),datcon2rec.intDate2)) {
				ufpricerec.statuz.set("ESTM");
			}
			if (isEQ(ufpricerec.function,"FLUCT")) {
				ufpricerec.statuz.set(varcom.oK);
			}
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(ufpricerec.function,"PRICE")
		|| isEQ(ufpricerec.function,"FLUCT")) {
			goTo(GotoLabel.getEstimatePrice2050);
		}
		if (isEQ(ufpricerec.function,"DEALP")) {
			ufpricerec.priceDate.set(ZERO);
			ufpricerec.barePrice.set(ZERO);
			ufpricerec.bidPrice.set(ZERO);
			ufpricerec.offerPrice.set(ZERO);
			ufpricerec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
	}

protected void getEstimatePrice2050()
	{
		vprnudlIO.setCompany(ufpricerec.company);
		vprnudlIO.setUnitVirtualFund(ufpricerec.unitVirtualFund);
		vprnudlIO.setUnitType(ufpricerec.unitType);
		if (isEQ(ufpricerec.function,"FLUCT")) {
			vprnudlIO.setEffdate(ufpricerec.effdate);
		}
		else {
			vprnudlIO.setEffdate(datcon2rec.intDate2);
		}
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError600();
		}
		if (isEQ(vprnudlIO.getStatuz(),varcom.endp)
		|| isNE(vprnudlIO.getCompany(),ufpricerec.company)
		|| isNE(vprnudlIO.getUnitVirtualFund(),ufpricerec.unitVirtualFund)
		|| isNE(vprnudlIO.getUnitType(),ufpricerec.unitType)) {
			vprnudlIO.setStatuz(varcom.endp);
		}
		if (isEQ(vprnudlIO.getStatuz(),varcom.oK)) {
			ufpricerec.priceDate.set(vprnudlIO.getEffdate());
			ufpricerec.barePrice.set(vprnudlIO.getUnitBarePrice());
			ufpricerec.bidPrice.set(vprnudlIO.getUnitBidPrice());
			ufpricerec.offerPrice.set(vprnudlIO.getUnitOfferPrice());
			if (isEQ(ufpricerec.function,"FLUCT")) {
				return ;
			}
		}
		else {
			ufpricerec.priceDate.set(ZERO);
			ufpricerec.barePrice.set(ZERO);
			ufpricerec.bidPrice.set(ZERO);
			ufpricerec.offerPrice.set(ZERO);
		}
		if (isEQ(ufpricerec.function,"PRICE")
		&& isEQ(vprnudlIO.getStatuz(),varcom.oK)) {
			ufpricerec.statuz.set("ESTM");
		}
		else {
			ufpricerec.statuz.set(varcom.endp);
		}
	}
/*
 * Class transformed  from Data Structure LAST-UFPRICE-ARRAY--INNER
 */
private static final class LastUfpriceArrayInner { 

		/* LAST-UFPRICE-ARRAY */
	private FixedLengthStringData[] lastUfpriceRec = FLSInittedArray (50, 46);
	private FixedLengthStringData[] lastFunction = FLSDArrayPartOfArrayStructure(5, lastUfpriceRec, 0);
	private FixedLengthStringData[] lastCompany = FLSDArrayPartOfArrayStructure(1, lastUfpriceRec, 14);
	private FixedLengthStringData[] lastUnitVirtualFund = FLSDArrayPartOfArrayStructure(4, lastUfpriceRec, 15);
	private FixedLengthStringData[] lastUnitType = FLSDArrayPartOfArrayStructure(1, lastUfpriceRec, 19);
	private PackedDecimalData[] lastEffdate = PDArrayPartOfArrayStructure(8, 0, lastUfpriceRec, 20, 0.0);
	private FixedLengthStringData[] lastNowDeferInd = FLSDArrayPartOfArrayStructure(1, lastUfpriceRec, 25);
	private PackedDecimalData[] lastPriceDate = PDArrayPartOfArrayStructure(8, 0, lastUfpriceRec, 26, 0.0);
	private PackedDecimalData[] lastBarePrice = PDArrayPartOfArrayStructure(9, 5, lastUfpriceRec, 31, 0.0);
	private PackedDecimalData[] lastBidPrice = PDArrayPartOfArrayStructure(9, 5, lastUfpriceRec, 36, 0.0);
	private PackedDecimalData[] lastOfferPrice = PDArrayPartOfArrayStructure(9, 5, lastUfpriceRec, 41, 0.0);
}
}
