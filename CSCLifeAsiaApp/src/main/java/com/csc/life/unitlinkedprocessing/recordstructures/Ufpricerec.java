package com.csc.life.unitlinkedprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:24
 * Description:
 * Copybook name: UFPRICEREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ufpricerec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData ufpriceRec = new FixedLengthStringData(51);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(ufpriceRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(ufpriceRec, 5);
  	public FixedLengthStringData mode = new FixedLengthStringData(5).isAPartOf(ufpriceRec, 9);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(ufpriceRec, 14);
  	public FixedLengthStringData unitVirtualFund = new FixedLengthStringData(4).isAPartOf(ufpriceRec, 15);
  	public FixedLengthStringData unitType = new FixedLengthStringData(1).isAPartOf(ufpriceRec, 19);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(ufpriceRec, 20);
  	public FixedLengthStringData nowDeferInd = new FixedLengthStringData(1).isAPartOf(ufpriceRec, 25);
  	public PackedDecimalData priceDate = new PackedDecimalData(8, 0).isAPartOf(ufpriceRec, 26);
  	public PackedDecimalData bsscIODate = new PackedDecimalData(8, 0).isAPartOf(ufpriceRec, 31).init(0);
  	public PackedDecimalData barePrice = new PackedDecimalData(9, 5).isAPartOf(ufpriceRec, 36);
  	public PackedDecimalData bidPrice = new PackedDecimalData(9, 5).isAPartOf(ufpriceRec, 41);
  	public PackedDecimalData offerPrice = new PackedDecimalData(9, 5).isAPartOf(ufpriceRec, 46);  	


	public void initialize() {
		COBOLFunctions.initialize(ufpriceRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ufpriceRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}