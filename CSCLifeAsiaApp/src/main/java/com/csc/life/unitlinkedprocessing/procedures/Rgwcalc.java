/*
 * File: Rgwcalc.java
 * Date: 30 August 2009 2:12:18
 * Author: Quipoz Limited
 * 
 * Class transformed from RGWCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrssurTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Rwcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6655rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  CALCULATION FOR REGULAR WITHDRAWAL.
*
*
*****************************************************************
* </pre>
*/
public class Rgwcalc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "RGWCALC";

	private FixedLengthStringData wsaaT5515Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5515Key, 0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, 1);
	private Validator partPlan = new Validator(wsaaPlanSwitch, 2);
	private Validator summaryPart = new Validator(wsaaPlanSwitch, 3);
	private ZonedDecimalData wsaaRgwcCcdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaRgwcCcdateR = new FixedLengthStringData(8).isAPartOf(wsaaRgwcCcdate, 0, REDEFINE);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaRgwcCcdateR, 0).setUnsigned();
	private ZonedDecimalData wsaaRgwcCesdte = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaRgwcCesdteR = new FixedLengthStringData(8).isAPartOf(wsaaRgwcCesdte, 0, REDEFINE);
	private ZonedDecimalData wsaaYear1 = new ZonedDecimalData(4, 0).isAPartOf(wsaaRgwcCesdteR, 0).setUnsigned();
	private ZonedDecimalData wsaaRgwcPtdte = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaRgwcPtdteR = new FixedLengthStringData(8).isAPartOf(wsaaRgwcPtdte, 0, REDEFINE);
	private ZonedDecimalData wsaaYear2 = new ZonedDecimalData(4, 0).isAPartOf(wsaaRgwcPtdteR, 0).setUnsigned();
	private ZonedDecimalData wsaaYrsToCess = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaPaidToYrs = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaRgwcFreq = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private static final String h141 = "H141";
	private static final String g094 = "G094";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5542 = "T5542";
	private static final String t5687 = "T5687";
	private static final String t6655 = "T6655";
	private static final String t5540 = "T5540";
	private PackedDecimalData wsaaInitialAmount = new PackedDecimalData(16, 5).init(0);
	private PackedDecimalData wsaaAccumAmount = new PackedDecimalData(16, 5).init(0);
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaInitialBidPrice = new PackedDecimalData(9, 5).init(0);
	private PackedDecimalData wsaaAccumBidPrice = new PackedDecimalData(9, 5).init(0);
	private ZonedDecimalData wsaaFactor = new ZonedDecimalData(5, 0);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private ZonedDecimalData wsaaInd = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaNoMore = new FixedLengthStringData(1);
	private Validator endOfFund = new Validator(wsaaNoMore, "Y");

	private FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq, "Y");
		/* WSAA-FREQUENCIES */
	private FixedLengthStringData wsaaFreq1 = new FixedLengthStringData(10).init("0001020412");
	private PackedDecimalData wsaaEstimateTotAmt = new PackedDecimalData(16, 5).init(0);
	private PackedDecimalData wsaaEstimateTotAmt1 = new PackedDecimalData(16, 5).init(0);

	private FixedLengthStringData wsaaUtype = new FixedLengthStringData(1);
	private Validator initialUnits = new Validator(wsaaUtype, "I");

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaSwitch, "Y");

	private FixedLengthStringData wsaaT6655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6655Meth = new FixedLengthStringData(4).isAPartOf(wsaaT6655Key, 0);
	private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(4).isAPartOf(wsaaT6655Key, 4);

	private FixedLengthStringData wsaaT5542Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5542Meth = new FixedLengthStringData(4).isAPartOf(wsaaT5542Key, 0);
	private FixedLengthStringData wsaaT5542Curr = new FixedLengthStringData(3).isAPartOf(wsaaT5542Key, 4);

	private FixedLengthStringData wsaaReadTable = new FixedLengthStringData(1);
	private Validator haveTabDets = new Validator(wsaaReadTable, "Y");
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
	private UtrssurTableDAM utrssurIO = new UtrssurTableDAM();
	private VprcTableDAM vprcIO = new VprcTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5540rec t5540rec = new T5540rec();
	private T5515rec t5515rec = new T5515rec();
	private T5542rec t5542rec = new T5542rec();
	private T5687rec t5687rec = new T5687rec();
	private T6655rec t6655rec = new T6655rec();
	private Rwcalcpy rwcalcpy = new Rwcalcpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readAccum152, 
		dummy565, 
		setType570
	}

	public Rgwcalc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rwcalcpy.withdrawRec = convertAndSetParam(rwcalcpy.withdrawRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		rwcalcpy.status.set("****");
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(utrssurIO.getStatuz(), "ENDP")
		|| isEQ(utrsclmIO.getStatuz(), "ENDP")) {
			calcSurrPenVal500();
			return ;
		}
		if (isEQ(rwcalcpy.function, "BEGN")) {
			rwcalcpy.function.set("NEXT");
			wsaaChdrcoy.set(rwcalcpy.chdrChdrcoy);
			wsaaChdrnum.set(rwcalcpy.chdrChdrnum);
			wsaaCoverage.set(rwcalcpy.covrCoverage);
			wsaaRider.set(rwcalcpy.covrRider);
			wsaaLife.set(rwcalcpy.lifeLife);
			wsaaPlanSuffix.set(rwcalcpy.planSuffix);
			wsaaRgwcCcdate.set(rwcalcpy.crrcd);
			wsaaRgwcCesdte.set(rwcalcpy.convUnits);
			wsaaRgwcPtdte.set(rwcalcpy.ptdate);
			wsaaT5542Curr.set(rwcalcpy.currcode);
			wsaaPlanSwitch.set(rwcalcpy.planType);
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			wsaaEstimateTotAmt.set(ZERO);
			rwcalcpy.estimatedVal.set(ZERO);
			rwcalcpy.actualVal.set(ZERO);
			wsaaVirtualFund.set(SPACES);
			wsaaUnitType.set(SPACES);
			wsaaReadTable.set(SPACES);
			rwcalcpy.description.set(SPACES);
			rwcalcpy.neUnits.set(SPACES);
			rwcalcpy.tmUnits.set(SPACES);
			rwcalcpy.psNotAllwd.set(SPACES);
		}
		/* decide which part of the plan is being regularly withdrawn*/
		/*       IF RGWC-PLAN-SUFFIX = ZERO*/
		/*          MOVE 1 TO WSAA-PLAN-SWITCH*/
		/*       ELSE*/
		/*          IF RGWC-PLAN-SUFFIX NOT > RGWC-POLSUM*/
		/*              MOVE 0 TO WSAA-PLAN-SUFFIX*/
		/*              MOVE 3 TO WSAA-PLAN-SWITCH*/
		/*       ELSE*/
		/*          MOVE 2 TO WSAA-PLAN-SWITCH.*/
		wsaaRgwcFreq.set(rwcalcpy.billfreq);
		datcon3rec.frequency.set(rwcalcpy.billfreq);
		wsaaNoMore.set("N");
		compute(wsaaYrsToCess, 0).set(sub(wsaaYear1, wsaaYear2));
		compute(wsaaPaidToYrs, 0).set(sub(wsaaYear2, wsaaYear));
		rwcalcpy.endf.set(SPACES);
		if (isEQ(wsaaYrsToCess, 0)) {
			wsaaYrsToCess.set(1);
		}
		if (isGT(wsaaYrsToCess, 50)) {
			wsaaYrsToCess.set(50);
		}
		if (isEQ(wsaaPaidToYrs, 0)) {
			wsaaPaidToYrs.set(1);
		}
		if (isEQ(rwcalcpy.status, "****")) {
			if (wholePlan.isTrue()) {
				readFundsWholePlan100();
			}
			else {
				readFundsPartPlan130();
			}
		}
	}

	/**
	* <pre>
	*    IF  UTRSSUR-STATUZ = 'ENDP'  OR
	*        UTRSCLM-STATUZ = 'ENDP'
	*        MOVE 'ENDP' TO RGWC-FUNCTION
	*        MOVE 'Y'  TO  RGWC-ENDF.
	* </pre>
	*/
protected void exit090()
	{
		exitProgram();
	}

protected void readFundsWholePlan100()
	{
		para101();
	}

protected void para101()
	{
		utrssurIO.setParams(SPACES);
		utrssurIO.setChdrcoy(wsaaChdrcoy);
		utrssurIO.setChdrnum(wsaaChdrnum);
		utrssurIO.setLife(wsaaLife);
		utrssurIO.setCoverage(wsaaCoverage);
		utrssurIO.setRider(wsaaRider);
		utrssurIO.setPlanSuffix(ZERO);
		utrssurIO.setUnitVirtualFund(wsaaVirtualFund);
		utrssurIO.setUnitType(wsaaUnitType);
		utrssurIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, utrssurIO);
		if (isNE(utrssurIO.getStatuz(), "****")
		&& isNE(utrssurIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(utrssurIO.getParams());
			fatalError9000();
		}
		if (isNE(utrssurIO.getChdrcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(utrssurIO.getChdrnum(), rwcalcpy.chdrChdrnum)
		|| isNE(utrssurIO.getLife(), rwcalcpy.lifeLife)
		|| isNE(utrssurIO.getCoverage(), rwcalcpy.covrCoverage)
		|| isNE(utrssurIO.getRider(), rwcalcpy.covrRider)
		|| isEQ(utrssurIO.getStatuz(), "ENDP")) {
			rwcalcpy.endf.set("Y");
			utrssurIO.setStatuz("ENDP");
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		rwcalcpy.estimatedVal.set(ZERO);
		rwcalcpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(utrssurIO.getUnitVirtualFund());
		wsaaUnitType.set(utrssurIO.getUnitType());
		while ( !(endOfFund.isTrue()
		|| isNE(utrssurIO.getUnitVirtualFund(), wsaaVirtualFund)
		|| isNE(utrssurIO.getUnitType(), wsaaUnitType))) {
			accumAmtWholePlan120();
		}
		
		if (isEQ(utrssurIO.getStatuz(), "ENDP")
		|| isEQ(utrssurIO.getStatuz(), "****")) {
			readVprc150();
			checkEof200();
			wsaaVirtualFund.set(utrssurIO.getUnitVirtualFund());
			wsaaUnitType.set(utrssurIO.getUnitType());
			wsaaLife.set(utrssurIO.getLife());
			wsaaRider.set(utrssurIO.getRider());
			wsaaCoverage.set(utrssurIO.getCoverage());
			wsaaChdrnum.set(utrssurIO.getChdrnum());
			wsaaSwitch.set("N");
		}
	}

protected void accumAmtWholePlan120()
	{
		para121();
	}

protected void para121()
	{
		if (isEQ(utrssurIO.getUnitType(), "I")) {
			wsaaInitialAmount.add(utrssurIO.getCurrentDunitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrssurIO.getCurrentDunitBal());
			wsaaUtype.set("A");
		}
		utrssurIO.setFunction("NEXTR");
		SmartFileCode.execute(appVars, utrssurIO);
		if (isNE(utrssurIO.getStatuz(), "****")
		&& isNE(utrssurIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(utrssurIO.getParams());
			fatalError9000();
		}
		if (isNE(utrssurIO.getChdrcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(utrssurIO.getChdrnum(), rwcalcpy.chdrChdrnum)
		|| isNE(utrssurIO.getLife(), rwcalcpy.lifeLife)
		|| isNE(utrssurIO.getCoverage(), rwcalcpy.covrCoverage)
		|| isNE(utrssurIO.getRider(), rwcalcpy.covrRider)
		|| isEQ(utrssurIO.getStatuz(), "ENDP")) {
			wsaaNoMore.set("Y");
		}
	}

protected void readFundsPartPlan130()
	{
		para131();
	}

protected void para131()
	{
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setChdrcoy(wsaaChdrcoy);
		utrsclmIO.setChdrnum(wsaaChdrnum);
		utrsclmIO.setLife(wsaaLife);
		utrsclmIO.setCoverage(wsaaCoverage);
		utrsclmIO.setRider(wsaaRider);
		utrsclmIO.setPlanSuffix(wsaaPlanSuffix);
		utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsclmIO.setUnitType(wsaaUnitType);
		utrsclmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(), "****")
		&& isNE(utrsclmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsclmIO.getChdrcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(), rwcalcpy.chdrChdrnum)
		|| isNE(utrsclmIO.getLife(), rwcalcpy.lifeLife)
		|| isNE(utrsclmIO.getCoverage(), rwcalcpy.covrCoverage)
		|| isNE(utrsclmIO.getRider(), rwcalcpy.covrRider)
		|| isNE(utrsclmIO.getPlanSuffix(), rwcalcpy.planSuffix)
		|| isEQ(utrsclmIO.getStatuz(), "ENDP")) {
			rwcalcpy.endf.set("Y");
			utrsclmIO.setStatuz("ENDP");
			return ;
		}
		wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsclmIO.getUnitType());
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		rwcalcpy.estimatedVal.set(ZERO);
		rwcalcpy.actualVal.set(ZERO);
		while ( !(endOfFund.isTrue()
		|| isNE(utrsclmIO.getUnitVirtualFund(), wsaaVirtualFund)
		|| isNE(utrsclmIO.getUnitType(), wsaaUnitType))) {
			accumAmtPartPlan140();
		}
		
		if (isEQ(utrsclmIO.getStatuz(), "****")
		|| isEQ(utrsclmIO.getStatuz(), "ENDP")) {
			readVprc150();
			checkEof200();
			wsaaChdrnum.set(utrsclmIO.getChdrnum());
			wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
			wsaaUnitType.set(utrsclmIO.getUnitType());
			wsaaLife.set(utrsclmIO.getLife());
			wsaaRider.set(utrsclmIO.getRider());
			wsaaCoverage.set(utrsclmIO.getCoverage());
			wsaaPlanSuffix.set(utrsclmIO.getPlanSuffix());
			wsaaSwitch.set("N");
		}
	}

protected void accumAmtPartPlan140()
	{
		para141();
	}

protected void para141()
	{
		if (isEQ(utrsclmIO.getUnitType(), "I")) {
			wsaaInitialAmount.add(utrsclmIO.getCurrentDunitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrsclmIO.getCurrentDunitBal());
			wsaaUtype.set("A");
		}
		utrsclmIO.setFunction("NEXTR");
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(), "****")
		&& isNE(utrsclmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsclmIO.getChdrcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(), rwcalcpy.chdrChdrnum)
		|| isNE(utrsclmIO.getLife(), rwcalcpy.lifeLife)
		|| isNE(utrsclmIO.getCoverage(), rwcalcpy.covrCoverage)
		|| isNE(utrsclmIO.getRider(), rwcalcpy.covrRider)
		|| isNE(utrsclmIO.getPlanSuffix(), rwcalcpy.planSuffix)
		|| isEQ(utrsclmIO.getStatuz(), "ENDP")) {
			wsaaNoMore.set("Y");
		}
	}

protected void readVprc150()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read151();
				case readAccum152: 
					readAccum152();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read151()
	{
		/*  read the vprc file for each fund type*/
		if (isEQ(wsaaInitialAmount, ZERO)) {
			goTo(GotoLabel.readAccum152);
		}
		vprcIO.setDataArea(SPACES);
		vprcIO.setUnitVirtualFund(wsaaVirtualFund);
		vprcIO.setUnitType("I");
		vprcIO.setEffdate(rwcalcpy.effdate);
		vprcIO.setJobno(ZERO);
		vprcIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, vprcIO);
		if (isNE(vprcIO.getStatuz(), "****")
		&& isNE(vprcIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund, vprcIO.getUnitVirtualFund())
		|| isNE(vprcIO.getUnitType(), "I")
		|| isEQ(vprcIO.getStatuz(), "ENDP")) {
			rwcalcpy.status.set("MRNF");
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		wsaaInitialBidPrice.set(vprcIO.getUnitBidPrice());
	}

protected void readAccum152()
	{
		if (isEQ(wsaaAccumAmount, ZERO)) {
			return ;
		}
		vprcIO.setDataArea(SPACES);
		vprcIO.setUnitVirtualFund(wsaaVirtualFund);
		vprcIO.setUnitType("A");
		vprcIO.setEffdate(rwcalcpy.effdate);
		vprcIO.setJobno(ZERO);
		vprcIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, vprcIO);
		if (isNE(vprcIO.getStatuz(), "****")
		&& isNE(vprcIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund, vprcIO.getUnitVirtualFund())
		|| isNE(vprcIO.getUnitType(), "A")
		|| isEQ(vprcIO.getStatuz(), "ENDP")) {
			rwcalcpy.status.set("MRNF");
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		wsaaAccumBidPrice.set(vprcIO.getUnitBidPrice());
	}

protected void checkEof200()
	{
		eof210();
	}

protected void eof210()
	{
		getSurrenderMeth230();
		readTableT5540270();
		readTableT5542280();
		readTableT5515310();
		if (initialUnits.isTrue()) {
			/*MOVE WSAA-INITIAL-AMOUNT TO RGWC-ESTIMATED-VAL           */
			compute(rwcalcpy.estimatedVal, 6).setRounded(mult(wsaaInitialAmount, wsaaInitialBidPrice));
			rwcalcpy.type.set("I");
		}
		else {
			rwcalcpy.type.set("A");
			/*MOVE WSAA-ACCUM-AMOUNT   TO RGWC-ESTIMATED-VAL.          */
			compute(rwcalcpy.estimatedVal, 6).setRounded(mult(wsaaAccumAmount, wsaaAccumBidPrice));
		}
		wsaaEstimateTotAmt.add(rwcalcpy.estimatedVal);
		rwcalcpy.fund.set(wsaaVirtualFund);
		readTable300();
	}

protected void getSurrenderMeth230()
	{
		read231();
	}

protected void read231()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(rwcalcpy.crtable);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), rwcalcpy.crtable)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableT6655250()
	{
		go251();
	}

protected void go251()
	{
		wsaaReadTable.set("Y");
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6655);
		wsaaT6655Meth.set(t5687rec.svMethod);
		wsaaPremStat.set(rwcalcpy.pstatcode);
		itdmIO.setItemitem(wsaaT6655Key);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6655)
		|| isNE(itdmIO.getItemitem(), wsaaT6655Key)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t6655rec.t6655Rec.set(itdmIO.getGenarea());
		}
		calcInitUnits400();
	}

	/**
	* <pre>
	*260-READ-TABLE-T6656   SECTION.                                  
	*261-GO.                                                          
	**** MOVE SPACES                 TO ITDM-DATA-KEY.                
	**** MOVE RGWC-CHDR-CHDRCOY      TO ITDM-ITEMCOY.                 
	**** MOVE T6656                  TO ITDM-ITEMTABL.                
	**** MOVE WSAA-T6655-KEY         TO ITDM-ITEMITEM.                
	**** MOVE RGWC-EFFDATE           TO ITDM-ITMFRM.                  
	**** MOVE 'BEGN'                 TO ITDM-FUNCTION.                
	**** CALL 'ITDMIO' USING         ITDM-PARAMS.                     
	**** IF ITDM-STATUZ              NOT = '****' AND                 
	****                             NOT = 'ENDP'                     
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 9000-FATAL-ERROR.                                 
	**** IF ITDM-ITEMCOY             NOT = RGWC-CHDR-CHDRCOY          
	****  OR ITDM-ITEMTABL           NOT = T6656                      
	****  OR ITDM-ITEMITEM           NOT = WSAA-T6655-KEY             
	****  OR ITDM-STATUZ             = 'ENDP'                         
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 9000-FATAL-ERROR                                  
	**** ELSE                                                         
	****    MOVE ITDM-GENAREA        TO T6656-T6656-REC.              
	*269-EXIT.                                                        
	***  EXIT.                                                        
	* </pre>
	*/
protected void readTableT5540270()
	{
		go271();
	}

protected void go271()
	{
		wsaaReadTable.set("Y");
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(rwcalcpy.crtable);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5540)
		|| isNE(itdmIO.getItemitem(), rwcalcpy.crtable)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableT5542280()
	{
		go281();
	}

protected void go281()
	{
		wsaaT5542Meth.set(t5540rec.wdmeth);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5542);
		itdmIO.setItemitem(wsaaT5542Key);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5542)
		|| isNE(itdmIO.getItemitem(), wsaaT5542Key)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5542rec.t5542Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTable300()
	{
		go300();
	}

protected void go300()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(rwcalcpy.chdrChdrcoy);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(rwcalcpy.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), "****")
		&& isNE(descIO.getStatuz(), "MRNF")) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(), "****")) {
			rwcalcpy.description.set(descIO.getLongdesc());
		}
		else {
			rwcalcpy.description.set(SPACES);
		}
		rwcalcpy.element.set(wsaaVirtualFund);
	}

protected void readTableT5515310()
	{
		start310();
	}

protected void start310()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemcoy(rwcalcpy.chdrChdrcoy);
		wsaaT5515Key.set(SPACES);
		wsaaT5515Fund.set(wsaaVirtualFund);
		itdmIO.setItemitem(wsaaT5515Key);
		itdmIO.setItmfrm(rwcalcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), rwcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5515)
		|| isNE(itdmIO.getItemitem(), wsaaT5515Key)
		|| isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		rwcalcpy.fundCurrcode.set(t5515rec.currcode);
	}

protected void calcInitUnits400()
	{
		/*PARA*/
		/*To calculate the surrender value of initial units*/
		/*we use the 'Nth' factor on table T6655 which has 50 occurences.*/
		/*The occurence we use is the no. of years remaining before*/
		/*cesstation. This factor is then used to multiply the deemed*/
		/*units by. This is then multiplied by the bid price giving the*/
		/*surrendrer value of initial units.*/
		wsaaInitialBidPrice.multiply(wsaaInitialAmount);
		/*EXIT*/
	}

protected void calcSurrPenVal500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					partSurr550();
				case dummy565: 
				case setType570: 
					setType570();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void partSurr550()
	{
		/* The frequency codes on the table T5542 have been hard coded*/
		/* and are as follows 1) daily 2) single 3) annual 4) half yearly*/
		/* 5) quarterly 6) monthly 7) every 4 weeks 8) twice per month 9)*/
		/* every fortnight 10) weekly.*/
		/* The penalty can be one of either A) Flat fee or B) % of surr.*/
		/* which is checked against a minimum and maximum value also*/
		/* held on the table.*/
		/* Enter a loop to get the correct surrender fee.*/
		if (haveTabDets.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			readTableT5540270();
			readTableT5542280();
		}
		wsaaEstimateTotAmt1.set(wsaaEstimateTotAmt);
		wsaaInd.set(ZERO);
		/* MOVE 'N'  TO WSAA-GOT-FREQ.                                  */
		wsaaGotFreq.set("Y");
		/*PERFORM 560-GET-FEE UNTIL GOT-FREQ.                          */
		getT5542Freq700();
		if (!gotFreq.isTrue()) {
			wsaaGotFreq.set("Y");
			rwcalcpy.actualVal.set(ZERO);
			goTo(GotoLabel.dummy565);
		}
		if (isNE(t5542rec.wdlFreq[wsaaInd.toInt()], ZERO)) {
			checkDatesFrmRcd600();
		}
		if (isNE(t5542rec.wdlAmount[wsaaInd.toInt()], ZERO)) {
			if (isGT(t5542rec.wdlAmount[wsaaInd.toInt()], wsaaEstimateTotAmt1)) {
				rwcalcpy.neUnits.set("Y");
			}
		}
		if (isNE(t5542rec.wdrem[wsaaInd.toInt()], ZERO)) {
			if (isLT(t5542rec.wdrem[wsaaInd.toInt()], wsaaEstimateTotAmt1)) {
				rwcalcpy.tmUnits.set("Y");
			}
		}
		goTo(GotoLabel.setType570);
	}

protected void setType570()
	{
		rwcalcpy.status.set("ENDP");
		wsaaSwitch.set("Y");
		rwcalcpy.type.set("C");
		rwcalcpy.description.set(SPACES);
		rwcalcpy.description.set("PENALTY");
		rwcalcpy.estimatedVal.set(ZERO);
		utrssurIO.setStatuz(SPACES);
		rwcalcpy.endf.set(SPACES);
		rwcalcpy.fund.set(SPACES);
		utrsclmIO.setStatuz(SPACES);
		/*EXIT*/
	}

protected void checkDatesFrmRcd600()
	{
		strt600();
	}

protected void strt600()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		/*    MOVE     'M'              TO DTC3-FREQUENCY.                 */
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(wsaaRgwcCcdate);
		datcon3rec.intDate2.set(rwcalcpy.effdate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, "****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		if (isLT(datcon3rec.freqFactor, t5542rec.wdlFreq[wsaaInd.toInt()])) {
			rwcalcpy.psNotAllwd.set("Y");
		}
	}

protected void getT5542Freq700()
	{
		start710();
		next750();
	}

protected void start710()
	{
		/* Check frequency with those on T5542.                            */
		/* MOVE ZERO TO WSAA-SUB.                                  <003>*/
		/* PERFORM VARYING WSAA-SUB FROM 1 BY 1 UNTIL              <003>*/
		/*                               WSAA-SUB > 9              <003>*/
		/*                                                            <003>*/
		/* IF RGWC-BILLFREQ = T5542-BILLFREQ(WSAA-SUB)             <003>*/
		/*     MOVE 'Y'                   TO WSAA-GOT-FREQ         <003>*/
		/*     MOVE WSAA-SUB              TO WSAA-IND              <003>*/
		/*     MOVE 10                    TO WSAA-SUB              <003>*/
		/*     GO                         TO 750-NEXT              <003>*/
		/* END-IF                                                  <003>*/
		/* END-PERFORM.                                            <003>*/
		/*                                                            <003>*/
		/* IF WSAA-SUB   > 9                                       <003>*/
		/*    MOVE H141 TO SYSR-STATUZ                             <003>*/
		/*    GO TO 790-EXIT                                       <003>*/
		/* END-IF.                                                 <003>*/
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(!gotFreq.isTrue()
		|| isEQ(rwcalcpy.billfreq, t5542rec.billfreq[wsaaSub.toInt()])); wsaaSub.add(1)){
			if (isGT(wsaaSub, 6)) {
				wsaaGotFreq.set("N");
				syserrrec.statuz.set(h141);
			}
		}
		wsaaInd.set(wsaaSub);
	}

protected void next750()
	{
		if (gotFreq.isTrue()) {
			if (isNE(t5542rec.ffamt[wsaaInd.toInt()], ZERO)) {
				rwcalcpy.actualVal.set(t5542rec.ffamt[wsaaInd.toInt()]);
			}
		}
		if (gotFreq.isTrue()
		&& isEQ(t5542rec.feepc[wsaaInd.toInt()], 0)) {
			rwcalcpy.actualVal.set(ZERO);
		}
		if (gotFreq.isTrue()) {
			compute(wsaaEstimateTotAmt, 5).set(div((mult(wsaaEstimateTotAmt, t5542rec.feepc[wsaaInd.toInt()])), 100));
			if (isLT(wsaaEstimateTotAmt, t5542rec.feemin[wsaaInd.toInt()])) {
				rwcalcpy.actualVal.set(t5542rec.feemin[wsaaInd.toInt()]);
			}
			else {
				if (isGT(wsaaEstimateTotAmt, t5542rec.feemax[wsaaInd.toInt()])
				&& isNE(t5542rec.feemax[wsaaInd.toInt()], ZERO)) {
					rwcalcpy.actualVal.set(t5542rec.feemax[wsaaInd.toInt()]);
				}
				else {
					rwcalcpy.actualVal.set(wsaaEstimateTotAmt);
				}
			}
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		rwcalcpy.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
