package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdelpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Udelpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UdelpfDAOImpl extends BaseDAOImpl<Udelpf> implements UdelpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UdelpfDAOImpl.class);

	public int deleteUdelpfRecord(String chdrFrom, String chdrTo, String chdrcoy) {
		StringBuilder sqlSelect = new StringBuilder("DELETE FROM UDELPF WHERE CHDRCOY=? ");
		if (chdrFrom != null && !chdrFrom.trim().isEmpty()) {
			sqlSelect.append(" AND CHDRNUM >= ? ");
		}
		if (chdrTo != null && !chdrTo.trim().isEmpty()) {
			sqlSelect.append(" AND CHDRNUM <= ? ");
		}
		PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
		int i = 1;
		try {
			ps.setString(i++, chdrcoy);
			if (chdrFrom != null && !chdrFrom.trim().isEmpty()) {
				ps.setString(i++, chdrFrom);
			}
			if (chdrTo != null && !chdrTo.trim().isEmpty()) {
				ps.setString(i++, chdrTo);
			}
			return ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteUdelpfRecord()".concat(e.getMessage()));
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public void insertUdelpfRecord(List<Udelpf> udelpfList) {
		String sqlSelect = "INSERT INTO UDELPF(CHDRCOY, CHDRNUM, USRPRF, JOBNM, DATIME) VALUES(?,?,?,?,?) ";

		PreparedStatement ps = getPrepareStatement(sqlSelect);
		try {
			for (Udelpf u : udelpfList) {
				ps.setString(1, u.getChdrcoy());
				ps.setString(2, u.getChdrnum());
                ps.setString(3, getUsrprf());
				ps.setString(4, getJobnm());
				ps.setTimestamp(5, new Timestamp(System.currentTimeMillis())); 
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertUdelpfRecord()".concat(e.getMessage()));
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public void deleteUdelpfRecordByUdel(List<Udelpf> udelpfList) {
		if (udelpfList != null && !udelpfList.isEmpty()) {
			PreparedStatement ps = getPrepareStatement("DELETE FROM UDEL WHERE CHDRCOY=? AND CHDRNUM=?  ");
			try {
				for (Udelpf u : udelpfList) {
					ps.setString(1, u.getChdrcoy());
					ps.setString(2, u.getChdrnum());
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("deleteUdelpfRecordByUdel()".concat(e.getMessage()));
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
	}
	
	public boolean isExistUdelRecord(String chdrcoy, String chdrnum) {
		String sql = "SELECT * FROM UDELPF WHERE CHDRCOY=? AND CHDRNUM=? ORDER BY CHDRCOY ASC,CHDRNUM ASC,UNIQUE_NUMBER DESC";
		PreparedStatement ps = getPrepareStatement(sql);
		ResultSet rs = null;
		boolean existFlag = false;
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);			
			rs=ps.executeQuery();
			if(rs.next()) {
				existFlag=true;
			}
		}catch(SQLException e) {
			LOGGER.error("isExistUdelRecord()".concat(e.getMessage()));
			throw new SQLRuntimeException(e);
		}finally {
			close(ps, rs);
		}
		
     return existFlag;
	}
}