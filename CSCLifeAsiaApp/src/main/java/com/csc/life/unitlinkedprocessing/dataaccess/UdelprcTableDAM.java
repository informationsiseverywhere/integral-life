package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UdelprcTableDAM.java
 * Date: Sun, 30 Aug 2009 03:50:51
 * Class transformed from UDELPRC.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UdelprcTableDAM extends UdelpfTableDAM {

	public UdelprcTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UDELPRC");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", PRCSEQ"
		             + ", VRTFND"
		             + ", UNITYP";
		
		QUALIFIEDCOLUMNS = 
		            "VRTFND, " +
		            "NDFIND, " +
		            "UNITYP, " +
		            "MONIESDT, " +
		            "PRCSEQ, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "TRANNO, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "PRCSEQ ASC, " +
		            "VRTFND ASC, " +
		            "UNITYP ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "PRCSEQ DESC, " +
		            "VRTFND DESC, " +
		            "UNITYP DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               unitVirtualFund,
                               nowDeferInd,
                               unitType,
                               moniesDate,
                               procSeqNo,
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               tranno,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(48);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getProcSeqNo().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, procSeqNo);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(unitVirtualFund.toInternal());
	nonKeyFiller30.setInternal(unitType.toInternal());
	nonKeyFiller50.setInternal(procSeqNo.toInternal());
	nonKeyFiller60.setInternal(chdrcoy.toInternal());
	nonKeyFiller70.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(80);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ getNowDeferInd().toInternal()
					+ nonKeyFiller30.toInternal()
					+ getMoniesDate().toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getTranno().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nowDeferInd);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, moniesDate);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(Object what) {
		setProcSeqNo(what, false);
	}
	public void setProcSeqNo(Object what, boolean rounded) {
		if (rounded)
			procSeqNo.setRounded(what);
		else
			procSeqNo.set(what);
	}
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getNowDeferInd() {
		return nowDeferInd;
	}
	public void setNowDeferInd(Object what) {
		nowDeferInd.set(what);
	}	
	public PackedDecimalData getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(Object what) {
		setMoniesDate(what, false);
	}
	public void setMoniesDate(Object what, boolean rounded) {
		if (rounded)
			moniesDate.setRounded(what);
		else
			moniesDate.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		procSeqNo.clear();
		unitVirtualFund.clear();
		unitType.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nowDeferInd.clear();
		nonKeyFiller30.clear();
		moniesDate.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		tranno.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}