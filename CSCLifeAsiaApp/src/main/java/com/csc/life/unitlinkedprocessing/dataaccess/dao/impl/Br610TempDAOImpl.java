package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.cashdividends.dataaccess.model.Dryh524DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br608TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br610TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br608DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br610TempDAOImpl extends BaseDAOImpl<Br610DTO> implements Br610TempDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Br610TempDAOImpl.class);
	public Map<String, List<Br610DTO>> getRecords(String company, int scheduleNumber) {
		List<Br610DTO> br610List = new ArrayList<Br610DTO>();
		Br610DTO data; 
		StringBuilder sql = new StringBuilder("SELECT UJ.CHDRCOY,UJ.CHDRNUM, UJ.JCTLJNUMST,UJ.USTMNO,UJ.STRPDATE,UJ.USTMTFLAG, ");
		sql.append("UN.FDBKIND,UN.MONIESDT,UN.TRANNO,UN.VRTFND,UN.FUNDAMNT,UN.CNTAMNT,UN.NOFUNT,UN.NOFDUNT,UN.PLNSFX, ");
		sql.append("UN.LIFE,UN.COVERAGE,UN.RIDER,UN.UNITYP,UN.UNIQUE_NUMBER USTN_UNIQUE_NUMBER,");
		sql.append( "UM.USTMNO USTFSTMNO,UM.CHDRCOY USTFCHDRCOY,UM.CHDRNUM USTFCHDRNUM,UM.PLNSFX USTFPLNSFX,UM.LIFE USTFLIFE, ");
		sql.append("UM.COVERAGE USTFCOVERAGE,UM.RIDER USTFRIDER,UM.VRTFND USTFVRTFND,UM.UNITYP USTFUNITYP, ");
		sql.append("UM.STCLDT,UM.STCLUN,UM.STCLDUN,UM.STCLFUCA,UM.STCLCOCA,UM.STTRFUCA,UM.STTRCOCA,UM.STTRDUN, ");
		sql.append(" UM.STTRUN,UM.STOPFUCA,UM.STOPCOCA,UM.STOPUN,UM.STOPDUN,UM.UNIQUE_NUMBER USTF_UNIQUE_NUMBER ");
		sql.append("FROM USTJ UJ LEFT JOIN USTN UN ON UN.CHDRCOY = UJ.CHDRCOY AND UN.CHDRNUM = UJ.CHDRNUM ");
		sql.append("LEFT JOIN USTFSTM UM ON UN.CHDRCOY = UM.CHDRCOY AND UN.CHDRNUM = UM.CHDRNUM AND UN.PLNSFX = UM.PLNSFX AND UN.LIFE = UM.LIFE ");
		sql.append(" AND UN.COVERAGE = UM.COVERAGE AND UN.RIDER = UM.RIDER AND UN.VRTFND = UM.VRTFND AND UN.UNITYP = UM.UNITYP ");
		sql.append(" where UJ.chdrcoy=? and UJ.JCTLJNUMST = ?  ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		Map<String, List<Br610DTO>> dataMap = new LinkedHashMap<String, List<Br610DTO>>();
		try {
			ps.setString(1, company);
			ps.setInt(2, scheduleNumber);
			rs = executeQuery(ps);
			while(rs.next()){
				Br610DTO br610DTO =  new Br610DTO();
				br610DTO.setCompany(rs.getString("CHDRCOY"));
				br610DTO.setChdrnum(rs.getString("CHDRNUM"));
				br610DTO.setJctljnumst(rs.getInt("JCTLJNUMST"));
				br610DTO.setUstmno(rs.getInt("USTMNO"));
				br610DTO.setStrpdate(rs.getInt("STRPDATE"));
				br610DTO.setUstmtflag(rs.getString("USTMTFLAG"));
				br610DTO.setFdbkind(rs.getString("FDBKIND"));
				br610DTO.setMoniesdt(rs.getInt("MONIESDT"));
				br610DTO.setTranno(rs.getInt("TRANNO"));
				br610DTO.setVrtfnd(rs.getString("VRTFND"));
				br610DTO.setFundamnt(rs.getBigDecimal("FUNDAMNT"));
				br610DTO.setCntamnt(rs.getBigDecimal("CNTAMNT"));
				br610DTO.setNofunts(rs.getBigDecimal("NOFUNT"));
				br610DTO.setNofdunts(rs.getBigDecimal("NOFDUNT"));
				br610DTO.setPlnsfx(rs.getInt("PLNSFX"));
				br610DTO.setLife(rs.getString("LIFE"));
				br610DTO.setCoverage(rs.getString("COVERAGE"));
				br610DTO.setRider(rs.getString("RIDER"));
				br610DTO.setUnityp(rs.getString("UNITYP"));
				br610DTO.setUstn_Unique_number(rs.getLong("USTN_UNIQUE_NUMBER"));
				if (dataMap.containsKey(br610DTO.getChdrnum())) {
					dataMap.get(br610DTO.getChdrnum()).add(br610DTO);
				} else {
					List<Br610DTO> ustmList = new ArrayList<Br610DTO>();
					ustmList.add(br610DTO);
					dataMap.put(br610DTO.getChdrnum().trim()+br610DTO.getUstn_Unique_number(), ustmList);
				}
			}	
		}catch(SQLException e) {
			LOGGER.error("Br610DAOImpl:getUstmDetails() threw SQLException :", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return dataMap;
	}

	public Map<String, List<Br610DTO>> searchRecord(String coy, List<String> chdrnumList,int extractSize, int partitionID ){
		Br610DTO data; 
		StringBuilder sql = new StringBuilder( " SELECT * FROM ( SELECT UN.CHDRCOY,UN.CHDRNUM, UN.FDBKIND,UN.MONIESDT,UN.TRANNO,UN.VRTFND,UN.FUNDAMNT,UN.CNTAMNT,UN.NOFUNT,UN.NOFDUNT,UN.PLNSFX, ");
		sql.append("UN.LIFE,UN.COVERAGE,UN.RIDER,UN.UNITYP,UN.UNIQUE_NUMBER USTN_UNIQUE_NUMBER,");
		sql.append( "UM.USTMNO USTFSTMNO,UM.CHDRCOY USTFCHDRCOY,UM.CHDRNUM USTFCHDRNUM,UM.PLNSFX USTFPLNSFX,UM.LIFE USTFLIFE, ");
		sql.append("UM.COVERAGE USTFCOVERAGE,UM.RIDER USTFRIDER,UM.VRTFND USTFVRTFND,UM.UNITYP USTFUNITYP, ");
		sql.append("UM.STCLDT,UM.STCLUN,UM.STCLDUN,UM.STCLFUCA,UM.STCLCOCA,UM.STTRFUCA,UM.STTRCOCA,UM.STTRDUN, ");
		sql.append(" UM.STTRUN,UM.STOPFUCA,UM.STOPCOCA,UM.STOPUN,UM.STOPDUN,UM.UNIQUE_NUMBER USTF_UNIQUE_NUMBER, ");
		sql.append("FLOOR((ROW_NUMBER()OVER(ORDER BY UN.CHDRCOY, UN.CHDRNUM)-1)/?) AS BATCHNUM FROM USTN UN LEFT JOIN USTFSTM UM ON UN.CHDRCOY = UM.CHDRCOY AND UN.CHDRNUM = UM.CHDRNUM AND UN.PLNSFX = UM.PLNSFX AND UN.LIFE = UM.LIFE ");
		sql.append(" AND UN.COVERAGE = UM.COVERAGE AND UN.RIDER = UM.RIDER AND UN.VRTFND = UM.VRTFND AND UN.UNITYP = UM.UNITYP WHERE UN.CHDRCOY = ? AND " );
		sql.append(getSqlInStr("UN.CHDRNUM", chdrnumList));
		sql.append(" ) MAIN WHERE BATCHNUM = ?  ");
		sql.append(" order by CHDRCOY ASC, CHDRNUM ASC, PLNSFX ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, VRTFND ASC, USTN_UNIQUE_NUMBER DESC  ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		Map<String, List<Br610DTO>> dataMap = new LinkedHashMap<String, List<Br610DTO>>();
		try {
			ps.setInt(1, extractSize);
			ps.setString(2, coy);
			ps.setInt(3, partitionID);
			rs = executeQuery(ps);
			while(rs.next()){
				Br610DTO br610DTO =  new Br610DTO();	
				br610DTO.setCompany(rs.getString("CHDRCOY"));
				br610DTO.setChdrnum(rs.getString("CHDRNUM"));
				br610DTO.setFdbkind(rs.getString("FDBKIND"));
				br610DTO.setMoniesdt(rs.getInt("MONIESDT"));
				br610DTO.setTranno(rs.getInt("TRANNO"));
				br610DTO.setVrtfnd(rs.getString("VRTFND"));
				br610DTO.setFundamnt(rs.getBigDecimal("FUNDAMNT"));
				br610DTO.setCntamnt(rs.getBigDecimal("CNTAMNT"));
				br610DTO.setNofunts(rs.getBigDecimal("NOFUNT"));
				br610DTO.setNofdunts(rs.getBigDecimal("NOFDUNT"));
				br610DTO.setPlnsfx(rs.getInt("PLNSFX"));
				br610DTO.setLife(rs.getString("LIFE"));
				br610DTO.setCoverage(rs.getString("COVERAGE"));
				br610DTO.setRider(rs.getString("RIDER"));
				br610DTO.setUnityp(rs.getString("UNITYP"));
				br610DTO.setUstn_Unique_number(rs.getLong("USTN_UNIQUE_NUMBER"));
				br610DTO.setUstfstmno(rs.getString("USTFSTMNO"));
				br610DTO.setUstfChdrcoy(rs.getString("USTFCHDRCOY"));
				br610DTO.setUstfChdrnum(rs.getString("USTFCHDRNUM"));
				br610DTO.setUstfPlnsfx(rs.getInt("USTFPLNSFX"));
				br610DTO.setUstfLife(rs.getString("USTFLIFE"));
				br610DTO.setUstfCoverage(rs.getString("USTFCOVERAGE"));
				br610DTO.setUstfRider(rs.getString("USTFRIDER"));
				br610DTO.setUstfVrtfnd(rs.getString("USTFVRTFND"));
				br610DTO.setUstfUnityp(rs.getString("USTFUNITYP"));
				br610DTO.setStcldt(rs.getInt("STCLDT"));
				br610DTO.setStclun(rs.getBigDecimal("STCLUN"));
				br610DTO.setStcldun(rs.getBigDecimal("STCLDUN"));
				br610DTO.setStclfuca(rs.getBigDecimal("STCLFUCA"));
				br610DTO.setStclcoca(rs.getBigDecimal("STCLCOCA"));
				br610DTO.setSttrfuca(rs.getBigDecimal("STTRFUCA"));
				br610DTO.setSttrcoca(rs.getBigDecimal("STTRCOCA"));
				br610DTO.setSttrdun(rs.getBigDecimal("STTRDUN"));
				br610DTO.setSttrun(rs.getBigDecimal("STTRUN"));
				br610DTO.setStopfuca(rs.getBigDecimal("STOPFUCA"));
				br610DTO.setStopcoca(rs.getBigDecimal("STOPCOCA"));
				br610DTO.setStopun(rs.getBigDecimal("STOPUN"));
				br610DTO.setStopdun(rs.getBigDecimal("STOPDUN"));
				br610DTO.setUstf_Unique_number(rs.getLong("USTF_UNIQUE_NUMBER"));



				if (dataMap.containsKey(br610DTO.getChdrnum())) {
					dataMap.get(br610DTO.getChdrnum()).add(br610DTO);
				} else {
					List<Br610DTO> ustmList = new ArrayList<Br610DTO>();
					ustmList.add(br610DTO);
					dataMap.put(br610DTO.getChdrnum().trim(), ustmList);
				}
			}	
		}catch(SQLException e) {
			LOGGER.error("Br610DAOImpl:getUstmDetails() threw SQLException :", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return dataMap;
	}


	public Map<String, List<Br610DTO>> getChdrulsRecord(String coy, List<String> chdrnumList) {
		StringBuilder sql = new StringBuilder( "select CHDR.CHDRCOY,CHDR.CHDRNUM,CHDR.CNTTYPE,CHDR.CNTCURR,CHDR.POLSUM,CHDR.OCCDATE,CHDR.COWNNUM,CHDR.COWNPFX,CHDR.AGNTCOY,"
				+ "CHDR.AGNTNUM,CHDR.BILLFREQ,LIFE.LIFCNUM "
				+ " FROM CHDRULS CHDR LEFT JOIN LIFEENQ LIFE ON CHDR.CHDRCOY = LIFE.CHDRCOY ");
		sql.append("AND CHDR.CHDRNUM = LIFE.CHDRNUM AND LIFE.LIFE = '01' AND LIFE.JLIFE= '00'" );
		sql.append("WHERE CHDR.CHDRCOY = ? AND CHDR.VALIDFLAG = '1' AND " );
		sql.append(getSqlInStr("CHDR.CHDRNUM", chdrnumList));
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		Map<String, List<Br610DTO>> dataMap = new LinkedHashMap<String, List<Br610DTO>>();
		try {
			ps.setString(1, coy);
			rs = executeQuery(ps);
			while(rs.next()){
				Br610DTO br611DTO = new Br610DTO();
				br611DTO.setChdruls_chdrnum(rs.getString("CHDRNUM"));
				br611DTO.setChdruls_chdrcoy(rs.getString("CHDRCOY"));
				br611DTO.setCntcurr(rs.getString("CNTCURR"));
				br611DTO.setCnttype(rs.getString("CNTTYPE"));
				br611DTO.setPolsum(rs.getBigDecimal("POLSUM"));
				br611DTO.setOccdate(rs.getInt("OCCDATE"));
				br611DTO.setCownnum(rs.getString("COWNNUM"));
				br611DTO.setCownpfx(rs.getString("COWNPFX"));
				br611DTO.setAgntcoy(rs.getString("AGNTCOY"));
				br611DTO.setAgntnum(rs.getString("AGNTNUM"));
				br611DTO.setLifcnum(rs.getString("LIFCNUM"));
				br611DTO.setBillfreq(rs.getString("BILLFREQ"));
				if (dataMap.containsKey(br611DTO.getChdrnum())) {
					dataMap.get(br611DTO.getChdruls_chdrnum()).add(br611DTO);
				} else {
					List<Br610DTO> ustmList = new ArrayList<Br610DTO>();
					ustmList.add(br611DTO);
					dataMap.put(br611DTO.getChdruls_chdrnum().trim(), ustmList);
				}
			}	
		}catch(SQLException e) {
			LOGGER.error("Br610DAOImpl:getUstmDetails() threw SQLException :", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return dataMap;
	}

}




