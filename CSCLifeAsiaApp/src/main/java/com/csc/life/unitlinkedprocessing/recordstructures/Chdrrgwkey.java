package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:22
 * Description:
 * Copybook name: CHDRRGWKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrrgwkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrrgwFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrrgwKey = new FixedLengthStringData(64).isAPartOf(chdrrgwFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrrgwChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrrgwKey, 0);
  	public FixedLengthStringData chdrrgwChdrnum = new FixedLengthStringData(8).isAPartOf(chdrrgwKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrrgwKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrrgwFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrrgwFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}