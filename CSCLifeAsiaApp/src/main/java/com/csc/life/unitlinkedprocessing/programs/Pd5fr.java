/*
 * File: P5146.java
 * Date: 30 August 2009 0:14:16
 * Author: Quipoz Limited
 * 
 * Class transformed from P5146.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import java.util.List;
import java.util.logging.Logger;

import org.slf4j.LoggerFactory;
import org.slf4j.LoggerFactory;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;

import com.csc.life.unitlinkedprocessing.screens.Sd5frScreenVars;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswhTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
 
import com.csc.life.unitlinkedprocessing.screens.Sd5frScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

 

import com.csc.life.unitlinkedprocessing.dataaccess.model.Uswdpf;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UswdpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.impl.UswdpfDAOImpl;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*            P5146 - Unit Switching Target Funds.
*
*  This program will be used to select the Funds into which
*  Switches will be made. If the system holds an 'Available
*  fund list' for the Component being processed, then these will
*  be displayed and the user will enter percentages against
*  the Funds to be used as targets for the monies. If no
*  Available fund list exists then the user will be allowed to
*  manually enter the Fund codes as well as the percentages for
*  Switching.
*
*  Unlike the Source Funds screen the user may only enter
*  percentages for the Target funds, amounts will not be
*  allowed.
*
*  When all the details have been validated the USWD record
*  will be created along with the necessary UTRN records. The
*  final processing will be carried out by the trigger module
*  invoked from the Unit Dealing run when it is processing
*  the UTRN records.
*
*  If a USWH - Unit Switching Header record does not exist, then
*  one will be created.
*  Initialise
*  ----------
*  RETRV the USWD record from the USWDIO module.
*
*  RETRV the USWH record from the USWHIO module.
*
*  Clear the subfile ready for loading.
*
*  The details of the contract being processed will be
*  stored in the CHDRMJA I/O module. Retrieve the details
*  and set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*  Contract Type, (CNTTYPE) - long description from T5688,
*
*  Contract Status, (STATCODE) - short description from T3623,
*
*  Premium Status, (PSTATCODE) - short description from T3588,
*
*  Servicing Branch, - long description from T1692.
*
*  The owner's client (CLTS) details.
*
*    The first Life Assured details from the LIFE dataset.
*
*    The Joint Life Assured details from the LIFE dataset
*    if they exist.
*
*    All the client numbers should have their names displayed
*    using CONFNAME. Obtain the confirmation names.
*
*
*  The details of all funds selected for the Component will be
*  held on the USWD record. If the selected policy was a
*  summarised policy then the Plan Suffix in the key of the
*  USWD record will be a 'notional' number, ie. that policy
*  will not actually exist. The program will be able to
*  determine this by comparing this number to the Number of
*  Policies Summarised, (POLSUM), from the Contract Header. If
*  the Plan Suffix is less than or equal to POLSUM then a
*  notional policy is being processed.
*
*  Load the subfile as follows:
*
*  Display the details from the USWD record. While these are
*  being written the program must also write out, on the same
*  lines, the available fund details if there is an Available
*  Fund list associated with the component code.
*
*  Before writing out any subfile records then table T5551 must
*  be read to find the Available fund list. Table T5551
*  should be read with a key derived from the edit code on table
*  and concatenated with the CHDRMJA currency code. (Table
*  T5671 is read with Transaction code and CRTABLE.) If T5551
*  Available Fund List field is blank then there is no list of
*  permitted Funds and the user is to be allowed to enter the
*  Fund codes. Each subfile record will then be written out with
*  the target Fund field unprotected.
*
*  If this field is not blank then use it to read T5543 and
*  obtain the permitted funds list. Each Fund in the list must
*  be displayed along with its currency from T5515. Under
*  these circumstances the Target Fund field must be protected.
*
*  Load all pages  required in the subfile and set the subfile
*  more indicator to no.
*  Validation
*  ----------
*  Use the Read Next Changed Subfile function to read all the
*  subfile records in which the user has made an entry. Ignore
*  any that have spaces and zeroes in the fund and percentage
*  fields.
*
*  If the user was allowed to enter funds of his choice then
*  validate them against T5515.
*
*  Check that none of the values entered are above 100.
*
*  Ensure that at least one entry has been made and that all
*  the entered values add up to 100.
*  Updating
*  --------
*  If 'CF11' was pressed the USWD record held in the USWDIO
*  module should be removed. If 'CF11' was pressed perform a
*  RLSE on USWD and skip the rest of this section.
*
*  Update the stored USWD record with the Target Fund details
*  including the currency code for each fund obtained from
*  T5515. Use a value in TRANNO that is one greater than that
*  from CHDRMJA. Perform a WRITS on USWD.
*
*  For each source fund create a negative UTRN record. This
*  will be keyed on the same key as the USWD record plus the
*  Source Fund and Fund Type.
*
*  Set the following fields on the UTRN record:
*
*     UTRN-DATA-KEY          - full key, (using the new TRANNO)
*     UTRN-TERMID            - \
*     UTRN-TRANSACTION-DATE  -  |
*     UTRN-TRANSACTION-TIME  -  |
*     UTRN-USER              -  | Standard WSSP information.
*     UTRN-BATCCOY           -  |
*     UTRN-BATCBRN           -  |
*     UTRN-BATCACTYR         -  |
*     UTRN-BATCACTMN         -  |
*     UTRN-BATCTRCDE         - /
*     UTRN-UNIT-SUB-ACCOUNT  - 'INIT' or 'ACUM'
*     UTRN-NOW-DEFER-IND     - Deallocation Indicator from
*                              T5515
*     UTRN-MONIES-DATE       - Switch Date
*     UTRN-CRTABLE           - CRTABLE from COVRMJA
*     UTRN-CNTCURR           - Contract Currency from CHDRMJA
*     UTRN-FEEDBACK-IND      - space
*     UTRN-CONTRACT-AMOUNT   - Enter the amount if AMOUNT was
*                              specified, as a negative value,
*                              otherwise set to zero. This will
*                              be in Fund Currency. Use the
*                              XCVRT subroutine to convert the
*                              entered amount from fund currency
*                              to contract currency.
*     UTRN-FUND-CURRENCY     - The fund currency from T5515
*     UTRN-FUND-AMOUNT       - zero
*     UTRN-SACSCODE          - \
*     UTRN-SACSTYP           -  from T5645, keyed by program id.
*                               line number 1
*     UTRN-GENLCODE          - /
*     UTRN-CONTRACT-TYPE     - Contract Type from CHDRMJA
*     UTRN-TRIGGER-MODULE    - 'FUNDSWCH'
*     UTRN-TRIGGER-KEY       -  The key to USWD except TRANNO.
*     UTRN-PROC-SEQ-NO       - ?
*     UTRN-SVP               - ?
*     UTRN-DISCOUNT-FACTOR   - ?
*     UTRN-SURRENDER-PERCENT - zero if AMOUNT entered, otherwise
*                              the entered percentage.
*     UTRN-CR-COM-DATE       - Coverage/Rider Commencement Date
*     UTRN-SWITCH-INDICATOR  - 'Y'
*
*  Perform a READR on USWH using the new TRANNO. If one does
*  not exist then create one using the incremented TRANNO that
*  was used on USWD.
*  Next Program
*  ------------
*  If "CF11" was requested move spaces to the current program
*  position and action field and exit otherwise add 1
*  to the program pointer and exit.
*  Notes.
*  ------
*  Tables Used:
*  T3588 - Contract Premium Status  Key: PSTATCODE  (CHDRMJA)
*  T3623 - Contract Risk Status     Key: STATCODE   (CHDRMJA)
*  T5515 - Virtual Funds            Key: FUND Code
*  T5681 - Coverage Premium Status  Key: PSTATCODE  (COVRMJA)
*  T5682 - Coverage Risk Status     Key: STATCODE   (COVRMJA)
*  T5688 - Contract Structure       Key: CNTTYPE
*  T5671 - Component Processing     Key: BATCTRCDE + CRTABLE
*  T5551 - Unit linked Edit Rules   Key: Validation item key
*                                        from T5671 + CURRCODE
*
*****************************************************************
* </pre>
*/
public class Pd5fr extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5FR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT5544Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaUsmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 4);

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Part1 = new FixedLengthStringData(5).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Part2 = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);

	private FixedLengthStringData wsaaIfValidFund = new FixedLengthStringData(1).init(SPACES);
	private Validator wsaaValidFund = new Validator(wsaaIfValidFund, "Y");

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);
	private FixedLengthStringData wsaaWholePlanFlag = new FixedLengthStringData(1);
	private String wsaaT5671Found = "N";
	private String wsaaBlank = "";

	private FixedLengthStringData wsaaTargetFunds = new FixedLengthStringData(48);
	private FixedLengthStringData wsaaTargFunds = new FixedLengthStringData(48).isAPartOf(wsaaTargetFunds, 0);
	private FixedLengthStringData wsaaTgFnds = new FixedLengthStringData(48).isAPartOf(wsaaTargFunds, 0, REDEFINE);
	private FixedLengthStringData[] wsaaTFnd = FLSArrayPartOfStructure(12, 4, wsaaTgFnds, 0);
		/* WSAA-TEMP-TARG-FUNDS */
	private FixedLengthStringData wsaaTempFunds = new FixedLengthStringData(48);

	private FixedLengthStringData wsaaTempFnds = new FixedLengthStringData(48).isAPartOf(wsaaTempFunds, 0, REDEFINE);
	private FixedLengthStringData[] wsaaTempFnd = FLSArrayPartOfStructure(12, 4, wsaaTempFnds, 0);

		/* WSAA-TARGET-AMTS */
	private FixedLengthStringData wsaaTargAmts = new FixedLengthStringData(30);
	private PackedDecimalData[] wsaaTAmt = PDArrayPartOfStructure(10, 5, 2, wsaaTargAmts, 0);
	private String wsaaDefTFnd = "";
	private String wsaaAlfnds = "";
	private PackedDecimalData wsaaTotal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub3 = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1);
	private Validator initialUnit = new Validator(wsaaUnitType, "I");
	private String wsaaP = "P";
	private String wsaaUnits = "U";
	private String wsaaInit = "INIT";
	private String wsaaAcum = "ACUM";

	private FixedLengthStringData wsaaIASrcInd = new FixedLengthStringData(2);
	private Validator initOnlySrcfund = new Validator(wsaaIASrcInd, "YN");
	private Validator acumOnlySrcfund = new Validator(wsaaIASrcInd, "NY");
	private Validator initAndAcumSrcfund = new Validator(wsaaIASrcInd, "YY");

	private FixedLengthStringData wsaaIAInd = new FixedLengthStringData(2).isAPartOf(wsaaIASrcInd, 0, REDEFINE);
	private FixedLengthStringData wsaaInitSource = new FixedLengthStringData(1).isAPartOf(wsaaIAInd, 0);
	private FixedLengthStringData wsaaAcumSource = new FixedLengthStringData(1).isAPartOf(wsaaIAInd, 1);
	private FixedLengthStringData wsaaEntry = new FixedLengthStringData(1);
	private String wsaaEntryFund = "";
	private String wsaaEditEffdate = "";
	private String wsaaScreenProcessed = "N";
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaChange = new FixedLengthStringData(1);
	private Validator changedCoverage = new Validator(wsaaChange, "Y");
	private String g695 = "G695";
	private String e631 = "E631";
	private String e207 = "E207";
	private String f025 = "F025";
	private String e761 = "E761";
	private String f348 = "F348";
	private String f665 = "F665";
	private String h456 = "H456";
	private String h503 = "H503";
	private String h974 = "H974";
	private String h975 = "H975";
	private String h976 = "H976";
	private String e308 = "E308";
	private String t044 = "T044";
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5515 = "T5515";
	private String t5688 = "T5688";
	private String t5544 = "T5544";
	private String t5551 = "T5551";
	private String t5671 = "T5671";
	private String t6647 = "T6647";
	private String t5687 = "T5687";
	private String itemrec = "ITEMREC";
	private String utrnrec = "UTRNREC";
	private String itdmrec = "ITEMREC";
	private String hitrrec = "HITRREC";
	
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Ccoverage/Rider details for UNL*/
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Interest Bearing Transaction Details*/
	private HitrTableDAM hitrIO = new HitrTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private T5543rec t5543rec = new T5543rec();
	private T5544rec t5544rec = new T5544rec();
	private T5551rec t5551rec = new T5551rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
		/*Unit Switch Details File.*/
	private UswdTableDAM uswdIO = new UswdTableDAM();
	//private UswdTableDAM uswdIOlocal = new UswdTableDAM();
	Uswdpf uswdIOlocal = new Uswdpf();
		/*Unit Switch Header Details.*/
	private UswhTableDAM uswhIO = new UswhTableDAM();
		/*UTRN LOGICAL VIEW.*/
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sd5frScreenVars sv = ScreenProgram.getScreenVars( Sd5frScreenVars.class);
	
	private UswdpfDAO objuswd = new UswdpfDAOImpl();
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Pd5fr.class);
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		retrvCovr1035, 
		fundCheck1054, 
		fundLoop1055, 
		secFundLoop1056, 
		endFundLoop1057, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		t55151410, 
		format1420, 
		tgtLoop1440, 
		addLine1450, 
		exit1490, 
		preExit, 
		checkErrors2020, 
		validateSubfile2030, 
		checkForErrors2050, 
		exit2090, 
		validation2620, 
		tgtSrcCompareLoop2621, 
		endCompareLoop2622, 
		updateErrorIndicators2670, 
		exit2790, 
		inputCheck3020, 
		nextSubfile3030, 
		exit3090, 
		uswdLoop3220, 
		exit3290, 
		exit4090,
		next6050,
		exit6090,
		utrnLoop6020
	}

	public Pd5fr() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5fr", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					retrUswd1020();
					retrUswh1030();
				}
				case retrvCovr1035: {
					retrvCovr1035();
					tables1050();
				}
				case fundCheck1054: {
					fundCheck1054();
				}
				case fundLoop1055: {
					fundLoop1055();
				}
				case secFundLoop1056: {
					secFundLoop1056();
				}
				case endFundLoop1057: {
					endFundLoop1057();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (wsspcomn.secActn[wsspcomn.programPtr.toInt()]!=null && isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
	}

protected void retrUswd1020()
	{
		uswdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		if (isNE(uswdIO.getCoverage(),sv.coverage)) {
			wsaaChange.set("Y");
			wsaaScreenProcessed = "N";
		}
		if (isNE(wsaaScreenProcessed,"Y")
		|| isNE(wsspcomn.lastActn,"L")
		|| changedCoverage.isTrue()) {
			scrnparams.function.set(varcom.sclr);
			processScreen("SD5FR", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.subfileRrn.set(1);
			syserrrec.subrname.set(wsaaProg);
			wsaaBatckey.set(wsspcomn.batchkey);
			wsaaTempFunds.set(SPACES);
			wsaaTargFunds.set(SPACES);
			wsaaWholePlanFlag.set(SPACES);
			sv.dataArea.set(SPACES);
			sv.subfileArea.set(SPACES);
			sv.effdate.set(varcom.vrcmMaxDate);
			sv.pcntamt.set(ZERO);
			sv.percentage.set(ZERO);
			sv.numpols.set(ZERO);
			sv.planSuffix.set(ZERO);
		}
	}

protected void retrUswh1030()
	{
		if (isEQ(wsaaWholePlanFlag,"Y")) {
			goTo(GotoLabel.retrvCovr1035);
		}
		uswhIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError600();
		}
		wsaaWholePlanFlag.set("Y");
	}

protected void retrvCovr1035()
	{
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		/*HEADS*/
		screenHeads1300();
		if (isNE(sv.chdrnumErr,SPACES)) {
			scrnparams.function.set(varcom.prot);
		}
	}

protected void tables1050()
	{
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covrmjaIO.getCrtable());
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5551Item.set(SPACES);
		wsaaT5671Found = "N";
		for (x.set(1); !(isGT(x,4)
		|| isEQ(wsaaT5671Found,"Y")); x.add(1)){
			if (isEQ(t5671rec.pgm[x.toInt()],wsaaProg)) {
				wsaaT5551Part1.set(t5671rec.edtitm[x.toInt()]);
				wsaaT5671Found = "Y";
				if (isEQ(t5671rec.edtitm[x.toInt()],SPACES)) {
					scrnparams.errorCode.set(f025);
					wsspcomn.edterror.set("Y");
					x.set(5);
				}
				else {
					wsaaT5551Part1.set(t5671rec.edtitm[x.toInt()]);
					wsaaT5671Found = "Y";
				}
			}
		}
		if (isEQ(wsaaT5671Found,"N")) {
			scrnparams.errorCode.set(f025);
			wsspcomn.edterror.set("Y");
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
			readT55511500();
		}
		if (isEQ(t5551rec.alfnds,SPACES)) {
			wsaaTempFunds.set(uswdIO.getTgtfunds());
			wsaaAlfnds = "N";
			goTo(GotoLabel.fundCheck1054);
		}
		wsaaTargFunds.set(SPACES);
		itemIO.setDataArea(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl("T5543");
		itemIO.setItemitem(t5551rec.alfnds);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5543rec.t5543Rec.set(itemIO.getGenarea());
		wsaaTempFunds.set(t5543rec.unitVirtualFunds);
		if (isEQ(wsspcomn.lastActn,"L")) {
			if (isEQ(t5551rec.alfnds,SPACES)) {
				wsaaTargetFunds.set(uswdIO.getTgtfunds());
				goTo(GotoLabel.endFundLoop1057);
			}
			else {
				wsaaTargetFunds.set(t5543rec.unitVirtualFunds);
				goTo(GotoLabel.endFundLoop1057);
			}
		}
	}

protected void fundCheck1054()
	{
		wsaaSub.set(1);
		wsaaSub2.set(0);
		wsaaSub3.set(0);
	}

protected void fundLoop1055()
	{
		if (isGT(wsaaSub,12)
		|| isEQ(wsaaTempFnd[wsaaSub.toInt()],SPACES)) {
			goTo(GotoLabel.endFundLoop1057);
		}
	}

protected void secFundLoop1056()
	{
		wsaaSub2.add(1);
		if (isGT(wsaaSub2,20)
		|| isEQ(uswdIO.getSrcfund(wsaaSub2),SPACES)) {
			wsaaSub3.add(1);
			if (isLTE(wsaaSub3, 12)) {
				wsaaTFnd[wsaaSub3.toInt()].set(wsaaTempFnd[wsaaSub.toInt()]);
				wsaaSub.add(1);
				wsaaSub2.set(0);
				goTo(GotoLabel.fundLoop1055);
			}
			else {
				goTo(GotoLabel.endFundLoop1057);
			}
		}
		if (isEQ(wsaaTempFnd[wsaaSub.toInt()],uswdIO.getSrcfund(wsaaSub2))) {
			goTo(GotoLabel.fundLoop1055);
		}
		else {
			goTo(GotoLabel.secFundLoop1056);
		}
	}

protected void endFundLoop1057()
	{
		wsaaSub.set(1);
		wsaaInitSource.set("N");
		wsaaAcumSource.set("N");
		scrnparams.subfileMore.set(SPACES);
		/*SUBFILE*/
		Uswdpf objus = new Uswdpf();
		objus.setChdrcoy(uswdIO.getChdrcoy().toString());
		objus.setChdrnum(uswdIO.getChdrnum().toString());
		objus.setSrcfund01(wsspcomn.batchkey2.toString());
		//objus.setScfndpol01(wsspcomn.environ01.toString());
		
		if(isEQ(wsspcomn.environ01.toString(),"P"))
			objus.setScfndpol01("1");
		else
			objus.setScfndpol01("2");
	
		
		List <Uswdpf> objlist=	objuswd.searchUswdpfRecord(objus);
		
		while ( !(isEQ(scrnparams.subfileMore,"N"))) {
			loadSubfile1400();
		}
		
		updateFundinfo(objlist);
	}

protected void updateFundinfo(List <Uswdpf> objlist)
{
	scrnparams.function.set(varcom.sstrt);
	processScreen("SD5FR", sv);
	if (isNE(scrnparams.statuz,varcom.oK)
	&& isNE(scrnparams.statuz,varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
 
	
	
	while (isNE(scrnparams.statuz,varcom.endp))
	{
		String sFund=sv.vfund.toString();		
		
		for(Uswdpf uw : objlist)
		{
			 
			 
			
			if(sFund.equals(uw.getTgtfund01()))
			{
				sv.percentage.set(uw.getTgtprcnt01());
			}
			else if(sFund.equals(uw.getTgtfund02()))
			{
				sv.percentage.set(uw.getTgtprcnt02());
			}
			else if(sFund.equals(uw.getTgtfund03()))
			{
				sv.percentage.set(uw.getTgtprcnt03());
			}
			else if(sFund.equals(uw.getTgtfund04()))
			{
				sv.percentage.set(uw.getTgtprcnt04());
			}
			else if(sFund.equals(uw.getTgtfund05()))
			{
				sv.percentage.set(uw.getTgtprcnt05());
			}
			else if(sFund.equals(uw.getTgtfund06()))
			{
				sv.percentage.set(uw.getTgtprcnt06());
			}
			else if(sFund.equals(uw.getTgtfund07()))
			{
				sv.percentage.set(uw.getTgtprcnt07());
			}
			else if(sFund.equals(uw.getTgtfund08()))
			{
				sv.percentage.set(uw.getTgtprcnt08());
			}
			else if(sFund.equals(uw.getTgtfund09()))
			{
				sv.percentage.set(uw.getTgtprcnt09());
			}
			else if(sFund.equals(uw.getTgtfund10()))
			{
				sv.percentage.set(uw.getTgtprcnt10());
			}
			else
			{
				sv.percentage.set(0);
			}
		}
		
		scrnparams.function.set(varcom.supd);
		processScreen("SD5FR", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		scrnparams.function.set(varcom.srdn);
		processScreen("SD5FR", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) 
		{
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}		
	}
}
protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void screenHeads1300()
	{
		chdrmja1310();
		accessT56881310();
		compLvlAcc1315();
		contStatus1320();
		premStatus1330();
		life1340();
		jlife1350();
		swDate1360();
		furtherFields1370();
		t56871380();
	}

protected void chdrmja1310()
	{
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isGT(chdrmjaIO.getPolinc(),chdrmjaIO.getPolsum())
		&& isEQ(covrmjaIO.getPlanSuffix(),chdrmjaIO.getPolinc())) {
			wsaaEditEffdate = "Y";
			sv.overrideFee.set("N");
		}
		else {
			wsaaEditEffdate = "N";
			sv.orswchfeOut[varcom.pr.toInt()].set("Y");
			sv.overrideFee.set(wssplife.wvswfee);
			sv.effdateOut[varcom.pr.toInt()].set("Y");
		}
		if ((isEQ(wsspcomn.lastActn,"O"))
		&& (isNE(wssplife.planPolicy,"X"))) {
			wssplife.planPolicy.set("X");
			wsspcomn.lastActn.set("X");
			sv.orswchfeOut[varcom.pr.toInt()].set("N");
			sv.overrideFee.set("N");
		}
		if ((isEQ(chdrmjaIO.getPolinc(),chdrmjaIO.getPolsum()))
		&& (isEQ(wsspcomn.lastActn,"L"))
		|| (isEQ(chdrmjaIO.getPolinc(),1))) {
			sv.overrideFee.set("N");
			sv.orswchfeOut[varcom.pr.toInt()].set("N");
		}
	}

protected void accessT56881310()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
	}

protected void compLvlAcc1315()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
       //Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		//End;
	
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError600();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void contStatus1320()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
	}

protected void premStatus1330()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void life1340()
	{
		lifeIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifeIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifeIO.setLife("01");
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
//		lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		lifeIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","JLIFE");
		//end;
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if (isNE(lifeIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		&& isNE(lifeIO.getChdrnum(),chdrmjaIO.getChdrnum())
		&& isNE(lifeIO.getLife(),"01")
		&& isNE(lifeIO.getJlife(),"00")) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeIO.getLifcnum());
		cltsIO.setClntnum(lifeIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jlife1350()
	{
		lifeIO.setJlife("01");
		lifeIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeIO);
		if (isEQ(lifeIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifeIO.getLifcnum());
			cltsIO.setClntnum(lifeIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

protected void swDate1360()
	{
		if (isNE(chdrmjaIO.getPolsum(),chdrmjaIO.getPolinc())
		&& isLT(uswdIO.getPlanSuffix(),chdrmjaIO.getPolinc())
		&& isNE(wssplife.effdate,varcom.vrcmMaxDate)) {
			sv.effdate.set(wssplife.effdate);
		}
		else {
			wssplife.effdate.set(uswdIO.getEffdate());
			sv.effdate.set(uswdIO.getEffdate());
		}
		sv.percentAmountInd.set(uswdIO.getPercentAmountInd());
		wsaaSub.set(1);
		sv.currcy.set(uswdIO.getScfndcur(wsaaSub));
		sv.fndtyp.set(uswdIO.getScfndtyp(wsaaSub));
		sv.unitVirtualFund.set(uswdIO.getSrcfund(wsaaSub));
		
		
		if (isEQ(wsspcomn.batchkey2,uswdIO.getSrcfund(wsaaSub)))
		{
			sv.pcntamt.set(uswdIO.getScprcamt(wsaaSub));
		    sv.fund.set(uswdIO.getSrcfund(wsaaSub));
			sv.fndcurr.set(uswdIO.getScfndcur(wsaaSub));
			sv.fndtype.set(uswdIO.getScfndtyp(wsaaSub));
			sv.pcnt.set(uswdIO.getScprcamt(wsaaSub));
		}
	}

protected void furtherFields1370()
	{
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.register.set(chdrmjaIO.getRegister());
		sv.planSuffix.set(uswdIO.getPlanSuffix());
		sv.numpols.set(chdrmjaIO.getPolinc());
		sv.life.set(uswdIO.getLife());
		sv.coverage.set(uswdIO.getCoverage());
		sv.rider.set(uswdIO.getRider());
		sv.crtable.set(covrmjaIO.getCrtable());
	}

protected void t56871380()
	{
		descIO.setDescitem(covrmjaIO.getCrtable());
		descIO.setDesctabl(t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		else {
			sv.crtabdesc.fill("?");
		}
	}

protected void loadSubfile1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case t55151410: {
					t55151410();
				}
				case format1420: {
					format1420();
				}
				case tgtLoop1440: {
					tgtLoop1440();
				}
				case addLine1450: {
					addLine1450();
					nextLine1460();
				}
				case exit1490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t55151410()
	{
		if (isGT(wsaaSub,12)) {
			goTo(GotoLabel.format1420);
		}
		if (isEQ(wsaaTFnd[wsaaSub.toInt()],SPACES)) {
			sv.vfund.set(SPACES);
			sv.fndcur.set(SPACES);
			sv.percentage.set(ZERO);
			goTo(GotoLabel.format1420);
		}
		if (isEQ(uswdIO.getTgtfund(1),SPACES)) {
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaTFnd[wsaaSub.toInt()]);
		itdmIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		  //end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5515");
		itdmIO.setItmfrm(sv.effdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),wsaaTFnd[wsaaSub.toInt()])
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		sv.fndcur.set(t5515rec.currcode);
		sv.vfund.set(wsaaTFnd[wsaaSub.toInt()]);
	}

protected void format1420()
	{
		if (isEQ(uswdIO.getSrcfund(wsaaSub),SPACES)) {
			sv.currcy.set(SPACES);
			sv.fndtyp.set(SPACES);
			sv.unitVirtualFund.set(SPACES);
			sv.pcntamt.set(ZERO);
		}
		else {
			if (isEQ(uswdIO.getScfndtyp(wsaaSub),"I")) {
				wsaaInitSource.set("Y");
			}
			else {
				wsaaAcumSource.set("Y");
			}
			sv.currcy.set(uswdIO.getScfndcur(wsaaSub));
			sv.fndtyp.set(uswdIO.getScfndtyp(wsaaSub));
			sv.unitVirtualFund.set(uswdIO.getSrcfund(wsaaSub));
			
			
			if (isEQ(wsspcomn.batchkey2,uswdIO.getSrcfund(wsaaSub)))
			{
			sv.pcntamt.set(uswdIO.getScprcamt(wsaaSub));
			sv.fund.set(uswdIO.getSrcfund(wsaaSub));
			sv.fndcurr.set(uswdIO.getScfndcur(wsaaSub));
			sv.fndtype.set(uswdIO.getScfndtyp(wsaaSub));
			sv.pcnt.set(uswdIO.getScprcamt(wsaaSub));
			
			}
		}
		wsaaSub2.set(1);
	}

protected void tgtLoop1440()
	{
		if (isGT(wsaaSub2,12)
		|| isEQ(uswdIO.getTgtfund(wsaaSub2),SPACES)) {
			goTo(GotoLabel.addLine1450);
		}
		if (isEQ(uswdIO.getTgtfund(wsaaSub2),wsaaTFnd[wsaaSub.toInt()])) {
			sv.percentage.set(ZERO);
			goTo(GotoLabel.addLine1450);
		}
		else {
			sv.percentage.set(ZERO);
		}
		wsaaSub2.add(1);
		goTo(GotoLabel.tgtLoop1440);
	}

protected void addLine1450()
	{
		if (isNE(wsaaAlfnds,"N")) {
			sv.vfundOut[varcom.pr.toInt()].set("Y");
			if (isEQ(sv.vfund,SPACES)) {
				sv.tgtpcntOut[varcom.pr.toInt()].set("Y");
			}
		}
		if(isNE(sv.fund,sv.vfund))
		{
			scrnparams.function.set(varcom.sadd);
			processScreen("SD5FR", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
	}

protected void nextLine1460()
	{
		wsaaSub.add(1);
		if ((isGT(wsaaSub,12)
		&& isEQ(uswdIO.getSrcfund(wsaaSub),SPACES))
		|| isGT(wsaaSub,20)) {
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit1490);
		}
		goTo(GotoLabel.t55151410);
	}

protected void readT55511500()
	{
		para1500();
	}

protected void para1500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		  //end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5551);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		wsaaT5551Part2.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5551Item);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5551)
		|| isNE(itdmIO.getItemitem(),wsaaT5551Item)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (wsspcomn.secActn[wsspcomn.programPtr.toInt()]!=null && isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsspcomn.lastActn,"L")
		&& isEQ(wsaaChange,"N")
		&& isEQ(wsaaScreenProcessed,"Y")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		else {
			wsaaChange.set("N");
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set("PROT");
		}
	}

protected void screenIo2010()
	{
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo12010();
				}
				case checkErrors2020: {
					checkErrors2020();
				}
				case validateSubfile2030: {
					validateSubfile2030();
				}
				case checkForErrors2050: {
					checkForErrors2050();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo12010()
	{
		if (isEQ(scrnparams.statuz,"SUBM ")) {
			wsspcomn.flag.set("K");
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsaaT5671Found,"N")) {
			scrnparams.errorCode.set(f025);
			scrnparams.function.set("PROT");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2050);
		}
		if (isEQ(scrnparams.statuz,"KILL")
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if ((isNE(sv.overrideFee,"Y"))
		&& (isNE(sv.overrideFee,"N"))) {
			sv.orswchfeErr.set(h503);
			goTo(GotoLabel.checkErrors2020);
		}
		if (isEQ(wsaaEditEffdate,"N")) {
			goTo(GotoLabel.validateSubfile2030);
		}
		if (isEQ(sv.effdate,ZERO)) {
			goTo(GotoLabel.checkErrors2020);
		}
		if (isEQ(sv.effdate,varcom.vrcmMaxDate)) {
			sv.effdateErr.set(f665);
			goTo(GotoLabel.checkErrors2020);
		}
		if (isLT(sv.effdate,covrmjaIO.getCrrcd())) {
			sv.effdateErr.set(t044);
		}
	}

protected void checkErrors2020()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2030()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("SD5FR", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			wsaaBlank = "Y";
		}
		wsaaTotal.set(ZERO);
		wsaaEntry.set(SPACES);
		sv.errorSubfile.set(SPACES);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isNE(wsaaEntry,"Y")
		&& isNE(wsaaEntryFund,"Y")
		&& isNE(wsaaBlank,"Y")) {
			sv.tgtpcntErr.set(e207);
			scrnparams.errorCode.set(e207);
			sv.tgtpcntOut[varcom.ri.toInt()].set("Y");
			goTo(GotoLabel.checkForErrors2050);
		}
		if (isNE(wsaaTotal,100)
		&& isNE(wsaaBlank,"Y")) {
			sv.tgtpcntErr.set(e631);
			scrnparams.errorCode.set(e631);
			sv.tgtpcntOut[varcom.ri.toInt()].set("Y");
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)
		|| isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.edterror,SPACES)) {
			wsaaScreenProcessed = "Y";
		}
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					checkEntry2610();
				}
				case validation2620: {
					validation2620();
				}
				case tgtSrcCompareLoop2621: {
					tgtSrcCompareLoop2621();
				}
				case endCompareLoop2622: {
					endCompareLoop2622();
					exitFunds2625();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkEntry2610()
	{
		wsaaIfValidFund.set("N");
		if (isNE(sv.vfund,SPACES)
		&& isNE(t5551rec.alfnds,SPACES)) {
			for (wsaaSub3.set(1); !(isGT(wsaaSub3,12)
			|| wsaaValidFund.isTrue()); wsaaSub3.add(1)){
				validateFund2700();
			}
			if (!wsaaValidFund.isTrue()) {
				sv.vfundErr.set(h456);
			}
		}
		if (isGT(sv.percentage,ZERO)) {
			wsaaEntry.set("Y");
			if (isNE(sv.vfund,SPACES)) {
				wsaaEntryFund = "Y";
				goTo(GotoLabel.validation2620);
			}
			else {
				sv.vfundErr.set(e761);
			}
		}
		else {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
	}

protected void validation2620()
	{
		if (isNE(wsaaAlfnds,"N")) {
			goTo(GotoLabel.endCompareLoop2622);
		}
		wsaaSub.set(1);
	}

protected void tgtSrcCompareLoop2621()
	{
		if (isGT(wsaaSub,12)
		|| isEQ(uswdIO.getSrcfund(wsaaSub),SPACES)) {
			goTo(GotoLabel.endCompareLoop2622);
		}
		if (isEQ(uswdIO.getSrcfund(wsaaSub),sv.vfund)) {
			sv.vfundErr.set(h974);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		wsaaSub.add(1);
		goTo(GotoLabel.tgtSrcCompareLoop2621);
	}

protected void endCompareLoop2622()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.vfund);
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		  //end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5515");
		itdmIO.setItmfrm(sv.effdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),sv.vfund)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.vfundErr.set(g695);
			t5515rec.t5515Rec.set(SPACES);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		sv.fndcur.set(t5515rec.currcode);
	}

protected void exitFunds2625()
	{
		if (isEQ(t5515rec.unitType,"I")
		&& !initOnlySrcfund.isTrue()) {
			sv.vfundErr.set(h975);
		}
		if (isEQ(t5515rec.unitType,"A")
		&& !acumOnlySrcfund.isTrue()) {
			sv.vfundErr.set(h976);
		}
		/*AMOUNTS*/
		wsaaTotal.add(sv.percentage);
		if (isGT(sv.percentage,100)) {
			sv.tgtpcntErr.set(f348);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SD5FR", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SD5FR", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void validateFund2700()
	{
		try {
			start2700();
		}
		catch (GOTOException e){
		}
	}

protected void start2700()
	{
		if (isEQ(t5543rec.unitVirtualFund[wsaaSub3.toInt()],SPACES)) {
			goTo(GotoLabel.exit2790);
		}
		if (isEQ(sv.vfund,t5543rec.unitVirtualFund[wsaaSub3.toInt()])) {
			wsaaIfValidFund.set("Y");
			goTo(GotoLabel.exit2790);
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					deleteUtrns6000();
					updatePara3000();
					startSubfile3010();
				}
				case inputCheck3020: {
					inputCheck3020();
				}
				case nextSubfile3030: {
					nextSubfile3030();
					writeUswdUtrn3070();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updatePara3000()
	{
		uswdIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.flag.set("K");
		}
		if (isEQ(scrnparams.statuz,"KILL")
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void startSubfile3010()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("SD5FR", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaSub.set(1);
		wsaaSub.set(1);
		initUtrnRec3100();
		a100InitHitrRec();
		tgtUswdSrcUtrn3200();
		wsaaSub.set(1);
	}

protected void inputCheck3020()
	{
		if (isLTE(sv.percentage,ZERO)) {
			goTo(GotoLabel.nextSubfile3030);
		}
		uswdIO.setTgfndcur(wsaaSub, sv.fndcur);
		uswdIO.setTgtprcnt(wsaaSub, sv.percentage);
		uswdIO.setTgtfund(wsaaSub, sv.vfund);
		wsaaSub.add(1);
	}

protected void nextSubfile3030()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SD5FR", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.inputCheck3020);
		}
	}

protected void writeUswdUtrn3070()
	{
		uswdIO.setEffdate(sv.effdate);
		wssplife.effdate.set(sv.effdate);
		uswdIO.setOverrideFee(sv.overrideFee);
		wssplife.wvswfee.set(sv.overrideFee);
	//	uswdIO.setFunction(varcom.updat);
		//SmartFileCode.execute(appVars, uswdIO);
		//if (isNE(uswdIO.getStatuz(),varcom.oK)) {
			//syserrrec.params.set(uswdIO.getParams());
			//fatalError600();
		//}
		writeUswd100();
		wsspcomn.currfrom.set(sv.effdate);
	}

protected void initUtrnRec3100()
	{
		para3110();
	}


protected void writeUswd100()
{
			LOGGER.info("Insert uswd");
	
			uswdIOlocal.setChdrcoy(uswdIO.getChdrcoy().toString());
			uswdIOlocal.setChdrnum(uswdIO.getChdrnum().toString());
			uswdIOlocal.setLife(uswdIO.getLife().toString());
			uswdIOlocal.setCoverage(uswdIO.getCoverage().toString());
			uswdIOlocal.setRider(uswdIO.getRider().toString());			
			uswdIOlocal.setPlnsfx(uswdIO.getPlanSuffix().toInt());
			uswdIOlocal.setTranno(uswdIO.getTranno().toInt());
		
			uswdIOlocal.setSrcfund01(wsspcomn.batchkey2.toString());		
			uswdIOlocal.setSrcfund02(SPACES.toString());
			uswdIOlocal.setSrcfund03(SPACES.toString());
			uswdIOlocal.setSrcfund04(SPACES.toString());
			uswdIOlocal.setSrcfund05(SPACES.toString());
			uswdIOlocal.setSrcfund06(SPACES.toString());
			uswdIOlocal.setSrcfund07(SPACES.toString());
			uswdIOlocal.setSrcfund08(SPACES.toString());
			uswdIOlocal.setSrcfund09(SPACES.toString());
			uswdIOlocal.setSrcfund10(SPACES.toString());
		
			uswdIOlocal.setScfndtyp01(uswdIO.getSourceFundType01().toString());
			uswdIOlocal.setScfndtyp02(SPACES.toString());
			uswdIOlocal.setScfndtyp03(SPACES.toString());
			uswdIOlocal.setScfndtyp04(SPACES.toString());
			uswdIOlocal.setScfndtyp05(SPACES.toString());
			uswdIOlocal.setScfndtyp06(SPACES.toString());
			uswdIOlocal.setScfndtyp07(SPACES.toString());
			uswdIOlocal.setScfndtyp08(SPACES.toString());
			uswdIOlocal.setScfndtyp09(SPACES.toString());
			uswdIOlocal.setScfndtyp10(SPACES.toString());
		 
			uswdIOlocal.setScfndcur01(uswdIO.getSourceFundCurrency01().toString());
			uswdIOlocal.setScfndcur02(uswdIO.getSourceFundCurrency02().toString());
			uswdIOlocal.setScfndcur03(uswdIO.getSourceFundCurrency03().toString());
			uswdIOlocal.setScfndcur04(uswdIO.getSourceFundCurrency04().toString());
			uswdIOlocal.setScfndcur05(uswdIO.getSourceFundCurrency05().toString());
			uswdIOlocal.setScfndcur06(uswdIO.getSourceFundCurrency06().toString());
			uswdIOlocal.setScfndcur07(uswdIO.getSourceFundCurrency07().toString());
			uswdIOlocal.setScfndcur08(uswdIO.getSourceFundCurrency08().toString());
			uswdIOlocal.setScfndcur09(uswdIO.getSourceFundCurrency09().toString());
			uswdIOlocal.setScfndcur10(uswdIO.getSourceFundCurrency10().toString());
			ZonedDecimalData nZero = new ZonedDecimalData(10);
			nZero.set(ZERO);
			uswdIOlocal.setScprcamt01(sv.pcnt.getbigdata());
			uswdIOlocal.setScprcamt02(nZero.getbigdata());
			uswdIOlocal.setScprcamt03(nZero.getbigdata());
			uswdIOlocal.setScprcamt04(nZero.getbigdata());
			uswdIOlocal.setScprcamt05(nZero.getbigdata());
			uswdIOlocal.setScprcamt06(nZero.getbigdata());
			uswdIOlocal.setScprcamt07(nZero.getbigdata());
			uswdIOlocal.setScprcamt08(nZero.getbigdata());
			uswdIOlocal.setScprcamt09(nZero.getbigdata());
			uswdIOlocal.setScprcamt10(nZero.getbigdata());
			
			uswdIOlocal.setScestval01(uswdIO.getSourceEstimatedValue01().getbigdata());
			uswdIOlocal.setScestval02(nZero.getbigdata());
			uswdIOlocal.setScestval03(nZero.getbigdata());
			uswdIOlocal.setScestval04(nZero.getbigdata());
			uswdIOlocal.setScestval05(nZero.getbigdata());
			uswdIOlocal.setScestval06(nZero.getbigdata());
			uswdIOlocal.setScestval07(nZero.getbigdata());
			uswdIOlocal.setScestval08(nZero.getbigdata());
			uswdIOlocal.setScestval09(nZero.getbigdata());
			uswdIOlocal.setScestval10(nZero.getbigdata());
			
			uswdIOlocal.setScactval01(uswdIO.getSourceActualValue01().getbigdata());
			uswdIOlocal.setScactval02(nZero.getbigdata());
			uswdIOlocal.setScactval03(nZero.getbigdata());
			uswdIOlocal.setScactval04(nZero.getbigdata());
			uswdIOlocal.setScactval05(nZero.getbigdata());
			uswdIOlocal.setScactval06(nZero.getbigdata());
			uswdIOlocal.setScactval07(nZero.getbigdata());
			uswdIOlocal.setScactval08(nZero.getbigdata());
			uswdIOlocal.setScactval09(nZero.getbigdata());
			uswdIOlocal.setScactval10(nZero.getbigdata());
			
			uswdIOlocal.setTgtfund01(uswdIO.getTargetFund01().toString());
			uswdIOlocal.setTgtfund02(uswdIO.getTargetFund02().toString());
			uswdIOlocal.setTgtfund03(uswdIO.getTargetFund03().toString());
			uswdIOlocal.setTgtfund04(uswdIO.getTargetFund04().toString());
			uswdIOlocal.setTgtfund05(uswdIO.getTargetFund05().toString());
			uswdIOlocal.setTgtfund06(uswdIO.getTargetFund06().toString());
			uswdIOlocal.setTgtfund07(uswdIO.getTargetFund07().toString());
			uswdIOlocal.setTgtfund08(uswdIO.getTargetFund08().toString());
			uswdIOlocal.setTgtfund09(uswdIO.getTargetFund09().toString());
			uswdIOlocal.setTgtfund10(uswdIO.getTargetFund10().toString());
			
			uswdIOlocal.setTgfndtyp01(uswdIO.getTargetFundType01().toString());
			uswdIOlocal.setTgfndtyp02(uswdIO.getTargetFundType02().toString());
			uswdIOlocal.setTgfndtyp03(uswdIO.getTargetFundType03().toString());
			uswdIOlocal.setTgfndtyp04(uswdIO.getTargetFundType04().toString());
			uswdIOlocal.setTgfndtyp05(uswdIO.getTargetFundType05().toString());
			uswdIOlocal.setTgfndtyp06(uswdIO.getTargetFundType06().toString());
			uswdIOlocal.setTgfndtyp07(uswdIO.getTargetFundType07().toString());
			uswdIOlocal.setTgfndtyp08(uswdIO.getTargetFundType08().toString());
			uswdIOlocal.setTgfndtyp09(uswdIO.getTargetFundType09().toString());
			uswdIOlocal.setTgfndtyp10(uswdIO.getTargetFundType10().toString());
			
			uswdIOlocal.setTgfndcur01(uswdIO.getTargetFundCurrency01().toString());
			uswdIOlocal.setTgfndcur02(uswdIO.getTargetFundCurrency02().toString());
			uswdIOlocal.setTgfndcur03(uswdIO.getTargetFundCurrency03().toString());
			uswdIOlocal.setTgfndcur04(uswdIO.getTargetFundCurrency04().toString());
			uswdIOlocal.setTgfndcur05(uswdIO.getTargetFundCurrency05().toString());
			uswdIOlocal.setTgfndcur06(uswdIO.getTargetFundCurrency06().toString());
			uswdIOlocal.setTgfndcur07(uswdIO.getTargetFundCurrency07().toString());
			uswdIOlocal.setTgfndcur08(uswdIO.getTargetFundCurrency08().toString());
			uswdIOlocal.setTgfndcur09(uswdIO.getTargetFundCurrency09().toString());
			uswdIOlocal.setTgfndcur10(uswdIO.getTargetFundCurrency10().toString());
			
			uswdIOlocal.setTgtprcnt01(uswdIO.getTargetPercent01().getbigdata());
			uswdIOlocal.setTgtprcnt02(uswdIO.getTargetPercent02().getbigdata());
			uswdIOlocal.setTgtprcnt03(uswdIO.getTargetPercent03().getbigdata());
			uswdIOlocal.setTgtprcnt04(uswdIO.getTargetPercent04().getbigdata());
			uswdIOlocal.setTgtprcnt05(uswdIO.getTargetPercent05().getbigdata());
			uswdIOlocal.setTgtprcnt06(uswdIO.getTargetPercent06().getbigdata());
			uswdIOlocal.setTgtprcnt07(uswdIO.getTargetPercent07().getbigdata());
			uswdIOlocal.setTgtprcnt08(uswdIO.getTargetPercent08().getbigdata());
			uswdIOlocal.setTgtprcnt09(uswdIO.getTargetPercent09().getbigdata());
			uswdIOlocal.setTgtprcnt10(uswdIO.getTargetPercent10().getbigdata());
			uswdIOlocal.setEffdate(uswdIO.getEffdate().toInt());
			uswdIOlocal.setPcamtind("P");
			//uswdIOlocal.setEffdate(uswdIO.getOverrideFee().toInt());
			//uswdIOlocal.setFormat(uswdIO.getFormat());
			//uswdIOlocal.setFunction(varcom.writr);
			//SmartFileCode.execute(appVars, uswdIOlocal);			
			//if (isNE(uswdIOlocal.getStatuz(),varcom.oK)) 
			//{
				//syserrrec.params.set(uswdIOlocal.getParams());
				//fatalError600();
			//}	 
			if(isEQ(wsspcomn.environ01.toString(),"P"))
					uswdIOlocal.setScfndpol01("1");
			else
				 uswdIOlocal.setScfndpol01("2");
			
			//uswdIOlocal.setScfndpol01(wsspcomn.environ01.toString());
			
			objuswd.deleteUswdpf(uswdIOlocal);
			objuswd.insertIntoUswdpf(uswdIOlocal);
			
			LOGGER.info("After Insert uswd");
}

protected void para3110()
	{
		utrnIO.setDataArea(SPACES);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setUstmno(ZERO);
		setPrecision(utrnIO.getTranno(), 0);
		utrnIO.setTranno(add(1,chdrmjaIO.getTranno()));
		utrnIO.setChdrcoy(uswdIO.getChdrcoy());
		utrnIO.setChdrnum(uswdIO.getChdrnum());
		utrnIO.setPlanSuffix(uswdIO.getPlanSuffix());
		utrnIO.setLife(uswdIO.getLife());
		utrnIO.setCoverage(uswdIO.getCoverage());
		utrnIO.setRider(uswdIO.getRider());
		utrnIO.setUser(varcom.vrcmUser);
		utrnIO.setTermid(varcom.vrcmTermid);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		utrnIO.setTransactionDate(varcom.vrcmDate);
		utrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		utrnIO.setContractType(chdrmjaIO.getCnttype());
		utrnIO.setCntcurr(chdrmjaIO.getCntcurr());
		utrnIO.setCrtable(covrmjaIO.getCrtable());
		utrnIO.setCrComDate(covrmjaIO.getCrrcd());
		utrnIO.setTriggerModule("FUNDSWCH");
		utrnIO.setTriggerKey(SPACES);
		utrnIO.setFeedbackInd(SPACES);
		utrnIO.setSvp(1);
		utrnIO.setDiscountFactor(1);
		utrnIO.setCanInitUnitInd("N");
		utrnIO.setSwitchIndicator("Y");
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		  //end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T6647");
		itdmIO.setItmfrm(sv.effdate);
		wsaaTranscode.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrmjaIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6647)
		|| isNE(itdmIO.getItemitem(),wsaaT6647Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		if (isEQ(t6647rec.efdcode,"DD")) {
			utrnIO.setMoniesDate(sv.effdate);
		}
		else {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			utrnIO.setMoniesDate(datcon1rec.intDate);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		  //end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5544);
		wsaaUsmeth.set(t6647rec.swmeth);
		wsaaCurrcode.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5544Key);
		itdmIO.setItmfrm(sv.effdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5544)
		|| isNE(itdmIO.getItemitem(),wsaaT5544Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t5544rec.t5544Rec.set(itdmIO.getGenarea());
		}
		utrnIO.setNowDeferInd(t5544rec.nowDeferInd);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl("T5645");
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
	}

protected void tgtUswdSrcUtrn3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case uswdLoop3220: {
					uswdLoop3220();
				}
				case exit3290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void uswdLoop3220()
{
	if (isGT(wsaaSub,20)) {
		goTo(GotoLabel.exit3290);
	}
	if (isLTE(wsaaSub,12)) {
		uswdIO.setTgtfund(wsaaSub, SPACES);
		uswdIO.setTgfndtyp(wsaaSub, SPACES);
		uswdIO.setTgfndcur(wsaaSub, SPACES);
		uswdIO.setTgtprcnt(wsaaSub, ZERO);
	}
	if (isNE(uswdIO.getSrcfund(wsaaSub),SPACES)) {
		a200ReadT5515();
		if (isEQ(t5515rec.zfundtyp,"D")) {
			a300WriteHitr();
		}
		 
			utrnIO.setUnitVirtualFund(uswdIO.getSrcfund(wsaaSub));
			utrnIO.setFundCurrency(uswdIO.getScfndcur(wsaaSub));
			utrnIO.setUnitType(uswdIO.getScfndtyp(wsaaSub));	
			wsaaUnitType.set(uswdIO.getScfndtyp(wsaaSub));
			if (initialUnit.isTrue()) {
				utrnIO.setUnitSubAccount(wsaaInit);
			}
			else {
				utrnIO.setUnitSubAccount(wsaaAcum);
			}
			if (isEQ(uswdIO.getPercentAmountInd(),wsaaP)) {
				utrnIO.setSurrenderPercent(uswdIO.getScprcamt(wsaaSub));
				utrnIO.setContractAmount(ZERO);
			}
			else {
				if (isEQ(uswdIO.getPercentAmountInd(),wsaaUnits)) {
					setPrecision(utrnIO.getNofDunits(), 0);
					utrnIO.setNofDunits(mult(uswdIO.getScprcamt(wsaaSub),-1));
					utrnIO.setContractAmount(ZERO);
				}
				else {
					setPrecision(utrnIO.getContractAmount(), 0);
					utrnIO.setContractAmount(mult(uswdIO.getScprcamt(wsaaSub),-1));
					utrnIO.setSurrenderPercent(ZERO);
				}
			}
			if(isEQ(wsspcomn.environ01.toString(),"P"))
				utrnIO.setFundPool("1");
			else
				utrnIO.setFundPool("2");
			
			utrnIO.setFormat(utrnrec);
			utrnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, utrnIO);
			if (isNE(utrnIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(utrnIO.getParams());
				fatalError600();
			}
		 
	}
	wsaaSub.add(1);
	goTo(GotoLabel.uswdLoop3220);
}

protected void a100InitHitrRec()
	{
		a110Para();
	}

protected void a110Para()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setContractAmount(ZERO);
		hitrIO.setZintrate(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		setPrecision(hitrIO.getTranno(), 0);
		hitrIO.setTranno(add(1,chdrmjaIO.getTranno()));
		hitrIO.setChdrcoy(uswdIO.getChdrcoy());
		hitrIO.setChdrnum(uswdIO.getChdrnum());
		hitrIO.setPlanSuffix(uswdIO.getPlanSuffix());
		hitrIO.setLife(uswdIO.getLife());
		hitrIO.setCoverage(uswdIO.getCoverage());
		hitrIO.setRider(uswdIO.getRider());
		hitrIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		hitrIO.setCnttyp(chdrmjaIO.getCnttype());
		hitrIO.setCntcurr(chdrmjaIO.getCntcurr());
		hitrIO.setCrtable(covrmjaIO.getCrtable());
		hitrIO.setTriggerModule("FUNDSWCH");
		hitrIO.setTriggerKey(SPACES);
		hitrIO.setFeedbackInd(SPACES);
		hitrIO.setSvp(1);
		hitrIO.setSwitchIndicator("Y");
		hitrIO.setZintalloc("N");
		hitrIO.setZrectyp("P");
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setEffdate(sv.effdate);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			hitrIO.setSacscode(t5645rec.sacscode02);
			hitrIO.setSacstyp(t5645rec.sacstype02);
			hitrIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode01);
			hitrIO.setSacstyp(t5645rec.sacstype01);
			hitrIO.setGenlcde(t5645rec.glmap01);
		}
	}

protected void a200ReadT5515()
	{
		a210T5515();
	}

protected void a210T5515()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(uswdIO.getSrcfund(wsaaSub));
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		  //end;
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),uswdIO.getSrcfund(wsaaSub))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void a300WriteHitr()
	{
		a310Hitr();
	}

protected void a310Hitr()
	{
		hitrIO.setZintbfnd(uswdIO.getSrcfund(wsaaSub));
		hitrIO.setFundCurrency(uswdIO.getScfndcur(wsaaSub));
		if (isEQ(uswdIO.getPercentAmountInd(),wsaaP)) {
			hitrIO.setSurrenderPercent(uswdIO.getScprcamt(wsaaSub));
			hitrIO.setContractAmount(ZERO);
		}
		else {
			setPrecision(hitrIO.getContractAmount(), 0);
			hitrIO.setContractAmount(mult(uswdIO.getScprcamt(wsaaSub),-1));
			hitrIO.setSurrenderPercent(ZERO);
		}
		
		hitrIO.setFundPool("1");
		
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		try {
			para4000();
		}
		catch (GOTOException e){
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*NEXT-PROGRAM*/
		//wsspcomn.programPtr.set(1);		
	
		wsspcomn.nextprog.set("PD5FQ");
		appVars.commit();
	}

protected void deleteUtrns6000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				delete6010();
			case utrnLoop6020: 
				utrnLoop6020();
			case next6050: 
				next6050();
			case exit6090: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}


protected void delete6010()
{
	/*    Delete UTRN's previously written by this transactiion.       */
	/*    This only happens during modify mode.                        */
	utrnIO.setDataKey(SPACES);
	utrnIO.setChdrcoy(uswdIO.getChdrcoy());
	utrnIO.setChdrnum(uswdIO.getChdrnum());
	utrnIO.setPlanSuffix(uswdIO.getPlanSuffix());
	utrnIO.setLife(uswdIO.getLife());
	utrnIO.setCoverage(uswdIO.getCoverage());
	utrnIO.setRider(uswdIO.getRider());
	utrnIO.setTranno((add(1,chdrmjaIO.getTranno())));
	hitrIO.setDataKey(SPACES);
	hitrIO.setChdrcoy(uswdIO.getChdrcoy());
	hitrIO.setChdrnum(uswdIO.getChdrnum());
	hitrIO.setPlanSuffix(uswdIO.getPlanSuffix());
	hitrIO.setLife(uswdIO.getLife());
	hitrIO.setCoverage(uswdIO.getCoverage());
	hitrIO.setRider(uswdIO.getRider());
	hitrIO.setTranno((add(1,chdrmjaIO.getTranno())));
	wsaaSub.set(1);
}

protected void utrnLoop6020()
{
	if (isGT(wsaaSub, 20)) {
		goTo(GotoLabel.exit4090);
	}
	 
	if (isEQ(t5515rec.zfundtyp, "D")) {
		hitrIO.setFunction(varcom.readh);
		hitrIO.setZintbfnd(uswdIO.getSrcfund(wsaaSub));
		SmartFileCode.execute(appVars, hitrIO);
		if (isEQ(hitrIO.getStatuz(), varcom.oK)) 
		{
			
			hitrIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, hitrIO);
			if (isNE(hitrIO.getStatuz(), varcom.oK)) 
			{
				syserrrec.params.set(hitrIO.getParams());
				fatalError600();
			}
		}
		goTo(GotoLabel.next6050);
	}
	utrnIO.setFunction(varcom.readh);
	utrnIO.setUnitVirtualFund(uswdIO.getSrcfund(wsaaSub));
	utrnIO.setFundCurrency(uswdIO.getScfndcur(wsaaSub));
	utrnIO.setUnitType(uswdIO.getScfndtyp(wsaaSub));
	 
	SmartFileCode.execute(appVars, utrnIO);
	if (isEQ(utrnIO.getStatuz(), varcom.oK)) {
		
	utrnIO.setFunction(varcom.delet);
	SmartFileCode.execute(appVars, utrnIO);
	if (isNE(utrnIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(utrnIO.getParams());
		fatalError600();
		}
	}
}

protected void next6050()
{
	wsaaSub.add(1);
	if (isEQ(uswdIO.getSrcfund(wsaaSub), SPACES)) {
		wsaaSub.set(1);
		return ;
	}
	goTo(GotoLabel.utrnLoop6020);
}

 
}
