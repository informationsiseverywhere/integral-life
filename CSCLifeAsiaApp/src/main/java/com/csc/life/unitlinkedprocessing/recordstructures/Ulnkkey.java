package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:25
 * Description:
 * Copybook name: ULNKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ulnkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ulnkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ulnkKey = new FixedLengthStringData(64).isAPartOf(ulnkFileKey, 0, REDEFINE);
  	public FixedLengthStringData ulnkChdrcoy = new FixedLengthStringData(1).isAPartOf(ulnkKey, 0);
  	public FixedLengthStringData ulnkChdrnum = new FixedLengthStringData(8).isAPartOf(ulnkKey, 1);
  	public FixedLengthStringData ulnkLife = new FixedLengthStringData(2).isAPartOf(ulnkKey, 9);
  	public FixedLengthStringData ulnkJlife = new FixedLengthStringData(2).isAPartOf(ulnkKey, 11);
  	public FixedLengthStringData ulnkCoverage = new FixedLengthStringData(2).isAPartOf(ulnkKey, 13);
  	public FixedLengthStringData ulnkRider = new FixedLengthStringData(2).isAPartOf(ulnkKey, 15);
  	public PackedDecimalData ulnkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ulnkKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(ulnkKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ulnkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ulnkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}