package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Incipf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface IncipfDAO extends BaseDAO<Incipf> {

	public List<Incipf> searchIncirevRecord(String coy, String chdrnum);
	public void deleteIncipfRecord(List<Incipf> taxdpfList);
}