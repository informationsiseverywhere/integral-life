package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:38
 * Description:
 * Copybook name: RDIRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rdirkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData rdirFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData rdirKey = new FixedLengthStringData(64).isAPartOf(rdirFileKey, 0, REDEFINE);
  	public FixedLengthStringData rdirChdrcoy = new FixedLengthStringData(1).isAPartOf(rdirKey, 0);
  	public FixedLengthStringData rdirChdrnum = new FixedLengthStringData(8).isAPartOf(rdirKey, 1);
  	public FixedLengthStringData rdirLife = new FixedLengthStringData(2).isAPartOf(rdirKey, 9);
  	public FixedLengthStringData rdirCoverage = new FixedLengthStringData(2).isAPartOf(rdirKey, 11);
  	public FixedLengthStringData rdirRider = new FixedLengthStringData(2).isAPartOf(rdirKey, 13);
  	public PackedDecimalData rdirPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(rdirKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(rdirKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(rdirFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rdirFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}