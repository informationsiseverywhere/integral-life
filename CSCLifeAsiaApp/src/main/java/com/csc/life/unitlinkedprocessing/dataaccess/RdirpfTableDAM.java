package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RdirpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:15
 * Class transformed from RDIRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RdirpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 260;
	public FixedLengthStringData rdirrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData rdirpfRecord = rdirrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(rdirrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(rdirrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(rdirrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(rdirrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(rdirrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(rdirrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund01 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund02 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund03 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund04 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund05 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund06 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund07 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund08 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund09 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public FixedLengthStringData unitAllocFund10 = DD.ualfnd.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt01 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt02 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt03 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt04 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt05 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt06 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt07 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt08 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt09 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitAllocPercAmt10 = DD.ualprc.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice01 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice02 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice03 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice04 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice05 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice06 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice07 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice08 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice09 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public PackedDecimalData unitSpecPrice10 = DD.uspcpr.copy().isAPartOf(rdirrec);
	public FixedLengthStringData percOrAmntInd = DD.prcamtind.copy().isAPartOf(rdirrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(rdirrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(rdirrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(rdirrec);
	public FixedLengthStringData premTopupInd = DD.ptopup.copy().isAPartOf(rdirrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(rdirrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(rdirrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(rdirrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RdirpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RdirpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RdirpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RdirpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RdirpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RdirpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RdirpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("RDIRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"UALFND01, " +
							"UALFND02, " +
							"UALFND03, " +
							"UALFND04, " +
							"UALFND05, " +
							"UALFND06, " +
							"UALFND07, " +
							"UALFND08, " +
							"UALFND09, " +
							"UALFND10, " +
							"UALPRC01, " +
							"UALPRC02, " +
							"UALPRC03, " +
							"UALPRC04, " +
							"UALPRC05, " +
							"UALPRC06, " +
							"UALPRC07, " +
							"UALPRC08, " +
							"UALPRC09, " +
							"UALPRC10, " +
							"USPCPR01, " +
							"USPCPR02, " +
							"USPCPR03, " +
							"USPCPR04, " +
							"USPCPR05, " +
							"USPCPR06, " +
							"USPCPR07, " +
							"USPCPR08, " +
							"USPCPR09, " +
							"USPCPR10, " +
							"PRCAMTIND, " +
							"CURRFROM, " +
							"CURRTO, " +
							"VALIDFLAG, " +
							"PTOPUP, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     unitAllocFund01,
                                     unitAllocFund02,
                                     unitAllocFund03,
                                     unitAllocFund04,
                                     unitAllocFund05,
                                     unitAllocFund06,
                                     unitAllocFund07,
                                     unitAllocFund08,
                                     unitAllocFund09,
                                     unitAllocFund10,
                                     unitAllocPercAmt01,
                                     unitAllocPercAmt02,
                                     unitAllocPercAmt03,
                                     unitAllocPercAmt04,
                                     unitAllocPercAmt05,
                                     unitAllocPercAmt06,
                                     unitAllocPercAmt07,
                                     unitAllocPercAmt08,
                                     unitAllocPercAmt09,
                                     unitAllocPercAmt10,
                                     unitSpecPrice01,
                                     unitSpecPrice02,
                                     unitSpecPrice03,
                                     unitSpecPrice04,
                                     unitSpecPrice05,
                                     unitSpecPrice06,
                                     unitSpecPrice07,
                                     unitSpecPrice08,
                                     unitSpecPrice09,
                                     unitSpecPrice10,
                                     percOrAmntInd,
                                     currfrom,
                                     currto,
                                     validflag,
                                     premTopupInd,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		unitAllocFund01.clear();
  		unitAllocFund02.clear();
  		unitAllocFund03.clear();
  		unitAllocFund04.clear();
  		unitAllocFund05.clear();
  		unitAllocFund06.clear();
  		unitAllocFund07.clear();
  		unitAllocFund08.clear();
  		unitAllocFund09.clear();
  		unitAllocFund10.clear();
  		unitAllocPercAmt01.clear();
  		unitAllocPercAmt02.clear();
  		unitAllocPercAmt03.clear();
  		unitAllocPercAmt04.clear();
  		unitAllocPercAmt05.clear();
  		unitAllocPercAmt06.clear();
  		unitAllocPercAmt07.clear();
  		unitAllocPercAmt08.clear();
  		unitAllocPercAmt09.clear();
  		unitAllocPercAmt10.clear();
  		unitSpecPrice01.clear();
  		unitSpecPrice02.clear();
  		unitSpecPrice03.clear();
  		unitSpecPrice04.clear();
  		unitSpecPrice05.clear();
  		unitSpecPrice06.clear();
  		unitSpecPrice07.clear();
  		unitSpecPrice08.clear();
  		unitSpecPrice09.clear();
  		unitSpecPrice10.clear();
  		percOrAmntInd.clear();
  		currfrom.clear();
  		currto.clear();
  		validflag.clear();
  		premTopupInd.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRdirrec() {
  		return rdirrec;
	}

	public FixedLengthStringData getRdirpfRecord() {
  		return rdirpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRdirrec(what);
	}

	public void setRdirrec(Object what) {
  		this.rdirrec.set(what);
	}

	public void setRdirpfRecord(Object what) {
  		this.rdirpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(rdirrec.getLength());
		result.set(rdirrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}