package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:20
 * Description:
 * Copybook name: UDERKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Uderkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData uderFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData uderKey = new FixedLengthStringData(64).isAPartOf(uderFileKey, 0, REDEFINE);
  	public FixedLengthStringData uderRecordtype = new FixedLengthStringData(4).isAPartOf(uderKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(uderKey, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(uderFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		uderFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}