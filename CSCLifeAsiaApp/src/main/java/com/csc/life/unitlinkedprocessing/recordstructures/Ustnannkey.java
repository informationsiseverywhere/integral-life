package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:45
 * Description:
 * Copybook name: USTNANNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustnannkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustnannFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustnannKey = new FixedLengthStringData(64).isAPartOf(ustnannFileKey, 0, REDEFINE);
  	public FixedLengthStringData ustnannChdrcoy = new FixedLengthStringData(1).isAPartOf(ustnannKey, 0);
  	public FixedLengthStringData ustnannChdrnum = new FixedLengthStringData(8).isAPartOf(ustnannKey, 1);
  	public PackedDecimalData ustnannPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ustnannKey, 9);
  	public FixedLengthStringData ustnannLife = new FixedLengthStringData(2).isAPartOf(ustnannKey, 12);
  	public FixedLengthStringData ustnannCoverage = new FixedLengthStringData(2).isAPartOf(ustnannKey, 14);
  	public FixedLengthStringData ustnannRider = new FixedLengthStringData(2).isAPartOf(ustnannKey, 16);
  	public FixedLengthStringData ustnannUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(ustnannKey, 18);
  	public PackedDecimalData ustnannTranno = new PackedDecimalData(5, 0).isAPartOf(ustnannKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(ustnannKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustnannFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustnannFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}