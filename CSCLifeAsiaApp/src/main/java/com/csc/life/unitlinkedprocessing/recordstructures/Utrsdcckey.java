package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:52
 * Description:
 * Copybook name: UTRSDCCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsdcckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsdccFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData utrsdccKey = new FixedLengthStringData(256).isAPartOf(utrsdccFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsdccChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsdccKey, 0);
  	public FixedLengthStringData utrsdccChdrnum = new FixedLengthStringData(8).isAPartOf(utrsdccKey, 1);
  	public FixedLengthStringData utrsdccLife = new FixedLengthStringData(2).isAPartOf(utrsdccKey, 9);
  	public FixedLengthStringData utrsdccCoverage = new FixedLengthStringData(2).isAPartOf(utrsdccKey, 11);
  	public FixedLengthStringData utrsdccRider = new FixedLengthStringData(2).isAPartOf(utrsdccKey, 13);
  	public FixedLengthStringData utrsdccUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrsdccKey, 15);
  	public FixedLengthStringData utrsdccUnitType = new FixedLengthStringData(1).isAPartOf(utrsdccKey, 19);
  	public PackedDecimalData utrsdccPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsdccKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(utrsdccKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsdccFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsdccFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}