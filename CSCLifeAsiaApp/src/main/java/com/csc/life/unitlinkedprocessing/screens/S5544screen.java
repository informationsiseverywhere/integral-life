package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5544screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5544ScreenVars sv = (S5544ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5544screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5544ScreenVars screenVars = (S5544ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.btobid.setClassString("");
		screenVars.btooff.setClassString("");
		screenVars.btodisc.setClassString("");
		screenVars.otooff.setClassString("");
		screenVars.nowDeferInd.setClassString("");
		screenVars.noOfFreeSwitches.setClassString("");
		screenVars.ppyears.setClassString("");
		screenVars.cfrwd.setClassString("");
		screenVars.ffamt.setClassString("");
		screenVars.feepc.setClassString("");
		screenVars.feemin.setClassString("");
		screenVars.feemax.setClassString("");
		screenVars.bidToFund.setClassString("");
		screenVars.discountOfferPercent.setClassString("");
	}

/**
 * Clear all the variables in S5544screen
 */
	public static void clear(VarModel pv) {
		S5544ScreenVars screenVars = (S5544ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.btobid.clear();
		screenVars.btooff.clear();
		screenVars.btodisc.clear();
		screenVars.otooff.clear();
		screenVars.nowDeferInd.clear();
		screenVars.noOfFreeSwitches.clear();
		screenVars.ppyears.clear();
		screenVars.cfrwd.clear();
		screenVars.ffamt.clear();
		screenVars.feepc.clear();
		screenVars.feemin.clear();
		screenVars.feemax.clear();
		screenVars.bidToFund.clear();
		screenVars.discountOfferPercent.clear();
	}
}
