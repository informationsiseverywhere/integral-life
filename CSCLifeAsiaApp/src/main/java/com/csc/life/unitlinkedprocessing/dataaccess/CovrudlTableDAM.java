package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.life.newbusiness.dataaccess.CovrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovrudlTableDAM.java
 * Date: Sun, 30 Aug 2009 03:35:45
 * Class transformed from COVRUDL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovrudlTableDAM extends CovrpfTableDAM {

	public CovrudlTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("COVRUDL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "VALIDFLAG, " +
		            "CRTABLE, " +
		            "CRDEBT, " +
		            "BBLDAT, " +
		            "STATCODE, " +
		            "PSTATCODE, " +
		            "CRRCD, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               currfrom,
                               currto,
                               validflag,
                               crtable,
                               coverageDebt,
                               benBillDate,
                               statcode,
                               pstatcode,
                               crrcd,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(102);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getValidflag().toInternal()
					+ getCrtable().toInternal()
					+ getCoverageDebt().toInternal()
					+ getBenBillDate().toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ getCrrcd().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, coverageDebt);
			what = ExternalData.chop(what, benBillDate);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, crrcd);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getCoverageDebt() {
		return coverageDebt;
	}
	public void setCoverageDebt(Object what) {
		setCoverageDebt(what, false);
	}
	public void setCoverageDebt(Object what, boolean rounded) {
		if (rounded)
			coverageDebt.setRounded(what);
		else
			coverageDebt.set(what);
	}	
	public PackedDecimalData getBenBillDate() {
		return benBillDate;
	}
	public void setBenBillDate(Object what) {
		setBenBillDate(what, false);
	}
	public void setBenBillDate(Object what, boolean rounded) {
		if (rounded)
			benBillDate.setRounded(what);
		else
			benBillDate.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Object what) {
		setCrrcd(what, false);
	}
	public void setCrrcd(Object what, boolean rounded) {
		if (rounded)
			crrcd.setRounded(what);
		else
			crrcd.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		currfrom.clear();
		currto.clear();
		validflag.clear();
		crtable.clear();
		coverageDebt.clear();
		benBillDate.clear();
		statcode.clear();
		pstatcode.clear();
		crrcd.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}