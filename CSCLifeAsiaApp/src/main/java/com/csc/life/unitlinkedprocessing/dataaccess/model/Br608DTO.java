package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Br608DTO {

	private String company;// 1
	private String vrtfund;// 4
	private String unityp;// 1
	private BigDecimal nofunts;// 

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getVrtfund() {
		return vrtfund;
	}

	public void setVrtfund(String vrtfund) {
		this.vrtfund = vrtfund;
	}

	public String getUnityp() {
		return unityp;
	}

	public void setUnityp(String unityp) {
		this.unityp = unityp;
	}

	public BigDecimal getNofunts() {
		return nofunts;
	}

	public void setNofunts(BigDecimal nofunts) {
		this.nofunts = nofunts;
	}

}
