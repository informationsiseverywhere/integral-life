/*
 * File: P5542.java
 * Date: 30 August 2009 0:30:30
 * Author: Quipoz Limited
 * 
 * Class transformed from P5542.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;

import com.csc.life.unitlinkedprocessing.procedures.T5542pt;
import com.csc.life.unitlinkedprocessing.screens.S5542ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;//ILIFE-7956

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* DATED DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-3.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FIELD
* MOVES TO AVOID CONVERSION OF COMP-3 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
*
*****************************************************************
* </pre>
*/
public class P5542 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5542");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5542rec t5542rec = new T5542rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5542ScreenVars sv = getLScreenVars();
	boolean maxwithConfig = false;//ILIFE-7956

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		other3080, 
		exit3090
	}

	public P5542() {
		super();
		screenVars = sv;
		new ScreenModel("S5542", AppVars.getInstance(), sv);
	}

	protected S5542ScreenVars getLScreenVars(){
		return ScreenProgram.getScreenVars( S5542ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	**********************************************************<D96NUM>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN            <D96NUM>
	**********************************************************<D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

	/**
	* <pre>
	*************************                                 <D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void initialise1010()
	{
		/**                                                         <D96NUM>*/
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/**                                                         <D96NUM>*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		/*                                                         <D96NUM>*/
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
		/**                                                         <D96NUM>*/
		/**                                                         <D96NUM>*/
		//ILIFE-7956 -START
		maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
		if(!maxwithConfig) {
			sv.maxwith01Out[varcom.nd.toInt()].set("Y");
			sv.maxwith02Out[varcom.nd.toInt()].set("Y");
			sv.maxwith03Out[varcom.nd.toInt()].set("Y");
			sv.maxwith04Out[varcom.nd.toInt()].set("Y");
			sv.maxwith05Out[varcom.nd.toInt()].set("Y");
			sv.maxwith06Out[varcom.nd.toInt()].set("Y");
		}
		if(maxwithConfig){
			t5542rec.maxwith01.set(BigDecimal.ZERO); 
			t5542rec.maxwith02.set(BigDecimal.ZERO);    
			t5542rec.maxwith03.set(BigDecimal.ZERO);
			t5542rec.maxwith04.set(BigDecimal.ZERO);
			t5542rec.maxwith05.set(BigDecimal.ZERO);
			t5542rec.maxwith06.set(BigDecimal.ZERO);
		}
		//ILIFE-7956 -END
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		/*                                                         <D96NUM>*/
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		/*                                                         <D96NUM>*/
		sv.longdesc.set(descIO.getLongdesc());
		/*                                                         <D96NUM>*/
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		/*                                                         <D96NUM>*/
		t5542rec.t5542Rec.set(itmdIO.getItemGenarea());
		/*                                                         <D96NUM>*/
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERI<D96NUM>*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS. <D96NUM>*/
		/*                                                         <D96NUM>*/
		/*                                                         <D96NUM>*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			return ;
		}
		/*                                                         <D96NUM>*/
		t5542rec.feemax01.set(ZERO);
		t5542rec.feemax02.set(ZERO);
		t5542rec.feemax03.set(ZERO);
		t5542rec.feemax04.set(ZERO);
		t5542rec.feemax05.set(ZERO);
		t5542rec.feemax06.set(ZERO);
		t5542rec.feemin01.set(ZERO);
		t5542rec.feemin02.set(ZERO);
		t5542rec.feemin03.set(ZERO);
		t5542rec.feemin04.set(ZERO);
		t5542rec.feemin05.set(ZERO);
		t5542rec.feemin06.set(ZERO);
		t5542rec.feepc01.set(ZERO);
		t5542rec.feepc02.set(ZERO);
		t5542rec.feepc03.set(ZERO);
		t5542rec.feepc04.set(ZERO);
		t5542rec.feepc05.set(ZERO);
		t5542rec.feepc06.set(ZERO);
		t5542rec.ffamt01.set(ZERO);
		t5542rec.ffamt02.set(ZERO);
		t5542rec.ffamt03.set(ZERO);
		t5542rec.ffamt04.set(ZERO);
		t5542rec.ffamt05.set(ZERO);
		t5542rec.ffamt06.set(ZERO);
		t5542rec.wdlAmount01.set(ZERO);
		t5542rec.wdlAmount02.set(ZERO);
		t5542rec.wdlAmount03.set(ZERO);
		t5542rec.wdlAmount04.set(ZERO);
		t5542rec.wdlAmount05.set(ZERO);
		t5542rec.wdlAmount06.set(ZERO);
		t5542rec.wdlFreq01.set(ZERO);
		t5542rec.wdlFreq02.set(ZERO);
		t5542rec.wdlFreq03.set(ZERO);
		t5542rec.wdlFreq04.set(ZERO);
		t5542rec.wdlFreq05.set(ZERO);
		t5542rec.wdlFreq06.set(ZERO);
		t5542rec.wdrem01.set(ZERO);
		t5542rec.wdrem02.set(ZERO);
		t5542rec.wdrem03.set(ZERO);
		t5542rec.wdrem04.set(ZERO);
		t5542rec.wdrem05.set(ZERO);
		t5542rec.wdrem06.set(ZERO);
		t5542rec.maxwith01.set(ZERO);
		t5542rec.maxwith02.set(ZERO);
		t5542rec.maxwith03.set(ZERO);
		t5542rec.maxwith04.set(ZERO);
		t5542rec.maxwith05.set(ZERO);
		t5542rec.maxwith06.set(ZERO);
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void generalArea1045()
	{
		/*                                                         <D96NUM>*/
		sv.billfreqs.set(t5542rec.billfreqs);
		/*    MOVE T5542-FEEMAXS                                   <D96NUM>*/
		/*      TO S5542-FEEMAXS                 .                 <D96NUM>*/
		/*    MOVE T5542-FEEMINS                                   <D96NUM>*/
		/*      TO S5542-FEEMINS                 .                 <D96NUM>*/
		/*    MOVE T5542-FEEPCS                                    <D96NUM>*/
		/*      TO S5542-FEEPCS                  .                 <D96NUM>*/
		/*    MOVE T5542-FFAMTS                                    <D96NUM>*/
		/*      TO S5542-FFAMTS                  .                 <D96NUM>*/
		/*                                                         <D96NUM>*/
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
			moveFields1600();
		}
		/*                                                         <D96NUM>*/
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		/*CONFIRMATION-FIELDS*/
		/**                                                         <D96NUM>*/
		/*OTHER*/
		/**                                                         <D96NUM>*/
		/*EXIT*/
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void moveFields1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.feemax[wsaaSub1.toInt()].set(t5542rec.feemax[wsaaSub1.toInt()]);
		sv.feemin[wsaaSub1.toInt()].set(t5542rec.feemin[wsaaSub1.toInt()]);
		sv.feepc[wsaaSub1.toInt()].set(t5542rec.feepc[wsaaSub1.toInt()]);
		sv.ffamt[wsaaSub1.toInt()].set(t5542rec.ffamt[wsaaSub1.toInt()]);
		sv.wdlAmount[wsaaSub1.toInt()].set(t5542rec.wdlAmount[wsaaSub1.toInt()]);
		sv.wdlFreq[wsaaSub1.toInt()].set(t5542rec.wdlFreq[wsaaSub1.toInt()]);
		sv.wdrem[wsaaSub1.toInt()].set(t5542rec.wdrem[wsaaSub1.toInt()]);
		//ILIFE-7956 -START
		if(maxwithConfig)
		{
			if(t5542rec.maxwiths == null  || isEQ(t5542rec.maxwiths, SPACES))
			{
				t5542rec.maxwith01.set(BigDecimal.ZERO); 
				t5542rec.maxwith02.set(BigDecimal.ZERO);    
				t5542rec.maxwith03.set(BigDecimal.ZERO);
				t5542rec.maxwith04.set(BigDecimal.ZERO);
				t5542rec.maxwith05.set(BigDecimal.ZERO);
				t5542rec.maxwith06.set(BigDecimal.ZERO);
			
				sv.maxwith[wsaaSub1.toInt()].set(t5542rec.maxwith[wsaaSub1.toInt()]);
			}
			else
			{
				sv.maxwith[wsaaSub1.toInt()].set(t5542rec.maxwith[wsaaSub1.toInt()]);
			}
		}
		//ILIFE-7956 -END
		/*EXIT*/
	}

	/**
	* <pre>
	**********************************************************<D96NUM>
	*     RETRIEVE SCREEN FIELDS AND EDIT                     <D96NUM>
	**********************************************************<D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*                                                         <D96NUM>*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		/*                                                         <D96NUM>*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

	/**
	* <pre>
	**************************                                <D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S5542IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5542-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*                                                         <D96NUM>*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
		/**                                                         <D96NUM>*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	**********************************************************<D96NUM>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION     <D96NUM>
	**********************************************************<D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	**********************                                    <D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void preparation3010()
	{
		/*                                                         <D96NUM>*/
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		/*                                                         <D96NUM>*/
		wsaaUpdateFlag = "N";
		/*                                                         <D96NUM>*/
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		/*                                                         <D96NUM>*/
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		/*                                                         <D96NUM>*/
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*                                                         <D96NUM>*/
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void updateRecord3055()
	{
		/*                                                         <D96NUM>*/
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5542rec.t5542Rec);
		itmdIO.setFunction(varcom.rewrt);
		/*                                                         <D96NUM>*/
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void checkChanges3100()
	{
		check3100();
	}

	/**
	* <pre>
	*--------------------------*                              <D96NUM>
	* </pre>
	*/
protected void check3100()
	{
		/*                                                         <D96NUM>*/
		if (isNE(sv.billfreqs, t5542rec.billfreqs)) {
			t5542rec.billfreqs.set(sv.billfreqs);
			wsaaUpdateFlag = "Y";
		}
		/*    IF S5542-FEEMAXS                 NOT =               <D96NUM>*/
		/*       T5542-FEEMAXS                                     <D96NUM>*/
		/*        MOVE S5542-FEEMAXS                               <D96NUM>*/
		/*          TO T5542-FEEMAXS                               <D96NUM>*/
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                    <D96NUM>*/
		/*    IF S5542-FEEMINS                 NOT =               <D96NUM>*/
		/*       T5542-FEEMINS                                     <D96NUM>*/
		/*        MOVE S5542-FEEMINS                               <D96NUM>*/
		/*          TO T5542-FEEMINS                               <D96NUM>*/
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                    <D96NUM>*/
		/*    IF S5542-FEEPCS                  NOT =               <D96NUM>*/
		/*       T5542-FEEPCS                                      <D96NUM>*/
		/*        MOVE S5542-FEEPCS                                <D96NUM>*/
		/*          TO T5542-FEEPCS                                <D96NUM>*/
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                    <D96NUM>*/
		/*    IF S5542-FFAMTS                  NOT =               <D96NUM>*/
		/*       T5542-FFAMTS                                      <D96NUM>*/
		/*        MOVE S5542-FFAMTS                                <D96NUM>*/
		/*          TO T5542-FFAMTS                                <D96NUM>*/
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                    <D96NUM>*/
		/*                                                         <D96NUM>*/
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		/*                                                         <D96NUM>*/
		/*   IF S5542-WDL-AMOUNTS             NOT =               <D96NUM>*/
		/*      T5542-WDL-AMOUNTS                                 <D96NUM>*/
		/*       MOVE S5542-WDL-AMOUNTS                           <D96NUM>*/
		/*         TO T5542-WDL-AMOUNTS                           <D96NUM>*/
		/*       MOVE 'Y' TO WSAA-UPDATE-FLAG.                    <D96NUM>*/
		/*   IF S5542-WDL-FREQS               NOT =               <D96NUM>*/
		/*      T5542-WDL-FREQS                                   <D96NUM>*/
		/*       MOVE S5542-WDL-FREQS                             <D96NUM>*/
		/*         TO T5542-WDL-FREQS                             <D96NUM>*/
		/*       MOVE 'Y' TO WSAA-UPDATE-FLAG.                    <D96NUM>*/
		/*   IF S5542-WDREMS                  NOT =               <D96NUM>*/
		/*      T5542-WDREMS                                      <D96NUM>*/
		/*       MOVE S5542-WDREMS                                <D96NUM>*/
		/*         TO T5542-WDREMS                                <D96NUM>*/
		/*       MOVE 'Y' TO WSAA-UPDATE-FLAG.                    <D96NUM>*/
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 6); loopVar2 += 1){
			updateFields3500();
		}
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void updateFields3500()
	{
		para3500();
	}

	/**
	* <pre>
	********************************                          <D96NUM>
	* </pre>
	*/
protected void para3500()
	{
		wsaaSub1.add(1);
		/*                                                         <D96NUM>*/
		if (isNE(sv.feemax[wsaaSub1.toInt()], t5542rec.feemax[wsaaSub1.toInt()])) {
			t5542rec.feemax[wsaaSub1.toInt()].set(sv.feemax[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*                                                         <D96NUM>*/
		if (isNE(sv.feemin[wsaaSub1.toInt()], t5542rec.feemin[wsaaSub1.toInt()])) {
			t5542rec.feemin[wsaaSub1.toInt()].set(sv.feemin[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*                                                         <D96NUM>*/
		if (isNE(sv.feepc[wsaaSub1.toInt()], t5542rec.feepc[wsaaSub1.toInt()])) {
			t5542rec.feepc[wsaaSub1.toInt()].set(sv.feepc[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*                                                         <D96NUM>*/
		if (isNE(sv.ffamt[wsaaSub1.toInt()], t5542rec.ffamt[wsaaSub1.toInt()])) {
			t5542rec.ffamt[wsaaSub1.toInt()].set(sv.ffamt[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*                                                         <D96NUM>*/
		if (isNE(sv.wdlAmount[wsaaSub1.toInt()], t5542rec.wdlAmount[wsaaSub1.toInt()])) {
			t5542rec.wdlAmount[wsaaSub1.toInt()].set(sv.wdlAmount[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*                                                         <D96NUM>*/
		if (isNE(sv.wdlFreq[wsaaSub1.toInt()], t5542rec.wdlFreq[wsaaSub1.toInt()])) {
			t5542rec.wdlFreq[wsaaSub1.toInt()].set(sv.wdlFreq[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*                                                         <D96NUM>*/
		if (isNE(sv.wdrem[wsaaSub1.toInt()], t5542rec.wdrem[wsaaSub1.toInt()])) {
			t5542rec.wdrem[wsaaSub1.toInt()].set(sv.wdrem[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		//ILIFE-7956 -START
		if(maxwithConfig)
		{
			if (isNE(sv.maxwith[wsaaSub1.toInt()], t5542rec.maxwith[wsaaSub1.toInt()])) {
				t5542rec.maxwith[wsaaSub1.toInt()].set(sv.maxwith[wsaaSub1.toInt()]);
				wsaaUpdateFlag = "Y";
			}
		}
		//ILIFE-7956 -END
			
	}   

	/**
	* <pre>
	**********************************************************<D96NUM>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT            <D96NUM>
	**********************************************************<D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	**********************************************************<D96NUM>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT<D96NUM>
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE. <D96NUM>
	**********************************************************<D96NUM>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		/*                                                         <D96NUM>*/
		callProgram(T5542pt.class, wsaaTablistrec);
		/*EXIT*/
		/**                                                         <D96NUM>*/
	}
}
