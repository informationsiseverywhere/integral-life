package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5146
 * @version 1.0 generated on 30/08/09 06:36
 * @author Quipoz
 */
public class Sd5frScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(660); //571
	public FixedLengthStringData dataFields = new FixedLengthStringData(260).isAPartOf(dataArea, 0); //235
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,60);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,145);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,202);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,210);
	public FixedLengthStringData overrideFee = DD.orswchfe.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData percentAmountInd = DD.pcamtind.copy().isAPartOf(dataFields,215);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,216);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,230);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,233);
	public FixedLengthStringData fund = DD.vfund.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData fndcurr = DD.fndcur.copy().isAPartOf(dataFields,239);
	public FixedLengthStringData fndtype = DD.fndtyp.copy().isAPartOf(dataFields,242);
	public ZonedDecimalData pcnt = DD.pcntamt.copyToZonedDecimal().isAPartOf(dataFields,243);

	
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(100).isAPartOf(dataArea, 260);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData orswchfeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData pcamtindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData fundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData fndcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData fndtypeErr =new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData pcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(300).isAPartOf(dataArea, 360);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] orswchfeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] pcamtindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] fundout = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] fundcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] fndtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] pcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	

	public FixedLengthStringData subfileArea = new FixedLengthStringData(151);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(37).isAPartOf(subfileArea, 0);
	public FixedLengthStringData currcy = DD.currcy.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData fndcur = DD.fndcur.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData fndtyp = DD.fndtyp.copy().isAPartOf(subfileFields,6);
	public ZonedDecimalData pcntamt = DD.pcntamt.copyToZonedDecimal().isAPartOf(subfileFields,7);
	public ZonedDecimalData percentage = DD.tgtpcnt.copyToZonedDecimal().isAPartOf(subfileFields,24);
	public FixedLengthStringData vfund = DD.vfund.copy().isAPartOf(subfileFields,29);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,33);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 37);
	public FixedLengthStringData currcyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData fndcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData fndtypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData pcntamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData tgtpcntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData vfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 65);
	public FixedLengthStringData[] currcyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] fndcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] fndtypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] pcntamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] tgtpcntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] vfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 149);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sd5frscreensflWritten = new LongData(0);
	public LongData Sd5frscreenctlWritten = new LongData(0);
	public LongData Sd5frscreenWritten = new LongData(0);
	public LongData Sd5frprotectWritten = new LongData(0);
	public GeneralTable Sd5frscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return Sd5frscreensfl;
	}

	public Sd5frScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vfundOut,new String[] {"01","02","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tgtpcntOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"15","16","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(orswchfeOut,new String[] {"20","22","-20",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {unitVirtualFund, currcy, pcntamt, fndtyp, vfund, fndcur, percentage};
		screenSflOutFields = new BaseData[][] {vrtfndOut, currcyOut, pcntamtOut, fndtypOut, vfundOut, fndcurOut, tgtpcntOut};
		screenSflErrFields = new BaseData[] {vrtfndErr, currcyErr, pcntamtErr, fndtypErr, vfundErr, fndcurErr, tgtpcntErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, percentAmountInd, planSuffix, numpols, life, coverage, rider, crtable, crtabdesc, effdate, overrideFee,fund,fndcurr,fndtype,pcnt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, pcamtindOut, plnsfxOut, numpolsOut, lifeOut, coverageOut, riderOut, crtableOut, crtabdescOut, effdateOut, orswchfeOut,fundout,fundcurrOut,fndtypeOut,pcntOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, pcamtindErr, plnsfxErr, numpolsErr, lifeErr, coverageErr, riderErr, crtableErr, crtabdescErr, effdateErr, orswchfeErr,fundErr,fndcurrErr,fndtypeErr,pcntErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5frscreen.class;
		screenSflRecord = Sd5frscreensfl.class;
		screenCtlRecord =Sd5frscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5frprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5frscreenctl.lrec.pageSubfile);
	}
}
