/*
 * File: P5429.java
 * Date: 30 August 2009 0:25:10
 * Author: Quipoz Limited
 * 
 * Class transformed from P5429.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.dataaccess.VprcupdTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S5429ScreenVars;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
*****************************************************************
* </pre>
*/
public class P5429 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5429");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSubfileSize = new ZonedDecimalData(2, 0).init(15).setUnsigned();
	private ZonedDecimalData wsaaPage = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-PREV-KEY */
	private FixedLengthStringData wsaaPrevType = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrevVirtualFund = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaTdayIntDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaTdayExtDate = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Validator wsaaRolu = new Validator(wsaaFunctionKey, "ROLU");
		/* FORMATS */
	private String vprcupdrec = "VPRCUPDREC";
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Unit Price file*/
	private VprcupdTableDAM vprcupdIO = new VprcupdTableDAM();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S5429ScreenVars sv = ScreenProgram.getScreenVars( S5429ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		writeToSubfile1250, 
		nextRecord1260, 
		setMoreSign1280, 
		preExit, 
		writeToSubfile2700, 
		nextRecord2800, 
		setMoreSign2700, 
		exit2090, 
		exit5190, 
		nextRecord6500
	}

	public P5429() {
		super();
		screenVars = sv;
		new ScreenModel("S5429", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1100();
					loadSubfile1200();
				}
				case writeToSubfile1250: {
					writeToSubfile1250();
				}
				case nextRecord1260: {
					nextRecord1260();
				}
				case setMoreSign1280: {
					setMoreSign1280();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.initBarePrice.set(ZERO);
		sv.initBidPrice.set(ZERO);
		sv.initOfferPrice.set(ZERO);
		sv.accBarePrice.set(ZERO);
		sv.accBidPrice.set(ZERO);
		sv.accOfferPrice.set(ZERO);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.company.set(wsspcomn.company);
		sv.effdate.set(wsspuprc.uprcEffdate);
		sv.jobno.set(wsspuprc.uprcJobno);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5429", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void loadSubfile1200()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(wsspuprc.uprcVrtfnd);
		
		//performance improvement -- < Niharika Modi >
		//vprcupdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//vprcupdIO.setFitKeysSearch("EFFDATE","JOBNO","VRTFND");
		
		getNextVprcupdRec5100();
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1280);
		}
		if (isNE(vprcupdIO.getProcflag(),"Y")
		|| isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| (isNE(vprcupdIO.getUnitVirtualFund(),wsspuprc.uprcVrtfnd)
		&& isNE(wsspuprc.uprcVrtfnd,SPACES))) {
			goTo(GotoLabel.nextRecord1260);
		}
	}

protected void writeToSubfile1250()
	{
		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5429", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void nextRecord1260()
	{
		vprcupdIO.setFunction(varcom.nextr);
		getNextVprcupdRec5100();
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1280);
		}
		if (isNE(vprcupdIO.getProcflag(),"Y")
		|| isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| (isNE(vprcupdIO.getUnitVirtualFund(),wsspuprc.uprcVrtfnd)
		&& isNE(wsspuprc.uprcVrtfnd,SPACES))) {
			goTo(GotoLabel.nextRecord1260);
		}
		if (isEQ(scrnparams.subfileRrn,wsaaSubfileSize)) {
			goTo(GotoLabel.setMoreSign1280);
		}
		goTo(GotoLabel.writeToSubfile1250);
	}

protected void setMoreSign1280()
	{
		if (isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case writeToSubfile2700: {
					writeToSubfile2700();
				}
				case nextRecord2800: {
					nextRecord2800();
				}
				case setMoreSign2700: {
					setMoreSign2700();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaFunctionKey.set(scrnparams.statuz);
		/*ROLL-UP*/
		if (!wsaaRolu.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void writeToSubfile2700()
	{
		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		callScreenIo5000();
	}

protected void nextRecord2800()
	{
		getNextVprcupdRec5100();
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign2700);
		}
		if (isNE(vprcupdIO.getProcflag(),"Y")
		|| isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| (isNE(vprcupdIO.getUnitVirtualFund(),wsspuprc.uprcVrtfnd)
		&& isNE(wsspuprc.uprcVrtfnd,SPACES))) {
			goTo(GotoLabel.nextRecord2800);
		}
		compute(wsaaPage, 0).setDivide(scrnparams.subfileRrn, (wsaaSubfileSize));
		wsaaRem.setRemainder(wsaaPage);
		if (isEQ(wsaaRem,ZERO)) {
			goTo(GotoLabel.setMoreSign2700);
		}
		goTo(GotoLabel.writeToSubfile2700);
	}

protected void setMoreSign2700()
	{
		if (isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		wsspcomn.edterror.set("Y");
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callScreenIo5000()
	{
		/*PARA*/
		processScreen("S5429", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void getNextVprcupdRec5100()
	{
		try {
			callVprcupd5120();
		}
		catch (GOTOException e){
		}
	}

protected void callVprcupd5120()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit5190);
		}
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
	}

protected void moveFieldsToSubfile6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para6000();
				}
				case nextRecord6500: {
					nextRecord6500();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6000()
	{
		if (isNE(vprcupdIO.getUnitType(),"A")
		&& isNE(vprcupdIO.getUnitType(),"I")) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		wsaaPrevVirtualFund.set(vprcupdIO.getUnitVirtualFund());
		sv.unitVirtualFund.set(vprcupdIO.getUnitVirtualFund());
		sv.validflag.set(vprcupdIO.getValidflag());
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(vprcupdIO.getUpdateDate());
		datcon1rec.function.set("CONU");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.update.set(datcon1rec.extDate);
		if (isEQ(vprcupdIO.getUnitType(),"I")) {
			sv.initBarePrice.set(vprcupdIO.getUnitBarePrice());
			sv.initBidPrice.set(vprcupdIO.getUnitBidPrice());
			sv.initOfferPrice.set(vprcupdIO.getUnitOfferPrice());
		}
		else {
			if (isEQ(vprcupdIO.getUnitType(),"A")) {
				sv.accBarePrice.set(vprcupdIO.getUnitBarePrice());
				sv.accBidPrice.set(vprcupdIO.getUnitBidPrice());
				sv.accOfferPrice.set(vprcupdIO.getUnitOfferPrice());
			}
		}
		wsaaPrevType.set(vprcupdIO.getUnitType());
	}

protected void nextRecord6500()
	{
		vprcupdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getProcflag(),"Y")
		|| isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| (isNE(vprcupdIO.getUnitVirtualFund(),wsspuprc.uprcVrtfnd)
		&& isNE(wsspuprc.uprcVrtfnd,SPACES))) {
			goTo(GotoLabel.nextRecord6500);
		}
		if (isEQ(vprcupdIO.getUnitType(),wsaaPrevType)
		|| isNE(vprcupdIO.getUnitVirtualFund(),wsaaPrevVirtualFund)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getUnitType(),"A")
		&& isNE(vprcupdIO.getUnitType(),"I")) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupdIO.getUnitType(),"I")) {
			sv.initBarePrice.set(vprcupdIO.getUnitBarePrice());
			sv.initBidPrice.set(vprcupdIO.getUnitBidPrice());
			sv.initOfferPrice.set(vprcupdIO.getUnitOfferPrice());
		}
		else {
			if (isEQ(vprcupdIO.getUnitType(),"A")) {
				sv.accBarePrice.set(vprcupdIO.getUnitBarePrice());
				sv.accBidPrice.set(vprcupdIO.getUnitBidPrice());
				sv.accOfferPrice.set(vprcupdIO.getUnitOfferPrice());
			}
		}
	}
}
