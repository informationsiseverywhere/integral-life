/******************************************************************************
 * File Name 		: Zrpwpf.java
 * Author			: CSC
 * Creation Date	: 08 May 2017
 * Project			: Integral Life
 * Description		: The Model Class for ZRPWPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			| *											| *				  |
 * ***************************************************************************/
package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class Zrpwpf which acts as Model Class for the table ZRPWPF.
 */
public class Zrpwpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "ZRPWPF";
	
	
	public Zrpwpf() {
		// Default constructor
	}

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int tranno;
	private String life;
	private String coverage;
	private String rider;
	private BigDecimal plnsfx;
	private String usrprf;
	private String jobnm;
	private String datime;

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public int getTranno(){
		return this.tranno;
	}
	public String getLife(){
		return this.life;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public BigDecimal getPlnsfx(){
		return this.plnsfx;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public String getDatime(){
		return this.datime;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setTranno( int tranno ){
		 this.tranno = tranno;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( BigDecimal plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime(String datime ){
		 this.datime = datime;
	}

	@Override
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
