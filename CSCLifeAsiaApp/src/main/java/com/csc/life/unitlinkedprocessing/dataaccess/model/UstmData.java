package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.util.Date;

public class UstmData {
	private String chdrpfx;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private long effdate;
	private long tranno;
	private String batcpfx;
	private String batccoy;
	private String batcbrn;
	private long batcactyr;
	private long batcactmn;
	private String batctrcde;
	private String batcbatch;
	private long strpdate;
	private long ustmno;
	private long jobnoStmt;
	private String validflag;
	private String cntcurr;
	private String clntpfx;
	private String clntcoy;
	private String clntnum;
	private String stmtType;
	private String unitStmtFlag;
	private String userProfile;
	private String jobName;
	private Date datime;

	//From chdrpf
	private String ownerCoy;
	private String ownerNum;
	private String ownerPfx;
	private String descCoy;
	private String descNum;
	private String descPfx;
	private long uniqueNumber;

	//getters and setters
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		if( chdrpfx != null)
			this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		if(chdrcoy != null)
			this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		if(chdrnum != null)
			this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		if(life != null)
			this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		if(jlife != null)
			this.jlife = jlife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		if(coverage != null)
			this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		if(rider != null)
			this.rider = rider;
	}
	public long getEffdate() {
		return effdate;
	}
	public void setEffdate(long effdate) {
		this.effdate = effdate;
	}
	public long getTranno() {
		return tranno;
	}
	public void setTranno(long tranno) {
		this.tranno = tranno;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		if(batcpfx != null)
			this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		if(batccoy != null)
			this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		if(batcbrn != null)
			this.batcbrn = batcbrn;
	}
	public long getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(long batcactyr) {
		this.batcactyr = batcactyr;
	}
	public long getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(long batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		if(batctrcde != null)
			this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		if(batcbatch != null)
			this.batcbatch = batcbatch;
	}
	public long getStrpdate() {
		return strpdate;
	}
	public void setStrpdate(long strpdate) {
		this.strpdate = strpdate;
	}
	public long getUstmno() {
		return ustmno;
	}
	public void setUstmno(long ustmno) {
		this.ustmno = ustmno;
	}
	public long getJobnoStmt() {
		return jobnoStmt;
	}
	public void setJobnoStmt(long jobnoStmt) {
		this.jobnoStmt = jobnoStmt;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		if(validflag != null)
			this.validflag = validflag;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		if(cntcurr != null)
			this.cntcurr = cntcurr;
	}
	public String getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(String clntpfx) {
		if(clntpfx != null)
			this.clntpfx = clntpfx;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		if(clntcoy != null)
			this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		if(clntnum != null)
			this.clntnum = clntnum;
	}
	public String getStmtType() {
		return stmtType;
	}
	public void setStmtType(String stmtType) {
		if(stmtType != null)
			this.stmtType = stmtType;
	}
	public String getUnitStmtFlag() {
		return unitStmtFlag;
	}
	public void setUnitStmtFlag(String unitStmtFlag) {
		if(unitStmtFlag != null)
			this.unitStmtFlag = unitStmtFlag;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		if(userProfile != null)
			this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		if(jobName != null)
			this.jobName = jobName;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		if(datime != null)
			this.datime = datime;
	}
	public String getOwnerCoy() {
		return ownerCoy;
	}
	public void setOwnerCoy(String ownerCoy) {
		this.ownerCoy = ownerCoy;
	}
	public String getOwnerNum() {
		return ownerNum;
	}
	public void setOwnerNum(String ownerNum) {
		this.ownerNum = ownerNum;
	}
	public String getOwnerPfx() {
		return ownerPfx;
	}
	public void setOwnerPfx(String ownerPfx) {
		this.ownerPfx = ownerPfx;
	}
	public String getDescCoy() {
		return descCoy;
	}
	public void setDescCoy(String descCoy) {
		this.descCoy = descCoy;
	}
	public String getDescNum() {
		return descNum;
	}
	public void setDescNum(String descNum) {
		this.descNum = descNum;
	}
	public String getDescPfx() {
		return descPfx;
	}
	public void setDescPfx(String descPfx) {
		this.descPfx = descPfx;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}


}
