package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Uderpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UderpfDAO extends BaseDAO<Uderpf> {
	public void deleteUderpfUTRNRecord();
	public void insertUderpfRecord(List<Uderpf> urList);
	public void deleteUderpfRecordByRC(String recordtype);
}