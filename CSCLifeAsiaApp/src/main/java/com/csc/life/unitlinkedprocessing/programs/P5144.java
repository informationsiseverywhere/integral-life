/*
 * File: P5144.java
 * Date: 30 August 2009 0:13:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P5144.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NEGATIVE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal; //ILIFE-8786
import java.util.ArrayList;	//ILIFE-7392
import java.util.List;
import java.util.Map;
import java.util.Optional; //ILIFE-8786

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
//ILIFE-8786 start
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart.recordstructures.Varcom;
//ILIFE-8786 end
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;

import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcddmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrncfiTableDAM;

import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.interestbearing.dataaccess.HitrcfiTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;

import com.csc.life.productdefinition.dataaccess.model.Covrpf;

import com.csc.life.productdefinition.recordstructures.PremValidaterec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;

import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.terminationclaims.recordstructures.Untallrec;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.life.unitlinkedprocessing.screens.S5144ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5510rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5533rec;	//ILIFE-7392
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;

import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;	//ILIFE-7392
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* This function will be used to apply a single premium top-up to
* a component within a contract. The amount(s) will be applied
* to individual components and so it will be up to the user to
* register the correct dissection amount to each coverage if the
* total is being spread across several components.
*
* The user may have selected to apply the top-up to either
* individual policies within the plan or to the whole plan. After
* this the actual component will be selected. If the top-up is
* being applied to individual policies then this screen will be
* invoked once for each selected component.
*
* If the whole plan has been selected, (COVRMJA-PLAN-SUFFIX =
* 0000), then this screen will again be invoked once for each
* component selected but it must loop around within itself and
* display the screen once for each matching COVR record that
* exists within the plan. It will loop around between the 2000
* and 4000 sections until all the COVR records with a key
* matching on Company, Contract Number, Life, Coverage and Rider
* have been processed. The program will read all the COVR records
* from the summary record, if it exists, to the last COVR record
* in the plan for the given rider key. The plan suffix being
* processed will be displayed in the heading portion of the
* screen in order to show to the user which policy is currently
* being processed. When the summary record is being processed
* display the range of policies represented by the summary, i.e.
*
*       Policy Number: 0001 - 0006
*
* INITIALISE (1000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* Perform a RETRV on the COVRMJA I/O module. This will return
* the first COVRMJA record on the selected policy.
*
* If COVRMJA-PLAN-SUFFIX = 0000  then the whole plan is being
* processed and every COVRMJA record whose key matches on the
* passed Company, Contract Number, Life, Coverage and Rider will
* be processed.
*
* If the COVRMJA-PLAN-SUFFIX is not greater than the value of
* CHDRMJA-POLSUM then a 'notional' policy is being processed,
* i.e. a policy has been selected that is at present part of the
* summarised portion of the contract. If this is the case then
* the COVRMJA summary record will have been saved by the KEEPS
* command and the values from here will be displayed.
*
* However, when the default values from the corresponding ULNK
* record are displayed if they are amounts and not percentages
* they must be divided by the number of policies summarised,
* (CHDRMJA-POLSUM) before being displayed. Any remainder from
* the division must be shown as belonging to the amounts when
* policy number one is displayed, as the following example will
* serve to illustrate:
*
*     Policies Summarised = 3
*     Amount = $100
*
* For notional policy #1 the calculation will be:
*
*     Value to Display = Amount - (Amount * (POLSUM - 1))
*                                           -------------
*                                             POLSUM
*                      = 100 - 100 * (3 - 1) / 3
*                      = 100 - 100 * 2/3
*                      = $34
*
* For notional policies #2 and #3 the value will simply be
* divided by 3 giving $33 each.
*
* If processing a notional policy remember to use a Plan Suffix
* of 0000 when reading the corresponding ULNK record. The UTRN
* records will however be written with the selected policy number
* and breakout will handle the physical breakout of all the other
* Contract Components later.
*
* The details of the contract being processed will be stored in
* the CHDRMJA I/O module. Retrieve the details user RETRV.
*
* The total amount of the transaction for all components must be
* maintained and validated against the amount currently in
* Contract Header suspense. The ACBL record will be read with
* a key of 'CH', the contract Company, Contract number
* Contract Currency, a SACSCODE of 'LP' and SACSTYP of 'S'.
* Within this transaction a field called WSSP-BIG-AMT which is
* held in the COPYBOOK WSSPLIFE, will be used to keep a total of
* all Single Premium TOPUPS done against all components
* attached to this contract.
* WSSP-BIG-AMT will be subtracted from the amount on the ACBL
* record to give the total actually left in CONTRACT suspense.
*
* Get the following descriptions and names:
*    - Contract Type  (T5688)
*    - Contract Status  (T3623)
*    - Premium Status  (T3588)
*    - Servicing Branch  (T1692)
*    - Owner Client   (CLTS)
*    - First Life Assured  (LIFE)
*    - Joint Life Assured  (LIFE)
*
* Read the corresponding ULNK record to obtain the existing fund
* split plan. If the fund split is by percentage then display
* the existing funds and percentages from ULNK and their
* Currency Codes from T5515. The user may then use the existing
* split as a default but will be allowed to amend it if required.
*
* Read T5540. If the Available Fund List code is blank then
* there is no list of permitted funds and the user will be
* allowed to enter any valid funds of his choice. If there is
* an available fund list code then all Fund Code fields on the
* screen must be protected.
*
* If the details from the ULNK records were not displayed as
* they were not split by percentages then use the Available
* Fund List code to read T5543 and display the permitted fund
* codes on the screen. Protect all the fund code fields and the
* Fund Split Plan field. Also display each fund currency from
* T5515.
*
* Set the Percent/Amount Indicator to 'P'.
*
* VALIDATION (2000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* The effective date may not be in the past.
*
* The Percent/Amount indicator may only be 'P' or 'A'.
*
* If the Fund Split Plan is entered use it to read T5510 and
* obtain the Standard Unit Allocation. Set up the funds and their
* percentages and obtain their currency codes from T5515. Set the
* Percent/Amount indicator to 'P'. The screen will be re-displayed
* if the Fund Split Plan was entered so Fund Split Plan must be
* blanked out before looping back around to re-display the
* screen so that the program will not continue re-displaying the
* screen indefinitely.
*
* The WSAA-CHDR-SUSPENSE is found by looking at line 3, item
* P5144 on table T5645 which will give either the normal
* suspense account or a special suspense account for Single
* Premium Top-up. This account needs to be equal to line 3,
* item P5147 on the same table. The ammount is the balance
* on one of these accounts.
*
* If the Premium entered on the screen is greater then amount of
* available in Contract suspence ie (S5144-SUSAMT) then the
* transaction may not proceed. Display an error message saying
* 'Insufficient money in suspense' and do not allow the user
* to continue except to kill the transaction or change the amount
*
* If it is not greater add the premium amount to WSSP-BIG-AMT.
* WSSP-BIG-AMT will thus be carried forward to all future occure-
* nces of the screen within the transaction and incremented
* each time by the new premium amount as the total amount for the
* whole transaction may not be greater than the amount in premium
* suspense.
*
* If amounts are entered they must add up to the entered premium
* for the component. If percentages are entered they must total
* 100%.
*
* The Commission Indicator may only have a spaces of 'X'.
*
* If the program is to loop around and re-display the screen or
* if 'KILL' has been pressed subtract the premium amount from
* WSSP-BIG-AMT.
*
* Commission Indicator is only allowed if an increase from the
* previous level has occurred.
*
* UPDATING (3000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* Each time we enter the 3000 section for a COMPONENT any
* existing UTRNS will be deleted any new ones created. This is to
* ensure that if we select and then return from the commission
* screen, if the screen details have changed then the UTRN record
* (s) created will reflect this.
*
* Write one UTRN for each fund with an amount or percentage. Set
* the following fields on the UTRN:
*
* CHDRCOY    |
* CHDRNUM    |
* LIFE       |
* COVERAGE   |     Full key using the new tranno
* RIDER      |
* PLNSFX     |
* TRANNO     | New Transaction Number (CHDRMJA-TRANNO + 1)
* USER        - WSSP information
* VTRFUND     - Fund Code
* UNITSA      - ?
* NDFIND      from T6647, use the allocation indicator
* UNITYP      the BASIS from T5540, add to it the sex and
*             transaction code as the key to T5537 which in
*             turn provides the key to T5536. In this table
*             select the values attributed to frequency 00.
* BATCOY     |
* BATCBRN    |
* BATCACTYR  |
* BATCACTMN  |     Standard WSSP information
* BATCTRCDE  |
* BATCBATCH  |
* MONIESDT    - from T6647, Effective Date for Transaction.
* CRTABLE     - CRTABLE Code from COVR.
* CNTCURR     - from CHDR
* CNTAMNT     - the amount of the premium for the particular fund
*               in contract currency * the enhancement factor.
*               Read T6647 for the Enhanced Allocation Basis. Add
*               to this the currency code to form the key to
*               T5545. Use the factor applicable to frequency 00.
* FNDCURR     - Fund Currency from T5515
* SACSCODE    - From T5645 keyed by Program Id. line 1
* SACSTYP     -  "     "     "   "    "      "   "   "
* GENLCDE     -  "     "     "   "    "      "   "   "
* CONTYP      -
* PRCSEQ      - From T6647
* CRCDTE      -
*
* If the entry in T5536 indicates that the amount invested is
* < 100% write a NON INVESTED UTRN as well as the standard UTRN.
*
* NEXT PROGRAM (4000- SECTION)
*
* If 'KILL' was requested move spaces to the current program
* pointer and exit.
*
* If the current stack action field is '*' then re-load the next
* 8 positions in the program stack from the saved area.
*
* If the Commission Indicator is '?' then overwrite it with '+'.
*
* If the Commission Indicator was selected with an 'X' then call
* GENSSW with an action of 'A' to obatin the optional program
* switching. Save the next 8 programs from the program stack and
* re-load those positions from the programs returned from GENSSW.
* Move '*' to the current stack action field and replace the
* 'X' in the Commission Indicator with '?'.
*
* If processing the Whole Plan, (COVRMJA-PLAN-SUFFIX = 0000),
* then the program will read the next COVRMJA record for the
* matching Company, Contract Number, Life, Coverage and Rider
* and loop back around to the 2000 section and re-display the
* details from the next COVRMJA record as thet were first set up
* by the first pass through the 1000 section. This will continue
* until all the matching COVRMJA records have been processed.
*
* Add 1 to the current program pointer.
*
******************Enhancements for Life Asia 1.0****************
*
* This enhancement allows the user to indicate which currency
* suspense to be debited to cover the top up amount, the default
* is contract currency. The logic applies is to convert the total
* top up amount from contract currency to the suspense currency
* indicated on the screen.  The amount is then compared with the
* suspense amount available, any insufficiency will be subject to
* the tolerance limit set on T5667.  Note that premiums can be
* injected onto more than one coverage within the same
* transaction.
*
*****************************************************************
* </pre>
*/
public class P5144 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(P5144.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5144");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaUlnkFound = new FixedLengthStringData(1).init(SPACES);
	private Validator ulnkFound = new Validator(wsaaUlnkFound, "Y");

	protected FixedLengthStringData wsaaUtrnFound = new FixedLengthStringData(1).init(SPACES);
	private Validator utrnFound = new Validator(wsaaUtrnFound, "Y");

	protected FixedLengthStringData wsaaUtrnWritten = new FixedLengthStringData(1).init(SPACES);
	private Validator utrnWritten = new Validator(wsaaUtrnWritten, "Y");

	private FixedLengthStringData wsaaHitrFound = new FixedLengthStringData(1).init(SPACES);
	private Validator hitrFound = new Validator(wsaaHitrFound, "Y");

	private FixedLengthStringData wsaaHitrWritten = new FixedLengthStringData(1).init(SPACES);
	private Validator hitrWritten = new Validator(wsaaHitrWritten, "Y");
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaIfValidFund = new FixedLengthStringData(1).init(SPACES);
	private Validator wsaaValidFund = new Validator(wsaaIfValidFund, "Y");
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalNetprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaChdrSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInvSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinglePrem = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaUtrnSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaHitrSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalPerc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPcUnits = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaPcInitUnits = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaPcMaxPeriods = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4);
	private PackedDecimalData wsaaPerc = new PackedDecimalData(5, 2);
	protected PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaCltdob = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaCltage = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaNetPremium = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaCurr = new FixedLengthStringData(3);
	private PackedDecimalData wsaaExrate = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaCalcTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance2 = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaToleranceApp = new ZonedDecimalData(4, 2);
	private ZonedDecimalData wsaaToleranceApp2 = new ZonedDecimalData(4, 2);
	private PackedDecimalData wsaaToleranceLimit = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaToleranceLimit2 = new PackedDecimalData(17, 2);
	private String wsaaT5671Found = "N";
	private String wsaaT5540Found = "N";
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(13, 2).init(0);
	protected FixedLengthStringData wsaaEnhancedAllocInd = new FixedLengthStringData(1).init(SPACES);
	protected PackedDecimalData wsaaProcSeqno = new PackedDecimalData(3, 0);
	protected FixedLengthStringData wsaaRiderAlpha = new FixedLengthStringData(2);
	protected ZonedDecimalData wsaaRiderNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderAlpha, 0, REDEFINE).setUnsigned();

	protected FixedLengthStringData wsaaT5515Item = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5515Item, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5515Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5536Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5536Allbas = new FixedLengthStringData(4).isAPartOf(wsaaT5536Item, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT5536Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5537Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5537Allbas = new FixedLengthStringData(3).isAPartOf(wsaaT5537Item, 0);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Item, 3);
	private FixedLengthStringData wsaaT5537Trans = new FixedLengthStringData(4).isAPartOf(wsaaT5537Item, 4);

	private FixedLengthStringData wsaaT5540Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5540Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5540Item, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaT5540Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5543Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5543Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5543Item, 0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(4).isAPartOf(wsaaT5543Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Currcode = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 4);

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Part1 = new FixedLengthStringData(5).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Part2 = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);

	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5645Prog = new FixedLengthStringData(5).isAPartOf(wsaaT5645Item, 0);
	private FixedLengthStringData filler4 = new FixedLengthStringData(3).isAPartOf(wsaaT5645Item, 5, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);
	private FixedLengthStringData filler5 = new FixedLengthStringData(1).isAPartOf(wsaaT6647Item, 7, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaFndsplItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFndopt = new FixedLengthStringData(4).isAPartOf(wsaaFndsplItem, 0);
	private FixedLengthStringData filler6 = new FixedLengthStringData(4).isAPartOf(wsaaFndsplItem, 4, FILLER).init(SPACES);
		/* WSAA-UNIT-VIRTUAL-FUNDS */
	private FixedLengthStringData[] wsaaUnitVirtualFund = FLSInittedArray(10, 4);
		/* WSAA-CURRCYS */
	private FixedLengthStringData[] wsaaCurrcy = FLSInittedArray(10, 3);

	private FixedLengthStringData wsaaUnitAllPercAmts = new FixedLengthStringData(170);
	private ZonedDecimalData[] wsaaUnitAllPercAmt = ZDArrayPartOfStructure(10, 17, 2, wsaaUnitAllPercAmts, 0);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator wsaaWholePlan = new Validator(wsaaPlanproc, "Y");
	private Validator wsaaSgleComponent = new Validator(wsaaPlanproc, "N");

	private FixedLengthStringData wsaaIfFirstTime = new FixedLengthStringData(1);
	private Validator wsaaFirstTime = new Validator(wsaaIfFirstTime, "Y");

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);

	private FixedLengthStringData wsaaPayrkey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrkey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrkey, 8);

	private FixedLengthStringData wsaaAgtTerminatedFlag = new FixedLengthStringData(1);
	
	private Validator agtTerminated = new Validator(wsaaAgtTerminatedFlag, "Y");
	private Validator agtNotTerminated = new Validator(wsaaAgtTerminatedFlag, "N");
	private PackedDecimalData wsaaTotTax = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaTaxableAmt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotNonInvest = new ZonedDecimalData(17, 2);
	private String wsaaTaxTranType = "";
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub3 = new PackedDecimalData(3, 0);
	protected PackedDecimalData index1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaLifeassurSex = new FixedLengthStringData(1);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PcddmjaTableDAM pcddmjaIO = new PcddmjaTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private UtrncfiTableDAM utrncfiIO = new UtrncfiTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T5510rec t5510rec = new T5510rec();
	private T5515rec t5515rec = new T5515rec();
	private T5536rec t5536rec = new T5536rec();
	private T5537rec t5537rec = new T5537rec();
	private T5540rec t5540rec = new T5540rec();
	private T5543rec t5543rec = new T5543rec();
	private T5545rec t5545rec = new T5545rec();
	private T5551rec t5551rec = new T5551rec();
	private T5671rec t5671rec = new T5671rec();
	protected T5645rec t5645rec = new T5645rec();
	protected T6647rec t6647rec = new T6647rec();
	protected T5688rec t5688rec = new T5688rec();
	private T6687rec t6687rec = new T6687rec();
	private T5667rec t5667rec = new T5667rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Prasrec prasrec = new Prasrec();
	private Gensswrec gensswrec = new Gensswrec();
	private T6659rec t6659rec = new T6659rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	protected Wssplife wssplife = new Wssplife();
	protected S5144ScreenVars sv = ScreenProgram.getScreenVars( S5144ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	protected FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private ExternalisedRules er = new ExternalisedRules();
	private Zutrpf zutrpf = new Zutrpf();
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);
	private FixedLengthStringData checkIfFirstTime = new FixedLengthStringData(1);
	
	private T5533rec t5533rec;	//ILIFE-7392
	private ItempfDAO itempfDAO = DAOFactory.getItempfDAO();
	private FixedLengthStringData tableItem = new FixedLengthStringData(8);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(tableItem, 0);
	private FixedLengthStringData prmcurr = new FixedLengthStringData(3).isAPartOf(tableItem, 4);
	private PremValidaterec premValidaterec;
	//ILIFE-8288 start
	//private PackedDecimalData cnttypercd = null;
	private int cnttypercd;
	//ILIFE-8288 end
	private Datcon2rec datcon2rec = new Datcon2rec();
	private ZonedDecimalData outdate1 = null;
	private ZonedDecimalData[] outdates = new ZonedDecimalData[12];
	private ZonedDecimalData outdate2 = null;
	private ZonedDecimalData cnttypepresentdate = null;
	private int[] cnttypeallowedperiod = new int[12];
	private boolean fundPermission = false;
	private int allowperiod =0;
	private boolean premValidation=false;
	//ILIFE-8137 
    protected Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	protected Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;
	private boolean fundList = false;//ILIFE-8164
	//ILIFE-8786 start
	private Acblpf acblpf = new Acblpf();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
	private Clrfpf clrfpf = new Clrfpf();
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private Clntpf clts = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	private Hitrpf hitrpf = new Hitrpf();
	private List<Hitrpf> hitrpfList = new ArrayList<>();
	private List<Hitrpf> hitrList = new ArrayList<>();
	protected Payrpf payrpf=new Payrpf();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Taxdpf taxdpf = new Taxdpf();
	private TaxdpfDAO taxdpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private List<Taxdpf> taxdInsertList = null;
	protected FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private Ulnkpf ulnkpf = new Ulnkpf();
	private UlnkpfDAO ulnkpfDAO = getApplicationContext().getBean("ulnkpfDAO", UlnkpfDAO.class);
	private UtrnpfDAO utrnpfDAO	 = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private List<Utrnpf> utrnList = new ArrayList<>();
	List<Utrnpf> utrnpfList  = new ArrayList<>();
	private Utrnpf utrnpf = new Utrnpf();
	private FixedLengthStringData function = new FixedLengthStringData(1);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf6647;
	private Itempf itempf5551;
	protected Map<String, List<Itempf>> tr52eListMap;
	protected Map<String, List<Itempf>> tr52dListMap;
	//ILIFE-8786 end
	
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readUlnks1050, 
		exit1090, 
		utrnLoop1205, 
		exit1390, 
		validate2020, 
		fndLoop2030, 
		endLoop2040, 
		redisplay2080, 
		exit2090, 
		utrnLoop3030, 
		continue3080, 
		processTax3050, 
		nonInvestTax3060, 
		exit3090, 
		exit4190, 
		init4850, 
		exit5190, 
		para5900, 
		gotT55375920, 
		yOrdinate5940, 
		increment5960, 
		xOrdinate5980, 
		increment5985, 
		n00Loop5950, 
		exit5990, 
		pcddLoop7010, 
		addPcdt7020, 
		call7030, 
		enhanb9020, 
		enhanc9030, 
		enhand9040, 
		enhane9050, 
		enhanf9060, 
		nearExit9080, 
		a105HitrLoop
	}

	public P5144() {
		super();
		screenVars = sv;
		new ScreenModel("S5144", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clts.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clts.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					retriveChdrDetails1010();
					getCovrPayrLife1020();
					readT55401040();
				case readUlnks1050: 
					readUlnks1050();
					sacs1060();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/* If returning from a multiple selection then go to exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsaaTotalNetprem.set(ZERO);
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaBusinessDate.set(datcon1rec.intDate);
		/* Initialise*/
		/* MOVE ZEROES                 TO S5144-TOTPREM                 */
		sv.totlprem.set(ZERO);
		sv.susamt.set(ZERO);
		sv.instprem.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.numpols.set(ZERO);
		sv.effdate.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.unitAllocPercAmt01.set(ZERO);
		sv.unitAllocPercAmt02.set(ZERO);
		sv.unitAllocPercAmt03.set(ZERO);
		sv.unitAllocPercAmt04.set(ZERO);
		sv.unitAllocPercAmt05.set(ZERO);
		sv.unitAllocPercAmt06.set(ZERO);
		sv.unitAllocPercAmt07.set(ZERO);
		sv.unitAllocPercAmt08.set(ZERO);
		sv.unitAllocPercAmt09.set(ZERO);
		sv.unitAllocPercAmt10.set(ZERO);
		sv.unitVirtualFund01.set(SPACES);
		sv.unitVirtualFund02.set(SPACES);
		sv.unitVirtualFund03.set(SPACES);
		sv.unitVirtualFund04.set(SPACES);
		sv.unitVirtualFund05.set(SPACES);
		sv.unitVirtualFund06.set(SPACES);
		sv.unitVirtualFund07.set(SPACES);
		sv.unitVirtualFund08.set(SPACES);
		sv.unitVirtualFund09.set(SPACES);
		sv.unitVirtualFund10.set(SPACES);
		sv.currcy01.set(SPACES);
		sv.currcy02.set(SPACES);
		sv.currcy03.set(SPACES);
		sv.currcy04.set(SPACES);
		sv.currcy05.set(SPACES);
		sv.currcy06.set(SPACES);
		sv.currcy07.set(SPACES);
		sv.currcy08.set(SPACES);
		sv.currcy09.set(SPACES);
		sv.currcy10.set(SPACES);
		wsaaTotalPerc.set(ZERO);
		wsaaUnitAllPercAmts.set(ZERO);
		wsaaUlnkFound.set(SPACES);
		prasrec.taxrelamt.set(ZERO);
		wsaaLifeassurSex.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.reserveUnitsDate.set(varcom.vrcmMaxDate);
		wsaaUtrnWritten.set(SPACES);
		wsaaHitrWritten.set(SPACES);
		fundPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP086", appVars, "IT");
		fundList = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP106", appVars, "IT");//ILIFE-8164

	}


protected void allowperiodcalulation2021()
{   int i =0;
     int j=0;
	allowperiod=0;
	int k=0;
	
	  
    initialize(datcon1rec.datcon1Rec);
    datcon1rec.datcon1Rec.set(SPACES);
    datcon1rec.function.set(varcom.tday);
    Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    cnttypepresentdate = datcon1rec.intDate;
	
	
	if (fundPermission == true )
	{
//ILIFE-8288 start
	//	cnttypercd = new PackedDecimalData(chdrpf.getCcdate());
		cnttypercd = chdrpf.getCcdate();
//ILIFE-8288 end
		readT55511500();
		readTableT55436600();
		for(FixedLengthStringData funds:sv.unitVirtualFund) { //loop through all funds selected on UI
		for(i=0; i<12;i++)
		{
			if(funds!=null && funds.equals(t5543rec.unitVirtualFund[i])) {
				break;
			}
			
		}
		if(i==12)
			k=11;
		else
			k=i;
		cnttypeallowedperiod[j]=0;
		if(!isEQ(t5543rec.allowperiod[i],SPACES)  && !isEQ(t5543rec.unitVirtualFund[i],SPACES)) {
			cnttypeallowedperiod[j] = t5543rec.allowperiod[i].toInt();
		
		}
	
	    
		outdates[j]=null;
		if (cnttypeallowedperiod[j]!=0)
		{
			
			initialize(datcon2rec.datcon2Rec);
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(cnttypercd);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(cnttypeallowedperiod[j]);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);  
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				datcon2rec.freqFactorx.set(ZERO);
			}
			outdates[j] = datcon2rec.intDate2;
		 }
	
		if (outdates[j]!= null && isLT(cnttypepresentdate.toInt(),outdates[j]) ) { 
			sv.vrtfndErr[j].set(errorsInner.rrku);
			wsspcomn.edterror.set("Y");
			
		}
		j++;
		}
	}  
	  
		

	    
	}
protected void retriveChdrDetails1010()
	{
		/* RETRIEVE CONTRACT HEADER DETAILS WHICH HAVE BEEN STORED IN*/
		/* THE CHDRMJA I/O MODULE.*/
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());    
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		sv.cnttype.set(chdrpf.getCnttype());
		sv.register.set(chdrpf.getReg());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.numpols.set(chdrpf.getPolinc());
	}

protected void getCovrPayrLife1020()
	{
		/*  The next check is to stop the program abending*/
		/*  when it is passed a blank key. In this case the program*/
		/*  should just go thru without processing.*/
		//ILIFE-8137
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
				}
				else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
				else {
					covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isEQ(covrmjaIO.getStatuz(), varcom.mrnf)) {
			covrmjaIO.setChdrnum(SPACES);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* As we have no control over the order Coverage/Rider Top-Ups     */
		/* are entered, always return to the Sub-menu to select            */
		/* the next Component for multiple inputs.                         */
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		/* Read the PAYR file with payer sequence number 1.*/
		//ILIFE-8786 start
		payrpf=new Payrpf();
		payrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		payrpf.setChdrnum(chdrpf.getChdrnum());
		payrpf.setPayrseqno(1);
		payrpf.setValidflag("1");	
		List<Payrpf> payrpfList = payrpfDAO.readPayrData(payrpf);
		if (payrpfList == null || payrpfList.isEmpty()) {
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set(chdrpf.getChdrnum());
			fatalError600();
		}
		
		/* Read the client role file to get the payer number.*/
		clrfpf = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrpf.getChdrpfx(), payrpf.getChdrcoy(),payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno())), "PY");
		if (clrfpf == null) {
			syserrrec.params.set(payrpf.getChdrcoy().concat(payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()))));
			fatalError600();
		}
		//ILIFE-8786 end
		
		sv.billcurr.set(payrpf.getBillcurr());
		/* Check agent termination status.  If agent is terminated at the  */
		/* point of collection, 2nd premium shortfall limit is not         */
		/* applicable.                                                     */
		a400CheckAgent();
		/* If the user has already entered the Top-Up for a component*/
		/* do not allow them to alter it, instead redisplay the*/
		/* component selection screen.*/
		utrnExist1200();
		a100HitrExist();
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.planSuffix.set(covrpf.getPlanSuffix());
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		sv.chdrnum.set(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		sv.life.set(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		//ILIFE-8786 start
		sv.lifenum.set(lifemjaIO.getLifcnum());
		wsaaCltdob.set(lifemjaIO.getCltdob());
		wsaaLifeassurSex.set(lifemjaIO.getCltsex());
		clts.setClntpfx("CN");
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntnum(lifemjaIO.getLifcnum().toString());
		clts = clntpfDAO.selectActiveClient(clts);
		if(clts==null) {
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set(lifemjaIO.getLifcnum());
			fatalError600();
		}
		
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* CHECK FOR EXISTENCE OF JOINT LIFE DETAILS.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifemjaIO.getLifcnum());
			clts.setClntcoy(wsspcomn.fsuco.toString());
			clts.setClntnum(lifemjaIO.getLifcnum().toString());
			clts = clntpfDAO.selectActiveClient(clts);
			if(clts==null) {
				syserrrec.params.set(lifemjaIO.getLifcnum());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		//ILIFE-8786 end
		sv.crtable.set(covrpf.getCrtable());
		sv.instprem.set(ZERO);
		sv.percentAmountInd.set("P");
		taxMethT56881300();
		
		crtable.set(covrpf.getCrtable());	//ILIFE-7392
		prmcurr.set(covrpf.getPremCurrency());
	}

protected void readT55401040()
	{
		/* Get fund split plan*/
		sv.virtFundSplitMethod.set(SPACES);
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5540Item.set(SPACES);
		wsaaT5540Crtable.set(covrpf.getCrtable());
		itdmIO.setItemitem(wsaaT5540Item);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5540)
		|| isNE(itdmIO.getItemitem(), wsaaT5540Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.h023);
			wsaaT5540Found = "N";
			goTo(GotoLabel.readUlnks1050);
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
			wsaaT5540Found = "Y";
		}
		sv.virtFundSplitMethod.set(SPACES);
		readT56711400();
		t5551rec.alfnds.set(SPACES);
		if (isEQ(wsaaT5671Found, "N")) {
			scrnparams.errorCode.set(errorsInner.f025);
			wsspcomn.edterror.set("Y");
		}
		else {
			readT55511500();
		}
		if (isNE(t5551rec.alfnds, SPACES)) {
			readTableT55436600();
		}
		readTr52d1600();
		if (isEQ(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			readTr52e1700();
		}
		//ILIFE-8164- STARTS
				if(fundList)
				{
					int ix = 0;
					for(FixedLengthStringData recData:t5543rec.unitVirtualFund) {
						if(recData != null) {
							sv.newFundList[ix].set(recData);	
						}
						ix++;
					}
					
					
						sv.newFundList01Out[varcom.nd.toInt()].set("N");
						sv.newFundList02Out[varcom.nd.toInt()].set("N");
						sv.newFundList03Out[varcom.nd.toInt()].set("N");
						sv.newFundList04Out[varcom.nd.toInt()].set("N");
						sv.newFundList05Out[varcom.nd.toInt()].set("N");
						sv.newFundList06Out[varcom.nd.toInt()].set("N");
						sv.newFundList07Out[varcom.nd.toInt()].set("N");
						sv.newFundList08Out[varcom.nd.toInt()].set("N");
						sv.newFundList09Out[varcom.nd.toInt()].set("N");
						sv.newFundList10Out[varcom.nd.toInt()].set("N");
						sv.newFundList11Out[varcom.nd.toInt()].set("N");
						sv.newFundList12Out[varcom.nd.toInt()].set("N");
						
					}
				else {

					sv.newFundList01Out[varcom.nd.toInt()].set("Y");
					sv.newFundList02Out[varcom.nd.toInt()].set("Y");
					sv.newFundList03Out[varcom.nd.toInt()].set("Y");
					sv.newFundList04Out[varcom.nd.toInt()].set("Y");
					sv.newFundList05Out[varcom.nd.toInt()].set("Y");
					sv.newFundList06Out[varcom.nd.toInt()].set("Y");
					sv.newFundList07Out[varcom.nd.toInt()].set("Y");
					sv.newFundList08Out[varcom.nd.toInt()].set("Y");
					sv.newFundList09Out[varcom.nd.toInt()].set("Y");
					sv.newFundList10Out[varcom.nd.toInt()].set("Y");
					sv.newFundList11Out[varcom.nd.toInt()].set("Y");
					sv.newFundList12Out[varcom.nd.toInt()].set("Y");
				}
				//ILIFE-8164- ENDS
	}

protected void readUlnks1050()
	{
		/*  Read ULNK to see if Details already exist*/
		//ILIFE-8786 start
		/* If the COVRMJA-PLAN-SUFFIX is not greater than the value of*/
		/* CHDRMJA-POLSUM then a 'notional' policy is being processed.*/
		ulnkpf = new Ulnkpf();
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			ulnkpf.setPlanSuffix(0);
		}
		else {
			ulnkpf.setPlanSuffix(covrpf.getPlanSuffix());
		}
		ulnkpf = ulnkpfDAO.searchUlnk(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00", covrpf.getCoverage(), covrpf.getRider(), ulnkpf.getPlanSuffix());
		
		if(ulnkpf==null) {
			wsaaUlnkFound.set("N");
			goTo(GotoLabel.exit1090);
		}
		else {
		wsaaUlnkFound.set("Y");
		for (x.set(1); !(isGT(x, 10)); x.add(1)){
			move1100();
		}
		sv.percentAmountInd.set(ulnkpf.getPercOrAmntInd());
		//ILIFE-8786 end
		}
	}

protected void sacs1060()
	{

	if (isEQ(wsspcomn.flag, "N")) {
		wsaaTotalPremium.set(ZERO);
		wsaaTotalNetprem.set(ZERO);
	}
	if (isNE(wsspcomn.flag, "N")) {
		sv.totlprem.set(wsaaTotalPremium);
		/* If Tolerance has been added to the previous premium, the        */
		/* TOTAL-NETPREM will be > than the Suspense amount. In this       */
		/* situation do not attempt to subtract TOTAL-NETPREM              */
		/* from Suspense, set Suspense amount to zero.                     */
		if (isGT(wsaaTotalNetprem, wsaaChdrSuspense)) {
			sv.susamt.set(ZERO);
		}
		else {
			compute(sv.susamt, 2).set(sub(wsaaChdrSuspense, wsaaTotalNetprem));
		}
		sv.paycurr.set(wsaaCurr);
		sv.exrat.set(wsaaExrate);
		sv.paycurrOut[varcom.pr.toInt()].set("Y");
		return ;
	}
	wsspcomn.flag.set(SPACES);
	
	/* Line 3, item P5144 on table T5645 is read and the right*/
	/* suspense account is chosen.*/
	readT56456400();
	acblpf = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode03.toString(), chdrpf.getChdrnum(), t5645rec.sacstype03.toString(), chdrpf.getCntcurr()); 
	Optional<Acblpf> isExists = Optional.ofNullable(acblpf);
	if (isExists.isPresent()) {		
		if(isNE(acblpf.getSacscurbal(), NEGATIVE)) {
			wsaaChdrSuspense.set(ZERO);
			sv.exrat.set(ZERO);
		}

	else {
			compute(wsaaChdrSuspense, 2).set(mult(acblpf.getSacscurbal(), -1));
		  }
	}
	
	compute(sv.susamt, 2).set(sub(wsaaChdrSuspense, wssplife.bigAmt));
	sv.totlprem.set(wssplife.bigAmt);
	sv.paycurr.set(chdrpf.getCntcurr());
	wsaaCurr.set(chdrpf.getCntcurr());
	/*    Payment currency is not necessary contract currency, so      */
	/*    default to zero instead of 1. User can get the exchange      */
	/*    rate by pressing Refresh key.                                */
	sv.exrat.set(ZERO);
	wsaaExrate.set(ZERO);
	wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
	if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
		covrpf.setPlanSuffix(9999);
		//covrmjaIO.setFunction(varcom.begn);
		sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		wsaaPlanproc.set("Y");
	}
	else {
		wsaaPlanproc.set("N");
		//covrmjaIO.setFunction(varcom.readr);
		sv.planSuffix.set(covrpf.getPlanSuffix());
	}
	policyLoad5000();
		}
			

	/**
	* <pre>
	* Sections performed from the 1000 section.
	* </pre>
	*/
protected void move1100()
	{
		/*MOVE-VALUES*/
		//ILIFE-8786 start
		sv.unitVirtualFund[x.toInt()].set(ulnkpf.getUalfnd(x.toInt()));
		wsaaUnitVirtualFund[x.toInt()].set(ulnkpf.getUalfnd(x.toInt()));
		sv.unitAllocPercAmt[x.toInt()].set(ulnkpf.getUalprc(x.toInt()));
		wsaaUnitAllPercAmt[x.toInt()].set(ulnkpf.getUalprc(x.toInt()));
		//ILIFE-8786 end
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[x.toInt()]);
		if (isNE(sv.unitVirtualFund[x.toInt()], SPACES)) {
			readT55156300();
			if (isEQ(wsspcomn.edterror, "Y")) {
				return ;
			}
			else {
				sv.currcy[x.toInt()].set(t5515rec.currcode);
				wsaaCurrcy[x.toInt()].set(t5515rec.currcode);
			}
		}
		/*EXIT*/
	}

protected void utrnExist1200()
	{
		/* Read T6647 to retrieve SEQ-NO, which is used to distinquish     */
		/* Coverages from Riders.                                          */
		/* Read UTRN to see if Details already exist*/
		wsaaUtrnFound.set("N");
		//ILIFE-8786 start
		
		utrnpf.setCoverage(covrpf.getCoverage());
		/* MOVE COVRMJA-RIDER         TO UTRN-RIDER.                    */
		utrnpf.setRider("00");
		utrnpf.setChdrcoy(covrpf.getChdrcoy());
		utrnpf.setChdrnum(covrpf.getChdrnum());
		utrnpf.setLife(covrpf.getLife());
		utrnpf.setPlanSuffix(covrpf.getPlanSuffix());
		utrnpf.setTranno(0);
		wsaaRiderAlpha.set(covrpf.getRider());
		compute(wsaaProcSeqno, 0).set(add(t6647rec.procSeqNo, wsaaRiderNum));
		utrnpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		utrnpf.setProcSeqNo(wsaaProcSeqno.toInt());
		utrnpfList = utrnpfDAO.searchUtrnpfRecord(utrnpf);
		if(!(utrnpfList.isEmpty())) {
			for(Utrnpf utrn:utrnpfList) {
							wsaaUtrnFound.set("Y");
							return ;
						}
		}else {
			return;
		}
	}

protected void taxMethT56881300()
	{
		try {
			para1310();
			readT66871320();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para1310()
	{
		/* Look up the tax relief method on T5688.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrpf.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5688rec.taxrelmth, SPACES)) {
			t6687rec.t6687Rec.set(SPACES);
			goTo(GotoLabel.exit1390);
		}
	}

protected void readT66871320()
	{
		/* Look up the tax relief subroutine on T6687*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(t5688rec.taxrelmth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6687rec.t6687Rec.set(itemIO.getGenarea());
	}

protected void readT56711400()
	{
		para1400();
	}

protected void para1400()
	{
		/* The Available Fund List default field is held on both table*/
		/* T5551 and T5540. Table T5551 is used at New Business to obtain*/
		/* the available Fund List, while table T5540 is used at Single*/
		/* Prem Top Up. This can lead to Inconsistencies when both tables -*/
		/* are set up with different values. This program will now use*/
		/* table T5551.*/
		/* Read table T5671 in order to get part of the key needed while*/
		/* reading table T5551.*/
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t5671);
		wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covrpf.getCrtable());
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5551Item.set(SPACES);
		wsaaT5671Found = "N";
		for (x.set(1); !(isGT(x, 4)
		|| isEQ(wsaaT5671Found, "Y")); x.add(1)){
			if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
				if (isEQ(t5671rec.edtitm[x.toInt()], SPACES)) {
					scrnparams.errorCode.set(errorsInner.f025);
					wsspcomn.edterror.set("Y");
					x.set(5);
				}
				else {
					wsaaT5551Part1.set(t5671rec.edtitm[x.toInt()]);
					wsaaT5671Found = "Y";
				}
			}
		}
	}

protected void readT55511500()
	{
		para1500();
	}

	/**
	* <pre>
	* Get T5551 to obtain correct available fund list default
	* </pre>
	*/
protected void para1500()
	{
		wsaaT5551Part2.set(chdrpf.getCntcurr());
	itempf5551 = itemDAO.findItemByItem(wsspcomn.company.toString(), tablesInner.t5551.toString(), wsaaT5551Item.toString());
	if(null  ==  itempf5551 ){
		syserrrec.params.set(wsspcomn.company.toString()+ tablesInner.t5551.toString()+wsaaT5551Item.toString());
			fatalError600();
		}
	else if (itempf5551.getItemitem().equals("")){
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
		t5551rec.t5551Rec.set(StringUtil.rawToString(itempf5551.getGenarea()));
		}
		
	}

protected void readTr52d1600()
	{
		start1610();
	}

protected void start1610()
	{
	tr52dListMap = itemDAO.loadSmartTable("IT",wsspcomn.company.toString(),tablesInner.tr52d.toString());	
	String keyItemitem = chdrpf.getReg();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr52dListMap.containsKey(keyItemitem)){	
		itempfList = tr52dListMap.get(keyItemitem);
		Itempf itempf = itempfList.get(0);
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
		itemFound = true;			
		}
	if (!itemFound) {
		keyItemitem = "***";
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (tr52dListMap.containsKey(keyItemitem)){	
			itempfList = tr52dListMap.get(keyItemitem);
			Itempf itempf = itempfList.get(0);
			tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound2 = true;					
		}		
		if (!itemFound2) {
			syserrrec.params.set("IT"+wsspcomn.company.toString()+tablesInner.tr52d.toString());
				fatalError600();
			}
		}
	}

protected void readTr52e1700()
	{
		start1710();
	}

protected void start1710()
	{
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());

	tr52eListMap = itemDAO.loadSmartTable("IT", chdrpf.getChdrcoy().toString(), "TR52E");	
	String keyItemitem = wsaaTr52eKey.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr52eListMap.containsKey(keyItemitem)){	
		itempfList = tr52eListMap.get(keyItemitem);
		Itempf itempf = itempfList.get(0);
		tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
		itemFound = true;			
		}
	if (!itemFound) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
		keyItemitem = wsaaTr52eKey.toString().trim();
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (tr52eListMap.containsKey(keyItemitem)){	
			itempfList = tr52eListMap.get(keyItemitem);
			Itempf itempf = itempfList.get(0);
			tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound2 = true;					
			}
		if (!itemFound2) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			keyItemitem = wsaaTr52eKey.toString().trim();
			itempfList = new ArrayList<Itempf>();
			boolean itemFound3 = false;
			if (tr52eListMap.containsKey(keyItemitem)){	
				itempfList = tr52eListMap.get(keyItemitem);
				Itempf itempf = itempfList.get(0);
				tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound3 = true;		
			}	
			if (!itemFound3) {
				return;		
			}		
			}
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| utrnFound.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.positionCursor.set("EFFDATE   ");
		if (isNE(scrnparams.errorCode, SPACES)) {
			scrnparams.function.set(varcom.prot);
			wsspcomn.flag.set("I");
		}
		
		
		// ILIFE-5946 Start
		if(isEQ(t5551rec.rsvflg, "N")){	//ILIFE-7392
			sv.rundteOut[varcom.pr.toInt()].set("Y");
			sv.rsuninOut[varcom.pr.toInt()].set("Y");
		}// ILIFE-5946 END
		
		// ILIFE-5446 Start
		if ((isEQ(t5551rec.rsvflg, "Y") && isEQ(sv.reserveUnitsInd, "Y")) ||
				(isEQ(t5551rec.rsvflg, "Y") && isEQ(sv.reserveUnitsInd, SPACE))){
			if(isNE(checkIfFirstTime, "Y")){
				sv.reserveUnitsInd.set(t5551rec.rsvflg);
			}
		}
		
		if ((isEQ(t5551rec.rsvflg, "N") && isEQ(sv.reserveUnitsInd, "N")) ||
				(isEQ(t5551rec.rsvflg, "N") && isEQ(sv.reserveUnitsInd, SPACE))){
			if(isNE(checkIfFirstTime, "Y")){ 
				sv.reserveUnitsInd.set(t5551rec.rsvflg);
			}
		}
		
		if ((isEQ(t5551rec.rsvflg, SPACES) && isEQ(sv.reserveUnitsInd, SPACES)) ||
				(isEQ(t5551rec.rsvflg, SPACES) && isEQ(sv.reserveUnitsInd, SPACE))){
				sv.reserveUnitsInd.set(t5551rec.rsvflg);
		}
		
		// ILIFE-5446 End
		
		/*CALL-SCREEN*/
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
				case validate2020: 
					validate2020();
				case fndLoop2030: 
					fndLoop2030();
				case endLoop2040: 
					endLoop2040();
				case redisplay2080: 
					redisplay2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5144IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   S5144-DATA-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaT5540Found, "N")) {
			scrnparams.errorCode.set(errorsInner.h023);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.redisplay2080);
		}
		if (isEQ(wsaaT5671Found, "N")) {
			scrnparams.errorCode.set(errorsInner.f025);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.redisplay2080);
		}
		/* FOR 'CALC' OPTION, EITHER VALIDATE ALL FIELDS OR*/
		/* READ T5510 AND REDISPLAY.*/
		if (isEQ(scrnparams.statuz, "CALC")) {
			if (isEQ(sv.virtFundSplitMethod, SPACES)) {
				goTo(GotoLabel.validate2020);
			}
			else {
				wsaaFndopt.set(sv.virtFundSplitMethod);
				readItem5100();
				if (isNE(sv.fndsplErr, SPACES)) {
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
				else {
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
			}
		}
		else {
			if (isNE(sv.virtFundSplitMethod, SPACES)) {
				wsaaFndopt.set(sv.virtFundSplitMethod);
				readItem5100();
				/** Screen errors are now handled in the calling program.           */
				/**          PERFORM 200-SCREEN-ERRORS                              */
			}
		}
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.validate2020);
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		/* UNREACHABLE CODE
		wsspcomn.edterror.set(varcom.oK);
		 */
		allowperiodcalulation2021();
	}

protected void validate2020()
	{
		/* Validate fields*/
		sv.errorIndicators.set(SPACES);
		sub1.set(ZERO);
		allowperiodcalulation2021();//ILIFE-7891
	}

protected void fndLoop2030()
	{
		sub1.add(1);
		if (isGT(sub1, 10)) {
			goTo(GotoLabel.endLoop2040);
		}
		if (isEQ(sv.unitVirtualFund[1], SPACES)) {
			sv.vrtfndErr[sub1.toInt()].set(errorsInner.g037);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.unitVirtualFund[sub1.toInt()], SPACES)) {
			sv.currcy[sub1.toInt()].set(SPACES);
			goTo(GotoLabel.fndLoop2030);
		}
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[sub1.toInt()]);
		readT55156300();
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.exit2090);
		}
		sv.currcy[sub1.toInt()].set(t5515rec.currcode);
		wsaaCurrcy[sub1.toInt()].set(t5515rec.currcode);
		wsaaIfValidFund.set("N");
		if (isNE(sv.unitVirtualFund[sub1.toInt()], SPACES)
		&& isNE(t5551rec.alfnds, SPACES)) {
			for (sub3.set(1); !(isGT(sub3, 12)
			|| wsaaValidFund.isTrue()); sub3.add(1)){
				validateFund8000();
			}
			if (!wsaaValidFund.isTrue()) {
				sv.vrtfndErr[sub1.toInt()].set(errorsInner.h456);
			}
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		goTo(GotoLabel.fndLoop2030);
	}

protected void endLoop2040()
	{
		if (isNE(sv.percentAmountInd, "P")
		&& isNE(sv.percentAmountInd, "A")) {
			sv.pcamtindErr.set(errorsInner.g347);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.effdate, 99999999)) {
			sv.effdateErr.set(errorsInner.e859);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isLT(sv.effdate, varcom.vrcmDate)) {
			sv.effdateErr.set(errorsInner.e859);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isLT(sv.effdate, chdrpf.getOccdate())) {
			sv.effdateErr.set(errorsInner.h359);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* IF  S5144-SUSAMT            =  ZERO                          */
		/*     MOVE E751               TO S5144-SUSAMT-ERR              */
		/*     MOVE 'Y'                TO WSSP-EDTERROR                 */
		/*     GO TO 2090-EXIT                                          */
		/* END-IF.                                                      */
		if (isEQ(sv.instprem, ZERO)) {
			sv.instpremErr.set(errorsInner.e199);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.instprem, ZERO)) {
			zrdecplcPojo.setAmountIn(sv.instprem.getbigdata()); //ILIFE-8786
			b000CallRounding();
			if (isNE(zrdecplcPojo.getAmountOut(), sv.instprem)) {
				sv.instpremErr.set(errorsInner.rfik);
			}
		}
		if (isEQ(sv.paycurr, SPACES)) {
			sv.paycurrErr.set(errorsInner.h960);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.comind, "X")
		&& isNE(sv.comind, "+")
		&& isNE(sv.comind, SPACES)) {
			sv.comindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		editFundSplit5500();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* If the tax relief method is not spaces calculate the tax*/
		/* relief amount and deduct it from the premium.*/
		prasrec.taxrelamt.set(ZERO);
		if (isNE(t5688rec.taxrelmth, SPACES)) {
			prasrec.clntnum.set(clrfpf.getClntnum()); //ILIFE-8786
			prasrec.clntcoy.set(clrfpf.getClntcoy()); //ILIFE-8786
			prasrec.incomeSeqNo.set(payrpf.getIncomeSeqNo());//ILIFE-8786
			prasrec.cnttype.set(chdrpf.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			prasrec.effdate.set(sv.effdate);
			prasrec.company.set(chdrpf.getChdrcoy());
			prasrec.grossprem.set(sv.instprem);
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
		}
		zrdecplcPojo.setAmountIn(prasrec.taxrelamt.getbigdata());//ILIFE-8786
		b000CallRounding();
		prasrec.taxrelamt.set(zrdecplcPojo.getAmountOut());//ILIFE-8786
		compute(wsaaNetPremium, 2).set(sub(sv.instprem, prasrec.taxrelamt));
		wsaaTotTax.set(ZERO);
		if (isNE(tr52drec.txcode, SPACES)
		&& isNE(sv.instprem, ZERO)
		&& isEQ(tr52erec.taxind01, "Y")) {
			wsaaTaxableAmt.set(sv.instprem);
			wsaaTaxTranType = "PREM";
			a500CalcTax();
		}
		wsaaNetPremium.add(wsaaTotTax);
		/*ILIFE-1073 starts*/
		sv.totTax.set(wsaaTotTax);
		/*ILIFE-1073-ends*/
		compute(sv.taxamt, 2).set(add(sv.instprem, wsaaTotTax));
		/* If Payment of premium is in a currency other than the           */
		/* Contract currency, retrieve the Suspense amount. The            */
		/* Currency can only be altered on the first Coverage,             */
		/* thereafter it is protected and used in all future iterations.   */
		/* If currency changed, validate against T3629.                    */
		if (isNE(sv.paycurr, wsaaCurr)) {
			itemIO.setDataArea(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.t3629);
			itemIO.setItemitem(sv.paycurr);
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				sv.paycurrErr.set(errorsInner.f982);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			findSuspense2000a();
		}
		/* Convert Net Premium if paying in currency other than the        */
		/* Contract currency.                                              */
		if (isNE(sv.paycurr, chdrpf.getCntcurr())) {
			convertNetPremium2000b();
		}
		/* If insufficient Suspense, calculate Tolerance.                  */
		wsaaTolerance.set(ZERO);
		wsaaTolerance2.set(ZERO);
		if (((setPrecision(wsaaChdrSuspense, 2)
		&& isGT((add(wsaaNetPremium, wsaaTotalNetprem)), wsaaChdrSuspense)))) {
			calcTolerance2000c();
		}
		/* Check sufficient suspense available.                            */
		if (((setPrecision(null, 2)
		&& isGT((add(wsaaNetPremium, wsaaTotalNetprem)), (add(wsaaTolerance, wsaaChdrSuspense)))))) {
			if (((setPrecision(null, 2)
			&& isGT((add(wsaaNetPremium, wsaaTotalNetprem)), (add(wsaaTolerance2, wsaaChdrSuspense)))))) {
				sv.instpremErr.set(errorsInner.hl06);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		/* Must convert premium if payment currency different to           */
		/* Contract currency before adding to total premium amount.        */
		/* Keep cummulative total of premiums in Contract currency.        */
		if (isEQ(scrnparams.statuz, "CALC")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(sv.paycurr, chdrpf.getCntcurr())) {
				wsaaTotalPremium.add(sv.instprem);
			}
			else {
				convertPremium2000d();
			}
			wsaaTotalNetprem.add(wsaaNetPremium);
		}
		
		// ILIFE-5446 Start
		
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("Y")){
			
			if (isEQ(sv.reserveUnitsDate, 99999999) || isEQ(sv.reserveUnitsDate, SPACES) || isEQ(sv.reserveUnitsDate, 00000000)) { 
				sv.rundteErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			
			if (isLT(sv.reserveUnitsDate, chdrpf.getCcdate())) {
				sv.rundteErr.set(errorsInner.h359);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			if (isGT(sv.reserveUnitsDate, datcon1rec.intDate)) {
				sv.rundteErr.set(errorsInner.rlcc);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			
		}  
		
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("N") 
				&& !sv.reserveUnitsDate.equals(varcom.vrcmMaxDate)){
			sv.rundteErr.set(errorsInner.g099);
			sv.rsuninErr.set(errorsInner.g099);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		
		//ILIFE-7392
		/*if (isEQ(sv.reserveUnitsInd, SPACES)) { 
			checkIfFirstTime.set("Y");
			sv.rsuninErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}*/
		// ILIFE-5446 end
		premValidation = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM005", appVars, "IT");
		if(!(premValidation && AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable()))){
		readT5533();	//ILIFE-7392
		if(isNE(t5533rec.t5533Rec, SPACES) && isNE(t5533rec.casualContribMin, ZERO) && isNE(t5533rec.casualContribMax, ZERO)
			 && (isLT(sv.instprem, t5533rec.casualContribMin) || isGT(sv.instprem, t5533rec.casualContribMax))){
				sv.instpremErr.set(errorsInner.g070);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}else{
			premValidaterec=new PremValidaterec();//ILIFE-7524
			premValidaterec.billFreq.set(chdrpf.getBillfreq());
			premValidaterec.inputPrem.set(sv.instprem);
			premValidaterec.covrgCode.set(covrpf.getCrtable());
			premValidaterec.currCode.set(chdrpf.getCntcurr());
			premValidaterec.ratingDate.set(chdrpf.getOccdate());
			premValidaterec.cnttype.set(chdrpf.getCnttype());
			premValidaterec.statuz.set(varcom.oK);
			callProgram("PRMVAL", premValidaterec);
			if(isNE(premValidaterec.statuz,varcom.oK)){
			sv.instpremErr.set(errorsInner.g070);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
			}
		}
	}

	/**
	* <pre>
	* Check the contract suspense for the net premium
	* This checking is removed.                                       
	**** IF  WSAA-NET-PREMIUM        >  S5144-SUSAMT                  
	****     MOVE E751               TO S5144-INSTPREM-ERR            
	****     MOVE 'Y'                TO WSSP-EDTERROR                 
	****     GO TO 2090-EXIT                                          
	**** END-IF.                                                      
	* Redisplay the screen if 'CALC'
	* </pre>
	*/
protected void redisplay2080()
	{
		if (isNE(scrnparams.statuz, "CALC")) {
			return ;
		}
		else {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     LOCATE SUSPENSE IN REQUESTED CURRENCY.                      
	* </pre>
	*/
protected void findSuspense2000a()
	{
		read2010a();
	}

protected void read2010a()
	{
		/* Retrieve Suspense for requested currency.                       */
		//ILIFE-8786 start
		acblpf = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode03.toString(), chdrpf.getChdrnum(), t5645rec.sacstype03.toString(), sv.paycurr.toString()); 
		if(acblpf==null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(chdrpf.getChdrnum());
			fatalError600();
		}
		if(isNE(acblpf.getSacscurbal(), NEGATIVE)) {
			wsaaChdrSuspense.set(ZERO);
			sv.susamt.set(ZERO);
			sv.exrat.set(ZERO);
			wsaaExrate.set(ZERO);
		}else {
			compute(wsaaChdrSuspense, 2).set(mult(acblpf.getSacscurbal(), -1));
			sv.susamt.set(wsaaChdrSuspense);
			if (isEQ(sv.paycurr, chdrpf.getCntcurr())) {
				sv.exrat.set(1);
				wsaaExrate.set(1);
			}
		}
		wsaaCurr.set(sv.paycurr);
		//ILIFE-8786 end
	}

	/**
	* <pre>
	*     CONVERT THE NET PREMIUM TO THE SUSPENSE CURRENCY.           
	* </pre>
	*/
protected void convertNetPremium2000b()
	{
		call2010b();
	}

protected void call2010b()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currIn.set(chdrpf.getCntcurr());
		conlinkrec.currOut.set(sv.paycurr);
		conlinkrec.amountIn.set(wsaaNetPremium);
		conlinkrec.company.set(wsspcomn.company);
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		wsaaNetPremium.set(conlinkrec.amountOut);
		sv.exrat.set(conlinkrec.rateUsed);
		wsaaExrate.set(conlinkrec.rateUsed);
	}

	/**
	* <pre>
	*     CALCULATE TOLERANCE LEVEL.                                  
	* </pre>
	*/
protected void calcTolerance2000c()
	{
		read2010c();
	}

protected void read2010c()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5667);
		wsaaT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5667Curr.set(sv.paycurr);
		itemIO.setItemitem(wsaaT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
		/* Calculate the Tolerance level for the payment frequency.        */
		wsaaToleranceApp.set(ZERO);
		wsaaToleranceLimit.set(ZERO);
		wsaaToleranceApp2.set(ZERO);
		wsaaToleranceLimit2.set(ZERO);
		for (sub1.set(1); !(isGT(sub1, 11)); sub1.add(1)){
			if (isEQ(chdrpf.getBillfreq(), t5667rec.freq[sub1.toInt()])) {
				wsaaToleranceApp.set(t5667rec.prmtol[sub1.toInt()]);
				wsaaToleranceApp2.set(t5667rec.prmtoln[sub1.toInt()]);
				wsaaToleranceLimit.set(t5667rec.maxAmount[sub1.toInt()]);
				wsaaToleranceLimit2.set(t5667rec.maxamt[sub1.toInt()]);
				sub1.set(11);
			}
		}
		/* Calculate the tolerance applicable.                             */
		compute(wsaaCalcTolerance, 2).set((div((mult(wsaaToleranceApp, (add(wsaaNetPremium, wsaaTotalNetprem)))), 100)));
		wsaaCalcTolerance2.set(ZERO);
		if (isNE(wsaaToleranceLimit2, ZERO)) {
			if (agtNotTerminated.isTrue()
			|| (agtTerminated.isTrue()
			&& isEQ(t5667rec.sfind, "2"))) {
				compute(wsaaCalcTolerance2, 2).set((div((mult(wsaaToleranceApp2, (add(wsaaNetPremium, wsaaTotalNetprem)))), 100)));
			}
		}
		/* Check % amount is less than Limit on T5667, if so use this      */
		/* else use Limit.                                                 */
		if (isLT(wsaaCalcTolerance, wsaaToleranceLimit)) {
			wsaaTolerance.set(wsaaCalcTolerance);
		}
		else {
			wsaaTolerance.set(wsaaToleranceLimit);
		}
		if (isLT(wsaaCalcTolerance2, wsaaToleranceLimit2)) {
			wsaaTolerance2.set(wsaaCalcTolerance2);
		}
		else {
			wsaaTolerance2.set(wsaaToleranceLimit2);
		}
	}

	/**
	* <pre>
	*     CONVERT THE PREMIUM TO THE SUSPENSE CURRENCY.               
	* </pre>
	*/
protected void convertPremium2000d()
	{
		call2010d();
	}

protected void call2010d()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currIn.set(chdrpf.getCntcurr());
		conlinkrec.currOut.set(sv.paycurr);
		conlinkrec.amountIn.set(sv.instprem);
		conlinkrec.company.set(wsspcomn.company);
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplcPojo.setAmountIn(conlinkrec.amountOut.getbigdata());//ILIFE-8786
		b000CallRounding();
		conlinkrec.amountOut.set(zrdecplcPojo.getAmountOut());//ILIFE-8786
		wsaaTotalPremium.add(conlinkrec.amountOut);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
					setUpUtrn3020();
				case utrnLoop3030: 
					utrnLoop3030();
				case continue3080: 
					continue3080();
				case processTax3050: 
					processTax3050();
				case nonInvestTax3060: 
					nonInvestTax3060();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		wsaaTotNonInvest.set(ZERO);
		/* If CF12 is pressed delete any records already created.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			delUtrn3100();
			a200DelHitr();
			delPcdt3300();
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| utrnFound.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(covrpf.getChdrnum(), SPACES)) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.tranrate.set(sv.instprem);
		wssplife.effdate.set(sv.effdate);
		wsspcomn.billcurr.set(sv.paycurr);
		/* If no PCDT record is to be created then the existing agent is*/
		/* to receive commission, therefore set up current details in*/
		/* PCDT to make AT processing uniform throughout.*/
		pcdtmjaIO.setFunction(varcom.begn);
		
		//performance improvement -- < Niharika Modi >
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
        
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrpf.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isNE(covrpf.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			defaultPcdt7000();
		}
		else {
			if (isNE(sv.comind, "X")) {
				updatePcdt7100();
			}
		}
		delUtrn3100();
		a200DelHitr();
		writeReservePricing();
	}

protected void setUpUtrn3020()
	{
		index1.set(ZERO);
//ILIFE-8786 start		
		if (utrnWritten.isTrue()) {
			function.set('U');
		}else {
			function.set('I');
		}
		
		if (hitrWritten.isTrue()) {
			function.set('U');
		}else {
			 function.set('I');
		}
//ILIFE-8786 end
	}

	/**
	* <pre>
	**** MOVE 'Y'                       TO WSAA-UTRN-WRITTEN. <INTBR> 
	* </pre>
	*/
protected void utrnLoop3030()
	{
		index1.add(1);
		if (isGT(index1, 10)) {
			/*        GO TO 3090-EXIT                                          */
			if (isNE(sv.comind, "X")) {
				goTo(GotoLabel.processTax3050);
			}
			else {
				goTo(GotoLabel.exit3090);
			}
		}
		if (isEQ(sv.unitVirtualFund[index1.toInt()], SPACES)) {
			/*        GO TO 3090-EXIT                                          */
			if (isNE(sv.comind, "X")) {
				goTo(GotoLabel.processTax3050);
			}
			else {
				goTo(GotoLabel.exit3090);
			}
		}
		readT5537T55365900();
		// VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models starts
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VALCL")  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable())) 
		{			
			Untallrec untallrec = new Untallrec();
			untallrec.untaCompany.set(wsspcomn.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(chdrpf.getOccdate());
			untallrec.untaBillfreq.set("00");	
			callProgram("VALCL", untallrec.untallrec,chdrpf);
			wsaaPcMaxPeriods.set(untallrec.untaMaxPeriod01);	
			wsaaPcInitUnits.set(untallrec.untaPcInitUnit01);
			wsaaPcUnits.set(untallrec.untaPcUnit01);
		}
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models end
			
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.exit3090);
		}
		//readT66475800();
		if (isNE(t6647rec.unitStatMethod, SPACES)) {
			readT66598500();
		}
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[index1.toInt()]);
		readT55156300();
		//readT55456500();
		// VPMS Externalization code start
		//ILIFE-2330 Start
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal("VALCL") || er.isCallExternal("VEUAL"))  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable()))) 
		{
			readT55456500();
		}
		else
		{			
			Untallrec untallrec = new Untallrec();
			untallrec.untaCompany.set(wsspcomn.company);
			untallrec.untaBatctrcde.set(wsaaBatckey.batcBatctrcde);
			untallrec.untaEnhall.set(t6647rec.enhall);
			untallrec.untaCntcurr.set(chdrpf.getCntcurr());
			untallrec.untaCalcType.set("S");
			untallrec.untaEffdate.set(chdrpf.getOccdate());
			untallrec.untaInstprem.set(sv.instprem);
			untallrec.untaAllocInit.set(sv.instprem);
			untallrec.untaAllocAccum.set(ZERO);
			untallrec.untaBillfreq.set("00");		

			//callProgram("VEUAL", untallrec.untallrec);
			callProgram("VEUAL", untallrec.untallrec,chdrpf);
			
			wsaaEnhanceAlloc.set(untallrec.untaEnhanceAlloc);	
			wsaaPerc.set(untallrec.untaEnhancePerc);
		}
		//ILIFE-2330 End
		/* Set WSAA-INV-SINGP to the premium entered for the COVR.*/
		if (isEQ(sv.percentAmountInd, "P")) {
			wsaaSinglePrem.set(sv.instprem);
			wsaaInvSingp.set(sv.instprem);
		}
		else {
			wsaaSinglePrem.set(sv.unitAllocPercAmt[index1.toInt()]);
			wsaaInvSingp.set(sv.unitAllocPercAmt[index1.toInt()]);
		}
		/* ENHANCE THE PREMIUM AMOUNT BEFORE APPORTIONING TO FUND TYPE.*/
		compute(wsaaSinglePrem, 3).setRounded(div(mult(wsaaSinglePrem, wsaaPerc), 100));
		/* Derive the Enhanced Amount                                   */
		//compute(wsaaEnhanceAlloc, 3).setRounded(sub(wsaaSinglePrem, wsaaInvSingp));
		//IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible START
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VEUAL")  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable()))) 
		{
			compute(wsaaEnhanceAlloc, 3).setRounded(sub(wsaaSinglePrem, wsaaInvSingp));
		}
		
		//IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible END
//		MIBT-129
		if(isLT(wsaaEnhanceAlloc,ZERO)){             // MTL002
			wsaaInvSingp.set(wsaaSinglePrem);        // MTL002
		}       //MTL002
		// if (isEQ(wsaaEnhanceAlloc,ZERO)) { MTL002
		if (isEQ(wsaaEnhanceAlloc,ZERO) ||  //MTL002
		    isLT(wsaaEnhanceAlloc,ZERO)) {	//MTL002
			goTo(GotoLabel.continue3080);
		}
//		MIBT-129 ENDS
		/* Derive the non-invested Enhanced Amount                      */
		compute(wsaaEnhanceAlloc, 3).setRounded(mult((sub(wsaaSinglePrem, wsaaInvSingp)), (sub(1, div(wsaaPcUnits, 100)))));
		/* Generate UTRN for the non-invested Enhanced Amount           */
		if (isGT(wsaaEnhanceAlloc, ZERO)) {
			wsaaEnhancedAllocInd.set("N");
			wsaaUnitType.set(SPACES);
			wsaaUtrnSingp.set(wsaaEnhanceAlloc);
			if (isNE(t5515rec.zfundtyp, "D")) {
				movesUtrn6000();
			}
			else {
				wsaaHitrSingp.set(wsaaEnhanceAlloc);
				a300MovesHitr();
			}
		}
		/* Derive the invested Enhanced Amount                          */
		compute(wsaaEnhanceAlloc, 3).setRounded(div(mult((sub(wsaaSinglePrem, wsaaInvSingp)), wsaaPcUnits), 100));
		/* Generate UTRN for the invested Enhanced Amount               */
		if (isGT(wsaaEnhanceAlloc, ZERO)) {
			wsaaEnhancedAllocInd.set("Y");
			wsaaUnitType.set(SPACES);
			wsaaUtrnSingp.set(wsaaEnhanceAlloc);
			if (isNE(t5515rec.zfundtyp, "D")) {
				movesUtrn6000();
			}
			else {
				wsaaHitrSingp.set(wsaaEnhanceAlloc);
				a300MovesHitr();
			}
		}
		wsaaEnhancedAllocInd.set("N");
	}

protected void continue3080()
	{
		/* If UNITS not 100% then adjust WSAA-INV-SINGP and create the*/
		/* uninvested UTRN.*/
		/* Calculate the non-invested amount                            */
		compute(wsaaUtrnSingp, 2).set(sub(wsaaInvSingp, (div((mult(wsaaInvSingp, wsaaPcUnits)), 100))));
		wsaaTotNonInvest.set(wsaaUtrnSingp);
		/* COMPUTE WSAA-UTRN-SINGP =  WSAA-INV-SINGP -                  */
		/*      (( WSAA-SINGLE-PREM * WSAA-PC-UNITS) / 100).            */
		/*Instead of posting the enhanced allocation amount as             */
		/*Non-investment, post it as an actual enhanced allocation amount  */
		/*whenever there is an enhanced allocation amount                  */
		/*  MOVE ZEROES                 TO WSAA-ENHANCE-ALLOC.   <MLS006>*/
//		MIBT-129 STARTS
		if (isLT(wsaaEnhanceAlloc, ZERO)) {  //MTL002
			wsaaTotNonInvest.set(0);     //MTL002
			compute(wsaaUtrnSingp, 2).set(mult(wsaaEnhanceAlloc,-1));  //MTL002
			wsaaTotNonInvest.set(wsaaUtrnSingp); //MTL002
		} //MTL002
//		MIBT-129	
		
		if (isNE(wsaaUtrnSingp, ZERO)) {
			/*   IF WSAA-UTRN-SINGP         >  ZERO                  <MLS006>*/
			/*      MOVE SPACES             TO WSAA-UNIT-TYPE        <MLS006>*/
			/*      IF T5515-ZFUNDTYP         NOT = 'D'              <MLS006>*/
			/*         PERFORM 6000-MOVES-UTRN                       <MLS006>*/
			/*      ELSE                                             <MLS006>*/
			/*         COMPUTE WSAA-HITR-SINGP  = WSAA-UTRN-SINGP    <MLS006>*/
			/*         PERFORM A300-MOVES-HITR                       <MLS006>*/
			/*      END-IF                                           <MLS006>*/
			/*   ELSE                                                <MLS006>*/
			if (isNE(t5515rec.zfundtyp, "D")) {
				wsaaUnitType.set(SPACES);
				/*      COMPUTE WSAA-UTRN-SINGP  = WSAA-UTRN-SINGP * -1  <MLS006>*/
				/*      MOVE WSAA-UTRN-SINGP    TO WSAA-ENHANCE-ALLOC    <MLS006>*/
				/*      MOVE 'Y'                TO WSAA-ENHANCED-ALLOC-IND       */
				movesUtrn6000();
				/****      MOVE SPACES             TO WSAA-ENHANCED-ALLOC-IND       */
			}
			else {
				wsaaUnitType.set(SPACES);
				wsaaHitrSingp.set(wsaaUtrnSingp);
				/*      COMPUTE WSAA-HITR-SINGP  = WSAA-UTRN-SINGP * -1  <MLS006>*/
				/*      MOVE WSAA-HITR-SINGP    TO WSAA-ENHANCE-ALLOC    <MLS006>*/
				/*      MOVE 'Y'                TO WSAA-ENHANCED-ALLOC-IND       */
				a300MovesHitr();
				/****      MOVE SPACES             TO WSAA-ENHANCED-ALLOC-IND       */
			}
			/*****  END-IF                                              <MLS006>*/
		}
		/* COMPUTE WSAA-INV-SINGP = WSAA-SINGLE-PREM *                  */
		/*                          WSAA-PC-UNITS / 100.                */
		/*                                                      <MLS006>*/
		/* Derive the invested amount                           <MLS006>*/
		/*                                                      <MLS006>*/
		compute(wsaaInvSingp, 2).set(div(mult(wsaaInvSingp, wsaaPcUnits), 100));
		/* Not to include the enhanced allocation amount into Single       */
		/* Premium Top-Up amount                                           */
		/* IF WSAA-ENHANCE-ALLOC        > 0                     <MLS006>*/
		/*    SUBTRACT WSAA-ENHANCE-ALLOC FROM WSAA-INV-SINGP   <MLS006>*/
		/* END-IF.                                              <MLS006>*/
		if (isEQ(t5515rec.zfundtyp, "D")) {
			wsaaUnitType.set("D");
			wsaaHitrSingp.set(wsaaInvSingp);
			a300MovesHitr();
			goTo(GotoLabel.utrnLoop3030);
		}
		/* Create UTRN for Initial*/
		if (isNE(wsaaPcInitUnits, ZERO)) {
			compute(wsaaUtrnSingp, 2).set(div(mult(wsaaInvSingp, wsaaPcInitUnits), 100));
			wsaaUnitType.set("I");
			movesUtrn6000();
		}
		/* If Initial Units not 100% then create Accumulation UTRN.*/
		if (isNE(wsaaPcInitUnits, 100)) {
			compute(wsaaUtrnSingp, 2).set(div(mult(wsaaInvSingp, (sub(100, wsaaPcInitUnits))), 100));
			wsaaUnitType.set("A");
			movesUtrn6000();
		}
		goTo(GotoLabel.utrnLoop3030);
	}

protected void processTax3050()
	{
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit3090);
		}
		/*  TXCL variable should hold the top up premium tax values.       */
		/*  We need to store the tax details for later posting.            */
		if (isEQ(txcalcrec.taxAmt[1], ZERO)
		&& isEQ(txcalcrec.taxAmt[2], ZERO)) {
			goTo(GotoLabel.nonInvestTax3060);
		}
		/* Create TAXD record                                              */
		//ILIFE-8786 start
		taxdpf = new Taxdpf();
		wsaaTranref.set(taxdpf.getTranref());
		taxdpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		taxdpf.setChdrnum(chdrpf.getChdrnum());
		taxdpf.setTrantype(txcalcrec.transType.toString());
		taxdpf.setLife(covrpf.getLife());
		taxdpf.setCoverage(covrpf.getCoverage());
		taxdpf.setRider(covrpf.getRider());
		taxdpf.setPlansfx(covrpf.getPlanSuffix());
		taxdpf.setEffdate(sv.effdate.toInt());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		int i = 1;
		taxdpf.setInstfrom(sv.effdate.toInt());
		taxdpf.setInstto(covrpf.getPremCessDate());
		taxdpf.setBillcd(sv.effdate.toInt());
		setPrecision(taxdpf.getTranno(), 0);
		taxdpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		taxdpf.setBaseamt(txcalcrec.amountIn.getbigdata());
		taxdpf.setTaxamt01(txcalcrec.taxAmt[i].getbigdata());
		taxdpf.setTxabsind01(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype01(txcalcrec.taxType[i].toString());
		++i;
		taxdpf.setTaxamt02(txcalcrec.taxAmt[i].getbigdata());
		taxdpf.setTxabsind02(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype02(txcalcrec.taxType[i].toString());
		taxdpf.setTaxamt03(BigDecimal.ZERO);
		taxdpf.setTxabsind03(SPACES.stringValue());
		taxdpf.setTxtype03(SPACES.stringValue());
		taxdpf.setPostflg(SPACES.stringValue());
		taxdInsertList = new ArrayList<Taxdpf>();
		taxdInsertList.add(taxdpf);
		taxdpfDAO.insertTaxdPF(taxdInsertList);
		//ILIFE-8786 end
	}

protected void nonInvestTax3060()
	{
		if (isEQ(wsaaTotNonInvest, ZERO)) {
			return ;
		}
		wsaaTaxableAmt.set(wsaaTotNonInvest);
		wsaaTaxTranType = "NINV";
		a500CalcTax();
		if (isEQ(txcalcrec.taxAmt[1], ZERO)
		&& isEQ(txcalcrec.taxAmt[2], ZERO)) {
			return ;
		}
		/* Create TAXD record                                              */
		//ILIFE-8786 start
		taxdpf = new Taxdpf();
		wsaaTranref.set(taxdpf.getTranref());
		taxdpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		taxdpf.setChdrnum(chdrpf.getChdrnum());
		taxdpf.setLife(covrpf.getLife());
		taxdpf.setCoverage(covrpf.getCoverage());
		taxdpf.setRider(covrpf.getRider());
		taxdpf.setPlansfx(covrpf.getPlanSuffix());
		taxdpf.setEffdate(sv.effdate.toInt());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdpf.setInstfrom(sv.effdate.toInt());
		taxdpf.setInstto(covrpf.getPremCessDate());
		taxdpf.setBillcd(sv.effdate.toInt());
		setPrecision(taxdpf.getTranno(), 0);
		taxdpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		taxdpf.setTrantype(txcalcrec.transType.toString());
		taxdpf.setBaseamt(txcalcrec.amountIn.getbigdata());
		int i = 1;
		taxdpf.setTaxamt01(txcalcrec.taxAmt[i].getbigdata());
		taxdpf.setTxabsind01(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype01(txcalcrec.taxType[i].toString());
		++i;
		taxdpf.setTaxamt02(txcalcrec.taxAmt[i].getbigdata());
		taxdpf.setTxabsind02(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype02(txcalcrec.taxType[i].toString());
		taxdpf.setTaxamt03(BigDecimal.ZERO);
		taxdpf.setTxabsind03(SPACES.stringValue());
		taxdpf.setTxtype03(SPACES.stringValue());
		taxdpf.setPostflg(SPACES.stringValue());
		taxdInsertList = new ArrayList<Taxdpf>();
		taxdInsertList.add(taxdpf);
		taxdpfDAO.insertTaxdPF(taxdInsertList);
		//ILIFE-8786 end
	}

protected void delUtrn3100()
	{
		//ILIFE-8786 start
		utrnpf = new Utrnpf();
		if(!utrnpfList.isEmpty()) {
			for(Utrnpf utrn:utrnpfList) {
				compute(wssplife.bigAmt, 2).set(sub(wssplife.bigAmt, utrn.getContractAmount()));
			}
			utrnpfDAO.deleteUtrnRecord(utrnpfList);
		}
	}
		//ILIFE-8786 end
protected void delPcdt3300()
	{
		para3310();
	}

protected void para3310()
	{
		pcdtmjaIO.setFunction(varcom.begn);
		
		//performance improvement -- < Niharika Modi >
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","PLNSFX","RIDER");
		
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isNE(covrpf.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())
		|| isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			pcdtmjaIO.setStatuz(varcom.endp);
			return ;
		}
		pcdtmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		pcdtmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4700();
			return ;
		}
		wsspcomn.nextprog.set(wsaaProg);
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (isEQ(sv.comind, "X")) {
			optionsExe4100();
		}
		else {
			if (isEQ(sv.comind, "?")) {
				optionsRet4300();
			}
			else {
				if (wsaaWholePlan.isTrue()) {
					//covrmjaIO.setFunction(varcom.nextr);
					clearScreen4800();
					policyLoad5000();
					if (covrpf != null) {
						wsspcomn.nextprog.set(wsaaProg);
						wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
						wayout4700();
					}
					else {
						wsspcomn.nextprog.set(scrnparams.scrname);
						wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
					}
				}
				else {
					wsspcomn.nextprog.set(wsaaProg);
					wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
					wayout4700();
				}
			}
		}
	}

protected void optionsExe4100()
	{
		try {
			para4100();
			endsave4120();
			endload4140();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para4100()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		covrpfDAO.setCacheObject(covrpf);
		/*covrmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		sv.comind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			save4200();
		}
	}

protected void endsave4120()
	{
		/*  Call GENSSWCH with and  action  of 'A' to retrieve the*/
		/*  program switching required,  and  move  them to the stack.*/
		gensswrec.function.set("A");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4190);
		}
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			load4250();
		}
	}

protected void endload4140()
	{
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4250()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4300()
	{
		para4300();
		endRestore4310();
	}

protected void para4300()
	{
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the options/extras indicator will be '?'. To handle*/
		/* the return from options and extras:*/
		/* Restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			restore4400();
		}
	}

protected void endRestore4310()
	{
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		if (isEQ(sv.comind, "?")) {
			sv.comind.set("+");
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		utrncfiIO.setParams(SPACES);
		utrncfiIO.setChdrcoy(covrpf.getChdrcoy());
		utrncfiIO.setChdrnum(covrpf.getChdrnum());
		utrncfiIO.setLife(covrpf.getLife());
		utrncfiIO.setCoverage(covrpf.getCoverage());
		utrncfiIO.setRider(covrpf.getRider());
		utrncfiIO.setPlanSuffix(covrpf.getPlanSuffix());
		wsaaTranno.set(ZERO);
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(), 1));
		utrncfiIO.setTranno(wsaaTranno);
		utrncfiIO.setFunction(varcom.begn);
		
		//performance improvement -- Niharika
		utrncfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		while ( !(isEQ(utrncfiIO.getStatuz(),varcom.endp))) {
			checkUtrn6200();
		}
		
	}

protected void restore4400()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		/*PARA*/
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/* If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/* spaces to  the current program entry in the program*/
		/* stack and exit.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void clearScreen4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case init4850: 
					init4850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		//ILIFE-8786 start
		acblpf = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode03.toString(), chdrpf.getChdrnum(), t5645rec.sacstype03.toString(), chdrpf.getCntcurr()); 
		if(acblpf == null) {
			wsaaChdrSuspense.set(ZERO);
		}else {
			compute(wsaaChdrSuspense, 2).set(mult(acblpf.getSacscurbal(), -1));
		}
		//ILIFE-8786 end
		compute(sv.susamt, 2).set(sub(wsaaChdrSuspense, wssplife.bigAmt));
		/* MOVE WSSP-BIG-AMT           TO S5144-TOTPREM                 */
		sv.totlprem.set(wssplife.bigAmt);
		sv.instprem.set(ZERO);
		sv.effdate.set(ZERO);
		sub1.set(ZERO);
		x.set(ZERO);
	}

protected void init4850()
	{
		x.add(1);
		if (isGT(x, 10)) {
			x.set(ZERO);
			return ;
		}
		sv.currcy[x.toInt()].set(wsaaCurrcy[x.toInt()]);
		sv.unitVirtualFund[x.toInt()].set(wsaaUnitVirtualFund[x.toInt()]);
		sv.unitAllocPercAmt[x.toInt()].set(wsaaUnitAllPercAmt[x.toInt()]);
		goTo(GotoLabel.init4850);
	}

protected void policyLoad5000()
	{
		/*LOAD-POLICY*/
		if (wsaaWholePlan.isTrue()) {
			readCovr5400();
			for (Covrpf covr : covrpfList) {
				if (covr == null) {
					return ;
				}
				else
					{
						covrpf.setPlanSuffix(covr.getPlanSuffix());//ILIFE-8231
					}
			}
		}
		/* The header is set to show which selection is being actioned.*/
		if (wsaaWholePlan.isTrue()) {
			if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
				sv.planSuffix.set(chdrpf.getPolsum());
				sv.plnsfxOut[varcom.hi.toInt()].set("Y");
			}
			else {
				sv.planSuffix.set(covrpf.getPlanSuffix());
			}
		}
		/*EXIT*/
	}

protected void readItem5100()
	{
		try {
			para5100();
			gotIt5120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para5100()
	{
		/* Gets Fund Split Plan from T5510.*/
		/* If Fund Option = spaces, zeroise all numeric fields*/
		/* for use in Standard Split check later, and that's all!*/
		if (isEQ(wsaaFndopt, SPACES)) {
			for (x.set(1); !(isGT(x, 10)); x.add(1)){
				zeroise5200();
			}
			goTo(GotoLabel.exit5190);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5510);
		itdmIO.setItemitem(wsaaFndsplItem);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5510))
		|| (isNE(itdmIO.getItemitem(), wsaaFndsplItem))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			sv.fndsplErr.set(errorsInner.g103);
			goTo(GotoLabel.exit5190);
		}
		else {
			t5510rec.t5510Rec.set(itdmIO.getGenarea());
		}
	}

protected void gotIt5120()
	{
		for (x.set(1); !(isGT(x, 10)); x.add(1)){
			move5300();
		}
	}

protected void zeroise5200()
	{
		/*PARA*/
		t5510rec.unitPremPercent[x.toInt()].set(ZERO);
		t5510rec.unitVirtualFund[x.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void move5300()
	{
		para5310();
	}

protected void para5310()
	{
		/* The default value should override any other values entered.*/
		sv.unitVirtualFund[x.toInt()].set(t5510rec.unitVirtualFund[x.toInt()]);
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[x.toInt()]);
		/* Only read T5515 if unit virtual fund is not equal to spaces*/
		/* (also move the control subscript ('X') to the subscript*/
		/* which is used in the read of T5515 (SUB1)).*/
		if (isNE(wsaaT5515Item, SPACES)) {
			readT55156300();
			if (isEQ(wsspcomn.edterror, "Y")) {
				sv.currcy[x.toInt()].set(SPACES);
			}
			else {
				sv.currcy[x.toInt()].set(t5515rec.currcode);
			}
		}
		else {
			sv.currcy[x.toInt()].set(SPACES);
		}
		if (isNE(sv.percentAmountInd, "A")) {
			sv.unitAllocPercAmt[x.toInt()].set(t5510rec.unitPremPercent[x.toInt()]);
		}
		if (isEQ(sv.unitVirtualFund[x.toInt()], SPACES)) {
			sv.unitAllocPercAmt[x.toInt()].set(ZERO);
		}
	}

protected void readCovr5400()
	{
		/*READ-COVRMJA*/
	//ILIFE-8137
			covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
			for (Covrpf covr:covrpfList) {
				if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
						|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
						|| isNE(covr.getLife(),wsaaCovrLife)
						|| isNE(covr.getCoverage(),wsaaCovrCoverage)
						|| isNE(covr.getRider(),wsaaCovrRider)) {
							return;
						}
			}
		/*SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), wsaaCovrChdrcoy)
		|| isNE(covrmjaIO.getChdrnum(), wsaaCovrChdrnum)
		|| isNE(covrmjaIO.getLife(), wsaaCovrLife)
		|| isNE(covrmjaIO.getCoverage(), wsaaCovrCoverage)
		|| isNE(covrmjaIO.getRider(), wsaaCovrRider)
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
	}

protected void editFundSplit5500()
	{
		para5510();
		checkTotal5530();
	}

protected void para5510()
	{
		x.set(ZERO);
		wsaaTotalPerc.set(ZERO);
		/*LOOP*/
		for (x.set(1); !(isGT(x, 10)); x.add(1)){
			checkUalprc5600();
		}
	}

protected void checkTotal5530()
	{
		if (isEQ(sv.percentAmountInd, "P")
		&& isNE(wsaaTotalPerc, 100)) {
			sv.ualprcErr[1].set(errorsInner.e631);
			for (x.set(2); !(isGT(x, 10)); x.add(1)){
				ualprcErrs5700();
			}
			return ;
		}
		if (isEQ(sv.percentAmountInd, "A")) {
			if (isEQ(sv.instprem, ZERO)) {
				return ;
			}
			else {
				if (isNE(wsaaTotalPerc, sv.instprem)) {
					sv.ualprcErr[1].set(errorsInner.g106);
					for (x.set(2); !(isGT(x, 10)); x.add(1)){
						ualprcErrs5700();
					}
				}
			}
		}
		/*EXIT*/
	}

protected void checkUalprc5600()
	{
		/*PARA*/
		if (isEQ(sv.unitVirtualFund[x.toInt()], SPACES)) {
			if (isNE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
				sv.ualprcErr[x.toInt()].set(errorsInner.g105);
				return ;
			}
			else {
				return ;
			}
		}
		else {
			if (isLTE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
				sv.ualprcErr[x.toInt()].set(errorsInner.f351);
			}
		}
		if (isEQ(sv.percentAmountInd, "P")
		&& isGT(sv.unitAllocPercAmt[x.toInt()], 100)) {
			sv.ualprcErr[x.toInt()].set(errorsInner.f348);
		}
		wsaaTotalPerc.add(sv.unitAllocPercAmt[x.toInt()]);
		/*EXIT*/
	}

protected void ualprcErrs5700()
	{
		/*PARA*/
		if (isNE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
			sv.ualprcErr[x.toInt()].set(sv.ualprcErr[1]);
		}
		/*EXIT*/
	}

protected void readT66475800()
	{
		para5800();
	}

protected void para5800()
	{
		//performance improvement -- < Niharika Modi >
		
		wsaaT6647Trcde.set(wsaaBatckey.batcBatctrcde);
		wsaaT6647Cnttype.set(chdrpf.getCnttype());
		
		itempf6647 = itemDAO.findItemByItem(wsspcomn.company.toString(), tablesInner.t6647.toString(), wsaaT6647Item.toString());
		if(null  ==  itempf6647 || itempf6647.getItemitem().equals("")){
			syserrrec.params.set(wsspcomn.company.toString()+ tablesInner.t6647.toString()+wsaaT6647Item.toString());
			fatalError600();
		}
		else {
			t6647rec.t6647Rec.set(StringUtil.rawToString(itempf6647.getGenarea()));
		}
	}

protected void readT5537T55365900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para5900: 
					para5900();
				case gotT55375920: 
					gotT55375920();
				case yOrdinate5940: 
					yOrdinate5940();
				case increment5960: 
					increment5960();
				case xOrdinate5980: 
					xOrdinate5980();
				case increment5985: 
					increment5985();
				case n00Loop5950: 
					n00Loop5950();
				case exit5990: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5900()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5537);
		wsaaT5537Item.set(SPACES);
		wsaaT5537Allbas.set(t5540rec.allbas);
		wsaaT5537Sex.set(wsaaLifeassurSex);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaT5537Trans.set(wsaaBatckey.batcBatctrcde);
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/*   If NO record found then try first with the basis, * for*/
		/*   Sex and Transaction code.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT5537Sex.set("*");
			itemIO.setItemitem(wsaaT5537Item);
			SmartFileCode.execute(appVars, itemIO);
			if ((isNE(itemIO.getStatuz(), varcom.oK))
			&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
				syserrrec.params.set(itemIO.getParams());
				itemIO.setStatuz(itemIO.getStatuz());
				fatalError600();
			}
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/*    If NO record found then try secondly with the basis, Sex*/
		/*    and '****'  Transaction code.*/
		wsaaT5537Sex.set(wsaaLifeassurSex);
		wsaaT5537Trans.fill("*");
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			itemIO.setStatuz(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/*    If NO record found then try first with the basis, * for*/
		/*    Sex and **** for Transaction code.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT5537Sex.set("*");
			wsaaT5537Trans.fill("*");
		}
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaT5537Trans.fill("*");
		/* If no match is found, try again with the global sex code*/
		/*    When doing a global change and the global data is being*/
		/*    moved to the WSAA-T5537-key make sure that you move the*/
		/*    WSAA-T5537-ITEM to the ITEM-ITEMITEM.*/
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/* And again if no match is found to prevent system falling over*/
		/* read T5537 with global key.*/
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void gotT55375920()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*  Always calculate the age.*/
		calculateAge6700();
	}

	/**
	* <pre>
	*  Work out the array's Y index based on ANB-AT-CCD.
	* </pre>
	*/
protected void yOrdinate5940()
	{
		wsaaZ.set(0);
	}

protected void increment5960()
	{
		wsaaZ.add(1);
		/* Check the continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 10)
		&& isNE(t5537rec.agecont, SPACES)) {
			readAgecont6900();
			goTo(GotoLabel.yOrdinate5940);
		}
		else {
			if (isGT(wsaaZ, 10)
			&& isEQ(t5537rec.agecont, SPACES)) {
				sv.effdateErr.set(errorsInner.e706);
				wsspcomn.nextprog.set(scrnparams.scrname);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit5990);
			}
		}
		/*    IF  WSAA-CLTAGE             >  T5537-TOAGE(WSAA-Z) AND       */
		if (isGT(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])
		&& isLTE(wsaaZ, 10)) {
			goTo(GotoLabel.increment5960);
		}
		/*    IF  WSAA-CLTAGE             <  T5537-TOAGE(WSAA-Z) OR        */
		/*                                =  T5537-TOAGE(WSAA-Z)           */
		if (isLT(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])
		|| isEQ(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])) {
			wsaaY.set(wsaaZ);
		}
		else {
			wsaaT5537Item.set(t5537rec.agecont);
			goTo(GotoLabel.para5900);
		}
	}

	/**
	* <pre>
	*  Work out the array's X index based on TERM.
	* </pre>
	*/
protected void xOrdinate5980()
	{
		wsaaZ.set(0);
	}

protected void increment5985()
	{
		wsaaZ.add(1);
		/* Check the term continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 9)
		&& isNE(t5537rec.trmcont, SPACES)) {
			readTermcont6950();
			goTo(GotoLabel.xOrdinate5980);
		}
		else {
			if (isGT(wsaaZ, 9)
			&& isEQ(t5537rec.trmcont, SPACES)) {
				sv.effdateErr.set(errorsInner.e707);
				wsspcomn.nextprog.set(scrnparams.scrname);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit5990);
			}
		}
		/*    Always calculate the term.*/
		calculateTerm6800();
		wsaaTerm.set(wsaaRiskCessTerm);
		if (isGT(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])
		&& isLT(wsaaZ, 10)) {
			goTo(GotoLabel.increment5985);
		}
		if (isLT(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])
		|| isEQ(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])) {
			wsaaX.set(wsaaZ);
		}
		else {
			wsaaT5537Item.set(t5537rec.trmcont);
			goTo(GotoLabel.para5900);
		}
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY, 9));
		wsaaIndex.add(wsaaX);
		wsaaAllocBasis.set(t5537rec.actAllocBasis[wsaaIndex.toInt()]);
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal("VALCL") || er.isCallExternal("VEUAL"))  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable())))
		{		
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5536);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5536Item.set(SPACES);
		wsaaT5536Allbas.set(wsaaAllocBasis);
		itdmIO.setItemitem(wsaaT5536Item);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5536))
		|| (isNE(itdmIO.getItemitem(), wsaaT5536Item))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
		sub1.set(ZERO);
		}
		//VPMS externalization code end
	}

protected void n00Loop5950()
	{
	//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal("VALCL") || er.isCallExternal("VEUAL"))  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable())))
	{
		sub1.add(1);
		if (isGT(sub1, 6)) {
			scrnparams.errorCode.set(errorsInner.t090);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(t5536rec.billfreq[sub1.toInt()], "00")) {
			goTo(GotoLabel.n00Loop5950);
		}
		if (isEQ(sub1, 1)) {
			wsaaPcUnits.set(t5536rec.pcUnitsa[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsa[1]);
		}
		if (isEQ(sub1, 2)) {
			wsaaPcUnits.set(t5536rec.pcUnitsb[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsb[1]);
		}
		if (isEQ(sub1, 3)) {
			wsaaPcUnits.set(t5536rec.pcUnitsc[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsc[1]);
		}
		if (isEQ(sub1, 4)) {
			wsaaPcUnits.set(t5536rec.pcUnitsd[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsd[1]);
		}
		if (isEQ(sub1, 5)) {
			wsaaPcUnits.set(t5536rec.pcUnitse[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitse[1]);
		}
		if (isEQ(sub1, 6)) {
			wsaaPcUnits.set(t5536rec.pcUnitsf[1]);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsf[1]);
		}
	}
	//VPMS externalization code end
	}

protected void movesUtrn6000()
	{
	
	if (isEQ(sv.comind, "X")) {
		return;
	}
		moveValues6000();
	}

protected void moveValues6000()
	{
		//ILIFE-8786 start
		wsaaUtrnWritten.set("Y");
		Utrnpf utrnIO = new Utrnpf();
		utrnIO.setChdrcoy(chdrpf.getChdrcoy().toString());
		utrnIO.setChdrnum(chdrpf.getChdrnum());
		utrnIO.setLife(covrpf.getLife());
		utrnIO.setCoverage(covrpf.getCoverage());
		/* MOVE COVRMJA-RIDER          TO UTRN-RIDER.                   */
		utrnIO.setRider("00");
		setPrecision(utrnIO.getTranno(), 0);
		utrnIO.setTranno(add(chdrpf.getTranno(), 1).toInt());
		utrnIO.setTermid(varcom.vrcmTermid.toString());
		utrnIO.setTransactionDate(varcom.vrcmDate.toInt());
		utrnIO.setTransactionTime(varcom.vrcmTime.toInt());
		utrnIO.setUser(varcom.vrcmUser.toInt());
		utrnIO.setBatccoy(wsspcomn.company.toString());
		utrnIO.setBatcbrn(wsspcomn.branch.toString());
		wsaaBatckey.set(wsspcomn.batchkey);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		readT66475800();//IBPLIFE-3896
		utrnIO.setNowDeferInd(t6647rec.aloind.toString());
		/* MOVE T6647-PROC-SEQ-NO      TO UTRN-PROC-SEQ-NO.             */
		utrnIO.setProcSeqNo(wsaaProcSeqno.toInt());
		if (isEQ(t6647rec.efdcode, "BD")) {
			utrnIO.setMoniesDate(wsaaBusinessDate.toLong());
		}
		else {
			if (isEQ(t6647rec.efdcode, "DD")) {
				utrnIO.setMoniesDate(sv.effdate.toLong());
			}
			else {
				if (isEQ(t6647rec.efdcode, "RD")) {
					utrnIO.setMoniesDate(wsaaBusinessDate.toLong());
				}
				else {
					if (isEQ(t6647rec.efdcode, "LO")) {
						utrnIO.setMoniesDate(sv.effdate.toLong());
					}
					else {
						utrnIO.setMoniesDate(Long.valueOf(0));
					}
				}
			}
		}

		if (isEQ(t6647rec.efdcode, "LO")
		&& isGT(wsaaBusinessDate, sv.effdate)) {
			utrnIO.setMoniesDate(wsaaBusinessDate.toLong());
		}
		// ILIFE-5446 Start
				 if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("Y")&&
						 (isGT(sv.reserveUnitsDate,0)  || (!sv.reserveUnitsDate.toString().trim().equals(varcom.vrcmMaxDate)))){ 
					 utrnIO.setMoniesDate(sv.reserveUnitsDate.toLong()); 
					 utrnIO.setNowDeferInd("N"); 
				 }
				// ILIFE-5446 End
		utrnIO.setCrtable(covrpf.getCrtable());
		utrnIO.setPlanSuffix(covrpf.getPlanSuffix());
		utrnIO.setCntcurr(chdrpf.getCntcurr());
		utrnIO.setCnttyp(chdrpf.getCnttype());
		utrnIO.setCrComDate(sv.effdate.toLong());
		utrnIO.setUnitVirtualFund(sv.unitVirtualFund[index1.toInt()].toString());
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[index1.toInt()]);
		utrnIO.setFundCurrency(sv.currcy[index1.toInt()].toString());
		readT56456400();
		/* If UTRN for Initial or Accumulate use the invested account*/
		/* otherwise use the non invested account. (GL, on table T5645)*/
		if (isEQ(wsaaUnitType, "I")
		|| isEQ(wsaaUnitType, "A")
		&& isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[4].toString());
			utrnIO.setSacstyp(t5645rec.sacstype[4].toString());
			utrnIO.setGenlcde(t5645rec.glmap[4].toString());
		}
		if (isEQ(wsaaUnitType, "I")
		|| isEQ(wsaaUnitType, "A")
		&& isNE(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[1].toString());
			utrnIO.setSacstyp(t5645rec.sacstype[1].toString());
			utrnIO.setGenlcde(t5645rec.glmap[1].toString());
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[5].toString());
			utrnIO.setSacstyp(t5645rec.sacstype[5].toString());
			utrnIO.setGenlcde(t5645rec.glmap[5].toString());
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& isNE(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[2].toString());
			utrnIO.setSacstyp(t5645rec.sacstype[2].toString());
			utrnIO.setGenlcde(t5645rec.glmap[2].toString());
		}
		if (isEQ(wsaaEnhancedAllocInd, "Y")) {
			if (isEQ(wsaaUnitType, SPACES)
			&& isEQ(t5688rec.comlvlacc, "Y")) {
				utrnIO.setSacscode(t5645rec.sacscode[7].toString());
				utrnIO.setSacstyp(t5645rec.sacstype[7].toString());
				utrnIO.setGenlcde(t5645rec.glmap[7].toString());
			}
		}
		if (isEQ(wsaaEnhancedAllocInd, "Y")) {
			if (isEQ(wsaaUnitType, SPACES)
			&& isNE(t5688rec.comlvlacc, "Y")) {
				utrnIO.setSacscode(t5645rec.sacscode[6].toString());
				utrnIO.setSacstyp(t5645rec.sacstype[6].toString());
				utrnIO.setGenlcde(t5645rec.glmap[6].toString());
			}
		}
		if (isEQ(wsaaUnitType, "I")) {
			utrnIO.setUnitSubAccount("INIT");
		}
		if (isEQ(wsaaUnitType, "A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& (isNE(wsaaEnhancedAllocInd, "Y"))) {
			utrnIO.setUnitVirtualFund(SPACES.stringValue());
			utrnIO.setUnitSubAccount("NVST");
			/**        MOVE 'NINV'             TO UTRN-UNIT-SUB-ACCOUNT         */
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& (isEQ(wsaaEnhancedAllocInd, "Y"))) {
			utrnIO.setUnitSubAccount("ACUM");
			wsaaUnitType.set("A");
		}
		if (isEQ(sv.percentAmountInd, "P")) {
			setPrecision(utrnIO.getContractAmount(), 3);
			utrnIO.setContractAmount(div(mult(wsaaUtrnSingp, sv.unitAllocPercAmt[index1.toInt()]), 100).getbigdata());
		}
		else {
			utrnIO.setContractAmount(wsaaUtrnSingp.getbigdata());
		}
		compute(wssplife.bigAmt, 2).set(add(wssplife.bigAmt, utrnIO.getContractAmount()));
		utrnIO.setUnitType(wsaaUnitType.toString());
		utrnIO.setInciprm01(BigDecimal.ZERO);
		utrnIO.setInciprm02(BigDecimal.ZERO);
		utrnIO.setNofUnits(BigDecimal.ZERO);
		utrnIO.setNofDunits(BigDecimal.ZERO);
		utrnIO.setFundRate(BigDecimal.ZERO);
		utrnIO.setStrpdate(((long) 0));
		utrnIO.setUstmno(0);
		utrnIO.setPriceDateUsed((long) 0);
		utrnIO.setPriceUsed(BigDecimal.ZERO);
		utrnIO.setUnitBarePrice(BigDecimal.ZERO);
		utrnIO.setInciNum(0);
		utrnIO.setInciPerd01(0);
		utrnIO.setInciPerd02(0);
		utrnIO.setFundAmount(BigDecimal.ZERO);
		utrnIO.setJobnoPrice(BigDecimal.ZERO);
		utrnIO.setDiscountFactor(BigDecimal.ZERO);
		utrnIO.setSurrenderPercent(BigDecimal.ZERO);
		utrnIO.setSvp(BigDecimal.ONE);
		utrnIO.setSwitchIndicator(SPACES.stringValue());
		utrnIO.setFeedbackInd(SPACES.stringValue());
		//ILIFE-1075
		utrnIO.setTriggerModule("ZSPTLETT");
		utrnIO.setTriggerKey(SPACES.stringValue());
		if(isEQ( wsaaTranscd,"T679"))
		{
			utrnIO.setFundPool("2");
		}
		/* MOVE WRITR                  TO UTRN-FUNCTION.                */
		
		utrnList.add(utrnIO);
		
		if(!(utrnList.isEmpty())) {
			if (isEQ(function,'U')) {
				
				utrnpfDAO.updatedUtrnpfRecord(utrnList);
				utrnList.clear();
			}
			else {
				utrnpfDAO.insertUtrnRecoed(utrnList);
				utrnList.clear();
				}
			}
		//ILIFE-8786 end
		
	}

protected void checkUtrn6200()
	{
		/*UTRN*/
		/* Delete UTRN's previously written by this transactiion.*/
		/* This only happens during modify mode.*/
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(), varcom.oK)
		&& isNE(utrncfiIO.getStatuz(), varcom.endp)) {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError600();
		}
		if (isNE(utrncfiIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(utrncfiIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(utrncfiIO.getCoverage(), covrpf.getCoverage())
		|| isNE(utrncfiIO.getLife(), covrpf.getLife())
		|| isNE(utrncfiIO.getRider(), covrpf.getRider())
		|| isNE(utrncfiIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isNE(utrncfiIO.getTranno(), wsaaTranno)
		|| isEQ(utrncfiIO.getStatuz(), varcom.endp)) {
			utrncfiIO.setStatuz(varcom.endp);
			return ;
		}
		utrncfiIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void readT55156300()
	{
		para6300();
	}

protected void para6300()
	{
		wsspcomn.edterror.set(varcom.oK);
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setItemitem(wsaaT5515Item);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5515))
		|| (isNE(itdmIO.getItemitem(), wsaaT5515Item))) {
			sv.vrtfndErr[sub1.toInt()].set(errorsInner.g037);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void readT56456400()
	{
		para6400();
	}

protected void para6400()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5645);
		wsaaT5645Item.set(SPACES);
		wsaaT5645Prog.set(wsaaProg);
		itemIO.setItemitem(wsaaT5645Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT55456500()
	{
		para6500();
	}

protected void para6500()
	{
		if (isEQ(t6647rec.enhall, SPACES)) {
			wsaaPerc.set(100);
			return ;
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5545);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5545Item.set(SPACES);
		wsaaT5545Enhall.set(t6647rec.enhall);
		wsaaT5545Currcode.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5545)
		|| isNE(itdmIO.getItemitem(), wsaaT5545Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t5545rec.t5545Rec.set(itdmIO.getGenarea());
		}
		wsaaPerc.set(0);
		sub1.set(1);
		while ( !((isGT(sub1, 6))
		|| (isNE(wsaaPerc, 0)))) {
			matchEnhanceBasis9000();
		}
		
	}

protected void readTableT55436600()
	{
		start6600();
	}

protected void start6600()
	{
		itemIO.setDataArea(SPACES);
		t5543rec.t5543Rec.set(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5543);
		wsaaT5543Item.set(SPACES);
		wsaaT5543Fund.set(t5551rec.alfnds);
		itemIO.setItemitem(wsaaT5543Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t5543rec.t5543Rec.set(itemIO.getGenarea());
		}
	}

protected void calculateAge6700()
	{
		para6700();
	}

protected void para6700()
	{
		/*  Round up the Age*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(wsaaCltdob);
		agecalcrec.intDate2.set(sv.effdate);
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaCltage.set(agecalcrec.agerating);
	}

protected void calculateTerm6800()
	{
		/*PARA*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(sv.effdate);
		/* MOVE WSAA-CLTDOB            TO DTC3-INT-DATE-2.              */
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		/*EXIT*/
	}

protected void readAgecont6900()
	{
		/*READ-AGECONT*/
		/* Read Age continuation Item.*/
		itemIO.setItemitem(t5537rec.agecont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getDataArea());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaIndex.subtract(99);
		/*EXIT*/
	}

protected void readTermcont6950()
	{
		/*READ-TERMCONT*/
		itemIO.setItemitem(t5537rec.trmcont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void defaultPcdt7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					moveValues7000();
					servagntComm7005();
				case pcddLoop7010: 
					pcddLoop7010();
				case addPcdt7020: 
					addPcdt7020();
				case call7030: 
					call7030();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void moveValues7000()
	{
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		pcdtmjaIO.setInstprem(sv.instprem);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
	}

	/**
	* <pre>
	* When no Commission Details are input the Servicing Agent gets
	* all the Commission.
	* </pre>
	*/
protected void servagntComm7005()
	{
		if (isEQ(sv.comind, SPACES)) {
			wsaaIndex.set(1);
			pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());
			pcdtmjaIO.setSplitc(wsaaIndex, 100);
			wsaaIndex.add(1);
			goTo(GotoLabel.addPcdt7020);
		}
		pcddmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcddmjaIO.setChdrnum(chdrpf.getChdrnum());
		pcddmjaIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		pcddmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcddmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		wsaaIndex.set(ZERO);
	}

protected void pcddLoop7010()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex, 10)) {
			goTo(GotoLabel.call7030);
		}
		pcddmjaIO.setFormat(formatsInner.pcddmjarec);
		SmartFileCode.execute(appVars, pcddmjaIO);
		if (isNE(pcddmjaIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(pcddmjaIO.getChdrcoy(), chdrpf.getChdrcoy())) {
			pcddmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcddmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcddmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcddmjaIO.getParams());
			syserrrec.statuz.set(pcddmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(pcddmjaIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.addPcdt7020);
		}
		pcdtmjaIO.setAgntnum(wsaaIndex, pcddmjaIO.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, pcddmjaIO.getSplitBcomm());
		pcddmjaIO.setFunction(varcom.nextr);
		goTo(GotoLabel.pcddLoop7010);
	}

protected void addPcdt7020()
	{
		if (isGT(wsaaIndex, 10)) {
			goTo(GotoLabel.call7030);
		}
		pcdtmjaIO.setAgntnum(wsaaIndex, SPACES);
		pcdtmjaIO.setSplitc(wsaaIndex, ZERO);
		wsaaIndex.add(1);
		goTo(GotoLabel.addPcdt7020);
	}

protected void call7030()
	{
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
		/*7190-EXIT.                                                       */
	}

protected void updatePcdt7100()
	{
		para7110();
	}

protected void para7110()
	{
		/* Update the PCDT to ensure that if the premium amount on the*/
		/* screen happens to change then the installment premium on the*/
		/* PCDT record is also updated. The Instalment premium on the*/
		/* PCDT record will be used later to create the Suspense ACMV*/
		/* posting.*/
		pcdtmjaIO.setParams(SPACES);
		pcdtmjaIO.setFunction(varcom.readh);
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		pcdtmjaIO.setInstprem(sv.instprem);
		pcdtmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validateFund8000()
	{
		/*START*/
		if (isEQ(t5543rec.unitVirtualFund[sub3.toInt()], SPACES)) {
			return ;
		}
		if (isEQ(sv.unitVirtualFund[sub1.toInt()], t5543rec.unitVirtualFund[sub3.toInt()])) {
			wsaaIfValidFund.set("Y");
			return ;
		}
		/*EXIT*/
	}

protected void readT66598500()
	{
		read8501();
		checkT6659Details8550();
	}

protected void read8501()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(wssplife.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(t6647rec.unitStatMethod, itdmIO.getItemitem())
		|| isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6659)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g029);
			fatalError600();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkT6659Details8550()
	{
		/* Check the details and call the generic subroutine from T6659.   */
		if (isEQ(t6659rec.subprog, SPACES)
		|| isNE(t6659rec.annOrPayInd, "P")
		|| isNE(t6659rec.osUtrnInd, "Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(wsspcomn.company);
		annprocrec.chdrnum.set(chdrpf.getChdrnum());
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(wssplife.effdate);
		annprocrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(varcom.vrcmUser);
		annprocrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		annprocrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		annprocrec.batccoy.set(wsaaBatckey.batcBatccoy);
		annprocrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		annprocrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		annprocrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError600();
		}
	}

protected void matchEnhanceBasis9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
					enhana9010();
				case enhanb9020: 
					enhanb9020();
				case enhanc9030: 
					enhanc9030();
				case enhand9040: 
					enhand9040();
				case enhane9050: 
					enhane9050();
				case enhanf9060: 
					enhanf9060();
				case nearExit9080: 
					nearExit9080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		/* First match the billing frequency.*/
		/* We are only dealing with Single Prems here, so check for '00'*/
		if (isEQ(t5545rec.billfreq[sub1.toInt()], SPACES)) {
			goTo(GotoLabel.nearExit9080);
		}
		if (isNE(t5545rec.billfreq[sub1.toInt()], "00")) {
			goTo(GotoLabel.nearExit9080);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.
	* There are 5 premiums in each billing frequency set, so
	* match each premium individually.
	* </pre>
	*/
protected void enhana9010()
	{
		if (isNE(sub1, 1)) {
			goTo(GotoLabel.enhanb9020);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanaPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhanb9020()
	{
		if (isNE(sub1, 2)) {
			goTo(GotoLabel.enhanc9030);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanbPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanbPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanbPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanbPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhanc9030()
	{
		if (isNE(sub1, 3)) {
			goTo(GotoLabel.enhand9040);
		}
		if ((isGTE(sv.instprem, t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhancPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhancPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhancPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhancPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhand9040()
	{
		if (isNE(sub1, 4)) {
			goTo(GotoLabel.enhane9050);
		}
		if ((isGTE(sv.instprem, t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhandPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhandPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhandPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhandPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhane9050()
	{
		if (isNE(sub1, 5)) {
			goTo(GotoLabel.enhanf9060);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanePc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanePc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanePc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanePc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhanf9060()
	{
		if (isNE(sub1, 6)) {
			goTo(GotoLabel.nearExit9080);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanfPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanfPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanfPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanfPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void nearExit9080()
	{
		sub1.add(1);
		/*EXIT*/
	}

protected void a100HitrExist()
	{
		/* Read T6647 to retrieve SEQ-NO, which is used to distinquish     */
		/* Coverages from Riders.                                          */
		//readT66475800();
		/* Read HITR to see if Details already exist                       */
		wsaaHitrFound.set("N");
		//ILIFE-8786 start
		hitrpf.setCoverage(covrpf.getCoverage());
		hitrpf.setRider("00");
		hitrpf.setChdrcoy(covrpf.getChdrcoy());
		hitrpf.setChdrnum(covrpf.getChdrnum());
		hitrpf.setLife(covrpf.getLife());
		hitrpf.setPlanSuffix(covrpf.getPlanSuffix());
		hitrpf.setTranno(0);
		wsaaRiderAlpha.set(covrpf.getRider());
		compute(wsaaProcSeqno, 0).set(add(t6647rec.procSeqNo, wsaaRiderNum));
		hitrpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		hitrpf.setProcSeqNo(wsaaProcSeqno.toInt());
		
		hitrList = hitrpfDAO.searchHitrRecord(hitrpf);
		if(!(hitrList.isEmpty())) {
			for (Hitrpf hitrIO : hitrList) {
					wsaaHitrFound.set("Y");
					return ;
			}
		//ILIFE-8786 end
		}
	}


protected void a200DelHitr()
	{
		//ILIFE-8786 start
		hitrpf = new  Hitrpf();
		if(!(hitrList.isEmpty())) {
			for (Hitrpf hitrIO : hitrList) {
				compute(wssplife.bigAmt, 2).set(sub(wssplife.bigAmt, hitrIO.getContractAmount()));
			}
			hitrpfDAO.deleteHitrpfRecord(hitrList);
		//ILIFE-8786 end
		}
	}

protected void a300MovesHitr()
	{
		a310Hitr();
	}

protected void a310Hitr()
	{
		wsaaHitrWritten.set("Y");
		//ILIFE-8786 start
		hitrpf = new Hitrpf();
		hitrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		hitrpf.setChdrnum(chdrpf.getChdrnum());
		hitrpf.setLife(covrpf.getLife());
		hitrpf.setCoverage(covrpf.getCoverage());
		hitrpf.setRider("00");
		setPrecision(hitrpf.getTranno(), 0);
		hitrpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		hitrpf.setBatccoy(wsspcomn.company.toString());
		hitrpf.setBatcbrn(wsspcomn.branch.toString());
		wsaaBatckey.set(wsspcomn.batchkey.toString());
		hitrpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		hitrpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		hitrpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		hitrpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		//readT66475800();
		hitrpf.setProcSeqNo(wsaaProcSeqno.toInt());
		hitrpf.setEffdate(sv.effdate.toInt());
		hitrpf.setCrtable(covrpf.getCrtable());
		hitrpf.setPlanSuffix(covrpf.getPlanSuffix());
		hitrpf.setCntcurr(chdrpf.getCntcurr());
		hitrpf.setCnttyp(chdrpf.getCnttype());
		hitrpf.setZintbfnd(sv.unitVirtualFund[index1.toInt()].toString());
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[index1.toInt()]);
		hitrpf.setFundCurrency(sv.currcy[index1.toInt()].toString());
		readT56456400();
		if (isEQ(wsaaUnitType, SPACES)) {
			if (isEQ(wsaaEnhancedAllocInd, "Y")) {
				if (isEQ(t5688rec.comlvlacc, "Y")) {
					hitrpf.setSacscode(t5645rec.sacscode[7].toString());
					hitrpf.setSacstyp(t5645rec.sacstype[7].toString());
					hitrpf.setGenlcde(t5645rec.glmap[7].toString());
				}
				else {
					hitrpf.setSacscode(t5645rec.sacscode[6].toString());
					hitrpf.setSacstyp(t5645rec.sacstype[6].toString());
					hitrpf.setGenlcde(t5645rec.glmap[6].toString());
				}
				hitrpf.setZrectyp("P");
			}
			else {
				if (isEQ(t5688rec.comlvlacc, "Y")) {
					hitrpf.setSacscode(t5645rec.sacscode[11].toString());
					hitrpf.setSacstyp(t5645rec.sacstype[11].toString());
					hitrpf.setGenlcde(t5645rec.glmap[11].toString());
				}
				else {
					hitrpf.setSacscode(t5645rec.sacscode[9].toString());
					hitrpf.setSacstyp(t5645rec.sacstype[9].toString());
					hitrpf.setGenlcde(t5645rec.glmap[9].toString());
				}
				hitrpf.setZrectyp("N");
				hitrpf.setZintbfnd(SPACES.stringValue());
			}
		}
		else {
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				hitrpf.setSacscode(t5645rec.sacscode[10].toString());
				hitrpf.setSacstyp(t5645rec.sacstype[10].toString());
				hitrpf.setGenlcde(t5645rec.glmap[10].toString());
			}
			else {
				hitrpf.setSacscode(t5645rec.sacscode[8].toString());
				hitrpf.setSacstyp(t5645rec.sacstype[8].toString());
				hitrpf.setGenlcde(t5645rec.glmap[8].toString());
			}
			hitrpf.setZrectyp("P");
		}
		if (isEQ(sv.percentAmountInd, "P")) {
			setPrecision(hitrpf.getContractAmount(), 3);
			hitrpf.setContractAmount(div(mult(wsaaHitrSingp, sv.unitAllocPercAmt[index1.toInt()]), 100).getbigdata());
		}
		else {
			hitrpf.setContractAmount(wsaaHitrSingp.getbigdata());
		}
		compute(wssplife.bigAmt, 2).set(add(wssplife.bigAmt, hitrpf.getContractAmount()));
		hitrpf.setInciprm01(BigDecimal.ZERO);
		hitrpf.setInciprm02(BigDecimal.ZERO);
		hitrpf.setUstmno(0);
		hitrpf.setZintrate(BigDecimal.ZERO);
		hitrpf.setInciNum(0);
		hitrpf.setInciPerd01(0);
		hitrpf.setInciPerd02(0);
		hitrpf.setFundAmount(BigDecimal.ZERO);
		hitrpf.setSurrenderPercent(BigDecimal.ZERO);
		hitrpf.setFundRate(BigDecimal.ZERO);
		hitrpf.setZintrate(BigDecimal.ZERO);
		hitrpf.setSvp(BigDecimal.ONE);
		hitrpf.setSwitchIndicator(SPACES.stringValue());
		hitrpf.setFeedbackInd(SPACES.stringValue());
		//ILIFE-1075
		hitrpf.setTriggerModule("ZSPTILET");
		hitrpf.setTriggerKey(SPACES.stringValue());
		hitrpf.setZintappind(SPACES.stringValue());
		hitrpf.setZlstintdte(varcom.vrcmMaxDate.toInt());
		hitrpf.setZinteffdt(varcom.vrcmMaxDate.toInt());
		if(isEQ(wsaaTranscd ,"T679"))
		{
			hitrpf.setFundPool("2");
		}
		hitrpfList.add(hitrpf);
		if(!(hitrpfList.isEmpty())){
			if(isEQ(function,'U')) {
				hitrpfDAO.updatedHitrpfRecord(hitrpfList);
				hitrpfList.clear();
			}
		else {
			hitrpfDAO.insertHitrpfRecord(hitrpfList);
			hitrpfList.clear();
			}
		}
		//ILIFE-8786 end
	}

protected void a400CheckAgent()
	{
		wsaaAgtTerminatedFlag.set("N");
		//ILIFE-8786 start
		Aglfpf aglfpf = aglfpfDAO.searchAglflnb(chdrpf.getAgntcoy().toString(), chdrpf.getAgntnum());
		if (isLT(aglfpf.getDtetrm(), datcon1rec.intDate)
		|| isLT(aglfpf.getDteexp(), datcon1rec.intDate)
		|| isGT(aglfpf.getDteapp(), datcon1rec.intDate)) {
			wsaaAgtTerminatedFlag.set("Y");
		}
		//ILIFE-8786 end
	}

protected void a500CalcTax()
	{
		a500Calc();
	}

protected void a500Calc()
	{
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(covrpf.getLife());
		txcalcrec.coverage.set(covrpf.getCoverage());
		txcalcrec.rider.set(covrpf.getRider());
		txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set(wsaaTaxTranType);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.amountIn.set(wsaaTaxableAmt);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
	
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setCompany(wsspcomn.company.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			fatalError600();
		}
		
		/*B900-EXIT*/
	}
protected void writeReservePricing(){

	zutrpf.setChdrpfx(chdrpf.getChdrpfx());
	zutrpf.setChdrcoy(chdrpf.getChdrcoy().toString().trim());
	zutrpf.setChdrnum(sv.chdrnum.toString().trim());
	
	zutrpf.setLife(covrpf.getLife().trim());	/* IJTI-1523 */
	zutrpf.setCoverage(covrpf.getCoverage().trim());/* IJTI-1523 */
	zutrpf.setRider(covrpf.getRider().trim());/* IJTI-1523 */
	zutrpf.setPlanSuffix(covrpf.getPlanSuffix());
	zutrpf.setTranno(chdrpf.getTranno()); // TODO ADD 1 ?
	zutrpf.setReserveUnitsInd(sv.reserveUnitsInd.toString().trim());
	zutrpf.setReserveUnitsDate(sv.reserveUnitsDate.toInt());
	zutrpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());	
	zutrpf.setTransactionDate(varcom.vrcmDate.toInt());
	zutrpf.setTransactionTime(varcom.vrcmTime.toInt());
	zutrpf.setUser(varcom.vrcmUser.toInt());
	zutrpf.setEffdate(sv.effdate.toInt());
	zutrpf.setValidflag("1");
	zutrpf.setDatesub(wsaaBusinessDate.toInt()); 
	zutrpf.setCrtuser(wsspcomn.userid.toString()); //TODO 
	zutrpf.setUserProfile(wsspcomn.userid.toString()); //TODO
	zutrpf.setJobName(appVars.getLoggedOnUser());//TODO
	zutrpf.setDatime("");//TODO
	
	try{
		zutrpfDAO.insertZutrpfRecord(zutrpf);
	}catch(Exception e){
		//syserrrec.params.set(covtlnbIO.getParams());	
		LOGGER.error("Exception occured in writeReservePricing()",e);
		fatalError600();
	}
	
}

//ILIFE-7392
protected void readT5533(){
	Itempf itempf = new Itempf();
	t5533rec= new T5533rec();
	List<Itempf> itemList= new ArrayList<Itempf>();
	itempf.setItemcoy(wsspcomn.company.toString().trim());
	itempf.setItempfx("IT");
	itempf.setItemtabl(tablesInner.t5533.toString().trim());
	itempf.setItemitem(tableItem.toString());
	itempf.setItmfrm(sv.effdate.getbigdata());
	itempf.setItmto(varcom.vrcmMaxDate.getbigdata());
	itemList = itempfDAO.findByItemDates(itempf);
	if(!itemList.isEmpty()){
		t5533rec.t5533Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
	}
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e199 = new FixedLengthStringData(4).init("E199");
	private FixedLengthStringData e631 = new FixedLengthStringData(4).init("E631");
	private FixedLengthStringData e706 = new FixedLengthStringData(4).init("E706");
	private FixedLengthStringData e707 = new FixedLengthStringData(4).init("E707");
	private FixedLengthStringData e859 = new FixedLengthStringData(4).init("E859");
	private FixedLengthStringData f348 = new FixedLengthStringData(4).init("F348");
	private FixedLengthStringData f351 = new FixedLengthStringData(4).init("F351");
	private FixedLengthStringData f982 = new FixedLengthStringData(4).init("F982");
	private FixedLengthStringData g037 = new FixedLengthStringData(4).init("G037");
	private FixedLengthStringData g103 = new FixedLengthStringData(4).init("G103");
	private FixedLengthStringData g105 = new FixedLengthStringData(4).init("G105");
	private FixedLengthStringData g106 = new FixedLengthStringData(4).init("G106");
	private FixedLengthStringData g347 = new FixedLengthStringData(4).init("G347");
	private FixedLengthStringData f025 = new FixedLengthStringData(4).init("F025");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData h023 = new FixedLengthStringData(4).init("H023");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h359 = new FixedLengthStringData(4).init("H359");
	private FixedLengthStringData h456 = new FixedLengthStringData(4).init("H456");
	private FixedLengthStringData h960 = new FixedLengthStringData(4).init("H960");
	private FixedLengthStringData hl06 = new FixedLengthStringData(4).init("HL06");
	private FixedLengthStringData rrku = new FixedLengthStringData(4).init("RRKU");
	private FixedLengthStringData t090 = new FixedLengthStringData(4).init("T090");
	private FixedLengthStringData g029 = new FixedLengthStringData(4).init("G029");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData rlcc = new FixedLengthStringData(4).init("RLCC");
	private FixedLengthStringData g099 = new FixedLengthStringData(4).init("G099");
	private FixedLengthStringData g070 = new FixedLengthStringData(4).init("G070");	//ILIFE-7392
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t5510 = new FixedLengthStringData(5).init("T5510");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5536 = new FixedLengthStringData(5).init("T5536");
	private FixedLengthStringData t5537 = new FixedLengthStringData(5).init("T5537");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5543 = new FixedLengthStringData(5).init("T5543");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t5551 = new FixedLengthStringData(5).init("T5551");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
	private FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5533 = new FixedLengthStringData(5).init("T5533");	//ILIFE-7392
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
public static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData pcddmjarec = new FixedLengthStringData(10).init("PCDDMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
}