package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:23
 * Description:
 * Copybook name: ZRSTTRAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrsttrakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrsttraFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zrsttraKey = new FixedLengthStringData(256).isAPartOf(zrsttraFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrsttraChdrcoy = new FixedLengthStringData(1).isAPartOf(zrsttraKey, 0);
  	public FixedLengthStringData zrsttraChdrnum = new FixedLengthStringData(8).isAPartOf(zrsttraKey, 1);
  	public FixedLengthStringData zrsttraLife = new FixedLengthStringData(2).isAPartOf(zrsttraKey, 9);
  	public FixedLengthStringData zrsttraCoverage = new FixedLengthStringData(2).isAPartOf(zrsttraKey, 11);
  	public FixedLengthStringData zrsttraRider = new FixedLengthStringData(2).isAPartOf(zrsttraKey, 13);
  	public PackedDecimalData zrsttraTranno = new PackedDecimalData(5, 0).isAPartOf(zrsttraKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(zrsttraKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrsttraFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrsttraFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}