/*
 * File: Cr503.java
 * Date: 30 August 2009 2:59:18
 * Author: $Id$
 * 
 * Class transformed from CR503.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.cls;

import com.csc.life.unitlinkedprocessing.batchprograms.Br503;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.printing.PrintManager;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cr503 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData statuz = new FixedLengthStringData(4);
	private FixedLengthStringData bsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData buparec = new FixedLengthStringData(1024);

	public Cr503() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		buparec = convertAndSetParam(buparec, parmArray, 4);
		bprdrec = convertAndSetParam(bprdrec, parmArray, 3);
		bsprrec = convertAndSetParam(bsprrec, parmArray, 2);
		bsscrec = convertAndSetParam(bsscrec, parmArray, 1);
		statuz = convertAndSetParam(statuz, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: 
					new PrintManager().overridePrinterFile("RR503", "RR503", new String[] {"LENGTH", "WIDTH", "WIDTH", "CPI"}, new Object[] {"66", "132", "*ROWCOL", "15"});
					callProgram(Br503.class, new Object[] {statuz, bsscrec, bsprrec, bprdrec, buparec});
				case returnVar: 
					return ;
				case error: 
					appVars.sendMessageToQueue("Unexpected errors occurred", "*");
					statuz.set("BOMB");
					qState = returnVar;
					break;
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("LBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
