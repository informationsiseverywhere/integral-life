/*
 * File: Unlprtp.java
 * Date: 30 August 2009 2:51:24
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLPRTP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO; //ILB-1097

import com.csc.life.productdefinition.dataaccess.model.Utrspf; //ILB-1097
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
*  UNLPRTP - Part Surrender Processing Method.
*  -------------------------------------------
*
*
*  This program is an item entry on T6598, the Surrender  claim
*  subroutine  method  table.  This  method is used in order to
*  update the Unit Transactions.
*
*  The following is the  linkage  information  passed  to  this
*  subroutine:
*
*             - company
*             - contract header number
*             - suffix
*             - life number
*             - joint-life number
*             - coverage
*             - rider
*             - crtable
*             - effective date
*             - estimated value
*             - percentage
*             - actual value
*             - currency
*             - element code
*             - type code
*             - status
*             - batch key
*
*  PROCESSING.
*  -----------
*
*  For  each  Unit  Summary  record  (UTRS)  for  this contract
*  perform the following:
*
*  If initial units write a  UTRN  record  with  the  following
*  data for the pending surrender sub-account:
*
*        Processing Seq No. -  from T6647
*        Surrender %        -  value from linkage
*        Amount (signed)    -  0
*        Effective date     -  Effective-date of the surrender
*        Bid/Offer Ind      -  'B'
*        Now/Deferred Ind   -  T6647 entry (deallocation field)
*        Price              -  0
*        Fund               -  linkage (element code)
*        Number of Units    -  0
*        Contract No.       -  linkage
*        Contract Type      -  linkage
*        Sub-Account Code   -  T5645 entry for this sub-account
*        Sub-Account Type   -  ditto
*        G/L key            -  ditto
*        Trigger module     -  T5687/T6598 entry
*        Trigger key        -  Contract no./Suffix/
*                              Life/Coverage/Rider
*        Surr Value Penalty -  0
*  If   accumulation   units  write  a  UTRN  record  with  the
*  following data for the pending surrender sub-account:
*
*        Processing Seq No. -  from T6647
*        Surrender %        -  value from linkage
*        Amount (signed)    -  0
*        Effective date     -  Effective-date of the surrender
*        Bid/Offer Ind      -  'B'
*        Now/Deferred Ind   -  T6647 entry (deallocation field)
*        Price              -  0
*        Fund               -  linkage (element code)
*        Number of Units    -  0
*        Contract No.       -  linkage
*        Contract Type      -  linkage
*        Sub-Account Code   -  T5645 entry for this sub-account
*        Sub-Account Type   -  ditto
*        G/L key            -  ditto
*        Trigger module     -  T5687/T6598 entry
*        Trigger key        -  Contract no./Suffix/
*                              Life/Coverage/Rider
*        Surr Value Penalty -  0
*
*****************************************************************
* </pre>
*/
public class Unlprtp extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Unlprtp.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLPRTP";
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String h115 = "H115";
	private static final String f294 = "F294";
	private static final String g029 = "G029";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t6647 = "T6647";
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	private static final String t5688 = "T5688";
	private static final String t6659 = "T6659";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaAccumValue = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaContractAmount = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaSurrender = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaTotalSurrender = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDiffAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSurrenderPercent = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaFundValue = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaPrice = new PackedDecimalData(9, 5);
	private FixedLengthStringData wsaaExit = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-AMOUNT-ARRAY */
	private ZonedDecimalData[] wsaaSubAmount = ZDInittedArray(99, 17, 5);

	private FixedLengthStringData wsaaTrigger = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaTriggerChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 0);
	private FixedLengthStringData wsaaTriggerChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTrigger, 1);
	private PackedDecimalData wsaaTriggerSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTrigger, 9);
	private PackedDecimalData wsaaTriggerTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTrigger, 12);
	private FixedLengthStringData wsaaTriggerCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 15);
	private FixedLengthStringData wsaaTriggerRider = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 17);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 19, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);

	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();

	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private T6647rec t6647rec = new T6647rec();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T6598rec t6598rec = new T6598rec();
	private T5688rec t5688rec = new T5688rec();
	private T5687rec t5687rec = new T5687rec();
	private T6659rec t6659rec = new T6659rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Surpcpy surpcpy = new Surpcpy();
	private FormatsInner formatsInner = new FormatsInner();
	private Zutrpf zutrpf = new Zutrpf();//ILIFE-5459
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);//ILIFE-5459
	private List<Zutrpf> zutrpfList = null; //ILIFE-5459
	private HitspfDAO hitspfDAO =  getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
	private Hitspf hitspf = new Hitspf();
	private List<Hitspf> hitspfList = new ArrayList<Hitspf>();	
 //ILB-1097 start
	private List<Utrspf> utrstotList;
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private Utrspf utrssur = new Utrspf();
	private Utrspf utrstot = new Utrspf();
	private int utrspfCount;
 //ILB-1097 end
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		zeroArray102, 
		exit279
	}

	public Unlprtp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		if (isEQ(surpcpy.type,"D")) {
			initialize100();
			a200InterestBearing();
			return ;
		}
		if ((isNE(surpcpy.type,"I"))
		&& (isNE(surpcpy.type,"A"))) {
			surpcpy.status.set("****");
			return ;
		}
		initialize100();
		mainProcessing200();
		/*EXIT*/
		exitProgram();
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go101();
				case zeroArray102: 
					zeroArray102();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go101()
	{
		surpcpy.status.set("****");
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(surpcpy.batckey);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTime.set(getCobolTime());
		if (isEQ(surpcpy.status,"****")) {
			readTabT6647120();
			readTabT5515130();
			readTabT5645140();
			readTabT5688145();
		}
		wsaaAccumValue.set(ZERO);
		wsaaFundValue.set(ZERO);
		wsaaContractAmount.set(ZERO);
		wsaaPrice.set(ZERO);
		wsaaSub2.set(ZERO);
		wsaaSurrender.set(ZERO);
		wsaaSurrenderPercent.set(ZERO);
		wsaaExit.set(SPACES);
	}

protected void zeroArray102()
	{
		wsaaSub2.add(1);
		if (isLT(wsaaSub2,99)) {
			wsaaSubAmount[wsaaSub2.toInt()].set(ZERO);
			goTo(GotoLabel.zeroArray102);
		}
		else {
			wsaaSub2.set(ZERO);
		}
		/*EXIT*/
	}

protected void readTabT6647120()
	{
		read121();
	}

protected void read121()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaTabBatckey.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(surpcpy.cnttype);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT6647Key,itdmIO.getItemitem())
		|| isNE(surpcpy.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t6647)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemitem(wsaaT6647Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h115);
			fatalError9000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTabT5515130()
	{
		read131();
	}

protected void read131()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(surpcpy.fund);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")) {
			itdmIO.setItemcoy(surpcpy.chdrcoy);
			itdmIO.setItemtabl(t5515);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(surpcpy.fund);
			itdmIO.setItmfrm(surpcpy.effdate);
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItempfx(),"IT")
		|| isNE(itdmIO.getItemitem(),surpcpy.fund)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemcoy(surpcpy.chdrcoy);
			itdmIO.setItemtabl(t5515);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(surpcpy.fund);
			itdmIO.setItmfrm(surpcpy.effdate);
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isNE(surpcpy.type,"P")
		&& isNE(surpcpy.type,t5515rec.unitType)
		&& isNE(surpcpy.type,t5515rec.zfundtyp)
		&& isNE(t5515rec.unitType,"B")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
	}

protected void readTabT5645140()
	{
		read141();
	}

protected void read141()
	{
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(surpcpy.chdrcoy);
		descIO.setLanguage(surpcpy.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),"****")
		&& isNE(descIO.getStatuz(),"MRNF")) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
	}

protected void readTabT5688145()
	{
		read146();
	}

protected void read146()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(surpcpy.cnttype);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN ");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(surpcpy.cnttype,itdmIO.getItemitem())
		|| isNE(surpcpy.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemitem(surpcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void callPriceFile150()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),"****")
		&& isNE(vprnudlIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if (isNE(vprnudlIO.getCompany(),surpcpy.chdrcoy)
		|| isNE(vprnudlIO.getUnitVirtualFund(),surpcpy.fund)
		|| isNE(vprnudlIO.getUnitType(),surpcpy.type)
		|| isEQ(vprnudlIO.getStatuz(),"ENDP")) {
			vprnudlIO.setStatuz("ENDP");
		}
		/*EXIT*/
	}

protected void mainProcessing200()
	{
			para201();
		}

protected void para201()
	{
		if (isEQ(surpcpy.estimatedVal,0)) {
			return ;
		}
		
		if (isNE(t6647rec.unitStatMethod,SPACES)) {
			a100ReadT6659();
		}
		if (isEQ(surpcpy.planSuffix,ZERO)) {
			if (isEQ(surpcpy.percreqd,ZERO)) {
				calcFundValue260();
				return ;
			}
			else {
				writeUtrnWholePlan280();
				return ;
			}
		}
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setPlanSuffix(surpcpy.planSuffix);
		utrsclmIO.setLife(surpcpy.life);
		utrsclmIO.setUnitVirtualFund(surpcpy.fund);
		utrsclmIO.setUnitType(surpcpy.type);
		utrsclmIO.setChdrcoy(surpcpy.chdrcoy);
		utrsclmIO.setChdrnum(surpcpy.chdrnum);
		utrsclmIO.setCoverage(surpcpy.coverage);
		utrsclmIO.setRider(surpcpy.rider);
		utrsclmIO.setFunction("READR");
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(),"****")) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		getTrigger500();
		if (isGT(surpcpy.percreqd,ZERO)) {
			wsaaSurrenderPercent.set(surpcpy.percreqd);
			wsaaSurrender.set(ZERO);
		}
		else {
			wsaaSurrenderPercent.set(ZERO);
			compute(wsaaSurrender, 5).set(mult(surpcpy.estimatedVal,-1));
		}
		setupUtrns600();
	}

protected void calcFundValue260()
	{
		para261();
	}

protected void para261()
	{
		 //ILB-1097 start
		utrstot = new Utrspf();
		utrstot.setChdrcoy(surpcpy.chdrcoy.toString());
		utrstot.setChdrnum(surpcpy.chdrnum.toString());
		utrstot.setLife(surpcpy.life.toString());
		utrstot.setCoverage(surpcpy.coverage.toString());
		utrstot.setRider(surpcpy.rider.toString());
		utrstot.setUnitVirtualFund(surpcpy.fund.toString());
		utrstot.setUnitType(surpcpy.type.toString());
		utrstotList = utrspfDAO.searchUtrssurRecord(utrstot);
		wsaaAccumValue.set(ZERO);
		wsaaExit.set("N");
		for(utrspfCount=0;utrspfCount<utrstotList.size();utrspfCount++)
		{   
			utrstot=utrstotList.get(utrspfCount);
			valueFundType270();
		}
 //ILB-1097 end
		writeUtrnWholePlan280();
	}

protected void valueFundType270()
	{
		try {
			para271();
			normalPrice272();
			fundContractAmt274();
			accumValue275();
		}
		catch (GOTOException e){
		}
	}

protected void para271()
	{
		 //ILB-1097 start
	
	 if(utrspfCount>=utrstotList.size() ) {
		 wsaaExit.set("Y");
	    	return;
	   }
	else {
		wsaaExit.set("N");
	}
	if (isEQ(utrstot.getCurrentUnitBal(),ZERO)) {
		wsaaExit.set("N");
		goTo(GotoLabel.exit279);
	}
	}
	 //ILB-1097 end

protected void normalPrice272()
	{
		vprnudlIO.setDataKey(SPACES);
		vprnudlIO.setCompany(surpcpy.chdrcoy);
		vprnudlIO.setUnitVirtualFund(utrstot.getUnitVirtualFund()); //ILB-1097
		vprnudlIO.setUnitType(utrstot.getUnitType()); //ILB-1097
		vprnudlIO.setEffdate(surpcpy.effdate);
		vprnudlIO.setFunction("BEGN");
		callPriceFile150();
		if (isEQ(vprnudlIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if (isEQ(t6647rec.bidoffer,"B")) {
			wsaaPrice.set(vprnudlIO.getUnitBidPrice());
		}
		else {
			wsaaPrice.set(vprnudlIO.getUnitOfferPrice());
		}
	}

protected void fundContractAmt274()
	{
		compute(wsaaFundValue, 6).setRounded((mult(utrstot.getCurrentUnitBal(),wsaaPrice))); //ILB-1097
		if (isNE(surpcpy.currcode,t5515rec.currcode)) {
			conlinkrec.currIn.set(t5515rec.currcode);
			conlinkrec.cashdate.set(vprnudlIO.getEffdate());
			conlinkrec.currOut.set(surpcpy.currcode);
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.function.set("CVRT");
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(surpcpy.chdrcoy);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,"****")) {
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError9000();
			}
			else {
				wsaaContractAmount.set(conlinkrec.amountOut);
			}
		}
		else {
			wsaaContractAmount.set(wsaaFundValue);
		}
		zrdecplrec.amountIn.set(wsaaContractAmount);
		a000CallRounding();
		wsaaContractAmount.set(zrdecplrec.amountOut);
		wsaaSub2.add(1);
		wsaaSubAmount[wsaaSub2.toInt()].set(wsaaContractAmount);
	}

protected void accumValue275()
	{
		wsaaAccumValue.add(wsaaContractAmount);
	}

protected void writeUtrnWholePlan280()
	{
		para281();
	}

protected void para281()
	{
		wsaaSub2.set(ZERO);
		 //ILB-1097 start
		utrssur = new Utrspf();
		utrssur.setLife(surpcpy.life.toString());
		utrssur.setUnitVirtualFund(surpcpy.fund.toString());
		if (isNE(surpcpy.type,"P")) {
			utrssur.setUnitType(surpcpy.type.toString());
		}
		utrssur.setChdrcoy(surpcpy.chdrcoy.toString());
		utrssur.setChdrnum(surpcpy.chdrnum.toString());
		utrssur.setCoverage(surpcpy.coverage.toString());
		utrssur.setRider(surpcpy.rider.toString());
		List<Utrspf> utrssurList = utrspfDAO.searchUtrssurRecord(utrssur);
		if (isNE(surpcpy.type,"P")
		&& isNE(surpcpy.type,utrssur.getUnitType())) {
			syserrrec.params.set(utrssur.getChdrnum().concat(utrssur.getChdrcoy()));
			fatalError9000();
		}
		wsaaTotalSurrender.set(0);
		
		if(!(utrssurList.isEmpty())) {
			for(Utrspf utrs:utrssurList) {
				utrssur = utrs;
				readUtrsWriteUtrn290();
		}
					
		if (isEQ(surpcpy.percreqd,ZERO)
		&& isNE(wsaaTotalSurrender,ZERO)) {
			compute(wsaaDiffAmt, 2).set(sub(surpcpy.estimatedVal,wsaaTotalSurrender));
			if (isNE(wsaaDiffAmt,ZERO)) {
				adjustUtrnAmt300();
			}
		  }
 //ILB-1097 end
		}
	}

protected void readUtrsWriteUtrn290()
	{
					read291();
					next293();
				}

protected void read291()
	{
		wsaaSub2.add(1);
		if (isEQ(utrssur.getCurrentUnitBal(),ZERO)) {
			return ;
		}
		if (isGT(surpcpy.percreqd,ZERO)) {
			wsaaSurrenderPercent.set(surpcpy.percreqd);
			wsaaSurrender.set(ZERO);
		}
		else {
			if (isLTE(surpcpy.estimatedVal,wsaaAccumValue)) {
				compute(wsaaSurrender, 6).setRounded((mult((div((mult(surpcpy.estimatedVal,wsaaSubAmount[wsaaSub2.toInt()])),wsaaAccumValue)),-1)));
				wsaaSurrenderPercent.set(ZERO);
			}
			else {
				if (isNE(wsaaAccumValue,ZERO)) {
					compute(wsaaSurrender, 6).setRounded((mult(wsaaSubAmount[wsaaSub2.toInt()],-1)));
					wsaaSurrenderPercent.set(ZERO);
				}
			}
		}
		compute(wsaaTotalSurrender, 5).set(add(wsaaTotalSurrender,(mult(wsaaSurrender,-1))));
		zrdecplrec.amountIn.set(wsaaTotalSurrender);
		a000CallRounding();
		wsaaTotalSurrender.set(zrdecplrec.amountOut);
		getTrigger500();
		setupUtrns600();
	}

protected void next293()
	{
		
		if (isNE(surpcpy.type,"P")
		&& isNE(surpcpy.type,utrssur.getUnitType())) {
			
			return;
		}
		/*EXIT*/
	}

protected void adjustUtrnAmt300()
	{
		start300();
	}

protected void start300()
	{
		getAppVars().addDiagnostic("ENTER 300 ");
		utrnIO.setFormat(formatsInner.utrnrec);
		utrnIO.setFunction("READH");
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),"****")) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
		setPrecision(utrnIO.getContractAmount(), 2);
		utrnIO.setContractAmount(add(utrnIO.getContractAmount(),(mult(wsaaDiffAmt,-1))));
		utrnIO.setFormat(formatsInner.utrnrec);
		utrnIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),"****")) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
		getAppVars().addDiagnostic("EXIT  300 ");
	}

protected void getTrigger500()
	{
		read531();
	}

protected void read531()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(surpcpy.crtable);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),surpcpy.crtable)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemitem(surpcpy.crtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.partsurr);
		itemIO.setItempfx("IT");
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void setupUtrns600()
	{
		go601();
	}

protected void go601()
	{
		utrnIO.setParams(SPACES);
		utrnIO.setTranno(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setProcSeqNo(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setCrComDate(ZERO);
		utrnIO.setSvp(1);
		covrenqIO.setDataKey(SPACES);
		covrenqIO.setChdrcoy(surpcpy.chdrcoy);
		covrenqIO.setChdrnum(surpcpy.chdrnum);
		covrenqIO.setLife(surpcpy.life);
		covrenqIO.setPlanSuffix(surpcpy.planSuffix);
		covrenqIO.setCoverage(surpcpy.coverage);
		covrenqIO.setRider(surpcpy.rider);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction("READH");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),"****")) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError9000();
		}
		utrnIO.setCrComDate(covrenqIO.getCrrcd());
		utrnIO.setChdrcoy(surpcpy.chdrcoy);
		wsaaTriggerChdrcoy.set(surpcpy.chdrcoy);
		utrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		wsaaTriggerTranno.set(surpcpy.tranno);
		utrnIO.setChdrnum(surpcpy.chdrnum);
		wsaaTriggerChdrnum.set(surpcpy.chdrnum);
		if (isEQ(surpcpy.planSuffix,ZERO)) {
			utrnIO.setPlanSuffix(utrssur.getPlanSuffix()); //ILB-1097
			wsaaTriggerSuffix.set(utrssur.getPlanSuffix()); //ILB-1097
		}
		else {
			utrnIO.setPlanSuffix(surpcpy.planSuffix);
			wsaaTriggerSuffix.set(surpcpy.planSuffix);
		}
		utrnIO.setLife(surpcpy.life);
		utrnIO.setCoverage(surpcpy.coverage);
		wsaaTriggerCoverage.set(surpcpy.coverage);
		utrnIO.setRider(surpcpy.rider);
		wsaaTriggerRider.set(surpcpy.rider);
		utrnIO.setCrtable(surpcpy.crtable);
		utrnIO.setMoniesDate(surpcpy.effdate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setSurrenderPercent(wsaaSurrenderPercent);
		utrnIO.setContractAmount(wsaaSurrender);
		utrnIO.setFundAmount(0);
		if (isEQ(surpcpy.element,SPACES)) {
			utrnIO.setUnitVirtualFund(surpcpy.fund);
		}
		else {
			utrnIO.setUnitVirtualFund(surpcpy.element);
		}
		utrnIO.setContractType(surpcpy.cnttype);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setNowDeferInd(t6647rec.dealin);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		utrnIO.setTriggerModule(t6598rec.addprocess);
		utrnIO.setTriggerKey(wsaaTrigger);
		utrnIO.setNofUnits(0);
		utrnIO.setTransactionDate(surpcpy.date_var);
		utrnIO.setTransactionTime(surpcpy.time);
		utrnIO.setTranno(surpcpy.tranno);
		utrnIO.setUser(surpcpy.user);
		if (isEQ(surpcpy.planSuffix,0)) {
			utrnIO.setUnitType(utrssur.getUnitType());
			if (isEQ(utrssur.getUnitType(),"A")) {
				utrnIO.setSvp(1);
			}
		}
		if (isNE(surpcpy.planSuffix,ZERO)) {
			utrnIO.setUnitType(surpcpy.type);
			if (isEQ(surpcpy.type,"A")) {
				utrnIO.setSvp(1);
			}
		}
		utrnIO.setTermid(SPACES);
		if (isEQ(utrnIO.getUnitType(),"A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		else {
			utrnIO.setUnitSubAccount("INIT");
		}
		utrnIO.setTermid(SPACES);
		utrnIO.setFundCurrency(t5515rec.currcode);
		/*    Set up the UTRN contract currency code from the              */
		/*    surrender currency code if it differs from that of           */
		/*    the contract.                                                */
		/* MOVE SURP-CNTCURR           TO UTRN-CNTCURR.                 */
		if (isNE(surpcpy.currcode, surpcpy.cntcurr)) {
			utrnIO.setCntcurr(surpcpy.currcode);
		}
		else {
			utrnIO.setCntcurr(surpcpy.cntcurr);
		}
		utrnIO.setFormat(formatsInner.utrnrec);
		utrnIO.setFunction("WRITR");
		
		readReservePricing();
		
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),"****")) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void a100ReadT6659()
	{
			a100Read();
			a1010CheckT6659Details();
		}

protected void a100Read()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(t6647rec.unitStatMethod,itdmIO.getItemitem())
		|| isNE(surpcpy.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t6659)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g029);
			fatalError9000();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void a1010CheckT6659Details()
	{
		if (isEQ(t6659rec.subprog,SPACES)
		|| isNE(t6659rec.annOrPayInd,"P")
		|| isNE(t6659rec.osUtrnInd,"Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(surpcpy.chdrcoy);
		annprocrec.chdrnum.set(surpcpy.chdrnum);
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(surpcpy.effdate);
		annprocrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(surpcpy.user);
		annprocrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		annprocrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		annprocrec.batccoy.set(wsaaBatckey.batcBatccoy);
		annprocrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		annprocrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		annprocrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz,"****")) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError9000();
		}
	}

protected void a200InterestBearing()
	{
			a210Para();
		}

protected void a210Para()
	{
		if (isEQ(surpcpy.estimatedVal,0)) {
			return ;
		}
		hitsIO.setParams(SPACES);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setLife(surpcpy.life);
		hitsIO.setZintbfnd(surpcpy.fund);
		hitsIO.setChdrcoy(surpcpy.chdrcoy);
		hitsIO.setChdrnum(surpcpy.chdrnum);
		hitsIO.setLife(surpcpy.life);
		hitsIO.setCoverage(surpcpy.coverage);
		hitsIO.setRider(surpcpy.rider);
		hitsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","ZINTBFND");
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(surpcpy.chdrnum, hitsIO.getChdrnum())
		|| isNE(surpcpy.chdrcoy,hitsIO.getChdrcoy())
		|| isNE(surpcpy.life,hitsIO.getLife())
		|| isNE(surpcpy.coverage,hitsIO.getCoverage())
		|| isNE(surpcpy.rider,hitsIO.getRider())
		|| isNE(surpcpy.fund,hitsIO.getZintbfnd())) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		while ( !(isEQ(hitsIO.getStatuz(),varcom.endp))) {
			a300ReadHitsWriteHitr();
		}
		
	}

protected void a300ReadHitsWriteHitr()
	{
		/*A310-READ*/
		if (isNE(hitsIO.getZcurprmbal(),0)) {
			calculateContractAmount300();
			getTrigger500();
			a400SetupHitrs();
		}
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(surpcpy.chdrnum,hitsIO.getChdrnum())
		|| isNE(surpcpy.chdrcoy,hitsIO.getChdrcoy())
		|| isNE(surpcpy.life,hitsIO.getLife())
		|| isNE(surpcpy.coverage,hitsIO.getCoverage())
		|| isNE(surpcpy.rider,hitsIO.getRider())
		|| isNE(surpcpy.fund,hitsIO.getZintbfnd())) {
			hitsIO.setStatuz(varcom.endp);
		}
		/*A390-EXIT*/
	}

protected void a400SetupHitrs()
	{
		a410Go();
	}

protected void a410Go()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setTranno(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setPlanSuffix(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setContractAmount(wsaaSurrender);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setProcSeqNo(ZERO);
		hitrIO.setSvp(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setChdrcoy(surpcpy.chdrcoy);
		wsaaTriggerChdrcoy.set(surpcpy.chdrcoy);
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		wsaaTriggerTranno.set(surpcpy.tranno);
		hitrIO.setChdrnum(surpcpy.chdrnum);
		wsaaTriggerChdrnum.set(surpcpy.chdrnum);
		hitrIO.setPlanSuffix(surpcpy.planSuffix);
		wsaaTriggerSuffix.set(surpcpy.planSuffix);
		hitrIO.setLife(surpcpy.life);
		hitrIO.setCoverage(surpcpy.coverage);
		wsaaTriggerCoverage.set(surpcpy.coverage);
		hitrIO.setRider(surpcpy.rider);
		wsaaTriggerRider.set(surpcpy.rider);
		hitrIO.setCrtable(surpcpy.crtable);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setSurrenderPercent(surpcpy.percreqd);
		hitrIO.setFundAmount(0);
		if (isEQ(surpcpy.element,SPACES)) {
			hitrIO.setZintbfnd(surpcpy.fund);
		}
		else {
			hitrIO.setZintbfnd(surpcpy.element);
		}
		hitrIO.setCnttyp(surpcpy.cnttype);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			hitrIO.setSacscode(t5645rec.sacscode02);
			hitrIO.setSacstyp(t5645rec.sacstype02);
			hitrIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode01);
			hitrIO.setSacstyp(t5645rec.sacstype01);
			hitrIO.setGenlcde(t5645rec.glmap01);
		}
		hitrIO.setTriggerModule(t6598rec.addprocess);
		hitrIO.setTriggerKey(wsaaTrigger);
		hitrIO.setTranno(surpcpy.tranno);
		hitrIO.setSvp(1);
		hitrIO.setZrectyp("P");
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hitrIO.getChdrcoy());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(hitrIO.getZintbfnd());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		hitrIO.setFundCurrency(t5515rec.currcode);
		/*    Set up the HITR contract currency code from the              */
		/*    surrender currency code if it differs from that of           */
		/*    the contract.                                                */
		/* MOVE SURP-CNTCURR           TO HITR-CNTCURR.         <A06575>*/
		if (isNE(surpcpy.currcode, surpcpy.cntcurr)) {
			hitrIO.setCntcurr(surpcpy.currcode);
		}
		else {
		hitrIO.setCntcurr(surpcpy.cntcurr);
		}
		hitrIO.setEffdate(surpcpy.effdate);
		hitrIO.setZintalloc("N");
		hitrIO.setZintrate(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setFormat(formatsInner.hitrrec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		surpcpy.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(surpcpy.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(surpcpy.currcode);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A900-EXIT*/
	}
//ILIFE-5459 Start
protected void readReservePricing(){
	
	zutrpf.setChdrcoy(surpcpy.chdrcoy.toString().trim());
	zutrpf.setChdrnum(surpcpy.chdrnum.toString().trim());
	zutrpf.setTranno(surpcpy.tranno.toInt());
	
	try {
		zutrpfList = zutrpfDAO.readReservePricing(zutrpf);
	} catch (Exception e) {
		LOGGER.error("Exception occured in readReservePricing()",e);
		fatalError9000();
	}
	
	if (isGT(zutrpfList.size(), 0)){
		
		for(Zutrpf zutrpf1 : zutrpfList){
			utrnIO.setMoniesDate(zutrpf1.getReserveUnitsDate()); 
			utrnIO.setNowDeferInd('N');
		}
	}	
	//ILIFE-5459 End
}
protected void calculateContractAmount300() {
	if (isEQ(surpcpy.estimatedVal,0)) {
		return ;
	}
	if (isEQ(surpcpy.planSuffix,ZERO)) {
		if (isEQ(surpcpy.percreqd,ZERO)) {
			calcFundValue310();
			return ;
		}
	}
}
protected void calcFundValue310() {
	valueFundType311();
	writeHitrWholePlan312();
}
protected void valueFundType311() { 
	try {
		para321();
		if(!(hitspfList.isEmpty())) {
			normalPrice322();
			fundContractAmt323();
			accumValue324();
		}
	}
	catch (GOTOException e){
	}
}
protected void para321() {
	hitspf.setChdrcoy(surpcpy.chdrcoy.toString());
	hitspf.setChdrnum(surpcpy.chdrnum.toString());
	hitspf.setCoverage(surpcpy.coverage.toString());
	hitspf.setLife(surpcpy.life.toString());
	hitspf.setRider(surpcpy.rider.toString());
	hitspfList = hitspfDAO.searchHitsRecord(hitspf);
}
protected void normalPrice322() {
	for(Hitspf hitspf : hitspfList){
		compute(wsaaFundValue, 5).setRounded(add(wsaaFundValue,hitspf.getZcurprmbal()));
		//wsaaFundValue.add(hitspf.getZcurprmbal().);
	}
}
protected void fundContractAmt323() {
		//compute(wsaaFundValue, 6).setRounded((mult(hitspfList.get(0).getZcurprmbal(),wsaaPrice)));
		if (isNE(surpcpy.currcode,t5515rec.currcode)) {
			conlinkrec.currIn.set(t5515rec.currcode);
			conlinkrec.cashdate.set(vprnudlIO.getEffdate());
			conlinkrec.currOut.set(surpcpy.currcode);
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.function.set("CVRT");
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(surpcpy.chdrcoy);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,"****")) {
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError9000();
			}
			else {
				wsaaContractAmount.set(conlinkrec.amountOut);
			}
		}
		else {
			wsaaContractAmount.set(wsaaFundValue);
		}
		zrdecplrec.amountIn.set(wsaaContractAmount);
		a000CallRounding();
		wsaaContractAmount.set(zrdecplrec.amountOut);
		wsaaSub2.add(1);
		wsaaSubAmount[wsaaSub2.toInt()].set(wsaaContractAmount);
}
protected void accumValue324() {
	wsaaAccumValue.add(wsaaContractAmount);
}
protected void writeHitrWholePlan312() { 
	para313();
}
protected void para313() {
	wsaaSub2.set(ZERO);
	hitspf.setChdrcoy(surpcpy.chdrcoy.toString());
	hitspf.setChdrnum(surpcpy.chdrnum.toString());
	hitspf.setCoverage(surpcpy.coverage.toString());
	hitspf.setLife(surpcpy.life.toString());
	hitspf.setRider(surpcpy.rider.toString());
	hitspfList = hitspfDAO.searchHitsRecord(hitspf);
	wsaaTotalSurrender.set(0);
	for(Hitspf hits : hitspfList) {
		readHitsWriteHitr314(hits);
	}
	if (isEQ(surpcpy.percreqd,ZERO)
	&& isNE(wsaaTotalSurrender,ZERO)) {
		compute(wsaaDiffAmt, 2).set(sub(surpcpy.estimatedVal,wsaaTotalSurrender));
		if (isNE(wsaaDiffAmt,ZERO)) {
			adjustHitrAmt300();
		}
	}
}
protected void readHitsWriteHitr314(Hitspf hits) {
	wsaaSub2.add(1);
	if (isEQ(hits.getZcurprmbal(),ZERO)) {
		return ;
	}
	if (isGT(surpcpy.percreqd,ZERO)) {
		wsaaSurrenderPercent.set(surpcpy.percreqd);
		wsaaSurrender.set(ZERO);
	}
	else {
		if (isLTE(surpcpy.estimatedVal,wsaaAccumValue)) {
			compute(wsaaSurrender, 6).setRounded((mult((div((mult(surpcpy.estimatedVal,wsaaSubAmount[wsaaSub2.toInt()])),wsaaAccumValue)),-1)));
			wsaaSurrenderPercent.set(ZERO);
		}
		else {
			if (isNE(wsaaAccumValue,ZERO)) {
				compute(wsaaSurrender, 6).setRounded((mult(wsaaSubAmount[wsaaSub2.toInt()],-1)));
				wsaaSurrenderPercent.set(ZERO);
			}
		}
	}
	compute(wsaaTotalSurrender, 5).set(add(wsaaTotalSurrender,(mult(wsaaSurrender,-1))));
	zrdecplrec.amountIn.set(wsaaTotalSurrender);
	a000CallRounding();
	wsaaTotalSurrender.set(zrdecplrec.amountOut);
}
protected void adjustHitrAmt300() {
	hitrIO.setFormat(formatsInner.utrnrec);
	hitrIO.setFunction("READH");
	SmartFileCode.execute(appVars, hitrIO);
	if (isNE(hitrIO.getStatuz(),"****")) {
		syserrrec.params.set(hitrIO.getParams());
		fatalError9000();
	}
	setPrecision(hitrIO.getContractAmount(), 2);
	hitrIO.setContractAmount(add(utrnIO.getContractAmount(),(mult(wsaaDiffAmt,-1))));
	hitrIO.setFormat(formatsInner.utrnrec);
	hitrIO.setFunction("REWRT");
	SmartFileCode.execute(appVars, hitrIO);
	if (isNE(hitrIO.getStatuz(),"****")) {
		syserrrec.params.set(hitrIO.getParams());
		fatalError9000();
	}
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).init("HITRREC");
}
}
