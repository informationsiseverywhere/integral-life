package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovupfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:36
 * Class transformed from COVUPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovupfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 18;
	public FixedLengthStringData covurec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData covupfRecord = covurec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(covurec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(covurec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(covurec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(covurec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(covurec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(covurec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CovupfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for CovupfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CovupfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CovupfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovupfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CovupfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovupfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COVUPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCovurec() {
  		return covurec;
	}

	public FixedLengthStringData getCovupfRecord() {
  		return covupfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCovurec(what);
	}

	public void setCovurec(Object what) {
  		this.covurec.set(what);
	}

	public void setCovupfRecord(Object what) {
  		this.covupfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(covurec.getLength());
		result.set(covurec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}