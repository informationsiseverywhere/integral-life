package com.csc.life.unitlinkedprocessing.recordstructures;

import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:09
 * Description:
 * Copybook name: CBUNITYPS
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cbunityps extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData unitType = new FixedLengthStringData(1);
  	public Validator init = new Validator(unitType, "I");
  	public Validator accum = new Validator(unitType, "A");
  	public Validator uninvested = new Validator(unitType, " ");
  	public Validator charges = new Validator(unitType, " ");
  	public Validator debt = new Validator(unitType, "D");
  	public Validator transfer = new Validator(unitType, " ");
  	public Validator notIA = new Validator(unitType, " ");
  	public FixedLengthStringData initVal = new FixedLengthStringData(1).init("I");
  	public FixedLengthStringData accumVal = new FixedLengthStringData(2).init("A");
  	public FixedLengthStringData uninvestedVal = new FixedLengthStringData(2).init(" ");
  	public FixedLengthStringData chargesVal = new FixedLengthStringData(2).init(" ");
  	public FixedLengthStringData debtVal = new FixedLengthStringData(2).init("D");
  	public FixedLengthStringData transferVal = new FixedLengthStringData(2).init(" ");


	public void initialize() {
		COBOLFunctions.initialize(unitType);
		COBOLFunctions.initialize(initVal);
		COBOLFunctions.initialize(accumVal);
		COBOLFunctions.initialize(uninvestedVal);
		COBOLFunctions.initialize(chargesVal);
		COBOLFunctions.initialize(debtVal);
		COBOLFunctions.initialize(transferVal);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		unitType.isAPartOf(baseString, true);
    		initVal.isAPartOf(baseString, true);
    		accumVal.isAPartOf(baseString, true);
    		uninvestedVal.isAPartOf(baseString, true);
    		chargesVal.isAPartOf(baseString, true);
    		debtVal.isAPartOf(baseString, true);
    		transferVal.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}