package com.csc.life.unitlinkedprocessing.dataaccess.model;


public class Covupf{
	private long uniqueNUmber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	public long getUniqueNUmber() {
		return uniqueNUmber;
	}
	public void setUniqueNUmber(long uniqueNUmber) {
		this.uniqueNUmber = uniqueNUmber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

}