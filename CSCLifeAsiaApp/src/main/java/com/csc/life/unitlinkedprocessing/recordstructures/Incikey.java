package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:56
 * Description:
 * Copybook name: INCIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData inciFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData inciKey = new FixedLengthStringData(64).isAPartOf(inciFileKey, 0, REDEFINE);
  	public FixedLengthStringData inciChdrcoy = new FixedLengthStringData(1).isAPartOf(inciKey, 0);
  	public FixedLengthStringData inciChdrnum = new FixedLengthStringData(8).isAPartOf(inciKey, 1);
  	public FixedLengthStringData inciLife = new FixedLengthStringData(2).isAPartOf(inciKey, 9);
  	public FixedLengthStringData inciCoverage = new FixedLengthStringData(2).isAPartOf(inciKey, 11);
  	public FixedLengthStringData inciRider = new FixedLengthStringData(2).isAPartOf(inciKey, 13);
  	public PackedDecimalData inciPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(inciKey, 15);
  	public PackedDecimalData inciInciNum = new PackedDecimalData(3, 0).isAPartOf(inciKey, 18);
  	public PackedDecimalData inciSeqno = new PackedDecimalData(2, 0).isAPartOf(inciKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(inciKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(inciFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		inciFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}