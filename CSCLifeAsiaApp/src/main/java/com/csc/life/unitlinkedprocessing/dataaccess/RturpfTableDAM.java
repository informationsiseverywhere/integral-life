package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RturpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:20
 * Class transformed from RTURPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RturpfTableDAM extends PFAdapterDAM {
    
	//ILIFE-1214 by smalchi2 STARTS
	public int pfRecLen = 62;
	//ENDS
	public FixedLengthStringData rturrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData rturpfRecord = rturrec;
	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(rturrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(rturrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(rturrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(rturrec);
	public FixedLengthStringData vfund = DD.vfund.copy().isAPartOf(rturrec);
	public PackedDecimalData currentDunitBal = DD.curduntbal.copy().isAPartOf(rturrec);
	public PackedDecimalData unitBidPrice = DD.ubidpr.copy().isAPartOf(rturrec);
	public PackedDecimalData age = DD.age.copy().isAPartOf(rturrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RturpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for RturpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RturpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RturpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RturpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RturpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RturpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("RTURPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRNUM, " +
							"STATCODE, " +
							"CRTABLE, " +
							"SUMINS, " +
							"VFUND, " +
							"CURDUNTBAL, " +
							"UBIDPR, " +
							"AGE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrnum,
                                     statcode,
                                     crtable,
                                     sumins,
                                     vfund,
                                     currentDunitBal,
                                     unitBidPrice,
                                     age,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrnum.clear();
  		statcode.clear();
  		crtable.clear();
  		sumins.clear();
  		vfund.clear();
  		currentDunitBal.clear();
  		unitBidPrice.clear();
  		age.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRturrec() {
  		return rturrec;
	}

	public FixedLengthStringData getRturpfRecord() {
  		return rturpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRturrec(what);
	}

	public void setRturrec(Object what) {
  		this.rturrec.set(what);
	}

	public void setRturpfRecord(Object what) {
  		this.rturpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(rturrec.getLength());
		result.set(rturrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}