package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Udelpf{
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private String termid;
	private int transactionDate;
	private int transactionTime;
	private int user;
	private int jobnoPrice;
	private String unitVirtualFund;
	private String unitSubAccount;
	private int strpdate;
	private String nowDeferInd;
	private BigDecimal nofUnits;
	private String unitType;
	private String batccoy;
	private String batcbrn;
	private int batcactyr;
	private int batcactmn;
	private String batctrcde;
	private String batcbatch;
	private int priceDateUsed;
	private int moniesDate;
	private BigDecimal priceUsed;
	private String crtable;
	private String cntcurr;
	private BigDecimal nofDunits;
	private String feedbackInd;
	private BigDecimal unitBarePrice;
	private int inciNum;
	private int inciPerd;
	private int ustmno;
	private BigDecimal contractAmount;
	private String fundCurrency;
	private BigDecimal fundAmount;
	private BigDecimal fundRate;
	private String sacscode;
	private String sacstyp;
	private String genlcde;
	private String allInd;
	private String contractType;
	private String triggerModule;
	private String triggerKey;
	private int procSeqNo;
	private String switchIndicator;
	private String userProfile;
	private String jobName;
	private String datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getJobnoPrice() {
		return jobnoPrice;
	}
	public void setJobnoPrice(int jobnoPrice) {
		this.jobnoPrice = jobnoPrice;
	}
	public String getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(String unitVirtualFund) {
		this.unitVirtualFund = unitVirtualFund;
	}
	public String getUnitSubAccount() {
		return unitSubAccount;
	}
	public void setUnitSubAccount(String unitSubAccount) {
		this.unitSubAccount = unitSubAccount;
	}
	public int getStrpdate() {
		return strpdate;
	}
	public void setStrpdate(int strpdate) {
		this.strpdate = strpdate;
	}
	public String getNowDeferInd() {
		return nowDeferInd;
	}
	public void setNowDeferInd(String nowDeferInd) {
		this.nowDeferInd = nowDeferInd;
	}
	public BigDecimal getNofUnits() {
		return nofUnits;
	}
	public void setNofUnits(BigDecimal nofUnits) {
		this.nofUnits = nofUnits;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public int getPriceDateUsed() {
		return priceDateUsed;
	}
	public void setPriceDateUsed(int priceDateUsed) {
		this.priceDateUsed = priceDateUsed;
	}
	public int getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(int moniesDate) {
		this.moniesDate = moniesDate;
	}
	public BigDecimal getPriceUsed() {
		return priceUsed;
	}
	public void setPriceUsed(BigDecimal priceUsed) {
		this.priceUsed = priceUsed;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public BigDecimal getNofDunits() {
		return nofDunits;
	}
	public void setNofDunits(BigDecimal nofDunits) {
		this.nofDunits = nofDunits;
	}
	public String getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(String feedbackInd) {
		this.feedbackInd = feedbackInd;
	}
	public BigDecimal getUnitBarePrice() {
		return unitBarePrice;
	}
	public void setUnitBarePrice(BigDecimal unitBarePrice) {
		this.unitBarePrice = unitBarePrice;
	}
	public int getInciNum() {
		return inciNum;
	}
	public void setInciNum(int inciNum) {
		this.inciNum = inciNum;
	}
	public int getInciPerd() {
		return inciPerd;
	}
	public void setInciPerd(int inciPerd) {
		this.inciPerd = inciPerd;
	}
	public int getUstmno() {
		return ustmno;
	}
	public void setUstmno(int ustmno) {
		this.ustmno = ustmno;
	}
	public BigDecimal getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(BigDecimal contractAmount) {
		this.contractAmount = contractAmount;
	}
	public String getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(String fundCurrency) {
		this.fundCurrency = fundCurrency;
	}
	public BigDecimal getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(BigDecimal fundAmount) {
		this.fundAmount = fundAmount;
	}
	public BigDecimal getFundRate() {
		return fundRate;
	}
	public void setFundRate(BigDecimal fundRate) {
		this.fundRate = fundRate;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getGenlcde() {
		return genlcde;
	}
	public void setGenlcde(String genlcde) {
		this.genlcde = genlcde;
	}
	public String getAllInd() {
		return allInd;
	}
	public void setAllInd(String allInd) {
		this.allInd = allInd;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getTriggerModule() {
		return triggerModule;
	}
	public void setTriggerModule(String triggerModule) {
		this.triggerModule = triggerModule;
	}
	public String getTriggerKey() {
		return triggerKey;
	}
	public void setTriggerKey(String triggerKey) {
		this.triggerKey = triggerKey;
	}
	public int getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(int procSeqNo) {
		this.procSeqNo = procSeqNo;
	}
	public String getSwitchIndicator() {
		return switchIndicator;
	}
	public void setSwitchIndicator(String switchIndicator) {
		this.switchIndicator = switchIndicator;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
}