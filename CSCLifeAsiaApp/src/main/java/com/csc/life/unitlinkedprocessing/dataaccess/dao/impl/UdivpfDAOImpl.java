package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdivpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Udivpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UdivpfDAOImpl extends BaseDAOImpl<Udivpf> implements UdivpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UdivpfDAOImpl.class);

	public void deleteUdivpfRecord(List<Udivpf> udivpfList) {
		if (udivpfList != null && !udivpfList.isEmpty()) {
			StringBuilder sqlSelect = new StringBuilder(
					"DELETE FROM UDIVPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? ");
			PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
			
			try {
				for (Udivpf u : udivpfList) {
					int i = 1;
					ps.setString(i++, u.getChdrcoy());
					ps.setString(i++, u.getChdrnum());
					ps.setInt(i++, u.getTranno());
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("deleteUdivpfRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
	}
	
	public void insertUdivpfRecord(List<Udivpf> urList) {
		if (urList == null || !urList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO UDIVPF(CHDRCOY,CHDRNUM,TRANNO,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		
		try {
			for (Udivpf u : urList) {
				int i = 1;
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setInt(i++, u.getTranno());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertUdivpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
}