package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UderTableDAM.java
 * Date: Sun, 30 Aug 2009 03:50:52
 * Class transformed from UDER.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UderTableDAM extends UderpfTableDAM {

	public UderTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UDER");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "RECORDTYPE";
		
		QUALIFIEDCOLUMNS = 
		            "AGE, " +
		            "RECORDTYPE, " +
		            "LAPIND, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VRTFND, " +
		            "UNITYP, " +
		            "TRANNO, " +
		            "BATCTRCDE, " +
		            "CNTAMNT, " +
		            "CNTCURR, " +
		            "FUNDAMNT, " +
		            "FNDCURR, " +
		            "FUNDRATE, " +
		            "TRIGER, " +
		            "RELRECNO, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "RECORDTYPE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "RECORDTYPE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               age,
                               recordtype,
                               lapind,
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               unitVirtualFund,
                               unitType,
                               tranno,
                               batctrcde,
                               contractAmount,
                               cntcurr,
                               fundAmount,
                               fundCurrency,
                               fundRate,
                               triggerModule,
                               relativeRecordNo,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(60);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getRecordtype().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, recordtype);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(4);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(recordtype.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(132);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ getLapind().toInternal()
					+ getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ getTranno().toInternal()
					+ getBatctrcde().toInternal()
					+ getContractAmount().toInternal()
					+ getCntcurr().toInternal()
					+ getFundAmount().toInternal()
					+ getFundCurrency().toInternal()
					+ getFundRate().toInternal()
					+ getAge().toInternal()
					+ getTriggerModule().toInternal()
					+ getRelativeRecordNo().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, lapind);
			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, contractAmount);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, fundAmount);
			what = ExternalData.chop(what, fundCurrency);
			what = ExternalData.chop(what, fundRate);
			what = ExternalData.chop(what, age);
			what = ExternalData.chop(what, triggerModule);
			what = ExternalData.chop(what, relativeRecordNo);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRecordtype() {
		return recordtype;
	}
	public void setRecordtype(Object what) {
		recordtype.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLapind() {
		return lapind;
	}
	public void setLapind(Object what) {
		lapind.set(what);
	}	
	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}	
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}	
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public PackedDecimalData getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(Object what) {
		setContractAmount(what, false);
	}
	public void setContractAmount(Object what, boolean rounded) {
		if (rounded)
			contractAmount.setRounded(what);
		else
			contractAmount.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public PackedDecimalData getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(Object what) {
		setFundAmount(what, false);
	}
	public void setFundAmount(Object what, boolean rounded) {
		if (rounded)
			fundAmount.setRounded(what);
		else
			fundAmount.set(what);
	}	
	public FixedLengthStringData getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(Object what) {
		fundCurrency.set(what);
	}	
	public PackedDecimalData getFundRate() {
		return fundRate;
	}
	public void setFundRate(Object what) {
		setFundRate(what, false);
	}
	public void setFundRate(Object what, boolean rounded) {
		if (rounded)
			fundRate.setRounded(what);
		else
			fundRate.set(what);
	}	
	public PackedDecimalData getAge() {
		return age;
	}
	public void setAge(Object what) {
		setAge(what, false);
	}
	public void setAge(Object what, boolean rounded) {
		if (rounded)
			age.setRounded(what);
		else
			age.set(what);
	}	
	public FixedLengthStringData getTriggerModule() {
		return triggerModule;
	}
	public void setTriggerModule(Object what) {
		triggerModule.set(what);
	}	
	public PackedDecimalData getRelativeRecordNo() {
		return relativeRecordNo;
	}
	public void setRelativeRecordNo(Object what) {
		setRelativeRecordNo(what, false);
	}
	public void setRelativeRecordNo(Object what, boolean rounded) {
		if (rounded)
			relativeRecordNo.setRounded(what);
		else
			relativeRecordNo.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		recordtype.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		lapind.clear();
		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		unitVirtualFund.clear();
		unitType.clear();
		tranno.clear();
		batctrcde.clear();
		contractAmount.clear();
		cntcurr.clear();
		fundAmount.clear();
		fundCurrency.clear();
		fundRate.clear();
		age.clear();
		triggerModule.clear();
		relativeRecordNo.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}