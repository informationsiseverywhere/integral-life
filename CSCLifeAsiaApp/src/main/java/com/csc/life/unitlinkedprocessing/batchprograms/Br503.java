/*
 * File: Br503.java
 * Date: 29 August 2009 22:10:00
 * Author: Quipoz Limited
 * 
 * Class transformed from BR503.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.unitlinkedprocessing.reports.Rr503Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*              Sterling Unit Reserve Report
*              ----------------------------
*
*  This program will report all contracts found in the temporary
*  file RTURPF, created in BR502.
*  The Actual Amount is calculated based on :
*
*            No. of Units * Unit Price
*
*  This derived amount will be compared against the Sum Insured.
*  If the Sum Insured is greater than the Actual Amount, the Net
*  Risk is the difference. Otherwise, the Net Risk is nil.
*
*****************************************************************
* </pre>
*/
public class Br503 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlrturpf1rs = null;
	private java.sql.PreparedStatement sqlrturpf1ps = null;
	private java.sql.Connection sqlrturpf1conn = null;
	private String sqlrturpf1 = "";
	private Rr503Report printerFile = new Rr503Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(320);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR503");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
		/* ERRORS */
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t5515 = "T5515";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-RTURPF */
	//ILIFE-1214 by smalchi2 STARTS
	private FixedLengthStringData sqlRturrec = new FixedLengthStringData(62);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlRturrec, 0);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlRturrec, 8);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlRturrec, 10);
	private PackedDecimalData sqlSumins = new PackedDecimalData(17, 2).isAPartOf(sqlRturrec, 14);
	private FixedLengthStringData sqlVfund = new FixedLengthStringData(4).isAPartOf(sqlRturrec, 31);
	private PackedDecimalData sqlCurduntbal = new PackedDecimalData(16, 5).isAPartOf(sqlRturrec, 35);
	private PackedDecimalData sqlUbidpr = new PackedDecimalData(9, 5).isAPartOf(sqlRturrec, 51);
	private PackedDecimalData sqlAge = new PackedDecimalData(3, 0).isAPartOf(sqlRturrec, 60);
    //ENDS
	
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaUnitFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(4).isAPartOf(wsaaUnitFn, 0, FILLER).init("RTUR");
	private FixedLengthStringData wsaaUnitRunid = new FixedLengthStringData(2).isAPartOf(wsaaUnitFn, 4);
	private ZonedDecimalData wsaaUnitJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUnitFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler5 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData rr503H01 = new FixedLengthStringData(10);
	private FixedLengthStringData rr503h01O = new FixedLengthStringData(10).isAPartOf(rr503H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr503h01O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5515rec t5515rec = new T5515rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Rr503D01Inner rr503D01Inner = new Rr503D01Inner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2060, 
		exit2090
	}

	public Br503() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaChdrnum.set(SPACES);
		wsaaOverflow.set("Y");
		wsaaUnitRunid.set(bprdIO.getSystemParam04());
		wsaaUnitJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(RTURPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaUnitFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Select RTUR records*/
		/* Define the query required by declaring a cursor*/
		/* Retrieve all records that exists*/
		//ILIFE-1214 by smalchi2 STARTS
		sqlrturpf1 = " SELECT  CHDRNUM, STATCODE, CRTABLE, SUMINS, VFUND, CURDUNTBAL, UBIDPR, AGE, MEMBER_NAME" +
" FROM   " + getAppVars().getTableNameOverriden("RTURPF") + " " +
" ORDER BY CHDRNUM";
		//ENDS
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlrturpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.unitlinkedprocessing.dataaccess.RturpfTableDAM());
			sqlrturpf1ps = getAppVars().prepareStatementEmbeded(sqlrturpf1conn, sqlrturpf1, "RTURPF");
			sqlrturpf1rs = getAppVars().executeQuery(sqlrturpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case eof2060: 
					eof2060();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlrturpf1rs)) {
				getAppVars().getDBObject(sqlrturpf1rs, 1, sqlChdrnum);
				getAppVars().getDBObject(sqlrturpf1rs, 2, sqlStatcode);
				getAppVars().getDBObject(sqlrturpf1rs, 3, sqlCrtable);
				getAppVars().getDBObject(sqlrturpf1rs, 4, sqlSumins);
				getAppVars().getDBObject(sqlrturpf1rs, 5, sqlVfund);
				getAppVars().getDBObject(sqlrturpf1rs, 6, sqlCurduntbal);
				getAppVars().getDBObject(sqlrturpf1rs, 7, sqlUbidpr);
				getAppVars().getDBObject(sqlrturpf1rs, 8, sqlAge);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		if (newPageReq.isTrue()) {
			printerFile.printRr503h01(rr503H01);
			wsaaOverflow.set("N");
		}
		if (isNE(sqlChdrnum,wsaaChdrnum)) {
			rr503D01Inner.chdrnum.set(sqlChdrnum);
			wsaaChdrnum.set(sqlChdrnum);
			rr503D01Inner.sumins.set(sqlSumins);
			rr503D01Inner.age.set(sqlAge);
			rr503D01Inner.crtable.set(sqlCrtable);
			rr503D01Inner.statcode.set(sqlStatcode);
		}
		else {
			rr503D01Inner.chdrnum.set(SPACES);
			rr503D01Inner.crtable.set(SPACES);
			rr503D01Inner.statcode.set(SPACES);
			rr503D01Inner.sumins.set(ZERO);
			rr503D01Inner.age.set(ZERO);
		}
		rr503D01Inner.vfund.set(sqlVfund);
		rr503D01Inner.curduntbal.set(sqlCurduntbal);
		rr503D01Inner.ubidpr.set(sqlUbidpr);
		getFundCurrency6000();
		compute(rr503D01Inner.fundvalc, 6).setRounded(mult(sqlCurduntbal, sqlUbidpr));
		/*    MOVE FUNDVALC OF RR503D01-O  TO ZRDP-AMOUNT-IN.              */
		/*    PERFORM 5000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT         TO FUNDVALC OF RR503D01-O.      */
		if (isNE(rr503D01Inner.fundvalc, ZERO)) {
			zrdecplrec.amountIn.set(rr503D01Inner.fundvalc);
			callRounding5000();
			rr503D01Inner.fundvalc.set(zrdecplrec.amountOut);
		}
		if (isLT(rr503D01Inner.fundvalc, sqlSumins)) {
			compute(rr503D01Inner.fundvalb, 3).setRounded(sub(sqlSumins, rr503D01Inner.fundvalc));
		}
		else {
			rr503D01Inner.fundvalb.set(ZERO);
		}
		/*    MOVE FUNDVALB OF RR503D01-O  TO ZRDP-AMOUNT-IN.              */
		/*    PERFORM 5000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT         TO FUNDVALB OF RR503D01-O.      */
		if (isNE(rr503D01Inner.fundvalb, ZERO)) {
			zrdecplrec.amountIn.set(rr503D01Inner.fundvalb);
			callRounding5000();
			rr503D01Inner.fundvalb.set(zrdecplrec.amountOut);
		}
		printerFile.printRr503d01(rr503D01Inner.rr503D01);
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		wsaaQcmdexc.set("DLTOVR FILE(RTURPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(t5515rec.currcode);
		zrdecplrec.batctrcde.set("****");
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void getFundCurrency6000()
	{
		readT55156100();
	}

protected void readT55156100()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("2");
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(sqlVfund);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure RR503-D01--INNER
 */
private static final class Rr503D01Inner { 

	private FixedLengthStringData rr503D01 = new FixedLengthStringData(99);
	private FixedLengthStringData rr503d01O = new FixedLengthStringData(99).isAPartOf(rr503D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr503d01O, 0);
	private FixedLengthStringData statcode = new FixedLengthStringData(2).isAPartOf(rr503d01O, 8);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rr503d01O, 10);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(rr503d01O, 14);
	private FixedLengthStringData vfund = new FixedLengthStringData(4).isAPartOf(rr503d01O, 31);
	private ZonedDecimalData curduntbal = new ZonedDecimalData(16, 5).isAPartOf(rr503d01O, 35);
	private ZonedDecimalData ubidpr = new ZonedDecimalData(9, 5).isAPartOf(rr503d01O, 51);
	private ZonedDecimalData fundvalc = new ZonedDecimalData(18, 5).isAPartOf(rr503d01O, 60);
	private ZonedDecimalData age = new ZonedDecimalData(3, 0).isAPartOf(rr503d01O, 78);
	private ZonedDecimalData fundvalb = new ZonedDecimalData(18, 5).isAPartOf(rr503d01O, 81);
}
}
