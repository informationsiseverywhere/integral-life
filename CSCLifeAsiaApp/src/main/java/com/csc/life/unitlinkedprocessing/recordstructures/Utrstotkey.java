package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:54
 * Description:
 * Copybook name: UTRSTOTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrstotkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrstotFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData utrstotKey = new FixedLengthStringData(256).isAPartOf(utrstotFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrstotChdrcoy = new FixedLengthStringData(1).isAPartOf(utrstotKey, 0);
  	public FixedLengthStringData utrstotChdrnum = new FixedLengthStringData(8).isAPartOf(utrstotKey, 1);
  	public FixedLengthStringData utrstotLife = new FixedLengthStringData(2).isAPartOf(utrstotKey, 9);
  	public FixedLengthStringData utrstotCoverage = new FixedLengthStringData(2).isAPartOf(utrstotKey, 11);
  	public FixedLengthStringData utrstotRider = new FixedLengthStringData(2).isAPartOf(utrstotKey, 13);
  	public FixedLengthStringData utrstotUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrstotKey, 15);
  	public FixedLengthStringData utrstotUnitType = new FixedLengthStringData(1).isAPartOf(utrstotKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(236).isAPartOf(utrstotKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrstotFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrstotFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}