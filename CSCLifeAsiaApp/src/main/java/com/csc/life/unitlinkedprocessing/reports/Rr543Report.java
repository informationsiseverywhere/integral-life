package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR543.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr543Report extends SMARTReportLayout { 

	private FixedLengthStringData billcurr = new FixedLengthStringData(3);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData cownnum = new FixedLengthStringData(8);
	private FixedLengthStringData dateReportVariable = new FixedLengthStringData(10);
	private FixedLengthStringData freq = new FixedLengthStringData(2);
	private FixedLengthStringData origcurr = new FixedLengthStringData(3);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sacscode = new FixedLengthStringData(2);
	private ZonedDecimalData sacscurbal = new ZonedDecimalData(17, 2);
	private FixedLengthStringData sacstyp = new FixedLengthStringData(2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData sinstamt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData statcode = new FixedLengthStringData(2);
	private FixedLengthStringData statdets = new FixedLengthStringData(30);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr543Report() {
		super();
	}


	/**
	 * Print the XML for Rr543d01
	 */
	public void printRr543d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		cownnum.setFieldName("cownnum");
		cownnum.setInternal(subString(recordData, 9, 8));
		freq.setFieldName("freq");
		freq.setInternal(subString(recordData, 17, 2));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 19, 3));
		billcurr.setFieldName("billcurr");
		billcurr.setInternal(subString(recordData, 22, 3));
		sinstamt.setFieldName("sinstamt");
		sinstamt.setInternal(subString(recordData, 25, 17));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.setInternal(subString(recordData, 42, 10));
		sacscode.setFieldName("sacscode");
		sacscode.setInternal(subString(recordData, 52, 2));
		sacstyp.setFieldName("sacstyp");
		sacstyp.setInternal(subString(recordData, 54, 2));
		origcurr.setFieldName("origcurr");
		origcurr.setInternal(subString(recordData, 56, 3));
		sacscurbal.setFieldName("sacscurbal");
		sacscurbal.setInternal(subString(recordData, 59, 17));
		printLayout("Rr543d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				cownnum,
				freq,
				cntcurr,
				billcurr,
				sinstamt,
				dateReportVariable,
				sacscode,
				sacstyp,
				origcurr,
				sacscurbal
			}
		);

	}

	/**
	 * Print the XML for Rr543d02
	 */
	public void printRr543d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		sacscode.setFieldName("sacscode");
		sacscode.setInternal(subString(recordData, 1, 2));
		sacstyp.setFieldName("sacstyp");
		sacstyp.setInternal(subString(recordData, 3, 2));
		origcurr.setFieldName("origcurr");
		origcurr.setInternal(subString(recordData, 5, 3));
		sacscurbal.setFieldName("sacscurbal");
		sacscurbal.setInternal(subString(recordData, 8, 17));
		printLayout("Rr543d02",			// Record name
			new BaseData[]{			// Fields:
				sacscode,
				sacstyp,
				origcurr,
				sacscurbal
			}
		);

	}

	/**
	 * Print the XML for Rr543h01
	 */
	public void printRr543h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(21).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 32, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 42, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 44, 30));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("Rr543h01",			// Record name
			new BaseData[]{			// Fields:
				pagnbr,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time
			}
			, new Object[] {			// indicators
				new Object[]{"ind21", indicArea.charAt(21)}
			}
		);

		currentPrintLine.set(8);
	}

	/**
	 * Print the XML for Rr543h02
	 */
	public void printRr543h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(21).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(3);

		statcode.setFieldName("statcode");
		statcode.setInternal(subString(recordData, 1, 2));
		statdets.setFieldName("statdets");
		statdets.setInternal(subString(recordData, 3, 30));
		printLayout("Rr543h02",			// Record name
			new BaseData[]{			// Fields:
				statcode,
				statdets
			}
			, new Object[] {			// indicators
				new Object[]{"ind21", indicArea.charAt(21)}
			}
		);

		currentPrintLine.add(3);
	}

	/**
	 * Print the XML for Rr543h03
	 */
	public void printRr543h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rr543h03",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rr543h04
	 */
	public void printRr543h04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(21).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		printLayout("Rr543h04",			// Record name
			new BaseData[]{			// Fields:
			}
			, new Object[] {			// indicators
				new Object[]{"ind21", indicArea.charAt(21)}
			}
		);

	}

	/**
	 * Print the XML for Rr543h05
	 */
	public void printRr543h05(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(21).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		printLayout("Rr543h05",			// Record name
			new BaseData[]{			// Fields:
			}
			, new Object[] {			// indicators
				new Object[]{"ind21", indicArea.charAt(21)}
			}
		);

	}


}
