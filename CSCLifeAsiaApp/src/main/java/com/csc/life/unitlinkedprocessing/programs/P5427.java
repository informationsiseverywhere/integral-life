/*
 * File: P5427.java
 * Date: 30 August 2009 0:25:00
 * Author: Quipoz Limited
 * 
 * Class transformed from P5427.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.recordstructures.Bidoffrec;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupdTableDAM;
import com.csc.life.unitlinkedprocessing.procedures.Bidoff;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.smart.cls.Jobsws;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Jobswsrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.programs.Infosdsp;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   P5427 - Calculate Bid/Offer Price.
*
*   This program allows the user to calculate the bid/offer price
*   of all the funds within a Company/Job Number/Effective Date
*   combination.
*
*   It is called from the Unit Linked Entry submenu(P5412) by
*   means of selecting the appropriate option.
*
*   The program flow is as follows:-
*
*      Initialisation.
*
*      Read VPRCUPD sequentially with the key given.
*      For each valid record, call BIDOFF to calculate the
*      Bid and Offer price. The is to be repeated until the key
*      of the record read has changed.
*
*      Update each valid record with the calculated prices and
*      valid flag '3'.
*
*      Return control to calling program.
*
*
*****************************************************************
* </pre>
*/
public class P5427 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5427");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUnitType = "";
	private FixedLengthStringData wsaaFund = new FixedLengthStringData(4);
	private ZonedDecimalData acumUnitBidPrice = new ZonedDecimalData(9, 5).setUnsigned();
	private ZonedDecimalData acumUnitOfferPrice = new ZonedDecimalData(9, 5).setUnsigned();
	private FixedLengthStringData acumStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData acumProcflag = new FixedLengthStringData(2);
	private ZonedDecimalData initUnitBidPrice = new ZonedDecimalData(9, 5).setUnsigned();
	private ZonedDecimalData initUnitOfferPrice = new ZonedDecimalData(9, 5).setUnsigned();
	private FixedLengthStringData initStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData initProcflag = new FixedLengthStringData(2);
	private String wsaaLoadMessage = "               New Bid/Offer Prices Being Calculated";
		/* FORMATS */
	private String vprcupdrec = "VPRCUPDREC";
	private Bidoffrec bidoffrec = new Bidoffrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Jobswsrec jobswsrec = new Jobswsrec();
		/*Unit Price file*/
	private VprcupdTableDAM vprcupdIO = new VprcupdTableDAM();
	private Wsspuprc wsspuprc = new Wsspuprc();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		callVprcupdio2005, 
		calculateBidoffer2100, 
		exit2900, 
		exit5900, 
		exit5950
	}

	public P5427() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.errorline.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		callProgram(Jobsws.class, jobswsrec.jobswsRec);
		if (isEQ(jobswsrec.switch1,"0")) {
			scrnparams.errorline.set(wsaaLoadMessage);
			scrnparams.function.set(varcom.info);
			callProgram(Infosdsp.class, scrnparams.screenParams);
		}
		/*EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					vprcupdio2001();
				}
				case callVprcupdio2005: {
					callVprcupdio2005();
				}
				case calculateBidoffer2100: {
					calculateBidoffer2100();
				}
				case exit2900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void vprcupdio2001()
	{
		acumProcflag.set(SPACES);
		initProcflag.set(SPACES);
		vprcupdIO.setDataArea(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		//performance improvement -- < Niharika Modi >
		vprcupdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcupdIO.setFitKeysSearch( "COMPANY","EFFDATE","JOBNO");
		
	}

protected void callVprcupdio2005()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2900);
		}
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate))
		|| (isNE(vprcupdIO.getCompany(),wsspcomn.company))
		|| (isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno))) {
			goTo(GotoLabel.exit2900);
		}
		if (isEQ(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.calculateBidoffer2100);
		}
		else {
			vprcupdIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callVprcupdio2005);
		}
	}

protected void calculateBidoffer2100()
	{
		bidoffrec.fund.set(vprcupdIO.getUnitVirtualFund());
		wsaaFund.set(vprcupdIO.getUnitVirtualFund());
		bidoffrec.barePrice.set(vprcupdIO.getUnitBarePrice());
		bidoffrec.unitType.set(vprcupdIO.getUnitType());
		bidoffrec.effdate.set(vprcupdIO.getEffdate());
		bidoffrec.company.set(wsspcomn.company);
		bidoffrec.language.set(wsspcomn.language);
		callProgram(Bidoff.class, bidoffrec.bidoffRec);
		if (isNE(bidoffrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(bidoffrec.statuz);
			syserrrec.params.set(bidoffrec.bidoffRec);
			fatalError600();
		}
		if (isEQ(vprcupdIO.getUnitType(),"A")) {
			acumUnitBidPrice.set(bidoffrec.bidPrice);
			acumUnitOfferPrice.set(bidoffrec.offerPrice);
			acumStatuz.set(bidoffrec.statuz);
			acumProcflag.set("Y");
			callVprcupdioUpdate5000();
		}
		else {
			initUnitBidPrice.set(bidoffrec.bidPrice);
			initUnitOfferPrice.set(bidoffrec.offerPrice);
			initStatuz.set(bidoffrec.statuz);
			initProcflag.set("Y");
			callVprcupdioUpdate5050();
		}
		vprcupdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callVprcupdio2005);
	}

protected void updateVprcupd3000()
	{
		update3000();
	}

protected void update3000()
	{
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callVprcupdioUpdate5000()
	{
		try {
			acum5000();
		}
		catch (GOTOException e){
		}
	}

protected void acum5000()
	{
		vprcupdIO.setFunction(varcom.readh);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(wsaaFund);
		wsaaUnitType = "A";
		vprcupdIO.setUnitType(wsaaUnitType);
		call5300();
		if (isEQ(vprcupdIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit5900);
		}
		vprcupdIO.setUnitBidPrice(acumUnitBidPrice);
		vprcupdIO.setUnitOfferPrice(acumUnitOfferPrice);
		vprcupdIO.setStatuz(acumStatuz);
		vprcupdIO.setProcflag(acumProcflag);
		update5400();
		goTo(GotoLabel.exit5900);
	}

protected void callVprcupdioUpdate5050()
	{
		try {
			init5050();
		}
		catch (GOTOException e){
		}
	}

protected void init5050()
	{
		vprcupdIO.setFunction(varcom.readh);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(wsaaFund);
		wsaaUnitType = "I";
		vprcupdIO.setUnitType(wsaaUnitType);
		call5300();
		if (isEQ(vprcupdIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit5950);
		}
		vprcupdIO.setUnitBidPrice(initUnitBidPrice);
		vprcupdIO.setUnitOfferPrice(initUnitOfferPrice);
		vprcupdIO.setStatuz(initStatuz);
		vprcupdIO.setProcflag(initProcflag);
		update5400();
	}

protected void call5300()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getCompany(),wsspcomn.company)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| isNE(vprcupdIO.getUnitVirtualFund(),wsaaFund)
		|| isNE(vprcupdIO.getUnitType(),wsaaUnitType)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void update5400()
	{
		/*PARA*/
		vprcupdIO.setValidflag("3");
		vprcupdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
}
