package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:21
 * Description:
 * Copybook name: T5515REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5515rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5515Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData acumbof = new ZonedDecimalData(8, 5).isAPartOf(t5515Rec, 0);
  	public ZonedDecimalData accumRounding = new ZonedDecimalData(3, 2).isAPartOf(t5515Rec, 8);
  	public FixedLengthStringData btobid = new FixedLengthStringData(1).isAPartOf(t5515Rec, 11);
  	public FixedLengthStringData btodisc = new FixedLengthStringData(1).isAPartOf(t5515Rec, 12);
  	public ZonedDecimalData discountOfferPercent = new ZonedDecimalData(5, 2).isAPartOf(t5515Rec, 13);
  	public FixedLengthStringData btooff = new FixedLengthStringData(1).isAPartOf(t5515Rec, 18);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(t5515Rec, 19);
  	public ZonedDecimalData initBidOffer = new ZonedDecimalData(8, 5).isAPartOf(t5515Rec, 22);
  	public ZonedDecimalData initialRounding = new ZonedDecimalData(3, 2).isAPartOf(t5515Rec, 30);
  	public FixedLengthStringData initAccumSame = new FixedLengthStringData(1).isAPartOf(t5515Rec, 33);
  	public ZonedDecimalData managementCharge = new ZonedDecimalData(5, 2).isAPartOf(t5515Rec, 34);
  	public FixedLengthStringData otoff = new FixedLengthStringData(1).isAPartOf(t5515Rec, 39);
  	public ZonedDecimalData tolerance = new ZonedDecimalData(4, 2).isAPartOf(t5515Rec, 40);
  	public FixedLengthStringData unitType = new FixedLengthStringData(1).isAPartOf(t5515Rec, 44);
  	public FixedLengthStringData zfundtyp = new FixedLengthStringData(1).isAPartOf(t5515Rec, 45);
  	public FixedLengthStringData filler = new FixedLengthStringData(454).isAPartOf(t5515Rec, 46, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5515Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5515Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}