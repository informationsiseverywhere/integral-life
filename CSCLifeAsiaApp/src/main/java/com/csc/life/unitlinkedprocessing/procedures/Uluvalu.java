/*
 * File: Uluvalu.java
 * Date: 30 August 2009 2:48:57
 * Author: Quipoz Limited
 * 
 * Class transformed from ULUVALU.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprdudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Cbndefind;
import com.csc.life.unitlinkedprocessing.recordstructures.Cbunityps;
import com.csc.life.unitlinkedprocessing.recordstructures.Uluvalurec;
import com.csc.life.unitlinkedprocessing.recordstructures.Utrskey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This subroutine is used to determine the value fo the unit
* holding (of either type) for a particular coverage.
*
* Function TOTAL:
* Calculate the value of the entire unit holding, across all
* funds, for the chosen unit type.
*
* Function FUND :
* Will calculate the holding in a particular fund for the chosen
* unit type.
*
* Function NEXT :
* Will fund the next fund invested in, after the value of fund
* passed in, and pass back its unit holding in the specified
* unit type.  If 'FUND' = spaces then the first fund will
* be found and evaluated
* a status of 'ENDP' indicates that all funds invested in have
* been processed.
*
* Function DEBT:
* This will return the value of the debt against the coverage
* or zero if none is found.
*
* This will be called from various parts of the claims and
* 'CASH VALUE INQUIRY' subsystems from both the 'ON-LINE' AND
* 'BATCH' environments.
*
*
* STATI
* '****'      OK!                 (all functions)
* 'ENDP'      All funds processed (Function 'NEXT' only)
*  E049       Invalid function
*  G178       No units for unit type (Function 'TOTAL')
*  G205       No units in chosen fund (Function 'FUND')
*  G250       Required price unavailable
****************************************************************
* </pre>
*/
public class Uluvalu extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ULUVALU";
	private PackedDecimalData wsaaValue = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaRealValue = new PackedDecimalData(17, 5);
	private ZonedDecimalData wsaaUnitBidPrice = new ZonedDecimalData(9, 5).init(0);
	private FixedLengthStringData wsaaFoundRec = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1);
	private Validator initialUnits = new Validator(wsaaUnitType, "I");
	private Validator accumulationUnits = new Validator(wsaaUnitType, "A");
		/* ERRORS */
	private String e049 = "E049";
	private String g178 = "G178";
	private String g205 = "G205";
		/* FORMATS */
	private String utrsrec = "UTRSREC";
	private String vprdudlrec = "VPRDUDLREC";
	private String vprnudlrec = "VPRNUDLREC";
	private Cbndefind cbndefind = new Cbndefind();
	private Cbunityps cbunityps = new Cbunityps();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Uluvalurec uluvalurec = new Uluvalurec();
		/*Unit transaction summary*/
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Varcom varcom = new Varcom();
		/*VPRCPF view for deferred units*/
	private VprdudlTableDAM vprdudlIO = new VprdudlTableDAM();
		/*VPRCPF view for now price for units*/
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Utrskey wsaaUtrskey = new Utrskey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		fundPrice105, 
		exit190, 
		readNextUtrs205, 
		fundPrice210, 
		exit290, 
		read1220, 
		notFound1280, 
		exit1290, 
		nowPrice1500, 
		exit1590
	}

	public Uluvalu() {
		super();
	}

public void mainline(Object... parmArray)
	{
		uluvalurec.uluvaluRec = convertAndSetParam(uluvalurec.uluvaluRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		start000();
		exit000();
	}

protected void start000()
	{
		syserrrec.subrname.set(wsaaSubr);
		uluvalurec.statuz.set(varcom.oK);
		if (isEQ(uluvalurec.function,"FUND ")
		|| isEQ(uluvalurec.function,"NEXT ")
		|| isEQ(uluvalurec.function,"DEBT ")) {
			fundMainline100();
		}
		else {
			if (isEQ(uluvalurec.function,"TOTAL ")) {
				totalMainline200();
			}
			else {
				uluvalurec.statuz.set(e049);
			}
		}
	}

protected void exit000()
	{
		exitProgram();
	}

protected void stop000()
	{
		stopRun();
	}

protected void fundMainline100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para100();
				}
				case fundPrice105: {
					fundPrice105();
				}
				case exit190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		initialise1000();
		wsaaUtrskey.utrsUnitVirtualFund.set(uluvalurec.vrtfnd);
		readUtrs1200();
		if (isEQ(uluvalurec.statuz,varcom.oK)) {
			goTo(GotoLabel.fundPrice105);
		}
		if (isEQ(uluvalurec.function,"NEXT")
		&& isNE(uluvalurec.vrtfnd,SPACES)) {
			uluvalurec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit190);
		}
		if (isEQ(uluvalurec.function,"DEBT")) {
			uluvalurec.statuz.set(varcom.oK);
			uluvalurec.value.set(ZERO);
			goTo(GotoLabel.exit190);
		}
		uluvalurec.statuz.set(g205);
		goTo(GotoLabel.exit190);
	}

protected void fundPrice105()
	{
		if (isEQ(uluvalurec.function,"DEBT")) {
			uluvalurec.value.set(0);
			goTo(GotoLabel.exit190);
		}
		uluvalurec.nofDunits.set(utrsIO.getCurrentDunitBal());
		uluvalurec.nofUnits.set(utrsIO.getCurrentUnitBal());
		if (isEQ(uluvalurec.function,"NEXT")) {
			uluvalurec.vrtfnd.set(utrsIO.getUnitVirtualFund());
		}
		findPrice1400();
		evalute1600();
	}

protected void totalMainline200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para200();
				}
				case readNextUtrs205: {
					readNextUtrs205();
				}
				case fundPrice210: {
					fundPrice210();
				}
				case exit290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para200()
	{
		initialise1000();
	}

protected void readNextUtrs205()
	{
		readUtrs1200();
		if (isEQ(uluvalurec.statuz,varcom.oK)) {
			goTo(GotoLabel.fundPrice210);
		}
		if (isEQ(wsaaFoundRec,"Y")) {
			uluvalurec.statuz.set(varcom.oK);
		}
		else {
			uluvalurec.statuz.set(g178);
		}
		goTo(GotoLabel.exit290);
	}

protected void fundPrice210()
	{
		uluvalurec.nofFunds.add(1);
		findPrice1400();
		evalute1600();
		goTo(GotoLabel.readNextUtrs205);
	}

protected void systemError570()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		uluvalurec.statuz.set(varcom.bomb);
		exit000();
	}

protected void databaseError580()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		uluvalurec.statuz.set(varcom.bomb);
		exit000();
	}

protected void initialise1000()
	{
		para1010();
	}

protected void para1010()
	{
		utrsIO.setParams(SPACES);
		wsaaUtrskey.utrsKey.set(SPACES);
		wsaaFoundRec.set(SPACES);
		wsaaValue.set(ZERO);
		wsaaRealValue.set(ZERO);
		uluvalurec.value.set(ZERO);
		uluvalurec.realValue.set(ZERO);
		uluvalurec.nofDunits.set(ZERO);
		uluvalurec.nofUnits.set(ZERO);
		uluvalurec.nofFunds.set(ZERO);
		cbndefind.nowDeferInd.set(uluvalurec.ndfind);
		wsaaUtrskey.utrsChdrcoy.set(uluvalurec.chdrChdrcoy);
		wsaaUtrskey.utrsChdrnum.set(uluvalurec.chdrChdrnum);
		wsaaUtrskey.utrsLife.set(uluvalurec.lifeLife);
		wsaaUtrskey.utrsCoverage.set(uluvalurec.covrCoverage);
		wsaaUtrskey.utrsRider.set(uluvalurec.covrRider);
		wsaaUtrskey.utrsPlanSuffix.set(uluvalurec.planSuffix);
		wsaaUtrskey.utrsUnitType.set(uluvalurec.unitType);
		wsaaUtrskey.utrsUnitVirtualFund.set(SPACES);
		cbunityps.unitType.set(uluvalurec.unitType);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		utrsIO.setFunction(varcom.begn);
		utrsIO.setFormat(utrsrec);
	}

protected void readUtrs1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1200();
				}
				case read1220: {
					read1220();
				}
				case notFound1280: {
					notFound1280();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1200()
	{
		if (isEQ(utrsIO.getFunction(),varcom.begn)) {
			utrsIO.setDataKey(wsaaUtrskey);
		}
	}

protected void read1220()
	{
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)) {
			if (isNE(utrsIO.getStatuz(),varcom.endp)) {
				syserrrec.dbparams.set(utrsIO.getParams());
				syserrrec.statuz.set(utrsIO.getStatuz());
				databaseError580();
			}
		}
		utrsIO.setFunction(varcom.nextr);
		if (isNE(utrsIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.notFound1280);
		}
		if ((isNE(utrsIO.getChdrcoy(),wsaaUtrskey.utrsChdrcoy))
		|| (isNE(utrsIO.getChdrnum(),wsaaUtrskey.utrsChdrnum))
		|| (isNE(utrsIO.getLife(),wsaaUtrskey.utrsLife))
		|| (isNE(utrsIO.getCoverage(),wsaaUtrskey.utrsCoverage))
		|| (isNE(utrsIO.getRider(),wsaaUtrskey.utrsRider))
		|| (isNE(utrsIO.getPlanSuffix(),wsaaUtrskey.utrsPlanSuffix))) {
			goTo(GotoLabel.notFound1280);
		}
		if ((isNE(utrsIO.getUnitVirtualFund(),wsaaUtrskey.utrsUnitVirtualFund))) {
			if (isEQ(uluvalurec.function,"FUND ")) {
				goTo(GotoLabel.notFound1280);
			}
		}
		if ((isEQ(utrsIO.getUnitVirtualFund(),wsaaUtrskey.utrsUnitVirtualFund))) {
			if (isEQ(uluvalurec.function,"NEXT ")) {
				goTo(GotoLabel.read1220);
			}
		}
		wsaaFoundRec.set("Y");
		goTo(GotoLabel.exit1290);
	}

protected void notFound1280()
	{
		uluvalurec.statuz.set(varcom.nogo);
	}

protected void findPrice1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1400();
				}
				case nowPrice1500: {
					nowPrice1500();
				}
				case exit1590: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1400()
	{
		if (cbndefind.now.isTrue()) {
			goTo(GotoLabel.nowPrice1500);
		}
		vprdudlIO.setParams(SPACES);
		vprdudlIO.setCompany(uluvalurec.chdrChdrcoy);
		vprdudlIO.setUnitType(uluvalurec.unitType);
		vprdudlIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		vprdudlIO.setEffdate(uluvalurec.priceEfd);
		vprdudlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprdudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprdudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		vprdudlIO.setFormat(vprdudlrec);
		SmartFileCode.execute(appVars, vprdudlIO);
		if ((isNE(vprdudlIO.getStatuz(),varcom.oK))
		&& (isNE(vprdudlIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(vprdudlIO.getStatuz());
			syserrrec.dbparams.set(vprdudlIO.getParams());
			databaseError580();
		}
		if (isEQ(vprdudlIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.nowPrice1500);
		}
		if ((isNE(vprdudlIO.getCompany(),uluvalurec.chdrChdrcoy))
		|| (isNE(vprdudlIO.getUnitType(),uluvalurec.unitType))
		|| (isNE(vprdudlIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund()))) {
			goTo(GotoLabel.nowPrice1500);
		}
		wsaaUnitBidPrice.set(vprdudlIO.getUnitBidPrice());
		goTo(GotoLabel.exit1590);
	}

protected void nowPrice1500()
	{
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(uluvalurec.chdrChdrcoy);
		vprnudlIO.setUnitType(uluvalurec.unitType);
		vprnudlIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		vprnudlIO.setEffdate(datcon1rec.intDate);
		vprnudlIO.setFunction(varcom.begn);
		vprnudlIO.setFormat(vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if ((isNE(vprnudlIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(vprnudlIO.getStatuz());
			syserrrec.dbparams.set(vprnudlIO.getParams());
			databaseError580();
		}
		wsaaUnitBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void evalute1600()
	{
		/*PARA*/
		if (initialUnits.isTrue()) {
			evalInit1800();
		}
		else {
			evalAccum1900();
		}
		compute(uluvalurec.value, 6).setRounded(add(uluvalurec.value,wsaaValue));
		compute(uluvalurec.realValue, 6).setRounded(add(uluvalurec.realValue,wsaaRealValue));
		/*EXIT*/
	}

protected void evalInit1800()
	{
		/*PARA*/
		compute(wsaaValue, 6).setRounded(mult(utrsIO.getCurrentDunitBal(),wsaaUnitBidPrice));
		/*EXIT*/
	}

protected void evalAccum1900()
	{
		/*PARA*/
		compute(wsaaValue, 6).setRounded(mult(utrsIO.getCurrentDunitBal(),wsaaUnitBidPrice));
		compute(wsaaRealValue, 6).setRounded(mult(utrsIO.getCurrentUnitBal(),wsaaUnitBidPrice));
		/*EXIT*/
	}
}
