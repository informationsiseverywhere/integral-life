package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5540
 * @version 1.0 generated on 30/08/09 06:42
 * @author Quipoz
 */
public class S5540ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(408);
	public FixedLengthStringData dataFields = new FixedLengthStringData(104).isAPartOf(dataArea, 0);
	public FixedLengthStringData alfnds = DD.alfnds.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData allbas = DD.allbas.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData fundSplitPlan = DD.fundsp.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,12);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,20);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,28);
	public FixedLengthStringData iuDiscFact = DD.iudisc.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData iuDiscBasis = DD.iudiscbas.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData ltypst = DD.ltypst.copy().isAPartOf(dataFields,74);
	public ZonedDecimalData maxfnd = DD.maxfnd.copyToZonedDecimal().isAPartOf(dataFields,78);
	public ZonedDecimalData rvwproc = DD.rvwproc.copyToZonedDecimal().isAPartOf(dataFields,80);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData unitCancInit = DD.ucanin.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData wdmeth = DD.wdmeth.copy().isAPartOf(dataFields,88);
	public FixedLengthStringData wholeIuDiscFact = DD.whoiudisc.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData zafropt1 = DD.zafr.copy().isAPartOf(dataFields,96);;//BRD-411
	public FixedLengthStringData zvariance = DD.zvariance.copy().isAPartOf(dataFields,100);;//BRD-411
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 104);
	public FixedLengthStringData alfndsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData allbasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData fundspErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData iudiscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData iudiscbasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ltypstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData maxfndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData rvwprocErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ucaninErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData wdmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData whoiudiscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData zafropt1Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);//BRD-411
	public FixedLengthStringData zvarianceErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);//BRD-411
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 176);
	public FixedLengthStringData[] alfndsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] allbasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] fundspOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] iudiscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] iudiscbasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ltypstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] maxfndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] rvwprocOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ucaninOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] wdmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] whoiudiscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] zafropt1Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);//BRD-411
	public FixedLengthStringData[] zvarianceOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);//BRD-411    
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5540screenWritten = new LongData(0);
	public LongData S5540protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5540ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(allbasOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rvwprocOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fundspOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(iudiscbasOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ltypstOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(iudiscOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(wdmethOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(alfndsOut,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxfndOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(whoiudiscOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ucaninOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zafropt1Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, allbas, rvwproc, fundSplitPlan, iuDiscBasis, ltypst, iuDiscFact, wdmeth, alfnds, maxfnd, wholeIuDiscFact, unitCancInit,zafropt1,zvariance};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, allbasOut, rvwprocOut, fundspOut, iudiscbasOut, ltypstOut, iudiscOut, wdmethOut, alfndsOut, maxfndOut, whoiudiscOut, ucaninOut,zafropt1Out,zvarianceOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, allbasErr, rvwprocErr, fundspErr, iudiscbasErr, ltypstErr, iudiscErr, wdmethErr, alfndsErr, maxfndErr, whoiudiscErr, ucaninErr,zafropt1Err,zvarianceErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5540screen.class;
		protectRecord = S5540protect.class;
	}

}
