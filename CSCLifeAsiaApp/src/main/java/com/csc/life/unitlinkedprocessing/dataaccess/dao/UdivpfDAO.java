package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Udivpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UdivpfDAO extends BaseDAO<Udivpf> {
	public void deleteUdivpfRecord(List<Udivpf> udivpfList);
	public void insertUdivpfRecord(List<Udivpf> urList);
}