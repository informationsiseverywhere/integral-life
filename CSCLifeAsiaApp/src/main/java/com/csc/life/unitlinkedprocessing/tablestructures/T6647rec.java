package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:52
 * Description:
 * Copybook name: T6647REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6647rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6647Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData aloind = new FixedLengthStringData(1).isAPartOf(t6647Rec, 0);
  	public FixedLengthStringData bidoffer = new FixedLengthStringData(1).isAPartOf(t6647Rec, 1);
  	public FixedLengthStringData dealin = new FixedLengthStringData(1).isAPartOf(t6647Rec, 2);
  	public FixedLengthStringData efdcode = new FixedLengthStringData(2).isAPartOf(t6647Rec, 3);
  	public FixedLengthStringData enhall = new FixedLengthStringData(4).isAPartOf(t6647Rec, 5);
  	public ZonedDecimalData monthsDebt = new ZonedDecimalData(3, 0).isAPartOf(t6647Rec, 9);
  	public ZonedDecimalData monthsError = new ZonedDecimalData(3, 0).isAPartOf(t6647Rec, 12);
  	public ZonedDecimalData monthsLapse = new ZonedDecimalData(3, 0).isAPartOf(t6647Rec, 15);
  	public ZonedDecimalData monthsNegUnits = new ZonedDecimalData(3, 0).isAPartOf(t6647Rec, 18);
  	public ZonedDecimalData procSeqNo = new ZonedDecimalData(3, 0).isAPartOf(t6647Rec, 21);
  	public FixedLengthStringData unitStatMethod = new FixedLengthStringData(4).isAPartOf(t6647Rec, 24);
  	public FixedLengthStringData swmeth = new FixedLengthStringData(4).isAPartOf(t6647Rec, 28);
  	public FixedLengthStringData moniesDate = new FixedLengthStringData(8).isAPartOf(t6647Rec, 32);
  	public FixedLengthStringData filler = new FixedLengthStringData(460).isAPartOf(t6647Rec, 40, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6647Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6647Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}