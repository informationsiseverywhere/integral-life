package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Br608DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br610TempDAO extends BaseDAO<Br610DTO> {

	public Map<String, List<Br610DTO>> getRecords(String company, int scheduleNumber);
	public Map<String, List<Br610DTO>> searchRecord(String coy, List<String> chdrnumList,int extractSize, int partitionID);
	public Map<String, List<Br610DTO>> getChdrulsRecord(String coy, List<String> chdrnumList);
}