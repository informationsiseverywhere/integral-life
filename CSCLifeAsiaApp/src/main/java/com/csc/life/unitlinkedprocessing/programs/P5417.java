/*
 * File: P5417.java
 * Date: 30 August 2009 0:24:38
 * Author: Quipoz Limited
 * 
 * Class transformed from P5417.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.model.Chdrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Undactncpy;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S5417ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5510rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Nxtprogrec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

import com.csc.life.productdefinition.tablestructures.Tr58Zrec;
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;  
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.life.productdefinition.tablestructures.Tr58yrec;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import java.util.HashMap;
import java.util.Iterator;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*AUTHOR.            PAXUS FINANCIAL SYSTEMS PTY. LTD.
*REMARKS.
*
* This  program  creates  transaction records which describe  how
* each instalment premium is to be  applied  to  purchase  units.
* This may be specified as actual  amounts,  which  must total to
* the  instalment premium, or as percentages,  which  must  total
* 100.
*
* These records apply to Coverage/Rider group transaction records
* defined by program P6326 (Coverage/Rider processing program for
* Unit Linked products).
*
*****************************************************************
* </pre>
*/
public class P5417 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5417");
	private String changedInd = "";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-VALUE */
	private FixedLengthStringData wsaaFndopt = new FixedLengthStringData(4);
	private FixedLengthStringData totPolsInPlan = new FixedLengthStringData(4);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData y = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData zI = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaT5515Sub = new PackedDecimalData(2, 0).init(ZERO);

	private FixedLengthStringData wsaaStandardPlan = new FixedLengthStringData(1).init(SPACES);
	private Validator standardPlan = new Validator(wsaaStandardPlan, "Y");
	private Validator nonStandardPlan = new Validator(wsaaStandardPlan, "N");
	private PackedDecimalData wsaaTotUalprc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalPrem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData one = new PackedDecimalData(3, 0).init(1).setUnsigned();
	private static final int two = 2;
	private static final int ten = 10;
	private static final int oneHundred = 100;

	private FixedLengthStringData wsaaUnltunlFound = new FixedLengthStringData(1).init(SPACES);
	private Validator unltunlFound = new Validator(wsaaUnltunlFound, "Y");

	private FixedLengthStringData wsaaGotFund = new FixedLengthStringData(1).init(SPACES);
	private Validator gotAtLeast1Fund = new Validator(wsaaGotFund, "Y");

	private FixedLengthStringData wsaaSaveFunds = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSaveFund = FLSArrayPartOfStructure(10, 4, wsaaSaveFunds, 0);
	private ZonedDecimalData wsaaInd = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaInd1 = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaFundDup = new FixedLengthStringData(1);
	private Validator gotDuplicate = new Validator(wsaaFundDup, "Y");
	private PackedDecimalData wsaaSubscript = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsbbFndsplItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbFndopt = new FixedLengthStringData(4).isAPartOf(wsbbFndsplItem, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsbbFndsplItem, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsaaItemCheck = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCheck1st4 = new FixedLengthStringData(4).isAPartOf(wsaaItemCheck, 0);
	private FixedLengthStringData wsaaItemCheckEnd4 = new FixedLengthStringData(4).isAPartOf(wsaaItemCheck, 4);

	private FixedLengthStringData wsaaFundOk = new FixedLengthStringData(1);
	private Validator fundNotFound = new Validator(wsaaFundOk, "N");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
		/* TABLES */
	private static final String t2240 = "T2240";
	private static final String t5510 = "T5510";
	private static final String t5515 = "T5515";
	private static final String t5540 = "T5540";
	private static final String t5543 = "T5543";
	private static final String t5687 = "T5687";
	private static final String t5551 = "T5551";
	private static final String t5671 = "T5671";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String covtunlrec = "COVTUNLREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String cltsrec = "CLTSREC";
	private static final String unltunlrec = "UNLTUNLREC";
	private static final String itemrec = "ITEMREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Nxtprogrec nxtprogrec = new Nxtprogrec();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T2240rec t2240rec = new T2240rec();
	private T5510rec t5510rec = new T5510rec();
	private T5687rec t5687rec = new T5687rec();
	private T5540rec t5540rec = new T5540rec();
	private T5543rec t5543rec = new T5543rec();
	private T5551rec t5551rec = new T5551rec();
	private T5671rec t5671rec = new T5671rec();
	private Undactncpy undactncpy = new Undactncpy();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S5417ScreenVars sv = ScreenProgram.getScreenVars( S5417ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	Boolean afrFlag;
	private static final String TR58U = "TR58U";
	private Tr58Zrec Tr58zrec = new Tr58Zrec();
	Boolean afrAvble = false;
	private ZswrpfDAO  zswrDAO =  getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);
	private Tr58yrec  tr58yrec = new Tr58yrec();
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> tr58ZListMap;
	private Map<String, List<Itempf>> tr58YListMap;
	private Map<String, List<Itempf>> tr58UListMap;
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(P5417.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private DescDAO descdao =new DescDAOImpl();//ILIFE-4036
	List<Descpf> descpf=null;//ILIFE-4036
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();		//ILIFE-4036
	
	private PackedDecimalData cnttypercd = null;
	private Datcon2rec datcon2rec = new Datcon2rec();
	private ZonedDecimalData outdate1 = null;
	private ZonedDecimalData[] outdates = new ZonedDecimalData[12];
	private ZonedDecimalData outdate2 = null;
	private ZonedDecimalData cnttypepresentdate = null;
	private int[] cnttypeallowedperiod = new int[12];
	private boolean fundPermission = false;
	private ZonedDecimalData outdate = null;
	private int allowperiod =0;
	private boolean fundList = false;//ILIFE-8164
/** 
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nxtpGoingUp, 
		nxtpEnd, 
		nxtpExit, 
		exit1900, 
		validate2100, 
		exit2090, 
		checkAmts, 
		checkFieldsExit, 
		gotIt5106, 
		exit5109, 
		loop5220, 
		checkPrice5240, 
		checkTotal5260, 
		exit5299, 
		exit6690, 
		matchProgName
	}

	public P5417() {
		super();
		screenVars = sv;
		new ScreenModel("S5417", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void nxtprog()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					nxtpMainline();
					nxtpGoingDown();
				case nxtpGoingUp: 
					nxtpGoingUp();
				case nxtpEnd: 
					nxtpEnd();
				case nxtpExit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nxtpMainline()
	{
		nxtprogrec.statuz.set("****");
		if (isEQ(wsspcomn.submenu, "P0075")) {
			wsspcomn.programPtr.add(1);
			wsspcomn.nextprog.set(wsaaProg);
			goTo(GotoLabel.nxtpExit);
		}
		if (isEQ(wsspcomn.nextprog, scrnparams.scrname)) {
			wsspcomn.lastprog.set(wsaaProg);
		}
		else {
			wsspcomn.lastprog.set(wsspcomn.nextprog);
		}
		wsspcomn.nextprog.set(SPACES);
		if (isEQ(nxtprogrec.function, "U")) {
			goTo(GotoLabel.nxtpGoingUp);
		}
	}  
 
protected void nxtpGoingDown()
	{
		if (isEQ(wsspcomn.lastprog, wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next4prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next1prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.submenu)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpGoingUp()
	{
		if (isEQ(wsspcomn.lastprog, wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog, wsspcomn.next4prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpEnd()
	{
		if (isEQ(wsspcomn.nextprog, SPACES)
		&& isNE(wsspcomn.flag, "W")) {
			wsspcomn.nextprog.set(wsspcomn.submenu);
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1100();
			unltunl1300();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*    DISPLAY '1000 SECTION'.
	* </pre>
	*/
protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		undactncpy.undAction.set(wsspcomn.undAction);
		/*    Initialise everything to be on the safe side.*/
		sv.instprem.set(ZERO);
		sv.unitBidPrice01.set(ZERO);
		sv.unitBidPrice02.set(ZERO);
		sv.unitBidPrice03.set(ZERO);
		sv.unitBidPrice04.set(ZERO);
		sv.unitBidPrice05.set(ZERO);
		sv.unitBidPrice06.set(ZERO);
		sv.unitBidPrice07.set(ZERO);
		sv.unitBidPrice08.set(ZERO);
		sv.unitBidPrice09.set(ZERO);
		sv.unitBidPrice10.set(ZERO);
		sv.unitAllocPercAmt01.set(ZERO);
		sv.unitAllocPercAmt02.set(ZERO);
		sv.unitAllocPercAmt03.set(ZERO);
		sv.unitAllocPercAmt04.set(ZERO);
		sv.unitAllocPercAmt05.set(ZERO);
		sv.unitAllocPercAmt06.set(ZERO);
		sv.unitAllocPercAmt07.set(ZERO);
		sv.unitAllocPercAmt08.set(ZERO);
		sv.unitAllocPercAmt09.set(ZERO);
		sv.unitAllocPercAmt10.set(ZERO);
		sv.anbAtCcd.set(ZERO);
		wsaaUnltunlFound.set(SPACES);
		/*    Set screen fields*/
		/*    Get Life Dets for screen*/
		/* RETRIEVE CONTRACT HEADER DETAILS WHICH HAVE BEEN STORED IN*/
		/* THE CHDRLNB I/O MODULE.*/
		chdrlnbIO.setFunction("RETRV");
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		totPolsInPlan.set(chdrlnbIO.getPolinc());
		//ILIFE-4036 started
		sv.cnttype.set(chdrlnbIO.getCnttype());
		
		/*    Obtain the Contract Type description from T5688.*/
		 //descpf= descdao.getdescData("IT", "T5688", sv.cnttype.toString(), wsspcomn.company.toString(),wsspcomn.language.toString());
		descpf= descdao.getItemByDescItem("IT", wsspcomn.company.toString(),"T5688", sv.cnttype.toString(),wsspcomn.language.toString());
		 
				if (descpf.size() == 0) {
					sv.ctypedes.fill("?");
				}
				else {
					sv.ctypedes.set(descpf.get(0).getLongdesc());
				}	
		covtlnbIO.setFunction("RETRV");
		covtlnbIO.setFormat("COVTLNBREC");
		SmartFileCode.execute(appVars, covtlnbIO);
		
		/*    Obtain the long description from DESC using DESCIO.*/
		descpf=null;
		 //descpf= descdao.getdescData("IT", "T5687", covtlnbIO.getCrtable().toString(), wsspcomn.company.toString(),wsspcomn.language.toString());
		descpf= descdao.getItemByDescItem("IT", wsspcomn.company.toString(),"T5687", covtlnbIO.getCrtable().toString(), wsspcomn.language.toString());
		
		if (descpf.size() == 0) {
			sv.crtabdesc.fill("?");
		}
		else {
			sv.crtabdesc.set(descpf.get(0).getLongdesc());
		}
		//ILIFE-4036 ended
		/*  The next check is to stop the program abending*/
		/*  when its is passed a blank key. In this case the program*/
		/*  should just go thru without processing.*/
		covtunlIO.setFunction("RETRV");
		covtunlIO.setFormat(covtunlrec);
		SmartFileCode.execute(appVars, covtunlIO);
		/*  The next check is to stop the program abending*/
		/*  when its is passed a blank key. In this case the program*/
		/*  should just go thru without processing.*/
		if (isEQ(covtunlIO.getStatuz(), varcom.mrnf)) {
			covtunlIO.setChdrnum(SPACES);
			goTo(GotoLabel.exit1900);
		}
		if (isNE(covtunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t2240);
		/* MOVE CHDRLNB-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			/* MTL002 */
			}
		}
		/*    Move fields passed through window to screen*/
		sv.coverage.set(covtunlIO.getCoverage());
		sv.rider.set(covtunlIO.getRider());
		lifelnbIO.setChdrcoy(covtunlIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtunlIO.getChdrnum());
		sv.chdrnum.set(covtunlIO.getChdrnum());
		lifelnbIO.setLife(covtunlIO.getLife());
		sv.life.set(covtunlIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction("READR");
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.anbAtCcd.set(lifelnbIO.getAnbAtCcd());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		sv.lifenum.set(lifelnbIO.getLifcnum());
		/* MOVE LIFELNB-CHDRCOY         TO CLTS-CLNTCOY.                */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction("READR");
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/* CHECK FOR EXISTENCE OF JOINT LIFE DETAILS.*/
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction("READR");
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		/*  IF LIFELNB-STATUZ            NOT = O-K                       */
		/*     MOVE 'NONE'               TO S5417-JLIFCNUM               */
		/*                                  S5417-JLINSNAME              */
		/*     ELSE                                                      */
		if (isEQ(lifelnbIO.getStatuz(), varcom.oK)) {
			sv.jlifcnum.set(lifelnbIO.getLifcnum());
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			/*      MOVE LIFELNB-CHDRCOY   TO CLTS-CLNTCOY                 */
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setFunction("READR");
			cltsIO.setFormat(cltsrec);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
			}
		}
		sv.numapp.set(covtunlIO.getNumapp());
		sv.crtable.set(covtunlIO.getCrtable());
		/*itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covtunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		/*  IF (ITDM-STATUZ             NOT = O-K AND MRNF)              
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covtunlIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtunlIO.getCrtable());
			syserrrec.statuz.set(errorsInner.e652);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());*/
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(covtunlIO.getCrtable().toString());
		itempf.setItemtabl(t5687);
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				t5687rec.t5687Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
		}		
		sv.statfund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.stsubsect.set(t5687rec.statSubSect);
		sv.instprem.set(covtunlIO.getInstprem());
		sv.instprem.add(covtunlIO.getSingp());
		wsaaTotalPrem.set(covtunlIO.getSingp());
		wsaaTotalPrem.add(covtunlIO.getInstprem());
		sv.percOrAmntInd.set("P");
		if ((isGT(covtunlIO.getSingp(), 0))
		&& (isGT(covtunlIO.getInstprem(), 0))) {
			sv.prcamtindOut[varcom.pr.toInt()].set("Y");
		}
		/*    Get fund split plan*/
		sv.virtFundSplitMethod.set(SPACES);
		/*itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5540);
		wsbbFndopt.set(covtunlIO.getCrtable());
		itdmIO.setItemitem(wsbbFndsplItem);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		/*  IF (ITDM-STATUZ             NOT = O-K AND MRNF)              
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5540)
		|| isNE(itdmIO.getItemitem(), wsbbFndsplItem)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(wsbbFndsplItem);
			syserrrec.statuz.set(errorsInner.h055);
			fatalError600();
		}*/
		/*  IF  ITDM-STATUZ              NOT = MRNF                     
		t5540rec.t5540Rec.set(itdmIO.getGenarea());*/
		wsbbFndopt.set(covtunlIO.getCrtable());
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsbbFndsplItem.toString());
		itempf.setItemtabl(t5540);
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				t5540rec.t5540Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
		}
		sv.virtFundSplitMethod.set(t5540rec.fundSplitPlan);
		
		//AFR
		
		List<Zswrpf> zswrpfiList=new ArrayList<>();
		Zswrpf zw=new Zswrpf();
		zw.setChdrpfx(chdrlnbIO.getChdrpfx().toString());
		zw.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		zw.setChdrnum(chdrlnbIO.getChdrnum().toString()); 
		zw.setValidflag("3" );
		zswrpfiList=zswrDAO.getZswrpfData( zw.getChdrnum() ,zw.getChdrcoy(),  zw.getChdrpfx()  ,  zw.getValidflag() );/* IJTI-1523 */	
		if (zswrpfiList.size()>0 )
		{
			 for (Zswrpf p : zswrpfiList) 
			 {             	 
			 sv.zafropt1.set(p.getZafropt());
			 sv.zafritem.set(p.getZafritem());
			 sv.zafrfreq.set(p.getZafrfreq());
			 }
		}
		else
		{
			sv.zafropt1.set("NO");
			sv.zafritem.set("NO");
		}
		
		afrFlag = false;
		readTR58U();
		if(!afrFlag){
			sv.zafropt1.set("NO");
			sv.zafritem.set("NO");
		}
		
		if (isEQ(wsspcomn.flag,"I")){			
			sv.zafropt1Out[varcom.pr.toInt()].set("Y");
			sv.zafropt1Out[varcom.nd.toInt()].set("Y");
			sv.zafrfreqOut[varcom.pr.toInt()].set("Y");
			sv.zafritemOut[varcom.pr.toInt()].set("Y");
		}
		
		if(!afrFlag){
			sv.zafritemOut[1].set("Y");
		}
				
		if (isEQ(wsspcomn.flag,"I")){			
			sv.zafritemOut[1].set("Y");
			sv.zafropt1Out[1].set("Y");			
		}
		fundPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP086", appVars, "IT");
		//ILIFE-8164- START
				fundList = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP106", appVars, "IT");
				
				if(fundList)
				{
					
					readT55516700();
					crossCheckT55436600();
					int ix = 0;
					for(FixedLengthStringData recData:t5543rec.unitVirtualFund) {
						if(recData != null) {
							sv.newFundList[ix].set(recData);	
						}
						ix++;
					}
					
					
						sv.newFundList01Out[varcom.nd.toInt()].set("N");
						sv.newFundList02Out[varcom.nd.toInt()].set("N");
						sv.newFundList03Out[varcom.nd.toInt()].set("N");
						sv.newFundList04Out[varcom.nd.toInt()].set("N");
						sv.newFundList05Out[varcom.nd.toInt()].set("N");
						sv.newFundList06Out[varcom.nd.toInt()].set("N");
						sv.newFundList07Out[varcom.nd.toInt()].set("N");
						sv.newFundList08Out[varcom.nd.toInt()].set("N");
						sv.newFundList09Out[varcom.nd.toInt()].set("N");
						sv.newFundList10Out[varcom.nd.toInt()].set("N");
						sv.newFundList11Out[varcom.nd.toInt()].set("N");
						sv.newFundList12Out[varcom.nd.toInt()].set("N");
						
					}
				else {

					sv.newFundList01Out[varcom.nd.toInt()].set("Y");
					sv.newFundList02Out[varcom.nd.toInt()].set("Y");
					sv.newFundList03Out[varcom.nd.toInt()].set("Y");
					sv.newFundList04Out[varcom.nd.toInt()].set("Y");
					sv.newFundList05Out[varcom.nd.toInt()].set("Y");
					sv.newFundList06Out[varcom.nd.toInt()].set("Y");
					sv.newFundList07Out[varcom.nd.toInt()].set("Y");
					sv.newFundList08Out[varcom.nd.toInt()].set("Y");
					sv.newFundList09Out[varcom.nd.toInt()].set("Y");
					sv.newFundList10Out[varcom.nd.toInt()].set("Y");
					sv.newFundList11Out[varcom.nd.toInt()].set("Y");
					sv.newFundList12Out[varcom.nd.toInt()].set("Y");
				}
				//ILIFE-8164-END
	}		
		//AFR
		

protected void allowperiodcalulation2021()
{   int i =0;
     int j=0;
	allowperiod=0;
	int k=0;
	
	  
    initialize(datcon1rec.datcon1Rec);
    datcon1rec.datcon1Rec.set(SPACES);
    datcon1rec.function.set(varcom.tday);
    Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    cnttypepresentdate = datcon1rec.intDate;
	
	
	if (fundPermission == true )
	{
		cnttypercd = chdrlnbIO.currfrom;
		readT55516700();
		crossCheckT55436600();
		for(FixedLengthStringData funds:sv.unitVirtualFund) { //loop through all funds selected on UI
		for(i=0; i<12;i++)
		{
			if(funds!=null && funds.equals(t5543rec.unitVirtualFund[i])) {
				break;
			}
			
		}
		if(i==12)
			k=11;
		else
			k=i;
		cnttypeallowedperiod[j]=0;
		if(!isEQ(t5543rec.allowperiod[i],SPACES)  && !isEQ(t5543rec.unitVirtualFund[i],SPACES)) {
			cnttypeallowedperiod[j] = t5543rec.allowperiod[i].toInt();
		
		}
	
	    
		outdates[j]=null;
		if (cnttypeallowedperiod[j]!=0)
		{
			
			initialize(datcon2rec.datcon2Rec);
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(cnttypercd);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(cnttypeallowedperiod[j]);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);  
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				datcon2rec.freqFactorx.set(ZERO);
			}
			outdates[j] = datcon2rec.intDate2;
		 }
	
		if (outdates[j]!= null && isLT(cnttypepresentdate.toInt(),outdates[j]) ) { 
			sv.vrtfndErr[j].set(errorsInner.rrku);
			wsspcomn.edterror.set("Y");
			
		}
		j++;
		}
	}  
	  
		

	    
	}	
protected void readTR58U()

{
	tr58UListMap = itemDAO.loadSmartTable("IT", chdrlnbIO.getChdrcoy().toString(), "TR58U");
	String keyItemitem = sv.crtable.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr58UListMap.containsKey(keyItemitem.trim())){	
		itempfList = tr58UListMap.get(keyItemitem.trim());
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();	 
				afrFlag = true;
		}		
	}
	 	
}

protected void unltunl1300()
	{
		/*    Read UNLTUNL to see if Details already exist*/
	
		unltunlIO.setParams(SPACES);
		unltunlIO.setDataKey(covtunlIO.getDataKey());
		unltunlIO.setFunction("READR");
		unltunlIO.setFormat(unltunlrec);
		SmartFileCode.execute(appVars, unltunlIO);
		/*    Record exists then use dets*/
		/*    or No record so use 'blank' screen.*/
		if (isEQ(unltunlIO.getStatuz(), "MRNF")
		|| isEQ(unltunlIO.getStatuz(), varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(unltunlIO.getParams());
			syserrrec.statuz.set(unltunlIO.getStatuz());
			fatalError600();
		}
		if (isEQ(unltunlIO.getStatuz(), "MRNF")) {
			wsaaUnltunlFound.set("N");
			return ;
		}
		/*    UNLTUNL record found - set up fields on the screen.*/
		wsaaUnltunlFound.set("Y");
		zI.set(0);
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			move1400();
		}
		sv.percOrAmntInd.set(unltunlIO.getPercOrAmntInd());
		sv.virtFundSplitMethod.set(unltunlIO.getFndSpl());//ILIFE-4036
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section.
	* </pre>
	*/
protected void move1400()
	{
		/*MOVE-VALUES*/
		/* MOVE UNLTUNL-UALFND (X)    TO S5417-UNIT-VIRTUAL-FUND (X).   */
		/* MOVE UNLTUNL-UALPRC (X)    TO S5417-UNIT-ALLOC-PERC-AMT (X). */
		/* MOVE UNLTUNL-USPCPR (X)    TO S5417-UNIT-BID-PRICE (X).      */
		if (isEQ(unltunlIO.getUalfnd(x), SPACES)) {
			return ;
		}
		zI.add(1);
		sv.unitVirtualFund[zI.toInt()].set(unltunlIO.getUalfnd(x));
		sv.unitAllocPercAmt[zI.toInt()].set(unltunlIO.getUalprc(x));
		sv.unitBidPrice[zI.toInt()].set(unltunlIO.getUspcpr(x));
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*  The next check is to stop the program abending                 */
		/*  when its is passed a blank key. In this case the program       */
		/*  should just go thru without processing.                        */
		if (isEQ(covtunlIO.getChdrnum(), SPACES)) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*    IF THIS IS AN INQUIRY SET THE FUNCTION TO HAVE THE FIELDS    */
		/*    PROTECTED WHEN THEY SHOW ON THE SCREEN.                      */
		/*    IF INQUIRE                                                   */
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		/*ALLOW FOR KILL ALSO -> SKIP VALIDATION.              */
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
				case validate2100: 
					validate2100();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    DISPLAY '2000 SECTION'.
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S5417IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5417-DATA-AREA.                       */
		/*    FOR 'CALC' OPTION, EITHER VALIDATE ALL FIELDS OR*/
		/*    READ T5510 AND REDISPLAY.*/
		if (isEQ(scrnparams.statuz, "CALC")) {
			
			checkAFRebalance();
			
			if (isNE(sv.virtFundSplitMethod,SPACES) &&  isEQ(sv.zafropt1,"RK" ))
			{					
				sv.zafropt1Err.set(errorsInner.afr3);
				wsspcomn.edterror.set("Y");
				return;				
			}	
			
			if (isEQ(sv.virtFundSplitMethod, SPACES)) {
				goTo(GotoLabel.validate2100);
			}
			else {
				wsaaFndopt.set(sv.virtFundSplitMethod);
				readItem5100();
				if (isNE(sv.fndsplErr, SPACES)) {
					wsspcomn.edterror.set("Y");
					/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
					/*             GO TO 2900-EXIT                                     */
					goTo(GotoLabel.exit2090);
				}
				else {
					/* The following line has been changed to follow standar ds        */
					/* and avoid an internal loop.                                     */
					/*             GO TO 2010-SCREEN-IO.                               */
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
			}
		}
		/*   ELSE*/
		/*      IF S5417-VIRT-FUND-SPLIT-METHOD NOT = SPACES*/
		/*MAJOR CLUDGE*/
		/*         MOVE S5417-VIRT-FUND-SPLIT-METHOD TO WSAA-FNDOPT*/
		/*   PERFORM 5100-READ-ITEM*/
		/*   PERFORM 200-SCREEN-ERRORS*/
		/*   MOVE O-K                    TO WSSP-EDTERROR*/
		/*END OF CLUDGE.*/
		/*         GO TO 2100-VALIDATE*/
		/*         ELSE*/
		/*            NEXT SENTENCE.*/
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		//AFR  ADD vALIADTION FOR Zafitem 411
				if (isEQ(sv.zafritem, SPACES)) {
					sv.zafritemErr.set(errorsInner.e186);
				}
		wsspcomn.edterror.set(varcom.oK);
		allowperiodcalulation2021();
	}

protected void validate2100()
	{
		/*    Validate fields*/
		/*    IF P5417-KILL-ACTION*/
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2900-EXIT.                                         */
		/*        GO TO 2090-EXIT.                                         */
	
		LOGGER.info("Validate method");
		LOGGER.info(sv.zafropt1.toString());
		LOGGER.info(sv.zafritem.toString());
		LOGGER.info(afrFlag.toString());
		
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		/*    IF THIS IS AN INQUIRY DONT VALIDATE MORE THAN THE ACTION*/
		if (undactncpy.inquire.isTrue()) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*         GO TO 2900-EXIT.                                        */
			return ;
		}
		
		//AFR
		if (afrFlag && isNE(sv.zafropt1,"NO"))
		{
			checkAFRTr58Z();
			if(isEQ(wsspcomn.edterror,"Y")){
				return;
			}
		}
		
		if (afrFlag && isNE(sv.zafropt1,"NO")){
			checkAFR();
		    if(isEQ(wsspcomn.edterror,"Y")){
				return;}
		}
		if (afrFlag && isNE(sv.zafropt1,"NO")){
			setAFRRiskBase();			
			if(isEQ(wsspcomn.edterror,"Y")){
					return;}
		}
		//AFR
		
		
		checkForDeletedFunds2300();
		
		sv.errorIndicators.set(SPACES);
		if (isNE(sv.percOrAmntInd, "P")
		&& isNE(sv.percOrAmntInd, "A")) {
			sv.prcamtindErr.set(errorsInner.g347);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		if (isEQ(sv.percOrAmntInd, "A")) {
			if (isNE(chdrlnbIO.getBillfreq(), freqcpy.single)) {
				sv.prcamtindErr.set(errorsInner.h403);
				wsspcomn.edterror.set("Y");
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*           GO TO 2900-EXIT.                                      */
				return ;
			}
		}
		wsaaSaveFunds.set(sv.unitVirtualFunds);
		if (isNE(sv.virtFundSplitMethod, SPACES)) {
			wsaaFndopt.set(sv.virtFundSplitMethod);
			readItem5100();
			if (isNE(sv.fndsplErr, SPACES)) {
				wsspcomn.edterror.set("Y");
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*           GO TO 2900-EXIT.                                      */
				return ;
			}					
		}
		
		
		if (isNE(sv.virtFundSplitMethod, SPACES)
		&& isNE(wsaaSaveFunds, SPACES)) {
			if (isNE(t5510rec.unitVirtualFunds, wsaaSaveFunds)) {
				sv.fndsplErr.set(errorsInner.h404);
				wsspcomn.edterror.set("Y");
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*            GO TO 2900-EXIT.                                     */
				return ;
			}
		}
		editFundSplit5200();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		readT55516700();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		/*    Read T5515 to validate the table entries                     */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		for (wsaaT5515Sub.set(1); !(isGT(wsaaT5515Sub, 10)); wsaaT5515Sub.add(1)){
			readT55156800();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
		crossCheckT55436600();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		wsaaStandardPlan.set("Y");
		scrTabCheck5400();
		if (isNE(sv.percOrAmntInd, "P")) {
			wsaaStandardPlan.set("N");
		}
		if (isNE(sv.unitBidPrices, ZERO)) {
			wsaaStandardPlan.set("N");
		}
		/*    Check for duplicate funds                                    */
		wsaaInd.set(1);
		wsaaFundDup.set(SPACES);
		for (wsaaInd.set(wsaaInd); !(gotDuplicate.isTrue()
		|| isGT(wsaaInd, 10)); wsaaInd.add(1)){
			checkFunds2500();
		}
		allowperiodcalulation2021();
		
	}
	
protected void checkAFR()
{
	if (isEQ(sv.zafropt1, SPACES)||isEQ(sv.zafritem, SPACES)) {
		sv.zafropt1Err.set(errorsInner.e186);
		wsspcomn.edterror.set("Y");
		return;
	}	
	 
	if ( isEQ(t5540rec.zafropt1,"AFRN")  )
	{
		sv.zafropt1Err.set(errorsInner.rfu4);
		wsspcomn.edterror.set("Y");
		return;
	}	 
}

protected void setAFRRiskBase(){
	
	//AFR
	if(isEQ(sv.zafropt1,"RK")){
		readTr58y();
		
		if(isNE( tr58yrec.tr58yrec ,SPACES) ){
			sv.unitAllocPercAmts.set("");
			//sv.unitBidPrices.set("");
			sv.unitVirtualFunds.set("");
			
			int age = sv.anbAtCcd.toInt();
				for(int i =0 ; i < 9; i++){
					if(tr58yrec.ageIssageFrm[i+1].toInt()<=age && tr58yrec.ageIssageTo[i+1].toInt()>=age){
						for (int j = 1; j<=5; j++){
								sv.unitAllocPercAmt[j].set(tr58yrec.unitPremPercent[(i*5)+j]);
								sv.unitVirtualFund[j].set(tr58yrec.unitVirtualFund[(i*5)+j]);
						}
					}
				}
		}else{
			wsspcomn.edterror.set("Y");
			syserrrec.statuz.set(errorsInner.afrt);
			sv.vrtfndErr[1].set(errorsInner.afrt);
			return;
		}
	}
}

protected void readTr58y()
{
	tr58YListMap = itemDAO.loadSmartTable("IT", chdrlnbIO.getChdrcoy().toString(), "TR58Y");
	String keyItemitem = sv.zafritem.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr58YListMap.containsKey(keyItemitem.trim())){	
		itempfList = tr58YListMap.get(keyItemitem.trim());
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				 
				tr58yrec.tr58yrec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				 
			}else{
				tr58yrec.tr58yrec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		sv.zafropt1Err.set(errorsInner.e048);
		wsspcomn.edterror.set("Y");		
	}	
	
}

protected void checkAFRTr58Z(){
	
	tr58ZListMap = itemDAO.loadSmartTable("IT", chdrlnbIO.getChdrcoy().toString(), "TR58Z");
	String keyItemitem = sv.crtable.toString()+sv.zafropt1.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr58ZListMap.containsKey(keyItemitem.trim())){	
		itempfList = tr58ZListMap.get(keyItemitem.trim());
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				 
					Tr58zrec.tr58zRec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				 
			}else{
				Tr58zrec.tr58zRec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		sv.zafropt1Err.set(errorsInner.e048);
		wsspcomn.edterror.set("Y");		
	}	
	
	if (!itemFound) {
		sv.zafritemErr.set(errorsInner.e048);
		wsspcomn.edterror.set("Y");		
	}	
	if(isEQ(sv.zafropt1,"NO") && isNE(sv.zafritem,"NO")){
		sv.zafritem.set("NO");
	}
	afrAvble = false;
	for(int i=1;i<=30;i++){
		if(Tr58zrec.billfreq[i].toString().equals(sv.zafritem.toString())){
			afrAvble = true;
		}
	}
	if(!afrAvble){
		sv.zafropt1Err.set(errorsInner.e048);
		wsspcomn.edterror.set("Y");
		return;
	}
}
protected void checkForDeletedFunds2300()
	{
		/*CHECK*/
		for (wsaaSubscript.set(1); !(isGT(wsaaSubscript, 10)); wsaaSubscript.add(1)){
			if (isEQ(sv.unitVirtualFund[wsaaSubscript.toInt()], SPACES)
			&& isNE(sv.hflag[wsaaSubscript.toInt()], SPACES)) {
				sv.hflag[wsaaSubscript.toInt()].set(SPACES);
			}
		}
		/*EXIT*/
	}

protected void checkFunds2500()
	{
		if (isEQ(sv.unitVirtualFund[wsaaInd.toInt()], SPACES)) {
			return ;
		}
		for (wsaaInd1.set(wsaaInd); !(gotDuplicate.isTrue()
		|| isGT(wsaaInd1, 8)); wsaaInd1.add(1)){
			checkDups2550();
		}
		/*EXIT*/
	}

protected void checkDups2550()
	{
		if ((setPrecision(sv.unitVirtualFund[wsaaInd.toInt()], 0)
		&& isEQ(sv.unitVirtualFund[wsaaInd.toInt()], sv.unitVirtualFund[add(wsaaInd1, 1).toInt()]))) {
			wsaaFundDup.set("Y");
			wsspcomn.edterror.set("Y");
			sv.vrtfndErr[wsaaInd.toInt()].set(errorsInner.e048);
			sv.vrtfndErr[add(wsaaInd1, 1).toInt()].set(errorsInner.e048);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*LOAD-WSSP-FIELDS*/
		/*  The next check is to stop the program abending*/
		/*  when its is passed a blank key. In this case the program*/
		/*  should just go thru without processing.*/
		if (isEQ(covtunlIO.getChdrnum(), SPACES)) {
			return ;
		}
		/*    IF THIS IS AN INQUIRY DONT UPDATE THE RECORD*/
		if (undactncpy.inquire.isTrue()) {
			return ;
		}
		/*     IF P5417-KILL-ACTION*/
		/*        GO TO 3900-EXIT.*/
		updates6000();
		if (nonStandardPlan.isTrue()) {
			wsspwindow.value.set(SPACES);
		}
		else {
			wsspwindow.value.set(sv.virtFundSplitMethod);
		}
		/*EXIT*/
		//AFR
		LOGGER.info("update method");
		LOGGER.info(sv.zafropt1.toString());
		LOGGER.info(sv.zafritem.toString());
		LOGGER.info(afrFlag.toString());
		if (afrFlag || isNE(sv.zafropt1,"NO")) 
		{
			rewriteZswr7400();
		}
		//AFR
}

protected void rewriteZswr7400() 
{
	zswrModify7500();
}


protected void zswrModify7500()
{
	//AFR	
	List<Zswrpf> zswrpfiList=new ArrayList<>();
	Zswrpf zw=new Zswrpf();
	zw.setChdrpfx(chdrlnbIO.getChdrpfx().toString());
	zw.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
	zw.setChdrnum(chdrlnbIO.getChdrnum().toString()); 
	zw.setValidflag("3" );
	zswrpfiList=zswrDAO.getZswrpfData( zw.getChdrnum() ,zw.getChdrcoy(),  zw.getChdrpfx()  ,  zw.getValidflag() );/* IJTI-1523 */	
	 
	if ( zswrpfiList.size()==0) 
	{		
		writeZswr7000();
	}
	else
	{
		zw.clearUnitAllocFundAndPerc();
		newZswr7100(zw);
		zI.set(0);
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			switch (x.toInt()) {
			case 1 : zw.setUalfnd01(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc01(sv.unitAllocPercAmt[x.toInt()].toLong());	 
					 break;
			case 2 : zw.setUalfnd02(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc02(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 3 : zw.setUalfnd03(sv.unitVirtualFund[x.toInt()].toString());
					  zw.setUalprc03(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 4 : zw.setUalfnd04(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc04(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 5 : zw.setUalfnd05(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc05(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 6 : zw.setUalfnd06(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc06(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break; 
			case 7 : zw.setUalfnd07(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc07(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 8 : zw.setUalfnd08(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc08(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 9 : zw.setUalfnd09(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc09(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 10 : zw.setUalfnd10(sv.unitVirtualFund[x.toInt()].toString());
			 		  zw.setUalprc10(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			default: return;  
			}
			
		}
		  zswrpfiList.add(zw);
		  zswrDAO.updateZswrpf(zswrpfiList);
	}
	
	
	} 
	
	
	protected void newZswr7100(Zswrpf zw ) 
	{
	zw.setChdrpfx(chdrlnbIO.getChdrpfx().toString());
	zw.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
	zw.setChdrnum(chdrlnbIO.getChdrnum().toString());
	zw.setCnntype(chdrlnbIO.getCnttype().toString());
	zw.setValidflag("3" );
	zw.setCrtable(covtunlIO.getCrtable().toString());
	zw.setZafropt(sv.zafropt1.toString());
	zw.setZchkopt("");
	zw.setZregdte(varcom.vrcmMaxDate.toInt());
	zw.setAge(sv.anbAtCcd.toInt());
	zw.setTranno(chdrlnbIO.getTranno().toInt());
	zw.setZafritem(sv.zafritem.toString());
	  if (isEQ(sv.zafropt1 , "FR")) {
		  zw.setZafrfreq(sv.zafritem.toString()); 
		  zw.setVrtfund(""); 
	  }
	  else if(isEQ(sv.zafropt1 , "NO")){
		  zw.setZafrfreq("NO");
		  zw.setVrtfund("");	  
	  }
	  else { 
		  zw.setZafrfreq("01");
		  zw.setVrtfund(""); 
	  }
		  /*Following Code to be implemented when doing changes for BRD-411 */
	/*  }*/
	  zw.setZwdwlrsn("");
	  zw.setZlastdte(varcom.maxdate.toInt());
	  zw.setZnextdte(varcom.vrcmMaxDate.toInt());
	  zw.setZwdlddte(varcom.vrcmMaxDate.toInt());
	  zw.setZenable("");	
	  zw.setZlstupdusr(wsspcomn.userid.toString());
	  datcon1rec.function.set(varcom.tday);
	  Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	  zw.setZlstupdate(datcon1rec.intDate.toInt());	
	   
} 

protected void writeZswr7000() 
{
 
	List<Zswrpf> zswrpfiList=new ArrayList<>();
	Zswrpf zw=new Zswrpf();
	zw.setChdrpfx(chdrlnbIO.getChdrpfx().toString());
	zw.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
	zw.setChdrnum(chdrlnbIO.getChdrnum().toString());
	zw.setCnntype(chdrlnbIO.getCnttype().toString());
	zw.setValidflag("3" );
	zw.setCrtable(covtunlIO.getCrtable().toString());
	zw.setZafropt(sv.zafropt1.toString());
	zw.setZchkopt("");
	zw.setZregdte(varcom.vrcmMaxDate.toInt());
	zw.setAge(sv.anbAtCcd.toInt());
	zw.setTranno(chdrlnbIO.getTranno().toInt());
	zw.setZafritem(sv.zafritem.toString());
	  if (isEQ(sv.zafropt1 , "FR")) {
		  zw.setZafrfreq(sv.zafritem.toString()); 
		  zw.setVrtfund(""); 
	  }
	  else if(isEQ(sv.zafropt1 , "NO")){
		  zw.setZafrfreq("NO");
		  zw.setVrtfund("");	  
	  }
	  else { 
		  zw.setZafrfreq("01");
		  zw.setVrtfund(""); 
	  }
		 
	  zw.setZwdwlrsn("");
	  zw.setZlastdte(varcom.maxdate.toInt());
	  zw.setZnextdte(varcom.vrcmMaxDate.toInt());
	  zw.setZwdlddte(varcom.vrcmMaxDate.toInt());
	  zw.setZenable("");
	  //MPTD-1088 start
	  //ZswrIO.setZlstupdte(varcom.vrcmMaxDate);
	  zw.setZlstupdusr(wsspcomn.userid.toString());
	  datcon1rec.function.set(varcom.tday);
	  Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	  zw.setZlstupdate(datcon1rec.intDate.toInt());	
	  zI.set(0);
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			switch (x.toInt()) {
			case 1 : zw.setUalfnd01(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc01(sv.unitAllocPercAmt[x.toInt()].toLong());	 
					 break;
			case 2 : zw.setUalfnd02(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc02(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 3 : zw.setUalfnd03(sv.unitVirtualFund[x.toInt()].toString());
					  zw.setUalprc03(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 4 : zw.setUalfnd04(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc04(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 5 : zw.setUalfnd05(sv.unitVirtualFund[x.toInt()].toString());
					 zw.setUalprc05(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 6 : zw.setUalfnd06(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc06(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break; 
			case 7 : zw.setUalfnd07(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc07(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 8 : zw.setUalfnd08(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc08(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 9 : zw.setUalfnd09(sv.unitVirtualFund[x.toInt()].toString());
			 		 zw.setUalprc09(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			case 10 : zw.setUalfnd10(sv.unitVirtualFund[x.toInt()].toString());
			 		  zw.setUalprc10(sv.unitAllocPercAmt[x.toInt()].toLong());
					 break;
			default: return; // Throw error instead?
			}
		}
	  zswrpfiList.add(zw);
	  zswrDAO.insertZswrpfListRecord(zswrpfiList);	
}
protected void move7200(int x) {

	 

	
	//ZswrIO.setUalfnd(zI, sv.unitVirtualFund[x.toInt()]);
	//ZswrIO.setUalprc(zI, sv.unitAllocPercAmt[x.toInt()]);	 

}

	/**
	* <pre>
	*    Sections performed from the 3000 section.
	* </pre>
	*/
protected void updates6000()
	{
		/*PARA*/
		/*    Decide if we need to Write new record, rewrite existing*/
		/*    record, 'Delete' old record or nothing!*/
		/*       If no UNLTUNL record exists for this group*/
		/*             write a new record.*/
		if (!unltunlFound.isTrue()) {
			write6020();
			return ;
		}
		/*          If a UNLTUNL record exists but the 'Number Applicable'*/
		/*          field on the corresponding COVTUNL record has been*/
		/*          reduced to 0, delete the UNLTUNL record.*/
		if (isEQ(covtunlIO.getNumapp(), 0)) {
			delete6030();
			return ;
		}
		/*           If any enterable field on the existing UNLTUNL*/
		/*           record or COVTUNL record has been changed,*/
		/*           rewrite the UNLTUNL record.*/
		changedInd = "N";
		checkChangedFields();
		if (isEQ(changedInd, "Y")) {
			rewrite6040();
		}
		/*EXIT*/
	}

protected void write6020()
	{
		/*WRITE*/
		newUnltunl6060();
		zI.set(0);
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			move6050();
		}
		unltunlIO.setPercOrAmntInd(sv.percOrAmntInd);
		unltunlIO.setFndSpl(sv.virtFundSplitMethod);//ILIFE-4036
		unltunlIO.setFunction(varcom.writr);
		/* MOVE 'UNLTUNLREC'           TO UNLTUNL-FORMAT.               */
		accessUnltunl6080();
		/*EXIT*/
	}

protected void delete6030()
	{
		/*DELETE*/
		unltunlIO.setFunction(varcom.readh);
		accessUnltunl6080();
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			return ;
		}
		unltunlIO.setFunction(varcom.delet);
		accessUnltunl6080();
		/*EXIT*/
	}

protected void rewrite6040()
	{
		rewrite6041();
	}

protected void rewrite6041()
	{
		unltunlIO.setFunction(varcom.readh);
		accessUnltunl6080();
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			return ;
		}
		if (isEQ(changedInd, "Y")) {
			unltunlIO.setUalfnds(SPACES);
			for (x.set(one); !(isGT(x, ten)); x.add(one)){
				zeroise6070();
			}
		}
		unltunlIO.setPercOrAmntInd(sv.percOrAmntInd);
		unltunlIO.setFndSpl(sv.virtFundSplitMethod);//ILIFE-4036
		for (zI.set(one); !(isGT(zI, ten)); zI.add(one)){
			zeroise6090();
		}
		zI.set(0);
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			move6050();
		}
		unltunlIO.setNumapp(covtunlIO.getNumapp());
		unltunlIO.setCurrfrom(covtunlIO.getEffdate());
		unltunlIO.setFunction(varcom.rewrt);
		accessUnltunl6080();
	}

protected void move6050()
	{
		/*MOVE*/
		/* MOVE S5417-UNIT-VIRTUAL-FUND   (X)  TO UNLTUNL-UALFND (X).   */
		/* MOVE S5417-UNIT-ALLOC-PERC-AMT (X)  TO UNLTUNL-UALPRC (X)   .*/
		/* MOVE S5417-UNIT-BID-PRICE      (X)  TO UNLTUNL-USPCPR (X).   */
		if (isEQ(sv.unitVirtualFund[x.toInt()], SPACES)) {
			return ;
		}
		zI.add(1);
		unltunlIO.setUalfnd(zI, sv.unitVirtualFund[x.toInt()]);
		unltunlIO.setUalprc(zI, sv.unitAllocPercAmt[x.toInt()]);
		unltunlIO.setUspcpr(zI, sv.unitBidPrice[x.toInt()]);
		/*EXIT*/
	}

protected void newUnltunl6060()
	{
		newUnltunl6061();
	}

protected void newUnltunl6061()
	{
		unltunlIO.setDataArea(SPACES);
		unltunlIO.setGenDate(varcom.vrcmDate);
		unltunlIO.setGenTime(varcom.vrcmTime);
		unltunlIO.setVn(wsspcomn.version);
		unltunlIO.setIo("UNLTUNLIO");
		/* MOVE 'UNLTUNLREC'           TO UNLTUNL-FORMAT.               */
		unltunlIO.setDataKey(covtunlIO.getDataKey());
		unltunlIO.setCurrto(varcom.vrcmMaxDate);
		unltunlIO.setCurrfrom(chdrlnbIO.getOccdate());
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			zeroise6070();
		}
		/*  MOVE ZEROS                  TO UNLTUNL-TRANNO.               */
		unltunlIO.setTranno(chdrlnbIO.getTranno());
		unltunlIO.setNumapp(covtunlIO.getNumapp());
	}

protected void zeroise6070()
	{
		/*ZEROISE*/
		unltunlIO.setUalprc(x, ZERO);
		unltunlIO.setUspcpr(x, ZERO);
		/*EXIT*/
	}

protected void accessUnltunl6080()
	{
		/*ACCESS-UNLTUNL*/
		unltunlIO.setStatuz(SPACES);
		unltunlIO.setFormat(unltunlrec);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void zeroise6090()
	{
		/*ZEROISE-PAR*/
		unltunlIO.setUalprc(zI, ZERO);
		unltunlIO.setUspcpr(zI, ZERO);
		unltunlIO.setUalfnd(zI, SPACES);
		/*EXIT*/
	}

protected void checkChangedFields()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkFields();
				case checkAmts: 
					checkAmts();
				case checkFieldsExit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkFields()
	{
		if (isNE(sv.numapp, unltunlIO.getNumapp())) {
			changedInd = "Y";
			goTo(GotoLabel.checkFieldsExit);
		}
		if (isNE(sv.percOrAmntInd, unltunlIO.getPercOrAmntInd())) {
			changedInd = "Y";
			goTo(GotoLabel.checkFieldsExit);
		}
		if (isNE(covtunlIO.getEffdate(), unltunlIO.getCurrfrom())) {
			changedInd = "Y";
			goTo(GotoLabel.checkFieldsExit);
		}
		//ILIFE-4036 starts
		if (isNE(sv.virtFundSplitMethod, unltunlIO.getFndSpl())) {
			changedInd = "Y";
			goTo(GotoLabel.checkFieldsExit);
		}
		//ILIFE-4036 ends
		x.set(1);
	}

protected void checkAmts()
	{
		if ((isNE(sv.unitAllocPercAmt[x.toInt()], unltunlIO.getUalprc(x)))
		|| (isNE(sv.unitBidPrice[x.toInt()], unltunlIO.getUspcpr(x)))
		|| (isNE(sv.unitVirtualFund[x.toInt()], unltunlIO.getUalfnd(x)))) {
			changedInd = "Y";
			return ;
		}
		else {
			x.add(1);
			if (isGT(x, 10)) {
				return ;
			}
			else {
				goTo(GotoLabel.checkAmts);
			}
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void readItem5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5100();
				case gotIt5106: 
					gotIt5106();
				case exit5109: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Gets Fund Split Plan from T5510.
	* </pre>
	*/
protected void para5100()
	{
		/*    If Fund Option = spaces, zeroise all numeric fields*/
		/*    for use in Standard Split check later, and that's all!*/
		/*    DISPLAY WSAA-FNDOPT.*/
		if (isEQ(wsaaFndopt, SPACES)) {
			for (x.set(one); !(isGT(x, 10)); x.add(one)){
				zeroise5108();
			}
			goTo(GotoLabel.exit5109);
		}
		/*itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5510);
		wsbbFndopt.set(wsaaFndopt);
		itdmIO.setItemitem(wsbbFndsplItem);
		/*    MOVE COVTLNB-CRTABLE        TO ITDM-ITEMITEM.
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		/*  IF (ITDM-STATUZ             NOT = O-K AND MRNF)              
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}*/
		wsbbFndopt.set(wsaaFndopt);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsbbFndsplItem.toString());
		itempf.setItemtabl(t5510);
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if(isNE(it.getItemitem(), wsbbFndsplItem))
				{
					sv.fndsplErr.set(errorsInner.g103);
					goTo(GotoLabel.exit5109);
				}
				t5510rec.t5510Rec.set(StringUtil.rawToString(it.getGenarea()));
				goTo(GotoLabel.gotIt5106);
				
			}
		} 
		
		if (isNE(wsaaItemCheck1st4, wsbbFndopt)) {
			sv.fndsplErr.set(errorsInner.g103);
			goTo(GotoLabel.exit5109);
		}
		
		/*  IF ITDM-STATUZ              = MRNF                           */
		/*      MOVE G103               TO S5417-FNDSPL-ERR              */
		/*      GO TO 5109-EXIT.                                         */
		/*    If table record not found, zeroise all numeric fields*/
		/*    for use in Standard Split check later!*/
		for (x.set(one); !(isGT(x, 10)); x.add(one)){
			zeroise5108();
		}
		goTo(GotoLabel.exit5109);
	}

protected void gotIt5106()
	{
		/*    DISPLAY '5106-GOT-IT'.*/
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			move5107();
		}
		goTo(GotoLabel.exit5109);
	}

protected void move5107()
	{
		sv.unitVirtualFund[x.toInt()].set(t5510rec.unitVirtualFund[x.toInt()]);
		if (isNE(sv.percOrAmntInd, "A")) {
			sv.unitAllocPercAmt[x.toInt()].set(t5510rec.unitPremPercent[x.toInt()]);
		}
		if (isEQ(t5510rec.unitVirtualFund[x.toInt()], ZERO)
		|| isEQ(t5510rec.unitVirtualFund[x.toInt()], SPACES)) {
			sv.unitBidPrice[x.toInt()].set(ZERO);
		}
	}

protected void zeroise5108()
	{
		/*    DISPLAY 'SHOULDN T BE HERE'.*/
		t5510rec.unitPremPercent[x.toInt()].set(ZERO);
		t5510rec.unitVirtualFund[x.toInt()].set(SPACES);
	}

protected void readItem5110()
	{
		/*READ-ITEM*/
		if (isLT(chdrlnbIO.getOccdate(), itdmIO.getItmfrm())
		|| isGT(chdrlnbIO.getOccdate(), itdmIO.getItmto())) {
			sv.fndsplErr.set(errorsInner.g104);
		}
		/*EXIT*/
	}

protected void editFundSplit5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5210();
				case loop5220: 
					loop5220();
					checkUalprc5230();
				case checkPrice5240: 
					checkPrice5240();
				case checkTotal5260: 
					checkTotal5260();
				case exit5299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5210()
	{
		x.set(ZERO);
		wsaaTotUalprc.set(ZERO);
		wsaaGotFund.set("N");
	}

protected void loop5220()
	{
		x.add(one);
		if (isGT(x, 10)) {
			goTo(GotoLabel.checkTotal5260);
		}
	}

protected void checkUalprc5230()
	{
		if (isNE(sv.unitVirtualFund[x.toInt()], SPACES)) {
			wsaaGotFund.set("Y");
		}
		if (isEQ(sv.unitVirtualFund[x.toInt()], SPACES)) {
			if (isNE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
				sv.ualprcErr[x.toInt()].set(errorsInner.g105);
				goTo(GotoLabel.checkPrice5240);
			}
			else {
				goTo(GotoLabel.checkPrice5240);
			}
		}
		else {
			if (isLTE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
				sv.ualprcErr[x.toInt()].set(errorsInner.f351);
				goTo(GotoLabel.checkPrice5240);
			}
		}
		if (isEQ(sv.percOrAmntInd, "P")
		&& isGT(sv.unitAllocPercAmt[x.toInt()], oneHundred)) {
			sv.ualprcErr[x.toInt()].set(errorsInner.f348);
			goTo(GotoLabel.checkPrice5240);
		}
		wsaaTotUalprc.add(sv.unitAllocPercAmt[x.toInt()]);
	}

protected void checkPrice5240()
	{
		if (isEQ(sv.unitVirtualFund[x.toInt()], SPACES)
		&& isNE(sv.unitBidPrice[x.toInt()], ZERO)) {
			sv.ubidprErr[x.toInt()].set(errorsInner.g105);
		}
		/*LOOP-BACK*/
		goTo(GotoLabel.loop5220);
	}

protected void checkTotal5260()
	{
		if (!gotAtLeast1Fund.isTrue()) {
			sv.vrtfndErr[1].set(errorsInner.h365);
		}
		if (isEQ(sv.percOrAmntInd, "P")
		&& isNE(wsaaTotUalprc, oneHundred)) {
			sv.ualprcErr[one.toInt()].set(errorsInner.e631);
			for (x.set(two); !(isGT(x, ten)); x.add(one)){
				ualprcErrs5270();
			}
			goTo(GotoLabel.exit5299);
		}
		if (isEQ(sv.percOrAmntInd, "A")) {
			/*       IF  S5417-INSTPREM      = ZERO*/
			/*           GO TO 5299-EXIT*/
			/*       ELSE*/
			/*           IF  WSAA-TOT-UALPRC NOT = S5417-INSTPREM*/
			if (isNE(wsaaTotUalprc, wsaaTotalPrem)) {
				sv.ualprcErr[one.toInt()].set(errorsInner.h405);
				for (x.set(two); !(isGT(x, ten)); x.add(one)){
					ualprcErrs5270();
				}
			}
		}
		goTo(GotoLabel.exit5299);
	}

protected void ualprcErrs5270()
	{
		if (isNE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
			sv.ualprcErr[x.toInt()].set(sv.ualprcErr[one.toInt()]);
		}
	}

protected void scrTabCheck5400()
	{
		/*PARA*/
		if ((isNE(t5510rec.unitVirtualFunds, sv.unitVirtualFunds))) {
			wsaaStandardPlan.set("N");
		}
		/*EXIT*/
	}

protected void crossCheckT55436600()
	{
		try {
			para6600();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para6600()
	{
		if (isEQ(t5551rec.alfnds, SPACES)) {
			goTo(GotoLabel.exit6690);
		}
	/*	itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5543);
		
		itdmIO.setItemitem(t5551rec.alfnds);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		/*  IF (ITDM-STATUZ             NOT = O-K AND MRNF)              
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5543)
		|| 
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
			t5543rec.t5543Rec.set(itdmIO.getGenarea());
		}*/
		
		
		wsbbFndopt.set(wsaaFndopt);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(t5551rec.alfnds.toString());
		itempf.setItemtabl(t5543);
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if(isNE(it.getItemitem(), t5551rec.alfnds))
				{
					sv.vrtfndErr[1].set(errorsInner.h364);
					goTo(GotoLabel.exit6690);
				}
				t5543rec.t5543Rec.set(StringUtil.rawToString(it.getGenarea()));
				wsaaItemCheck.set(it.getItemitem());
				
			}
		} 
		else {
			goTo(GotoLabel.exit6690);
		}
		
		
		
		if (isNE(wsaaItemCheck1st4, t5551rec.alfnds)) {
			goTo(GotoLabel.exit6690);
		}
		x.set(0);
		while ( !(isEQ(x, 10))) {
			check6650();
		}
		
		goTo(GotoLabel.exit6690);
	}

protected void check6650()
	{
		x.add(1);
		wsaaFundOk.set("N");
		if (isEQ(sv.unitVirtualFund[x.toInt()], ZERO)
		|| isEQ(sv.unitVirtualFund[x.toInt()], SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			y.set(0);
			while ( !(isEQ(y, 12))) {
				check16675();
			}
			
		}
	}

protected void check16675()
	{
		y.add(1);
		if (isEQ(sv.unitVirtualFund[x.toInt()], t5543rec.unitVirtualFund[y.toInt()])) {
			wsaaFundOk.set("Y");
			y.set(12);
		}
		if (isEQ(y, 12)
		&& fundNotFound.isTrue()) {
			sv.vrtfndErr[x.toInt()].set(errorsInner.h456);
		}
	}

protected void readT55516700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6700();
				case matchProgName: 
					matchProgName();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6700()
	{
		/* Coverage/Rider Switching.                                       */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(sv.crtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/*MATCH PROG WITH PROG ON T5551******************    */
		x.set(0);
	}

protected void matchProgName()
	{
		x.add(1);
		if (isGT(x, 4)) {
			scrnparams.errorCode.set(errorsInner.e302);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
			wsbbTran.set(t5671rec.edtitm[x.toInt()]);
		}
		else {
			goTo(GotoLabel.matchProgName);
		}

		/*itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5551);
		/*  MOVE S5417-CRTABLE          TO ITDM-ITEMITEM.                

		
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		/*  IF (ITDM-STATUZ             NOT = O-K AND MRNF)              
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5551)
		|| isNE(itdmIO.getItemitem(), wsbbTranCurrency)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			sv.crtableErr.set(errorsInner.h024);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}*/
		
		wsbbCurrency.set(chdrlnbIO.getCntcurr());
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsbbTranCurrency.toString());
		itempf.setItemtabl(t5551);
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				t5551rec.t5551Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
		} 
		else
		{
			sv.crtableErr.set(errorsInner.h024);
		}
		
	}

protected void readT55156800()
	{
		start6800();
	}

protected void start6800()
	{
		/*     VALIDATE T5515 TO CHECK FUNDS EXIST                         */
		if (isEQ(sv.unitVirtualFund[wsaaT5515Sub.toInt()], SPACES)) {
			return ;
		}
	/*	itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(sv.unitVirtualFund[wsaaT5515Sub.toInt()]);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isEQ(itdmIO.getStatuz(), varcom.endp))
		|| (isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), t5515))
		||  {
			
		}*/
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(sv.unitVirtualFund[wsaaT5515Sub.toInt()].toString());
		itempf.setItemtabl(t5515);
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if((isNE(it.getItemitem(), sv.unitVirtualFund[wsaaT5515Sub.toInt()])))
				{
					sv.vrtfndErr[wsaaT5515Sub.toInt()].set(errorsInner.g037);
					return ;

				}
				
				
			}
		} 
		
		if ((isGT(chdrlnbIO.getOccdate(), datcon1rec.intDate))) {
			checkTodaysT55156900();
		}
	}

protected void checkTodaysT55156900()
	{
		start6900();
	}

protected void start6900()
	{
		/*itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(sv.unitVirtualFund[wsaaT5515Sub.toInt()]);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isEQ(itdmIO.getStatuz(), varcom.endp))
		|| (isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), t5515))
		|| (isNE(itdmIO.getItemitem(), sv.unitVirtualFund[wsaaT5515Sub.toInt()]))) {
			sv.vrtfndErr[wsaaT5515Sub.toInt()].set(errorsInner.g037);
			return ;
		}*/
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(sv.unitVirtualFund[wsaaT5515Sub.toInt()].toString());
		itempf.setItemtabl(t5515);
		itempf.setItmfrm(datcon1rec.intDate.getbigdata());
		itempf.setItmto(datcon1rec.intDate.getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if((isNE(it.getItemitem(), sv.unitVirtualFund[wsaaT5515Sub.toInt()])))
				{
					sv.vrtfndErr[wsaaT5515Sub.toInt()].set(errorsInner.g037);
					return ;
				}
				
				
			}
		} 
		
	}
public void checkAFRebalance()
{
	afrFlag = false;
	readTR58U();
	if(!afrFlag){
		sv.zafropt1.set("NO");
		sv.zafritem.set("NO");
	}
	
	if(!afrFlag){
		sv.zafritemOut[1].set("Y");
	}
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e048 = new FixedLengthStringData(4).init("E048");
	private FixedLengthStringData e302 = new FixedLengthStringData(4).init("E302");
	private FixedLengthStringData e631 = new FixedLengthStringData(4).init("E631");
	private FixedLengthStringData e652 = new FixedLengthStringData(4).init("E652");
	private FixedLengthStringData f348 = new FixedLengthStringData(4).init("F348");
	private FixedLengthStringData f351 = new FixedLengthStringData(4).init("F351");
	private FixedLengthStringData g037 = new FixedLengthStringData(4).init("G037");
	private FixedLengthStringData g103 = new FixedLengthStringData(4).init("G103");
	private FixedLengthStringData g104 = new FixedLengthStringData(4).init("G104");
	private FixedLengthStringData g105 = new FixedLengthStringData(4).init("G105");
	private FixedLengthStringData g347 = new FixedLengthStringData(4).init("G347");
	private FixedLengthStringData h024 = new FixedLengthStringData(4).init("H024");
	private FixedLengthStringData h055 = new FixedLengthStringData(4).init("H055");
	private FixedLengthStringData h364 = new FixedLengthStringData(4).init("H364");
	private FixedLengthStringData h365 = new FixedLengthStringData(4).init("H365");
	private FixedLengthStringData h403 = new FixedLengthStringData(4).init("H403");
	private FixedLengthStringData h404 = new FixedLengthStringData(4).init("H404");
	private FixedLengthStringData h405 = new FixedLengthStringData(4).init("H405");
	private FixedLengthStringData rrku = new FixedLengthStringData(4).init("RRKU");
	private FixedLengthStringData h456 = new FixedLengthStringData(4).init("H456");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData afrt = new FixedLengthStringData(4).init("AFRT");
	private FixedLengthStringData afr2 = new FixedLengthStringData(4).init("AFR2");
	private FixedLengthStringData afr3 = new FixedLengthStringData(4).init("AFR3");
	private FixedLengthStringData rfu4 = new FixedLengthStringData(4).init("rfu4");
	 
}
}
