package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:56
 * Description:
 * Copybook name: SURDPENKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Surdpenkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData surdpenFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData surdpenKey = new FixedLengthStringData(64).isAPartOf(surdpenFileKey, 0, REDEFINE);
  	public FixedLengthStringData surdpenChdrcoy = new FixedLengthStringData(1).isAPartOf(surdpenKey, 0);
  	public FixedLengthStringData surdpenChdrnum = new FixedLengthStringData(8).isAPartOf(surdpenKey, 1);
  	public PackedDecimalData surdpenTranno = new PackedDecimalData(5, 0).isAPartOf(surdpenKey, 9);
  	public PackedDecimalData surdpenPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(surdpenKey, 12);
  	public FixedLengthStringData surdpenLife = new FixedLengthStringData(2).isAPartOf(surdpenKey, 15);
  	public FixedLengthStringData surdpenCoverage = new FixedLengthStringData(2).isAPartOf(surdpenKey, 17);
  	public FixedLengthStringData surdpenRider = new FixedLengthStringData(2).isAPartOf(surdpenKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(surdpenKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(surdpenFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		surdpenFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}