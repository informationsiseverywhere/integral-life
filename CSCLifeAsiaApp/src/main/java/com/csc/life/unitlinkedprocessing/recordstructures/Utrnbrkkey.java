package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:47
 * Description:
 * Copybook name: UTRNBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnbrkKey = new FixedLengthStringData(64).isAPartOf(utrnbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnbrkKey, 0);
  	public FixedLengthStringData utrnbrkChdrnum = new FixedLengthStringData(8).isAPartOf(utrnbrkKey, 1);
  	public FixedLengthStringData utrnbrkCoverage = new FixedLengthStringData(2).isAPartOf(utrnbrkKey, 9);
  	public FixedLengthStringData utrnbrkRider = new FixedLengthStringData(2).isAPartOf(utrnbrkKey, 11);
  	public PackedDecimalData utrnbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnbrkKey, 13);
  	public FixedLengthStringData utrnbrkLife = new FixedLengthStringData(2).isAPartOf(utrnbrkKey, 16);
  	public FixedLengthStringData utrnbrkUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnbrkKey, 18);
  	public FixedLengthStringData utrnbrkUnitType = new FixedLengthStringData(1).isAPartOf(utrnbrkKey, 22);
  	public PackedDecimalData utrnbrkTranno = new PackedDecimalData(5, 0).isAPartOf(utrnbrkKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(utrnbrkKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}