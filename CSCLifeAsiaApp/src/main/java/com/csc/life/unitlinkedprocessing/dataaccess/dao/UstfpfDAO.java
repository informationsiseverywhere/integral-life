package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Br608DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ustfpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UstfpfDAO extends BaseDAO<Ustfpf> {
	public void insertUstfstmRecord(List<Ustfpf> ustfstmInsertList);
	public void deleteUstfstmRecord(List<Ustfpf> ustfstmDeleteList);
	public Map<String, List<Ustfpf>> readUstfData(String coy,List<String> chdrnumList);
}