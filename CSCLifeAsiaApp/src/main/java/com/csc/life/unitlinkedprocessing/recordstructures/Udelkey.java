package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:20
 * Description:
 * Copybook name: UDELKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Udelkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData udelFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData udelKey = new FixedLengthStringData(64).isAPartOf(udelFileKey, 0, REDEFINE);
  	public FixedLengthStringData udelChdrcoy = new FixedLengthStringData(1).isAPartOf(udelKey, 0);
  	public FixedLengthStringData udelChdrnum = new FixedLengthStringData(8).isAPartOf(udelKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(udelKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(udelFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		udelFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}