package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:20
 * Description:
 * Copybook name: T5510REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5510rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5510Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData unitPremPercents = new FixedLengthStringData(50).isAPartOf(t5510Rec, 0);
  	public ZonedDecimalData[] unitPremPercent = ZDArrayPartOfStructure(10, 5, 2, unitPremPercents, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(unitPremPercents, 0, FILLER_REDEFINE);
  	public ZonedDecimalData unitPremPercent01 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData unitPremPercent02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
  	public ZonedDecimalData unitPremPercent03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
  	public ZonedDecimalData unitPremPercent04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
  	public ZonedDecimalData unitPremPercent05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
  	public ZonedDecimalData unitPremPercent06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
  	public ZonedDecimalData unitPremPercent07 = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
  	public ZonedDecimalData unitPremPercent08 = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData unitPremPercent09 = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
  	public ZonedDecimalData unitPremPercent10 = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
  	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(40).isAPartOf(t5510Rec, 50);
  	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(10, 4, unitVirtualFunds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData unitVirtualFund01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData unitVirtualFund02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData unitVirtualFund03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData unitVirtualFund04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData unitVirtualFund05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData unitVirtualFund06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData unitVirtualFund07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData unitVirtualFund08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData unitVirtualFund09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData unitVirtualFund10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(410).isAPartOf(t5510Rec, 90, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5510Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5510Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}