package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:41
 * Description:
 * Copybook name: T5542REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5542rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5542Rec = new FixedLengthStringData(366);
  	public FixedLengthStringData billfreqs = new FixedLengthStringData(12).isAPartOf(t5542Rec, 0);
  	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(6, 2, billfreqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData billfreq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData billfreq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData billfreq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData billfreq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData billfreq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData billfreq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData feemaxs = new FixedLengthStringData(54).isAPartOf(t5542Rec, 12);
  		/*                                         OCCURS 06 .*/
  	public PackedDecimalData[] feemax = PDArrayPartOfStructure(6, 17, 2, feemaxs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(54).isAPartOf(feemaxs, 0, FILLER_REDEFINE);
  	public PackedDecimalData feemax01 = new PackedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public PackedDecimalData feemax02 = new PackedDecimalData(17, 2).isAPartOf(filler1, 9);
  	public PackedDecimalData feemax03 = new PackedDecimalData(17, 2).isAPartOf(filler1, 18);
  	public PackedDecimalData feemax04 = new PackedDecimalData(17, 2).isAPartOf(filler1, 27);
  	public PackedDecimalData feemax05 = new PackedDecimalData(17, 2).isAPartOf(filler1, 36);
  	public PackedDecimalData feemax06 = new PackedDecimalData(17, 2).isAPartOf(filler1, 45);
  	public FixedLengthStringData feemins = new FixedLengthStringData(54).isAPartOf(t5542Rec, 66);
  		/*                                          OCCURS 06 .*/
  	public PackedDecimalData[] feemin = PDArrayPartOfStructure(6, 17, 2, feemins, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(feemins, 0, FILLER_REDEFINE);
  	public PackedDecimalData feemin01 = new PackedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public PackedDecimalData feemin02 = new PackedDecimalData(17, 2).isAPartOf(filler2, 9);
  	public PackedDecimalData feemin03 = new PackedDecimalData(17, 2).isAPartOf(filler2, 18);
  	public PackedDecimalData feemin04 = new PackedDecimalData(17, 2).isAPartOf(filler2, 27);
  	public PackedDecimalData feemin05 = new PackedDecimalData(17, 2).isAPartOf(filler2, 36);
  	public PackedDecimalData feemin06 = new PackedDecimalData(17, 2).isAPartOf(filler2, 45);
  	public FixedLengthStringData feepcs = new FixedLengthStringData(18).isAPartOf(t5542Rec, 120);
  		/*                                          OCCURS 06 .*/
  	public PackedDecimalData[] feepc = PDArrayPartOfStructure(6, 5, 2, feepcs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(18).isAPartOf(feepcs, 0, FILLER_REDEFINE);
  	public PackedDecimalData feepc01 = new PackedDecimalData(5, 2).isAPartOf(filler3, 0);
  	public PackedDecimalData feepc02 = new PackedDecimalData(5, 2).isAPartOf(filler3, 3);
  	public PackedDecimalData feepc03 = new PackedDecimalData(5, 2).isAPartOf(filler3, 6);
  	public PackedDecimalData feepc04 = new PackedDecimalData(5, 2).isAPartOf(filler3, 9);
  	public PackedDecimalData feepc05 = new PackedDecimalData(5, 2).isAPartOf(filler3, 12);
  	public PackedDecimalData feepc06 = new PackedDecimalData(5, 2).isAPartOf(filler3, 15);
  	public FixedLengthStringData ffamts = new FixedLengthStringData(54).isAPartOf(t5542Rec, 138);
  		/*                                          OCCURS 06 .*/
  	public PackedDecimalData[] ffamt = PDArrayPartOfStructure(6, 17, 2, ffamts, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(54).isAPartOf(ffamts, 0, FILLER_REDEFINE);
  	public PackedDecimalData ffamt01 = new PackedDecimalData(17, 2).isAPartOf(filler4, 0);
  	public PackedDecimalData ffamt02 = new PackedDecimalData(17, 2).isAPartOf(filler4, 9);
  	public PackedDecimalData ffamt03 = new PackedDecimalData(17, 2).isAPartOf(filler4, 18);
  	public PackedDecimalData ffamt04 = new PackedDecimalData(17, 2).isAPartOf(filler4, 27);
  	public PackedDecimalData ffamt05 = new PackedDecimalData(17, 2).isAPartOf(filler4, 36);
  	public PackedDecimalData ffamt06 = new PackedDecimalData(17, 2).isAPartOf(filler4, 45);
  	public FixedLengthStringData wdlAmounts = new FixedLengthStringData(54).isAPartOf(t5542Rec, 192);
  		/*                                          OCCURS 06 .*/
  	public PackedDecimalData[] wdlAmount = PDArrayPartOfStructure(6, 17, 2, wdlAmounts, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(54).isAPartOf(wdlAmounts, 0, FILLER_REDEFINE);
  	public PackedDecimalData wdlAmount01 = new PackedDecimalData(17, 2).isAPartOf(filler5, 0);
  	public PackedDecimalData wdlAmount02 = new PackedDecimalData(17, 2).isAPartOf(filler5, 9);
  	public PackedDecimalData wdlAmount03 = new PackedDecimalData(17, 2).isAPartOf(filler5, 18);
  	public PackedDecimalData wdlAmount04 = new PackedDecimalData(17, 2).isAPartOf(filler5, 27);
  	public PackedDecimalData wdlAmount05 = new PackedDecimalData(17, 2).isAPartOf(filler5, 36);
  	public PackedDecimalData wdlAmount06 = new PackedDecimalData(17, 2).isAPartOf(filler5, 45);
  	public FixedLengthStringData wdlFreqs = new FixedLengthStringData(12).isAPartOf(t5542Rec, 246);
  		/*                                           OCCURS 06 .*/
  	public PackedDecimalData[] wdlFreq = PDArrayPartOfStructure(6, 3, 0, wdlFreqs, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(12).isAPartOf(wdlFreqs, 0, FILLER_REDEFINE);
  	public PackedDecimalData wdlFreq01 = new PackedDecimalData(3, 0).isAPartOf(filler6, 0);
  	public PackedDecimalData wdlFreq02 = new PackedDecimalData(3, 0).isAPartOf(filler6, 2);
  	public PackedDecimalData wdlFreq03 = new PackedDecimalData(3, 0).isAPartOf(filler6, 4);
  	public PackedDecimalData wdlFreq04 = new PackedDecimalData(3, 0).isAPartOf(filler6, 6);
  	public PackedDecimalData wdlFreq05 = new PackedDecimalData(3, 0).isAPartOf(filler6, 8);
  	public PackedDecimalData wdlFreq06 = new PackedDecimalData(3, 0).isAPartOf(filler6, 10);
  	public FixedLengthStringData wdrems = new FixedLengthStringData(54).isAPartOf(t5542Rec, 258);
  		/*                                          OCCURS 06 .*/
  	public PackedDecimalData[] wdrem = PDArrayPartOfStructure(6, 17, 2, wdrems, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(54).isAPartOf(wdrems, 0, FILLER_REDEFINE);
  	public PackedDecimalData wdrem01 = new PackedDecimalData(17, 2).isAPartOf(filler7, 0);
  	public PackedDecimalData wdrem02 = new PackedDecimalData(17, 2).isAPartOf(filler7, 9);
  	public PackedDecimalData wdrem03 = new PackedDecimalData(17, 2).isAPartOf(filler7, 18);
  	public PackedDecimalData wdrem04 = new PackedDecimalData(17, 2).isAPartOf(filler7, 27);
  	public PackedDecimalData wdrem05 = new PackedDecimalData(17, 2).isAPartOf(filler7, 36);
  	public PackedDecimalData wdrem06 = new PackedDecimalData(17, 2).isAPartOf(filler7, 45);
  	
  	public FixedLengthStringData maxwiths = new FixedLengthStringData(54).isAPartOf(t5542Rec, 312);
		/*                                          OCCURS 06 .*/
  	
  	//ILIFE-7956 -START
	public PackedDecimalData[] maxwith = PDArrayPartOfStructure(6, 17, 2, maxwiths, 0);
	public FixedLengthStringData filler24 = new FixedLengthStringData(54).isAPartOf(maxwiths, 0, FILLER_REDEFINE);
	public PackedDecimalData maxwith01 = new PackedDecimalData(17, 2).isAPartOf(filler24, 0);
	public PackedDecimalData maxwith02 = new PackedDecimalData(17, 2).isAPartOf(filler24, 9);
	public PackedDecimalData maxwith03 = new PackedDecimalData(17, 2).isAPartOf(filler24, 18);
	public PackedDecimalData maxwith04 = new PackedDecimalData(17, 2).isAPartOf(filler24, 27);
	public PackedDecimalData maxwith05 = new PackedDecimalData(17, 2).isAPartOf(filler24, 36);
	public PackedDecimalData maxwith06 = new PackedDecimalData(17, 2).isAPartOf(filler24, 45);
	//ILIFE-7956 -END		

	public void initialize() {
		COBOLFunctions.initialize(t5542Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5542Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}