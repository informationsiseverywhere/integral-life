package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:06
 * Description:
 * Copybook name: VPRCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprcKey = new FixedLengthStringData(256).isAPartOf(vprcFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprcUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprcKey, 0);
  	public FixedLengthStringData vprcUnitType = new FixedLengthStringData(1).isAPartOf(vprcKey, 4);
  	public PackedDecimalData vprcEffdate = new PackedDecimalData(8, 0).isAPartOf(vprcKey, 5);
  	public PackedDecimalData vprcJobno = new PackedDecimalData(8, 0).isAPartOf(vprcKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(vprcKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}