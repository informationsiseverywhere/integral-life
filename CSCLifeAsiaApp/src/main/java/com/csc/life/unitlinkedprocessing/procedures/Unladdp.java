/*
 * File: Unladdp.java
 * Date: 30 August 2009 2:49:38
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLADDP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.agents.tablestructures.T6657rec;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrtrgTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SvltclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.SurdpenTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.SurdtrgTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrntrgTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* LUNLADDP - Full Surrender Trigger Module.
* -----------------------------------------
* This  program  is  an  item  entry  on  T6598,  the  surrender   laim
* subroutine  method  table. This method is used in order to updat  the
* relevant sub-accounts. The trigger module and key  were  passed  with
* other details with the UTRN record.
*
* PROCESSING.
* ----------
*    Read the Surrender Claim detail records (SURDCLM) for this tr gger
* key and sum all the estimated amounts and sum all the actual  am unts
* (each as a single currency) at PLAN level.
*
* If  this is a surrender detail record for the current component, also
* sum the estimated amounts and sum the actual amounts.
*
* If the accumulated estimated amount (for this component) is equa   to
* zero,  then  calculate  the  surrender penalty and post the less r of
* the penalty amount and the actual amount.
*
* If the amounts are  not  zero,  then  call  LIFACMV  ("cash"  po ting
* subroutine)  to  post  to  the  correct  surrendering accounts ( 01 -
* Pending Surrender" and "02 - Surrender  Penalty"  sub-accounts).  The
* posting  required is defined in the appropriate line no. on the  5645
* table entry. Set up and pass the linkage area as follows:
*
*            Function               - PSTW
*            Batch key              - AT linkage
*            Document number        - contract number
*            Transaction number     - transaction no.
*            Journal sequence no.   - 0
*            Sub-account code       - from applicable T5645 entry
*            Sub-account type       - ditto
*            Sub-account GL map     - ditto
*            Sub-account GL sign    - ditto
*            S/acct control total   - ditto
*            Cmpy code (sub ledger) - batch company
*            Cmpy code (GL)         - batch company
*            Subsidiary ledger      - contract number
*            Original currency code - currency payable in
*            Original currency amt  - claim amount
*            Accounting curr code   - blank (handled by subroutine 
*            Accounting curr amount - zero (handled by subroutine)
*            Exchange rate          - zero (handled by subroutine)
*            Trans reference        - contract transaction number
*            Trans description      - from transaction code descri tion
*            Posting month and year - defaulted
*            Effective date         - Surrender Claim effective-da e
*            Reconciliation amount  - zero
*            Reconciliation date    - Max date
*            Transaction Id         - AT linkage
*            Substitution code 1    - contract type
*
* If the total estimated amount for the Plan is  equal  to  zero,  then
* access  the  balance  for the 01 sub-account (defined above) and post
* (as above) to the "01- Pending Surrender" and "03 -Surrender  Ac ual"
* sub-accounts.
*****************************************************************
* </pre>
*/
public class Unladdp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLADDP";
		/* ERRORS */
	private static final String e308 = "E308";
	protected static final String g437 = "G437";
		/* TABLES */
	private static final String t1693 = "T1693";
	private static final String t6657 = "T6657";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	protected static final String tr384 = "TR384";
	private static final String tr52d = "TR52D";

	private FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq, "Y");
	protected String wsaaLetter = "";

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 1);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 3);
	private FixedLengthStringData wsaaSurrCurr = new FixedLengthStringData(3).isAPartOf(wsaaOtherKeys, 5);
	protected PackedDecimalData wsaaActualVal = new PackedDecimalData(18, 3).init(0);
	protected PackedDecimalData wsaaNetVal = new PackedDecimalData(18, 3).init(0);
	protected PackedDecimalData wsaaAdjustAmt = new PackedDecimalData(18, 3).init(0);
		/* WSAA-IN-CASE-OF-ERROR */
	private FixedLengthStringData wsaaErrorStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaErrorFormat = new FixedLengthStringData(16);
	protected PackedDecimalData wsaaTotFee = new PackedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	protected PackedDecimalData wsaaTaxAmt = new PackedDecimalData(18, 3).init(0);
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(18, 3).init(0);
	protected PackedDecimalData wsaaFeeTaxTot = new PackedDecimalData(18, 3).init(0);

	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();

		/* WSAA-GENERAL-FIELDS */
	protected FixedLengthStringData wsaaEndTrans = new FixedLengthStringData(1);
	protected Validator endOfTransaction = new Validator(wsaaEndTrans, "Y");

	private FixedLengthStringData wsaaNotReady = new FixedLengthStringData(1);
	protected Validator noNeedToPost = new Validator(wsaaNotReady, "Y");

	private FixedLengthStringData wsaa1stTime = new FixedLengthStringData(1);
	private Validator n1stTimeThru = new Validator(wsaa1stTime, "Y");
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaPostingTable = new FixedLengthStringData(210);
	protected FixedLengthStringData[] wsaaPostAmount = FLSArrayPartOfStructure(10, 21, wsaaPostingTable, 0);
	protected FixedLengthStringData[] wsaaPostCurr = FLSDArrayPartOfArrayStructure(3, wsaaPostAmount, 0);
	private PackedDecimalData[] wsaaPostActualAmt = PDArrayPartOfArrayStructure(17, 2, wsaaPostAmount, 3);
	protected PackedDecimalData[] wsaaPostTotFee = PDArrayPartOfArrayStructure(17, 2, wsaaPostAmount, 12);

	protected FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	protected FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	protected FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	protected FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	protected FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private ZonedDecimalData wsaaSvltSub = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-PLAN-SUFF */
	protected ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	protected ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	protected IntegerData postIndex = new IntegerData();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	protected ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private HitrtrgTableDAM hitrtrgIO = new HitrtrgTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private SurdpenTableDAM surdpenIO = new SurdpenTableDAM();
	private SurdtrgTableDAM surdtrgIO = new SurdtrgTableDAM();
	protected SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private SvltclmTableDAM svltclmIO = new SvltclmTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	protected UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrntrgTableDAM utrntrgIO = new UtrntrgTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Getdescrec getdescrec = new Getdescrec();
	protected Syserrrec syserrrec = new Syserrrec();
	protected Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
	protected T5645rec t5645rec = new T5645rec();
	private T6657rec t6657rec = new T6657rec();
	private T5688rec t5688rec = new T5688rec();
	protected Tr384rec tr384rec = new Tr384rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	protected Udtrigrec udtrigrec = new Udtrigrec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		processComm080, 
		exit090, 
		exit2800, 
		getNextSurdclm3000, 
		exit3099, 
		getNextSurdclm13000, 
		exit3299, 
		b320Call, 
		b390Exit
	}

	public Unladdp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para010();
				case processComm080: 
					processComm080();
				case exit090: 
					exit090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para010()
	{
		initialize1000();
		processTransactionLoop12000();
		wsaaEndTrans.set(SPACES);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a200CheckFeedbackIndHitr();
		}
		else {
			checkFeedbackIndUtrn2200();
		}
		/* There are still outstanding funds to be dealt, so no need to*/
		/* process*/
		if (noNeedToPost.isTrue()) {
			goTo(GotoLabel.exit090);
		}
		wsaaEndTrans.set(SPACES);
		startSurdclm1400();
		while ( !(endOfTransaction.isTrue()
		|| noNeedToPost.isTrue())) {
			processTransactionLoop22300();
		}
		
		/*  Rewrite the SVLT record with the additional Actual Values.     */
		if (isEQ(wsaaLetter, "Y")) {
			rewriteSvlt2850();
		}
		/* There are still outstanding funds to be dealt, so no need to*/
		/* process*/
		if (noNeedToPost.isTrue()) {
			goTo(GotoLabel.exit090);
		}
		openBatch4200();
		if (isEQ(surhclmIO.getOtheradjst(), ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			postIndex.set(1);
			 searchlabel1:
			{
				for (; isLT(postIndex, wsaaPostAmount.length); postIndex.add(1)){
					if (isEQ(wsaaPostCurr[postIndex.toInt()], SPACES)
					|| isEQ(wsaaPostCurr[postIndex.toInt()], chdrsurIO.getCntcurr())) {
						wsaaPostCurr[postIndex.toInt()].set(chdrsurIO.getCntcurr());
						wsaaPostTotFee[postIndex.toInt()].add(surhclmIO.getOtheradjst());
						break searchlabel1;
					}
				}
			}
		}
		/* Save adjustment amount if it's not = zero.                      */
		if (isNE(surhclmIO.getOtheradjst(), ZERO)) {
			wsaaAdjustAmt.set(surhclmIO.getOtheradjst());
		}
		if (isEQ(surhclmIO.getTaxamt(), ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			postIndex.set(1);
			 searchlabel2:
			{
				for (; isLT(postIndex, wsaaPostAmount.length); postIndex.add(1)){
					if (isEQ(wsaaPostCurr[postIndex.toInt()], SPACES)
					|| isEQ(wsaaPostCurr[postIndex.toInt()], chdrsurIO.getCntcurr())) {
						wsaaPostCurr[postIndex.toInt()].set(chdrsurIO.getCntcurr());
						wsaaPostTotFee[postIndex.toInt()].subtract(surhclmIO.getTaxamt());
						break searchlabel2;
					}
				}
			}
		}
		/* Save tax amount if it's not = zero.                             */
		if (isNE(surhclmIO.getTaxamt(), ZERO)) {
			wsaaTaxAmt.set(surhclmIO.getTaxamt());
		}
		/*    POSTING BY CURRECY IF SURHCLM-CURRCD EQUAL TO SPACE.         */
		if (isEQ(surhclmIO.getCurrcd(), SPACES)) {
			for (postIndex.set(1); !(isEQ(wsaaPostCurr[postIndex.toInt()], SPACES)
			|| isGT(postIndex, 10)); postIndex.add(1)){
				postingByCurrency2600();
			}
			goTo(GotoLabel.processComm080);
		}
		/*  Subtract total fee from total value of funds giving amount to*/
		/*  be posted*/
		/*    IF SURHCLM-OTHERADJST   NOT = ZERO                           */
		/*       ADD SURHCLM-OTHERADJST   TO WSAA-TOT-FEE.                 */
		/* IF SURHCLM-OTHERADJST   NOT = ZERO                   <INTBR> */
		/*    ADD SURHCLM-OTHERADJST   TO WSAA-ACTUAL-VAL.      <INTBR> */
		if (isGT(wsaaTotFee, wsaaActualVal)) {
			updatePenaltyRec3300();
		}
		/* SUBTRACT WSAA-TOT-FEE    FROM WSAA-ACTUAL-VAL                */
		/*                          GIVING WSAA-NET-VAL.                */
		compute(wsaaNetVal, 3).set(sub(add(sub(wsaaActualVal, wsaaTotFee), wsaaAdjustAmt), wsaaTaxAmt));
		compute(wsaaNetVal, 3).set(sub(wsaaNetVal, wsaaFeeTaxTot));
		/* Post to Pending Withdrawal Account*/
		postGross3000();
		postNet3100();
		postPenalty3200();
		wsaaNetVal.set(ZERO);
		wsaaActualVal.set(ZERO);
		wsaaTotFee.set(ZERO);
		/* Post Adjustment amount.                                         */
		if (isNE(wsaaAdjustAmt, ZERO)) {
			postAdjust3400();
		}
		/* Post Tax Amount.                                                */
		if (isNE(wsaaTaxAmt, ZERO)) {
			postTax3500();
		}
	}

protected void processComm080()
	{
		closeBatch4300();
	}

	/**
	* <pre>
	*****PERFORM 4000-COMM-CLAWBACK.                                  
	* </pre>
	*/
protected void exit090()
	{
		exitProgram();
	}

protected void initialize1000()
	{
		go1010();
	}

protected void go1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTime.set(getCobolTime());
		wsaaEndTrans.set(SPACES);
		wsaaNotReady.set(SPACES);
		wsaaFeeTax.set(ZERO);
		wsaaFeeTaxTot.set(ZERO);
		wsaaPostingTable.set(SPACES);
		for (postIndex.set(1); !(isGT(postIndex, 10)); postIndex.add(1)){
			initPostingTable1100();
		}
		readSurhclm1200();
		readChdrsur1300();
		/*PERFORM 1400-START-SURDCLM.*/
		/*    Get company name.*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set("0");
		wsaaItemkey.itemItemtabl.set(t1693);
		wsaaItemkey.itemItemitem.set(surhclmIO.getChdrcoy());
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set("E");
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		/*PERFORM 1900-READ-TAB-T6657.*/
		readTabT56451800();
		readTabT56881895();
		b100TaxInfo();
	}

protected void initPostingTable1100()
	{
		/*START*/
		wsaaPostActualAmt[postIndex.toInt()].set(0);
		wsaaPostTotFee[postIndex.toInt()].set(0);
		/*EXIT*/
	}

protected void readSurhclm1200()
	{
		start1200();
	}

protected void start1200()
	{
		/*  Set up Header rec. key.*/
		surhclmIO.setParams(SPACES);
		surhclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surhclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");

		surhclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		surhclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		surhclmIO.setTranno(udtrigrec.tk2Tranno);
		surhclmIO.setPlanSuffix(ZERO);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)
		&& isNE(surhclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surhclmIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY NOT = SURHCLM-CHDRCOY OR             */
		/*    WSAA-TRIGGER-CHDRNUM NOT = SURHCLM-CHDRNUM OR             */
		/*    WSAA-TRIGGER-TRANNO  NOT =  SURHCLM-TRANNO                */
		if (isNE(udtrigrec.tk2Chdrcoy, surhclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, surhclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, surhclmIO.getTranno())) {
			syserrrec.params.set(surhclmIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void readChdrsur1300()
	{
		/*START*/
		chdrsurIO.setChdrcoy(surhclmIO.getChdrcoy());
		chdrsurIO.setChdrnum(surhclmIO.getChdrnum());
		chdrsurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void startSurdclm1400()
	{
		start1400();
	}

protected void start1400()
	{
		/*  Start detail rec.*/
		surdclmIO.setParams(SPACES);
		surdclmIO.setFunction(varcom.begn);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO SURDCLM-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO SURDCLM-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO SURDCLM-TRANNO.               */
		/* MOVE WSAA-TRIGGER-SUFFIX    TO SURDCLM-PLAN-SUFFIX.          */
		/* MOVE WSAA-TRIGGER-COVERAGE  TO SURDCLM-COVERAGE.             */
		/* MOVE WSAA-TRIGGER-RIDER     TO SURDCLM-RIDER.                */
		surdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		surdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		surdclmIO.setTranno(udtrigrec.tk2Tranno);
		surdclmIO.setPlanSuffix(udtrigrec.tk2Suffix);
		surdclmIO.setLife(udtrigrec.life);
		surdclmIO.setCoverage(udtrigrec.tk2Coverage);
		surdclmIO.setRider(udtrigrec.tk2Rider);
		surdclmIO.setFormat(formatsInner.surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = SURDCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = SURDCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = SURDCLM-TRANNO             */
		/* OR WSAA-TRIGGER-SUFFIX      NOT = SURDCLM-PLAN-SUFFIX        */
		/* OR WSAA-TRIGGER-COVERAGE    NOT = SURDCLM-COVERAGE           */
		/* OR WSAA-TRIGGER-RIDER       NOT = SURDCLM-RIDER              */
		if (isNE(udtrigrec.tk2Chdrcoy, surdclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, surdclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, surdclmIO.getTranno())
		|| isNE(udtrigrec.tk2Suffix, surdclmIO.getPlanSuffix())
		|| isNE(udtrigrec.life, surdclmIO.getLife())
		|| isNE(udtrigrec.tk2Coverage, surdclmIO.getCoverage())
		|| isNE(udtrigrec.tk2Rider, surdclmIO.getRider())
		|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
			checkPlanSuffix22975();
		}
		wsaa1stTime.set("Y");
	}

	/**
	* <pre>
	*1500-READ-T5540   SECTION.
	*1500-GO.
	*    MOVE SPACES                 TO ITDM-DATA-KEY.
	*    MOVE SURHCLM-CHDRCOY        TO ITDM-ITEMCOY.
	*    MOVE T5540                  TO ITDM-ITEMTABL.
	*    MOVE SURDCLM-CRTABLE        TO ITDM-ITEMITEM.
	*    IF  SURHCLM-EFFDATE        =  ZEROS
	*        MOVE 999999            TO ITDM-ITMFRM
	*    ELSE
	*        MOVE SURHCLM-EFFDATE   TO ITDM-ITMFRM.
	*    MOVE 'BEGN'                 TO ITDM-FUNCTION.
	*    CALL 'ITDMIO' USING         ITDM-PARAMS.
	*    IF ITDM-STATUZ              NOT = '****' AND
	*                                NOT = 'ENDP'
	*       MOVE ITDM-PARAMS         TO SYSR-PARAMS
	*        MOVE 'ITDMREC'          TO WSAA-ERROR-FORMAT
	*       PERFORM 9000-FATAL-ERROR.
	*    IF ITDM-ITEMCOY             NOT = SURHCLM-CHDRCOY
	*     OR ITDM-ITEMTABL           NOT = T5540
	*     OR ITDM-ITEMITEM           NOT = SURDCLM-CRTABLE
	*     OR ITDM-STATUZ             = 'ENDP'
	*       MOVE ITDM-PARAMS         TO SYSR-PARAMS
	*        MOVE 'ITDMREC'          TO WSAA-ERROR-FORMAT
	*       PERFORM 9000-FATAL-ERROR
	*    ELSE
	*       MOVE ITDM-GENAREA        TO T5540-T5540-REC.
	*1500-EXIT.
	*    EXIT.
	*1550-READ-T5542   SECTION.
	*1550-GO.
	*    MOVE T5540-WDMETH           TO WSAA-T5542-METH.
	*    MOVE SPACES                 TO ITDM-DATA-KEY.
	*    MOVE SURHCLM-CHDRCOY        TO ITDM-ITEMCOY.
	*    MOVE SURHCLM-CURRCD         TO WSAA-T5542-CURR.
	*    MOVE T5542                  TO ITDM-ITEMTABL.
	*    MOVE WSAA-T5542-KEY         TO ITDM-ITEMITEM.
	*    IF  SURHCLM-EFFDATE        =  ZEROS
	*        MOVE 999999            TO ITDM-ITMFRM
	*    ELSE
	*        MOVE SURHCLM-EFFDATE   TO ITDM-ITMFRM.
	*    MOVE 'BEGN'                 TO ITDM-FUNCTION.
	*    CALL 'ITDMIO' USING         ITDM-PARAMS.
	*    IF ITDM-STATUZ              NOT = '****' AND
	*                                NOT = 'ENDP'
	*       MOVE ITDM-PARAMS         TO SYSR-PARAMS
	*        MOVE 'ITDMREC'          TO WSAA-ERROR-FORMAT
	*       PERFORM 9000-FATAL-ERROR.
	*    IF ITDM-ITEMCOY             NOT = SURHCLM-CHDRCOY
	*     OR ITDM-ITEMTABL           NOT = T5542
	*     OR ITDM-ITEMITEM           NOT = WSAA-T5542-KEY
	*     OR ITDM-STATUZ             = 'ENDP'
	*       MOVE ITDM-PARAMS         TO SYSR-PARAMS
	*        MOVE 'ITDMREC'          TO WSAA-ERROR-FORMAT
	*       PERFORM 9000-FATAL-ERROR
	*    ELSE
	*       MOVE ITDM-GENAREA        TO T5542-T5542-REC.
	*1550-EXIT.
	*    EXIT.
	* </pre>
	*/
protected void readTabT56451800()
	{
		read1810();
	}

protected void read1810()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(surhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(surhclmIO.getChdrcoy());
		descIO.setLanguage("E");
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), "****")
		&& isNE(descIO.getStatuz(), "MRNF")) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void readTabT56881895()
	{
		read1897();
	}

protected void read1897()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(surhclmIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(surhclmIO.getCnttype().trim());
		itdmIO.setItmfrm(surhclmIO.getEffdate());
		itdmIO.setFunction("READR");//ILIFE-2621
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "MRNF")) {//ILIFE-2621
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), surhclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), surhclmIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTabT66571900()
	{
		read1910();
	}

protected void read1910()
	{
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO ITDM-ITEMCOY.                 */
		itdmIO.setItemcoy(udtrigrec.tk2Chdrcoy);
		itdmIO.setItemtabl(t6657);
		itdmIO.setItemitem(surhclmIO.getCurrcd());
		if (isEQ(surhclmIO.getEffdate(), ZERO)) {
			itdmIO.setItmfrm(999999);
		}
		else {
			itdmIO.setItmfrm(surhclmIO.getEffdate());
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		if (isNE(surhclmIO.getCurrcd(), itdmIO.getItemitem())
		|| isNE(surhclmIO.getChdrcoy(), itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t6657)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.statuz.set(varcom.mrnf);
			fatalError9000();
		}
		else {
			t6657rec.t6657Rec.set(itdmIO.getGenarea());
		}
	}

protected void processTransactionLoop12000()
	{
		para2010();
	}

protected void para2010()
	{
		readUtrn2700();
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a100ReadHitr();
			return ;
		}
		wsaaBatckey.batcBatccoy.set(utrnIO.getBatccoy());
		wsaaBatckey.batcBatcbrn.set(utrnIO.getBatcbrn());
		wsaaBatckey.batcBatcactyr.set(utrnIO.getBatcactyr());
		wsaaBatckey.batcBatcactmn.set(utrnIO.getBatcactmn());
		wsaaBatckey.batcBatctrcde.set(utrnIO.getBatctrcde());
		wsaaBatckey.batcBatcbatch.set(utrnIO.getBatcbatch());
		/*  Start detail rec.*/
		surdtrgIO.setParams(SPACES);
		/* MOVE BEGNH                  TO SURDTRG-FUNCTION.             */
		surdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		surdtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		surdtrgIO.setChdrcoy(utrnIO.getChdrcoy());
		surdtrgIO.setChdrnum(utrnIO.getChdrnum());
		surdtrgIO.setTranno(utrnIO.getTranno());
		surdtrgIO.setPlanSuffix(utrnIO.getPlanSuffix());
		surdtrgIO.setLife(utrnIO.getLife());
		surdtrgIO.setCoverage(utrnIO.getCoverage());
		surdtrgIO.setRider(utrnIO.getRider());
		surdtrgIO.setFieldType(utrnIO.getUnitType());
		surdtrgIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		surdtrgIO.setFormat(formatsInner.surdtrgrec);
		SmartFileCode.execute(appVars, surdtrgIO);
		if (isNE(surdtrgIO.getStatuz(), varcom.oK)
		&& isNE(surdtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdtrgIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		if (isNE(utrnIO.getChdrcoy(), surdtrgIO.getChdrcoy())
		|| isNE(utrnIO.getChdrnum(), surdtrgIO.getChdrnum())
		|| isNE(utrnIO.getTranno(), surdtrgIO.getTranno())
		|| isNE(utrnIO.getPlanSuffix(), surdtrgIO.getPlanSuffix())
		|| isNE(utrnIO.getLife(), surdtrgIO.getLife())
		|| isNE(utrnIO.getCoverage(), surdtrgIO.getCoverage())
		|| isNE(utrnIO.getRider(), surdtrgIO.getRider())
		|| isNE(utrnIO.getUnitType(), surdtrgIO.getFieldType())
		|| isNE(utrnIO.getUnitVirtualFund(), surdtrgIO.getVirtualFund())
		|| isEQ(surdtrgIO.getStatuz(), varcom.endp)) {
			checkPlanSuffix2950();
		}
		/*   Update detail record*/
		if (isEQ(utrnIO.getContractAmount(), ZERO)) {
			return ;
		}
		surdtrgIO.setEstMatValue(ZERO);
		if (isLTE(utrnIO.getContractAmount(), ZERO)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(mult(utrnIO.getContractAmount(), -1));
		}
		setPrecision(surdtrgIO.getActvalue(), 2);
		surdtrgIO.setActvalue(add(surdtrgIO.getActvalue(), utrnIO.getContractAmount()));
		/* MOVE REWRT                  TO SURDTRG-FUNCTION.             */
		surdtrgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, surdtrgIO);
		if (isNE(surdtrgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surdtrgIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void checkFeedbackIndUtrn2200()
	{
		check2210();
		continue2220();
	}

protected void check2210()
	{
		utrntrgIO.setDataArea(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO UTRNTRG-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO UTRNTRG-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO UTRNTRG-TRANNO.               */
		utrntrgIO.setChdrnum(udtrigrec.tk2Chdrnum);
		utrntrgIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		utrntrgIO.setTranno(udtrigrec.tk2Tranno);
		utrntrgIO.setFunction("BEGN");
	}

protected void continue2220()
	{
		SmartFileCode.execute(appVars, utrntrgIO);
		if (isNE(utrntrgIO.getStatuz(), "****")
		&& isNE(utrntrgIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(utrntrgIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		/*    IF WSAA-TRIGGER-CHDRNUM     NOT = UTRNTRG-CHDRNUM OR         */
		/*       WSAA-TRIGGER-CHDRCOY     NOT = UTRNTRG-CHDRCOY OR         */
		/*       WSAA-TRIGGER-TRANNO      NOT = UTRNTRG-TRANNO             */
		/*       MOVE ' '                 TO WSAA-NOT-READY                */
		/*       MOVE 'ENDP'              TO UTRNTRG-STATUZ                */
		/*       GO TO 2290-EXIT.                                          */
		/* IF (WSAA-TRIGGER-CHDRNUM     NOT = UTRNTRG-CHDRNUM OR   <006>*/
		/*    WSAA-TRIGGER-CHDRCOY     NOT = UTRNTRG-CHDRCOY OR    <006>*/
		/*    WSAA-TRIGGER-TRANNO      NOT = UTRNTRG-TRANNO ) OR   <006>*/
		if (isNE(udtrigrec.tk2Chdrnum, utrntrgIO.getChdrnum())
		|| isNE(udtrigrec.tk2Chdrcoy, utrntrgIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Tranno, utrntrgIO.getTranno())
		|| isEQ(utrntrgIO.getStatuz(), "ENDP")) {
			wsaaNotReady.set(" ");
			utrntrgIO.setStatuz("ENDP");
			return ;
		}
		if (isNE(utrntrgIO.getFeedbackInd(), "Y")) {
			wsaaNotReady.set("Y");
		}
		else {
			wsaaNotReady.set(" ");
			utrntrgIO.setFunction(varcom.nextr);
			continue2220();
			return ;
		}
	}

protected void checkPlanSuffix2950()
	{
		para2960();
	}

protected void para2960()
	{
		surdtrgIO.setParams(SPACES);
		/* MOVE BEGNH                  TO SURDTRG-FUNCTION.             */
		surdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		surdtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		surdtrgIO.setChdrcoy(utrnIO.getChdrcoy());
		surdtrgIO.setChdrnum(utrnIO.getChdrnum());
		surdtrgIO.setTranno(utrnIO.getTranno());
		surdtrgIO.setPlanSuffix(ZERO);
		surdtrgIO.setLife(utrnIO.getLife());
		surdtrgIO.setCoverage(utrnIO.getCoverage());
		surdtrgIO.setRider(utrnIO.getRider());
		surdtrgIO.setFieldType(utrnIO.getUnitType());
		surdtrgIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		SmartFileCode.execute(appVars, surdtrgIO);
		if (isNE(surdtrgIO.getStatuz(), varcom.oK)
		&& isNE(surdtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdtrgIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		if (isNE(utrnIO.getChdrcoy(), surdtrgIO.getChdrcoy())
		|| isNE(utrnIO.getChdrnum(), surdtrgIO.getChdrnum())
		|| isNE(utrnIO.getTranno(), surdtrgIO.getTranno())
		|| isNE(utrnIO.getLife(), surdtrgIO.getLife())
		|| isNE(utrnIO.getCoverage(), surdtrgIO.getCoverage())
		|| isNE(utrnIO.getRider(), surdtrgIO.getRider())
		|| isNE(surdtrgIO.getPlanSuffix(), ZERO)
		|| isNE(utrnIO.getUnitType(), surdtrgIO.getFieldType())
		|| isNE(utrnIO.getUnitVirtualFund(), surdtrgIO.getVirtualFund())) {
			syserrrec.statuz.set(varcom.mrnf);
			fatalError9000();
		}
	}

protected void checkPlanSuffix22975()
	{
		para2980();
	}

protected void para2980()
	{
		surdclmIO.setParams(SPACES);
		surdclmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","LIFE","COVERAGE","RIDER","PLNSFX");
		
		surdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		surdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		surdclmIO.setTranno(udtrigrec.tk2Tranno);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setLife(udtrigrec.life);
		surdclmIO.setCoverage(udtrigrec.tk2Coverage);
		surdclmIO.setRider(udtrigrec.tk2Rider);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = SURDCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = SURDCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = SURDCLM-TRANNO             */
		/* OR WSAA-TRIGGER-COVERAGE    NOT = SURDCLM-COVERAGE           */
		/* OR WSAA-TRIGGER-RIDER       NOT = SURDCLM-RIDER              */
		/* OR SURDCLM-PLAN-SUFFIX      NOT = ZERO                       */
		if (isNE(udtrigrec.tk2Chdrcoy, surdclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, surdclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, surdclmIO.getTranno())
		|| isNE(udtrigrec.life, surdclmIO.getLife())
		|| isNE(udtrigrec.tk2Coverage, surdclmIO.getCoverage())
		|| isNE(udtrigrec.tk2Rider, surdclmIO.getRider())
		|| isNE(surdclmIO.getPlanSuffix(), ZERO)) {
			syserrrec.statuz.set(varcom.mrnf);
			fatalError9000();
		}
	}

protected void processTransactionLoop22300()
	{
		para2310();
	}

protected void para2310()
	{
		/*  READ all RGWD recs for this transaction*/
		if (isNE(surdclmIO.getChdrcoy(), surhclmIO.getChdrcoy())
		|| isNE(surdclmIO.getChdrnum(), surhclmIO.getChdrnum())
		|| isNE(surdclmIO.getTranno(), surhclmIO.getTranno())
		|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
			wsaaEndTrans.set("Y");
			wsaaNotReady.set(" ");
			return ;
		}
		if (isNE(surdclmIO.getEstMatValue(), ZERO)) {
			wsaaNotReady.set("Y");
			/*MOVE ENDP TO SURDCLM-STATUZ*/
			return ;
		}
		/*   Accumulate actual fee*/
		/*    IF  SURDCLM-COVERAGE NOT =  WSAA-COVERAGE*/
		/*AND NOT 1ST-TIME-THRU*/
		/*PERFORM 1500-READ-T5540*/
		/*PERFORM 1550-READ-T5542*/
		if (n1stTimeThru.isTrue()) {
			readSvltRecord2800();
			/*        MOVE ZERO TO WSAA-COV-TOT  WSAA-TOT-FEE                  */
			wsaaTotFee.set(ZERO);
			wsaa1stTime.set("N");
		}
		/*   Accumulate Total Actual amount*/
		if (isEQ(surdclmIO.getFieldType(), "C")) {
			wsaaTotFee.add(surdclmIO.getActvalue());
			b200CalcTax();
			wsaaFeeTaxTot.add(wsaaFeeTax);
		}
		else {
			/*    ADD SURDCLM-ACTVALUE        TO WSAA-ACTUAL-VAL               */
			/*                                   WSAA-COV-TOT.                 */
			wsaaActualVal.add(surdclmIO.getActvalue());
		}
		/*  Update the SVLT Actual Value with the Surrender Actual Value.  */
		if (isEQ(wsaaLetter, "Y")) {
			wsaaSvltSub.add(1);
			/*    MOVE SURDCLM-ACTVALUE TO SVLTCLM-ACTVALUE(WSAA-SVLT-SUB). */
			if (isEQ(surdclmIO.getFieldType(), "C")) {
				setPrecision(svltclmIO.getActvalue(wsaaSvltSub), 2);
				svltclmIO.setActvalue(wsaaSvltSub, mult(-1, surdclmIO.getActvalue()));
			}
			else {
				svltclmIO.setActvalue(wsaaSvltSub, surdclmIO.getActvalue());
			}
		}
		postIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(postIndex, wsaaPostAmount.length); postIndex.add(1)){
				if (isEQ(wsaaPostCurr[postIndex.toInt()], SPACES)
				|| isEQ(wsaaPostCurr[postIndex.toInt()], surdclmIO.getCurrcd())) {
					updatePostAmount2400();
					break searchlabel1;
				}
			}
		}
		/*   Read next detail record*/
		surdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void updatePostAmount2400()
	{
		/*START*/
		wsaaPostCurr[postIndex.toInt()].set(surdclmIO.getCurrcd());
		if (isEQ(surdclmIO.getFieldType(), "C")) {
			wsaaPostTotFee[postIndex.toInt()].add(surdclmIO.getActvalue());
		}
		else {
			wsaaPostActualAmt[postIndex.toInt()].add(surdclmIO.getActvalue());
		}
		/*EXIT*/
	}

protected void postingByCurrency2600()
	{
		start2600();
	}

protected void start2600()
	{
		wsaaNetVal.set(ZERO);
		wsaaActualVal.set(ZERO);
		wsaaTotFee.set(ZERO);
		wsaaActualVal.set(wsaaPostActualAmt[postIndex.toInt()]);
		wsaaTotFee.set(wsaaPostTotFee[postIndex.toInt()]);
		surhclmIO.setCurrcd(wsaaPostCurr[postIndex.toInt()]);
		if (isGT(wsaaTotFee, wsaaActualVal)) {
			updatePenaltyRec3300();
		}
		compute(wsaaNetVal, 3).set(sub(wsaaActualVal, wsaaTotFee));
		compute(wsaaNetVal, 3).set(sub(wsaaNetVal, wsaaFeeTaxTot));
		/* Post to Pending Withdrawal Account*/
		postGross3000();
		postNet3100();
		postPenalty3200();
	}

protected void readUtrn2700()
	{
		read2710();
	}

protected void read2710()
	{
		utrnIO.setDataArea(SPACES);
		/* MOVE UTRN-LINKAGE           TO UTRN-DATA-KEY.                */
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		utrnIO.setFunction("READR");
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), "****")
		&& isNE(utrnIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void readSvltRecord2800()
	{
		try {
			readT66342810();
			start2820();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*5210-READ-T6634.                                            <013>
	* </pre>
	*/
protected void readT66342810()
	{
		/*  Get the Letter type from T6634.                                */
		wsaaLetter = "N";
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrsurIO.getChdrcoy());
		/* MOVE T6634                      TO ITEM-ITEMTABL.    <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrsurIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(g437);
			fatalError9000();
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.  <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE           = SPACES             <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			goTo(GotoLabel.exit2800);
		}
		else {
			wsaaLetter = "Y";
		}
	}

	/**
	* <pre>
	*  Read the SVLT record for this contract.                        
	*2800-START.                                                 <013>
	* </pre>
	*/
protected void start2820()
	{
		wsaaSvltSub.set(ZERO);
		svltclmIO.setChdrcoy(surdclmIO.getChdrcoy());
		svltclmIO.setChdrnum(surdclmIO.getChdrnum());
		svltclmIO.setFormat(formatsInner.svltclmrec);
		svltclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltclmIO.getParams());
			syserrrec.statuz.set(svltclmIO.getStatuz());
			fatalError9000();
		}
	}

protected void rewriteSvlt2850()
	{
		/*START*/
		svltclmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltclmIO.getParams());
			syserrrec.statuz.set(svltclmIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void postGross3000()
	{
		start3000();
	}

protected void start3000()
	{
		if (isEQ(wsaaActualVal, ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFA-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFA-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.               */
		/*  MOVE 'T686'                 TO LIFA-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFA-BATCBATCH.               */
		lifacmvrec.rdocnum.set(surhclmIO.getChdrnum());
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(surhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(surhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(surhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(surhclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaActualVal);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(surhclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(surhclmIO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		/* MOVE PARM-TRANID                TO VRCM-TRANID.              */
		/* MOVE VRCM-TERMID                TO LIFA-TERMID.              */
		/* MOVE VRCM-USER                  TO LIFA-USER.                */
		/* MOVE VRCM-TIME                  TO LIFA-TRANSACTION-TIME.    */
		/* MOVE VRCM-DATE                  TO LIFA-TRANSACTION-DATE.    */
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			startSurdclm1400();
			wsaaEndTrans.set(SPACES);
			while ( !(endOfTransaction.isTrue())) {
				postActualComp3000();
			}
			
			return ;
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void postActualComp3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start13000();
				case getNextSurdclm3000: 
					getNextSurdclm3000();
				case exit3099: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start13000()
	{
		/*  READ all SURD recs for this transaction                        */
		if (isNE(surdclmIO.getChdrcoy(), surhclmIO.getChdrcoy())
		|| isNE(surdclmIO.getChdrnum(), surhclmIO.getChdrnum())
		|| isNE(surdclmIO.getTranno(), surhclmIO.getTranno())
		|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
			wsaaEndTrans.set("Y");
			goTo(GotoLabel.exit3099);
		}
		/*   Post individual Components                                    */
		if (isEQ(surdclmIO.getActvalue(), ZERO)
		|| isEQ(surdclmIO.getFieldType(), "C")) {
			/*  GO TO 3099-EXIT.                                   <012>*/
			goTo(GotoLabel.getNextSurdclm3000);
		}
		lifacmvrec.origamt.set(surdclmIO.getActvalue());
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		wsaaRldgChdrnum.set(surhclmIO.getChdrnum());
		wsaaPlan.set(surdclmIO.getPlanSuffix());
		wsaaRldgLife.set(surdclmIO.getLife());
		wsaaRldgCoverage.set(surdclmIO.getCoverage());
		wsaaRldgRider.set(surdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[6].set(surdclmIO.getCrtable());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError9000();
		}
	}

	/**
	* <pre>
	*   Read next detail record                                       
	* </pre>
	*/
protected void getNextSurdclm3000()
	{
		surdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError9000();
		}
	}

protected void postNet3100()
	{
		start3100();
	}

protected void start3100()
	{
		if (isEQ(wsaaNetVal, ZERO)) {
			return ;
		}
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(ZERO);
		lifrtrnrec.frcdate.set(ZERO);
		lifrtrnrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFR-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFR-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFR-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFR-BATCACTMN.               */
		/*  MOVE 'T686'                 TO LIFR-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFR-BATCBATCH.               */
		lifrtrnrec.rdocnum.set(surhclmIO.getChdrnum());
		lifrtrnrec.tranno.set(surhclmIO.getTranno());
		lifrtrnrec.sacscode.set(t5645rec.sacscode02);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype02);
		lifrtrnrec.glcode.set(t5645rec.glmap02);
		lifrtrnrec.glsign.set(t5645rec.sign02);
		lifrtrnrec.contot.set(t5645rec.cnttot02);
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.rldgcoy.set(surhclmIO.getChdrcoy());
		lifrtrnrec.genlcoy.set(surhclmIO.getChdrcoy());
		lifrtrnrec.rldgacct.set(surhclmIO.getChdrnum());
		lifrtrnrec.origcurr.set(surhclmIO.getCurrcd());
		lifrtrnrec.origamt.set(wsaaNetVal);
		lifrtrnrec.genlcur.set(SPACES);
		lifrtrnrec.acctamt.set(ZERO);
		lifrtrnrec.crate.set(ZERO);
		lifrtrnrec.postyear.set(SPACES);
		lifrtrnrec.postmonth.set(SPACES);
		lifrtrnrec.tranref.set(surhclmIO.getTranno());
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifrtrnrec.effdate.set(surhclmIO.getEffdate());
		lifrtrnrec.frcdate.set("99999999");
		lifrtrnrec.substituteCode[1].set(surhclmIO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		/* MOVE PARM-TRANID                TO VRCM-TRANID.              */
		/* MOVE VRCM-TERMID                TO LIFR-TERMID.              */
		/* MOVE VRCM-USER                  TO LIFR-USER.                */
		/* MOVE VRCM-TIME                  TO LIFR-TRANSACTION-TIME.    */
		/* MOVE VRCM-DATE                  TO LIFR-TRANSACTION-DATE.    */
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, "****")) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		writeLetc3100a();
	}

protected void writeLetc3100a()
	{
		letc3100a();
		letcRecord3120a();
	}

protected void letc3100a()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrsurIO.getChdrcoy());
		/* MOVE T6634                      TO ITEM-ITEMTABL.    <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(surhclmIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(g437);
			fatalError9000();
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.  <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
	}

protected void letcRecord3120a()
	{
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(chdrsurIO.getChdrcoy());
		chdrlifIO.setChdrnum(chdrsurIO.getChdrnum());
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(udtrigrec.chdrcoy);
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(udtrigrec.chdrcoy);
		letrqstrec.chdrcoy.set(udtrigrec.chdrcoy);
		letrqstrec.rdocnum.set(udtrigrec.chdrnum);
		letrqstrec.chdrnum.set(udtrigrec.chdrnum);
		letrqstrec.tranno.set(udtrigrec.tranno);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.otherKeys.set(SPACES);
		wsaaOtherKeys.set(SPACES);
		wsaaLanguage.set(udtrigrec.language);
		wsaaSacscode.set(t5645rec.sacscode02);
		wsaaSacstype.set(t5645rec.sacstype02);
		wsaaSurrCurr.set(surhclmIO.getCurrcd());
		letrqstrec.otherKeys.set(wsaaOtherKeys);
		letrqstrec.function.set("ADD");
		/* IF  T6634-LETTER-TYPE       NOT = SPACES             <PCPPRT>*/
		if (isNE(tr384rec.letterType, SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				fatalError9000();
			}
		}
	}

protected void postPenalty3200()
	{
		start3200();
	}

protected void start3200()
	{
		if (isEQ(wsaaTotFee, ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFA-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFA-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.               */
		/*  MOVE 'T686'                 TO LIFA-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFA-BATCBATCH.               */
		lifacmvrec.rdocnum.set(surhclmIO.getChdrnum());
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(surhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(surhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(surhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(surhclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaTotFee);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(surhclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(surhclmIO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		/* MOVE PARM-TRANID                TO VRCM-TRANID.              */
		/* MOVE VRCM-TERMID                TO LIFA-TERMID.              */
		/* MOVE VRCM-USER                  TO LIFA-USER.                */
		/* MOVE VRCM-TIME                  TO LIFA-TRANSACTION-TIME.    */
		/* MOVE VRCM-DATE                  TO LIFA-TRANSACTION-DATE.    */
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		/* Post the Tax                                                    */
		if (isNE(tr52drec.txcode, SPACES)) {
			b300ProcessTax();
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			startSurdclm1400();
			wsaaEndTrans.set(SPACES);
			while ( !(endOfTransaction.isTrue())) {
				/*    GO TO 3290-EXIT.                                  <V74L01>*/
				postPenaltyComp3200();
			}
			
			return ;
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void postPenaltyComp3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start13200();
				case getNextSurdclm13000: 
					getNextSurdclm13000();
				case exit3299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start13200()
	{
		/*  READ all SURD recs for this transaction                        */
		if (isNE(surdclmIO.getChdrcoy(), surhclmIO.getChdrcoy())
		|| isNE(surdclmIO.getChdrnum(), surhclmIO.getChdrnum())
		|| isNE(surdclmIO.getTranno(), surhclmIO.getTranno())
		|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
			wsaaEndTrans.set("Y");
			goTo(GotoLabel.exit3299);
		}
		/*   Post individual Components                                    */
		if (isEQ(surdclmIO.getActvalue(), ZERO)
		|| isNE(surdclmIO.getFieldType(), "C")) {
			/*  GO TO 3299-EXIT.                                   <012>*/
			goTo(GotoLabel.getNextSurdclm13000);
		}
		lifacmvrec.origamt.set(surdclmIO.getActvalue());
		lifacmvrec.sacscode.set(t5645rec.sacscode05);
		lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec.glcode.set(t5645rec.glmap05);
		lifacmvrec.glsign.set(t5645rec.sign05);
		lifacmvrec.contot.set(t5645rec.cnttot05);
		wsaaRldgChdrnum.set(surhclmIO.getChdrnum());
		wsaaPlan.set(surdclmIO.getPlanSuffix());
		wsaaRldgLife.set(surdclmIO.getLife());
		wsaaRldgCoverage.set(surdclmIO.getCoverage());
		wsaaRldgRider.set(surdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[6].set(surdclmIO.getCrtable());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError9000();
		}
	}

	/**
	* <pre>
	*   Read next detail record                                       
	* </pre>
	*/
protected void getNextSurdclm13000()
	{
		surdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError9000();
		}
	}

protected void updatePenaltyRec3300()
	{
		readSurd3310();
	}

protected void readSurd3310()
	{
		surdpenIO.setParams(SPACES);
		surdpenIO.setFunction(varcom.readh);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO SURDPEN-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO SURDPEN-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO SURDPEN-TRANNO.               */
		/* MOVE WSAA-TRIGGER-SUFFIX    TO SURDPEN-PLAN-SUFFIX.          */
		/* MOVE WSAA-TRIGGER-COVERAGE  TO SURDPEN-COVERAGE.             */
		/* MOVE WSAA-TRIGGER-RIDER     TO SURDPEN-RIDER.                */
		surdpenIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		surdpenIO.setChdrnum(udtrigrec.tk2Chdrnum);
		surdpenIO.setTranno(udtrigrec.tk2Tranno);
		surdpenIO.setPlanSuffix(udtrigrec.tk2Suffix);
		surdpenIO.setLife(udtrigrec.life);
		surdpenIO.setCoverage(udtrigrec.tk2Coverage);
		surdpenIO.setRider(udtrigrec.tk2Rider);
		surdpenIO.setFormat(formatsInner.surdpenrec);
		SmartFileCode.execute(appVars, surdpenIO);
		if (isNE(surdpenIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surdpenIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
		wsaaTotFee.set(wsaaActualVal);
		surdpenIO.setActvalue(wsaaActualVal);
		surdpenIO.setFunction(varcom.rewrt);
		surdpenIO.setFormat(formatsInner.surdpenrec);
		SmartFileCode.execute(appVars, surdpenIO);
		if (isNE(surdpenIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surdpenIO.getParams());
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void postAdjust3400()
	{
		process3410();
	}

	/**
	* <pre>
	*  Post ACMV for Adjustment value (read from the header)          
	* </pre>
	*/
protected void process3410()
	{
		lifacmvrec.origamt.set(wsaaAdjustAmt);
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(surhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(surhclmIO.getChdrcoy());
		wsaaRldgChdrnum.set(surhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(surhclmIO.getCurrcd());
		wsaaPlan.set(surdclmIO.getPlanSuffix());
		wsaaRldgLife.set(surdclmIO.getLife());
		wsaaRldgCoverage.set(surdclmIO.getCoverage());
		wsaaRldgRider.set(surdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(surhclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		/* MOVE PARM-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.                  */
		/* MOVE VRCM-USER              TO LIFA-USER.                    */
		/* MOVE VRCM-TIME              TO LIFA-TRANSACTION-TIME.        */
		/* MOVE VRCM-DATE              TO LIFA-TRANSACTION-DATE.        */
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

protected void postTax3500()
	{
		process3510();
	}

	/**
	* <pre>
	*  Post ACMV for Tax Amount (read from the header)                
	* </pre>
	*/
protected void process3510()
	{
		lifacmvrec.origamt.set(wsaaTaxAmt);
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(surhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(surhclmIO.getChdrcoy());
		wsaaRldgChdrnum.set(surhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(surhclmIO.getCurrcd());
		wsaaPlan.set(surdclmIO.getPlanSuffix());
		wsaaRldgLife.set(surdclmIO.getLife());
		wsaaRldgCoverage.set(surdclmIO.getCoverage());
		wsaaRldgRider.set(surdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(surhclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(syserrrec.dbioStatuz);
			fatalError9000();
		}
	}

	/**
	* <pre>
	*4000-COMM-CLAWBACK SECTION.                                      
	*    Calculates any commission clawback & posts appropriately     
	*4010-BEGN-AGCM.                                                  
	*****MOVE SPACE                   TO AGCMSUR-DATA-AREA.           
	*****MOVE WSAA-TRIGGER-CHDRCOY    TO AGCMSUR-CHDRCOY.             
	*****MOVE WSAA-TRIGGER-CHDRNUM    TO AGCMSUR-CHDRNUM.             
	*****MOVE WSAA-TRIGGER-SUFFIX     TO AGCMSUR-PLAN-SUFFIX.         
	*****MOVE BEGNH                   TO AGCMSUR-FUNCTION.            
	*****CALL 'AGCMSURIO'                USING AGCMSUR-PARAMS.        
	*****IF AGCMSUR-STATUZ               NOT = O-K AND                
	*****                             NOT = ENDP                      
	*****   MOVE AGCMSUR-PARAMS          TO SYSR-PARAMS               
	*****   MOVE AGCMSURREC              TO WSAA-ERROR-FORMAT         
	*****   PERFORM 9000-FATAL-ERROR.                                 
	*****IF AGCMSUR-STATUZ               = ENDP                       
	*****   GO 4090-EXIT.                                             
	*****IF AGCMSUR-CHDRCOY            NOT = WSAA-TRIGGER-CHDRCOY OR  
	*****   AGCMSUR-CHDRNUM            NOT = WSAA-TRIGGER-CHDRNUM OR  
	*****   AGCMSUR-COVERAGE           NOT = WSAA-TRIGGER-COVERAGE OR 
	*****   AGCMSUR-RIDER              NOT = WSAA-TRIGGER-RIDER       
	*****   GO 4090-EXIT.                                             
	*****IF WSAA-TRIGGER-SUFFIX       = ZERO                          
	*****   GO 4020-PROCESS.                                          
	*****IF AGCMSUR-PLAN-SUFFIX          NOT = WSAA-TRIGGER-SUFFIX    
	*****   GO 4090-EXIT.                                             
	*4020-PROCESS.                                                    
	*****IF AGCMSUR-COMPAY               NOT = AGCMSUR-COMERN         
	*****   PERFORM 4100-CALC-COMM.                                   
	*4030-NEXT.                                                       
	*****MOVE NEXTR                   TO AGCMSUR-FUNCTION.            
	*****CALL 'AGCMSURIO'                USING AGCMSUR-PARAMS.        
	*****IF AGCMSUR-STATUZ               NOT = O-K AND                
	*****                             NOT = ENDP                      
	*****   MOVE AGCMSUR-PARAMS          TO SYSR-PARAMS               
	*****   MOVE AGCMSURREC              TO WSAA-ERROR-FORMAT         
	*****   PERFORM 9000-FATAL-ERROR.                                 
	*****IF AGCMSUR-STATUZ                = ENDP                      
	*****   GO 4090-EXIT.                                             
	*****IF AGCMSUR-CHDRCOY          NOT = WSAA-TRIGGER-CHDRCOY OR    
	*****   AGCMSUR-CHDRNUM          NOT = WSAA-TRIGGER-CHDRNUM OR    
	*****   AGCMSUR-COVERAGE         NOT = WSAA-TRIGGER-COVERAGE OR   
	*****   AGCMSUR-RIDER            NOT = WSAA-TRIGGER-RIDER         
	*****   GO 4090-EXIT.                                             
	*****IF WSAA-TRIGGER-SUFFIX       = ZERO                          
	*****   GO 4020-PROCESS.                                          
	*****IF AGCMSUR-PLAN-SUFFIX          NOT = WSAA-TRIGGER-SUFFIX    
	*****   GO 4090-EXIT.                                             
	*****GO 4020-PROCESS.                                             
	*4090-EXIT.                                                       
	*    EXIT.                                                        
	*4100-CALC-COMM SECTION.                                          
	*    Where it is necessary to clawback commission, the AGCM       
	*    record is updated & ACMV records are output.                 
	*4110-CLAWBACK-AMT.                                               
	*****SUBTRACT AGCMSUR-COMERN       FROM AGCMSUR-COMPAY            
	*****                           GIVING WSAA-CLAWBACK-AMT.         
	*4120-UPDATE-AGCM.                                                
	*    MOVE AGCMSUR-INITCOM          TO AGCMSUR-COMPAY.        <001>
	*****MOVE AGCMSUR-COMERN           TO AGCMSUR-COMPAY.        <001>
	*****MOVE AGCMSURREC               TO AGCMSUR-FORMAT.             
	*****MOVE REWRT                 TO AGCMSUR-FUNCTION.              
	*****CALL 'AGCMSURIO'              USING AGCMSUR-PARAMS.          
	*****IF AGCMSUR-STATUZ             NOT = O-K                      
	*****   MOVE AGCMSUR-PARAMS        TO SYSR-PARAMS                 
	*****   MOVE AGCMSURREC            TO WSAA-ERROR-FORMAT           
	*****   PERFORM 9000-FATAL-ERROR.                                 
	*4130-ACMV-1.                                                     
	*****MOVE ZERO                       TO LIFA-RCAMT                
	*****                                   LIFA-CONTOT               
	*****                                   LIFA-RCAMT                
	*****                                   LIFA-FRCDATE.             
	*****MOVE 'PSTW'                     TO LIFA-FUNCTION.            
	***  MOVE PARM-COMPANY           TO LIFA-BATCCOY.            <007>
	***  MOVE PARM-BRANCH            TO LIFA-BATCBRN.            <007>
	***  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.          <007>
	***  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.          <007>
	***  MOVE 'T686'                 TO LIFA-BATCTRCDE.          <007>
	***  MOVE SPACE                  TO LIFA-BATCBATCH.          <007>
	*****MOVE SURHCLM-CHDRNUM            TO LIFA-RDOCNUM.        <001>
	*    MOVE AGCMSUR-AGNTNUM            TO LIFA-RDOCNUM.        <001>
	*****MOVE SURHCLM-TRANNO             TO LIFA-TRANNO.              
	*****MOVE T5645-SACSCODE-04          TO LIFA-SACSCODE.            
	*****MOVE T5645-SACSTYPE-04          TO LIFA-SACSTYP.             
	*****MOVE T5645-GLMAP-04             TO LIFA-GLCODE.              
	*****MOVE T5645-SIGN-04              TO LIFA-GLSIGN.              
	*****MOVE T5645-CNTTOT-04            TO LIFA-CONTOT.              
	*****MOVE ZERO                       TO LIFA-JRNSEQ.              
	*****MOVE SURHCLM-CHDRCOY            TO LIFA-RLDGCOY              
	*****                                   LIFA-GENLCOY.             
	*****MOVE AGCMSUR-AGNTNUM            TO LIFA-RLDGACCT.       <001>
	*    MOVE SURHCLM-CHDRNUM            TO LIFA-RLDGACCT.       <001>
	*    MOVE SURHCLM-CURRCD             TO LIFA-ORIGCURR.       <001>
	*****MOVE CHDRSUR-CNTCURR            TO LIFA-ORIGCURR.       <001>
	*****MOVE WSAA-CLAWBACK-AMT          TO LIFA-ORIGAMT.             
	*****MOVE SPACES                     TO LIFA-GENLCUR.             
	*****MOVE ZERO                       TO LIFA-ACCTAMT              
	*****                                   LIFA-CRATE.               
	*****MOVE SPACES                     TO LIFA-POSTYEAR,            
	*****                                   LIFA-POSTMONTH.           
	*****MOVE SURHCLM-TRANNO             TO LIFA-TRANREF.             
	*****MOVE DESC-LONGDESC              TO LIFA-TRANDESC.            
	*****MOVE SURHCLM-EFFDATE            TO LIFA-EFFDATE.             
	*****MOVE '99999999'                 TO LIFA-FRCDATE.             
	*****MOVE SURHCLM-CNTTYPE            TO LIFA-SUBSTITUTE-CODE(01). 
	*****MOVE SPACES                 TO LIFA-SUBSTITUTE-CODE(06).<012>
	*****MOVE ZERO                       TO LIFA-USER                 
	*****                                   LIFA-TRANSACTION-TIME     
	*****                                   LIFA-TRANSACTION-DATE.    
	*****CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.             
	*****IF LIFA-STATUZ       NOT = '****'                            
	*****     MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS                 
	*****     MOVE 'LIFACMVREC'        TO WSAA-ERROR-FORMAT           
	*****     PERFORM 9000-FATAL-ERROR.                               
	*4140-ACMV-2.                                                     
	*****MOVE ZERO                       TO LIFA-RCAMT                
	*****                                   LIFA-CONTOT               
	*****                                   LIFA-RCAMT                
	*****                                   LIFA-FRCDATE.             
	*****MOVE 'PSTW'                     TO LIFA-FUNCTION.            
	***  MOVE PARM-COMPANY           TO LIFA-BATCCOY.            <007>
	***  MOVE PARM-BRANCH            TO LIFA-BATCBRN.            <007>
	***  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.          <007>
	***  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.          <007>
	***  MOVE 'T686'                 TO LIFA-BATCTRCDE.          <007>
	***  MOVE SPACE                  TO LIFA-BATCBATCH.          <007>
	*****MOVE SURHCLM-CHDRNUM          TO LIFA-RDOCNUM.               
	*****MOVE SURHCLM-TRANNO             TO LIFA-TRANNO.              
	*****MOVE T5645-SACSCODE-05          TO LIFA-SACSCODE.            
	*****MOVE T5645-SACSTYPE-05          TO LIFA-SACSTYP.             
	*****MOVE T5645-GLMAP-05             TO LIFA-GLCODE.              
	*****MOVE T5645-SIGN-05              TO LIFA-GLSIGN.              
	*****MOVE T5645-CNTTOT-05            TO LIFA-CONTOT.              
	*****MOVE ZERO                       TO LIFA-JRNSEQ.              
	*****MOVE SURHCLM-CHDRCOY            TO LIFA-RLDGCOY              
	*****                                   LIFA-GENLCOY.             
	*    MOVE SURHCLM-CHDRNUM            TO LIFA-RLDGACCT.       <001>
	*****MOVE AGCMSUR-AGNTNUM            TO LIFA-RLDGACCT.       <001>
	*****MOVE CHDRSUR-CNTCURR            TO LIFA-ORIGCURR.       <001>
	*    MOVE SURHCLM-CURRCD             TO LIFA-ORIGCURR.       <001>
	*****MOVE WSAA-CLAWBACK-AMT          TO LIFA-ORIGAMT.             
	*****MOVE SPACES                     TO LIFA-GENLCUR.             
	*****MOVE ZERO                       TO LIFA-ACCTAMT              
	*****                                   LIFA-CRATE.               
	*****MOVE SPACES                     TO LIFA-POSTYEAR,            
	*****                                   LIFA-POSTMONTH.           
	*****MOVE SURHCLM-TRANNO             TO LIFA-TRANREF.             
	*****MOVE DESC-LONGDESC              TO LIFA-TRANDESC.            
	*****MOVE SURHCLM-EFFDATE            TO LIFA-EFFDATE.             
	*****MOVE '99999999'                 TO LIFA-FRCDATE.             
	*****MOVE SURHCLM-CNTTYPE            TO LIFA-SUBSTITUTE-CODE(01). 
	*****MOVE ZERO                       TO LIFA-USER                 
	*****                                   LIFA-TRANSACTION-TIME     
	*****                                   LIFA-TRANSACTION-DATE.    
	*****CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.             
	*****IF LIFA-STATUZ       NOT = '****'                            
	*****     MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS                 
	*****     MOVE 'LIFACMVREC'        TO WSAA-ERROR-FORMAT           
	*****     PERFORM 9000-FATAL-ERROR.                               
	**4190-EXIT.                                                      
	***** EXIT.                                                       
	* </pre>
	*/
protected void openBatch4200()
	{
		open4210();
	}

protected void open4210()
	{
		batcdorrec.batcdorRec.set(SPACES);
		batcdorrec.function.set("AUTO");
		/* MOVE PARM-TRANID            TO BATD-TRANID.             <007>*/
		/* MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.                 */
		/* MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.                 */
		/* MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.               */
		/* MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.               */
		/* MOVE UTRN-BATCTRCDE         TO LIFA-BATCTRCDE.               */
		/* MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.               */
		/*   MOVE WSAA-BATCKEY           TO LIFA-BATCKEY.         <LA4524>*/
		lifacmvrec.batckey.set(udtrigrec.batchkey);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcdorrec.batchkey.set(lifacmvrec.batckey);
		batcdorrec.prefix.set("BA");
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifrtrnrec.batckey.set(batcdorrec.batchkey);
	}

protected void closeBatch4300()
	{
		close4310();
	}

protected void close4310()
	{
		batcuprec.function.set(varcom.writs);
		batcuprec.batchkey.set(batcdorrec.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		batcdorrec.function.set("CLOSE");
		/* MOVE PARM-TRANID            TO BATD-TRANID.             <007>*/
		batcdorrec.batchkey.set(batcdorrec.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
	}

protected void a100ReadHitr()
	{
		a110Read();
	}

protected void a110Read()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(udtrigrec.chdrcoy);
		hitrclmIO.setChdrnum(udtrigrec.chdrnum);
		hitrclmIO.setLife(udtrigrec.life);
		hitrclmIO.setCoverage(udtrigrec.coverage);
		hitrclmIO.setRider(udtrigrec.rider);
		hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
		hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
		hitrclmIO.setTranno(udtrigrec.tranno);
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		wsaaBatckey.batcBatccoy.set(hitrclmIO.getBatccoy());
		wsaaBatckey.batcBatcbrn.set(hitrclmIO.getBatcbrn());
		wsaaBatckey.batcBatcactyr.set(hitrclmIO.getBatcactyr());
		wsaaBatckey.batcBatcactmn.set(hitrclmIO.getBatcactmn());
		wsaaBatckey.batcBatctrcde.set(hitrclmIO.getBatctrcde());
		wsaaBatckey.batcBatcbatch.set(hitrclmIO.getBatcbatch());
		surdtrgIO.setParams(SPACES);
		/* MOVE BEGNH                  TO SURDTRG-FUNCTION.     <LA3993>*/
		surdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surdtrgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdtrgIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","LIFE","COVERAGE","RIDER","PLNSFX","VRTFUND");
	
		surdtrgIO.setChdrcoy(hitrclmIO.getChdrcoy());
		surdtrgIO.setChdrnum(hitrclmIO.getChdrnum());
		surdtrgIO.setTranno(hitrclmIO.getTranno());
		surdtrgIO.setPlanSuffix(hitrclmIO.getPlanSuffix());
		surdtrgIO.setLife(hitrclmIO.getLife());
		surdtrgIO.setCoverage(hitrclmIO.getCoverage());
		surdtrgIO.setRider(hitrclmIO.getRider());
		surdtrgIO.setVirtualFund(hitrclmIO.getZintbfnd());
		surdtrgIO.setFormat(formatsInner.surdtrgrec);
		SmartFileCode.execute(appVars, surdtrgIO);
		if (isNE(surdtrgIO.getStatuz(), varcom.oK)
		&& isNE(surdtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdtrgIO.getParams());
			fatalError9000();
		}
		if (isNE(hitrclmIO.getChdrcoy(), surdtrgIO.getChdrcoy())
		|| isNE(hitrclmIO.getChdrnum(), surdtrgIO.getChdrnum())
		|| isNE(hitrclmIO.getTranno(), surdtrgIO.getTranno())
		|| isNE(hitrclmIO.getPlanSuffix(), surdtrgIO.getPlanSuffix())
		|| isNE(hitrclmIO.getLife(), surdtrgIO.getLife())
		|| isNE(hitrclmIO.getCoverage(), surdtrgIO.getCoverage())
		|| isNE(hitrclmIO.getRider(), surdtrgIO.getRider())
		|| isNE(hitrclmIO.getZintbfnd(), surdtrgIO.getVirtualFund())
		|| isEQ(surdtrgIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(hitrclmIO.getContractAmount(), ZERO)) {
			return ;
		}
		surdtrgIO.setEstMatValue(ZERO);
		if (isLTE(hitrclmIO.getContractAmount(), ZERO)) {
			setPrecision(hitrclmIO.getContractAmount(), 2);
			hitrclmIO.setContractAmount(mult(hitrclmIO.getContractAmount(), -1));
		}
		setPrecision(surdtrgIO.getActvalue(), 2);
		surdtrgIO.setActvalue(add(surdtrgIO.getActvalue(), hitrclmIO.getContractAmount()));
		/* MOVE REWRT                  TO SURDTRG-FUNCTION.     <LA3993>*/
		surdtrgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, surdtrgIO);
		if (isNE(surdtrgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surdtrgIO.getParams());
			fatalError9000();
		}
	}

protected void a200CheckFeedbackIndHitr()
	{
		a210Check();
		a220Continue();
	}

protected void a210Check()
	{
		hitrtrgIO.setParams(SPACES);
		hitrtrgIO.setChdrnum(udtrigrec.tk2Chdrnum);
		hitrtrgIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		hitrtrgIO.setTranno(udtrigrec.tk2Tranno);
		hitrtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		hitrtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void a220Continue()
	{
		SmartFileCode.execute(appVars, hitrtrgIO);
		if (isNE(hitrtrgIO.getStatuz(), varcom.oK)
		&& isNE(hitrtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrtrgIO.getParams());
			fatalError9000();
		}
		if (isNE(udtrigrec.tk2Chdrnum, hitrtrgIO.getChdrnum())
		|| isNE(udtrigrec.tk2Chdrcoy, hitrtrgIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Tranno, hitrtrgIO.getTranno())
		|| isEQ(hitrtrgIO.getStatuz(), varcom.endp)) {
			wsaaNotReady.set(" ");
			hitrtrgIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(hitrtrgIO.getFeedbackInd(), "Y")) {
			wsaaNotReady.set("Y");
		}
		else {
			wsaaNotReady.set(" ");
			hitrtrgIO.setFunction(varcom.nextr);
			a220Continue();
			return ;
		}
	}

protected void b100TaxInfo()
	{
		b110ReadChdr();
		b120ReadTr52d();
	}

protected void b110ReadChdr()
	{
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setChdrcoy(chdrsurIO.getChdrcoy());
		chdrenqIO.setChdrnum(chdrsurIO.getChdrnum());
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError9000();
		}
	}

protected void b120ReadTr52d()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrenqIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrenqIO.getChdrcoy());
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError9000();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void b200CalcTax()
	{
		b210Calc();
	}

protected void b210Calc()
	{
		wsaaFeeTax.set(ZERO);
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(surdclmIO.getLife());
		txcalcrec.coverage.set(surdclmIO.getCoverage());
		txcalcrec.rider.set(surdclmIO.getRider());
		txcalcrec.crtable.set(surdclmIO.getCrtable());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.ccy.set(chdrenqIO.getCntcurr());
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.cntTaxInd.set(SPACES);
		txcalcrec.amountIn.set(surdclmIO.getActvalue());
		txcalcrec.transType.set("SURF");
		txcalcrec.effdate.set(surhclmIO.getEffdate());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError9000();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void b300ProcessTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b310Init();
				case b320Call: 
					b320Call();
					b330Proc();
				case b390Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b310Init()
	{
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		surdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		surdclmIO.setTranno(udtrigrec.tk2Tranno);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setFunction(varcom.begn);
	}

protected void b320Call()
	{
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(surdclmIO.getStatuz());
			syserrrec.params.set(surdclmIO.getParams());
			fatalError9000();
		}
		if (isEQ(surdclmIO.getStatuz(), varcom.endp)
		|| isNE(surdclmIO.getTranno(), udtrigrec.tk2Tranno)
		|| isNE(surdclmIO.getChdrnum(), udtrigrec.tk2Chdrnum)
		|| isNE(surdclmIO.getChdrcoy(), udtrigrec.tk2Chdrcoy)) {
			surdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.b390Exit);
		}
		if (isNE(surdclmIO.getFieldType(), "C")) {
			goTo(GotoLabel.b390Exit);
		}
	}

protected void b330Proc()
	{
		b400PostTax();
		/*B380-NEXT*/
		surdclmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.b320Call);
	}

protected void b400PostTax()
	{
		b410Post();
		b420Taxd();
	}

protected void b410Post()
	{
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(surdclmIO.getLife());
		txcalcrec.coverage.set(surdclmIO.getCoverage());
		txcalcrec.rider.set(surdclmIO.getRider());
		txcalcrec.crtable.set(surdclmIO.getCrtable());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.effdate.set(surhclmIO.getEffdate());
		txcalcrec.amountIn.set(surdclmIO.getActvalue());
		txcalcrec.tranno.set(udtrigrec.tk2Tranno);
		txcalcrec.transType.set("SURF");
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.jrnseq.set(ZERO);
		txcalcrec.ccy.set(surhclmIO.getCurrcd());
		txcalcrec.batckey.set(wsaaBatckey);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError9000();
		}
	}

protected void b420Taxd()
	{
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrenqIO.getChdrcoy());
		taxdIO.setChdrnum(chdrenqIO.getChdrnum());
		taxdIO.setLife(surdclmIO.getLife());
		taxdIO.setCoverage(surdclmIO.getCoverage());
		taxdIO.setRider(surdclmIO.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(surhclmIO.getEffdate());
		taxdIO.setInstfrom(varcom.vrcmMaxDate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(udtrigrec.tk2Tranno);
		taxdIO.setTrantype("SURF");
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(taxdIO.getTranref());
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		/* MOVE WSAA-IN-CASE-OF-ERROR  TO WSAA-TRIGGER.                 */
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		/* MOVE BOMB                   TO PARM-STATUZ.             <007>*/
		udtrigrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData surdpenrec = new FixedLengthStringData(10).init("SURDPENREC");
	private FixedLengthStringData surdtrgrec = new FixedLengthStringData(10).init("SURDTRGREC");
	private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData svltclmrec = new FixedLengthStringData(10).init("SVLTCLMREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
}
