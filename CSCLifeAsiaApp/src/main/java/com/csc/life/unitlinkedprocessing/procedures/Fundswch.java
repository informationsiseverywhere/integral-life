/*
 * File: Fundswch.java
 * Date: 29 August 2009 22:48:49
 * Author: Quipoz Limited
 *
 * Class transformed from FUNDSWCH.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UswdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswhTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.Gettim;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Unit Switching Trigger Module
*
*
*
* FUNDSWCH - Unit Fund Switching Trigger Module
*
* Overview
*
*
*      This program will be called from the Unit Dealing
*      suite whenever it is able to process one of the UTRN
*      records set up by P5146, the Target Fund Switching
*      program. P5146 will have set up a negative UTRN record
*      for each individual Life, Component, Fund combination
*      within the transaction. These will have been set up
*      with estimated values. Whenever the Unit Dealing
*      program is able to find the Actual values it will call
*      this trigger module which will attempt to complete the
*      process.
*
*      The trigger module will only complete the process when
*      ALL the UTRN records created by the transaction are
*      able to be processed, i.e. all of them have their
*      Actual prices.
*
*      There are three phases to this program:
*
*      Phase One
*
*      This will deal with the USWD record associated with the
*      triggering UTRN record, and ALL the other UTRN records
*      associated with THAT particular USWD record.
*
*      Phase Two
*
*      This will deal with ALL the USWD records for the
*      transaction and ALL their associated UTRN records -
*      the 'Selling' UTRN records.
*           =======
*
*      Phase Three
*
*      Here will be created all the 'Buying' UTRN records,
*      delete all the USWD records and the USHW record and
*      will create two ACMV records if there is a charge for
*      the switch.
*
* Processing
*
*
* Phase One
*
*
*      In the first phase the Trigger Module updates the USWD
*      record with all the available Actual Prices from the
*      corresponding UTRN records.
*
*      The trigger module will be called with a 20-byte
*      parameter which will contain the key, (except TRANNO),
*      of USWD - the Unit Switch Detail file. This will be
*      used to access all the corresponding UTRN records.
*
*      The Trigger Module will only carry out its full
*      processes when all the USWD records have all their
*      Actual prices. The first step is to attempt to find
*      the actual prices of all the funds on the USWD record
*      with the supplied key.
*
*      Use the Trigger Key to set up the partial key and
*      perform a BEGNH on USWD.
*
*      The record returned should match on the amount of the
*      key supplied, if it does not then report a system
*      error.
*
*      Use the TRANNO from the USWD record and each source
*      fund and source fund type in turn to read the
*      corresponding UTRN records. If the UTRN record has
*      a non zero Fund Amount for the transaction,
*      then set this value into the Actual Amount for the
*      corresponding Source Fund on USWD (with sign change)
*      and zeroise the Estimated Amount. Set the Feedback
*      Indicator to 'Y' and rewrite the UTRN record.
*
*      When all the UTRN records for the USWD record have
*      been processed rewrite the USWD record.
*
*      If some of the Source Funds on the USWD record still
*      remain with Estimated prices then no further
*      processing is necessary in this invocation of the
*      Trigger Module.
*
*      However if all the Source Funds have been updated then
*      the second phase of the Trigger Module should be
*      carried out.
*
*
* Phase Two
*
*
*      In this phase the Trigger Module attempts to finish
*      the processing for all of the Source Funds, that is,
*      to complete ALL the Actual Prices for ALL the Source
*      Funds within ALL the USWD records for the transaction.
*
*      Use a partial key of Company and Contract Number to
*      perform a BEGNH on USWD. All the USWD records which
*      match on this part of the key will be processed.
*
*      While it is processing it must keep a running total of
*      ALL of the Source Fund Contract Amounts for ALL the
*      USWD records for the transaction. This will give the
*      total value of the transaction which may be used in
*      charging by Phase Three. It will be maintained in
*      Contract Currency, which may be obtained from the
*      corresponding UTRN record.
*
*      Check the Source Funds on the USWD record. If all the
*      Source Funds have their Estimated Prices set to zero
*      then all the corresponding UTRN records for that USWD
*      have been completed and no further processing is
*      necessary on the USWD or its UTRN records in this
*      phase. All that is required is to maintain the total
*      value of the transaction by reading all the UTRN
*      records associated with the USWD and accumulating the
*      UTRN-CONTRACT-AMOUNT. Read the next USWD.
*
*      If some of the funds on the USWD still have non-zero
*      Estimated Values then use the TRANNO from the USWD and
*      the Source Fund and Source Fund Type to read the
*      corresponding UTRN record. If the UTRN-PRICE-USED is
*      non-zero then an Actual Price exists and the USWD
*      record should be updated accordingly. Zeroise the
*      corresponding Estimated Value and move the
*      UTRN-PRICE-USED to the Actual Value and rewrite the
*      USWD record. Maintain the total value of the
*      transaction by reading all the UTRN records associated
*      with the USWD and accumulating the UTRN-CONTRACT-AMOUNT.
*
*      Perform this check for all the Source Funds on the
*      USWD. If any of the Source Funds were updated then
*      update the USWD record.
*
*      Continue with this processing until the USWD Company
*      or Contract Number changes or end of file is reached.
*
*
* Phase Three
*
*
*      This phase will only be attempted if ALL the USWD
*      records have been FULLY updated by Phase Two, i.e.
*      EVERY USWD record has got an Actual Price for ALL of
*      its Source Funds. This fact should be determined while
*      processing Phase Two.
*
*      The purpose of this phase is to calculate all of the
*      Target Fund amounts and create all of the UTRN records
*      for buying units in the Target Funds. As this is done
*      the USWD records associated with the transaction will
*      be deleted. This phase will also determine whether or
*      not this transaction is a free switch or whether a
*      charge is necessary. If a charge is necessary then two
*      ACMV records are created.
*
*      It is important to note that monies switched out of
*      Initial units may only be used to buy into Initial
*      Units, and monies switched out of Accumulation Units
*      must be used to buy into Accumulation Units.
*
* Charges
*
*
*      The first part of Phase Three is to process the
*      charges, if any. First determine whether or not the
*      current switch is free or chargeable.
*
*      To do this you will require the following fields from
*      Contract Header:
*
*           Risk Commencement Date;
*           Total Switches Used;
*           Total Free Switches Left;
*           Date of Last Switch;
*
*      and from T5544:
*
*           Number of Free Switches, (per period);
*           Period, in Policy Years; and
*           Carry Forward Figure.
*
*      The contract is allowed a certain number of free
*      switches in any given period, the period being
*      expressed in policy years. One policy year being a
*      whole year from Risk Commencement Date less one day.
*      At the end of each period a certain number of unused
*      free switches may be carried forward to the next
*      period.
*
*      The calculations will be as follows:
*
*      1.   If the 'Override Switch Fee' field on the USWD
*           record is set to 'Y', NO FEE is to be charged. If
*           the 'Override Switch Fee' field on the USWD
*           record is NOT set to 'Y', proceed as shown below.
*
*      2.   Compare the effective date of the switch and the
*           Risk Commencement Date and work out in which
*           period, (i.e. Period #1, Period #2 etc.), the
*           switch is taking place.
*
*      3.   Last Switch In Current Period: Take the date of
*           the last switch, if this is in the current period
*           then the total of free switches left will be
*           found in the 'Total Free Switches Left' field on
*           the Contract Header.
*
*           If this figure is greater than zero then it will
*           be a free switch, so decrement this value on the
*           Contract Header by one, increment the 'Total Used
*           So Far' by one and set the 'Date of Last Switch'
*           to the Switch Effective Date and update the
*           Contract Header.
*
*           If it is less than one a charge will be
*           necessary, so set the 'Total Free Switches Left'
*           on the Contract Header to zero, set the 'Date of
*           Last Switch' to the Switch Effective Date and
*           update the Contract Header.
*
*      4.   Last Switch In Previous Period: Take the date of
*           the last switch, if this is in any previous
*           period then some calculation will be necessary to
*           determine how many free switches, if any, are
*           available in the current period.
*
*           Establish the number of complete "Periods" from
*           the 'Switch Anniversary Date' (remember that
*           T5544 defines how many years there are between
*           'Switch Anniversary Dates') which next followed
*           the date of the last Switch and the Effective
*           date of the present Switch. For each such period,
*           accumulate the number of Free Switches available
*           and add to this, the 'Number of Free Switches
*           Available' from the Contract Header.
*
*           The lower of this figure and the ('Carry Forward'
*           figure from T5544 plus the 'Number of Free
*           Switches per Period' from T5544) will be the current
*           'Number of Free Switches Available'.
*
*           If this is greater than zero then this is a free
*           switch, so decrement it by one and set the new
*           value in the field on the Contract Header.
*           Increment the 'Total Used So Far' by 1 and set
*           the 'Date of Last Switch' on the Contract Header
*           to the Switch Effective Date and update the
*           Contract Header.
*
*           If it is less than one a charge will be
*           necessary, so set the 'Total Free Switches Left'
*           on the Contract Header to zero, set the 'Date of
*           Last Switch' to the Switch Effective Date and
*           update the Contract Header.
*
*      If this is a free switch then no more processing is
*      necessary in this phase, otherwise the charge must be
*      calculated and an ACMV record written.
*
*      The charge itself is calculated or obtained from
*      T5544. This will provide either a flat charge or a
*      percentage. If a flat charge is found then use this,
*      otherwise calculate the charge as the percentage of
*      the total transaction value calculated previously,
*      (this will be the total of all the actual source funds
*      on all the USWD records).
*
*      If the calculated value falls outside the Minimum or
*      Maximum fee values from T5544, (providing that these
*      are non-zero, e.g. there could be a minimum and no
*      maximum), then use the appropriate one of these
*      instead of the calculated value.
*
*      Create two ACMV records with details of the charge. The
*      amount of the charge will be deducted from the total
*      transaction amount and this reduced figure will then
*      be used when buying into the target funds. If a flat
*      fee was used calculate what that represents as a
*      percentage of the total transaction value. The G/L
*      code for the ACMV records will be found on table T5645.
*      Read T5645 with the program id. - FUNDSWCH - as a key.
*      Line 3 will provide the G/L code for the switch
*      account which must go on to the UTRN record and lines
*      1 & 2 will provide the G/L code for the switch charge
*      accounts for the ACMV records.
*
* Main Processing
*
*
*      The main driver through this phase will be the USWD
*      records. Use a generic key of Company and Contract
*      Number from the Trigger Key and perform a BEGNH on
*      USWD. Continue processing the USWD using NEXTR until
*      that portion of the key changes or end of file is
*      reached.
*
*      For each USWD record use the TRANNO and each Source
*      Fund and Source Fund Type to read each associated UTRN
*      record. This will be necessary to obtain the Contract
*      Amount.
*
*      As each USWD record is being processed build up the
*      "Total Initial Unit Value", (TIUV), and the "Total
*      Accumulation Unit Value", (TAUV) for all the Source
*      Funds on the USWD. This value will be calculated in
*      Contract Currency so use the UTRN-CONTRACT-AMOUNT.
*
*      If there is a mixture of Initial and Accumulation
*      units on the Source Fund list then two UTRN records
*      will have to be written for each Target Fund. The
*      amount on the UTRN record will be in Contract Currency
*      so place the calculated value in UTRN-CONTRACT-AMOUNT.
*      The Total Initial Unit Value, (TIUV), will be spread
*      across all Target Funds and then the Total
*      Accumulation Unit Value, (TAUV), will be spread across
*      all Target Funds using the percentages specified on
*      the USWD record.
*
*      If TIUV is non-zero then for each Target Fund on the
*      USWD create a positive UTRN record with Fund Type as
*      'I' for Initial and a value calculated as TIUV
*      multiplied by the fund's corresponding target
*      percentage (from USWD). If a charge is being made
*      then before the previous calculation is performed
*      reduce the TIUV by the charge percentage. In this
*      situation also, the appropriate Discount Factor must
*      be obtained and inserted into the Target Fund UTRN.
*      Read T5540 for the General Unit Linked Details. Use
*      Discount Basis from T5540 (if present) to read T5519
*      for initial unit discount details. There are 3 methods
*      to obtain the Discount Factor. The one used is decided
*      by the entries read from T5519 & T5540 (Only one
*      method is allowed). If whole-of-life discount factor
*      (T5540-WHOLE-IU-DISC-FACT) is blank (i.e. term discount
*      basis is not blank) and the fixed term (T5519-FIXTRM)
*      is zero, calculate the 'term-left-to-run' and use this
*      to read the appropriate Discount Factor from T5539. If
*      whole-of-life discount factor (T5540-WHOLE-IU-DISC-
*      FACT) is blank (i.e. term discount basis is not blank)
*      and the fixed term (T5519-FIXTRM) is NOT zero, use the
*      Fixed Term to read the appropriate Discount Factor
*      from T5539. If whole-of-life discount factor
*      (T5540-WHOLE-IU-DISC-FACT) is not blank, use the age
*      next birthday to obtain from T6646 the Discount
*      Factor. Lastly, only for Initial Unit UTRN records,
*      the UTRN-CONTRACT-AMOUNT as set above, must be divided
*      by the Discount Factor and the result used to
*      overwrite the UTRN-CONTRACT-AMOUNT. This ensures that
*      when the UTRN is processed inside UNITDEAL, that the
*      correct number of deemed and real Initial Units are
*      bought.
*
*      If TAUV is non-zero then for each Target Fund on the
*      USWD create a positive UTRN record with Fund Type as
*      'A' for Accumulation and a value calculated as TAUV
*      multiplied by the fund's corresponding target
*      percentage (from USWD). If a charge is being made
*      then before the previous calculation is performed
*      reduce the TAUV by the charge percentage.
*
*      Ensure that the field UTRN-SWITCH-INDICATOR is set to
*      'Y'.
*
*      When all the Target Funds on the USWD record have been
*      processed delete the USWD record.
*
*      When all the USWD records have been deleted delete the
*      USWH record.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*      Complete HITR records created by the on-line
*      transaction for source funds.
*
*      Write new HITR records for target funds which are
*      interest bearing, and if an HITD record does not
*      already exist for the fund, create one.
*
*
*****************************************************************
* </pre>
*/
public class Fundswch extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "FUNDSWCH";
	private static final String e308 = "E308";
	private static final String h115 = "H115";
	private static final String h116 = "H116";
	private static final String g033 = "G033";
	private static final String g029 = "G029";
	protected static final String g437 = "G437";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaProcSeqNo = new PackedDecimalData(3, 0).init(ZERO);
	private FixedLengthStringData wsaaT5515Item = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaEndProc = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaUpdateUswd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaContractTotal = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaTermLeftToRun = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaTermLeft = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler = new FixedLengthStringData(11).isAPartOf(wsaaTermLeft, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermLeftInteger = new ZonedDecimalData(6, 0).isAPartOf(filler, 0).setUnsigned();
	private ZonedDecimalData wsaaTermLeftRemain = new ZonedDecimalData(5, 0).isAPartOf(filler, 6).setUnsigned();
		/*                                                         <D9604>*/
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
		/* WSAA-TABLE */
	private FixedLengthStringData[] wsaaCurr = FLSInittedArray(20, 4);
	private ZonedDecimalData wsaaPeriodFactor = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaWholePeriods = new ZonedDecimalData(6, 0);
	private ZonedDecimalData wsaaPeriodYears = new ZonedDecimalData(6, 0);
	private ZonedDecimalData wsaaCurrPeriodStartD = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNoFreeSwitches = new ZonedDecimalData(4, 0);
	private ZonedDecimalData wsaaMaxCfrwd = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private static final long wsaaSequenceNo = 0L;
	private ZonedDecimalData wsaaTotalTax = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSwitchFee = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTranref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTranref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 15);

	private FixedLengthStringData wsaaBatckey = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaBatckey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaBatckey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaBatckey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaBatckey, 14);
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).setUnsigned();
	private FixedLengthStringData wsaaDescription = new FixedLengthStringData(30);
	private PackedDecimalData wsaaCalc = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAdjustmentDiff = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTiuv = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTauv = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTiuvValueRemain = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTauvValueRemain = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaChargeTiuv = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaChargeTauv = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRemainder = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTiuvValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTauvValue = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaTiuvRec = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaTauvRec = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaUtrnBatctrcde = new FixedLengthStringData(4);
	private PackedDecimalData wsaaChargePercent = new PackedDecimalData(18, 9);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT5544Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaUsmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 4);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* WSAA-REPLY */
	private FixedLengthStringData wsaaBomb = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaFormat = new FixedLengthStringData(10);
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private HitdTableDAM hitdIO = new HitdTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	protected UswdTableDAM uswdIO = new UswdTableDAM();
	protected UswhTableDAM uswhIO = new UswhTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	protected Syserrrec syserrrec = new Syserrrec();
	protected Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5544rec t5544rec = new T5544rec();
	private T6647rec t6647rec = new T6647rec();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T5519rec t5519rec = new T5519rec();
	private T5539rec t5539rec = new T5539rec();
	private T5540rec t5540rec = new T5540rec();
	private T6646rec t6646rec = new T6646rec();
	private T5688rec t5688rec = new T5688rec();
	private T6659rec t6659rec = new T6659rec();
	private Th510rec th510rec = new Th510rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	protected Tr384rec tr384rec = new Tr384rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Udtrigrec udtrigrec = new Udtrigrec();
	protected FormatsInner formatsInner = new FormatsInner();
	protected TablesInner tablesInner = new TablesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		uswd1220,
		exit1290,
		checkFund1320,
		exit1390,
		estVals2020,
		exit2090,
		uswd2110,
		incSub2130,
		read2310,
		nxtFund2350,
		endFreeSwProc3055,
		delUswd3075,
		uswh3085,
		readUtrn3510,
		nxtFund3530,
		tiuv3620,
		tauv3640,
		format3660,
		hitr3680,
		nextFund3685,
		t56453930,
		decideDiscountMethod6100,
		fixedTermMethod6300,
		wholeOfLifeMethod6400,
		exit6900,
		a420Uswd,
		a490Exit
	}

	public Fundswch() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			mainline010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline010()
	{
		para010();
		exit090();
	}

	/**
	* <pre>
	*****INITIALIZE WSAA-SWITCH-ERROR-INFO.
	* </pre>
	*/
protected void para010()
	{
		udtrigrec.statuz.set(varcom.oK);
		lifacmvrec.transactionDate.set(getCobolDate());
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.user.set(ZERO);
		phaseOne1000();
		if (isEQ(wsaaEndProc, "Y")) {
			return ;
		}
		phaseTwo2000();
		if (isEQ(wsaaEndProc, "Y")) {
			return ;
		}
		phaseThree3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readT5540200()
	{
		para205();
	}

protected void para205()
	{
		itdmIO.setDataKey(SPACES);
		/* MOVE PARM-COMPANY           TO ITDM-ITEMCOY.            <022>*/
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		/* MOVE PARM-EFFDATE           TO ITDM-ITMFRM.             <022>*/
		itdmIO.setItmfrm(udtrigrec.effdate);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		/* IF (ITDM-ITEMCOY            NOT = PARM-COMPANY) OR      <022>*/
		if ((isNE(itdmIO.getItemcoy(), udtrigrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
		|| (isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable()))) {
			itdmIO.setGenarea(SPACES);
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
	}

protected void readT5539300()
	{
		para305();
	}

protected void para305()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		/* MOVE PARM-COMPANY           TO ITEM-ITEMCOY.            <022>*/
		itemIO.setItemcoy(udtrigrec.company);
		itemIO.setItemtabl(tablesInner.t5539);
		itemIO.setItemitem(t5540rec.iuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
	}

protected void readT6646400()
	{
		para405();
	}

protected void para405()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		/* MOVE PARM-COMPANY             TO ITEM-ITEMCOY.          <022>*/
		itemIO.setItemcoy(udtrigrec.company);
		itemIO.setItemtabl(tablesInner.t6646);
		itemIO.setItemitem(t5540rec.wholeIuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}

protected void readT5519500()
	{
		para505();
	}

protected void para505()
	{
		itdmIO.setDataKey(SPACES);
		/* MOVE PARM-COMPANY           TO ITDM-ITEMCOY.            <022>*/
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(tablesInner.t5519);
		/* MOVE PARM-EFFDATE           TO ITDM-ITMFRM.             <022>*/
		itdmIO.setItmfrm(udtrigrec.effdate);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		/* IF (ITDM-ITEMCOY NOT = PARM-COMPANY) OR                 <022>*/
		if ((isNE(itdmIO.getItemcoy(), udtrigrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5519))
		|| (isNE(itdmIO.getItemitem(), t5540rec.iuDiscBasis))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
	}

protected void phaseOne1000()
	{
		initialise1005();
		readhUswd1010();
		processUtrn1020();
		checkUswd1030();
	}

protected void initialise1005()
	{
		wsaaCalc.set(ZERO);
		wsaaEndProc.set(SPACES);
	}

protected void readhUswd1010()
	{
		uswdIO.setDataKey(SPACES);
		/* MOVE WSAA-CHDRCOY           TO USWD-CHDRCOY.                 */
		/* MOVE WSAA-CHDRNUM           TO USWD-CHDRNUM.                 */
		/* MOVE WSAA-PLAN-SUFFIX       TO USWD-PLAN-SUFFIX.             */
		/* MOVE WSAA-LIFE              TO USWD-LIFE.                    */
		/* MOVE WSAA-COVERAGE          TO USWD-COVERAGE.                */
		/* MOVE WSAA-RIDER             TO USWD-RIDER.                   */
		/* MOVE WSAA-TRANNO            TO USWD-TRANNO.                  */
		uswdIO.setChdrcoy(udtrigrec.chdrcoy);
		uswdIO.setChdrnum(udtrigrec.chdrnum);
		uswdIO.setPlanSuffix(udtrigrec.planSuffix);
		uswdIO.setLife(udtrigrec.life);
		uswdIO.setCoverage(udtrigrec.coverage);
		uswdIO.setRider(udtrigrec.rider);
		uswdIO.setTranno(udtrigrec.tranno);
		uswdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
	}

protected void processUtrn1020()
	{
		wsaaT5515Item.set(udtrigrec.unitVirtualFund);
		a200ReadT5515();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			a300ProcessHitr();
			return ;
		}
		processUtrn1100();
	}

protected void checkUswd1030()
	{
		uswdEstPrice1300();
		/*REWRITE-USWD*/
		/*    Rewrites USWD record*/
		uswdIO.setFunction(varcom.rewrt);
		uswdIO.setFormat(formatsInner.uswdrec);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void processUtrn1100()
	{
		standardKey1120();
		read1130();
		actualPrice1140();
	}

	/**
	* <pre>
	*    Reads the UTRN record using the linkage key
	*    & processes if Actual Price exists
	* </pre>
	*/
protected void standardKey1120()
	{
		utrnIO.setDataKey(SPACES);
		utrnIO.setChdrcoy(uswdIO.getChdrcoy());
		/* MOVE WSAA-CHDRNUM           TO UTRN-CHDRNUM.                 */
		/* MOVE WSAA-LIFE              TO UTRN-LIFE.                    */
		/* MOVE WSAA-COVERAGE          TO UTRN-COVERAGE.                */
		/* MOVE WSAA-RIDER             TO UTRN-RIDER.                   */
		/* MOVE WSAA-PLAN-SUFFIX       TO UTRN-PLAN-SUFFIX.             */
		/* MOVE WSAA-TRANNO            TO UTRN-TRANNO.                  */
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setTranno(udtrigrec.tranno);
	}

protected void read1130()
	{
		/* MOVE WSAA-UNIT-VIRT-FUND    TO UTRN-UNIT-VIRTUAL-FUND.       */
		/* MOVE WSAA-UNIT-TYPE         TO UTRN-UNIT-TYPE.               */
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
		wsaaProcSeqNo.set(utrnIO.getProcSeqNo());
	}

protected void actualPrice1140()
	{
		if (isNE(utrnIO.getFundAmount(), ZERO)) {
			wsaaSub.set(1);
			/*PERFORM 1200-UPDATE-RECS.                                 */
			updateRecs1200();
		}
		else {
			utrnIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, utrnIO);
			if (isNE(utrnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(utrnIO.getParams());
				fatalError9000();
			}
		}
		/*EXIT*/
	}

protected void updateRecs1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					findFund1210();
				case uswd1220:
					uswd1220();
					rewriteUtrn1230();
				case exit1290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Updates USWD & Rewrites UTRN if Actual Price exists
	* </pre>
	*/
protected void findFund1210()
	{
		if (isEQ(uswdIO.getSrcfund(wsaaSub), utrnIO.getUnitVirtualFund())
		&& isEQ(uswdIO.getScfndtyp(wsaaSub), utrnIO.getUnitType())) {
			goTo(GotoLabel.uswd1220);
		}
		wsaaSub.add(1);
		if (isGT(wsaaSub, 20)) {
			goTo(GotoLabel.exit1290);
		}
		findFund1210();
		return ;
	}

protected void uswd1220()
	{
		/* MULTIPLY UTRN-FUND-AMOUNT   BY -1 GIVING                     */
		setPrecision(uswdIO.getScactval(wsaaSub), 2);
		uswdIO.setScactval(wsaaSub, mult(utrnIO.getContractAmount(), -1));
		uswdIO.setScestval(wsaaSub, ZERO);
	}

protected void rewriteUtrn1230()
	{
		utrnIO.setFeedbackInd("Y");
		utrnIO.setFormat(formatsInner.utrnrec);
		utrnIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void uswdEstPrice1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					sub1310();
				case checkFund1320:
					checkFund1320();
				case exit1390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Checks for Source funds on USWD with estimated prices
	*    remaining
	* </pre>
	*/
protected void sub1310()
	{
		wsaaSub.set(1);
	}

protected void checkFund1320()
	{
		if (isGT(uswdIO.getScestval(wsaaSub), ZERO)) {
			wsaaEndProc.set("Y");
			goTo(GotoLabel.exit1390);
		}
		/*INC-SUB*/
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 20)) {
			goTo(GotoLabel.checkFund1320);
		}
	}

protected void phaseTwo2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begnUswd2010();
				case estVals2020:
					estVals2020();
					updateUswd2040();
					nextUswd2050();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnUswd2010()
	{
		wsaaUpdateUswd.set(SPACES);
		uswdIO.setDataKey(SPACES);
		/* MOVE WSAA-CHDRCOY           TO USWD-CHDRCOY.                 */
		/* MOVE WSAA-CHDRNUM           TO USWD-CHDRNUM.                 */
		/* MOVE WSAA-TRANNO            TO USWD-TRANNO.                  */
		/* MOVE ZERO                   TO USWD-PLAN-SUFFIX.             */
		uswdIO.setChdrcoy(udtrigrec.chdrcoy);
		uswdIO.setChdrnum(udtrigrec.chdrnum);
		uswdIO.setPlanSuffix(udtrigrec.planSuffix);
		uswdIO.setLife(udtrigrec.life);
		uswdIO.setCoverage(udtrigrec.coverage);
		uswdIO.setRider(udtrigrec.rider);
		uswdIO.setTranno(udtrigrec.tranno);
		/* MOVE BEGNH                  TO USWD-FUNCTION.                */
		uswdIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
//		uswdIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		|| isNE(udtrigrec.chdrcoy, uswdIO.getChdrcoy())
		|| isNE(udtrigrec.chdrnum, uswdIO.getChdrnum())
		|| isNE(udtrigrec.planSuffix, uswdIO.getPlanSuffix())
		|| isNE(udtrigrec.life, uswdIO.getLife())
		|| isNE(udtrigrec.coverage, uswdIO.getCoverage())
		|| isNE(udtrigrec.rider, uswdIO.getRider())
		|| isNE(udtrigrec.tranno, uswdIO.getTranno())) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		wsaaContractTotal.set(ZERO);
	}

protected void estVals2020()
	{
		wsaaSub.set(1);
		estValues2100();
		/*ACCUM-TOT*/
		wsaaSub.set(1);
		totalUtrns2300();
	}

protected void updateUswd2040()
	{
		if (isNE(wsaaUpdateUswd, "Y")) {
			/*     GO 2050-NEXT-USWD.                                       */
			goTo(GotoLabel.exit2090);
			/* UNREACHABLE CODE
			uswdIO.setFormat(formatsInner.uswdrec);
			 */
		}
		/* MOVE REWRT                  TO USWD-FUNCTION.                */
		uswdIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		wsaaUpdateUswd.set(SPACES);
	}

protected void nextUswd2050()
	{
		nextUswd2400();
		if (isNE(uswdIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.estVals2020);
		}
	}

protected void estValues2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case uswd2110:
					uswd2110();
				case incSub2130:
					incSub2130();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Checks Estimated values on USWD record
	* </pre>
	*/
protected void uswd2110()
	{
		if (isLTE(uswdIO.getScestval(wsaaSub), ZERO)) {
			goTo(GotoLabel.incSub2130);
		}
		/*UPDATE*/
		wsaaT5515Item.set(uswdIO.getSrcfund(wsaaSub));
		a200ReadT5515();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			a500UpdateUswd();
			goTo(GotoLabel.incSub2130);
		}
		updateUswd2200();
	}

protected void incSub2130()
	{
		wsaaSub.add(1);
		if (isLT(wsaaSub, 21)) {
			goTo(GotoLabel.uswd2110);
		}
		/*EXIT*/
	}

protected void updateUswd2200()
	{
		utrn2210();
		actualPrice2220();
	}

	/**
	* <pre>
	*    Attempts to update USWD with Actual Price from UTRN
	* </pre>
	*/
protected void utrn2210()
	{
		utrnIO.setChdrcoy(uswdIO.getChdrcoy());
		utrnIO.setChdrnum(uswdIO.getChdrnum());
		utrnIO.setLife(uswdIO.getLife());
		utrnIO.setCoverage(uswdIO.getCoverage());
		utrnIO.setRider(uswdIO.getRider());
		utrnIO.setPlanSuffix(uswdIO.getPlanSuffix());
		utrnIO.setUnitVirtualFund(uswdIO.getSrcfund(wsaaSub));
		utrnIO.setUnitType(uswdIO.getScfndtyp(wsaaSub));
		utrnIO.setTranno(uswdIO.getTranno());
		/*MOVE READH                  TO UTRN-FUNCTION.                */
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void actualPrice2220()
	{
		if (isEQ(utrnIO.getFundAmount(), ZERO)) {
			/*    MOVE 'Y'                 TO WSAA-END-PROC                 */
			return ;
		}
		setPrecision(uswdIO.getScactval(wsaaSub), 2);
		uswdIO.setScactval(wsaaSub, mult(utrnIO.getFundAmount(), -1));
		uswdIO.setScestval(wsaaSub, ZERO);
		wsaaUpdateUswd.set("Y");
		/*EXIT*/
	}

protected void totalUtrns2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case read2310:
					read2310();
					add2340();
				case nxtFund2350:
					nxtFund2350();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2310()
	{
		wsaaT5515Item.set(uswdIO.getSrcfund(wsaaSub));
		a200ReadT5515();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			a600TotalHitrs();
			goTo(GotoLabel.nxtFund2350);
		}
		utrnIO.setChdrcoy(uswdIO.getChdrcoy());
		utrnIO.setChdrnum(uswdIO.getChdrnum());
		utrnIO.setLife(uswdIO.getLife());
		utrnIO.setCoverage(uswdIO.getCoverage());
		utrnIO.setRider(uswdIO.getRider());
		utrnIO.setPlanSuffix(uswdIO.getPlanSuffix());
		utrnIO.setUnitVirtualFund(uswdIO.getSrcfund(wsaaSub));
		utrnIO.setUnitType(uswdIO.getScfndtyp(wsaaSub));
		utrnIO.setTranno(uswdIO.getTranno());
		/*MOVE READH                   TO UTRN-FUNCTION.               */
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void add2340()
	{
		if (isEQ(wsaaSub, 1)) {
			wsaaUtrnBatctrcde.set(utrnIO.getBatctrcde());
		}
		wsaaContractTotal.add(utrnIO.getContractAmount());
	}

protected void nxtFund2350()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 20)
		|| isEQ(uswdIO.getSrcfund(wsaaSub), SPACES)) {
			return ;
		}
		goTo(GotoLabel.read2310);
	}

protected void nextUswd2400()
	{
		/*NEXT*/
		uswdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		/* IF USWD-STATUZ               = O-K AND                       */
		/*    USWD-CHDRCOY              = WSAA-CHDRCOY AND              */
		/*    USWD-CHDRNUM              = WSAA-CHDRNUM                  */
		/*    GO 2490-EXIT.                                             */
		if (isEQ(uswdIO.getStatuz(), varcom.oK)
		&& isEQ(uswdIO.getChdrcoy(), udtrigrec.chdrcoy)
		&& isEQ(uswdIO.getChdrnum(), udtrigrec.chdrnum)
		&& isEQ(uswdIO.getPlanSuffix(), udtrigrec.planSuffix)
		&& isEQ(uswdIO.getLife(), udtrigrec.life)
		&& isEQ(uswdIO.getCoverage(), udtrigrec.coverage)
		&& isEQ(uswdIO.getRider(), udtrigrec.rider)
		&& isEQ(uswdIO.getTranno(), udtrigrec.tranno)) {
			return ;
		}
		uswdIO.setStatuz(varcom.endp);
		/*EXIT*/
	}

protected void phaseThree3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					conHdr3010();
					readT56453015();
					readUswd3020();
					tables3030();
					period3040();
				case endFreeSwProc3055:
					endFreeSwProc3055();
					readTr3843057();
					mainProcessing3060();
					utrnRead3065();
				case delUswd3075:
					delUswd3075();
					nextUswd3080();
				case uswh3085:
					uswh3085();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void conHdr3010()
	{
		/*    Reads Contract Header*/
		chdrmjaIO.setDataKey(SPACES);
		/* MOVE WSAA-CHDRCOY            TO CHDRMJA-CHDRCOY.             */
		/* MOVE WSAA-CHDRNUM            TO CHDRMJA-CHDRNUM.             */
		chdrmjaIO.setChdrcoy(udtrigrec.chdrcoy);
		chdrmjaIO.setChdrnum(udtrigrec.chdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
	}

protected void readT56453015()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.begn);
		/* MOVE WSAA-CHDRCOY           TO ITEM-ITEMCOY.            <019>*/
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itemIO.setItemcoy(udtrigrec.chdrcoy);
		itemIO.setItemtabl("T5645");
		itemIO.setItemitem("FUNDSWCH");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Check to see if Component or Contract level accounting          */
		/* is required.                                                    */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setItempfx("IT");
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readUswd3020()
	{
		/*    Read first USWD to obtain Switch Effective Date*/
		uswdIO.setDataKey(SPACES);
		/* MOVE WSAA-CHDRCOY            TO USWD-CHDRCOY.                */
		/* MOVE WSAA-CHDRNUM            TO USWD-CHDRNUM.                */
		/* MOVE WSAA-PLAN-SUFFIX        TO USWD-PLAN-SUFFIX.            */
		/* MOVE WSAA-LIFE               TO USWD-LIFE.                   */
		/* MOVE WSAA-COVERAGE           TO USWD-COVERAGE.               */
		/* MOVE WSAA-RIDER              TO USWD-RIDER.                  */
		/* MOVE WSAA-TRANNO             TO USWD-TRANNO.                 */
		uswdIO.setChdrcoy(udtrigrec.chdrcoy);
		uswdIO.setChdrnum(udtrigrec.chdrnum);
		uswdIO.setPlanSuffix(udtrigrec.planSuffix);
		uswdIO.setLife(udtrigrec.life);
		uswdIO.setCoverage(udtrigrec.coverage);
		uswdIO.setRider(udtrigrec.rider);
		uswdIO.setTranno(udtrigrec.tranno);
		uswdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
	}

protected void tables3030()
	{
		t6647T55443100();
		if (isNE(t6647rec.unitStatMethod, SPACES)) {
			a100ReadT6659();
		}
	}

protected void period3040()
	{
		/*    Works out in which period the switch is taking place*/
		/*    MOVE USWD-EFFDATE            TO WSAA-WORKDATE.               */
		/*    MOVE WSAA-WORKDATE           TO WSAA-EFFDATE.                */
		/*    MOVE CHDRMJA-CCDATE          TO WSAA-WORKDATE.               */
		/*    MOVE WSAA-WORKDATE           TO WSAA-CCDATE.                 */
		/*    SUBTRACT WSAA-CC-YR          FROM WSAA-EFF-YR                */
		/*                                 GIVING WSAA-YEAR.               */
		/*    IF  WSAA-EFF-DDMM            < WSAA-CC-DDMM                  */
		/*        MOVE WSAA-YEAR           TO WSAA-SW-PERIOD               */
		/*    ELSE                                                         */
		/*        ADD 1 WSAA-YEAR          GIVING WSAA-SW-PERIOD.          */
		/*3050-LAST-SW-PERIOD.                                             */
		/*    Works out in which period the last switch took place*/
		/*    IF CHDRMJA-LAST-SWITCH-DATE  = ZERO                          */
		/*       MOVE ZERO                 TO WSAA-LSW-PERIOD              */
		/*       GO 3055-CURR-PERIOD.                                      */
		/*    MOVE CHDRMJA-LAST-SWITCH-DATE TO WSAA-WORKDATE.              */
		/*    MOVE WSAA-WORKDATE           TO WSAA-EFFDATE.                */
		/*    SUBTRACT WSAA-CC-YR          FROM WSAA-EFF-YR                */
		/*                                 GIVING WSAA-YEAR.               */
		/*    IF  WSAA-EFF-DDMM            < WSAA-CC-DDMM                  */
		/*        MOVE WSAA-YEAR           TO WSAA-LSW-PERIOD              */
		/*    ELSE                                                         */
		/*        ADD 1 WSAA-YEAR          GIVING WSAA-LSW-PERIOD.         */
		/*3055-CURR-PERIOD.                                                */
		/*    Works out Current Period                                     */
		/*    MOVE TDAY                    TO DTC1-FUNCTION.               */
		/*    CALL 'DATCON1'               USING DTC1-DATCON1-REC.         */
		/*    MOVE DTC1-INT-DATE           TO WSAA-TODAY.                  */
		/*    SUBTRACT WSAA-CC-YR          FROM WSAA-TD-YR                 */
		/*                                 GIVING WSAA-YEAR.               */
		/*    IF WSAA-TD-DDMM              < WSAA-CC-DDMM                  */
		/*       MOVE WSAA-YEAR            TO WSAA-CURR-PERIOD             */
		/*    ELSE                                                         */
		/*       ADD 1 WSAA-YEAR           GIVING WSAA-CURR-PERIOD.        */
		/*    IF  WSAA-LSW-PERIOD          NOT < WSAA-CURR-PERIOD OR       */
		/*        WSAA-LSW-PERIOD          = ZERO                          */
		/*        PERFORM 3200-CURR-PERIOD                                 */
		/*    ELSE                                                         */
		/*        PERFORM 3300-PREV-PERIOD.                                */
		/*    PERFORM 3100-T6647-T5544.                                    */
		/*    If this is the first switch ever, initialise the switch      */
		/*    fields.                                                      */
		if (isEQ(chdrmjaIO.getLastSwitchDate(), ZERO)) {
			chdrmjaIO.setFreeSwitchesLeft(t5544rec.noOfFreeSwitches);
			chdrmjaIO.setLastSwitchDate(chdrmjaIO.getCcdate());
			chdrmjaIO.setFreeSwitchesUsed(ZERO);
		}
		/*                                                       <011> */
		/* Work out last switch period and this switch period to  <011> */
		/* decide whether switches need to be carried forward     <011> */
		/* into this switch period.                               <011> */
		/*                                                       <011> */
		/*                                                        <011> */
		/* MOVE CHDRMJA-CCDATE         TO WSAA-CCDATE-9.          <012> */
		/* MOVE CHDRMJA-LAST-SWITCH-DATE TO WSAA-EFFDATE-9.       <012> */
		/* COMPUTE WSAA-YEAR       = WSAA-EFF-YR - WSAA-CC-YR.    <011> */
		/* IF WSAA-EFF-DDMM             > WSAA-CC-DDMM            <011> */
		/*     ADD 1                    TO WSAA-YEAR              <011> */
		/* END-IF.                                                <011> */
		/* COMPUTE WSAA-LSW-PERIOD=(WSAA-YEAR /T5544-PPYEARS) + 1.<011> */
		/*                                                        <011> */
		/* MOVE USWD-EFFDATE            TO WSAA-EFFDATE-9.        <012> */
		/* COMPUTE WSAA-YEAR       = WSAA-EFF-YR - WSAA-CC-YR.    <011> */
		/* IF  WSAA-EFF-DDMM            > WSAA-CC-DDMM            <011> */
		/*     ADD 1                    TO WSAA-YEAR              <011> */
		/* END-IF.                                                <011> */
		/* COMPUTE WSAA-SW-PERIOD =(WSAA-YEAR /T5544-PPYEARS) + 1.<011> */
		/*                                                        <011> */
		/* IF WSAA-SW-PERIOD            > WSAA-LSW-PERIOD         <011> */
		/*    PERFORM 3200-CARRY-SWITCHES-FWD                     <011> */
		/* END-IF.                                                <011> */
		wsaaPeriodFactor.set(ZERO);
		wsaaWholePeriods.set(ZERO);
		wsaaPeriodYears.set(ZERO);
		wsaaCurrPeriodStartD.set(ZERO);
		wsaaNoFreeSwitches.set(ZERO);
		wsaaMaxCfrwd.set(ZERO);
		/*    Calculate the date at which the current switch period        */
		/*    started.                                                     */
		/*    If we are still within the first switch period or the        */
		/*    last switch occured within the current switch period         */
		/*    then no adjustment of the no. of free switches available     */
		/*    is neccessary.                                               */
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(chdrmjaIO.getCcdate());
		datcon3rec.intDate2.set(uswdIO.getEffdate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		compute(wsaaPeriodFactor, 5).set(div(datcon3rec.freqFactor, t5544rec.ppyears));
		if (isLT(wsaaPeriodFactor, 1)) {
			goTo(GotoLabel.endFreeSwProc3055);
		}
		wsaaWholePeriods.set(wsaaPeriodFactor);
		compute(wsaaPeriodYears, 0).set(mult(wsaaWholePeriods, t5544rec.ppyears));
		datcon2rec.function.set(SPACES);
		datcon2rec.intDate1.set(chdrmjaIO.getCcdate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaPeriodYears);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaCurrPeriodStartD.set(datcon2rec.intDate2);
		/* IF WSAA-CURR-PERIOD-START-D <  CHDRMJA-LAST-SWITCH-DATE <026>*/
		if (isLTE(wsaaCurrPeriodStartD, chdrmjaIO.getLastSwitchDate())) {
			goTo(GotoLabel.endFreeSwProc3055);
		}
		/*    If T5544 says no switches are to be carried forward          */
		/*    & we are in a new switch period then set CHDR free           */
		/*    switches left to T5544 no free switches per period.          */
		if (isEQ(t5544rec.cfrwd, ZERO)) {
			chdrmjaIO.setFreeSwitchesLeft(t5544rec.noOfFreeSwitches);
			goTo(GotoLabel.endFreeSwProc3055);
		}
		/*    Calculate the number of periods between the last switch      */
		/*    & the start date of the currrent switch period then cal-     */
		/*    ulate ((no. of periods + 1) * no. of switches per period)    */
		/*    + no. of switches left in prev period.                       */
		/*    The contract header no. of switches left will then be set    */
		/*    to the lesser of the above & T5544 no. to carry forward +    */
		/*    no. of free switches for new period.                         */
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(chdrmjaIO.getLastSwitchDate());
		datcon3rec.intDate2.set(wsaaCurrPeriodStartD);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		compute(wsaaPeriodFactor, 5).set(div(datcon3rec.freqFactor, t5544rec.ppyears));
		if (isLT(wsaaPeriodFactor, 1)) {
			compute(wsaaNoFreeSwitches, 0).set(add(chdrmjaIO.getFreeSwitchesLeft(), t5544rec.noOfFreeSwitches));
		}
		else {
			wsaaWholePeriods.set(wsaaPeriodFactor);
			compute(wsaaNoFreeSwitches, 0).set(add((mult((add(wsaaWholePeriods, 1)), t5544rec.noOfFreeSwitches)), chdrmjaIO.getFreeSwitchesLeft()));
		}
		compute(wsaaMaxCfrwd, 0).set(add(t5544rec.cfrwd, t5544rec.noOfFreeSwitches));
		if (isLT(wsaaNoFreeSwitches, wsaaMaxCfrwd)) {
			chdrmjaIO.setFreeSwitchesLeft(wsaaNoFreeSwitches);
		}
		else {
			chdrmjaIO.setFreeSwitchesLeft(wsaaMaxCfrwd);
		}
	}

protected void endFreeSwProc3055()
	{
		/*    Rewrite the CHDRMJA and charge if no free switches left      */
		rewriteChdrAndCharge3300();
	}

protected void readTr3843057()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(wsaaUtrnBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***");
			stringVariable2.addExpression(wsaaUtrnBatctrcde);
			stringVariable2.setStringInto(itemIO.getItemitem());
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError9000();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(g437);
				fatalError9000();
			}
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*3060-BEGN-USWD.
	* </pre>
	*/
protected void mainProcessing3060()
	{
		/*    Begin USWD for main processing*/
		uswdIO.setDataKey(SPACES);
		uswdIO.setPlanSuffix(ZERO);
		uswdIO.setTranno(ZERO);
		/* MOVE WSAA-CHDRCOY           TO USWD-CHDRCOY.                 */
		/* MOVE WSAA-CHDRNUM           TO USWD-CHDRNUM.                 */
		uswdIO.setChdrcoy(udtrigrec.chdrcoy);
		uswdIO.setChdrnum(udtrigrec.chdrnum);
		uswdIO.setPlanSuffix(udtrigrec.planSuffix);
		uswdIO.setLife(udtrigrec.life);
		uswdIO.setCoverage(udtrigrec.coverage);
		uswdIO.setRider(udtrigrec.rider);
		uswdIO.setTranno(udtrigrec.tranno);
		/* MOVE BEGNH                  TO USWD-FUNCTION.                */
		uswdIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
//		uswdIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		if (isEQ(uswdIO.getStatuz(), varcom.endp)
		|| isNE(uswdIO.getChdrcoy(), udtrigrec.chdrcoy)
		|| isNE(uswdIO.getChdrnum(), udtrigrec.chdrnum)
		|| isNE(uswdIO.getPlanSuffix(), udtrigrec.planSuffix)
		|| isNE(uswdIO.getLife(), udtrigrec.life)
		|| isNE(uswdIO.getCoverage(), udtrigrec.coverage)
		|| isNE(uswdIO.getRider(), udtrigrec.rider)
		|| isNE(uswdIO.getTranno(), udtrigrec.tranno)) {
			goTo(GotoLabel.uswh3085);
		}
	}

	/**
	* <pre>
	**** IF USWD-STATUZ               = ENDP OR
	****    (USWD-CHDRCOY             NOT = WSAA-CHDRCOY OR
	****     USWD-CHDRNUM             NOT = WSAA-CHDRNUM)
	****     GO 3085-USWH.
	* </pre>
	*/
protected void utrnRead3065()
	{
		/*    Read associated UTRN records totalling TIUV & TAUV*/
		wsaaTiuv.set(ZERO);
		wsaaTauv.set(ZERO);
		wsaaSub.set(1);
		utrnTots3500();
		calcChargeSplit4000();
		/*UTRN-WRITE*/
		/*    Tests accumulated totals to be > zero & if so,*/
		/*    writes positive UTRN records & replica UDEL records*/
		/*IF WSAA-SW-ERR               = 'Y'                           */
		/*  GO 3075-DEL-USWD.                                         */
		if (isEQ(wsaaTiuv, ZERO)
		&& isEQ(wsaaTauv, ZERO)) {
			goTo(GotoLabel.delUswd3075);
		}
		positiveUtrn3600();
	}

protected void delUswd3075()
	{
		/*    Delete USWD record once UTRN records have been written*/
		/* MOVE DELET                   TO USWD-FUNCTION.               */
		uswdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
	}

protected void nextUswd3080()
	{
		/*    Reads next USWD record for processing*/
		uswdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		if (isEQ(uswdIO.getStatuz(), varcom.endp)
		|| isNE(uswdIO.getChdrcoy(), udtrigrec.chdrcoy)
		|| isNE(uswdIO.getChdrnum(), udtrigrec.chdrnum)
		|| isNE(uswdIO.getPlanSuffix(), udtrigrec.planSuffix)
		|| isNE(uswdIO.getLife(), udtrigrec.life)
		|| isNE(uswdIO.getCoverage(), udtrigrec.coverage)
		|| isNE(uswdIO.getRider(), udtrigrec.rider)
		|| isNE(uswdIO.getTranno(), udtrigrec.tranno)) {
			goTo(GotoLabel.uswh3085);
		}
	}

	/**
	* <pre>
	**** IF USWD-STATUZ               = ENDP OR
	****    (USWD-CHDRCOY             NOT = WSAA-CHDRCOY OR
	****     USWD-CHDRNUM             NOT = WSAA-CHDRNUM)
	****     GO 3085-USWH.
	**** GO 3065-UTRN-READ.
	* </pre>
	*/
protected void uswh3085()
	{
		/*    Retrieves & deletes USWH record when all processing of USWD*/
		/*    is complete*/
		checkForOtherSwitchs8000();
		if (isNE(uswdIO.getStatuz(), varcom.endp)) {
			return ;
		}
		uswhIO.setDataKey(SPACES);
		/*MOVE USWD-CHDRCOY             TO USWH-CHDRCOY.               */
		/*MOVE USWD-CHDRNUM             TO USWH-CHDRNUM.               */
		/*MOVE USWD-TRANNO              TO USWH-TRANNO.                */
		/* MOVE WSAA-CHDRCOY             TO USWH-CHDRCOY.          <001>*/
		/* MOVE WSAA-CHDRNUM             TO USWH-CHDRNUM.          <001>*/
		/* MOVE WSAA-TRANNO              TO USWH-TRANNO.           <001>*/
		uswhIO.setChdrcoy(udtrigrec.chdrcoy);
		uswhIO.setChdrnum(udtrigrec.chdrnum);
		uswhIO.setTranno(udtrigrec.tranno);
		uswhIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError9000();
		}
		uswhIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError9000();
		}
		/*  Fund Switch Letter request if USWD processing is complete.     */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(wsaaUtrnBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(g437);
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
	}

protected void t6647T55443100()
	{
		t66473110();
		t55153115();
		t55443120();
	}

protected void t66473110()
	{
		/*    Reads table T6647 to get switch rule to access T5544 table*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		/* MOVE WSAA-CHDRCOY            TO ITDM-ITEMCOY.                */
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		itdmIO.setItemcoy(udtrigrec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6647);
		/* MOVE PARM-TRANSCODE          TO WSAA-TRANSCODE.              */
		wsaaTranscode.set(wsaaUtrnBatctrcde);
		wsaaCnttype.set(chdrmjaIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		/*MOVE USWD-EFFDATE            TO ITDM-ITMFRM.                 */
		/* MOVE CHDRMJA-CCDATE          TO ITDM-ITMFRM.            <011>*/
		itdmIO.setItmfrm(uswdIO.getEffdate());
		SmartFileCode.execute(appVars, itdmIO);
		/* IF ITDM-STATUZ               NOT = O-K                       */
		/*    MOVE ITDM-PARAMS          TO SYSR-PARAMS                  */
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		/* IF ITDM-ITEMCOY           NOT = WSAA-CHDRCOY            <027>*/
		if (isNE(itdmIO.getItemcoy(), udtrigrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6647)
		|| isNE(itdmIO.getItemitem(), wsaaT6647Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT6647Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h115);
			fatalError9000();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void t55153115()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		/* MOVE WSAA-CHDRCOY            TO ITDM-ITEMCOY.                */
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		itdmIO.setItemcoy(udtrigrec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t5515);
		/*MOVE USWD-EFFDATE            TO ITDM-ITMFRM.                 */
		/* MOVE CHDRMJA-CCDATE          TO ITDM-ITMFRM.            <011>*/
		itdmIO.setItmfrm(uswdIO.getEffdate());
		/* MOVE WSAA-UNIT-VIRT-FUND     TO ITDM-ITEMITEM.               */
		itdmIO.setItemitem(udtrigrec.unitVirtualFund);
		SmartFileCode.execute(appVars, itdmIO);
		/* IF ITDM-STATUZ               NOT = O-K                       */
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		/* IF ITDM-ITEMCOY           NOT = WSAA-CHDRCOY            <027>*/
		if (isNE(itdmIO.getItemcoy(), udtrigrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), udtrigrec.unitVirtualFund)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*       MOVE WSAA-UNIT-VIRT-FUND TO ITDM-ITEMITEM         <027>*/
			itdmIO.setItemitem(udtrigrec.unitVirtualFund);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h116);
			fatalError9000();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void t55443120()
	{
		/*    Reads table T5544 which indicates whether Bid or Offer price*/
		/*    is to be used*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		/* MOVE WSAA-CHDRCOY             TO ITDM-ITEMCOY.               */
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		itdmIO.setItemcoy(udtrigrec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t5544);
		wsaaUsmeth.set(t6647rec.swmeth);
		/*    MOVE T5515-CURRCODE           TO WSAA-CURRCODE.              */
		wsaaCurrcode.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5544Key);
		/*MOVE USWD-EFFDATE            TO ITDM-ITMFRM.                 */
		/* MOVE CHDRMJA-CCDATE          TO ITDM-ITMFRM.            <011>*/
		itdmIO.setItmfrm(uswdIO.getEffdate());
		SmartFileCode.execute(appVars, itdmIO);
		/* IF ITDM-STATUZ                NOT = O-K                      */
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		/* IF ITDM-ITEMCOY           NOT = WSAA-CHDRCOY            <027>*/
		if (isNE(itdmIO.getItemcoy(), udtrigrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5544)
		|| isNE(itdmIO.getItemitem(), wsaaT5544Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT5544Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g033);
			fatalError9000();
		}
		t5544rec.t5544Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	*3200-CURR-PERIOD SECTION.
	*    Last switch in Current Period processing
	*3210-SW-LEFT.
	*    IF CHDRMJA-FREE-SWITCHES-LEFT NOT > ZERO
	*       GO 3230-CHARGE.
	*    SUBTRACT 1                  FROM CHDRMJA-FREE-SWITCHES-LEFT.
	*    ADD 1                       TO CHDRMJA-FREE-SWITCHES-USED.
	*    MOVE USWD-EFFDATE           TO CHDRMJA-LAST-SWITCH-DATE.
	*    PERFORM 3400-REWRITE-CHDR.
	*    GO 3290-EXIT.
	*3230-CHARGE.
	*    MOVE ZERO                   TO CHDRMJA-FREE-SWITCHES-LEFT.
	*    MOVE USWD-EFFDATE           TO CHDRMJA-LAST-SWITCH-DATE.
	*    ADD 1                       TO CHDRMJA-FREE-SWITCHES-USED.
	*    PERFORM 3400-REWRITE-CHDR.
	*    PERFORM 3900-CHARGE.
	*3290-EXIT.
	*    EXIT.
	*3200-CARRY-SWITCHES-FWD SECTION.                           <011>
	*********************************                           <011>
	*3205-PARA.                                                 <011>
	*****                                                       <011>
	**** Check if last switch was in a previous period or number<011>
	**** of switches greater than maximum to be carried forward.<011>
	*****                                                       <011>
	**** ADD 1                       TO WSAA-LSW-PERIOD.        <011>
	****                                                        <011>
	**** IF (WSAA-SW-PERIOD          > WSAA-SW-PERIOD) OR       <011>
	****                                                        <024>
	**** IF (WSAA-SW-PERIOD          > WSAA-LSW-PERIOD) OR      <024>
	****    (T5544-CFRWD          < CHDRMJA-FREE-SWITCHES-LEFT) <011>
	****    MOVE T5544-CFRWD         TO WSAA-CFF                <011>
	**** ELSE                                                   <011>
	****    MOVE CHDRMJA-FREE-SWITCHES-LEFT TO WSAA-CFF         <011>
	**** END-IF.
	**** COMPUTE CHDRMJA-FREE-SWITCHES-LEFT =                   <011>
	****                        T5544-NO-OF-FREE-SWITCHES + WSAA-CFF.
	****                                                        <011>
	*3290-EXIT.                                                 <011>
	**** EXIT.                                                  <011>
	*3300-PREV-PERIOD SECTION.
	*    Processing if Last Switch in Previous Period
	*3310-CALC.
	*    IF T5544-CFRWD              < CHDRMJA-FREE-SWITCHES-LEFT
	*       MOVE T5544-CFRWD         TO WSAA-CFF
	*    ELSE
	*       MOVE CHDRMJA-FREE-SWITCHES-LEFT
	*                                TO WSAA-CFF.
	*    ADD T5544-PPYEARS           TO WSAA-CFF.
	*    ADD 1                       TO WSAA-LSW-PERIOD.
	*    IF  WSAA-LSW-PERIOD         NOT < WSAA-CURR-PERIOD
	*        GO 3315-REWRITE.
	*    IF T5544-CFRWD              < WSAA-CFF
	*       MOVE T5544-CFRWD         TO WSAA-CFF.
	*    ADD T5544-NO-OF-FREE-SWITCHES TO WSAA-CFF.
	*3315-REWRITE.
	*    Rewrite CHDR record
	*    MOVE USWD-EFFDATE           TO CHDRMJA-LAST-SWITCH-DATE.
	*    ADD 1                       TO CHDRMJA-FREE-SWITCHES-USED.
	*    IF WSAA-CFF                 > ZERO
	*       SUBTRACT 1        FROM   CHDRMJA-FREE-SWITCHES-LEFT
	*    ELSE
	*       MOVE ZERO                TO CHDRMJA-FREE-SWITCHES-LEFT.
	*    PERFORM 3400-REWRITE-CHDR.
	*3320-FREE-SW.
	*    IF WSAA-CFF                 > ZERO
	*       GO 3390-EXIT.
	*    PERFORM 3900-CHARGE.
	*3390-EXIT.
	*    EXIT.
	* </pre>
	*/
protected void rewriteChdrAndCharge3300()
	{
		/*PARA*/
		/*    Rewrite the CHDRMJA and charge if no free switches left.     */
		chdrmjaIO.setLastSwitchDate(uswdIO.getEffdate());
		/*    ADD 1                       TO CHDRMJA-FREE-SWITCHES-USED.   */
		if (isGT(chdrmjaIO.getFreeSwitchesLeft(), ZERO)) {
			setPrecision(chdrmjaIO.getFreeSwitchesLeft(), 0);
			chdrmjaIO.setFreeSwitchesLeft(sub(chdrmjaIO.getFreeSwitchesLeft(), 1));
			setPrecision(chdrmjaIO.getFreeSwitchesUsed(), 0);
			chdrmjaIO.setFreeSwitchesUsed(add(chdrmjaIO.getFreeSwitchesUsed(), 1));
		}
		else {
			if (isEQ(uswdIO.getOverrideFee(), "N")) {
				charge3900();
			}
		}
		rewriteChdr3400();
		/*EXIT*/
	}

protected void rewriteChdr3400()
	{
		/*REWRITE*/
		/*    Rewrites CHDRMJA record*/
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void utrnTots3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case readUtrn3510:
					readUtrn3510();
					totals3520();
				case nxtFund3530:
					nxtFund3530();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Accumulates Total Initial value & Total Accumulation Unit
	*    Value from UTRN records
	* </pre>
	*/
protected void readUtrn3510()
	{
		wsaaT5515Item.set(uswdIO.getSrcfund(wsaaSub));
		a200ReadT5515();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			a700HitrTots();
			goTo(GotoLabel.nxtFund3530);
		}
		utrnIO.setDataKey(SPACES);
		utrnIO.setChdrcoy(uswdIO.getChdrcoy());
		utrnIO.setChdrnum(uswdIO.getChdrnum());
		utrnIO.setLife(uswdIO.getLife());
		utrnIO.setCoverage(uswdIO.getCoverage());
		utrnIO.setRider(uswdIO.getRider());
		utrnIO.setPlanSuffix(uswdIO.getPlanSuffix());
		utrnIO.setTranno(uswdIO.getTranno());
		utrnIO.setUnitVirtualFund(uswdIO.getSrcfund(wsaaSub));
		utrnIO.setUnitType(uswdIO.getScfndtyp(wsaaSub));
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void totals3520()
	{
		/*    Add Contract Amount to TIUV or TAUV depending on value*/
		/*    of Unit Type*/
		if (isEQ(utrnIO.getUnitType(), "I")) {
			wsaaTiuv.add(utrnIO.getContractAmount());
		}
		if (isEQ(utrnIO.getUnitType(), "A")) {
			wsaaTauv.add(utrnIO.getContractAmount());
		}
	}

protected void nxtFund3530()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 20)) {
			return ;
		}
		if (isEQ(uswdIO.getSrcfund(wsaaSub), SPACES)) {
			return ;
		}
		goTo(GotoLabel.readUtrn3510);
	}

protected void positiveUtrn3600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					loadSrcCurrTable3605();
					sub3610();
				case tiuv3620:
					tiuv3620();
				case tauv3640:
					tauv3640();
					covr3650();
				case format3660:
					format3660();
					writeUtrn3670();
				case hitr3680:
					hitr3680();
				case nextFund3685:
					nextFund3685();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loadSrcCurrTable3605()
	{
		wsaaSub.set(ZERO);
		loadSrcCurrTab5000();
		batcdorrec.function.set("AUTO");
		/* MOVE PARM-COMPANY           TO WSAA-BATCCOY.            <026>*/
		/* MOVE PARM-BRANCH            TO WSAA-BATCBRN.            <026>*/
		/* MOVE PARM-ACCTYEAR          TO WSAA-BATCACTYR.          <026>*/
		/* MOVE PARM-ACCTMONTH         TO WSAA-BATCACTMN.          <026>*/
		/* MOVE WSAA-UTRN-BATCTRCDE    TO WSAA-BATCTRCDE.          <026>*/
		/* MOVE PARM-TRANID            TO BATD-TRANID.             <026>*/
		/* MOVE SPACES                 TO WSAA-BATCBATCH.          <026>*/
		/* MOVE WSAA-BATCKEY           TO BATD-BATCHKEY.           <026>*/
		batcdorrec.batchkey.set(udtrigrec.batchkey);
		batcdorrec.batch.set(SPACES);
		batcdorrec.prefix.set("BA");
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		wsaaBatckey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*    Writes positive UTRN records & replica UDEL records
	* </pre>
	*/
protected void sub3610()
	{
		wsaaSub.set(1);
		wsaaSub2.set(1);
	}

protected void tiuv3620()
	{
		wsaaT5515Item.set(uswdIO.getTgtfund(wsaaSub));
		a200ReadT5515();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			goTo(GotoLabel.hitr3680);
		}
		/*    Checks to see if TIUV UTRN has already been written for*/
		/*    this occurrence of Target Fund (WSAA-TIUV-REC = 'Y')*/
		/*    and if not, whether Initial Units apply (WSAA-TIUV > ZERO)*/
		wsaaAdjustmentDiff.set(ZERO);
		if (isEQ(wsaaTiuv, ZERO)
		|| isEQ(wsaaTiuvRec, "Y")) {
			goTo(GotoLabel.tauv3640);
		}
		calculateTiuvValue3700();
		covrmjaIO.setDataKey(SPACES);
		covrmjaIO.setChdrcoy(uswdIO.getChdrcoy());
		covrmjaIO.setChdrnum(uswdIO.getChdrnum());
		covrmjaIO.setLife(uswdIO.getLife());
		covrmjaIO.setCoverage(uswdIO.getCoverage());
		covrmjaIO.setRider(uswdIO.getRider());
		if (isLTE(uswdIO.getPlanSuffix(), chdrmjaIO.getPolsum())) {
			covrmjaIO.setPlanSuffix(ZERO);
		}
		else {
			covrmjaIO.setPlanSuffix(uswdIO.getPlanSuffix());
		}
		covrmjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		initUnitDiscount6000();
		/* COMPUTE UTRN-CONTRACT-AMOUNT =  (WSAA-TIUV-VALUE /   <D9604> */
		/*                                  WSAA-INIT-UNIT-DISC-<D9604> */
		/*                                  *  -1.              <D9604> */
		setPrecision(utrnIO.getContractAmount(), 2);
		utrnIO.setContractAmount(mult(wsaaTiuvValue, -1));
		compute(wsaaAdjustmentDiff, 2).set(add(utrnIO.getContractAmount(), wsaaTiuvValue));
		if (isNE(wsaaAdjustmentDiff, ZERO)) {
			postAdjustment3650();
		}
		/* MULTIPLY WSAA-TIUV-VALUE    BY -1                            */
		/*                             GIVING UTRN-CONTRACT-AMOUNT.     */
		/*    DIVIDE UTRN-CONTRACT-AMOUNT BY 100 GIVING                    */
		/*                                UTRN-CONTRACT-AMOUNT.            */
		utrnIO.setUnitSubAccount("INIT");
		utrnIO.setUnitType("I");
		/* MOVE 1                      TO UTRN-SURRENDER-PERCENT.*/
		utrnIO.setSurrenderPercent(ZERO);
		wsaaTiuvRec.set("Y");
		/*    GO 3660-FORMAT.                                              */
		/* GO 3650-COVR.                                          <004> */
		goTo(GotoLabel.format3660);
	}

protected void tauv3640()
	{
		/*    Processes as per TIUV above*/
		if (isEQ(wsaaTauvRec, "Y")) {
			goTo(GotoLabel.nextFund3685);
		}
		if (isNE(wsaaTauv, ZERO)) {
			calculateTauvValue3800();
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(mult(wsaaTauvValue, -1));
			/*       DIVIDE UTRN-CONTRACT-AMOUNT BY 100                        */
			/*                                GIVING UTRN-CONTRACT-AMOUNT      */
			utrnIO.setUnitSubAccount("ACUM");
			utrnIO.setUnitType("A");
			utrnIO.setSurrenderPercent(ZERO);
			wsaaTauvRec.set("Y");
			wsaaInitUnitDiscFact.set(1);
		}
		else {
			goTo(GotoLabel.nextFund3685);
		}
	}

protected void covr3650()
	{
		covrmjaIO.setDataKey(SPACES);
		covrmjaIO.setChdrcoy(uswdIO.getChdrcoy());
		covrmjaIO.setChdrnum(uswdIO.getChdrnum());
		covrmjaIO.setLife(uswdIO.getLife());
		covrmjaIO.setCoverage(uswdIO.getCoverage());
		covrmjaIO.setRider(uswdIO.getRider());
		if (isLTE(uswdIO.getPlanSuffix(), chdrmjaIO.getPolsum())) {
			covrmjaIO.setPlanSuffix(ZERO);
		}
		else {
			covrmjaIO.setPlanSuffix(uswdIO.getPlanSuffix());
		}
		covrmjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
	}

	/**
	* <pre>
	**** PERFORM 6000-INIT-UNIT-DISCOUNT.                       <022>
	* </pre>
	*/
protected void format3660()
	{
		utrnIO.setChdrcoy(uswdIO.getChdrcoy());
		utrnIO.setChdrnum(uswdIO.getChdrnum());
		utrnIO.setLife(uswdIO.getLife());
		utrnIO.setCoverage(uswdIO.getCoverage());
		utrnIO.setRider(uswdIO.getRider());
		utrnIO.setPlanSuffix(uswdIO.getPlanSuffix());
		utrnIO.setTranno(uswdIO.getTranno());
		utrnIO.setSwitchIndicator("Y");
		/* MOVE PARM-COMPANY           TO UTRN-BATCCOY.                 */
		/* MOVE PARM-BRANCH            TO UTRN-BATCBRN.                 */
		/* MOVE PARM-ACCTYEAR          TO UTRN-BATCACTYR.               */
		/* MOVE PARM-ACCTMONTH         TO UTRN-BATCACTMN.               */
		/* MOVE 'T676'                 TO UTRN-BATCTRCDE.          <022>*/
		/* MOVE WSAA-UTRN-BATCTRCDE    TO UTRN-BATCTRCDE.          <022>*/
		/* MOVE SPACE                  TO UTRN-BATCBATCH           <026>*/
		/*                                UTRN-NOW-DEFER-IND            */
		/* MOVE WSAA-BATCBATCH         TO UTRN-BATCBATCH.          <026>*/
		utrnIO.setBatccoy(udtrigrec.company);
		utrnIO.setBatcbrn(udtrigrec.branch);
		utrnIO.setBatcactyr(udtrigrec.actyear);
		utrnIO.setBatcactmn(udtrigrec.actmonth);
		utrnIO.setBatctrcde(wsaaUtrnBatctrcde);
		utrnIO.setBatcbatch(wsaaBatcbatch);
		utrnIO.setNowDeferInd(SPACES);
		utrnIO.setCrtable(SPACES);
		utrnIO.setFeedbackInd(SPACES);
		utrnIO.setTermid(SPACES);
		utrnIO.setSacscode(SPACES);
		utrnIO.setSacstyp(SPACES);
		utrnIO.setGenlcde(SPACES);
		utrnIO.setTriggerModule(SPACES);
		utrnIO.setTriggerKey(SPACES);
		utrnIO.setCanInitUnitInd(SPACES);
		utrnIO.setInciperd(1, ZERO);
		utrnIO.setInciperd(2, ZERO);
		utrnIO.setInciprm(1, ZERO);
		utrnIO.setInciprm(2, ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setCrComDate(ZERO);
		/* ADD 1, WSAA-PROC-SEQ-NO GIVING UTRN-PROC-SEQ-NO.             */
		utrnIO.setProcSeqNo(wsaaProcSeqNo);
		/* MOVE 1                      TO UTRN-SVP.             <D9604>*/
		utrnIO.setSvp(1);
		/* MOVE WSAA-INIT-UNIT-DISC-FACT                        <A06673>*/
		/*                             TO UTRN-SVP.             <A06673>*/
		utrnIO.setDiscountFactor(wsaaInitUnitDiscFact);
		/* MOVE PARM-EFFDATE           TO UTRN-TRANSACTION-DATE.        */
		utrnIO.setTransactionDate(udtrigrec.effdate);
		/* MOVE ZERO                   TO UTRN-TRANSACTION-TIME.        */
		callProgram(Gettim.class, wsaaTime);
		utrnIO.setTransactionTime(wsaaTime);
		if (isEQ(t6647rec.efdcode, "DD")) {
			utrnIO.setMoniesDate(uswdIO.getEffdate());
		}
		else {
			if (isEQ(t6647rec.efdcode, "RD")) {
				utrnIO.setMoniesDate(udtrigrec.effdate);
			}
			else {
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				utrnIO.setMoniesDate(datcon1rec.intDate);
			}
		}
		/* MOVE WSAA-CURR(WSAA-SUB2)    TO UTRN-CNTCURR.                */
		utrnIO.setCntcurr(chdrmjaIO.getCntcurr());
		utrnIO.setFundCurrency(uswdIO.getTgfndcur(wsaaSub));
		utrnIO.setContractType(chdrmjaIO.getCnttype());
		utrnIO.setUnitVirtualFund(uswdIO.getTgtfund(wsaaSub));
		utrnIO.setCrtable(covrmjaIO.getCrtable());
		utrnIO.setCrComDate(covrmjaIO.getCrrcd());
		utrnIO.setNowDeferInd(t5544rec.nowDeferInd);
		/*    MOVE T6647-DEALIN           TO UTRN-NOW-DEFER-IND.           */
		/*    MOVE T6647-PROC-SEQ-NO      TO UTRN-PROC-SEQ-NO.             */
		/* MOVE T5645-SACSCODE-03      TO UTRN-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-03      TO UTRN-SACSTYP.                 */
		/* MOVE T5645-GLMAP-03         TO UTRN-GENLCDE.                 */
		/* Check for Component level accounting & select correct postings  */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setGenlcde(t5645rec.glmap08);
			utrnIO.setSacscode(t5645rec.sacscode08);
			utrnIO.setSacstyp(t5645rec.sacstype08);
		}
		else {
			utrnIO.setGenlcde(t5645rec.glmap03);
			utrnIO.setSacscode(t5645rec.sacscode03);
			utrnIO.setSacstyp(t5645rec.sacstype03);
		}
		if (isNE(tr384rec.letterType, SPACES)) {
			utrnIO.setTriggerModule("ZFSWLETT");
		}
	}

protected void writeUtrn3670()
	{
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(formatsInner.utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
		/*3680-UDEL.                                                       */
		/* IF  UDTG-FUNCTION           = 'B5102'                <D9604> */
		/*     GO 3620-TIUV.                                    <D9604> */
		/* MOVE UTRN-DATA-KEY          TO UDELUSW-DATA-KEY.             */
		/* MOVE UTRN-TERMID            TO UDELUSW-TERMID.               */
		/* MOVE UTRN-TRANSACTION-DATE  TO UDELUSW-TRANSACTION-DATE.     */
		/* MOVE UTRN-TRANSACTION-TIME  TO UDELUSW-TRANSACTION-TIME.     */
		/* MOVE UTRN-USER              TO UDELUSW-USER.                 */
		/* MOVE UTRN-BATCCOY           TO UDELUSW-BATCCOY.              */
		/* MOVE UTRN-BATCBRN           TO UDELUSW-BATCBRN.              */
		/* MOVE UTRN-BATCACTYR         TO UDELUSW-BATCACTYR.            */
		/* MOVE UTRN-BATCACTMN         TO UDELUSW-BATCACTMN.            */
		/* MOVE UTRN-BATCTRCDE         TO UDELUSW-BATCTRCDE.            */
		/* MOVE UTRN-BATCBATCH         TO UDELUSW-BATCBATCH.            */
		/* MOVE UTRN-FUND-RATE         TO UDELUSW-FUND-RATE.            */
		/* MOVE UTRN-UNIT-SUB-ACCOUNT  TO UDELUSW-UNIT-SUB-ACCOUNT.     */
		/* MOVE UTRN-STRPDATE          TO UDELUSW-STRPDATE.             */
		/* MOVE UTRN-NOW-DEFER-IND     TO UDELUSW-NOW-DEFER-IND.        */
		/* MOVE UTRN-NOF-UNITS         TO UDELUSW-NOF-UNITS.            */
		/* MOVE UTRN-NOF-DUNITS        TO UDELUSW-NOF-DUNITS.           */
		/* MOVE UTRN-MONIES-DATE       TO UDELUSW-MONIES-DATE.          */
		/* MOVE UTRN-PRICE-DATE-USED   TO UDELUSW-PRICE-DATE-USED.      */
		/* MOVE UTRN-PRICE-USED        TO UDELUSW-PRICE-USED.           */
		/* MOVE UTRN-UNIT-BARE-PRICE   TO UDELUSW-UNIT-BARE-PRICE.      */
		/* MOVE UTRN-CRTABLE           TO UDELUSW-CRTABLE.              */
		/* MOVE UTRN-CNTCURR           TO UDELUSW-CNTCURR.              */
		/* MOVE UTRN-INCI-NUM          TO UDELUSW-INCI-NUM.             */
		/* MOVE UTRN-CONTRACT-AMOUNT   TO UDELUSW-CONTRACT-AMOUNT.      */
		/* MOVE UTRN-FUND-CURRENCY     TO UDELUSW-FUND-CURRENCY.        */
		/* MOVE UTRN-FUND-AMOUNT       TO UDELUSW-FUND-AMOUNT.          */
		/* MOVE UTRN-CONTRACT-TYPE     TO UDELUSW-CONTRACT-TYPE.        */
		/* MOVE UTRN-TRIGGER-MODULE    TO UDELUSW-TRIGGER-MODULE.       */
		/* MOVE UTRN-TRIGGER-KEY       TO UDELUSW-TRIGGER-KEY.          */
		/* MOVE UTRN-SWITCH-INDICATOR  TO UDELUSW-SWITCH-INDICATOR.     */
		/* MOVE ZERO                   TO UDELUSW-INCI-PERD.            */
		/* MOVE SPACE                  TO UDELUSW-ALL-IND.              */
		/*    ADD 1, WSAA-PROC-SEQ-NO GIVING UDELUSW-PROC-SEQ-NO.          */
		/* MOVE UTRN-PROC-SEQ-NO       TO UDELUSW-PROC-SEQ-NO.  <D9604> */
		/* MOVE WRITR                  TO UDELUSW-FUNCTION.             */
		/* MOVE UDELUSWREC             TO UDELUSW-FORMAT.               */
		/* CALL 'UDELUSWIO'            USING UDELUSW-PARAMS.            */
		/* IF UDELUSW-STATUZ           NOT = O-K                        */
		/*    MOVE UDELUSW-PARAMS      TO SYSR-PARAMS                   */
		/*    MOVE 'BOMB'              TO WSAA-BOMB                     */
		/*    MOVE UDELUSW-FORMAT      TO WSAA-FORMAT                   */
		/*    PERFORM 9000-FATAL-ERROR.                                 */
		goTo(GotoLabel.tiuv3620);
	}

protected void checkSourceCurr3683()
	{
		/*    Each source fund currency has been stored in an occurrence*/
		/*    in WSAA-TABLE. This is necessary as, if there are different*/
		/*    currency source funds, a target fund UTRN will have to be*/
		/*    created for each different source fund currency.*/
		wsaaSub2.add(1);
		if (isGT(wsaaSub2, 20)
		|| isEQ(wsaaCurr[wsaaSub2.toInt()], SPACES)) {
			wsaaSub2.set(1);
			goTo(GotoLabel.nextFund3685);
		}
		if (isEQ(wsaaCurr[wsaaSub2.toInt()], utrnIO.getCntcurr())) {
			checkSourceCurr3683();
			return ;
		}
		goTo(GotoLabel.format3660);
	}

protected void hitr3680()
	{
		if (isEQ(wsaaTauv, 0)) {
			goTo(GotoLabel.nextFund3685);
		}
		compute(wsaaTauvValue, 2).set(sub(wsaaTauv, wsaaChargeTauv));
		compute(wsaaTauvValue, 3).setRounded(div(mult(wsaaTauvValue, uswdIO.getTgtprcnt(wsaaSub)), 100));
		wsaaTauvValueRemain.add(wsaaTauvValue);
		compute(wsaaRemainder, 2).set(sub(sub(wsaaTauv, wsaaTauvValueRemain), wsaaChargeTauv));
		if (isLT(wsaaRemainder, 0.11)
		&& isGT(wsaaRemainder, -0.11)) {
			wsaaTauvValue.add(wsaaRemainder);
		}
		covrmjaIO.setDataKey(SPACES);
		covrmjaIO.setChdrcoy(uswdIO.getChdrcoy());
		covrmjaIO.setChdrnum(uswdIO.getChdrnum());
		covrmjaIO.setLife(uswdIO.getLife());
		covrmjaIO.setCoverage(uswdIO.getCoverage());
		covrmjaIO.setRider(uswdIO.getRider());
		if (isLTE(uswdIO.getPlanSuffix(), chdrmjaIO.getPolsum())) {
			covrmjaIO.setPlanSuffix(ZERO);
		}
		else {
			covrmjaIO.setPlanSuffix(uswdIO.getPlanSuffix());
		}
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		setPrecision(hitrIO.getContractAmount(), 2);
		hitrIO.setContractAmount(mult(wsaaTauvValue, -1));
		hitrIO.setZrectyp("P");
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setChdrcoy(uswdIO.getChdrcoy());
		hitrIO.setChdrnum(uswdIO.getChdrnum());
		hitrIO.setLife(uswdIO.getLife());
		hitrIO.setCoverage(uswdIO.getCoverage());
		hitrIO.setRider(uswdIO.getRider());
		hitrIO.setPlanSuffix(uswdIO.getPlanSuffix());
		hitrIO.setTranno(uswdIO.getTranno());
		hitrIO.setSwitchIndicator("Y");
		hitrIO.setBatccoy(udtrigrec.company);
		hitrIO.setBatcbrn(udtrigrec.branch);
		hitrIO.setBatcactyr(udtrigrec.actyear);
		hitrIO.setBatcactmn(udtrigrec.actmonth);
		hitrIO.setBatctrcde(wsaaUtrnBatctrcde);
		hitrIO.setBatcbatch(wsaaBatcbatch);
		hitrIO.setCrtable(SPACES);
		hitrIO.setFeedbackInd(SPACES);
		hitrIO.setSacscode(SPACES);
		hitrIO.setSacstyp(SPACES);
		hitrIO.setGenlcde(SPACES);
		hitrIO.setTriggerModule(SPACES);
		hitrIO.setTriggerKey(SPACES);
		hitrIO.setInciperd(1, ZERO);
		hitrIO.setInciperd(2, ZERO);
		hitrIO.setInciprm(1, ZERO);
		hitrIO.setInciprm(2, ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setZintrate(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setProcSeqNo(wsaaProcSeqNo);
		hitrIO.setSvp(1);
		hitrIO.setEffdate(uswdIO.getEffdate());
		hitrIO.setCntcurr(chdrmjaIO.getCntcurr());
		hitrIO.setFundCurrency(uswdIO.getTgfndcur(wsaaSub));
		hitrIO.setCnttyp(chdrmjaIO.getCnttype());
		hitrIO.setZintbfnd(uswdIO.getTgtfund(wsaaSub));
		hitrIO.setCrtable(covrmjaIO.getCrtable());
		hitrIO.setZrectyp("P");
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			hitrIO.setGenlcde(t5645rec.glmap08);
			hitrIO.setSacscode(t5645rec.sacscode08);
			hitrIO.setSacstyp(t5645rec.sacstype08);
		}
		else {
			hitrIO.setGenlcde(t5645rec.glmap03);
			hitrIO.setSacscode(t5645rec.sacscode03);
			hitrIO.setSacstyp(t5645rec.sacstype03);
		}
		if (isNE(tr384rec.letterType, SPACES)) {
			hitrIO.setTriggerModule("ZFSWLETT");
		}
		hitrIO.setFunction(varcom.writr);
		hitrIO.setFormat(formatsInner.hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			fatalError9000();
		}
		a800CheckHitd();
	}

protected void nextFund3685()
	{
		/*    Increments subscript by 1 if all processing is complete*/
		/*    for that occurrence of TARGET FUND*/
		wsaaTiuvRec.set(SPACES);
		wsaaTauvRec.set(SPACES);
		wsaaSub.add(1);
		/*IF WSAA-SUB > 20 OR                                          */
		if (isGT(wsaaSub, 10)
		|| isEQ(uswdIO.getTgtfund(wsaaSub), SPACES)) {
			return ;
		}
		goTo(GotoLabel.tiuv3620);
	}

protected void postAdjustment3650()
	{
		postAdjustment3651();
		acmv13651();
	}

protected void postAdjustment3651()
	{
		lifacmvrec.batckey.set(wsaaBatckey);
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		/* MOVE PARM-COMPANY           TO WSKY-ITEM-ITEMCOY.       <026>*/
		wsaaItemkey.itemItemcoy.set(udtrigrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(wsaaUtrnBatctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		/* MOVE PARM-LANGUAGE          TO GTDS-LANGUAGE.           <026>*/
		getdescrec.language.set(udtrigrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			wsaaDescription.set(SPACES);
		}
		else {
			wsaaDescription.set(getdescrec.longdesc);
		}
	}

protected void acmv13651()
	{
		lifacmvrec.function.set("PSTW");
		/* MOVE WSAA-CHDRNUM           TO LIFA-RDOCNUM.            <026>*/
		lifacmvrec.rdocnum.set(udtrigrec.chdrnum);
		/* MOVE WSAA-CHDRNUM           TO LIFA-RLDGACCT.           <026>*/
		/* MOVE WSAA-TRANNO            TO LIFA-TRANNO.             <026>*/
		lifacmvrec.tranno.set(udtrigrec.tranno);
		lifacmvrec.jrnseq.set(ZERO);
		/* MOVE PARM-COMPANY           TO LIFA-RLDGCOY,            <026>*/
		lifacmvrec.rldgcoy.set(udtrigrec.company);
		lifacmvrec.genlcoy.set(udtrigrec.company);
		/* MOVE WSAA-TRANNO            TO LIFA-TRANREF.            <026>*/
		lifacmvrec.tranref.set(udtrigrec.tranno);
		/* MOVE T5645-GLMAP-04         TO LIFA-GLCODE.             <026>*/
		/* MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.           <026>*/
		/* MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.            <026>*/
		/* MOVE T5645-SIGN-04          TO LIFA-GLSIGN.             <026>*/
		/* decide whether we are doing component level accounting or not   */
		/* and set the RLDGACCT field accordingly as well as choosing the  */
		/* correct entry from T5645                                        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			/*    MOVE WSAA-CHDRNUM        TO WSAA-RLDG-CHDRNUM        <027>*/
			/*    MOVE WSAA-LIFE           TO WSAA-RLDG-LIFE           <027>*/
			/*    MOVE WSAA-COVERAGE       TO WSAA-RLDG-COVERAGE       <027>*/
			/*    MOVE WSAA-RIDER          TO WSAA-RLDG-RIDER          <027>*/
			/*    MOVE WSAA-PLAN-SUFFIX    TO WSAA-PLAN                <027>*/
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlan.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(covrmjaIO.getCrtable());
			lifacmvrec.substituteCode[2].set(uswdIO.getTgtfund(wsaaSub));
		}
		else {
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.rldgacct.set(SPACES);
			/*    MOVE WSAA-CHDRNUM        TO LIFA-RLDGACCT            <027>*/
			lifacmvrec.rldgacct.set(udtrigrec.chdrnum);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.substituteCode[2].set(uswdIO.getTgtfund(wsaaSub));
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(uswdIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(0);
		lifacmvrec.origamt.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.rcamt.set(0);
		/* ACCEPT LIFA-TRANSACTION-DATE FROM DATE.                 <029>*/
		/* ACCEPT LIFA-TRANSACTION-TIME FROM TIME.                 <029>*/
		/* MOVE PARM-USER              TO LIFA-USER.               <029>*/
		lifacmvrec.user.set(ZERO);
		/* MOVE PARM-TRANID            TO VRCM-TRANID.             <029>*/
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.             <029>*/
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaAdjustmentDiff);
		lifacmvrec.trandesc.set(wsaaDescription);
		lifacmvrec.substituteCode[2].set(uswdIO.getTgtfund(wsaaSub));
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
		/* MOVE T5645-GLMAP-05         TO LIFA-GLCODE.             <026>*/
		/* MOVE T5645-SACSCODE-05      TO LIFA-SACSCODE.           <026>*/
		/* MOVE T5645-SACSTYPE-05      TO LIFA-SACSTYP.            <026>*/
		/* MOVE T5645-SIGN-05          TO LIFA-GLSIGN.             <026>*/
		/* decide whether we are doing component level accounting or not   */
		/* and set the RLDGACCT field accordingly as well as choosing the  */
		/* correct entry from T5645                                        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			/*    MOVE WSAA-CHDRNUM        TO WSAA-RLDG-CHDRNUM        <027>*/
			/*    MOVE WSAA-LIFE           TO WSAA-RLDG-LIFE           <027>*/
			/*    MOVE WSAA-COVERAGE       TO WSAA-RLDG-COVERAGE       <027>*/
			/*    MOVE WSAA-RIDER          TO WSAA-RLDG-RIDER          <027>*/
			/*    MOVE WSAA-PLAN-SUFFIX    TO WSAA-PLAN                <027>*/
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlan.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(covrmjaIO.getCrtable());
		}
		else {
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.rldgacct.set(SPACES);
			/*    MOVE WSAA-CHDRNUM        TO LIFA-RLDGACCT            <027>*/
			lifacmvrec.rldgacct.set(udtrigrec.chdrnum);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void calculateTiuvValue3700()
	{
		/*CHECK-CHARGE*/
		/*    Where a charge has been made, subtract the charge from*/
		/*    the total TIUV value*/
		/*    IF  WSAA-CHARGE-PERCENT     > ZERO                           */
		/*        SUBTRACT WSAA-CALC      FROM WSAA-TIUV.                  */
		/*    COMPUTE WSAA-TIUV ROUNDED =                                  */
		/*       (WSAA-TIUV - ((WSAA-CALC * USWD-TGTPRCNT (WSAA-SUB))      */
		/*        / 100)).                                                 */
		/*    COMPUTE WSAA-TIUV ROUNDED =                                  */
		/*       (WSAA-TIUV - (WSAA-CALC * (WSAA-TIUV /                    */
		/*                      WSAA-CONTRACT-TOTAL))).                    */
		compute(wsaaTiuvValue, 2).set(sub(wsaaTiuv, wsaaChargeTiuv));
		/*CALC-VALUE*/
		/*    MULTIPLY WSAA-TIUV          BY USWD-TGTPRCNT(WSAA-SUB)       */
		/*                                GIVING WSAA-TIUV-VALUE.          */
		/*    Calculate the total fund split using the percentage          */
		/*    allocated to each fund                                       */
		compute(wsaaTiuvValue, 3).setRounded((div((mult(wsaaTiuvValue, uswdIO.getTgtprcnt(wsaaSub))), 100)));
		zrdecplrec.amountIn.set(wsaaTiuvValue);
		a000CallRounding();
		wsaaTiuvValue.set(zrdecplrec.amountOut);
		/*    When doing a fund split, if during the split there is        */
		/*    a remainder of a currency then add this into the last        */
		/*    split value to produce a balanced reconciliation.            */
		wsaaTiuvValueRemain.add(wsaaTiuvValue);
		compute(wsaaRemainder, 2).set(sub(sub(wsaaTiuv, wsaaTiuvValueRemain), wsaaChargeTiuv));
		if (isLT(wsaaRemainder, 0.11)
		&& isGT(wsaaRemainder, -0.11)) {
			wsaaTiuvValue.add(wsaaRemainder);
		}
		/*EXIT*/
	}

protected void calculateTauvValue3800()
	{
		/*CHECK-CHARGE*/
		/*    Where a charge has been made, subtract charge from the*/
		/*    total TAUV value*/
		/*    IF  WSAA-CHARGE-PERCENT    > ZERO                            */
		/*        SUBTRACT WSAA-CALC     FROM WSAA-TAUV.                   */
		/*    COMPUTE WSAA-TAUV ROUNDED =                                  */
		/*       (WSAA-TAUV - ((WSAA-CALC * USWD-TGTPRCNT (WSAA-SUB))      */
		/*        / 100)).                                                 */
		/*    COMPUTE WSAA-TAUV ROUNDED =                                  */
		/*       (WSAA-TAUV - (WSAA-CALC * (WSAA-TAUV /                    */
		/*                      WSAA-CONTRACT-TOTAL))).                    */
		compute(wsaaTauvValue, 2).set(sub(wsaaTauv, wsaaChargeTauv));
		/*CALC-VALUE*/
		/*    MULTIPLY WSAA-TAUV         BY USWD-TGTPRCNT(WSAA-SUB)        */
		/*                               GIVING WSAA-TAUV-VALUE.           */
		/*    Calculate the total fund split using the percentage          */
		/*    allocated to each fund                                       */
		compute(wsaaTauvValue, 3).setRounded((div((mult(wsaaTauvValue, uswdIO.getTgtprcnt(wsaaSub))), 100)));
		zrdecplrec.amountIn.set(wsaaTauvValue);
		a000CallRounding();
		wsaaTauvValue.set(zrdecplrec.amountOut);
		/*    When doing a fund split, if during the split there is        */
		/*    a remainder of a currency then add this into the last        */
		/*    split value to produce a balanced reconciliation.            */
		wsaaTauvValueRemain.add(wsaaTauvValue);
		compute(wsaaRemainder, 2).set(sub(sub(wsaaTauv, wsaaTauvValueRemain), wsaaChargeTauv));
		if (isLT(wsaaRemainder, 0.11)
		&& isGT(wsaaRemainder, -0.11)) {
			wsaaTauvValue.add(wsaaRemainder);
		}
		/*EXIT*/
	}

protected void charge3900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					t55443910();
					chargePercent3920();
				case t56453930:
					t56453930();
					acmv13940();
					acmv23950();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Writes ACMV records detailing the charge made
	* </pre>
	*/
protected void t55443910()
	{
		wsaaCalc.set(ZERO);
		if (isGT(t5544rec.ffamt, ZERO)) {
			wsaaCalc.set(t5544rec.ffamt);
			//MIBT-359
			zrdecplrec.amountIn.set(wsaaCalc);
			a000CallRounding(); 
			wsaaCalc.set(zrdecplrec.amountOut); 
			wsaaSwitchFee.set(wsaaCalc); 
			goTo(GotoLabel.t56453930);
		}
		wsaaCalc.set(t5544rec.feepc);
		/*    MULTIPLY WSAA-CALC          BY WSAA-CONTRACT-TOTAL           */
		/*                                GIVING WSAA-CALC.                */
		/*    COMPUTE WSAA-CALC ROUNDED =                                  */
		/*                  ((WSAA-CONTRACT-TOTAL * WSAA-CALC) / 100)      */
		compute(wsaaCalc, 3).setRounded((mult((div((mult(wsaaContractTotal, wsaaCalc)), 100)), -1)));
		if (isGT(wsaaCalc, t5544rec.feemax)) {
			wsaaCalc.set(t5544rec.feemax);
		}
		if (isLT(wsaaCalc, t5544rec.feemin)) {
			wsaaCalc.set(t5544rec.feemin);
		}
	}

protected void chargePercent3920()
	{
		/*    Calculates charge as a percentage of transaction value*/
		zrdecplrec.amountIn.set(wsaaCalc);
		a000CallRounding();
		wsaaCalc.set(zrdecplrec.amountOut);
		wsaaSwitchFee.set(wsaaCalc);
	}

protected void t56453930()
	{
		/*   IF THE SWITCH FEE IS GREATER THAN THE TOTAL FUNDS             */
		/*   AVAIABLE FOR SWITCH                                           */
		/*      1) MAKE SWITHCH-FEE = FUNDS AVAIABLE FOR SWITCH            */
		/*      2 ) SET ERROR INDICTOR TO BY PASS CREATING UTRN            */
		/*          FOR TARGET FUNDS ALSO PASS BACK TO CALLING PROG        */
		/*          TO REPORT ERROR.                                       */
		/*    MOVE WSAA-CONTRACT-TOTAL      TO WSAA-POSITIVE-AMOUNT.       */
		/*    IF WSAA-POSITIVE-AMOUNT  < 0                                 */
		/*      COMPUTE WSAA-POSITIVE-AMOUNT = WSAA-POSITIVE-AMOUNT * -1 . */
		/*    IF  WSAA-CALC > WSAA-POSITIVE-AMOUNT                         */
		/*        MOVE 'Y'                 TO WSAA-SW-ERR                  */
		/*        MOVE WSAA-CALC           TO WSAA-SW-EXPECTED-FEE         */
		/*        MOVE WSAA-CONTRACT-TOTAL TO WSAA-SW-ACTUAL-FEE           */
		/*        MOVE WSAA-CONTRACT-TOTAL TO WSAA-CALC.                   */
		compute(wsaaCalc, 2).set((mult(wsaaCalc, -1)));
		/*3930-T5645.                                                      */
		/*    Create ACMV record                                           */
		/*    COMPUTE WSAA-CALC = (WSAA-CALC * -1).                        */
		/*    MOVE SPACES                 TO ITEM-DATA-AREA.               */
		/*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/*    MOVE BEGN                   TO ITEM-FUNCTION.                */
		/*    MOVE WSAA-CHDRCOY           TO ITEM-ITEMCOY.                 */
		/*    MOVE 'T5645'                TO ITEM-ITEMTABL.                */
		/*    MOVE 'FUNDSWCH'             TO ITEM-ITEMITEM.                */
		/*    CALL 'ITEMIO'               USING ITEM-PARAMS.               */
		/*    IF ITEM-STATUZ              NOT = O-K                        */
		/*       MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*       MOVE 'BOMB'              TO WSAA-BOMB                     */
		/*       PERFORM 9000-FATAL-ERROR.                                 */
		/*    MOVE ITEM-GENAREA           TO T5645-T5645-REC.              */
		batcdorrec.function.set("AUTO");
		/* MOVE PARM-TRANID            TO BATD-TRANID.             <016>*/
		/*   MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.         <LA4524>*/
		/*   MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.         <LA4524>*/
		/*   MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.       <LA4524>*/
		/*   MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.       <LA4524>*/
		lifacmvrec.batckey.set(udtrigrec.batchkey);
		lifacmvrec.batctrcde.set(utrnIO.getBatctrcde());
		/*   MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.       <LA4524>*/
		batcdorrec.batchkey.set(lifacmvrec.batckey);
		batcdorrec.prefix.set("BA");
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		/* MOVE PARM-COMPANY           TO WSKY-ITEM-ITEMCOY.       <026>*/
		wsaaItemkey.itemItemcoy.set(udtrigrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(lifacmvrec.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		/* MOVE PARM-LANGUAGE          TO GTDS-LANGUAGE.           <026>*/
		getdescrec.language.set(udtrigrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			wsaaDescription.set(SPACES);
		}
		else {
			wsaaDescription.set(getdescrec.longdesc);
		}
		covrmjaIO.setDataKey(SPACES);
		covrmjaIO.setChdrcoy(uswdIO.getChdrcoy());
		covrmjaIO.setChdrnum(uswdIO.getChdrnum());
		covrmjaIO.setLife(uswdIO.getLife());
		covrmjaIO.setCoverage(uswdIO.getCoverage());
		covrmjaIO.setRider(uswdIO.getRider());
		if (isLTE(uswdIO.getPlanSuffix(), chdrmjaIO.getPolsum())) {
			covrmjaIO.setPlanSuffix(ZERO);
		}
		else {
			covrmjaIO.setPlanSuffix(uswdIO.getPlanSuffix());
		}
		covrmjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
		}
		else {
			datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		}
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError9000();
		}
	}

protected void acmv13940()
	{
		lifacmvrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFA-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFA-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.               */
		/*  MOVE 'T676'                 TO LIFA-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFA-BATCBATCH.               */
		/* MOVE WSAA-CHDRNUM           TO LIFA-RDOCNUM.                 */
		lifacmvrec.rdocnum.set(udtrigrec.chdrnum);
		/* MOVE WSAA-CHDRNUM           TO LIFA-RLDGACCT.           <009>*/
		/* MOVE WSAA-TRANNO            TO LIFA-TRANNO.                  */
		lifacmvrec.tranno.set(udtrigrec.tranno);
		lifacmvrec.jrnseq.set(ZERO);
		/* MOVE PARM-COMPANY           TO LIFA-RLDGCOY,                 */
		lifacmvrec.rldgcoy.set(udtrigrec.company);
		lifacmvrec.genlcoy.set(udtrigrec.company);
		/* MOVE WSAA-TRANNO            TO LIFA-TRANREF.                 */
		lifacmvrec.tranref.set(udtrigrec.tranno);
		/* MOVE T5645-GLMAP-01         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SACSCODE-01      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-01      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-SIGN-01          TO LIFA-GLSIGN.                  */
		/* decide whether we are doing component level accounting or not   */
		/* and set the RLDGACCT field accordingly as well as choosing the  */
		/* correct entry from T5645                                        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			if (isLT(uswdIO.getEffdate(), datcon2rec.intDate2)) {
				lifacmvrec.glcode.set(t5645rec.glmap06);
				lifacmvrec.sacscode.set(t5645rec.sacscode06);
				lifacmvrec.sacstyp.set(t5645rec.sacstype06);
				lifacmvrec.glsign.set(t5645rec.sign06);
				lifacmvrec.contot.set(t5645rec.cnttot06);
			}
			else {
				lifacmvrec.glcode.set(t5645rec.glmap12);
				lifacmvrec.sacscode.set(t5645rec.sacscode12);
				lifacmvrec.sacstyp.set(t5645rec.sacstype12);
				lifacmvrec.glsign.set(t5645rec.sign12);
				lifacmvrec.contot.set(t5645rec.cnttot12);
			}
			/*    MOVE WSAA-CHDRNUM        TO WSAA-RLDG-CHDRNUM        <027>*/
			/*    MOVE WSAA-LIFE           TO WSAA-RLDG-LIFE           <027>*/
			/*    MOVE WSAA-COVERAGE       TO WSAA-RLDG-COVERAGE       <027>*/
			/*    MOVE WSAA-RIDER          TO WSAA-RLDG-RIDER          <027>*/
			/*    MOVE WSAA-PLAN-SUFFIX    TO WSAA-PLAN                <027>*/
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlan.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(utrnIO.getCrtable());
		}
		else {
			if (isLT(uswdIO.getEffdate(), datcon2rec.intDate2)) {
				lifacmvrec.glcode.set(t5645rec.glmap01);
				lifacmvrec.sacscode.set(t5645rec.sacscode01);
				lifacmvrec.sacstyp.set(t5645rec.sacstype01);
				lifacmvrec.glsign.set(t5645rec.sign01);
				lifacmvrec.contot.set(t5645rec.cnttot01);
			}
			else {
				lifacmvrec.glcode.set(t5645rec.glmap11);
				lifacmvrec.sacscode.set(t5645rec.sacscode11);
				lifacmvrec.sacstyp.set(t5645rec.sacstype11);
				lifacmvrec.glsign.set(t5645rec.sign11);
				lifacmvrec.contot.set(t5645rec.cnttot11);
			}
			lifacmvrec.rldgacct.set(SPACES);
			/*    MOVE WSAA-CHDRNUM        TO LIFA-RLDGACCT            <027>*/
			lifacmvrec.rldgacct.set(udtrigrec.chdrnum);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/*MOVE WSAA-CALC              TO LIFA-ACCTAMT.                 */
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(uswdIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(0);
		lifacmvrec.origamt.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.rcamt.set(0);
		/* ACCEPT LIFA-TRANSACTION-DATE FROM DATE.                 <029>*/
		/* ACCEPT LIFA-TRANSACTION-TIME FROM TIME.                 <029>*/
		/* MOVE PARM-USER              TO LIFA-USER.               <029>*/
		/* MOVE PARM-TRANID            TO VRCM-TRANID.             <029>*/
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.             <029>*/
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		/* MOVE WSAA-CALC              TO LIFA-ORIGAMT.           <006> */
		compute(lifacmvrec.origamt, 2).set(mult(wsaaCalc, -1));
		lifacmvrec.trandesc.set(wsaaDescription);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
		/* Calculate tax on the switch fee                                 */
		if (isNE(wsaaSwitchFee, 0)) {
			a900ProcessTax();
			compute(wsaaCalc, 2).set(add(wsaaCalc, mult(wsaaTotalTax, -1)));
		}
	}

protected void acmv23950()
	{
		lifacmvrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFA-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFA-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.               */
		/*  MOVE 'T676'                 TO LIFA-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFA-BATCBATCH.               */
		/* MOVE WSAA-CHDRNUM           TO LIFA-RDOCNUM.                 */
		lifacmvrec.rdocnum.set(udtrigrec.chdrnum);
		/* MOVE WSAA-CHDRNUM           TO LIFA-RLDGACCT.           <009>*/
		/* MOVE WSAA-TRANNO            TO LIFA-TRANNO.                  */
		lifacmvrec.tranno.set(udtrigrec.tranno);
		lifacmvrec.jrnseq.set(ZERO);
		/* MOVE PARM-COMPANY           TO LIFA-RLDGCOY,                 */
		lifacmvrec.rldgcoy.set(udtrigrec.company);
		lifacmvrec.genlcoy.set(udtrigrec.company);
		/* MOVE WSAA-TRANNO            TO LIFA-TRANREF.                 */
		lifacmvrec.tranref.set(udtrigrec.tranno);
		/* MOVE T5645-GLMAP-02         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-SIGN-02          TO LIFA-GLSIGN.                  */
		/* decide whether we are doing component level accounting or not   */
		/* and set the RLDGACCT field accordingly as well as choosing the  */
		/* correct entry from T5645                                        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			/*    MOVE WSAA-CHDRNUM        TO WSAA-RLDG-CHDRNUM        <027>*/
			/*    MOVE WSAA-LIFE           TO WSAA-RLDG-LIFE           <027>*/
			/*    MOVE WSAA-COVERAGE       TO WSAA-RLDG-COVERAGE       <027>*/
			/*    MOVE WSAA-RIDER          TO WSAA-RLDG-RIDER          <027>*/
			/*    MOVE WSAA-PLAN-SUFFIX    TO WSAA-PLAN                <027>*/
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlan.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(utrnIO.getCrtable());
		}
		else {
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.rldgacct.set(SPACES);
			/*    MOVE WSAA-CHDRNUM        TO LIFA-RLDGACCT            <027>*/
			lifacmvrec.rldgacct.set(udtrigrec.chdrnum);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/*MOVE WSAA-CALC              TO LIFA-ACCTAMT.                 */
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(uswdIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.rcamt.set(0);
		/* ACCEPT LIFA-TRANSACTION-DATE FROM DATE.                 <029>*/
		/* ACCEPT LIFA-TRANSACTION-TIME FROM TIME.                 <029>*/
		/* MOVE PARM-USER              TO LIFA-USER.               <029>*/
		/* MOVE PARM-TRANID            TO VRCM-TRANID.             <029>*/
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.             <029>*/
		/* MOVE WSAA-CALC              TO LIFA-ORIGAMT.           <006> */
		/*    COMPUTE LIFA-ORIGAMT        =  WSAA-CALC * -1.         <026> */
		compute(lifacmvrec.origamt, 2).set(mult(wsaaCalc, -1));
		lifacmvrec.trandesc.set(wsaaDescription);
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void calcChargeSplit4000()
	{
		/*START*/
		compute(wsaaChargeTauv, 3).setRounded((mult(wsaaCalc, (div(wsaaTauv, wsaaContractTotal)))));
		zrdecplrec.amountIn.set(wsaaChargeTauv);
		a000CallRounding();
		wsaaChargeTauv.set(zrdecplrec.amountOut);
		compute(wsaaChargeTiuv, 2).set(sub(wsaaCalc, wsaaChargeTauv));
		/*EXIT*/
	}

protected void loadSrcCurrTab5000()
	{
		sub5010();
	}

	/**
	* <pre>
	*    Loads each occurrence of source fund currency into
	*    working-storage table (irrespective of whether the same
	*    currency is being stored more than once)
	* </pre>
	*/
protected void sub5010()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 20)) {
			return ;
		}
		if (isEQ(uswdIO.getScfndcur(wsaaSub), SPACES)) {
			return ;
		}
		wsaaCurr[wsaaSub.toInt()].set(uswdIO.getScfndcur(wsaaSub));
		sub5010();
		return ;
	}

protected void initUnitDiscount6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para6005();
				case decideDiscountMethod6100:
					decideDiscountMethod6100();
					termToRunMethod6200();
				case fixedTermMethod6300:
					fixedTermMethod6300();
				case wholeOfLifeMethod6400:
					wholeOfLifeMethod6400();
				case exit6900:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6005()
	{
		/* Read T5540 for the general unit linked details.                 */
		readT5540200();
		wsaaInitUnitDiscFact.set(1);
		if (((isEQ(t5540rec.iuDiscBasis, SPACES))
		|| (isEQ(t5540rec.iuDiscFact, SPACES)))
		&& (isEQ(t5540rec.wholeIuDiscFact, SPACES))) {
			goTo(GotoLabel.exit6900);
		}
		/*READ-T5519*/
		/*    Use Discount Basis from T5540 (if present) to                */
		/*    read T5519 for initial unit discount details.                */
		if (isEQ(t5540rec.iuDiscBasis, SPACES)) {
			goTo(GotoLabel.decideDiscountMethod6100);
		}
		readT5519500();
	}

protected void decideDiscountMethod6100()
	{
		/*  There are 3 methods to obtain the discount factor.             */
		/*  The one we use is decided by the entries just read             */
		/*  in T5519 & T5540. (Only one method is allowed).                */
		/*  If whole-of-life discount factor (T5540-WHOLE-IU-DISC-FACT)    */
		/*  is blank (i.e. term discount basis is not blank) and the       */
		/*  fixed term (T5519-FIXTRM) is zero, calculate the               */
		/*  term-left-to-run and use this to read the appropriate          */
		/*  discount factor from T5539.                                    */
		if (isNE(t5540rec.wholeIuDiscFact, SPACES)) {
			goTo(GotoLabel.wholeOfLifeMethod6400);
		}
		if (isNE(t5519rec.fixdtrm, 0)) {
			goTo(GotoLabel.fixedTermMethod6300);
		}
	}

protected void termToRunMethod6200()
	{
		calculateTermToRun7000();
		if (isEQ(wsaaTermLeftToRun, 0)) {
			goTo(GotoLabel.exit6900);
		}
		/*  Read T5539 for initial unit discount factor.                   */
		readT5539300();
		/*    MOVE T5539-DFACT(WSAA-TERM-LEFT-TO-RUN) TO           <V73L03>*/
		/*         WSAA-INIT-UNIT-DISC-FACT.                       <V73L03>*/
		if (isLT(wsaaTermLeftToRun, 100)) {
			wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaTermLeftToRun.toInt()]);
		}
		else {
			compute(wsaaIndex, 0).set(sub(wsaaTermLeftToRun, 99));
			wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaIndex.toInt()]);
		}
		compute(wsaaInitUnitDiscFact, 6).setRounded(div(wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void fixedTermMethod6300()
	{
		/* Use this fixed term to read off T5539 to obtain the             */
		/* discount factor.                                                */
		readT5539300();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                   <V73L03>*/
		/*         WSAA-INIT-UNIT-DISC-FACT.                       <V73L03>*/
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaIndex, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaIndex.toInt()]);
		}
		compute(wsaaInitUnitDiscFact, 6).setRounded(div(wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void wholeOfLifeMethod6400()
	{
		/* Use the age at next birthday to read off T6646 to obtain        */
		/* the discount factor.                                            */
		if (isEQ(covrmjaIO.getAnbAtCcd(), 0)) {
			return ;
		}
		readT6646400();
		/*    MOVE T6646-DFACT(COVRMJA-ANB-AT-CCD) TO              <V73L03>*/
		/*         WSAA-INIT-UNIT-DISC-FACT.                       <V73L03>*/
		if (isLT(covrmjaIO.getAnbAtCcd(), 100)) {
			wsaaInitUnitDiscFact.set(t6646rec.dfact[covrmjaIO.getAnbAtCcd().toInt()]);
		}
		else {
			compute(wsaaIndex, 0).set(sub(covrmjaIO.getAnbAtCcd(), 99));
			wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaIndex.toInt()]);
		}
		compute(wsaaInitUnitDiscFact, 6).setRounded(div(wsaaInitUnitDiscFact, 100000));
		return ;
	}

protected void calculateTermToRun7000()
	{
		para7005();
	}

protected void para7005()
	{
		/*    Discount factor is based on whole years remaining.           */
		/* MOVE PARM-EFFDATE            TO WSAA-EFFDATE-9.         <022>*/
		/* MOVE UDTG-EFFDATE            TO WSAA-EFFDATE-9.      <D9604> */
		/* MOVE COVRMJA-RISK-CESS-DATE  TO WSAA-CCDATE-9.       <D9604> */
		/* IF  WSAA-EFF-YR              NOT < WSAA-CC-YR        <D9604> */
		/*     MOVE 0                   TO WSAA-TERM-LEFT-TO-RUN<D9604> */
		/*     GO TO 7900-EXIT.                                 <D9604> */
		/* SUBTRACT WSAA-EFF-YR         FROM WSAA-CC-YR         <D9604> */
		/* GIVING WSAA-TERM-LEFT-TO-RUN.                        <D9604> */
		/* IF  WSAA-EFF-DDMM            > WSAA-CC-DDMM          <D9604> */
		/*     SUBTRACT 1               FROM WSAA-TERM-LEFT-TO-R<D9604> */
		datcon3rec.intDate1.set(udtrigrec.effdate);
		datcon3rec.intDate2.set(covrmjaIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		wsaaTermLeft.set(datcon3rec.freqFactor);
		/* Part years are always rounded up.                    <D9604>*/
		if (isNE(wsaaTermLeftRemain, 0)) {
			wsaaTermLeft.add(1);
		}
		wsaaTermLeftToRun.set(wsaaTermLeftInteger);
	}

protected void checkForOtherSwitchs8000()
	{
		checkForOtherSwitchs8010();
		nextUswd8020();
	}

protected void checkForOtherSwitchs8010()
	{
		uswdIO.setDataKey(SPACES);
		uswdIO.setChdrcoy(udtrigrec.chdrcoy);
		uswdIO.setChdrnum(udtrigrec.chdrnum);
		uswdIO.setPlanSuffix(ZERO);
		uswdIO.setLife(SPACES);
		uswdIO.setCoverage(SPACES);
		uswdIO.setRider(SPACES);
		uswdIO.setTranno(ZERO);
		uswdIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		uswdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		uswdIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

protected void nextUswd8020()
	{
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		|| isNE(udtrigrec.chdrcoy, uswdIO.getChdrcoy())
		|| isNE(udtrigrec.chdrnum, uswdIO.getChdrnum())) {
			uswdIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(uswdIO.getTranno(), udtrigrec.tranno)) {
			uswdIO.setStatuz(varcom.oK);
			return ;
		}
		uswdIO.setFunction(varcom.nextr);
		nextUswd8020();
		return ;
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(udtrigrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A090-EXIT*/
	}

protected void a100ReadT6659()
	{
		a100Read();
		a1010CheckT6659Details();
	}

protected void a100Read()
	{
		itdmIO.setItemcoy(udtrigrec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(udtrigrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(t6647rec.unitStatMethod, itdmIO.getItemitem())
		|| isNE(udtrigrec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6659)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g029);
			fatalError9000();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void a1010CheckT6659Details()
	{
		/* Check the details and call the generic subroutine from T6659.   */
		if (isEQ(t6659rec.subprog, SPACES)
		|| isNE(t6659rec.annOrPayInd, "P")
		|| isNE(t6659rec.osUtrnInd, "Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(udtrigrec.chdrcoy);
		annprocrec.chdrnum.set(udtrigrec.chdrnum);
		annprocrec.effdate.set(udtrigrec.effdate);
		annprocrec.batchkey.set(udtrigrec.batchkey);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError9000();
		}
	}

protected void a200ReadT5515()
	{
		a210T5515();
	}

protected void a210T5515()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(wsaaT5515Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.endp)
		&& isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), udtrigrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), wsaaT5515Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void a300ProcessHitr()
	{
		a310Hitr();
	}

protected void a310Hitr()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(uswdIO.getChdrcoy());
		hitrclmIO.setChdrnum(udtrigrec.chdrnum);
		hitrclmIO.setLife(udtrigrec.life);
		hitrclmIO.setCoverage(udtrigrec.coverage);
		hitrclmIO.setRider(udtrigrec.rider);
		hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
		hitrclmIO.setTranno(udtrigrec.tranno);
		hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
		hitrclmIO.setFormat(formatsInner.hitrclmrec);
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		wsaaProcSeqNo.set(hitrclmIO.getProcSeqNo());
		if (isNE(hitrclmIO.getFundAmount(), ZERO)) {
			wsaaSub.set(1);
			a400UpdateRecs();
		}
		utrnIO.setCrtable(hitrclmIO.getCrtable());
		utrnIO.setBatccoy(hitrclmIO.getBatccoy());
		utrnIO.setBatcbrn(hitrclmIO.getBatcbrn());
		utrnIO.setBatcactyr(hitrclmIO.getBatcactyr());
		utrnIO.setBatcactmn(hitrclmIO.getBatcactmn());
		utrnIO.setBatctrcde(hitrclmIO.getBatctrcde());
		utrnIO.setBatcbatch(hitrclmIO.getBatcbatch());
	}

protected void a400UpdateRecs()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a410FindFund();
				case a420Uswd:
					a420Uswd();
					a430RewriteHitr();
				case a490Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410FindFund()
	{
		if (isEQ(uswdIO.getSrcfund(wsaaSub), hitrclmIO.getZintbfnd())) {
			goTo(GotoLabel.a420Uswd);
		}
		wsaaSub.add(1);
		if (isGT(wsaaSub, 20)) {
			goTo(GotoLabel.a490Exit);
		}
		a410FindFund();
		return ;
	}

protected void a420Uswd()
	{
		setPrecision(uswdIO.getScactval(wsaaSub), 2);
		uswdIO.setScactval(wsaaSub, mult(hitrclmIO.getContractAmount(), -1));
		uswdIO.setScestval(wsaaSub, ZERO);
	}

protected void a430RewriteHitr()
	{
		hitrclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		hitrclmIO.setFeedbackInd("Y");
		hitrclmIO.setFormat(formatsInner.hitrclmrec);
		hitrclmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
	}

protected void a500UpdateUswd()
	{
		a510Uswd();
	}

protected void a510Uswd()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(uswdIO.getChdrcoy());
		hitrclmIO.setChdrnum(uswdIO.getChdrnum());
		hitrclmIO.setLife(uswdIO.getLife());
		hitrclmIO.setCoverage(uswdIO.getCoverage());
		hitrclmIO.setRider(uswdIO.getRider());
		hitrclmIO.setPlanSuffix(uswdIO.getPlanSuffix());
		hitrclmIO.setZintbfnd(uswdIO.getSrcfund(wsaaSub));
		hitrclmIO.setTranno(uswdIO.getTranno());
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		if (isEQ(hitrclmIO.getFundAmount(), ZERO)) {
			wsaaEndProc.set("Y");
			return ;
		}
		setPrecision(uswdIO.getScactval(wsaaSub), 2);
		uswdIO.setScactval(wsaaSub, mult(hitrclmIO.getFundAmount(), -1));
		uswdIO.setScestval(wsaaSub, ZERO);
		wsaaUpdateUswd.set("Y");
	}

protected void a600TotalHitrs()
	{
		a610Read();
	}

protected void a610Read()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(uswdIO.getChdrcoy());
		hitrclmIO.setChdrnum(uswdIO.getChdrnum());
		hitrclmIO.setLife(uswdIO.getLife());
		hitrclmIO.setCoverage(uswdIO.getCoverage());
		hitrclmIO.setRider(uswdIO.getRider());
		hitrclmIO.setPlanSuffix(uswdIO.getPlanSuffix());
		hitrclmIO.setZintbfnd(uswdIO.getSrcfund(wsaaSub));
		hitrclmIO.setTranno(uswdIO.getTranno());
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		if (isEQ(wsaaSub, 1)) {
			wsaaUtrnBatctrcde.set(hitrclmIO.getBatctrcde());
		}
		wsaaContractTotal.add(hitrclmIO.getContractAmount());
	}

protected void a700HitrTots()
	{
		a710ReadHitr();
	}

protected void a710ReadHitr()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(uswdIO.getChdrcoy());
		hitrclmIO.setChdrnum(uswdIO.getChdrnum());
		hitrclmIO.setLife(uswdIO.getLife());
		hitrclmIO.setCoverage(uswdIO.getCoverage());
		hitrclmIO.setRider(uswdIO.getRider());
		hitrclmIO.setPlanSuffix(uswdIO.getPlanSuffix());
		hitrclmIO.setZintbfnd(uswdIO.getSrcfund(wsaaSub));
		hitrclmIO.setTranno(uswdIO.getTranno());
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		wsaaTauv.add(hitrclmIO.getContractAmount());
	}

protected void a800CheckHitd()
	{
		a810Hitd();
	}

protected void a810Hitd()
	{
		hitdIO.setParams(SPACES);
		hitdIO.setChdrcoy(udtrigrec.company);
		hitdIO.setChdrnum(udtrigrec.chdrnum);
		hitdIO.setLife(udtrigrec.life);
		hitdIO.setCoverage(udtrigrec.coverage);
		hitdIO.setRider(udtrigrec.rider);
		hitdIO.setPlanSuffix(udtrigrec.planSuffix);
		hitdIO.setZintbfnd(hitrIO.getZintbfnd());
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)
		&& isNE(hitdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError9000();
		}
		if (isEQ(hitdIO.getStatuz(), varcom.oK)) {
			return ;
		}
		hitdIO.setZlstintdte(varcom.vrcmMaxDate);
		hitdIO.setZlstsmtdt(varcom.vrcmMaxDate);
		hitdIO.setZlstfndval(ZERO);
		hitdIO.setZlstsmtno(ZERO);
		hitdIO.setZlstsmtbal(ZERO);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(tablesInner.th510);
		itdmIO.setItmfrm(uswdIO.getEffdate());
		itdmIO.setItemitem(hitrIO.getZintbfnd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), udtrigrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.th510)
		|| isNE(itdmIO.getItemitem(), hitrIO.getZintbfnd())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemtabl(tablesInner.th510);
			itdmIO.setItemitem(hitrIO.getZintbfnd());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		th510rec.th510Rec.set(itdmIO.getGenarea());
		a1000GetNextFreq();
		hitdIO.setZnxtintdte(datcon2rec.intDate2);
		hitdIO.setTranno(udtrigrec.tranno);
		hitdIO.setValidflag("1");
		hitdIO.setEffdate(uswdIO.getEffdate());
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError9000();
		}
	}

protected void a900ProcessTax()
	{
		a900Start();
	}

protected void a900Start()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(udtrigrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(udtrigrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError9000();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(udtrigrec.company);
		itemIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(udtrigrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError9000();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(udtrigrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError9000();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		if (isNE(tr52erec.taxind11, "Y")) {
			return ;
		}
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.language.set(udtrigrec.language);
		txcalcrec.chdrcoy.set(udtrigrec.company);
		txcalcrec.chdrnum.set(udtrigrec.chdrnum);
		txcalcrec.life.set(covrmjaIO.getLife());
		txcalcrec.coverage.set(covrmjaIO.getCoverage());
		txcalcrec.rider.set(covrmjaIO.getRider());
		if (isEQ(covrmjaIO.getPlanSuffix(), NUMERIC)) {
			txcalcrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		}
		else {
			txcalcrec.planSuffix.set(ZERO);
		}
		txcalcrec.crtable.set(covrmjaIO.getCrtable());
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCntcurr(), SPACES);
		stringVariable1.addExpression(tr52erec.txitem, SPACES);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		wsaaBatccoy.set(utrnIO.getBatccoy());
		wsaaBatcbrn.set(utrnIO.getBatcbrn());
		wsaaBatcactyr.set(utrnIO.getBatcactyr());
		wsaaBatcactmn.set(utrnIO.getBatcactmn());
		wsaaBatctrcde.set(utrnIO.getBatctrcde());
		wsaaBatcbatch.set(utrnIO.getBatcbatch());
		txcalcrec.batckey.set(wsaaBatckey);
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		txcalcrec.effdate.set(udtrigrec.effdate);
		txcalcrec.amountIn.set(wsaaSwitchFee);
		txcalcrec.transType.set("SWCF");
		txcalcrec.tranno.set(udtrigrec.tranno);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(0);
		txcalcrec.taxAmt[2].set(0);
		txcalcrec.jrnseq.set(wsaaSequenceNo);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError9000();
		}
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(udtrigrec.chdrcoy);
		taxdIO.setChdrnum(udtrigrec.chdrnum);
		taxdIO.setLife(udtrigrec.life);
		taxdIO.setCoverage(udtrigrec.coverage);
		taxdIO.setRider(udtrigrec.rider);
		taxdIO.setPlansfx(udtrigrec.planSuffix);
		taxdIO.setEffdate(udtrigrec.effdate);
		taxdIO.setInstfrom(udtrigrec.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(udtrigrec.tranno);
		txcalcrec.jrnseq.set(wsaaSequenceNo);
		taxdIO.setTrantype(txcalcrec.transType);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(txcalcrec.taxrule);
		stringVariable2.addExpression(txcalcrec.rateItem);
		stringVariable2.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(0);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03("0");
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			fatalError9000();
		}
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTotalTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTotalTax.add(txcalcrec.taxAmt[2]);
		}
	}

protected void a1000GetNextFreq()
	{
		a1110Next();
	}

protected void a1110Next()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(uswdIO.getEffdate());
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(th510rec.zintalofrq);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.syserrType.set("2");
			fatalError9000();
		}
		if (isNE(th510rec.zintfixdd, ZERO)) {
			/*     MOVE TH510-ZINTFIXDD    TO DTC2-INT-DATE-2 (5:2) <LA4737>*/
			datcon2rec.intDate2.setSub1String(7, 2, th510rec.zintfixdd);
		}
	}

protected void fatalError9000()
	{
		/*ERROR*/
		/* IF SYSR-STATUZ              = 'BOMB'                         */
		/*    GO TO 9090-EXIT.                                          */
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/* MOVE WSAA-REPLY             TO UTRN-TRIGGER-KEY.             */
		/* MOVE WSAA-REPLY             TO WSAA-TRIGGER-KEY.        <034>*/
		udtrigrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
public static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t5544 = new FixedLengthStringData(5).init("T5544");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5519 = new FixedLengthStringData(5).init("T5519");
	private FixedLengthStringData t5539 = new FixedLengthStringData(5).init("T5539");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	public FixedLengthStringData tr384 = new FixedLengthStringData(6).init("TR384");
	private FixedLengthStringData t6646 = new FixedLengthStringData(5).init("T6646");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData th510 = new FixedLengthStringData(5).init("TH510");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
public static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData uswdrec = new FixedLengthStringData(10).init("USWDREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	public FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).init("HITRREC");
	private FixedLengthStringData hitrclmrec = new FixedLengthStringData(10).init("HITRCLMREC");
	private FixedLengthStringData hitdrec = new FixedLengthStringData(10).init("HITDREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
}
