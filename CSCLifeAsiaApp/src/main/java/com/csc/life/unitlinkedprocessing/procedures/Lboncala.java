/*
 * File: Zboncala.java
 * Date: 30 August 2009 2:56:55
 * Author: Quipoz Limited
 * 
 * Class transformed from Zboncala.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.ChdrpfDAOImpl;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.dao.LbonpfDAO;
import com.csc.life.enquiries.dataaccess.dao.impl.LbonpfDAOImpl;
import com.csc.life.enquiries.dataaccess.model.Lbonpf;
import com.csc.life.flexiblepremium.procedures.Fpunital;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.terminationclaims.recordstructures.Untallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5535rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
*REMARKS.
*
*      LOYALTY BONUS SUBROUTINE1
*
* OVERVIEW.
* ---------
*
* This program replaces UNLISS.
*
* This generic subroutine allocates units (premium) to a component
* at contract issue stage. It is identical to the subroutine
* UNLISS except that UTRN records are created at Coverage
* level, (even if the unit holding is applied to a Rider).
* The UTRN Sequence Number is used to differentiate between units
* allocated to Coverages & Riders.
* This change is required to bring unit allocation at Contract Issue
* and Premium Collection stages into line with Benefit Billing,
* where units are allocated at Coverage level, even when the benefit
* component is a Rider.
*
* T5671 has also been incorporated to facilitate calling the unit
* allocation subroutine generically.
*
* ZRULISS is the generic  subroutine  which  will  be  called  to
* perform the processing  necessary  on  the  issue  of  a  Unit
* Linked generic component.  It  will  be  called   (via  T5671,
* Coverage/Rider Switching) from within an AT module  for   each
* Unit Linked component found on a proposal being issued.
*
* ZRULISS will handle Coverage/Riders created at  proposal  issue
* and any new components added via automatic increases.
*
* It will perform the following functions:
*
* 1. Convert the UNLTUNL records to ULNKUNL records  and  delete
* UNLTUNL records afterwards.  (Note  that  'invest  by  amount'
* records will be converted into  'invest by percentage' values,
* even if the percentage amount  indicator on the UNLT is set to
* 'A'.)
*
* 2. Write an INCI record, (not for single premium only)
*
* 3. Write unit Fund transaction records:
*      UTRN record for the uninvested portion of the premium,
*      UTRN record for the Initial Units,
*      UTRN record for the Accumulation Units,
*
* 4. Update the Coverage/Rider record via COVRUNL.
*
* PROCESSING.
* -----------
*
* The processing takes place in five phases:
*
*      1. Obtain Existing Information.
*      2. Convert Fund Direction Transaction (UNLT) records to
*      ULNK records.
*      3. Create INCI (Increase) records.
*      4. Allocation - create UTRN records.
*      (This will be done by calling the subroutine ZRULALOC in
*      the case of regular premium contracts.)
*      5. Update the newly created Coverage/Rider record with
*      relevant data fields.
*
* 1. Obtain Existing Information.
* -------------------------------
*
* Read the Contract Header using  the  RETRV  function  and  the
* logical view CHDRLNB. This will  return  the  Contract  Header
* details for the component being processed.
*
* Read the Coverage/Rider record using the  key  passed  in  the
* linkage area,  a  function  of  READH  and  the  logical  view
* COVRUNL.
*
* The following dated tables are used by this program:
*
* T5687 - read using  the  Coverage/Rider code (COVR-CRTABLE) as
* key.
*
* T5540 - read using the Coverage/Rider code (as above).
*
* T5519 - if the Initial  Unit  Discount  Basis  from  T5540  is
* non-blank then use it to read T5519.
*
* T5535 - if the Loyalty/Persistency code  (LTYPST)  from  T5540
* is non-blank then use it to read T5535.
*
* Read T6647 with the Transaction Code (passed  in  the linkage)
* and CHDR-Contract Type as a key.   This  is  the  Unit  Linked
* Contract Details table and holds the  rules  specific  to that
* product and transaction.
*
* Read T5537 with a key of allocation basis  (from  T5540),  Sex
* and transaction code. If the full key is not found,  then  '*'
* is substituted for sex and the  table  read  again.  If  still
* unsuccessful, Allocation Basis,  Sex  and Transaction Code set
* as '****' is searched for. If still  unsuccessful,  Allocation
* Basis, Sex as '*' and Transaction code as '****'  is  searched
* for. If still unsuccessful, '********' is searched  for.  This
* table holds a matrix of  Allocation  Basis  codes  held  in  a
* two-dimensional table with  Age as the vertical ('y') axis and
* Term as the horizontal ('x')  axis.  Locate  the occurrence of
* Age that is greater than or equal to the  Age  Next  Birthday.
* Locate the occurrence of Term that is greater  than  or  equal
* to the Term. Locate the appropriate method.
* Move the Item Code at  this  position  to  a  working  storage
* variable for Allocation Basis  (WSAA-ALLOC-BASIS)  for  future
* use.
*
* Read T5536 with a key of  the  Allocation  Basis  just  found.
* Find the Billing Frequency  on  the  table  that  matches  the
* billing frequency from  the  contract  header  (BILLFREQ)  and
* move   its  associated  group  of  three  items:  MAX-PERIODS,
* PC-UNITS  and PC-INIT-UNITS to store for future use. Calculate
* PC-ACCUM-UNITS  as  (100 - PC-INIT-UNITS),  i.e.  if  not 100%
* allocated   to   initial   units,   the   remainder    becomes
* accumulation units.
*
* 2. Normal Processing.
* ---------------------
*
* This section deals  with  the  conversion  of  UNLT  to   ULNK
* records.  If  plan  processing  is  applicable  (and  this  is
* typically  only  the  case   in  UK  sites),   the   following
* information is of relevance, otherwise, go to Section 3.
*
* Plan Processing.
* ----------------
*
* When the COVTUNL records are  converted  into  COVR records by
* the Proposal Issue suite, one COVR record will  be created for
* each policy within the Plan except for the  first   group   of
* identical policies where one COVR record will  be created that
* represents a group of policies  that  are  identical,  i.e.  a
* summarised record. The  number  in  this  group  will  be  the
* smallest group represented  by  the  first  COVTUNL record for
* each Coverage/Rider. All other groups  will  be  'broken  out'
* into   individual   COVR  records.    The  following   example
* illustrates this.
*
*   Number of Policies in Plan = 10
*
*   Coverage #1, 3 COVTUNL records in groups of 5
*                                               3
*                                               2
*
*   Coverage #2, 2 COVTUNL records in groups of 3
*                                               7
* When the COVTUNL records are converted  into COVR records, the
* system will look at the first group of  definitions  for  each
* Coverage/Rider component (marked with  a  double  asterisk  in
* the above example) and select the  smallest  group -  in  this
* case 3. However,  the  COVTUNL  record  for  Coverage/Rider #1
* represents 5 policies.  This  will  have  to be converted to 3
* COVR records - #1 representing 3/5 of its  policies and #2 and
* #3 representing 1/5  each.   All  other  COVTUNL  records  for
* COVTUNL #1 will be  'broken out'  fully giving one COVR record
* per policy represented.
*
* The Plan Suffix will be '0000' on  the first COVR record as it
* represents a group of policies. The  other  COVR  records will
* have their  'Plan Suffixes'  allocated  sequentially  starting
* from a number  that allows for all the policies on the summary
* group and going  up  to  a  number equivalent to the number of
* policies in the plan.
*
* The following  example  shows  the  previous  COVTUNL  records
* converted to COVR  records.  The  largest  number  that can be
* summarised by a Coverage/Rider group for  the  whole  Plan  is
* '3', so the first group on Coverage/Rider #1  has to be broken
* down itself.
*
*   COV/RIDER      COVTUNL      COVR          PLAN SUFFIX
*                    No.
*                 Applicable
*
*      01             5          3/5              0000
*                                1/5              0004
*                                1/5              0005
*                     3          1/3              0006
*                                1/3              0007
*                                1/3              0008
*                     2          1/2              0009
*                                1/2              0010
*   - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*      02             3          3/3              0000
*                     7          1/7              0004
*                                1/7              0005
*                                1/7              0006
*                                1/7              0007
*                                1/7              0008
*                                1/7              0009
*                                1/7              0010
*
* The UNLTUNL records will be converted as follows:
*
* The ULNK records will mirror exactly the COVR  records created
* by  the  Proposal  Issue  suite.   The   group   of   policies
* represented  by  the  first  ULNK  record will be the smallest
* group for any Coverage/Rider component  defined  for the Plan.
* Note that this may be less than the group represented  by  the
* first COVTUNL and UNLTUNL record as in the above example.
*
* The number summarised by the first COVR record  will  be  held
* on the Contract Header record. This will tell the  program how
* many policies are to be summarised by the first  ULNK  record.
* All other UNLTUNL records will be broken down giving  one ULNK
* record per policy  and  each  having  its  own   'Plan Suffix'
* number. This will be  done  by  one  pass  through the UNLTUNL
* data-set on the first call  for  a  Coverage/Rider  component.
* The program maintains a flag  that  lets it know whether it is
* on its first pass or on subsequent passes.
*
* In the following example, it can  be  seen  that  the  UNLTUNL
* records first matched the COVTUNL  records  one  for one. When
* this program runs, the COVTUNL records will  already have been
* converted  into  the  COVR  records  shown  with  their  'Plan
* Suffixes'  in their keys. By looking at the Contract Header to
* see how many  policies  are  being  summarised,   it  will  be
* possible   to   convert  all  the  UNLTUNL  records  for  THIS
* Coverage/Rider component  in  one  pass.   Subsequent calls to
* this module for the SAME Coverage/Rider  component  will cause
* the program to read the corresponding ULNK record  created  on
* the first pass. The 'Plan Suffix' may be used to identify  the
* correct ULNK record.
*
* Note: this subroutine will be  called  once  for  every   COVR
* record created by the Proposal Issue suite.
*
*   COV/     COVTUNL --> COVR    PLAN   UNLTUNL --> ULNK   PLAN
*   RIDER      No.              SUFFIX    No.             SUFFIX
*           Applicable                 Applicable
*
*    01         5         3/5    0000      5        3/5    0000
*                         1/5    0004               1/5    0004
*                         1/5    0005               1/5    0005
*               3         1/3    0006      3        1/3    0006
*                         1/3    0007               1/3    0007
*                         1/3    0008               1/3    0008
*               2         1/2    0009      2        1/2    0009
*                         1/2    0010               1/2    0010
*   - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - -
*    02         3         3/3    0000      3        3/3    0000
*               7         1/7    0004      7        1/7    0004
*                         1/7    0005               1/7    0005
*                         1/7    0006               1/7    0006
*                         1/7    0007               1/7    0007
*                         1/7    0008               1/7    0008
*                         1/7    0009               1/7    0009
*                         1/7    0010               1/7    0010
*
* 3. Issue - Create INCI Record.
* ------------------------------
*
* Set up the fields for creating the  INCI  record  (Increases).
* The sources of the fields are:
*
*  Company Number      -  Passed in Linkage
*  Contract Number     -    "     "    "
*  Life                -    "     "    "
*  Coverage            -    "     "    "
*  Rider               -    "     "    "
*  Plan Suffix         - Calculated as described previously
*  Validflag           - '1'
*  Dormant Flag        - 'N'
*  INCI Number         - 001
*  TRANNO              - Passed in Linkage
*  RCDATE              - Contract Header
*  PREM-CESS-DATE      - Coverage/Rider
*  CRTABLE             -     "      "
*  ORIG-PREM           - COVRUNL-INSTPREM
*  CURR-PREM           - This either moves INSTPREM or
*                        WSAA-PREMIUM-PAID which is derived
*                        earlier by multiplying the INSTPREM by
*                        the FREQ-FACTOR.
*
* INCI Calculations.
* ------------------
*
* Other fields are calculated as follows:
*
* Here  we  are  first concerned with a group of fields on  the
* INCI record that occur in small groups of four:  INCI-PCUNIT,
* INCI-USPLITPC, INCI-PREMST and INCI-PREMCURR.
*
* Set INCI-PREMCURR  and  INCI-PREMST  to  the   Premium  No. of
* periods applicable from T5536.
*
* Set INCI-PCUNIT to the PCUINT from T5536.
*
* Set INCI-USPLITPC to PC-INIT-UNITS.
*
* The calling program  will  pass  as  one of its parameters the
* 'Premium   Available'.   This  may  differ  from  the   actual
* Instalment  Premium on the Coverage/Rider record.In most cases
* it will be  the  same  as the coverage although some contracts
* may be issued if there  is  no  money  actually  available  in
* suspense at the time of issue. If the  'Premium Available'  is
* zero   then   the   allocation  processing,   including  these
* calculations, should be skipped  entirely.  If  this  is   the
* case, then write the  INCI  record  now.   If  performing  the
* allocation processing,  then the 'Premium Available' should be
* used in case it differs from the Instalment Premium.
*
* First find the period with  which  we  are going to work. This
* will be the  first  of  the  INCI-PREMCURR  fields   that   is
* non-zero.
*
* If the split  for  the  period  is zero, then there is no more
* allocation to be done. Check   INCI-PCUNIT  and  INCI-USPLITPC
* and if both  are  zero  then   finish  with   the   allocation
* processing.
*
* Default the   enhancement  percentage  (PC-ENHANCE)   and life
* enhancement (LIFE-ENHANCE) to 100. If the Enhanced  Allocation
* code from T6647 (key = Contract Type)   is  not  spaces,  then
* read   T5545  with  the  Transaction  Code  and  the  Enhanced
* Allocation  Code  as the key. Match the Billing Frequency with
* that on T5545 and  take the corresponding Enhanced Percentages
* and Enhanced Premiums  group.   Match  the  Instalment Premium
* against the list to see if it is greater than  or equal to any
* of  the  Enhanced   Premiums.   If  it  is   then   take   the
* corresponding  Enhanced  Percentage  as  PC-ENHANCE  (Enhanced
* Percentage) to be used in later calculations.
*
* If the amount  of  the  Premium  Available  covers  the  first
* period and into  the  next  period, first allocate the premium
* to the current period and at  the  end  allocate the remainder
* to the next period.
*
* Set NET-PREMIUM to PREMIUM-AVAILABLE.
*
* If NET-PREMIUM is greater  than  INCI-PREMCURR  then  subtract
* INCI-PREMCURR  from  NET-PREMIUM  and  set  the  remainder  as
* NEXT-PERIOD and set NET-PREMIUM to INCI-PREMCURR.
*
* Calculate Current Period.
*
* Obtain the % Initial  Units  (PC-INIT-UNITS)  - from T5536 and
* the calculated %  Accumulation  units  (PC-ACCUM-UNITS).  Note
* that these figures  are still held as integers and are not yet
* held as percentage  multipliers.   To  obtain  these  figures,
* perform the  following  calculations.   The  receiving  fields
* should be defined as S9(03)V9(07) COMP-3.
*
* Divide  PC-UNITS, PC-INIT-UNITS, PC-ACCUM-UNITS and PC-ENHANCE
* by 100 rounding the answer.
*
* Calculate the number of UNITS (rounded) as PC-UNITS *
* PC-ENHANCE.
*
* Subtract the number of UNITS from 1 giving the UNINVESTED.
*
* Calculate the INIT-UNITS (rounded)        as PC-UNITS
*                                            * PC-INIT-UNITS
*                                            * PC-ENHANCE.
*
* Calculate the ACCUM-UNITS (rounded)       as PC-UNITS
*                                            * PC-ACCUM-UNITS
*                                            * PC-ENHANCE.
*
* Calculate the UNITS-VALUE (rounded)       as PC-UNITS
*                                            * NET-PREMIUM.
*
* Calculate the UNINVESTED-VALUE (rounded)  as UNINVESTED
*                                            * NET-PREMIUM.
*
* Calculate the INIT-UNITS-VALUE (rounded)  as INIT-UNITS
*                                            * NET-PREMIUM.
*
* Calculate the ACCUM-UNITS-VALUE (rounded) as ACCUM-UNITS
*                                            * NET-PREMIUM.
*
* If the  NEXT-PERIOD  is  greater  than zero, then perform  the
* calculations again for the next period using  the  NEXT-PERIOD
* as the new NET-PREMIUM.
*
* If the NEXT-PERIOD is not zero, subtract the second
* NET-PREMIUM from the second INCI-PREMCURR.
*
* Subtract the first NET-PREMIUM from the first INCI-PREMCURR.
*
* Write the INCI record.
*
* 4. Allocation - Create UTRN records.
* ------------------------------------
*
* Allocation of units  and  the  writing  of  UTRN  records  for
* Regular and Single premiums are dealt with separately.  Single
* premiums are allocated within this  module,  regular  premiums
* are handled by  calling  ZRULALOC.  To  cater  for  components
* which   have   a  Lump  Sum  paid  in  at  New  Business,  the
* appropriate  sections are only performed if the premium amount
* in question is non-zero.
*
* Monies Date - Unit Allocation Date.
* -----------------------------------
*
* The Monies Date,  the  effective  date  for  Unit  Allocation,
* should be calculated. The code returned  from  T6647  will  be
* either 'BD' - Business Date, 'DD' - Due Date,  which for Issue
* is the Risk Commencement Date or 'LO' -  Later  Of  the  above
* two. This will determine which  date  is to  be  used  as  the
* Monies Date by  Unit  Allocation.  If  it  is  'BD'  then  the
* current system  date will be used, if it is 'DD' then the Risk
* Commencement Date  will  be  used.   If  it  is  'LO' then the
* greater of the above two dates will be  used.   However,  this
* will be overridden if the user has entered  a  'Reserve  Date'
* which will be stored on the COVR record. If the  Reserve  Date
* from COVR is non zero then use this as the Monies Date.
*
* The allocation of units  will  be  on  either  a  'Now'  or  a
* 'Deferred'  basis.    This  depends  on  the  value   of   the
* 'Allocation'   field  from  T6647.    If  this  is  'D',  then
* allocation is  deferred, and if this is 'N' then allocation is
* immediate.
*
* Allocation.
* -----------
*
* First determine  the fund split by reading the ULNKUNL record.
* The correct ULNKUNL  record will be obtained by using the same
* key as is on the corresponding COVR record being processed.
*
* The following Allocation  Processing  will  be  performed once
* for the current period and possibly again if a  second  set of
* details was set up in the INCI record due to the  presence  of
* a greater premium than was required.
*
* A UTRN record will be created for the  uninvested  portion  of
* the premium. Note that there may be two sets of  INCI  details
* on the INCI record if there  is  enough  premium  to  allocate
* into the next period. If so  then  TWO  UTRN  records  will be
* created.The usual key and transaction  data  will  be  set  up
* plus the following:
*
*  UTRN-RLDGCOY - Company from CHDR
*  UTRN-RLDGACCT  - Contract Number from CHDR
*  UTRN-JOB-NAME-ORIG - spaces
*  UTRN-JOBNO-ORIG  - zero
*  UTRN-JOBNO-EXTRACT - zero
*  UTRN-JOBNO-UPDATE- zero
*  UTRN-JOBNO-PRICE - zero
*  UTRN-STAT-FUND - from COVRUNL
*  UTRN-STAT-SECT -   "     "
*  UTRN-STAT-SUBSECT- "     "
*  UTRN-UNIT-VIRTUAL-FUND - spaces
*  UTRN-UNIT-SUB-ACCOUNT- spaces
*  UTRN-EFFDATE - Risk Commencement Date
*  UTRN-STRPDATE  - zero
*  UTRN-NOW-DEFER-IND - 'N' - Now or 'D' -Deferred
*  UTRN-UNIT-TYPE - spaces
*  UTRN-NOF-UNITS - zero
*  UTRN-NOF-DUNITS  - zero
*  UTRN-UL-AMOUNT - Uninvested Amount from INCI record
*  UTRN-MONIES-DATE - The previously determined Monies Date
*  UTRN-PRICE-DATE-USED - Risk Commencement Date
*  UTRN-PRICE-USED  - zero
*  UTRN-BARE-PRICE  - zero
*  UTRN-CRTABLE - from COVRUNL
*  UTRN-CNTCURR - Contract Currency from CHDR
*  UTRN-CSHD-EFFD-IND - spaces
*  UTRN-FEEDBACK-IND- spaces
*  UTRN-INCI-NUM  - INCI number from the INCI record
*  UTRN-INCI-PERD - INCI period, i.e. 1 or 2
*  UTRN-INCI-PREMIUM- Uninvested Amount from INCI record
*  UTRN-SURR-PENALTY-FACTOR - zero
*
* The following processing will be carried out for each fund  in
* the fund split as specified on the ULNKUNL record. There is  a
* maximum of ten funds on the ULNKUNL table. The  processing  is
* divided into two  very  similar  parts:  firstly  for  Initial
* Units and then secondly for Accumulation Units.
*
*      For the Pricing  Date  the  Monies  Date  is  used  if no
*      Reserve  Units  Date  is  held  on  the  COVRUNL  record,
*      otherwise the COVRUNL Reserve Units Date is used.
*
*      Set the UTRN-UNIT-SUB-ACCOUNT to 'INIT', for Initial
*      Units.
*
*      Calculate  the  basic  premium  (unenhanced) for the fund
*      split. It may  be  used  later  for  windbacks.   If  the
*      ULNKUNL record  indicates  to use a percentage, calculate
*      the following:
*
*      INCI-FUND-AMT (rounded) = UNIT-PC
*                              * Percent/Amount (from ULNKUNL)
*                              / 100.
*
*      If the ULNKUNL record  indicates  that an amount has been
*      entered and not a percentage do the following
*      calculation:
*
*      INCI-FUND-AMT (rounded) = UNIT-PC
*                              * Percent/Amount (from ULNKUNL).
*
*      This result is then rounded to  two  decimal  places   by
*      adding zero to it with  the  rounding  clause  and  using
*      UTRN-INCI-AMT as the receiving field defined as
*      S9(11)V9(02) COMP-3.
*
*      The result - UTRN-INCI-AMT -  is later placed on the UTRN
*      record.
*
*      The Enhanced Premium  Amount  is  calculated  as  follows
*      where percentage are concerned:
*
*      FUND-AMT rounded = TEMP-AMT
*                       * Percentage/Amount (from ULNKUNL)
*                       / 100
*
*      where   TEMP-AMT  is  either   the   INIT-UNITS-VAL    or
*      ACCUM-UNITS-VAL calculated earlier depending  on  whether
*      initial units or accumulation units are  being  processed
*      - and as follows where an amount is concerned:
*
*      FUND-AMT rounded = UNIT-PC
*                       * Percentage/Amount (from ULNKUNL).
*
*      This result is also then rounded to  two  decimal  places
*      by adding zero to it with the rounding clause  and  using
*      UTRN-AMT as the receiving field defined as S9(11)V9(02)
*      COMP-3.
*
*      The number of units on the actual money  amount  must  be
*      calculated to two decimal places and rounded, whereas  up
*      to now it has been handled with seven decimal places.
*
*      When creating each UTRN record  note  that  the  Contract
*      Currency may differ from the Fund  Currency  as  held  on
*      T5515. In this case the Price and Units held on the  UTRN
*      record will have to be altered to take this into account.
*
* A partial UTRN record will have the UTRN-JOBNO,  UTRN-PRICEDT,
* UTRN-UNITS, UTRN-DUNITS, JOBNO-PRICE, OFFER-PRICE and
* BARE-PRICE set to zero.
*
* 5. Coverage Calculations.
* -------------------------
*
* The total premium for the coverage will have  been  passed  in
* the Linkage Section.
*
* Certain dates are calculated and stored on the COVR as
* follows:
*
*   ANNIVERSARY PROCESSING DATE
*   CONVERT INITIAL UNITS DATE
*   REVIEW PROCESSING DATE
*   UNIT STATEMENT DATE
*   CPI-DATE
*   INIT-UNIT-CANC-DATE
*   EXTRA-ALLOC-DATE
*   INIT-UNIT-INCREASE-DATE
*
* The dates are originally  set  to  all  '9's  and   then   the
* following logic is performed  which may or may not alter them.
* Anniversary Processing  Date -   this  processing  is   always
* performed:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1   -  OCCDATE
*      DTC2-FREQUENCY    -  '01'
*      DTC2-FREQ-FACTOR  -  1
*
*      Call DATCON2 and use the returned date to call DATCON2  a
*      second time with the parameters set as follows:
*
*      DTC2-INT-DATE-1   -  DTC2-INT-DATE-2
*      DTC2-FREQUENCY    -  'DY'
*      DTC2-FREQ-FACTOR  -  -1
*
*      Call DATCON2 and use the returned date as the
*      Anniversary Processing Date.
*
* Convert Initial Units Date  -  this  processing  is  dependent
* upon the retrieved data from a  read  of  T5519.   Read  T5519
* using the Initial Unit Discount  Basis  as  a  key.   If   the
* returned TERM-TO-RUN is 'Y' then perform  the  'Term  to  Run'
* processing, otherwise if the  'Whole  of  Life'  is  'Y'  then
* perform the 'Whole  of  Life'  processing,  otherwise  if  the
* FIXDTRM is not zero  then perform the 'Fixed Term' processing.
* If none of these has  been  set, then there is an error - give
* error message number E686   and  perform  the   system   error
* processing.
*
*      TERM TO RUN - set the following fields for a call to
*      DATCON2:
*
*      DTC2-INT-DATE-1  -  Risk Cessation Date
*      DTC2-FREQUENCY   -  'DY'
*      DTC2-FREQ-FACTOR -  -1
*
*      Call DATCON2 and use the returned date as the Convert
*      Initial Units Date.
*
*      WHOLE OF LIFE - set the Convert Initial Units Date to
*      all '9's.
*
*      FIXED TERM - set the following fields for a call to
*      DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  Fixed Term from T5519
*
*      Call DATCON2 and use the returned date to call  DATCON2 a
*      second time with the parameters set as follows:
*
*      DTC2-INT-DATE-1  -  DTC2-INT-DATE-2
*      DTC2-FREQUENCY   -  'DY'
*      DTC2-FREQ-FACTOR -  -1
*
*      Call DATCON2 and use the returned date as the Convert
*      Initial Units Date.
*
* Review Processing Date - perform this processing only  if this
* field (RVWPROC) from the COVRUNL is non-zero.
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  RVWPROC
*
*      Call DATCON2 and use the returned date as the Review
*      Processing Date.
*
* Unit Statement Date - this processing is always performed:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  1
*
*      Call DATCON2 and use the returned date as the Unit
*      Statement Date.
*
* Initial Unit Increase Date -  If  the  Initial  Unit  Discount
* Basis  from  T5540  is  blank  then  skip   this   processing,
* otherwise:
*
*      If the Differing Price flag (from T5519) is  not  'Y'  OR
*      the With Unit Adjustment flag  (from T5519)  is  not  'Y'
*      then skip this processing,  otherwise   set  the  Initial
*      Unit Increase Date to the Anniversary Processing Date.
*
* Initial Unit Cancellation Date - If the Initial Unit  Discount
* Basis from T5540 is blank then skip this processing,
* otherwise:
*
*      If the Unit Deduction Flag from T5519  is  'Y'  then  set
*      the Initial Unit Cancellation  Date  as  the  Anniversary
*      Processing Date.
*
* Extra Allocation  Date  -  if  the  Loyalty/Persistency   code
* (LTYPST) is spaces then skip this processing, otherwise:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '12'
*      DTC2-FREQ-FACTOR -  'After Duration' (1) from T5535
*
*      Call DATCON2 and use the returned date as the Extra
*      Allocation Date.
*
* Any date not modified by the previous processing  is  left  as
* all '9's.
*
* Update the COVRUNL record.
*
* MODIFICATIONS.
* --------------
*
* Tables accessed and their keys:
*
* T5687 - key = Coverage/Rider code - CRTABLE (from COVRUNL)
* T5540 - key = Coverage/Rider code - CRTABLE (from COVRUNL)
* T6647 - key = Transaction Code + Contract Type (from CHDRLNB)
* T5537 - key = Basis Select(from T5540) + sex + Transaction
* Code
* T5536 - key = Allocation Basis  (from T5537)
* T5545 - key = Enhanced Allocation Code(from T6647)
* T5519 - key = Initial Unit Discount Basis (from T5540)
* T5533 - key = Loyalty/Persistency Code(from T5540)
* T5515 - key = Fund Code
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Cater for Interest Bearing Fund lump sum investment and create
* a new Interest Details record (HITD) for each new Interest
* bearing fund. Pass ISUA-EFFDATE (which was set as the component
* commencement date in P5074AT) to RNLA-EFFDATE when calling the
* unit allocation routine instead of today's date, so that the
* HITRs are written with with the correct date.
*
*****************************************************************
* </pre>
*/
public class Lboncala extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "LBONCALA";

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaContract = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4);


	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTref = new FixedLengthStringData(30);
	
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	
	private PackedDecimalData wsaaDuration = new PackedDecimalData(7, 2);
	private PackedDecimalData wsaaPeriod = new PackedDecimalData(7, 2);
	//private PackedDecimalData wsaaFact = new PackedDecimalData(3, 0);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();

	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	//private Datcon4rec datcon4rec = new Datcon4rec();
	//private Itemkey wsaaItemkey = new Itemkey();
	private T5687rec t5687rec = new T5687rec();
	//private T6646rec t6646rec = new T6646rec();
	//private T5539rec t5539rec = new T5539rec();
	private T5540rec t5540rec = new T5540rec();
	//private T5544rec t5544rec = new T5544rec();
	private T6647rec t6647rec = new T6647rec();

	private T5535rec t5535rec = new T5535rec();
	private T5645rec t5645rec = new T5645rec();

	private Rnlallrec rnlallrec = new Rnlallrec();
	//private Txcalcrec txcalcrec = new Txcalcrec();
	//private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	//private Zrdecplrec zrdecplrec = new Zrdecplrec();
	 
	private ErrorsInner errorsInner = new ErrorsInner();
	//private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	//private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();
	//private ExternalisedRules er = new ExternalisedRules();
	
	private PackedDecimalData instprem = new PackedDecimalData(17,2);
	private PackedDecimalData crrcd = new PackedDecimalData(8,0);
	private Payrpf payrpf = new Payrpf();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	private List<Payrpf> payrpfList= null;
	ZonedDecimalData ptdate = new ZonedDecimalData(8,0);
	//private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0) ;
	//private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0) ;
	
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (15, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData TotalPrem=new PackedDecimalData(10,2);
	private PackedDecimalData IPrem=new PackedDecimalData(10,2);
    private PackedDecimalData Factor1=new PackedDecimalData(10,2);	 
   // private PackedDecimalData Factor2=new PackedDecimalData(10,2);	 
    private PackedDecimalData MinFactor=new PackedDecimalData(10,2);	 
    private PackedDecimalData LoyaltyBasis=new PackedDecimalData(17,2);	 
    private PackedDecimalData LoyaltyBonus = new PackedDecimalData(17,2);
    private LbonpfDAO lbonpfDAO = new LbonpfDAOImpl();
    private List<Lbonpf> lbonpfBulkOpList= new ArrayList<Lbonpf>();
    protected BsscTableDAM bsscIO = new BsscTableDAM();
    private List<Ptrnpf> ptrnpfInsertList;
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
 
	private ZonedDecimalData wsaaTodayDate = new ZonedDecimalData(8, 0);
	private List<Chdrpf> chdrpfList = new ArrayList<Chdrpf>();
	private ChdrpfDAO chdrDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAOImpl.class);
	private Chdrpf chdrpf = null;
	private AcmvpfDAO acmvpfDAO =  getApplicationContext().getBean("acmvDAO", AcmvpfDAO.class);
	private List<Acmvpf> acmvpfList;
	private Acmvpf acmvpf = null;
	private String sacscode = "LE";
	private String sacstype = "LP";
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont10450, 
		cont10460, 
		keepsChdrmja, 
		readItem10515, 
		cont10520, 
		yOrdinate10540, 
		xOrdinate10580, 
		endLoop10650, 
		exit10690, 
		nextRecord20100, 
		read20200, 
		exit40090, 
		extraAllocDate50400, 
		unitStmntDate50500, 
		updateCovrunl50700, 
		exit50090, 
		fundAllocation60050, 
		endLoop60150, 
		exit60190, 
		comlvlacc61925, 
		cont61950, 
		enhanb63200, 
		enhanc63300, 
		enhand63400, 
		enhane63500, 
		enhanf63600, 
		nearExit63800, 
		next64150, 
		accumUnit64200, 
		next64250, 
		nearExit64800, 
		termToRun65200, 
		fixedTerm65300, 
		exit65900, 
		b100CreateZrst, 
		b100Exit, 
		b300WriteTax2
	}

	public Lboncala() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rnlallrec.rnlallRec  = convertAndSetParam(rnlallrec.rnlallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		obtainInfo10000();
	}

protected void exit090()
	{
		exitProgram();
	}
protected void updatechdr()
{
	chdrpf = new Chdrpf();
	chdrpf.setChdrcoy(chdrlnbIO.getChdrcoy().charat(0));
	chdrpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	chdrpf.setTranno(add(chdrlnbIO.getTranno(),1).toInt());
	chdrpfList.add(chdrpf);
	if(chdrpfList != null && chdrpfList.size() > 0 ){
		chdrDAO.updateChdrTranno(chdrpfList);
		chdrpfList.clear();
		chdrpfList = null;
	}
}

protected void obtainInfo10000()
	{
		/*PARA*/
		varcom.vrcmTimex.set(getCobolTime());
		varcom.vrcmTime.set(varcom.vrcmTimen);
		wsaaJrnseq.set(ZERO);
		rnlallrec.statuz.set(varcom.oK);
		getFiles10100();
	 	getT554010300();	 	 
		getT553511100();	
		getT5645100();
		calcLoyaltybonus();
	}
protected void getT553511100()
{
		para11110();
	}
protected void para11110()
{
	/*  Read T5535 for loyalty allocation.*/
	//t5540rec.ltypst.set("LB01");
	if (isEQ(t5540rec.ltypst,SPACES)) {
		return ;
	}
	itdmIO.setDataKey(SPACES);
	itdmIO.setItemcoy(rnlallrec.company);
	itdmIO.setItemtabl(tablesInner.t5535);
	itdmIO.setItemitem(t5540rec.ltypst);
	itdmIO.setItmfrm(chdrlnbIO.getOccdate());
	itdmIO.setFunction(varcom.begn);
    //performance improvement --  Niharika Modi 
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

	SmartFileCode.execute(appVars, itdmIO);
	if ((isNE(itdmIO.getStatuz(),varcom.oK))
	&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(itdmIO.getStatuz());
		dbError8100();
	}
	if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
	|| (isNE(itdmIO.getItemtabl(), tablesInner.t5535))
	|| (isNE(itdmIO.getItemitem(),t5540rec.ltypst))
	|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5535);
		itdmIO.setItemitem(t5540rec.ltypst);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(errorsInner.g032);
		systemError8000();
	}
	else {
		t5535rec.t5535Rec.set(itdmIO.getGenarea());
	}
}

protected void  getT5645100()
{
	itemIO.setDataArea(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itemIO.setItemtabl(tablesInner.t5645);
	itemIO.setItemitem("LBONCALA");
	itemIO.setItemseq(SPACES);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(itemIO.getStatuz());
		systemError8000();
	}
	t5645rec.t5645Rec.set(itemIO.getGenarea());
	/* Set up array containing 2nd page of T5645 entries*/
	wsaaCount.set(ZERO);
	while ( !(isEQ(wsaaCount,15))) {
		wsaaCount.add(1);
		wsaaT5645Cnttot[wsaaCount.toInt()].set(t5645rec.cnttot[wsaaCount.toInt()]);
		wsaaT5645Sacscode[wsaaCount.toInt()].set(t5645rec.sacscode[wsaaCount.toInt()]);
		wsaaT5645Sacstype[wsaaCount.toInt()].set(t5645rec.sacstype[wsaaCount.toInt()]);
		wsaaT5645Glmap[wsaaCount.toInt()].set(t5645rec.glmap[wsaaCount.toInt()]);
		wsaaT5645Sign[wsaaCount.toInt()].set(t5645rec.sign[wsaaCount.toInt()]);
	}
}
protected void getFiles10100()
	{
		para10110();
	}

protected void para10110()
	{
		/* Retrieve contract header details.*/
	
		chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setChdrcoy(rnlallrec.company);
		chdrlnbIO.setChdrnum(rnlallrec.chdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
				 
		/*Retrieve the CHDRMJA logical view as this contains the free*/
		/*switch information fields which will be updated.*/
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setDataKey(chdrlnbIO.getDataKey());
		chdrmjaIO.setStatuz(varcom.oK);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			dbError8100();
		}
		
		 
		 
		covrunlIO.setChdrcoy(rnlallrec.company);
		covrunlIO.setChdrnum(rnlallrec.chdrnum);
		covrunlIO.setLife(rnlallrec.life);
		covrunlIO.setCoverage(rnlallrec.coverage);
		covrunlIO.setRider(rnlallrec.rider);
		covrunlIO.setPlanSuffix(rnlallrec.planSuffix);
		/*    MOVE READH                  TO COVRUNL-FUNCTION.             */
		covrunlIO.setFunction(varcom.readr);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(),varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			dbError8100();
		}
		 
		crrcd.set(covrunlIO.getCrrcd());
		instprem.set(covrunlIO.getInstprem());
		
		
		payrpf.setChdrcoy(rnlallrec.company.toString());
		payrpf.setChdrnum(rnlallrec.chdrnum.toString());
		payrpf.setValidflag("1");
		payrpf.setPayrseqno(1);
		payrpfList = payrpfDAO.readPayrData(payrpf);
		if(payrpfList==null || (payrpfList!=null && payrpfList.size()==0))
			{
				return;
			}
		else
		{
		payrpf = payrpfList.get(0);
		 ptdate.set(payrpf.getPtdate());
		}
		
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(crrcd);
		
		if (isEQ(rnlallrec.batctrcde.toString(),"B536"))
		{
			datcon3rec.intDate2.set(payrpf.getPtdate());
		}
		else  
		{	
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaTodayDate.set(datcon1rec.intDate);
			datcon3rec.intDate2.set(datcon1rec.intDate);
		}			
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaDuration.set(div(datcon3rec.freqFactor,12));
				
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaTodayDate.set(datcon1rec.intDate);
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(crrcd);
			datcon3rec.intDate2.set(datcon1rec.intDate);
		
			 if (isEQ(rnlallrec.billfreq,"01"))   //ann
		     {
			  datcon3rec.frequency.set("01"); 
		     }
		     else if (isEQ(rnlallrec.billfreq,"02")) //half
		     {
		    	 datcon3rec.frequency.set("02");
		     }
		     else if (isEQ(rnlallrec.billfreq,"12")) //mo
		     {
		    	 datcon3rec.frequency.set("12");
		     }		    
		     else  if (isEQ(rnlallrec.billfreq,"04"))  //q
		     {
		    	 datcon3rec.frequency.set("04");
		     }	
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				datcon3rec.freqFactor.set(0);
			}
			wsaaPeriod.set(datcon3rec.freqFactor);
			
		
	}

protected void calcLoyaltybonus()
{
			TotalPrem.set(ZERO);
			CalcTotalPrem();
		
			//TotalPrem.set(add(mult(instprem,wsaaPeriod),instprem));
			
			 
			
			compute(Factor1, 0).set(div(TotalPrem,instprem));
		 
			if (isLT(Factor1,wsaaDuration))
				MinFactor.set(Factor1);
			else if (isLT(wsaaDuration,Factor1))	
				MinFactor.set(wsaaDuration);
			else
				MinFactor.set(Factor1);
			
			if ( isLTE( MinFactor,t5535rec.unitAfterDur01 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc01);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur02 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc02);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur03 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc03);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur04 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc04);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur05))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc05);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur06 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc06);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur07 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc07);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur08 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc08);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur09 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc09);
			}
			else if ( isLTE( MinFactor,t5535rec.unitAfterDur10 ))
			{
				LoyaltyBasis.set(t5535rec.unitExtPcAlloc10);
			}
			
			compute(LoyaltyBonus, 0).set(div( mult(LoyaltyBasis,instprem),100));
			rnlallrec.totrecd.set(LoyaltyBonus);	
			
			postTransaction();
}
protected void postTransaction()
{
	if (isLTE(rnlallrec.totrecd,ZERO))
	{
		return;
	}
	postLoyaltyBonus101();
	postLoyaltyBonus102();	
	insertLbonpf();
	writePtrn();
	updatechdr();
	processUnit();
	
}

protected void postLifetrans()
{
	
 
	lifacmvrec.function.set("PSTW");
	lifacmvrec.batcactmn.set(rnlallrec.batcactmn);
	lifacmvrec.batcactyr.set(rnlallrec.batcactyr);
	lifacmvrec.batcbatch.set(rnlallrec.batcbatch);
	lifacmvrec.batcbrn.set(rnlallrec.batcbrn);
	lifacmvrec.batccoy.set(chdrlnbIO.getChdrcoy());
	lifacmvrec.batctrcde.set("BAEH");	
	lifacmvrec.trandesc.set("Loyalty Bonus");
	lifacmvrec.transactionTime.set(varcom.vrcmDate);
	lifacmvrec.transactionDate.set(varcom.vrcmTime);
	lifacmvrec.termid.set(varcom.vrcmCompTermid);
	lifacmvrec.user.set(varcom.vrcmUser);
	lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
	lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
	lifacmvrec.origcurr.set(rnlallrec.cntcurr);
	lifacmvrec.tranno.set(add(chdrlnbIO.getTranno(),1));	
	lifacmvrec.crate.set(0);
	lifacmvrec.acctamt.set(0);
	lifacmvrec.rcamt.set(0);
	lifacmvrec.genlcur.set(rnlallrec.cntcurr);
	lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
	lifacmvrec.postyear.set(SPACES);
	lifacmvrec.postmonth.set(SPACES);
	lifacmvrec.effdate.set(wsaaTodayDate);
	lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());	 
	wsaaRldgChdrnum.set(covrunlIO.getChdrnum());
	wsaaRldgLife.set(covrunlIO.getLife());
	wsaaRldgCoverage.set(covrunlIO.getCoverage());
	wsaaRldgRider.set(covrunlIO.getRider());
	wsaaPlan.set(covrunlIO.getPlanSuffix());
	wsaaRldgPlanSuffix.set(wsaaPlansuff);
	lifacmvrec.rldgacct.set(wsaaRldgacct);
}

protected void postLoyaltyBonus101()
{
	if (isNE(rnlallrec.totrecd,0)) {
		postLifetrans();
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[1]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[1]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[1]);
		lifacmvrec.glsign.set(wsaaT5645Sign[1]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[1]);
		lifacmvrec.tranref.set(covrunlIO.getChdrnum());
		lifacmvrec.origamt.set(rnlallrec.totrecd);	 
		lifacmvrec.acctamt.set(rnlallrec.totrecd);	 
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		lifacmvCheck2950();
	}
}
protected void postLoyaltyBonus102()
{
	if (isNE(rnlallrec.totrecd,0)) {
		postLifetrans();
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[2]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[2]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[2]);
		lifacmvrec.glsign.set(wsaaT5645Sign[2]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[2]);
		lifacmvrec.tranref.set(covrunlIO.getChdrnum());
		lifacmvrec.origamt.set(rnlallrec.totrecd);	 
		lifacmvrec.acctamt.set(rnlallrec.totrecd);	 
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		lifacmvCheck2950();
	}
}

protected void insertLbonpf()
{
	 Lbonpf obj= new Lbonpf();
	 obj.setChdrcoy(chdrlnbIO.getChdrcoy().toString());	 
	 obj.setChdrnum(chdrlnbIO.getChdrnum().toString());	 
	 obj.setLife(covrunlIO.getLife().toString());
	 obj.setCoverage(covrunlIO.getCoverage().toString());		                 
	 obj.setRider(covrunlIO.getRider().toString());
	 obj.setPlnsfx(covrunlIO.getPlanSuffix().toString());
	 obj.setEffdate(wsaaTodayDate.toInt());
	 obj.setTranno(add(chdrlnbIO.getTranno(),1).toInt());					
	 obj.setTrcde(rnlallrec.batctrcde.toString());
	 ZonedDecimalData amt = new ZonedDecimalData(17,2);
	 amt.set(rnlallrec.totrecd);
	 obj.setTrnamt(IPrem.getbigdata() );
	 obj.setNetpre(TotalPrem.toInt());
	  BigDecimal amt1;
	 amt1=LoyaltyBasis.getbigdata();
	 obj.setUextpc(amt1);
	 obj.setUalprc(amt.getbigdata());
	 lbonpfBulkOpList.add(obj);
	 lbonpfDAO.insertLBonPFValidRecord(lbonpfBulkOpList);
}

protected void getT568710200()
	{
		para10210();
	}

protected void para10210()
	{
		/*  Read T5687 for general coverage/rider details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5687))
		|| (isNE(itdmIO.getItemitem(),covrunlIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			itdmIO.setItemtabl(tablesInner.t5687);
			itdmIO.setItemitem(covrunlIO.getCrtable());
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.h053);
			systemError8000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT554010300()
	{
		para10310();
	}

protected void para10310()
	{
		/*  Read T5540 for general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItemitem(covrunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
		|| (isNE(itdmIO.getItemitem(),covrunlIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			itdmIO.setItemtabl(tablesInner.t5540);
			itdmIO.setItemitem(covrunlIO.getCrtable());
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.h055);
			systemError8000();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT664710400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10410();
				case cont10450: 
					cont10450();
				case cont10460: 
					cont10460();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10410()
	{
		/* Read T6647 for unit linked contract details.*/
		/* Key is transaction code plus contract type.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(rnlallrec.batctrcde);
		wsaaContract.set(chdrlnbIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			goTo(GotoLabel.cont10450);
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.cont10460);
		}
	}

protected void cont10450()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(rnlallrec.batctrcde);
		wsaaContract.set("***");
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			itdmIO.setItemtabl(tablesInner.t6647);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.h115);
			systemError8000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void cont10460()
	{
		/* Calculate the monies date.*/
		if (isNE(covrunlIO.getReserveUnitsDate(),ZERO)
		&& isNE(covrunlIO.getReserveUnitsDate(),varcom.vrcmMaxDate)) {
			wsaaMoniesDate.set(covrunlIO.getReserveUnitsDate());
			return ;
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaMoniesDate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode,"BD")) {
			return ;
		}
		if (isEQ(t6647rec.efdcode,"DD")) {
			wsaaMoniesDate.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t6647rec.efdcode,"LO")) {
			if (isGT(chdrlnbIO.getOccdate(),wsaaMoniesDate)) {
				wsaaMoniesDate.set(chdrlnbIO.getOccdate());
			}
		}
	}
protected void lifacmvCheck2950()
{
	/*CHECK*/
	if (isNE(lifacmvrec.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec.statuz);
		dbError8100();
	}
	/*EXIT*/
}

protected void  CalcTotalPrem()
{
	FixedLengthStringData sBatch= new FixedLengthStringData(10);
	acmvpf = new Acmvpf();
	acmvpf.setRldgacct(rnlallrec.chdrnum.toString());
	acmvpf.setRldgcoy(rnlallrec.company.charat(0));	 
	acmvpf.setRdocnum(rnlallrec.chdrnum.toString());
	acmvpf.setGlcode("PREMREC");
	acmvpf.setSacscode(sacscode);
	acmvpf.setSacstyp(sacstype);
	
	acmvpfList = acmvpfDAO.ReadAcmvpf(acmvpf);	
	if(acmvpfList !=null && acmvpfList.size() > 0)
	{
		for(Acmvpf acmv : acmvpfList)
		{
			 sBatch.set(acmv.getBatctrcde());
			  if ( isEQ(sBatch,"T642") ||  isEQ(sBatch,"B536")    ||  isEQ(sBatch,"T536")    )
			  {
				 IPrem.set(acmv.getOrigamt());
				 TotalPrem.add(IPrem);
			  }
		}
	}
}

protected void dbError8100()
{
		db8100();
		dbExit8190();
	}

protected void writePtrn()
{
	  Ptrnpf p = getPtrnpf();
      p.setTranno(add(chdrlnbIO.getTranno(),1).toInt());
      p.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
      p.setChdrnum(chdrlnbIO.getChdrnum().toString());
      p.setBatcpfx("BA"); 
      p.setDatesub(wsaaTodayDate.toInt());
      if (ptrnpfInsertList == null) {
          ptrnpfInsertList = new ArrayList<Ptrnpf>();
      }
      ptrnpfInsertList.add(p);
      
      if (ptrnpfInsertList != null && !ptrnpfInsertList.isEmpty())
      {
    	  ptrnpfDAO.insertPtrnPF(ptrnpfInsertList);
      }
  }
     

  private Ptrnpf getPtrnpf() {
      Ptrnpf p = new Ptrnpf();
      p.setUserT(0);
      p.setPtrneff(wsaaTodayDate.toInt());
      p.setTrdt(varcom.vrcmDate.toInt());
      p.setBatccoy(chdrlnbIO.getChdrcoy().toString());
      p.setBatcactyr(rnlallrec.batcactyr.toInt());
      p.setBatcactmn(rnlallrec.batcactmn.toInt());
      p.setBatctrcde("BAEH");
      p.setBatcbatch(rnlallrec.batcbatch.toString());
      p.setBatcbrn(rnlallrec.batcbrn.toString());
      p.setTrtm(varcom.vrcmTime.toInt());
      p.setValidflag("");
      return p;
}

  private void processUnit()
  {
	  
	   rnlallrec.covrInstprem.set(rnlallrec.totrecd);
	   rnlallrec.totrecd.set(ZERO);		
	   rnlallrec.tranno.set(add(chdrlnbIO.getTranno(),1).toInt());
	   callProgram(Fpunital.class, rnlallrec.rnlallRec);
		if (isNE(rnlallrec.statuz, varcom.oK)) {
			syserrrec.set(rnlallrec.rnlallRec);
			syserrrec.params.set(wsaaSubr);
			syserrrec.statuz.set(rnlallrec.statuz);
			systemError8000();
		}
  }
  
protected void db8100()
{
	if (isEQ(syserrrec.statuz,varcom.bomb)) {
		return ;
	}
	syserrrec.subrname.set(wsaaSubr);
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("1");
	callProgram(Syserr.class, syserrrec.syserrRec);
}

protected void dbExit8190()
{
	rnlallrec.statuz.set(varcom.bomb);
	exit090();
}

protected void systemError8000()
{
		se8000();
		seExit8090();
	}

protected void se8000()
{
	if (isEQ(syserrrec.statuz,varcom.bomb)) {
		return ;
	}
	syserrrec.subrname.set(wsaaSubr);
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
}

protected void seExit8090()
{
	rnlallrec.statuz.set(varcom.bomb);
	exit090();
}


 /*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
private static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldAllocAccum = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldAllocInit = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaUtrnContractAmount = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhanceAmt = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhPerFund = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaHitrContractAmount = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaTotUlSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaTotIbSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaInitAmount = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAccumAmount = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAllocTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunTot = new ZonedDecimalData(18, 2).init(0);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e615 = new FixedLengthStringData(4).init("E615");
	private FixedLengthStringData e616 = new FixedLengthStringData(4).init("E616");
	private FixedLengthStringData e706 = new FixedLengthStringData(4).init("E706");
	private FixedLengthStringData e707 = new FixedLengthStringData(4).init("E707");
	private FixedLengthStringData g027 = new FixedLengthStringData(4).init("G027");
	private FixedLengthStringData g029 = new FixedLengthStringData(4).init("G029");
	private FixedLengthStringData g032 = new FixedLengthStringData(4).init("G032");
	private FixedLengthStringData g033 = new FixedLengthStringData(4).init("G033");
	private FixedLengthStringData g513 = new FixedLengthStringData(4).init("G513");
	private FixedLengthStringData h053 = new FixedLengthStringData(4).init("H053");
	private FixedLengthStringData h055 = new FixedLengthStringData(4).init("H055");
	private FixedLengthStringData h115 = new FixedLengthStringData(4).init("H115");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData ulnkrec = new FixedLengthStringData(10).init("ULNKREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData ulnkrnlrec = new FixedLengthStringData(10).init("ULNKRNLREC");
	private FixedLengthStringData unltunlrec = new FixedLengthStringData(10).init("UNLTUNLREC");
	private FixedLengthStringData incirec = new FixedLengthStringData(10).init("INCIREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitdrec = new FixedLengthStringData(10).init("HITDREC");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).init("HITRREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).init("ZRSTREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5544 = new FixedLengthStringData(5).init("T5544");
	private FixedLengthStringData t5519 = new FixedLengthStringData(5).init("T5519");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t5537 = new FixedLengthStringData(5).init("T5537");
	private FixedLengthStringData t5535 = new FixedLengthStringData(5).init("T5535");
	private FixedLengthStringData t5536 = new FixedLengthStringData(5).init("T5536");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5539 = new FixedLengthStringData(5).init("T5539");
	private FixedLengthStringData t6646 = new FixedLengthStringData(5).init("T6646");
	private FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData th510 = new FixedLengthStringData(5).init("TH510");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
}
}
