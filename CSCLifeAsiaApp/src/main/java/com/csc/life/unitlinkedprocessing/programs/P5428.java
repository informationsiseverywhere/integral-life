/*
 * File: P5428.java
 * Date: 30 August 2009 0:25:04
 * Author: Quipoz Limited
 * 
 * Class transformed from P5428.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.unitlinkedprocessing.dataaccess.VprcupdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupeTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S5428ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P5428 - Modify Virtual Fund Bid/Offer Prices.
*
*  This program all the user to modify the calculated Bid/Offer
*  prices.
*
*  It validates and saves any changes made to the
*  calculated bid/offer prices for a virtual fund. It then
*  writes the new data to the VPRC subfile.
*
*  The changes will be checked against the tolerance level in
*  table T5515. If changes are outside the tolerance level, a
*  warning message is displayed. The user can override the
*  tolerance by pressing ENTER to accept the changes or CF1 to
*  abandon the changes.
*
*  Matching also available by entering 1, 2 ,3  or 4
*  characters. Consequently the program will display the fund
*  that matches these characters as the first fund in the
*  subfile.
*
*  It is called from the Unit Linked Entry submenu(P5412) by
*  means of selecting the appropriate option.
*
*  The program flow is as follows:-
*
*      Initialisation.
*
*      Read VPRCUPD and load subfile.
*      Display screen and read screen.
*
*      Validate changes until there is no error.
*
*      If no more error
*          Check the changes against the tolerence level
*          If outside tolerance
*              Display message and protect screen.
*
*      If no error and ENTER is pressed
*          Accept the modifications.
*
*      Update VPRCUPD.
*
*      Return control to calling program.
*
*
*****************************************************************
* </pre>
*/
public class P5428 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5428");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private int wsaaSubfileSize = 15;
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaNormError = "";
		/* WSAA-PREV-KEY */
	private FixedLengthStringData wsaaPrevType = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrevFund = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaRecObtainedFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaRecObtained = new Validator(wsaaRecObtainedFlag, "Y");

	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Validator wsaaRolu = new Validator(wsaaFunctionKey, "ROLU");
	private ZonedDecimalData wsaaTdayIntDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaTdayExtDate = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaPartFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPartF1 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 0);
	private FixedLengthStringData wsaaPartF2 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 1);
	private FixedLengthStringData wsaaPartF3 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 2);
	private FixedLengthStringData wsaaPartF4 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 3);

	private FixedLengthStringData wsaaItemFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaItemF1 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 0);
	private FixedLengthStringData wsaaItemF2 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 1);
	private FixedLengthStringData wsaaItemF3 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 2);
	private FixedLengthStringData wsaaItemF4 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 3);
	private FixedLengthStringData wsaaSearchFund = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaTolerance = new ZonedDecimalData(8, 5).setUnsigned();

	private FixedLengthStringData wsaaFirstFlags = new FixedLengthStringData(100);
	private FixedLengthStringData[] wsaaFirsts = FLSArrayPartOfStructure(100, 1, wsaaFirstFlags, 0);
	private ZonedDecimalData[] wsaaFirst = ZDArrayPartOfArrayStructure(1, 0, wsaaFirsts, 0, UNSIGNED_TRUE);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).setUnsigned();
		/* ERRORS */
	private String f351 = "F351";
	private String g037 = "G037";
	private String h025 = "H025";
	private String h094 = "H094";
	private String h095 = "H095";
	//ILIFE-1340 Start by vchawda
	private String t1688 = "T1688";
	private Batckey wsaaBatckey = new Batckey();
	private DescTableDAM descIO = new DescTableDAM();
	private String descrec = "DESCREC";
		/* FORMATS */
	private String vprcupdrec = "VPRCUPDREC";
	private String t5515 = "T5515";
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5515rec t5515rec = new T5515rec();
		/*Unit Price file*/
	private VprcupdTableDAM vprcupdIO = new VprcupdTableDAM();
		/*Unit Price File (For checking tolerance)*/
	private VprcupeTableDAM vprcupeIO = new VprcupeTableDAM();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S5428ScreenVars sv = ScreenProgram.getScreenVars( S5428ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		writeToSubfile1300, 
		nextRecord1400, 
		setMoreSign1450, 
		preExit, 
		validateSubfile2100, 
		updateDate2300, 
		updateSubfile2400, 
		rollUp2600, 
		writeToSubfile2700, 
		nextRecord2800, 
		setMoreSign2700, 
		exit2090, 
		moreUpdateRec3200, 
		exit3900, 
		callVprcupd5100, 
		exit5190, 
		exit6090, 
		init7300, 
		exit7900, 
		exit8900, 
		checkInitUnit14200, 
		exit14900
	}

	public P5428() {
		super();
		screenVars = sv;
		new ScreenModel("S5428", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1100();
					loadSubfile1200();
				}
				case writeToSubfile1300: {
					writeToSubfile1300();
				}
				case nextRecord1400: {
					nextRecord1400();
				}
				case setMoreSign1450: {
					setMoreSign1450();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaNormError = "Y";
		wsaaIndex.set(0);
		while ( !(isGT(wsaaIndex,99))) {
			initialiseTable9000();
		}
		
		sv.xtranno.set(0);
		sv.initBarePrice.set(0);
		sv.initBidPrice.set(0);
		sv.initOfferPrice.set(0);
		sv.accBarePrice.set(0);
		sv.accBidPrice.set(0);
		sv.accOfferPrice.set(0);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTdayIntDate.set(datcon1rec.intDate);
		sv.company.set(wsspcomn.company);
		sv.effdate.set(wsspuprc.uprcEffdate);
		sv.jobno.set(wsspuprc.uprcJobno);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5428", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		getTranscodeDesc1110();
	}
protected void getTranscodeDesc1110()
{
	wsaaBatckey.batcKey.set(wsspcomn.batchkey);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(wsspcomn.company);
	descIO.setDesctabl(t1688);
	descIO.setDescitem(wsaaBatckey.batcBatctrcde);
	descIO.setLanguage(wsspcomn.language);
	descIO.setFormat(descrec);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
	}
	if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
		sv.shortdesc.set("?");
	}
	else {
		sv.shortdesc.set(descIO.getShortdesc());
	}
	sv.flag.setEnabled(BaseScreenData.ENABLED);
	if(isEQ(wsaaBatckey.batcBatctrcde,"T511")){
	sv.flag.setEnabled(BaseScreenData.DISABLED);
	}
}
//ILIFE-1340 End

protected void loadSubfile1200()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		//performance improvement -- < Niharika Modi >
		//vprcupdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//vprcupdIO.setFitKeysSearch("COMPANY","EFFDATE","JOBNO");
		
		if (isNE(wsspuprc.uprcVrtfnd,SPACES)) {
			wsaaPartFund.set(wsspuprc.uprcVrtfnd);
			wsaaSearchFund.set("Y");
		}
		else {
			wsaaSearchFund.set("N");
		}
		getNextVprcupdRec5100();
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1450);
		}
		if (isNE(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.nextRecord1400);
		}
		if (isEQ(wsaaSearchFund,"Y")) {
			searchFund8000();
			if (isEQ(wsaaSearchFund,"Y")) {
				goTo(GotoLabel.nextRecord1400);
			}
		}
	}

protected void writeToSubfile1300()
	{
		wsaaRecObtainedFlag.set("N");
		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5428", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void nextRecord1400()
	{
		if (!wsaaRecObtained.isTrue()) {
			vprcupdIO.setFunction(varcom.nextr);
			getNextVprcupdRec5100();
		}
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1450);
		}
		if (isNE(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.nextRecord1400);
		}
		if (isEQ(wsaaSearchFund,"Y")) {
			searchFund8000();
			if (isEQ(wsaaSearchFund,"Y")) {
				goTo(GotoLabel.nextRecord1400);
			}
		}
		if (isEQ(scrnparams.subfileRrn,wsaaSubfileSize)) {
			wsaaRem.set(0);
			goTo(GotoLabel.setMoreSign1450);
		}
		goTo(GotoLabel.writeToSubfile1300);
	}

protected void setMoreSign1450()
	{
		if (isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		wsaaFunctionKey.set(SPACES);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if ((isEQ(wsaaNormError,"N"))
		&& (isEQ(wsspcomn.edterror,"Y"))
		&& (!wsaaRolu.isTrue())) {
			scrnparams.errorCode.set(h094);
			scrnparams.function.set(varcom.prot);
		}
		wsaaIndex.set(0);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case validateSubfile2100: {
					validateSubfile2100();
				}
				case updateDate2300: {
					updateDate2300();
				}
				case updateSubfile2400: {
					updateSubfile2400();
				}
				case rollUp2600: {
					rollUp2600();
				}
				case writeToSubfile2700: {
					writeToSubfile2700();
				}
				case nextRecord2800: {
					nextRecord2800();
				}
				case setMoreSign2700: {
					setMoreSign2700();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaNormError = "N";
		wsaaFunctionKey.set(scrnparams.statuz);
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			goTo(GotoLabel.rollUp2600);
		}
		scrnparams.function.set(varcom.sstrt);
	}

protected void validateSubfile2100()
	{
		processScreen("S5428", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.rollUp2600);
		}
		wsaaIndex.add(1);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	  
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(sv.unitVirtualFund,itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.unitType,"I")) {
			if ((isNE(sv.accBidPrice,ZERO)
			|| isNE(sv.accOfferPrice,ZERO))) {
				sv.abidprErr.set(h025);
				sv.aoffprErr.set(h025);
				wsaaNormError = "Y";
				wsspcomn.edterror.set("Y");
			}
		}
		else {
			if (isEQ(t5515rec.unitType,"A")) {
				if ((isNE(sv.initBidPrice,ZERO)
				|| isNE(sv.initOfferPrice,ZERO))) {
					sv.ibidprErr.set(h025);
					sv.ioffprErr.set(h025);
					wsaaNormError = "Y";
					wsspcomn.edterror.set("Y");
				}
			}
			else {
				if ((isEQ(sv.accBidPrice,ZERO)
				|| isEQ(sv.accOfferPrice,ZERO))
				&& (isNE(sv.initBidPrice,ZERO)
				|| isNE(sv.initOfferPrice,ZERO))) {
					sv.abidprErr.set(f351);
					sv.ibidprErr.set(f351);
					sv.aoffprErr.set(f351);
					sv.ioffprErr.set(f351);
					wsaaNormError = "Y";
					wsspcomn.edterror.set("Y");
				}
			}
		}
		if (isNE(sv.ioffprErr,SPACES)) {
			goTo(GotoLabel.updateSubfile2400);
		}
		if (isNE(wsaaNormError,"Y")) {
			if (isEQ(wsaaFirsts[wsaaIndex.toInt()],0)) {
				checkTolerances14000();
				wsaaFirsts[wsaaIndex.toInt()].set(1);
			}
		}
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupdIO.setUnitType("A");
		vprcupdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isEQ(vprcupdIO.getStatuz(),varcom.oK))
		&& (isEQ(vprcupdIO.getUnitBidPrice(),sv.accBidPrice))
		&& (isEQ(vprcupdIO.getUnitOfferPrice(),sv.accOfferPrice))) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.updateDate2300);
		}
		vprcupdIO.setUnitType("I");
		vprcupdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isEQ(vprcupdIO.getStatuz(),varcom.oK))
		&& (isEQ(vprcupdIO.getUnitBidPrice(),sv.initBidPrice))
		&& (isEQ(vprcupdIO.getUnitOfferPrice(),sv.initOfferPrice))) {
			goTo(GotoLabel.updateSubfile2400);
		}
	}

protected void updateDate2300()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(wsaaTdayIntDate);
		datcon1rec.function.set("CONU");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.update.set(datcon1rec.extDate);
	}

protected void updateSubfile2400()
	{
		sv.xtranno.add(1);
		scrnparams.function.set(varcom.supd);
		processScreen("S5428", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.validateSubfile2100);
	}

protected void rollUp2600()
	{
		if (!wsaaRolu.isTrue()) {
			if (isEQ(wsaaNormError,"Y")) {
				wsaaIndex.set(0);
				while ( !(isGT(wsaaIndex,99))) {
					initialiseTable9000();
				}
				
				goTo(GotoLabel.exit2090);
			}
			else {
				goTo(GotoLabel.exit2090);
			}
		}
	}

protected void writeToSubfile2700()
	{
		if (isEQ(scrnparams.function,varcom.prot)) {
			goTo(GotoLabel.exit2090);
		}
		wsaaRecObtainedFlag.set("N");
		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		callScreenIo5000();
		wsaaRem.add(1);
	}

protected void nextRecord2800()
	{
		if (!wsaaRecObtained.isTrue()) {
			vprcupdIO.setFunction(varcom.nextr);
			getNextVprcupdRec5100();
		}
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign2700);
		}
		if (isNE(vprcupdIO.getProcflag(),"Y")) {
			goTo(GotoLabel.nextRecord2800);
		}
		if (isGTE(wsaaRem,wsaaSubfileSize)) {
			goTo(GotoLabel.setMoreSign2700);
		}
		goTo(GotoLabel.writeToSubfile2700);
	}

protected void setMoreSign2700()
	{
		if (isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		wsspcomn.edterror.set("Y");
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateDatabase3100();
				}
				case moreUpdateRec3200: {
					moreUpdateRec3200();
				}
				case exit3900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3100()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void moreUpdateRec3200()
	{
		processScreen("S5428", sv);
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		writeToVprcupdFile7000();
		goTo(GotoLabel.moreUpdateRec3200);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callScreenIo5000()
	{
		/*PARA*/
		processScreen("S5428", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void getNextVprcupdRec5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case callVprcupd5100: {
					callVprcupd5100();
				}
				case exit5190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callVprcupd5100()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit5190);
		}
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate))
		|| (isNE(vprcupdIO.getCompany(),wsspcomn.company))
		|| (isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno))
		|| (isNE(vprcupdIO.getProcflag(),"Y"))) {
			vprcupdIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callVprcupd5100);
		}
	}

protected void moveFieldsToSubfile6000()
	{
		try {
			para6000();
		}
		catch (GOTOException e){
		}
	}

protected void para6000()
	{
		if (isNE(vprcupdIO.getUnitType(),"A")
		&& isNE(vprcupdIO.getUnitType(),"I")) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		wsaaPrevFund.set(vprcupdIO.getUnitVirtualFund());
		sv.unitVirtualFund.set(vprcupdIO.getUnitVirtualFund());
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(vprcupdIO.getUpdateDate());
		datcon1rec.function.set("CONU");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.update.set(datcon1rec.extDate);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(vprcupdIO.getUnitVirtualFund());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(vprcupdIO.getUnitVirtualFund(),itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.unitType,"A")) {
			sv.ibidprOut[varcom.pr.toInt()].set("Y");
			sv.ioffprOut[varcom.pr.toInt()].set("Y");
			sv.abidprOut[varcom.pr.toInt()].set(SPACES);
			sv.aoffprOut[varcom.pr.toInt()].set(SPACES);
			sv.abreprOut[varcom.pr.toInt()].set(SPACES);
		}
		if (isEQ(t5515rec.unitType,"I")) {
			sv.abidprOut[varcom.pr.toInt()].set("Y");
			sv.aoffprOut[varcom.pr.toInt()].set("Y");
			sv.ibidprOut[varcom.pr.toInt()].set(SPACES);
			sv.ioffprOut[varcom.pr.toInt()].set(SPACES);
			sv.ibreprOut[varcom.pr.toInt()].set(SPACES);
		}
		if (isEQ(t5515rec.unitType,"B")) {
			sv.abidprOut[varcom.pr.toInt()].set(SPACES);
			sv.aoffprOut[varcom.pr.toInt()].set(SPACES);
			sv.ibidprOut[varcom.pr.toInt()].set(SPACES);
			sv.ioffprOut[varcom.pr.toInt()].set(SPACES);
			sv.ibreprOut[varcom.pr.toInt()].set(SPACES);
		}
		sv.xtranno.set(vprcupdIO.getTranno());
		if (isEQ(vprcupdIO.getUnitType(),"I")) {
			sv.accBarePrice.set(ZERO);
			sv.accBidPrice.set(ZERO);
			sv.accOfferPrice.set(ZERO);
			sv.initBarePrice.set(vprcupdIO.getUnitBarePrice());
			sv.initBidPrice.set(vprcupdIO.getUnitBidPrice());
			sv.initOfferPrice.set(vprcupdIO.getUnitOfferPrice());
			goTo(GotoLabel.exit6090);
		}
		else {
			if (isEQ(vprcupdIO.getUnitType(),"A")) {
				sv.accBarePrice.set(vprcupdIO.getUnitBarePrice());
				sv.accBidPrice.set(vprcupdIO.getUnitBidPrice());
				sv.accOfferPrice.set(vprcupdIO.getUnitOfferPrice());
			}
		}
		vprcupdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate))
		|| (isNE(vprcupdIO.getCompany(),wsspcomn.company))
		|| (isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno))) {
			goTo(GotoLabel.exit6090);
		}
		if ((isNE(vprcupdIO.getUnitType(),"A"))
		&& (isNE(vprcupdIO.getUnitType(),"I"))) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isEQ(vprcupdIO.getUnitVirtualFund(),wsaaPrevFund))
		&& (isEQ(vprcupdIO.getUnitType(),"I"))) {
			sv.initBarePrice.set(vprcupdIO.getUnitBarePrice());
			sv.initBidPrice.set(vprcupdIO.getUnitBidPrice());
			sv.initOfferPrice.set(vprcupdIO.getUnitOfferPrice());
		}
		else {
			sv.initBarePrice.set(ZERO);
			sv.initBidPrice.set(ZERO);
			sv.initOfferPrice.set(ZERO);
			wsaaRecObtainedFlag.set("Y");
		}
	}

protected void writeToVprcupdFile7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para7000();
				}
				case init7300: {
					init7300();
				}
				case exit7900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para7000()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.readh);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupdIO.setUnitType("A");
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupdIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.init7300);
		}
		if (isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| isNE(vprcupdIO.getUnitType(),"A")
		|| isNE(vprcupdIO.getUnitVirtualFund(),sv.unitVirtualFund)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isEQ(sv.accBidPrice,0))
		&& (isEQ(sv.accOfferPrice,0))) {
			vprcupdIO.setFunction(varcom.delet);
		}
		else {
			vprcupdIO.setFunction(varcom.rewrt);
		}
		vprcupdIO.setUnitBidPrice(sv.accBidPrice);
		vprcupdIO.setUnitOfferPrice(sv.accOfferPrice);
		vprcupdIO.setTranno(sv.xtranno);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.extDate.set(sv.update);
		datcon1rec.function.set(varcom.edit);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		vprcupdIO.setUpdateDate(datcon1rec.intDate);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			fatalError600();
		}
	}

protected void init7300()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.readh);
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
		vprcupdIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupdIO.setUnitType("I");
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)
		&& isNE(vprcupdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupdIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit7900);
		}
		if (isNE(vprcupdIO.getEffdate(),wsspuprc.uprcEffdate)
		|| isNE(vprcupdIO.getCompany(),wsspcomn.company)
		|| isNE(vprcupdIO.getJobno(),wsspuprc.uprcJobno)
		|| isNE(vprcupdIO.getUnitVirtualFund(),sv.unitVirtualFund)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isEQ(sv.initBidPrice,0))
		&& (isEQ(sv.initOfferPrice,0))) {
			vprcupdIO.setFunction(varcom.delet);
		}
		else {
			vprcupdIO.setFunction(varcom.rewrt);
		}
		vprcupdIO.setUnitBidPrice(sv.initBidPrice);
		vprcupdIO.setUnitOfferPrice(sv.initOfferPrice);
		vprcupdIO.setTranno(sv.xtranno);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.extDate.set(sv.update);
		datcon1rec.function.set(varcom.edit);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		vprcupdIO.setUpdateDate(datcon1rec.intDate);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			fatalError600();
		}
	}

protected void searchFund8000()
	{
		try {
			para8010();
		}
		catch (GOTOException e){
		}
	}

protected void para8010()
	{
		wsaaItemFund.set(vprcupdIO.getUnitVirtualFund());
		if (isNE(wsaaPartF1,wsaaItemF1)) {
			if (isLT(wsaaPartF1,wsaaItemF1)) {
				/*NEXT_SENTENCE*/
			}
			else {
				goTo(GotoLabel.exit8900);
			}
		}
		else {
			if (isNE(wsaaPartF2,SPACES)) {
				if (isNE(wsaaPartF2,wsaaItemF2)) {
					if (isLT(wsaaPartF2,wsaaItemF2)) {
						/*NEXT_SENTENCE*/
					}
					else {
						goTo(GotoLabel.exit8900);
					}
				}
				else {
					if (isNE(wsaaPartF3,SPACES)) {
						if (isNE(wsaaPartF3,wsaaItemF3)) {
							if (isLT(wsaaPartF3,wsaaItemF3)) {
								/*NEXT_SENTENCE*/
							}
							else {
								goTo(GotoLabel.exit8900);
							}
						}
						else {
							if (isNE(wsaaPartF4,SPACES)) {
								if (isNE(wsaaPartF4,wsaaItemF4)) {
									if (isLT(wsaaPartF4,wsaaItemF4)) {
										/*NEXT_SENTENCE*/
									}
									else {
										goTo(GotoLabel.exit8900);
									}
								}
							}
						}
					}
				}
			}
		}
		wsaaSearchFund.set("N");
	}

protected void initialiseTable9000()
	{
		/*REPEAT*/
		wsaaIndex.add(1);
		wsaaFirsts[wsaaIndex.toInt()].set(0);
		/*EXIT*/
	}

protected void checkTolerances14000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start14100();
					checkAccUnit14100();
				}
				case checkInitUnit14200: {
					checkInitUnit14200();
				}
				case exit14900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start14100()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(sv.unitVirtualFund,itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.tolerance,0)) {
			goTo(GotoLabel.exit14900);
		}
		vprcupeIO.setCompany(wsspcomn.company);
		vprcupeIO.setValidflag("1");
		vprcupeIO.setEffdate(varcom.vrcmMaxDate);
		vprcupeIO.setJobno(99999999);
		vprcupeIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupeIO.setUnitType("A");
		vprcupeIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.checkInitUnit14200);
		}
	}

protected void checkAccUnit14100()
	{
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBidPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.accBidPrice, 5)
		&& isGT(sv.accBidPrice,(add(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))
		|| (setPrecision(sv.accBidPrice, 5)
		&& isLT(sv.accBidPrice,(sub(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))) {
			sv.abidprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitOfferPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.accOfferPrice, 5)
		&& isGT(sv.accOfferPrice,(add(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))
		|| (setPrecision(sv.accOfferPrice, 5)
		&& isLT(sv.accOfferPrice,(sub(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))) {
			sv.aoffprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
		vprcupeIO.setUnitType("I");
		vprcupeIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit14900);
		}
	}

protected void checkInitUnit14200()
	{
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBidPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.initBidPrice, 5)
		&& isGT(sv.initBidPrice,(add(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))
		|| (setPrecision(sv.initBidPrice, 5)
		&& isLT(sv.initBidPrice,(sub(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))) {
			sv.ibidprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitOfferPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.initOfferPrice, 5)
		&& isGT(sv.initOfferPrice,(add(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))
		|| (setPrecision(sv.initOfferPrice, 5)
		&& isLT(sv.initOfferPrice,(sub(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))) {
			sv.ioffprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
	}
}
