package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;

public class Zrstpf {
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer seqno;
	private Integer tranno;
	private Integer xtranno;
	private String batctrcde;
	private String sacstyp;
	private BigDecimal zramount01;
	private BigDecimal zramount02;
	private Integer trandate;
	private Integer ustmno;
	private String fdbkind;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
 //ILIFE-8786 start	
public Zrstpf() {
	super();
}
	
	
public Zrstpf(Zrstpf zrstpf) {
	this.chdrcoy=zrstpf.chdrcoy;
	this.chdrnum=zrstpf.chdrnum;
	this.life=zrstpf.life;
	this.jlife=zrstpf.jlife;
	this.coverage=zrstpf.coverage;
	this.rider=zrstpf.rider;
	this.seqno=zrstpf.seqno;
	this.tranno=zrstpf.tranno;
	this.xtranno=zrstpf.xtranno;
	this.batctrcde=zrstpf.batctrcde;
	this.sacstyp=zrstpf.sacstyp;
	this.zramount01=zrstpf.zramount01;
	this.zramount02=zrstpf.zramount02;
	this.trandate=zrstpf.trandate;
	this.ustmno=zrstpf.ustmno;
	this.fdbkind=zrstpf.fdbkind;
	this.usrprf=zrstpf.usrprf;
	this.jobnm=zrstpf.jobnm;
	this.datime=zrstpf.datime;
	
}
	 //ILIFE-8786 end
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getSeqno() {
		return seqno == null ? 0 : seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public int getTranno() {
		return tranno == null ? 0 : tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getXtranno() {
		return xtranno == null ? 0 : xtranno;
	}
	public void setXtranno(int xtranno) {
		this.xtranno = xtranno;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public BigDecimal getZramount01() {
		return zramount01;
	}
	public void setZramount01(BigDecimal zramount01) {
		this.zramount01 = zramount01;
	}
	public BigDecimal getZramount02() {
		return zramount02;
	}
	public void setZramount02(BigDecimal zramount02) {
		this.zramount02 = zramount02;
	}
	public int getTrandate() {
		return trandate == null ? 0 : trandate;
	}
	public void setTrandate(int trandate) {
		this.trandate = trandate;
	}
	public int getUstmno() {
		return ustmno == null ? 0 : ustmno;
	}
	public void setUstmno(int ustmno) {
		this.ustmno = ustmno;
	}
	public String getFeedbackInd() {
		return fdbkind;
	}
	public void setFeedbackInd(String feedbackInd) {
		this.fdbkind = feedbackInd;
	}
	public String getUserProfile() {
		return usrprf;
	}
	public void setUserProfile(String userProfile) {
		this.usrprf = userProfile;
	}
	public String getJobName() {
		return jobnm;
	}
	public void setJobName(String jobName) {
		this.jobnm = jobName;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
}