/*
 * File: T5514pt.java
 * Date: 30 August 2009 2:21:16
 * Author: Quipoz Limited
 * 
 * Class transformed from T5514PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5514rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5514.
*
*
*****************************************************************
* </pre>
*/
public class T5514pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine001, 32, FILLER).init("Virtual Fund - Create                  S5514");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(61);
	private FixedLengthStringData filler7 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Virtual Fund:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 17);
	private FixedLengthStringData filler8 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine003, 31);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(27);
	private FixedLengthStringData filler9 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  From Date:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 17);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(50);
	private FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine005, 24, FILLER).init("Real Fund Code           %");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(53);
	private FixedLengthStringData filler12 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 29);
	private FixedLengthStringData filler13 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(53);
	private FixedLengthStringData filler14 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 29);
	private FixedLengthStringData filler15 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(53);
	private FixedLengthStringData filler16 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 29);
	private FixedLengthStringData filler17 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(53);
	private FixedLengthStringData filler18 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 29);
	private FixedLengthStringData filler19 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(53);
	private FixedLengthStringData filler20 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 29);
	private FixedLengthStringData filler21 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(53);
	private FixedLengthStringData filler22 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 29);
	private FixedLengthStringData filler23 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine011, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(53);
	private FixedLengthStringData filler24 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 29);
	private FixedLengthStringData filler25 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(53);
	private FixedLengthStringData filler26 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 29);
	private FixedLengthStringData filler27 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(53);
	private FixedLengthStringData filler28 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 29);
	private FixedLengthStringData filler29 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine014, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(53);
	private FixedLengthStringData filler30 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 29);
	private FixedLengthStringData filler31 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine015, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 47).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(53);
	private FixedLengthStringData filler32 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 29);
	private FixedLengthStringData filler33 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine016, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 47).setPattern("ZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5514rec t5514rec = new T5514rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5514pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5514rec.t5514Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t5514rec.unitVirtualFund);
		fieldNo006.set(t5514rec.vfundesc);
		datcon1rec.intDate.set(t5514rec.fromdate);
		callDatcon1100();
		fieldNo007.set(datcon1rec.extDate);
		fieldNo008.set(t5514rec.fcode01);
		fieldNo009.set(t5514rec.fperc01);
		fieldNo010.set(t5514rec.fcode02);
		fieldNo011.set(t5514rec.fperc02);
		fieldNo012.set(t5514rec.fcode03);
		fieldNo013.set(t5514rec.fperc03);
		fieldNo014.set(t5514rec.fcode04);
		fieldNo015.set(t5514rec.fperc04);
		fieldNo016.set(t5514rec.fcode05);
		fieldNo017.set(t5514rec.fperc05);
		fieldNo018.set(t5514rec.fcode06);
		fieldNo019.set(t5514rec.fperc06);
		fieldNo020.set(t5514rec.fcode07);
		fieldNo021.set(t5514rec.fperc07);
		fieldNo022.set(t5514rec.fcode08);
		fieldNo023.set(t5514rec.fperc08);
		fieldNo024.set(t5514rec.fcode09);
		fieldNo025.set(t5514rec.fperc09);
		fieldNo026.set(t5514rec.fcode10);
		fieldNo027.set(t5514rec.fperc10);
		fieldNo028.set(t5514rec.fcode11);
		fieldNo029.set(t5514rec.fperc11);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
