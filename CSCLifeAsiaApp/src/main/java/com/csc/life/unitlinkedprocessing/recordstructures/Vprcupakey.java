package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:06
 * Description:
 * Copybook name: VPRCUPAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprcupakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprcupaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprcupaKey = new FixedLengthStringData(256).isAPartOf(vprcupaFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprcupaCompany = new FixedLengthStringData(1).isAPartOf(vprcupaKey, 0);
  	public FixedLengthStringData vprcupaUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprcupaKey, 1);
  	public PackedDecimalData vprcupaEffdate = new PackedDecimalData(8, 0).isAPartOf(vprcupaKey, 5);
  	public PackedDecimalData vprcupaJobno = new PackedDecimalData(8, 0).isAPartOf(vprcupaKey, 10);
  	public FixedLengthStringData vprcupaUnitType = new FixedLengthStringData(1).isAPartOf(vprcupaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(vprcupaKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprcupaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprcupaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}