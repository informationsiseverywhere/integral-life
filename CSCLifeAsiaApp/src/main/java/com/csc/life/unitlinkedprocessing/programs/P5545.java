/*
 * File: P5545.java
 * Date: 30 August 2009 0:30:42
 * Author: Quipoz Limited
 * 
 * Class transformed from P5545.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.procedures.T5545pt;
import com.csc.life.unitlinkedprocessing.screens.S5545ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* DATED DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-3.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FIELD
* MOVES TO AVOID CONVERSION OF COMP-3 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
*
*****************************************************************
* </pre>
*/
public class P5545 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5545");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5545rec t5545rec = new T5545rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5545ScreenVars sv = ScreenProgram.getScreenVars( S5545ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5545() {
		super();
		screenVars = sv;
		new ScreenModel("S5545", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5545rec.t5545Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5545rec.enhanaPc01.set(ZERO);
		t5545rec.enhanaPc02.set(ZERO);
		t5545rec.enhanaPc03.set(ZERO);
		t5545rec.enhanaPc04.set(ZERO);
		t5545rec.enhanaPc05.set(ZERO);
		t5545rec.enhanaPrm01.set(ZERO);
		t5545rec.enhanaPrm02.set(ZERO);
		t5545rec.enhanaPrm03.set(ZERO);
		t5545rec.enhanaPrm04.set(ZERO);
		t5545rec.enhanaPrm05.set(ZERO);
		t5545rec.enhanbPc01.set(ZERO);
		t5545rec.enhanbPc02.set(ZERO);
		t5545rec.enhanbPc03.set(ZERO);
		t5545rec.enhanbPc04.set(ZERO);
		t5545rec.enhanbPc05.set(ZERO);
		t5545rec.enhanbPrm01.set(ZERO);
		t5545rec.enhanbPrm02.set(ZERO);
		t5545rec.enhanbPrm03.set(ZERO);
		t5545rec.enhanbPrm04.set(ZERO);
		t5545rec.enhanbPrm05.set(ZERO);
		t5545rec.enhancPc01.set(ZERO);
		t5545rec.enhancPc02.set(ZERO);
		t5545rec.enhancPc03.set(ZERO);
		t5545rec.enhancPc04.set(ZERO);
		t5545rec.enhancPc05.set(ZERO);
		t5545rec.enhancPrm01.set(ZERO);
		t5545rec.enhancPrm02.set(ZERO);
		t5545rec.enhancPrm03.set(ZERO);
		t5545rec.enhancPrm04.set(ZERO);
		t5545rec.enhancPrm05.set(ZERO);
		t5545rec.enhandPc01.set(ZERO);
		t5545rec.enhandPc02.set(ZERO);
		t5545rec.enhandPc03.set(ZERO);
		t5545rec.enhandPc04.set(ZERO);
		t5545rec.enhandPc05.set(ZERO);
		t5545rec.enhandPrm01.set(ZERO);
		t5545rec.enhandPrm02.set(ZERO);
		t5545rec.enhandPrm03.set(ZERO);
		t5545rec.enhandPrm04.set(ZERO);
		t5545rec.enhandPrm05.set(ZERO);
		t5545rec.enhanePc01.set(ZERO);
		t5545rec.enhanePc02.set(ZERO);
		t5545rec.enhanePc03.set(ZERO);
		t5545rec.enhanePc04.set(ZERO);
		t5545rec.enhanePc05.set(ZERO);
		t5545rec.enhanePrm01.set(ZERO);
		t5545rec.enhanePrm02.set(ZERO);
		t5545rec.enhanePrm03.set(ZERO);
		t5545rec.enhanePrm04.set(ZERO);
		t5545rec.enhanePrm05.set(ZERO);
		t5545rec.enhanfPc01.set(ZERO);
		t5545rec.enhanfPc02.set(ZERO);
		t5545rec.enhanfPc03.set(ZERO);
		t5545rec.enhanfPc04.set(ZERO);
		t5545rec.enhanfPc05.set(ZERO);
		t5545rec.enhanfPrm01.set(ZERO);
		t5545rec.enhanfPrm02.set(ZERO);
		t5545rec.enhanfPrm03.set(ZERO);
		t5545rec.enhanfPrm04.set(ZERO);
		t5545rec.enhanfPrm05.set(ZERO);
	}

protected void generalArea1045()
	{
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
			moveBillfreq1600();
		}
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 5); loopVar2 += 1){
			moveEnhanPcs1700();
		}
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 5); loopVar3 += 1){
			moveEnhanPrms1800();
		}
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
	}

protected void moveBillfreq1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.billfreq[wsaaSub1.toInt()].set(t5545rec.billfreq[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void moveEnhanPcs1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.enhanaPc[wsaaSub1.toInt()].set(t5545rec.enhanaPc[wsaaSub1.toInt()]);
		sv.enhanbPc[wsaaSub1.toInt()].set(t5545rec.enhanbPc[wsaaSub1.toInt()]);
		sv.enhancPc[wsaaSub1.toInt()].set(t5545rec.enhancPc[wsaaSub1.toInt()]);
		sv.enhandPc[wsaaSub1.toInt()].set(t5545rec.enhandPc[wsaaSub1.toInt()]);
		sv.enhanePc[wsaaSub1.toInt()].set(t5545rec.enhanePc[wsaaSub1.toInt()]);
		sv.enhanfPc[wsaaSub1.toInt()].set(t5545rec.enhanfPc[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void moveEnhanPrms1800()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.enhanaPrm[wsaaSub1.toInt()].set(t5545rec.enhanaPrm[wsaaSub1.toInt()]);
		sv.enhanbPrm[wsaaSub1.toInt()].set(t5545rec.enhanbPrm[wsaaSub1.toInt()]);
		sv.enhancPrm[wsaaSub1.toInt()].set(t5545rec.enhancPrm[wsaaSub1.toInt()]);
		sv.enhandPrm[wsaaSub1.toInt()].set(t5545rec.enhandPrm[wsaaSub1.toInt()]);
		sv.enhanePrm[wsaaSub1.toInt()].set(t5545rec.enhanePrm[wsaaSub1.toInt()]);
		sv.enhanfPrm[wsaaSub1.toInt()].set(t5545rec.enhanfPrm[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5545rec.t5545Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		wsaaSub1.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 6); loopVar4 += 1){
			updateBillfreq3400();
		}
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 5); loopVar5 += 1){
			updateEnhanPcs3500();
		}
		wsaaSub1.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 5); loopVar6 += 1){
			updateEnhanPrms3600();
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
	}

protected void updateBillfreq3400()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.billfreq[wsaaSub1.toInt()],t5545rec.billfreq[wsaaSub1.toInt()])) {
			t5545rec.billfreq[wsaaSub1.toInt()].set(sv.billfreq[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateEnhanPcs3500()
	{
		para3500();
	}

protected void para3500()
	{
		wsaaSub1.add(1);
		if (isNE(sv.enhanaPc[wsaaSub1.toInt()],t5545rec.enhanaPc[wsaaSub1.toInt()])) {
			t5545rec.enhanaPc[wsaaSub1.toInt()].set(sv.enhanaPc[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhanbPc[wsaaSub1.toInt()],t5545rec.enhanbPc[wsaaSub1.toInt()])) {
			t5545rec.enhanbPc[wsaaSub1.toInt()].set(sv.enhanbPc[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhancPc[wsaaSub1.toInt()],t5545rec.enhancPc[wsaaSub1.toInt()])) {
			t5545rec.enhancPc[wsaaSub1.toInt()].set(sv.enhancPc[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhandPc[wsaaSub1.toInt()],t5545rec.enhandPc[wsaaSub1.toInt()])) {
			t5545rec.enhandPc[wsaaSub1.toInt()].set(sv.enhandPc[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhanePc[wsaaSub1.toInt()],t5545rec.enhanePc[wsaaSub1.toInt()])) {
			t5545rec.enhanePc[wsaaSub1.toInt()].set(sv.enhanePc[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhanfPc[wsaaSub1.toInt()],t5545rec.enhanfPc[wsaaSub1.toInt()])) {
			t5545rec.enhanfPc[wsaaSub1.toInt()].set(sv.enhanfPc[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
	}

protected void updateEnhanPrms3600()
	{
		para3600();
	}

protected void para3600()
	{
		wsaaSub1.add(1);
		if (isNE(sv.enhanaPrm[wsaaSub1.toInt()],t5545rec.enhanaPrm[wsaaSub1.toInt()])) {
			t5545rec.enhanaPrm[wsaaSub1.toInt()].set(sv.enhanaPrm[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhanbPrm[wsaaSub1.toInt()],t5545rec.enhanbPrm[wsaaSub1.toInt()])) {
			t5545rec.enhanbPrm[wsaaSub1.toInt()].set(sv.enhanbPrm[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhancPrm[wsaaSub1.toInt()],t5545rec.enhancPrm[wsaaSub1.toInt()])) {
			t5545rec.enhancPrm[wsaaSub1.toInt()].set(sv.enhancPrm[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhandPrm[wsaaSub1.toInt()],t5545rec.enhandPrm[wsaaSub1.toInt()])) {
			t5545rec.enhandPrm[wsaaSub1.toInt()].set(sv.enhandPrm[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhanePrm[wsaaSub1.toInt()],t5545rec.enhanePrm[wsaaSub1.toInt()])) {
			t5545rec.enhanePrm[wsaaSub1.toInt()].set(sv.enhanePrm[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.enhanfPrm[wsaaSub1.toInt()],t5545rec.enhanfPrm[wsaaSub1.toInt()])) {
			t5545rec.enhanfPrm[wsaaSub1.toInt()].set(sv.enhanfPrm[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5545pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
