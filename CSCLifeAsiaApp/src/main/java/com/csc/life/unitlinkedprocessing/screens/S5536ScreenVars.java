package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5536
 * @version 1.0 generated on 30/08/09 06:42
 * @author Quipoz
 */
public class S5536ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(2024); //1772
	public FixedLengthStringData dataFields = new FixedLengthStringData(664).isAPartOf(dataArea, 0); //412
	public FixedLengthStringData billfreqs = new FixedLengthStringData(12).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(6, 2, billfreqs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01 = DD.billfreq.copy().isAPartOf(filler,0);
	public FixedLengthStringData billfreq02 = DD.billfreq.copy().isAPartOf(filler,2);
	public FixedLengthStringData billfreq03 = DD.billfreq.copy().isAPartOf(filler,4);
	public FixedLengthStringData billfreq04 = DD.billfreq.copy().isAPartOf(filler,6);
	public FixedLengthStringData billfreq05 = DD.billfreq.copy().isAPartOf(filler,8);
	public FixedLengthStringData billfreq06 = DD.billfreq.copy().isAPartOf(filler,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,13);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,21);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,29);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData maxPeriodas = new FixedLengthStringData(28).isAPartOf(dataFields, 67);
	public ZonedDecimalData[] maxPerioda = ZDArrayPartOfStructure(7, 4, 0, maxPeriodas, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(maxPeriodas, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxPerioda01 = DD.maxprda.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData maxPerioda02 = DD.maxprda.copyToZonedDecimal().isAPartOf(filler1,4);
	public ZonedDecimalData maxPerioda03 = DD.maxprda.copyToZonedDecimal().isAPartOf(filler1,8);
	public ZonedDecimalData maxPerioda04 = DD.maxprda.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData maxPerioda05 = DD.maxprda.copyToZonedDecimal().isAPartOf(filler1,16);
	public ZonedDecimalData maxPerioda06 = DD.maxprda.copyToZonedDecimal().isAPartOf(filler1,20);
	public ZonedDecimalData maxPerioda07 = DD.maxprda.copyToZonedDecimal().isAPartOf(filler1,24);
	
	
	public FixedLengthStringData maxPeriodbs = new FixedLengthStringData(28).isAPartOf(dataFields, 95);   //83
	public ZonedDecimalData[] maxPeriodb = ZDArrayPartOfStructure(7, 4, 0, maxPeriodbs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(28).isAPartOf(maxPeriodbs, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxPeriodb01 = DD.maxprdb.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData maxPeriodb02 = DD.maxprdb.copyToZonedDecimal().isAPartOf(filler2,4);
	public ZonedDecimalData maxPeriodb03 = DD.maxprdb.copyToZonedDecimal().isAPartOf(filler2,8);
	public ZonedDecimalData maxPeriodb04 = DD.maxprdb.copyToZonedDecimal().isAPartOf(filler2,12);
	public ZonedDecimalData maxPeriodb05 = DD.maxprdb.copyToZonedDecimal().isAPartOf(filler2,16);
	public ZonedDecimalData maxPeriodb06 = DD.maxprdb.copyToZonedDecimal().isAPartOf(filler2,20);
	public ZonedDecimalData maxPeriodb07 = DD.maxprdb.copyToZonedDecimal().isAPartOf(filler2,24);
	
	public FixedLengthStringData maxPeriodcs = new FixedLengthStringData(28).isAPartOf(dataFields, 123); //99
	public ZonedDecimalData[] maxPeriodc = ZDArrayPartOfStructure(7, 4, 0, maxPeriodcs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(28).isAPartOf(maxPeriodcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxPeriodc01 = DD.maxprdc.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData maxPeriodc02 = DD.maxprdc.copyToZonedDecimal().isAPartOf(filler3,4);
	public ZonedDecimalData maxPeriodc03 = DD.maxprdc.copyToZonedDecimal().isAPartOf(filler3,8);
	public ZonedDecimalData maxPeriodc04 = DD.maxprdc.copyToZonedDecimal().isAPartOf(filler3,12);
	public ZonedDecimalData maxPeriodc05 = DD.maxprdc.copyToZonedDecimal().isAPartOf(filler3,16);
	public ZonedDecimalData maxPeriodc06 = DD.maxprdc.copyToZonedDecimal().isAPartOf(filler3,20);
	public ZonedDecimalData maxPeriodc07 = DD.maxprdc.copyToZonedDecimal().isAPartOf(filler3,24);
	
	public FixedLengthStringData maxPeriodds = new FixedLengthStringData(28).isAPartOf(dataFields, 151);  //115
	public ZonedDecimalData[] maxPeriodd = ZDArrayPartOfStructure(7, 4, 0, maxPeriodds, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(28).isAPartOf(maxPeriodds, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxPeriodd01 = DD.maxprdd.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData maxPeriodd02 = DD.maxprdd.copyToZonedDecimal().isAPartOf(filler4,4);
	public ZonedDecimalData maxPeriodd03 = DD.maxprdd.copyToZonedDecimal().isAPartOf(filler4,8);
	public ZonedDecimalData maxPeriodd04 = DD.maxprdd.copyToZonedDecimal().isAPartOf(filler4,12);
	public ZonedDecimalData maxPeriodd05 = DD.maxprdd.copyToZonedDecimal().isAPartOf(filler4,16);
	public ZonedDecimalData maxPeriodd06 = DD.maxprdd.copyToZonedDecimal().isAPartOf(filler4,20);
	public ZonedDecimalData maxPeriodd07 = DD.maxprdd.copyToZonedDecimal().isAPartOf(filler4,24);
	
	public FixedLengthStringData maxPeriodes = new FixedLengthStringData(28).isAPartOf(dataFields, 179); //131
	public ZonedDecimalData[] maxPeriode = ZDArrayPartOfStructure(7, 4, 0, maxPeriodes, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(28).isAPartOf(maxPeriodes, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxPeriode01 = DD.maxprde.copyToZonedDecimal().isAPartOf(filler5,0);
	public ZonedDecimalData maxPeriode02 = DD.maxprde.copyToZonedDecimal().isAPartOf(filler5,4);
	public ZonedDecimalData maxPeriode03 = DD.maxprde.copyToZonedDecimal().isAPartOf(filler5,8);
	public ZonedDecimalData maxPeriode04 = DD.maxprde.copyToZonedDecimal().isAPartOf(filler5,12);
	public ZonedDecimalData maxPeriode05 = DD.maxprde.copyToZonedDecimal().isAPartOf(filler5,16);
	public ZonedDecimalData maxPeriode06 = DD.maxprde.copyToZonedDecimal().isAPartOf(filler5,20);
	public ZonedDecimalData maxPeriode07 = DD.maxprde.copyToZonedDecimal().isAPartOf(filler5,24);
	
	
	public FixedLengthStringData maxPeriodfs = new FixedLengthStringData(28).isAPartOf(dataFields, 207); //147
	public ZonedDecimalData[] maxPeriodf = ZDArrayPartOfStructure(7, 4, 0, maxPeriodfs, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(28).isAPartOf(maxPeriodfs, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxPeriodf01 = DD.maxprdf.copyToZonedDecimal().isAPartOf(filler6,0);
	public ZonedDecimalData maxPeriodf02 = DD.maxprdf.copyToZonedDecimal().isAPartOf(filler6,4);
	public ZonedDecimalData maxPeriodf03 = DD.maxprdf.copyToZonedDecimal().isAPartOf(filler6,8);
	public ZonedDecimalData maxPeriodf04 = DD.maxprdf.copyToZonedDecimal().isAPartOf(filler6,12);
	public ZonedDecimalData maxPeriodf05 = DD.maxprdf.copyToZonedDecimal().isAPartOf(filler6,16);
	public ZonedDecimalData maxPeriodf06 = DD.maxprdf.copyToZonedDecimal().isAPartOf(filler6,20);
	public ZonedDecimalData maxPeriodf07 = DD.maxprdf.copyToZonedDecimal().isAPartOf(filler6,24);
	
	
	public FixedLengthStringData pcInitUnitsas = new FixedLengthStringData(35).isAPartOf(dataFields, 235); //163
	public ZonedDecimalData[] pcInitUnitsa = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsas, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsas, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcInitUnitsa01 = DD.pcinita.copyToZonedDecimal().isAPartOf(filler7,0);
	public ZonedDecimalData pcInitUnitsa02 = DD.pcinita.copyToZonedDecimal().isAPartOf(filler7,5);
	public ZonedDecimalData pcInitUnitsa03 = DD.pcinita.copyToZonedDecimal().isAPartOf(filler7,10);
	public ZonedDecimalData pcInitUnitsa04 = DD.pcinita.copyToZonedDecimal().isAPartOf(filler7,15);
	public ZonedDecimalData pcInitUnitsa05 = DD.pcinita.copyToZonedDecimal().isAPartOf(filler7,20);
	public ZonedDecimalData pcInitUnitsa06 = DD.pcinita.copyToZonedDecimal().isAPartOf(filler7,25);
	public ZonedDecimalData pcInitUnitsa07 = DD.pcinita.copyToZonedDecimal().isAPartOf(filler7,30);
	
	
	public FixedLengthStringData pcInitUnitsbs = new FixedLengthStringData(35).isAPartOf(dataFields, 270);  //183
	public ZonedDecimalData[] pcInitUnitsb = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsbs, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsbs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcInitUnitsb01 = DD.pcinitb.copyToZonedDecimal().isAPartOf(filler8,0);
	public ZonedDecimalData pcInitUnitsb02 = DD.pcinitb.copyToZonedDecimal().isAPartOf(filler8,5);
	public ZonedDecimalData pcInitUnitsb03 = DD.pcinitb.copyToZonedDecimal().isAPartOf(filler8,10);
	public ZonedDecimalData pcInitUnitsb04 = DD.pcinitb.copyToZonedDecimal().isAPartOf(filler8,15);
	public ZonedDecimalData pcInitUnitsb05 = DD.pcinitb.copyToZonedDecimal().isAPartOf(filler8,20);
	public ZonedDecimalData pcInitUnitsb06 = DD.pcinitb.copyToZonedDecimal().isAPartOf(filler8,25);
	public ZonedDecimalData pcInitUnitsb07 = DD.pcinitb.copyToZonedDecimal().isAPartOf(filler8,30);
	
	
	public FixedLengthStringData pcInitUnitscs = new FixedLengthStringData(35).isAPartOf(dataFields, 305); //203
	public ZonedDecimalData[] pcInitUnitsc = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitscs, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(35).isAPartOf(pcInitUnitscs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcInitUnitsc01 = DD.pcinitc.copyToZonedDecimal().isAPartOf(filler9,0);
	public ZonedDecimalData pcInitUnitsc02 = DD.pcinitc.copyToZonedDecimal().isAPartOf(filler9,5);
	public ZonedDecimalData pcInitUnitsc03 = DD.pcinitc.copyToZonedDecimal().isAPartOf(filler9,10);
	public ZonedDecimalData pcInitUnitsc04 = DD.pcinitc.copyToZonedDecimal().isAPartOf(filler9,15);
	public ZonedDecimalData pcInitUnitsc05 = DD.pcinitc.copyToZonedDecimal().isAPartOf(filler9,20);
	public ZonedDecimalData pcInitUnitsc06 = DD.pcinitc.copyToZonedDecimal().isAPartOf(filler9,25);
	public ZonedDecimalData pcInitUnitsc07 = DD.pcinitc.copyToZonedDecimal().isAPartOf(filler9,30);
	
	public FixedLengthStringData pcInitUnitsds = new FixedLengthStringData(35).isAPartOf(dataFields, 340); //223
	public ZonedDecimalData[] pcInitUnitsd = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsds, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsds, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcInitUnitsd01 = DD.pcinitd.copyToZonedDecimal().isAPartOf(filler10,0);
	public ZonedDecimalData pcInitUnitsd02 = DD.pcinitd.copyToZonedDecimal().isAPartOf(filler10,5);
	public ZonedDecimalData pcInitUnitsd03 = DD.pcinitd.copyToZonedDecimal().isAPartOf(filler10,10);
	public ZonedDecimalData pcInitUnitsd04 = DD.pcinitd.copyToZonedDecimal().isAPartOf(filler10,15);
	public ZonedDecimalData pcInitUnitsd05 = DD.pcinitd.copyToZonedDecimal().isAPartOf(filler10,20);
	public ZonedDecimalData pcInitUnitsd06 = DD.pcinitd.copyToZonedDecimal().isAPartOf(filler10,25);
	public ZonedDecimalData pcInitUnitsd07 = DD.pcinitd.copyToZonedDecimal().isAPartOf(filler10,30); 
	
	public FixedLengthStringData pcInitUnitses = new FixedLengthStringData(35).isAPartOf(dataFields, 375);
	public ZonedDecimalData[] pcInitUnitse = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitses, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(35).isAPartOf(pcInitUnitses, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcInitUnitse01 = DD.pcinite.copyToZonedDecimal().isAPartOf(filler11,0);
	public ZonedDecimalData pcInitUnitse02 = DD.pcinite.copyToZonedDecimal().isAPartOf(filler11,5);
	public ZonedDecimalData pcInitUnitse03 = DD.pcinite.copyToZonedDecimal().isAPartOf(filler11,10);
	public ZonedDecimalData pcInitUnitse04 = DD.pcinite.copyToZonedDecimal().isAPartOf(filler11,15);
	public ZonedDecimalData pcInitUnitse05 = DD.pcinite.copyToZonedDecimal().isAPartOf(filler11,20);
	public ZonedDecimalData pcInitUnitse06 = DD.pcinite.copyToZonedDecimal().isAPartOf(filler11,25);
	public ZonedDecimalData pcInitUnitse07 = DD.pcinite.copyToZonedDecimal().isAPartOf(filler11,30);
	
	public FixedLengthStringData pcInitUnitsfs = new FixedLengthStringData(35).isAPartOf(dataFields, 410); //263
	public ZonedDecimalData[] pcInitUnitsf = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsfs, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsfs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcInitUnitsf01 = DD.pcinitf.copyToZonedDecimal().isAPartOf(filler12,0);
	public ZonedDecimalData pcInitUnitsf02 = DD.pcinitf.copyToZonedDecimal().isAPartOf(filler12,5);
	public ZonedDecimalData pcInitUnitsf03 = DD.pcinitf.copyToZonedDecimal().isAPartOf(filler12,10);
	public ZonedDecimalData pcInitUnitsf04 = DD.pcinitf.copyToZonedDecimal().isAPartOf(filler12,15);
	public ZonedDecimalData pcInitUnitsf05 = DD.pcinitf.copyToZonedDecimal().isAPartOf(filler12,20);
	public ZonedDecimalData pcInitUnitsf06 = DD.pcinitf.copyToZonedDecimal().isAPartOf(filler12,25);
	public ZonedDecimalData pcInitUnitsf07 = DD.pcinitf.copyToZonedDecimal().isAPartOf(filler12,30);
	
	public FixedLengthStringData pcUnitsas = new FixedLengthStringData(35).isAPartOf(dataFields, 445);    //283
	public ZonedDecimalData[] pcUnitsa = ZDArrayPartOfStructure(7, 5, 2, pcUnitsas, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(35).isAPartOf(pcUnitsas, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcUnitsa01 = DD.pcunita.copyToZonedDecimal().isAPartOf(filler13,0);
	public ZonedDecimalData pcUnitsa02 = DD.pcunita.copyToZonedDecimal().isAPartOf(filler13,5);
	public ZonedDecimalData pcUnitsa03 = DD.pcunita.copyToZonedDecimal().isAPartOf(filler13,10);
	public ZonedDecimalData pcUnitsa04 = DD.pcunita.copyToZonedDecimal().isAPartOf(filler13,15);
	public ZonedDecimalData pcUnitsa05 = DD.pcunita.copyToZonedDecimal().isAPartOf(filler13,20);
	public ZonedDecimalData pcUnitsa06 = DD.pcunita.copyToZonedDecimal().isAPartOf(filler13,25);
	public ZonedDecimalData pcUnitsa07 = DD.pcunita.copyToZonedDecimal().isAPartOf(filler13,30);
	
	
	public FixedLengthStringData pcUnitsbs = new FixedLengthStringData(35).isAPartOf(dataFields, 480);  //303
	public ZonedDecimalData[] pcUnitsb = ZDArrayPartOfStructure(7, 5, 2, pcUnitsbs, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(35).isAPartOf(pcUnitsbs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcUnitsb01 = DD.pcunitb.copyToZonedDecimal().isAPartOf(filler14,0);
	public ZonedDecimalData pcUnitsb02 = DD.pcunitb.copyToZonedDecimal().isAPartOf(filler14,5);
	public ZonedDecimalData pcUnitsb03 = DD.pcunitb.copyToZonedDecimal().isAPartOf(filler14,10);
	public ZonedDecimalData pcUnitsb04 = DD.pcunitb.copyToZonedDecimal().isAPartOf(filler14,15);
	public ZonedDecimalData pcUnitsb05 = DD.pcunitb.copyToZonedDecimal().isAPartOf(filler14,20);
	public ZonedDecimalData pcUnitsb06 = DD.pcunitb.copyToZonedDecimal().isAPartOf(filler14,25);
	public ZonedDecimalData pcUnitsb07 = DD.pcunitb.copyToZonedDecimal().isAPartOf(filler14,30);
	
	public FixedLengthStringData pcUnitscs = new FixedLengthStringData(35).isAPartOf(dataFields, 515); //323
	public ZonedDecimalData[] pcUnitsc = ZDArrayPartOfStructure(7, 5, 2, pcUnitscs, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(35).isAPartOf(pcUnitscs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcUnitsc01 = DD.pcunitc.copyToZonedDecimal().isAPartOf(filler15,0);
	public ZonedDecimalData pcUnitsc02 = DD.pcunitc.copyToZonedDecimal().isAPartOf(filler15,5);
	public ZonedDecimalData pcUnitsc03 = DD.pcunitc.copyToZonedDecimal().isAPartOf(filler15,10);
	public ZonedDecimalData pcUnitsc04 = DD.pcunitc.copyToZonedDecimal().isAPartOf(filler15,15);
	public ZonedDecimalData pcUnitsc05 = DD.pcunitc.copyToZonedDecimal().isAPartOf(filler15,20);
	public ZonedDecimalData pcUnitsc06 = DD.pcunitc.copyToZonedDecimal().isAPartOf(filler15,25);
	public ZonedDecimalData pcUnitsc07 = DD.pcunitc.copyToZonedDecimal().isAPartOf(filler15,30);
	
	
	public FixedLengthStringData pcUnitsds = new FixedLengthStringData(35).isAPartOf(dataFields, 550); //343
	public ZonedDecimalData[] pcUnitsd = ZDArrayPartOfStructure(7, 5, 2, pcUnitsds, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(35).isAPartOf(pcUnitsds, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcUnitsd01 = DD.pcunitd.copyToZonedDecimal().isAPartOf(filler16,0);
	public ZonedDecimalData pcUnitsd02 = DD.pcunitd.copyToZonedDecimal().isAPartOf(filler16,5);
	public ZonedDecimalData pcUnitsd03 = DD.pcunitd.copyToZonedDecimal().isAPartOf(filler16,10);
	public ZonedDecimalData pcUnitsd04 = DD.pcunitd.copyToZonedDecimal().isAPartOf(filler16,15);
	public ZonedDecimalData pcUnitsd05 = DD.pcunitd.copyToZonedDecimal().isAPartOf(filler16,20);
	public ZonedDecimalData pcUnitsd06 = DD.pcunitd.copyToZonedDecimal().isAPartOf(filler16,25);
	public ZonedDecimalData pcUnitsd07 = DD.pcunitd.copyToZonedDecimal().isAPartOf(filler16,30);
	
	
	public FixedLengthStringData pcUnitses = new FixedLengthStringData(35).isAPartOf(dataFields, 585); //363
	public ZonedDecimalData[] pcUnitse = ZDArrayPartOfStructure(7, 5, 2, pcUnitses, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(35).isAPartOf(pcUnitses, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcUnitse01 = DD.pcunite.copyToZonedDecimal().isAPartOf(filler17,0);
	public ZonedDecimalData pcUnitse02 = DD.pcunite.copyToZonedDecimal().isAPartOf(filler17,5);
	public ZonedDecimalData pcUnitse03 = DD.pcunite.copyToZonedDecimal().isAPartOf(filler17,10);
	public ZonedDecimalData pcUnitse04 = DD.pcunite.copyToZonedDecimal().isAPartOf(filler17,15);
	public ZonedDecimalData pcUnitse05 = DD.pcunite.copyToZonedDecimal().isAPartOf(filler17,20);
	public ZonedDecimalData pcUnitse06 = DD.pcunite.copyToZonedDecimal().isAPartOf(filler17,25);
	public ZonedDecimalData pcUnitse07 = DD.pcunite.copyToZonedDecimal().isAPartOf(filler17,30);
	
	
	public FixedLengthStringData pcUnitsfs = new FixedLengthStringData(35).isAPartOf(dataFields, 620); //383
	public ZonedDecimalData[] pcUnitsf = ZDArrayPartOfStructure(7, 5, 2, pcUnitsfs, 0);
	public FixedLengthStringData filler18 = new FixedLengthStringData(35).isAPartOf(pcUnitsfs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pcUnitsf01 = DD.pcunitf.copyToZonedDecimal().isAPartOf(filler18,0);
	public ZonedDecimalData pcUnitsf02 = DD.pcunitf.copyToZonedDecimal().isAPartOf(filler18,5);
	public ZonedDecimalData pcUnitsf03 = DD.pcunitf.copyToZonedDecimal().isAPartOf(filler18,10);
	public ZonedDecimalData pcUnitsf04 = DD.pcunitf.copyToZonedDecimal().isAPartOf(filler18,15);
	public ZonedDecimalData pcUnitsf05 = DD.pcunitf.copyToZonedDecimal().isAPartOf(filler18,20);
	public ZonedDecimalData pcUnitsf06 = DD.pcunitf.copyToZonedDecimal().isAPartOf(filler18,25);
	public ZonedDecimalData pcUnitsf07 = DD.pcunitf.copyToZonedDecimal().isAPartOf(filler18,30);
	
	
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,655); // 403
	public FixedLengthStringData yotalmth = DD.yotalmth.copy().isAPartOf(dataFields,660); //408
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(340).isAPartOf(dataArea, 664); //412
	public FixedLengthStringData billfreqsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] billfreqErr = FLSArrayPartOfStructure(6, 4, billfreqsErr, 0);
	public FixedLengthStringData filler19 = new FixedLengthStringData(24).isAPartOf(billfreqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01Err = new FixedLengthStringData(4).isAPartOf(filler19, 0);
	public FixedLengthStringData billfreq02Err = new FixedLengthStringData(4).isAPartOf(filler19, 4);
	public FixedLengthStringData billfreq03Err = new FixedLengthStringData(4).isAPartOf(filler19, 8);
	public FixedLengthStringData billfreq04Err = new FixedLengthStringData(4).isAPartOf(filler19, 12);
	public FixedLengthStringData billfreq05Err = new FixedLengthStringData(4).isAPartOf(filler19, 16);
	public FixedLengthStringData billfreq06Err = new FixedLengthStringData(4).isAPartOf(filler19, 20);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData maxprdasErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] maxprdaErr = FLSArrayPartOfStructure(4, 4, maxprdasErr, 0);
	public FixedLengthStringData filler20 = new FixedLengthStringData(16).isAPartOf(maxprdasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxprda01Err = new FixedLengthStringData(4).isAPartOf(filler20, 0);
	public FixedLengthStringData maxprda02Err = new FixedLengthStringData(4).isAPartOf(filler20, 4);
	public FixedLengthStringData maxprda03Err = new FixedLengthStringData(4).isAPartOf(filler20, 8);
	public FixedLengthStringData maxprda04Err = new FixedLengthStringData(4).isAPartOf(filler20, 12);
	public FixedLengthStringData maxprdbsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] maxprdbErr = FLSArrayPartOfStructure(4, 4, maxprdbsErr, 0);
	public FixedLengthStringData filler21 = new FixedLengthStringData(16).isAPartOf(maxprdbsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxprdb01Err = new FixedLengthStringData(4).isAPartOf(filler21, 0);
	public FixedLengthStringData maxprdb02Err = new FixedLengthStringData(4).isAPartOf(filler21, 4);
	public FixedLengthStringData maxprdb03Err = new FixedLengthStringData(4).isAPartOf(filler21, 8);
	public FixedLengthStringData maxprdb04Err = new FixedLengthStringData(4).isAPartOf(filler21, 12);
	public FixedLengthStringData maxprdcsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData[] maxprdcErr = FLSArrayPartOfStructure(4, 4, maxprdcsErr, 0);
	public FixedLengthStringData filler22 = new FixedLengthStringData(16).isAPartOf(maxprdcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxprdc01Err = new FixedLengthStringData(4).isAPartOf(filler22, 0);
	public FixedLengthStringData maxprdc02Err = new FixedLengthStringData(4).isAPartOf(filler22, 4);
	public FixedLengthStringData maxprdc03Err = new FixedLengthStringData(4).isAPartOf(filler22, 8);
	public FixedLengthStringData maxprdc04Err = new FixedLengthStringData(4).isAPartOf(filler22, 12);
	public FixedLengthStringData maxprddsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] maxprddErr = FLSArrayPartOfStructure(4, 4, maxprddsErr, 0);
	public FixedLengthStringData filler23 = new FixedLengthStringData(16).isAPartOf(maxprddsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxprdd01Err = new FixedLengthStringData(4).isAPartOf(filler23, 0);
	public FixedLengthStringData maxprdd02Err = new FixedLengthStringData(4).isAPartOf(filler23, 4);
	public FixedLengthStringData maxprdd03Err = new FixedLengthStringData(4).isAPartOf(filler23, 8);
	public FixedLengthStringData maxprdd04Err = new FixedLengthStringData(4).isAPartOf(filler23, 12);
	public FixedLengthStringData maxprdesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] maxprdeErr = FLSArrayPartOfStructure(4, 4, maxprdesErr, 0);
	public FixedLengthStringData filler24 = new FixedLengthStringData(16).isAPartOf(maxprdesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxprde01Err = new FixedLengthStringData(4).isAPartOf(filler24, 0);
	public FixedLengthStringData maxprde02Err = new FixedLengthStringData(4).isAPartOf(filler24, 4);
	public FixedLengthStringData maxprde03Err = new FixedLengthStringData(4).isAPartOf(filler24, 8);
	public FixedLengthStringData maxprde04Err = new FixedLengthStringData(4).isAPartOf(filler24, 12);
	public FixedLengthStringData maxprdfsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData[] maxprdfErr = FLSArrayPartOfStructure(4, 4, maxprdfsErr, 0);
	public FixedLengthStringData filler25 = new FixedLengthStringData(16).isAPartOf(maxprdfsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxprdf01Err = new FixedLengthStringData(4).isAPartOf(filler25, 0);
	public FixedLengthStringData maxprdf02Err = new FixedLengthStringData(4).isAPartOf(filler25, 4);
	public FixedLengthStringData maxprdf03Err = new FixedLengthStringData(4).isAPartOf(filler25, 8);
	public FixedLengthStringData maxprdf04Err = new FixedLengthStringData(4).isAPartOf(filler25, 12);
	public FixedLengthStringData pcinitasErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData[] pcinitaErr = FLSArrayPartOfStructure(4, 4, pcinitasErr, 0);
	public FixedLengthStringData filler26 = new FixedLengthStringData(16).isAPartOf(pcinitasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcinita01Err = new FixedLengthStringData(4).isAPartOf(filler26, 0);
	public FixedLengthStringData pcinita02Err = new FixedLengthStringData(4).isAPartOf(filler26, 4);
	public FixedLengthStringData pcinita03Err = new FixedLengthStringData(4).isAPartOf(filler26, 8);
	public FixedLengthStringData pcinita04Err = new FixedLengthStringData(4).isAPartOf(filler26, 12);
	public FixedLengthStringData pcinitbsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData[] pcinitbErr = FLSArrayPartOfStructure(4, 4, pcinitbsErr, 0);
	public FixedLengthStringData filler27 = new FixedLengthStringData(16).isAPartOf(pcinitbsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcinitb01Err = new FixedLengthStringData(4).isAPartOf(filler27, 0);
	public FixedLengthStringData pcinitb02Err = new FixedLengthStringData(4).isAPartOf(filler27, 4);
	public FixedLengthStringData pcinitb03Err = new FixedLengthStringData(4).isAPartOf(filler27, 8);
	public FixedLengthStringData pcinitb04Err = new FixedLengthStringData(4).isAPartOf(filler27, 12);
	public FixedLengthStringData pcinitcsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData[] pcinitcErr = FLSArrayPartOfStructure(4, 4, pcinitcsErr, 0);
	public FixedLengthStringData filler28 = new FixedLengthStringData(16).isAPartOf(pcinitcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcinitc01Err = new FixedLengthStringData(4).isAPartOf(filler28, 0);
	public FixedLengthStringData pcinitc02Err = new FixedLengthStringData(4).isAPartOf(filler28, 4);
	public FixedLengthStringData pcinitc03Err = new FixedLengthStringData(4).isAPartOf(filler28, 8);
	public FixedLengthStringData pcinitc04Err = new FixedLengthStringData(4).isAPartOf(filler28, 12);
	public FixedLengthStringData pcinitdsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData[] pcinitdErr = FLSArrayPartOfStructure(4, 4, pcinitdsErr, 0);
	public FixedLengthStringData filler29 = new FixedLengthStringData(16).isAPartOf(pcinitdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcinitd01Err = new FixedLengthStringData(4).isAPartOf(filler29, 0);
	public FixedLengthStringData pcinitd02Err = new FixedLengthStringData(4).isAPartOf(filler29, 4);
	public FixedLengthStringData pcinitd03Err = new FixedLengthStringData(4).isAPartOf(filler29, 8);
	public FixedLengthStringData pcinitd04Err = new FixedLengthStringData(4).isAPartOf(filler29, 12);
	public FixedLengthStringData pcinitesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData[] pciniteErr = FLSArrayPartOfStructure(4, 4, pcinitesErr, 0);
	public FixedLengthStringData filler30 = new FixedLengthStringData(16).isAPartOf(pcinitesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcinite01Err = new FixedLengthStringData(4).isAPartOf(filler30, 0);
	public FixedLengthStringData pcinite02Err = new FixedLengthStringData(4).isAPartOf(filler30, 4);
	public FixedLengthStringData pcinite03Err = new FixedLengthStringData(4).isAPartOf(filler30, 8);
	public FixedLengthStringData pcinite04Err = new FixedLengthStringData(4).isAPartOf(filler30, 12);
	public FixedLengthStringData pcinitfsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData[] pcinitfErr = FLSArrayPartOfStructure(4, 4, pcinitfsErr, 0);
	public FixedLengthStringData filler31 = new FixedLengthStringData(16).isAPartOf(pcinitfsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcinitf01Err = new FixedLengthStringData(4).isAPartOf(filler31, 0);
	public FixedLengthStringData pcinitf02Err = new FixedLengthStringData(4).isAPartOf(filler31, 4);
	public FixedLengthStringData pcinitf03Err = new FixedLengthStringData(4).isAPartOf(filler31, 8);
	public FixedLengthStringData pcinitf04Err = new FixedLengthStringData(4).isAPartOf(filler31, 12);
	public FixedLengthStringData pcunitasErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData[] pcunitaErr = FLSArrayPartOfStructure(4, 4, pcunitasErr, 0);
	public FixedLengthStringData filler32 = new FixedLengthStringData(16).isAPartOf(pcunitasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcunita01Err = new FixedLengthStringData(4).isAPartOf(filler32, 0);
	public FixedLengthStringData pcunita02Err = new FixedLengthStringData(4).isAPartOf(filler32, 4);
	public FixedLengthStringData pcunita03Err = new FixedLengthStringData(4).isAPartOf(filler32, 8);
	public FixedLengthStringData pcunita04Err = new FixedLengthStringData(4).isAPartOf(filler32, 12);
	public FixedLengthStringData pcunitbsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 252);
	public FixedLengthStringData[] pcunitbErr = FLSArrayPartOfStructure(4, 4, pcunitbsErr, 0);
	public FixedLengthStringData filler33 = new FixedLengthStringData(16).isAPartOf(pcunitbsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcunitb01Err = new FixedLengthStringData(4).isAPartOf(filler33, 0);
	public FixedLengthStringData pcunitb02Err = new FixedLengthStringData(4).isAPartOf(filler33, 4);
	public FixedLengthStringData pcunitb03Err = new FixedLengthStringData(4).isAPartOf(filler33, 8);
	public FixedLengthStringData pcunitb04Err = new FixedLengthStringData(4).isAPartOf(filler33, 12);
	public FixedLengthStringData pcunitcsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 268);
	public FixedLengthStringData[] pcunitcErr = FLSArrayPartOfStructure(4, 4, pcunitcsErr, 0);
	public FixedLengthStringData filler34 = new FixedLengthStringData(16).isAPartOf(pcunitcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcunitc01Err = new FixedLengthStringData(4).isAPartOf(filler34, 0);
	public FixedLengthStringData pcunitc02Err = new FixedLengthStringData(4).isAPartOf(filler34, 4);
	public FixedLengthStringData pcunitc03Err = new FixedLengthStringData(4).isAPartOf(filler34, 8);
	public FixedLengthStringData pcunitc04Err = new FixedLengthStringData(4).isAPartOf(filler34, 12);
	public FixedLengthStringData pcunitdsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 284);
	public FixedLengthStringData[] pcunitdErr = FLSArrayPartOfStructure(4, 4, pcunitdsErr, 0);
	public FixedLengthStringData filler35 = new FixedLengthStringData(16).isAPartOf(pcunitdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcunitd01Err = new FixedLengthStringData(4).isAPartOf(filler35, 0);
	public FixedLengthStringData pcunitd02Err = new FixedLengthStringData(4).isAPartOf(filler35, 4);
	public FixedLengthStringData pcunitd03Err = new FixedLengthStringData(4).isAPartOf(filler35, 8);
	public FixedLengthStringData pcunitd04Err = new FixedLengthStringData(4).isAPartOf(filler35, 12);
	public FixedLengthStringData pcunitesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 300);
	public FixedLengthStringData[] pcuniteErr = FLSArrayPartOfStructure(4, 4, pcunitesErr, 0);
	public FixedLengthStringData filler36 = new FixedLengthStringData(16).isAPartOf(pcunitesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcunite01Err = new FixedLengthStringData(4).isAPartOf(filler36, 0);
	public FixedLengthStringData pcunite02Err = new FixedLengthStringData(4).isAPartOf(filler36, 4);
	public FixedLengthStringData pcunite03Err = new FixedLengthStringData(4).isAPartOf(filler36, 8);
	public FixedLengthStringData pcunite04Err = new FixedLengthStringData(4).isAPartOf(filler36, 12);
	public FixedLengthStringData pcunitfsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 316);
	public FixedLengthStringData[] pcunitfErr = FLSArrayPartOfStructure(4, 4, pcunitfsErr, 0);
	public FixedLengthStringData filler37 = new FixedLengthStringData(16).isAPartOf(pcunitfsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pcunitf01Err = new FixedLengthStringData(4).isAPartOf(filler37, 0);
	public FixedLengthStringData pcunitf02Err = new FixedLengthStringData(4).isAPartOf(filler37, 4);
	public FixedLengthStringData pcunitf03Err = new FixedLengthStringData(4).isAPartOf(filler37, 8);
	public FixedLengthStringData pcunitf04Err = new FixedLengthStringData(4).isAPartOf(filler37, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 332);
	public FixedLengthStringData yotalmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 336);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1020).isAPartOf(dataArea, 1004); //752
	public FixedLengthStringData billfreqsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(6, 12, billfreqsOut, 0);
	public FixedLengthStringData[][] billfreqO = FLSDArrayPartOfArrayStructure(12, 1, billfreqOut, 0);
	public FixedLengthStringData filler38 = new FixedLengthStringData(72).isAPartOf(billfreqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] billfreq01Out = FLSArrayPartOfStructure(12, 1, filler38, 0);
	public FixedLengthStringData[] billfreq02Out = FLSArrayPartOfStructure(12, 1, filler38, 12);
	public FixedLengthStringData[] billfreq03Out = FLSArrayPartOfStructure(12, 1, filler38, 24);
	public FixedLengthStringData[] billfreq04Out = FLSArrayPartOfStructure(12, 1, filler38, 36);
	public FixedLengthStringData[] billfreq05Out = FLSArrayPartOfStructure(12, 1, filler38, 48);
	public FixedLengthStringData[] billfreq06Out = FLSArrayPartOfStructure(12, 1, filler38, 60);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData maxprdasOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] maxprdaOut = FLSArrayPartOfStructure(4, 12, maxprdasOut, 0);
	public FixedLengthStringData[][] maxprdaO = FLSDArrayPartOfArrayStructure(12, 1, maxprdaOut, 0);
	public FixedLengthStringData filler39 = new FixedLengthStringData(48).isAPartOf(maxprdasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxprda01Out = FLSArrayPartOfStructure(12, 1, filler39, 0);
	public FixedLengthStringData[] maxprda02Out = FLSArrayPartOfStructure(12, 1, filler39, 12);
	public FixedLengthStringData[] maxprda03Out = FLSArrayPartOfStructure(12, 1, filler39, 24);
	public FixedLengthStringData[] maxprda04Out = FLSArrayPartOfStructure(12, 1, filler39, 36);
	public FixedLengthStringData maxprdbsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] maxprdbOut = FLSArrayPartOfStructure(4, 12, maxprdbsOut, 0);
	public FixedLengthStringData[][] maxprdbO = FLSDArrayPartOfArrayStructure(12, 1, maxprdbOut, 0);
	public FixedLengthStringData filler40 = new FixedLengthStringData(48).isAPartOf(maxprdbsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxprdb01Out = FLSArrayPartOfStructure(12, 1, filler40, 0);
	public FixedLengthStringData[] maxprdb02Out = FLSArrayPartOfStructure(12, 1, filler40, 12);
	public FixedLengthStringData[] maxprdb03Out = FLSArrayPartOfStructure(12, 1, filler40, 24);
	public FixedLengthStringData[] maxprdb04Out = FLSArrayPartOfStructure(12, 1, filler40, 36);
	public FixedLengthStringData maxprdcsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 228);
	public FixedLengthStringData[] maxprdcOut = FLSArrayPartOfStructure(4, 12, maxprdcsOut, 0);
	public FixedLengthStringData[][] maxprdcO = FLSDArrayPartOfArrayStructure(12, 1, maxprdcOut, 0);
	public FixedLengthStringData filler41 = new FixedLengthStringData(48).isAPartOf(maxprdcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxprdc01Out = FLSArrayPartOfStructure(12, 1, filler41, 0);
	public FixedLengthStringData[] maxprdc02Out = FLSArrayPartOfStructure(12, 1, filler41, 12);
	public FixedLengthStringData[] maxprdc03Out = FLSArrayPartOfStructure(12, 1, filler41, 24);
	public FixedLengthStringData[] maxprdc04Out = FLSArrayPartOfStructure(12, 1, filler41, 36);
	public FixedLengthStringData maxprddsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] maxprddOut = FLSArrayPartOfStructure(4, 12, maxprddsOut, 0);
	public FixedLengthStringData[][] maxprddO = FLSDArrayPartOfArrayStructure(12, 1, maxprddOut, 0);
	public FixedLengthStringData filler42 = new FixedLengthStringData(48).isAPartOf(maxprddsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxprdd01Out = FLSArrayPartOfStructure(12, 1, filler42, 0);
	public FixedLengthStringData[] maxprdd02Out = FLSArrayPartOfStructure(12, 1, filler42, 12);
	public FixedLengthStringData[] maxprdd03Out = FLSArrayPartOfStructure(12, 1, filler42, 24);
	public FixedLengthStringData[] maxprdd04Out = FLSArrayPartOfStructure(12, 1, filler42, 36);
	public FixedLengthStringData maxprdesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] maxprdeOut = FLSArrayPartOfStructure(4, 12, maxprdesOut, 0);
	public FixedLengthStringData[][] maxprdeO = FLSDArrayPartOfArrayStructure(12, 1, maxprdeOut, 0);
	public FixedLengthStringData filler43 = new FixedLengthStringData(48).isAPartOf(maxprdesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxprde01Out = FLSArrayPartOfStructure(12, 1, filler43, 0);
	public FixedLengthStringData[] maxprde02Out = FLSArrayPartOfStructure(12, 1, filler43, 12);
	public FixedLengthStringData[] maxprde03Out = FLSArrayPartOfStructure(12, 1, filler43, 24);
	public FixedLengthStringData[] maxprde04Out = FLSArrayPartOfStructure(12, 1, filler43, 36);
	public FixedLengthStringData maxprdfsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 372);
	public FixedLengthStringData[] maxprdfOut = FLSArrayPartOfStructure(4, 12, maxprdfsOut, 0);
	public FixedLengthStringData[][] maxprdfO = FLSDArrayPartOfArrayStructure(12, 1, maxprdfOut, 0);
	public FixedLengthStringData filler44 = new FixedLengthStringData(48).isAPartOf(maxprdfsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxprdf01Out = FLSArrayPartOfStructure(12, 1, filler44, 0);
	public FixedLengthStringData[] maxprdf02Out = FLSArrayPartOfStructure(12, 1, filler44, 12);
	public FixedLengthStringData[] maxprdf03Out = FLSArrayPartOfStructure(12, 1, filler44, 24);
	public FixedLengthStringData[] maxprdf04Out = FLSArrayPartOfStructure(12, 1, filler44, 36);
	public FixedLengthStringData pcinitasOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 420);
	public FixedLengthStringData[] pcinitaOut = FLSArrayPartOfStructure(4, 12, pcinitasOut, 0);
	public FixedLengthStringData[][] pcinitaO = FLSDArrayPartOfArrayStructure(12, 1, pcinitaOut, 0);
	public FixedLengthStringData filler45 = new FixedLengthStringData(48).isAPartOf(pcinitasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcinita01Out = FLSArrayPartOfStructure(12, 1, filler45, 0);
	public FixedLengthStringData[] pcinita02Out = FLSArrayPartOfStructure(12, 1, filler45, 12);
	public FixedLengthStringData[] pcinita03Out = FLSArrayPartOfStructure(12, 1, filler45, 24);
	public FixedLengthStringData[] pcinita04Out = FLSArrayPartOfStructure(12, 1, filler45, 36);
	public FixedLengthStringData pcinitbsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 468);
	public FixedLengthStringData[] pcinitbOut = FLSArrayPartOfStructure(4, 12, pcinitbsOut, 0);
	public FixedLengthStringData[][] pcinitbO = FLSDArrayPartOfArrayStructure(12, 1, pcinitbOut, 0);
	public FixedLengthStringData filler46 = new FixedLengthStringData(48).isAPartOf(pcinitbsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcinitb01Out = FLSArrayPartOfStructure(12, 1, filler46, 0);
	public FixedLengthStringData[] pcinitb02Out = FLSArrayPartOfStructure(12, 1, filler46, 12);
	public FixedLengthStringData[] pcinitb03Out = FLSArrayPartOfStructure(12, 1, filler46, 24);
	public FixedLengthStringData[] pcinitb04Out = FLSArrayPartOfStructure(12, 1, filler46, 36);
	public FixedLengthStringData pcinitcsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 516);
	public FixedLengthStringData[] pcinitcOut = FLSArrayPartOfStructure(4, 12, pcinitcsOut, 0);
	public FixedLengthStringData[][] pcinitcO = FLSDArrayPartOfArrayStructure(12, 1, pcinitcOut, 0);
	public FixedLengthStringData filler47 = new FixedLengthStringData(48).isAPartOf(pcinitcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcinitc01Out = FLSArrayPartOfStructure(12, 1, filler47, 0);
	public FixedLengthStringData[] pcinitc02Out = FLSArrayPartOfStructure(12, 1, filler47, 12);
	public FixedLengthStringData[] pcinitc03Out = FLSArrayPartOfStructure(12, 1, filler47, 24);
	public FixedLengthStringData[] pcinitc04Out = FLSArrayPartOfStructure(12, 1, filler47, 36);
	public FixedLengthStringData pcinitdsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 564);
	public FixedLengthStringData[] pcinitdOut = FLSArrayPartOfStructure(4, 12, pcinitdsOut, 0);
	public FixedLengthStringData[][] pcinitdO = FLSDArrayPartOfArrayStructure(12, 1, pcinitdOut, 0);
	public FixedLengthStringData filler48 = new FixedLengthStringData(48).isAPartOf(pcinitdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcinitd01Out = FLSArrayPartOfStructure(12, 1, filler48, 0);
	public FixedLengthStringData[] pcinitd02Out = FLSArrayPartOfStructure(12, 1, filler48, 12);
	public FixedLengthStringData[] pcinitd03Out = FLSArrayPartOfStructure(12, 1, filler48, 24);
	public FixedLengthStringData[] pcinitd04Out = FLSArrayPartOfStructure(12, 1, filler48, 36);
	public FixedLengthStringData pcinitesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 612);
	public FixedLengthStringData[] pciniteOut = FLSArrayPartOfStructure(4, 12, pcinitesOut, 0);
	public FixedLengthStringData[][] pciniteO = FLSDArrayPartOfArrayStructure(12, 1, pciniteOut, 0);
	public FixedLengthStringData filler49 = new FixedLengthStringData(48).isAPartOf(pcinitesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcinite01Out = FLSArrayPartOfStructure(12, 1, filler49, 0);
	public FixedLengthStringData[] pcinite02Out = FLSArrayPartOfStructure(12, 1, filler49, 12);
	public FixedLengthStringData[] pcinite03Out = FLSArrayPartOfStructure(12, 1, filler49, 24);
	public FixedLengthStringData[] pcinite04Out = FLSArrayPartOfStructure(12, 1, filler49, 36);
	public FixedLengthStringData pcinitfsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 660);
	public FixedLengthStringData[] pcinitfOut = FLSArrayPartOfStructure(4, 12, pcinitfsOut, 0);
	public FixedLengthStringData[][] pcinitfO = FLSDArrayPartOfArrayStructure(12, 1, pcinitfOut, 0);
	public FixedLengthStringData filler50 = new FixedLengthStringData(48).isAPartOf(pcinitfsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcinitf01Out = FLSArrayPartOfStructure(12, 1, filler50, 0);
	public FixedLengthStringData[] pcinitf02Out = FLSArrayPartOfStructure(12, 1, filler50, 12);
	public FixedLengthStringData[] pcinitf03Out = FLSArrayPartOfStructure(12, 1, filler50, 24);
	public FixedLengthStringData[] pcinitf04Out = FLSArrayPartOfStructure(12, 1, filler50, 36);
	public FixedLengthStringData pcunitasOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 708);
	public FixedLengthStringData[] pcunitaOut = FLSArrayPartOfStructure(4, 12, pcunitasOut, 0);
	public FixedLengthStringData[][] pcunitaO = FLSDArrayPartOfArrayStructure(12, 1, pcunitaOut, 0);
	public FixedLengthStringData filler51 = new FixedLengthStringData(48).isAPartOf(pcunitasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcunita01Out = FLSArrayPartOfStructure(12, 1, filler51, 0);
	public FixedLengthStringData[] pcunita02Out = FLSArrayPartOfStructure(12, 1, filler51, 12);
	public FixedLengthStringData[] pcunita03Out = FLSArrayPartOfStructure(12, 1, filler51, 24);
	public FixedLengthStringData[] pcunita04Out = FLSArrayPartOfStructure(12, 1, filler51, 36);
	public FixedLengthStringData pcunitbsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 756);
	public FixedLengthStringData[] pcunitbOut = FLSArrayPartOfStructure(4, 12, pcunitbsOut, 0);
	public FixedLengthStringData[][] pcunitbO = FLSDArrayPartOfArrayStructure(12, 1, pcunitbOut, 0);
	public FixedLengthStringData filler52 = new FixedLengthStringData(48).isAPartOf(pcunitbsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcunitb01Out = FLSArrayPartOfStructure(12, 1, filler52, 0);
	public FixedLengthStringData[] pcunitb02Out = FLSArrayPartOfStructure(12, 1, filler52, 12);
	public FixedLengthStringData[] pcunitb03Out = FLSArrayPartOfStructure(12, 1, filler52, 24);
	public FixedLengthStringData[] pcunitb04Out = FLSArrayPartOfStructure(12, 1, filler52, 36);
	public FixedLengthStringData pcunitcsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 804);
	public FixedLengthStringData[] pcunitcOut = FLSArrayPartOfStructure(4, 12, pcunitcsOut, 0);
	public FixedLengthStringData[][] pcunitcO = FLSDArrayPartOfArrayStructure(12, 1, pcunitcOut, 0);
	public FixedLengthStringData filler53 = new FixedLengthStringData(48).isAPartOf(pcunitcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcunitc01Out = FLSArrayPartOfStructure(12, 1, filler53, 0);
	public FixedLengthStringData[] pcunitc02Out = FLSArrayPartOfStructure(12, 1, filler53, 12);
	public FixedLengthStringData[] pcunitc03Out = FLSArrayPartOfStructure(12, 1, filler53, 24);
	public FixedLengthStringData[] pcunitc04Out = FLSArrayPartOfStructure(12, 1, filler53, 36);
	public FixedLengthStringData pcunitdsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 852);
	public FixedLengthStringData[] pcunitdOut = FLSArrayPartOfStructure(4, 12, pcunitdsOut, 0);
	public FixedLengthStringData[][] pcunitdO = FLSDArrayPartOfArrayStructure(12, 1, pcunitdOut, 0);
	public FixedLengthStringData filler54 = new FixedLengthStringData(48).isAPartOf(pcunitdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcunitd01Out = FLSArrayPartOfStructure(12, 1, filler54, 0);
	public FixedLengthStringData[] pcunitd02Out = FLSArrayPartOfStructure(12, 1, filler54, 12);
	public FixedLengthStringData[] pcunitd03Out = FLSArrayPartOfStructure(12, 1, filler54, 24);
	public FixedLengthStringData[] pcunitd04Out = FLSArrayPartOfStructure(12, 1, filler54, 36);
	public FixedLengthStringData pcunitesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 900);
	public FixedLengthStringData[] pcuniteOut = FLSArrayPartOfStructure(4, 12, pcunitesOut, 0);
	public FixedLengthStringData[][] pcuniteO = FLSDArrayPartOfArrayStructure(12, 1, pcuniteOut, 0);
	public FixedLengthStringData filler55 = new FixedLengthStringData(48).isAPartOf(pcunitesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcunite01Out = FLSArrayPartOfStructure(12, 1, filler55, 0);
	public FixedLengthStringData[] pcunite02Out = FLSArrayPartOfStructure(12, 1, filler55, 12);
	public FixedLengthStringData[] pcunite03Out = FLSArrayPartOfStructure(12, 1, filler55, 24);
	public FixedLengthStringData[] pcunite04Out = FLSArrayPartOfStructure(12, 1, filler55, 36);
	public FixedLengthStringData pcunitfsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 948);
	public FixedLengthStringData[] pcunitfOut = FLSArrayPartOfStructure(4, 12, pcunitfsOut, 0);
	public FixedLengthStringData[][] pcunitfO = FLSDArrayPartOfArrayStructure(12, 1, pcunitfOut, 0);
	public FixedLengthStringData filler56 = new FixedLengthStringData(48).isAPartOf(pcunitfsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pcunitf01Out = FLSArrayPartOfStructure(12, 1, filler56, 0);
	public FixedLengthStringData[] pcunitf02Out = FLSArrayPartOfStructure(12, 1, filler56, 12);
	public FixedLengthStringData[] pcunitf03Out = FLSArrayPartOfStructure(12, 1, filler56, 24);
	public FixedLengthStringData[] pcunitf04Out = FLSArrayPartOfStructure(12, 1, filler56, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 996);
	public FixedLengthStringData[] yotalmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 1008);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5536screenWritten = new LongData(0);
	public LongData S5536protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5536ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itmfrmOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itmtoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq03Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprda01Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunita01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinita01Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdb01Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitb01Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitb01Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdc01Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitc01Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitc01Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprda02Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunita02Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinita02Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdb02Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitb02Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitb02Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdc02Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitc02Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitc02Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprda03Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunita03Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinita03Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdb03Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitb03Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitb03Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdc03Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitc03Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitc03Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprda04Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunita04Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinita04Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdb04Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitb04Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitb04Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdc04Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitc04Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitc04Out,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq04Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq05Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq06Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdd01Out,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitd01Out,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitd01Out,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprde01Out,new String[] {"57",null, "-57",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunite01Out,new String[] {"58",null, "-58",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinite01Out,new String[] {"59",null, "-59",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdf01Out,new String[] {"69",null, "-69",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitf01Out,new String[] {"70",null, "-70",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitf01Out,new String[] {"71",null, "-71",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdd02Out,new String[] {"48",null, "-48",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitd02Out,new String[] {"49",null, "-49",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitd02Out,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprde02Out,new String[] {"60",null, "-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunite02Out,new String[] {"61",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinite02Out,new String[] {"62",null, "-62",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdf02Out,new String[] {"72",null, "-72",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitf02Out,new String[] {"73",null, "-73",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitf02Out,new String[] {"74",null, "-74",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdd03Out,new String[] {"51",null, "-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitd03Out,new String[] {"52",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitd03Out,new String[] {"53",null, "-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprde03Out,new String[] {"63",null, "-63",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunite03Out,new String[] {"64",null, "-64",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinite03Out,new String[] {"65",null, "-65",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdf03Out,new String[] {"75",null, "-75",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitf03Out,new String[] {"76",null, "-76",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitf03Out,new String[] {"77",null, "-77",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdd04Out,new String[] {"54",null, "-54",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitd04Out,new String[] {"55",null, "-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitd04Out,new String[] {"56",null, "-56",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprde04Out,new String[] {"66",null, "-66",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunite04Out,new String[] {"67",null, "-67",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinite04Out,new String[] {"68",null, "-68",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxprdf04Out,new String[] {"78",null, "-78",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcunitf04Out,new String[] {"79",null, "-79",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcinitf04Out,new String[] {"80",null, "-80",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(yotalmthOut,new String[] {"81",null, "-81",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, billfreq01, billfreq02, billfreq03, maxPerioda01, pcUnitsa01, pcInitUnitsa01, maxPeriodb01, pcUnitsb01, pcInitUnitsb01, maxPeriodc01, pcUnitsc01, pcInitUnitsc01, maxPerioda02, pcUnitsa02, pcInitUnitsa02, maxPeriodb02, pcUnitsb02, pcInitUnitsb02, maxPeriodc02, pcUnitsc02, pcInitUnitsc02, maxPerioda03, pcUnitsa03, pcInitUnitsa03, maxPeriodb03, pcUnitsb03, pcInitUnitsb03, maxPeriodc03, pcUnitsc03, pcInitUnitsc03, maxPerioda04, pcUnitsa04, pcInitUnitsa04, maxPeriodb04, pcUnitsb04, pcInitUnitsb04, maxPeriodc04, pcUnitsc04, pcInitUnitsc04, billfreq04, billfreq05, billfreq06, maxPeriodd01, pcUnitsd01, pcInitUnitsd01, maxPeriode01, pcUnitse01, pcInitUnitse01, maxPeriodf01, pcUnitsf01, pcInitUnitsf01, maxPeriodd02, pcUnitsd02, pcInitUnitsd02, maxPeriode02, pcUnitse02, pcInitUnitse02, maxPeriodf02, pcUnitsf02, pcInitUnitsf02, maxPeriodd03, pcUnitsd03, pcInitUnitsd03, maxPeriode03, pcUnitse03, pcInitUnitse03, maxPeriodf03, pcUnitsf03, pcInitUnitsf03, maxPeriodd04, pcUnitsd04, pcInitUnitsd04, maxPeriode04, pcUnitse04, pcInitUnitse04, maxPeriodf04, pcUnitsf04, pcInitUnitsf04, yotalmth};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, billfreq01Out, billfreq02Out, billfreq03Out, maxprda01Out, pcunita01Out, pcinita01Out, maxprdb01Out, pcunitb01Out, pcinitb01Out, maxprdc01Out, pcunitc01Out, pcinitc01Out, maxprda02Out, pcunita02Out, pcinita02Out, maxprdb02Out, pcunitb02Out, pcinitb02Out, maxprdc02Out, pcunitc02Out, pcinitc02Out, maxprda03Out, pcunita03Out, pcinita03Out, maxprdb03Out, pcunitb03Out, pcinitb03Out, maxprdc03Out, pcunitc03Out, pcinitc03Out, maxprda04Out, pcunita04Out, pcinita04Out, maxprdb04Out, pcunitb04Out, pcinitb04Out, maxprdc04Out, pcunitc04Out, pcinitc04Out, billfreq04Out, billfreq05Out, billfreq06Out, maxprdd01Out, pcunitd01Out, pcinitd01Out, maxprde01Out, pcunite01Out, pcinite01Out, maxprdf01Out, pcunitf01Out, pcinitf01Out, maxprdd02Out, pcunitd02Out, pcinitd02Out, maxprde02Out, pcunite02Out, pcinite02Out, maxprdf02Out, pcunitf02Out, pcinitf02Out, maxprdd03Out, pcunitd03Out, pcinitd03Out, maxprde03Out, pcunite03Out, pcinite03Out, maxprdf03Out, pcunitf03Out, pcinitf03Out, maxprdd04Out, pcunitd04Out, pcinitd04Out, maxprde04Out, pcunite04Out, pcinite04Out, maxprdf04Out, pcunitf04Out, pcinitf04Out, yotalmthOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, billfreq01Err, billfreq02Err, billfreq03Err, maxprda01Err, pcunita01Err, pcinita01Err, maxprdb01Err, pcunitb01Err, pcinitb01Err, maxprdc01Err, pcunitc01Err, pcinitc01Err, maxprda02Err, pcunita02Err, pcinita02Err, maxprdb02Err, pcunitb02Err, pcinitb02Err, maxprdc02Err, pcunitc02Err, pcinitc02Err, maxprda03Err, pcunita03Err, pcinita03Err, maxprdb03Err, pcunitb03Err, pcinitb03Err, maxprdc03Err, pcunitc03Err, pcinitc03Err, maxprda04Err, pcunita04Err, pcinita04Err, maxprdb04Err, pcunitb04Err, pcinitb04Err, maxprdc04Err, pcunitc04Err, pcinitc04Err, billfreq04Err, billfreq05Err, billfreq06Err, maxprdd01Err, pcunitd01Err, pcinitd01Err, maxprde01Err, pcunite01Err, pcinite01Err, maxprdf01Err, pcunitf01Err, pcinitf01Err, maxprdd02Err, pcunitd02Err, pcinitd02Err, maxprde02Err, pcunite02Err, pcinite02Err, maxprdf02Err, pcunitf02Err, pcinitf02Err, maxprdd03Err, pcunitd03Err, pcinitd03Err, maxprde03Err, pcunite03Err, pcinite03Err, maxprdf03Err, pcunitf03Err, pcinitf03Err, maxprdd04Err, pcunitd04Err, pcinitd04Err, maxprde04Err, pcunite04Err, pcinite04Err, maxprdf04Err, pcunitf04Err, pcinitf04Err, yotalmthErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5536screen.class;
		protectRecord = S5536protect.class;
	}

}
