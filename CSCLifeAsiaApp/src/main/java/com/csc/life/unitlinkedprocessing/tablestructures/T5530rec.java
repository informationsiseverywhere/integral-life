package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:22
 * Description:
 * Copybook name: T5530REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5530rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5530Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData statfund = new FixedLengthStringData(1).isAPartOf(t5530Rec, 0);
  	public FixedLengthStringData statSect = new FixedLengthStringData(2).isAPartOf(t5530Rec, 1);
  	public FixedLengthStringData stsubsect = new FixedLengthStringData(4).isAPartOf(t5530Rec, 3);
  	public FixedLengthStringData filler = new FixedLengthStringData(493).isAPartOf(t5530Rec, 7, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5530Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5530Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}