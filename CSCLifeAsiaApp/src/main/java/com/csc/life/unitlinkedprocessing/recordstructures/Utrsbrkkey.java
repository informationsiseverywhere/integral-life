package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:52
 * Description:
 * Copybook name: UTRSBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsbrkFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData utrsbrkKey = new FixedLengthStringData(256).isAPartOf(utrsbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsbrkKey, 0);
  	public FixedLengthStringData utrsbrkChdrnum = new FixedLengthStringData(8).isAPartOf(utrsbrkKey, 1);
  	public FixedLengthStringData utrsbrkCoverage = new FixedLengthStringData(2).isAPartOf(utrsbrkKey, 9);
  	public FixedLengthStringData utrsbrkRider = new FixedLengthStringData(2).isAPartOf(utrsbrkKey, 11);
  	public PackedDecimalData utrsbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsbrkKey, 13);
  	public FixedLengthStringData utrsbrkLife = new FixedLengthStringData(2).isAPartOf(utrsbrkKey, 16);
  	public FixedLengthStringData utrsbrkUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrsbrkKey, 18);
  	public FixedLengthStringData utrsbrkUnitType = new FixedLengthStringData(1).isAPartOf(utrsbrkKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(utrsbrkKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}