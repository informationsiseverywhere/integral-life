/*
 * File: Br611.java
 * Date: 29 August 2009 22:23:44
 * Author: Quipoz Limited
 * 
 * Class transformed from BR611.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.csc.common.DD;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br610TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UstfpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UstmDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ustfpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.life.unitlinkedprocessing.reports.Rr611Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrncpy;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *                    UNIT STATEMENT PRINT
 *
 *     BR541 is a clone from B6508. This is a re-write of BR541
 *     in the new batch structure.
 *
 *     This program reads USTJ for all the trigger records wioth the
 *     associated Job Number.
 *
 *     It  will  produce a Summary Statement and optionally a Detail
 *     Statement if the Statement Level is set to 'D'.
 *
 * Summary Statement.
 * ------------------
 *
 *     The  first  USTF record for the contract with the most recent
 *     statement  number,  (ie. the latest obtained from the trigger
 *     record), is read.
 *
 *     All  the  USTF records for the contract and statement nunmber
 *     are processed and a table built up of all the funds and their
 *     closing  balances of deemed units. This table is then printed
 *     as the Summary report.
 *
 * Detail Statement.
 * -----------------
 *
 *     As  with  the  Summary  Statement  this is driven by the USTF
 *     records.
 *
 *     A new page is started for each new policy, life, component or
 *     fund.
 *
 *     If the transactions for a fund go across a page boundary then
 *     the  Fund/Currency  sub-heading  is  re-printed  but  the the
 *     Opening balance details are only printed on change of fund.
 *
 *     For  each  USTF  component, fund and statement number all the
 *     matching USTS records are read and their details printed. The
 *     Opening and Closing balances are printed from USTF.
 *
 *     If  Plan  processing  applies  and  a Summary record is being
 *     processed,  (Plan  Suffix = zero, and the policy number being
 *     processed   is  not  greater  than  the  number  of  policies
 *     summarised, CHDRULS-POLSUM), then all amounts will have to be
 *     divided by the number of policies summarised. Ensure that any
 *     rounding  discrepancies  are  absorbed  by  the  first policy
 *     printed.
 *
 *     If a different sequence is required for the processing of the
 *     Trigger records and/or the Transaction records then the files
 *     may be accessed using OPNQRYF in the CLP member CR541.
 *
 *#
 *    CONTROL TOTALS
 *    ==============
 *    01   Trigger Records Read.
 *    02   Statements Printed.
 *    03   Transactions Read.
 *    06   Pages Printed.
 *
 ***********************************************************************
 * </pre>
 */
public class Br611 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rr611Report printFile = new Rr611Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(350);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR611");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData sub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaInCount = new ZonedDecimalData(9, 0).setUnsigned();

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private ZonedDecimalData wsaaLinecount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPagenum = new ZonedDecimalData(4, 0).setUnsigned();
	private static final int wsaaMaxLines = 37;

	/* WSAA-SUMMARY-TABLE */
	private FixedLengthStringData wsaaFundTable = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaFund = FLSArrayPartOfStructure(50, 4, wsaaFundTable, 0);
	private FixedLengthStringData[] wsaaFundType = FLSArrayPartOfStructure(50, 4, wsaaFundTable, 200);

	private FixedLengthStringData wsaaDunitsTable = new FixedLengthStringData(450);
	private PackedDecimalData[] wsaaClDunits = PDArrayPartOfStructure(50, 16, 5, wsaaDunitsTable, 0);
	/* WSAA-STORE-COMPONENT */
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaStoreLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);

	private FixedLengthStringData wsspNameTab = new FixedLengthStringData(47).isAPartOf(wsspLongconfname, 0, REDEFINE);
	private FixedLengthStringData[] wsspName = FLSArrayPartOfStructure(47, 1, wsspNameTab, 0);
	private PackedDecimalData wsaaSummaryCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaStmtType = new FixedLengthStringData(1);
	private Validator wsaaNewStmt = new Validator(wsaaStmtType, "P");
	private Validator wsaaReprint = new Validator(wsaaStmtType, "R");
	private PackedDecimalData wsaaUnits = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaFundCash = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaContCash = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaTotalUnits = new PackedDecimalData(11, 5);
	private ZonedDecimalData wsaaCloBalUnits = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaTotnofunts = new ZonedDecimalData(13, 2);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(13, 2).setUnsigned();
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(20).init(SPACES);
	private PackedDecimalData wsaaFees = new PackedDecimalData(12, 5);
	private static final int wsaaTextfieldLen = 13;


	private FixedLengthStringData wsaaValidRecord = new FixedLengthStringData(1);
	private Validator validRecord = new Validator(wsaaValidRecord, "Y");
	private Validator invalidRecord = new Validator(wsaaValidRecord, "N");
	private FixedLengthStringData wsaaZrstmnoFound = new FixedLengthStringData(1);
	/*  Storage for T3695 table items for multi-languages*/
	//private static final int wsaaT3695Size = 150;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT3695Size = 1000;

	/* WSAA-T3695-ARRAY */
	private FixedLengthStringData[] wsaaT3695Rec = FLSInittedArray (1000, 23);
	private FixedLengthStringData[] wsaaT3695Key = FLSDArrayPartOfArrayStructure(3, wsaaT3695Rec, 0);
	private FixedLengthStringData[] wsaaT3695Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT3695Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT3695Language = FLSDArrayPartOfArrayStructure(1, wsaaT3695Key, 2, HIVALUES);
	private FixedLengthStringData[] wsaaT3695Data = FLSDArrayPartOfArrayStructure(20, wsaaT3695Rec, 3);
	private FixedLengthStringData[] wsaaT3695Longdesc = FLSDArrayPartOfArrayStructure(20, wsaaT3695Data, 0);
	/*  Storage for T5688 table items for multi-languages*/
	//private static final int wsaaT5688Size = 60;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;

	/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 24);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(4, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5688Language = FLSDArrayPartOfArrayStructure(1, wsaaT5688Key, 3, HIVALUES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(20, wsaaT5688Rec, 4);
	private FixedLengthStringData[] wsaaT5688Longdesc = FLSDArrayPartOfArrayStructure(20, wsaaT5688Data, 0);
	/*  Storage for T5515 table items for multi-languages*/
	//private static final int wsaaT5515Size = 30;
	//Ticket #5152 Job L2UNISTM2X Aborted
	//+ Increase size of wsaaT5515Size to 100
	//private static final int wsaaT5515Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5515Size = 1000;

	/* WSAA-T5515-ARRAY */
	private FixedLengthStringData[] wsaaT5515Rec = FLSInittedArray (1000, 50); //ILIFE-1651 START by nnazeer
	private FixedLengthStringData[] wsaaT5515Key = FLSDArrayPartOfArrayStructure(5, wsaaT5515Rec, 0);
	private FixedLengthStringData[] wsaaT5515Vrtfnd = FLSDArrayPartOfArrayStructure(4, wsaaT5515Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5515Language = FLSDArrayPartOfArrayStructure(1, wsaaT5515Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5515Data = FLSDArrayPartOfArrayStructure(20, wsaaT5515Rec, 5);
	private FixedLengthStringData[] wsaaT5515Longdesc = FLSDArrayPartOfArrayStructure(20, wsaaT5515Data, 0);
	/*  Storage for T3645 table items for multi-languages*/
	//private static final int wsaaT3645Size = 250;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT3645Size = 1000;

	/* WSAA-T3645-ARRAY */
	private FixedLengthStringData[] wsaaT3645Rec = FLSInittedArray (1000, 24);
	private FixedLengthStringData[] wsaaT3645Key = FLSDArrayPartOfArrayStructure(4, wsaaT3645Rec, 0);
	private FixedLengthStringData[] wsaaT3645Ctrycode = FLSDArrayPartOfArrayStructure(3, wsaaT3645Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT3645Language = FLSDArrayPartOfArrayStructure(1, wsaaT3645Key, 3, HIVALUES);
	private FixedLengthStringData[] wsaaT3645Data = FLSDArrayPartOfArrayStructure(20, wsaaT3645Rec, 4);
	private FixedLengthStringData[] wsaaT3645Longdesc = FLSDArrayPartOfArrayStructure(20, wsaaT3645Data, 0);
	/*  Storage for T5687 table items for multi-languages*/
	//private static final int wsaaT5687Size = 200;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5687Size = 1000;

	/* WSAA-T5687-ARRAY */
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (1000, 25);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(5, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5687Language = FLSDArrayPartOfArrayStructure(1, wsaaT5687Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(20, wsaaT5687Rec, 5);
	private FixedLengthStringData[] wsaaT5687Longdesc = FLSDArrayPartOfArrayStructure(20, wsaaT5687Data, 0);
	/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	/* TABLES */

	private static final String e308 = "E308";
	private static final String h053 = "H053";
	private static final String h791 = "H791";
	private static final String h115 = "H115";
	private static final String h135 = "H135";
	private static final String f993 = "F993";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct06 = 6;

	/* INDIC-AREA */
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rr611h01Record = new FixedLengthStringData(162);
	private FixedLengthStringData rr611h01O = new FixedLengthStringData(162).isAPartOf(rr611h01Record, 0);
	private FixedLengthStringData stmtdate = new FixedLengthStringData(10).isAPartOf(rr611h01O, 0);
	private ZonedDecimalData pagenum = new ZonedDecimalData(4, 0).isAPartOf(rr611h01O, 10);
	private FixedLengthStringData asgname1 = new FixedLengthStringData(30).isAPartOf(rr611h01O, 14);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr611h01O, 44);
	private FixedLengthStringData asgname2 = new FixedLengthStringData(30).isAPartOf(rr611h01O, 52);
	private FixedLengthStringData name1 = new FixedLengthStringData(30).isAPartOf(rr611h01O, 82);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rr611h01O, 112);
	private FixedLengthStringData name2 = new FixedLengthStringData(30).isAPartOf(rr611h01O, 115);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(rr611h01O, 145);

	private FixedLengthStringData rr611h02Record = new FixedLengthStringData(30+DD.cltaddr.length*5);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData rr611h02O = new FixedLengthStringData(30+DD.cltaddr.length*5).isAPartOf(rr611h02Record, 0);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr611h02O, 0);
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr611h02O, DD.cltaddr.length);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr611h02O, DD.cltaddr.length*2);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr611h02O, DD.cltaddr.length*3);
	private FixedLengthStringData addr5 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr611h02O, DD.cltaddr.length*4);
	private FixedLengthStringData ctypedes = new FixedLengthStringData(30).isAPartOf(rr611h02O, DD.cltaddr.length*5);

	private FixedLengthStringData rr611h03Record = new FixedLengthStringData(30);
	private FixedLengthStringData rr611h03O = new FixedLengthStringData(30).isAPartOf(rr611h03Record, 0);
	private FixedLengthStringData vrtfnddsc = new FixedLengthStringData(30).isAPartOf(rr611h03O, 0);

	private FixedLengthStringData rr611h04Record = new FixedLengthStringData(24);
	private FixedLengthStringData rr611h04O = new FixedLengthStringData(24).isAPartOf(rr611h04Record, 0);
	private FixedLengthStringData stopdt = new FixedLengthStringData(10).isAPartOf(rr611h04O, 0);
	private ZonedDecimalData zrtotnun = new ZonedDecimalData(14, 3).isAPartOf(rr611h04O, 10);

	private FixedLengthStringData rr611h05Record = new FixedLengthStringData(42);
	private FixedLengthStringData rr611h05O = new FixedLengthStringData(42).isAPartOf(rr611h05Record, 0);
	private FixedLengthStringData stopdt1 = new FixedLengthStringData(10).isAPartOf(rr611h05O, 0);
	private ZonedDecimalData uoffpr02 = new ZonedDecimalData(9, 5).isAPartOf(rr611h05O, 10);
	private ZonedDecimalData ubidpr02 = new ZonedDecimalData(9, 5).isAPartOf(rr611h05O, 19);
	private ZonedDecimalData zrtotnun1 = new ZonedDecimalData(14, 3).isAPartOf(rr611h05O, 28);

	private FixedLengthStringData rr611d01Record = new FixedLengthStringData(68);
	private FixedLengthStringData rr611d01O = new FixedLengthStringData(68).isAPartOf(rr611d01Record, 0);
	private FixedLengthStringData moniesdt = new FixedLengthStringData(10).isAPartOf(rr611d01O, 0);
	private ZonedDecimalData zrordpay = new ZonedDecimalData(9, 2).isAPartOf(rr611d01O, 10);
	private FixedLengthStringData textfield = new FixedLengthStringData(13).isAPartOf(rr611d01O, 19);
	private ZonedDecimalData uoffpr = new ZonedDecimalData(9, 5).isAPartOf(rr611d01O, 32);
	private ZonedDecimalData ubidpr = new ZonedDecimalData(9, 5).isAPartOf(rr611d01O, 41);
	private ZonedDecimalData zrbonprd1 = new ZonedDecimalData(9, 3).isAPartOf(rr611d01O, 50);
	private ZonedDecimalData zrbonprd2 = new ZonedDecimalData(9, 3).isAPartOf(rr611d01O, 59);

	private FixedLengthStringData rr611s01Record = new FixedLengthStringData(23);
	private FixedLengthStringData rr611s01O = new FixedLengthStringData(23).isAPartOf(rr611s01Record, 0);
	private FixedLengthStringData stmtdate1 = new FixedLengthStringData(10).isAPartOf(rr611s01O, 0);
	private ZonedDecimalData zrtotvlu = new ZonedDecimalData(13, 2).isAPartOf(rr611s01O, 10);

	private FixedLengthStringData rr611s02Record = new FixedLengthStringData(110);
	private FixedLengthStringData rr611s02O = new FixedLengthStringData(110).isAPartOf(rr611s02Record, 0);
	private FixedLengthStringData despname1 = new FixedLengthStringData(47).isAPartOf(rr611s02O, 0);
	private FixedLengthStringData cltphone = new FixedLengthStringData(16).isAPartOf(rr611s02O, 47);
	private FixedLengthStringData despname2 = new FixedLengthStringData(47).isAPartOf(rr611s02O, 63);

	private FixedLengthStringData rr611s03Record = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT3695Ix = new IntegerData();
	private IntegerData wsaaT5688Ix = new IntegerData();
	private IntegerData wsaaT5515Ix = new IntegerData();
	private IntegerData wsaaT3645Ix = new IntegerData();
	private IntegerData wsaaT5687Ix = new IntegerData();
	//private ChdrulsTableDAM chdrulsIO = new ChdrulsTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T1693rec t1693rec = new T1693rec();
	private T5645rec t5645rec = new T5645rec();
	private T5515rec t5515rec = new T5515rec();
	private Dbcstrncpy dbcstrncpy = new Dbcstrncpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();


	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private UstmData ustmData ;

	private List<Descpf> descpfList;
	private Descpf descpf = null;
	private int rowCount;
	List<String> chdrnumList = null;

	private UstmDAO ustmDAO = getApplicationContext().getBean("ustmDAO", UstmDAO.class);
	List<UstmData> ustjList;
	Map<String,List<UstmData>> ustmMap ; 

	private UstfpfDAO ustfpfDAO = getApplicationContext().getBean("ustfpfDAO", UstfpfDAO.class);
	private Ustfpf ustfpf;
	Map<String,List<Ustfpf>> ustfMap ; 
	List<Ustfpf> ustfpfList;
	private Iterator<Ustfpf> ustfpfIter; 

	private Br610TempDAO br610TempDAO = getApplicationContext().getBean("br610TempDAO", Br610TempDAO.class);
	Map<String,List<Br610DTO>> dataMap ;  
	
	List<Br610DTO> chdrenqList;
	Br610DTO chdrenq;

	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf;
	Map<String,List<Covrpf>> covrMap ; 
	List<Covrpf> covrpfList;
	private Iterator<Covrpf> covrpfIter; 

	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	Clntpf clntpf;

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	private Map<String, List<Itempf>> t5515ListMap;
	private Map<String, List<Itempf>> t5645ListMap;
	private Map<String, List<Itempf>> t1693ListMap;
	private List<Itempf> itempfList;

	private int ctrCT01; 
	private int ctrCT02;
	private int ctrCT06;
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	List<Zrstpf> zrstpfList;
	Map<String,List<Zrstpf>> zrstpfMap ; 
	private Zrstpf zrstpf;
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	
	List<Utrnpf> utrnpfList;
	Map<String,List<Utrnpf>> utrnpfMap ; 
	private Utrnpf utrnpf;
	private Clntpf cltsIO = null;
	private int minRecord = 1;
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextCovr3600a, 
		exit3600a
	}

	public Br611() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

	protected void initialise1000()
	{
		initialise1010();
	}

	protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaStmtType.set(bprdIO.getSystemParam01());
		wsaaOverflow.set("N");
		t5515ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString().trim(), "T5515");
		t1693ListMap = itemDAO.loadSmartTable("IT", "0", "T1693");
		t5645ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString().trim(), "T5645");
		b100ReadT1693();
		b300ReadT5645();

		descpf = new Descpf();
		descpfList = descDAO.getItems("IT", bsprIO.getCompany().toString(), "T3695");
		if(descpfList==null ||(descpfList !=null && descpfList.size()==0)){
			return;
		}
		wsaaT3695Ix.set(1);
		if(descpfList.size() > 0) {
			for (Descpf desc : descpfList) {
				descpf = desc;
				loadT36951100();
			}
		} 

		descpf = new Descpf();
		descpfList.clear();
		descpfList = descDAO.getItems("IT", bsprIO.getCompany().toString(), "T5688");
		if(descpfList==null ||(descpfList !=null && descpfList.size()==0)){
			return;
		}
		wsaaT5688Ix.set(1);
		if(descpfList.size() > 0) {
			for (Descpf desc : descpfList) {
				descpf = desc;
				loadT56881200();
			}
		} 

		descpf = new Descpf();
		descpfList.clear();
		descpfList = descDAO.getItems("IT", bsprIO.getCompany().toString(), "T5515");
		if(descpfList==null ||(descpfList !=null && descpfList.size()==0)){
			return;
		}
		wsaaT5515Ix.set(1);
		if(descpfList.size() > 0) {
			for (Descpf desc : descpfList) {
				descpf = desc;
				loadT55151300();
			}
		} 

		descpf = new Descpf();
		descpfList.clear();
		descpfList = descDAO.getItems("IT", t1693rec.fsuco.toString(), "T3645");
		if(descpfList==null ||(descpfList !=null && descpfList.size()==0)){
			return;
		}
		wsaaT3645Ix.set(1);
		if(descpfList.size() > 0) {
			for (Descpf desc : descpfList) {
				descpf = desc;
				loadT36451400();			
			}
		} 

		descpf = new Descpf();
		descpfList.clear();
		descpfList = descDAO.getItems("IT", bsprIO.getCompany().toString(), "T5687");
		if(descpfList==null ||(descpfList !=null && descpfList.size()==0)){
			return;
		}
		wsaaT5687Ix.set(1);
		if(descpfList.size() > 0) {
			for (Descpf desc : descpfList) {
				descpf = desc;
				loadT56871500();			
			}
		} 

		rowCount = bprdIO.cyclesPerCommit.toInt();
		if(rowCount <= 0)
			rowCount = 1000; //default size set as 1000  to ensure minimal speed
		printFile.openOutput();
		ctrCT01 = 0; 
		ctrCT02 = 0;
		ctrCT06 = 0;

	}

	protected void loadT36951100()
	{
		start1110();
	}

	protected void start1110()
	{
		if (isGT(wsaaT3695Ix,wsaaT3695Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("T3695");
			fatalError600();
		}
		if (isNE(descpf.getLanguage(),bsscIO.getLanguage())) {
			//descIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaT3695Sacstype[wsaaT3695Ix.toInt()].set(descpf.getDescitem());
		wsaaT3695Language[wsaaT3695Ix.toInt()].set(descpf.getLanguage());
		wsaaT3695Longdesc[wsaaT3695Ix.toInt()].set(descpf.getShortdesc());
		//	descIO.setFunction(varcom.nextr);
		wsaaT3695Ix.add(1);
	}

	protected void loadT56881200()
	{
		start1210();
	}

	protected void start1210()
	{
		if (isGT(wsaaT5688Ix,wsaaT5688Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("T5688");
			fatalError600();
		}
		if (isNE(descpf.getLanguage(),bsscIO.getLanguage())) {
			//descIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaT5688Cnttype[wsaaT5688Ix.toInt()].set(descpf.getDescitem());
		wsaaT5688Language[wsaaT5688Ix.toInt()].set(descpf.getLanguage());
		wsaaT5688Longdesc[wsaaT5688Ix.toInt()].set(descpf.getShortdesc());
		//descIO.setFunction(varcom.nextr);
		wsaaT5688Ix.add(1);
	}

	protected void loadT55151300()
	{
		start1310();
	}

	protected void start1310()
	{
		if (isGT(wsaaT5515Ix,wsaaT5515Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("T5515");
			fatalError600();
		}
		if (isNE(descpf.getLanguage(),bsscIO.getLanguage())) {
			//descIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaT5515Vrtfnd[wsaaT5515Ix.toInt()].set(descpf.getDescitem());
		wsaaT5515Language[wsaaT5515Ix.toInt()].set(descpf.getLanguage());
		wsaaT5515Longdesc[wsaaT5515Ix.toInt()].set(descpf.getShortdesc());
		//	descIO.setFunction(varcom.nextr);
		wsaaT5515Ix.add(1);
	}

	protected void loadT36451400()
	{
		start1410();
	}

	protected void start1410()
	{
		if (isGT(wsaaT3645Ix,wsaaT3645Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("T3645");
			fatalError600();
		}
		if (isNE(descpf.getLanguage(),bsscIO.getLanguage())) {
			//descIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaT3645Ctrycode[wsaaT3645Ix.toInt()].set(descpf.getDescitem());
		wsaaT3645Language[wsaaT3645Ix.toInt()].set(descpf.getLanguage());
		wsaaT3645Longdesc[wsaaT3645Ix.toInt()].set(descpf.getShortdesc());
		//descIO.setFunction(varcom.nextr);
		wsaaT3645Ix.add(1);
	}

	protected void loadT56871500()
	{
		start1510();
	}

	protected void start1510()
	{
		if (isGT(wsaaT5687Ix,wsaaT5687Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("T5687");
			fatalError600();
		}
		if (isNE(descpf.getLanguage(),bsscIO.getLanguage())) {
			// descIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaT5687Crtable[wsaaT5687Ix.toInt()].set(descpf.getDescitem());
		wsaaT5687Language[wsaaT5687Ix.toInt()].set(descpf.getLanguage());
		wsaaT5687Longdesc[wsaaT5687Ix.toInt()].set(descpf.getShortdesc());
		//descIO.setFunction(varcom.nextr);
		wsaaT5687Ix.add(1);
	}

	protected void readFile2000()
	{
		readFile2010();
	}

	protected void readFile2010()
	{
		ustmMap = ustmDAO.readUstjData(bsscIO.getScheduleNumber().toInt(),bprdIO.getCompany().toString(),minRecord,minRecord+rowCount);
		if(ustmMap==null ||(ustmMap !=null && ustmMap.size()==0)){
			wsspEdterror.set(varcom.endp);
			return;

		}
		Set<String> keys = ustmMap.keySet();
		chdrnumList = new ArrayList<String>();
		for (String key:keys)
		{
			chdrnumList.add(key.substring(0,8));
		}

		// fix the ticket ILIFE-8360
		// The 'in' sql statement is too long
		if (chdrnumList.size() > 1000) {
			ustfMap = new HashMap<>();
			dataMap = new HashMap<>();
			zrstpfMap = new HashMap<>();
			utrnpfMap = new HashMap<>();
			int count = chdrnumList.size() / 1000 + 1;
            for (int i = 0; i < count; i++) {
                int idx = 1000 * (i + 1);
                if (idx > chdrnumList.size()) {
                    idx = chdrnumList.size();
                }
                Map<String,List<Ustfpf>> tmpUstf=ustfpfDAO.
                		readUstfData(bprdIO.getCompany().toString(),chdrnumList.subList(1000*i,idx));
                tmpUstf.forEach((key, list) -> {
                	if (ustfMap.containsKey(key)) {
                		ustfMap.get(key).addAll(list);
    				} else {
    					ustfMap.put(key, list);
    				}
                });
                Map<String, List<Br610DTO>> tmpData=br610TempDAO.
                		getChdrulsRecord(bprdIO.getCompany().toString(),chdrnumList.subList(1000*i,idx));
                tmpData.forEach((key, list) -> {
                	if (dataMap.containsKey(key)) {
    					dataMap.get(key).addAll(list);
    				} else {
    					dataMap.put(key, list);
    				}
                });
                Map<String,List<Zrstpf>> tmpZrst=zrstpfDAO.
                		searchZrststmByChdrnum(chdrnumList.subList(1000*i,idx),bprdIO.getCompany().toString());
                tmpZrst.forEach((key, list) -> {
                	if (zrstpfMap.containsKey(key)) {
                		zrstpfMap.get(key).addAll(list);
    				} else {
    					zrstpfMap.put(key, list);
    				}
                });
                Map<String,List<Utrnpf>> tmpUtrn= utrnpfDAO.
                		searchUstsByChdrnum(chdrnumList.subList(1000*i,idx),bprdIO.getCompany().toString());
                tmpUtrn.forEach((key, list) -> {
                	if (utrnpfMap.containsKey(key)) {
    					utrnpfMap.get(key).addAll(list);
    				} else {
    					utrnpfMap.put(key, list);
    				}
                });
            }
		} else {
			ustfMap = ustfpfDAO.readUstfData(bprdIO.getCompany().toString(),chdrnumList);
			dataMap = br610TempDAO.getChdrulsRecord(bprdIO.getCompany().toString(),chdrnumList);
			zrstpfMap = zrstpfDAO.searchZrststmByChdrnum(chdrnumList,bprdIO.getCompany().toString());
			utrnpfMap =utrnpfDAO.searchUstsByChdrnum(chdrnumList,bprdIO.getCompany().toString());
		}
		// end ILIFE-8360
		covrMap = covrpfDAO.readCovrData(bprdIO.getCompany().toString(),chdrnumList);

		OuterLoop : for(Map.Entry<String, List<UstmData>> entry : ustmMap.entrySet()) {
			ctrCT01++;
			String key = entry.getKey();
			ustjList = ustmMap.get(key);
			ustmData = ustjList.get(0);
			            boolean chk = false;
			            if(zrstpfMap.containsKey(key)){
				zrstpfList = zrstpfMap.get(key);
			                if(dataMap.containsKey(key))
			                {
				chdrenqList = dataMap.get(key);
				chdrenq = chdrenqList.get(0);
			                    chk = true;     
			                }
			                if(chk && ustfMap.containsKey(key))
			                {
				ustfpfList = ustfMap.get(key);
			                }
			                else
			                {
			                    chk = false;
			                }                
			                if(chk && utrnpfMap.containsKey(key)){
			                	utrnpfList = utrnpfMap.get(key);
			                }
			                else {
			                	chk = false;
			                }
			            } 
			            if(!chk)
			            {
			                continue OuterLoop ;
			            }
			covrpfList = covrMap.get(key) ;

			if (isNE(ustmData.getStmtType(),wsaaStmtType)) {
				wsspEdterror.set(SPACES);
				return ;
			}	
			summaryStatement3100();
			if (isEQ(ustmData.getUnitStmtFlag(),"D")) {
				detailStatement3200();
			}


		}

	}

	protected void edit2500()
	{
		/*EDIT*/
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*if (isNE(ustjIO.getStmtType(),wsaaStmtType)) {
			wsspEdterror.set(SPACES);
			return ;
		}*/
		/*EXIT*/
	}

	protected void update3000()
	{
		/*UPDATE*/
		ctrCT02++;
		minRecord +=rowCount;
		/*EXIT*/
	}

	protected void summaryStatement3100()
	{
		para3100();
	}

	protected void para3100()
	{
		chdrnum.set(ustmData.getChdrnum());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ustmData.getStrpdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		stmtdate.set(datcon1rec.extDate);
		stmtdate1.set(datcon1rec.extDate);


		wsaaT5688Ix.set(1);
		searchlabel1:
		{
			for (; isLT(wsaaT5688Ix,wsaaT5688Rec.length); wsaaT5688Ix.add(1)){
				if (isEQ(wsaaT5688Cnttype[wsaaT5688Ix.toInt()],chdrenq.getCnttype())
						&& isEQ(wsaaT5688Language[wsaaT5688Ix.toInt()],bsscIO.getLanguage())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(e308);
			StringUtil stringVariable1 = new StringUtil();
			String stringVariable2;
			stringVariable1.addExpression("T5688");
			stringVariable2 = chdrenq.getChdruls_chdrcoy()+chdrenq.getChdruls_chdrnum();
			FixedLengthStringData groupTEMP = new FixedLengthStringData(stringVariable2);

			stringVariable1.addExpression(groupTEMP);
			//chdrulsIO.setParams(groupTEMP);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*    MOVE WSAA-LONGDESC (WSAA-T5688-IX)                           */
		/*                                TO CTYPEDES OF RR611H02-O.       */
		ctypedes.set(wsaaT5688Longdesc[wsaaT5688Ix.toInt()]);
		cntcurr.set(chdrenq.getCntcurr());
		/* Obtain the Life Assured client number and name.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/

		clntpf = clntpfDAO.findClient(t1693rec.fsuco.toString(),"CN",chdrenq.getLifcnum());
		if(null == clntpf){
			fatalError600();
		}

		plainname();
		if (isEQ(wsspName[31],SPACES)
				&& isEQ(wsspName[32],SPACES)) {
			name1.set(wsspLongconfname);
		}
		else {
			if (isEQ(clntpf.getEthorig(),"1")) {
				name1.set(clntpf.getSurname());
				name2.set(clntpf.getGivname());
			}
			else {
				name1.set(clntpf.getGivname());
				name2.set(clntpf.getSurname());
			}
		}
		/* to get additional information for new format of sttm printing*/
		extractDetails3500a();
		sub.set(1);

		if(null != ustfpfList && ustfpfList.size() > 0){
			ustfpfIter = ustfpfList.iterator();
			while(ustfpfIter.hasNext()){
				ustfpf = ustfpfIter.next();
				if(ustfpf.getUstmno() == ustmData.getUstmno())	{

					buildFundTable2100();
				}
			}
		}
		sub.set(1);
	}
	protected void largename()
	{
		/*LGNM-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

	protected void plainname()
	{
		/*PLAIN-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
		}
		else {
			wsspLongconfname.set(clntpf.getSurname());
		}
	}

	protected void payeename()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspLongconfname);
		/*PAYEE-EXIT*/
	}

	protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspLongconfname);
		/*CORP-EXIT*/
	}

	/**
	 * <pre>
	 *  END OF CONFNAME **********************************************
	 * </pre>
	 */
	protected void buildFundTable2100()
	{
		para2100();
	}

	protected void para2100()
	{
		sub.add(1);
	}

	protected void summaryHeadings2400()
	{
		para2400();
	}

	protected void para2400()
	{
		wsaaOverflow.set("N");
		wsaaLinecount.set(11);
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		printRecord.set(SPACES);
		//printFile.printRr611h01(rr611h01Record);
		// fix bug1116
		printFile.printRr611h01(rr611h01Record, indicArea);
		/*                              INDICATORS INDIC-AREA.           */
		ctrCT06++;
		/*    If a reprint is in progress print the Reprint lines from*/
		/*    T5678.*/
		if (wsaaReprint.isTrue()) {
			printRecord.set(SPACES);
			printRecord.set(SPACES);
		}
	}

	protected void detailStatement3200()
	{
		para3200();
	}
	protected void para3200()
	{
		wsaaLinecount.set(ZERO);
		wsaaTotnofunts.set(ZERO);
		wsaaTotalUnits.set(ZERO);
		wsaaPagenum.set(ZERO);
		wsaaSummaryCount.set(ZERO);
		/*   Read the first USTF record for the contract with the most*/
		/*   recent statement number, ie. the latest.*/
		
		ustfpfIter = ustfpfList.iterator();
		while(ustfpfIter.hasNext()){
			ustfpf = ustfpfIter.next();
			if(ustfpf.getUstmno()== ustmData.getUstmno()){
			processUstfHeaders3250();
			}
		}
	}

	protected void processUstfHeaders3250()
	{
		para3250();
	}

	protected void para3250()
	{
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		if (isEQ(ustfpf.getPlnsfx(),ZERO)
				&& isNE(chdrenq.getPolsum(),ZERO)) {
			wsaaSummaryCount.add(1);
		}
		printRecord.set(SPACES);
		// fix bug1116
		printFile.printRr611h01(rr611h01Record, indicArea);
		// end
		//printFile.printRr611h01(rr611h01Record);
		printRecord.set(SPACES);
		printFile.printRr611h02(rr611h02Record);
		wsaaOverflow.set("N");
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		wsaaLinecount.set(21);
		/*    Store the component from the USTF record.*/
		wsaaStorePlanSuffix.set(ustfpf.getPlnsfx());
		/*    A new page will be started for each new component.*/
		/*    If the transactions for a single fund go across a page*/
		/*    boundary then the Fund/Currency sub-heading is re-printed*/
		/*    but the Opening Balance details are only printed on change*/
		/*    of Fund.*/
		/*    Do not check for change in sttmt no., Life, Coverage, rider,*/
		/*    plan suffix*/
	
		
		ustfpfIter = ustfpfList.iterator();
		while(ustfpfIter.hasNext()){
			ustfpf = ustfpfIter.next();
			processUstfComponent3260();
		}

		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		/* To print the bottom portion of the statment after all the*/
		/* details line have been printed*/
		zrtotvlu.set(wsaaTotnofunts);
		printRecord.set(SPACES);
		printFile.printRr611s01(rr611s01Record);
		getAgent3700a();
		printRecord.set(SPACES);
		printFile.printRr611s02(rr611s02Record);
		/*   If we are still processing the summary records then re-read*/
		/*   the first USTF record for the contract with the most and*/
		/*   re-process all the USTF records for the summary.*/
		if (isEQ(wsaaStorePlanSuffix,ZERO)
				&& isNE(chdrenq.getPolsum(),ZERO)
				&& isLT(wsaaSummaryCount,chdrenq.getPolsum())) {
			ustfpfIter = ustfpfList.iterator();
			while(ustfpfIter.hasNext()){
				ustfpf = ustfpfIter.next();
				para3250();
				return ;
			}
		}
	}

	protected void processUstfComponent3260()
	{
		para3260();
	}

	protected void para3260()
	{
		/* To initialize total units at new or change of fund*/
		wsaaTotalUnits.set(ZERO);
		wsaaCloBalUnits.set(ZERO);
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}

		boolean itemFound = false;
		String keyItemitem = ustfpf.getVrtfnd().trim();
		if (t5515ListMap.containsKey(keyItemitem)){	
			itempfList = t5515ListMap.get(keyItemitem);
			t5515rec.t5515Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));	
			itemFound = true;
		}

		if (!itemFound) {
			syserrrec.params.set(t5515rec.t5515Rec);
			syserrrec.statuz.set(h053);
			fatalError600();		
		}	


		/*    Obtain the Fund description from T5515.*/
		wsaaT5515Ix.set(1);
		searchlabel1:
		{
			for (; isLT(wsaaT5515Ix,wsaaT5515Rec.length); wsaaT5515Ix.add(1)){
				if (isEQ(wsaaT5515Vrtfnd[wsaaT5515Ix.toInt()],ustfpf.getVrtfnd())
						&& isEQ(wsaaT5515Language[wsaaT5515Ix.toInt()],bsscIO.getLanguage())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(h115);
			StringUtil stringVariable1 = new StringUtil();
			String stringVariable2 ;
			stringVariable1.addExpression("T5515");
			stringVariable2 = chdrenq.getChdruls_chdrcoy()+chdrenq.getChdruls_chdrnum();
			FixedLengthStringData groupTEMP = new FixedLengthStringData(stringVariable2);
			stringVariable1.addExpression(groupTEMP);
			//chdrulsIO.setParams(groupTEMP);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		vrtfnddsc.set(wsaaT5515Longdesc[wsaaT5515Ix.toInt()]);
		if (isGT(wsaaLinecount,wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr611s03(rr611s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		else {
			printRecord.set(SPACES);
			printFile.printRr611h03(rr611h03Record);
			wsaaLinecount.add(3);
		}
		/*    Set up Opening Details from USTF.*/
		if (isEQ(ustfpf.getStopdt(),ZERO)) {
			stopdt.set(SPACES);
		}
		else {
			datcon1rec.function.set(varcom.conv);
			datcon1rec.intDate.set(ustfpf.getStopdt());
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			stopdt.set(datcon1rec.extDate);
		}
		if (isEQ(ustfpf.getPlnsfx(),ZERO)
				&& isNE(chdrenq.getPolsum(),ZERO)) {
			compute(wsaaUnits, 6).setRounded(div(ustfpf.getStopdun(),chdrenq.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustfpf.getStofuca(),chdrenq.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustfpf.getStopcoca(), chdrenq.getPolsum()));
			zrtotnun.set(wsaaUnits);
			wsaaTotalUnits.set(wsaaUnits);
		}
		else {
			wsaaTotalUnits.set(ustfpf.getStopdun());
			zrtotnun.setRounded(ustfpf.getStopdun());
		}
		if (isNE(wsaaFundCash, 0)) {
			zrdecplrec.amountIn.set(wsaaFundCash);
			zrdecplrec.currency.set(t5515rec.currcode);
			a000CallRounding();
			wsaaFundCash.set(zrdecplrec.amountOut);
		}
		if (isNE(wsaaContCash, 0)) {
			zrdecplrec.amountIn.set(wsaaContCash);
			zrdecplrec.currency.set(chdrenq.getCntcurr());
			a000CallRounding();
			wsaaContCash.set(zrdecplrec.amountOut);
		}
		if (isGT(wsaaLinecount,wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr611s03(rr611s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		printRecord.set(SPACES);
		printFile.printRr611h04(rr611h04Record);
		wsaaLinecount.add(1);
		
		Iterator<Utrnpf> utrnpfIter = utrnpfList.iterator();
		while(utrnpfIter.hasNext()){
			utrnpf = utrnpfIter.next();
			if (isEQ(utrnpf.getUnitVirtualFund(),ustfpf.getVrtfnd())
					&& isEQ(utrnpf.getUnitType(),ustfpf.getUnityp())) {
			processUstsTransactions3300();
			}
		}

		/* To obtain bid price and offer price*/
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(ustfpf.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(ustfpf.getVrtfnd());
		vprnudlIO.setUnitType(ustfpf.getUnityp());
		vprnudlIO.setEffdate(ustfpf.getStcldt());
		vprnudlIO.setFunction(varcom.begn);
		vprnudlIO.setFormat(formatsInner.vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vprnudlIO.getParams());
			syserrrec.statuz.set(vprnudlIO.getStatuz());
			fatalError600();
		}
		uoffpr02.set(vprnudlIO.getUnitOfferPrice());
		ubidpr02.set(vprnudlIO.getUnitBidPrice());
		/*    Set up Closing Details from USTF.*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ustfpf.getStcldt());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		stopdt1.set(datcon1rec.extDate);
		if (isEQ(ustfpf.getPlnsfx(),ZERO)
				&& isNE(chdrenq.getPolsum(),ZERO)) {
			compute(wsaaUnits, 6).setRounded(div(ustfpf.getStcldun(),chdrenq.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustfpf.getStclfuca(),chdrenq.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustfpf.getStclcoca(), chdrenq.getPolsum()));
			zrtotnun1.set(wsaaUnits);
			wsaaCloBalUnits.set(wsaaUnits);
		}
		else {
			wsaaCloBalUnits.set(ustfpf.getStcldun());
			zrtotnun1.setRounded(ustfpf.getStcldun());
		}
		if (isNE(wsaaFundCash, 0)) {
			zrdecplrec.amountIn.set(wsaaFundCash);
			zrdecplrec.currency.set(t5515rec.currcode);
			a000CallRounding();
			wsaaFundCash.set(zrdecplrec.amountOut);
		}
		if (isNE(wsaaContCash, 0)) {
			zrdecplrec.amountIn.set(wsaaContCash);
			zrdecplrec.currency.set(chdrenq.getCntcurr());
			a000CallRounding();
			wsaaContCash.set(zrdecplrec.amountOut);
		}
		unitsValue3800a();
		if (isGT(wsaaLinecount,wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr611s03(rr611s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		printRecord.set(SPACES);
		printFile.printRr611h05(rr611h05Record);
		wsaaLinecount.add(1);
		/*    Read the next USTF record.*/
	}

	protected void processUstsTransactions3300()
	{
		para3300();
		callUsts33a0();
	}

	protected void para3300()
	{
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
	     Iterator<Zrstpf> zrstpfIter;
		/*    Set up Transaction Details and write details line D01.*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(utrnpf.getMoniesDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		moniesdt.set(datcon1rec.extDate);
		/* To obtain bid price and offer price*/
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(ustfpf.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(ustfpf.getVrtfnd());
		vprnudlIO.setUnitType(ustfpf.getUnityp());
		vprnudlIO.setEffdate(utrnpf.getPriceDateUsed());
		vprnudlIO.setFunction(varcom.readr);
		vprnudlIO.setFormat(formatsInner.vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vprnudlIO.getParams());
			syserrrec.statuz.set(vprnudlIO.getStatuz());
			fatalError600();
		}
		uoffpr.set(vprnudlIO.getUnitOfferPrice());
		ubidpr.set(vprnudlIO.getUnitBidPrice());
		if (isEQ(ustfpf.getPlnsfx(),ZERO)
				&& isNE(chdrenq.getPolsum(),ZERO)) {
			compute(wsaaUnits, 6).setRounded(div(utrnpf.getNofDunits(),chdrenq.getPolsum()));
			compute(wsaaFundCash, 5).set(div(utrnpf.getFundAmount(),chdrenq.getPolsum()));
			compute(wsaaContCash, 5).set(div(utrnpf.getContractAmount(), chdrenq.getPolsum()));
			if (isNE(wsaaFundCash, 0)) {
				zrdecplrec.amountIn.set(wsaaFundCash);
				zrdecplrec.currency.set(t5515rec.currcode);
				a000CallRounding();
				wsaaFundCash.set(zrdecplrec.amountOut);
			}
			if (isNE(wsaaContCash, 0)) {
				zrdecplrec.amountIn.set(wsaaContCash);
				zrdecplrec.currency.set(chdrenq.getCntcurr());
				a000CallRounding();
				wsaaContCash.set(zrdecplrec.amountOut);
			}
			zrbonprd1.setRounded(wsaaUnits);
			wsaaTotalUnits.add(wsaaUnits);
			zrbonprd2.setRounded(wsaaTotalUnits);
			zrordpay.set(wsaaFundCash);
		}
		else {
			/* Splitting mortality charges and fees*/
			if (isEQ(utrnpf.getBatctrcde(),"B633")) {
				zrstpfIter = zrstpfList.iterator();
				while(zrstpfIter.hasNext()){
					zrstpf = zrstpfIter.next();
					b100ReadZrstmno();
				}

				if (isEQ(wsaaZrstmnoFound,"Y")) {
					return ;
				}
			}
		}
		if (isNE(utrnpf.getBatctrcde(),"B633")
				|| isEQ(wsaaZrstmnoFound,SPACES)) {
			if (isEQ(chdrenq.getBillfreq(),"00")) {
				wsaaSacstype.set(t5645rec.sacstype01);
			}
			else {
				wsaaSacstype.set(t5645rec.sacstype02);
			}
			if (isEQ(utrnpf.getSacstyp(),"EN")) {
				wsaaSacstype.set(utrnpf.getSacstyp());
			}
			b400ReadT3695();
			dbcstrncpy.dbcsInputString.set(wsaaLongdesc);
			dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
			dbcstrncpy.dbcsStatuz.set(SPACES);
			dbcsTrnc(dbcstrncpy.rec);
			if (isNE(dbcstrncpy.dbcsStatuz,"****")) {
				dbcstrncpy.dbcsOutputString.set(SPACES);
			}
			textfield.set(dbcstrncpy.dbcsOutputString);
			zrordpay.set(utrnpf.getContractAmount());
			zrbonprd1.setRounded(utrnpf.getNofDunits());
			wsaaTotalUnits = add(wsaaTotalUnits,utrnpf.getNofDunits());
			zrbonprd2.setRounded(wsaaTotalUnits);
		}
		if (isGT(wsaaLinecount,wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr611s03(rr611s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		wsaaLinecount.add(1);
		printRecord.set(SPACES);
		printFile.printRr611d01(rr611d01Record);
	}

	protected void callUsts33a0()
	{
		/*EXIT*/
	}

	protected void detailHeading3400()
	{
		para3400();
	}

	protected void para3400()
	{
		wsaaOverflow.set("N");
		wsaaLinecount.set(21);
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		printRecord.set(SPACES);
		//printFile.printRr611h01(rr611h01Record);
		// fix bug1116
		printFile.printRr611h01(rr611h01Record, indicArea);
		// end
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		/*    If a reprint is in progress print the Reprint lines from*/
		/*    T5678.*/
		if (wsaaReprint.isTrue()) {
			printRecord.set(SPACES);
		}
		printRecord.set(SPACES);
		printFile.printRr611h02(rr611h02Record);
		printRecord.set(SPACES);
		printFile.printRr611h03(rr611h03Record);
	}

	protected void extractDetails3500a()
	{
		extrDetails3500a();
	}

	protected void extrDetails3500a()
	{
		addr1.set(SPACES);
		addr2.set(SPACES);
		addr3.set(SPACES);
		addr4.set(SPACES);
		addr5.set(SPACES);
		/* Write first line of address.*/
		addr1.set(clntpf.getCltaddr01());
		/* Write second line of address.*/
		if (isEQ(addr1,SPACES)) {
			addr1.set(clntpf.getCltaddr02());
		}
		else {
			addr2.set(clntpf.getCltaddr02());
		}
		/* Write third line of address.*/
		if (isEQ(addr1,SPACES)) {
			addr1.set(clntpf.getCltaddr03());
		}
		else {
			if (isEQ(addr2,SPACES)) {
				addr2.set(clntpf.getCltaddr03());
			}
			else {
				addr3.set(clntpf.getCltaddr03());
			}
		}
		/* Write fourth line of address.*/
		if (isEQ(addr1,SPACES)) {
			addr1.set(clntpf.getCltaddr04());
		}
		else {
			if (isEQ(addr2,SPACES)) {
				addr2.set(clntpf.getCltaddr04());
			}
			else {
				if (isEQ(addr3,SPACES)) {
					addr3.set(clntpf.getCltaddr04());
				}
				else {
					addr4.set(clntpf.getCltaddr04());
				}
			}
		}
		wsaaT3645Ix.set(1);
		searchlabel1:
		{
			for (; isLT(wsaaT3645Ix,wsaaT3645Rec.length); wsaaT3645Ix.add(1)){
				if (isEQ(wsaaT3645Ctrycode[wsaaT3645Ix.toInt()],clntpf.getCtrycode())
						&& isEQ(wsaaT3645Language[wsaaT3645Ix.toInt()],bsscIO.getLanguage())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(f993);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("T3645");
			FixedLengthStringData groupTEMP = clntIO.getParams();
			stringVariable1.addExpression(groupTEMP);
			clntIO.setParams(groupTEMP);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/* Write fifth line of address.*/
		if (isEQ(addr1,SPACES)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(wsaaT3645Longdesc[wsaaT3645Ix.toInt()], "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(clntpf.getCltpcode(), " ");
			stringVariable2.setStringInto(addr1);
		}
		else {
			if (isEQ(addr2,SPACES)) {
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(wsaaT3645Longdesc[wsaaT3645Ix.toInt()], "  ");
				stringVariable3.addExpression(" ");
				stringVariable3.addExpression(clntpf.getCltpcode(), " ");
				stringVariable3.setStringInto(addr2);
			}
			else {
				if (isEQ(addr3,SPACES)) {
					StringUtil stringVariable4 = new StringUtil();
					stringVariable4.addExpression(wsaaT3645Longdesc[wsaaT3645Ix.toInt()], "  ");
					stringVariable4.addExpression(" ");
					stringVariable4.addExpression(clntpf.getCltpcode(), " ");
					stringVariable4.setStringInto(addr3);
				}
				else {
					if (isEQ(addr4,SPACES)) {
						StringUtil stringVariable5 = new StringUtil();
						stringVariable5.addExpression(wsaaT3645Longdesc[wsaaT3645Ix.toInt()], "  ");
						stringVariable5.addExpression(" ");
						stringVariable5.addExpression(clntpf.getCltpcode(), " ");
						stringVariable5.setStringInto(addr4);
					}
					else {
						StringUtil stringVariable6 = new StringUtil();
						stringVariable6.addExpression(wsaaT3645Longdesc[wsaaT3645Ix.toInt()], "  ");
						stringVariable6.addExpression(" ");
						stringVariable6.addExpression(clntpf.getCltpcode(), " ");
						stringVariable6.setStringInto(addr5);
					}
				}
			}
		}
		/* IF   DESC-STATUZ            = MRNF*/
		/*      MOVE ALL '?'           TO ADDR5 OF RR611H02-O*/
		/* ELSE*/
		/*   IF ADDR1 OF RR611H02-O    = SPACES*/
		/*      STRING DESC-LONGDESC   DELIMITED BY '  ',*/
		/*             ' '             DELIMITED BY SIZE*/
		/*             CLNT-CLTPCODE   DELIMITED BY ' '*/
		/*                             INTO ADDR1 OF RR611H02-O*/
		/*   ELSE*/
		/*   IF ADDR2 OF RR611H02-O    = SPACES*/
		/*      STRING DESC-LONGDESC   DELIMITED BY '  ',*/
		/*             ' '             DELIMITED BY SIZE*/
		/*             CLNT-CLTPCODE   DELIMITED BY ' '*/
		/*                             INTO ADDR2 OF RR611H02-O*/
		/*   ELSE*/
		/*   IF ADDR3 OF RR611H02-O    = SPACES*/
		/*      STRING DESC-LONGDESC   DELIMITED BY '  ',*/
		/*             ' '             DELIMITED BY SIZE*/
		/*             CLNT-CLTPCODE   DELIMITED BY ' '*/
		/*                             INTO ADDR3 OF RR611H02-O*/
		/*   ELSE*/
		/*   IF ADDR4 OF RR611H02-O    = SPACES*/
		/*      STRING DESC-LONGDESC   DELIMITED BY '  ',*/
		/*             ' '             DELIMITED BY SIZE*/
		/*             CLNT-CLTPCODE   DELIMITED BY ' '*/
		/*                             INTO ADDR4 OF RR611H02-O*/
		/*   ELSE*/
		/*      STRING DESC-LONGDESC   DELIMITED BY '  ',*/
		/*             ' '             DELIMITED BY SIZE*/
		/*             CLNT-CLTPCODE   DELIMITED BY ' '*/
		/*                             INTO ADDR5 OF RR611H02-O*/
		/*   END-IF*/
		/* END-IF.*/
		/*obtain name of assured or assignee*/

		clntpf = clntpfDAO.findClient(t1693rec.fsuco.toString(), chdrenq.getCownpfx(), chdrenq.getCownnum());

		plainname();
		if (isEQ(wsspName[31],SPACES)
				&& isEQ(wsspName[32],SPACES)) {
			asgname1.set(wsspLongconfname);
		}
		else {
			if (isEQ(clntpf.getEthorig(),"1")) {
				asgname1.set(clntpf.getSurname());
				asgname2.set(clntpf.getGivname());
			}
			else {
				asgname1.set(clntpf.getGivname());
				asgname2.set(clntpf.getSurname());
			}
		}
		/*To obtain mode description*/
		if (isEQ(chdrenq.getBillfreq(),"00")) {
			indicTable[1].set("1");
		}
		else {
			indicTable[1].set("0");
		}
		/*To obtain sum assured*/
		wsaaValidRecord.set("N");
		wsaaSumins.set(ZERO);

		covrpfIter = covrpfList.iterator();
		while (covrpfIter.hasNext()
				&& !(validRecord.isTrue())) {
			covrpf = covrpfIter.next();
			readCovr3600a();
		}

		if (validRecord.isTrue()) {
			sumins.set(wsaaSumins);
		}
	}

	protected void readCovr3600a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCovrRec3600a();
				case nextCovr3600a: 
					nextCovr3600a();
				case exit3600a: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void readCovrRec3600a()
	{
		if (isNE(covrpf.getValidflag(),"1")) {
			goTo(GotoLabel.nextCovr3600a);
		}
		wsaaValidRecord.set("Y");
		wsaaSumins.set(covrpf.getSumins());
	}

	protected void nextCovr3600a()
	{
		//covrIO.setFunction(varcom.nextr);
	}

	/**
	 * <pre>
	 *To obtain Agent information
	 * </pre>
	 */
	protected void getAgent3700a(){
		readAgntenqRec3700a();
	}

	protected void readAgntenqRec3700a()
	{
		Agntpf agntenqIO = agntpfDAO.getAgntlagData(chdrenq.getAgntcoy(), chdrenq.getAgntnum()); //IBPLIFE-2362, previous method was not present in repo
		if (agntenqIO == null) {
			fatalError600();
		}
 //IBPLIFE-2145 start
		Clntpf cltsIO = new Clntpf();
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntcoy(agntenqIO.getClntcoy());
		cltsIO.setClntpfx(chdrenq.getCownpfx());
		cltsIO = clntpfDAO.selectClient(cltsIO);
 //IBPLIFE-2145 end
		if (cltsIO == null) {
			fatalError600();
		}
		plainname();
		despname1.set(wsspLongconfname);
		despname2.set(wsspLongconfname);
		cltphone.set(cltsIO.getCltphone02());
		/*  Print the office phone if these is not then Home phone.*/
		if (isEQ(cltsIO.getCltphone02(),SPACES)) {
			cltphone.set(cltsIO.getCltphone01());
		}
	}

	/**
	 * <pre>
	 * Total value of Units = total units * bid price at closing
	 * </pre>
	 */
	protected void unitsValue3800a()
	{
		unitsValueProcess3800a();
	}

	protected void unitsValueProcess3800a()
	{
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(ustfpf.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(ustfpf.getVrtfnd());
		vprnudlIO.setUnitType(ustfpf.getUnityp());
		vprnudlIO.setEffdate(ustfpf.getStcldt());
		vprnudlIO.setFunction(varcom.begn);
		vprnudlIO.setFormat(formatsInner.vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
				&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			syserrrec.statuz.set(vprnudlIO.getStatuz());
			fatalError600();
		}
		compute(wsaaTotnofunts, 6).setRounded(add(wsaaTotnofunts,(mult(wsaaCloBalUnits,vprnudlIO.getUnitBidPrice()))));
		if (isNE(wsaaTotnofunts, 0)) {
			zrdecplrec.amountIn.set(wsaaTotnofunts);
			zrdecplrec.currency.set(t5515rec.currcode);
			a000CallRounding();
			wsaaTotnofunts.set(zrdecplrec.amountOut);
		}
	}

	protected void b100ReadZrstmno()
	{
		b110ReadZrstmno();
	}

	protected void b110ReadZrstmno()
	{
		if (isNE(zrstpf.getLife(),ustfpf.getLife())
				|| isNE(zrstpf.getCoverage(),ustfpf.getCoverage())) {
			return ;
		}
		if (isEQ(zrstpf.getXtranno(),utrnpf.getTranno())) {
			wsaaZrstmnoFound.set("Y");
			/* To get description from T5687 if Mortality Charge is for Riders*/
			if (isEQ(zrstpf.getSacstyp(),"MC")) {
				b500ReadT5687();
			}
			else {
				wsaaSacstype.set(zrstpf	.getSacstyp());
				b400ReadT3695();
			}
			dbcstrncpy.dbcsInputString.set(wsaaLongdesc);
			dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
			dbcstrncpy.dbcsStatuz.set(SPACES);
			dbcsTrnc(dbcstrncpy.rec);
			if (isNE(dbcstrncpy.dbcsStatuz,"****")) {
				dbcstrncpy.dbcsOutputString.set(SPACES);
			}
			textfield.set(dbcstrncpy.dbcsOutputString);
			compute(wsaaFees, 6).setRounded((div((mult(utrnpf.getContractAmount(),zrstpf.getZramount01())),zrstpf.getZramount02())));
			if (isNE(wsaaFees, 0)) {
				zrdecplrec.amountIn.set(wsaaFees);
				zrdecplrec.currency.set(chdrenq.getCntcurr());
				a000CallRounding();
				wsaaFees.set(zrdecplrec.amountOut);
			}
			zrordpay.setRounded(wsaaFees);
			compute(wsaaUnits, 6).setRounded((div(wsaaFees,vprnudlIO.getUnitBidPrice())));
			zrbonprd1.setRounded(wsaaUnits);
			wsaaTotalUnits.add(wsaaUnits);
			zrbonprd2.setRounded(wsaaTotalUnits);
			if (isGT(wsaaLinecount,wsaaMaxLines)) {
				wsaaOverflow.set("Y");
				printFile.printRr611s03(rr611s03Record);
			}
			if (pageOverflow.isTrue()) {
				detailHeading3400();
			}
			printFile.printRr611d01(rr611d01Record);
			wsaaLinecount.add(1);
		}
	}

	protected void b100ReadT1693(){
		b100Para();
	}

	protected void b100Para()
	{
		boolean itemFound = false;
		String itemKey = bprdIO.getCompany().toString().trim();
		if (t1693ListMap.containsKey(itemKey)){	
			itempfList = t1693ListMap.get(itemKey);
			t1693rec.t1693Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));	
			itemFound = true;
		}
		if (!itemFound) {
			fatalError600();
		}
	}

	protected void b300ReadT5645(){
		b300Para();
	}

	protected void b300Para()
	{
		boolean itemFound = false;
		String itemKey = wsaaProg.toString().trim();
		if (t5645ListMap.containsKey(itemKey)){	
			itempfList = t5645ListMap.get(itemKey);
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));	
			itemFound = true;
		}
		if (!itemFound) {
			fatalError600();
		}
	}

	protected void b400ReadT3695()
	{
		/*B400-PARA*/
		wsaaT3695Ix.set(1);
		searchlabel1:
		{
			for (; isLT(wsaaT3695Ix,wsaaT3695Rec.length); wsaaT3695Ix.add(1)){
				if (isEQ(wsaaT3695Sacstype[wsaaT3695Ix.toInt()],wsaaSacstype)
						&& isEQ(wsaaT3695Language[wsaaT3695Ix.toInt()],bsscIO.getLanguage())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(h135);
			StringUtil stringVariable1 = new StringUtil();
			String stringVariable2;
			stringVariable1.addExpression("T3695");
			stringVariable2 = chdrenq.getChdruls_chdrcoy()+chdrenq.getChdruls_chdrnum();
			FixedLengthStringData groupTEMP = new FixedLengthStringData(stringVariable2);
			stringVariable1.addExpression(groupTEMP);
			//chdrulsIO.setParams(groupTEMP);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		wsaaLongdesc.set(wsaaT3695Longdesc[wsaaT3695Ix.toInt()]);
		/*B400-EXIT*/
	}

	protected void b500ReadT5687(){
		b510Para();
	}

	protected void b510Para()
	{
		Covrpf covrIO=covrpfDAO.getCovrRecord(zrstpf.getChdrcoy(),zrstpf.getChdrnum(),zrstpf.getLife(),zrstpf.getCoverage(),
				zrstpf.getRider(),ustfpf.getPlnsfx(),"1");
		if (covrIO == null) {
			fatalError600();
		}
		wsaaT5687Ix.set(1);
		searchlabel1:
		{
			for (; isLT(wsaaT5687Ix,wsaaT5687Rec.length); wsaaT5687Ix.add(1)){
				if (isEQ(wsaaT5687Crtable[wsaaT5687Ix.toInt()],covrIO.getCrtable())
						&& isEQ(wsaaT5687Language[wsaaT5687Ix.toInt()],bsscIO.getLanguage())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(h053);
			StringUtil stringVariable1 = new StringUtil();
			String stringVariable2;
			stringVariable1.addExpression("T5687");
			stringVariable2 = chdrenq.getChdruls_chdrcoy()+chdrenq.getChdruls_chdrnum();
			FixedLengthStringData groupTEMP = new FixedLengthStringData(stringVariable2);
			stringVariable1.addExpression(groupTEMP);
			//chdrulsIO.setParams(groupTEMP);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		wsaaLongdesc.set(wsaaT5687Longdesc[wsaaT5687Ix.toInt()]);
	}

	protected void commitControlTotals(){
		//ct01
		contotrec.totno.set(ct01);
		contotrec.totval.set(ctrCT01);
		callProgram(Contot.class, contotrec.contotRec);		
		ctrCT01 = 0;

		//ct02
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT02 = 0;

		//ct06
		contotrec.totno.set(ct06);
		contotrec.totval.set(ctrCT06);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT06 = 0;

	}


	protected void commit3500()
	{
		/*COMMIT*/
		commitControlTotals();
		/*EXIT*/
	}

	protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

	protected void close4000()
	{
		/*CLOSE-FILES*/
		printFile.close();
	
		if(chdrnumList != null && !chdrnumList.isEmpty()){
		chdrnumList.clear();
		chdrnumList = null;
		}
		if(ustjList != null && !ustjList.isEmpty()){
		ustjList.clear();
		ustjList = null;
		}
		if(ustfpfList != null && !ustfpfList.isEmpty()){
		ustfpfList.clear();
		ustfpfList = null;
		}
		if(chdrenqList != null && !chdrenqList.isEmpty()){
		chdrenqList.clear();
		chdrenqList = null;
		}
		if(covrpfList != null && !covrpfList.isEmpty()){
		covrpfList.clear();
		covrpfList = null;
		}
		if(zrstpfList != null && !zrstpfList.isEmpty()){
		zrstpfList.clear();
		zrstpfList = null;
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

	protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		/* MOVE CHDRULS-CNTCURR        TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 
		private FixedLengthStringData chdrulsrec = new FixedLengthStringData(10).init("CHDRULSREC");
		private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
		private FixedLengthStringData clntrec = new FixedLengthStringData(10).init("CLNTREC");
		private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
		private FixedLengthStringData agntenqrec = new FixedLengthStringData(10).init("AGNTREC");
		private FixedLengthStringData vprnudlrec = new FixedLengthStringData(10).init("VPRNUDLREC");
		private FixedLengthStringData ustjrec = new FixedLengthStringData(10).init("USTJREC");
	}
}
