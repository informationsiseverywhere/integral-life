package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5544
 * @version 1.0 generated on 30/08/09 06:43
 * @author Quipoz
 */
public class S5544ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(452);
	public FixedLengthStringData dataFields = new FixedLengthStringData(132).isAPartOf(dataArea, 0);
	public FixedLengthStringData btobid = DD.btobid.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData btodisc = DD.btodisc.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData discountOfferPercent = DD.btodiscpc.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bidToFund = DD.btofund.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData btooff = DD.btooff.copy().isAPartOf(dataFields,8);
	public ZonedDecimalData cfrwd = DD.cfrwd.copyToZonedDecimal().isAPartOf(dataFields,9);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,10);
	public ZonedDecimalData feemax = DD.feemax.copyToZonedDecimal().isAPartOf(dataFields,11);
	public ZonedDecimalData feemin = DD.feemin.copyToZonedDecimal().isAPartOf(dataFields,28);
	public ZonedDecimalData feepc = DD.feepc.copyToZonedDecimal().isAPartOf(dataFields,45);
	public ZonedDecimalData ffamt = DD.ffamt.copyToZonedDecimal().isAPartOf(dataFields,50);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,67);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,75);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,83);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(dataFields,121);
	public ZonedDecimalData noOfFreeSwitches = DD.nfswts.copyToZonedDecimal().isAPartOf(dataFields,122);
	public FixedLengthStringData otooff = DD.otooff.copy().isAPartOf(dataFields,125);
	public ZonedDecimalData ppyears = DD.ppyears.copyToZonedDecimal().isAPartOf(dataFields,126);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,127);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 132);
	public FixedLengthStringData btobidErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData btodiscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btodiscpcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData btofundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData btooffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cfrwdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData feemaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData feeminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData feepcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ffamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ndfindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData nfswtsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData otooffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ppyearsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 212);
	public FixedLengthStringData[] btobidOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] btodiscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btodiscpcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] btofundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] btooffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cfrwdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] feemaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] feeminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] feepcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ffamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ndfindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] nfswtsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] otooffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ppyearsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5544screenWritten = new LongData(0);
	public LongData S5544protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5544ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(btobidOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btooffOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btodiscOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otooffOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ndfindOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nfswtsOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ppyearsOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cfrwdOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ffamtOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feepcOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feeminOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feemaxOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btofundOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btodiscpcOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, btobid, btooff, btodisc, otooff, nowDeferInd, noOfFreeSwitches, ppyears, cfrwd, ffamt, feepc, feemin, feemax, bidToFund, discountOfferPercent};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, btobidOut, btooffOut, btodiscOut, otooffOut, ndfindOut, nfswtsOut, ppyearsOut, cfrwdOut, ffamtOut, feepcOut, feeminOut, feemaxOut, btofundOut, btodiscpcOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, btobidErr, btooffErr, btodiscErr, otooffErr, ndfindErr, nfswtsErr, ppyearsErr, cfrwdErr, ffamtErr, feepcErr, feeminErr, feemaxErr, btofundErr, btodiscpcErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5544screen.class;
		protectRecord = S5544protect.class;
	}

}
