package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IncirnlTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:41
 * Class transformed from INCIRNL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IncirnlTableDAM extends IncipfTableDAM {

	public IncirnlTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("INCIRNL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "CURRPRM, " +
		            "PREMCURR01, " +
		            "PREMCURR02, " +
		            "PREMCURR03, " +
		            "PREMCURR04, " +
		            "PREMCURR05, " +
		            "PREMCURR06, " +
		            "PREMCURR07, " +
		            "PCUNIT01, " +
		            "PCUNIT02, " +
		            "PCUNIT03, " +
		            "PCUNIT04, " +
		            "PCUNIT05, " +
		            "PCUNIT06, " +
		            "PCUNIT07, " +
		            "USPLITPC01, " +
		            "USPLITPC02, " +
		            "USPLITPC03, " +
		            "USPLITPC04, " +
		            "USPLITPC05, " +
		            "USPLITPC06, " +
		            "USPLITPC07, " +
		            "INCINUM, " +
		            "VALIDFLAG, " +
		            "SEQNO, " +
		            "DORMFLAG, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               currPrem,
                               premCurr01,
                               premCurr02,
                               premCurr03,
                               premCurr04,
                               premCurr05,
                               premCurr06,
                               premCurr07,
                               pcUnits01,
                               pcUnits02,
                               pcUnits03,
                               pcUnits04,
                               pcUnits05,
                               pcUnits06,
                               pcUnits07,
                               unitSplit01,
                               unitSplit02,
                               unitSplit03,
                               unitSplit04,
                               unitSplit05,
                               unitSplit06,
                               unitSplit07,
                               inciNum,
                               validflag,
                               seqno,
                               dormantFlag,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(139);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getCurrPrem().toInternal()
					+ getPremCurr01().toInternal()
					+ getPremCurr02().toInternal()
					+ getPremCurr03().toInternal()
					+ getPremCurr04().toInternal()
					+ getPcUnits01().toInternal()
					+ getPcUnits02().toInternal()
					+ getPcUnits03().toInternal()
					+ getPcUnits04().toInternal()
					+ getUnitSplit01().toInternal()
					+ getUnitSplit02().toInternal()
					+ getUnitSplit03().toInternal()
					+ getUnitSplit04().toInternal()
					+ getInciNum().toInternal()
					+ getValidflag().toInternal()
					+ getSeqno().toInternal()
					+ getDormantFlag().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, currPrem);
			what = ExternalData.chop(what, premCurr01);
			what = ExternalData.chop(what, premCurr02);
			what = ExternalData.chop(what, premCurr03);
			what = ExternalData.chop(what, premCurr04);
			what = ExternalData.chop(what, premCurr05);
			what = ExternalData.chop(what, premCurr06);
			what = ExternalData.chop(what, premCurr07);
			what = ExternalData.chop(what, pcUnits01);
			what = ExternalData.chop(what, pcUnits02);
			what = ExternalData.chop(what, pcUnits03);
			what = ExternalData.chop(what, pcUnits04);
			what = ExternalData.chop(what, pcUnits05);
			what = ExternalData.chop(what, pcUnits06);
			what = ExternalData.chop(what, pcUnits07);
			what = ExternalData.chop(what, unitSplit01);
			what = ExternalData.chop(what, unitSplit02);
			what = ExternalData.chop(what, unitSplit03);
			what = ExternalData.chop(what, unitSplit04);
			what = ExternalData.chop(what, unitSplit05);
			what = ExternalData.chop(what, unitSplit06);
			what = ExternalData.chop(what, unitSplit07);
			what = ExternalData.chop(what, inciNum);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, dormantFlag);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCurrPrem() {
		return currPrem;
	}
	public void setCurrPrem(Object what) {
		setCurrPrem(what, false);
	}
	public void setCurrPrem(Object what, boolean rounded) {
		if (rounded)
			currPrem.setRounded(what);
		else
			currPrem.set(what);
	}	
	public PackedDecimalData getPremCurr01() {
		return premCurr01;
	}
	public void setPremCurr01(Object what) {
		setPremCurr01(what, false);
	}
	public void setPremCurr01(Object what, boolean rounded) {
		if (rounded)
			premCurr01.setRounded(what);
		else
			premCurr01.set(what);
	}	
	public PackedDecimalData getPremCurr02() {
		return premCurr02;
	}
	public void setPremCurr02(Object what) {
		setPremCurr02(what, false);
	}
	public void setPremCurr02(Object what, boolean rounded) {
		if (rounded)
			premCurr02.setRounded(what);
		else
			premCurr02.set(what);
	}	
	public PackedDecimalData getPremCurr03() {
		return premCurr03;
	}
	public void setPremCurr03(Object what) {
		setPremCurr03(what, false);
	}
	public void setPremCurr03(Object what, boolean rounded) {
		if (rounded)
			premCurr03.setRounded(what);
		else
			premCurr03.set(what);
	}	
	public PackedDecimalData getPremCurr04() {
		return premCurr04;
	}
	public void setPremCurr04(Object what) {
		setPremCurr04(what, false);
	}
	public void setPremCurr04(Object what, boolean rounded) {
		if (rounded)
			premCurr04.setRounded(what);
		else
			premCurr04.set(what);
	}	
	public PackedDecimalData getPremCurr05() {
		return premCurr05;
	}
	public void setPremCurr05(Object what) {
		setPremCurr05(what, false);
	}
	public void setPremCurr05(Object what, boolean rounded) {
		if (rounded)
			premCurr05.setRounded(what);
		else
			premCurr05.set(what);
	}	
	
	public PackedDecimalData getPremCurr06() {
		return premCurr06;
	}
	public void setPremCurr06(Object what) {
		setPremCurr06(what, false);
	}
	public void setPremCurr06(Object what, boolean rounded) {
		if (rounded)
			premCurr06.setRounded(what);
		else
			premCurr06.set(what);
	}	
	
	public PackedDecimalData getPremCurr07() {
		return premCurr07;
	}
	public void setPremCurr07(Object what) {
		setPremCurr07(what, false);
	}
	public void setPremCurr07(Object what, boolean rounded) {
		if (rounded)
			premCurr07.setRounded(what);
		else
			premCurr07.set(what);
	}	
	public PackedDecimalData getPcUnits01() {
		return pcUnits01;
	}
	public void setPcUnits01(Object what) {
		setPcUnits01(what, false);
	}
	public void setPcUnits01(Object what, boolean rounded) {
		if (rounded)
			pcUnits01.setRounded(what);
		else
			pcUnits01.set(what);
	}	
	public PackedDecimalData getPcUnits02() {
		return pcUnits02;
	}
	public void setPcUnits02(Object what) {
		setPcUnits02(what, false);
	}
	public void setPcUnits02(Object what, boolean rounded) {
		if (rounded)
			pcUnits02.setRounded(what);
		else
			pcUnits02.set(what);
	}	
	public PackedDecimalData getPcUnits03() {
		return pcUnits03;
	}
	public void setPcUnits03(Object what) {
		setPcUnits03(what, false);
	}
	public void setPcUnits03(Object what, boolean rounded) {
		if (rounded)
			pcUnits03.setRounded(what);
		else
			pcUnits03.set(what);
	}
	
	
	public PackedDecimalData getPcUnits04() {
		return pcUnits04;
	}
	public void setPcUnits04(Object what) {
		setPcUnits04(what, false);
	}
	public void setPcUnits04(Object what, boolean rounded) {
		if (rounded)
			pcUnits04.setRounded(what);
		else
			pcUnits04.set(what);
	}
	public PackedDecimalData getPcUnits05() {
		return pcUnits05;
	}
	public void setPcUnits05(Object what) {
		setPcUnits05(what, false);
	}
	public void setPcUnits05(Object what, boolean rounded) {
		if (rounded)
			pcUnits05.setRounded(what);
		else
			pcUnits05.set(what);
	}	
	
	public PackedDecimalData getPcUnits06() {
		return pcUnits06;
	}
	public void setPcUnits06(Object what) {
		setPcUnits06(what, false);
	}
	public void setPcUnits06(Object what, boolean rounded) {
		if (rounded)
			pcUnits06.setRounded(what);
		else
			pcUnits06.set(what);
	}	
	
	public PackedDecimalData getPcUnits07() {
		return pcUnits07;
	}
	public void setPcUnits07(Object what) {
		setPcUnits07(what, false);
	}
	public void setPcUnits07(Object what, boolean rounded) {
		if (rounded)
			pcUnits07.setRounded(what);
		else
			pcUnits07.set(what);
	}	
	
	public PackedDecimalData getUnitSplit01() {
		return unitSplit01;
	}
	public void setUnitSplit01(Object what) {
		setUnitSplit01(what, false);
	}
	public void setUnitSplit01(Object what, boolean rounded) {
		if (rounded)
			unitSplit01.setRounded(what);
		else
			unitSplit01.set(what);
	}	
	public PackedDecimalData getUnitSplit02() {
		return unitSplit02;
	}
	public void setUnitSplit02(Object what) {
		setUnitSplit02(what, false);
	}
	public void setUnitSplit02(Object what, boolean rounded) {
		if (rounded)
			unitSplit02.setRounded(what);
		else
			unitSplit02.set(what);
	}	
	public PackedDecimalData getUnitSplit03() {
		return unitSplit03;
	}
	public void setUnitSplit03(Object what) {
		setUnitSplit03(what, false);
	}
	public void setUnitSplit03(Object what, boolean rounded) {
		if (rounded)
			unitSplit03.setRounded(what);
		else
			unitSplit03.set(what);
	}	
	
	public PackedDecimalData getUnitSplit04() {
		return unitSplit04;
	}
	public void setUnitSplit04(Object what) {
		setUnitSplit04(what, false);
	}
	public void setUnitSplit04(Object what, boolean rounded) {
		if (rounded)
			unitSplit04.setRounded(what);
		else
			unitSplit04.set(what);
	}	
	
	public PackedDecimalData getUnitSplit05() {
		return unitSplit05;
	}
	public void setUnitSplit05(Object what) {
		setUnitSplit05(what, false);
	}
	public void setUnitSplit05(Object what, boolean rounded) {
		if (rounded)
			unitSplit05.setRounded(what);
		else
			unitSplit05.set(what);
	}	
	
	public PackedDecimalData getUnitSplit06() {
		return unitSplit06;
	}
	public void setUnitSplit06(Object what) {
		setUnitSplit06(what, false);
	}
	public void setUnitSplit06(Object what, boolean rounded) {
		if (rounded)
			unitSplit06.setRounded(what);
		else
			unitSplit06.set(what);
	}	
	
	
	public PackedDecimalData getUnitSplit07() {
		return unitSplit07;
	}
	public void setUnitSplit07(Object what) {
		setUnitSplit07(what, false);
	}
	public void setUnitSplit07(Object what, boolean rounded) {
		if (rounded)
			unitSplit07.setRounded(what);
		else
			unitSplit07.set(what);
	}	
	
	
	
	public PackedDecimalData getInciNum() {
		return inciNum;
	}
	public void setInciNum(Object what) {
		setInciNum(what, false);
	}
	public void setInciNum(Object what, boolean rounded) {
		if (rounded)
			inciNum.setRounded(what);
		else
			inciNum.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}	
	public FixedLengthStringData getDormantFlag() {
		return dormantFlag;
	}
	public void setDormantFlag(Object what) {
		dormantFlag.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getUsplitpcs() {
		return new FixedLengthStringData(unitSplit01.toInternal()
										+ unitSplit02.toInternal()
										+ unitSplit03.toInternal()
										+ unitSplit04.toInternal()
										+ unitSplit05.toInternal()
										+ unitSplit06.toInternal()
										+ unitSplit07.toInternal());
	}
	public void setUsplitpcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getUsplitpcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, unitSplit01);
		what = ExternalData.chop(what, unitSplit02);
		what = ExternalData.chop(what, unitSplit03);
		what = ExternalData.chop(what, unitSplit04);
		what = ExternalData.chop(what, unitSplit05);
		what = ExternalData.chop(what, unitSplit06);
		what = ExternalData.chop(what, unitSplit07);
	}
	public PackedDecimalData getUsplitpc(BaseData indx) {
		return getUsplitpc(indx.toInt());
	}
	public PackedDecimalData getUsplitpc(int indx) {

		switch (indx) {
			case 1 : return unitSplit01;
			case 2 : return unitSplit02;
			case 3 : return unitSplit03;
			case 4 : return unitSplit04;
			case 5 : return unitSplit05;
			case 6 : return unitSplit06;
			case 7 : return unitSplit07;
			default: return null; // Throw error instead?
		}
	
	}
	public void setUsplitpc(BaseData indx, Object what) {
		setUsplitpc(indx, what, false);
	}
	public void setUsplitpc(BaseData indx, Object what, boolean rounded) {
		setUsplitpc(indx.toInt(), what, rounded);
	}
	public void setUsplitpc(int indx, Object what) {
		setUsplitpc(indx, what, false);
	}
	public void setUsplitpc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setUnitSplit01(what, rounded);
					 break;
			case 2 : setUnitSplit02(what, rounded);
					 break;
			case 3 : setUnitSplit03(what, rounded);
					 break;
			case 4 : setUnitSplit04(what, rounded);
					 break;
			case 5 : setUnitSplit05(what, rounded);
			 break;
			case 6 : setUnitSplit06(what, rounded);
			 break;
			case 7 : setUnitSplit07(what, rounded);
			 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getPremcurrs() {
		return new FixedLengthStringData(premCurr01.toInternal()
										+ premCurr02.toInternal()
										+ premCurr03.toInternal()
										+ premCurr04.toInternal()
										+ premCurr05.toInternal()
										+ premCurr06.toInternal()
										+ premCurr07.toInternal());
	}
	public void setPremcurrs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPremcurrs().getLength()).init(obj);
	
		what = ExternalData.chop(what, premCurr01);
		what = ExternalData.chop(what, premCurr02);
		what = ExternalData.chop(what, premCurr03);
		what = ExternalData.chop(what, premCurr04);
		what = ExternalData.chop(what, premCurr05);
		what = ExternalData.chop(what, premCurr06);
		what = ExternalData.chop(what, premCurr07);
	}
	public PackedDecimalData getPremcurr(BaseData indx) {
		return getPremcurr(indx.toInt());
	}
	public PackedDecimalData getPremcurr(int indx) {

		switch (indx) {
			case 1 : return premCurr01;
			case 2 : return premCurr02;
			case 3 : return premCurr03;
			case 4 : return premCurr04;
			case 5 : return premCurr05;
			case 6 : return premCurr06;
			case 7 : return premCurr07;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPremcurr(BaseData indx, Object what) {
		setPremcurr(indx, what, false);
	}
	public void setPremcurr(BaseData indx, Object what, boolean rounded) {
		setPremcurr(indx.toInt(), what, rounded);
	}
	public void setPremcurr(int indx, Object what) {
		setPremcurr(indx, what, false);
	}
	public void setPremcurr(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setPremCurr01(what, rounded);
					 break;
			case 2 : setPremCurr02(what, rounded);
					 break;
			case 3 : setPremCurr03(what, rounded);
					 break;
			case 4 : setPremCurr04(what, rounded);
					 break;
			case 5 : setPremCurr05(what, rounded);
			 		 break;
			case 6 : setPremCurr06(what, rounded);
					 break;
			case 7 : setPremCurr07(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getPcunits() {
		return new FixedLengthStringData(pcUnits01.toInternal()
										+ pcUnits02.toInternal()
										+ pcUnits03.toInternal()
										+ pcUnits04.toInternal()
										+ pcUnits05.toInternal()
										+ pcUnits06.toInternal()
										+ pcUnits07.toInternal());
	}
	public void setPcunits(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPcunits().getLength()).init(obj);
	
		what = ExternalData.chop(what, pcUnits01);
		what = ExternalData.chop(what, pcUnits02);
		what = ExternalData.chop(what, pcUnits03);
		what = ExternalData.chop(what, pcUnits04);
		what = ExternalData.chop(what, pcUnits05);
		what = ExternalData.chop(what, pcUnits06);
		what = ExternalData.chop(what, pcUnits07);
	}
	public PackedDecimalData getPcunit(BaseData indx) {
		return getPcunit(indx.toInt());
	}
	public PackedDecimalData getPcunit(int indx) {

		switch (indx) {
			case 1 : return pcUnits01;
			case 2 : return pcUnits02;
			case 3 : return pcUnits03;
			case 4 : return pcUnits04;
			case 5 : return pcUnits05;
			case 6 : return pcUnits06;
			case 7 : return pcUnits07;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPcunit(BaseData indx, Object what) {
		setPcunit(indx, what, false);
	}
	public void setPcunit(BaseData indx, Object what, boolean rounded) {
		setPcunit(indx.toInt(), what, rounded);
	}
	public void setPcunit(int indx, Object what) {
		setPcunit(indx, what, false);
	}
	public void setPcunit(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setPcUnits01(what, rounded);
					 break;
			case 2 : setPcUnits02(what, rounded);
					 break;
			case 3 : setPcUnits03(what, rounded);
					 break;
			case 4 : setPcUnits04(what, rounded);
					 break;
			case 5 : setPcUnits05(what, rounded);
			 		 break;
			case 6 : setPcUnits06(what, rounded);
					 break;
			case 7 : setPcUnits07(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		currPrem.clear();
		premCurr01.clear();
		premCurr02.clear();
		premCurr03.clear();
		premCurr04.clear();
		premCurr05.clear();
		premCurr06.clear();
		premCurr07.clear();
		
		pcUnits01.clear();
		pcUnits02.clear();
		pcUnits03.clear();
		pcUnits04.clear();
		pcUnits05.clear();
		pcUnits06.clear();
		pcUnits07.clear();
		
		unitSplit01.clear();
		unitSplit02.clear();
		unitSplit03.clear();
		unitSplit04.clear();
		unitSplit05.clear();
		unitSplit06.clear();
		unitSplit07.clear();
		
		inciNum.clear();
		validflag.clear();
		seqno.clear();
		dormantFlag.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}