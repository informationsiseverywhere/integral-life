/*
 * File: T5539pt.java
 * Date: 30 August 2009 2:22:03
 * Author: Quipoz Limited
 * 
 * Class transformed from T5539PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5539.
*
*
*****************************************************************
* </pre>
*/
public class T5539pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5539rec t5539rec = new T5539rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5539pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5539rec.t5539Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(t5539rec.dfact01);
		generalCopyLinesInner.fieldNo006.set(t5539rec.dfact02);
		generalCopyLinesInner.fieldNo007.set(t5539rec.dfact03);
		generalCopyLinesInner.fieldNo008.set(t5539rec.dfact04);
		generalCopyLinesInner.fieldNo009.set(t5539rec.dfact05);
		generalCopyLinesInner.fieldNo010.set(t5539rec.dfact06);
		generalCopyLinesInner.fieldNo011.set(t5539rec.dfact07);
		generalCopyLinesInner.fieldNo012.set(t5539rec.dfact08);
		generalCopyLinesInner.fieldNo013.set(t5539rec.dfact09);
		generalCopyLinesInner.fieldNo014.set(t5539rec.dfact10);
		generalCopyLinesInner.fieldNo015.set(t5539rec.dfact11);
		generalCopyLinesInner.fieldNo016.set(t5539rec.dfact12);
		generalCopyLinesInner.fieldNo017.set(t5539rec.dfact13);
		generalCopyLinesInner.fieldNo018.set(t5539rec.dfact14);
		generalCopyLinesInner.fieldNo019.set(t5539rec.dfact15);
		generalCopyLinesInner.fieldNo020.set(t5539rec.dfact16);
		generalCopyLinesInner.fieldNo021.set(t5539rec.dfact17);
		generalCopyLinesInner.fieldNo022.set(t5539rec.dfact18);
		generalCopyLinesInner.fieldNo023.set(t5539rec.dfact19);
		generalCopyLinesInner.fieldNo024.set(t5539rec.dfact20);
		generalCopyLinesInner.fieldNo025.set(t5539rec.dfact21);
		generalCopyLinesInner.fieldNo026.set(t5539rec.dfact22);
		generalCopyLinesInner.fieldNo027.set(t5539rec.dfact23);
		generalCopyLinesInner.fieldNo028.set(t5539rec.dfact24);
		generalCopyLinesInner.fieldNo029.set(t5539rec.dfact25);
		generalCopyLinesInner.fieldNo030.set(t5539rec.dfact26);
		generalCopyLinesInner.fieldNo031.set(t5539rec.dfact27);
		generalCopyLinesInner.fieldNo032.set(t5539rec.dfact28);
		generalCopyLinesInner.fieldNo033.set(t5539rec.dfact29);
		generalCopyLinesInner.fieldNo034.set(t5539rec.dfact30);
		generalCopyLinesInner.fieldNo035.set(t5539rec.dfact31);
		generalCopyLinesInner.fieldNo036.set(t5539rec.dfact32);
		generalCopyLinesInner.fieldNo037.set(t5539rec.dfact33);
		generalCopyLinesInner.fieldNo038.set(t5539rec.dfact34);
		generalCopyLinesInner.fieldNo039.set(t5539rec.dfact35);
		generalCopyLinesInner.fieldNo040.set(t5539rec.dfact36);
		generalCopyLinesInner.fieldNo041.set(t5539rec.dfact37);
		generalCopyLinesInner.fieldNo042.set(t5539rec.dfact38);
		generalCopyLinesInner.fieldNo043.set(t5539rec.dfact39);
		generalCopyLinesInner.fieldNo044.set(t5539rec.dfact40);
		generalCopyLinesInner.fieldNo045.set(t5539rec.dfact41);
		generalCopyLinesInner.fieldNo046.set(t5539rec.dfact42);
		generalCopyLinesInner.fieldNo047.set(t5539rec.dfact43);
		generalCopyLinesInner.fieldNo048.set(t5539rec.dfact44);
		generalCopyLinesInner.fieldNo049.set(t5539rec.dfact45);
		generalCopyLinesInner.fieldNo050.set(t5539rec.dfact46);
		generalCopyLinesInner.fieldNo051.set(t5539rec.dfact47);
		generalCopyLinesInner.fieldNo052.set(t5539rec.dfact48);
		generalCopyLinesInner.fieldNo053.set(t5539rec.dfact49);
		generalCopyLinesInner.fieldNo054.set(t5539rec.dfact50);
		generalCopyLinesInner.fieldNo055.set(t5539rec.dfact51);
		generalCopyLinesInner.fieldNo056.set(t5539rec.dfact52);
		generalCopyLinesInner.fieldNo057.set(t5539rec.dfact53);
		generalCopyLinesInner.fieldNo058.set(t5539rec.dfact54);
		generalCopyLinesInner.fieldNo059.set(t5539rec.dfact55);
		generalCopyLinesInner.fieldNo060.set(t5539rec.dfact56);
		generalCopyLinesInner.fieldNo061.set(t5539rec.dfact57);
		generalCopyLinesInner.fieldNo062.set(t5539rec.dfact58);
		generalCopyLinesInner.fieldNo063.set(t5539rec.dfact59);
		generalCopyLinesInner.fieldNo064.set(t5539rec.dfact60);
		generalCopyLinesInner.fieldNo065.set(t5539rec.dfact61);
		generalCopyLinesInner.fieldNo066.set(t5539rec.dfact62);
		generalCopyLinesInner.fieldNo067.set(t5539rec.dfact63);
		generalCopyLinesInner.fieldNo068.set(t5539rec.dfact64);
		generalCopyLinesInner.fieldNo069.set(t5539rec.dfact65);
		generalCopyLinesInner.fieldNo070.set(t5539rec.dfact66);
		generalCopyLinesInner.fieldNo071.set(t5539rec.dfact67);
		generalCopyLinesInner.fieldNo072.set(t5539rec.dfact68);
		generalCopyLinesInner.fieldNo073.set(t5539rec.dfact69);
		generalCopyLinesInner.fieldNo074.set(t5539rec.dfact70);
		generalCopyLinesInner.fieldNo075.set(t5539rec.dfact71);
		generalCopyLinesInner.fieldNo076.set(t5539rec.dfact72);
		generalCopyLinesInner.fieldNo077.set(t5539rec.dfact73);
		generalCopyLinesInner.fieldNo078.set(t5539rec.dfact74);
		generalCopyLinesInner.fieldNo079.set(t5539rec.dfact75);
		generalCopyLinesInner.fieldNo080.set(t5539rec.dfact76);
		generalCopyLinesInner.fieldNo081.set(t5539rec.dfact77);
		generalCopyLinesInner.fieldNo082.set(t5539rec.dfact78);
		generalCopyLinesInner.fieldNo083.set(t5539rec.dfact79);
		generalCopyLinesInner.fieldNo084.set(t5539rec.dfact80);
		generalCopyLinesInner.fieldNo085.set(t5539rec.dfact81);
		generalCopyLinesInner.fieldNo086.set(t5539rec.dfact82);
		generalCopyLinesInner.fieldNo087.set(t5539rec.dfact83);
		generalCopyLinesInner.fieldNo088.set(t5539rec.dfact84);
		generalCopyLinesInner.fieldNo089.set(t5539rec.dfact85);
		generalCopyLinesInner.fieldNo090.set(t5539rec.dfact86);
		generalCopyLinesInner.fieldNo091.set(t5539rec.dfact87);
		generalCopyLinesInner.fieldNo092.set(t5539rec.dfact88);
		generalCopyLinesInner.fieldNo093.set(t5539rec.dfact89);
		generalCopyLinesInner.fieldNo094.set(t5539rec.dfact90);
		generalCopyLinesInner.fieldNo095.set(t5539rec.dfact91);
		generalCopyLinesInner.fieldNo096.set(t5539rec.dfact92);
		generalCopyLinesInner.fieldNo097.set(t5539rec.dfact93);
		generalCopyLinesInner.fieldNo098.set(t5539rec.dfact94);
		generalCopyLinesInner.fieldNo099.set(t5539rec.dfact95);
		generalCopyLinesInner.fieldNo100.set(t5539rec.dfact96);
		generalCopyLinesInner.fieldNo101.set(t5539rec.dfact97);
		generalCopyLinesInner.fieldNo102.set(t5539rec.dfact98);
		generalCopyLinesInner.fieldNo103.set(t5539rec.dfact99);
		generalCopyLinesInner.fieldNo104.set(t5539rec.fact01);
		generalCopyLinesInner.fieldNo105.set(t5539rec.fact02);
		generalCopyLinesInner.fieldNo106.set(t5539rec.fact03);
		generalCopyLinesInner.fieldNo107.set(t5539rec.fact04);
		generalCopyLinesInner.fieldNo108.set(t5539rec.fact05);
		generalCopyLinesInner.fieldNo109.set(t5539rec.fact06);
		generalCopyLinesInner.fieldNo110.set(t5539rec.fact07);
		generalCopyLinesInner.fieldNo111.set(t5539rec.fact08);
		generalCopyLinesInner.fieldNo112.set(t5539rec.fact09);
		generalCopyLinesInner.fieldNo113.set(t5539rec.fact10);
		generalCopyLinesInner.fieldNo114.set(t5539rec.fact11);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Discount Factors - Contract or Fixed Term      S5539");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(70);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 10, FILLER).init(" Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 18);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 23, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 31);
	private FixedLengthStringData filler6 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 40);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(76);
	private FixedLengthStringData filler7 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine003, 12, FILLER).init("1      2      3     4      5      6      7      8      9     10");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler9 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" 0");
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(76);
	private FixedLengthStringData filler19 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 0, FILLER).init("10");
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(76);
	private FixedLengthStringData filler29 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 0, FILLER).init("20");
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(76);
	private FixedLengthStringData filler39 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init("30");
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(76);
	private FixedLengthStringData filler49 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init("40");
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(76);
	private FixedLengthStringData filler59 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 0, FILLER).init("50");
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(76);
	private FixedLengthStringData filler69 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init("60");
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(76);
	private FixedLengthStringData filler79 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 0, FILLER).init("70");
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(76);
	private FixedLengthStringData filler89 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 0, FILLER).init("80");
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(76);
	private FixedLengthStringData filler99 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 0, FILLER).init("90");
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(76);
	private FixedLengthStringData filler109 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 0, FILLER).init("100");
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler113 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 71).setPattern("ZZZZZ");
}
}
