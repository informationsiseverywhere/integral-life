/*
 * File: Br608.java
 * Date: 29 August 2009 22:23:04
 * Author: Quipoz Limited
 * 
 * Class transformed from BR608.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.UfndudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br608TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br608DTO;
import com.csc.smart.procedures.Gettim;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*                  DEFERRED UNIT FUND BALANCE UPDATE
*                  ---------------------------------
*
* This program updates the unit fund balances for the company
* processing has been deferred from the batch schedules.
*
* Due to the way some batch schedules work, if multi-thread
* processing is used, it is possible for the processing to abort
* if one of the threads tries to access an unit fund balance
* (UFND) record which is already held for update by another
* thread.
* To avoid this situation, it is now possible to defer the
* updates to the UFND to allow the update records to be sorted
* and split in such a way as not to cause a clash.
*
* This program reads through the UFNX Splitter file, accumulates
* the balance and on change of Key, updates the UFNDPF.
*
* Control Totals used in this program:
*
*    CT01 - Number of fund records updated.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Br608 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	//private UfnxpfTableDAM ufnxpf = new UfnxpfTableDAM();
	//private UfnxpfTableDAM ufnxpfRec = new UfnxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR608");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* UFNX parameters*/
	//private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	//private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	//private FixedLengthStringData wsaaUfnxFn = new FixedLengthStringData(10);
	//private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaUfnxFn, 0, FILLER).init("UFNX");
	//private FixedLengthStringData wsaaUfnxRunid = new FixedLengthStringData(2).isAPartOf(wsaaUfnxFn, 4);
	//private ZonedDecimalData wsaaUfnxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUfnxFn, 6).setUnsigned();

	//private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	//private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	//private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaFirstTimeIn = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeIn = new Validator(wsaaFirstTimeIn, "Y");
	private ZonedDecimalData wsaaNofunts = new ZonedDecimalData(16, 5);
	private FixedLengthStringData wsaaEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaUfnxKey = new FixedLengthStringData(15);
	private FixedLengthStringData wsaaUfnxCompany = new FixedLengthStringData(1).isAPartOf(wsaaUfnxKey, 0);
	private FixedLengthStringData wsaaUfnxVrtfund = new FixedLengthStringData(4).isAPartOf(wsaaUfnxKey, 1);
	private FixedLengthStringData wsaaUfnxUnittype = new FixedLengthStringData(1).isAPartOf(wsaaUfnxKey, 5);
	//private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	//private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	//private PackedDecimalData wsaaJobno = new PackedDecimalData(8, 0);
	private static final String ufndudlrec = "UFNDUDLREC";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private UfndudlTableDAM ufndudlIO = new UfndudlTableDAM();
	//private UfnxpfTableDAM ufnxpfData = new UfnxpfTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	
    private Br608TempDAO tempDAO = getApplicationContext().getBean("br608TempDAO", Br608TempDAO.class);
	private List<Br608DTO> br608DtoList = new LinkedList<Br608DTO>();
	private Iterator<Br608DTO> br608DtoIter;

	private int batchID;
	private int batchExtractSize;
	private Br608DTO dto;
	
	//private int ctrCT01; 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		accumTotals3040, 
		exit3090
	}

	public Br608() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/***** No additional restart processing.*/
		/*EXIT*/
	}

	protected void initialise1000() {
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		this.tempDAO.buildTempData();
		br608DtoList = tempDAO.findTempDataResult(batchExtractSize, batchID);
		if (br608DtoList != null && br608DtoList.size() > 0) {
			br608DtoIter = br608DtoList.iterator();
		} else {
			wsaaEdterror.set(varcom.endp);
			return;
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		callProgram(Gettim.class, wsaaTime);
	}

//protected void initialise1010()
//	{
//		/* Point to correct member of UFNXPF.*/
//		wsaaUfnxRunid.set(bprdIO.getSystemParam04());
//		wsaaUfnxJobno.set(bsscIO.getScheduleNumber());
//		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
//		/* Do the override*/
//		StringUtil stringVariable1 = new StringUtil();
//		stringVariable1.addExpression("OVRDBF FILE(UFNXPF) TOFILE(");
//		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
//		stringVariable1.addExpression("/");
//		stringVariable1.addExpression(wsaaUfnxFn);
//		stringVariable1.addExpression(") MBR(");
//		stringVariable1.addExpression(wsaaThreadMember);
//		stringVariable1.addExpression(")  SEQONLY(*YES 1000)");
//		stringVariable1.setStringInto(wsaaQcmdexc);
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//		ufnxpf.openInput();
//		datcon1rec.function.set(varcom.tday);
//		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
//		if (isNE(datcon1rec.statuz,varcom.oK)) {
//			syserrrec.statuz.set(datcon1rec.statuz);
//			syserrrec.params.set(datcon1rec.datcon1Rec);
//			fatalError600();
//		}
//		callProgram(Gettim.class, wsaaTime);
//	}

	/**
	* <pre>
	*1100-DELETE-UFNS SECTION.                                <S19FIX>
	*1100-READ-FILE.                                                  
	*    INITIALIZE                     UFNS-DATA-AREA.               
	*    MOVE WSAA-COMPANY           TO UFNS-COMPANY.                 
	*    MOVE WSAA-JOBNO             TO UFNS-JOBNO.                   
	*    MOVE UFNSREC                TO UFNS-FORMAT.                  
	*    MOVE BEGNH                  TO UFNS-FUNCTION.                
	*    CALL 'UFNSIO'            USING UFNS-PARAMS.                  
	*    PERFORM                                                      
	*      UNTIL UFNS-STATUZ      NOT = O-K                           
	*      OR    UFNS-COMPANY     NOT = WSAA-COMPANY                  
	*      OR    UFNS-JOBNO       NOT = WSAA-JOBNO                    
	*        MOVE DELET              TO UFNS-FUNCTION                 
	*        CALL 'UFNSIO'        USING UFNS-PARAMS                   
	*        IF UFNS-STATUZ       NOT = O-K                           
	*           MOVE UFNS-STATUZ     TO SYSR-STATUZ                   
	*           MOVE UFNS-PARAMS     TO SYSR-PARAMS                   
	*           PERFORM 600-FATAL-ERROR                               
	*        END-IF                                                   
	*        MOVE NEXTR              TO UFNS-FUNCTION                 
	*        CALL 'UFNSIO'        USING UFNS-PARAMS                   
	*        IF UFNS-STATUZ       NOT = O-K AND ENDP                  
	*           MOVE UFNS-STATUZ     TO SYSR-STATUZ                   
	*           MOVE UFNS-PARAMS     TO SYSR-PARAMS                   
	*           PERFORM 600-FATAL-ERROR                               
	*        END-IF                                                   
	*    END-PERFORM.                                                 
	*1190-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
	protected void readFile2000() {
		// fwang3
		
		if (br608DtoList != null && br608DtoList.size() > 0) {
			if (br608DtoIter.hasNext()) {
				dto = br608DtoIter.next();
			
		} else {
			wsaaEdterror.set(varcom.endp);
			return;
		}		
	
	//	if (br608DtoIter.hasNext()) {
	//		dto = br608DtoIter.next();
		} else {
			batchID++;
			br608DtoList = tempDAO.findTempDataResult(batchExtractSize, batchID);
			if (br608DtoList != null && br608DtoList.size() > 0) {
				br608DtoIter = br608DtoList.iterator();
				dto = br608DtoIter.next();
			} else {
				wsaaEdterror.set(varcom.endp);
				return;
			}
		}
	}

	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
					readUfnd3020();
					setWriteFunction3030();
				case accumTotals3040: 		
					accumTotals3040();
				case exit3090: 
				}	
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}

	}

protected void update3010()
	{
		/* Below, the working storage UFNX key is set. This is compared*/
		/* with the UFND key to determine if there has been key change.*/
		/* Once EOF has been reached, the program needs to complete*/
		/* the 3000-UPDATE section once more to update the final UFND*/
		/* record. Obviously, there is no UFNX key so in this*/
		/* circumstance, the program will set this key to spaces. This*/
		/* will force a change of key and so cause the final record to*/
		/* be updated.*/
		if (isEQ(wsaaEdterror,varcom.endp)) {
			wsaaUfnxKey.set(SPACES);
		}
		else {
			wsaaUfnxCompany.set(dto.getCompany());
			wsaaUfnxVrtfund.set(dto.getVrtfund());
			wsaaUfnxUnittype.set(dto.getUnityp());
		}
		/* By-pass processing if first time through.*/
		/* If there is no change in key, recalculate the current*/
		/* balance. Otherwise, update the BBAL record.*/
		if (firstTimeIn.isTrue()) {
			wsaaFirstTimeIn.set("N");
		}
		else {
			if (isEQ(wsaaUfnxKey,ufndudlIO.getDataKey())) {
				goTo(GotoLabel.accumTotals3040);
			}
			else {
				ufndudlIO.setNofunts(wsaaNofunts);
				SmartFileCode.execute(appVars, ufndudlIO);
				if (isNE(ufndudlIO.getStatuz(),varcom.oK)) {
					syserrrec.statuz.set(ufndudlIO.getStatuz());
					syserrrec.params.set(ufndudlIO.getParams());
					fatalError600();
				}
				if (isEQ(ufndudlIO.getFunction(),varcom.readh)) {
					ufndudlIO.setNofunts(wsaaNofunts);
					ufndudlIO.setFunction(varcom.rewrt);
					SmartFileCode.execute(appVars, ufndudlIO);
					if (isNE(ufndudlIO.getStatuz(),varcom.oK)) {
						syserrrec.statuz.set(ufndudlIO.getStatuz());
						syserrrec.params.set(ufndudlIO.getParams());
						fatalError600();
					}
					contotrec.totno.set(ct01);
					contotrec.totval.set(1);
					callContot001();
				}
			}
		}
		if (isEQ(wsaaEdterror,varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
	}

protected void readUfnd3020()
	{
		/* Read UFND key.*/
		ufndudlIO.setDataKey(wsaaUfnxKey);
		ufndudlIO.setFormat(ufndudlrec);
		ufndudlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, ufndudlIO);
		if (isNE(ufndudlIO.getStatuz(),varcom.oK)
		&& isNE(ufndudlIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(ufndudlIO.getStatuz());
			syserrrec.params.set(ufndudlIO.getParams());
			fatalError600();
		}
	}

protected void setWriteFunction3030()
	{
		/* Determine UFND function, either REWRT or WRITR.*/
		ufndudlIO.setFormat(ufndudlrec);
		if (isEQ(ufndudlIO.getStatuz(),varcom.oK)) {
			ufndudlIO.setFunction(varcom.readh);
			wsaaNofunts.set(ufndudlIO.getNofunts());
		}
		else {
			ufndudlIO.setFunction(varcom.writr);
			wsaaNofunts.set(ZERO);
		}
	}

	protected void accumTotals3040() {
		/* Recalculate balance of incoming UFNX records. */
		wsaaNofunts.set(wsaaNofunts.getbigdata().add(dto.getNofunts()));
	}

protected void commit3500()
	{
		/*COMMIT*/
		/***** No additional commitment processing.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/***** No additional rollback processing.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/* Close UFNXPF and delete override.*/
		//ufnxpf.close();
		//wsaaQcmdexc.set("DLTOVR FILE(UFNXPF)");
		//com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
