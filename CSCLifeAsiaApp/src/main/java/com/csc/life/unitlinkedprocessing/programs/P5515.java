/*
 * File: P5515.java
 * Date: 30 August 2009 0:29:36
 * Author: Quipoz Limited
 * 
 * Class transformed from P5515.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.procedures.T5515pt;
import com.csc.life.unitlinkedprocessing.screens.S5515ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5515 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5515");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5515ScreenVars sv = ScreenProgram.getScreenVars( S5515ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5515() {
		super();
		screenVars = sv;
		new ScreenModel("S5515", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5515rec.t5515Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5515rec.acumbof.set(ZERO);
		t5515rec.accumRounding.set(ZERO);
		t5515rec.discountOfferPercent.set(ZERO);
		t5515rec.initBidOffer.set(ZERO);
		t5515rec.initialRounding.set(ZERO);
		t5515rec.managementCharge.set(ZERO);
		t5515rec.tolerance.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.acumbof.set(t5515rec.acumbof);
		sv.accumRounding.set(t5515rec.accumRounding);
		sv.btobid.set(t5515rec.btobid);
		sv.btodisc.set(t5515rec.btodisc);
		sv.discountOfferPercent.set(t5515rec.discountOfferPercent);
		sv.btooff.set(t5515rec.btooff);
		sv.currcode.set(t5515rec.currcode);
		sv.initBidOffer.set(t5515rec.initBidOffer);
		sv.initialRounding.set(t5515rec.initialRounding);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.initAccumSame.set(t5515rec.initAccumSame);
		sv.managementCharge.set(t5515rec.managementCharge);
		sv.otoff.set(t5515rec.otoff);
		sv.tolerance.set(t5515rec.tolerance);
		sv.unitType.set(t5515rec.unitType);
		sv.zfundtyp.set(t5515rec.zfundtyp);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5515rec.t5515Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.acumbof,t5515rec.acumbof)) {
			t5515rec.acumbof.set(sv.acumbof);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.accumRounding,t5515rec.accumRounding)) {
			t5515rec.accumRounding.set(sv.accumRounding);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.btobid,t5515rec.btobid)) {
			t5515rec.btobid.set(sv.btobid);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.btodisc,t5515rec.btodisc)) {
			t5515rec.btodisc.set(sv.btodisc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.discountOfferPercent,t5515rec.discountOfferPercent)) {
			t5515rec.discountOfferPercent.set(sv.discountOfferPercent);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.btooff,t5515rec.btooff)) {
			t5515rec.btooff.set(sv.btooff);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.currcode,t5515rec.currcode)) {
			t5515rec.currcode.set(sv.currcode);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.initBidOffer,t5515rec.initBidOffer)) {
			t5515rec.initBidOffer.set(sv.initBidOffer);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.initialRounding,t5515rec.initialRounding)) {
			t5515rec.initialRounding.set(sv.initialRounding);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.initAccumSame,t5515rec.initAccumSame)) {
			t5515rec.initAccumSame.set(sv.initAccumSame);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.managementCharge,t5515rec.managementCharge)) {
			t5515rec.managementCharge.set(sv.managementCharge);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.otoff,t5515rec.otoff)) {
			t5515rec.otoff.set(sv.otoff);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.tolerance,t5515rec.tolerance)) {
			t5515rec.tolerance.set(sv.tolerance);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.unitType,t5515rec.unitType)) {
			t5515rec.unitType.set(sv.unitType);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zfundtyp,t5515rec.zfundtyp)) {
			t5515rec.zfundtyp.set(sv.zfundtyp);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5515pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
