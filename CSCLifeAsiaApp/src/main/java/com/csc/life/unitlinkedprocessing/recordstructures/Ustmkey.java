package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:44
 * Description:
 * Copybook name: USTMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustmKey = new FixedLengthStringData(64).isAPartOf(ustmFileKey, 0, REDEFINE);
  	public FixedLengthStringData ustmChdrcoy = new FixedLengthStringData(1).isAPartOf(ustmKey, 0);
  	public FixedLengthStringData ustmChdrnum = new FixedLengthStringData(8).isAPartOf(ustmKey, 1);
  	public PackedDecimalData ustmEffdate = new PackedDecimalData(8, 0).isAPartOf(ustmKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(ustmKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}