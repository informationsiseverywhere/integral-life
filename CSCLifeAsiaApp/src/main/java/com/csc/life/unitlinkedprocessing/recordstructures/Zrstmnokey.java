package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:23
 * Description:
 * Copybook name: ZRSTMNOKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrstmnokey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrstmnoFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zrstmnoKey = new FixedLengthStringData(256).isAPartOf(zrstmnoFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrstmnoChdrcoy = new FixedLengthStringData(1).isAPartOf(zrstmnoKey, 0);
  	public FixedLengthStringData zrstmnoChdrnum = new FixedLengthStringData(8).isAPartOf(zrstmnoKey, 1);
  	public FixedLengthStringData zrstmnoLife = new FixedLengthStringData(2).isAPartOf(zrstmnoKey, 9);
  	public FixedLengthStringData zrstmnoCoverage = new FixedLengthStringData(2).isAPartOf(zrstmnoKey, 11);
  	public FixedLengthStringData zrstmnoRider = new FixedLengthStringData(2).isAPartOf(zrstmnoKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(zrstmnoKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrstmnoFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrstmnoFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}