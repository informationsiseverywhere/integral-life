package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:26
 * Description:
 * Copybook name: ULNKRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ulnkrnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ulnkrnlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ulnkrnlKey = new FixedLengthStringData(64).isAPartOf(ulnkrnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData ulnkrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(ulnkrnlKey, 0);
  	public FixedLengthStringData ulnkrnlChdrnum = new FixedLengthStringData(8).isAPartOf(ulnkrnlKey, 1);
  	public FixedLengthStringData ulnkrnlLife = new FixedLengthStringData(2).isAPartOf(ulnkrnlKey, 9);
  	public FixedLengthStringData ulnkrnlCoverage = new FixedLengthStringData(2).isAPartOf(ulnkrnlKey, 11);
  	public FixedLengthStringData ulnkrnlRider = new FixedLengthStringData(2).isAPartOf(ulnkrnlKey, 13);
  	public PackedDecimalData ulnkrnlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ulnkrnlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(ulnkrnlKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ulnkrnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ulnkrnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}