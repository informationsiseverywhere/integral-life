/*
 * File: P5102.java
 * Date: 30 August 2009 0:07:18
 * Author: Quipoz Limited
 *
 * Class transformed from P5102.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.procedures.Vpucwth;
import com.csc.life.productdefinition.procedures.Vpxutrs;
import com.csc.life.productdefinition.procedures.Vpxutrsrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6690rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.unitlinkedprocessing.recordstructures.Rwcalcpy;
import com.csc.life.unitlinkedprocessing.screens.S5102ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*  P5102 - Regular Withdrawals.
*  ----------------------------
*
*  Overview.
*  ---------
*
*  This program will perform some of the maintenance  functions
*  for  Regular  Withdrawals.  It  will be invoked initially by
*  the Regular Payments sub-menu and may also  be  called  from
*  within Contract Enquiry.
*
*  The following functions will be catered for:
*
*       . Register
*       . Adjust
*       . Enquire
*
*  The  Register  function  corresponds  to  Create and will be
*  carried out when WSSP-FLAG is 'C'.
*
*  The Adjust  function  corresponds  to  Modify  and  will  be
*  performed when WSSP-FLAG is 'M'.
*
*  The  Enquire  function will be carried out when WSSP-FLAG is
*  'I'.
*
*  The actual  payments  themselves  are  created  by  a  batch
*  payments  job  which  will  only operate on withdrawals that
*  are in an 'Approved' state and  are  due  for  payment.  The
*  Batch  Payments  job  will  effect  any currency conversions
*  that are necessary  at  the  time  the  payments  are  made.
*  Withdrawals  will  be  registered  in  the  currency  of the
*  contract  to  which  they  attach  but  a  separate  Payment
*  Currency  may  be  specified  at  the time the withdrawal is
*  approved.
*
*  The Sub-menu will control which actions are allowed  against
*  the withdrawal details.
*
*
*  Initialise.
*  -----------
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  Initialise all relevant variables  and  prepare  the  screen
*  for display.
*
*  Set the variable heading of the screen to the long description
*  of the Transaction Code by reading table T1688.
*
*  Perform a RETRV on the CHDRRGP and REGP files.
*
*  Use  CHDRRGP  to  display  the heading details on the screen
*  using T5688  for  the  contract  type  description  and  the
*  Client  file  for  the  relevant  client  names.  The  short
*  descriptions for  the  contracts  risk  and  premium  status
*  codes  should  be  obtained  from  T3623,  (Risk Status) and
*  T3588, (Premium Status). The contract  currency  description
*  should be obtained from T3629.
*
*  For  Registration  set  the Payment Currency to the Contract
*  Currency.
*
*  Display the REGP details on the screen  looking  up  all  of
*  the descriptions from DESC where appropriate.
*
*  For  Adjustments  or  Enquiries use the claim status to read
*  table T5400 to obtain the short description.  Also  use  the
*  Payment  Type  to  read  table  T6691  to obtain the Regular
*  Payment Type short description. For  Registration  where  no
*  claim  status  or payment type has yet been established read
*  T6690 with the Component Code as key, pick  up  the  Default
*  Regular  Payment  Type  and use this to access T6691 for the
*  short description. Remember to replace  this  later  if  the
*  entered Reason Code has a different Payment Type.
*
*  Read   table   T6693   with   the   current  Payment  Status
*  concatenated with the CRTABLE of the  associated  component.
*  Locate  the  Transaction  Code  of the transaction currently
*  being processed and select the  corresponding  Next  Payment
*  Status.  For  Registration the payment status will be set to
*  '**' for the read of the table. Also  for  Registration  use
*  the  Next Payment Status immediately to access T5400 for the
*  short description.
*
*  If the bank details on REGP are non-blank set a '+'  in  the
*  Bank Details indicator field.
*
*  If there are Follow Ups currently awaiting completion set  a
*  '+'   in  the  Follow  Ups  indicator  field.  This  can  be
*  determined  by  reading  FLUPRGP  with  a  key  of  CHDRCOY,
*  CHDRNUM,  CLAMNUM a FUPNO of zero and a function of BEGN. If
*  a record is found that matches on Company,  Contract  Number
*  and Claim Number then there are Follow Ups for the payment.
*
*  For  Register, the Regular Payment Sequence Number, RGPYNUM,
*  should be  set  to  1  greater  than  the  previous  highest
*  sequence  number  for  this contract. So read REGPENQ with a
*  function of BEGN and a Sequence Number set  to  all  9's  to
*  find  the  most  recently  added  Regular Payment record and
*  increment that Sequence Number by 1 to use on this file.  If
*  End  of  File  is  received  or  the  returned  record has a
*  different Company or Contract number then set  the  Sequence
*  Number to 1. Display the value on the screen.
*
*  For  Modify  and Enquire display the frequency from the REGP
*  file otherwise leave blank.
*
*  If there is a cancellation date display  it  on  the  screen
*  and  set off the indicators so that the date and its literal
*  appear on the screen.
*
*  Calculate an estimated fund value to display on  the  screen
*  to   give   an   indication  of  the  amount  available  for
*  withdrawal. It will  not  be  completely  accurate  as  fund
*  prices   will   vary  between  registering  the  withdrawal,
*  approving it and each time it gets processed  in  the  batch
*  run.
*
*       Read in the coverage details using COVRRGW with a
*       BEGN, obtain the calculation subroutine by reading
*       T5540 with the CRTABLE and then reading T6598 with the
*       withdrawal method.
*
*       Call the calculation subroutine using the copybook
*       RWCALCPY.
*
*      If the Withdrawal currency is different from the Fund
*      Currency convert the estimated amount to the
*      withdrawal currency.
*
*      If the withdrawal is on a summarised part plan and the
*      estimated value is not zero, check if the plan suffix
*      = 1 and calculate the estimated value =
*      (estimated value - (estimated value * (policies
*       summarised - 1) / policies summarised)).
*
*      If it is not = 1 calculate the estimated value =
*     (estimated value / policies summarised).
*
*     Add the estimated value to the estimated total.
*
*  Move the estimated total to the screen.
*
*
*  Display and Validation. (2000 Section).
*  ---------------------------------------
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If WSSP-FLAG  =  'I'  protect  the  screen  apart  from  the
*  screen switching indicator.
*
*  Default  the  Claim  Currency  to the Contract Currency Code
*  from CHDRRGP.
*
*  If the payment is not  active  i.e.  Last  Payment  Date  is
*  equal VRCM-MAX-DATE, protect the Next Payment Date.
*
*  Converse with the screen using the I/O module.
*
*  If  SCRN-STATUZ  equals  anything  other  than  O-K  or CALC
*  redisplay the screen with an error message.
*
*  If 'CALC' has been pressed set WSSP-EDTERROR to 'Y'.
*
*  Read  T6691  to  obtain  the  Regular  Payment  Type   short
*  description for display.
*
*  .  Payment Method. This is mandatory, check that it has been
*  entered. If entered it will be validated by the  screen  I/O
*  module. Read the DESC file to obtain the short description.
*
*  .  Payment Currency. This is validated by the I/O module but
*  the  program  must  read  the  DESC  file  to   obtain   the
*  decription.  If the user leaves it blank then re-set it from
*  the Contract Currency.
*
*  .  Frequency  Code.  The  user   may   only   select   those
*  frequencies  as  used  on T5542, so if the frequency entered
*  does not match one on the table, redisplay the  screen  with
*  an  error  message. T5542 is read with the withdrawal method
*  from T5540 concatonated with the currency code.  Read  T3590
*  to obtain the short description.
*
*  .  Payee  Client.  Read  table  T6694  with  the  Method  of
*  Payment. The 'TO' Sub Account is obtained  from  here  along
*  with  rules  determining  what  extra  details are required.
*  Check  the  Bank,  Payee  and  Contract   details   required
*  indicators.  These are not mutually exclusive so all must be
*  checked. If the  'Payee  Details'  is  'Y'  then  the  Payee
*  client  field is mandatory otherwise it must not be entered.
*  If it is mandatory check that the entry is  a  valid  client
*  number. If found format the name for display.
*
*  .  Percentage.  If  an amount is entered then the percentage
*  field cannot contain a  value.  If  it  does  redisplay  the
*  screen with an error message.
*
*  .  Amount To Pay. If a percentage value is entered then this
*  field must be left blank, if it is not redisplay the  screen
*  with an error.
*
*      If neither field is entered redisplay the screen with
*      an error message stating one must be entered.
*
*      Check that the payment amount or the percentage amount
*      (expressed as a value of the estimated fund value) are
*      not less than the minimum withdrawal amount from T5542.
*
*      If the payment amount is > than estimated fund value
*      redisplay the screen requesting approval.
*
*      If the difference in the Withdrawal value (percent or
*      amount) from the Minimum Withdrawal amount (T5542) is
*      less than the minimum fee (T5542) redisplay the screen
*      requesting approval
*
*  .  Destination  Key.  If  the  'Contract  Details  Required'
*  indicator on T6694 is 'Y' then a valid contract number  must
*  be entered here. Otherwise the field must be blank.
*
*  .   Registration   Date.  This  is  the  Effective  Date  of
*  Withdrawal and is mandatory. It may not be greater than  the
*  current  system date and although a date in the past will be
*  accepted,  it  may  not  be  less  than  the  Contract  Risk
*  Commencement Date.
*
*  .  First  Payment  Date.  This is mandatory. If no entry has
*  been made then redisplay the screen with an error message.
*  If an entry has been made then check that it does  not  fall
*  within  the  Minimum  Period as defined on T5542 and that it
*  is not less than the Registration Date.
*
*  . Review Date. This is optional. If it is entered it  should
*  not  be less than the First Payment Date or the Registration
*  Date. If it is not entered it should be set to Max Date.
*
*  . Next Payment  Date.  For  Registration  default  the  Next
*  Payment Date to the First Payment Date and protect it.
*  If  it  is  a modification the Next Payment Date can only be
*  altered if the Last Payment Date is not = Max  Date.  If  so
*  the following must be checked.
*
*       . If the Next Payment Date is blank, redisplay the
*         screen with a message that the field must be entered.
*
*       . It must be greater than the Last Payment Date.
*
*       . It must not be greater than the Review Date.
*
*       . It must not be greater than the Final Payment Date.
*
*  .  Final  Payment  Date.  This is optional and if left blank
*  will be set to Max Date. If entered  it  must  not  be  less
*  than the Next Payment Date.
*
*  .  Anniversary  Date.  This  is  optional  but if entered it
*  should not be less than Registration Date.
*
*  . The Bank details indicator may only be 'X', '+' or space.
*
*       If the Bank Details required indicator from T6694
*       equals 'Y' move 'X' to the Bank Details indicator on
*       the screen.
*
*       If the Payee on the screen is different from the
*       previous Payee and the Bank Details indicator from
*       T6694 = 'Y' blank out the Bank Details on the REGP
*       record and move 'X' to the Indicator on the screen.
*
*       If the Bank Details indicator on T6694 is not = 'Y'
*       move spaces to the Bank Details on the REGP record and
*       spaces to the indicator on the screen.
*
*  Updating.
*  ---------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If  WSSP-FLAG  =  'I'  and First Time Flag = 'Y' read in the
*  CHDR record, then skip this section.
*
*  If this is the first time through the 3000 section,  ('First
*  Time  Flag'  is  set  to 'Y'), then carry out the Valid Flag
*  '2' updating on the Contract Header, write  a  new  Contract
*  Header  with  an  incremented  TRANNO and updated POLSUM and
*  POLINC fields.
*
*  If the program is in Modify mode, (WSSP-FLAG  =  'M'),  then
*  the  current  REGP record must be made into a history record
*  by setting the REGP  Validflag  to  '2'  and  performing  an
*  UPDAT on REGP.
*
*  Move  all the screen fields to REGP and place the new TRANNO
*  on the record.
*
*  If the Bank Details indicator is 'X' then  perform  a  KEEPS
*  on the REGP file and exit section.
*
*  Otherwise  the update proper is to be performed so write the
*  new REGP record.
*
*  Write a PTRN record to reflect changes to Contract Header.
*
*  If it is required  to  break  out  the  Plan,  transfer  the
*  softlock  to  AT  and  call  the  AT  module  otherwise call
*  'SFTLOCK' to unlock the contract.
*
*
*  Where Next.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then  re-load the next 8 programs in
*  the stack from Working Storage.
*
*  If returning from the Bank Details  path  the  Bank  Details
*  indicator  will  be '?'. Check if Bank Details were actually
*  added by performing a RETRV on the REGP file.  If  the  Bank
*  details  on the REGP file are non blank indicate this to the
*  user by setting the Bank Details indicator field to '+'.  If
*  there  are  no  details  move  space  to  the  Bank  Details
*  indicator field.
*
*  If the Bank Details indicator is  'X'  then  switch  to  the
*  Bank Details path as follows:
*
*       Store  the  next  8  programs  in  the stack in Working
*       Storage.
*
*       Call GENSSW with an action of 'A'.
*
*       If there is an error code returned from GENSSW  use  it
*       as  an  error code on the Bank Details indicator field,
*       set WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG  to
*       SCRN-SCRNAME and go to exit.
*
*       Load  the  8  programs  returned  from GENSSW in to the
*       next 8 positions in the program stack.
*
*       Set the Bank Details indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then move space to WSSP-SEC-ACTN and
*  cause the program to redisplay  from  the  2000  section  by
*  setting WSSP-NEXTPROG to SCRN-SCRNAME and go to exit.
*
*  Add 1 to the program pointer and exit.
*
*  Notes.
*  ------
*
*
*  Tables Used.
*  ------------
*
*  . T3629 - Currency Code Details
*            Key: CURRCD
*
*  . T1688 - Transaction Codes
*            Key: Transaction Code
*
*  . T3588 - Contract Premium Status Codes
*            Key: PSTCDE from CHDRRGP
*
*  . T3623 - Contract Risk Status Codes
*            Key: STATCODE from CHDRRGP
*
*  . T5540 - General Unit Linked Details
*            Key: CRTABLE
*
*  . T5542 - Unit Withdrawal Rules
*            Key: T5540 Withdrawal Method || Currency Code
*
*  . T5661 - Follow-up Codes
*            Key: Follow-up Code
*
*  . T5677 - Default Follow-up Codes
*            Key: Transaction Code and Follow-up Method
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T6598 - Surrender Value Calculation Methods
*            Key: Withdrawal Method from T5540
*
*  . T5400 - Regular Payment Status
*            Key: Regular Payment Status Code
*
*  . T6690 - Default Regular Payment Types
*            Key: CRTABLE
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*  . T6694 - Regular Payment Method of Payment
*            Key: Regular Payment MOP
*
*****************************************************************
* </pre>
*/
public class P5102 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5102");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private ZonedDecimalData wsaaSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaSwitch, "1");
	private Validator partPlan = new Validator(wsaaSwitch, "2");
	private Validator summaryPartPlan = new Validator(wsaaSwitch, "3");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmountRemaining = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRevisedFundTotal = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaRegptype = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private String wsaaValidStatuz = "";
	private String wsaaStopProcess = "";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaT5542Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5542KeyWdmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5542Key, 0);
	private FixedLengthStringData wsaaT5542KeyCurrcd = new FixedLengthStringData(3).isAPartOf(wsaaT5542Key, 4);
	private PackedDecimalData wsaaMinimumPerc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMinimumAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAuxPerc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAuxAmount = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaFreqAux = new FixedLengthStringData(2);
	private String wsaaFreqFlag = "";
		/* WSAA-T5542-INFO */
	private PackedDecimalData wsaaFeemax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFeemin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFeepc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaFfamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWdlAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWdrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWdlFreq = new PackedDecimalData(3, 0);
	private static final int wsaaT5542TableSize = 6;

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(19);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaBrkoutPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);

	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Rgpystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);
	private ZonedDecimalData wsaaNextRgpy = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaPayclt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaStoreSub = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPymt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPymt2 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPrcntVal = new ZonedDecimalData(17, 2);
	private PackedDecimalData index1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private String firstTime = "";
	private FixedLengthStringData wsaaPaymentCurr = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaFirstTimeFlag = new FixedLengthStringData(1).init(SPACES);
	private Validator firstTimeIn = new Validator(wsaaFirstTimeFlag, "Y");

	private FixedLengthStringData wsaaClamnum2 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaClamnumFill = new ZonedDecimalData(3, 0).isAPartOf(wsaaClamnum2, 0).setUnsigned();
	private ZonedDecimalData wsaaRgpynum = new ZonedDecimalData(5, 0).isAPartOf(wsaaClamnum2, 3).setUnsigned();
		/* Variables to store temporary values of POLINC and POLSUM*/
	private PackedDecimalData wsaaPolinc = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
//	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
//	private RegpenqTableDAM regpenqIO = new RegpenqTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Rwcalcpy rwcalcpy = new Rwcalcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5542rec t5542rec = new T5542rec();
	private T5679rec t5679rec = new T5679rec();
	private T6690rec t6690rec = new T6690rec();
	private T6693rec t6693rec = new T6693rec();
	private T6694rec t6694rec = new T6694rec();
	private T5540rec t5540rec = new T5540rec();
	private T6598rec t6598rec = new T6598rec();
	private Wssplife wssplife = new Wssplife();
	private S5102ScreenVars sv = ScreenProgram.getScreenVars( S5102ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t6693Map = null;
	//ILJ-48 Starts
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private String itemPFX = "IT";
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-48 End

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		create1060,
		exit1090,
		checkCurrency2035,
		checkPercentage2050,
		checkDestination2060,
		checkReviewDate2075,
		checkForErrors2080,
		exit2090,
		gensww4010,
		nextProgram4020,
		exit4090
	}

	public P5102() {
		super();
		screenVars = sv;
		new ScreenModel("S5102", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		if(initialise1010()){
			create1060();
		}
	}

protected boolean initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			/*     MOVE ZERO                   TO WSAA-PYMT                  */
			/*     MOVE S5102-PYMT             TO WSAA-PYMT2                 */
			return false;
		}
		firstTimeIn.setTrue();
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		firstTime = "Y";
		wsaaValidStatuz = "N";
		wsaaStopProcess = "N";
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.aprvdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.cancelDate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.lastPaydate.set(varcom.vrcmMaxDate);
		sv.nextPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.revdte.set(varcom.vrcmMaxDate);
		if(cntDteFlag)	{
			sv.riskcommdte.set(varcom.vrcmMaxDate);	//ILJ-48
		}
		wsaaPymt.set(ZERO);
		wsaaPymt2.set(ZERO);
		wsaaFfamt.set(ZERO);
		wsaaFeepc.set(ZERO);
		wsaaFeemin.set(ZERO);
		wsaaWdlAmount.set(ZERO);
		wsaaWdrem.set(ZERO);
		
		wsaaWdlFreq.set(ZERO);
		sv.pymt.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.totamnt.set(ZERO);
		wsaaMinimumPerc.set(ZERO);
		wsaaAuxPerc.set(ZERO);
		wsaaAuxAmount.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaAmountRemaining.set(ZERO);
		wsaaCrtable.set(SPACES);
		wsaaPaymentCurr.set(SPACES);
		/* Find today's date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		/*  Read the Transaction Code Description for variable heading     */
		descIO.setDataKey(SPACES);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setDesctabl(tablesInner.t1688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		}
		else {
			sv.descrip.fill("?");
		}
		/*  Retrieve the Contract Header.*/
		chdrrgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/* Store POLINC and POLSUM in WSAA fields to update CHDRRGP*/
		/* after writing CHDR.*/
		wsaaPolsum.set(chdrrgpIO.getPolsum());
		wsaaPolinc.set(chdrrgpIO.getPolinc());
		/*  Release the Contract Header.*/
		chdrrgpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/*  Read the Contract Type Description.*/
		descIO.setDescitem(chdrrgpIO.getCnttype());
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/*  Read the Contract Currency Description.*/
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		}
		else {
			sv.currds.fill("?");
		}
		/*  Read the Premium Status Description.*/
		descIO.setDescitem(chdrrgpIO.getPstatcode());
		descIO.setDesctabl(tablesInner.t3588);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getLongdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/*  Read the Risk Status Description.*/
		descIO.setDescitem(chdrrgpIO.getStatcode());
		descIO.setDesctabl(tablesInner.t3623);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getLongdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/* Read T5679 to get allowable Coverage statii.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/*    Read Owner Details.*/
		cltsIO.setClntnum(chdrrgpIO.getCownnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(), "1"))) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		
		//ILJ-48 Starts
		if(cntDteFlag) {
			Hpadpf hpadpf = hpadpfDAO.getHpadData(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString());
			if(hpadpf!=null) {
				sv.riskcommdte.set(hpadpf.getRskcommdate());
			}
		}
		//ILJ-48 End
		/*    Set screen fields*/
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		sv.cownnum.set(chdrrgpIO.getCownnum());
		sv.occdate.set(chdrrgpIO.getOccdate());
		sv.ptdate.set(chdrrgpIO.getPtdate());
		sv.btdate.set(chdrrgpIO.getBtdate());
		sv.currcd.set(chdrrgpIO.getCntcurr());
		wsaaPaymentCurr.set(chdrrgpIO.getCntcurr());
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill("?");
		}
		sv.polinc.set(chdrrgpIO.getPolinc());
		/*    Retrieve the Regular Payment Record.*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/*    Release the Regular Payment Record.*/
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Set up the Cmpnt (Component) field on the screen - this is the*/
		/*  Coverage/Rider field from the REGP record.*/
		sv.crtable.set(regpIO.getCrtable());
		sv.plansfx.set(regpIO.getPlanSuffix());
		if (isEQ(regpIO.getPlanSuffix(), ZERO)) {
			sv.plansfx.set(chdrrgpIO.getPolinc());
			sv.plansfxOut[varcom.hi.toInt()].set("Y");
		}
		/*    Read the LIFE Details.*/
		lifeenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrrgpIO.getChdrnum());
		lifeenqIO.setLife(regpIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		SmartFileCode.execute(appVars, lifeenqIO);
		if ((isNE(lifeenqIO.getStatuz(), varcom.oK))
		&& (isNE(lifeenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(), lifeenqIO.getChdrcoy()))
		|| (isNE(chdrrgpIO.getChdrnum(), lifeenqIO.getChdrnum()))
		|| (isNE(regpIO.getLife(), lifeenqIO.getLife()))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
			return true;
		}
		sv.lifcnum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(), "1"))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/*  Non-display Cancel Date and Total Paid if in Create mode.*/
		if (isEQ(wsspcomn.flag, "C")) {
			sv.canceldateOut[varcom.nd.toInt()].set("Y");
		}
		return true;
	}

protected void create1060()
	{
		/*    Create a new Regular Payment or Modify/Enquire upon*/
		/*    and existing one.*/
		if (isEQ(wsspcomn.flag, "C")) {
			readT66935200();
			setUpRegp1700();
		}
		else {
			readRegpDetails1300();
		}
		checkFollowUp5000();
		/*    Calculate Fund Estimated value.*/
		if (isEQ(regpIO.getPlanSuffix(), ZERO)) {
			wsaaSwitch.set(1);
		}
		else {
			if (isGT(regpIO.getPlanSuffix(), chdrrgpIO.getPolsum())
			|| isEQ(chdrrgpIO.getPolsum(), 1)) {
				wsaaSwitch.set(2);
			}
			else {
				wsaaSwitch.set(3);
			}
		}
		rwcalcpy.planType.set(wsaaSwitch);
		List<Covrpf> covrrgwIOList = covrpfDAO.getCovrByComAndNum(regpIO.getChdrcoy().toString(),regpIO.getChdrnum().toString());
		boolean beginFlag = true;
		if(covrrgwIOList!=null&&!covrrgwIOList.isEmpty()){
		    for(Covrpf covrrgwIO:covrrgwIOList){
		        /* Begin on the coverage/rider record*/
		        if ((isNE(covrrgwIO.getLife(), regpIO.getLife()))
		        || (isNE(covrrgwIO.getCoverage(), regpIO.getCoverage()))
		        || (isNE(covrrgwIO.getRider(), regpIO.getRider()))) {
		            continue;
		        }
		        if (partPlan.isTrue()) {
		            if (isNE(regpIO.getPlanSuffix(), covrrgwIO.getPlanSuffix())) {
		                continue;
		            }
		        }
		        /* Check coverage statii against T5679*/
		        /*MOVE 1                      TO INDEX1.                       */
		        wsaaValidStatuz = "N";
		        wsaaStatcode.set(covrrgwIO.getStatcode());
		        wsaaPstcde.set(covrrgwIO.getPstatcode());
		        /*PERFORM UNTIL (INDEX1        > 12)  OR                       */
		        /*       (WSAA-VALID-STATUZ    = 'Y')                          */
		        /*   IF T5679-COV-RISK-STAT(INDEX1) NOT = SPACE                */
		        /*      IF (T5679-COV-RISK-STAT(INDEX1) = WSAA-STATCODE) AND   */
		        /*         (T5679-COV-PREM-STAT(INDEX1) = WSAA-PSTCDE)         */
		        /*          MOVE 13           TO INDEX1                        */
		        /*          MOVE 'Y'          TO WSAA-VALID-STATUZ             */
		        /*      END-IF                                                 */
		        /*   END-IF                                                    */
		        /*   ADD 1                    TO INDEX1                        */
		        /*END-PERFORM.                                                 */
		        for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		        || isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
		            if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
		                for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		                || isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
		                    if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
		                        wsaaValidStatuz = "Y";
		                    }
		                }
		            }
		        }
		        if (beginFlag) {
		            rwcalcpy.function.set(varcom.begn);
		            processComponents1850(covrrgwIO);
		        }
		        if (wholePlan.isTrue()) {
		            beginFlag=false;
		            continue;
		        }
		        else {
		           break;
		        }
		    }
		}
		

		if (isEQ(wsaaValidStatuz, "N")) {
			wsaaStopProcess = "Y";
		}
		sv.totestamt.set(wsaaEstimateTot);
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(), varcom.oK))
		&& (isNE(cltsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readRegpDetails1300()
	{
        /*    Read Payment Type Details.*/
        descIO.setDescitem(regpIO.getRgpytype());
        wsaaRegptype.set(regpIO.getRgpytype());
        descIO.setDesctabl(tablesInner.t6691);
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.rgpytypesd.set(descIO.getShortdesc());
        }
        else {
            sv.rgpytypesd.fill("?");
        }
        /*    Read Payee Details.*/
        if (isNE(regpIO.getPayclt(), SPACES)) {
            cltsIO.setClntnum(regpIO.getPayclt());
            getClientDetails1200();
            if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
            || (isNE(cltsIO.getValidflag(), "1"))) {
                sv.payenmeErr.set(errorsInner.e335);
                sv.payenme.set(SPACES);
            }
            else {
                plainname();
                sv.payenme.set(wsspcomn.longconfname);
            }
        }
        /*  Read the Claim Status Description.*/
        descIO.setDescitem(regpIO.getRgpystat());
        /* MOVE T6663                  TO DESC-DESCTABL.                */
        descIO.setDesctabl(tablesInner.t5400);
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.statdsc.set(descIO.getShortdesc());
        }
        else {
            sv.statdsc.fill("?");
        }
        /*  Read the Payment Method Description.*/
        descIO.setDescitem(regpIO.getRgpymop());
        descIO.setDesctabl(tablesInner.t6694);
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.rgpyshort.set(descIO.getShortdesc());
        }
        else {
            sv.rgpyshort.fill("?");
        }
        /*    Read the Frequency Code Description.*/
        descIO.setDescitem(regpIO.getRegpayfreq());
        descIO.setDesctabl(tablesInner.t3590);
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.frqdesc.set(descIO.getShortdesc());
        }
        else {
            sv.frqdesc.fill("?");
        }
        /*  Read the Claim Currency Description.*/
        descIO.setDescitem(regpIO.getCurrcd());
        descIO.setDesctabl(tablesInner.t3629);
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.clmcurdsc.set(descIO.getShortdesc());
        }
        else {
            sv.clmcurdsc.fill("?");
        }
        /* Set up the screen.*/
        sv.payclt.set(regpIO.getPayclt());
        sv.rgpynum.set(regpIO.getRgpynum());
        sv.rgpystat.set(regpIO.getRgpystat());
        sv.rgpymop.set(regpIO.getRgpymop());
        sv.regpayfreq.set(regpIO.getRegpayfreq());
        wsaaFreq.set(regpIO.getRegpayfreq());
        sv.prcnt.set(regpIO.getPrcnt());
        wsaaAuxPerc.set(regpIO.getPrcnt());
        sv.destkey.set(regpIO.getDestkey());
        sv.pymt.set(regpIO.getPymt());
        wsaaAuxAmount.set(regpIO.getPymt());
        sv.totamnt.set(regpIO.getTotamnt());
        sv.claimcur.set(regpIO.getCurrcd());
        if (isEQ(wsspcomn.flag, "I")) {
            sv.aprvdate.set(regpIO.getAprvdate());
        }
        else {
            sv.aprvdate.set(varcom.vrcmMaxDate);
        }
        sv.crtdate.set(regpIO.getCrtdate());
        sv.revdte.set(regpIO.getRevdte());
        sv.firstPaydate.set(regpIO.getFirstPaydate());
        sv.lastPaydate.set(regpIO.getLastPaydate());
        sv.nextPaydate.set(regpIO.getNextPaydate());
        sv.anvdate.set(regpIO.getAnvdate());
        sv.finalPaydate.set(regpIO.getFinalPaydate());
        if (isEQ(wsspcomn.flag, "I")) {
            sv.cancelDate.set(regpIO.getCancelDate());
            sv.canceldateOut[varcom.nd.toInt()].set(" ");
        }
        else {
            sv.canceldateOut[varcom.nd.toInt()].set("Y");
        }
        if ((isEQ(wsspcomn.flag, "M"))
        && (isNE(regpIO.getPayclt(), SPACES))) {
            wsaaPayclt.set(regpIO.getPayclt());
        }
        /* Check if bank details are required on the current contract.*/
        itdmIO.setDataKey(SPACES);
        itdmIO.setItemcoy(wsspcomn.company);
        itdmIO.setFormat(formatsInner.itdmrec);
        itdmIO.setItemtabl(tablesInner.t6694);
        itdmIO.setItempfx("IT");
        itdmIO.setItemitem(regpIO.getRgpymop());
        itdmIO.setItmfrm(chdrrgpIO.getOccdate());
        itdmIO.setFunction(varcom.begn);
        //performance improvement --  Niharika Modi
        itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
        itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
    
        SmartFileCode.execute(appVars, itdmIO);
        if ((isNE(itdmIO.getStatuz(), varcom.oK))
        && (isNE(itdmIO.getStatuz(), varcom.endp))) {
            syserrrec.params.set(itdmIO.getParams());
            fatalError600();
        }
        if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
        || (isNE(itdmIO.getItemtabl(), tablesInner.t6694))
        || (isNE(itdmIO.getItemitem(), regpIO.getRgpymop()))
        || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
            itdmIO.setGenarea(SPACES);
            sv.rgpymopErr.set(errorsInner.g529);
            return ;
        }
        t6694rec.t6694Rec.set(itdmIO.getGenarea());
        if (isEQ(t6694rec.bankreq, "Y")) {
            if ((isEQ(regpIO.getBankkey(), SPACES))
            || (isEQ(regpIO.getBankacckey(), SPACES))) {
                sv.ddind.set("X");
            }
            else {
                sv.ddind.set("+");
            }
        }
        else {
            if (isEQ(regpIO.getBankkey(), SPACES)
            && isEQ(regpIO.getBankacckey(), SPACES)) {
                sv.ddind.set(SPACES);
            }
            else {
                sv.ddind.set("+");
            }
        }
        sv.crtdateOut[varcom.pr.toInt()].set("Y");
        if ((isLT(regpIO.getFirstPaydate(), datcon1rec.intDate))
        && (isNE(regpIO.getLastPaydate(), varcom.vrcmMaxDate))) {
            sv.fpaydateOut[varcom.pr.toInt()].set("Y");
        }
	}

protected void setUpRegp1700()
	{
        /*    Locate most recent Regular Payment for this component and so*/
        /*    calculate the Regular Payment Number as being 1 greater.*/
        wsaaNextRgpy.set(ZERO);
        List<Regppf> regpenqIOList = regppfDAO.readRegpRecord(regpIO.getChdrcoy().toString(), regpIO.getChdrnum()
                .toString());
        if (regpenqIOList != null && !regpenqIOList.isEmpty()) {
            for (Regppf regpenqIO : regpenqIOList) {
                if (!"1".equals(regpenqIO.getValidflag())) {
                    continue;
                }
                if (isGT(regpenqIO.getRgpynum(), wsaaNextRgpy)) {
                    wsaaNextRgpy.set(regpenqIO.getRgpynum());
                }
            }
        }
    
        setPrecision(regpIO.getRgpynum(), 0);
        regpIO.setRgpynum(add(wsaaNextRgpy, 1));
        sv.crtdate.set(datcon1rec.intDate);
        sv.claimcur.set(chdrrgpIO.getCntcurr());
        sv.rgpynum.set(regpIO.getRgpynum());
        regpIO.setCurrcd(chdrrgpIO.getCntcurr());
        sv.claimcur.set(chdrrgpIO.getCntcurr());
        descIO.setDescitem(chdrrgpIO.getCntcurr());
        descIO.setDesctabl(tablesInner.t3629);
        /*  Read the Claim Currency description.*/
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.clmcurdsc.set(descIO.getShortdesc());
        }
        else {
            sv.clmcurdsc.fill("?");
        }
        /*  Read the Claim Status description.*/
        sv.rgpystat.set(wsaaRgpystat);
        descIO.setDescitem(wsaaRgpystat);
        /* MOVE T6663                  TO DESC-DESCTABL.                */
        descIO.setDesctabl(tablesInner.t5400);
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.statdsc.set(descIO.getShortdesc());
        }
        else {
            sv.statdsc.fill("?");
        }
        /*  Read the default Claim Type description.*/
        itemIO.setDataKey(SPACES);
        itemIO.setItempfx("IT");
        itemIO.setItemcoy(wsspcomn.company);
        itemIO.setItemtabl(tablesInner.t6690);
        itemIO.setItemitem(regpIO.getCrtable());
        itemIO.setFunction(varcom.readr);
        itemIO.setFormat(formatsInner.itemrec);
        SmartFileCode.execute(appVars, itemIO);
        if (isNE(itemIO.getStatuz(), varcom.oK)) {
            syserrrec.params.set(itemIO.getParams());
            fatalError600();
        }
        t6690rec.t6690Rec.set(itemIO.getGenarea());
        descIO.setDescitem(t6690rec.rgpytype);
        descIO.setDesctabl(tablesInner.t6691);
        findDesc1100();
        if (isEQ(descIO.getStatuz(), varcom.oK)) {
            sv.rgpytypesd.set(descIO.getShortdesc());
        }
        else {
            sv.rgpytypesd.fill("?");
        }

	}

protected void processComponents1850(Covrpf covrrgwIO)
	{
        /* Read table T5540 to find the surrender calculation subroutine.*/
        wsaaCrtable.set(covrrgwIO.getCrtable());
        obtainSurrenderCalc1500();
        rwcalcpy.estimatedVal.set(ZERO);
        rwcalcpy.polsum.set(ZERO);
        rwcalcpy.chdrChdrcoy.set(covrrgwIO.getChdrcoy());
        rwcalcpy.chdrChdrnum.set(covrrgwIO.getChdrnum());
        rwcalcpy.lifeLife.set(covrrgwIO.getLife());
        rwcalcpy.lifeJlife.set(covrrgwIO.getJlife());
        rwcalcpy.covrCoverage.set(covrrgwIO.getCoverage());
        rwcalcpy.covrRider.set(covrrgwIO.getRider());
        rwcalcpy.crtable.set(covrrgwIO.getCrtable());
        rwcalcpy.crrcd.set(covrrgwIO.getCrrcd());
        rwcalcpy.planSuffix.set(covrrgwIO.getPlanSuffix());
        rwcalcpy.convUnits.set(covrrgwIO.getConvertInitialUnits());
        rwcalcpy.pstatcode.set(covrrgwIO.getPstatcode());
        rwcalcpy.currcode.set(covrrgwIO.getPremCurrency());
        rwcalcpy.ptdate.set(sv.ptdate);
        rwcalcpy.language.set(wsspcomn.language);
        /* Allow a frequency of 00 for one-off payments                    */
        /*    MOVE WSAA-FREQ              TO RGWC-BILLFREQ.                */
        if (isEQ(wsaaFreq, ZERO)) {
            rwcalcpy.billfreq.set("01");
        }
        else {
            rwcalcpy.billfreq.set(wsaaFreq);
        }
        rwcalcpy.status.set(SPACES);
        if (isNE(regpIO.getPlanSuffix(), ZERO)) {
            rwcalcpy.polsum.set(chdrrgpIO.getPolsum());
        }
        while ( !(isEQ(rwcalcpy.status, varcom.endp))) {
            callSurMethod1600(covrrgwIO);
        }
	}

protected void obtainSurrenderCalc1500()
	{
        itdmIO.setDataKey(SPACES);
        itdmIO.setItemcoy(wsspcomn.company);
        itdmIO.setItemtabl(tablesInner.t5540);
        itdmIO.setItemitem(wsaaCrtable);
        itdmIO.setItmfrm(chdrrgpIO.getOccdate());
        itdmIO.setFunction(varcom.begn);
        //performance improvement --  Niharika Modi
        itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
        itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
    
        SmartFileCode.execute(appVars, itdmIO);
        if ((isNE(itdmIO.getStatuz(), varcom.oK))
        && (isNE(itdmIO.getStatuz(), varcom.endp))) {
            syserrrec.params.set(itdmIO.getParams());
            fatalError600();
        }
        if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
        || (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
        || (isNE(itdmIO.getItemitem(), wsaaCrtable))
        || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
            syserrrec.params.set(itdmIO.getParams());
            syserrrec.statuz.set(errorsInner.h055);
            fatalError600();
        }
        else {
            t5540rec.t5540Rec.set(itdmIO.getGenarea());
        }
        itemIO.setDataKey(SPACES);
        itemIO.setItemcoy(wsspcomn.company);
        itemIO.setFormat(formatsInner.itemrec);
        itemIO.setItemtabl(tablesInner.t6598);
        itemIO.setItempfx("IT");
        itemIO.setItemitem(t5540rec.wdmeth);
        itemIO.setFunction(varcom.readr);
        SmartFileCode.execute(appVars, itemIO);
        if ((isNE(itemIO.getStatuz(), varcom.oK))
        && (isNE(itemIO.getStatuz(), varcom.mrnf))) {
            syserrrec.params.set(itemIO.getParams());
            fatalError600();
        }
        if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
            /*     MOVE U040                TO SCRN-ERROR-CODE               */
            scrnparams.errorCode.set(errorsInner.h072);
            t6598rec.t6598Rec.set(SPACES);
        }
        else {
            t6598rec.t6598Rec.set(itemIO.getGenarea());
        }

	}

protected void callSurMethod1600(Covrpf covrrgwIO)
	{
        rwcalcpy.effdate.set(wsaaToday);
        rwcalcpy.type.set("F");
        if (isGT(covrrgwIO.getInstprem(), ZERO)) {
            rwcalcpy.singp.set(covrrgwIO.getInstprem());
        }
        else {
            rwcalcpy.singp.set(covrrgwIO.getSingp());
        }
        rwcalcpy.currcode.set(regpIO.getCurrcd());
        //Ticket #IVE-798 - RUL Product - Regular Withdrawls Calculation - Integration with latest PA compatible models - START
        //****Ticket #IVE-165 RUL Calculations - Regural Withdrwal  start
        Vpmcalcrec vpmcalcrec = new Vpmcalcrec();       
        Vpxutrsrec vpxutrsrec = new Vpxutrsrec();
        Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
        
        if(!AppVars.getInstance().getAppConfig().isVpmsEnable()) 
        {
            callProgram(t6598rec.calcprog, rwcalcpy.withdrawRec);
        }
        else
        {           
            vpmcalcrec.linkageArea.set(rwcalcpy.withdrawRec);       
            vpxutrsrec.function.set("INIT");
            vpmfmtrec.initialize();
            vpmfmtrec.date01.set(rwcalcpy.effdate);
            callProgram(Vpxutrs.class, vpmcalcrec.vpmcalcRec,vpxutrsrec);
            rwcalcpy.withdrawRec.set(vpmcalcrec.linkageArea);
            callProgram(t6598rec.calcprog, vpmfmtrec, rwcalcpy.withdrawRec, vpxutrsrec, chdrrgpIO);
            callProgram(Vpucwth.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
        } 
        //****Ticket #IVE-165 RUL Calculations - Regural Withdrwal  end
        //Ticket #IVE-798 - RUL Product - Regular Withdrawls Calculation - Integration with latest PA compatible models - END       
        
        if (isEQ(rwcalcpy.status, varcom.bomb)) {
            syserrrec.statuz.set(rwcalcpy.status);
            fatalError600();
        }
        if ((isNE(rwcalcpy.status, varcom.oK))
        && (isNE(rwcalcpy.status, varcom.endp))) {
            syserrrec.statuz.set(rwcalcpy.status);
            fatalError600();
        }
        if (isEQ(rwcalcpy.endf, "Y")) {
            return ;
        }
        if (isEQ(rwcalcpy.type, "C")) {
            if (isEQ(rwcalcpy.estimatedVal, ZERO)) {
                return ;
            }
        }
        if (isNE(sv.currcd, rwcalcpy.fundCurrcode)) {
            conlinkrec.amountIn.set(rwcalcpy.estimatedVal);
            convertCurrency1650();
            rwcalcpy.estimatedVal.set(conlinkrec.amountOut);
        }
        if (summaryPartPlan.isTrue()) {
            if (isNE(rwcalcpy.estimatedVal, ZERO)) {
                if (isEQ(sv.plansfx, 1)) {
                    compute(rwcalcpy.estimatedVal, 2).set((sub(rwcalcpy.estimatedVal, (div(mult(rwcalcpy.estimatedVal, (sub(chdrrgpIO.getPolsum(), 1))), chdrrgpIO.getPolsum())))));
                }
                else {
                    compute(rwcalcpy.estimatedVal, 2).set((div(rwcalcpy.estimatedVal, chdrrgpIO.getPolsum())));
                }
            }
        }
        zrdecplrec.amountIn.set(rwcalcpy.estimatedVal);
        callRounding6000();
        rwcalcpy.estimatedVal.set(zrdecplrec.amountOut);
        wsaaEstimateTot.add(rwcalcpy.estimatedVal);

	}

protected void convertCurrency1650()
	{
		/*START*/
		conlinkrec.function.set("CVRT");
		conlinkrec.currIn.set(rwcalcpy.fundCurrcode);
		conlinkrec.cashdate.set(wsaaToday);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(wsspcomn.company);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, "****")) {
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			/*         GO TO 2090-EXIT                                         */
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			protectScr2400();
		}
		/* If the payment has not yet started, ie. no date in the Last     */
		/* Paid Date field. then the Next Payment Date will be protected.  */
		/* This means that before the first payment has been made the Next */
		/* Payment Date will be protected, but the user will be able to    */
		/* influence the Next Payment Date by changing the First Payment   */
		/* Date. Once payment has started the user will have the option of */
		/* directly changing the Next Payment Date.                        */
		if (isEQ(sv.lastPaydate, varcom.vrcmMaxDate)) {
			sv.npaydateOut[varcom.pr.toInt()].set("Y");
		}
		/*  If invalid coverage status is discovered, output error message */
		/*   and protect the screen.                                       */
		if (isEQ(wsaaStopProcess, "Y")) {
			scrnparams.errorCode.set(errorsInner.h136);
			scrnparams.function.set(varcom.prot);
			wsspcomn.edterror.set("Y");
		}
		return ;
	}

protected void screenEdit2000()
	{
        /*    CALL 'S5102IO'              USING SCRN-SCREEN-PARAMS         */
        /*                                      S5102-DATA-AREA.           */
        /* Screen errors are now handled in the calling program.           */
        /*    PERFORM 200-SCREEN-ERRORS.                                   */
        wsspcomn.edterror.set(varcom.oK);
        if(validate2020()){
            checkPayment2030();
            checkCurrency2035();
            checkFreqency2040();
            if(checkPercentage2050()){
                validateFollow2065();
            }
            if(!checkDestination2060()){
                return;
            }
            checkReviewDate2075();
            checkPayDate2077();
            checkForErrors2080();
        }
	}

protected boolean validate2020()
	{
		/* If Stop Processing flag has been set by an invalid coverage stat*/
		/*  protect the entire screen AND ALLOW NO FURTHER PROCESSING*/
		if (isEQ(wsaaStopProcess, "Y")) {
			wsspcomn.edterror.set("Y");
			return false;
		}
		/*    Validate fields*/
		if ((isNE(scrnparams.statuz, varcom.calc))
		&& (isNE(scrnparams.statuz, varcom.oK))) {
			scrnparams.errorCode.set(errorsInner.curs);
			wsspcomn.edterror.set("Y");
			return false;
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		
		
		if (isEQ(wsspcomn.flag, "I")) {
			/* Even in enquiry mode, we must validate the bank account*/
			/* flag field.*/
			if ((isNE(sv.ddind, "+")
			&& isNE(sv.ddind, "X")
			&& isNE(sv.ddind, SPACES))) {
				sv.ddindErr.set(errorsInner.h118);
			}
			/* If user selects bank details and there are none, put out error*/
			if ((isEQ(sv.ddind, "X")
			&& isEQ(regpIO.getBankkey(), SPACES))) {
				//sv.ddindErr.set(errorsInner.e493);
			}
			checkForErrors2080();
			return false;
		}
		/*    Read the Claim Type details.*/
		descIO.setDescitem(t6690rec.rgpytype);
		if (isEQ(wsspcomn.flag, "M")) {
			descIO.setDescitem(wsaaRegptype);
		}
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill("?");
		}
		return true;
	}

	/**
	* <pre>
	* Validate Payment Method.
	* </pre>
	*/
protected void checkPayment2030()
	{
		if (isEQ(sv.rgpymop, SPACES)) {
			sv.rgpymopErr.set(errorsInner.e186);
			return;
		}
		descIO.setDescitem(sv.rgpymop);
		descIO.setDesctabl(tablesInner.t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		else {
			sv.rgpyshort.fill("?");
		}
	}

	/**
	* <pre>
	* Validate Claim Currency.
	* </pre>
	*/
protected void checkCurrency2035()
	{
		if (isEQ(sv.claimcur, SPACES)) {
			sv.claimcur.set(chdrrgpIO.getCntcurr());
		}
		descIO.setDescitem(sv.claimcur);
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill("?");
		}
	}

	/**
	* <pre>
	* Validate Frequency.
	* </pre>
	*/
protected void checkFreqency2040()
	{
		if (isEQ(sv.regpayfreq, SPACES)) {
			sv.regpayfreqErr.set(errorsInner.e186);
		}
		if (isEQ(sv.regpayfreq, ZERO)) {
			sv.revdteOut[varcom.pr.toInt()].set("Y");
			sv.epaydateOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.revdteOut[varcom.pr.toInt()].set("N");
			sv.epaydateOut[varcom.pr.toInt()].set("N");
		}
		/*  Read the Frequency Code description.*/
		descIO.setDescitem(sv.regpayfreq);
		descIO.setDesctabl(tablesInner.t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
			readT55425400();
		}
		else {
			sv.frqdesc.fill("?");
		}
		/* Validate the Payee Client.*/
		if (isEQ(sv.rgpymop, SPACES)) {
			sv.rgpymopErr.set(errorsInner.e186);
			return;
		}
		t6694rec.t6694Rec.set(SPACES);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
		|| (isNE(sv.rgpymop, itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
			return;
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		if (isEQ(t6694rec.payeereq, "Y")) {
			if (isEQ(sv.payclt, SPACES)) {
				sv.payclt.set(sv.cownnum);
			}
		}
		else {
			if (isNE(sv.payclt, SPACES)) {
				sv.paycltErr.set(errorsInner.e492);
			}
		}
		if (isEQ(sv.payclt, SPACES)) {
			sv.payenme.set(SPACES);
		}
		if (isEQ(t6694rec.payeereq, "Y")) {
			if (isEQ(sv.paycltErr, SPACES)) {
				cltsIO.setClntnum(sv.payclt);
				getClientDetails1200();
				if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
				|| (isNE(cltsIO.getValidflag(), "1"))) {
					sv.paycltErr.set(errorsInner.e335);
					sv.payenme.set(SPACES);
				}
				else {
					plainname();
					sv.payenme.set(wsspcomn.longconfname);
				}
			}
		}
	}

	/**
	* <pre>
	* Validate Percentage.
	* </pre>
	*/
protected boolean checkPercentage2050()
	{
		if ((isEQ(sv.prcnt, ZERO))
		&& (isEQ(sv.pymt, ZERO))) {
			sv.prcntErr.set(errorsInner.g194);
			sv.pymtErr.set(errorsInner.g194);
			return false;
		}
		if ((isNE(sv.prcnt, ZERO))
		&& (isNE(sv.pymt, ZERO))) {
			sv.prcntErr.set(errorsInner.g571);
			sv.pymtErr.set(errorsInner.g571);
			return false;
		}
		if (isGT(sv.prcnt, 100)) {
			sv.prcntErr.set(errorsInner.g515);
		}
		if (isNE(sv.pymt, ZERO)) {
			zrdecplrec.amountIn.set(sv.pymt);
			callRounding6000();
			if (isNE(zrdecplrec.amountOut, sv.pymt)) {
				sv.pymtErr.set(errorsInner.rfik);
			}
		}
		/*  Convert Payment Amount to payment currency, if required.*/
		if (isNE(sv.claimcur, wsaaPaymentCurr)) {
			conlinkrec.currOut.set(sv.claimcur);
			wsaaPaymentCurr.set(sv.claimcur);
			conlinkrec.currIn.set(chdrrgpIO.getCntcurr());
			conlinkrec.amountIn.set(sv.pymt);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.cashdate.set(wsaaToday);
			conlinkrec.company.set(wsspcomn.company);
			conlinkrec.function.set("CVRT");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			callRounding6000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaPymt2.set(conlinkrec.amountOut);
			/*  Recalculate the estimated fund value in payment currency*/
			conlinkrec.currOut.set(sv.claimcur);
			conlinkrec.currIn.set(chdrrgpIO.getCntcurr());
			conlinkrec.amountIn.set(wsaaEstimateTot);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.cashdate.set(wsaaToday);
			conlinkrec.company.set(wsspcomn.company);
			conlinkrec.function.set("CVRT");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			callRounding6000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaEstimateTot.set(conlinkrec.amountOut);
		}
		else {
			wsaaPymt2.set(sv.pymt);
		}
		/* Calculate fee*/
		if (isNE(sv.regpayfreqErr, SPACES)) {
			return false;
		}
		if (isNE(sv.prcnt, ZERO)) {
			compute(wsaaPymt2, 2).set((div(mult(sv.totestamt, sv.prcnt), 100)));
		}
		zrdecplrec.amountIn.set(wsaaPymt2);
		callRounding6000();
		wsaaPymt2.set(zrdecplrec.amountOut);
		if ((isNE(wsaaFfamt, ZERO))) {
			if ((isLT(wsaaFfamt, wsaaPymt2))
			&& (isNE(wsaaPymt2, wsaaAuxAmount))) {
				wsaaAuxAmount.set(wsaaPymt2);
				scrnparams.errorCode.set(errorsInner.f054);
				wsspcomn.edterror.set("Y");
				if (isNE(sv.prcnt, ZERO)) {
					sv.prcntErr.set(errorsInner.g572);
					/**          ELSE                                                   */
					/**             MOVE G572          TO S5102-PYMT-ERR                */
				}
			}
		}
		else {
			if ((isNE(wsaaFeepc, ZERO))) {
				compute(wsaaMinimumPerc, 2).set((div((mult(wsaaPymt2, wsaaFeepc)), 100)));
				if ((isLT(wsaaMinimumPerc, wsaaFeemin))
				&& (isNE(wsaaMinimumPerc, wsaaAuxPerc))) {
					wsaaAuxPerc.set(wsaaMinimumPerc);
					scrnparams.errorCode.set(errorsInner.f054);
					wsspcomn.edterror.set("Y");
					/**             IF S5102-PRCNT         = ZERO                       */
					/**                MOVE G572             TO S5102-PYMT-ERR          */
					/**             ELSE                                                */
					/**                MOVE G572             TO S5102-PRCNT-ERR         */
					/**             END-IF                                              */
				}
			}
		}
		/*  Check if amount within estimated fund value and request approva*/
		if (isEQ(sv.prcnt, ZERO)) {
			if ((isLT(wsaaPymt2, wsaaWdlAmount))
			&& (isNE(wsaaPymt2, wsaaPymt))) {
				wsaaPymt.set(wsaaPymt2);
				sv.pymtErr.set(errorsInner.g220);
				return false;
			}
			if ((isLT(wsaaEstimateTot, wsaaPymt2))
			&& (isNE(wsaaPymt2, wsaaPymt))) {
				wsaaPymt.set(wsaaPymt2);
				sv.pymtErr.set(errorsInner.f054);
				return false;
			}
		}
		else {
			compute(wsaaPrcntVal, 2).set((div(mult(wsaaEstimateTot, sv.prcnt), 100)));
			if ((isLT(wsaaPrcntVal, wsaaWdlAmount))
			&& (isNE(wsaaPrcntVal, wsaaPymt))) {
				wsaaPymt.set(wsaaPrcntVal);
				sv.prcntErr.set(errorsInner.g220);
				return false;
			}
		}
		/*  Check if sufficient funds remain in the fund*/
		if (isEQ(sv.prcnt, ZERO)) {
			compute(wsaaRevisedFundTotal, 2).set(sub(wsaaEstimateTot, wsaaPymt2));
		}
		else {
			compute(wsaaRevisedFundTotal, 2).set(sub(wsaaEstimateTot, wsaaPrcntVal));
		}
		if ((isGT(wsaaWdrem, wsaaRevisedFundTotal))
		&& (isNE(wsaaAmountRemaining, wsaaWdrem))) {
			sv.pymtErr.set(errorsInner.e855);
			wsaaAmountRemaining.set(wsaaWdrem);
		}
		return true;
	}

protected void validateFollow2065()
	{
		/* Validate Follow-Up Indicator.                                   */
		if ((isNE(sv.fupflg, "+"))
		&& (isNE(sv.fupflg, "X"))
		&& (isNE(sv.fupflg, SPACES))) {
			sv.fupflgErr.set(errorsInner.h118);
		}
	}

protected boolean checkDestination2060()
	{
		/* Validate Destination.*/
		if (isEQ(t6694rec.contreq, "Y")) {
			if (isEQ(sv.destkey, SPACES)) {
				sv.destkeyErr.set(errorsInner.e186);
			}
		}
		else {
			if (isNE(sv.destkey, SPACES)) {
				sv.destkey.set(SPACES);
			}
		}
		if ((isEQ(t6694rec.contreq, "Y"))
		&& (isEQ(sv.destkeyErr, SPACES))) {
			checkContractNo2100();
		}
		/* Validate Registration Date.*/
		if (isEQ(sv.crtdate, varcom.vrcmMaxDate)) {
			sv.crtdateErr.set(errorsInner.e186);
			checkForErrors2080();
			return false;
		}
		/* Registration Date must not be in the future.*/
		if (isGT(sv.crtdate, datcon1rec.intDate)) {
			sv.crtdateErr.set(errorsInner.f073);
			checkForErrors2080();
			return false;
		}
		/* Registration Date must not be earlier than Risk Commencement.*/
		if (isLT(sv.crtdate, sv.occdate)) {
			sv.crtdateErr.set(errorsInner.f616);
			checkForErrors2080();
			return false;
		}
		/* Validate First Payment Date.*/
		if (isEQ(sv.firstPaydate, varcom.vrcmMaxDate)) {
			sv.fpaydateErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			return true;
		}
		if (isLT(sv.firstPaydate, sv.crtdate)) {
			sv.fpaydateErr.set(errorsInner.h132);
			wsspcomn.edterror.set("Y");
			return true;
		}
		/* Check if the first payment date falls within the minimum period*/
		/*  determined by T5542*/
		if ((isNE(sv.regpayfreq, SPACES))
		&& (isNE(wsaaWdlFreq, ZERO))) {
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(wsaaWdlFreq);
			datcon2rec.intDate1.set(sv.occdate);
			datcon2rec.intDate2.set(ZERO);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.statuz);
				fatalError600();
			}
			if (isLT(sv.firstPaydate, datcon2rec.intDate2)) {
				sv.fpaydateErr.set(errorsInner.g221);
				wsspcomn.edterror.set("Y");
			}
		}
		return true;
	}

protected void checkReviewDate2075()
	{
		/* Validate Review Date.*/
		if ((isEQ(sv.revdte, varcom.vrcmMaxDate))
		|| (isEQ(sv.revdte, ZERO))) {
			sv.revdte.set(varcom.vrcmMaxDate);
		}
		/*NO-REVIEW-TERM*/
		if ((isNE(sv.revdte, varcom.vrcmMaxDate))) {
			if ((isLT(sv.revdte, sv.firstPaydate))) {
				sv.revdteErr.set(errorsInner.g530);
			}
			if (isLT(sv.revdte, sv.crtdate)) {
				sv.revdteErr.set(errorsInner.h132);
			}
		}
	}

protected void checkPayDate2077()
	{
		/* Validate Next Payment Date.*/
		if (isNE(sv.lastPaydate, varcom.vrcmMaxDate)) {
			if ((isEQ(sv.nextPaydate, varcom.vrcmMaxDate))
			|| (isEQ(sv.nextPaydate, ZERO))) {
				sv.npaydateErr.set(errorsInner.g540);
			}
			if (isLTE(sv.nextPaydate, sv.lastPaydate)) {
				sv.npaydateErr.set(errorsInner.g537);
			}
			if (isGT(sv.nextPaydate, sv.revdte)) {
				sv.npaydateErr.set(errorsInner.g538);
			}
			if (isGT(sv.nextPaydate, sv.finalPaydate)) {
				sv.npaydateErr.set(errorsInner.g539);
			}
		}
		else {
			sv.nextPaydate.set(sv.firstPaydate);
		}
		/* Validate Final Payment Date.*/
		if (isEQ(sv.regpayfreq, "00")) {
			sv.finalPaydate.set(sv.firstPaydate);
			wsaaFreqFlag = "Y";
		}
		else {
			/*       IF REGP-FINAL-PAYDATE     = 0                     <LA1200>*/
			/*          MOVE VRCM-MAX-DATE    TO S5102-FINAL-PAYDATE   <LA1200>*/
			/*       ELSE                                              <LA1200>*/
			if (isNE(regpIO.getFinalPaydate(), 0)) {
				if (isEQ(wsaaFreqFlag, "Y")
				|| isEQ(sv.finalPaydate, varcom.vrcmMaxDate)) {
					sv.finalPaydate.set(regpIO.getFinalPaydate());
				}
			}
			wsaaFreqFlag = "N";
		}
		if (isNE(sv.finalPaydate, varcom.vrcmMaxDate)
		&& isLT(sv.finalPaydate, sv.nextPaydate)) {
			sv.epaydateErr.set(errorsInner.g519);
		}
		/* Validate Anniversary Date.*/
		/* Anniversary Date should not be less than Registration Date.*/
		if ((isNE(sv.anvdate, varcom.vrcmMaxDate))
		&& (isNE(sv.anvdate, ZERO))) {
			if (isLT(sv.anvdate, sv.crtdate)) {
				sv.anvdateErr.set(errorsInner.g534);
				return;
			}
		}
		/* Validate bank Details indicator.*/
		if ((isNE(sv.ddind, "+"))
		&& (isNE(sv.ddind, "X"))
		&& (isNE(sv.ddind, SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
		}
		if ((isNE(wsaaPayclt, SPACES))
		&& (isNE(sv.payclt, SPACES))) {
			if (isNE(wsaaPayclt, sv.payclt)) {
				if (isEQ(t6694rec.bankreq, "Y")) {
					regpIO.setBankkey(SPACES);
					regpIO.setBankacckey(SPACES);
					sv.ddind.set("X");
				}
				else {
					if ((isEQ(t6694rec.bankreq, "N"))
					|| (isEQ(t6694rec.bankreq, " "))) {
						regpIO.setBankkey(SPACES);
						regpIO.setBankacckey(SPACES);
						sv.ddind.set(SPACES);
					}
				}
			}
		}
		if ((isEQ(t6694rec.bankreq, "Y"))
		&& (isEQ(sv.ddind, SPACES))) {
			sv.ddind.set("X");
		}
		if (isNE(t6694rec.bankreq, "Y")) {
			sv.ddind.set(SPACES);
			regpIO.setBankacckey(SPACES);
			regpIO.setBankkey(SPACES);
		}
		if (isEQ(sv.regpayfreq, ZERO)
		&& isNE(sv.firstPaydate, varcom.vrcmMaxDate)
		&& isNE(wsspcomn.edterror, "Y")) {
			if (firstTimeIn.isTrue()) {
				sv.finalPaydate.set(sv.firstPaydate);
				sv.revdte.set(varcom.vrcmMaxDate);
				sv.revdteOut[varcom.pr.toInt()].set("Y");
				sv.epaydateOut[varcom.pr.toInt()].set("Y");
				wsspcomn.edterror.set("Y");
				wsaaFirstTimeFlag.set("N");
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	* </pre>
	*/
protected void checkContractNo2100()
	{
		/*CHECK*/
		chdrenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		chdrenqIO.setChdrnum(sv.destkey);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if ((isNE(chdrenqIO.getStatuz(), varcom.oK))
		&& (isNE(chdrenqIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(), varcom.mrnf)) {
			sv.destkeyErr.set(errorsInner.g126);
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void protectScr2400()
	{
		/*PROTECT*/
		sv.paycltOut[varcom.pr.toInt()].set("Y");
		sv.rgpymopOut[varcom.pr.toInt()].set("Y");
		sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
		sv.destkeyOut[varcom.pr.toInt()].set("Y");
		sv.pymtOut[varcom.pr.toInt()].set("Y");
		sv.claimcurOut[varcom.pr.toInt()].set("Y");
		sv.crtdateOut[varcom.pr.toInt()].set("Y");
		sv.revdteOut[varcom.pr.toInt()].set("Y");
		sv.fpaydateOut[varcom.pr.toInt()].set("Y");
		sv.anvdateOut[varcom.pr.toInt()].set("Y");
		sv.canceldateOut[varcom.pr.toInt()].set("Y");
		sv.epaydateOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
        if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
            return ;
        }
        if ((isEQ(firstTime, "Y"))
        && (isNE(wsspcomn.flag, "I"))) {
            chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
            chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
            chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
            chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
            chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
            chdrlifIO.setFormat(formatsInner.chdrlifrec);
            /*       MOVE BEGNH               TO CHDRLIF-FUNCTION              */
            chdrlifIO.setFunction(varcom.begn);
            SmartFileCode.execute(appVars, chdrlifIO);
            if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
                syserrrec.params.set(chdrlifIO.getParams());
                fatalError600();
            }
            chdrlifIO.setCurrto(datcon1rec.intDate);
            chdrlifIO.setValidflag("2");
            /*       MOVE REWRT               TO CHDRLIF-FUNCTION              */
            chdrlifIO.setFunction(varcom.writd);
            chdrlifIO.setFormat(formatsInner.chdrlifrec);
            SmartFileCode.execute(appVars, chdrlifIO);
            if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
                syserrrec.params.set(chdrlifIO.getParams());
                fatalError600();
            }
            chdrlifIO.setCurrfrom(datcon1rec.intDate);
            chdrlifIO.setCurrto(varcom.vrcmMaxDate);
            chdrlifIO.setValidflag("1");
            setPrecision(chdrlifIO.getTranno(), 0);
            chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
            chdrlifIO.setFunction(varcom.writr);
            chdrlifIO.setFormat(formatsInner.chdrlifrec);
            SmartFileCode.execute(appVars, chdrlifIO);
            if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
                syserrrec.params.set(chdrlifIO.getParams());
                fatalError600();
            }
            /*  Call CHDRRGPIO to update POLSUM and POLINC fields*/
            callChdrrgpio3500();
        }
        if ((isEQ(firstTime, "Y"))
        && (isEQ(wsspcomn.flag, "I"))) {
            chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
            chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
            chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
            chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
            chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
            chdrlifIO.setFormat(formatsInner.chdrlifrec);
            chdrlifIO.setFunction(varcom.begn);
            SmartFileCode.execute(appVars, chdrlifIO);
            if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
                syserrrec.params.set(chdrlifIO.getParams());
                fatalError600();
            }
        }
        if (isEQ(firstTime, "Y")) {
            firstTime = "N";
            if (isEQ(wsspcomn.flag, "M")) {
                regpIO.setValidflag("2");
                regpIO.setFormat(formatsInner.regprec);
                regpIO.setFunction(varcom.updat);
                SmartFileCode.execute(appVars, regpIO);
                if (isNE(regpIO.getStatuz(), varcom.oK)) {
                    syserrrec.params.set(regpIO.getParams());
                    fatalError600();
                }
                regpIO.setValidflag("1");
            }
        }
        /*  Update database files as required / WSSP*/
        if (isEQ(sv.ddind, "X")) {
            moveToRegp3400();
            regpIO.setFunction(varcom.keeps);
            regpIO.setFormat(formatsInner.regprec);
            SmartFileCode.execute(appVars, regpIO);
            if (isNE(regpIO.getStatuz(), varcom.oK)) {
                syserrrec.params.set(regpIO.getParams());
                fatalError600();
            }
            return ;
        }
        if (isEQ(sv.fupflg, "X")) {
            moveToRegp3400();
            regpIO.setFunction(varcom.keeps);
            regpIO.setFormat(formatsInner.regprec);
            SmartFileCode.execute(appVars, regpIO);
            if (isNE(regpIO.getStatuz(), varcom.oK)) {
                syserrrec.params.set(regpIO.getParams());
                fatalError600();
            }
            return ;
        }
        /*    No update on Enquiry.*/
        if (isEQ(wsspcomn.flag, "I")) {
            return ;
        }
        readT66935200();
        if (isEQ(wsspcomn.flag, "C")) {
            createRegp3100();
        }
        if (isEQ(wsspcomn.flag, "M")) {
            updateRegp3200();
        }
        /*    Check if Breakout is necessary*/
        if ((isEQ(wsspcomn.flag, "C"))
        && (isNE(regpIO.getPlanSuffix(), ZERO))) {
            if ((isLTE(regpIO.getPlanSuffix(), chdrrgpIO.getPolsum()))
            && (isGT(chdrrgpIO.getPolsum(), 1))) {
                callAtrqForBrkout3800();
                return ;
            }
        }
        if (isNE(wsspcomn.flag, "I")) {
            releaseSoftlock3900();
        }
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void createRegp3100()
	{
        if (isEQ(t6694rec.contreq, "Y")) {
            regpIO.setDestkey(sv.destkey);
        }
        else {
            regpIO.setDestkey(SPACES);
        }
        regpIO.setGlact(t6694rec.glact);
        regpIO.setSacscode(t6694rec.sacscode);
        regpIO.setSacstype(t6694rec.sacstype);
        regpIO.setDebcred(t6694rec.debcred);
        regpIO.setRgpytype(t6690rec.rgpytype);
        regpIO.setTranno(chdrlifIO.getTranno());
        regpIO.setValidflag("1");
        regpIO.setRgpystat(wsaaRgpystat);
        regpIO.setRgpynum(sv.rgpynum);
        regpIO.setPayclt(sv.payclt);
        regpIO.setRgpymop(sv.rgpymop);
        regpIO.setRegpayfreq(sv.regpayfreq);
        regpIO.setPrcnt(sv.prcnt);
        regpIO.setPymt(sv.pymt);
        regpIO.setCurrcd(sv.claimcur);
        regpIO.setAprvdate(sv.aprvdate);
        regpIO.setCrtdate(sv.crtdate);
        regpIO.setRevdte(sv.revdte);
        regpIO.setFirstPaydate(sv.firstPaydate);
        regpIO.setAnvdate(sv.anvdate);
        /* MOVE S5102-FINAL-PAYDATE    TO REGP-FINAL-PAYDATE.           */
        if (isEQ(regpIO.getRegpayfreq(), ZERO)) {
            regpIO.setFinalPaydate(sv.firstPaydate);
            regpIO.setRevdte(varcom.vrcmMaxDate);
        }
        else {
            regpIO.setFinalPaydate(sv.finalPaydate);
            regpIO.setRevdte(sv.revdte);
        }
        regpIO.setCancelDate(varcom.vrcmMaxDate);
        regpIO.setAprvdate(varcom.vrcmMaxDate);
        regpIO.setNextPaydate(sv.nextPaydate);
        regpIO.setLastPaydate(varcom.vrcmMaxDate);
        regpIO.setCertdate(varcom.vrcmMaxDate);
        regpIO.setRecvdDate(varcom.vrcmMaxDate);
        regpIO.setIncurdt(varcom.vrcmMaxDate);
        regpIO.setCrtable(sv.crtable);
        regpIO.setPaycoy(wsspcomn.fsuco);
        regpIO.setFunction(varcom.writr);
        regpIO.setFormat(formatsInner.regprec);
        SmartFileCode.execute(appVars, regpIO);
        if (isNE(regpIO.getStatuz(), varcom.oK)) {
            syserrrec.params.set(regpIO.getParams());
            fatalError600();
        }
        updatPtrn3300();

	}

protected void updateRegp3200()
	{
        if (isEQ(t6694rec.contreq, "Y")) {
            regpIO.setDestkey(sv.destkey);
        }
        else {
            regpIO.setDestkey(SPACES);
        }
        regpIO.setGlact(t6694rec.glact);
        regpIO.setSacscode(t6694rec.sacscode);
        regpIO.setSacstype(t6694rec.sacstype);
        regpIO.setDebcred(t6694rec.debcred);
        regpIO.setRgpytype(wsaaRegptype);
        regpIO.setTranno(chdrlifIO.getTranno());
        if (isNE(wsaaRgpystat, SPACES)) {
            regpIO.setRgpystat(wsaaRgpystat);
        }
        regpIO.setRgpynum(sv.rgpynum);
        regpIO.setPayclt(sv.payclt);
        regpIO.setRgpymop(sv.rgpymop);
        regpIO.setRegpayfreq(sv.regpayfreq);
        regpIO.setPrcnt(sv.prcnt);
        regpIO.setPymt(sv.pymt);
        regpIO.setCurrcd(sv.claimcur);
        regpIO.setCrtable(sv.crtable);
        regpIO.setAprvdate(sv.aprvdate);
        regpIO.setCrtdate(sv.crtdate);
        regpIO.setRevdte(sv.revdte);
        regpIO.setFirstPaydate(sv.firstPaydate);
        regpIO.setNextPaydate(sv.nextPaydate);
        regpIO.setAnvdate(sv.anvdate);
        regpIO.setFinalPaydate(sv.finalPaydate);
        regpIO.setCancelDate(varcom.vrcmMaxDate);
        regpIO.setAprvdate(varcom.vrcmMaxDate);
        regpIO.setRecvdDate(varcom.vrcmMaxDate);
        regpIO.setIncurdt(varcom.vrcmMaxDate);
        regpIO.setFunction(varcom.writr);
        regpIO.setFormat(formatsInner.regprec);
        SmartFileCode.execute(appVars, regpIO);
        if (isNE(regpIO.getStatuz(), varcom.oK)) {
            syserrrec.params.set(regpIO.getParams());
            fatalError600();
        }
        updatPtrn3300();
	}

protected void updatPtrn3300()
	{
        /* Write a PTRN record.*/
        ptrnIO.setParams(SPACES);
        ptrnIO.setDataKey(SPACES);
        ptrnIO.setDataKey(wsspcomn.batchkey);
        ptrnIO.setChdrpfx("CH");
        ptrnIO.setChdrcoy(chdrrgpIO.getChdrcoy());
        ptrnIO.setChdrnum(chdrrgpIO.getChdrnum());
        ptrnIO.setRecode(chdrrgpIO.getRecode());
        ptrnIO.setTranno(chdrlifIO.getTranno());
        ptrnIO.setPtrneff(datcon1rec.intDate);
        ptrnIO.setDatesub(datcon1rec.intDate);
        ptrnIO.setTransactionDate(varcom.vrcmDate);
        ptrnIO.setTransactionTime(varcom.vrcmTime);
        ptrnIO.setTermid(varcom.vrcmTerm);
        ptrnIO.setUser(varcom.vrcmUser);
        ptrnIO.setCrtuser(wsspcomn.userid); //PINNACLE-2931
        ptrnIO.setBatccoy(wsspcomn.company);
        ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
        ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
        ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
        ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
        ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
        ptrnIO.setPrtflg(SPACES);
        ptrnIO.setValidflag("1");
        ptrnIO.setFormat(formatsInner.ptrnrec);
        ptrnIO.setFunction(varcom.writr);
        SmartFileCode.execute(appVars, ptrnIO);
        if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
            syserrrec.params.set(ptrnIO.getParams());
            fatalError600();
        }
	}

protected void moveToRegp3400()
	{
        regpIO.setDestkey(sv.destkey);
        regpIO.setRgpynum(sv.rgpynum);
        regpIO.setPayclt(sv.payclt);
        regpIO.setRgpymop(sv.rgpymop);
        regpIO.setRegpayfreq(sv.regpayfreq);
        regpIO.setPrcnt(sv.prcnt);
        regpIO.setPymt(sv.pymt);
        regpIO.setCurrcd(sv.claimcur);
        regpIO.setAprvdate(sv.aprvdate);
        regpIO.setCrtdate(sv.crtdate);
        regpIO.setRevdte(sv.revdte);
        regpIO.setFirstPaydate(sv.firstPaydate);
        regpIO.setAnvdate(sv.anvdate);
        regpIO.setFinalPaydate(sv.finalPaydate);
        regpIO.setCancelDate(sv.cancelDate);
        regpIO.setAprvdate(sv.aprvdate);
        regpIO.setNextPaydate(sv.nextPaydate);
        regpIO.setLastPaydate(sv.lastPaydate);
        regpIO.setCrtable(sv.crtable);
        regpIO.setTranno(chdrlifIO.getTranno());
	}

protected void callChdrrgpio3500()
	{
        /* Rewrite CHDRRGP record with POLSUM and POLINC updated.*/
        chdrrgpIO.setFormat(formatsInner.chdrrgprec);
        chdrrgpIO.setFunction(varcom.readh);
        SmartFileCode.execute(appVars, chdrrgpIO);
        if ((isNE(chdrrgpIO.getStatuz(), varcom.oK))
        && (isNE(chdrrgpIO.getStatuz(), varcom.mrnf))) {
            syserrrec.params.set(chdrrgpIO.getParams());
            fatalError600();
        }
        chdrrgpIO.setPolsum(wsaaPolsum);
        chdrrgpIO.setPolinc(wsaaPolinc);
        chdrrgpIO.setFormat(formatsInner.chdrrgprec);
        chdrrgpIO.setFunction(varcom.rewrt);
        SmartFileCode.execute(appVars, chdrrgpIO);
        if ((isNE(chdrrgpIO.getStatuz(), varcom.oK))
        && (isNE(chdrrgpIO.getStatuz(), varcom.mrnf))) {
            syserrrec.params.set(chdrrgpIO.getParams());
            fatalError600();
        }
	}

protected void callAtrqForBrkout3800()
	{
        sftlockrec.function.set("TOAT");
        sftlockrec.statuz.set(varcom.oK);
        sftlockrec.company.set(wsspcomn.company);
        sftlockrec.enttyp.set("CH");
        sftlockrec.entity.set(chdrrgpIO.getChdrnum());
        sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
        sftlockrec.user.set(varcom.vrcmUser);
        callProgram(Sftlock.class, sftlockrec.sftlockRec);
        if ((isNE(sftlockrec.statuz, varcom.oK))
        && (isNE(sftlockrec.statuz, "LOCK"))) {
            syserrrec.statuz.set(sftlockrec.statuz);
            fatalError600();
        }
        if (isEQ(sftlockrec.statuz, "LOCK")) {
            syserrrec.statuz.set(sftlockrec.statuz);
            fatalError600();
        }
        /*  call the at module ATREQ*/
        atreqrec.atreqRec.set(SPACES);
        atreqrec.acctYear.set(ZERO);
        atreqrec.acctMonth.set(ZERO);
        atreqrec.module.set("P5102AT");
        atreqrec.batchKey.set(wsspcomn.batchkey);
        atreqrec.reqProg.set(wsaaProg);
        atreqrec.reqUser.set(varcom.vrcmUser);
        atreqrec.reqTerm.set(varcom.vrcmTerm);
        atreqrec.reqDate.set(wsaaToday);
        atreqrec.reqTime.set(varcom.vrcmTime);
        atreqrec.language.set(wsspcomn.language);
        wsaaPrimaryChdrnum.set(chdrrgpIO.getChdrnum());
        atreqrec.primaryKey.set(wsaaPrimaryKey);
        wsaaTransactionDate.set(varcom.vrcmDate);
        wsaaTransactionTime.set(varcom.vrcmTime);
        wsaaUser.set(varcom.vrcmUser);
        wsaaTermid.set(varcom.vrcmTerm);
        wsaaBrkoutPlanSuffix.set(regpIO.getPlanSuffix());
        atreqrec.transArea.set(wsaaTransactionRec);
        atreqrec.statuz.set(varcom.oK);
        callProgram(Atreq.class, atreqrec.atreqRec);
        if (isNE(atreqrec.statuz, varcom.oK)) {
            syserrrec.params.set(atreqrec.atreqRec);
            fatalError600();
        }
	}

protected void releaseSoftlock3900()
	{
        sftlockrec.sftlockRec.set(SPACES);
        sftlockrec.company.set(wsspcomn.company);
        sftlockrec.entity.set(chdrrgpIO.getChdrnum());
        sftlockrec.enttyp.set("CH");
        sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
        sftlockrec.statuz.set(SPACES);
        sftlockrec.function.set("UNLK");
        callProgram(Sftlock.class, sftlockrec.sftlockRec);
        if (isNE(sftlockrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(sftlockrec.statuz);
            fatalError600();
        }
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
        if(nextProgram4010()){
            popUp4050();
            if(gensww4010()){
                wsspcomn.nextprog.set(wsaaProg);
                wsspcomn.programPtr.add(1);
            }
        }
	}

protected boolean nextProgram4010()
	{
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
		        wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		        sub1.add(1);
		        sub2.add(1);
			}
		}
		if (isEQ(sv.fupflg, "?")) {
			checkFollowUp5000();
		}
		if (isEQ(sv.ddind, "?")) {
			checkBankDetails4400();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return false;
		}
		return true;
	}

	/**
	* <pre>
	*    If any of the indicators have been selected, (value - 'X'),
	*    then set an asterisk in the program stack action field to
	*    ensure that control returns here, set the parameters for
	*    generalised secondary switching and save the original
	*    programs from the program stack.
	* </pre>
	*/
protected void popUp4050()
	{
		/*    IF S5102-DDIND               = 'X'                           */
		if ((isEQ(sv.ddind, "X"))
		|| (isEQ(sv.fupflg, "X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		        wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		        sub1.add(1);
		        sub2.add(1);
			}
		}
		gensswrec.function.set(SPACES);
		/*   If FOLLOW-UP has been selected set 'B' in the function.       */
		if (isEQ(sv.fupflg, "X")) {
			sv.fupflg.set("?");
			gensswrec.function.set("B");
			return;
		}
		/*   If BANK DETAILS has been selected set 'A' in the function.*/
		if (isEQ(sv.ddind, "X")) {
			sv.ddind.set("?");
			gensswrec.function.set("A");
			return;
		}
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call
	*   the generalised secondary switching module to obtain the
	*   next 8 programs and load them into the program stack.
	* </pre>
	*/
protected boolean gensww4010()
	{
		if (isEQ(gensswrec.function, SPACES)) {
		    return true;
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz, varcom.oK))
		&& (isNE(gensswrec.statuz, varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the*/
		/* screen with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return false;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		     wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		     sub1.add(1);
		     sub2.add(1);
		}
		return true;
	}

protected void checkBankDetails4400()
	{
        regpIO.setFunction(varcom.retrv);
        SmartFileCode.execute(appVars, regpIO);
        if (isNE(regpIO.getStatuz(), varcom.oK)) {
            syserrrec.params.set(regpIO.getParams());
            fatalError600();
        }
        /*    Release the Regular Payment Record.*/
        regpIO.setFunction(varcom.rlse);
        SmartFileCode.execute(appVars, regpIO);
        if (isNE(regpIO.getStatuz(), varcom.oK)) {
            syserrrec.params.set(regpIO.getParams());
            fatalError600();
        }
        /* Check if there are any bank details on the current contract.*/
        if ((isEQ(regpIO.getBankkey(), SPACES))
        && (isEQ(regpIO.getBankacckey(), SPACES))) {
            sv.ddind.set(SPACES);
        }
        else {
            sv.ddind.set("+");
            wsaaPayclt.set(sv.payclt);
        }
	}

protected void checkFollowUp5000()
	{
        /* Check if there are any follow-ups details on the current        */
        /* Contract.                                                       */
        fluprgpIO.setChdrcoy(wsspcomn.company);
        fluprgpIO.setChdrnum(chdrrgpIO.getChdrnum());
        wsaaRgpynum.set(regpIO.getRgpynum());
        wsaaClamnumFill.set(ZERO);
        fluprgpIO.setClamnum(wsaaClamnum2);
        fluprgpIO.setFupno(ZERO);
        /* Use WSAA-CLAMNUM2 to compare to FLUPRGP-CLAMNUM                 */
        /* instead of comparing REGP-RGPYNUM and FLUPRGP-CLAMNUM.          */
        /* Due to the fact that RGPYNUM is 5 bytes long                    */
        /* and CLAMNUM is 8 bytes long.                                    */
        fluprgpIO.setFunction(varcom.begn);
    
        //performance improvement --  Niharika Modi
        fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
        fluprgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","CLAMNUM");
    
        SmartFileCode.execute(appVars, fluprgpIO);
        if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
        && (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
            syserrrec.params.set(fluprgpIO.getParams());
            fatalError600();
        }
        if ((isEQ(wsspcomn.company, fluprgpIO.getChdrcoy()))
        && (isEQ(chdrrgpIO.getChdrnum(), fluprgpIO.getChdrnum()))
        && (isEQ(fluprgpIO.getClamnum(), wsaaClamnum2))
        && (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
            sv.fupflg.set("+");
        }
        else {
            sv.fupflg.set(SPACES);
        }
	}

protected void readT66935200()
 {
        if ((isEQ(wsspcomn.flag, "M")) || (isEQ(wsspcomn.flag, "I"))) {
            wsaaT6693Rgpystat.set(regpIO.getRgpystat());
        } else {
            wsaaT6693Rgpystat.set("**");
        }
        wsaaT6693Crtable.set(regpIO.getCrtable());
        if (t6693Map == null || t6693Map.isEmpty()) {
            t6693Map = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T6693");
        }
        Itempf itemT6693 = null;
        if (t6693Map != null) {
            if (t6693Map.containsKey(wsaaT6693Key.toString())) {
                for (Itempf item : t6693Map.get(wsaaT6693Key.toString())) {
                    if (item.getItmfrm().compareTo(chdrrgpIO.getOccdate().getbigdata()) <= 0) {
                        itemT6693 = item;
                        break;
                    }
                }
            } else {
                wsaaT6693Crtable.set("****");
                if (t6693Map.containsKey(wsaaT6693Key.toString())) {
                    for (Itempf item : t6693Map.get(wsaaT6693Key.toString())) {
                        if (item.getItmfrm().compareTo(chdrrgpIO.getOccdate().getbigdata()) <= 0) {
                            itemT6693 = item;
                            break;
                        }
                    }
                }

            }
            if (itemT6693 != null) {
                t6693rec.t6693Rec.set(StringUtil.rawToString(itemT6693.getGenarea()));
                wsaaBatckey.set(wsspcomn.batchkey);
                wsaaRgpystat.set(SPACES);
                index1.set(1);
                while (!(isGT(index1, 12))) {
                    /* FIND-STATUS */
                    if (isEQ(t6693rec.trcode[index1.toInt()], wsaaBatckey.batcBatctrcde)) {
                        wsaaRgpystat.set(t6693rec.rgpystat[index1.toInt()]);
                        index1.set(99);
                    }
                    index1.add(1);
                    /* EXIT */
                }
            }
        }
    }

protected void readT55425400()
	{
        /*     Read the Table T5542 to obtain the withdrawal information*/
        itdmIO.setDataKey(SPACES);
        itdmIO.setItemcoy(wsspcomn.company);
        itdmIO.setItemtabl(tablesInner.t5542);
        wsaaT5542KeyWdmeth.set(t5540rec.wdmeth);
        wsaaT5542KeyCurrcd.set(sv.claimcur);
        itdmIO.setItemitem(wsaaT5542Key);
        itdmIO.setItmfrm(wsaaToday);
        itdmIO.setFunction(varcom.begn);
        //performance improvement --  Niharika Modi
        itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
        itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
    
        SmartFileCode.execute(appVars, itdmIO);
        if ((isNE(itdmIO.getStatuz(), varcom.oK))
        && (isNE(itdmIO.getStatuz(), varcom.endp))) {
            syserrrec.params.set(itdmIO.getParams());
            fatalError600();
        }
        if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
        || (isNE(itdmIO.getItemtabl(), tablesInner.t5542))
        || (isNE(itdmIO.getItemitem(), wsaaT5542Key))
        || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
            syserrrec.params.set(itdmIO.getParams());
            syserrrec.statuz.set(varcom.endp);
            fatalError600();
        }
        else {
            t5542rec.t5542Rec.set(itdmIO.getGenarea());
        }
        /* MOVE ZERO                   TO SUB1.                         */
        /* IF S5102-REGPAYFREQ          = '00'                          */
        /*     MOVE 1                  TO SUB1                          */
        /* END-IF.                                                      */
        /* IF S5102-REGPAYFREQ          = '01'                          */
        /*     MOVE 2                  TO SUB1                          */
        /* END-IF.                                                      */
        /* IF S5102-REGPAYFREQ          = '02'                          */
        /*    MOVE 3                   TO SUB1                          */
        /* END-IF.                                                      */
        /* IF S5102-REGPAYFREQ          = '04'                          */
        /*     MOVE 4                  TO SUB1                          */
        /* END-IF.                                                      */
        /* IF S5102-REGPAYFREQ          = '12'                          */
        /*     MOVE 5                  TO SUB1                          */
        /* END-IF.                                                      */
        /* IF SUB1                      = ZERO                          */
        /*    MOVE H141                TO S5102-REGPAYFREQ-ERR          */
        /*    GO TO 5490-EXIT                                           */
        /* END-IF.                                                      */
        /* IF SUB1                  NOT = WSAA-STORE-SUB                */
        /*    MOVE ZEROES              TO WSAA-PYMT                     */
        /*                                WSAA-AUX-PERC                 */
        /*                                WSAA-AUX-AMOUNT               */
        /*                                WSAA-AMOUNT-REMAINING         */
        /* END-IF.*/
        /* MOVE SUB1                   TO WSAA-STORE-SUB.               */
        /* MOVE T5542-FEEMAX (SUB1)    TO WSAA-FEEMAX.                  */
        /* MOVE T5542-FEEMIN (SUB1)    TO WSAA-FEEMIN.                  */
        /* MOVE T5542-FEEPC (SUB1)     TO WSAA-FEEPC.                   */
        /* MOVE T5542-FFAMT (SUB1)     TO WSAA-FFAMT.                   */
        /* MOVE T5542-WDL-AMOUNT(SUB1) TO WSAA-WDL-AMOUNT.              */
        /* MOVE T5542-WDREM(SUB1)      TO WSAA-WDREM.                   */
        /* MOVE T5542-WDL-FREQ (SUB1)  TO WSAA-WDL-FREQ.                */
        /* Check frequency with those on T5542.                            */
        wsaaSub.set(ZERO);
        for (wsaaSub.set(1); !(isGT(wsaaSub, wsaaT5542TableSize)); wsaaSub.add(1)){
            if (isEQ(sv.regpayfreq, t5542rec.billfreq[wsaaSub.toInt()])) {
                wsaaFeemin.set(t5542rec.feemin[wsaaSub.toInt()]);
                wsaaFeepc.set(t5542rec.feepc[wsaaSub.toInt()]);
                wsaaFfamt.set(t5542rec.ffamt[wsaaSub.toInt()]);
                wsaaWdlAmount.set(t5542rec.wdlAmount[wsaaSub.toInt()]);
                wsaaWdrem.set(t5542rec.wdrem[wsaaSub.toInt()]);
                wsaaWdlFreq.set(t5542rec.wdlFreq[wsaaSub.toInt()]);
                wsaaSub.set(10);
                return ;
            }
        }
        /*    IF WSAA-SUB   > 9                                            */
        if (isGT(wsaaSub, wsaaT5542TableSize)) {
            sv.regpayfreqErr.set(errorsInner.h141);
            wsspcomn.edterror.set("Y");
            return ;
        }

	}

protected void callRounding6000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.claimcur);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
	private FixedLengthStringData e855 = new FixedLengthStringData(4).init("E855");
	private FixedLengthStringData f054 = new FixedLengthStringData(4).init("F054");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData g126 = new FixedLengthStringData(4).init("G126");
	private FixedLengthStringData g194 = new FixedLengthStringData(4).init("G194");
	private FixedLengthStringData g220 = new FixedLengthStringData(4).init("G220");
	private FixedLengthStringData g221 = new FixedLengthStringData(4).init("G221");
	private FixedLengthStringData g515 = new FixedLengthStringData(4).init("G515");
	private FixedLengthStringData g519 = new FixedLengthStringData(4).init("G519");
	private FixedLengthStringData g529 = new FixedLengthStringData(4).init("G529");
	private FixedLengthStringData g530 = new FixedLengthStringData(4).init("G530");
	private FixedLengthStringData g534 = new FixedLengthStringData(4).init("G534");
	private FixedLengthStringData g537 = new FixedLengthStringData(4).init("G537");
	private FixedLengthStringData g538 = new FixedLengthStringData(4).init("G538");
	private FixedLengthStringData g539 = new FixedLengthStringData(4).init("G539");
	private FixedLengthStringData g540 = new FixedLengthStringData(4).init("G540");
	private FixedLengthStringData g571 = new FixedLengthStringData(4).init("G571");
	private FixedLengthStringData g572 = new FixedLengthStringData(4).init("G572");
	private FixedLengthStringData h055 = new FixedLengthStringData(4).init("H055");
	private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
	private FixedLengthStringData h132 = new FixedLengthStringData(4).init("H132");
	private FixedLengthStringData h136 = new FixedLengthStringData(4).init("H136");
	private FixedLengthStringData h141 = new FixedLengthStringData(4).init("H141");
	private FixedLengthStringData h072 = new FixedLengthStringData(4).init("H072");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
	private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5542 = new FixedLengthStringData(5).init("T5542");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData t5400 = new FixedLengthStringData(5).init("T5400");
	private FixedLengthStringData t6690 = new FixedLengthStringData(5).init("T6690");
	private FixedLengthStringData t6691 = new FixedLengthStringData(5).init("T6691");
	private FixedLengthStringData t6693 = new FixedLengthStringData(5).init("T6693");
	private FixedLengthStringData t6694 = new FixedLengthStringData(5).init("T6694");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData chdrrgprec = new FixedLengthStringData(10).init("CHDRRGPREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
	private FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC   ");
}
}
