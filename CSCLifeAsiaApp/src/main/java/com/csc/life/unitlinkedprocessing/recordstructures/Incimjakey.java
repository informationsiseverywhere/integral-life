package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:57
 * Description:
 * Copybook name: INCIMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incimjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incimjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incimjaKey = new FixedLengthStringData(64).isAPartOf(incimjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData incimjaChdrcoy = new FixedLengthStringData(1).isAPartOf(incimjaKey, 0);
  	public FixedLengthStringData incimjaChdrnum = new FixedLengthStringData(8).isAPartOf(incimjaKey, 1);
  	public FixedLengthStringData incimjaLife = new FixedLengthStringData(2).isAPartOf(incimjaKey, 9);
  	public FixedLengthStringData incimjaCoverage = new FixedLengthStringData(2).isAPartOf(incimjaKey, 11);
  	public FixedLengthStringData incimjaRider = new FixedLengthStringData(2).isAPartOf(incimjaKey, 13);
  	public PackedDecimalData incimjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incimjaKey, 15);
  	public PackedDecimalData incimjaInciNum = new PackedDecimalData(3, 0).isAPartOf(incimjaKey, 18);
  	public PackedDecimalData incimjaSeqno = new PackedDecimalData(2, 0).isAPartOf(incimjaKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(incimjaKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incimjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incimjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}