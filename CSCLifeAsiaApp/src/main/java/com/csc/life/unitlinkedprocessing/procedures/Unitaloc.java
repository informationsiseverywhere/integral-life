/*
 * File: Unitaloc.java
 * Date: 30 August 2009 2:49:23
 * Author: Quipoz Limited
 * 
 * Class transformed from UNITALOC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.IncirnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
* This generic subroutine allocates units (premium) to a component
* following receipt of premium.
*
*    Firstly the module has to establish premium allocations which
* may be split between initial units, accumulation units and non-
* invested amounts. This process also applies to any unit enhance-
* ment which may be relevant.
*
*    Once the net  allocation  to  units  is  established, the  unit
* apportionment file (ULNKPF)  is  accessed  to find out which funds
* are to be  invested in. Finally the unit transaction file (UTRNPF)
* is updated with  a  space  in  the 'feedback indicator' which will
* trigger later processing by the unit dealing batch run.
*
*
* 1.) Establish premium allocation.
*
*
*    This processing is  centred around two 'loops' which go through
* the premium allocation file (INCIPF) and within each of the INCIPF
* records:
*
*    Read all INCIPF  records  for  this component using the logical
* view INCIRNL and allocate the INCI-CURR-PREM amount as follows:
*
*        Within each INCIPF record read  through  each occurrence of
*     INCI-PREM-CURR and carry out the  following  processing  until
*     the INCI-CURR-PREM been fully allocated:
*
*        If this occurrence of INCI-PREM-CURR is zero ignore it and
*          look up the next occurrence.
*        If the premium we are allocating is greater than this
*          occurrence of INCI-PREM-CURR perform Calculate Split
*          using INCI-PREM-CURR as the amount.
*        If INCI-PREM-CURR > premium to be allocated perform
*          Calculate Split using the premium to be allocated as the
*          amount.
*
* Calculate Split.
*
*        Compute the non-invested premium as:
*
*     Amount to be allocated * (100 - INCI-PCUNIT)
*     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*                          100
*
*        Compute the allocation to initial units as:
*
*     (Amount to be allocated - non invested prem) * INCI-UNIT-SPLIT
*     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*                          100
*
*        Compute the allocation to accumulation units as:
*
*     Amount to be allocated - (non invested prem + initial units)
*
*        Accumulate these three amounts in working storage as there
*          may be more to be added to them if the premium falls
*          within two or more bands of allocation.
*        Update the INCIPF record by subtracting the amount of
*          allocation (for this occurrence) from the relevant
*          INCI-PREM-CURR field.
*        If the total of the amounts allocated in working storage =
*          INCI-CURR-PREM (i.e. the premium has been allocated)
*          rewrite the INCIPF record back to the database
*        Else
*        Calculate the amount to be allocated for next time round
*          the loop as:
*             INCI-CURR-PREM - total of working storage amounts
*
*    After processing all  INCIPF  records  the total amount held in
* the 3 working  storage  areas  should  equal the amount of premium
* originally passed in linkage - if not this is a fatal error.
*
*    After  finding the amounts to be allocated we need to check for
* any  enhancements.  Firstly  we need to look up table T6647 with a
* key  of  transaction code/contract type - if the enhancement basis
* field is spaces go to the next section.
*
*    Look   up   T5545  with  a  key  of  transaction  code/enhanced
* allocation basis (from T6647). Find the occurrence of fields which
* relates to the billing frequency as passed in  the linkage area to
* this program.  If the instalment premium is  greater than or equal
* to any of the premium occurrences for this frequency.  If it is we
* can use the corresponding enhancement percentage:
*
*        Multiply allocation to initial units by enhancement
*          percentage (then divide by 100).
*        Multiply allocation to accumulation units by enhancement
*          percentage (then divide by 100).
*
*
* 2.) Establish Fund allocation split.
*
*    Calculate the initial unit discount factor. First look up
* T5540 for the discount basis, then use this basis to access
* T5519. We use the information in T5519 to decide whether to
* look up T5539 or T6646 for the actual discount figure. Both
* T5539 and T6646 are accessed by the discount factor in T5540.
*
*    Each  coverage has a  specific  apportionment  for  units  into
* different funds. This should not  be confused with unit allocation
* which   determines   how    premium    is    distributed   between
* non-investment, initial and accumulation units. This fund split is
* held in the ULNKPF file.
*
*    Read  the  ULNKPF  file using the logical view ULNKRNL with the
* component key.  Write  a  UTRN  for  each  of the fund allocations
* (initial or accumulation) in working storage which are not 0. They
* should be  apportioned  between  the  funds as held on ULNKPF. The
* UTRNPF record should be set up as follows:
*
*          - Component key from COVRPF
*          - Virtual Fund from ULNKPF
*          - Unit type dependent on working storage type being
*                                                  processed.
*          - Tran-no from linkage
*          - Term ID and user from run parameters
*          - Transaction date and time from system parameters
*          - Batch key as set up in this program
*          - Unit sub account 'INIT' or 'ACUM' dependent on type
*                                           being processed.
*          - Now/Deferred indicator from T6647 using transaction/
*            contract type
*            If this is an off movement (negative amount) look up
*            the deallocation indicator - if this is now (look at
*            copybook CBNOWDEF) move 'N' to this field else move 'D'.
*            If this is an on  movement (positive amount) look up
*            the allocation indicator - if this is now (look at
*            copybook CBNOWDEF) move 'N' to this field else move 'D'.
*          - Monies date as bill date from linkage
*          - CRTABLE from COVRPF
*          - Contract currency from linkage
*          - Contract amount = allocation amount
*          - Fund currency from T5515 (key = fund)
*          - SACS code and type and GL code from linkage
*          - Contract type from linkage
*          - Processing sequence number from T6647
*          - SVP = 1
*          - CR comm date - from COVRPF
*
*    Write  the  record  to  the database.
*
*
* 3.) Write the uninvested record.
*
*
*    After  dealing  with  the  allocation  a  single UTRNPF must be
* created  for the non investment allocation. Create a UTRNPF record
* as follows:
*
*          - Initialise all fields
*          - Component key from COVRPF
*          - Tran-no from linkage
*          - Term ID and user from run parameters
*          - Transaction date and time from system parameters
*          - Batch key as set up in this program
*          - Unit sub account 'NVST'
*          - Monies date as bill date from linkage
*          - CRTABLE from COVRPF
*          - Contract currency from linkage
*          - Contract amount = uninvested amount
*          - SACS code and type and GL code from linkage
*          - Contract type from linkage
*          - CR comm date from COVRPF
*
*    Write the record to the database.
*
****************************************************************
* </pre>
*/
public class Unitaloc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNITALOC";
	private PackedDecimalData wsaaInciCurrPrem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1).init("N");
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5545Item, 4);
	private ZonedDecimalData wsaaFact = new ZonedDecimalData(3, 0);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String g027 = "G027";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5545 = "T5545";
	private static final String t6647 = "T6647";
	private static final String t6646 = "T6646";
	private static final String t5540 = "T5540";
	private static final String t5519 = "T5519";
	private static final String t5539 = "T5539";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
		/* FORMATS */
	private static final String incirnlrec = "INCIRNLREC";
	private static final String utrnrec = "UTRNREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String ulnkrnlrec = "ULNKRNLREC";
	private IncirnlTableDAM incirnlIO = new IncirnlTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5545rec t5545rec = new T5545rec();
	private T5515rec t5515rec = new T5515rec();
	private T6647rec t6647rec = new T6647rec();
	private T5540rec t5540rec = new T5540rec();
	private T6646rec t6646rec = new T6646rec();
	private T5539rec t5539rec = new T5539rec();
	private T5519rec t5519rec = new T5519rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextIncirnl105, 
		readIncirnl110, 
		fundAllocation180, 
		exit190, 
		readT5540220, 
		readT5729280, 
		notProrata2050, 
		exit2900, 
		enhanb3200, 
		enhanc3300, 
		enhand3400, 
		enhane3500, 
		enhanf3600, 
		nearExit3800, 
		next4150, 
		accumUnit4200, 
		next4250, 
		nearExit4800, 
		comlvlacc5105, 
		cont5110, 
		writrUtrn5800, 
		fixedTerm6200, 
		wholeOfLife6300, 
		exit6900
	}

	public Unitaloc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rnlallrec.rnlallRec = convertAndSetParam(rnlallrec.rnlallRec, parmArray, 0);
		try {
			process();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

private void process()
	{
		mainline100();
		initialisation200();
		getT5539300();
		getT6646400();
		systemError570();
	}

protected void mainline100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para100();
				case nextIncirnl105: 
					nextIncirnl105();
				case readIncirnl110: 
					readIncirnl110();
				case fundAllocation180: 
					fundAllocation180();
				case exit190: 
					exit190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		initialisation200();
		/* Read all INCIRNL records for this component.*/
		/* Only unit linked contract will have a corresponding INCI record.*/
		/* There could be more than one INCI reocrd for each coverage/rider*/
		/* If it is not a unit linked contract, no processing will be*/
		/* required.*/
		incirnlIO.setDataKey(SPACES);
		incirnlIO.setChdrcoy(rnlallrec.company);
		incirnlIO.setChdrnum(rnlallrec.chdrnum);
		incirnlIO.setLife(rnlallrec.life);
		incirnlIO.setCoverage(rnlallrec.coverage);
		incirnlIO.setRider(rnlallrec.rider);
		incirnlIO.setPlanSuffix(rnlallrec.planSuffix);
		/* MOVE BEGNH                  TO INCIRNL-FUNCTION.             */
		incirnlIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
				
		incirnlIO.setFormat(incirnlrec);
		goTo(GotoLabel.readIncirnl110);
	}

protected void nextIncirnl105()
	{
		incirnlIO.setFunction(varcom.nextr);
		wsaaAmountHoldersInner.wsaaInciPerd[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciPerd[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[2].set(ZERO);
	}

protected void readIncirnl110()
	{
		SmartFileCode.execute(appVars, incirnlIO);
		if ((isNE(incirnlIO.getStatuz(),varcom.oK))
		&& (isNE(incirnlIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(incirnlIO.getStatuz());
			syserrrec.params.set(incirnlIO.getParams());
			databaseError580();
		}
		if (isEQ(incirnlIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit190);
		}
		if ((isNE(incirnlIO.getChdrcoy(),rnlallrec.company))
		|| (isNE(incirnlIO.getChdrnum(),rnlallrec.chdrnum))
		|| (isNE(incirnlIO.getLife(),rnlallrec.life))
		|| (isNE(incirnlIO.getCoverage(),rnlallrec.coverage))
		|| (isNE(incirnlIO.getRider(),rnlallrec.rider))
		|| (isNE(incirnlIO.getPlanSuffix(),rnlallrec.planSuffix))) {
			goTo(GotoLabel.exit190);
		}
		/*     MOVE REWRT              TO INCIRNL-FUNCTION              */
		/*     CALL 'INCIRNLIO' USING INCIRNL-PARAMS                    */
		/*     IF (INCIRNL-STATUZ NOT = O-K)                            */
		/*         MOVE INCIRNL-STATUZ TO SYSR-STATUZ                   */
		/*         MOVE INCIRNL-PARAMS TO SYSR-PARAMS                   */
		/*         PERFORM 580-DATABASE-ERROR                           */
		/*     ELSE                                                     */
		/*         GO TO 190-EXIT.                                      */
		/* For each INCIRNL record read, split the instalment into*/
		/* 3 different portions accordingly.*/
		wsaaAmountHoldersInner.wsaaNonInvestPremTot.set(0);
		wsaaAmountHoldersInner.wsaaAllocInitTot.set(0);
		wsaaAmountHoldersInner.wsaaAllocAccumTot.set(0);
		/*                                                         <D9604>*/
		/* If there is an amount passed in the RNLA-TOTRECD        <D9604>*/
		/* this must be a pro-rata premium amount passed from      <D9604>*/
		/* issue as in all other cases this amount will be zero.   <D9604>*/
		/* When this occurs, use the RNLA-COVR-INSTPREM as the prem<D9604>*/
		/* allocate, not the INCIRNL-CURR-PREM.                    <D9604>*/
		/*                                                         <D9604>*/
		if (isGT(rnlallrec.totrecd,0)) {
			wsaaInciCurrPrem.set(incirnlIO.getCurrPrem());
			wsaaAmountHoldersInner.wsaaInciPrem.set(rnlallrec.covrInstprem);
		}
		else {
			wsaaAmountHoldersInner.wsaaInciPrem.set(incirnlIO.getCurrPrem());
		}
		/*    MOVE INCIRNL-CURR-PREM          TO WSAA-INCI-PREM.   <D9604> */
		wsaaAmountHoldersInner.wsaaIndex.set(1);
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex, 4))
		|| (isEQ(wsaaAmountHoldersInner.wsaaInciPrem, 0)))) {
			premiumAllocation1000();
		}
		
		/* Rewrite the INCIRNL record once the instalment premium has*/
		/* been allocated fully.*/
		/* MOVE REWRT                  TO INCIRNL-FUNCTION.             */
		incirnlIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, incirnlIO);
		if ((isNE(incirnlIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(incirnlIO.getStatuz());
			syserrrec.params.set(incirnlIO.getParams());
			databaseError580();
		}
		/* Check for any enhancement for these preimums from T6647.*/
		/* (T6647 has been read in 200-INITIALISATION section.)*/
		if (isEQ(t6647rec.enhall,SPACES)) {
			goTo(GotoLabel.fundAllocation180);
		}
		/*                                                         <D9604>*/
		/* Enhanced allocation is not applicable for flexible premi<D9604>*/
		/* contracts. Therefore, bypass processing.                <D9604>*/
		/*                                                         <D9604>*/
		if (flexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.fundAllocation180);
		}
		/* Read T5545 to get the enhancement percentage.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5545);
		itdmIO.setItmfrm(rnlallrec.effdate);
		wsaaT5545Cntcurr.set(rnlallrec.cntcurr);
		wsaaT5545Enhall.set(t6647rec.enhall);
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5545))
		|| (isNE(itdmIO.getItemitem(),wsaaT5545Item))) {
			itdmIO.setGenarea(SPACES);
		}
		t5545rec.t5545Rec.set(itdmIO.getGenarea());
		if (isEQ(t5545rec.t5545Rec,SPACES)) {
			goTo(GotoLabel.fundAllocation180);
		}
		/* Look for the correct percentage.*/
		wsaaAmountHoldersInner.wsaaEnhancePerc.set(0);
		wsaaAmountHoldersInner.wsaaIndex2.set(1);
		/* Should use the total coverage premium instead of the current    */
		/* INCI premium to decide if enhanced allocation is needed.        */
		/* Store the current INCI premium away and put the total coverage  */
		/* premium into it instead so that we dont't have to amend the code*/
		/* in 3000-.                                                       */
		/* Make sure RNLA-COVR-INSTPREM is numeric first, just in case of  */
		/* the calling program has not updated the field correctly.        */
		if (isEQ(rnlallrec.covrInstprem,NUMERIC)) {
			wsaaInciCurrPrem.set(incirnlIO.getCurrPrem());
			incirnlIO.setCurrPrem(rnlallrec.covrInstprem);
		}
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex2, 6))
		|| (isNE(wsaaAmountHoldersInner.wsaaEnhancePerc, 0)))) {
			matchEnhanceBasis3000();
		}
		
		/* Move the original amount back to current INCI premium.          */
		if (isEQ(rnlallrec.covrInstprem,NUMERIC)) {
			incirnlIO.setCurrPrem(wsaaInciCurrPrem);
		}
		if (isEQ(wsaaAmountHoldersInner.wsaaEnhancePerc, 0)) {
			goTo(GotoLabel.fundAllocation180);
		}
		/* Calculate the enhanced premiums.*/
		wsaaAmountHoldersInner.wsaaOldAllocInitTot.set(wsaaAmountHoldersInner.wsaaAllocInitTot);
		compute(wsaaAmountHoldersInner.wsaaAllocInitTot, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInitTot);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocInitTot.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(sub(wsaaAmountHoldersInner.wsaaOldAllocInitTot, wsaaAmountHoldersInner.wsaaAllocInitTot));
		wsaaAmountHoldersInner.wsaaOldAllocAccumTot.set(wsaaAmountHoldersInner.wsaaAllocAccumTot);
		compute(wsaaAmountHoldersInner.wsaaAllocAccumTot, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccumTot);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocAccumTot.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(add(wsaaAmountHoldersInner.wsaaEnhanceAlloc, (sub(wsaaAmountHoldersInner.wsaaOldAllocAccumTot, wsaaAmountHoldersInner.wsaaAllocAccumTot))));
	}

protected void fundAllocation180()
	{
		/* Read ULNKRNL to get the fund split for the coverage/rider.*/
		ulnkrnlIO.setDataKey(SPACES);
		ulnkrnlIO.setChdrcoy(rnlallrec.company);
		ulnkrnlIO.setChdrnum(rnlallrec.chdrnum);
		ulnkrnlIO.setLife(rnlallrec.life);
		ulnkrnlIO.setCoverage(rnlallrec.coverage);
		ulnkrnlIO.setRider(rnlallrec.rider);
		ulnkrnlIO.setPlanSuffix(rnlallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			syserrrec.params.set(ulnkrnlIO.getParams());
			databaseError580();
		}
		/* Calculate initial unit discount factor.*/
		/* Write a UTRN record for each fund split.*/
		initUnitDiscDetails6000();
		wsaaAmountHoldersInner.wsaaIndex2.set(1);
		wsaaAmountHoldersInner.wsaaRunInitTot.set(0);
		wsaaAmountHoldersInner.wsaaRunAccumTot.set(0);
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex2, 10))
		|| (isEQ(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2), SPACES)))) {
			splitAllocation4000();
		}
		
		/* Write a UTRN record for the uninvested premium.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPremTot, 0).set(add(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaEnhanceAlloc));
		if (isNE(wsaaAmountHoldersInner.wsaaNonInvestPremTot, 0)) {
			utrnIO.setParams(SPACES);
			utrnIO.setUnitSubAccount("NVST");
			utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPremTot);
			utrnIO.setDiscountFactor(0);
			setUpWriteUtrn5000();
		}
		goTo(GotoLabel.nextIncirnl105);
	}

protected void exit190()
	{
		exitProgram();
	}

protected void stop199()
	{
		stopRun();
	}

protected void initialisation200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para201();
					readT6647210();
				case readT5540220: 
					readT5540220();
					readT5645240();
					readT5688250();
					readT5519260();
				case readT5729280: 
					readT5729280();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para201()
	{
		syserrrec.subrname.set(wsaaSubr);
		rnlallrec.statuz.set(varcom.oK);
		/* ACCEPT VRCM-TIME            FROM TIME.                       */
		varcom.vrcmTimex.set(getCobolTime());
		varcom.vrcmTime.set(varcom.vrcmTimen);
		varcom.vrcmDate.set(getCobolDate());
		wsaaAmountHoldersInner.wsaaInciPerd[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciPerd[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[2].set(ZERO);
	}

protected void readT6647210()
	{
		/* Read T6647 for the relevant details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t6647);
		itdmIO.setItmfrm(rnlallrec.effdate);
		wsaaT6647Batctrcde.set(rnlallrec.batctrcde);
		wsaaT6647Cnttype.set(rnlallrec.cnttype);
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.readT5540220);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(),t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Item))) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.readT5540220);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void readT5540220()
	{
		/* Read T5540 for the general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(rnlallrec.crtable);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5540))
		|| (isNE(itdmIO.getItemitem(),rnlallrec.crtable))) {
			itdmIO.setGenarea(SPACES);
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
	}

protected void readT5645240()
	{
		/* Read T5645 for the SUB ACC details.                             */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT5688250()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(rnlallrec.company);
		/*  MOVE RNLA-EFFDATE           TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(rnlallrec.moniesDate);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(rnlallrec.cnttype);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),rnlallrec.company)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),rnlallrec.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(rnlallrec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			systemError570();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	*230-READ-T5519.                                                  
	* </pre>
	*/
protected void readT5519260()
	{
		/* Read T5519 for initial unit discount details.*/
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			/*        GO TO 290-EXIT.                                  <D9604>*/
			goTo(GotoLabel.readT5729280);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5519);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5519))
		|| (isNE(itdmIO.getItemitem(),t5540rec.iuDiscBasis))) {
			itdmIO.setItemitem(t5540rec.iuDiscBasis);
			syserrrec.params.set(itdmIO.getParams());
			/*      MOVE ITDM-STATUZ        TO SYSR-STATUZ                   */
			syserrrec.statuz.set(g027);
			systemError570();
		}
		/*      PERFORM 580-DATABASE-ERROR.                              */
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	*                                                         <D9604>
	* Read T5729 to see if this is a Flexible Premium Contract<D9604>
	*                                                         <D9604>
	* </pre>
	*/
protected void readT5729280()
	{
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(rnlallrec.cnttype);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(rnlallrec.company, SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(rnlallrec.cnttype, SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),rnlallrec.company)
		|| isNE(itdmIO.getItemtabl(),t5729)
		|| isNE(itdmIO.getItemitem(),rnlallrec.cnttype)) {
			return ;
		}
		wsaaFlexPrem.set("Y");
	}

protected void getT5539300()
	{
			para310();
		}

protected void para310()
	{
		/*  Read T5539 for initial unit discount factor for term.*/
		if (isEQ(t5540rec.iuDiscFact,SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(t5539);
		itemIO.setItemitem(t5540rec.iuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
	}

protected void getT6646400()
	{
			para410();
		}

protected void para410()
	{
		/* Read T5546 for initial unit discount factor for whole of life.*/
		if (isEQ(t5540rec.wholeIuDiscFact,SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(t6646);
		itemIO.setItemitem(t5540rec.wholeIuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}

protected void systemError570()
	{
			para570();
			exit579();
		}

protected void para570()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError580()
	{
			para580();
			exit589();
		}

protected void para580()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void premiumAllocation1000()
	{
					para1010();
					nearExit1800();
				}

protected void para1010()
	{
		/* WSAA-ALLOC-AMT is the amount to be allocated to buy units.*/
		/* It is not necessary to be the same as the premium.(Depends*/
		/* on INCI details.)*/
		if (isEQ(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), 0)) {
			return ;
		}
		if (isGTE(wsaaAmountHoldersInner.wsaaInciPrem, incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex))) {
			wsaaAmountHoldersInner.wsaaAllocAmt.set(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex));
		}
		if (isGT(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), wsaaAmountHoldersInner.wsaaInciPrem)) {
			wsaaAmountHoldersInner.wsaaAllocAmt.set(wsaaAmountHoldersInner.wsaaInciPrem);
		}
		calculateSplit2000();
	}

protected void nearExit1800()
	{
		wsaaAmountHoldersInner.wsaaIndex.add(1);
		/*EXIT*/
	}

protected void calculateSplit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2010();
				case notProrata2050: 
					notProrata2050();
				case exit2900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2010()
	{
		/* Calculate the uninvested allocation.*/
		wsaaAmountHoldersInner.wsaaEnhanceAlloc.set(0);
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 1).setRounded(div((mult(wsaaAmountHoldersInner.wsaaAllocAmt, (sub(100, incirnlIO.getPcunit(wsaaAmountHoldersInner.wsaaIndex))))), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying initial units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 1).setRounded(div(mult((sub(wsaaAmountHoldersInner.wsaaAllocAmt, wsaaAmountHoldersInner.wsaaNonInvestPrem)), incirnlIO.getUsplitpc(wsaaAmountHoldersInner.wsaaIndex)), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying accumulation units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 1).setRounded(sub(wsaaAmountHoldersInner.wsaaAllocAmt, (add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaAllocInit))));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		/* Accumulate totals.*/
		wsaaAmountHoldersInner.wsaaNonInvestPremTot.add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		wsaaAmountHoldersInner.wsaaAllocInitTot.add(wsaaAmountHoldersInner.wsaaAllocInit);
		wsaaAmountHoldersInner.wsaaAllocAccumTot.add(wsaaAmountHoldersInner.wsaaAllocAccum);
		/* Update the premium left to pay for that period.*/
		setPrecision(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), 1);
		incirnlIO.setPremcurr(wsaaAmountHoldersInner.wsaaIndex, sub(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), wsaaAmountHoldersInner.wsaaAllocAmt), true);
		/* Store the INCI amounts in working storage.                      */
		if (isEQ(wsaaAmountHoldersInner.wsaaInciPerd[1], ZERO)) {
			wsaaAmountHoldersInner.wsaaInciPerd[1].set(wsaaAmountHoldersInner.wsaaIndex);
			wsaaAmountHoldersInner.wsaaInciInit[1].add(wsaaAmountHoldersInner.wsaaAllocInit);
			wsaaAmountHoldersInner.wsaaInciAlloc[1].add(wsaaAmountHoldersInner.wsaaAllocAccum);
			wsaaAmountHoldersInner.wsaaInciNInvs[1].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		else {
			wsaaAmountHoldersInner.wsaaInciPerd[2].set(wsaaAmountHoldersInner.wsaaIndex);
			wsaaAmountHoldersInner.wsaaInciInit[2].add(wsaaAmountHoldersInner.wsaaAllocInit);
			wsaaAmountHoldersInner.wsaaInciAlloc[2].add(wsaaAmountHoldersInner.wsaaAllocAccum);
			wsaaAmountHoldersInner.wsaaInciNInvs[2].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		/* If the instalment premium is fully allocated, get out and*/
		/* rewrite the INCIRNL record. Otherwise work out premium left*/
		/* to allocate and go round the loop again.*/
		/* If we are allocating a pro-rata premium (ie RNLA-TOTRECD<D9604>*/
		/* not = zero) compare the coverage instalment premium     <D9604>*/
		/* passed not the INCIRNL-CURR-PREM as this is the         <D9604>*/
		/* amount we are allocating.                               <D9604>*/
		/*                                                         <D9604>*/
		if (isEQ(rnlallrec.totrecd,ZERO)) {
			goTo(GotoLabel.notProrata2050);
		}
		if ((setPrecision(rnlallrec.covrInstprem, 0)
		&& isEQ(rnlallrec.covrInstprem, (add(add(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot))))) {
			wsaaAmountHoldersInner.wsaaInciPrem.set(0);
		}
		else {
			compute(wsaaAmountHoldersInner.wsaaInciPrem, 3).setRounded(sub(sub(sub(rnlallrec.covrInstprem, wsaaAmountHoldersInner.wsaaNonInvestPremTot), wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot));
		}
		goTo(GotoLabel.exit2900);
	}

protected void notProrata2050()
	{
		if ((setPrecision(incirnlIO.getCurrPrem(), 0)
		&& isEQ(incirnlIO.getCurrPrem(), (add(add(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot))))) {
			wsaaAmountHoldersInner.wsaaInciPrem.set(0);
		}
		else {
			compute(wsaaAmountHoldersInner.wsaaInciPrem, 3).setRounded(sub(sub(sub(incirnlIO.getCurrPrem(), wsaaAmountHoldersInner.wsaaNonInvestPremTot), wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot));
		}
	}

protected void matchEnhanceBasis3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3010();
					enhana3100();
				case enhanb3200: 
					enhanb3200();
				case enhanc3300: 
					enhanc3300();
				case enhand3400: 
					enhand3400();
				case enhane3500: 
					enhane3500();
				case enhanf3600: 
					enhanf3600();
				case nearExit3800: 
					nearExit3800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3010()
	{
		/* First match the billing frequency.*/
		if (isEQ(t5545rec.billfreq[wsaaAmountHoldersInner.wsaaIndex2.toInt()], SPACES)) {
			goTo(GotoLabel.nearExit3800);
		}
		if (isNE(rnlallrec.billfreq, t5545rec.billfreq[wsaaAmountHoldersInner.wsaaIndex2.toInt()])) {
			goTo(GotoLabel.nearExit3800);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.
	* There are 5 premiums in each billing frequency set, so
	* match each premium individually.
	* </pre>
	*/
protected void enhana3100()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 1)) {
			goTo(GotoLabel.enhanb3200);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc02);
					}
					else {
						/* IF (INCIRNL-CURR-PREM NOT < T5545-ENHANA-PRM-01)             */
						/*    AND (T5545-ENHANA-PRM-01 NOT = 0)                    <003>*/
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhanb3200()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 2)) {
			goTo(GotoLabel.enhanc3300);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc02);
					}
					else {
						/* IF (INCIRNL-CURR-PREM NOT < T5545-ENHANB-PRM-01)             */
						/*    AND (T5545-ENHANB-PRM-01 NOT = 0)                    <003>*/
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhanc3300()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 3)) {
			goTo(GotoLabel.enhand3400);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc02);
					}
					else {
						/* IF (INCIRNL-CURR-PREM NOT < T5545-ENHANC-PRM-01)             */
						/*    AND (T5545-ENHANC-PRM-01 NOT = 0)                    <003>*/
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhand3400()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 4)) {
			goTo(GotoLabel.enhane3500);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc02);
					}
					else {
						/* IF (INCIRNL-CURR-PREM NOT < T5545-ENHAND-PRM-01)             */
						/*    AND (T5545-ENHAND-PRM-01 NOT = 0)                    <003>*/
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhane3500()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 5)) {
			goTo(GotoLabel.enhanf3600);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc02);
					}
					else {
						/* IF (INCIRNL-CURR-PREM NOT < T5545-ENHANE-PRM-01)             */
						/*    AND (T5545-ENHANE-PRM-01 NOT = 0)                    <003>*/
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhanf3600()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 6)) {
			goTo(GotoLabel.nearExit3800);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc02);
					}
					else {
						/* IF (INCIRNL-CURR-PREM NOT < T5545-ENHANF-PRM-01)             */
						/*    AND (T5545-ENHANF-PRM-01 NOT = 0)                    <003>*/
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void nearExit3800()
	{
		wsaaAmountHoldersInner.wsaaIndex2.add(1);
		/*EXIT*/
	}

protected void splitAllocation4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initUnit4100();
				case next4150: 
					next4150();
				case accumUnit4200: 
					accumUnit4200();
				case next4250: 
					next4250();
				case nearExit4800: 
					nearExit4800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initUnit4100()
	{
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocInitTot, 0)) {
			goTo(GotoLabel.accumUnit4200);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaRunInitTot));
			goTo(GotoLabel.next4150);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaRunInitTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocInitTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
		}
	}

protected void next4150()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		callRounding8000();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		utrnIO.setUnitType("I");
		utrnIO.setUnitSubAccount("INIT");
		utrnIO.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
		setUpWriteUtrn5000();
	}

	/**
	* <pre>
	* Invest the accumulation units premium in the funds specified in
	* the ULNKRNL record. Write a UTRN record for each fund.
	* </pre>
	*/
protected void accumUnit4200()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocAccumTot, 0)) {
			goTo(GotoLabel.nearExit4800);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaRunAccumTot));
			goTo(GotoLabel.next4250);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaRunAccumTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocAccumTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
		}
	}

protected void next4250()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		callRounding8000();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		utrnIO.setUnitType("A");
		utrnIO.setUnitSubAccount("ACUM");
		utrnIO.setDiscountFactor(0);
		setUpWriteUtrn5000();
	}

protected void nearExit4800()
	{
		wsaaAmountHoldersInner.wsaaIndex2.add(1);
		/*EXIT*/
	}

protected void setUpWriteUtrn5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5100();
				case comlvlacc5105: 
					comlvlacc5105();
				case cont5110: 
					cont5110();
				case writrUtrn5800: 
					writrUtrn5800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5100()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5515))
		|| (isNE(itdmIO.getItemitem(),utrnIO.getUnitVirtualFund()))) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		utrnIO.setChdrcoy(rnlallrec.company);
		utrnIO.setChdrnum(rnlallrec.chdrnum);
		utrnIO.setCoverage(rnlallrec.coverage);
		utrnIO.setLife(rnlallrec.life);
		utrnIO.setRider(rnlallrec.rider);
		utrnIO.setPlanSuffix(rnlallrec.planSuffix);
		utrnIO.setTranno(rnlallrec.tranno);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		/* MOVE RNLA-EFFDATE           TO UTRN-TRANSACTION-DATE.        */
		utrnIO.setTransactionDate(varcom.vrcmDate);
		utrnIO.setUser(rnlallrec.user);
		utrnIO.setBatccoy(rnlallrec.batccoy);
		utrnIO.setBatcbrn(rnlallrec.batcbrn);
		utrnIO.setBatcactyr(rnlallrec.batcactyr);
		utrnIO.setBatcactmn(rnlallrec.batcactmn);
		utrnIO.setBatctrcde(rnlallrec.batctrcde);
		utrnIO.setBatcbatch(rnlallrec.batcbatch);
		utrnIO.setCrtable(rnlallrec.crtable);
		utrnIO.setCntcurr(rnlallrec.cntcurr);
		/*  IF UTRN-UNIT-SUB-ACCOUNT = 'NVST'                            */
		/*      MOVE RNLA-SACSCODE-02   TO UTRN-SACSCODE                 */
		/*      MOVE RNLA-SACSTYP-02    TO UTRN-SACSTYP                  */
		/*      MOVE RNLA-GENLCDE-02    TO UTRN-GENLCDE                  */
		/*  ELSE                                                         */
		/*      MOVE RNLA-SACSCODE      TO UTRN-SACSCODE                 */
		/*      MOVE RNLA-SACSTYP       TO UTRN-SACSTYP                  */
		/*      MOVE RNLA-GENLCDE       TO UTRN-GENLCDE.                 */
		/*MOVE RNLA-SACSCODE          TO UTRN-SACSCODE.                */
		/*MOVE RNLA-SACSTYP           TO UTRN-SACSTYP.                 */
		/*MOVE RNLA-GENLCDE           TO UTRN-GENLCDE.                 */
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			goTo(GotoLabel.comlvlacc5105);
		}
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		goTo(GotoLabel.cont5110);
	}

protected void comlvlacc5105()
	{
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode04);
			utrnIO.setSacstyp(t5645rec.sacstype04);
			utrnIO.setGenlcde(t5645rec.glmap04);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode03);
			utrnIO.setSacstyp(t5645rec.sacstype03);
			utrnIO.setGenlcde(t5645rec.glmap03);
		}
	}

protected void cont5110()
	{
		utrnIO.setContractType(rnlallrec.cnttype);
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(rnlallrec.crdate);
		utrnIO.setFundCurrency(t5515rec.currcode);
		if (isLT(utrnIO.getContractAmount(),0)) {
			utrnIO.setNowDeferInd(t6647rec.dealin);
		}
		else {
			utrnIO.setNowDeferInd(t6647rec.aloind);
		}
		/* Calculate the monies date.                                      */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaMoniesDate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode,"BD")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(t6647rec.efdcode,"DD")) {
				/*MOVE RNLA-BILLCD        TO WSAA-MONIES-DATE        <008>*/
				wsaaMoniesDate.set(rnlallrec.duedate);
			}
			else {
				if (isEQ(t6647rec.efdcode,"RD")) {
					wsaaMoniesDate.set(rnlallrec.moniesDate);
				}
				else {
					if (isEQ(t6647rec.efdcode,"LO")) {
						/*IF RNLA-BILLCD     > WSAA-MONIES-DATE              <008>*/
						/*MOVE RNLA-BILLCD     TO WSAA-MONIES-DATE.      <008>*/
						if (isGT(rnlallrec.duedate,wsaaMoniesDate)) {
							wsaaMoniesDate.set(rnlallrec.duedate);
						}
					}
				}
			}
		}
		/*  AS ISSUE SETS UP THIS INFO CORRECTLY ,ITS SEEMS POINTLESS      */
		/*  TO REPEAT / OR REMOVE CORRECT CODING.                          */
		if (isEQ(rnlallrec.batctrcde,"T642")) {
			utrnIO.setMoniesDate(rnlallrec.moniesDate);
		}
		else {
			utrnIO.setMoniesDate(wsaaMoniesDate);
		}
		/*MOVE RNLA-MONIES-DATE       TO UTRN-MONIES-DATE.             */
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setNofUnits(0);
		utrnIO.setNofDunits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setFundAmount(0);
		utrnIO.setSurrenderPercent(0);
		utrnIO.setUstmno(0);
		utrnIO.setInciprm01(0);
		utrnIO.setInciprm02(0);
		/*  MOVE INCIRNL-INCI-NUM       TO UTRN-INCI-NUM.                */
		utrnIO.setInciNum(incirnlIO.getSeqno());
		utrnIO.setInciPerd01(wsaaAmountHoldersInner.wsaaInciPerd[1]);
		utrnIO.setInciPerd02(wsaaAmountHoldersInner.wsaaInciPerd[2]);
		if (isNE(wsaaAmountHoldersInner.wsaaEnhancePerc, ZERO)) {
			/* Nothing to do. */
		}
		/* IF WSAA-INCIPRM (01) > ZERO*/
		/*    COMPUTE                  UTRN-INCIPRM01              <001>*/
		/*    =   (WSAA-INCIPRM (01) * WSAA-CONTRACT-AMOUNT)       <001>*/
		/*    /   WSAA-INCIPRM (01).                               <001>*/
		/* IF WSAA-INCIPRM (02) > ZERO                                  */
		/*    COMPUTE                  UTRN-INCIPRM02              <001>*/
		/*    =   (WSAA-INCIPRM (02) * WSAA-CONTRACT-AMOUNT)       <001>*/
		/*    /   WSAA-INCIPRM (02).                               <001>*/
		/* Use the amounts previously stored for each allocation           */
		/* period rather than the  whole contract amounts.                 */
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			utrnIO.setInciprm01(wsaaAmountHoldersInner.wsaaInciNInvs[1]);
			utrnIO.setInciprm02(wsaaAmountHoldersInner.wsaaInciNInvs[2]);
			goTo(GotoLabel.writrUtrn5800);
		}
		if (isEQ(utrnIO.getUnitSubAccount(),"INIT")) {
			compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult((div(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocInitTot)), 100));
			if (isGT(wsaaAmountHoldersInner.wsaaInciInit[1], ZERO)) {
				setPrecision(utrnIO.getInciprm01(), 1);
				utrnIO.setInciprm01(div((mult(wsaaAmountHoldersInner.wsaaInciInit[1], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
			}
			if (isGT(wsaaAmountHoldersInner.wsaaInciInit[2], ZERO)) {
				setPrecision(utrnIO.getInciprm02(), 1);
				utrnIO.setInciprm02(div((mult(wsaaAmountHoldersInner.wsaaInciInit[2], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
			}
			goTo(GotoLabel.writrUtrn5800);
		}
		if (isEQ(utrnIO.getUnitSubAccount(),"ACUM")) {
			compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult((div(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocAccumTot)), 100));
			if (isGT(wsaaAmountHoldersInner.wsaaInciAlloc[1], ZERO)) {
				setPrecision(utrnIO.getInciprm01(), 1);
				utrnIO.setInciprm01(div((mult(wsaaAmountHoldersInner.wsaaInciAlloc[1], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
			}
			if (isGT(wsaaAmountHoldersInner.wsaaInciAlloc[2], ZERO)) {
				setPrecision(utrnIO.getInciprm02(), 1);
				utrnIO.setInciprm02(div((mult(wsaaAmountHoldersInner.wsaaInciAlloc[2], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
			}
		}
	}

protected void writrUtrn5800()
	{
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			databaseError580();
		}
		/*EXIT*/
	}

protected void initUnitDiscDetails6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6010();
					termToRun6100();
				case fixedTerm6200: 
					fixedTerm6200();
				case wholeOfLife6300: 
					wholeOfLife6300();
				case exit6900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6010()
	{
		wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(1);
		if (((isEQ(t5540rec.iuDiscBasis,SPACES))
		|| (isEQ(t5540rec.iuDiscFact,SPACES)))
		&& (isEQ(t5540rec.wholeIuDiscFact,SPACES))) {
			goTo(GotoLabel.exit6900);
		}
	}

	/**
	* <pre>
	* There are 3 methods to obtain the discount factor, which one to
	* use is dependent on the field entries in T5519 & T5540 which
	* has been read in initialisation section.(Only one method
	* allowed.)
	* </pre>
	*/
protected void termToRun6100()
	{
		/* If whole of life discount factor(T5540-WHOLE-IU-DISC-FACT) is*/
		/* blank,(i.e. term discount basis not blank) and if fixed term*/
		/* (T5519-FIXTRM) is zero, use this the term left to run to read*/
		/* off T5539 to obtain the discount factor.*/
		if (isNE(t5540rec.wholeIuDiscFact,SPACES)) {
			goTo(GotoLabel.wholeOfLife6300);
		}
		if (isNE(t5519rec.fixdtrm,0)) {
			goTo(GotoLabel.fixedTerm6200);
		}
		if (isEQ(rnlallrec.termLeftToRun,0)) {
			goTo(GotoLabel.exit6900);
		}
		getT5539300();
		/*    MOVE T5539-DFACT(RNLA-TERM-LEFT-TO-RUN) TO                   */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(rnlallrec.termLeftToRun, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[rnlallrec.termLeftToRun.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(rnlallrec.termLeftToRun, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void fixedTerm6200()
	{
		/* Use this fixed term to read off T5539 to obtain the*/
		/* discount factor.*/
		getT5539300();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void wholeOfLife6300()
	{
		/* Use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		if (isEQ(rnlallrec.anbAtCcd,0)) {
			return ;
		}
		getT6646400();
		/*    MOVE T6646-DFACT(RNLA-ANB-AT-CCD) TO                         */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(rnlallrec.anbAtCcd, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.dfact[rnlallrec.anbAtCcd.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(rnlallrec.anbAtCcd, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		return ;
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(rnlallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rnlallrec.cntcurr);
		zrdecplrec.batctrcde.set(rnlallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError580();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
private static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAllocAmt = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaNonInvestPremTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaOldAllocInitTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocInitTot = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaOldAllocAccumTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocAccumTot = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaInciPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaIndex2 = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaEnhancePerc = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private PackedDecimalData[] wsaaInciPerd = PDInittedArray(2, 1, 0);
	private PackedDecimalData[] wsaaInciprm = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciInit = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciAlloc = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciNInvs = PDInittedArray(2, 17, 2);
	private PackedDecimalData wsaaContractAmount = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaFundPc = new ZonedDecimalData(17, 2).init(0);
	}
}
