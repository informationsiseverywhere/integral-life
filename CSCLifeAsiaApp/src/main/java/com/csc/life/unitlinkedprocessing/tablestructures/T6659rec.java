package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:02
 * Description:
 * Copybook name: T6659REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6659rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6659Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData annOrPayInd = new FixedLengthStringData(1).isAPartOf(t6659Rec, 0);
  	public FixedLengthStringData freq = new FixedLengthStringData(2).isAPartOf(t6659Rec, 1);
  	public FixedLengthStringData osUtrnInd = new FixedLengthStringData(1).isAPartOf(t6659Rec, 3);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(t6659Rec, 4);
  	public FixedLengthStringData filler = new FixedLengthStringData(486).isAPartOf(t6659Rec, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6659Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6659Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}