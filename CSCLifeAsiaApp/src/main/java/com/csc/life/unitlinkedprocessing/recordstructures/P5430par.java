package com.csc.life.unitlinkedprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:30
 * Description:
 * Copybook name: P5430PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P5430par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(22);
  	public FixedLengthStringData asAtDate = new FixedLengthStringData(1).isAPartOf(parmRecord, 0);
  	public ZonedDecimalData dteeff = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 1);
  	public ZonedDecimalData jobno = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 9);
  	public FixedLengthStringData printBarePrice = new FixedLengthStringData(1).isAPartOf(parmRecord, 17);
  	public FixedLengthStringData unitVirtualFund = new FixedLengthStringData(4).isAPartOf(parmRecord, 18);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}