package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:53
 * Description:
 * Copybook name: UTRSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrsKey = new FixedLengthStringData(64).isAPartOf(utrsFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsKey, 0);
  	public FixedLengthStringData utrsChdrnum = new FixedLengthStringData(8).isAPartOf(utrsKey, 1);
  	public FixedLengthStringData utrsLife = new FixedLengthStringData(2).isAPartOf(utrsKey, 9);
  	public FixedLengthStringData utrsCoverage = new FixedLengthStringData(2).isAPartOf(utrsKey, 11);
  	public FixedLengthStringData utrsRider = new FixedLengthStringData(2).isAPartOf(utrsKey, 13);
  	public PackedDecimalData utrsPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsKey, 15);
  	public FixedLengthStringData utrsUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrsKey, 18);
  	public FixedLengthStringData utrsUnitType = new FixedLengthStringData(1).isAPartOf(utrsKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(utrsKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}