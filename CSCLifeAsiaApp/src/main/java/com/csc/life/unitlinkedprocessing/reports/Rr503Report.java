package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR503.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rr503Report extends SMARTReportLayout { 

	private ZonedDecimalData age = new ZonedDecimalData(3, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private ZonedDecimalData curduntbal = new ZonedDecimalData(16, 5);
	private ZonedDecimalData fundvalb = new ZonedDecimalData(18, 5);
	private ZonedDecimalData fundvalc = new ZonedDecimalData(18, 5);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData statcode = new FixedLengthStringData(2);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData ubidpr = new ZonedDecimalData(9, 5);
	private FixedLengthStringData vfund = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr503Report() {
		super();
	}


	/**
	 * Print the XML for Rr503d01
	 */
	public void printRr503d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		statcode.setFieldName("statcode");
		statcode.setInternal(subString(recordData, 9, 2));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 11, 4));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 15, 17));
		vfund.setFieldName("vfund");
		vfund.setInternal(subString(recordData, 32, 4));
		curduntbal.setFieldName("curduntbal");
		curduntbal.setInternal(subString(recordData, 36, 16));
		ubidpr.setFieldName("ubidpr");
		ubidpr.setInternal(subString(recordData, 52, 9));
		fundvalc.setFieldName("fundvalc");
		fundvalc.setInternal(subString(recordData, 61, 18));
		age.setFieldName("age");
		age.setInternal(subString(recordData, 79, 3));
		fundvalb.setFieldName("fundvalb");
		fundvalb.setInternal(subString(recordData, 82, 18));
		printLayout("Rr503d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				statcode,
				crtable,
				sumins,
				vfund,
				curduntbal,
				ubidpr,
				fundvalc,
				age,
				fundvalb
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr503h01
	 */
	public void printRr503h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("Rr503h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				time
			}
		);

		currentPrintLine.set(9);
	}


}
