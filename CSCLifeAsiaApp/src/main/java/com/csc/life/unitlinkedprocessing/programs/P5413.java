/*
 * File: P5413.java
 * Date: 30 August 2009 0:24:32
 * Author: Quipoz Limited
 * 
 * Class transformed from P5413.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.unitlinkedprocessing.dataaccess.VprcupeTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcuprTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S5413ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*   P5413 - Create U.L. Bare Prices.
*
*   This program allows the user to enter a set of bare prices
*   for funds. These prices are identified by Job Number and
*   Effective Date.
*
*   For each fund there must be a non-zero entry of accumulation
*   unit. Matching also available by entering 1, 2 ,3  or 4
*   characters. Consequently the program will display the fund
*   that matches these characters as the first fund in the
*   subfile.
*
*   It is called from the Unit Linked Entry submenu(P5412) by
*   means of selecting the appropriate option.
*
*   The main program flow is as follows:-
*
*        Initialisation.
*
*        Read T5515 and select records, then load to the subfile.
*        This is to be repeated until subfile size equals to the
*        screen size or end of file.
*
*        Check screen status.
*        If ROLU
*            Display next page of the subfile by reading T5515 and
*            loading as above.
*        If not ROLU
*            Validate the screen fields until there is no error.
*
*        Once the prices are captured, write two records(Initial and
*        Accumulation) for each fund that has prices.
*
*        Return control to calling program.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*        Do not display interest bearing funds since they             *
*        do not require prices.                                       *
*                                                                     *
*****************************************************************
* </pre>
*/
public class P5413 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5413");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaNormError = "N";
	private String wsaaDateRecFound = "";

	private FixedLengthStringData wsaaPartFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPartF1 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 0);
	private FixedLengthStringData wsaaPartF2 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 1);
	private FixedLengthStringData wsaaPartF3 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 2);
	private FixedLengthStringData wsaaPartF4 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 3);

	private FixedLengthStringData wsaaItemFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaItemF1 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 0);
	private FixedLengthStringData wsaaItemF2 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 1);
	private FixedLengthStringData wsaaItemF3 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 2);
	private FixedLengthStringData wsaaItemF4 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 3);
	private FixedLengthStringData wsaaSearchFund = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevItem = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaLastItem = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaLastItmfrm = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaTdayIntDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaTdayOutDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Validator wsaaRolu = new Validator(wsaaFunctionKey, "ROLU");
	private Validator wsaaCalc = new Validator(wsaaFunctionKey, "CALC");
	private ZonedDecimalData wsaaTolerance = new ZonedDecimalData(8, 5).setUnsigned();
	private String wsaaToleranceFlag = "";
		/* ERRORS */
	private String f351 = "F351";
	private String g037 = "G037";
	private String h025 = "H025";
	private String h094 = "H094";
	private String h095 = "H095";
	private String h978 = "H978";
		/* TABLES */
	private String t5515 = "T5515";
		/* FORMATS */
	private String vprcuprrec = "VPRCUPRREC";
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Getdescrec getdescrec = new Getdescrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5515rec t5515rec = new T5515rec();
		/*Unit Price File (For checking tolerance)*/
	private VprcupeTableDAM vprcupeIO = new VprcupeTableDAM();
		/*Unit Price File*/
	private VprcuprTableDAM vprcuprIO = new VprcuprTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S5413ScreenVars sv = ScreenProgram.getScreenVars( S5413ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		writeToSubfile1300, 
		nextRecord1400, 
		setMoreSign1400, 
		preExit, 
		validateSubfile2100, 
		errorExit2200, 
		rollUp2500, 
		writeToSubfile2600, 
		setMoreSign2650, 
		exit2090, 
		moreUpdateRec3200, 
		exit3900, 
		call5110, 
		exit5190, 
		acumut7100, 
		exit7900, 
		exit8900, 
		begnInitut14150, 
		checkInitut14200, 
		exit14900
	}

	public P5413() {
		super();
		screenVars = sv;
		new ScreenModel("S5413", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1100();
					loadSubfile1200();
				}
				case writeToSubfile1300: {
					writeToSubfile1300();
				}
				case nextRecord1400: {
					nextRecord1400();
				}
				case setMoreSign1400: {
					setMoreSign1400();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1100()
	{
		wsaaPrevItem.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.initut.set(ZERO);
		sv.acumut.set(ZERO);
		wsaaToleranceFlag = "N";
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTdayIntDate.set(datcon1rec.intDate);
		wsaaPartFund.set(wsspuprc.uprcVrtfnd);
		sv.company.set(wsspcomn.company);
		sv.effdate.set(wsspuprc.uprcEffdate);
		sv.jobno.set(wsspuprc.uprcJobno);
		if (isEQ(wsspuprc.uprcVrtfnd,SPACES)) {
			wsaaSearchFund.set("N");
		}
		else {
			wsaaSearchFund.set("Y");
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S5413", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");
	}

protected void loadSubfile1200()
	{
		callItdmIo5100();
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1400);
		}
	}

protected void writeToSubfile1300()
	{
		if (isEQ(t5515rec.zfundtyp,"D")) {
			goTo(GotoLabel.nextRecord1400);
		}
		moveFieldsToSubfile6000();
		getDescription6100();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5413", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextRecord1400()
	{
		itdmIO.setFunction(varcom.nextr);
		callItdmIo5100();
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1400);
		}
		if (isEQ(scrnparams.subfileRrn,sv.subfilePage)) {
			wsaaRem.set(0);
			goTo(GotoLabel.setMoreSign1400);
		}
		goTo(GotoLabel.writeToSubfile1300);
	}

protected void setMoreSign1400()
	{
		if (isNE(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
			wsaaLastItem.set(itdmIO.getItemitem());
			wsaaLastItmfrm.set(itdmIO.getItmfrm());
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if ((isEQ(wsaaNormError,"N"))
		&& (isEQ(wsspcomn.edterror,"Y"))
		&& (!wsaaRolu.isTrue())
		&& (isEQ(wsaaToleranceFlag,"N"))) {
			wsaaToleranceFlag = "Y";
			scrnparams.errorCode.set(h094);
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case validateSubfile2100: {
					validateSubfile2100();
				}
				case errorExit2200: {
					errorExit2200();
				}
				case rollUp2500: {
					rollUp2500();
				}
				case writeToSubfile2600: {
					writeToSubfile2600();
				}
				case setMoreSign2650: {
					setMoreSign2650();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaNormError = "N";
		if ((isNE(scrnparams.function,varcom.prot))) {
			wsaaFunctionKey.set(scrnparams.statuz);
		}
		if (wsaaCalc.isTrue()) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5413", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.rollUp2500);
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(sv.unitVirtualFund,itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		wsaaDateRecFound = "N";
		while ( !(isEQ(wsaaDateRecFound,"Y")
		|| isEQ(itdmIO.getStatuz(),"ENDP"))) {
			checkDates8000();
		}
		
		if (isEQ(itdmIO.getStatuz(),"ENDP")) {
			sv.acumutErr.set(h978);
			sv.initutErr.set(h978);
			wsaaNormError = "Y";
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.errorExit2200);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5515rec.unitType,"I")) {
			if (isNE(sv.acumut,ZERO)) {
				sv.acumutErr.set(h025);
				wsaaNormError = "Y";
				wsspcomn.edterror.set("Y");
			}
		}
		else {
			if (isEQ(t5515rec.unitType,"A")) {
				if (isNE(sv.initut,ZERO)) {
					sv.initutErr.set(h025);
					wsaaNormError = "Y";
					wsspcomn.edterror.set("Y");
				}
			}
			else {
				if (isNE(sv.initut,ZERO)
				&& isEQ(sv.acumut,ZERO)) {
					sv.acumutErr.set(f351);
					wsaaNormError = "Y";
					wsspcomn.edterror.set("Y");
				}
			}
		}
		if (isEQ(wsaaToleranceFlag,"N")) {
			if (isNE(wsaaNormError,"Y")) {
				checkTolerances14000();
			}
		}
	}

protected void errorExit2200()
	{
		if (isNE(wsspcomn.edterror,SPACES)) {
			sv.acumutOut[varcom.pr.toInt()].set(SPACES);
			sv.initutOut[varcom.pr.toInt()].set(SPACES);
		}
		sv.updateDate.set(wsaaTdayIntDate);
		scrnparams.function.set(varcom.supd);
		processScreen("S5413", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		goTo(GotoLabel.validateSubfile2100);
	}

protected void rollUp2500()
	{
		if (!wsaaRolu.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
		if ((isNE(wsspcomn.edterror,varcom.oK))
		&& (isEQ(wsaaNormError,"Y"))) {
			goTo(GotoLabel.exit2090);
		}
		sv.acumutErr.set(SPACES);
		sv.errorSubfile.set(SPACES);
		wsspcomn.edterror.set("N");
		itdmIO.setItemitem(wsaaLastItem);
		itdmIO.setItmfrm(wsaaLastItmfrm);
		itdmIO.setFunction(varcom.readr);
	}

protected void writeToSubfile2600()
	{
		callItdmIo5100();
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign2650);
		}
		if (isGTE(wsaaRem,sv.subfilePage)) {
			wsaaLastItem.set(itdmIO.getItemitem());
			wsaaLastItmfrm.set(itdmIO.getItmfrm());
			wsaaRem.set(0);
			goTo(GotoLabel.setMoreSign2650);
		}
		itdmIO.setFunction(varcom.nextr);
		if (isEQ(t5515rec.zfundtyp,"D")) {
			goTo(GotoLabel.writeToSubfile2600);
		}
		moveFieldsToSubfile6000();
		getDescription6100();
		scrnparams.function.set(varcom.sadd);
		callScreenIo5000();
		wsaaRem.add(1);
		goTo(GotoLabel.writeToSubfile2600);
	}

protected void setMoreSign2650()
	{
		if (isNE(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		wsspcomn.edterror.set("Y");
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateDatabase3100();
				}
				case moreUpdateRec3200: {
					moreUpdateRec3200();
				}
				case exit3900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3100()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void moreUpdateRec3200()
	{
		processScreen("S5413", sv);
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		if (isEQ(sv.acumut,ZERO)
		&& isEQ(sv.initut,ZERO)) {
			goTo(GotoLabel.moreUpdateRec3200);
		}
		writeToVprcFile7000();
		goTo(GotoLabel.moreUpdateRec3200);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callScreenIo5000()
	{
		/*PARA*/
		processScreen("S5413", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callItdmIo5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case call5110: {
					call5110();
				}
				case exit5190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call5110()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemtabl(),"T5515")) {
			itdmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5190);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(itdmIO.getValidflag(),2)) {
			itdmIO.setFunction(varcom.nextr);
			goTo(GotoLabel.call5110);
		}
		if (isEQ(itdmIO.getItemitem(),"*ALL ")) {
			itdmIO.setFunction(varcom.nextr);
			goTo(GotoLabel.call5110);
		}
		if (isEQ(itdmIO.getItemitem(),wsaaPrevItem)
		&& isNE(itdmIO.getFunction(),varcom.readr)) {
			itdmIO.setFunction(varcom.nextr);
			goTo(GotoLabel.call5110);
		}
		wsaaPrevItem.set(itdmIO.getItemitem());
		if (isEQ(wsaaSearchFund,"N")) {
			goTo(GotoLabel.exit5190);
		}
		wsaaItemFund.set(itdmIO.getItemitem());
		if (isNE(wsaaPartF1,wsaaItemF1)) {
			if (isLT(wsaaPartF1,wsaaItemF1)) {
				/*NEXT_SENTENCE*/
			}
			else {
				itdmIO.setFunction(varcom.nextr);
				goTo(GotoLabel.call5110);
			}
		}
		else {
			if (isNE(wsaaPartF2,SPACES)) {
				if (isNE(wsaaPartF2,wsaaItemF2)) {
					if (isLT(wsaaPartF2,wsaaItemF2)) {
						/*NEXT_SENTENCE*/
					}
					else {
						itdmIO.setFunction(varcom.nextr);
						goTo(GotoLabel.call5110);
					}
				}
				else {
					if (isNE(wsaaPartF3,SPACES)) {
						if (isNE(wsaaPartF3,wsaaItemF3)) {
							if (isLT(wsaaPartF3,wsaaItemF3)) {
								/*NEXT_SENTENCE*/
							}
							else {
								itdmIO.setFunction(varcom.nextr);
								goTo(GotoLabel.call5110);
							}
						}
						else {
							if (isNE(wsaaPartF4,SPACES)) {
								if (isNE(wsaaPartF4,wsaaItemF4)) {
									if (isLT(wsaaPartF4,wsaaItemF4)) {
										/*NEXT_SENTENCE*/
									}
									else {
										itdmIO.setFunction(varcom.nextr);
										goTo(GotoLabel.call5110);
									}
								}
							}
						}
					}
				}
			}
		}
		wsaaSearchFund.set("N");
	}

protected void moveFieldsToSubfile6000()
	{
		para6000();
	}

protected void para6000()
	{
		sv.subfileArea.set(SPACES);
		if (isEQ(t5515rec.unitType,"A")
		|| isEQ(t5515rec.unitType,"I")
		|| isEQ(t5515rec.unitType,"B")) {
			if (isEQ(t5515rec.unitType,"A")) {
				sv.initutOut[varcom.pr.toInt()].set("Y");
			}
			if (isEQ(t5515rec.unitType,"I")) {
				sv.acumutOut[varcom.pr.toInt()].set("Y");
			}
			if (isEQ(t5515rec.unitType,"B")) {
				sv.acumutOut[varcom.pr.toInt()].set(SPACES);
				sv.initutOut[varcom.pr.toInt()].set(SPACES);
			}
		}
		else {
			sv.acumutOut[varcom.pr.toInt()].set(SPACES);
			sv.initutOut[varcom.pr.toInt()].set(SPACES);
		}
		sv.unitVirtualFund.set(itdmIO.getItemitem());
		sv.initut.set(ZERO);
		sv.acumut.set(ZERO);
		sv.updateDate.set(wsaaTdayIntDate);
	}

protected void getDescription6100()
	{
		para6100();
	}

protected void para6100()
	{
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsspcomn.company);
		wsaaItemkey.itemItemtabl.set(t5515);
		wsaaItemkey.itemItemitem.set(itdmIO.getItemitem());
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(wsspcomn.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isEQ(getdescrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(getdescrec.statuz);
			fatalError600();
		}
		if (isNE(getdescrec.statuz,varcom.oK)) {
			sv.ffnddsc.set(SPACES);
		}
		else {
			sv.ffnddsc.set(getdescrec.longdesc);
		}
	}

protected void writeToVprcFile7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para7000();
				}
				case acumut7100: {
					acumut7100();
				}
				case exit7900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para7000()
	{
		vprcuprIO.setFunction(varcom.writr);
		vprcuprIO.setFormat(vprcuprrec);
		vprcuprIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcuprIO.setJobno(wsspuprc.uprcJobno);
		vprcuprIO.setEffdate(wsspuprc.uprcEffdate);
		vprcuprIO.setCompany(wsspcomn.company);
		if (isEQ(sv.initut,ZERO)) {
			goTo(GotoLabel.acumut7100);
		}
		vprcuprIO.setUnitType("I");
		vprcuprIO.setUnitBarePrice(sv.initut);
		vprcuprIO.setUnitBidPrice(ZERO);
		vprcuprIO.setUnitOfferPrice(ZERO);
		vprcuprIO.setValidflag("2");
		vprcuprIO.setTranno(1);
		vprcuprIO.setUpdateDate(wsaaTdayIntDate);
		vprcuprIO.setTransactionTime(0);
		SmartFileCode.execute(appVars, vprcuprIO);
		if (isNE(vprcuprIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcuprIO.getStatuz());
			fatalError600();
		}
		if (isEQ(sv.acumut,ZERO)) {
			goTo(GotoLabel.exit7900);
		}
	}

protected void acumut7100()
	{
		vprcuprIO.setUnitType("A");
		vprcuprIO.setUnitBarePrice(sv.acumut);
		vprcuprIO.setUnitBidPrice(ZERO);
		vprcuprIO.setUnitOfferPrice(ZERO);
		vprcuprIO.setValidflag("2");
		vprcuprIO.setTranno(1);
		vprcuprIO.setUpdateDate(wsaaTdayIntDate);
		vprcuprIO.setTransactionTime(0);
		SmartFileCode.execute(appVars, vprcuprIO);
		if (isNE(vprcuprIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcuprIO.getStatuz());
			fatalError600();
		}
	}

protected void checkDates8000()
	{
		try {
			chkDts8100();
		}
		catch (GOTOException e){
		}
	}

protected void chkDts8100()
	{
		if (isEQ(sv.acumut,ZERO)
		&& isEQ(sv.initut,ZERO)) {
			wsaaDateRecFound = "Y";
			goTo(GotoLabel.exit8900);
		}
		if ((isGTE(wsspuprc.uprcEffdate,itdmIO.getItmfrm()))
		&& (isLTE(wsspuprc.uprcEffdate,itdmIO.getItmto()))) {
			wsaaDateRecFound = "Y";
			goTo(GotoLabel.exit8900);
		}
		itdmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(sv.unitVirtualFund,itdmIO.getItemitem())) {
			itdmIO.setStatuz("ENDP");
		}
	}

protected void checkTolerances14000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start14100();
					checkAccumut14100();
				}
				case begnInitut14150: {
					begnInitut14150();
				}
				case checkInitut14200: {
					checkInitut14200();
				}
				case exit14900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start14100()
	{
		if (isEQ(t5515rec.tolerance,0)) {
			goTo(GotoLabel.exit14900);
		}
		vprcupeIO.setCompany(wsspcomn.company);
		vprcupeIO.setValidflag("1");
		vprcupeIO.setEffdate(varcom.vrcmMaxDate);
		vprcupeIO.setJobno(99999999);
		vprcupeIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupeIO.setUnitType("A");
		vprcupeIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.checkInitut14200);
		}
	}

protected void checkAccumut14100()
	{
		if (isEQ(t5515rec.unitType,"I")) {
			goTo(GotoLabel.begnInitut14150);
		}
		if (isEQ(sv.acumut,0)) {
			goTo(GotoLabel.begnInitut14150);
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBarePrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.acumut, 5)
		&& isGT(sv.acumut,(add(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))
		|| (setPrecision(sv.acumut, 5)
		&& isLT(sv.acumut,(sub(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))) {
			sv.acumutErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
	}

protected void begnInitut14150()
	{
		if (isEQ(t5515rec.unitType,"A")) {
			goTo(GotoLabel.exit14900);
		}
		if (isEQ(sv.initut,0)) {
			goTo(GotoLabel.exit14900);
		}
		vprcupeIO.setUnitType("I");
		vprcupeIO.setFunction(varcom.begn);
	}

protected void checkInitut14200()
	{
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit14900);
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBarePrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.initut, 5)
		&& isGT(sv.initut,(add(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))
		|| (setPrecision(sv.initut, 5)
		&& isLT(sv.initut,(sub(vprcupeIO.getUnitBarePrice(),wsaaTolerance))))) {
			sv.initutErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
	}
}
