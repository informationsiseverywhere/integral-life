package com.csc.life.unitlinkedprocessing.batchservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.impl.BabrpfDAOImpl;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.integral.batch.model.FileFormat;
import com.csc.integral.batch.model.FileProcessingRequest;
import com.csc.integral.batch.service.FileProcessor;
import com.csc.integral.batch.service.FileService;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Descpf;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

public class BNKFileService extends FileService {
	private String[] headers = {
			"BSB Code",
			"Bank Description",
			"Branch Description",
			"Address",
			"District / City",
			"State",
			"Postal Code",
			"Country Code",
			"Factoring House1",
			"Factoring House2",
			"Currencies",
			"Auto Prem Payment Applicable"
			};
	private String headerError = "";
	private List<ValidatedData> validatedDataList;
	private DescDAO descdao =new DescDAOImpl();
	private BabrpfDAO babrpfDAO = new BabrpfDAOImpl();
	private String digitsOnly = "[0-9]+";
	
	@Override
	public FileProcessor getFileParser(FileProcessingRequest request) {
		String formatString = request.getUploadedFilePath().substring(request.getUploadedFilePath().lastIndexOf(".") + 1);
		FileFormat fileFormat = FileFormat.valueOf(formatString == null ? "" :formatString.trim());
		FileProcessor fileProcessor = null;
		switch (fileFormat) {
		case csv:
			fileProcessor = new CSVFileProcessor(request.getUploadedFilePath());
			break;
		case txt: 
			// TODO Text and other File Formats can be support by creating FileProcessor and configuring here
			fileProcessor = null;
			break;
		default:
			break;
		}
		return fileProcessor;
	}
	
	@Override
	public List<Map<String, String>>  updateDatabase() {
		List<Babrpf> babrpfListinsert = new ArrayList<>();
		List<Babrpf> babrpfListupdate = new ArrayList<>();
		for(ValidatedData data : validatedDataList){
			if(data.errorMessage.equalsIgnoreCase("") && data.daoObj != null){
				Babrpf babrpf=babrpfDAO.searchBankkey(data.daoObj.getBankkey());
				if(babrpf==null)
				babrpfListinsert.add(data.daoObj);
				else {
					if((isNE(babrpf.getBankdesc().trim(),data.daoObj.getBankdesc()==null?"":data.daoObj.getBankdesc().trim()))
							||(isNE(babrpf.getBankAddr01().trim(),data.daoObj.getBankAddr01()==null?"":data.daoObj.getBankAddr01().trim()))
							||(babrpf.getBankAddr02()==null
							||isNE(babrpf.getBankAddr02().trim(),data.daoObj.getBankAddr02()==null?"":data.daoObj.getBankAddr02().trim()))
							||(babrpf.getBankAddr03()==null
							||isNE(babrpf.getBankAddr03().trim(),data.daoObj.getBankAddr03()==null?"":data.daoObj.getBankAddr03().trim()))
							||(babrpf.getBankAddr04()==null
							||isNE(babrpf.getBankAddr04().trim(),data.daoObj.getBankAddr04()==null?"":data.daoObj.getBankAddr04().trim()))
							||(babrpf.getBankAddr05()==null
							||isNE(babrpf.getBankAddr05().trim(),data.daoObj.getBankAddr05()==null?"":data.daoObj.getBankAddr05().trim()))
							||(babrpf.getBankAddr03()==null
							||isNE(babrpf.getBankPostcode().trim(),data.daoObj.getBankPostcode()==null?"":data.daoObj.getBankPostcode().trim()))
							||(babrpf.getCtrycode()==null
							||isNE(babrpf.getCtrycode().trim(),data.daoObj.getCtrycode()==null?"":data.daoObj.getCtrycode().trim()))
							||(babrpf.getFacthous01()==null
							||isNE(babrpf.getFacthous01().trim(),data.daoObj.getFacthous01()==null?"":data.daoObj.getFacthous01().trim()))
							||(babrpf.getFacthous02()==null
							||isNE(babrpf.getFacthous02().trim(),data.daoObj.getFacthous02()==null?"":data.daoObj.getFacthous02().trim()))
							||(babrpf.getCurrcode01()==null
							||isNE(babrpf.getCurrcode01().trim(),data.daoObj.getCurrcode01()==null?"":data.daoObj.getCurrcode01().trim()))
							||(babrpf.getAppflag()==null
							||isNE(babrpf.getAppflag().trim(),data.daoObj.getAppflag()==null?"":data.daoObj.getAppflag().trim()))){
						babrpfListupdate.add(data.daoObj);
				}
					
				}
			}
		}
		List<Integer> insertResult = babrpfDAO.insert(babrpfListinsert);
		List<Integer> updateResult = babrpfDAO.update(babrpfListupdate);
		updateInsertResult(babrpfListinsert, babrpfListupdate, insertResult,updateResult);
		return getUpdatedDataForOutput();
	}
	private void updateInsertResult(List<Babrpf> babrpfInsertList,List<Babrpf> babrpfUpdateList, List<Integer> insertResult, List<Integer> updateResult){
		for(int index = 0; index < insertResult.size(); index++){
			if(insertResult.get(index) < 0){
				for(ValidatedData data : validatedDataList){
					if(data.daoObj != null && data.daoObj.equals(babrpfInsertList.get(index))){
						data.errorMessage = "Error";
						break;
					}
				}
			}else{
				for(ValidatedData data : validatedDataList){
					if(data.daoObj != null && data.daoObj.equals(babrpfInsertList.get(index))){
						data.dbInsertStatus = true;
						break;
					}
				}
			}
		}
		for(int index = 0; index < updateResult.size(); index++){
			if(updateResult.get(index) < 0){
				for(ValidatedData data : validatedDataList){
					if(data.daoObj != null && data.daoObj.equals(babrpfUpdateList.get(index))){
						data.errorMessage = "Error";
						break;
					}
				}
			}else{
				for(ValidatedData data : validatedDataList){
					if(data.daoObj != null && data.daoObj.equals(babrpfUpdateList.get(index))){
						data.dbUpdateStatus = true;
						break;
					}
				}
			}
		}
	}
	
	
	
	
	private boolean validateHeaders(List<String> headers){
		boolean isValidHeaders = true;
		List<String> headersInFile = headers;
		if(headersInFile == null || headersInFile.size() <= 0){
			headerError = "Header not available in the file";
			isValidHeaders = false;
		}else{
			for(String header : headers){
				if(!headersInFile.contains(header)){
					isValidHeaders = false;
					headerError = headerError  + header +" "; 
				}
			}
			if(!isValidHeaders){
				headerError = "Header(s) " + headerError + "not available";
			}
		}
		return isValidHeaders;
	}
	
	@Override
	protected boolean validateData(List<String> headers, List<Map<String,String>> fileData) {
		if(validateHeaders(headers)){
			validatedDataList = new ArrayList<>();
			int recordSize = fileData.size();
			for(int recordIndex = 0; recordIndex < recordSize ; recordIndex++){
				String errorMsg = "";
				Babrpf babrpf = new Babrpf();
				for(String header : headers){
					switch (header) {
					case "BSB Code":
						String BSBCode = fileData.get(recordIndex).get(header);
						ValidatedData validatedData = null;
						if(BSBCode == null || BSBCode.trim().equalsIgnoreCase("")||(validatedData = getBSBCodeData(BSBCode)) != null){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setBankkey(BSBCode);
						}
						break;
						
					case "Bank Description":
						String bnkDesc = fileData.get(recordIndex).get(header);
						if(bnkDesc == null || bnkDesc.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Error";
						}else {
							
							babrpf.setBankdesc(String.format("%-30s", bnkDesc));
						}
						break;
						
					case "Branch Description":
						String brnch = fileData.get(recordIndex).get(header);
						
						if(brnch == null || brnch.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setBankdesc(babrpf.getBankdesc()+brnch);
						}
						break;
						
					case "Address":
						String addr = fileData.get(recordIndex).get(header);
						if(addr == null || addr.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Error";
						}else{
						    babrpf.setBankAddr02(" ");
							babrpf.setBankAddr03(" ");
							if(addr.length()>60){
								babrpf.setBankAddr01(addr.substring(0, 30));
								babrpf.setBankAddr02(addr.substring(30, 60));
								babrpf.setBankAddr03(addr.substring(60, addr.length()));
							}
							else if(addr.length()>30){
								babrpf.setBankAddr01(addr.substring(0, 30));
								babrpf.setBankAddr02(addr.substring(30, addr.length()));
							}
							else
								babrpf.setBankAddr01(addr);
						}
						break;
						
					case "District / City":
						String dist = fileData.get(recordIndex).get(header);
						if(dist == null || dist.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setBankAddr04(dist);
						}
						break;
					case "State":
						String stat = fileData.get(recordIndex).get(header);
						if(stat == null || stat.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setBankAddr05(stat);
						}
						break;
					case "Postal Code":
						String pcode = fileData.get(recordIndex).get(header);
						if(pcode == null || pcode.trim().equalsIgnoreCase("") || !pcode.trim().matches(digitsOnly)){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setBankPostcode(pcode);
						}
						break;
					case "Country Code":
						String ccode = fileData.get(recordIndex).get(header);
						Descpf descpf1= descdao.getitemByDesc("IT", "T3645", "9", ccode.trim());
						if(ccode == null || ccode.trim().equalsIgnoreCase("")||descpf1==null){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setCtrycode(descpf1.getDescitem().trim());
						}
						break;
					case "Factoring House1":
						String facth1 = fileData.get(recordIndex).get(header);
						Descpf descpf2= descdao.getitemByDesc("IT", "T3684", "9", facth1.trim());
						if(facth1 == null || facth1.trim().equalsIgnoreCase("")||descpf2==null){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setFacthous01(descpf2.getDescitem().trim());
						}
						break;
					case "Factoring House2":
						String facth2 = fileData.get(recordIndex).get(header);
						babrpf.setFacthous02(" ");
						if(facth2 != null && isNE(facth2.trim(),"")){
						Descpf descpf3= descdao.getitemByDesc("IT", "T3684", "9", facth2.trim());
						if(descpf3==null){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setFacthous02(descpf3.getDescitem().trim());
						}
						}
						break;
					case "Currencies":
						String curr = fileData.get(recordIndex).get(header);
						Descpf descpf4= descdao.getitemByDesc("IT", "T3629", "9", curr.trim());
						if(curr == null || curr.trim().equalsIgnoreCase("")||descpf4==null){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setCurrcode01(descpf4.getDescitem().trim());
						}
						break;
					case "Auto Prem Payment Applicable":
						String appa = fileData.get(recordIndex).get(header);
						if(appa == null || appa.trim().equalsIgnoreCase("")){
							errorMsg = errorMsg + "Error";
						}else{
							babrpf.setAppflag(appa.substring(0, 1));
						}
						break;
						
					default:
						break;
					}
				}
				
				if(errorMsg.equalsIgnoreCase("")){
					
					validatedDataList.add(new ValidatedData(fileData.get(recordIndex), errorMsg, babrpf));
				}else{
					validatedDataList.add(new ValidatedData(fileData.get(recordIndex), errorMsg, null));
				}
			}
			return true;
		}else{
			return false;
		}
	}
	private List<Map<String, String>> getUpdatedDataForOutput(){
		List<Map<String, String>> outputData = new ArrayList<>();
		for(ValidatedData data : validatedDataList){
			Map<String,String> outputValues = data.recFromFile;
			if(data.errorMessage.equalsIgnoreCase("") && data.dbInsertStatus){
				outputValues.put("Status", "Add");
			}
			else if(data.errorMessage.equalsIgnoreCase("") && data.dbUpdateStatus){
				outputValues.put("Status", "Modify");
			}
			else if(!data.errorMessage.equalsIgnoreCase("")){
				outputValues.put("Status", "Error");
			}
			else
				outputValues.put("Status", "");
			outputData.add(outputValues);
		}
		return outputData;
	}
	
	
	@Override
	public String[] getHeaders() {
		return Arrays.copyOf(headers, headers.length);//IJTI-316
	}
	private ValidatedData getBSBCodeData(String BSBCode){
		for(ValidatedData validatedData : validatedDataList){
			if(validatedData.daoObj != null && validatedData.daoObj.getBankkey() != null &&
					validatedData.daoObj.getBankkey().equalsIgnoreCase(BSBCode)){
				return validatedData;
			}
		}
		return null;
	}
	
	@Override
	protected String getDataValidationError() {
		return headerError;
	}
private class ValidatedData{
		
		private Map<String, String> recFromFile;
		
		private String errorMessage;
		
		private Babrpf daoObj;
		
		private boolean dbInsertStatus = false;
		private boolean dbUpdateStatus = false;
		
		private ValidatedData(Map<String, String> recFromFile, String errorMessage, Babrpf daoObj){
			this.recFromFile = recFromFile;
			this.errorMessage = errorMessage;
			this.daoObj = daoObj;
		}
	}
}