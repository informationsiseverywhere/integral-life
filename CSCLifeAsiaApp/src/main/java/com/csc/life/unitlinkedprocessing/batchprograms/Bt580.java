package com.csc.life.unitlinkedprocessing.batchprograms;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.FileSystems;

import org.apache.commons.io.FilenameUtils;

import com.csc.integral.batch.model.FileProcessingRequest;
import com.csc.integral.batch.model.FileProcessingResponse;
import com.csc.integral.batch.model.FileResponseStatus;
import com.csc.integral.batch.service.FileService;
import com.csc.life.unitlinkedprocessing.batchservice.NAVFileService;
import com.csc.life.unitlinkedprocessing.batchservice.BNKFileService;
import com.csc.life.unitlinkedprocessing.recordstructures.Pt582par;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

public class Bt580  extends Mainb {
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BT580");
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private Pt582par pt582par = new Pt582par();
	private FileService fileService = null;
	private FileProcessingRequest request = null;
	private FileProcessingResponse response = null;

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	@Override
	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	@Override
	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	@Override
	protected void restart0900() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initialise1000() {
		if(isEQ(bupaIO.getScheduleName().toString(),"L2NAVUPLD "))
			fileService = new NAVFileService();
		else if(isEQ(bupaIO.getScheduleName().toString(),"L2BNKUPLD "))
			fileService = new BNKFileService();
			
		String navDirPath = bupaIO.getParmarea().toString().trim(); //IJTI-463
		if(navDirPath != null){
			File outPutFileDir = FileSystems.getDefault().getPath(navDirPath).toFile();//IJTI-593
			File outPutUploadDir = FileSystems.getDefault().getPath(navDirPath+ File.separator + "Upload").toFile();//IJTI-593
			File outPutDownloadDir = FileSystems.getDefault().getPath(navDirPath+ File.separator + "Download").toFile();//IJTI-593
			if(outPutFileDir.exists() && outPutFileDir.isDirectory()
					&& outPutUploadDir.exists() && outPutUploadDir.isDirectory()){//IJTI-593
				File[] navCSVFiles = outPutUploadDir.listFiles(new FileFilter() {//IJTI-593
					@Override
					public boolean accept(File pathname) {
						String format = pathname.getAbsolutePath().substring(pathname.getAbsolutePath().lastIndexOf(".") + 1);
						if(format != null && !format.trim().equalsIgnoreCase("") && format.equalsIgnoreCase("csv")){
							return true;
						}else{
							return false;
						}
					}
				});
				if(!outPutDownloadDir.exists()){//IJTI-593
					outPutDownloadDir.mkdirs();//IJTI-593
				}
				for(File navCSVFile : navCSVFiles){
					request = new FileProcessingRequest();
					request.setUploadedFilePath(navCSVFile.getAbsolutePath());
					request.setDownloadFilePath(outPutDownloadDir.getAbsolutePath()//IJTI-593
							+ File.separator + FilenameUtils.getName(navCSVFile.getName()));//IJTI-310
					response = fileService.processFile(request);
					if(response.getStatus() == FileResponseStatus.SUCCESS){
						if(!navCSVFile.delete()){
							fatalError600();
						}
					}else{
						fatalError600();
					}
				}
			}else{
				fatalError600();
			}
		}else{
			fatalError600();
		}

	}

	@Override
	protected void readFile2000() {
		
		wsspEdterror.set(varcom.endp);
		
	}

	@Override
	protected void edit2500() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void update3000() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void commit3500() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void close4000() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void rollback3600() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	@Override
	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	@Override
	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	@Override
	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	@Override
	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
		
	}

	@Override
	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	@Override
	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}
	
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

}
