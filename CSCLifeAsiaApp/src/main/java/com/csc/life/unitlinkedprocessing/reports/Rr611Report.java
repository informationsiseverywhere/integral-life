package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.common.DD;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR611.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr611Report extends SMARTReportLayout { 

	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr5 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData asgname1 = new FixedLengthStringData(30);
	private FixedLengthStringData asgname2 = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cltphone = new FixedLengthStringData(16);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData ctypedes = new FixedLengthStringData(30);
	private FixedLengthStringData despname1 = new FixedLengthStringData(47);
	private FixedLengthStringData despname2 = new FixedLengthStringData(47);
	private FixedLengthStringData moniesdt = new FixedLengthStringData(10);
	private FixedLengthStringData name1 = new FixedLengthStringData(30);
	private FixedLengthStringData name2 = new FixedLengthStringData(30);
	private ZonedDecimalData pagenum = new ZonedDecimalData(4, 0);
	private FixedLengthStringData stmtdate = new FixedLengthStringData(10);
	private FixedLengthStringData stopdt = new FixedLengthStringData(10);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);
	private FixedLengthStringData textfield = new FixedLengthStringData(13);
	private ZonedDecimalData ubidpr = new ZonedDecimalData(9, 5);
	private ZonedDecimalData ubidpr02 = new ZonedDecimalData(9, 5);
	private ZonedDecimalData uoffpr = new ZonedDecimalData(9, 5);
	private ZonedDecimalData uoffpr02 = new ZonedDecimalData(9, 5);
	private FixedLengthStringData vrtfnddsc = new FixedLengthStringData(30);
	private ZonedDecimalData zrbonprd1 = new ZonedDecimalData(9, 3);
	private ZonedDecimalData zrbonprd2 = new ZonedDecimalData(9, 3);
	private ZonedDecimalData zrordpay = new ZonedDecimalData(9, 2);
	private ZonedDecimalData zrtotnun = new ZonedDecimalData(14, 3);
	private ZonedDecimalData zrtotvlu = new ZonedDecimalData(13, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr611Report() {
		super();
	}


	/**
	 * Print the XML for Rr611d01
	 */
	public void printRr611d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		moniesdt.setFieldName("moniesdt");
		moniesdt.setInternal(subString(recordData, 1, 10));
		zrordpay.setFieldName("zrordpay");
		zrordpay.setInternal(subString(recordData, 11, 9));
		textfield.setFieldName("textfield");
		textfield.setInternal(subString(recordData, 20, 13));
		uoffpr.setFieldName("uoffpr");
		uoffpr.setInternal(subString(recordData, 33, 9));
		ubidpr.setFieldName("ubidpr");
		ubidpr.setInternal(subString(recordData, 42, 9));
		zrbonprd1.setFieldName("zrbonprd1");
		zrbonprd1.setInternal(subString(recordData, 51, 9));
		zrbonprd2.setFieldName("zrbonprd2");
		zrbonprd2.setInternal(subString(recordData, 60, 9));
		printLayout("Rr611d01",			// Record name
			new BaseData[]{			// Fields:
				moniesdt,
				zrordpay,
				textfield,
				uoffpr,
				ubidpr,
				zrbonprd1,
				zrbonprd2
			}
		);

	}

	/**
	 * Print the XML for Rr611h01
	 */
	public void printRr611h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(1).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stmtdate.setFieldName("stmtdate");
		stmtdate.setInternal(subString(recordData, 1, 10));
		pagenum.setFieldName("pagenum");
		pagenum.setInternal(subString(recordData, 11, 4));
		asgname1.setFieldName("asgname1");
		asgname1.setInternal(subString(recordData, 15, 30));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 45, 8));
		asgname2.setFieldName("asgname2");
		asgname2.setInternal(subString(recordData, 53, 30));
		name1.setFieldName("name1");
		name1.setInternal(subString(recordData, 83, 30));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 113, 3));
		name2.setFieldName("name2");
		name2.setInternal(subString(recordData, 116, 30));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 146, 17));
		printLayout("Rr611h01",			// Record name
			new BaseData[]{			// Fields:
				stmtdate,
				pagenum,
				asgname1,
				chdrnum,
				asgname2,
				name1,
				cntcurr,
				name2,
				sumins
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)}
			}
		);

		currentPrintLine.set(16);
	}

	/**
	 * Print the XML for Rr611h02
	 */
	public void printRr611h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		addr1.setFieldName("addr1");
		addr1.setInternal(subString(recordData, 1, DD.cltaddr.length));/*pmujavadiya ILIFE-3212*/
		addr2.setFieldName("addr2");
		addr2.setInternal(subString(recordData, 1+DD.cltaddr.length, DD.cltaddr.length));
		addr3.setFieldName("addr3");
		addr3.setInternal(subString(recordData, 1+DD.cltaddr.length*2, DD.cltaddr.length));
		addr4.setFieldName("addr4");
		addr4.setInternal(subString(recordData, 1+DD.cltaddr.length*3, DD.cltaddr.length));
		addr5.setFieldName("addr5");
		addr5.setInternal(subString(recordData, 1+DD.cltaddr.length*4, DD.cltaddr.length));
		ctypedes.setFieldName("ctypedes");
		ctypedes.setInternal(subString(recordData, 1+DD.cltaddr.length*5, 30));/*pmujavadiya ILIFE-3212*/
		printLayout("Rr611h02",			// Record name
			new BaseData[]{			// Fields:
				addr1,
				addr2,
				addr3,
				addr4,
				addr5,
				ctypedes
			}
		);

		currentPrintLine.add(10);
	}

	/**
	 * Print the XML for Rr611h03
	 */
	public void printRr611h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		vrtfnddsc.setFieldName("vrtfnddsc");
		vrtfnddsc.setInternal(subString(recordData, 1, 30));
		printLayout("Rr611h03",			// Record name
			new BaseData[]{			// Fields:
				vrtfnddsc
			}
		);

	}

	/**
	 * Print the XML for Rr611h04
	 */
	public void printRr611h04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stopdt.setFieldName("stopdt");
		stopdt.setInternal(subString(recordData, 1, 10));
		zrtotnun.setFieldName("zrtotnun");
		zrtotnun.setInternal(subString(recordData, 11, 14));
		printLayout("Rr611h04",			// Record name
			new BaseData[]{			// Fields:
				stopdt,
				zrtotnun
			}
		);

	}

	/**
	 * Print the XML for Rr611h05
	 */
	public void printRr611h05(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stopdt.setFieldName("stopdt");
		stopdt.setInternal(subString(recordData, 1, 10));
		uoffpr02.setFieldName("uoffpr02");
		uoffpr02.setInternal(subString(recordData, 11, 9));
		ubidpr02.setFieldName("ubidpr02");
		ubidpr02.setInternal(subString(recordData, 20, 9));
		zrtotnun.setFieldName("zrtotnun");
		zrtotnun.setInternal(subString(recordData, 29, 14));
		printLayout("Rr611h05",			// Record name
			new BaseData[]{			// Fields:
				stopdt,
				uoffpr02,
				ubidpr02,
				zrtotnun
			}
		);

	}

	/**
	 * Print the XML for Rr611s01
	 */
	public void printRr611s01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stmtdate.setFieldName("stmtdate");
		stmtdate.setInternal(subString(recordData, 1, 10));
		zrtotvlu.setFieldName("zrtotvlu");
		zrtotvlu.setInternal(subString(recordData, 11, 13));
		printLayout("Rr611s01",			// Record name
			new BaseData[]{			// Fields:
				stmtdate,
				zrtotvlu
			}
		);

		currentPrintLine.set(45);
	}

	/**
	 * Print the XML for Rr611s02
	 */
	public void printRr611s02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		despname1.setFieldName("despname1");
		despname1.setInternal(subString(recordData, 1, 47));
		cltphone.setFieldName("cltphone");
		cltphone.setInternal(subString(recordData, 48, 16));
		despname2.setFieldName("despname2");
		despname2.setInternal(subString(recordData, 64, 47));
		printLayout("Rr611s02",			// Record name
			new BaseData[]{			// Fields:
				despname1,
				cltphone,
				despname2
			}
		);

		currentPrintLine.set(59);
	}

	/**
	 * Print the XML for Rr611s03
	 */
	public void printRr611s03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rr611s03",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}


}
