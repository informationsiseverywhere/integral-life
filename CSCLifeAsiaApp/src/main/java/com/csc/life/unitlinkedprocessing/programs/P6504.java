/*
 * File: P6504.java
 * Date: 30 August 2009 0:46:17
 * Author: Quipoz Limited
 * 
 * Class transformed from P6504.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.recordstructures.Ulparams;
import com.csc.life.unitlinkedprocessing.screens.S6504ScreenVars;
import com.csc.smart.procedures.Gettim;
import com.csc.smart.procedures.Jctlem;
import com.csc.smart.procedures.Jobparm;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Jctlrec;
import com.csc.smart.recordstructures.Jobparmrec;
import com.csc.smart.recordstructures.Jobsrec;
import com.csc.smart.recordstructures.Subjobrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.batch.cls.Subjob;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*         UNIT STATEMENT PROCESSING PARAMETERS
*
* The processing date is the only parameter required for the Unit
* Statement Print Request. This must be a valid date and will be used
* to select Trigger Records on or before that date.
*
*
*****************************************************************
* </pre>
*/
public class P6504 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6504");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).setUnsigned();
		/* ERRORS */
	private String e215 = "E215";
	private String jctlrec = "JCTLREC";
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Jctlrec jctlrec1 = new Jctlrec();
	private Jobparmrec jobparmrec = new Jobparmrec();
	private Subjobrec subjobrec = new Subjobrec();
	private Ulparams ulparams = new Ulparams();
	private Batckey wsaaBatckey = new Batckey();
	private Jobsrec wsaaJobskey = new Jobsrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6504ScreenVars sv = ScreenProgram.getScreenVars( S6504ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit
	}

	public P6504() {
		super();
		screenVars = sv;
		new ScreenModel("S6504", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaJobskey.set(wsspsmart.jobskey);
		sv.effdate.set(wsspsmart.effdate);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.acctmonth.set(wsaaBatckey.batcBatcactmn);
		sv.acctyear.set(wsaaBatckey.batcBatcactyr);
		sv.jobname.set(wsaaJobskey.jobsJctljn);
		jobparmrec.company.set(wsspcomn.company);
		jobparmrec.jobname.set(wsaaJobskey.jobsJctljn);
		jobparmrec.tranid.set(wsspcomn.tranid);
		jobparmrec.function.set("READ");
		callProgram(Jobparm.class, jobparmrec.jobparmRec);
		if (isNE(jobparmrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(jobparmrec.statuz);
			fatalError600();
		}
		sv.runid.set(jobparmrec.jobcRunid);
		sv.jobq.set(jobparmrec.jobcJobq);
		sv.runlib.set(jobparmrec.jobcRunlib);
		sv.stmEffdte.set(varcom.vrcmMaxDate);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*LOAD-FIELDS*/
		getNextJob5100();
		moveParameters5200();
		initialiseJctl5300();
		submitJob5400();
		appVars.commit();
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void getNextJob5100()
	{
		/*NEXT-JOB-NO*/
		jobparmrec.company.set(wsaaJobskey.jobsJctlcoy);
		jobparmrec.jobname.set(wsaaJobskey.jobsJctljn);
		jobparmrec.tranid.set(wsspcomn.tranid);
		jobparmrec.function.set("UPDAT");
		callProgram(Jobparm.class, jobparmrec.jobparmRec);
		if (isNE(jobparmrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(jobparmrec.statuz);
			fatalError600();
		}
		wsaaJobskey.jobsJctljnum.set(jobparmrec.jobcJobno);
		/*EXIT*/
	}

protected void moveParameters5200()
	{
		/*MOVE-TO-PARM-RECORD*/
		ulparams.parmRecord.set(SPACES);
		ulparams.stmEffdate.set(sv.stmEffdte);
		ulparams.stmJobno.set(jobparmrec.jobcJobno);
		ulparams.stmType.set("P");
		/*EXIT*/
	}

protected void initialiseJctl5300()
	{
		initJctl5310();
	}

protected void initJctl5310()
	{
		jctlrec1.dataArea.set(SPACES);
		jctlrec1.jobsts.set("W");
		jctlrec1.jctlpfx.set("JC");
		jctlrec1.jctlcoy.set(wsaaJobskey.jobsJctlcoy);
		jctlrec1.jctljn.set(wsaaJobskey.jobsJctljn);
		jctlrec1.jctlacyr.set(wsaaJobskey.jobsJctlacyr);
		jctlrec1.jctlacmn.set(wsaaJobskey.jobsJctlacmn);
		jctlrec1.jctljnum.set(wsaaJobskey.jobsJctljnum);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		jctlrec1.tranid.set(varcom.vrcmCompTranid);
		jctlrec1.s38jobno.set(0);
		jctlrec1.user.set(varcom.vrcmUser);
		jctlrec1.company.set(wsaaJobskey.jobsJctlcoy);
		jctlrec1.language.set(wsspcomn.language);
		wsaaBatckey.set(wsspcomn.batchkey);
		jctlrec1.branch.set(wsaaBatckey.batcBatcbrn);
		jctlrec1.batcbranch.set(wsaaBatckey.batcBatcbrn);
		jctlrec1.acctyear.set(wsaaJobskey.jobsJctlacyr);
		jctlrec1.acctmonth.set(wsaaJobskey.jobsJctlacmn);
		jctlrec1.jobd.set(jobparmrec.jobcJobd);
		jctlrec1.jobq.set(jobparmrec.jobcJobq);
		jctlrec1.runid.set(jobparmrec.jobcRunid);
		jctlrec1.runlib.set(jobparmrec.jobcRunlib);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		jctlrec1.datesub.set(datcon1rec.intDate);
		callProgram(Gettim.class, wsaaTime);
		jctlrec1.timesub.set(wsaaTime);
		jctlrec1.effdate.set(wsspsmart.effdate);
		jctlrec1.datestart.set(0);
		jctlrec1.timestart.set(0);
		jctlrec1.jobstep.set("000");
		jctlrec1.dateend.set(0);
		jctlrec1.timeend.set(0);
		jctlrec1.eltime.set(0);
		jctlrec1.pstdate.set(0);
		jctlrec1.psttime.set(0);
		jctlrec1.pnddate.set(0);
		jctlrec1.pndtime.set(0);
		jctlrec1.parmarea.set(ulparams.parmRecord);
	}

protected void submitJob5400()
	{
		submitJob5410();
		writeJctlRecord5420();
	}

protected void submitJob5410()
	{
		subjobrec.company.set(jctlrec1.jctlcoy);
		subjobrec.jobname.set(jctlrec1.jctljn);
		subjobrec.acctyear.set(jctlrec1.jctlacyr);
		subjobrec.acctmonth.set(jctlrec1.jctlacmn);
		subjobrec.jobnum.set(jctlrec1.jctljnum);
		subjobrec.jobd.set(jobparmrec.jobcJobd);
		subjobrec.jobq.set(jobparmrec.jobcJobq);
		callProgram(Subjob.class, subjobrec.subjobRec);
		if (isNE(subjobrec.statuz,varcom.oK)) {
			subjobrec.statuz.set(e215);
			fatalError600();
		}
		jctlrec1.s38jobno.set(subjobrec.jobnum);
		jctlrec1.s38user.set(subjobrec.user);
		jctlrec1.function.set(varcom.writr);
		jctlrec1.format.set(jctlrec);
	}

protected void writeJctlRecord5420()
	{
		callProgram(Jctlem.class, jctlrec1.params);
		if (isNE(jctlrec1.statuz,varcom.oK)) {
			syserrrec.params.set(jctlrec1.params);
			fatalError600();
		}
		/*EXIT*/
	}
}
