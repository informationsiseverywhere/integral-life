package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UreppfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ureppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UreppfDAOImpl extends BaseDAOImpl<Ureppf> implements UreppfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UreppfDAOImpl.class);

	public void updateUreppfRecord(List<Ureppf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		String sql = "UPDATE UREPPF SET PROCFLG=?,JOBNM=?,USRPRF=?,DATIME=? WHERE CHDRNUM=? AND CHDRCOY=? AND SEQNUM=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		
		try {
			for (Ureppf u : urList) {
				int i = 1;
				ps.setString(i++, u.getProcflg());
				ps.setString(i++, getJobnm());
				ps.setString(i++, getUsrprf());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getChdrcoy());
				ps.setInt(i++, u.getSeqnum());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateUreppfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public Map<String, List<Ureppf>> searchSeqByChdrnum(List<String> chdrnumList){
		
		if(chdrnumList == null || chdrnumList.isEmpty()){
			return null;
		}
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT CHDRNUM,SEQNUM,CHDRCOY ");
	    sqlSelect1.append(" FROM UREPPF WHERE ");
	    sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, SEQNUM DESC, UNIQUE_NUMBER DESC ");
	    
	    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
	    ResultSet sqlrs = null;
	    Map<String, List<Ureppf>> urepfMap = new HashMap<>();
	    
	    try {
	    	sqlrs = executeQuery(psSelect);
	
	        while (sqlrs.next()) {
	        	Ureppf u = new Ureppf();
	        	u.setChdrnum(sqlrs.getString("CHDRNUM"));
	        	u.setChdrcoy(sqlrs.getString("CHDRCOY"));
	        	u.setSeqnum(sqlrs.getInt("SEQNUM"));
	            
				if (urepfMap.containsKey(u.getChdrnum())) {
					urepfMap.get(u.getChdrnum()).add(u);
				} else {
					List<Ureppf> ureppfList = new ArrayList<>();
					ureppfList.add(u);
					urepfMap.put(u.getChdrnum(), ureppfList);
				}
	        }
	    } catch (SQLException e) {
	        LOGGER.error("searchSeqByChdrnum()", e);//IJTI-1561
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(psSelect, sqlrs);
	    }
		return urepfMap;
	}
	
	public void insertUreppfRecord(List<Ureppf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO UREPPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,TRANNO,VRTFND,NOFUNT,UNITYP,BATCTRCDE,PRICEDT,MONIESDT,PRICEUSED,NOFDUNT,FNDCURR,FUNDAMNT,BSCHEDNAM,BSCHEDNUM,SEQNUM,PROCFLG,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		
		try {
			for (Ureppf u : urList) {
				int i = 1;
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getLife());
				ps.setString(i++, u.getCoverage());
				ps.setString(i++, u.getRider());
				ps.setInt(i++, u.getPlanSuffix());
				ps.setInt(i++, u.getTranno());
				ps.setString(i++, u.getUnitVirtualFund());
				ps.setBigDecimal(i++, u.getNofUnits());
				ps.setString(i++, u.getUnitType());
				ps.setString(i++, u.getBatctrcde());
				ps.setInt(i++, u.getPriceDateUsed());
				ps.setInt(i++, u.getMoniesDate());
				ps.setBigDecimal(i++, u.getPriceUsed());
				ps.setBigDecimal(i++, u.getNofDunits());
				ps.setString(i++, u.getFundCurrency());
				ps.setBigDecimal(i++, u.getFundAmount());
				ps.setString(i++, u.getScheduleName());
				ps.setInt(i++, u.getScheduleNumber());
				ps.setInt(i++, u.getSeqnum());
				ps.setString(i++, u.getProcflg());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertUreppfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
}