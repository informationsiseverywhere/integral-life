package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:23
 * Description:
 * Copybook name: ZRSTSTMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrststmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrststmFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zrststmKey = new FixedLengthStringData(256).isAPartOf(zrststmFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrststmChdrcoy = new FixedLengthStringData(1).isAPartOf(zrststmKey, 0);
  	public FixedLengthStringData zrststmChdrnum = new FixedLengthStringData(8).isAPartOf(zrststmKey, 1);
  	public FixedLengthStringData zrststmLife = new FixedLengthStringData(2).isAPartOf(zrststmKey, 9);
  	public FixedLengthStringData zrststmCoverage = new FixedLengthStringData(2).isAPartOf(zrststmKey, 11);
  	public FixedLengthStringData zrststmRider = new FixedLengthStringData(2).isAPartOf(zrststmKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(zrststmKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrststmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrststmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}