package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Br608DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br608TempDAO extends BaseDAO<Br608DTO> {
	public void buildTempData();

	public List<Br608DTO> findTempDataResult(int batchExtractSize, int batchID);

}