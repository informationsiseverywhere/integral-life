package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UstmDAO extends BaseDAO<UstmData>{

	public Map<String,List<UstmData>> getUstmDetails(String stmtType, String company, Integer effDate, int extractSize, int partitionId);
	public void updateUstmRecord(List<UstmData> ustmList);	
	public Map<String, List<UstmData>> readUstjData(int scheduleNumber,String company,int extractSize, int partitionId);
	public List<UstmData> getUstjData(UstmData ustmpf);
	public void updateUstjData(List<UstmData> ustmList);
}
