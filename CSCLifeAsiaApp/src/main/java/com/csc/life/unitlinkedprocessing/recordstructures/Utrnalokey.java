package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:46
 * Description:
 * Copybook name: UTRNALOKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnalokey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnaloFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnaloKey = new FixedLengthStringData(64).isAPartOf(utrnaloFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnaloChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnaloKey, 0);
  	public FixedLengthStringData utrnaloChdrnum = new FixedLengthStringData(8).isAPartOf(utrnaloKey, 1);
  	public FixedLengthStringData utrnaloLife = new FixedLengthStringData(2).isAPartOf(utrnaloKey, 9);
  	public FixedLengthStringData utrnaloCoverage = new FixedLengthStringData(2).isAPartOf(utrnaloKey, 11);
  	public FixedLengthStringData utrnaloRider = new FixedLengthStringData(2).isAPartOf(utrnaloKey, 13);
  	public PackedDecimalData utrnaloPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnaloKey, 15);
  	public FixedLengthStringData utrnaloUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnaloKey, 18);
  	public FixedLengthStringData utrnaloUnitType = new FixedLengthStringData(1).isAPartOf(utrnaloKey, 22);
  	public PackedDecimalData utrnaloProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(utrnaloKey, 23);
  	public PackedDecimalData utrnaloTranno = new PackedDecimalData(5, 0).isAPartOf(utrnaloKey, 25);
  	public PackedDecimalData utrnaloMoniesDate = new PackedDecimalData(8, 0).isAPartOf(utrnaloKey, 28);
  	public FixedLengthStringData filler = new FixedLengthStringData(31).isAPartOf(utrnaloKey, 33, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnaloFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnaloFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}