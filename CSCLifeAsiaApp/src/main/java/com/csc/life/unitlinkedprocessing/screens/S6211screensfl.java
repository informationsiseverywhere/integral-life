package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6211screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 18;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {6, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6211ScreenVars sv = (S6211ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6211screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6211screensfl, 
			sv.S6211screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6211ScreenVars sv = (S6211ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6211screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6211ScreenVars sv = (S6211ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6211screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6211screensflWritten.gt(0))
		{
			sv.s6211screensfl.setCurrentIndex(0);
			sv.S6211screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6211ScreenVars sv = (S6211ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6211screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6211ScreenVars screenVars = (S6211ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.unitType.setFieldName("unitType");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.unitBarePrice.setFieldName("unitBarePrice");
				screenVars.unitBidPrice.setFieldName("unitBidPrice");
				screenVars.unitOfferPrice.setFieldName("unitOfferPrice");
				screenVars.updateDateDisp.setFieldName("updateDateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.shortdesc.set(dm.getField("shortdesc"));
			screenVars.unitType.set(dm.getField("unitType"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.unitBarePrice.set(dm.getField("unitBarePrice"));
			screenVars.unitBidPrice.set(dm.getField("unitBidPrice"));
			screenVars.unitOfferPrice.set(dm.getField("unitOfferPrice"));
			screenVars.updateDateDisp.set(dm.getField("updateDateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6211ScreenVars screenVars = (S6211ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.unitType.setFieldName("unitType");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.unitBarePrice.setFieldName("unitBarePrice");
				screenVars.unitBidPrice.setFieldName("unitBidPrice");
				screenVars.unitOfferPrice.setFieldName("unitOfferPrice");
				screenVars.updateDateDisp.setFieldName("updateDateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("shortdesc").set(screenVars.shortdesc);
			dm.getField("unitType").set(screenVars.unitType);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("unitBarePrice").set(screenVars.unitBarePrice);
			dm.getField("unitBidPrice").set(screenVars.unitBidPrice);
			dm.getField("unitOfferPrice").set(screenVars.unitOfferPrice);
			dm.getField("updateDateDisp").set(screenVars.updateDateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6211screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6211ScreenVars screenVars = (S6211ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.shortdesc.clearFormatting();
		screenVars.unitType.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.unitBarePrice.clearFormatting();
		screenVars.unitBidPrice.clearFormatting();
		screenVars.unitOfferPrice.clearFormatting();
		screenVars.updateDateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6211ScreenVars screenVars = (S6211ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.unitType.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.unitBarePrice.setClassString("");
		screenVars.unitBidPrice.setClassString("");
		screenVars.unitOfferPrice.setClassString("");
		screenVars.updateDateDisp.setClassString("");
	}

/**
 * Clear all the variables in S6211screensfl
 */
	public static void clear(VarModel pv) {
		S6211ScreenVars screenVars = (S6211ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.shortdesc.clear();
		screenVars.unitType.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.unitBarePrice.clear();
		screenVars.unitBidPrice.clear();
		screenVars.unitOfferPrice.clear();
		screenVars.updateDateDisp.clear();
		screenVars.updateDate.clear();
	}
}
