package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:25
 * Description:
 * Copybook name: ULNKBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ulnkbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ulnkbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ulnkbrkKey = new FixedLengthStringData(64).isAPartOf(ulnkbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData ulnkbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(ulnkbrkKey, 0);
  	public FixedLengthStringData ulnkbrkChdrnum = new FixedLengthStringData(8).isAPartOf(ulnkbrkKey, 1);
  	public FixedLengthStringData ulnkbrkCoverage = new FixedLengthStringData(2).isAPartOf(ulnkbrkKey, 9);
  	public FixedLengthStringData ulnkbrkRider = new FixedLengthStringData(2).isAPartOf(ulnkbrkKey, 11);
  	public FixedLengthStringData ulnkbrkLife = new FixedLengthStringData(2).isAPartOf(ulnkbrkKey, 13);
  	public FixedLengthStringData ulnkbrkJlife = new FixedLengthStringData(2).isAPartOf(ulnkbrkKey, 15);
  	public PackedDecimalData ulnkbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ulnkbrkKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(ulnkbrkKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ulnkbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ulnkbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}