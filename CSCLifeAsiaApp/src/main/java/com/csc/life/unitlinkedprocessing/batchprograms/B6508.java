/*
 * File: B6508.java
 * Date: 29 August 2009 21:21:50
 * Author: Quipoz Limited
 * 
 * Class transformed from B6508.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ChdrulsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstjTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstsTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Ulparams;
import com.csc.life.unitlinkedprocessing.reports.R6508Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6578rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
***
*                    UNIT STATEMENT PRINT
*
*
*     This program reads USTJ for all the trigger records wioth the
*     associated Job Number.
*
*     It  will  produce a Summary Statement and optionally a Detail
*     Statement if the Statement Level is set to 'D'.
*
* Summary Statement.
* ------------------
*
*     The  first  USTF record for the contract with the most recent
*     statement  number,  (ie. the latest obtained from the trigger
*     record), is read.
*
*     All  the  USTF records for the contract and statement nunmber
*     are processed and a table built up of all the funds and their
*     closing  balances of deemed units. This table is then printed
*     as the Summary report.
*
* Detail Statement.
* -----------------
*
*     As  with  the  Summary  Statement  this is driven by the USTF
*     records.
*
*     A new page is started for each new policy, life, component or
*     fund.
*
*     If the transactions for a fund go across a page boundary then
*     the  Fund/Currency  sub-heading  is  re-printed  but  the the
*     Opening balance details are only printed on change of fund.
*
*     For  each  USTF  component, fund and statement number all the
*     matching USTS records are read and their details printed. The
*     Opening and Closing balances are printed from USTF.
*
*     If  Plan  processing  applies  and  a Summary record is being
*     processed,  (Plan  Suffix = zero, and the policy number being
*     processed   is  not  greater  than  the  number  of  policies
*     summarised, CHDRULS-POLSUM), then all amounts will have to be
*     divided by the number of policies summarised. Ensure that any
*     rounding  discrepancies  are  absorbed  by  the  first policy
*     printed.
*
*     If  reprints  are  being processed then the reprint clause is
*     obtained  from  table  T6578.  This  will  be accessed by the
*     Transaction  code  and  will provide two 60-byte fields which
*     will be printed immediately after the main heading.
*
*     If a different sequence is required for the processing of the
*     Trigger records and/or the Transaction records then the files
*     may be accessed using OPNQRYF in the CLP member C6508.
*
*
*#
*    CONTROL TOTALS
*    ==============
*    01   Trigger Records Read.
*    02   Statements Printed.
*    03   Transactions Read.
*    06   Pages Printed.
*
*****************************************************************
* </pre>
*/
public class B6508 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R6508Report printFile = new R6508Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(150);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6508");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData sub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaInCount = new ZonedDecimalData(9, 0).setUnsigned();

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private ZonedDecimalData wsaaLinecount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPagenum = new ZonedDecimalData(4, 0).setUnsigned();

		/* WSAA-SUMMARY-TABLE */
	private FixedLengthStringData wsaaFundTable = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaFund = FLSArrayPartOfStructure(50, 4, wsaaFundTable, 0);
	private FixedLengthStringData[] wsaaFundType = FLSArrayPartOfStructure(50, 4, wsaaFundTable, 200);

	private FixedLengthStringData wsaaDunitsTable = new FixedLengthStringData(450);
	private PackedDecimalData[] wsaaClDunits = PDArrayPartOfStructure(50, 16, 5, wsaaDunitsTable, 0);
		/* WSAA-STORE-COMPONENT */
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaStoreLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	private PackedDecimalData wsaaSummaryCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaStmtType = new FixedLengthStringData(1);
	private Validator wsaaNewStmt = new Validator(wsaaStmtType, "P");
	private Validator wsaaReprint = new Validator(wsaaStmtType, "R");

	private FixedLengthStringData wsaaPrintDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPrintDd = new FixedLengthStringData(2).isAPartOf(wsaaPrintDate, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaPrintDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaPrintMm = new FixedLengthStringData(2).isAPartOf(wsaaPrintDate, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaPrintDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaPrintYy = new FixedLengthStringData(4).isAPartOf(wsaaPrintDate, 6);
	private ZonedDecimalData wsaaFileDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaFileDateX = new FixedLengthStringData(8).isAPartOf(wsaaFileDate, 0, REDEFINE);
	private FixedLengthStringData wsaaFileYy = new FixedLengthStringData(4).isAPartOf(wsaaFileDateX, 0);
	private FixedLengthStringData wsaaFileMm = new FixedLengthStringData(2).isAPartOf(wsaaFileDateX, 4);
	private FixedLengthStringData wsaaFileDd = new FixedLengthStringData(2).isAPartOf(wsaaFileDateX, 6);
	private PackedDecimalData wsaaUnits = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaFundCash = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaContCash = new PackedDecimalData(17, 5);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
		/* FORMATS */
	private String chdrulsrec = "CHDRULSREC";
	private String ustjrec = "USTJREC";
	private String ustfrec = "USTFREC";
		/* TABLES */
	private String t5688 = "T5688";
	private String t3629 = "T3629";
	private String t5515 = "T5515";
	private String t6649 = "T6649";
	private String t6578 = "T6578";
	private String e228 = "E228";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct06 = 6;

	private FixedLengthStringData r6508h01Record = new FixedLengthStringData(147);
	private FixedLengthStringData r6508h01O = new FixedLengthStringData(147).isAPartOf(r6508h01Record, 0);
	private ZonedDecimalData pagenum = new ZonedDecimalData(4, 0).isAPartOf(r6508h01O, 0);
	private FixedLengthStringData stmtdate = new FixedLengthStringData(10).isAPartOf(r6508h01O, 4);
	private ZonedDecimalData ustmno = new ZonedDecimalData(5, 0).isAPartOf(r6508h01O, 14);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r6508h01O, 19);
	private FixedLengthStringData ctypedes = new FixedLengthStringData(30).isAPartOf(r6508h01O, 27);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r6508h01O, 57);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(r6508h01O, 61);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(r6508h01O, 64);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r6508h01O, 94);
	private FixedLengthStringData lifename = new FixedLengthStringData(47).isAPartOf(r6508h01O, 96);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r6508h01O, 143);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r6508h01O, 145);

	private FixedLengthStringData r6508h02Record = new FixedLengthStringData(79);
	private FixedLengthStringData r6508h02O = new FixedLengthStringData(79).isAPartOf(r6508h02Record, 0);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(r6508h02O, 0);
	private FixedLengthStringData vrtfnddsc = new FixedLengthStringData(30).isAPartOf(r6508h02O, 4);
	private FixedLengthStringData unitypdsc = new FixedLengthStringData(12).isAPartOf(r6508h02O, 34);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(r6508h02O, 46);
	private FixedLengthStringData fndcurrdsc = new FixedLengthStringData(30).isAPartOf(r6508h02O, 49);

	private FixedLengthStringData r6508h03Record = new FixedLengthStringData(60);
	private FixedLengthStringData r6508h03O = new FixedLengthStringData(60).isAPartOf(r6508h03Record, 0);
	private FixedLengthStringData stopdt = new FixedLengthStringData(10).isAPartOf(r6508h03O, 0);
	private ZonedDecimalData stopdun = new ZonedDecimalData(16, 5).isAPartOf(r6508h03O, 10);
	private ZonedDecimalData stopfuca = new ZonedDecimalData(17, 2).isAPartOf(r6508h03O, 26);
	private ZonedDecimalData stopcoca = new ZonedDecimalData(17, 2).isAPartOf(r6508h03O, 43);

	private FixedLengthStringData r6508h05Record = new FixedLengthStringData(60);
	private FixedLengthStringData r6508h05O = new FixedLengthStringData(60).isAPartOf(r6508h05Record, 0);
	private FixedLengthStringData stopdt1 = new FixedLengthStringData(10).isAPartOf(r6508h05O, 0);
	private ZonedDecimalData stopdun1 = new ZonedDecimalData(16, 5).isAPartOf(r6508h05O, 10);
	private ZonedDecimalData stopfuca1 = new ZonedDecimalData(17, 2).isAPartOf(r6508h05O, 26);
	private ZonedDecimalData stopcoca1 = new ZonedDecimalData(17, 2).isAPartOf(r6508h05O, 43);

	private FixedLengthStringData r6508d01Record = new FixedLengthStringData(78);
	private FixedLengthStringData r6508d01O = new FixedLengthStringData(78).isAPartOf(r6508d01Record, 0);
	private FixedLengthStringData moniesdt = new FixedLengthStringData(10).isAPartOf(r6508d01O, 0);
	private ZonedDecimalData nofdunt = new ZonedDecimalData(16, 5).isAPartOf(r6508d01O, 10);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2).isAPartOf(r6508d01O, 26);
	private ZonedDecimalData cntamnt = new ZonedDecimalData(17, 2).isAPartOf(r6508d01O, 43);
	private ZonedDecimalData fundrate = new ZonedDecimalData(18, 9).isAPartOf(r6508d01O, 60);

	private FixedLengthStringData r6508d02Record = new FixedLengthStringData(95);
	private FixedLengthStringData r6508d02O = new FixedLengthStringData(95).isAPartOf(r6508d02Record, 0);
	private FixedLengthStringData vrtfnd1 = new FixedLengthStringData(4).isAPartOf(r6508d02O, 0);
	private FixedLengthStringData vrtfnddsc1 = new FixedLengthStringData(30).isAPartOf(r6508d02O, 4);
	private FixedLengthStringData unitypdsc1 = new FixedLengthStringData(12).isAPartOf(r6508d02O, 34);
	private FixedLengthStringData fndcurr1 = new FixedLengthStringData(3).isAPartOf(r6508d02O, 46);
	private FixedLengthStringData fndcurrdsc1 = new FixedLengthStringData(30).isAPartOf(r6508d02O, 49);
	private ZonedDecimalData nofdunt1 = new ZonedDecimalData(16, 5).isAPartOf(r6508d02O, 79);

	private FixedLengthStringData r6508h07Record = new FixedLengthStringData(120);
	private FixedLengthStringData r6508h07O = new FixedLengthStringData(120).isAPartOf(r6508h07Record, 0);
	private FixedLengthStringData rpntlin1 = new FixedLengthStringData(60).isAPartOf(r6508h07O, 0);
	private FixedLengthStringData rpntlin2 = new FixedLengthStringData(60).isAPartOf(r6508h07O, 60);
		/*Contract Header Information for Unit Sta*/
	private ChdrulsTableDAM chdrulsIO = new ChdrulsTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private T6578rec t6578rec = new T6578rec();
	private Ulparams ulparams = new Ulparams();
		/*Unit Statement Fund Header Details.*/
	private UstfTableDAM ustfIO = new UstfTableDAM();
		/*Unit Statement Trigger Records with Stat*/
	private UstjTableDAM ustjIO = new UstjTableDAM();
		/*Unit Statement Transactions - With State*/
	private UstsTableDAM ustsIO = new UstsTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextUstj1080, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		para3100
	}

	public B6508() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		para100();
	}

protected void para100()
	{
		ulparams.parmRecord.set(conjobrec.params);
		wsaaStmtType.set(ulparams.stmType);
		wsaaInCount.set(ZERO);
		wsaaOverflow.set("N");
		printFile.openOutput();
		ustjIO.setDataKey(SPACES);
		ustjIO.setJobnoStmt(ulparams.stmJobno);
		ustjIO.setEffdate(ZERO);
		ustjIO.setChdrcoy(runparmrec.company);
		ustjIO.setFormat(ustjrec);
		ustjIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ustjIO);
		if (isNE(ustjIO.getStatuz(),varcom.oK)
		&& isNE(ustjIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustjIO.getParams());
			databaseError006();
		}
		while ( !(isNE(ustjIO.getJobnoStmt(),runparmrec.jobnum)
		|| isNE(ustjIO.getChdrcoy(),runparmrec.company)
		|| isEQ(ustjIO.getStatuz(),varcom.endp))) {
			processUstjRecord1000();
		}
		
		if (isEQ(wsaaInCount,0)) {
			conlogrec.params.set(SPACES);
			conlogrec.error.set(e228);
			callConlog003();
		}
		printFile.close();
	}

protected void processUstjRecord1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
				}
				case nextUstj1080: {
					nextUstj1080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		updateReqd004();
		if (isEQ(controlrec.flag,"N")) {
			goTo(GotoLabel.nextUstj1080);
		}
		if (isNE(ustjIO.getStmtType(),wsaaStmtType)) {
			goTo(GotoLabel.nextUstj1080);
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		summaryStatement2000();
		if (isEQ(ustjIO.getUnitStmtFlag(),"D")) {
			detailStatement3000();
		}
	}

protected void nextUstj1080()
	{
		ustjIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustjIO);
		if (isNE(ustjIO.getStatuz(),varcom.oK)
		&& isNE(ustjIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustjIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void summaryStatement2000()
	{
		para2000();
	}

protected void para2000()
	{
		indicArea.set("0");
		ustfIO.setDataArea(SPACES);
		ustfIO.setChdrcoy(ustjIO.getChdrcoy());
		ustfIO.setChdrnum(ustjIO.getChdrnum());
		ustfIO.setUstmno(ustjIO.getUstmno());
		ustfIO.setPlanSuffix(ZERO);
		ustfIO.setFunction(varcom.begn);
		ustfIO.setFormat(ustfrec);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)
		&& isNE(ustfIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
		wsaaPagenum.set(1);
		pagenum.set(wsaaPagenum);
		ustmno.set(ustjIO.getUstmno());
		chdrnum.set(ustjIO.getChdrnum());
		wsaaFileDate.set(ustjIO.getEffdate());
		wsaaPrintDd.set(wsaaFileDd);
		wsaaPrintMm.set(wsaaFileMm);
		wsaaPrintYy.set(wsaaFileYy);
		stmtdate.set(wsaaPrintDate);
		chdrulsIO.setDataKey(SPACES);
		chdrulsIO.setChdrcoy(runparmrec.company);
		chdrulsIO.setChdrnum(ustjIO.getChdrnum());
		chdrulsIO.setFormat(chdrulsrec);
		chdrulsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrulsIO);
		if (isNE(chdrulsIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(chdrulsIO.getParams());
			databaseError006();
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrulsIO.getCnttype());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			ctypedes.fill("?");
		}
		else {
			ctypedes.set(descIO.getLongdesc());
		}
		cntcurr.set(chdrulsIO.getCntcurr());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(chdrulsIO.getCntcurr());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			currdesc.fill("?");
		}
		else {
			currdesc.set(descIO.getLongdesc());
		}
		life.set(ustfIO.getLife());
		coverage.set(ustfIO.getCoverage());
		rider.set(ustfIO.getRider());
		lifeenqIO.setChdrcoy(chdrulsIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrulsIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(lifeenqIO.getParams());
			databaseError006();
		}
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(runparmrec.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(cltsIO.getParams());
			databaseError006();
		}
		plainname();
		lifename.set(wsspLongconfname);
		printRecord.set(SPACES);
		printFile.printR6508h01(r6508h01Record, indicArea);
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		if (wsaaReprint.isTrue()) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrulsIO.getChdrcoy());
			itemIO.setItemtabl(t6578);
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(runparmrec.language.toString());
			stringVariable1.append(runparmrec.transcode.toString());
			itemIO.getItemitem().setLeft(stringVariable1.toString());
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(itemIO.getParams());
			databaseError006();
		}
		else {
			t6578rec.t6578Rec.set(itemIO.getGenarea());
			rpntlin1.set(t6578rec.ustprcom1);
			rpntlin2.set(t6578rec.ustprcom2);
		}
		if (wsaaReprint.isTrue()) {
			printRecord.set(SPACES);
			printFile.printR6508h07(r6508h07Record, indicArea);
		}
		printRecord.set(SPACES);
		printFile.printR6508h06(printRecord, indicArea);
		wsaaFundTable.set(SPACES);
		wsaaDunitsTable.set(ZERO);
		sub.set(1);
		while ( !(isNE(ustfIO.getChdrcoy(),ustjIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),ustjIO.getChdrnum())
		|| isNE(ustfIO.getUstmno(),ustjIO.getUstmno())
		|| isEQ(ustfIO.getStatuz(),varcom.endp))) {
			buildFundTable2100();
		}
		
		sub.set(1);
		while ( !(isEQ(wsaaFund[sub.toInt()],SPACES))) {
			printSummaryLine2300();
		}
		
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void buildFundTable2100()
	{
		/*PARA*/
		wsaaInCount.add(1);
		wsaaFund[sub.toInt()].set(ustfIO.getUnitVirtualFund());
		wsaaFundType[sub.toInt()].set(ustfIO.getUnitType());
		wsaaClDunits[sub.toInt()].set(ustfIO.getStmtClDunits());
		sub.add(1);
		ustfIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)
		&& isNE(ustfIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void printSummaryLine2300()
	{
		para2300();
	}

protected void para2300()
	{
		if (pageOverflow.isTrue()) {
			summaryHeadings2400();
		}
		vrtfnd1.set(wsaaFund[sub.toInt()]);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrulsIO.getChdrcoy());
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaFund[sub.toInt()]);
		itdmIO.setItmfrm(chdrulsIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),chdrulsIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),wsaaFund[sub.toInt()])) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		fndcurr1.set(t5515rec.currcode);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(wsaaFund[sub.toInt()]);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			vrtfnddsc1.fill("?");
		}
		else {
			vrtfnddsc1.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t6649);
		descIO.setDescitem(wsaaFundType[sub.toInt()]);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			unitypdsc1.fill("?");
		}
		else {
			unitypdsc1.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(t5515rec.currcode);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			fndcurrdsc1.fill("?");
		}
		else {
			fndcurrdsc1.set(descIO.getLongdesc());
		}
		nofdunt1.set(wsaaClDunits[sub.toInt()]);
		printRecord.set(SPACES);
		printFile.printR6508d02(r6508d02Record, indicArea);
		sub.add(1);
	}

protected void summaryHeadings2400()
	{
		para2400();
	}

protected void para2400()
	{
		wsaaOverflow.set("N");
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		printRecord.set(SPACES);
		printFile.printR6508h01(r6508h01Record, indicArea);
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		if (wsaaReprint.isTrue()) {
			printRecord.set(SPACES);
			printFile.printR6508h07(r6508h07Record, indicArea);
		}
		printRecord.set(SPACES);
		printFile.printR6508h06(printRecord, indicArea);
	}

protected void detailStatement3000()
	{
		para3000();
	}

protected void para3000()
	{
		indicTable[10].set("1");
		wsaaSummaryCount.set(ZERO);
		ustfIO.setDataArea(SPACES);
		ustfIO.setChdrcoy(ustjIO.getChdrcoy());
		ustfIO.setChdrnum(ustjIO.getChdrnum());
		ustfIO.setUstmno(ustjIO.getUstmno());
		ustfIO.setPlanSuffix(ZERO);
		ustfIO.setFormat(ustfrec);
		ustfIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)
		&& isNE(ustfIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
		while ( !(isNE(ustfIO.getChdrcoy(),ustjIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),ustjIO.getChdrnum())
		|| isNE(ustfIO.getUstmno(),ustjIO.getUstmno())
		|| isEQ(ustfIO.getStatuz(),varcom.endp))) {
			processUstfHeaders3100();
		}
		
	}

protected void processUstfHeaders3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case para3100: {
					para3100();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3100()
	{
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		ustmno.set(ustfIO.getUstmno());
		if (isEQ(ustfIO.getPlanSuffix(),ZERO)
		&& isNE(chdrulsIO.getPolsum(),ZERO)) {
			wsaaSummaryCount.add(1);
			plnsfx.set(wsaaSummaryCount);
		}
		else {
			plnsfx.set(ustfIO.getPlanSuffix());
		}
		printRecord.set(SPACES);
		printFile.printR6508h01(r6508h01Record, indicArea);
		wsaaOverflow.set("N");
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		if (wsaaReprint.isTrue()) {
			printRecord.set(SPACES);
			printFile.printR6508h07(r6508h07Record, indicArea);
		}
		wsaaStorePlanSuffix.set(ustfIO.getPlanSuffix());
		wsaaStoreLife.set(ustfIO.getLife());
		wsaaStoreCoverage.set(ustfIO.getCoverage());
		wsaaStoreRider.set(ustfIO.getRider());
		while ( !(isNE(ustfIO.getChdrcoy(),ustjIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(),ustjIO.getChdrnum())
		|| isNE(ustfIO.getUstmno(),ustjIO.getUstmno())
		|| isNE(ustfIO.getLife(),wsaaStoreLife)
		|| isNE(ustfIO.getCoverage(),wsaaStoreCoverage)
		|| isNE(ustfIO.getRider(),wsaaStoreRider)
		|| isNE(ustfIO.getPlanSuffix(),wsaaStorePlanSuffix)
		|| isEQ(ustfIO.getStatuz(),varcom.endp))) {
			processUstfComponent3200();
		}
		
		if (isEQ(wsaaStorePlanSuffix,ZERO)
		&& isNE(chdrulsIO.getPolsum(),ZERO)
		&& isLT(wsaaSummaryCount,chdrulsIO.getPolsum())) {
			ustfIO.setDataArea(SPACES);
			ustfIO.setChdrcoy(ustjIO.getChdrcoy());
			ustfIO.setChdrnum(ustjIO.getChdrnum());
			ustfIO.setUstmno(ustjIO.getUstmno());
			ustfIO.setPlanSuffix(ZERO);
			ustfIO.setFormat(ustfrec);
			ustfIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, ustfIO);
			if (isNE(ustfIO.getStatuz(),varcom.oK)
			&& isNE(ustfIO.getStatuz(),varcom.endp)) {
				conerrrec.dbparams.set(ustfIO.getParams());
				databaseError006();
			}
			else {
				goTo(GotoLabel.para3100);
			}
		}
	}

protected void processUstfComponent3200()
	{
		para3200();
	}

protected void para3200()
	{
		vrtfnd.set(ustfIO.getUnitVirtualFund());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrulsIO.getChdrcoy());
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(ustfIO.getUnitVirtualFund());
		itdmIO.setItmfrm(chdrulsIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),chdrulsIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),ustfIO.getUnitVirtualFund())) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		fndcurr.set(t5515rec.currcode);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(ustfIO.getUnitVirtualFund());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			vrtfnddsc.fill("?");
		}
		else {
			vrtfnddsc.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t6649);
		descIO.setDescitem(ustfIO.getUnitType());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			unitypdsc.fill("?");
		}
		else {
			unitypdsc.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(t5515rec.currcode);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			fndcurrdsc.fill("?");
		}
		else {
			fndcurrdsc.set(descIO.getLongdesc());
		}
		printRecord.set(SPACES);
		printFile.printR6508h02(r6508h02Record, indicArea);
		wsaaFileDate.set(ustfIO.getStmtOpDate());
		wsaaPrintDd.set(wsaaFileDd);
		wsaaPrintMm.set(wsaaFileMm);
		wsaaPrintYy.set(wsaaFileYy);
		stopdt.set(wsaaPrintDate);
		if (isEQ(ustfIO.getPlanSuffix(),ZERO)
		&& isNE(chdrulsIO.getPolsum(),ZERO)) {
			compute(wsaaUnits, 5).set(div(ustfIO.getStmtOpDunits(),chdrulsIO.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustfIO.getStmtOpFundCash(),chdrulsIO.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustfIO.getStmtOpContCash(),chdrulsIO.getPolsum()));
			stopdun.set(wsaaUnits);
			stopfuca.set(wsaaFundCash);
			stopcoca.set(wsaaContCash);
		}
		else {
			stopdun.set(ustfIO.getStmtOpDunits());
			stopfuca.set(ustfIO.getStmtOpFundCash());
			stopcoca.set(ustfIO.getStmtOpContCash());
		}
		printRecord.set(SPACES);
		printFile.printR6508h03(r6508h03Record, indicArea);
		printRecord.set(SPACES);
		printFile.printR6508h04(printRecord, indicArea);
		ustsIO.setChdrcoy(ustfIO.getChdrcoy());
		ustsIO.setChdrnum(ustfIO.getChdrnum());
		ustsIO.setUstmno(ustfIO.getUstmno());
		ustsIO.setPlanSuffix(ustfIO.getPlanSuffix());
		ustsIO.setLife(ustfIO.getLife());
		ustsIO.setCoverage(ustfIO.getCoverage());
		ustsIO.setRider(ustfIO.getRider());
		ustsIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		ustsIO.setUnitType(ustfIO.getUnitType());
		ustsIO.setMoniesDate(ZERO);
		ustsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ustsIO);
		if (isNE(ustsIO.getStatuz(),varcom.oK)
		&& isNE(ustsIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustsIO.getParams());
			databaseError006();
		}
		while ( !(isNE(ustsIO.getChdrcoy(),ustfIO.getChdrcoy())
		|| isNE(ustsIO.getChdrnum(),ustfIO.getChdrnum())
		|| isNE(ustsIO.getUstmno(),ustfIO.getUstmno())
		|| isNE(ustsIO.getPlanSuffix(),ustfIO.getPlanSuffix())
		|| isNE(ustsIO.getLife(),ustfIO.getLife())
		|| isNE(ustsIO.getCoverage(),ustfIO.getCoverage())
		|| isNE(ustsIO.getRider(),ustfIO.getRider())
		|| isNE(ustsIO.getUnitVirtualFund(),ustfIO.getUnitVirtualFund())
		|| isNE(ustsIO.getUnitType(),ustfIO.getUnitType())
		|| isEQ(ustsIO.getStatuz(),varcom.endp))) {
			processUstsTransactions3300();
		}
		
		wsaaFileDate.set(ustfIO.getStmtClDate());
		wsaaPrintDd.set(wsaaFileDd);
		wsaaPrintMm.set(wsaaFileMm);
		wsaaPrintYy.set(wsaaFileYy);
		stopdt1.set(wsaaPrintDate);
		if (isEQ(ustfIO.getPlanSuffix(),ZERO)
		&& isNE(chdrulsIO.getPolsum(),ZERO)) {
			compute(wsaaUnits, 5).set(div(ustfIO.getStmtClDunits(),chdrulsIO.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustfIO.getStmtClFundCash(),chdrulsIO.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustfIO.getStmtClContCash(),chdrulsIO.getPolsum()));
			stopdun1.set(wsaaUnits);
			stopfuca1.set(wsaaFundCash);
			stopcoca1.set(wsaaContCash);
		}
		else {
			stopdun1.set(ustfIO.getStmtClDunits());
			stopfuca1.set(ustfIO.getStmtClFundCash());
			stopcoca1.set(ustfIO.getStmtClContCash());
		}
		printRecord.set(SPACES);
		printFile.printR6508h05(r6508h05Record, indicArea);
		ustfIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(),varcom.oK)
		&& isNE(ustfIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
	}

protected void processUstsTransactions3300()
	{
		para3300();
	}

protected void para3300()
	{
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		wsaaFileDate.set(ustsIO.getMoniesDate());
		wsaaPrintDd.set(wsaaFileDd);
		wsaaPrintMm.set(wsaaFileMm);
		wsaaPrintYy.set(wsaaFileYy);
		moniesdt.set(wsaaPrintDate);
		if (isEQ(ustfIO.getPlanSuffix(),ZERO)
		&& isNE(chdrulsIO.getPolsum(),ZERO)) {
			compute(wsaaUnits, 5).set(div(ustsIO.getNofDunits(),chdrulsIO.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustsIO.getFundAmount(),chdrulsIO.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustsIO.getContractAmount(),chdrulsIO.getPolsum()));
			nofdunt.set(wsaaUnits);
			fundamnt.set(wsaaFundCash);
			cntamnt.set(wsaaContCash);
		}
		else {
			nofdunt.set(ustsIO.getNofDunits());
			fundamnt.set(ustsIO.getFundAmount());
			cntamnt.set(ustsIO.getContractAmount());
		}
		fundrate.set(ustsIO.getFundRate());
		printRecord.set(SPACES);
		printFile.printR6508d01(r6508d01Record, indicArea);
		ustsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustsIO);
		if (isNE(ustsIO.getStatuz(),varcom.oK)
		&& isNE(ustsIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustsIO.getParams());
			databaseError006();
		}
	}

protected void detailHeading3400()
	{
		para3400();
	}

protected void para3400()
	{
		wsaaOverflow.set("N");
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		printRecord.set(SPACES);
		printFile.printR6508h01(r6508h01Record, indicArea);
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		if (wsaaReprint.isTrue()) {
			printRecord.set(SPACES);
			printFile.printR6508h07(r6508h07Record, indicArea);
		}
		printRecord.set(SPACES);
		printFile.printR6508h02(r6508h02Record, indicArea);
		printRecord.set(SPACES);
		printFile.printR6508h04(printRecord, indicArea);
	}
}
