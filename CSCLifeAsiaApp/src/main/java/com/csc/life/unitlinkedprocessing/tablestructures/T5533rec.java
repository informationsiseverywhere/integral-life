package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:24
 * Description:
 * Copybook name: T5533REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5533rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5533Rec = new FixedLengthStringData(772);
  	public FixedLengthStringData cmaxs = new FixedLengthStringData(136).isAPartOf(t5533Rec, 0);
  	public ZonedDecimalData[] cmax = ZDArrayPartOfStructure(8, 17, 2, cmaxs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(136).isAPartOf(cmaxs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cmax01 = new ZonedDecimalData(17, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData cmax02 = new ZonedDecimalData(17, 2).isAPartOf(filler, 17);
  	public ZonedDecimalData cmax03 = new ZonedDecimalData(17, 2).isAPartOf(filler, 34);
  	public ZonedDecimalData cmax04 = new ZonedDecimalData(17, 2).isAPartOf(filler, 51);
  	public ZonedDecimalData cmax05 = new ZonedDecimalData(17, 2).isAPartOf(filler, 68);
  	public ZonedDecimalData cmax06 = new ZonedDecimalData(17, 2).isAPartOf(filler, 85);
  	public ZonedDecimalData cmax07 = new ZonedDecimalData(17, 2).isAPartOf(filler, 102);
  	public ZonedDecimalData cmax08 = new ZonedDecimalData(17, 2).isAPartOf(filler, 119);
  	public FixedLengthStringData cmins = new FixedLengthStringData(136).isAPartOf(t5533Rec, 136);
  	public ZonedDecimalData[] cmin = ZDArrayPartOfStructure(8, 17, 2, cmins, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(136).isAPartOf(cmins, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cmin01 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData cmin02 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 17);
  	public ZonedDecimalData cmin03 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 34);
  	public ZonedDecimalData cmin04 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 51);
  	public ZonedDecimalData cmin05 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 68);
  	public ZonedDecimalData cmin06 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData cmin07 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 102);
  	public ZonedDecimalData cmin08 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 119);
  	public ZonedDecimalData casualContribMax = new ZonedDecimalData(17, 2).isAPartOf(t5533Rec, 272);
  	public ZonedDecimalData casualContribMin = new ZonedDecimalData(17, 2).isAPartOf(t5533Rec, 289);
  	public FixedLengthStringData frequencys = new FixedLengthStringData(16).isAPartOf(t5533Rec, 306);
  	public FixedLengthStringData[] frequency = FLSArrayPartOfStructure(8, 2, frequencys, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(frequencys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData frequency01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData frequency02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData frequency03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData frequency04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData frequency05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData frequency06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData frequency07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData frequency08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public ZonedDecimalData premUnit = new ZonedDecimalData(3, 0).isAPartOf(t5533Rec, 322);
  	public ZonedDecimalData rndfact = new ZonedDecimalData(6, 0).isAPartOf(t5533Rec, 325);
	public FixedLengthStringData cIncrmaxs = new FixedLengthStringData(136).isAPartOf(t5533Rec, 331);
  	public ZonedDecimalData[] cIncrmax = ZDArrayPartOfStructure(8, 17, 2, cIncrmaxs, 0);
  	public FixedLengthStringData fillerNew1 = new FixedLengthStringData(136).isAPartOf(cIncrmaxs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cIncrmax01 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 0);
  	public ZonedDecimalData cIncrmax02 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 17);
  	public ZonedDecimalData cIncrmax03 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 34);
  	public ZonedDecimalData cIncrmax04 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 51);
  	public ZonedDecimalData cIncrmax05 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 68);
  	public ZonedDecimalData cIncrmax06 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 85);
  	public ZonedDecimalData cIncrmax07 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 102);
  	public ZonedDecimalData cIncrmax08 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew1, 119);
  	public FixedLengthStringData cIncrmins = new FixedLengthStringData(136).isAPartOf(t5533Rec, 467);
  	public ZonedDecimalData[] cIncrmin = ZDArrayPartOfStructure(8, 17, 2, cIncrmins, 0);
  	public FixedLengthStringData fillerNew2 = new FixedLengthStringData(136).isAPartOf(cIncrmins, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cIncrmin01 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 0);
  	public ZonedDecimalData cIncrmin02 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 17);
  	public ZonedDecimalData cIncrmin03 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 34);
  	public ZonedDecimalData cIncrmin04 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 51);
  	public ZonedDecimalData cIncrmin05 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 68);
  	public ZonedDecimalData cIncrmin06 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 85);
  	public ZonedDecimalData cIncrmin07 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 102);
  	public ZonedDecimalData cIncrmin08 = new ZonedDecimalData(17, 2).isAPartOf(fillerNew2, 119);
  	
  	public FixedLengthStringData filler3 = new FixedLengthStringData(169).isAPartOf(t5533Rec, 603, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5533Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5533Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}