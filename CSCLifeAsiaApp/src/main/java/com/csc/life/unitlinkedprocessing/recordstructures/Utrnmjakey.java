package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:49
 * Description:
 * Copybook name: UTRNMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnmjaKey = new FixedLengthStringData(64).isAPartOf(utrnmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnmjaKey, 0);
  	public FixedLengthStringData utrnmjaChdrnum = new FixedLengthStringData(8).isAPartOf(utrnmjaKey, 1);
  	public FixedLengthStringData utrnmjaLife = new FixedLengthStringData(2).isAPartOf(utrnmjaKey, 9);
  	public FixedLengthStringData utrnmjaCoverage = new FixedLengthStringData(2).isAPartOf(utrnmjaKey, 11);
  	public FixedLengthStringData utrnmjaRider = new FixedLengthStringData(2).isAPartOf(utrnmjaKey, 13);
  	public PackedDecimalData utrnmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnmjaKey, 15);
  	public FixedLengthStringData utrnmjaUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnmjaKey, 18);
  	public FixedLengthStringData utrnmjaUnitType = new FixedLengthStringData(1).isAPartOf(utrnmjaKey, 22);
  	public PackedDecimalData utrnmjaTranno = new PackedDecimalData(5, 0).isAPartOf(utrnmjaKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(utrnmjaKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}