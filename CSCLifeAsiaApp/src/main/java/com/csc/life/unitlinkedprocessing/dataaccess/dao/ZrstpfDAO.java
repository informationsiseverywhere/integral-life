package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZrstpfDAO extends BaseDAO<Zrstpf> {
	public Map<String, List<Zrstpf>> searchZrstnudRecordByChdrnum(List<String> chdrnumList);
	public void insertZrstpfRecord(List<Zrstpf> urList);
	public void updateZrstXTranno(List<Zrstpf> urList);

	public Map<String, List<Zrstpf>> searchZrstRecordByChdrnum(List<String> chdrnumList, String chdrcoy);
	public Map<String, List<Zrstpf>> searchZrsttraRecordByChdrnum(List<String> chdrnumList, String chdrcoy);
	public Map<String, List<Zrstpf>> searchZrststmByChdrnum(List<String> chdrnumList, String chdrcoy);
	public void updateZrststm(List<Zrstpf> zrstUpdateList);
	
	public List<Zrstpf> searchZrstReccordByChdrnumFdbkInd(String chdrcoy, String chdrnum);
	public List<Zrstpf> searchZrstnudRecord(String chdrcoy, String chdrnum, String life, String covreage, String rider, String fdbkind); //ILIFE-8786
	public List<Zrstpf> searchZrstReccordByChdrnumLife(String chdrcoy, String chdrnum, 
			String life, String coverage); //IBPLIFE-3963
	public void updateZrst(List<Zrstpf> zrstUpdateList); //IBPLIFE-3963
}
