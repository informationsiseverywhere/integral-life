package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:03
 * Description:
 * Copybook name: T5691REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5691rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5691Rec = new FixedLengthStringData(getRecSize());
  	public ZonedDecimalData inifeeamn = new ZonedDecimalData(11, 2).isAPartOf(t5691Rec, 0);
  	public ZonedDecimalData perfeeamn = new ZonedDecimalData(11, 2).isAPartOf(t5691Rec, 11);
  	public ZonedDecimalData zradmnpc = new ZonedDecimalData(5, 2).isAPartOf(t5691Rec, 22);
  	public ZonedDecimalData zrtupadpc = new ZonedDecimalData(5, 2).isAPartOf(t5691Rec, 27);
  	public ZonedDecimalData zrtupfee = new ZonedDecimalData(11, 2).isAPartOf(t5691Rec, 32);
  	public ZonedDecimalData advfeepc = new ZonedDecimalData(5, 2).isAPartOf(t5691Rec, 48);
  	public FixedLengthStringData filler = new FixedLengthStringData(452).isAPartOf(t5691Rec, 48, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5691Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5691Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getRecSize() {
		return 500;
	}


}