/******************************************************************************
 * File Name 		: Uswdpf.java
 * Author			: sjothiman2
 * Creation Date	: 23 May 2018
 * Project			: Integral Life
 * Description		: The Model Class for USWDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Uswdpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "USWDPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private Integer plnsfx;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer tranno;
	private String pcamtind;
	private String srcfund01;
	private String srcfund02;
	private String srcfund03;
	private String srcfund04;
	private String srcfund05;
	private String srcfund06;
	private String srcfund07;
	private String srcfund08;
	private String srcfund09;
	private String srcfund10;
	private String srcfund11;
	private String srcfund12;
	private String srcfund13;
	private String srcfund14;
	private String srcfund15;
	private String srcfund16;
	private String srcfund17;
	private String srcfund18;
	private String srcfund19;
	private String srcfund20;
	private String scfndtyp01;
	private String scfndtyp02;
	private String scfndtyp03;
	private String scfndtyp04;
	private String scfndtyp05;
	private String scfndtyp06;
	private String scfndtyp07;
	private String scfndtyp08;
	private String scfndtyp09;
	private String scfndtyp10;
	private String scfndtyp11;
	private String scfndtyp12;
	private String scfndtyp13;
	private String scfndtyp14;
	private String scfndtyp15;
	private String scfndtyp16;
	private String scfndtyp17;
	private String scfndtyp18;
	private String scfndtyp19;
	private String scfndtyp20;
	private String scfndcur01;
	private String scfndcur02;
	private String scfndcur03;
	private String scfndcur04;
	private String scfndcur05;
	private String scfndcur06;
	private String scfndcur07;
	private String scfndcur08;
	private String scfndcur09;
	private String scfndcur10;
	private String scfndcur11;
	private String scfndcur12;
	private String scfndcur13;
	private String scfndcur14;
	private String scfndcur15;
	private String scfndcur16;
	private String scfndcur17;
	private String scfndcur18;
	private String scfndcur19;
	private String scfndcur20;
	private BigDecimal scprcamt01;
	private BigDecimal scprcamt02;
	private BigDecimal scprcamt03;
	private BigDecimal scprcamt04;
	private BigDecimal scprcamt05;
	private BigDecimal scprcamt06;
	private BigDecimal scprcamt07;
	private BigDecimal scprcamt08;
	private BigDecimal scprcamt09;
	private BigDecimal scprcamt10;
	private BigDecimal scprcamt11;
	private BigDecimal scprcamt12;
	private BigDecimal scprcamt13;
	private BigDecimal scprcamt14;
	private BigDecimal scprcamt15;
	private BigDecimal scprcamt16;
	private BigDecimal scprcamt17;
	private BigDecimal scprcamt18;
	private BigDecimal scprcamt19;
	private BigDecimal scprcamt20;
	private BigDecimal scestval01;
	private BigDecimal scestval02;
	private BigDecimal scestval03;
	private BigDecimal scestval04;
	private BigDecimal scestval05;
	private BigDecimal scestval06;
	private BigDecimal scestval07;
	private BigDecimal scestval08;
	private BigDecimal scestval09;
	private BigDecimal scestval10;
	private BigDecimal scestval11;
	private BigDecimal scestval12;
	private BigDecimal scestval13;
	private BigDecimal scestval14;
	private BigDecimal scestval15;
	private BigDecimal scestval16;
	private BigDecimal scestval17;
	private BigDecimal scestval18;
	private BigDecimal scestval19;
	private BigDecimal scestval20;
	private BigDecimal scactval01;
	private BigDecimal scactval02;
	private BigDecimal scactval03;
	private BigDecimal scactval04;
	private BigDecimal scactval05;
	private BigDecimal scactval06;
	private BigDecimal scactval07;
	private BigDecimal scactval08;
	private BigDecimal scactval09;
	private BigDecimal scactval10;
	private BigDecimal scactval11;
	private BigDecimal scactval12;
	private BigDecimal scactval13;
	private BigDecimal scactval14;
	private BigDecimal scactval15;
	private BigDecimal scactval16;
	private BigDecimal scactval17;
	private BigDecimal scactval18;
	private BigDecimal scactval19;
	private BigDecimal scactval20;
	private String tgtfund01;
	private String tgtfund02;
	private String tgtfund03;
	private String tgtfund04;
	private String tgtfund05;
	private String tgtfund06;
	private String tgtfund07;
	private String tgtfund08;
	private String tgtfund09;
	private String tgtfund10;
	private String tgfndtyp01;
	private String tgfndtyp02;
	private String tgfndtyp03;
	private String tgfndtyp04;
	private String tgfndtyp05;
	private String tgfndtyp06;
	private String tgfndtyp07;
	private String tgfndtyp08;
	private String tgfndtyp09;
	private String tgfndtyp10;
	private String tgfndcur01;
	private String tgfndcur02;
	private String tgfndcur03;
	private String tgfndcur04;
	private String tgfndcur05;
	private String tgfndcur06;
	private String tgfndcur07;
	private String tgfndcur08;
	private String tgfndcur09;
	private String tgfndcur10;
	private BigDecimal tgtprcnt01;
	private BigDecimal tgtprcnt02;
	private BigDecimal tgtprcnt03;
	private BigDecimal tgtprcnt04;
	private BigDecimal tgtprcnt05;
	private BigDecimal tgtprcnt06;
	private BigDecimal tgtprcnt07;
	private BigDecimal tgtprcnt08;
	private BigDecimal tgtprcnt09;
	private BigDecimal tgtprcnt10;
	private Integer effdate;
	private String orswchfe;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String scfndpol01;
	private String scfndpol02;
	private String scfndpol03;
	private String scfndpol04;
	private String scfndpol05;
	private String scfndpol06;
	private String scfndpol07;
	private String scfndpol08;
	private String scfndpol09;
	private String scfndpol10;
	private String scfndpol11;
	private String scfndpol12;
	private String scfndpol13;
	private String scfndpol14;
	private String scfndpol15;
	private String scfndpol16;
	private String scfndpol17;
	private String scfndpol18;
	private String scfndpol19;
	private String scfndpol20;
	private String tgfnpol01;
	private String tgfnpol02;
	private String tgfnpol03;
	private String tgfnpol04;
	private String tgfnpol05;
	private String tgfnpol06;
	private String tgfnpol07;
	private String tgfnpol08;
	private String tgfnpol09;
	private String tgfnpol10;

	// Constructor
	public Uswdpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public String getPcamtind(){
		return this.pcamtind;
	}
	public String getSrcfund01(){
		return this.srcfund01;
	}
	public String getSrcfund02(){
		return this.srcfund02;
	}
	public String getSrcfund03(){
		return this.srcfund03;
	}
	public String getSrcfund04(){
		return this.srcfund04;
	}
	public String getSrcfund05(){
		return this.srcfund05;
	}
	public String getSrcfund06(){
		return this.srcfund06;
	}
	public String getSrcfund07(){
		return this.srcfund07;
	}
	public String getSrcfund08(){
		return this.srcfund08;
	}
	public String getSrcfund09(){
		return this.srcfund09;
	}
	public String getSrcfund10(){
		return this.srcfund10;
	}
	public String getSrcfund11(){
		return this.srcfund11;
	}
	public String getSrcfund12(){
		return this.srcfund12;
	}
	public String getSrcfund13(){
		return this.srcfund13;
	}
	public String getSrcfund14(){
		return this.srcfund14;
	}
	public String getSrcfund15(){
		return this.srcfund15;
	}
	public String getSrcfund16(){
		return this.srcfund16;
	}
	public String getSrcfund17(){
		return this.srcfund17;
	}
	public String getSrcfund18(){
		return this.srcfund18;
	}
	public String getSrcfund19(){
		return this.srcfund19;
	}
	public String getSrcfund20(){
		return this.srcfund20;
	}
	public String getScfndtyp01(){
		return this.scfndtyp01;
	}
	public String getScfndtyp02(){
		return this.scfndtyp02;
	}
	public String getScfndtyp03(){
		return this.scfndtyp03;
	}
	public String getScfndtyp04(){
		return this.scfndtyp04;
	}
	public String getScfndtyp05(){
		return this.scfndtyp05;
	}
	public String getScfndtyp06(){
		return this.scfndtyp06;
	}
	public String getScfndtyp07(){
		return this.scfndtyp07;
	}
	public String getScfndtyp08(){
		return this.scfndtyp08;
	}
	public String getScfndtyp09(){
		return this.scfndtyp09;
	}
	public String getScfndtyp10(){
		return this.scfndtyp10;
	}
	public String getScfndtyp11(){
		return this.scfndtyp11;
	}
	public String getScfndtyp12(){
		return this.scfndtyp12;
	}
	public String getScfndtyp13(){
		return this.scfndtyp13;
	}
	public String getScfndtyp14(){
		return this.scfndtyp14;
	}
	public String getScfndtyp15(){
		return this.scfndtyp15;
	}
	public String getScfndtyp16(){
		return this.scfndtyp16;
	}
	public String getScfndtyp17(){
		return this.scfndtyp17;
	}
	public String getScfndtyp18(){
		return this.scfndtyp18;
	}
	public String getScfndtyp19(){
		return this.scfndtyp19;
	}
	public String getScfndtyp20(){
		return this.scfndtyp20;
	}
	public String getScfndcur01(){
		return this.scfndcur01;
	}
	public String getScfndcur02(){
		return this.scfndcur02;
	}
	public String getScfndcur03(){
		return this.scfndcur03;
	}
	public String getScfndcur04(){
		return this.scfndcur04;
	}
	public String getScfndcur05(){
		return this.scfndcur05;
	}
	public String getScfndcur06(){
		return this.scfndcur06;
	}
	public String getScfndcur07(){
		return this.scfndcur07;
	}
	public String getScfndcur08(){
		return this.scfndcur08;
	}
	public String getScfndcur09(){
		return this.scfndcur09;
	}
	public String getScfndcur10(){
		return this.scfndcur10;
	}
	public String getScfndcur11(){
		return this.scfndcur11;
	}
	public String getScfndcur12(){
		return this.scfndcur12;
	}
	public String getScfndcur13(){
		return this.scfndcur13;
	}
	public String getScfndcur14(){
		return this.scfndcur14;
	}
	public String getScfndcur15(){
		return this.scfndcur15;
	}
	public String getScfndcur16(){
		return this.scfndcur16;
	}
	public String getScfndcur17(){
		return this.scfndcur17;
	}
	public String getScfndcur18(){
		return this.scfndcur18;
	}
	public String getScfndcur19(){
		return this.scfndcur19;
	}
	public String getScfndcur20(){
		return this.scfndcur20;
	}
	public BigDecimal getScprcamt01(){
		return this.scprcamt01;
	}
	public BigDecimal getScprcamt02(){
		return this.scprcamt02;
	}
	public BigDecimal getScprcamt03(){
		return this.scprcamt03;
	}
	public BigDecimal getScprcamt04(){
		return this.scprcamt04;
	}
	public BigDecimal getScprcamt05(){
		return this.scprcamt05;
	}
	public BigDecimal getScprcamt06(){
		return this.scprcamt06;
	}
	public BigDecimal getScprcamt07(){
		return this.scprcamt07;
	}
	public BigDecimal getScprcamt08(){
		return this.scprcamt08;
	}
	public BigDecimal getScprcamt09(){
		return this.scprcamt09;
	}
	public BigDecimal getScprcamt10(){
		return this.scprcamt10;
	}
	public BigDecimal getScprcamt11(){
		return this.scprcamt11;
	}
	public BigDecimal getScprcamt12(){
		return this.scprcamt12;
	}
	public BigDecimal getScprcamt13(){
		return this.scprcamt13;
	}
	public BigDecimal getScprcamt14(){
		return this.scprcamt14;
	}
	public BigDecimal getScprcamt15(){
		return this.scprcamt15;
	}
	public BigDecimal getScprcamt16(){
		return this.scprcamt16;
	}
	public BigDecimal getScprcamt17(){
		return this.scprcamt17;
	}
	public BigDecimal getScprcamt18(){
		return this.scprcamt18;
	}
	public BigDecimal getScprcamt19(){
		return this.scprcamt19;
	}
	public BigDecimal getScprcamt20(){
		return this.scprcamt20;
	}
	public BigDecimal getScestval01(){
		return this.scestval01;
	}
	public BigDecimal getScestval02(){
		return this.scestval02;
	}
	public BigDecimal getScestval03(){
		return this.scestval03;
	}
	public BigDecimal getScestval04(){
		return this.scestval04;
	}
	public BigDecimal getScestval05(){
		return this.scestval05;
	}
	public BigDecimal getScestval06(){
		return this.scestval06;
	}
	public BigDecimal getScestval07(){
		return this.scestval07;
	}
	public BigDecimal getScestval08(){
		return this.scestval08;
	}
	public BigDecimal getScestval09(){
		return this.scestval09;
	}
	public BigDecimal getScestval10(){
		return this.scestval10;
	}
	public BigDecimal getScestval11(){
		return this.scestval11;
	}
	public BigDecimal getScestval12(){
		return this.scestval12;
	}
	public BigDecimal getScestval13(){
		return this.scestval13;
	}
	public BigDecimal getScestval14(){
		return this.scestval14;
	}
	public BigDecimal getScestval15(){
		return this.scestval15;
	}
	public BigDecimal getScestval16(){
		return this.scestval16;
	}
	public BigDecimal getScestval17(){
		return this.scestval17;
	}
	public BigDecimal getScestval18(){
		return this.scestval18;
	}
	public BigDecimal getScestval19(){
		return this.scestval19;
	}
	public BigDecimal getScestval20(){
		return this.scestval20;
	}
	public BigDecimal getScactval01(){
		return this.scactval01;
	}
	public BigDecimal getScactval02(){
		return this.scactval02;
	}
	public BigDecimal getScactval03(){
		return this.scactval03;
	}
	public BigDecimal getScactval04(){
		return this.scactval04;
	}
	public BigDecimal getScactval05(){
		return this.scactval05;
	}
	public BigDecimal getScactval06(){
		return this.scactval06;
	}
	public BigDecimal getScactval07(){
		return this.scactval07;
	}
	public BigDecimal getScactval08(){
		return this.scactval08;
	}
	public BigDecimal getScactval09(){
		return this.scactval09;
	}
	public BigDecimal getScactval10(){
		return this.scactval10;
	}
	public BigDecimal getScactval11(){
		return this.scactval11;
	}
	public BigDecimal getScactval12(){
		return this.scactval12;
	}
	public BigDecimal getScactval13(){
		return this.scactval13;
	}
	public BigDecimal getScactval14(){
		return this.scactval14;
	}
	public BigDecimal getScactval15(){
		return this.scactval15;
	}
	public BigDecimal getScactval16(){
		return this.scactval16;
	}
	public BigDecimal getScactval17(){
		return this.scactval17;
	}
	public BigDecimal getScactval18(){
		return this.scactval18;
	}
	public BigDecimal getScactval19(){
		return this.scactval19;
	}
	public BigDecimal getScactval20(){
		return this.scactval20;
	}
	public String getTgtfund01(){
		return this.tgtfund01;
	}
	public String getTgtfund02(){
		return this.tgtfund02;
	}
	public String getTgtfund03(){
		return this.tgtfund03;
	}
	public String getTgtfund04(){
		return this.tgtfund04;
	}
	public String getTgtfund05(){
		return this.tgtfund05;
	}
	public String getTgtfund06(){
		return this.tgtfund06;
	}
	public String getTgtfund07(){
		return this.tgtfund07;
	}
	public String getTgtfund08(){
		return this.tgtfund08;
	}
	public String getTgtfund09(){
		return this.tgtfund09;
	}
	public String getTgtfund10(){
		return this.tgtfund10;
	}
	public String getTgfndtyp01(){
		return this.tgfndtyp01;
	}
	public String getTgfndtyp02(){
		return this.tgfndtyp02;
	}
	public String getTgfndtyp03(){
		return this.tgfndtyp03;
	}
	public String getTgfndtyp04(){
		return this.tgfndtyp04;
	}
	public String getTgfndtyp05(){
		return this.tgfndtyp05;
	}
	public String getTgfndtyp06(){
		return this.tgfndtyp06;
	}
	public String getTgfndtyp07(){
		return this.tgfndtyp07;
	}
	public String getTgfndtyp08(){
		return this.tgfndtyp08;
	}
	public String getTgfndtyp09(){
		return this.tgfndtyp09;
	}
	public String getTgfndtyp10(){
		return this.tgfndtyp10;
	}
	public String getTgfndcur01(){
		return this.tgfndcur01;
	}
	public String getTgfndcur02(){
		return this.tgfndcur02;
	}
	public String getTgfndcur03(){
		return this.tgfndcur03;
	}
	public String getTgfndcur04(){
		return this.tgfndcur04;
	}
	public String getTgfndcur05(){
		return this.tgfndcur05;
	}
	public String getTgfndcur06(){
		return this.tgfndcur06;
	}
	public String getTgfndcur07(){
		return this.tgfndcur07;
	}
	public String getTgfndcur08(){
		return this.tgfndcur08;
	}
	public String getTgfndcur09(){
		return this.tgfndcur09;
	}
	public String getTgfndcur10(){
		return this.tgfndcur10;
	}
	public BigDecimal getTgtprcnt01(){
		return this.tgtprcnt01;
	}
	public BigDecimal getTgtprcnt02(){
		return this.tgtprcnt02;
	}
	public BigDecimal getTgtprcnt03(){
		return this.tgtprcnt03;
	}
	public BigDecimal getTgtprcnt04(){
		return this.tgtprcnt04;
	}
	public BigDecimal getTgtprcnt05(){
		return this.tgtprcnt05;
	}
	public BigDecimal getTgtprcnt06(){
		return this.tgtprcnt06;
	}
	public BigDecimal getTgtprcnt07(){
		return this.tgtprcnt07;
	}
	public BigDecimal getTgtprcnt08(){
		return this.tgtprcnt08;
	}
	public BigDecimal getTgtprcnt09(){
		return this.tgtprcnt09;
	}
	public BigDecimal getTgtprcnt10(){
		return this.tgtprcnt10;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public String getOrswchfe(){
		return this.orswchfe;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return this.datime;
	}
	public String getScfndpol01(){
		return this.scfndpol01;
	}
	public String getScfndpol02(){
		return this.scfndpol02;
	}
	public String getScfndpol03(){
		return this.scfndpol03;
	}
	public String getScfndpol04(){
		return this.scfndpol04;
	}
	public String getScfndpol05(){
		return this.scfndpol05;
	}
	public String getScfndpol06(){
		return this.scfndpol06;
	}
	public String getScfndpol07(){
		return this.scfndpol07;
	}
	public String getScfndpol08(){
		return this.scfndpol08;
	}
	public String getScfndpol09(){
		return this.scfndpol09;
	}
	public String getScfndpol10(){
		return this.scfndpol10;
	}
	public String getScfndpol11(){
		return this.scfndpol11;
	}
	public String getScfndpol12(){
		return this.scfndpol12;
	}
	public String getScfndpol13(){
		return this.scfndpol13;
	}
	public String getScfndpol14(){
		return this.scfndpol14;
	}
	public String getScfndpol15(){
		return this.scfndpol15;
	}
	public String getScfndpol16(){
		return this.scfndpol16;
	}
	public String getScfndpol17(){
		return this.scfndpol17;
	}
	public String getScfndpol18(){
		return this.scfndpol18;
	}
	public String getScfndpol19(){
		return this.scfndpol19;
	}
	public String getScfndpol20(){
		return this.scfndpol20;
	}
	public String getTgfnpol01(){
		return this.tgfnpol01;
	}
	public String getTgfnpol02(){
		return this.tgfnpol02;
	}
	public String getTgfnpol03(){
		return this.tgfnpol03;
	}
	public String getTgfnpol04(){
		return this.tgfnpol04;
	}
	public String getTgfnpol05(){
		return this.tgfnpol05;
	}
	public String getTgfnpol06(){
		return this.tgfnpol06;
	}
	public String getTgfnpol07(){
		return this.tgfnpol07;
	}
	public String getTgfnpol08(){
		return this.tgfnpol08;
	}
	public String getTgfnpol09(){
		return this.tgfnpol09;
	}
	public String getTgfnpol10(){
		return this.tgfnpol10;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setPcamtind( String pcamtind ){
		 this.pcamtind = pcamtind;
	}
	public void setSrcfund01( String srcfund01 ){
		 this.srcfund01 = srcfund01;
	}
	public void setSrcfund02( String srcfund02 ){
		 this.srcfund02 = srcfund02;
	}
	public void setSrcfund03( String srcfund03 ){
		 this.srcfund03 = srcfund03;
	}
	public void setSrcfund04( String srcfund04 ){
		 this.srcfund04 = srcfund04;
	}
	public void setSrcfund05( String srcfund05 ){
		 this.srcfund05 = srcfund05;
	}
	public void setSrcfund06( String srcfund06 ){
		 this.srcfund06 = srcfund06;
	}
	public void setSrcfund07( String srcfund07 ){
		 this.srcfund07 = srcfund07;
	}
	public void setSrcfund08( String srcfund08 ){
		 this.srcfund08 = srcfund08;
	}
	public void setSrcfund09( String srcfund09 ){
		 this.srcfund09 = srcfund09;
	}
	public void setSrcfund10( String srcfund10 ){
		 this.srcfund10 = srcfund10;
	}
	public void setSrcfund11( String srcfund11 ){
		 this.srcfund11 = srcfund11;
	}
	public void setSrcfund12( String srcfund12 ){
		 this.srcfund12 = srcfund12;
	}
	public void setSrcfund13( String srcfund13 ){
		 this.srcfund13 = srcfund13;
	}
	public void setSrcfund14( String srcfund14 ){
		 this.srcfund14 = srcfund14;
	}
	public void setSrcfund15( String srcfund15 ){
		 this.srcfund15 = srcfund15;
	}
	public void setSrcfund16( String srcfund16 ){
		 this.srcfund16 = srcfund16;
	}
	public void setSrcfund17( String srcfund17 ){
		 this.srcfund17 = srcfund17;
	}
	public void setSrcfund18( String srcfund18 ){
		 this.srcfund18 = srcfund18;
	}
	public void setSrcfund19( String srcfund19 ){
		 this.srcfund19 = srcfund19;
	}
	public void setSrcfund20( String srcfund20 ){
		 this.srcfund20 = srcfund20;
	}
	public void setScfndtyp01( String scfndtyp01 ){
		 this.scfndtyp01 = scfndtyp01;
	}
	public void setScfndtyp02( String scfndtyp02 ){
		 this.scfndtyp02 = scfndtyp02;
	}
	public void setScfndtyp03( String scfndtyp03 ){
		 this.scfndtyp03 = scfndtyp03;
	}
	public void setScfndtyp04( String scfndtyp04 ){
		 this.scfndtyp04 = scfndtyp04;
	}
	public void setScfndtyp05( String scfndtyp05 ){
		 this.scfndtyp05 = scfndtyp05;
	}
	public void setScfndtyp06( String scfndtyp06 ){
		 this.scfndtyp06 = scfndtyp06;
	}
	public void setScfndtyp07( String scfndtyp07 ){
		 this.scfndtyp07 = scfndtyp07;
	}
	public void setScfndtyp08( String scfndtyp08 ){
		 this.scfndtyp08 = scfndtyp08;
	}
	public void setScfndtyp09( String scfndtyp09 ){
		 this.scfndtyp09 = scfndtyp09;
	}
	public void setScfndtyp10( String scfndtyp10 ){
		 this.scfndtyp10 = scfndtyp10;
	}
	public void setScfndtyp11( String scfndtyp11 ){
		 this.scfndtyp11 = scfndtyp11;
	}
	public void setScfndtyp12( String scfndtyp12 ){
		 this.scfndtyp12 = scfndtyp12;
	}
	public void setScfndtyp13( String scfndtyp13 ){
		 this.scfndtyp13 = scfndtyp13;
	}
	public void setScfndtyp14( String scfndtyp14 ){
		 this.scfndtyp14 = scfndtyp14;
	}
	public void setScfndtyp15( String scfndtyp15 ){
		 this.scfndtyp15 = scfndtyp15;
	}
	public void setScfndtyp16( String scfndtyp16 ){
		 this.scfndtyp16 = scfndtyp16;
	}
	public void setScfndtyp17( String scfndtyp17 ){
		 this.scfndtyp17 = scfndtyp17;
	}
	public void setScfndtyp18( String scfndtyp18 ){
		 this.scfndtyp18 = scfndtyp18;
	}
	public void setScfndtyp19( String scfndtyp19 ){
		 this.scfndtyp19 = scfndtyp19;
	}
	public void setScfndtyp20( String scfndtyp20 ){
		 this.scfndtyp20 = scfndtyp20;
	}
	public void setScfndcur01( String scfndcur01 ){
		 this.scfndcur01 = scfndcur01;
	}
	public void setScfndcur02( String scfndcur02 ){
		 this.scfndcur02 = scfndcur02;
	}
	public void setScfndcur03( String scfndcur03 ){
		 this.scfndcur03 = scfndcur03;
	}
	public void setScfndcur04( String scfndcur04 ){
		 this.scfndcur04 = scfndcur04;
	}
	public void setScfndcur05( String scfndcur05 ){
		 this.scfndcur05 = scfndcur05;
	}
	public void setScfndcur06( String scfndcur06 ){
		 this.scfndcur06 = scfndcur06;
	}
	public void setScfndcur07( String scfndcur07 ){
		 this.scfndcur07 = scfndcur07;
	}
	public void setScfndcur08( String scfndcur08 ){
		 this.scfndcur08 = scfndcur08;
	}
	public void setScfndcur09( String scfndcur09 ){
		 this.scfndcur09 = scfndcur09;
	}
	public void setScfndcur10( String scfndcur10 ){
		 this.scfndcur10 = scfndcur10;
	}
	public void setScfndcur11( String scfndcur11 ){
		 this.scfndcur11 = scfndcur11;
	}
	public void setScfndcur12( String scfndcur12 ){
		 this.scfndcur12 = scfndcur12;
	}
	public void setScfndcur13( String scfndcur13 ){
		 this.scfndcur13 = scfndcur13;
	}
	public void setScfndcur14( String scfndcur14 ){
		 this.scfndcur14 = scfndcur14;
	}
	public void setScfndcur15( String scfndcur15 ){
		 this.scfndcur15 = scfndcur15;
	}
	public void setScfndcur16( String scfndcur16 ){
		 this.scfndcur16 = scfndcur16;
	}
	public void setScfndcur17( String scfndcur17 ){
		 this.scfndcur17 = scfndcur17;
	}
	public void setScfndcur18( String scfndcur18 ){
		 this.scfndcur18 = scfndcur18;
	}
	public void setScfndcur19( String scfndcur19 ){
		 this.scfndcur19 = scfndcur19;
	}
	public void setScfndcur20( String scfndcur20 ){
		 this.scfndcur20 = scfndcur20;
	}
	public void setScprcamt01( BigDecimal scprcamt01 ){
		 this.scprcamt01 = scprcamt01;
	}
	public void setScprcamt02( BigDecimal scprcamt02 ){
		 this.scprcamt02 = scprcamt02;
	}
	public void setScprcamt03( BigDecimal scprcamt03 ){
		 this.scprcamt03 = scprcamt03;
	}
	public void setScprcamt04( BigDecimal scprcamt04 ){
		 this.scprcamt04 = scprcamt04;
	}
	public void setScprcamt05( BigDecimal scprcamt05 ){
		 this.scprcamt05 = scprcamt05;
	}
	public void setScprcamt06( BigDecimal scprcamt06 ){
		 this.scprcamt06 = scprcamt06;
	}
	public void setScprcamt07( BigDecimal scprcamt07 ){
		 this.scprcamt07 = scprcamt07;
	}
	public void setScprcamt08( BigDecimal scprcamt08 ){
		 this.scprcamt08 = scprcamt08;
	}
	public void setScprcamt09( BigDecimal scprcamt09 ){
		 this.scprcamt09 = scprcamt09;
	}
	public void setScprcamt10( BigDecimal scprcamt10 ){
		 this.scprcamt10 = scprcamt10;
	}
	public void setScprcamt11( BigDecimal scprcamt11 ){
		 this.scprcamt11 = scprcamt11;
	}
	public void setScprcamt12( BigDecimal scprcamt12 ){
		 this.scprcamt12 = scprcamt12;
	}
	public void setScprcamt13( BigDecimal scprcamt13 ){
		 this.scprcamt13 = scprcamt13;
	}
	public void setScprcamt14( BigDecimal scprcamt14 ){
		 this.scprcamt14 = scprcamt14;
	}
	public void setScprcamt15( BigDecimal scprcamt15 ){
		 this.scprcamt15 = scprcamt15;
	}
	public void setScprcamt16( BigDecimal scprcamt16 ){
		 this.scprcamt16 = scprcamt16;
	}
	public void setScprcamt17( BigDecimal scprcamt17 ){
		 this.scprcamt17 = scprcamt17;
	}
	public void setScprcamt18( BigDecimal scprcamt18 ){
		 this.scprcamt18 = scprcamt18;
	}
	public void setScprcamt19( BigDecimal scprcamt19 ){
		 this.scprcamt19 = scprcamt19;
	}
	public void setScprcamt20( BigDecimal scprcamt20 ){
		 this.scprcamt20 = scprcamt20;
	}
	public void setScestval01( BigDecimal scestval01 ){
		 this.scestval01 = scestval01;
	}
	public void setScestval02( BigDecimal scestval02 ){
		 this.scestval02 = scestval02;
	}
	public void setScestval03( BigDecimal scestval03 ){
		 this.scestval03 = scestval03;
	}
	public void setScestval04( BigDecimal scestval04 ){
		 this.scestval04 = scestval04;
	}
	public void setScestval05( BigDecimal scestval05 ){
		 this.scestval05 = scestval05;
	}
	public void setScestval06( BigDecimal scestval06 ){
		 this.scestval06 = scestval06;
	}
	public void setScestval07( BigDecimal scestval07 ){
		 this.scestval07 = scestval07;
	}
	public void setScestval08( BigDecimal scestval08 ){
		 this.scestval08 = scestval08;
	}
	public void setScestval09( BigDecimal scestval09 ){
		 this.scestval09 = scestval09;
	}
	public void setScestval10( BigDecimal scestval10 ){
		 this.scestval10 = scestval10;
	}
	public void setScestval11( BigDecimal scestval11 ){
		 this.scestval11 = scestval11;
	}
	public void setScestval12( BigDecimal scestval12 ){
		 this.scestval12 = scestval12;
	}
	public void setScestval13( BigDecimal scestval13 ){
		 this.scestval13 = scestval13;
	}
	public void setScestval14( BigDecimal scestval14 ){
		 this.scestval14 = scestval14;
	}
	public void setScestval15( BigDecimal scestval15 ){
		 this.scestval15 = scestval15;
	}
	public void setScestval16( BigDecimal scestval16 ){
		 this.scestval16 = scestval16;
	}
	public void setScestval17( BigDecimal scestval17 ){
		 this.scestval17 = scestval17;
	}
	public void setScestval18( BigDecimal scestval18 ){
		 this.scestval18 = scestval18;
	}
	public void setScestval19( BigDecimal scestval19 ){
		 this.scestval19 = scestval19;
	}
	public void setScestval20( BigDecimal scestval20 ){
		 this.scestval20 = scestval20;
	}
	public void setScactval01( BigDecimal scactval01 ){
		 this.scactval01 = scactval01;
	}
	public void setScactval02( BigDecimal scactval02 ){
		 this.scactval02 = scactval02;
	}
	public void setScactval03( BigDecimal scactval03 ){
		 this.scactval03 = scactval03;
	}
	public void setScactval04( BigDecimal scactval04 ){
		 this.scactval04 = scactval04;
	}
	public void setScactval05( BigDecimal scactval05 ){
		 this.scactval05 = scactval05;
	}
	public void setScactval06( BigDecimal scactval06 ){
		 this.scactval06 = scactval06;
	}
	public void setScactval07( BigDecimal scactval07 ){
		 this.scactval07 = scactval07;
	}
	public void setScactval08( BigDecimal scactval08 ){
		 this.scactval08 = scactval08;
	}
	public void setScactval09( BigDecimal scactval09 ){
		 this.scactval09 = scactval09;
	}
	public void setScactval10( BigDecimal scactval10 ){
		 this.scactval10 = scactval10;
	}
	public void setScactval11( BigDecimal scactval11 ){
		 this.scactval11 = scactval11;
	}
	public void setScactval12( BigDecimal scactval12 ){
		 this.scactval12 = scactval12;
	}
	public void setScactval13( BigDecimal scactval13 ){
		 this.scactval13 = scactval13;
	}
	public void setScactval14( BigDecimal scactval14 ){
		 this.scactval14 = scactval14;
	}
	public void setScactval15( BigDecimal scactval15 ){
		 this.scactval15 = scactval15;
	}
	public void setScactval16( BigDecimal scactval16 ){
		 this.scactval16 = scactval16;
	}
	public void setScactval17( BigDecimal scactval17 ){
		 this.scactval17 = scactval17;
	}
	public void setScactval18( BigDecimal scactval18 ){
		 this.scactval18 = scactval18;
	}
	public void setScactval19( BigDecimal scactval19 ){
		 this.scactval19 = scactval19;
	}
	public void setScactval20( BigDecimal scactval20 ){
		 this.scactval20 = scactval20;
	}
	public void setTgtfund01( String tgtfund01 ){
		 this.tgtfund01 = tgtfund01;
	}
	public void setTgtfund02( String tgtfund02 ){
		 this.tgtfund02 = tgtfund02;
	}
	public void setTgtfund03( String tgtfund03 ){
		 this.tgtfund03 = tgtfund03;
	}
	public void setTgtfund04( String tgtfund04 ){
		 this.tgtfund04 = tgtfund04;
	}
	public void setTgtfund05( String tgtfund05 ){
		 this.tgtfund05 = tgtfund05;
	}
	public void setTgtfund06( String tgtfund06 ){
		 this.tgtfund06 = tgtfund06;
	}
	public void setTgtfund07( String tgtfund07 ){
		 this.tgtfund07 = tgtfund07;
	}
	public void setTgtfund08( String tgtfund08 ){
		 this.tgtfund08 = tgtfund08;
	}
	public void setTgtfund09( String tgtfund09 ){
		 this.tgtfund09 = tgtfund09;
	}
	public void setTgtfund10( String tgtfund10 ){
		 this.tgtfund10 = tgtfund10;
	}
	public void setTgfndtyp01( String tgfndtyp01 ){
		 this.tgfndtyp01 = tgfndtyp01;
	}
	public void setTgfndtyp02( String tgfndtyp02 ){
		 this.tgfndtyp02 = tgfndtyp02;
	}
	public void setTgfndtyp03( String tgfndtyp03 ){
		 this.tgfndtyp03 = tgfndtyp03;
	}
	public void setTgfndtyp04( String tgfndtyp04 ){
		 this.tgfndtyp04 = tgfndtyp04;
	}
	public void setTgfndtyp05( String tgfndtyp05 ){
		 this.tgfndtyp05 = tgfndtyp05;
	}
	public void setTgfndtyp06( String tgfndtyp06 ){
		 this.tgfndtyp06 = tgfndtyp06;
	}
	public void setTgfndtyp07( String tgfndtyp07 ){
		 this.tgfndtyp07 = tgfndtyp07;
	}
	public void setTgfndtyp08( String tgfndtyp08 ){
		 this.tgfndtyp08 = tgfndtyp08;
	}
	public void setTgfndtyp09( String tgfndtyp09 ){
		 this.tgfndtyp09 = tgfndtyp09;
	}
	public void setTgfndtyp10( String tgfndtyp10 ){
		 this.tgfndtyp10 = tgfndtyp10;
	}
	public void setTgfndcur01( String tgfndcur01 ){
		 this.tgfndcur01 = tgfndcur01;
	}
	public void setTgfndcur02( String tgfndcur02 ){
		 this.tgfndcur02 = tgfndcur02;
	}
	public void setTgfndcur03( String tgfndcur03 ){
		 this.tgfndcur03 = tgfndcur03;
	}
	public void setTgfndcur04( String tgfndcur04 ){
		 this.tgfndcur04 = tgfndcur04;
	}
	public void setTgfndcur05( String tgfndcur05 ){
		 this.tgfndcur05 = tgfndcur05;
	}
	public void setTgfndcur06( String tgfndcur06 ){
		 this.tgfndcur06 = tgfndcur06;
	}
	public void setTgfndcur07( String tgfndcur07 ){
		 this.tgfndcur07 = tgfndcur07;
	}
	public void setTgfndcur08( String tgfndcur08 ){
		 this.tgfndcur08 = tgfndcur08;
	}
	public void setTgfndcur09( String tgfndcur09 ){
		 this.tgfndcur09 = tgfndcur09;
	}
	public void setTgfndcur10( String tgfndcur10 ){
		 this.tgfndcur10 = tgfndcur10;
	}
	public void setTgtprcnt01( BigDecimal tgtprcnt01 ){
		 this.tgtprcnt01 = tgtprcnt01;
	}
	public void setTgtprcnt02( BigDecimal tgtprcnt02 ){
		 this.tgtprcnt02 = tgtprcnt02;
	}
	public void setTgtprcnt03( BigDecimal tgtprcnt03 ){
		 this.tgtprcnt03 = tgtprcnt03;
	}
	public void setTgtprcnt04( BigDecimal tgtprcnt04 ){
		 this.tgtprcnt04 = tgtprcnt04;
	}
	public void setTgtprcnt05( BigDecimal tgtprcnt05 ){
		 this.tgtprcnt05 = tgtprcnt05;
	}
	public void setTgtprcnt06( BigDecimal tgtprcnt06 ){
		 this.tgtprcnt06 = tgtprcnt06;
	}
	public void setTgtprcnt07( BigDecimal tgtprcnt07 ){
		 this.tgtprcnt07 = tgtprcnt07;
	}
	public void setTgtprcnt08( BigDecimal tgtprcnt08 ){
		 this.tgtprcnt08 = tgtprcnt08;
	}
	public void setTgtprcnt09( BigDecimal tgtprcnt09 ){
		 this.tgtprcnt09 = tgtprcnt09;
	}
	public void setTgtprcnt10( BigDecimal tgtprcnt10 ){
		 this.tgtprcnt10 = tgtprcnt10;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setOrswchfe( String orswchfe ){
		 this.orswchfe = orswchfe;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		 this.datime = datime;
	}
	public void setScfndpol01( String scfndpol01 ){
		 this.scfndpol01 = scfndpol01;
	}
	public void setScfndpol02( String scfndpol02 ){
		 this.scfndpol02 = scfndpol02;
	}
	public void setScfndpol03( String scfndpol03 ){
		 this.scfndpol03 = scfndpol03;
	}
	public void setScfndpol04( String scfndpol04 ){
		 this.scfndpol04 = scfndpol04;
	}
	public void setScfndpol05( String scfndpol05 ){
		 this.scfndpol05 = scfndpol05;
	}
	public void setScfndpol06( String scfndpol06 ){
		 this.scfndpol06 = scfndpol06;
	}
	public void setScfndpol07( String scfndpol07 ){
		 this.scfndpol07 = scfndpol07;
	}
	public void setScfndpol08( String scfndpol08 ){
		 this.scfndpol08 = scfndpol08;
	}
	public void setScfndpol09( String scfndpol09 ){
		 this.scfndpol09 = scfndpol09;
	}
	public void setScfndpol10( String scfndpol10 ){
		 this.scfndpol10 = scfndpol10;
	}
	public void setScfndpol11( String scfndpol11 ){
		 this.scfndpol11 = scfndpol11;
	}
	public void setScfndpol12( String scfndpol12 ){
		 this.scfndpol12 = scfndpol12;
	}
	public void setScfndpol13( String scfndpol13 ){
		 this.scfndpol13 = scfndpol13;
	}
	public void setScfndpol14( String scfndpol14 ){
		 this.scfndpol14 = scfndpol14;
	}
	public void setScfndpol15( String scfndpol15 ){
		 this.scfndpol15 = scfndpol15;
	}
	public void setScfndpol16( String scfndpol16 ){
		 this.scfndpol16 = scfndpol16;
	}
	public void setScfndpol17( String scfndpol17 ){
		 this.scfndpol17 = scfndpol17;
	}
	public void setScfndpol18( String scfndpol18 ){
		 this.scfndpol18 = scfndpol18;
	}
	public void setScfndpol19( String scfndpol19 ){
		 this.scfndpol19 = scfndpol19;
	}
	public void setScfndpol20( String scfndpol20 ){
		 this.scfndpol20 = scfndpol20;
	}
	public void setTgfnpol01( String tgfnpol01 ){
		 this.tgfnpol01 = tgfnpol01;
	}
	public void setTgfnpol02( String tgfnpol02 ){
		 this.tgfnpol02 = tgfnpol02;
	}
	public void setTgfnpol03( String tgfnpol03 ){
		 this.tgfnpol03 = tgfnpol03;
	}
	public void setTgfnpol04( String tgfnpol04 ){
		 this.tgfnpol04 = tgfnpol04;
	}
	public void setTgfnpol05( String tgfnpol05 ){
		 this.tgfnpol05 = tgfnpol05;
	}
	public void setTgfnpol06( String tgfnpol06 ){
		 this.tgfnpol06 = tgfnpol06;
	}
	public void setTgfnpol07( String tgfnpol07 ){
		 this.tgfnpol07 = tgfnpol07;
	}
	public void setTgfnpol08( String tgfnpol08 ){
		 this.tgfnpol08 = tgfnpol08;
	}
	public void setTgfnpol09( String tgfnpol09 ){
		 this.tgfnpol09 = tgfnpol09;
	}
	public void setTgfnpol10( String tgfnpol10 ){
		 this.tgfnpol10 = tgfnpol10;
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("PCAMTIND:		");
		output.append(getPcamtind());
		output.append("\r\n");
		output.append("SRCFUND01:		");
		output.append(getSrcfund01());
		output.append("\r\n");
		output.append("SRCFUND02:		");
		output.append(getSrcfund02());
		output.append("\r\n");
		output.append("SRCFUND03:		");
		output.append(getSrcfund03());
		output.append("\r\n");
		output.append("SRCFUND04:		");
		output.append(getSrcfund04());
		output.append("\r\n");
		output.append("SRCFUND05:		");
		output.append(getSrcfund05());
		output.append("\r\n");
		output.append("SRCFUND06:		");
		output.append(getSrcfund06());
		output.append("\r\n");
		output.append("SRCFUND07:		");
		output.append(getSrcfund07());
		output.append("\r\n");
		output.append("SRCFUND08:		");
		output.append(getSrcfund08());
		output.append("\r\n");
		output.append("SRCFUND09:		");
		output.append(getSrcfund09());
		output.append("\r\n");
		output.append("SRCFUND10:		");
		output.append(getSrcfund10());
		output.append("\r\n");
		output.append("SRCFUND11:		");
		output.append(getSrcfund11());
		output.append("\r\n");
		output.append("SRCFUND12:		");
		output.append(getSrcfund12());
		output.append("\r\n");
		output.append("SRCFUND13:		");
		output.append(getSrcfund13());
		output.append("\r\n");
		output.append("SRCFUND14:		");
		output.append(getSrcfund14());
		output.append("\r\n");
		output.append("SRCFUND15:		");
		output.append(getSrcfund15());
		output.append("\r\n");
		output.append("SRCFUND16:		");
		output.append(getSrcfund16());
		output.append("\r\n");
		output.append("SRCFUND17:		");
		output.append(getSrcfund17());
		output.append("\r\n");
		output.append("SRCFUND18:		");
		output.append(getSrcfund18());
		output.append("\r\n");
		output.append("SRCFUND19:		");
		output.append(getSrcfund19());
		output.append("\r\n");
		output.append("SRCFUND20:		");
		output.append(getSrcfund20());
		output.append("\r\n");
		output.append("SCFNDTYP01:		");
		output.append(getScfndtyp01());
		output.append("\r\n");
		output.append("SCFNDTYP02:		");
		output.append(getScfndtyp02());
		output.append("\r\n");
		output.append("SCFNDTYP03:		");
		output.append(getScfndtyp03());
		output.append("\r\n");
		output.append("SCFNDTYP04:		");
		output.append(getScfndtyp04());
		output.append("\r\n");
		output.append("SCFNDTYP05:		");
		output.append(getScfndtyp05());
		output.append("\r\n");
		output.append("SCFNDTYP06:		");
		output.append(getScfndtyp06());
		output.append("\r\n");
		output.append("SCFNDTYP07:		");
		output.append(getScfndtyp07());
		output.append("\r\n");
		output.append("SCFNDTYP08:		");
		output.append(getScfndtyp08());
		output.append("\r\n");
		output.append("SCFNDTYP09:		");
		output.append(getScfndtyp09());
		output.append("\r\n");
		output.append("SCFNDTYP10:		");
		output.append(getScfndtyp10());
		output.append("\r\n");
		output.append("SCFNDTYP11:		");
		output.append(getScfndtyp11());
		output.append("\r\n");
		output.append("SCFNDTYP12:		");
		output.append(getScfndtyp12());
		output.append("\r\n");
		output.append("SCFNDTYP13:		");
		output.append(getScfndtyp13());
		output.append("\r\n");
		output.append("SCFNDTYP14:		");
		output.append(getScfndtyp14());
		output.append("\r\n");
		output.append("SCFNDTYP15:		");
		output.append(getScfndtyp15());
		output.append("\r\n");
		output.append("SCFNDTYP16:		");
		output.append(getScfndtyp16());
		output.append("\r\n");
		output.append("SCFNDTYP17:		");
		output.append(getScfndtyp17());
		output.append("\r\n");
		output.append("SCFNDTYP18:		");
		output.append(getScfndtyp18());
		output.append("\r\n");
		output.append("SCFNDTYP19:		");
		output.append(getScfndtyp19());
		output.append("\r\n");
		output.append("SCFNDTYP20:		");
		output.append(getScfndtyp20());
		output.append("\r\n");
		output.append("SCFNDCUR01:		");
		output.append(getScfndcur01());
		output.append("\r\n");
		output.append("SCFNDCUR02:		");
		output.append(getScfndcur02());
		output.append("\r\n");
		output.append("SCFNDCUR03:		");
		output.append(getScfndcur03());
		output.append("\r\n");
		output.append("SCFNDCUR04:		");
		output.append(getScfndcur04());
		output.append("\r\n");
		output.append("SCFNDCUR05:		");
		output.append(getScfndcur05());
		output.append("\r\n");
		output.append("SCFNDCUR06:		");
		output.append(getScfndcur06());
		output.append("\r\n");
		output.append("SCFNDCUR07:		");
		output.append(getScfndcur07());
		output.append("\r\n");
		output.append("SCFNDCUR08:		");
		output.append(getScfndcur08());
		output.append("\r\n");
		output.append("SCFNDCUR09:		");
		output.append(getScfndcur09());
		output.append("\r\n");
		output.append("SCFNDCUR10:		");
		output.append(getScfndcur10());
		output.append("\r\n");
		output.append("SCFNDCUR11:		");
		output.append(getScfndcur11());
		output.append("\r\n");
		output.append("SCFNDCUR12:		");
		output.append(getScfndcur12());
		output.append("\r\n");
		output.append("SCFNDCUR13:		");
		output.append(getScfndcur13());
		output.append("\r\n");
		output.append("SCFNDCUR14:		");
		output.append(getScfndcur14());
		output.append("\r\n");
		output.append("SCFNDCUR15:		");
		output.append(getScfndcur15());
		output.append("\r\n");
		output.append("SCFNDCUR16:		");
		output.append(getScfndcur16());
		output.append("\r\n");
		output.append("SCFNDCUR17:		");
		output.append(getScfndcur17());
		output.append("\r\n");
		output.append("SCFNDCUR18:		");
		output.append(getScfndcur18());
		output.append("\r\n");
		output.append("SCFNDCUR19:		");
		output.append(getScfndcur19());
		output.append("\r\n");
		output.append("SCFNDCUR20:		");
		output.append(getScfndcur20());
		output.append("\r\n");
		output.append("SCPRCAMT01:		");
		output.append(getScprcamt01());
		output.append("\r\n");
		output.append("SCPRCAMT02:		");
		output.append(getScprcamt02());
		output.append("\r\n");
		output.append("SCPRCAMT03:		");
		output.append(getScprcamt03());
		output.append("\r\n");
		output.append("SCPRCAMT04:		");
		output.append(getScprcamt04());
		output.append("\r\n");
		output.append("SCPRCAMT05:		");
		output.append(getScprcamt05());
		output.append("\r\n");
		output.append("SCPRCAMT06:		");
		output.append(getScprcamt06());
		output.append("\r\n");
		output.append("SCPRCAMT07:		");
		output.append(getScprcamt07());
		output.append("\r\n");
		output.append("SCPRCAMT08:		");
		output.append(getScprcamt08());
		output.append("\r\n");
		output.append("SCPRCAMT09:		");
		output.append(getScprcamt09());
		output.append("\r\n");
		output.append("SCPRCAMT10:		");
		output.append(getScprcamt10());
		output.append("\r\n");
		output.append("SCPRCAMT11:		");
		output.append(getScprcamt11());
		output.append("\r\n");
		output.append("SCPRCAMT12:		");
		output.append(getScprcamt12());
		output.append("\r\n");
		output.append("SCPRCAMT13:		");
		output.append(getScprcamt13());
		output.append("\r\n");
		output.append("SCPRCAMT14:		");
		output.append(getScprcamt14());
		output.append("\r\n");
		output.append("SCPRCAMT15:		");
		output.append(getScprcamt15());
		output.append("\r\n");
		output.append("SCPRCAMT16:		");
		output.append(getScprcamt16());
		output.append("\r\n");
		output.append("SCPRCAMT17:		");
		output.append(getScprcamt17());
		output.append("\r\n");
		output.append("SCPRCAMT18:		");
		output.append(getScprcamt18());
		output.append("\r\n");
		output.append("SCPRCAMT19:		");
		output.append(getScprcamt19());
		output.append("\r\n");
		output.append("SCPRCAMT20:		");
		output.append(getScprcamt20());
		output.append("\r\n");
		output.append("SCESTVAL01:		");
		output.append(getScestval01());
		output.append("\r\n");
		output.append("SCESTVAL02:		");
		output.append(getScestval02());
		output.append("\r\n");
		output.append("SCESTVAL03:		");
		output.append(getScestval03());
		output.append("\r\n");
		output.append("SCESTVAL04:		");
		output.append(getScestval04());
		output.append("\r\n");
		output.append("SCESTVAL05:		");
		output.append(getScestval05());
		output.append("\r\n");
		output.append("SCESTVAL06:		");
		output.append(getScestval06());
		output.append("\r\n");
		output.append("SCESTVAL07:		");
		output.append(getScestval07());
		output.append("\r\n");
		output.append("SCESTVAL08:		");
		output.append(getScestval08());
		output.append("\r\n");
		output.append("SCESTVAL09:		");
		output.append(getScestval09());
		output.append("\r\n");
		output.append("SCESTVAL10:		");
		output.append(getScestval10());
		output.append("\r\n");
		output.append("SCESTVAL11:		");
		output.append(getScestval11());
		output.append("\r\n");
		output.append("SCESTVAL12:		");
		output.append(getScestval12());
		output.append("\r\n");
		output.append("SCESTVAL13:		");
		output.append(getScestval13());
		output.append("\r\n");
		output.append("SCESTVAL14:		");
		output.append(getScestval14());
		output.append("\r\n");
		output.append("SCESTVAL15:		");
		output.append(getScestval15());
		output.append("\r\n");
		output.append("SCESTVAL16:		");
		output.append(getScestval16());
		output.append("\r\n");
		output.append("SCESTVAL17:		");
		output.append(getScestval17());
		output.append("\r\n");
		output.append("SCESTVAL18:		");
		output.append(getScestval18());
		output.append("\r\n");
		output.append("SCESTVAL19:		");
		output.append(getScestval19());
		output.append("\r\n");
		output.append("SCESTVAL20:		");
		output.append(getScestval20());
		output.append("\r\n");
		output.append("SCACTVAL01:		");
		output.append(getScactval01());
		output.append("\r\n");
		output.append("SCACTVAL02:		");
		output.append(getScactval02());
		output.append("\r\n");
		output.append("SCACTVAL03:		");
		output.append(getScactval03());
		output.append("\r\n");
		output.append("SCACTVAL04:		");
		output.append(getScactval04());
		output.append("\r\n");
		output.append("SCACTVAL05:		");
		output.append(getScactval05());
		output.append("\r\n");
		output.append("SCACTVAL06:		");
		output.append(getScactval06());
		output.append("\r\n");
		output.append("SCACTVAL07:		");
		output.append(getScactval07());
		output.append("\r\n");
		output.append("SCACTVAL08:		");
		output.append(getScactval08());
		output.append("\r\n");
		output.append("SCACTVAL09:		");
		output.append(getScactval09());
		output.append("\r\n");
		output.append("SCACTVAL10:		");
		output.append(getScactval10());
		output.append("\r\n");
		output.append("SCACTVAL11:		");
		output.append(getScactval11());
		output.append("\r\n");
		output.append("SCACTVAL12:		");
		output.append(getScactval12());
		output.append("\r\n");
		output.append("SCACTVAL13:		");
		output.append(getScactval13());
		output.append("\r\n");
		output.append("SCACTVAL14:		");
		output.append(getScactval14());
		output.append("\r\n");
		output.append("SCACTVAL15:		");
		output.append(getScactval15());
		output.append("\r\n");
		output.append("SCACTVAL16:		");
		output.append(getScactval16());
		output.append("\r\n");
		output.append("SCACTVAL17:		");
		output.append(getScactval17());
		output.append("\r\n");
		output.append("SCACTVAL18:		");
		output.append(getScactval18());
		output.append("\r\n");
		output.append("SCACTVAL19:		");
		output.append(getScactval19());
		output.append("\r\n");
		output.append("SCACTVAL20:		");
		output.append(getScactval20());
		output.append("\r\n");
		output.append("TGTFUND01:		");
		output.append(getTgtfund01());
		output.append("\r\n");
		output.append("TGTFUND02:		");
		output.append(getTgtfund02());
		output.append("\r\n");
		output.append("TGTFUND03:		");
		output.append(getTgtfund03());
		output.append("\r\n");
		output.append("TGTFUND04:		");
		output.append(getTgtfund04());
		output.append("\r\n");
		output.append("TGTFUND05:		");
		output.append(getTgtfund05());
		output.append("\r\n");
		output.append("TGTFUND06:		");
		output.append(getTgtfund06());
		output.append("\r\n");
		output.append("TGTFUND07:		");
		output.append(getTgtfund07());
		output.append("\r\n");
		output.append("TGTFUND08:		");
		output.append(getTgtfund08());
		output.append("\r\n");
		output.append("TGTFUND09:		");
		output.append(getTgtfund09());
		output.append("\r\n");
		output.append("TGTFUND10:		");
		output.append(getTgtfund10());
		output.append("\r\n");
		output.append("TGFNDTYP01:		");
		output.append(getTgfndtyp01());
		output.append("\r\n");
		output.append("TGFNDTYP02:		");
		output.append(getTgfndtyp02());
		output.append("\r\n");
		output.append("TGFNDTYP03:		");
		output.append(getTgfndtyp03());
		output.append("\r\n");
		output.append("TGFNDTYP04:		");
		output.append(getTgfndtyp04());
		output.append("\r\n");
		output.append("TGFNDTYP05:		");
		output.append(getTgfndtyp05());
		output.append("\r\n");
		output.append("TGFNDTYP06:		");
		output.append(getTgfndtyp06());
		output.append("\r\n");
		output.append("TGFNDTYP07:		");
		output.append(getTgfndtyp07());
		output.append("\r\n");
		output.append("TGFNDTYP08:		");
		output.append(getTgfndtyp08());
		output.append("\r\n");
		output.append("TGFNDTYP09:		");
		output.append(getTgfndtyp09());
		output.append("\r\n");
		output.append("TGFNDTYP10:		");
		output.append(getTgfndtyp10());
		output.append("\r\n");
		output.append("TGFNDCUR01:		");
		output.append(getTgfndcur01());
		output.append("\r\n");
		output.append("TGFNDCUR02:		");
		output.append(getTgfndcur02());
		output.append("\r\n");
		output.append("TGFNDCUR03:		");
		output.append(getTgfndcur03());
		output.append("\r\n");
		output.append("TGFNDCUR04:		");
		output.append(getTgfndcur04());
		output.append("\r\n");
		output.append("TGFNDCUR05:		");
		output.append(getTgfndcur05());
		output.append("\r\n");
		output.append("TGFNDCUR06:		");
		output.append(getTgfndcur06());
		output.append("\r\n");
		output.append("TGFNDCUR07:		");
		output.append(getTgfndcur07());
		output.append("\r\n");
		output.append("TGFNDCUR08:		");
		output.append(getTgfndcur08());
		output.append("\r\n");
		output.append("TGFNDCUR09:		");
		output.append(getTgfndcur09());
		output.append("\r\n");
		output.append("TGFNDCUR10:		");
		output.append(getTgfndcur10());
		output.append("\r\n");
		output.append("TGTPRCNT01:		");
		output.append(getTgtprcnt01());
		output.append("\r\n");
		output.append("TGTPRCNT02:		");
		output.append(getTgtprcnt02());
		output.append("\r\n");
		output.append("TGTPRCNT03:		");
		output.append(getTgtprcnt03());
		output.append("\r\n");
		output.append("TGTPRCNT04:		");
		output.append(getTgtprcnt04());
		output.append("\r\n");
		output.append("TGTPRCNT05:		");
		output.append(getTgtprcnt05());
		output.append("\r\n");
		output.append("TGTPRCNT06:		");
		output.append(getTgtprcnt06());
		output.append("\r\n");
		output.append("TGTPRCNT07:		");
		output.append(getTgtprcnt07());
		output.append("\r\n");
		output.append("TGTPRCNT08:		");
		output.append(getTgtprcnt08());
		output.append("\r\n");
		output.append("TGTPRCNT09:		");
		output.append(getTgtprcnt09());
		output.append("\r\n");
		output.append("TGTPRCNT10:		");
		output.append(getTgtprcnt10());
		output.append("\r\n");
		output.append("EFFDATE:		");
		output.append(getEffdate());
		output.append("\r\n");
		output.append("ORSWCHFE:		");
		output.append(getOrswchfe());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");
		output.append("SCFNDPOL01:		");
		output.append(getScfndpol01());
		output.append("\r\n");
		output.append("SCFNDPOL02:		");
		output.append(getScfndpol02());
		output.append("\r\n");
		output.append("SCFNDPOL03:		");
		output.append(getScfndpol03());
		output.append("\r\n");
		output.append("SCFNDPOL04:		");
		output.append(getScfndpol04());
		output.append("\r\n");
		output.append("SCFNDPOL05:		");
		output.append(getScfndpol05());
		output.append("\r\n");
		output.append("SCFNDPOL06:		");
		output.append(getScfndpol06());
		output.append("\r\n");
		output.append("SCFNDPOL07:		");
		output.append(getScfndpol07());
		output.append("\r\n");
		output.append("SCFNDPOL08:		");
		output.append(getScfndpol08());
		output.append("\r\n");
		output.append("SCFNDPOL09:		");
		output.append(getScfndpol09());
		output.append("\r\n");
		output.append("SCFNDPOL10:		");
		output.append(getScfndpol10());
		output.append("\r\n");
		output.append("SCFNDPOL11:		");
		output.append(getScfndpol11());
		output.append("\r\n");
		output.append("SCFNDPOL12:		");
		output.append(getScfndpol12());
		output.append("\r\n");
		output.append("SCFNDPOL13:		");
		output.append(getScfndpol13());
		output.append("\r\n");
		output.append("SCFNDPOL14:		");
		output.append(getScfndpol14());
		output.append("\r\n");
		output.append("SCFNDPOL15:		");
		output.append(getScfndpol15());
		output.append("\r\n");
		output.append("SCFNDPOL16:		");
		output.append(getScfndpol16());
		output.append("\r\n");
		output.append("SCFNDPOL17:		");
		output.append(getScfndpol17());
		output.append("\r\n");
		output.append("SCFNDPOL18:		");
		output.append(getScfndpol18());
		output.append("\r\n");
		output.append("SCFNDPOL19:		");
		output.append(getScfndpol19());
		output.append("\r\n");
		output.append("SCFNDPOL20:		");
		output.append(getScfndpol20());
		output.append("\r\n");
		output.append("TGFNPOL01:		");
		output.append(getTgfnpol01());
		output.append("\r\n");
		output.append("TGFNPOL02:		");
		output.append(getTgfnpol02());
		output.append("\r\n");
		output.append("TGFNPOL03:		");
		output.append(getTgfnpol03());
		output.append("\r\n");
		output.append("TGFNPOL04:		");
		output.append(getTgfnpol04());
		output.append("\r\n");
		output.append("TGFNPOL05:		");
		output.append(getTgfnpol05());
		output.append("\r\n");
		output.append("TGFNPOL06:		");
		output.append(getTgfnpol06());
		output.append("\r\n");
		output.append("TGFNPOL07:		");
		output.append(getTgfnpol07());
		output.append("\r\n");
		output.append("TGFNPOL08:		");
		output.append(getTgfnpol08());
		output.append("\r\n");
		output.append("TGFNPOL09:		");
		output.append(getTgfnpol09());
		output.append("\r\n");
		output.append("TGFNPOL10:		");
		output.append(getTgfnpol10());
		output.append("\r\n");

		return output.toString();
	}

}
