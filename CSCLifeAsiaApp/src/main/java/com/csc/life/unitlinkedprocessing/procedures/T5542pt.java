/*
 * File: T5542pt.java
 * Date: 30 August 2009 2:22:19
 * Author: Quipoz Limited
 * 
 * Class transformed from T5542PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5542.
*
*
*****************************************************************
* </pre>
*/
public class T5542pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5542rec t5542rec = new T5542rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5542pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

	/**
	* <pre>
	**********************                                    <D96NUM>
	* </pre>
	*/
protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void moveFields020()
	{
		/*                                                         <D96NUM>*/
		t5542rec.t5542Rec.set(tablistrec.generalArea);
		/*                                                         <D96NUM>*/
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5542rec.billfreq01);
		generalCopyLinesInner.fieldNo008.set(t5542rec.wdlFreq01);
		generalCopyLinesInner.fieldNo009.set(t5542rec.wdlAmount01);
		generalCopyLinesInner.fieldNo010.set(t5542rec.wdrem01);
		generalCopyLinesInner.fieldNo011.set(t5542rec.ffamt01);
		generalCopyLinesInner.fieldNo012.set(t5542rec.feepc01);
		generalCopyLinesInner.fieldNo013.set(t5542rec.feemin01);
		generalCopyLinesInner.fieldNo014.set(t5542rec.feemax01);
		generalCopyLinesInner.fieldNo015.set(t5542rec.billfreq02);
		generalCopyLinesInner.fieldNo016.set(t5542rec.wdlFreq02);
		generalCopyLinesInner.fieldNo017.set(t5542rec.wdlAmount02);
		generalCopyLinesInner.fieldNo018.set(t5542rec.wdrem02);
		generalCopyLinesInner.fieldNo019.set(t5542rec.ffamt02);
		generalCopyLinesInner.fieldNo020.set(t5542rec.feepc02);
		generalCopyLinesInner.fieldNo021.set(t5542rec.feemin02);
		generalCopyLinesInner.fieldNo022.set(t5542rec.feemax02);
		generalCopyLinesInner.fieldNo023.set(t5542rec.billfreq03);
		generalCopyLinesInner.fieldNo024.set(t5542rec.wdlFreq03);
		generalCopyLinesInner.fieldNo025.set(t5542rec.wdlAmount03);
		generalCopyLinesInner.fieldNo026.set(t5542rec.wdrem03);
		generalCopyLinesInner.fieldNo027.set(t5542rec.ffamt03);
		generalCopyLinesInner.fieldNo028.set(t5542rec.feepc03);
		generalCopyLinesInner.fieldNo029.set(t5542rec.feemin03);
		generalCopyLinesInner.fieldNo030.set(t5542rec.feemax03);
		generalCopyLinesInner.fieldNo031.set(t5542rec.billfreq04);
		generalCopyLinesInner.fieldNo032.set(t5542rec.wdlFreq04);
		generalCopyLinesInner.fieldNo033.set(t5542rec.wdlAmount04);
		generalCopyLinesInner.fieldNo034.set(t5542rec.wdrem04);
		generalCopyLinesInner.fieldNo035.set(t5542rec.ffamt04);
		generalCopyLinesInner.fieldNo036.set(t5542rec.feepc04);
		generalCopyLinesInner.fieldNo037.set(t5542rec.feemin04);
		generalCopyLinesInner.fieldNo038.set(t5542rec.feemax04);
		generalCopyLinesInner.fieldNo039.set(t5542rec.billfreq05);
		generalCopyLinesInner.fieldNo040.set(t5542rec.wdlFreq05);
		generalCopyLinesInner.fieldNo041.set(t5542rec.wdlAmount05);
		generalCopyLinesInner.fieldNo042.set(t5542rec.wdrem05);
		generalCopyLinesInner.fieldNo043.set(t5542rec.ffamt05);
		generalCopyLinesInner.fieldNo044.set(t5542rec.feepc05);
		generalCopyLinesInner.fieldNo045.set(t5542rec.feemin05);
		generalCopyLinesInner.fieldNo046.set(t5542rec.feemax05);
		generalCopyLinesInner.fieldNo047.set(t5542rec.billfreq06);
		generalCopyLinesInner.fieldNo048.set(t5542rec.wdlFreq06);
		generalCopyLinesInner.fieldNo049.set(t5542rec.wdlAmount06);
		generalCopyLinesInner.fieldNo050.set(t5542rec.wdrem06);
		generalCopyLinesInner.fieldNo051.set(t5542rec.ffamt06);
		generalCopyLinesInner.fieldNo052.set(t5542rec.feepc06);
		generalCopyLinesInner.fieldNo053.set(t5542rec.feemin06);
		generalCopyLinesInner.fieldNo054.set(t5542rec.feemax06);
	}

protected void writeOutputLines030()
	{
		/*                                                         <D96NUM>*/
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		/*                                                         <D96NUM>*/
		goTo(GotoLabel.endUp090);
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void writeRecord040()
	{
		/*                                                         <D96NUM>*/
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

	/**
	* <pre>
	*                                                         <D96NUM>
	* </pre>
	*/
protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		/*                                                         <D96NUM>*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Unit Withdrawal Amount                   S5542");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(69);
	private FixedLengthStringData filler9 = new FixedLengthStringData(69).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Freq   Min.From RCD   Min.Withdrawal Amount   Min Amount Remaining /");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler10 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine005, 5, FILLER).init("/ Flat Withdrawal Fee     %Fee          Minimum Fee          Maximum Fee");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(79);
	private FixedLengthStringData filler12 = new FixedLengthStringData(79).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" ------------------------------------------------------------------------------");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(65);
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 1);
	private FixedLengthStringData filler14 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 3, FILLER).init("  |");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 8).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 23).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 47).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 8).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 29).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 38).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(65);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 1);
	private FixedLengthStringData filler22 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 3, FILLER).init("  |");
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 8).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 23).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 47).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler25 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 8).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 29).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 38).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(65);
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 1);
	private FixedLengthStringData filler30 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 3, FILLER).init("  |");
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 8).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 23).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 47).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(77);
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 8).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 29).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 38).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(65);
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 1);
	private FixedLengthStringData filler38 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 3, FILLER).init("  |");
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 8).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 23).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 47).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(77);
	private FixedLengthStringData filler41 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 8).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 29).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 38).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(65);
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 1);
	private FixedLengthStringData filler46 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 3, FILLER).init("  |");
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 8).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 23).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 47).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(77);
	private FixedLengthStringData filler49 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 8).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 29).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 38).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 59).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(65);
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 1);
	private FixedLengthStringData filler54 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 3, FILLER).init("  |");
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 8).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine017, 23).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine017, 47).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(77);
	private FixedLengthStringData filler57 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine018, 8).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine018, 26, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine018, 29).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine018, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine018, 38).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine018, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine018, 59).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
}
}
