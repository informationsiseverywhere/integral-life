package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:51
 * Description:
 * Copybook name: UTRSACCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsacckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsaccFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData utrsaccKey = new FixedLengthStringData(256).isAPartOf(utrsaccFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsaccChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsaccKey, 0);
  	public FixedLengthStringData utrsaccChdrnum = new FixedLengthStringData(8).isAPartOf(utrsaccKey, 1);
  	public FixedLengthStringData utrsaccLife = new FixedLengthStringData(2).isAPartOf(utrsaccKey, 9);
  	public FixedLengthStringData utrsaccCoverage = new FixedLengthStringData(2).isAPartOf(utrsaccKey, 11);
  	public FixedLengthStringData utrsaccRider = new FixedLengthStringData(2).isAPartOf(utrsaccKey, 13);
  	public PackedDecimalData utrsaccPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsaccKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(utrsaccKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsaccFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsaccFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}