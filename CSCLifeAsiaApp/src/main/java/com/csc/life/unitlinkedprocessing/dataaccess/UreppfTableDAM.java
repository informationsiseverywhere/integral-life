package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UreppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:47
 * Class transformed from UREPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UreppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 141;
	public FixedLengthStringData ureprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ureppfRecord = ureprec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ureprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ureprec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ureprec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ureprec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ureprec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ureprec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ureprec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(ureprec);
	public PackedDecimalData nofUnits = DD.nofunt.copy().isAPartOf(ureprec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(ureprec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(ureprec);
	public PackedDecimalData priceDateUsed = DD.pricedt.copy().isAPartOf(ureprec);
	public PackedDecimalData moniesDate = DD.moniesdt.copy().isAPartOf(ureprec);
	public PackedDecimalData priceUsed = DD.priceused.copy().isAPartOf(ureprec);
	public PackedDecimalData nofDunits = DD.nofdunt.copy().isAPartOf(ureprec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(ureprec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(ureprec);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(ureprec);
	public PackedDecimalData scheduleNumber = DD.bschednum.copy().isAPartOf(ureprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ureprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ureprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ureprec);
	public PackedDecimalData seqnum = DD.seqnum.copy().isAPartOf(ureprec);
	public FixedLengthStringData procflg = DD.procflg.copy().isAPartOf(ureprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UreppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UreppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UreppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UreppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UreppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UreppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UreppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UREPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"VRTFND, " +
							"NOFUNT, " +
							"UNITYP, " +
							"BATCTRCDE, " +
							"PRICEDT, " +
							"MONIESDT, " +
							"PRICEUSED, " +
							"NOFDUNT, " +
							"FNDCURR, " +
							"FUNDAMNT, " +
							"BSCHEDNAM, " +
							"BSCHEDNUM, " +
							"DATIME, " +
							"USRPRF, " +
							"JOBNM, " +
							"SEQNUM, " +
							"PROCFLG, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     unitVirtualFund,
                                     nofUnits,
                                     unitType,
                                     batctrcde,
                                     priceDateUsed,
                                     moniesDate,
                                     priceUsed,
                                     nofDunits,
                                     fundCurrency,
                                     fundAmount,
                                     scheduleName,
                                     scheduleNumber,
                                     datime,
                                     userProfile,
                                     jobName,
                                     seqnum,
                                     procflg,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		unitVirtualFund.clear();
  		nofUnits.clear();
  		unitType.clear();
  		batctrcde.clear();
  		priceDateUsed.clear();
  		moniesDate.clear();
  		priceUsed.clear();
  		nofDunits.clear();
  		fundCurrency.clear();
  		fundAmount.clear();
  		scheduleName.clear();
  		scheduleNumber.clear();
  		datime.clear();
  		userProfile.clear();
  		jobName.clear();
  		seqnum.clear();
  		procflg.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUreprec() {
  		return ureprec;
	}

	public FixedLengthStringData getUreppfRecord() {
  		return ureppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUreprec(what);
	}

	public void setUreprec(Object what) {
  		this.ureprec.set(what);
	}

	public void setUreppfRecord(Object what) {
  		this.ureppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ureprec.getLength());
		result.set(ureprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}