/******************************************************************************
 * File Name 		: ZrpwpfDAO.java
 * Author			: CSC
 * Creation Date	: 08 May 2017
 * Project			: Integral Life
 * Description		: The DAO Interface for ZRPWPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			| *											| *				  |
 * ***************************************************************************/
package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrpwpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface ZrpwpfDAO extends BaseDAO<Zrpwpf> {

	public List<Zrpwpf> searchZrpwpfRecord(Zrpwpf zrpwpf) throws SQLRuntimeException;

	public int deleteZrpwpf(Zrpwpf zrpwpf) throws SQLRuntimeException;

}
