package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5510screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5510ScreenVars sv = (S5510ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5510screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5510ScreenVars screenVars = (S5510ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.unitVirtualFund01.setClassString("");
		screenVars.unitVirtualFund02.setClassString("");
		screenVars.unitVirtualFund03.setClassString("");
		screenVars.unitVirtualFund04.setClassString("");
		screenVars.unitVirtualFund05.setClassString("");
		screenVars.unitVirtualFund06.setClassString("");
		screenVars.unitVirtualFund07.setClassString("");
		screenVars.unitVirtualFund08.setClassString("");
		screenVars.unitVirtualFund09.setClassString("");
		screenVars.unitVirtualFund10.setClassString("");
		screenVars.unitPremPercent01.setClassString("");
		screenVars.unitPremPercent02.setClassString("");
		screenVars.unitPremPercent03.setClassString("");
		screenVars.unitPremPercent04.setClassString("");
		screenVars.unitPremPercent05.setClassString("");
		screenVars.unitPremPercent06.setClassString("");
		screenVars.unitPremPercent07.setClassString("");
		screenVars.unitPremPercent08.setClassString("");
		screenVars.unitPremPercent09.setClassString("");
		screenVars.unitPremPercent10.setClassString("");
	}

/**
 * Clear all the variables in S5510screen
 */
	public static void clear(VarModel pv) {
		S5510ScreenVars screenVars = (S5510ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.unitVirtualFund01.clear();
		screenVars.unitVirtualFund02.clear();
		screenVars.unitVirtualFund03.clear();
		screenVars.unitVirtualFund04.clear();
		screenVars.unitVirtualFund05.clear();
		screenVars.unitVirtualFund06.clear();
		screenVars.unitVirtualFund07.clear();
		screenVars.unitVirtualFund08.clear();
		screenVars.unitVirtualFund09.clear();
		screenVars.unitVirtualFund10.clear();
		screenVars.unitPremPercent01.clear();
		screenVars.unitPremPercent02.clear();
		screenVars.unitPremPercent03.clear();
		screenVars.unitPremPercent04.clear();
		screenVars.unitPremPercent05.clear();
		screenVars.unitPremPercent06.clear();
		screenVars.unitPremPercent07.clear();
		screenVars.unitPremPercent08.clear();
		screenVars.unitPremPercent09.clear();
		screenVars.unitPremPercent10.clear();
	}
}
