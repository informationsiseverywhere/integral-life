package com.csc.life.unitlinkedprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:30
 * Description:
 * Copybook name: P5412REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P5412rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData p5412Rec = new FixedLengthStringData(132);
  	public FixedLengthStringData progname = new FixedLengthStringData(8).isAPartOf(p5412Rec, 0);
  	public FixedLengthStringData tranid = new FixedLengthStringData(22).isAPartOf(p5412Rec, 8);
  	public FixedLengthStringData dteeff = new FixedLengthStringData(10).isAPartOf(p5412Rec, 30);
  	public ZonedDecimalData jobno = new ZonedDecimalData(8, 0).isAPartOf(p5412Rec, 40);
  	public FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(p5412Rec, 48);
  	public FixedLengthStringData partfund = new FixedLengthStringData(4).isAPartOf(p5412Rec, 52);
  	public ZonedDecimalData todate = new ZonedDecimalData(8, 0).isAPartOf(p5412Rec, 56);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(22).isAPartOf(p5412Rec, 64);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(p5412Rec, 86);
  	public FixedLengthStringData scrname = new FixedLengthStringData(5).isAPartOf(p5412Rec, 87);
  	public FixedLengthStringData primaryKey = new FixedLengthStringData(36).isAPartOf(p5412Rec, 92);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(p5412Rec, 128);


	public void initialize() {
		COBOLFunctions.initialize(p5412Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		p5412Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}