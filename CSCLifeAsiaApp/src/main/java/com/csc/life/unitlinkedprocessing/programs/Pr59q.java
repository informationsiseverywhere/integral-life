package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;


import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.unitlinkedprocessing.screens.Sr59qScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.tablestructures.Tr59rrec;
import com.csc.life.newbusiness.tablestructures.Tr59srec;
import com.csc.life.newbusiness.tablestructures.Tr60irec;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
public class Pr59q extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR59Q");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	

	/* TABLES */
	private Tr59rrec tr59rrec = new Tr59rrec();
	private Tr59srec tr59srec = new Tr59srec();
	
	/* ERRORS */
	private static final String e186 = "E186";
	
	
	private static final String itemrec = "ITEMREC";
	private static final String descrec = "DESCREC";
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private String covtlnbrec = "COVTLNBREC";
	private Sr59qScreenVars sv = ScreenProgram.getScreenVars( Sr59qScreenVars.class);
	
	public Pr59q() {
		super();
		screenVars = sv;
		new ScreenModel("Sr59q", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
{
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
	try {
		super.mainline();
	}
	catch (COBOLExitProgramException e) {
	// Expected exception for control flow purposes
	}
}
public void processBo(Object... parmArray) {
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

	try {
		processBoMainline(sv, sv.dataArea, parmArray);
	} catch (COBOLExitProgramException e) {
	// Expected exception for control flow purposes
	}
}


protected void initialise1000()
{
	initialise1010();
}

protected void initialise1010()
{
	sv.dataArea.set(SPACES);
	sv.gmiblongdesc.set(SPACES);
	sv.gmdblongdesc.set(SPACES);
	sv.gmwblongdesc.set(SPACES);
	
	//RETIRVIN FUNCYION 
	covtlnbIO.setFunction("RETRV");
	SmartFileCode.execute(appVars, covtlnbIO);
	if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(covtlnbIO.getParams());
		syserrrec.statuz.set(covtlnbIO.getStatuz());
		fatalError600();
	}
//	covtpf = new Covtpf();
//	covtDAO.getCacheObject(covtpf);
	
	sv.gmibsel.set(covtlnbIO.getGmib());
	sv.gmdbsel.set(covtlnbIO.getGmdb());
	sv.gmwbsel.set(covtlnbIO.getGmwb());
	if (isNE(sv.gmibsel,SPACE)){
		sv.gmib.set("Y");
	}
	if (isNE(sv.gmdbsel,SPACE)){
		sv.gmdb.set("Y");
	}
	if (isNE(sv.gmwbsel,SPACE)){
		sv.gmwb.set("Y");
	}
	if (isNE(sv.gmibsel,SPACE)){
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("TR59R");
		descIO.setDescitem(sv.gmibsel);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.gmiblongdesc.fill("?");
		}
		else {
			sv.gmiblongdesc.set(descIO.getLongdesc());
		}
	}
		if (isNE(sv.gmdbsel,SPACE)){
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl("TR59S");
			descIO.setDescitem(sv.gmdbsel);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)
			&& isNE(descIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
		
			if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
				sv.gmdblongdesc.fill("?");
			}
			else {
				sv.gmdblongdesc.set(descIO.getLongdesc());
			}
			
		}	
			if (isNE(sv.gmwbsel,SPACE)){
				descIO.setDataKey(SPACES);
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl("TR60I");
				descIO.setDescitem(sv.gmwbsel);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(),varcom.oK)
				&& isNE(descIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
					sv.gmwblongdesc.fill("?");
				}
				else {
					sv.gmwblongdesc.set(descIO.getLongdesc());
				}
			}
	
			if (isEQ(wsspcomn.flag, "I")) {
				sv.gmibOut[varcom.pr.toInt()].set("Y");
				sv.gmdbOut[varcom.pr.toInt()].set("Y");
				sv.gmwbOut[varcom.pr.toInt()].set("Y");
				sv.gmibselOut[varcom.pr.toInt()].set("Y");
				sv.gmdbselOut[varcom.pr.toInt()].set("Y");
				sv.gmwbselOut[varcom.pr.toInt()].set("Y");
			}
			else
			{
				sv.gmibOut[varcom.pr.toInt()].set("N");
				sv.gmdbOut[varcom.pr.toInt()].set("N");
				sv.gmwbOut[varcom.pr.toInt()].set("N");
				sv.gmibselOut[varcom.pr.toInt()].set("N");
				sv.gmdbselOut[varcom.pr.toInt()].set("N");
				sv.gmwbselOut[varcom.pr.toInt()].set("N");
			}
}
protected void preScreenEdit()
{
	/*PRE-START*/
	/*    This section will handle any action required on the screen **/
	/*    before the screen is painted.                              **/
	if (isEQ(wsspcomn.flag, "I")) {
		sv.gmibOut[varcom.pr.toInt()].set("Y");
		sv.gmdbOut[varcom.pr.toInt()].set("Y");
		sv.gmwbOut[varcom.pr.toInt()].set("Y");
		sv.gmibselOut[varcom.pr.toInt()].set("Y");
		sv.gmdbselOut[varcom.pr.toInt()].set("Y");
		sv.gmwbselOut[varcom.pr.toInt()].set("Y");
		
	}
	return ;
	/*PRE-EXIT*/
}

/**
* <pre>
*     RETRIEVE SCREEN FIELDS AND EDIT
* </pre>
*/
protected void screenEdit2000()
{
	/*SCREEN-IO*/
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(scrnparams.statuz,"KILL")){
		return;
	}
	
	if (isEQ(scrnparams.statuz, varcom.calc)) {     
		
		wsspcomn.edterror.set("Y");
	}
	
	/*VALIDATE*/
	/**    Validate fields*/
	/*CHECK-FOR-ERRORS*/

	if (isNE(sv.gmibsel,"Y")){
		sv.gmiblongdesc.set(SPACES);
	}
	if (isNE(sv.gmdbsel,"Y")){
		sv.gmdblongdesc.set(SPACES);
	}
	if (isNE(sv.gmwbsel,"Y")){
		sv.gmwblongdesc.set(SPACES);
	}
	if (isNE(sv.gmib, "Y")
		&& isNE(sv.gmibsel, SPACE)){
		sv.gmibErr.set(e186);
		sv.gmibOut[varcom.ri.toInt()].set("Y");
	}
	if (isEQ(sv.gmib, "Y")
			&& isEQ(sv.gmibsel, SPACE)){
			sv.gmibselErr.set(e186);
		}
	if (isNE(sv.gmdb, "Y")
			&& isNE(sv.gmdbsel, SPACE)){
			sv.gmdbErr.set(e186);
			sv.gmdbOut[varcom.ri.toInt()].set("Y");
		}
	if (isEQ(sv.gmdb, "Y")
			&& isEQ(sv.gmdbsel, SPACE)){
			sv.gmdbselErr.set(e186);
		}	
	if (isNE(sv.gmwb, "Y")
			&& isNE(sv.gmwbsel, SPACE)){
			sv.gmwbErr.set(e186);
			sv.gmwbOut[varcom.ri.toInt()].set("Y");
		}
		if (isEQ(sv.gmwb, "Y")
				&& isEQ(sv.gmwbsel, SPACE)){
				sv.gmwbselErr.set(e186);
			}
	if (isNE(sv.gmibsel,SPACE)){
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("TR59R");
		descIO.setDescitem(sv.gmibsel);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.gmiblongdesc.fill("?");
		}
		else {
			sv.gmiblongdesc.set(descIO.getLongdesc());
		}
	}
		if (isNE(sv.gmdbsel,SPACE)){
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl("TR59S");
			descIO.setDescitem(sv.gmdbsel);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)
			&& isNE(descIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
				sv.gmdblongdesc.fill("?");
			}
			else {
				sv.gmdblongdesc.set(descIO.getLongdesc());
			}
	
	}
		if (isNE(sv.gmwbsel,SPACE)){
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl("TR60I");
			descIO.setDescitem(sv.gmwbsel);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)
			&& isNE(descIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
				sv.gmwblongdesc.fill("?");
			}
			else {
				sv.gmwblongdesc.set(descIO.getLongdesc());
			}
	
	}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
}
		

	/*EXIT*/



protected void update3000()
{
	if (isEQ(scrnparams.statuz,"KILL")
	|| isEQ(wsspcomn.flag,"I")) {
		return ;
		}
	covtlnbIO.setGmib(sv.gmibsel);
	covtlnbIO.setGmdb(sv.gmdbsel);
	covtlnbIO.setGmwb(sv.gmwbsel);
	covtlnbIO.setFunction("KEEPS");
	covtlnbIO.setFormat(covtlnbrec);
	SmartFileCode.execute(appVars, covtlnbIO);
	if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(covtlnbIO.getParams());
		fatalError600();
	}
	/*EXIT*/
}
protected void whereNext4000()
{
	/*NEXT-PROGRAM*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/

}

}