/******************************************************************************
 * File Name 		: UswdpfDAOImpl.java
 * Author			: sjothiman2
 * Creation Date	: 23 May 2018
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for USWDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UswdpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Uswdpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UswdpfDAOImpl extends BaseDAOImpl<Uswdpf> implements UswdpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(UswdpfDAOImpl.class);

	public List<Uswdpf> searchUswdpfRecord(Uswdpf uswdpf) throws SQLRuntimeException{
		List<Uswdpf> outputList = new ArrayList<Uswdpf>();
		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, PLNSFX, LIFE, JLIFE, ");
		sqlSelect.append("COVERAGE, RIDER, TRANNO, PCAMTIND, SRCFUND01, ");
		sqlSelect.append("SRCFUND02, SRCFUND03, SRCFUND04, SRCFUND05, SRCFUND06, ");
		sqlSelect.append("SRCFUND07, SRCFUND08, SRCFUND09, SRCFUND10, SRCFUND11, ");
		sqlSelect.append("SRCFUND12, SRCFUND13, SRCFUND14, SRCFUND15, SRCFUND16, ");
		sqlSelect.append("SRCFUND17, SRCFUND18, SRCFUND19, SRCFUND20, SCFNDTYP01, ");
		sqlSelect.append("SCFNDTYP02, SCFNDTYP03, SCFNDTYP04, SCFNDTYP05, SCFNDTYP06, ");
		sqlSelect.append("SCFNDTYP07, SCFNDTYP08, SCFNDTYP09, SCFNDTYP10, SCFNDTYP11, ");
		sqlSelect.append("SCFNDTYP12, SCFNDTYP13, SCFNDTYP14, SCFNDTYP15, SCFNDTYP16, ");
		sqlSelect.append("SCFNDTYP17, SCFNDTYP18, SCFNDTYP19, SCFNDTYP20, SCFNDCUR01, ");
		sqlSelect.append("SCFNDCUR02, SCFNDCUR03, SCFNDCUR04, SCFNDCUR05, SCFNDCUR06, ");
		sqlSelect.append("SCFNDCUR07, SCFNDCUR08, SCFNDCUR09, SCFNDCUR10, SCFNDCUR11, ");
		sqlSelect.append("SCFNDCUR12, SCFNDCUR13, SCFNDCUR14, SCFNDCUR15, SCFNDCUR16, ");
		sqlSelect.append("SCFNDCUR17, SCFNDCUR18, SCFNDCUR19, SCFNDCUR20, SCPRCAMT01, ");
		sqlSelect.append("SCPRCAMT02, SCPRCAMT03, SCPRCAMT04, SCPRCAMT05, SCPRCAMT06, ");
		sqlSelect.append("SCPRCAMT07, SCPRCAMT08, SCPRCAMT09, SCPRCAMT10, SCPRCAMT11, ");
		sqlSelect.append("SCPRCAMT12, SCPRCAMT13, SCPRCAMT14, SCPRCAMT15, SCPRCAMT16, ");
		sqlSelect.append("SCPRCAMT17, SCPRCAMT18, SCPRCAMT19, SCPRCAMT20, SCESTVAL01, ");
		sqlSelect.append("SCESTVAL02, SCESTVAL03, SCESTVAL04, SCESTVAL05, SCESTVAL06, ");
		sqlSelect.append("SCESTVAL07, SCESTVAL08, SCESTVAL09, SCESTVAL10, SCESTVAL11, ");
		sqlSelect.append("SCESTVAL12, SCESTVAL13, SCESTVAL14, SCESTVAL15, SCESTVAL16, ");
		sqlSelect.append("SCESTVAL17, SCESTVAL18, SCESTVAL19, SCESTVAL20, SCACTVAL01, ");
		sqlSelect.append("SCACTVAL02, SCACTVAL03, SCACTVAL04, SCACTVAL05, SCACTVAL06, ");
		sqlSelect.append("SCACTVAL07, SCACTVAL08, SCACTVAL09, SCACTVAL10, SCACTVAL11, ");
		sqlSelect.append("SCACTVAL12, SCACTVAL13, SCACTVAL14, SCACTVAL15, SCACTVAL16, ");
		sqlSelect.append("SCACTVAL17, SCACTVAL18, SCACTVAL19, SCACTVAL20, TGTFUND01, ");
		sqlSelect.append("TGTFUND02, TGTFUND03, TGTFUND04, TGTFUND05, TGTFUND06, ");
		sqlSelect.append("TGTFUND07, TGTFUND08, TGTFUND09, TGTFUND10, TGFNDTYP01, ");
		sqlSelect.append("TGFNDTYP02, TGFNDTYP03, TGFNDTYP04, TGFNDTYP05, TGFNDTYP06, ");
		sqlSelect.append("TGFNDTYP07, TGFNDTYP08, TGFNDTYP09, TGFNDTYP10, TGFNDCUR01, ");
		sqlSelect.append("TGFNDCUR02, TGFNDCUR03, TGFNDCUR04, TGFNDCUR05, TGFNDCUR06, ");
		sqlSelect.append("TGFNDCUR07, TGFNDCUR08, TGFNDCUR09, TGFNDCUR10, TGTPRCNT01, ");
		sqlSelect.append("TGTPRCNT02, TGTPRCNT03, TGTPRCNT04, TGTPRCNT05, TGTPRCNT06, ");
		sqlSelect.append("TGTPRCNT07, TGTPRCNT08, TGTPRCNT09, TGTPRCNT10, EFFDATE, ");
		sqlSelect.append("ORSWCHFE, USRPRF, JOBNM, DATIME, SCFNDPOL01, ");
		sqlSelect.append("SCFNDPOL02, SCFNDPOL03, SCFNDPOL04, SCFNDPOL05, SCFNDPOL06, ");
		sqlSelect.append("SCFNDPOL07, SCFNDPOL08, SCFNDPOL09, SCFNDPOL10, SCFNDPOL11, ");
		sqlSelect.append("SCFNDPOL12, SCFNDPOL13, SCFNDPOL14, SCFNDPOL15, SCFNDPOL16, ");
		sqlSelect.append("SCFNDPOL17, SCFNDPOL18, SCFNDPOL19, SCFNDPOL20, TGFNPOL01, ");
		sqlSelect.append("TGFNPOL02, TGFNPOL03, TGFNPOL04, TGFNPOL05, TGFNPOL06, ");
		sqlSelect.append("TGFNPOL07, TGFNPOL08, TGFNPOL09, TGFNPOL10 ");
		sqlSelect.append(" FROM USWDPF   WHERE  CHDRNUM=? AND CHDRCOY=? AND SRCFUND01=? AND SCFNDPOL01=?  ");
		


		PreparedStatement psUswdpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlUswdpfRs = null;
		//List<Uswdpf> outputList = new ArrayList<Uswdpf>();
		int index=1;
		try {

	 

			 psUswdpfSelect.setString(1, uswdpf.getChdrnum());
			 psUswdpfSelect.setString(2, uswdpf.getChdrcoy());
			 psUswdpfSelect.setString(3, uswdpf.getSrcfund01());
			 psUswdpfSelect.setString(4, uswdpf.getScfndpol01());		
				
			sqlUswdpfRs = executeQuery(psUswdpfSelect);

			while (sqlUswdpfRs.next()) {

				Uswdpf objuswdpf = new Uswdpf();

			    objuswdpf.setSrcfund01(sqlUswdpfRs.getString("SRCFUND01"));
			    objuswdpf.setScprcamt01(sqlUswdpfRs.getBigDecimal("SCPRCAMT01"));
			    
				objuswdpf.setTgtfund01(sqlUswdpfRs.getString("TGTFUND01"));
				objuswdpf.setTgtfund02(sqlUswdpfRs.getString("TGTFUND02"));
				objuswdpf.setTgtfund03(sqlUswdpfRs.getString("TGTFUND03"));
				objuswdpf.setTgtfund04(sqlUswdpfRs.getString("TGTFUND04"));
				objuswdpf.setTgtfund05(sqlUswdpfRs.getString("TGTFUND05"));
				objuswdpf.setTgtfund06(sqlUswdpfRs.getString("TGTFUND06"));
				objuswdpf.setTgtfund07(sqlUswdpfRs.getString("TGTFUND07"));
				objuswdpf.setTgtfund08(sqlUswdpfRs.getString("TGTFUND08"));
				objuswdpf.setTgtfund09(sqlUswdpfRs.getString("TGTFUND09"));
				objuswdpf.setTgtfund10(sqlUswdpfRs.getString("TGTFUND10"));
			
				objuswdpf.setTgtprcnt01(sqlUswdpfRs.getBigDecimal("TGTPRCNT01"));
				objuswdpf.setTgtprcnt02(sqlUswdpfRs.getBigDecimal("TGTPRCNT02"));
				objuswdpf.setTgtprcnt03(sqlUswdpfRs.getBigDecimal("TGTPRCNT03"));
				objuswdpf.setTgtprcnt04(sqlUswdpfRs.getBigDecimal("TGTPRCNT04"));
				objuswdpf.setTgtprcnt05(sqlUswdpfRs.getBigDecimal("TGTPRCNT05"));
				objuswdpf.setTgtprcnt06(sqlUswdpfRs.getBigDecimal("TGTPRCNT06"));
				objuswdpf.setTgtprcnt07(sqlUswdpfRs.getBigDecimal("TGTPRCNT07"));
				objuswdpf.setTgtprcnt08(sqlUswdpfRs.getBigDecimal("TGTPRCNT08"));
				objuswdpf.setTgtprcnt09(sqlUswdpfRs.getBigDecimal("TGTPRCNT09"));
				objuswdpf.setTgtprcnt10(sqlUswdpfRs.getBigDecimal("TGTPRCNT10"));
				 
				objuswdpf.setTgfnpol01(sqlUswdpfRs.getString("TGFNPOL01"));
				objuswdpf.setTgfnpol02(sqlUswdpfRs.getString("TGFNPOL02"));
				objuswdpf.setTgfnpol03(sqlUswdpfRs.getString("TGFNPOL03"));
				objuswdpf.setTgfnpol04(sqlUswdpfRs.getString("TGFNPOL04"));
				objuswdpf.setTgfnpol05(sqlUswdpfRs.getString("TGFNPOL05"));
				objuswdpf.setTgfnpol06(sqlUswdpfRs.getString("TGFNPOL06"));
				objuswdpf.setTgfnpol07(sqlUswdpfRs.getString("TGFNPOL07"));
				objuswdpf.setTgfnpol08(sqlUswdpfRs.getString("TGFNPOL08"));
				objuswdpf.setTgfnpol09(sqlUswdpfRs.getString("TGFNPOL09"));
				objuswdpf.setTgfnpol10(sqlUswdpfRs.getString("TGFNPOL10"));

				outputList.add(objuswdpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchUswdpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psUswdpfSelect, sqlUswdpfRs);
		}
 
		return outputList;
	}

	public List<Uswdpf> searchAllUswdpf(Uswdpf uswdpf) throws SQLRuntimeException{
		List<Uswdpf> outputList = new ArrayList<Uswdpf>();
		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, PLNSFX, LIFE, JLIFE, ");
		sqlSelect.append("COVERAGE, RIDER, TRANNO, PCAMTIND, SRCFUND01, ");
		sqlSelect.append("SRCFUND02, SRCFUND03, SRCFUND04, SRCFUND05, SRCFUND06, ");
		sqlSelect.append("SRCFUND07, SRCFUND08, SRCFUND09, SRCFUND10, SRCFUND11, ");
		sqlSelect.append("SRCFUND12, SRCFUND13, SRCFUND14, SRCFUND15, SRCFUND16, ");
		sqlSelect.append("SRCFUND17, SRCFUND18, SRCFUND19, SRCFUND20, SCFNDTYP01, ");
		sqlSelect.append("SCFNDTYP02, SCFNDTYP03, SCFNDTYP04, SCFNDTYP05, SCFNDTYP06, ");
		sqlSelect.append("SCFNDTYP07, SCFNDTYP08, SCFNDTYP09, SCFNDTYP10, SCFNDTYP11, ");
		sqlSelect.append("SCFNDTYP12, SCFNDTYP13, SCFNDTYP14, SCFNDTYP15, SCFNDTYP16, ");
		sqlSelect.append("SCFNDTYP17, SCFNDTYP18, SCFNDTYP19, SCFNDTYP20, SCFNDCUR01, ");
		sqlSelect.append("SCFNDCUR02, SCFNDCUR03, SCFNDCUR04, SCFNDCUR05, SCFNDCUR06, ");
		sqlSelect.append("SCFNDCUR07, SCFNDCUR08, SCFNDCUR09, SCFNDCUR10, SCFNDCUR11, ");
		sqlSelect.append("SCFNDCUR12, SCFNDCUR13, SCFNDCUR14, SCFNDCUR15, SCFNDCUR16, ");
		sqlSelect.append("SCFNDCUR17, SCFNDCUR18, SCFNDCUR19, SCFNDCUR20, SCPRCAMT01, ");
		sqlSelect.append("SCPRCAMT02, SCPRCAMT03, SCPRCAMT04, SCPRCAMT05, SCPRCAMT06, ");
		sqlSelect.append("SCPRCAMT07, SCPRCAMT08, SCPRCAMT09, SCPRCAMT10, SCPRCAMT11, ");
		sqlSelect.append("SCPRCAMT12, SCPRCAMT13, SCPRCAMT14, SCPRCAMT15, SCPRCAMT16, ");
		sqlSelect.append("SCPRCAMT17, SCPRCAMT18, SCPRCAMT19, SCPRCAMT20, SCESTVAL01, ");
		sqlSelect.append("SCESTVAL02, SCESTVAL03, SCESTVAL04, SCESTVAL05, SCESTVAL06, ");
		sqlSelect.append("SCESTVAL07, SCESTVAL08, SCESTVAL09, SCESTVAL10, SCESTVAL11, ");
		sqlSelect.append("SCESTVAL12, SCESTVAL13, SCESTVAL14, SCESTVAL15, SCESTVAL16, ");
		sqlSelect.append("SCESTVAL17, SCESTVAL18, SCESTVAL19, SCESTVAL20, SCACTVAL01, ");
		sqlSelect.append("SCACTVAL02, SCACTVAL03, SCACTVAL04, SCACTVAL05, SCACTVAL06, ");
		sqlSelect.append("SCACTVAL07, SCACTVAL08, SCACTVAL09, SCACTVAL10, SCACTVAL11, ");
		sqlSelect.append("SCACTVAL12, SCACTVAL13, SCACTVAL14, SCACTVAL15, SCACTVAL16, ");
		sqlSelect.append("SCACTVAL17, SCACTVAL18, SCACTVAL19, SCACTVAL20, TGTFUND01, ");
		sqlSelect.append("TGTFUND02, TGTFUND03, TGTFUND04, TGTFUND05, TGTFUND06, ");
		sqlSelect.append("TGTFUND07, TGTFUND08, TGTFUND09, TGTFUND10, TGFNDTYP01, ");
		sqlSelect.append("TGFNDTYP02, TGFNDTYP03, TGFNDTYP04, TGFNDTYP05, TGFNDTYP06, ");
		sqlSelect.append("TGFNDTYP07, TGFNDTYP08, TGFNDTYP09, TGFNDTYP10, TGFNDCUR01, ");
		sqlSelect.append("TGFNDCUR02, TGFNDCUR03, TGFNDCUR04, TGFNDCUR05, TGFNDCUR06, ");
		sqlSelect.append("TGFNDCUR07, TGFNDCUR08, TGFNDCUR09, TGFNDCUR10, TGTPRCNT01, ");
		sqlSelect.append("TGTPRCNT02, TGTPRCNT03, TGTPRCNT04, TGTPRCNT05, TGTPRCNT06, ");
		sqlSelect.append("TGTPRCNT07, TGTPRCNT08, TGTPRCNT09, TGTPRCNT10, EFFDATE, ");
		sqlSelect.append("ORSWCHFE, USRPRF, JOBNM, DATIME, SCFNDPOL01, ");
		sqlSelect.append("SCFNDPOL02, SCFNDPOL03, SCFNDPOL04, SCFNDPOL05, SCFNDPOL06, ");
		sqlSelect.append("SCFNDPOL07, SCFNDPOL08, SCFNDPOL09, SCFNDPOL10, SCFNDPOL11, ");
		sqlSelect.append("SCFNDPOL12, SCFNDPOL13, SCFNDPOL14, SCFNDPOL15, SCFNDPOL16, ");
		sqlSelect.append("SCFNDPOL17, SCFNDPOL18, SCFNDPOL19, SCFNDPOL20, TGFNPOL01, ");
		sqlSelect.append("TGFNPOL02, TGFNPOL03, TGFNPOL04, TGFNPOL05, TGFNPOL06, ");
		sqlSelect.append("TGFNPOL07, TGFNPOL08, TGFNPOL09, TGFNPOL10 ");
		sqlSelect.append(" FROM USWDPF   WHERE  CHDRNUM=? AND CHDRCOY=?   ");
		


		PreparedStatement psUswdpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlUswdpfRs = null;
		//List<Uswdpf> outputList = new ArrayList<Uswdpf>();
		int index=1;
		try {

	 

			 psUswdpfSelect.setString(1, uswdpf.getChdrnum());
			 psUswdpfSelect.setString(2, uswdpf.getChdrcoy());
 	
				
			sqlUswdpfRs = executeQuery(psUswdpfSelect);

			while (sqlUswdpfRs.next()) {

				Uswdpf objuswdpf = new Uswdpf();

			    objuswdpf.setSrcfund01(sqlUswdpfRs.getString("SRCFUND01"));
			    objuswdpf.setScprcamt01(sqlUswdpfRs.getBigDecimal("SCPRCAMT01"));
			    objuswdpf.setScfndpol01(sqlUswdpfRs.getString("SCFNDPOL01"));
				objuswdpf.setTgtfund01(sqlUswdpfRs.getString("TGTFUND01"));
				objuswdpf.setTgtfund02(sqlUswdpfRs.getString("TGTFUND02"));
				objuswdpf.setTgtfund03(sqlUswdpfRs.getString("TGTFUND03"));
				objuswdpf.setTgtfund04(sqlUswdpfRs.getString("TGTFUND04"));
				objuswdpf.setTgtfund05(sqlUswdpfRs.getString("TGTFUND05"));
				objuswdpf.setTgtfund06(sqlUswdpfRs.getString("TGTFUND06"));
				objuswdpf.setTgtfund07(sqlUswdpfRs.getString("TGTFUND07"));
				objuswdpf.setTgtfund08(sqlUswdpfRs.getString("TGTFUND08"));
				objuswdpf.setTgtfund09(sqlUswdpfRs.getString("TGTFUND09"));
				objuswdpf.setTgtfund10(sqlUswdpfRs.getString("TGTFUND10"));
			
				objuswdpf.setTgtprcnt01(sqlUswdpfRs.getBigDecimal("TGTPRCNT01"));
				objuswdpf.setTgtprcnt02(sqlUswdpfRs.getBigDecimal("TGTPRCNT02"));
				objuswdpf.setTgtprcnt03(sqlUswdpfRs.getBigDecimal("TGTPRCNT03"));
				objuswdpf.setTgtprcnt04(sqlUswdpfRs.getBigDecimal("TGTPRCNT04"));
				objuswdpf.setTgtprcnt05(sqlUswdpfRs.getBigDecimal("TGTPRCNT05"));
				objuswdpf.setTgtprcnt06(sqlUswdpfRs.getBigDecimal("TGTPRCNT06"));
				objuswdpf.setTgtprcnt07(sqlUswdpfRs.getBigDecimal("TGTPRCNT07"));
				objuswdpf.setTgtprcnt08(sqlUswdpfRs.getBigDecimal("TGTPRCNT08"));
				objuswdpf.setTgtprcnt09(sqlUswdpfRs.getBigDecimal("TGTPRCNT09"));
				objuswdpf.setTgtprcnt10(sqlUswdpfRs.getBigDecimal("TGTPRCNT10"));
				 
				objuswdpf.setTgfnpol01(sqlUswdpfRs.getString("TGFNPOL01"));
				objuswdpf.setTgfnpol02(sqlUswdpfRs.getString("TGFNPOL02"));
				objuswdpf.setTgfnpol03(sqlUswdpfRs.getString("TGFNPOL03"));
				objuswdpf.setTgfnpol04(sqlUswdpfRs.getString("TGFNPOL04"));
				objuswdpf.setTgfnpol05(sqlUswdpfRs.getString("TGFNPOL05"));
				objuswdpf.setTgfnpol06(sqlUswdpfRs.getString("TGFNPOL06"));
				objuswdpf.setTgfnpol07(sqlUswdpfRs.getString("TGFNPOL07"));
				objuswdpf.setTgfnpol08(sqlUswdpfRs.getString("TGFNPOL08"));
				objuswdpf.setTgfnpol09(sqlUswdpfRs.getString("TGFNPOL09"));
				objuswdpf.setTgfnpol10(sqlUswdpfRs.getString("TGFNPOL10"));

				outputList.add(objuswdpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchUswdpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psUswdpfSelect, sqlUswdpfRs);
		}
 
		return outputList;
	}
	public void insertIntoUswdpf(Uswdpf uswdpf) throws SQLRuntimeException{

		StringBuilder sqlInsert = new StringBuilder();

		sqlInsert.append("INSERT INTO USWDPF (");
		sqlInsert.append("CHDRCOY, CHDRNUM, PLNSFX, LIFE, JLIFE, ");
		sqlInsert.append("COVERAGE, RIDER, TRANNO, PCAMTIND, SRCFUND01, ");
		sqlInsert.append("SRCFUND02, SRCFUND03, SRCFUND04, SRCFUND05, SRCFUND06, ");
		sqlInsert.append("SRCFUND07, SRCFUND08, SRCFUND09, SRCFUND10, SRCFUND11, ");
		sqlInsert.append("SRCFUND12, SRCFUND13, SRCFUND14, SRCFUND15, SRCFUND16, ");
		sqlInsert.append("SRCFUND17, SRCFUND18, SRCFUND19, SRCFUND20, SCFNDTYP01, ");
		sqlInsert.append("SCFNDTYP02, SCFNDTYP03, SCFNDTYP04, SCFNDTYP05, SCFNDTYP06, ");
		sqlInsert.append("SCFNDTYP07, SCFNDTYP08, SCFNDTYP09, SCFNDTYP10, SCFNDTYP11, ");
		sqlInsert.append("SCFNDTYP12, SCFNDTYP13, SCFNDTYP14, SCFNDTYP15, SCFNDTYP16, ");
		sqlInsert.append("SCFNDTYP17, SCFNDTYP18, SCFNDTYP19, SCFNDTYP20, SCFNDCUR01, ");
		sqlInsert.append("SCFNDCUR02, SCFNDCUR03, SCFNDCUR04, SCFNDCUR05, SCFNDCUR06, ");
		sqlInsert.append("SCFNDCUR07, SCFNDCUR08, SCFNDCUR09, SCFNDCUR10, SCFNDCUR11, ");
		sqlInsert.append("SCFNDCUR12, SCFNDCUR13, SCFNDCUR14, SCFNDCUR15, SCFNDCUR16, ");
		sqlInsert.append("SCFNDCUR17, SCFNDCUR18, SCFNDCUR19, SCFNDCUR20, SCPRCAMT01, ");
		sqlInsert.append("SCPRCAMT02, SCPRCAMT03, SCPRCAMT04, SCPRCAMT05, SCPRCAMT06, ");
		sqlInsert.append("SCPRCAMT07, SCPRCAMT08, SCPRCAMT09, SCPRCAMT10, SCPRCAMT11, ");
		sqlInsert.append("SCPRCAMT12, SCPRCAMT13, SCPRCAMT14, SCPRCAMT15, SCPRCAMT16, ");
		sqlInsert.append("SCPRCAMT17, SCPRCAMT18, SCPRCAMT19, SCPRCAMT20, SCESTVAL01, ");
		sqlInsert.append("SCESTVAL02, SCESTVAL03, SCESTVAL04, SCESTVAL05, SCESTVAL06, ");
		sqlInsert.append("SCESTVAL07, SCESTVAL08, SCESTVAL09, SCESTVAL10, SCESTVAL11, ");
		sqlInsert.append("SCESTVAL12, SCESTVAL13, SCESTVAL14, SCESTVAL15, SCESTVAL16, ");
		sqlInsert.append("SCESTVAL17, SCESTVAL18, SCESTVAL19, SCESTVAL20, SCACTVAL01, ");
		sqlInsert.append("SCACTVAL02, SCACTVAL03, SCACTVAL04, SCACTVAL05, SCACTVAL06, ");
		sqlInsert.append("SCACTVAL07, SCACTVAL08, SCACTVAL09, SCACTVAL10, SCACTVAL11, ");
		sqlInsert.append("SCACTVAL12, SCACTVAL13, SCACTVAL14, SCACTVAL15, SCACTVAL16, ");
		sqlInsert.append("SCACTVAL17, SCACTVAL18, SCACTVAL19, SCACTVAL20, TGTFUND01, ");
		sqlInsert.append("TGTFUND02, TGTFUND03, TGTFUND04, TGTFUND05, TGTFUND06, ");
		sqlInsert.append("TGTFUND07, TGTFUND08, TGTFUND09, TGTFUND10, TGFNDTYP01, ");
		sqlInsert.append("TGFNDTYP02, TGFNDTYP03, TGFNDTYP04, TGFNDTYP05, TGFNDTYP06, ");
		sqlInsert.append("TGFNDTYP07, TGFNDTYP08, TGFNDTYP09, TGFNDTYP10, TGFNDCUR01, ");
		sqlInsert.append("TGFNDCUR02, TGFNDCUR03, TGFNDCUR04, TGFNDCUR05, TGFNDCUR06, ");
		sqlInsert.append("TGFNDCUR07, TGFNDCUR08, TGFNDCUR09, TGFNDCUR10, TGTPRCNT01, ");
		sqlInsert.append("TGTPRCNT02, TGTPRCNT03, TGTPRCNT04, TGTPRCNT05, TGTPRCNT06, ");
		sqlInsert.append("TGTPRCNT07, TGTPRCNT08, TGTPRCNT09, TGTPRCNT10, EFFDATE, ");
		sqlInsert.append("ORSWCHFE, USRPRF, JOBNM, DATIME, SCFNDPOL01, ");
		sqlInsert.append("SCFNDPOL02, SCFNDPOL03, SCFNDPOL04, SCFNDPOL05, SCFNDPOL06, ");
		sqlInsert.append("SCFNDPOL07, SCFNDPOL08, SCFNDPOL09, SCFNDPOL10, SCFNDPOL11, ");
		sqlInsert.append("SCFNDPOL12, SCFNDPOL13, SCFNDPOL14, SCFNDPOL15, SCFNDPOL16, ");
		sqlInsert.append("SCFNDPOL17, SCFNDPOL18, SCFNDPOL19, SCFNDPOL20, TGFNPOL01, ");
		sqlInsert.append("TGFNPOL02, TGFNPOL03, TGFNPOL04, TGFNPOL05, TGFNPOL06, ");
		sqlInsert.append("TGFNPOL07, TGFNPOL08, TGFNPOL09, TGFNPOL10 ");
		sqlInsert.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement psUswdpfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;

		try {
// 	psUtrnInsert.getString(i++, u.getChdrcoy());
			psUswdpfInsert.setString(index++, uswdpf.getChdrcoy());
			psUswdpfInsert.setString(index++, uswdpf.getChdrnum());
			psUswdpfInsert.setLong(index++, uswdpf.getPlnsfx());
			psUswdpfInsert.setString(index++, uswdpf.getLife());
			psUswdpfInsert.setString(index++, uswdpf.getJlife());
			psUswdpfInsert.setString(index++, uswdpf.getCoverage());
			psUswdpfInsert.setString(index++, uswdpf.getRider());
			psUswdpfInsert.setLong(index++, uswdpf.getTranno());
			psUswdpfInsert.setString(index++, uswdpf.getPcamtind());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund01());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund02());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund03());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund04());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund05());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund06());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund07());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund08());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund09());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund10());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund11());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund12());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund13());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund14());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund15());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund16());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund17());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund18());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund19());
			psUswdpfInsert.setString(index++, uswdpf.getSrcfund20());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp01());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp02());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp03());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp04());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp05());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp06());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp07());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp08());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp09());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp10());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp11());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp12());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp13());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp14());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp15());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp16());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp17());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp18());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp19());
			psUswdpfInsert.setString(index++, uswdpf.getScfndtyp20());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur01());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur02());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur03());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur04());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur05());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur06());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur07());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur08());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur09());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur10());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur11());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur12());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur13());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur14());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur15());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur16());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur17());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur18());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur19());
			psUswdpfInsert.setString(index++, uswdpf.getScfndcur20());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt01());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt02());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt03());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt04());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt05());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt06());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt07());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt08());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt09());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt10());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt11());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt11());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt12());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt14());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt15());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt16());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt17());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt18());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt19());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScprcamt20());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval01());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval02());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval03());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval04());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval05());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval06());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval07());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval08());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval09());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval10());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval11());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval12());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval13());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval14());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval15());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval16());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval17());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval18());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval19());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScestval20());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval01());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval02());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval03());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval04());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval05());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval06());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval07());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval08());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval09());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval10());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval11());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval12());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval13());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval14());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval15());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval16());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval17());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval18());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval19());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getScactval20());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund01());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund02());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund03());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund04());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund05());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund06());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund07());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund08());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund09());
			psUswdpfInsert.setString(index++, uswdpf.getTgtfund10());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp01());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp02());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp03());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp04());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp05());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp06());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp07());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp08());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp09());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndtyp10());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur01());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur02());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur03());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur04());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur05());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur06());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur07());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur08());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur09());
			psUswdpfInsert.setString(index++, uswdpf.getTgfndcur10());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt01());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt02());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt03());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt04());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt05());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt06());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt07());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt08());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt09());
			psUswdpfInsert.setBigDecimal(index++, uswdpf.getTgtprcnt10());
			psUswdpfInsert.setInt(index++, uswdpf.getEffdate());
			psUswdpfInsert.setString(index++, uswdpf.getOrswchfe());
			psUswdpfInsert.setString(index++, uswdpf.getUsrprf());
			psUswdpfInsert.setString(index++, uswdpf.getJobnm());		
			psUswdpfInsert.setTimestamp(index++, new Timestamp(System.currentTimeMillis()));
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol01());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol02());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol03());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol04());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol05());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol06());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol07());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol08());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol09());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol10());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol11());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol12());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol13());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol14());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol15());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol16());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol17());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol18());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol19());
			psUswdpfInsert.setString(index++, uswdpf.getScfndpol20());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol01());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol02());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol03());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol04());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol05());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol06());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol07());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol08());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol09());
			psUswdpfInsert.setString(index++, uswdpf.getTgfnpol10());

			int affectedCount = executeUpdate(psUswdpfInsert);
			getConnection().commit();
			LOGGER.debug("insertIntoUswdpf {} rows inserted.", affectedCount);//IJTI-1561

		} catch (SQLException e) {
			LOGGER.error("insertIntoUswdpf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psUswdpfInsert, null);
		}
	}

	public void deleteUswdpf(Uswdpf uswdpf) throws SQLRuntimeException
	{

				StringBuilder sqlDelete = new StringBuilder();
		
				sqlDelete.append("DELETE FROM USWDPF WHERE  CHDRNUM=? AND CHDRCOY=? AND SRCFUND01=? AND SCFNDPOL01=? ");
				
				PreparedStatement psUswdpfInsert = getPrepareStatement(sqlDelete.toString());
				int index = 1;
		
				try {
					//psUtrnInsert.getString(i++, u.getChdrcoy());		
					psUswdpfInsert.setString(index++, uswdpf.getChdrnum());
					psUswdpfInsert.setString(index++, uswdpf.getChdrcoy());
					psUswdpfInsert.setString(index++, uswdpf.getSrcfund01());
					psUswdpfInsert.setString(index++, uswdpf.getScfndpol01());		
					
					int affectedCount = executeUpdate(psUswdpfInsert);
				 
					LOGGER.debug("insertIntoUswdpf {} rows inserted.", affectedCount);//IJTI-1561

				 
			}catch (SQLException e) 
			{
				LOGGER.error("deleteUswdpf()", e);
				throw new SQLRuntimeException(e);
			}
			finally 
			{
				close(psUswdpfInsert, null);
			}
	}
}