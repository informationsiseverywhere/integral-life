package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;

public interface UlnkpfDAO {
	public Map<String, List<Ulnkpf>> searchUlnkrnlRecordByChdrnum(List<String> chdrnumList);
	public int checkUlnkpf(Ulnkpf ulnkpf);
	//ILIFE-5140
	public List<Ulnkpf> getUlnkpfByChdrnum(Ulnkpf ulnkpf);
	//ILIFE-5140
	
	public List<Ulnkpf> searchUlnkrevRecord(String coy, String chdrnum);
	public void deleteUlnkpfRecord(List<Ulnkpf> ulnkpfList);
	public int isExistUlnk(String chdrcoy, String chdrnum);
	public Ulnkpf searchUlnk(String chdrcoy, String chdrnum, String life, String jlife, String coverage, String rider, int plnsfx); //ILIFE-8786
	
}
