/*
 * File: Zrulaloc.java
 * Date: 30 August 2009 2:56:40
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRULALOC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.dataaccess.ZrraTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;

import com.csc.life.terminationclaims.recordstructures.Untallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.IncirnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstnudTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      UNIT LINKED GENERIC COLLECTION SUBROUTINE
*
* This generic subroutine allocates units(premium) to a component
* following receipt of premium. It is identical to the subroutine
* UNITALOC except that UTRN records are created at Coverage
* level. The UTRN Sequence Number is used to differentiate
* between Coverages & Riders. (See Para 2).
*
* Also a new file has been introduced, ZRRA, which retains the
* UTRN monies date. It is used when units are re-allocated after
* a reversal, to ensure consistent dates are used in unit pricing.
*
*    Firstly the module has to establish premium allocations which
* may be split between initial units, accumulation units and non-
* invested amounts. This process also applies to any unit enhance-
* ment which may be relevant.
*
*    Once the net  allocation  to  units  is  established, the  unit
* apportionment file (ULNKPF)  is  accessed  to find out which funds
* are to be  invested in. Finally the unit transaction file (UTRNPF)
* is updated with  a  space  in  the 'feedback indicator' which will
* trigger later processing by the unit dealing batch run.
*
*
* 1.) Establish premium allocation.
*
*
*    This processing is  centred around two 'loops' which go through
* the premium allocation file (INCIPF) and within each of the INCIPF
* records:
*
*    Read all INCIPF  records  for  this component using the logical
* view INCIRNL and allocate the INCI-CURR-PREM amount as follows:
*
*        Within each INCIPF record read  through  each occurrence of
*     INCI-PREM-CURR and carry out the  following  processing  until
*     the INCI-CURR-PREM been fully allocated:
*
*        If this occurrence of INCI-PREM-CURR is zero ignore it and
*          look up the next occurrence.
*        If the premium we are allocating is greater than this
*          occurrence of INCI-PREM-CURR perform Calculate Split
*          using INCI-PREM-CURR as the amount.
*        If INCI-PREM-CURR > premium to be allocated perform
*          Calculate Split using the premium to be allocated as the
*          amount.
*
* Calculate Split.
*
*        Compute the non-invested premium as:
*
*     Amount to be allocated * (100 - INCI-PCUNIT)
*     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*                          100
*
*        Compute the allocation to initial units as:
*
*     (Amount to be allocated - non invested prem) * INCI-UNIT-SPLIT
*     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*                          100
*
*        Compute the allocation to accumulation units as:
*
*     Amount to be allocated - (non invested prem + initial units)
*
*        Accumulate these three amounts in working storage as there
*          may be more to be added to them if the premium falls
*          within two or more bands of allocation.
*        Update the INCIPF record by subtracting the amount of
*          allocation (for this occurrence) from the relevant
*          INCI-PREM-CURR field.
*        If the total of the amounts allocated in working storage =
*          INCI-CURR-PREM (i.e. the premium has been allocated)
*          rewrite the INCIPF record back to the database
*        Else
*        Calculate the amount to be allocated for next time round
*          the loop as:
*             INCI-CURR-PREM - total of working storage amounts
*
*    After processing all  INCIPF  records  the total amount held in
* the 3 working  storage  areas  should  equal the amount of premium
* originally passed in linkage - if not this is a fatal error.
*
*    After  finding the amounts to be allocated we need to check for
* any  enhancements.  Firstly  we need to look up table T6647 with a
* key  of  transaction code/contract type - if the enhancement basis
* field is spaces go to the next section.
*
*    Look   up   T5545  with  a  key  of  transaction  code/enhanced
* allocation basis (from T6647). Find the occurrence of fields which
* relates to the billing frequency as passed in  the linkage area to
* this program.  If the instalment premium is  greater than or equal
* to any of the premium occurrences for this frequency.  If it is we
* can use the corresponding enhancement percentage:
*
*        Multiply allocation to initial units by enhancement
*          percentage (then divide by 100).
*        Multiply allocation to accumulation units by enhancement
*          percentage (then divide by 100).
*
*
* 2.) Establish Fund allocation split.
*
*    Calculate the initial unit discount factor. First look up
* T5540 for the discount basis, then use this basis to access
* T5519. We use the information in T5519 to decide whether to
* look up T5539 or T6646 for the actual discount figure. Both
* T5539 and T6646 are accessed by the discount factor in T5540.
*
*    Each  coverage has a  specific  apportionment  for  units  into
* different funds. This should not  be confused with unit allocation
* which   determines   how    premium    is    distributed   between
* non-investment, initial and accumulation units. This fund split is
* held in the ULNKPF file.
*
*    Read  the  ULNKPF  file using the logical view ULNKRNL with the
* component key.  Write  a  UTRN  for  each  of the fund allocations
* (initial or accumulation) in working storage which are not 0. They
* should be  apportioned  between  the  funds as held on ULNKPF. The
* Units are allocated at Coverage level to be consistent with the
* benefit billing structure. To idenitfy unit holdings on a Coverage
* or Rider the UTRN sequence number is composed from
* T6647-SEQUENCE-NO + RNLA-Rider.
* The UTRNPF record should be set up as follows:
*
*          - Component key from COVRPF except Rider value.
*          - Rider set to '00'.
*          - Virtual Fund from ULNKPF
*          - Unit type dependent on working storage type being
*                                                  processed.
*          - Tran-no from linkage
*          - Term ID and user from run parameters
*          - Transaction date and time from system parameters
*          - Batch key as set up in this program
*          - Unit sub account 'INIT' or 'ACUM' dependent on type
*                                           being processed.
*          - Now/Deferred indicator from T6647 using transaction/
*            contract type
*            If this is an off movement (negative amount) look up
*            the deallocation indicator - if this is now (look at
*            copybook CBNOWDEF) move 'N' to this field else move 'D'.
*            If this is an on  movement (positive amount) look up
*            the allocation indicator - if this is now (look at
*            copybook CBNOWDEF) move 'N' to this field else move 'D'.
*          - Monies date as bill date from linkage
*          - CRTABLE from COVRPF
*          - Contract currency from linkage
*          - Contract amount = allocation amount
*          - Fund currency from T5515 (key = fund)
*          - SACS code and type and GL code from linkage
*          - Contract type from linkage
*          - T6647 processing sequence number + Rider from linkage
*          - SVP = 1
*          - CR comm date - from COVRPF
*
*    Write  the  record  to  the database.
*
*
* 3.) Write the uninvested record.
*
*
*    After  dealing  with  the  allocation  a  single UTRNPF must be
* created  for the non investment allocation. Create a UTRNPF record
* as follows:
*
*          - Initialise all fields
*          - Component key from COVRPF
*          - Tran-no from linkage
*          - Term ID and user from run parameters
*          - Transaction date and time from system parameters
*          - Batch key as set up in this program
*          - Unit sub account 'NVST'
*          - Monies date as bill date from linkage
*          - CRTABLE from COVRPF
*          - Contract currency from linkage
*          - Contract amount = uninvested amount
*          - SACS code and type and GL code from linkage
*          - Contract type from linkage
*          - CR comm date from COVRPF
*          - Processing sequence number from (T6647 + COVR-RIDER)
*
*    Write the record to the database.
*
* 4.) Finally, when all INCI records have been processed, if
*     this contract had previously been reversed, a record
*     with the original instalment date would be present.
*     This must be deleted.
*
****************************************************************
*
* </pre>
*/
public class Zrulaloc extends SMARTCodeModel {
	private static final Logger LOGGER = LoggerFactory.getLogger(Zrulaloc.class);

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZRULALOC";
	private PackedDecimalData wsaaInciCurrPrem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1).init("N");
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
		/* WSAA-RIDER */
	private FixedLengthStringData wsaaRiderAlpha = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRiderNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderAlpha, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5545Item, 4);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTref, 15);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaTaxs = new FixedLengthStringData(34);
	private ZonedDecimalData[] wsaaTax = ZDArrayPartOfStructure(2, 17, 2, wsaaTaxs, 0);
	private PackedDecimalData wsaaTotCd = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaFact = new ZonedDecimalData(3, 0);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String g027 = "G027";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private IncirnlTableDAM incirnlIO = new IncirnlTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private ZrraTableDAM zrraIO = new ZrraTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private ZrstnudTableDAM zrstnudIO = new ZrstnudTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	protected Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5545rec t5545rec = new T5545rec();
	private T5515rec t5515rec = new T5515rec();
	private T6647rec t6647rec = new T6647rec();
	protected T5540rec t5540rec = new T5540rec();
	private T6646rec t6646rec = new T6646rec();
	private T5539rec t5539rec = new T5539rec();
	private T5519rec t5519rec = new T5519rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Rnlallrec rnlallrec = new Rnlallrec();
	private FormatsInner formatsInner = new FormatsInner();
	protected TablesInner tablesInner = new TablesInner();
	protected WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();
	private ExternalisedRules er = new ExternalisedRules();
	private static final String  FMC_FEATURE_ID = "BTPRO026";	//IBPLIFE-1379
	private boolean fmcOnFlag = false;	//IBPLIFE-1379
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextIncirnl105, 
		readIncirnl110, 
		fundAllocation180, 
		exit190, 
		setSeqNo215, 
		readT5729280, 
		readZrra285, 
		notProrata2050, 
		exit2900, 
		enhanb3200, 
		enhanc3300, 
		enhand3400, 
		enhane3500, 
		enhanf3600, 
		nearExit3800, 
		next4150, 
		accumUnit4200, 
		next4250, 
		nearExit4800, 
		comlvlacc5105, 
		cont5110, 
		writrUtrn5800, 
		writrHitr5250, 
		fixedTerm6200, 
		wholeOfLife6300, 
		exit6900, 
		b300PostTax2
	}

	public Zrulaloc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rnlallrec.rnlallRec = convertAndSetParam(rnlallrec.rnlallRec, parmArray, 0);
		try {
			process();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

private void process()
	{
		mainline100();
		initialisation200();
		a200DeleteZrra();
		getT5539300();
		getT6646400();
		systemError570();
	}

protected void mainline100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para100();
				case nextIncirnl105: 
					nextIncirnl105();
				case readIncirnl110: 
					readIncirnl110();
				case fundAllocation180: 
					fundAllocation180();
				case exit190: 
					exit190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		initialisation200();
		/* Read all INCIRNL records for this component.*/
		/* Only unit linked contract will have a corresponding INCI record.*/
		/* There could be more than one INCI record for each coverage/rider*/
		/* If it is not a unit linked contract, no processing will be*/
		/* required.*/
		incirnlIO.setDataKey(SPACES);
		incirnlIO.setChdrcoy(rnlallrec.company);
		incirnlIO.setChdrnum(rnlallrec.chdrnum);
		incirnlIO.setLife(rnlallrec.life);
		incirnlIO.setCoverage(rnlallrec.coverage);
		incirnlIO.setRider(rnlallrec.rider);
		incirnlIO.setPlanSuffix(rnlallrec.planSuffix);
		/* MOVE BEGNH                  TO INCIRNL-FUNCTION.             */
		incirnlIO.setFunction(varcom.begn);
		//performance improvement -- Niharika Modi 
//		incirnlIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		incirnlIO.setFormat(formatsInner.incirnlrec);
		goTo(GotoLabel.readIncirnl110);
	}

protected void nextIncirnl105()
	{
		incirnlIO.setFunction(varcom.nextr);
		wsaaAmountHoldersInner.wsaaInciPerd[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciPerd[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciTot[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciTot[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[2].set(ZERO);
	}

protected void readIncirnl110()
	{
		SmartFileCode.execute(appVars, incirnlIO);
		if ((isNE(incirnlIO.getStatuz(),varcom.oK))
		&& (isNE(incirnlIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(incirnlIO.getStatuz());
			syserrrec.params.set(incirnlIO.getParams());
			databaseError580();
		}
		if (isEQ(incirnlIO.getStatuz(),varcom.endp)) {
			a200DeleteZrra();
			goTo(GotoLabel.exit190);
		}
		if ((isNE(incirnlIO.getChdrcoy(),rnlallrec.company))
		|| (isNE(incirnlIO.getChdrnum(),rnlallrec.chdrnum))
		|| (isNE(incirnlIO.getLife(),rnlallrec.life))
		|| (isNE(incirnlIO.getCoverage(),rnlallrec.coverage))
		|| (isNE(incirnlIO.getRider(),rnlallrec.rider))
		|| (isNE(incirnlIO.getPlanSuffix(),rnlallrec.planSuffix))) {
			a200DeleteZrra();
			goTo(GotoLabel.exit190);
		}
		/*     MOVE REWRT              TO INCIRNL-FUNCTION              */
		/*     CALL 'INCIRNLIO' USING INCIRNL-PARAMS                    */
		/*     IF (INCIRNL-STATUZ NOT = O-K)                            */
		/*         MOVE INCIRNL-STATUZ TO SYSR-STATUZ                   */
		/*         MOVE INCIRNL-PARAMS TO SYSR-PARAMS                   */
		/*         PERFORM 580-DATABASE-ERROR                           */
		/*     ELSE                                                     */
		/*         GO TO 190-EXIT.                                      */
		/* For each INCIRNL record read, split the instalment into*/
		/* 3 different portions accordingly.*/
		wsaaAmountHoldersInner.wsaaNonInvestPremTot.set(0);
		wsaaAmountHoldersInner.wsaaTotUlSplit.set(0);
		wsaaAmountHoldersInner.wsaaTotIbSplit.set(0);
		wsaaAmountHoldersInner.wsaaAllocInitTot.set(0);
		wsaaAmountHoldersInner.wsaaAllocTot.set(0);
		wsaaAmountHoldersInner.wsaaAllocAccumTot.set(0);
		/*                                                         <D9604>*/
		/* If there is an amount passed in the RNLA-TOTRECD        <D9604>*/
		/* this must be a pro-rata premium amount passed from      <D9604>*/
		/* issue as in all other cases this amount will be zero.   <D9604>*/
		/* When this occurs, use the RNLA-COVR-INSTPREM as the prem<D9604>*/
		/* allocate, not the INCIRNL-CURR-PREM.                    <D9604>*/
		/*                                                         <D9604>*/
		if (isGT(rnlallrec.totrecd,0)) {
			wsaaInciCurrPrem.set(incirnlIO.getCurrPrem());
			wsaaAmountHoldersInner.wsaaInciPrem.set(rnlallrec.covrInstprem);
		}
		else {
			wsaaAmountHoldersInner.wsaaInciPrem.set(incirnlIO.getCurrPrem());
		}
		/*    MOVE INCIRNL-CURR-PREM          TO WSAA-INCI-PREM.   <D9604> */
		wsaaAmountHoldersInner.wsaaIndex.set(1);
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex, 7))
		|| (isEQ(wsaaAmountHoldersInner.wsaaInciPrem, 0)))) {
			premiumAllocation1000();
		}
		
		/* Rewrite the INCIRNL record once the instalment premium has*/
		/* been allocated fully.*/
		/* MOVE REWRT                  TO INCIRNL-FUNCTION.             */
		incirnlIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, incirnlIO);
		if ((isNE(incirnlIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(incirnlIO.getStatuz());
			syserrrec.params.set(incirnlIO.getParams());
			databaseError580();
		}
		/* Check for any enhancement for these preimums from T6647.*/
		/* (T6647 has been read in 200-INITIALISATION section.)*/
		if (isEQ(t6647rec.enhall,SPACES)) {
			goTo(GotoLabel.fundAllocation180);
		}
		/*                                                         <D9604>*/
		/* Enhanced allocation is not applicable for flexible premi<D9604>*/
		/* contracts. Therefore, bypass processing.                <D9604>*/
		/*                                                         <D9604>*/
		if (flexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.fundAllocation180);
		}
		/* Read T5545 to get the enhancement percentage.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5545);
		itdmIO.setItmfrm(rnlallrec.effdate);
		wsaaT5545Cntcurr.set(rnlallrec.cntcurr);
		wsaaT5545Enhall.set(t6647rec.enhall);
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
				
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5545))
		|| (isNE(itdmIO.getItemitem(),wsaaT5545Item))) {
			itdmIO.setGenarea(SPACES);
		}
		t5545rec.t5545Rec.set(itdmIO.getGenarea());
		if (isEQ(t5545rec.t5545Rec,SPACES)) {
			goTo(GotoLabel.fundAllocation180);
		}
		/* Look for the correct percentage.*/
		wsaaAmountHoldersInner.wsaaEnhancePerc.set(0);
		wsaaAmountHoldersInner.wsaaIndex2.set(1);
		/* Should use the total coverage premium instead of the current*/
		/* INCI premium to decide if enhanced allocation is needed.*/
		/* Store the current INCI premium away and put the total coverage*/
		/* premium into it instead so that we dont't have to amend the code*/
		/* in 3000-.*/
		/* Make sure RNLA-COVR-INSTPREM is numeric first, just in case of*/
		/* the calling program has not updated the field correctly.*/
		if (isEQ(rnlallrec.covrInstprem,NUMERIC)) {
			wsaaInciCurrPrem.set(incirnlIO.getCurrPrem());
			incirnlIO.setCurrPrem(rnlallrec.covrInstprem);
		}
//		MIBT-279 STARTS
		if ( isEQ(rnlallrec.covrInstprem,rnlallrec.totrecd)  &&  isNE(rnlallrec.totrecd,ZERO)  )
		{
			incirnlIO.setCurrPrem(wsaaInciCurrPrem);		
		}
//		MIBT-279 Ends		
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex2, 6))
		|| (isNE(wsaaAmountHoldersInner.wsaaEnhancePerc, 0)))) {
			//matchEnhanceBasis3000();
			//IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible START
			//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
			
			//VPMS Fix made for VEUAL calculation rule - start
			chdrenqIO.setParams(SPACES);
			chdrenqIO.setChdrcoy(rnlallrec.company);
			chdrenqIO.setChdrnum(rnlallrec.chdrnum);
			chdrenqIO.setFormat(formatsInner.chdrenqrec);
			chdrenqIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				databaseError580();
			}
			
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VEUAL") && er.isExternalized(chdrenqIO.cnttype.toString(), rnlallrec.crtable.toString())))
			{
				matchEnhanceBasis3000();
			}
			else
			{				

				Untallrec untallrec = new Untallrec();
				untallrec.untaCompany.set(rnlallrec.company);
				untallrec.untaBatctrcde.set(rnlallrec.batctrcde);
				untallrec.untaEnhall.set(t6647rec.enhall);
				untallrec.untaCntcurr.set(rnlallrec.cntcurr);
				untallrec.untaCalcType.set("S");
				untallrec.untaEffdate.set(rnlallrec.effdate);
				untallrec.untaInstprem.set(incirnlIO.currPrem);
				untallrec.untaAllocInit.set(wsaaAmountHoldersInner.wsaaAllocInitTot);
				untallrec.untaAllocAccum.set(wsaaAmountHoldersInner.wsaaAllocAccumTot);
				untallrec.untaBillfreq.set(rnlallrec.billfreq);	
	
				callProgram("VEUAL", untallrec.untallrec,chdrenqIO);
				
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(untallrec.untaEnhancePerc);	
				wsaaAmountHoldersInner.wsaaEnhanceAlloc.set(untallrec.untaEnhanceAlloc);
				wsaaAmountHoldersInner.wsaaIndex2.add(1);
				
			}
			//VPMS Fix made for VEUAL calculation rule - end
			//IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible END
		}
		
		/* Move the original amount back to current INCI premium.*/
		if (isEQ(rnlallrec.covrInstprem,NUMERIC)) {
			incirnlIO.setCurrPrem(wsaaInciCurrPrem);
		}
		if (isEQ(wsaaAmountHoldersInner.wsaaEnhancePerc, 0)) {
			goTo(GotoLabel.fundAllocation180);
		}
		/* Calculate the enhanced premiums.*/
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInitTot);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocInitTot.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaOldAllocInitTot.set(wsaaAmountHoldersInner.wsaaAllocInitTot);
		compute(wsaaAmountHoldersInner.wsaaAllocInitTot, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInitTot);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocInitTot.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(sub(wsaaAmountHoldersInner.wsaaOldAllocInitTot, wsaaAmountHoldersInner.wsaaAllocInitTot));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccumTot);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocAccumTot.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaOldAllocAccumTot.set(wsaaAmountHoldersInner.wsaaAllocAccumTot);
		compute(wsaaAmountHoldersInner.wsaaAllocAccumTot, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccumTot);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocAccumTot.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(add(wsaaAmountHoldersInner.wsaaEnhanceAlloc, (sub(wsaaAmountHoldersInner.wsaaOldAllocAccumTot, wsaaAmountHoldersInner.wsaaAllocAccumTot))));
	}

protected void fundAllocation180()
	{
		/* Read ULNKRNL to get the fund split for the coverage/rider.*/
		ulnkrnlIO.setDataKey(SPACES);
		ulnkrnlIO.setChdrcoy(rnlallrec.company);
		ulnkrnlIO.setChdrnum(rnlallrec.chdrnum);
		ulnkrnlIO.setLife(rnlallrec.life);
		ulnkrnlIO.setCoverage(rnlallrec.coverage);
		ulnkrnlIO.setRider(rnlallrec.rider);
		ulnkrnlIO.setPlanSuffix(rnlallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(formatsInner.ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			syserrrec.params.set(ulnkrnlIO.getParams());
			databaseError580();
		}
		/* Calculate initial unit discount factor.*/
		/* Write a UTRN record for each fund split.*/
		initUnitDiscDetails6000();
		wsaaAmountHoldersInner.wsaaIndex2.set(1);
		wsaaAmountHoldersInner.wsaaRunInitTot.set(0);
		wsaaAmountHoldersInner.wsaaRunTot.set(0);
		wsaaAmountHoldersInner.wsaaTotUlSplit.set(0);
		wsaaAmountHoldersInner.wsaaTotIbSplit.set(0);
		wsaaAmountHoldersInner.wsaaRunAccumTot.set(0);
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex2, 10))
		|| (isEQ(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2), SPACES)))) {
			splitAllocation4000();
		}
		
		/* Write a UTRN record for the uninvested premium.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPremTot, 0).set(add(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaEnhanceAlloc));
		/* IF WSAA-NON-INVEST-PREM-TOT NOT = 0                  <INTBR> */
		/*     MOVE SPACE              TO UTRN-PARAMS           <INTBR> */
		/*     MOVE 'NVST'             TO UTRN-UNIT-SUB-ACCOUNT <INTBR> */
		/*     MOVE WSAA-NON-INVEST-PREM-TOT TO UTRN-CONTRACT-AMOUNT    */
		/*     MOVE 0                  TO UTRN-DISCOUNT-FACTOR  <INTBR> */
		/*     PERFORM 5000-SET-UP-WRITE-UTRN.                  <INTBR> */
		if (isNE(wsaaAmountHoldersInner.wsaaNonInvestPremTot, 0)) {
			if (isGT(wsaaAmountHoldersInner.wsaaTotUlSplit, 0)) {
				utrnIO.setParams(SPACES);
				utrnIO.setUnitSubAccount("NVST");
				setPrecision(utrnIO.getContractAmount(), 0);
				utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaTotUlSplit), 100));
				utrnIO.setDiscountFactor(0);
				setPrecision(utrnIO.getInciprm01(), 0);
				utrnIO.setInciprm01(div(mult(wsaaAmountHoldersInner.wsaaInciNInvs[1], wsaaAmountHoldersInner.wsaaTotUlSplit), 100));
				zrdecplrec.amountIn.set(utrnIO.getInciprm01());
				callRounding8000();
				utrnIO.setInciprm01(zrdecplrec.amountOut);
				setPrecision(utrnIO.getInciprm02(), 0);
				utrnIO.setInciprm02(div(mult(wsaaAmountHoldersInner.wsaaInciNInvs[2], wsaaAmountHoldersInner.wsaaTotUlSplit), 100));
				zrdecplrec.amountIn.set(utrnIO.getInciprm02());
				callRounding8000();
				utrnIO.setInciprm02(zrdecplrec.amountOut);
				setUpWriteUtrn5000();
				if (isGT(wsaaAmountHoldersInner.wsaaTotIbSplit, 0)) {
					hitrIO.setParams(SPACES);
					setPrecision(hitrIO.getContractAmount(), 2);
					hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaNonInvestPremTot, utrnIO.getContractAmount()));
					setPrecision(hitrIO.getInciprm01(), 2);
					hitrIO.setInciprm01(sub(wsaaAmountHoldersInner.wsaaInciNInvs[1], utrnIO.getInciprm01()));
					zrdecplrec.amountIn.set(hitrIO.getInciprm01());
					callRounding8000();
					hitrIO.setInciprm01(zrdecplrec.amountOut);
					setPrecision(hitrIO.getInciprm02(), 2);
					hitrIO.setInciprm02(sub(wsaaAmountHoldersInner.wsaaInciNInvs[1], utrnIO.getInciprm02()));
					zrdecplrec.amountIn.set(hitrIO.getInciprm02());
					callRounding8000();
					hitrIO.setInciprm02(zrdecplrec.amountOut);
					setUpWriteHitr5200();
				}
			}
			else {
				hitrIO.setParams(SPACES);
				hitrIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPremTot);
				hitrIO.setInciprm01(wsaaAmountHoldersInner.wsaaInciNInvs[1]);
				hitrIO.setInciprm02(wsaaAmountHoldersInner.wsaaInciNInvs[2]);
				zrdecplrec.amountIn.set(hitrIO.getInciprm01());
				callRounding8000();
				hitrIO.setInciprm01(zrdecplrec.amountOut);
				zrdecplrec.amountIn.set(hitrIO.getInciprm02());
				callRounding8000();
				hitrIO.setInciprm02(zrdecplrec.amountOut);
				setUpWriteHitr5200();
			}
		}
		/*  before going back to 105-NEXT-INCIRNL paragraph,               */
		/*  do the following;                                              */
		fmcOnFlag = FeaConfg.isFeatureExist(rnlallrec.company.toString().trim(), FMC_FEATURE_ID, appVars, "IT"); 
		if(!fmcOnFlag){	//IBPLIFE-1379
		if (isNE(wsaaAmountHoldersInner.wsaaNonInvestPremTot, 0)) {
			b100ProcessTax();
		}
		}
		goTo(GotoLabel.nextIncirnl105);
	}

protected void exit190()
	{
		exitProgram();
	}

protected void stop199()
	{
		stopRun();
	}

protected void initialisation200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para201();
					readT6647210();
				case setSeqNo215: 
					setSeqNo215();
					readT5540220();
					readT5645240();
					readT5688250();
					readT5519260();
				case readT5729280: 
					readT5729280();
				case readZrra285: 
					readZrra285();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para201()
	{
		syserrrec.subrname.set(wsaaSubr);
		rnlallrec.statuz.set(varcom.oK);
		varcom.vrcmTimex.set(getCobolTime());
		varcom.vrcmTime.set(varcom.vrcmTimen);
		varcom.vrcmDate.set(getCobolDate());
		wsaaAmountHoldersInner.wsaaInciPerd[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciPerd[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciTot[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciTot[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[2].set(ZERO);
	}

protected void readT6647210()
	{
		/* Read T6647 for the relevant details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		itdmIO.setItmfrm(rnlallrec.effdate);
		wsaaT6647Batctrcde.set(rnlallrec.batctrcde);
		wsaaT6647Cnttype.set(rnlallrec.cnttype);
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.setSeqNo215);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Item))) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.setSeqNo215);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void setSeqNo215()
	{
		/* Add Rider to T6647-SEQ-NO. This will be used to identify*/
		/* Coverages from Riders when reversing UTRN's.*/
		wsaaRiderAlpha.set(rnlallrec.rider);
		compute(wsaaAmountHoldersInner.wsaaSeqNo, 0).set(add(wsaaRiderNum, t6647rec.procSeqNo));
	}

protected void readT5540220()
	{
		/* Read T5540 for the general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(rnlallrec.crtable);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
		|| (isNE(itdmIO.getItemitem(),rnlallrec.crtable))) {
			itdmIO.setGenarea(SPACES);
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
	}

protected void readT5645240()
	{
		/* Read T5645 for the SUB ACC details.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT5688250()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItmfrm(rnlallrec.moniesDate);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(rnlallrec.cnttype);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),rnlallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),rnlallrec.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(rnlallrec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			systemError570();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readT5519260()
	{
		/* Read T5519 for initial unit discount details.*/
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			/*        GO TO 290-EXIT.                                  <D9604>*/
			goTo(GotoLabel.readT5729280);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5519);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5519))
		|| (isNE(itdmIO.getItemitem(),t5540rec.iuDiscBasis))) {
			itdmIO.setItemitem(t5540rec.iuDiscBasis);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g027);
			systemError570();
		}
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
	}

protected void readT5729280()
	{
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5729);
		itdmIO.setItemitem(rnlallrec.cnttype);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(rnlallrec.company, SPACES);
			stringVariable1.addExpression(tablesInner.t5729, SPACES);
			stringVariable1.addExpression(rnlallrec.cnttype, SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),rnlallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5729)
		|| isNE(itdmIO.getItemitem(),rnlallrec.cnttype)) {
			/*    GO TO 290-EXIT                                            */
			goTo(GotoLabel.readZrra285);
		}
		wsaaFlexPrem.set("Y");
	}

protected void readZrra285()
	{
		zrraIO.setParams(SPACES);
		zrraIO.setChdrcoy(rnlallrec.company);
		zrraIO.setChdrnum(rnlallrec.chdrnum);
		zrraIO.setLife(rnlallrec.life);
		zrraIO.setCoverage(rnlallrec.coverage);
		zrraIO.setRider(rnlallrec.rider);
		zrraIO.setPlanSuffix(rnlallrec.planSuffix);
		zrraIO.setInstfrom(rnlallrec.duedate);
		zrraIO.setBillfreq(rnlallrec.billfreq);
		zrraIO.setFormat(formatsInner.zrrarec);
		zrraIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, zrraIO);
		if ((isNE(zrraIO.getStatuz(),varcom.oK))
		&& (isNE(zrraIO.getStatuz(),varcom.mrnf))) {
			syserrrec.statuz.set(zrraIO.getStatuz());
			syserrrec.params.set(zrraIO.getParams());
			databaseError580();
		}
	}

protected void a200DeleteZrra()
	{
		/*A200-PARA*/
		/* Delete ZRRA.*/
		if (isNE(zrraIO.getStatuz(),varcom.oK)) {
			return ;
		}
		zrraIO.setFormat(formatsInner.zrrarec);
		zrraIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, zrraIO);
		if (isNE(zrraIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zrraIO.getStatuz());
			syserrrec.params.set(zrraIO.getParams());
			databaseError580();
		}
	}

protected void getT5539300()
	{
			para310();
		}

protected void para310()
	{
		if (isEQ(t5540rec.iuDiscFact,SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.t5539);
		itemIO.setItemitem(t5540rec.iuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
	}

protected void getT6646400()
	{
			para410();
		}

protected void para410()
	{
		if (isEQ(t5540rec.wholeIuDiscFact,SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.t6646);
		itemIO.setItemitem(t5540rec.wholeIuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}

protected void systemError570()
	{
			para570();
			exit579();
		}

protected void para570()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError580()
	{
			para580();
			exit589();
		}

protected void para580()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void premiumAllocation1000()
	{
					para1010();
					nearExit1800();
				}

protected void para1010()
	{
		/* WSAA-ALLOC-AMT is the amount to be allocated to buy units.*/
		/* It is not necessary to be the same as the premium.(Depends*/
		/* on INCI details.)*/
		if (isEQ(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), 0)) {
			return ;
		}
		if (isGTE(wsaaAmountHoldersInner.wsaaInciPrem, incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex))) {
			wsaaAmountHoldersInner.wsaaAllocAmt.set(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex));
		}
		if (isGT(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), wsaaAmountHoldersInner.wsaaInciPrem)) {
			wsaaAmountHoldersInner.wsaaAllocAmt.set(wsaaAmountHoldersInner.wsaaInciPrem);
		}
		calculateSplit2000();
	}

protected void nearExit1800()
	{
		wsaaAmountHoldersInner.wsaaIndex.add(1);
		/*EXIT*/
	}

protected void calculateSplit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2010();
				case notProrata2050: 
					notProrata2050();
				case exit2900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2010()
	{
		/* Calculate the uninvested allocation.*/
		wsaaAmountHoldersInner.wsaaEnhanceAlloc.set(0);
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 1).setRounded(div((mult(wsaaAmountHoldersInner.wsaaAllocAmt, (sub(100, incirnlIO.getPcunit(wsaaAmountHoldersInner.wsaaIndex))))), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying initial units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 1).setRounded(div(mult((sub(wsaaAmountHoldersInner.wsaaAllocAmt, wsaaAmountHoldersInner.wsaaNonInvestPrem)), incirnlIO.getUsplitpc(wsaaAmountHoldersInner.wsaaIndex)), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying accumulation units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 1).setRounded(sub(wsaaAmountHoldersInner.wsaaAllocAmt, (add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaAllocInit))));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		/* Accumulate totals.*/
		wsaaAmountHoldersInner.wsaaNonInvestPremTot.add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		wsaaAmountHoldersInner.wsaaAllocInitTot.add(wsaaAmountHoldersInner.wsaaAllocInit);
		wsaaAmountHoldersInner.wsaaAllocAccumTot.add(wsaaAmountHoldersInner.wsaaAllocAccum);
		/* Update the premium left to pay for that period.*/
		setPrecision(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), 1);
		incirnlIO.setPremcurr(wsaaAmountHoldersInner.wsaaIndex, sub(incirnlIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), wsaaAmountHoldersInner.wsaaAllocAmt), true);
		/* Store the INCI amounts in working storage.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaInciPerd[1], ZERO)) {
			wsaaAmountHoldersInner.wsaaInciPerd[1].set(wsaaAmountHoldersInner.wsaaIndex);
			wsaaAmountHoldersInner.wsaaInciInit[1].add(wsaaAmountHoldersInner.wsaaAllocInit);
			wsaaAmountHoldersInner.wsaaInciAlloc[1].add(wsaaAmountHoldersInner.wsaaAllocAccum);
			wsaaAmountHoldersInner.wsaaInciNInvs[1].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		else {
			wsaaAmountHoldersInner.wsaaInciPerd[2].set(wsaaAmountHoldersInner.wsaaIndex);
			wsaaAmountHoldersInner.wsaaInciInit[2].add(wsaaAmountHoldersInner.wsaaAllocInit);
			wsaaAmountHoldersInner.wsaaInciAlloc[2].add(wsaaAmountHoldersInner.wsaaAllocAccum);
			wsaaAmountHoldersInner.wsaaInciNInvs[2].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		/* If the instalment premium is fully allocated, get out and*/
		/* rewrite the INCIRNL record. Otherwise work out premium left*/
		/* to allocate and go round the loop again.*/
		/* If we are allocating a pro-rata premium (ie RNLA-TOTRECD<D9604>*/
		/* not = zero) compare the coverage instalment premium     <D9604>*/
		/* passed not the INCIRNL-CURR-PREM as this is the         <D9604>*/
		/* amount we are allocating.                               <D9604>*/
		/*                                                         <D9604>*/
		if (isEQ(rnlallrec.totrecd,ZERO)) {
			goTo(GotoLabel.notProrata2050);
		}
		if ((setPrecision(rnlallrec.covrInstprem, 0)
		&& isEQ(rnlallrec.covrInstprem, (add(add(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot))))) {
			wsaaAmountHoldersInner.wsaaInciPrem.set(0);
		}
		else {
			compute(wsaaAmountHoldersInner.wsaaInciPrem, 3).setRounded(sub(sub(sub(rnlallrec.covrInstprem, wsaaAmountHoldersInner.wsaaNonInvestPremTot), wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot));
		}
		goTo(GotoLabel.exit2900);
	}

protected void notProrata2050()
	{
		if ((setPrecision(incirnlIO.getCurrPrem(), 0)
		&& isEQ(incirnlIO.getCurrPrem(), (add(add(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot))))) {
			wsaaAmountHoldersInner.wsaaInciPrem.set(0);
		}
		else {
			compute(wsaaAmountHoldersInner.wsaaInciPrem, 3).setRounded(sub(sub(sub(incirnlIO.getCurrPrem(), wsaaAmountHoldersInner.wsaaNonInvestPremTot), wsaaAmountHoldersInner.wsaaAllocInitTot), wsaaAmountHoldersInner.wsaaAllocAccumTot));
		}
	}

protected void matchEnhanceBasis3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3010();
					enhana3100();
				case enhanb3200: 
					enhanb3200();
				case enhanc3300: 
					enhanc3300();
				case enhand3400: 
					enhand3400();
				case enhane3500: 
					enhane3500();
				case enhanf3600: 
					enhanf3600();
				case nearExit3800: 
					nearExit3800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3010()
	{
		/* First match the billing frequency.*/
		if (isEQ(t5545rec.billfreq[wsaaAmountHoldersInner.wsaaIndex2.toInt()], SPACES)) {
			goTo(GotoLabel.nearExit3800);
		}
		if (isNE(rnlallrec.billfreq, t5545rec.billfreq[wsaaAmountHoldersInner.wsaaIndex2.toInt()])) {
			goTo(GotoLabel.nearExit3800);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.
	* There are 5 premiums in each billing frequency set, so
	* match each premium individually.
	* </pre>
	*/
protected void enhana3100()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 1)) {
			goTo(GotoLabel.enhanb3200);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc02);
					}
					else {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhanb3200()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 2)) {
			goTo(GotoLabel.enhanc3300);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc02);
					}
					else {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhanc3300()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 3)) {
			goTo(GotoLabel.enhand3400);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc02);
					}
					else {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhand3400()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 4)) {
			goTo(GotoLabel.enhane3500);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc02);
					}
					else {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhane3500()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 5)) {
			goTo(GotoLabel.enhanf3600);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc02);
					}
					else {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit3800);
	}

protected void enhanf3600()
	{
		if (isNE(wsaaAmountHoldersInner.wsaaIndex2, 6)) {
			goTo(GotoLabel.nearExit3800);
		}
		if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05,0))) {
			wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc05);
		}
		else {
			if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04,0))) {
				wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc04);
			}
			else {
				if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03,0))) {
					wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc03);
				}
				else {
					if ((isGTE(incirnlIO.getCurrPrem(),t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02,0))) {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc02);
					}
					else {
						wsaaAmountHoldersInner.wsaaEnhancePerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void nearExit3800()
	{
		wsaaAmountHoldersInner.wsaaIndex2.add(1);
		/*EXIT*/
	}

protected void splitAllocation4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initUnit4100();
				case next4150: 
					next4150();
				case accumUnit4200: 
					accumUnit4200();
				case next4250: 
					next4250();
				case nearExit4800: 
					nearExit4800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initUnit4100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),rnlallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.zfundtyp,"D")) {
			compute(wsaaAmountHoldersInner.wsaaAllocTot, 0).set(add(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaAllocAccumTot));
			wsaaAmountHoldersInner.wsaaTotIbSplit.add(ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2));
			if (isNE(wsaaAmountHoldersInner.wsaaAllocTot, 0)) {
				calcHitrAmount5100();
				setUpWriteHitr5200();
			}
			goTo(GotoLabel.nearExit4800);
		}
		wsaaAmountHoldersInner.wsaaTotUlSplit.add(ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2));
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocInitTot, 0)) {
			goTo(GotoLabel.accumUnit4200);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaRunInitTot));
			goTo(GotoLabel.next4150);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaRunInitTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocInitTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
		}
	}

protected void next4150()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		callRounding8000();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		utrnIO.setUnitType("I");
		utrnIO.setUnitSubAccount("INIT");
		utrnIO.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
		setUpWriteUtrn5000();
	}

protected void accumUnit4200()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocAccumTot, 0)) {
			goTo(GotoLabel.nearExit4800);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaRunAccumTot));
			goTo(GotoLabel.next4250);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaRunAccumTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocAccumTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
		}
	}

protected void next4250()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		callRounding8000();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		utrnIO.setUnitType("A");
		utrnIO.setUnitSubAccount("ACUM");
		utrnIO.setDiscountFactor(0);
		utrnIO.setFundPool(ulnkrnlIO.getFundPool(wsaaAmountHoldersInner.wsaaIndex2));
		setUpWriteUtrn5000();
	}

protected void nearExit4800()
	{
		wsaaAmountHoldersInner.wsaaIndex2.add(1);
		/*EXIT*/
	}

protected void setUpWriteUtrn5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5100();
				case comlvlacc5105: 
					comlvlacc5105();
				case cont5110: 
					cont5110();
				case writrUtrn5800: 
					writrUtrn5800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5100()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5515))
		|| (isNE(itdmIO.getItemitem(),utrnIO.getUnitVirtualFund()))) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		utrnIO.setChdrcoy(rnlallrec.company);
		utrnIO.setChdrnum(rnlallrec.chdrnum);
		utrnIO.setCoverage(rnlallrec.coverage);
		utrnIO.setLife(rnlallrec.life);
		//ILAE-65
//		utrnIO.setRider("00");
		utrnIO.setRider(rnlallrec.rider);
		utrnIO.setPlanSuffix(rnlallrec.planSuffix);
		utrnIO.setTranno(rnlallrec.tranno);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		utrnIO.setTransactionDate(varcom.vrcmDate);
		utrnIO.setUser(rnlallrec.user);
		utrnIO.setBatccoy(rnlallrec.batccoy);
		utrnIO.setBatcbrn(rnlallrec.batcbrn);
		utrnIO.setBatcactyr(rnlallrec.batcactyr);
		utrnIO.setBatcactmn(rnlallrec.batcactmn);
		utrnIO.setBatctrcde(rnlallrec.batctrcde);
		utrnIO.setBatcbatch(rnlallrec.batcbatch);
		utrnIO.setCrtable(rnlallrec.crtable);
		utrnIO.setCntcurr(rnlallrec.cntcurr);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			goTo(GotoLabel.comlvlacc5105);
		}
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		goTo(GotoLabel.cont5110);
	}

protected void comlvlacc5105()
	{
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode04);
			utrnIO.setSacstyp(t5645rec.sacstype04);
			utrnIO.setGenlcde(t5645rec.glmap04);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode03);
			utrnIO.setSacstyp(t5645rec.sacstype03);
			utrnIO.setGenlcde(t5645rec.glmap03);
		}
	}

protected void cont5110()
	{
		utrnIO.setContractType(rnlallrec.cnttype);
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(rnlallrec.crdate);
		utrnIO.setFundCurrency(t5515rec.currcode);
		if (isLT(utrnIO.getContractAmount(),0)) {
			utrnIO.setNowDeferInd(t6647rec.dealin);
		}
		else {
			utrnIO.setNowDeferInd(t6647rec.aloind);
		}
		/* Calculate the monies date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaMoniesDate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode,"BD")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(t6647rec.efdcode,"DD")) {
				wsaaMoniesDate.set(rnlallrec.duedate);
			}
			else {
				if (isEQ(t6647rec.efdcode,"LO")) {
					if (isGT(rnlallrec.duedate,wsaaMoniesDate)) {
						wsaaMoniesDate.set(rnlallrec.duedate);
					}
				}
			}
		}
		/*  AS ISSUE SETS UP THIS INFO CORRECTLY ,ITS SEEMS POINTLESS*/
		/*  TO REPEAT / OR REMOVE CORRECT CODING.*/
		/* If this instalment has been reversed before, use the original*/
		/* monies date to ensure no descrpency in unit prices for now*/
		/* and then.*/
		if (isEQ(zrraIO.getStatuz(),varcom.oK)) {
			utrnIO.setMoniesDate(zrraIO.getMoniesDate());
		}
		else {
			if (isEQ(rnlallrec.batctrcde,"T642")) {
				utrnIO.setMoniesDate(rnlallrec.moniesDate);
			}
			else {
				utrnIO.setMoniesDate(wsaaMoniesDate);
			}
		}
		utrnIO.setProcSeqNo(wsaaAmountHoldersInner.wsaaSeqNo);
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setNofUnits(0);
		utrnIO.setNofDunits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setFundAmount(0);
		utrnIO.setSurrenderPercent(0);
		utrnIO.setUstmno(0);
		/*                                UTRN-INCIPRM01        <INTBR> */
		/*                                UTRN-INCIPRM02.       <INTBR> */
		utrnIO.setInciNum(incirnlIO.getSeqno());
		utrnIO.setInciPerd01(wsaaAmountHoldersInner.wsaaInciPerd[1]);
		utrnIO.setInciPerd02(wsaaAmountHoldersInner.wsaaInciPerd[2]);
		
		if (isEQ(rnlallrec.batctrcde,"T642")) 
		{
			utrnIO.setFundPool("1");
		}
		
		if (isNE(wsaaAmountHoldersInner.wsaaEnhancePerc, ZERO)) {
			compute(wsaaAmountHoldersInner.wsaaContractAmount, 2).set(mult((div(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaEnhancePerc)), 100));
		}
		else {
			wsaaAmountHoldersInner.wsaaContractAmount.set(utrnIO.getContractAmount());
		}
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaContractAmount);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaContractAmount.set(zrdecplrec.amountOut);
		/* Use the amounts previously stored for each allocation*/
		/* period rather than the  whole contract amounts.*/
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			/*     MOVE WSAA-INCI-N-INVS (01) TO UTRN-INCIPRM01             */
			/*     MOVE WSAA-INCI-N-INVS (02) TO UTRN-INCIPRM02             */
			goTo(GotoLabel.writrUtrn5800);
		}
		if (isEQ(utrnIO.getUnitSubAccount(),"INIT")) {
			compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult((div(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocInitTot)), 100));
			if (isGT(wsaaAmountHoldersInner.wsaaInciInit[1], ZERO)) {
				setPrecision(utrnIO.getInciprm01(), 1);
				utrnIO.setInciprm01(div((mult(wsaaAmountHoldersInner.wsaaInciInit[1], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
				zrdecplrec.amountIn.set(utrnIO.getInciprm01());
				callRounding8000();
				utrnIO.setInciprm01(zrdecplrec.amountOut);
			}
			else {
				utrnIO.setInciprm01(ZERO);
			}
			if (isGT(wsaaAmountHoldersInner.wsaaInciInit[2], ZERO)) {
				setPrecision(utrnIO.getInciprm02(), 1);
				utrnIO.setInciprm02(div((mult(wsaaAmountHoldersInner.wsaaInciInit[2], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
				zrdecplrec.amountIn.set(utrnIO.getInciprm02());
				callRounding8000();
				utrnIO.setInciprm02(zrdecplrec.amountOut);
			}
			else {
				utrnIO.setInciprm02(ZERO);
			}
			goTo(GotoLabel.writrUtrn5800);
		}
		if (isEQ(utrnIO.getUnitSubAccount(),"ACUM")) {
			compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult((div(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocAccumTot)), 100));
			if (isGT(wsaaAmountHoldersInner.wsaaInciAlloc[1], ZERO)) {
				setPrecision(utrnIO.getInciprm01(), 1);
				utrnIO.setInciprm01(div((mult(wsaaAmountHoldersInner.wsaaInciAlloc[1], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
				zrdecplrec.amountIn.set(utrnIO.getInciprm01());
				callRounding8000();
				utrnIO.setInciprm01(zrdecplrec.amountOut);
			}
			else {
				utrnIO.setInciprm01(ZERO);
			}
			if (isGT(wsaaAmountHoldersInner.wsaaInciAlloc[2], ZERO)) {
				setPrecision(utrnIO.getInciprm02(), 1);
				utrnIO.setInciprm02(div((mult(wsaaAmountHoldersInner.wsaaInciAlloc[2], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
				zrdecplrec.amountIn.set(utrnIO.getInciprm02());
				callRounding8000();
				utrnIO.setInciprm02(zrdecplrec.amountOut);
			}
			else {
				utrnIO.setInciprm02(ZERO);
			}
		}
	}

protected void writrUtrn5800()
	{
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(formatsInner.utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			databaseError580();
		}
		/*EXIT*/
	}

protected void calcHitrAmount5100()
	{
		calc5110();
	}

	/**
	* <pre>
	*******************************                           <INTBR> 
	* </pre>
	*/
protected void calc5110()
	{
		hitrIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(hitrIO.getContractAmount(), 0);
			hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
		}
		else {
			if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
				setPrecision(hitrIO.getContractAmount(), 0);
				hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
			}
			else {
				setPrecision(hitrIO.getContractAmount(), 1);
				hitrIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
				compute(wsaaAmountHoldersInner.wsaaInitAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInitTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100));
				compute(wsaaAmountHoldersInner.wsaaAccumAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccumTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100));
			}
		}
		zrdecplrec.amountIn.set(hitrIO.getContractAmount());
		callRounding8000();
		hitrIO.setContractAmount(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaInitAmount);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaInitAmount.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAccumAmount);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAccumAmount.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunTot.add(hitrIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunInitTot.add(wsaaAmountHoldersInner.wsaaInitAmount);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(wsaaAmountHoldersInner.wsaaAccumAmount);
		hitrIO.setZintbfnd(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
	}

protected void setUpWriteHitr5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5210();
				case writrHitr5250: 
					writrHitr5250();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	********************************                          <INTBR> 
	* </pre>
	*/
protected void para5210()
	{
		hitrIO.setChdrcoy(rnlallrec.company);
		hitrIO.setChdrnum(rnlallrec.chdrnum);
		hitrIO.setCoverage(rnlallrec.coverage);
		hitrIO.setLife(rnlallrec.life);
		hitrIO.setRider("00");
		hitrIO.setPlanSuffix(rnlallrec.planSuffix);
		hitrIO.setTranno(rnlallrec.tranno);
		hitrIO.setBatccoy(rnlallrec.batccoy);
		hitrIO.setBatcbrn(rnlallrec.batcbrn);
		hitrIO.setBatcactyr(rnlallrec.batcactyr);
		hitrIO.setBatcactmn(rnlallrec.batcactmn);
		hitrIO.setBatctrcde(rnlallrec.batctrcde);
		hitrIO.setBatcbatch(rnlallrec.batcbatch);
		hitrIO.setCrtable(rnlallrec.crtable);
		hitrIO.setCntcurr(rnlallrec.cntcurr);
		if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
			if (isEQ(t5688rec.comlvlacc,"Y")) {
				hitrIO.setSacscode(t5645rec.sacscode08);
				hitrIO.setSacstyp(t5645rec.sacstype08);
				hitrIO.setGenlcde(t5645rec.glmap08);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode06);
				hitrIO.setSacstyp(t5645rec.sacstype06);
				hitrIO.setGenlcde(t5645rec.glmap06);
			}
		}
		else {
			if (isEQ(t5688rec.comlvlacc,"Y")) {
				hitrIO.setSacscode(t5645rec.sacscode07);
				hitrIO.setSacstyp(t5645rec.sacstype07);
				hitrIO.setGenlcde(t5645rec.glmap07);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode05);
				hitrIO.setSacstyp(t5645rec.sacstype05);
				hitrIO.setGenlcde(t5645rec.glmap05);
			}
		}
		hitrIO.setCnttyp(rnlallrec.cnttype);
		hitrIO.setSvp(1);
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setProcSeqNo(wsaaAmountHoldersInner.wsaaSeqNo);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(incirnlIO.getSeqno());
		hitrIO.setInciPerd01(wsaaAmountHoldersInner.wsaaInciPerd[1]);
		hitrIO.setInciPerd02(wsaaAmountHoldersInner.wsaaInciPerd[2]);
		/* Use the amounts previously stored for each allocation   <INTBR> */
		/* period rather than the  whole contract amounts.         <INTBR> */
		if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
			goTo(GotoLabel.writrHitr5250);
		}
		compute(wsaaAmountHoldersInner.wsaaInciTot[1], 0).set(add(wsaaAmountHoldersInner.wsaaInciInit[1], wsaaAmountHoldersInner.wsaaInciAlloc[1]));
		compute(wsaaAmountHoldersInner.wsaaInciTot[2], 0).set(add(wsaaAmountHoldersInner.wsaaInciInit[2], wsaaAmountHoldersInner.wsaaInciAlloc[2]));
		compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult(div(hitrIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocTot), 100));
		if (isGT(wsaaAmountHoldersInner.wsaaInciTot[1], ZERO)) {
			setPrecision(hitrIO.getInciprm01(), 1);
			hitrIO.setInciprm01(div(mult(wsaaAmountHoldersInner.wsaaInciTot[1], wsaaAmountHoldersInner.wsaaFundPc), 100), true);
			zrdecplrec.amountIn.set(hitrIO.getInciprm01());
			callRounding8000();
			hitrIO.setInciprm01(zrdecplrec.amountOut);
		}
		else {
			hitrIO.setInciprm01(ZERO);
		}
		if (isGT(wsaaAmountHoldersInner.wsaaInciTot[2], ZERO)) {
			setPrecision(hitrIO.getInciprm02(), 1);
			hitrIO.setInciprm02(div(mult(wsaaAmountHoldersInner.wsaaInciTot[2], wsaaAmountHoldersInner.wsaaFundPc), 100), true);
			zrdecplrec.amountIn.set(hitrIO.getInciprm02());
			callRounding8000();
			hitrIO.setInciprm02(zrdecplrec.amountOut);
		}
		else {
			hitrIO.setInciprm02(ZERO);
		}
	}

protected void writrHitr5250()
	{
		if (isEQ(rnlallrec.batctrcde,"T642")) 
		{
			hitrIO.setFundPool("1");
		}
			
		if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
			hitrIO.setZrectyp("N");
		}
		else {
			hitrIO.setZrectyp("P");
		}
		hitrIO.setEffdate(rnlallrec.effdate);
		hitrIO.setZintappind(SPACES);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZintrate(ZERO);
		hitrIO.setFunction(varcom.writr);
		hitrIO.setFormat(formatsInner.hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			syserrrec.statuz.set(hitrIO.getStatuz());
			databaseError580();
		}
	}

protected void initUnitDiscDetails6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6010();
					termToRun6100();
				case fixedTerm6200: 
					fixedTerm6200();
				case wholeOfLife6300: 
					wholeOfLife6300();
				case exit6900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6010()
	{
		wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(1);
		if (((isEQ(t5540rec.iuDiscBasis,SPACES))
		|| (isEQ(t5540rec.iuDiscFact,SPACES)))
		&& (isEQ(t5540rec.wholeIuDiscFact,SPACES))) {
			goTo(GotoLabel.exit6900);
		}
	}

	/**
	* <pre>
	* There are 3 methods to obtain the discount factor, which one to
	* use is dependent on the field entries in T5519 & T5540 which
	* has been read in initialisation section.(Only one method
	* allowed.)
	* </pre>
	*/
protected void termToRun6100()
	{
		/* If whole of life discount factor(T5540-WHOLE-IU-DISC-FACT) is*/
		/* blank,(i.e. term discount basis not blank) and if fixed term*/
		/* (T5519-FIXTRM) is zero, use this the term left to run to read*/
		/* off T5539 to obtain the discount factor.*/
		if (isNE(t5540rec.wholeIuDiscFact,SPACES)) {
			goTo(GotoLabel.wholeOfLife6300);
		}
		if (isNE(t5519rec.fixdtrm,0)) {
			goTo(GotoLabel.fixedTerm6200);
		}
		if (isEQ(rnlallrec.termLeftToRun,0)) {
			goTo(GotoLabel.exit6900);
		}
		getT5539300();
		/*    MOVE T5539-DFACT(RNLA-TERM-LEFT-TO-RUN) TO                   */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(rnlallrec.termLeftToRun, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[rnlallrec.termLeftToRun.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(rnlallrec.termLeftToRun, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void fixedTerm6200()
	{
		/* Use this fixed term to read off T5539 to obtain the*/
		/* discount factor.*/
		getT5539300();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void wholeOfLife6300()
	{
		/* Use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		if (isEQ(rnlallrec.anbAtCcd,0)) {
			return ;
		}
		getT6646400();
		/*    MOVE T6646-DFACT(RNLA-ANB-AT-CCD) TO                         */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(rnlallrec.anbAtCcd, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.dfact[rnlallrec.anbAtCcd.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(rnlallrec.anbAtCcd, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		return ;
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(rnlallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rnlallrec.cntcurr);
		zrdecplrec.batctrcde.set(rnlallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError580();
		}
		/*EXIT*/
	}

protected void b100ProcessTax()
	{
		b100Start();
	}

protected void b100Start()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaNonInvestPremTot, 0)) {
			return ;
		}
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(rnlallrec.company);
		chdrenqIO.setChdrnum(rnlallrec.chdrnum);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError580();
		}
		/*  Read coverage/rider record.                                    */
		covrunlIO.setParams(SPACES);
		covrunlIO.setChdrcoy(rnlallrec.company);
		covrunlIO.setChdrnum(rnlallrec.chdrnum);
		covrunlIO.setLife(rnlallrec.life);
		covrunlIO.setCoverage(rnlallrec.coverage);
		covrunlIO.setRider(rnlallrec.rider);
		covrunlIO.setPlanSuffix(rnlallrec.planSuffix);
		covrunlIO.setFunction(varcom.readr);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			databaseError580();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrenqIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(rnlallrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
		wsaaTr52eCrtable.set(rnlallrec.crtable);
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(rnlallrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(rnlallrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		if (isNE(tr52erec.taxind05, "Y")) {
			return ;
		}
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(covrunlIO.getLife());
		txcalcrec.coverage.set(covrunlIO.getCoverage());
		txcalcrec.rider.set(covrunlIO.getRider());
		txcalcrec.planSuffix.set(covrunlIO.getPlanSuffix());
		txcalcrec.crtable.set(covrunlIO.getCrtable());
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrenqIO.getCntcurr());
		wsaaCntCurr.set(chdrenqIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.transType.set("NINV");
		txcalcrec.effdate.set(rnlallrec.effdate);
		txcalcrec.tranno.set(chdrenqIO.getTranno());
		txcalcrec.language.set(rnlallrec.language);
		txcalcrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPremTot);
		txcalcrec.batckey.set(rnlallrec.batchkey);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.jrnseq.set(0);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			databaseError580();
		}
		if (isEQ(txcalcrec.taxAmt[1], 0)
		&& isEQ(txcalcrec.taxAmt[2], 0)) {
			return ;
		}
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(covrunlIO.getChdrcoy());
		taxdIO.setChdrnum(covrunlIO.getChdrnum());
		taxdIO.setLife(covrunlIO.getLife());
		taxdIO.setCoverage(covrunlIO.getCoverage());
		taxdIO.setRider(covrunlIO.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(txcalcrec.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setInstfrom(varcom.vrcmMaxDate);
		wsaaTranref.set(SPACES);
		wsaaTaxs.fill("0");
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setTranno(rnlallrec.tranno);
		taxdIO.setTrantype("NINV");
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setInstfrom(rnlallrec.duedate);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setBillcd(rnlallrec.billcd);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax[1].set(0);
		}
		else {
			wsaaTax[1].set(txcalcrec.taxAmt[1]);
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax[2].set(0);
		}
		else {
			wsaaTax[2].set(txcalcrec.taxAmt[2]);
		}
		if (isEQ(wsaaTax[1], 0)
		&& isEQ(wsaaTax[2], 0)) {
			return ;
		}
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstnudIO.setChdrnum(covrunlIO.getChdrnum());
		zrstnudIO.setLife(covrunlIO.getLife());
		zrstnudIO.setFormat(formatsInner.zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstnudIO.getStatuz(), varcom.endp))) {
			b200ReadZrst();
		}
		
		compute(wsaaTotCd, 2).add(add(wsaaTax[1], wsaaTax[2]));
		b300WriteZrst();
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstnudIO.setChdrnum(covrunlIO.getChdrnum());
		zrstnudIO.setLife(covrunlIO.getLife());
		zrstnudIO.setFormat(formatsInner.zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstnudIO.getStatuz(), varcom.endp))) {
			b400UpdateZrst();
		}
		
		/* Update the coverage file coverage debt.                         */
		setPrecision(covrunlIO.getCoverageDebt(), 2);
		covrunlIO.setCoverageDebt(add(covrunlIO.getCoverageDebt(), add(wsaaTax[1], wsaaTax[2])));
		covrunlIO.setFunction(varcom.writd);
		covrunlIO.setFormat("COVRREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			databaseError580();
		}
		/* Post coverage debt                                              */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(rnlallrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(rnlallrec.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(rnlallrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(rnlallrec.chdrnum);
		lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
		lifacmvrec.batccoy.set(rnlallrec.company);
		lifacmvrec.rldgcoy.set(rnlallrec.company);
		lifacmvrec.genlcoy.set(rnlallrec.company);
		lifacmvrec.batcactyr.set(rnlallrec.batcactyr);
		lifacmvrec.batctrcde.set(rnlallrec.batctrcde);
		lifacmvrec.batcactmn.set(rnlallrec.batcactmn);
		lifacmvrec.batcbatch.set(rnlallrec.batcbatch);
		lifacmvrec.batcbrn.set(rnlallrec.batcbrn);
		lifacmvrec.origcurr.set(rnlallrec.cntcurr);
		compute(lifacmvrec.origamt, 2).set(add(wsaaTax[1], wsaaTax[2]));
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(rnlallrec.tranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(rnlallrec.effdate);
		/* Set up TRANREF to contain the Full component key of the         */
		/*  Coverage/Rider that is Creating the debt so that if a          */
		/*  reversal is required at a later date, the correct component    */
		/*  can be identified.                                             */
		wsaaTref.set(SPACES);
		wsaaTrefChdrcoy.set(rnlallrec.company);
		wsaaTrefChdrnum.set(rnlallrec.chdrnum);
		wsaaPlan.set(rnlallrec.planSuffix);
		wsaaTrefLife.set(rnlallrec.life);
		wsaaTrefCoverage.set(rnlallrec.coverage);
		wsaaTrefRider.set(rnlallrec.rider);
		wsaaTrefPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaTref);
		lifacmvrec.user.set(rnlallrec.user);
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(rnlallrec.chdrnum);
			wsaaPlan.set(rnlallrec.planSuffix);
			wsaaRldgLife.set(rnlallrec.life);
			wsaaRldgCoverage.set(rnlallrec.coverage);
			/*        Post ACMV against the Coverage, not Rider since the      */
			/*         debt is also set against the Coverage                   */
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/*        Set correct substitution code                            */
			lifacmvrec.substituteCode[6].set(covrunlIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.contot.set(t5645rec.cnttot09);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(rnlallrec.chdrnum);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(rnlallrec.cnttype);
		lifacmvrec.transactionDate.set(rnlallrec.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void b200ReadZrst()
	{
		/*B200-START*/
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)
		&& isNE(zrstnudIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstnudIO.getParams());
			/*        MOVE ZRSTNUD-STATUZ     TO SYSR-PARAMS           <LA4895>*/
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError580();
		}
		if (isNE(zrstnudIO.getChdrcoy(), covrunlIO.getChdrcoy())
		|| isNE(zrstnudIO.getChdrnum(), covrunlIO.getChdrnum())
		|| isNE(zrstnudIO.getLife(), covrunlIO.getLife())
		|| isEQ(zrstnudIO.getStatuz(), varcom.endp)) {
			zrstnudIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaTotCd.add(zrstnudIO.getZramount01());
		zrstnudIO.setFunction(varcom.nextr);
		/*B200-EXIT*/
	}

protected void b300WriteZrst()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b300Start();
				case b300PostTax2: 
					b300PostTax2();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b300Start()
	{
		zrstIO.setParams(SPACES);
		zrstIO.setZramount01(0);
		zrstIO.setZramount02(0);
		zrstIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstIO.setChdrnum(covrunlIO.getChdrnum());
		zrstIO.setLife(covrunlIO.getLife());
		zrstIO.setJlife(SPACES);
		zrstIO.setCoverage(covrunlIO.getCoverage());
		zrstIO.setRider(covrunlIO.getRider());
		zrstIO.setBatctrcde(rnlallrec.batctrcde);
		zrstIO.setTranno(rnlallrec.tranno);
		zrstIO.setTrandate(rnlallrec.effdate);
		wsaaSeqno.add(1);
		zrstIO.setXtranno(ZERO);
		zrstIO.setSeqno(wsaaSeqno);
		zrstIO.setUstmno(ZERO);
		zrstIO.setFormat(formatsInner.zrstrec);
		if (isEQ(wsaaTax[1], 0)) {
			goTo(GotoLabel.b300PostTax2);
		}
		zrstIO.setZramount01(wsaaTax[1]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (1)      TO ZRST-SACSTYP          <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[1]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			databaseError580();
		}
	}

protected void b300PostTax2()
	{
		if (isEQ(wsaaTax[2], 0)) {
			return ;
		}
		zrstIO.setZramount01(wsaaTax[2]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (2)      TO ZRST-SACSTYP          <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[2]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			databaseError580();
		}
	}

protected void b400UpdateZrst()
	{
		b400Start();
	}

protected void b400Start()
	{
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)
		&& isNE(zrstnudIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.params.set(zrstnudIO.getStatuz());
			databaseError580();
		}
		if (isNE(zrstnudIO.getChdrcoy(), covrunlIO.getChdrcoy())
		|| isNE(zrstnudIO.getChdrnum(), covrunlIO.getChdrnum())
		|| isNE(zrstnudIO.getLife(), covrunlIO.getLife())
		|| isEQ(zrstnudIO.getStatuz(), varcom.endp)) {
			zrstnudIO.setStatuz(varcom.endp);
			return ;
		}
		zrstnudIO.setZramount02(wsaaTotCd);
		zrstnudIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError580();
		}
		zrstnudIO.setFunction(varcom.nextr);
	}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
public static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAmt = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaNonInvestPremTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaOldAllocInitTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocInitTot = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaOldAllocAccumTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocAccumTot = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaInitAmount = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAccumAmount = new ZonedDecimalData(18, 3).init(0);
	public ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaInciPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaIndex2 = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaEnhancePerc = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private PackedDecimalData[] wsaaInciPerd = PDInittedArray(2, 1, 0);
	private PackedDecimalData[] wsaaInciprm = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciInit = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciAlloc = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciTot = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciNInvs = PDInittedArray(2, 17, 2);
	private PackedDecimalData wsaaContractAmount = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaFundPc = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSeqNo = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaTotUlSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaTotIbSplit = new ZonedDecimalData(5, 2).init(0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t6646 = new FixedLengthStringData(5).init("T6646");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5519 = new FixedLengthStringData(5).init("T5519");
	private FixedLengthStringData t5539 = new FixedLengthStringData(5).init("T5539");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData incirnlrec = new FixedLengthStringData(10).init("INCIRNLREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData zrrarec = new FixedLengthStringData(10).init("ZRRAREC   ");
	private FixedLengthStringData ulnkrnlrec = new FixedLengthStringData(10).init("ULNKRNLREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC   ");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).init("HITRREC   ");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).init("ZRSTREC");
	private FixedLengthStringData zrstnudrec = new FixedLengthStringData(10).init("ZRSTNUDREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	}
}
