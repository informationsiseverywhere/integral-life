/*
 * File: Br547.java
 * Date: 29 August 2009 22:19:30
 * Author: Quipoz Limited
 * 
 * Class transformed from BR547.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;

import com.csc.life.unitlinkedprocessing.dataaccess.UstfstmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Ulparams;
import com.csc.smart.procedures.Conlog;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.ConversionUtil;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
* ANNIVERSARY UNIT STATEMENTS - EXTRACT MISSING TRANSACTION RECORDS
*
*    This  is  one of the  modules in the Anniversary Unit Link
* processing(L2ANNIVPRC).
*
*    It runs before the BR542 which prints the Unit statement
* - Anniversary Report. It is going to pick up those policies
* which have never been printed under Unit Statement batch job
* (L2UNITSTMT) before; which creates USTF.
*
*    This is because the Anniversary Program assumes the
* existence of a file USTF which stores the opening and closing
* banlances.
*
*    It uses OPNQRYF in the calling CL program to select all
* contracts  which  are  due for anniversary unit statement.
*
*    For each contract, check the Unit Link transaction details
* file (UTRS), If the Unit Balance for each fund is greater zero,
* check if USTF for that func is found.
*
*    If it does not exist, create a new USTF record for the fund
* whith initialized value for the statement Unit/Cash/Deem Unit
* values.
*
*    Control totals maintained by this program are:
*    ==============
*
*      1. Total CHDRs read
*      2. CHDRs processed
*      3. CHDRs not processed
*      4. no. of USTF created
*      5.
*      6.
*      7. NO. OF URTS FAILED FOR READ
*
*
*****************************************************************
* </pre>
*/
public class Br547 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR547");
	private String endOfFile = "N";

	private FixedLengthStringData wsaaCreatedFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaCreated = new Validator(wsaaCreatedFlag, "Y");

	private FixedLengthStringData wsaaFinishFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaFinish = new Validator(wsaaFinishFlag, "Y");
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private String e676 = "E676";
		/* FORMATS */
	private String ustfrec = "USTFREC";
	private String utrsrec = "UTRSREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private Ulparams ulparams = new Ulparams();
		/*Unit Statements - Fund details*/
	private UstfstmTableDAM ustfstmIO = new UstfstmTableDAM();
		/*Unit transaction summary*/
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Varcom varcom = new Varcom();
	
	private Iterator<Chdrpf> chdrpfIterator;
	private Chdrpf chdrpfEntity;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1900, 
		readUstf2002, 
		nextUtrs2008, 
		exit2009
	}

	public Br547() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		chdrpfIterator = (Iterator<Chdrpf>) parmArray[1];
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAINLINE*/
		initialise200();
		/*MAIN-PROCESS*/
		while ( !(isEQ(endOfFile,"Y"))) {
			mainProcessing1000();
		}
		
		/*OUT*/
		/*MAIN-EXIT*/
	}

protected void initialise200()
	{
		/*INITIALISE*/
		ulparams.parmRecord.set(conjobrec.params);
		endOfFile = "N";
		/*EXIT*/
	}

protected void mainProcessing1000()
	{
		try {
			process1000();
		}
		catch (GOTOException e){
		}
	}

protected void process1000()
	{
		updateReqd004();
		if (isEQ(controlrec.flag,"N")) {
			goTo(GotoLabel.exit1900);
		}
		if (chdrpfIterator.hasNext()) {
			chdrpfEntity = chdrpfIterator.next();
		} else {
			endOfFile = "Y";
			goTo(GotoLabel.exit1900);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		utrsIO.setDataArea(SPACES);
		utrsIO.setChdrcoy(ConversionUtil.toFixedLengthString(chdrpfEntity.getChdrcoy()));
		wsaaChdrcoy.set(ConversionUtil.toFixedLengthString(chdrpfEntity.getChdrcoy()));
		utrsIO.setChdrnum(ConversionUtil.toFixedLengthString(chdrpfEntity.getChdrnum()));
		wsaaChdrnum.set(ConversionUtil.toFixedLengthString(chdrpfEntity.getChdrnum()));
		utrsIO.setPlanSuffix(ZERO);
		utrsIO.setFunction(varcom.begn);
		utrsIO.setFormat(utrsrec);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(utrsIO.getParams());
			contotrec.totval.set(1);
			contotrec.totno.set(ct05);
			callContot001();
			conlogrec.conlogRec.set(SPACES);
			conlogrec.subrname.set(wsaaProg);
			conlogrec.error.set(e676);
			conlogrec.params.set(utrsIO.getDataKey());
			callProgram(Conlog.class, conlogrec.conlogRec);
			goTo(GotoLabel.exit1900);
		}
		wsaaCreatedFlag.set("N");
		wsaaFinishFlag.set("N");
		while ( !(wsaaFinish.isTrue())) {
			processUtrs2000();
		}
		
		if (wsaaCreated.isTrue()) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void processUtrs2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2000();
				}
				case readUstf2002: {
					readUstf2002();
					createUstf2007();
				}
				case nextUtrs2008: {
					nextUtrs2008();
				}
				case exit2009: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2000()
	{
		if (isNE(utrsIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(utrsIO.getChdrnum(),wsaaChdrnum)
		|| isEQ(utrsIO.getStatuz(),varcom.endp)) {
			wsaaFinishFlag.set("Y");
			goTo(GotoLabel.exit2009);
		}
		if (isGT(utrsIO.getCurrentUnitBal(),ZERO)) {
			goTo(GotoLabel.readUstf2002);
		}
		else {
			goTo(GotoLabel.nextUtrs2008);
		}
	}

protected void readUstf2002()
	{
		ustfstmIO.setDataKey(SPACES);
		ustfstmIO.setChdrcoy(utrsIO.getChdrcoy());
		ustfstmIO.setChdrnum(utrsIO.getChdrnum());
		ustfstmIO.setPlanSuffix(utrsIO.getPlanSuffix());
		ustfstmIO.setLife(utrsIO.getLife());
		ustfstmIO.setCoverage(utrsIO.getCoverage());
		ustfstmIO.setRider(utrsIO.getRider());
		ustfstmIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		ustfstmIO.setUnitType(utrsIO.getUnitType());
		ustfstmIO.setUstmno(99999);
		ustfstmIO.setFormat(ustfrec);
		ustfstmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ustfstmIO);
		if (isNE(ustfstmIO.getStatuz(),varcom.oK)
		&& isNE(ustfstmIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(ustfstmIO.getParams());
			databaseError006();
		}
		if (isEQ(ustfstmIO.getStatuz(),varcom.oK)
		&& isEQ(utrsIO.getChdrcoy(),ustfstmIO.getChdrcoy())
		&& isEQ(utrsIO.getChdrnum(),ustfstmIO.getChdrnum())
		&& isEQ(utrsIO.getPlanSuffix(),ustfstmIO.getPlanSuffix())
		&& isEQ(utrsIO.getLife(),ustfstmIO.getLife())
		&& isEQ(utrsIO.getCoverage(),ustfstmIO.getCoverage())
		&& isEQ(utrsIO.getRider(),ustfstmIO.getRider())
		&& isEQ(utrsIO.getUnitVirtualFund(),ustfstmIO.getUnitVirtualFund())
		&& isEQ(utrsIO.getUnitType(),ustfstmIO.getUnitType())) {
			goTo(GotoLabel.nextUtrs2008);
		}
	}

protected void createUstf2007()
	{
		writeUstf2700();
	}

protected void nextUtrs2008()
	{
		utrsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(utrsIO.getParams());
			databaseError006();
		}
	}

protected void writeUstf2700()
	{
		start2700();
	}

protected void start2700()
	{
		ustfstmIO.setDataArea(SPACES);
		ustfstmIO.setDataKey(SPACES);
		ustfstmIO.setChdrcoy(utrsIO.getChdrcoy());
		ustfstmIO.setChdrnum(utrsIO.getChdrnum());
		ustfstmIO.setPlanSuffix(utrsIO.getPlanSuffix());
		ustfstmIO.setLife(utrsIO.getLife());
		ustfstmIO.setCoverage(utrsIO.getCoverage());
		ustfstmIO.setRider(utrsIO.getRider());
		ustfstmIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		ustfstmIO.setUnitType(utrsIO.getUnitType());
		ustfstmIO.setUstmno(ZERO);
		ustfstmIO.setStmtOpDate(ZERO);
		ustfstmIO.setStmtOpUnits(ZERO);
		ustfstmIO.setStmtOpDunits(ZERO);
		ustfstmIO.setStmtOpFundCash(ZERO);
		ustfstmIO.setStmtOpContCash(ZERO);
		ustfstmIO.setStmtClDate(ZERO);
		ustfstmIO.setStmtClUnits(ZERO);
		ustfstmIO.setStmtClDunits(ZERO);
		ustfstmIO.setStmtClFundCash(ZERO);
		ustfstmIO.setStmtClContCash(ZERO);
		ustfstmIO.setStmtTrnUnits(ZERO);
		ustfstmIO.setStmtTrnDunits(ZERO);
		ustfstmIO.setStmtTrFundCash(ZERO);
		ustfstmIO.setStmtTrContCash(ZERO);
		ustfstmIO.setFormat(ustfrec);
		ustfstmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ustfstmIO);
		if (isNE(ustfstmIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(ustfstmIO.getParams());
			databaseError006();
		}
		wsaaCreatedFlag.set("Y");
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
	}
}
