/*
 * File: T5540pt.java
 * Date: 30 August 2009 2:22:12
 * Author: Quipoz Limited
 * 
 * Class transformed from T5540PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5540.
*
*
*****************************************************************
* </pre>
*/
public class T5540pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("General Non-Traditional Details                S5540");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 16);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 26, FILLER).init("   To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(70);
	private FixedLengthStringData filler9 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Units:   Alloc. Basis Std/Incr:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 37);
	private FixedLengthStringData filler10 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 40, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 45, FILLER).init("Review Processing:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 68).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(41);
	private FixedLengthStringData filler12 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine005, 11, FILLER).init("Default Fund Split Plan:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 37);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(38);
	private FixedLengthStringData filler14 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  Cancellation of Initial units:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 37);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(72);
	private FixedLengthStringData filler15 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  Initial Units  Discount Basis:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 37);
	private FixedLengthStringData filler16 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine007, 41, FILLER).init("    Loyalty/Persistency:");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 68);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(72);
	private FixedLengthStringData filler17 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine008, 17, FILLER).init("Discount Factor:");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 37);
	private FixedLengthStringData filler19 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine008, 41, FILLER).init("    Withdrawal Method:");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 68);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(41);
	private FixedLengthStringData filler20 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler21 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine009, 14, FILLER).init("Or Discount Factor:");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 37);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(32);
	private FixedLengthStringData filler22 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 17, FILLER).init("(Whole of Life)");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(70);
	private FixedLengthStringData filler24 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  Funds:   Available Fund List:");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 37);
	private FixedLengthStringData filler25 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine011, 41, FILLER).init("    Max No Funds/Coverage:");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 68).setPattern("ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5540rec t5540rec = new T5540rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5540pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5540rec.t5540Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5540rec.allbas);
		fieldNo008.set(t5540rec.rvwproc);
		fieldNo009.set(t5540rec.fundSplitPlan);
		fieldNo011.set(t5540rec.iuDiscBasis);
		fieldNo012.set(t5540rec.ltypst);
		fieldNo013.set(t5540rec.iuDiscFact);
		fieldNo014.set(t5540rec.wdmeth);
		fieldNo016.set(t5540rec.alfnds);
		fieldNo017.set(t5540rec.maxfnd);
		fieldNo015.set(t5540rec.wholeIuDiscFact);
		fieldNo010.set(t5540rec.unitCancInit);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
