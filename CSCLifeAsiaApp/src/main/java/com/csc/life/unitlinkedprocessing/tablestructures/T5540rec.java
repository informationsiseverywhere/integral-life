package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:39
 * Description:
 * Copybook name: T5540REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5540rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5540Rec = new FixedLengthStringData(504);
  	public FixedLengthStringData alfnds = new FixedLengthStringData(4).isAPartOf(t5540Rec, 0);
  	public FixedLengthStringData allbas = new FixedLengthStringData(3).isAPartOf(t5540Rec, 4);
  	public FixedLengthStringData fundSplitPlan = new FixedLengthStringData(4).isAPartOf(t5540Rec, 7);
  	public FixedLengthStringData iuDiscFact = new FixedLengthStringData(4).isAPartOf(t5540Rec, 11);
  	public FixedLengthStringData iuDiscBasis = new FixedLengthStringData(4).isAPartOf(t5540Rec, 15);
  	public FixedLengthStringData ltypst = new FixedLengthStringData(4).isAPartOf(t5540Rec, 19);
  	public ZonedDecimalData maxfnd = new ZonedDecimalData(2, 0).isAPartOf(t5540Rec, 23);
  	public ZonedDecimalData rvwproc = new ZonedDecimalData(2, 0).isAPartOf(t5540Rec, 25);
  	public FixedLengthStringData unitCancInit = new FixedLengthStringData(1).isAPartOf(t5540Rec, 27);
  	public FixedLengthStringData wdmeth = new FixedLengthStringData(4).isAPartOf(t5540Rec, 28);
  	public FixedLengthStringData wholeIuDiscFact = new FixedLengthStringData(4).isAPartOf(t5540Rec, 32);
  	public FixedLengthStringData zafropt1 = new FixedLengthStringData(4).isAPartOf(t5540Rec, 36);//BRD-411
  	public FixedLengthStringData zvariance  = new FixedLengthStringData(4).isAPartOf(t5540Rec, 40);//BRD-411
  	public FixedLengthStringData filler = new FixedLengthStringData(464).isAPartOf(t5540Rec, 40, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5540Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5540Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}