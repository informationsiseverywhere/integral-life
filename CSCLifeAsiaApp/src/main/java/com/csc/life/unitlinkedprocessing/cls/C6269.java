/*
 * File: C6269.java
 * Date: 30 August 2009 2:58:53
 * Author: $Id$
 * 
 * Class transformed from C6269.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.unitlinkedprocessing.batchprograms.B6269;
import com.csc.smart.procedures.Passparm;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.query.Opnqryf;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C6269 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData coy = new FixedLengthStringData(1);
	private FixedLengthStringData parmdate = new FixedLengthStringData(8);
	private FixedLengthStringData conj = new FixedLengthStringData(860);
	private FixedLengthStringData chdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData chdrnumto = new FixedLengthStringData(8);
	private java.sql.ResultSet covrpfQryfset = null;
	private java.sql.ResultSet covrpfQryfset1 = null;

	public C6269() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conj});
					chdrnumfrm.set(subString(conj, 1, 8));
					chdrnumto.set(subString(conj, 9, 8));
					branch.set(subString(params, 143, 2));
					parmdate.set(subString(params, 113, 8));
					coy.set(subString(params, 6, 1));
					appVars.overrideTable("COVRPF", null);
					appVars.overrideTableShare("COVRPF", true);
					if (isEQ(chdrnumfrm,"        ")
					&& isEQ(chdrnumto,"        ")) {
						java.sql.ResultSet covrpfQryfset = null;
						Opnqryf.getInstance().execute(new Object[] {"FILE", "FORMAT", "QRYSLT", "KEYFLD", "MAPFLD"}, new Object[] {"COVRPF", "COVRPF", parmdate, new String[] {"CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX"}, new String[] {"CHDRCOY 'COVRPF/CHDRCOY'", "CHDRNUM 'COVRPF/CHDRNUM'", "COVRSTAT 'COVRPF/STATCODE'", "COVRVALID 'COVRPF/VALIDFLAG'", "VALIDFLAG 'CHDRPF/VALIDFLAG'", "STATCODE 'CHDRPF/STATCODE'", "COVRPSTAT 'COVRPF/PSTATCODE'", "ICANDT 'COVRPF/ICANDT'", "CBCVIN 'COVRPF/CBCVIN'", "TRANNO 'COVRPF/TRANNO'", "STATREASN 'COVRPF/STATREASN'", "CAMPAIGN 'COVRPF/CAMPAIGN'", "CURRTO 'COVRPF/CURRTO'", "CURRFROM 'CHDRPF/CURRFROM'", "USRPRF 'CHDRPF/USRPRF'", "JOBNM 'CHDRPF/JOBNM'", "DATIME 'CHDRPF/DATIME'", "BILLCHNL 'CHDRPF/BILLCHNL'"}});
					}
					else {
						java.sql.ResultSet covrpfQryfset1 = null;
						Opnqryf.getInstance().execute(new Object[] {"FILE", "FORMAT", "QRYSLT", "KEYFLD", "MAPFLD"}, new Object[] {"COVRPF", "COVRPF", chdrnumfrm, new String[] {"CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX"}, new String[] {"CHDRCOY 'COVRPF/CHDRCOY'", "CHDRNUM 'COVRPF/CHDRNUM'", "COVRSTAT 'COVRPF/STATCODE'", "COVRVALID 'COVRPF/VALIDFLAG'", "VALIDFLAG 'CHDRPF/VALIDFLAG'", "STATCODE 'CHDRPF/STATCODE'", "COVRPSTAT 'COVRPF/PSTATCODE'", "ICANDT 'COVRPF/ICANDT'", "CBCVIN 'COVRPF/CBCVIN'", "TRANNO 'COVRPF/TRANNO'", "STATREASN 'COVRPF/STATREASN'", "CAMPAIGN 'COVRPF/CAMPAIGN'", "CURRTO 'COVRPF/CURRTO'", "CURRFROM 'COVRPF/CURRFROM'", "USRPRF 'COVRPF/USRPRF'", "JOBNM 'COVRPF/JOBNM'", "DATIME 'COVRPF/DATIME'", "BILLCHNL 'CHDRPF/BILLCHNL'"}});
					}
					callProgram(B6269.class, new Object[] {params});
					try {
						Opnqryf.getInstance().close("COVRPF");
					}
					catch (ExtMsgException ex1){
						if (ex1.messageMatches("CPF4520")) {
						}
						else {
							throw ex1;
						}
					}
					appVars.deleteOverride("COVRPF", ROUTINE);
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.addExtMessage("Unexpected Errors Have Occured"+".");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
