package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:22
 * Description:
 * Copybook name: T6332REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6332rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6332Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData liencds = new FixedLengthStringData(12).isAPartOf(t6332Rec, 0);
  	public FixedLengthStringData[] liencd = FLSArrayPartOfStructure(6, 2, liencds, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(liencds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData liencd01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData liencd02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData liencd03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData liencd04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData liencd05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData liencd06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public ZonedDecimalData premmult = new ZonedDecimalData(5, 2).isAPartOf(t6332Rec, 12);
  	public FixedLengthStringData rsvflg = new FixedLengthStringData(1).isAPartOf(t6332Rec, 17);
  	public ZonedDecimalData sumInsMax = new ZonedDecimalData(15, 0).isAPartOf(t6332Rec, 18);
  	public ZonedDecimalData sumInsMin = new ZonedDecimalData(15, 0).isAPartOf(t6332Rec, 33);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(452).isAPartOf(t6332Rec, 48, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6332Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6332Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}