package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:51
 * Description:
 * Copybook name: UTRNUPDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnupdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnupdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnupdKey = new FixedLengthStringData(64).isAPartOf(utrnupdFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnupdChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnupdKey, 0);
  	public FixedLengthStringData utrnupdChdrnum = new FixedLengthStringData(8).isAPartOf(utrnupdKey, 1);
  	public FixedLengthStringData utrnupdLife = new FixedLengthStringData(2).isAPartOf(utrnupdKey, 9);
  	public FixedLengthStringData utrnupdCoverage = new FixedLengthStringData(2).isAPartOf(utrnupdKey, 11);
  	public FixedLengthStringData utrnupdRider = new FixedLengthStringData(2).isAPartOf(utrnupdKey, 13);
  	public PackedDecimalData utrnupdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnupdKey, 15);
  	public FixedLengthStringData utrnupdUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnupdKey, 18);
  	public FixedLengthStringData utrnupdUnitType = new FixedLengthStringData(1).isAPartOf(utrnupdKey, 22);
  	public PackedDecimalData utrnupdTranno = new PackedDecimalData(5, 0).isAPartOf(utrnupdKey, 23);
  	public PackedDecimalData utrnupdProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(utrnupdKey, 26);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(utrnupdKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnupdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnupdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}