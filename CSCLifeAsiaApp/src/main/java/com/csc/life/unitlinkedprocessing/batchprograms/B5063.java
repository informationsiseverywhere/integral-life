/*
 * File: B5063.java
 * Date: 29 August 2009 20:53:32
 * Author: Quipoz Limited
 * 
 * Class transformed from B5063.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.IncitrdTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*          INCIPF CONVERSION PROGRAM.
*          --------------------------
*
*   INCI conversion program for AQR 3962.
*    Set new field 'Dormant Flag' based on Valid flag.
*     Reset Valid flag values from '2' to '1'.
*
* PROCESSING......
*
*    BEGN on INCI using INCITRD logical view
*
*       If VALID FLAG = '1'
*              Set the new field DORMANT FLAG to 'N'.
*       If VALID FLAG = '2'
*              Set the new field DORMANT FLAG to 'Y',
*              Reset Valid flag to '1'.
*
*    Continue above processing until ENDP statuz is achieved
*        on INCITRD.
*
*****************************************************************
* </pre>
*/
public class B5063 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5063");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
		/* FORMATS */
	private static final String incitrdrec = "INCITRDREC";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private IncitrdTableDAM incitrdIO = new IncitrdTableDAM();
	private Varcom varcom = new Varcom();
	private P6671par p6671par = new P6671par();

	public B5063() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		control1000();
		/*EXIT*/
	}

protected void control1000()
	{
		start1000();
	}

protected void start1000()
	{
		p6671par.parmRecord.set(conjobrec.params);
		wsaaChdrnumfrm.set(p6671par.chdrnum);
		wsaaChdrnumto.set(p6671par.chdrnum1);
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(ZERO);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set("99999999");
		}
		incitrdIO.setChdrnum(wsaaChdrnumfrm);
		incitrdIO.setChdrcoy(SPACES);
		incitrdIO.setLife(SPACES);
		incitrdIO.setCoverage(SPACES);
		incitrdIO.setRider(SPACES);
		incitrdIO.setPlanSuffix(ZERO);
		incitrdIO.setValidflag(SPACES);
		incitrdIO.setTranno(99);
		incitrdIO.setSeqno(ZERO);
		incitrdIO.setInciNum(ZERO);
		incitrdIO.setFunction(varcom.begn);
		incitrdIO.setFormat(incitrdrec);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(incitrdIO.getParams());
			conerrrec.statuz.set(incitrdIO.getStatuz());
			databaseError006();
		}
		while ( !(isEQ(incitrdIO.getStatuz(),varcom.endp))) {
			processInci2000();
		}
		
	}

protected void processInci2000()
	{
		start2100();
	}

protected void start2100()
	{
		/* Check whether Contract is is range or not.*/
		if (isLT(incitrdIO.getChdrnum(), wsaaChdrnumfrm)
		|| isGT(incitrdIO.getChdrnum(), wsaaChdrnumto)) {
			incitrdIO.setStatuz(varcom.endp);
			return ;
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		if (isEQ(incitrdIO.getValidflag(),"1")) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			incitrdIO.setDormantFlag("N");
		}
		if (isEQ(incitrdIO.getValidflag(),"2")) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			incitrdIO.setDormantFlag("Y");
			incitrdIO.setValidflag("1");
		}
		incitrdIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(incitrdIO.getParams());
			conerrrec.statuz.set(incitrdIO.getStatuz());
			databaseError006();
		}
		incitrdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incitrdIO);
		if ((isNE(incitrdIO.getStatuz(),varcom.oK)
		&& isNE(incitrdIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(incitrdIO.getParams());
			conerrrec.statuz.set(incitrdIO.getStatuz());
			databaseError006();
		}
	}
}
