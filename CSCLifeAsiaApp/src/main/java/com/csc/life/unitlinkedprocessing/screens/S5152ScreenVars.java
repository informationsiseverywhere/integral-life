package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5152
 * @version 1.0 generated on 30/08/09 06:36
 * @author Quipoz
 */
public class S5152ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1685);
	public FixedLengthStringData dataFields = new FixedLengthStringData(677).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData virtFundSplitMethod = DD.fndspl.copy().isAPartOf(dataFields,47);
	public ZonedDecimalData instprem = DD.instprem.copyToZonedDecimal().isAPartOf(dataFields,51);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData percOrAmntInd = DD.prcamtind.copy().isAPartOf(dataFields,180);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,181);
	public FixedLengthStringData statfund = DD.statfund.copy().isAPartOf(dataFields,183);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,184);
	public FixedLengthStringData stsubsect = DD.stsubsect.copy().isAPartOf(dataFields,186);
	public FixedLengthStringData unitAllocPercAmts = new FixedLengthStringData(170).isAPartOf(dataFields, 190);
	public ZonedDecimalData[] unitAllocPercAmt = ZDArrayPartOfStructure(10, 17, 2, unitAllocPercAmts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(170).isAPartOf(unitAllocPercAmts, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitAllocPercAmt01 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData unitAllocPercAmt02 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,17);
	public ZonedDecimalData unitAllocPercAmt03 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,34);
	public ZonedDecimalData unitAllocPercAmt04 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,51);
	public ZonedDecimalData unitAllocPercAmt05 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,68);
	public ZonedDecimalData unitAllocPercAmt06 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,85);
	public ZonedDecimalData unitAllocPercAmt07 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,102);
	public ZonedDecimalData unitAllocPercAmt08 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,119);
	public ZonedDecimalData unitAllocPercAmt09 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,136);
	public ZonedDecimalData unitAllocPercAmt10 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler,153);
	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(40).isAPartOf(dataFields, 360);
	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(10, 4, unitVirtualFunds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
	public FixedLengthStringData unitVirtualFund01 = DD.vrtfnd.copy().isAPartOf(filler1,0);
	public FixedLengthStringData unitVirtualFund02 = DD.vrtfnd.copy().isAPartOf(filler1,4);
	public FixedLengthStringData unitVirtualFund03 = DD.vrtfnd.copy().isAPartOf(filler1,8);
	public FixedLengthStringData unitVirtualFund04 = DD.vrtfnd.copy().isAPartOf(filler1,12);
	public FixedLengthStringData unitVirtualFund05 = DD.vrtfnd.copy().isAPartOf(filler1,16);
	public FixedLengthStringData unitVirtualFund06 = DD.vrtfnd.copy().isAPartOf(filler1,20);
	public FixedLengthStringData unitVirtualFund07 = DD.vrtfnd.copy().isAPartOf(filler1,24);
	public FixedLengthStringData unitVirtualFund08 = DD.vrtfnd.copy().isAPartOf(filler1,28);
	public FixedLengthStringData unitVirtualFund09 = DD.vrtfnd.copy().isAPartOf(filler1,32);
	public FixedLengthStringData unitVirtualFund10 = DD.vrtfnd.copy().isAPartOf(filler1,36);
	public FixedLengthStringData winfndopt = DD.winfndopt.copy().isAPartOf(dataFields,400);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,520);
	public FixedLengthStringData zafropt1 = DD.zafropt1.copy().isAPartOf(dataFields,533);
	public FixedLengthStringData zafritem = DD.zafritem.copy().isAPartOf(dataFields,537);
	public FixedLengthStringData unitBidPrices = new FixedLengthStringData(90).isAPartOf(dataFields, 539);
	public ZonedDecimalData[] unitBidPrice = ZDArrayPartOfStructure(10, 9, 5, unitBidPrices, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(90).isAPartOf(unitBidPrices, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitBidPrice01 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,0);
	public ZonedDecimalData unitBidPrice02 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,9);
	public ZonedDecimalData unitBidPrice03 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,18);
	public ZonedDecimalData unitBidPrice04 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,27);
	public ZonedDecimalData unitBidPrice05 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,36);
	public ZonedDecimalData unitBidPrice06 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,45);
	public ZonedDecimalData unitBidPrice07 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,54);
	public ZonedDecimalData unitBidPrice08 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,63);
	public ZonedDecimalData unitBidPrice09 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,72);
	public ZonedDecimalData unitBidPrice10 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler6,81);
	//ILIFE-8164- STARTS
	public FixedLengthStringData newFundLists = new FixedLengthStringData(48).isAPartOf(dataFields, 629);
	public FixedLengthStringData[] newFundList = FLSArrayPartOfStructure(12, 4, newFundLists, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(48).isAPartOf(newFundLists, 0, FILLER_REDEFINE);
	public FixedLengthStringData newFundList01 = DD.newFundList.copy().isAPartOf(filler12,0);
	public FixedLengthStringData newFundList02 = DD.newFundList.copy().isAPartOf(filler12,4);
	public FixedLengthStringData newFundList03 = DD.newFundList.copy().isAPartOf(filler12,8);
	public FixedLengthStringData newFundList04 = DD.newFundList.copy().isAPartOf(filler12,12);
	public FixedLengthStringData newFundList05 = DD.newFundList.copy().isAPartOf(filler12,16);
	public FixedLengthStringData newFundList06 = DD.newFundList.copy().isAPartOf(filler12,20);
	public FixedLengthStringData newFundList07 = DD.newFundList.copy().isAPartOf(filler12,24);
	public FixedLengthStringData newFundList08 = DD.newFundList.copy().isAPartOf(filler12,28);
	public FixedLengthStringData newFundList09 = DD.newFundList.copy().isAPartOf(filler12,32);
	public FixedLengthStringData newFundList10 = DD.newFundList.copy().isAPartOf(filler12,36);
	public FixedLengthStringData newFundList11 = DD.newFundList.copy().isAPartOf(filler12,40);
	public FixedLengthStringData newFundList12 = DD.newFundList.copy().isAPartOf(filler12,44);
	//ILIFE-8164 -ENDS
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 677);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData fndsplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData instpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData prcamtindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData statfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData stsubsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ualprcsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] ualprcErr = FLSArrayPartOfStructure(10, 4, ualprcsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(ualprcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ualprc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData ualprc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData ualprc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData ualprc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData ualprc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData ualprc06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData ualprc07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData ualprc08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData ualprc09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData ualprc10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData vrtfndsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] vrtfndErr = FLSArrayPartOfStructure(10, 4, vrtfndsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(vrtfndsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData vrtfnd01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData vrtfnd02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData vrtfnd03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData vrtfnd04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData vrtfnd05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData vrtfnd06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData vrtfnd07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData vrtfnd08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData vrtfnd09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData vrtfnd10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData winfndoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData zafropt1Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData zafritemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData ubidprsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData[] ubidprErr = FLSArrayPartOfStructure(10, 4, ubidprsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(ubidprsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ubidpr01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData ubidpr02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData ubidpr03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData ubidpr04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData ubidpr05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData ubidpr06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData ubidpr07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData ubidpr08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData ubidpr09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData ubidpr10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	//ILIFE-8164- STARTS
	public FixedLengthStringData newFundListErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData[] newFundListsErr = FLSArrayPartOfStructure(12, 4, newFundListErr, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(48).isAPartOf(newFundListErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData newFundList01Err = new FixedLengthStringData(4).isAPartOf(filler13, 0);
	public FixedLengthStringData newFundList02Err = new FixedLengthStringData(4).isAPartOf(filler13, 4);
	public FixedLengthStringData newFundList03Err = new FixedLengthStringData(4).isAPartOf(filler13, 8);
	public FixedLengthStringData newFundList04Err = new FixedLengthStringData(4).isAPartOf(filler13, 12);
	public FixedLengthStringData newFundList05Err = new FixedLengthStringData(4).isAPartOf(filler13, 16);
	public FixedLengthStringData newFundList06Err = new FixedLengthStringData(4).isAPartOf(filler13, 20);
	public FixedLengthStringData newFundList07Err = new FixedLengthStringData(4).isAPartOf(filler13, 24);
	public FixedLengthStringData newFundList08Err = new FixedLengthStringData(4).isAPartOf(filler13, 28);
	public FixedLengthStringData newFundList09Err = new FixedLengthStringData(4).isAPartOf(filler13, 32);
	public FixedLengthStringData newFundList10Err = new FixedLengthStringData(4).isAPartOf(filler13, 36);
	public FixedLengthStringData newFundList11Err = new FixedLengthStringData(4).isAPartOf(filler13, 40);
	public FixedLengthStringData newFundList12Err = new FixedLengthStringData(4).isAPartOf(filler13, 44);
	//ILIFE-8164- ENDS
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(756).isAPartOf(dataArea, 929); 
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] fndsplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] prcamtindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] statfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] stsubsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData ualprcsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] ualprcOut = FLSArrayPartOfStructure(10, 12, ualprcsOut, 0);
	public FixedLengthStringData[][] ualprcO = FLSDArrayPartOfArrayStructure(12, 1, ualprcOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(ualprcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ualprc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] ualprc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] ualprc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] ualprc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] ualprc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] ualprc06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] ualprc07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] ualprc08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] ualprc09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] ualprc10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData vrtfndsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(10, 12, vrtfndsOut, 0);
	public FixedLengthStringData[][] vrtfndO = FLSDArrayPartOfArrayStructure(12, 1, vrtfndOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(vrtfndsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] vrtfnd01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] vrtfnd02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] vrtfnd03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] vrtfnd04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] vrtfnd05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] vrtfnd06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] vrtfnd07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] vrtfnd08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] vrtfnd09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] vrtfnd10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] winfndoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] zafropt1Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] zafritemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData ubidprsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 492);
	public FixedLengthStringData[] ubidprOut = FLSArrayPartOfStructure(10, 12, ubidprsOut, 0);
	public FixedLengthStringData[][] ubidprO = FLSDArrayPartOfArrayStructure(12, 1, ubidprOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(ubidprsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ubidpr01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] ubidpr02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] ubidpr03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] ubidpr04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] ubidpr05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] ubidpr06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] ubidpr07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] ubidpr08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] ubidpr09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] ubidpr10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	//ILIFE-8164 -STARTS
	public FixedLengthStringData newFundListsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 612);
	public FixedLengthStringData[] newFundListOut = FLSArrayPartOfStructure(12, 12, newFundListsOut, 0);
	public FixedLengthStringData[][] newFundLisO = FLSDArrayPartOfArrayStructure(12, 1, newFundListOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(144).isAPartOf(newFundListsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] newFundList01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] newFundList02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] newFundList03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] newFundList04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] newFundList05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] newFundList06Out = FLSArrayPartOfStructure(12, 1, filler14, 60);
	public FixedLengthStringData[] newFundList07Out = FLSArrayPartOfStructure(12, 1, filler14, 72);
	public FixedLengthStringData[] newFundList08Out = FLSArrayPartOfStructure(12, 1, filler14, 84);
	public FixedLengthStringData[] newFundList09Out = FLSArrayPartOfStructure(12, 1, filler14, 96);
	public FixedLengthStringData[] newFundList10Out = FLSArrayPartOfStructure(12, 1, filler14, 108);
	public FixedLengthStringData[] newFundList11Out = FLSArrayPartOfStructure(12, 1, filler14, 120);
	public FixedLengthStringData[] newFundList12Out = FLSArrayPartOfStructure(12, 1, filler14, 132);
	//ILIFE-8164- ENDS
	
	
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5152screenWritten = new LongData(0);
	public LongData S5152protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5152ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(crtableOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {"44",null, "44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instpremOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fndsplOut,new String[] {"45","01","-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcamtindOut,new String[] {"61",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd01Out,new String[] {"04","01","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc01Out,new String[] {"06","01","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd02Out,new String[] {"08","01","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc02Out,new String[] {"10","01","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd03Out,new String[] {"12","01","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc03Out,new String[] {"14","01","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd04Out,new String[] {"16","01","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc04Out,new String[] {"18","01","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd05Out,new String[] {"20","01","-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc05Out,new String[] {"22","01","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd06Out,new String[] {"24","01","-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc06Out,new String[] {"26","01","-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd07Out,new String[] {"28","01","-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc07Out,new String[] {"30","01","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd08Out,new String[] {"32","01","-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc08Out,new String[] {"34","01","-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd09Out,new String[] {"36","01","-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc09Out,new String[] {"38","01","-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd10Out,new String[] {"40","01","-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ualprc10Out,new String[] {"42","01","-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zafropt1Out,new String[] {"46","49","-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zafritemOut,new String[] {"15","50","01","-48", null, null, null, null, null, null, null, null});//BRD-411
		//ILIFE-8164 STARTS
		fieldIndMap.put(newFundList01Out,new String[] {"50","51","-50", "101", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList02Out,new String[] {"54","55","-54", "102", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList03Out,new String[] {"58","59","-58", "103", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList04Out,new String[] {"62","63","-62", "104", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList05Out,new String[] {"66","67","-66", "105", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList06Out,new String[] {"70","71","-70", "106", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList07Out,new String[] {"74","75","-74", "107", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList08Out,new String[] {"78","79","-78", "108", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList09Out,new String[] {"82","83","-82", "109", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList10Out,new String[] {"86","87","-86", "110", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList11Out,new String[] {"90","91","-90", "111", null, null, null, null, null, null, null, null});
		fieldIndMap.put(newFundList12Out,new String[] {"94","95","-94", "112", null, null, null, null, null, null, null, null});
		//ILIFE-8164- ENDS
		screenFields = new BaseData[] {winfndopt, chdrnum, life, coverage, rider, lifenum, linsname, crtable, crtabdesc, zagelit, anbAtCcd, statfund, statSect, stsubsect, jlifcnum, jlinsname, instprem, virtFundSplitMethod, percOrAmntInd, unitVirtualFund01, unitAllocPercAmt01, unitVirtualFund02, unitAllocPercAmt02, unitVirtualFund03, unitAllocPercAmt03, unitVirtualFund04, unitAllocPercAmt04, unitVirtualFund05, unitAllocPercAmt05, unitVirtualFund06, unitAllocPercAmt06, unitVirtualFund07, unitAllocPercAmt07, unitVirtualFund08, unitAllocPercAmt08, unitVirtualFund09, unitAllocPercAmt09, unitVirtualFund10, unitAllocPercAmt10, zafropt1, zafritem,unitBidPrice01,unitBidPrice02,unitBidPrice03,unitBidPrice04,unitBidPrice05,unitBidPrice06,unitBidPrice07,unitBidPrice08,unitBidPrice09,unitBidPrice10,newFundList01,newFundList02,newFundList03,newFundList04,newFundList05,newFundList06,newFundList07,newFundList08,newFundList09,newFundList10,newFundList11,newFundList12};//ILIFE-8164
		screenOutFields = new BaseData[][] {winfndoptOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, linsnameOut, crtableOut, crtabdescOut, zagelitOut, anbccdOut, statfundOut, stsectOut, stsubsectOut, jlifcnumOut, jlinsnameOut, instpremOut, fndsplOut, prcamtindOut, vrtfnd01Out, ualprc01Out, vrtfnd02Out, ualprc02Out, vrtfnd03Out, ualprc03Out, vrtfnd04Out, ualprc04Out, vrtfnd05Out, ualprc05Out, vrtfnd06Out, ualprc06Out, vrtfnd07Out, ualprc07Out, vrtfnd08Out, ualprc08Out, vrtfnd09Out, ualprc09Out, vrtfnd10Out, ualprc10Out, zafropt1Out, zafritemOut,ubidpr01Out,ubidpr02Out,ubidpr03Out,ubidpr04Out,ubidpr05Out,ubidpr06Out,ubidpr07Out,ubidpr08Out,ubidpr09Out,ubidpr10Out,newFundList01Out,newFundList02Out,newFundList03Out,newFundList04Out,newFundList05Out,newFundList06Out,newFundList07Out,newFundList08Out,newFundList09Out,newFundList10Out,newFundList11Out,newFundList12Out};//ILIFE-8164
		screenErrFields = new BaseData[] {winfndoptErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, linsnameErr, crtableErr, crtabdescErr, zagelitErr, anbccdErr, statfundErr, stsectErr, stsubsectErr, jlifcnumErr, jlinsnameErr, instpremErr, fndsplErr, prcamtindErr, vrtfnd01Err, ualprc01Err, vrtfnd02Err, ualprc02Err, vrtfnd03Err, ualprc03Err, vrtfnd04Err, ualprc04Err, vrtfnd05Err, ualprc05Err, vrtfnd06Err, ualprc06Err, vrtfnd07Err, ualprc07Err, vrtfnd08Err, ualprc08Err, vrtfnd09Err, ualprc09Err, vrtfnd10Err, ualprc10Err, zafropt1Err, zafritemErr,ubidpr01Err,ubidpr02Err,ubidpr03Err,ubidpr04Err,ubidpr05Err,ubidpr06Err,ubidpr07Err,ubidpr08Err,ubidpr09Err,ubidpr10Err,newFundList01Err,newFundList02Err,newFundList03Err,newFundList04Err,newFundList05Err,newFundList06Err,newFundList07Err,newFundList08Err,newFundList09Err,newFundList10Err,newFundList11Err,newFundList12Err};//ILIFE-8164
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5152screen.class;
		protectRecord = S5152protect.class;
	}

}
