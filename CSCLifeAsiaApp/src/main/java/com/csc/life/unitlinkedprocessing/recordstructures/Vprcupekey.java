package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:07
 * Description:
 * Copybook name: VPRCUPEKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprcupekey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprcupeFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprcupeKey = new FixedLengthStringData(256).isAPartOf(vprcupeFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprcupeCompany = new FixedLengthStringData(1).isAPartOf(vprcupeKey, 0);
  	public FixedLengthStringData vprcupeUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprcupeKey, 1);
  	public FixedLengthStringData vprcupeUnitType = new FixedLengthStringData(1).isAPartOf(vprcupeKey, 5);
  	public FixedLengthStringData vprcupeValidflag = new FixedLengthStringData(1).isAPartOf(vprcupeKey, 6);
  	public PackedDecimalData vprcupeEffdate = new PackedDecimalData(8, 0).isAPartOf(vprcupeKey, 7);
  	public PackedDecimalData vprcupeJobno = new PackedDecimalData(8, 0).isAPartOf(vprcupeKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(239).isAPartOf(vprcupeKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprcupeFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprcupeFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}