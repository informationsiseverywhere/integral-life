package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5118
 * @version 1.0 generated on 30/08/09 06:34
 * @author Quipoz
 */
public class S5118ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(472);
	public FixedLengthStringData dataFields = new FixedLengthStringData(216).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,82);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,129);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,137);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,184);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,188);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,198);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,206);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 216);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 280);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(221);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(75).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData covPremStat = DD.crpstat.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData covRiskStat = DD.crrstat.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,6);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public FixedLengthStringData pstatdesc = DD.pstatdesc.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData rstatdesc = DD.rstatdesc.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,74);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 75);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crpstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData crrstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData pstatdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData rstatdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 111);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crpstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] crrstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] pstatdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] rstatdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 219);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5118screensflWritten = new LongData(0);
	public LongData S5118screenctlWritten = new LongData(0);
	public LongData S5118screenWritten = new LongData(0);
	public LongData S5118protectWritten = new LongData(0);
	public GeneralTable s5118screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5118screensfl;
	}

	public S5118ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","05","-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {rider, coverage, life, select, planSuffix, covRiskStat, rstatdesc, covPremStat, pstatdesc};
		screenSflOutFields = new BaseData[][] {riderOut, coverageOut, lifeOut, selectOut, plnsfxOut, crrstatOut, rstatdescOut, crpstatOut, pstatdescOut};
		screenSflErrFields = new BaseData[] {riderErr, coverageErr, lifeErr, selectErr, plnsfxErr, crrstatErr, rstatdescErr, crpstatErr, pstatdescErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, lifcnum, linsname, cownnum, ownername, occdate, rstate, pstate, ptdate, btdate, currcd, currds, polinc, crtable};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifcnumOut, linsnameOut, cownnumOut, ownernameOut, occdateOut, rstateOut, pstateOut, ptdateOut, btdateOut, currcdOut, currdsOut, polincOut, crtableOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifcnumErr, linsnameErr, cownnumErr, ownernameErr, occdateErr, rstateErr, pstateErr, ptdateErr, btdateErr, currcdErr, currdsErr, polincErr, crtableErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5118screen.class;
		screenSflRecord = S5118screensfl.class;
		screenCtlRecord = S5118screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5118protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5118screenctl.lrec.pageSubfile);
	}
}
