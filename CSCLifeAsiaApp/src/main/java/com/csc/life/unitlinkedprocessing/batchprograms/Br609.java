/*
 * File: Br609.java
 * Date: 29 August 2009 22:23:14
 * Author: Quipoz Limited
 * 
 * Class transformed from BR609.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UstmDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(C) Copyright CSC Corporation Limited 2005.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *             UNIT STATEMENTS - EXTRACT NEW TRIGGER RECORDS
 *             ---------------------------------------------
 *
 * Overview.
 * --------
 *
 *     This  program  selects  the Trigger records that have not yet
 *     been  processed  and  that conform to the run parameters, ie.
 *     date  and  print/reprint  criteria, and changes them from the
 *     USTM access path to the USTJ access path with the addition of
 *     the current Job Number.
 *
 * Processing.
 * -----------
 *
 *     Perform  a  BEGNH on USTM using spaces and zeroes in the key.
 *     Process all the USTM records until end of file.
 *
 *     Extract only those USTM records that match the run parameters
 *     on  Company and Run Type and have an effective date less than
 *     or equal to the run trigger date.
 *
 *     Duplicate  keys  are  possible. If the key of the record just
 *     read  matches the previous one on Company and Contract Number
 *     then set its Job Number to all 9's and rewrite it.
 *
 *     Read  the  Contract Header record for the selected USTM using
 *     CHDRULS.  Set up the Client Prefix, Company and Number on the
 *     USTM  record from either the Contract Header Despatch details
 *     or the Owner details if the despatch details are blank. Place
 *     the Statement Effective date on the record and the Job Number
 *     and rewrite it.
 *
 *
 *#
 *    CONTROL TOTALS
 *    ==============
 *    01   NEW TRIGGER RECORDS READ
 *    02   NEW TRIGGER RECORDS SELECTED
 *    03   DUPLICATE NEW STMT TRIGGER RECORDS IGNORED
 *    04   TRIGGER RECORDS DROPPED.
 *
 *
 ***********************************************************************
 * </pre>
 */
public class Br609 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR609");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	/* ERRORS */
	private String e228 = "E228";
	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;

	private ZonedDecimalData wsaaInCount = new ZonedDecimalData(9, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaStmtType = new FixedLengthStringData(1);
	private Validator wsaaNewStmt = new Validator(wsaaStmtType, "P");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	int partitionId;
	private Iterator<UstmData> ustmIter; 
	private UstmData ustmData = null; 
	private String wsaaThisKey;
	private String wsaaLastKey; 
	List<UstmData> updateList = null;
	List<UstmData> duplicateList = null;
	private UstmDAO ustmDAO = getApplicationContext().getBean("ustmDAO", UstmDAO.class);
	private String company;
	private int rowCount;
	private int ctrCT01; 
	private int ctrCT02;
	private int ctrCT03;
	private int minRecord = 1;

	//Susmitha - Performance Improvement End

	public Br609() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void restart0900() {
		/*RESTART*/
		/*EXIT*/
	}

	protected void initialise1000()	{
		/*INITIALISE*/
		wsspEdterror.set(Varcom.oK);
		wsaaInCount.set(0);
		// Batch Improvement
		wsaaThisKey = "";
		wsaaLastKey = "";
		wsaaStmtType.set(bprdIO.getSystemParam01());
		company = bprdIO.getCompany().toString();
		rowCount = bprdIO.cyclesPerCommit.toInt();
		if(rowCount <= 0)
			rowCount = 1000; //default size set as 1000  to ensure minimal speed
		partitionId = 0;
		ctrCT01 = 0;
		ctrCT02 = 0;
		ctrCT03 = 0;

	}

	protected void readFile2000(){
		try {

			/*Extract only those USTM records that match the run parameters on Company and stmt type Type and have an effective date less than
		or equal to the run trigger date.*/
			extractUstmDetails(wsaaStmtType.toString(),company, bsscIO.getEffectiveDate().toInt(), minRecord,minRecord+rowCount);
		}
		catch (GOTOException e){

		}
	}

	private void extractUstmDetails(String stmtType, String company, int effectiveDate, int minRecord, int maxRecord) {
		Map<String,List<UstmData>> ustmMap = ustmDAO.getUstmDetails(stmtType,company, effectiveDate, minRecord,maxRecord);

		if(ustmMap == null || ustmMap.isEmpty()){
			wsspEdterror.set(Varcom.endp);
			if (isEQ(wsaaInCount,0)) {
				conlogrec.params.set(SPACES);
				conlogrec.error.set(e228);
				callConlog003();
			}
			return;
		}else{
			updateList = new ArrayList<UstmData>(); 
			duplicateList = new ArrayList<UstmData>(); 
			for(Map.Entry<String, List<UstmData>> entry : ustmMap.entrySet()) {
				String key = entry.getKey();
//				String key1 = key.substring(0, 9); // IBPLIFE-2145
				List<UstmData> value = entry.getValue();
				wsaaThisKey = key; // IBPLIFE-2145
				//All except the first can be treated as duplicate and updated as duplicate
				ustmIter = value.iterator();
				while(ustmIter.hasNext()){
					//Total rec read
					ustmData = ustmIter.next();
					ctrCT01++;
					if(isNE(wsaaThisKey ,wsaaLastKey)){
						fillUstmData();
						updateList.add(ustmData);
						wsaaInCount.add(1);
						ctrCT02++;
						wsaaLastKey = key; // IBPLIFE-2145
					}else{
						//total duplicate records ignored
						ctrCT03++;
						ustmData.setJobnoStmt(99999999);
						ustmData.setStrpdate(bsscIO.getEffectiveDate().toLong());
						duplicateList.add(ustmData);
					}
				}
			}
		}
	}

	protected void edit2500() {
		wsspEdterror.set(Varcom.oK);
	}

	protected void update3000() {
		updateUstmData();
		minRecord += rowCount;
	}

	protected void commit3500() {
		commitControlTotals();
	}

	protected void rollback3600() {

	}

	protected void updateUstmData() {
		ustmDAO.updateUstmRecord(duplicateList);
		ustmDAO.updateUstmRecord(updateList);

	}

	protected void fillUstmData(){
		if(ustmData != null){
			if (wsaaNewStmt.isTrue()) {
				ustmData.setStrpdate(bsscIO.getEffectiveDate().toLong());
			}
			ustmData.setJobnoStmt(bsscIO.getScheduleNumber().toLong());
			if (isNE(ustmData.getDescPfx(),SPACES)
					&& isNE(ustmData.getDescCoy(),SPACES)
					&& isNE(ustmData.getDescNum(),SPACES)) {
				ustmData.setClntpfx(ustmData.getDescPfx());
				ustmData.setClntcoy(ustmData.getDescCoy());
				ustmData.setClntnum(ustmData.getDescNum());
			}
			else {
				ustmData.setClntpfx(ustmData.getOwnerPfx());
				ustmData.setClntcoy(ustmData.getOwnerCoy());
				ustmData.setClntnum(ustmData.getOwnerNum());
			}
		}else
			return;
		//ustmpf.setCntcurr(chdrulsIO.getCntcurr());

	}

	protected void commitControlTotals(){
		//ct01
		contotrec.totno.set(ct01);
		contotrec.totval.set(ctrCT01);
		callProgram(Contot.class, contotrec.contotRec);		
		ctrCT01 = 0;

		//ct02
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT02 = 0;

		//ct03
		contotrec.totno.set(ct03);
		contotrec.totval.set(ctrCT03);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT03 = 0;
	}

	protected void close4000()	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(Varcom.oK);
		if (updateList != null && !updateList.isEmpty()){
		updateList.clear();
		updateList = null;
		}
		if (duplicateList != null && !duplicateList.isEmpty()){
		duplicateList.clear();
		duplicateList = null;
		}
		/*EXIT*/
	}
}
