package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:49
 * Description:
 * Copybook name: T5546REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5546rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5546Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData efdcode = new FixedLengthStringData(2).isAPartOf(t5546Rec, 0);
  	public FixedLengthStringData rbind = new FixedLengthStringData(1).isAPartOf(t5546Rec, 2);
  	public FixedLengthStringData filler = new FixedLengthStringData(497).isAPartOf(t5546Rec, 3, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5546Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5546Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}