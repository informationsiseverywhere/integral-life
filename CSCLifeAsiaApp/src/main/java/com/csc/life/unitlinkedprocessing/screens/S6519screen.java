package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6519screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6519ScreenVars sv = (S6519ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6519screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6519ScreenVars screenVars = (S6519ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.jowner.setClassString("");
		screenVars.jownername.setClassString("");
		screenVars.payer.setClassString("");
		screenVars.payername.setClassString("");
		screenVars.servagnt.setClassString("");
		screenVars.servagnam.setClassString("");
		screenVars.servbr.setClassString("");
		screenVars.brchname.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.lststmdteDisp.setClassString("");
		screenVars.ustmno.setClassString("");
		screenVars.stmtdateDisp.setClassString("");
		screenVars.stmtlevel.setClassString("");
	}

/**
 * Clear all the variables in S6519screen
 */
	public static void clear(VarModel pv) {
		S6519ScreenVars screenVars = (S6519ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.jowner.clear();
		screenVars.jownername.clear();
		screenVars.payer.clear();
		screenVars.payername.clear();
		screenVars.servagnt.clear();
		screenVars.servagnam.clear();
		screenVars.servbr.clear();
		screenVars.brchname.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.numpols.clear();
		screenVars.lststmdteDisp.clear();
		screenVars.lststmdte.clear();
		screenVars.ustmno.clear();
		screenVars.stmtdateDisp.clear();
		screenVars.stmtdate.clear();
		screenVars.stmtlevel.clear();
	}
}
