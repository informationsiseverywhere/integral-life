package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sd5fqscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {10, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {13, 22, 3, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5fqScreenVars sv = (Sd5fqScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.Sd5fqscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.Sd5fqscreensfl, 
			sv.Sd5fqscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sd5fqScreenVars sv = (Sd5fqScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.Sd5fqscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sd5fqScreenVars sv = (Sd5fqScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.Sd5fqscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sd5fqscreensflWritten.gt(0))
		{
			sv.Sd5fqscreensfl.setCurrentIndex(0);
			sv.Sd5fqscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sd5fqScreenVars sv = (Sd5fqScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.Sd5fqscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5fqScreenVars screenVars = (Sd5fqScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.fndtyp.setFieldName("fndtyp");
				screenVars.currcy.setFieldName("currcy");
				screenVars.nofDunits.setFieldName("nofDunits");
				screenVars.estval.setFieldName("estval");
				screenVars.pcntamt.setFieldName("pcntamt");
				
				screenVars.select.setFieldName("select");
				screenVars.percentAmountInd.setFieldName("percentAmountInd");
				screenVars.fundPool.setFieldName("fundPool");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.fndtyp.set(dm.getField("fndtyp"));
			screenVars.currcy.set(dm.getField("currcy"));
			screenVars.nofDunits.set(dm.getField("nofDunits"));
			screenVars.estval.set(dm.getField("estval"));
			screenVars.pcntamt.set(dm.getField("pcntamt"));
			
			screenVars.select.set(dm.getField("select"));
			screenVars.percentAmountInd.set(dm.getField("percentAmountInd"));
			screenVars.fundPool.set(dm.getField("fundPool"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5fqScreenVars screenVars = (Sd5fqScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.fndtyp.setFieldName("fndtyp");
				screenVars.currcy.setFieldName("currcy");
				screenVars.nofDunits.setFieldName("nofDunits");
				screenVars.estval.setFieldName("estval");
				screenVars.pcntamt.setFieldName("pcntamt");
				
				screenVars.select.setFieldName("select");
				screenVars.percentAmountInd.setFieldName("percentAmountInd");
				screenVars.fundPool.setFieldName("fundPool");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("fndtyp").set(screenVars.fndtyp);
			dm.getField("currcy").set(screenVars.currcy);
			dm.getField("nofDunits").set(screenVars.nofDunits);
			dm.getField("estval").set(screenVars.estval);
			dm.getField("pcntamt").set(screenVars.pcntamt);			
			dm.getField("select").set(screenVars.select);
			dm.getField("percentAmountInd").set(screenVars.percentAmountInd);
			dm.getField("fundPool").set(screenVars.fundPool);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sd5fqscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sd5fqScreenVars screenVars = (Sd5fqScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.fndtyp.clearFormatting();
		screenVars.currcy.clearFormatting();
		screenVars.nofDunits.clearFormatting();
		screenVars.estval.clearFormatting();
		screenVars.pcntamt.clearFormatting();
		
		screenVars.select.clearFormatting();
		screenVars.percentAmountInd.clearFormatting();
		screenVars.fundPool.clearFormatting();
		
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sd5fqScreenVars screenVars = (Sd5fqScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.fndtyp.setClassString("");
		screenVars.currcy.setClassString("");
		screenVars.nofDunits.setClassString("");
		screenVars.estval.setClassString("");
		screenVars.pcntamt.setClassString("");
		
		screenVars.select.setClassString("");
		screenVars.percentAmountInd.setClassString("");
		screenVars.fundPool.setClassString("");
	}

/**
 * Clear all the variables in S5145screensfl
 */
	public static void clear(VarModel pv) {
		Sd5fqScreenVars screenVars = (Sd5fqScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.fndtyp.clear();
		screenVars.currcy.clear();
		screenVars.nofDunits.clear();
		screenVars.estval.clear();
		screenVars.pcntamt.clear();		
		screenVars.select.clear();
		screenVars.percentAmountInd.clear();
		screenVars.fundPool.clear();
		
	}
}
