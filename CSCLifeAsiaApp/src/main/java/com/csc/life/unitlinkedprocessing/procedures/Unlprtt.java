/*
 * File: Unlprtt.java
 * Date: 30 August 2009 2:51:35
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLPRTT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrtrgTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.PtsdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtsdpenTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtsdsrcTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtsdtrgTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtshclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrntrgTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrpwTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  UNLPRTT - Part Surrender Trigger Module.
*  ----------------------------------------
*
*  This program is an item entry on T6598, the surrender  claim
*  subroutine  method  table.  This  method is used in order to
*  update the relevant sub-accounts.  The  trigger  module  and
*  key were passed with other details with the UTRN record.
*
*  PROCESSING.
*  -----------
*
*  Read  the  Surrender Claim detail records (PTSDCLM) for this
*  trigger key and sum all the estimated amounts  and  sum  all
*  the  actual amounts (each as a single currency). Sum the Fee
*  amounts.
*
*  If this  is  a  surrender  detail  record  for  the  current
*  component,  also  sum  the  estimated  amounts  and  sum the
*  actual amounts.
*
*  If the amounts are  not  zero,  then  call  LIFACMV  ("cash"
*  posting  subroutine)  to  post  to  the correct surrendering
*  accounts  ("01  -Pending  Surrender"  sub-account).      The
*  posting  required is defined in the appropriate line no.  on
*  the T5645 table entry.  Set up and pass the linkage area  as
*  follows:
*
*             Function               - PSTW
*             Batch key              - AT linkage
*             Document number        - contract number
*             Transaction number     - transaction no.
*             Journal sequence no.   - 0
*             Sub-account code       - from applicable T5645
*                                      entry
*             Sub-account type       - ditto
*             Sub-account GL map     - ditto
*             Sub-account GL sign    - ditto
*             S/acct control total   - ditto
*             Cmpy code (sub ledger) - batch company
*             Cmpy code (GL)         - batch company
*             Subsidiary ledger      - contract number
*             Original currency code - currency payable in
*             Original currency amt  - claim amount
*             Accounting curr code   - blank (handled by
*                                      subroutine)
*             Accounting curr amount - zero (handled by
*                                      subroutine)
*             Exchange rate          - zero (handled by
*                                      subroutine)
*             Trans reference        - contract transaction
*                                      number
*             Trans description      - from transaction code
*                                      description
*             Posting month and year - defaulted
*             Effective date         - Surrender Claim
*                                      effective-date
*             Reconciliation amount  - zero
*             Reconciliation date    - Max date
*             Transaction Id         - AT linkage
*             Substitution code 1    - contract type
*
*  If  the  total  estimated  amount  for  the Plan is equal to
*  zero,  then  access  the  balance  for  the  01  sub-account
*  (defined  above)  and  post  (as above) to the "01 - Pending
*  Surrender" and "03 - Surrender Actual" sub-accounts.
*
*  Post to the following accounts:
*
*       1) Credit Pending Surrender  (01)
*       2) Debit Pending Surrender   (01)
*       3) Credit Fees               (02)
*       4) Credit Surrender Proceeds (03)
*
*****************************************************************
* </pre>
*/
public class Unlprtt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLPRTT";
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String h388 = "H388";
	private static final String e662 = "E662";
		/* TABLES */
	private static final String t1693 = "T1693";
	private static final String t5645 = "T5645";
	private static final String tr384 = "TR384";
	private static final String t5687 = "T5687";
	private static final String t5542 = "T5542";
	private static final String t5688 = "T5688";
	private static final String t7508 = "T7508";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";

	private FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq, "Y");
	protected PackedDecimalData wsaaActualVal = new PackedDecimalData(18, 3).init(0);
	protected PackedDecimalData wsaaNetVal = new PackedDecimalData(14, 3).init(0);
		/* WSAA-IN-CASE-OF-ERROR */
	private FixedLengthStringData wsaaErrorStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaErrorFormat = new FixedLengthStringData(16);
	protected PackedDecimalData wsaaTotFee = new PackedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	protected PackedDecimalData wsaaTotSurChrg = new PackedDecimalData(13, 2).init(0);
	private PackedDecimalData wsaaCmpSurChrg = new PackedDecimalData(16, 5).init(0);
	private PackedDecimalData wsaaCmpCsv = new PackedDecimalData(13, 2).init(0);
	private PackedDecimalData wsaaCntCsv = new PackedDecimalData(13, 2).init(0);
	protected PackedDecimalData wsaaTotalTax = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaT5542Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5542Meth = new FixedLengthStringData(4).isAPartOf(wsaaT5542Key, 0);
	private FixedLengthStringData wsaaT5542Curr = new FixedLengthStringData(3).isAPartOf(wsaaT5542Key, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();


	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsaaPlanSuff = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanSuff, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaFiller = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 0).setUnsigned();
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(16);
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 12);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaOthLife = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 1);
	private FixedLengthStringData wsaaOthCoverage = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 3);
	private FixedLengthStringData wsaaOthRider = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 5);
	private PackedDecimalData wsaaOthPlnsfx = new PackedDecimalData(4, 0).isAPartOf(wsaaOtherKeys, 7).setUnsigned();
	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(4).isAPartOf(wsaaOtherKeys, 10);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private HitrtrgTableDAM hitrtrgIO = new HitrtrgTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtsdclmTableDAM ptsdclmIO = new PtsdclmTableDAM();
	private PtsdpenTableDAM ptsdpenIO = new PtsdpenTableDAM();
	private PtsdsrcTableDAM ptsdsrcIO = new PtsdsrcTableDAM();
	private PtsdtrgTableDAM ptsdtrgIO = new PtsdtrgTableDAM();
	private PtshclmTableDAM ptshclmIO = new PtshclmTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	protected UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrntrgTableDAM utrntrgIO = new UtrntrgTableDAM();
	private ZrpwTableDAM zrpwIO = new ZrpwTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Syserrrec syserrrec = new Syserrrec();
	protected Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private T5645rec t5645rec = new T5645rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T5687rec t5687rec = new T5687rec();
	private T5542rec t5542rec = new T5542rec();
	protected T5688rec t5688rec = new T5688rec();
	private T7508rec t7508rec = new T7508rec();
	protected Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Udtrigrec udtrigrec = new Udtrigrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	protected WsaaGeneralFieldsInner wsaaGeneralFieldsInner = new WsaaGeneralFieldsInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		rewrtPtsdpen3740, 
		exit3700, 
		rewrtPtsdpen3840, 
		exit3800, 
		callPtsdsrc3903, 
		exit3909, 
		callPtsdclm3922, 
		nextPtsdclm3928, 
		exit3929, 
		callPtsdclm3932, 
		nextPtsdclm3938, 
		exit3939, 
		b320Call, 
		b380Next, 
		b390Exit, 
		b490Exit, 
		b590Exit
	}

	public Unlprtt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
			para010();
			exit090();
		}

protected void para010()
	{
		initialize1000();
		processTransactionLoop12000();
		wsaaGeneralFieldsInner.wsaaEndTrans.set(SPACES);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			a300CheckFeedbackIndHitr();
		}
		else {
			checkFeedbackIndUtrn2200();
		}
		writeLetc3200a();
		a100CreateZrpw();
		/* There are still outstanding funds to be dealt, so no need to*/
		/* process*/
		if (wsaaGeneralFieldsInner.noNeedToPost.isTrue()) {
			return ;
		}
		startPtsdclm1400();
		wsaaGeneralFieldsInner.wsaaEndTrans.set(SPACES);
		while ( !(wsaaGeneralFieldsInner.endOfTransaction.isTrue()
		|| wsaaGeneralFieldsInner.noNeedToPost.isTrue())) {
			processTransactionLoop22300();
		}
		
		/* There are still outstanding funds to be dealt, so no need to*/
		/* process*/
		if (wsaaGeneralFieldsInner.noNeedToPost.isTrue()) {
			return ;
		}
		openBatch3400();
		if (isGT(wsaaTotFee,wsaaActualVal)) {
			updatePenaltyRec3300();
		}
		/*  Post the Tax on surrender fee                                  */
		if (isNE(tr52drec.txcode, SPACES)) {
			b300ProcFeeTax();
		}
		/*                                                         <V74L01>*/
		calcSurChrg3900();
		compute(wsaaNetVal, 3).set(sub(sub(sub(wsaaActualVal, wsaaTotSurChrg), wsaaTotalTax), wsaaTotFee));
		/* Post to Pending Withdrawal Account*/
		postGross3000();
		postNet3100();
		postPenalty3200();
		if (isNE(t5688rec.comlvlacc,"Y")) {
			postCntSurChrg3950();
		}
		wsaaNetVal.set(ZERO);
		wsaaActualVal.set(ZERO);
		wsaaTotFee.set(ZERO);
		/* PERFORM A100-CREATE-ZRPW.                            <LA1646>*/
		closeBatch3500();
		dryProcessing8000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize1000()
	{
		go1010();
	}

protected void go1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTime.set(getCobolTime());
		wsaaGeneralFieldsInner.wsaaEndTrans.set(SPACES);
		wsaaGeneralFieldsInner.wsaaNotReady.set(SPACES);
		wsaaTotalTax.set(ZERO);
		readPtshclm1200();
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set("0");
		wsaaItemkey.itemItemtabl.set(t1693);
		wsaaItemkey.itemItemitem.set(ptshclmIO.getChdrcoy());
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set("E");
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		/*PERFORM 1900-READ-TAB-T6657.*/
		readTabT56451800();
		readTabT56881895();
		wsaaGeneralFieldsInner.wsaaStoreFee.set(ZERO);
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(udtrigrec.chdrcoy);
		chdrenqIO.setChdrnum(udtrigrec.chdrnum);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError9000();
		}
		readCovrmja1100();
		/* Retrive the Tax information from TR52D                          */
		b100TaxInfo();
	}

protected void readCovrmja1100()
	{
		start1110();
	}

protected void start1110()
	{
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(udtrigrec.chdrcoy);
		covrmjaIO.setChdrnum(udtrigrec.chdrnum);
		covrmjaIO.setLife(udtrigrec.life);
		covrmjaIO.setCoverage(udtrigrec.coverage);
		covrmjaIO.setRider(udtrigrec.rider);
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
	}

protected void readPtshclm1200()
	{
		start1200();
	}

protected void start1200()
	{
		/*  Set up Header rec. key.*/
		ptshclmIO.setParams(SPACES);
		ptshclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ptshclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptshclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		ptshclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		ptshclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		ptshclmIO.setTranno(udtrigrec.tk2Tranno);
		ptshclmIO.setPlanSuffix(ZERO);
		SmartFileCode.execute(appVars, ptshclmIO);
		if (isNE(ptshclmIO.getStatuz(),varcom.oK)
		&& isNE(ptshclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptshclmIO.getParams());
			fatalError9000();
		}
		if (isNE(udtrigrec.tk2Chdrcoy, ptshclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum,ptshclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno,ptshclmIO.getTranno())) {
			syserrrec.params.set(ptshclmIO.getParams());
			fatalError9000();
		}
	}

protected void startPtsdclm1400()
	{
		start1400();
	}

protected void start1400()
	{
		ptsdclmIO.setParams(SPACES);
		ptsdclmIO.setFunction(varcom.begn);
		
		//performance improvement --  Niharika Modi 
		ptshclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptshclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		
		ptsdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		ptsdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		ptsdclmIO.setTranno(udtrigrec.tk2Tranno);
		ptsdclmIO.setPlanSuffix(ZERO);
		ptsdclmIO.setLife(ZERO);
		ptsdclmIO.setCoverage(ZERO);
		ptsdclmIO.setRider(ZERO);
		ptsdclmIO.setFormat(formatsInner.ptsdclmrec);
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
		&& isNE(ptsdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdclmIO.getParams());
			fatalError9000();
		}
		/*  This next check appears to be completely unnecessary and will  */
		/*  cause a MRNF in 2975-CHECK-PLAN-SUFFIX2 as we have read the    */
		/*  first PTSD coverage.                                           */
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = PTSDCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = PTSDCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = PTSDCLM-TRANNO             */
		/* OR WSAA-TRIGGER-SUFFIX      NOT = PTSDCLM-PLAN-SUFFIX        */
		/* OR WSAA-TRIGGER-COVERAGE    NOT = PTSDCLM-COVERAGE           */
		/* OR WSAA-TRIGGER-RIDER       NOT = PTSDCLM-RIDER              */
		/* OR PTSDCLM-STATUZ           = ENDP                           */
		/*    PERFORM 2975-CHECK-PLAN-SUFFIX2.                          */
		/*  Check that the correct record has been found.               */
		/* IF  WSAA-TRIGGER-CHDRCOY    NOT = PTSDCLM-CHDRCOY    <D60401>*/
		/* OR  WSAA-TRIGGER-CHDRNUM    NOT = PTSDCLM-CHDRNUM    <D60401>*/
		/* OR  WSAA-TRIGGER-TRANNO     NOT = PTSDCLM-TRANNO     <D60401>*/
		if (isNE(udtrigrec.tk2Chdrcoy, ptsdclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum,ptsdclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno,ptsdclmIO.getTranno())) {
			syserrrec.params.set(ptsdclmIO.getParams());
			syserrrec.statuz.set(varcom.mrnf);
			fatalError9000();
		}
		/*  Store the details of the first record in WS.                */
		wsaaGeneralFieldsInner.wsaaPtsdChdrnum.set(ptsdclmIO.getChdrnum());
		wsaaGeneralFieldsInner.wsaaCrtable.set(ptsdclmIO.getCrtable());
		wsaaGeneralFieldsInner.wsaaLife.set(ptsdclmIO.getLife());
		wsaaGeneralFieldsInner.wsaaCoverage.set(ptsdclmIO.getCoverage());
		wsaaGeneralFieldsInner.wsaaRider.set(ptsdclmIO.getRider());
		wsaaGeneralFieldsInner.wsaa1stTime.set("Y");
	}

	/**
	* <pre>
	*1500-READ-T5540   SECTION.                                       
	*1500-GO.                                                         
	*****MOVE SPACES                 TO ITDM-DATA-KEY.                
	*****MOVE PTSHCLM-CHDRCOY        TO ITDM-ITEMCOY.                 
	*****MOVE T5540                  TO ITDM-ITEMTABL.                
	*****MOVE PTSDCLM-CRTABLE        TO ITDM-ITEMITEM.                
	*****IF  PTSHCLM-EFFDATE        =  ZEROS                          
	*****    MOVE 999999            TO ITDM-ITMFRM                    
	*****ELSE                                                         
	*****    MOVE PTSHCLM-EFFDATE   TO ITDM-ITMFRM.                   
	*****MOVE 'BEGN'                 TO ITDM-FUNCTION.                
	*****CALL 'ITDMIO' USING         ITDM-PARAMS.                     
	*****IF ITDM-STATUZ              NOT = '****' AND                 
	*****                            NOT = 'ENDP'                     
	*****   MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	*****    MOVE 'ITDMREC'          TO WSAA-ERROR-FORMAT             
	*****   PERFORM 9000-FATAL-ERROR.                                 
	*****IF ITDM-ITEMCOY             NOT = PTSHCLM-CHDRCOY            
	***** OR ITDM-ITEMTABL           NOT = T5540                      
	***** OR ITDM-ITEMITEM           NOT = PTSDCLM-CRTABLE            
	***** OR ITDM-STATUZ             = 'ENDP'                         
	*****   MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	*****    MOVE 'ITDMREC'          TO WSAA-ERROR-FORMAT             
	*****   PERFORM 9000-FATAL-ERROR                                  
	*****ELSE                                                         
	*****   MOVE ITDM-GENAREA        TO T5540-T5540-REC.              
	*1500-EXIT.                                                       
	*****EXIT.                                                        
	* </pre>
	*/
protected void readT56871530()
	{
			go1530();
		}

protected void go1530()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(ptshclmIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		/* MOVE PTSDCLM-CRTABLE        TO ITDM-ITEMITEM.           <011>*/
		itdmIO.setItemitem(ptsdtrgIO.getCrtable());
		if (isEQ(ptshclmIO.getEffdate(),ZERO)) {
			itdmIO.setItmfrm(999999);
		}
		else {
			itdmIO.setItmfrm(ptshclmIO.getEffdate());
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),ptshclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),ptsdtrgIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5687rec.partsurr,SPACES)) {
			syserrrec.params.set(e662);
			return ;
		}
	}

protected void readT55421550()
	{
		go1550();
	}

protected void go1550()
	{
		/*MOVE T5540-WDMETH           TO WSAA-T5542-METH.              */
		wsaaT5542Meth.set(t5687rec.partsurr);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(ptshclmIO.getChdrcoy());
		wsaaT5542Curr.set(ptshclmIO.getCurrcd());
		itdmIO.setItemtabl(t5542);
		itdmIO.setItemitem(wsaaT5542Key);
		if (isEQ(ptshclmIO.getEffdate(),ZERO)) {
			itdmIO.setItmfrm(999999);
		}
		else {
			itdmIO.setItmfrm(ptshclmIO.getEffdate());
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),ptshclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5542)
		|| isNE(itdmIO.getItemitem(),wsaaT5542Key)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			/*        MOVE 'ITDMREC'          TO WSAA-ERROR-FORMAT*/
			fatalError9000();
		}
		else {
			t5542rec.t5542Rec.set(itdmIO.getGenarea());
		}
	}

protected void getFreqT55421600()
	{
		/*GO*/
		/* Check frequency with those on T5542.                            */
		/*                                                      <A05691>*/
		/* MOVE SPACES                 TO CHDRPTS-PARAMS.       <A05691>*/
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO CHDRPTS-CHDRCOY.      <A05691>*/
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO CHDRPTS-CHDRNUM.      <A05691>*/
		/* MOVE READR                  TO CHDRPTS-FUNCTION.     <A05691>*/
		/*                                                      <A05691>*/
		/* CALL 'CHDRPTSIO'            USING CHDRPTS-PARAMS.    <A05691>*/
		/*                                                      <A05691>*/
		/* IF CHDRPTS-STATUZ           NOT = O-K AND            <A05691>*/
		/*                             NOT = MRNF               <A05691>*/
		/* MOVE CHDRPTS-PARAMS         TO SYSR-PARAMS           <A05691>*/
		/* PERFORM 9000-FATAL-ERROR.                            <A05691>*/
		/* If this is a single premium component then move '00' to the     */
		/* WSAA-BILLFREQ, otherwise read the PAYR file to obtain the       */
		/* Contract Billing frequency.                                     */
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			wsaaBillfreq.set("00");
		}
		else {
			payrIO.setDataArea(SPACES);
			/*     MOVE WSAA-TRIGGER-CHDRCOY TO PAYR-CHDRCOY        <D60401>*/
			/*     MOVE WSAA-TRIGGER-CHDRNUM TO PAYR-CHDRNUM        <D60401>*/
			payrIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
			payrIO.setChdrnum(udtrigrec.tk2Chdrnum);
			payrIO.setPayrseqno(1);
			payrIO.setValidflag("1");
			payrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, payrIO);
			if ((isNE(payrIO.getStatuz(),varcom.oK))) {
				syserrrec.params.set(payrIO.getParams());
				syserrrec.statuz.set(payrIO.getStatuz());
				fatalError9000();
			}
			wsaaBillfreq.set(payrIO.getBillfreq());
		}
		wsaaSub.set(ZERO);
		/* PERFORM VARYING WSAA-SUB    FROM 1 BY 1 UNTIL        <A06625>*/
		/*                             WSAA-SUB > 9   OR        <A06625>*/
		/* CHDRPTS-BILLFREQ            = T5542-BILLFREQ(WSAA-SUB)<A05691*/
		/* WSAA-BILLFREQ               = T5542-BILLFREQ(WSAA-SUB)<A06625*/
		/* IF WSAA-SUB                 > 9                      <A06625>*/
		/*   MOVE  H388                TO SYSR-STATUZ           <A06625>*/
		/*   GO                        TO 1690-EXIT             <A06625>*/
		/* END-IF                                               <A06625>*/
		/* END-PERFORM.                                         <A06625>*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			if (isEQ(wsaaBillfreq,t5542rec.billfreq[wsaaSub.toInt()])) {
				return ;
			}
		}
		syserrrec.statuz.set(h388);
		fatalError9000();
	}

protected void readTabT56451800()
	{
		read1810();
	}

protected void read1810()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(ptshclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(ptshclmIO.getChdrcoy());
		descIO.setLanguage("E");
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),"****")
		&& isNE(descIO.getStatuz(),"MRNF")) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
	}

protected void readTabT56881895()
	{
			read1897();
		}

protected void read1897()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(ptshclmIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(ptshclmIO.getCnttype());
		itdmIO.setItmfrm(ptshclmIO.getEffdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),ptshclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),ptshclmIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	*1900-READ-TAB-T6657             SECTION.                         
	*1910-READ.
	**** MOVE WSAA-TRIGGER-CHDRCOY   TO ITDM-ITEMCOY.                 
	**** MOVE UDTG-TK2-CHDRCOY       TO ITDM-ITEMCOY.                 
	**** MOVE T6657                  TO ITDM-ITEMTABL.                
	**** MOVE PTSHCLM-CURRCD         TO ITDM-ITEMITEM.                
	**** IF  PTSHCLM-EFFDATE        =  ZEROS                          
	****     MOVE 999999            TO ITDM-ITMFRM                    
	**** ELSE                                                         
	****     MOVE PTSHCLM-EFFDATE        TO ITDM-ITMFRM.              
	**** MOVE 'BEGN'                 TO ITDM-FUNCTION.                
	**** CALL 'ITDMIO' USING         ITDM-PARAMS.                     
	**** IF ITDM-STATUZ              NOT = '****' AND                 
	****    ITDM-STATUZ              NOT = 'ENDP'                     
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****     MOVE 'ITDMREC'           TO WSAA-ERROR-FORMAT            
	****    PERFORM 9000-FATAL-ERROR.                                 
	**** IF  PTSHCLM-CURRCD          NOT = ITDM-ITEMITEM              
	****  OR PTSHCLM-CHDRCOY         NOT = ITDM-ITEMCOY               
	****  OR ITDM-ITEMTABL           NOT = T6657                      
	****  OR ITDM-STATUZ             = 'ENDP'                         
	****   MOVE 'ITDMREC'           TO WSAA-ERROR-FORMAT              
	****    MOVE MRNF                TO SYSR-STATUZ                   
	****    PERFORM 9000-FATAL-ERROR                                  
	**** ELSE                                                         
	****    MOVE ITDM-GENAREA        TO T6657-T6657-REC.              
	*1990-EXIT.                                                       
	**** EXIT .                                                       
	* </pre>
	*/
protected void processTransactionLoop12000()
	{
			para2010();
		}

protected void para2010()
	{
		readUtrn2700();
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			a200ReadHitr();
			return ;
		}
		wsaaBatckey.batcBatccoy.set(utrnIO.getBatccoy());
		wsaaBatckey.batcBatcbrn.set(utrnIO.getBatcbrn());
		wsaaBatckey.batcBatcactyr.set(utrnIO.getBatcactyr());
		wsaaBatckey.batcBatcactmn.set(utrnIO.getBatcactmn());
		wsaaBatckey.batcBatctrcde.set(utrnIO.getBatctrcde());
		wsaaBatckey.batcBatcbatch.set(utrnIO.getBatcbatch());
		/*  Start detail rec.*/
		ptsdtrgIO.setParams(SPACES);
		/* MOVE BEGNH                  TO PTSDTRG-FUNCTION.             */
		ptsdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		ptsdtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		ptsdtrgIO.setChdrcoy(utrnIO.getChdrcoy());
		ptsdtrgIO.setChdrnum(utrnIO.getChdrnum());
		ptsdtrgIO.setTranno(utrnIO.getTranno());
		ptsdtrgIO.setPlanSuffix(utrnIO.getPlanSuffix());
		ptsdtrgIO.setLife(utrnIO.getLife());
		ptsdtrgIO.setCoverage(utrnIO.getCoverage());
		ptsdtrgIO.setRider(utrnIO.getRider());
		ptsdtrgIO.setFieldType(utrnIO.getUnitType());
		ptsdtrgIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		ptsdtrgIO.setFormat(formatsInner.ptsdtrgrec);
		SmartFileCode.execute(appVars, ptsdtrgIO);
		if (isNE(ptsdtrgIO.getStatuz(),varcom.oK)
		&& isNE(ptsdtrgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdtrgIO.getParams());
			fatalError9000();
		}
		if (isNE(utrnIO.getChdrcoy(),ptsdtrgIO.getChdrcoy())
		|| isNE(utrnIO.getChdrnum(),ptsdtrgIO.getChdrnum())
		|| isNE(utrnIO.getTranno(),ptsdtrgIO.getTranno())
		|| isNE(utrnIO.getPlanSuffix(),ptsdtrgIO.getPlanSuffix())
		|| isNE(utrnIO.getLife(),ptsdtrgIO.getLife())
		|| isNE(utrnIO.getCoverage(),ptsdtrgIO.getCoverage())
		|| isNE(utrnIO.getRider(),ptsdtrgIO.getRider())
		|| isNE(utrnIO.getUnitType(),ptsdtrgIO.getFieldType())
		|| isNE(utrnIO.getUnitVirtualFund(),ptsdtrgIO.getVirtualFund())
		|| isEQ(ptsdtrgIO.getStatuz(),varcom.endp)) {
			checkPlanSuffix2950();
		}
		/*   Update detail record*/
		if (isEQ(utrnIO.getContractAmount(),ZERO)) {
			return ;
		}
		ptsdtrgIO.setEstMatValue(ZERO);
		if (isLTE(utrnIO.getContractAmount(),ZERO)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(mult(utrnIO.getContractAmount(),-1));
		}
		setPrecision(ptsdtrgIO.getActvalue(), 2);
		ptsdtrgIO.setActvalue(add(ptsdtrgIO.getActvalue(),utrnIO.getContractAmount()));
		/* MOVE REWRT                  TO PTSDTRG-FUNCTION.             */
		ptsdtrgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, ptsdtrgIO);
		if (isNE(ptsdtrgIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdtrgIO.getParams());
			fatalError9000();
		}
	}

protected void checkFeedbackIndUtrn2200()
	{
		check2210();
		continue2220();
	}

protected void check2210()
	{
		utrntrgIO.setDataArea(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO UTRNTRG-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO UTRNTRG-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO UTRNTRG-TRANNO.               */
		utrntrgIO.setChdrnum(udtrigrec.tk2Chdrnum);
		utrntrgIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		utrntrgIO.setTranno(udtrigrec.tk2Tranno);
		utrntrgIO.setFunction("BEGN");
	}

protected void continue2220()
	{
		SmartFileCode.execute(appVars, utrntrgIO);
		if (isNE(utrntrgIO.getStatuz(),"****")
		&& isNE(utrntrgIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(utrntrgIO.getParams());
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRNUM     NOT = UTRNTRG-CHDRNUM OR         */
		/*    WSAA-TRIGGER-CHDRCOY     NOT = UTRNTRG-CHDRCOY OR         */
		/*    WSAA-TRIGGER-TRANNO      NOT = UTRNTRG-TRANNO             */
		/*    WSAA-TRIGGER-TRANNO      NOT = UTRNTRG-TRANNO  OR <D60401>*/
		if (isNE(udtrigrec.tk2Chdrcoy,utrntrgIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum,utrntrgIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno,utrntrgIO.getTranno())
		|| isEQ(utrntrgIO.getStatuz(),"ENDP")) {
			wsaaGeneralFieldsInner.wsaaNotReady.set(" ");
			utrntrgIO.setStatuz("ENDP");
			return ;
		}
		if (isNE(utrntrgIO.getFeedbackInd(),"Y")) {
			wsaaGeneralFieldsInner.wsaaNotReady.set("Y");
		}
		else {
			wsaaGeneralFieldsInner.wsaaNotReady.set(" ");
			utrntrgIO.setFunction(varcom.nextr);
			continue2220();
			return ;
		}
	}

protected void checkPlanSuffix2950()
	{
		para2960();
	}

protected void para2960()
	{
		ptsdtrgIO.setParams(SPACES);
		/* MOVE BEGNH                  TO PTSDTRG-FUNCTION.             */
		ptsdtrgIO.setFunction(varcom.begn);
		
		//performance improvement --  Niharika Modi 
//		ptsdtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		ptsdtrgIO.setChdrcoy(utrnIO.getChdrcoy());
		ptsdtrgIO.setChdrnum(utrnIO.getChdrnum());
		ptsdtrgIO.setTranno(utrnIO.getTranno());
		ptsdtrgIO.setPlanSuffix(ZERO);
		ptsdtrgIO.setLife(utrnIO.getLife());
		ptsdtrgIO.setCoverage(utrnIO.getCoverage());
		ptsdtrgIO.setRider(utrnIO.getRider());
		ptsdtrgIO.setFieldType(utrnIO.getUnitType());
		ptsdtrgIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		SmartFileCode.execute(appVars, ptsdtrgIO);
		if (isNE(ptsdtrgIO.getStatuz(),varcom.oK)
		&& isNE(ptsdtrgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdtrgIO.getParams());
			fatalError9000();
		}
		if (isNE(utrnIO.getChdrcoy(), ptsdtrgIO.getChdrcoy())
		|| isNE(utrnIO.getChdrnum(),ptsdtrgIO.getChdrnum())
		|| isNE(utrnIO.getTranno(),ptsdtrgIO.getTranno())
		|| isNE(utrnIO.getLife(),ptsdtrgIO.getLife())
		|| isNE(utrnIO.getCoverage(),ptsdtrgIO.getCoverage())
		|| isNE(utrnIO.getRider(),ptsdtrgIO.getRider())
		|| isNE(ptsdtrgIO.getPlanSuffix(),ZERO)
		|| isNE(utrnIO.getUnitType(),ptsdtrgIO.getFieldType())
		|| isNE(utrnIO.getUnitVirtualFund(),ptsdtrgIO.getVirtualFund())) {
			syserrrec.statuz.set(varcom.mrnf);
			fatalError9000();
		}
	}

protected void checkPlanSuffix22975()
	{
		para2980();
	}

protected void para2980()
	{
		ptsdclmIO.setParams(SPACES);
		ptsdclmIO.setFunction(varcom.begn);
		ptsdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		ptsdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		ptsdclmIO.setTranno(udtrigrec.tk2Tranno);
		ptsdclmIO.setPlanSuffix(ZERO);
		ptsdclmIO.setLife(udtrigrec.life);
		ptsdclmIO.setCoverage(udtrigrec.tk2Coverage);
		ptsdclmIO.setRider(udtrigrec.tk2Rider);
		
		//performance improvement --  Niharika Modi 
//		ptsdclmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
		&& isNE(ptsdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdclmIO.getParams());
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = PTSDCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = PTSDCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = PTSDCLM-TRANNO             */
		/* OR WSAA-TRIGGER-COVERAGE    NOT = PTSDCLM-COVERAGE           */
		/* OR WSAA-TRIGGER-RIDER       NOT = PTSDCLM-RIDER              */
		if (isNE(udtrigrec.tk2Chdrcoy, ptsdclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum,ptsdclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno,ptsdclmIO.getTranno())
		|| isNE(udtrigrec.life,ptsdclmIO.getLife())
		|| isNE(udtrigrec.tk2Coverage,ptsdclmIO.getCoverage())
		|| isNE(udtrigrec.tk2Rider,ptsdclmIO.getRider())
		|| isNE(ptsdclmIO.getPlanSuffix(),ZERO)) {
			syserrrec.statuz.set(varcom.mrnf);
			fatalError9000();
		}
	}

protected void processTransactionLoop22300()
	{
			para2310();
		}

protected void para2310()
	{
		/*  READ all RGWD recs for this transaction*/
		if (isNE(ptsdclmIO.getChdrcoy(),ptshclmIO.getChdrcoy())
		|| isNE(ptsdclmIO.getChdrnum(),ptshclmIO.getChdrnum())
		|| isNE(ptsdclmIO.getTranno(),ptshclmIO.getTranno())
		|| isEQ(ptsdclmIO.getStatuz(),varcom.endp)) {
			calculateFee3800();
			wsaaGeneralFieldsInner.wsaaEndTrans.set("Y");
			wsaaGeneralFieldsInner.wsaaNotReady.set(" ");
			return ;
		}
		/* IF  PTSDCLM-EST-MAT-VALUE     = ZERO                         */
		/* IF  PTSDCLM-EST-MAT-VALUE NOT = ZERO                    <009>*/
		/*     MOVE 'Y' TO WSAA-NOT-READY                               */
		/*MOVE ENDP TO PTSDCLM-STATUZ*/
		/*     GO TO 2390-EXIT.                                         */
		if (isNE(ptsdclmIO.getEstMatValue(),ZERO)
		&& isNE(ptsdclmIO.getFieldType(),"C")
		&& isNE(ptsdclmIO.getFieldType(),"J")) {
			wsaaGeneralFieldsInner.wsaaNotReady.set("Y");
			return ;
		}
		/*   Accumulate actual fee*/
		/* IF  PTSDCLM-COVERAGE NOT =  WSAA-COVERAGE                    */
		/*  AND NOT 1ST-TIME-THRU                                       */
		/*     PERFORM 1500-READ-T5540                                  */
		/*     PERFORM 1550-READ-T5542                                  */
		/* MOVE  PTSDCLM-COVERAGE TO WSAA-COVERAGE.                     */
		/* IF NOT 1ST-TIME-THRU                                    <011>*/
		if (isNE(ptsdclmIO.getCoverage(), wsaaGeneralFieldsInner.wsaaCoverage)
		|| isNE(ptsdclmIO.getChdrnum(), wsaaGeneralFieldsInner.wsaaPtsdChdrnum)
		|| isNE(ptsdclmIO.getCrtable(), wsaaGeneralFieldsInner.wsaaCrtable)
		|| isNE(ptsdclmIO.getRider(), wsaaGeneralFieldsInner.wsaaRider)
		|| isNE(ptsdclmIO.getLife(), wsaaGeneralFieldsInner.wsaaLife)) {
			/*       PERFORM 1530-READ-T5687                           <011>*/
			/*       PERFORM 1550-READ-T5542                           <011>*/
			/*       PERFORM 1600-GET-FREQ-T5542                       <011>*/
			calculateFee3800();
			/*    IF T5542-FEEPC(WSAA-SUB) = ZEROS AND                 <011>*/
			/*       T5542-FFAMT(WSAA-SUB) = ZEROS                     <011>*/
			/*       MOVE H388             TO SYSR-STATUZ              <011>*/
			/*       GO TO 2390-EXIT                                   <011>*/
			/*    END-IF                                               <011>*/
			wsaaGeneralFieldsInner.wsaaPtsdChdrnum.set(ptsdclmIO.getChdrnum());
			wsaaGeneralFieldsInner.wsaaCrtable.set(ptsdclmIO.getCrtable());
			wsaaGeneralFieldsInner.wsaaLife.set(ptsdclmIO.getLife());
			wsaaGeneralFieldsInner.wsaaCoverage.set(ptsdclmIO.getCoverage());
			wsaaGeneralFieldsInner.wsaaRider.set(ptsdclmIO.getRider());
		}
		/* END-IF.                                                 <011>*/
		if (wsaaGeneralFieldsInner.n1stTimeThru.isTrue()) {
			/*      MOVE ZERO TO WSAA-COV-TOT  WSAA-TOT-FEE          <D96NUM>*/
			wsaaTotFee.set(ZERO);
			wsaaGeneralFieldsInner.wsaa1stTime.set("N");
		}
		if (isNE(ptsdclmIO.getFieldType(),"C")
		&& isNE(ptsdclmIO.getFieldType(),"J")) {
			wsaaGeneralFieldsInner.wsaaStoreFee.add(ptsdclmIO.getActvalue());
			wsaaActualVal.add(ptsdclmIO.getActvalue());
		}
		ptsdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
		&& isNE(ptsdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdclmIO.getParams());
			fatalError9000();
		}
	}

protected void readUtrn2700()
	{
		read2710();
	}

protected void read2710()
	{
		utrnIO.setDataArea(SPACES);
		/* MOVE UTRN-LINKAGE           TO UTRN-DATA-KEY.                */
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		utrnIO.setFunction("READR");
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),"****")
		&& isNE(utrnIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void postGross3000()
	{
		start3000();
	}

protected void start3000()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFA-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFA-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.               */
		/*  MOVE 'T683'                 TO LIFA-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFA-BATCBATCH.               */
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rdocnum.set(ptshclmIO.getChdrnum());
		lifacmvrec.tranno.set(ptshclmIO.getTranno());
		/*MOVE T5645-SACSCODE-01          TO LIFA-SACSCODE.            */
		/*MOVE T5645-SACSTYPE-01          TO LIFA-SACSTYP.             */
		/*MOVE T5645-GLMAP-01             TO LIFA-GLCODE.              */
		/*MOVE T5645-SIGN-01              TO LIFA-GLSIGN.              */
		/*MOVE T5645-CNTTOT-01            TO LIFA-CONTOT.              */
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(ptshclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(ptshclmIO.getChdrcoy());
		/*MOVE PTSHCLM-CHDRNUM            TO LIFA-RLDGACCT.            */
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			wsaaRldgChdrnum.set(ptshclmIO.getChdrnum());
			wsaaPlan.set(ptsdclmIO.getPlanSuffix());
			/*       MOVE PTSDCLM-LIFE     TO WSAA-RLDG-LIFE          <LA4524>*/
			/*       MOVE PTSDCLM-COVERAGE TO WSAA-RLDG-COVERAGE      <LA4524>*/
			/*       MOVE PTSDCLM-RIDER    TO WSAA-RLDG-RIDER         <LA4524>*/
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/*        Set up Coverage/Rider substitution code No 6             */
			/*       MOVE PTSDCLM-CRTABLE  TO LIFA-SUBSTITUTE-CODE(06)<LA4524>*/
			lifacmvrec.substituteCode[6].set(covrmjaIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(ptshclmIO.getChdrnum());
			/*        Set up Coverage/Rider substitution code No 6             */
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.origcurr.set(ptshclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaActualVal);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(ptshclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(ptshclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(ptshclmIO.getCnttype());
		/* MOVE SPACES              TO LIFA-SUBSTITUTE-CODE(06).   <008>*/
		/* MOVE PARM-TRANID                TO VRCM-TRANID.              */
		/* MOVE VRCM-TERMID                TO LIFA-TERMID.              */
		/* MOVE VRCM-USER                  TO LIFA-USER.                */
		/* MOVE VRCM-TIME                  TO LIFA-TRANSACTION-TIME.    */
		/* MOVE VRCM-DATE                  TO LIFA-TRANSACTION-DATE.    */
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void postNet3100()
	{
		start3100();
	}

protected void start3100()
	{
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(ZERO);
		lifrtrnrec.frcdate.set(ZERO);
		lifrtrnrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFR-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFR-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFR-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFR-BATCACTMN.               */
		/*  MOVE 'T683'                 TO LIFR-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFR-BATCBATCH.               */
		lifrtrnrec.batckey.set(batcdorrec.batchkey);
		lifrtrnrec.rdocnum.set(ptshclmIO.getChdrnum());
		lifrtrnrec.tranno.set(ptshclmIO.getTranno());
		lifrtrnrec.sacscode.set(t5645rec.sacscode02);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype02);
		lifrtrnrec.glcode.set(t5645rec.glmap02);
		lifrtrnrec.glsign.set(t5645rec.sign02);
		lifrtrnrec.contot.set(t5645rec.cnttot02);
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.rldgcoy.set(ptshclmIO.getChdrcoy());
		lifrtrnrec.genlcoy.set(ptshclmIO.getChdrcoy());
		lifrtrnrec.rldgacct.set(ptshclmIO.getChdrnum());
		lifrtrnrec.origcurr.set(ptshclmIO.getCurrcd());
		lifrtrnrec.origamt.set(wsaaNetVal);
		lifrtrnrec.genlcur.set(SPACES);
		lifrtrnrec.acctamt.set(ZERO);
		lifrtrnrec.crate.set(ZERO);
		lifrtrnrec.postyear.set(SPACES);
		lifrtrnrec.postmonth.set(SPACES);
		lifrtrnrec.tranref.set(ptshclmIO.getTranno());
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifrtrnrec.effdate.set(ptshclmIO.getEffdate());
		lifrtrnrec.frcdate.set("99999999");
		lifrtrnrec.substituteCode[1].set(ptshclmIO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		/* MOVE PARM-TRANID                TO VRCM-TRANID.              */
		/* MOVE VRCM-TERMID                TO LIFR-TERMID.              */
		/* MOVE VRCM-USER                  TO LIFR-USER.                */
		/* MOVE VRCM-TIME                  TO LIFR-TRANSACTION-TIME.    */
		/* MOVE VRCM-DATE                  TO LIFR-TRANSACTION-DATE.    */
		lifrtrnrec.user.set(ZERO);
		lifrtrnrec.transactionTime.set(getCobolTime());
		lifrtrnrec.transactionDate.set(getCobolDate());
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, "****")) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			fatalError9000();
		}
	}

protected void postPenalty3200()
	{
			start3200();
		}

protected void start3200()
	{
		if (isEQ(wsaaTotFee,ZERO)) {
			return ;
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(chdrenqIO.getOccdate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError9000();
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		/*  MOVE PARM-COMPANY           TO LIFA-BATCCOY.                 */
		/*  MOVE PARM-BRANCH            TO LIFA-BATCBRN.                 */
		/*  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.               */
		/*  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.               */
		/*  MOVE 'T683'                 TO LIFA-BATCTRCDE.               */
		/*  MOVE SPACE                  TO LIFA-BATCBATCH.               */
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rdocnum.set(ptshclmIO.getChdrnum());
		lifacmvrec.tranno.set(ptshclmIO.getTranno());
		/*    MOVE T5645-SACSCODE-03          TO LIFA-SACSCODE.            */
		/*    MOVE T5645-SACSTYPE-03          TO LIFA-SACSTYP.             */
		/*    MOVE T5645-GLMAP-03             TO LIFA-GLCODE.              */
		/*    MOVE T5645-SIGN-03              TO LIFA-GLSIGN.              */
		/*    MOVE T5645-CNTTOT-03            TO LIFA-CONTOT.              */
		if (isLT(ptshclmIO.getEffdate(),datcon2rec.intDate2)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
		}
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(ptshclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(ptshclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(ptshclmIO.getChdrnum());
		lifacmvrec.origcurr.set(ptshclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaTotFee);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(ptshclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(ptshclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(ptshclmIO.getCnttype());
		/* blank out Code 6 which is only used for component level         */
		/* accounting                                                      */
		lifacmvrec.substituteCode[6].set(SPACES);
		/* MOVE PARM-TRANID                TO VRCM-TRANID.              */
		/* MOVE VRCM-TERMID                TO LIFA-TERMID.              */
		/* MOVE VRCM-USER                  TO LIFA-USER.                */
		/* MOVE VRCM-TIME                  TO LIFA-TRANSACTION-TIME.    */
		/* MOVE VRCM-DATE                  TO LIFA-TRANSACTION-DATE.    */
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void writeLetc3200a()
	{
			letc3210a();
		}

protected void letc3210a()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ptshclmIO.getChdrcoy());
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ptshclmIO.getCnttype());
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		/*    Bypass the letter requested process when item not found.     */
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			/*     MOVE G437               TO SYSR-STATUZ           <V4LAQR>*/
			/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <V4LAQR>*/
			/*     PERFORM 9000-FATAL-ERROR.                        <V4LAQR>*/
			return ;
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		letrqstrec.statuz.set(SPACES);
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.clntcoy.set(chdrenqIO.getCowncoy());
		letrqstrec.clntnum.set(chdrenqIO.getCownnum());
		letrqstrec.branch.set(chdrenqIO.getCntbranch());
		/* MOVE UDTG-PREFIX            TO LETRQST-RDOCPFX.              */
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.requestCompany.set(udtrigrec.chdrcoy);
		letrqstrec.chdrcoy.set(udtrigrec.chdrcoy);
		letrqstrec.rdoccoy.set(udtrigrec.chdrcoy);
		letrqstrec.rdocnum.set(udtrigrec.chdrnum);
		letrqstrec.chdrnum.set(udtrigrec.chdrnum);
		letrqstrec.tranno.set(udtrigrec.tranno);
		wsaaLanguage.set(udtrigrec.language);
		wsaaOthLife.set(udtrigrec.life);
		wsaaOthCoverage.set(udtrigrec.coverage);
		wsaaOthRider.set(udtrigrec.rider);
		wsaaOthPlnsfx.set(udtrigrec.planSuffix);
		wsaaTrcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.otherKeys.set(wsaaOtherKeys);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			fatalError9000();
		}
	}

protected void updatePenaltyRec3300()
	{
		readPtsd3310();
	}

protected void readPtsd3310()
	{
		ptsdpenIO.setParams(SPACES);
		ptsdpenIO.setFunction(varcom.readh);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO PTSDPEN-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO PTSDPEN-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO PTSDPEN-TRANNO.               */
		/* MOVE WSAA-TRIGGER-SUFFIX    TO PTSDPEN-PLAN-SUFFIX.          */
		/* MOVE WSAA-TRIGGER-COVERAGE  TO PTSDPEN-COVERAGE.             */
		/* MOVE WSAA-TRIGGER-RIDER     TO PTSDPEN-RIDER.                */
		ptsdpenIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		ptsdpenIO.setChdrnum(udtrigrec.tk2Chdrnum);
		ptsdpenIO.setTranno(udtrigrec.tk2Tranno);
		ptsdpenIO.setPlanSuffix(udtrigrec.tk2Suffix);
		ptsdpenIO.setLife(udtrigrec.life);
		ptsdpenIO.setCoverage(udtrigrec.tk2Coverage);
		ptsdpenIO.setRider(udtrigrec.tk2Rider);
		ptsdpenIO.setFormat(formatsInner.ptsdpenrec);
		SmartFileCode.execute(appVars, ptsdpenIO);
		if (isNE(ptsdpenIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdpenIO.getParams());
			fatalError9000();
		}
		wsaaTotFee.set(wsaaActualVal);
		ptsdpenIO.setFunction(varcom.rewrt);
		/* MOVE PTSDCLMREC             TO PTSDPEN-FORMAT.               */
		ptsdpenIO.setFormat(formatsInner.ptsdpenrec);
		SmartFileCode.execute(appVars, ptsdpenIO);
		if (isNE(ptsdpenIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdpenIO.getParams());
			fatalError9000();
		}
	}

protected void openBatch3400()
	{
		open3410();
	}

protected void open3410()
	{
		batcdorrec.function.set("AUTO");
		/* MOVE PARM-TRANID            TO BATD-TRANID.          <D60401>*/
		wsaaTransTime.set(getCobolTime());
		wsaaTransDate.set(getCobolDate());
		wsaaTermid.set(SPACES);
		wsaaUser.set(ZERO);
		batcdorrec.tranid.set(wsaaTranid);
		/* MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.         <INTBR> */
		/* MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.         <INTBR> */
		/* MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.       <INTBR> */
		/* MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.       <INTBR> */
		/* MOVE UTRN-BATCTRCDE         TO LIFA-BATCTRCDE.       <INTBR> */
		/* MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.       <INTBR> */
		/*   MOVE WSAA-BATCKEY           TO LIFA-BATCKEY.         <LA4524>*/
		lifacmvrec.batckey.set(udtrigrec.batchkey);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaBatckey.batcKey.set(lifacmvrec.batckey);
		batcdorrec.batchkey.set(lifacmvrec.batckey);
		batcdorrec.prefix.set("BA");
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		lifacmvrec.batckey.set(batcdorrec.batchkey);
	}

protected void closeBatch3500()
	{
		close3510();
	}

protected void close3510()
	{
		batcuprec.function.set(varcom.writs);
		batcuprec.batchkey.set(batcdorrec.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		batcdorrec.function.set("CLOSE");
		/* MOVE PARM-TRANID            TO BATD-TRANID.          <D60401>*/
		wsaaTransTime.set(getCobolTime());
		wsaaTransDate.set(getCobolDate());
		wsaaTermid.set(SPACES);
		wsaaUser.set(ZERO);
		batcdorrec.tranid.set(wsaaTranid);
		batcdorrec.batchkey.set(batcdorrec.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
	}

protected void updatFee3600()
	{
		/*START*/
		/***** MOVE SPACES                 TO PTSDCLM-PARAMS.          <011>*/
		/***** MOVE READH                  TO PTSDCLM-FUNCTION.        <011>*/
		/***** MOVE WSAA-TRIGGER-CHDRCOY   TO PTSDCLM-CHDRCOY.         <011>*/
		/***** MOVE WSAA-TRIGGER-CHDRNUM   TO PTSDCLM-CHDRNUM.         <011>*/
		/***** MOVE WSAA-TRIGGER-TRANNO    TO PTSDCLM-TRANNO.          <011>*/
		/***** MOVE WSAA-TRIGGER-SUFFIX    TO PTSDCLM-PLAN-SUFFIX.     <011>*/
		/***** MOVE WSAA-TRIGGER-COVERAGE  TO PTSDCLM-COVERAGE.        <011>*/
		/***** MOVE WSAA-TRIGGER-RIDER     TO PTSDCLM-RIDER.           <011>*/
		/***** MOVE PTSDCLMREC             TO PTSDCLM-FORMAT.          <011>*/
		/*****                                                         <011>*/
		/***** CALL 'PTSDCLMIO' USING PTSDCLM-PARAMS.                  <011>*/
		/***** IF PTSDCLM-STATUZ           NOT = O-K                   <011>*/
		/*****     MOVE PTSDCLM-PARAMS     TO SYSR-PARAMS              <011>*/
		/*****     MOVE PTSDCLMREC         TO WSAA-ERROR-FORMAT        <011>*/
		/*****     PERFORM 9000-FATAL-ERROR.                           <011>*/
		/*****                                                         <011>*/
		/***** MOVE WSAA-TOT-FEE           TO PTSDCLM-ACTVALUE.        <011>*/
		/*****                                                         <011>*/
		/***** MOVE REWRT                  TO PTSDCLM-FUNCTION.        <011>*/
		/*****                                                         <011>*/
		/***** MOVE PTSDCLMREC             TO PTSDCLM-FORMAT.          <011>*/
		/*****                                                         <011>*/
		/***** CALL 'PTSDCLMIO' USING PTSDCLM-PARAMS.                  <011>*/
		/*****                                                         <011>*/
		/***** IF PTSDCLM-STATUZ           NOT = O-K                   <011>*/
		/*****     MOVE PTSDCLM-PARAMS     TO SYSR-PARAMS              <011>*/
		/*****     MOVE PTSDCLMREC         TO WSAA-ERROR-FORMAT        <011>*/
		/*****     PERFORM 9000-FATAL-ERROR.                           <011>*/
		/*EXIT*/
	}

protected void calculateFee3700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					calc3710();
				case rewrtPtsdpen3740: 
					rewrtPtsdpen3740();
				case exit3700: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void calc3710()
	{
		/*  NOTE, change <015> has made this section redundant. To  *****/
		/*  avoid excessive commenting out, only the PERFORM of     *****/
		/*  this section has been commented out and the FEE is now  *****/
		/*  calculated in the 3800- section                         *****/
		/* IF T5542-FEEPC(WSAA-SUB)    = ZEROS                     <011>*/
		/*   MOVE T5542-FFAMT(WSAA-SUB) TO WSAA-TOT-FEE            <011>*/
		/*   GO TO 3700-EXIT                                       <011>*/
		/* ELSE                                                    <011>*/
		/*   COMPUTE WSAA-ROUND-FEE ROUNDED  = WSAA-ACTUAL-VAL *   <011>*/
		/*                               T5542-FEEPC(WSAA-SUB)/ 100<011>*/
		/*   MOVE WSAA-ROUND-FEE         TO WSAA-TOT-FEE           <011>*/
		/* END-IF.                                                 <011>*/
		/*                                                         <011>*/
		/* IF  WSAA-TOT-FEE            < T5542-FEEMIN(WSAA-SUB)    <011>*/
		/*   MOVE T5542-FEEMIN(WSAA-SUB) TO WSAA-TOT-FEE           <011>*/
		/* END-IF.                                                 <011>*/
		/*                                                         <011>*/
		/* IF WSAA-TOT-FEE             > T5542-FEEMAX(WSAA-SUB)    <011>*/
		/*   MOVE T5542-FEEMAX(WSAA-SUB) TO WSAA-TOT-FEE           <011>*/
		/* END-IF.                                                 <011>*/
		/*                                                         <011>*/
		/*  Read the appropriate fee record for the claim record currently */
		/*  being processed. If no fee record is present, no need to       */
		/*  calculate or update, and so exit this section.                 */
		ptsdpenIO.setParams(SPACES);
		ptsdpenIO.setFunction(varcom.readh);
		ptsdpenIO.setChdrcoy(ptsdtrgIO.getChdrcoy());
		ptsdpenIO.setChdrnum(ptsdtrgIO.getChdrnum());
		ptsdpenIO.setTranno(ptsdtrgIO.getTranno());
		ptsdpenIO.setPlanSuffix(ptsdtrgIO.getPlanSuffix());
		ptsdpenIO.setLife(ptsdtrgIO.getLife());
		ptsdpenIO.setCoverage(ptsdtrgIO.getCoverage());
		ptsdpenIO.setRider(ptsdtrgIO.getRider());
		ptsdpenIO.setFormat(formatsInner.ptsdpenrec);
		SmartFileCode.execute(appVars, ptsdpenIO);
		if (isEQ(ptsdpenIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3700);
		}
		if (isNE(ptsdpenIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdpenIO.getParams());
			fatalError9000();
		}
		/*  Retrieve the minimum/maximum fee amounts and fee percentage    */
		/*  from the appropriate item on table T5542 for the trigger       */
		/*  record.                                                        */
		readT56871530();
		readT55421550();
		getFreqT55421600();
		/*  Either use the table fee amount for the actual fee amount,     */
		/*  or calculate it using the actual claim amount and the          */
		/*  percentage.                                                    */
		if (isEQ(t5542rec.feepc[wsaaSub.toInt()],ZERO)) {
			ptsdpenIO.setActvalue(t5542rec.ffamt[wsaaSub.toInt()]);
			goTo(GotoLabel.rewrtPtsdpen3740);
		}
		else {
			setPrecision(ptsdpenIO.getActvalue(), 3);
			ptsdpenIO.setActvalue(div(mult(ptsdtrgIO.getActvalue(),t5542rec.feepc[wsaaSub.toInt()]),100), true);
		}
		if (isEQ(t5542rec.feemin[wsaaSub.toInt()],ZERO)
		&& isEQ(t5542rec.feemax[wsaaSub.toInt()],ZERO)) {
			goTo(GotoLabel.rewrtPtsdpen3740);
		}
		if (isLT(ptsdpenIO.getActvalue(),t5542rec.feemin[wsaaSub.toInt()])) {
			ptsdpenIO.setActvalue(t5542rec.feemin[wsaaSub.toInt()]);
		}
		if (isGT(ptsdpenIO.getActvalue(),t5542rec.feemax[wsaaSub.toInt()])
		&& isGT(t5542rec.feemax[wsaaSub.toInt()],ZERO)) {
			ptsdpenIO.setActvalue(t5542rec.feemax[wsaaSub.toInt()]);
		}
	}

protected void rewrtPtsdpen3740()
	{
		/*  Update the fee record with the calculated actual value         */
		/*  and zeroise the estimated value.                               */
		ptsdpenIO.setEstMatValue(ZERO);
		ptsdpenIO.setFunction(varcom.rewrt);
		ptsdpenIO.setFormat(formatsInner.ptsdpenrec);
		SmartFileCode.execute(appVars, ptsdpenIO);
		if (isNE(ptsdpenIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdpenIO.getParams());
			fatalError9000();
		}
	}

protected void calculateFee3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3800();
				case rewrtPtsdpen3840: 
					rewrtPtsdpen3840();
				case exit3800: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3800()
	{
		/*  Calculate the fee(s) for this transaction. There is one     */
		/*  for each component (which may have several fund holdings).  */
		/*  This is only carried out when all UTRNs for the             */
		/*  transaction have been processed.                            */
		/*  Read the appropriate fee record for the claim record        */
		/*  currently being processed. If no fee record is present,     */
		/*  no need to calculate or update, and so exit this section.   */
		ptsdpenIO.setParams(SPACES);
		ptsdpenIO.setFunction(varcom.readh);
		ptsdpenIO.setChdrcoy(ptsdtrgIO.getChdrcoy());
		ptsdpenIO.setChdrnum(ptsdtrgIO.getChdrnum());
		ptsdpenIO.setTranno(ptsdtrgIO.getTranno());
		ptsdpenIO.setPlanSuffix(ptsdtrgIO.getPlanSuffix());
		ptsdpenIO.setLife(wsaaGeneralFieldsInner.wsaaLife);
		ptsdpenIO.setCoverage(wsaaGeneralFieldsInner.wsaaCoverage);
		ptsdpenIO.setRider(wsaaGeneralFieldsInner.wsaaRider);
		ptsdpenIO.setFormat(formatsInner.ptsdpenrec);
		SmartFileCode.execute(appVars, ptsdpenIO);
		if (isEQ(ptsdpenIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3800);
		}
		if (isNE(ptsdpenIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdpenIO.getParams());
			fatalError9000();
		}
		/*  Retrieve the Fee calculation details from T5542.            */
		readT56871530();
		readT55421550();
		getFreqT55421600();
		/*  Either use the table fee amount for the actual fee amount,  */
		/*  or calculate it using the actual claim amount and the       */
		/*  percentage.                                                 */
		if (isEQ(t5542rec.feepc[wsaaSub.toInt()],ZERO)) {
			ptsdpenIO.setActvalue(t5542rec.ffamt[wsaaSub.toInt()]);
			goTo(GotoLabel.rewrtPtsdpen3840);
		}
		else {
			setPrecision(ptsdpenIO.getActvalue(), 3);
			ptsdpenIO.setActvalue(div((mult(wsaaGeneralFieldsInner.wsaaStoreFee, t5542rec.feepc[wsaaSub.toInt()])), 100), true);
		}
		zrdecplrec.amountIn.set(ptsdpenIO.getActvalue());
		a000CallRounding();
		ptsdpenIO.setActvalue(zrdecplrec.amountOut);
		if (isEQ(t5542rec.feemin[wsaaSub.toInt()],ZERO)
		&& isEQ(t5542rec.feemax[wsaaSub.toInt()],ZERO)) {
			goTo(GotoLabel.rewrtPtsdpen3840);
		}
		if (isLT(ptsdpenIO.getActvalue(),t5542rec.feemin[wsaaSub.toInt()])) {
			ptsdpenIO.setActvalue(t5542rec.feemin[wsaaSub.toInt()]);
		}
		if (isGT(ptsdpenIO.getActvalue(),t5542rec.feemax[wsaaSub.toInt()])
		&& isGT(t5542rec.feemax[wsaaSub.toInt()],ZERO)) {
			ptsdpenIO.setActvalue(t5542rec.feemax[wsaaSub.toInt()]);
		}
	}

protected void rewrtPtsdpen3840()
	{
		/*  Update the fee record with the calculated actual value      */
		/*  and zeroise the estimated value.                            */
		wsaaTotFee.add(ptsdpenIO.getActvalue());
		wsaaGeneralFieldsInner.wsaaStoreFee.set(ZERO);
		ptsdpenIO.setEstMatValue(ZERO);
		ptsdpenIO.setFunction(varcom.rewrt);
		ptsdpenIO.setFormat(formatsInner.ptsdpenrec);
		SmartFileCode.execute(appVars, ptsdpenIO);
		if (isNE(ptsdpenIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdpenIO.getParams());
			fatalError9000();
		}
	}

protected void calcSurChrg3900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					init3901();
				case callPtsdsrc3903: 
					callPtsdsrc3903();
					updActSurChrg3907();
				case exit3909: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init3901()
	{
		wsaaTotSurChrg.set(ZERO);
		/*SETUP-PTSDSRC-KEY*/
		ptsdsrcIO.setParams(SPACES);
		ptsdsrcIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		ptsdsrcIO.setChdrnum(udtrigrec.tk2Chdrnum);
		ptsdsrcIO.setTranno(udtrigrec.tk2Tranno);
		ptsdsrcIO.setPlanSuffix(ZERO);
		/* MOVE BEGNH                  TO PTSDSRC-FUNCTION.     <LA3993>*/
		ptsdsrcIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ptsdsrcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptsdsrcIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		
	}

protected void callPtsdsrc3903()
	{
		SmartFileCode.execute(appVars, ptsdsrcIO);
		if (isNE(ptsdsrcIO.getStatuz(),varcom.oK)
		&& isNE(ptsdsrcIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdsrcIO.getParams());
			fatalError9000();
		}
		if (isNE(ptsdsrcIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit3909);
		}
		if (isNE(ptsdsrcIO.getChdrcoy(),udtrigrec.tk2Chdrcoy)
		|| isNE(ptsdsrcIO.getChdrnum(),udtrigrec.tk2Chdrnum)
		|| isNE(ptsdsrcIO.getTranno(),udtrigrec.tk2Tranno)) {
			/*     MOVE REWRT              TO PTSDSRC-FUNCTION      <LA4094>*/
			/*     CALL 'PTSDSRCIO'        USING PTSDSRC-PARAMS     <LA4094>*/
			/*     IF  PTSDSRC-STATUZ      NOT = O-K                <LA4094>*/
			/*         MOVE PTSDSRC-PARAMS TO SYSR-PARAMS           <LA4094>*/
			/*         MOVE PTSDSRCREC     TO WSAA-ERROR-FORMAT     <LA4094>*/
			/*         PERFORM 9000-FATAL-ERROR                     <LA4094>*/
			/*     END-IF                                           <LA4094>*/
			goTo(GotoLabel.exit3909);
		}
		calcCmpSurChrg3910();
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			postCmpSurChrg3940();
		}
		wsaaTotSurChrg.add(wsaaCmpSurChrg);
		/* Post the tax on the surrender penalty                           */
		if (isNE(tr52drec.txcode, SPACES)) {
			b500PostPenaltyTax();
		}
	}

protected void updActSurChrg3907()
	{
		ptsdsrcIO.setEstMatValue(ZERO);
		ptsdsrcIO.setActvalue(wsaaCmpSurChrg);
		/* MOVE REWRT                  TO PTSDSRC-FUNCTION.     <LA4094>*/
		ptsdsrcIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, ptsdsrcIO);
		if (isNE(ptsdsrcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdsrcIO.getParams());
			fatalError9000();
		}
		/*NEXT-PTSDSRC*/
		ptsdsrcIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callPtsdsrc3903);
	}

protected void calcCmpSurChrg3910()
	{
			start3911();
		}

protected void start3911()
	{
		sumCmpCsv3920();
		if (isNE(ptsdsrcIO.getPercreqd(),ZERO)) {
			/*     COMPUTE WSAA-CMP-SUR-CHRG = WSAA-CMP-CSV         <LA4965>*/
			compute(wsaaCmpSurChrg, 6).setRounded(div(mult(wsaaCmpCsv, ptsdsrcIO.getPercreqd()), 100));
			zrdecplrec.amountIn.set(wsaaCmpSurChrg);
			a000CallRounding();
			wsaaCmpSurChrg.set(zrdecplrec.amountOut);
			return ;
		}
		if (isNE(ptshclmIO.getPrcnt(),ZERO)) {
			/*     COMPUTE WSAA-CMP-SUR-CHRG = (WSAA-CMP-CSV        <LA4965>*/
			compute(wsaaCmpSurChrg, 6).setRounded(mult((div(mult(wsaaCmpCsv, ptshclmIO.getPrcnt()), 100)), (div(ptsdsrcIO.getPercreqd(), 100))));
			zrdecplrec.amountIn.set(wsaaCmpSurChrg);
			a000CallRounding();
			wsaaCmpSurChrg.set(zrdecplrec.amountOut);
			return ;
		}
		sumCntCsv3930();
		/* COMPUTE WSAA-CMP-SUR-CHRG   = (PTSHCLM-TOTALAMT      <LA4965>*/
		compute(wsaaCmpSurChrg, 6).setRounded(mult((mult(ptshclmIO.getTotalamt(), (div(wsaaCmpCsv, wsaaCntCsv)))), (div(ptsdsrcIO.getPercreqd(), 100))));
		zrdecplrec.amountIn.set(wsaaCmpSurChrg);
		a000CallRounding();
		wsaaCmpSurChrg.set(zrdecplrec.amountOut);
	}

protected void sumCmpCsv3920()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3921();
				case callPtsdclm3922: 
					callPtsdclm3922();
				case nextPtsdclm3928: 
					nextPtsdclm3928();
				case exit3929: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3921()
	{
		wsaaCmpCsv.set(ZERO);
		ptsdclmIO.setParams(SPACES);
		ptsdclmIO.setChdrcoy(ptsdsrcIO.getChdrcoy());
		ptsdclmIO.setChdrnum(ptsdsrcIO.getChdrnum());
		ptsdclmIO.setTranno(ptsdsrcIO.getTranno());
		ptsdclmIO.setPlanSuffix(ptsdsrcIO.getPlanSuffix());
		ptsdclmIO.setLife(ptsdsrcIO.getLife());
		ptsdclmIO.setCoverage(ptsdsrcIO.getCoverage());
		ptsdclmIO.setRider(ptsdsrcIO.getRider());
		ptsdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		ptsdclmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void callPtsdclm3922()
	{
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
		&& isNE(ptsdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdclmIO.getParams());
			fatalError9000();
		}
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
		|| isNE(ptsdclmIO.getRider(),ptsdsrcIO.getRider())
		|| isNE(ptsdclmIO.getCoverage(),ptsdsrcIO.getCoverage())
		|| isNE(ptsdclmIO.getLife(),ptsdsrcIO.getLife())
		|| isNE(ptsdclmIO.getPlanSuffix(),ptsdsrcIO.getPlanSuffix())
		|| isNE(ptsdclmIO.getTranno(),ptsdsrcIO.getTranno())
		|| isNE(ptsdclmIO.getChdrnum(),ptsdsrcIO.getChdrnum())
		|| isNE(ptsdclmIO.getChdrcoy(),ptsdsrcIO.getChdrcoy())) {
			goTo(GotoLabel.exit3929);
		}
		if (isEQ(ptsdclmIO.getFieldType(),"C")
		|| isEQ(ptsdclmIO.getFieldType(),"J")) {
			goTo(GotoLabel.nextPtsdclm3928);
		}
		wsaaCmpCsv.add(ptsdclmIO.getActvalue());
	}

protected void nextPtsdclm3928()
	{
		ptsdclmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callPtsdclm3922);
	}

protected void sumCntCsv3930()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3931();
				case callPtsdclm3932: 
					callPtsdclm3932();
				case nextPtsdclm3938: 
					nextPtsdclm3938();
				case exit3939: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3931()
	{
		wsaaCntCsv.set(ZERO);
		ptsdclmIO.setParams(SPACES);
		ptsdclmIO.setChdrcoy(ptsdsrcIO.getChdrcoy());
		ptsdclmIO.setChdrnum(ptsdsrcIO.getChdrnum());
		ptsdclmIO.setTranno(ptsdsrcIO.getTranno());
		ptsdclmIO.setPlanSuffix(ZERO);
		ptsdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ptsdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptsdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
	}

protected void callPtsdclm3932()
	{
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
		&& isNE(ptsdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdclmIO.getParams());
			fatalError9000();
		}
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
		|| isNE(ptsdclmIO.getTranno(),ptsdsrcIO.getTranno())
		|| isNE(ptsdclmIO.getChdrnum(),ptsdsrcIO.getChdrnum())
		|| isNE(ptsdclmIO.getChdrcoy(),ptsdsrcIO.getChdrcoy())) {
			/*      GO TO 3929-EXIT                                  <S19FIX>*/
			goTo(GotoLabel.exit3939);
		}
		if (isEQ(ptsdclmIO.getFieldType(),"C")
		|| isEQ(ptsdclmIO.getFieldType(),"J")) {
			goTo(GotoLabel.nextPtsdclm3938);
		}
		wsaaCntCsv.add(ptsdclmIO.getActvalue());
	}

protected void nextPtsdclm3938()
	{
		ptsdclmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callPtsdclm3932);
	}

protected void postCmpSurChrg3940()
	{
			start3941();
		}

protected void start3941()
	{
		if (isEQ(wsaaCmpSurChrg,ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rdocnum.set(ptshclmIO.getChdrnum());
		lifacmvrec.tranno.set(ptshclmIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(ptshclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(ptshclmIO.getChdrcoy());
		wsaaRldgChdrnum.set(ptsdsrcIO.getChdrnum());
		wsaaRldgLife.set(ptsdsrcIO.getLife());
		wsaaRldgCoverage.set(ptsdsrcIO.getCoverage());
		wsaaRldgRider.set(ptsdsrcIO.getRider());
		wsaaPlanSuff.set(ptsdsrcIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.origcurr.set(ptshclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaCmpSurChrg);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(ptshclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(ptshclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(ptshclmIO.getCnttype());
		lifacmvrec.substituteCode[6].set(ptsdsrcIO.getCrtable());
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void postCntSurChrg3950()
	{
			start3951();
		}

protected void start3951()
	{
		if (isEQ(wsaaTotSurChrg,ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rdocnum.set(ptshclmIO.getChdrnum());
		lifacmvrec.tranno.set(ptshclmIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(ptshclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(ptshclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(ptshclmIO.getChdrnum());
		lifacmvrec.origcurr.set(ptshclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaTotSurChrg);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(ptshclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(ptshclmIO.getEffdate());
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(ptshclmIO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void a100CreateZrpw()
	{
			a101CreateZrpw();
		}

protected void a101CreateZrpw()
	{
		zrpwIO.setParams(SPACES);
		zrpwIO.setChdrcoy(udtrigrec.chdrcoy);
		zrpwIO.setChdrnum(udtrigrec.chdrnum);
		zrpwIO.setTranno(udtrigrec.tranno);
		zrpwIO.setLife(udtrigrec.life);
		zrpwIO.setCoverage(udtrigrec.coverage);
		zrpwIO.setFunction(varcom.readr);
		zrpwIO.setFormat(formatsInner.zrpwrec);
		SmartFileCode.execute(appVars, zrpwIO);
		if (isNE(zrpwIO.getStatuz(),varcom.oK)
		&& isNE(zrpwIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(zrpwIO.getParams());
			fatalError9000();
		}
		if (isEQ(zrpwIO.getStatuz(),varcom.oK)) {
			return ;
		}
		zrpwIO.setParams(SPACES);
		zrpwIO.setChdrcoy(udtrigrec.chdrcoy);
		zrpwIO.setChdrnum(udtrigrec.chdrnum);
		zrpwIO.setTranno(udtrigrec.tranno);
		zrpwIO.setLife(udtrigrec.life);
		zrpwIO.setCoverage(udtrigrec.coverage);
		zrpwIO.setRider(udtrigrec.rider);
		zrpwIO.setPlanSuffix(udtrigrec.planSuffix);
		zrpwIO.setFunction(varcom.writr);
		zrpwIO.setFormat(formatsInner.zrpwrec);
		SmartFileCode.execute(appVars, zrpwIO);
		if (isNE(zrpwIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zrpwIO.getParams());
			fatalError9000();
		}
	}

protected void a200ReadHitr()
	{
			a210Read();
		}

protected void a210Read()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(udtrigrec.chdrcoy);
		hitrclmIO.setChdrnum(udtrigrec.chdrnum);
		hitrclmIO.setLife(udtrigrec.life);
		hitrclmIO.setCoverage(udtrigrec.coverage);
		hitrclmIO.setRider(udtrigrec.rider);
		hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
		hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
		hitrclmIO.setTranno(udtrigrec.tranno);
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		wsaaBatckey.batcBatccoy.set(hitrclmIO.getBatccoy());
		wsaaBatckey.batcBatcbrn.set(hitrclmIO.getBatcbrn());
		wsaaBatckey.batcBatcactyr.set(hitrclmIO.getBatcactyr());
		wsaaBatckey.batcBatcactmn.set(hitrclmIO.getBatcactmn());
		wsaaBatckey.batcBatctrcde.set(hitrclmIO.getBatctrcde());
		wsaaBatckey.batcBatcbatch.set(hitrclmIO.getBatcbatch());
		ptsdtrgIO.setParams(SPACES);
		/* MOVE BEGNH                  TO PTSDTRG-FUNCTION.     <LA3993>*/
		ptsdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ptsdtrgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptsdtrgIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","PLNSFX","LIFE","COVERAGE","RIDER","VRTFUND");
		
		ptsdtrgIO.setChdrcoy(hitrclmIO.getChdrcoy());
		ptsdtrgIO.setChdrnum(hitrclmIO.getChdrnum());
		ptsdtrgIO.setTranno(hitrclmIO.getTranno());
		ptsdtrgIO.setPlanSuffix(hitrclmIO.getPlanSuffix());
		ptsdtrgIO.setLife(hitrclmIO.getLife());
		ptsdtrgIO.setCoverage(hitrclmIO.getCoverage());
		ptsdtrgIO.setRider(hitrclmIO.getRider());
		ptsdtrgIO.setVirtualFund(hitrclmIO.getZintbfnd());
		ptsdtrgIO.setFormat(formatsInner.ptsdtrgrec);
		SmartFileCode.execute(appVars, ptsdtrgIO);
		if (isNE(ptsdtrgIO.getStatuz(),varcom.oK)
		&& isNE(ptsdtrgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptsdtrgIO.getParams());
			fatalError9000();
		}
		if (isNE(hitrclmIO.getChdrcoy(),ptsdtrgIO.getChdrcoy())
		|| isNE(hitrclmIO.getChdrnum(),ptsdtrgIO.getChdrnum())
		|| isNE(hitrclmIO.getTranno(),ptsdtrgIO.getTranno())
		|| isNE(hitrclmIO.getPlanSuffix(),ptsdtrgIO.getPlanSuffix())
		|| isNE(hitrclmIO.getLife(),ptsdtrgIO.getLife())
		|| isNE(hitrclmIO.getCoverage(),ptsdtrgIO.getCoverage())
		|| isNE(hitrclmIO.getRider(),ptsdtrgIO.getRider())
		|| isNE(hitrclmIO.getZintbfnd(),ptsdtrgIO.getVirtualFund())
		|| isEQ(ptsdtrgIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isEQ(hitrclmIO.getContractAmount(),ZERO)) {
			return ;
		}
		ptsdtrgIO.setEstMatValue(ZERO);
		if (isLTE(hitrclmIO.getContractAmount(),ZERO)) {
			setPrecision(hitrclmIO.getContractAmount(), 2);
			hitrclmIO.setContractAmount(mult(hitrclmIO.getContractAmount(),-1));
		}
		setPrecision(ptsdtrgIO.getActvalue(), 2);
		ptsdtrgIO.setActvalue(add(ptsdtrgIO.getActvalue(),hitrclmIO.getContractAmount()));
		/* MOVE REWRT                  TO PTSDTRG-FUNCTION.     <LA3993>*/
		ptsdtrgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, ptsdtrgIO);
		if (isNE(ptsdtrgIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdtrgIO.getParams());
			fatalError9000();
		}
	}

protected void a300CheckFeedbackIndHitr()
	{
		a310Check();
		a320Continue();
	}

protected void a310Check()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		hitrclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		hitrclmIO.setTranno(udtrigrec.tk2Tranno);
		hitrclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		hitrtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void a320Continue()
	{
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(),varcom.oK)
		&& isNE(hitrclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		if (isNE(udtrigrec.tk2Chdrnum,hitrclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Chdrcoy,hitrclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Tranno,hitrclmIO.getTranno())
		|| isEQ(hitrclmIO.getStatuz(),varcom.endp)) {
			wsaaGeneralFieldsInner.wsaaNotReady.set(" ");
			hitrclmIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(hitrclmIO.getFeedbackInd(),"Y")) {
			wsaaGeneralFieldsInner.wsaaNotReady.set("Y");
		}
		else {
			wsaaGeneralFieldsInner.wsaaNotReady.set(" ");
			hitrclmIO.setFunction(varcom.nextr);
			a320Continue();
			return ;
		}
	}

protected void b100TaxInfo()
	{
		b110Start();
	}

protected void b110Start()
	{
		/* Read table TR52D                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrenqIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrenqIO.getChdrcoy());
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError9000();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void b200ReadTr52e()
	{
		b210Start();
	}

protected void b210Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(ptshclmIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (((isNE(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(wsaaTr52eKey);
			fatalError9000();
		}
		if (((isEQ(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		&& (isEQ(itdmIO.getItemtabl(), tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void b300ProcFeeTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b310Init();
				case b320Call: 
					b320Call();
					b330Proc();
				case b380Next: 
					b380Next();
				case b390Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b310Init()
	{
		ptsdclmIO.setParams(SPACES);
		ptsdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		ptsdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		ptsdclmIO.setTranno(udtrigrec.tk2Tranno);
		ptsdclmIO.setPlanSuffix(ZERO);
		ptsdclmIO.setFunction(varcom.begn);
				}

protected void b320Call()
	{
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(), varcom.oK)
		&& isNE(ptsdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ptsdclmIO.getStatuz());
			syserrrec.params.set(ptsdclmIO.getParams());
			fatalError9000();
		}
		if (isEQ(ptsdclmIO.getStatuz(), varcom.endp)
		|| isNE(ptsdclmIO.getTranno(), udtrigrec.tk2Tranno)
		|| isNE(ptsdclmIO.getChdrnum(), udtrigrec.tk2Chdrnum)
		|| isNE(ptsdclmIO.getChdrcoy(), udtrigrec.tk2Chdrcoy)) {
			ptsdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.b390Exit);
				}
		if (isNE(ptsdclmIO.getFieldType(), "C")) {
			goTo(GotoLabel.b380Next);
		}
	}

protected void b330Proc()
	{
		b400PostFeeTax();
	}

protected void b380Next()
	{
		ptsdclmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.b320Call);
	}

protected void b400PostFeeTax()
	{
		try {
			b410ReadTr52e();
			b420Post();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void b410ReadTr52e()
	{
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
		wsaaTr52eCrtable.set(ptsdclmIO.getCrtable());
		b200ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		/* IF TR52E-TAXIND-05          NOT = 'Y'                        */
		if (isNE(tr52erec.taxind03, "Y")) {
			goTo(GotoLabel.b490Exit);
		}
	}

protected void b420Post()
	{
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.cntTaxInd.set("Y");
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(ptshclmIO.getCurrcd());
		wsaaCntCurr.set(ptshclmIO.getCurrcd());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(ptshclmIO.getEffdate());
		txcalcrec.amountIn.set(ptsdpenIO.getActvalue());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.jrnseq.set(ZERO);
		txcalcrec.tranno.set(udtrigrec.tk2Tranno);
		txcalcrec.transType.set("SURF");
		txcalcrec.batckey.set(wsaaBatckey);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError9000();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotalTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotalTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrenqIO.getChdrcoy());
		taxdIO.setChdrnum(chdrenqIO.getChdrnum());
		taxdIO.setLife(SPACES);
		taxdIO.setCoverage(SPACES);
		taxdIO.setRider(SPACES);
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(ptshclmIO.getEffdate());
		taxdIO.setInstfrom(varcom.vrcmMaxDate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(udtrigrec.tk2Tranno);
		taxdIO.setTrantype("SURF");
		wsaaGeneralFieldsInner.wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaGeneralFieldsInner.wsaaTranref);
		taxdIO.setTranref(wsaaGeneralFieldsInner.wsaaTranref);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			fatalError9000();
		}
	}

protected void b500PostPenaltyTax()
	{
		try {
			b510ReadTr52e();
			b520Post();
			}
			catch (GOTOException e){
			/* Expected exception for control flow purposes. */
			}
		}

protected void b510ReadTr52e()
	{
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
		wsaaTr52eCrtable.set(ptsdsrcIO.getCrtable());
		b200ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		/* IF TR52E-TAXIND-05          NOT = 'Y'                        */
		if (isNE(tr52erec.taxind03, "Y")) {
			goTo(GotoLabel.b590Exit);
		}
	}

protected void b520Post()
	{
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.cntTaxInd.set(SPACES);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(ptsdsrcIO.getLife());
		txcalcrec.coverage.set(ptsdsrcIO.getCoverage());
		txcalcrec.rider.set(ptsdsrcIO.getRider());
		txcalcrec.crtable.set(ptsdsrcIO.getCrtable());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(ptshclmIO.getCurrcd());
		wsaaCntCurr.set(ptshclmIO.getCurrcd());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(ptshclmIO.getEffdate());
		txcalcrec.amountIn.set(wsaaCmpSurChrg);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.jrnseq.set(ZERO);
		txcalcrec.tranno.set(udtrigrec.tk2Tranno);
		txcalcrec.transType.set("SURF");
		txcalcrec.batckey.set(wsaaBatckey);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError9000();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotalTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotalTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrenqIO.getChdrcoy());
		taxdIO.setChdrnum(chdrenqIO.getChdrnum());
		taxdIO.setLife(ptsdsrcIO.getLife());
		taxdIO.setCoverage(ptsdsrcIO.getCoverage());
		taxdIO.setRider(ptsdsrcIO.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(ptshclmIO.getEffdate());
		taxdIO.setInstfrom(varcom.vrcmMaxDate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(udtrigrec.tk2Tranno);
		taxdIO.setTrantype("SURF");
		wsaaGeneralFieldsInner.wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaGeneralFieldsInner.wsaaTranref);
		taxdIO.setTranref(wsaaGeneralFieldsInner.wsaaTranref);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			fatalError9000();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrenqIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(udtrigrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(udtrigrec.chdrnum);
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(chdrenqIO.getCowncoy());
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(udtrigrec.tranno);
		drypDryprcRecInner.drypBillchnl.set(chdrenqIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrenqIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrenqIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrenqIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrenqIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrenqIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrenqIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrenqIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrenqIO.getOccdate());
//		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);C
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
//		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
//		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError9000();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(udtrigrec.chdrcoy);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		/* MOVE WSAA-IN-CASE-OF-ERROR  TO WSAA-TRIGGER.                 */
		if (isEQ(syserrrec.statuz,"BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		/* MOVE BOMB                   TO PARM-STATUZ.          <D60401>*/
		udtrigrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(udtrigrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(ptsdtrgIO.getCurrcd());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData ptsdclmrec = new FixedLengthStringData(10).init("PTSDCLMREC");
	private FixedLengthStringData ptsdtrgrec = new FixedLengthStringData(10).init("PTSDTRGREC");
	private FixedLengthStringData ptsdpenrec = new FixedLengthStringData(10).init("PTSDPENREC");
	private FixedLengthStringData ptsdsrcrec = new FixedLengthStringData(10).init("PTSDSRCREC");
	private FixedLengthStringData ptshclmrec = new FixedLengthStringData(10).init("PTSHCLMREC");
	private FixedLengthStringData zrpwrec = new FixedLengthStringData(10).init("ZRPWREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
/*
 * Class transformed  from Data Structure WSAA-GENERAL-FIELDS--INNER
 */
public static final class WsaaGeneralFieldsInner { 

		/* WSAA-GENERAL-FIELDS */
	public FixedLengthStringData wsaaEndTrans = new FixedLengthStringData(1);
	public Validator endOfTransaction = new Validator(wsaaEndTrans, "Y");

	private FixedLengthStringData wsaaNotReady = new FixedLengthStringData(1);
	public Validator noNeedToPost = new Validator(wsaaNotReady, "Y");

	private FixedLengthStringData wsaa1stTime = new FixedLengthStringData(1);
	private Validator n1stTimeThru = new Validator(wsaa1stTime, "Y");
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPtsdChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30);
	private PackedDecimalData wsaaStoreFee = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(2, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private FixedLengthStringData drypDetailParams1 = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput1 = new FixedLengthStringData(71).isAPartOf(drypDetailParams1, 0);
	private FixedLengthStringData drypCnttype1 = new FixedLengthStringData(3).isAPartOf(drypDetailInput1, 0);
	private FixedLengthStringData drypBillfreq1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 3);
	private FixedLengthStringData drypBillchnl1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 5);
	private FixedLengthStringData drypStatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 7);
	private FixedLengthStringData drypPstatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 9);
	private PackedDecimalData drypBtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 11);
	private PackedDecimalData drypPtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 16);
	private PackedDecimalData drypBillcd1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 21);
	private PackedDecimalData drypOccdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 46);
}
}
