package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:48
 * Description:
 * Copybook name: UTRNCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnclmKey = new FixedLengthStringData(64).isAPartOf(utrnclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnclmChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnclmKey, 0);
  	public FixedLengthStringData utrnclmChdrnum = new FixedLengthStringData(8).isAPartOf(utrnclmKey, 1);
  	public FixedLengthStringData utrnclmCoverage = new FixedLengthStringData(2).isAPartOf(utrnclmKey, 9);
  	public FixedLengthStringData utrnclmRider = new FixedLengthStringData(2).isAPartOf(utrnclmKey, 11);
  	public FixedLengthStringData utrnclmUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnclmKey, 13);
  	public FixedLengthStringData utrnclmUnitType = new FixedLengthStringData(1).isAPartOf(utrnclmKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(utrnclmKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}