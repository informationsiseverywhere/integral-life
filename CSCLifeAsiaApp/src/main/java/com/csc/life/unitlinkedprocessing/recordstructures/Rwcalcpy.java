package com.csc.life.unitlinkedprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:14
 * Description:
 * Copybook name: RWCALCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rwcalcpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData withdrawRec = new FixedLengthStringData(137);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(withdrawRec, 0);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(withdrawRec, 4);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(withdrawRec, 5);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(withdrawRec, 13);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(withdrawRec, 16);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(withdrawRec, 19);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(withdrawRec, 21);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(withdrawRec, 23);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(withdrawRec, 25);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(withdrawRec, 27);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(withdrawRec, 31);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(withdrawRec, 36);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(withdrawRec, 41);
  	public PackedDecimalData convUnits = new PackedDecimalData(8, 0).isAPartOf(withdrawRec, 46);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(withdrawRec, 51);
  	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(withdrawRec, 52);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(withdrawRec, 61);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(withdrawRec, 70);
  	public FixedLengthStringData chdrCurr = new FixedLengthStringData(3).isAPartOf(withdrawRec, 73);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(2).isAPartOf(withdrawRec, 76);
  	public FixedLengthStringData element = new FixedLengthStringData(1).isAPartOf(withdrawRec, 78);
  	public FixedLengthStringData description = new FixedLengthStringData(30).isAPartOf(withdrawRec, 79);
  	public PackedDecimalData singp = new PackedDecimalData(17, 2).isAPartOf(withdrawRec, 109);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(withdrawRec, 118);
  	public FixedLengthStringData type = new FixedLengthStringData(1).isAPartOf(withdrawRec, 120);
  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(withdrawRec, 121);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(withdrawRec, 125);
  	public FixedLengthStringData endf = new FixedLengthStringData(1).isAPartOf(withdrawRec, 129);
  	public FixedLengthStringData neUnits = new FixedLengthStringData(1).isAPartOf(withdrawRec, 130);
  	public FixedLengthStringData tmUnits = new FixedLengthStringData(1).isAPartOf(withdrawRec, 131);
  	public FixedLengthStringData psNotAllwd = new FixedLengthStringData(1).isAPartOf(withdrawRec, 132);
  	public FixedLengthStringData fundCurrcode = new FixedLengthStringData(3).isAPartOf(withdrawRec, 133);
  	public ZonedDecimalData planType = new ZonedDecimalData(1, 0).isAPartOf(withdrawRec, 136).setUnsigned();


	public void initialize() {
		COBOLFunctions.initialize(withdrawRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		withdrawRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}