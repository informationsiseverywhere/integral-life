package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:51
 * Description:
 * Copybook name: UTRNUDDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnuddkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnuddFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnuddKey = new FixedLengthStringData(64).isAPartOf(utrnuddFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnuddChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnuddKey, 0);
  	public FixedLengthStringData utrnuddChdrnum = new FixedLengthStringData(8).isAPartOf(utrnuddKey, 1);
  	public FixedLengthStringData utrnuddLife = new FixedLengthStringData(2).isAPartOf(utrnuddKey, 9);
  	public FixedLengthStringData utrnuddCoverage = new FixedLengthStringData(2).isAPartOf(utrnuddKey, 11);
  	public FixedLengthStringData utrnuddRider = new FixedLengthStringData(2).isAPartOf(utrnuddKey, 13);
  	public PackedDecimalData utrnuddPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnuddKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(utrnuddKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnuddFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnuddFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}