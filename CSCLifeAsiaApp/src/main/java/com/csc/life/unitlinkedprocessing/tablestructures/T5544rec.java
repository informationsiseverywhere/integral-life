package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:45
 * Description:
 * Copybook name: T5544REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5544rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5544Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData btobid = new FixedLengthStringData(1).isAPartOf(t5544Rec, 0);
  	public FixedLengthStringData btodisc = new FixedLengthStringData(1).isAPartOf(t5544Rec, 1);
  	public ZonedDecimalData discountOfferPercent = new ZonedDecimalData(5, 2).isAPartOf(t5544Rec, 2);
  	public FixedLengthStringData bidToFund = new FixedLengthStringData(1).isAPartOf(t5544Rec, 7);
  	public FixedLengthStringData btooff = new FixedLengthStringData(1).isAPartOf(t5544Rec, 8);
  	public ZonedDecimalData cfrwd = new ZonedDecimalData(1, 0).isAPartOf(t5544Rec, 9);
  	public ZonedDecimalData feemax = new ZonedDecimalData(17, 2).isAPartOf(t5544Rec, 10);
  	public ZonedDecimalData feemin = new ZonedDecimalData(17, 2).isAPartOf(t5544Rec, 27);
  	public ZonedDecimalData feepc = new ZonedDecimalData(5, 2).isAPartOf(t5544Rec, 44);
  	public ZonedDecimalData ffamt = new ZonedDecimalData(17, 2).isAPartOf(t5544Rec, 49);
  	public FixedLengthStringData nowDeferInd = new FixedLengthStringData(1).isAPartOf(t5544Rec, 66);
  	public ZonedDecimalData noOfFreeSwitches = new ZonedDecimalData(3, 0).isAPartOf(t5544Rec, 67);
  	public FixedLengthStringData otooff = new FixedLengthStringData(1).isAPartOf(t5544Rec, 70);
  	public ZonedDecimalData ppyears = new ZonedDecimalData(1, 0).isAPartOf(t5544Rec, 71);
  	public FixedLengthStringData filler = new FixedLengthStringData(428).isAPartOf(t5544Rec, 72, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5544Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5544Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}