package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Br607DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br607TempDAO extends BaseDAO<Br607DTO> {
	int buildTempData(Br607DTO dto);
}
