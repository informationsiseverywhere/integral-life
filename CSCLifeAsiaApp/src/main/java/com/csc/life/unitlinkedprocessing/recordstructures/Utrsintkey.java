package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:53
 * Description:
 * Copybook name: UTRSINTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsintkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsintFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrsintKey = new FixedLengthStringData(64).isAPartOf(utrsintFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsintChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsintKey, 0);
  	public FixedLengthStringData utrsintChdrnum = new FixedLengthStringData(8).isAPartOf(utrsintKey, 1);
  	public FixedLengthStringData utrsintLife = new FixedLengthStringData(2).isAPartOf(utrsintKey, 9);
  	public FixedLengthStringData utrsintCoverage = new FixedLengthStringData(2).isAPartOf(utrsintKey, 11);
  	public FixedLengthStringData utrsintRider = new FixedLengthStringData(2).isAPartOf(utrsintKey, 13);
  	public PackedDecimalData utrsintPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsintKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(utrsintKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsintFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsintFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}