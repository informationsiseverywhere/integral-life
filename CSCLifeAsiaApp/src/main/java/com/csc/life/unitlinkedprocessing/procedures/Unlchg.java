/*
 * File: Unlchg.java
 * Date: 30 August 2009 2:49:57
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLCHG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.terminationclaims.recordstructures.Untallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.Incimj2TableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.IncimjaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   UNLCHG - COMPONENT CHANGE - UNIT LINKED GENERIC AT MODULE
*
* OVERVIEW.
* ---------
*
* This program will be called from the AT General Processing
* module for Component Change so that any processing specific
* to Unit Linked components may be carried out.
*
* Process INCI Records:
* The INCI records represent the distribution of the premium, or
* portion of the premium, amongst Initial Units, Accumulation
* units or uninvested portion. They are handled in the same
* way as AGCM records are in P5128AT, with the exception that
* as there is no equivalent of the Commission Split there is
* only an vertical spread of INCI records, not a horizontal
* one where a portion of premium is split between several agents.
* So for an INCI record for a particular component key there will
* only be one record with any given sequence number.
*
*  UNLCHG - Component Change - Unit Linked Generic AT Module.
*  This program will be called from the AT General Processing
*  module for Component Change so that any processing specific
*  to Unit Linked components may be carried out. In this case
*  this will be the processing of INCI records.
*
*  INCI Record Processing.
*  -----------------------
*  Investment details are represented by INCI records. These
*  hold the details of the premium and its distribution
*  amongst the different types of units. When a premium varies
*  over the life of a contract new INCI records will be
*  created that represent the new increased portion. These
*  will have their own Risk Commencement Date. When a decrease
*  has occurred the INCI records will be 'deactivated' and if
*  necessary split into two; one part holding the still active
*  portion of the premium the other showing the now 'inactive'
*  portion. Over the life of a contract then there may be
*  several INCI records representing the current amount of
*  premium and also several, now 'inactive', representing the
*  oldest highest value of the premium. These INCI records
*  will have sequence numbers; number one will be the original
*  and oldest record representing te lowest value the premium
*  ever had and the highest sequence number will be a portion
*  of the premium that represents the highest value that the
*  premium ever reached.
*
*  Where a decrease has occurred the INCI records will be
*  de-activated from the old highest first.
*
*  Where an increase has occurred the old inactive INCI
*  records will be activated from the old lowest sequence
*  number first. Where a record is not being completely
*  activated or de-activated it is split into two.
*
*  Increases:
*
*  Where an increase has occurred the increased portion will
*  be represented by a new INCI record. If an increase is
*  being made where a previous decrease had been in effect the
*  old, 'inactive', INCI records will be activated first and
*  then, if they are insufficient to cover the new increase, a
*  new INCI will be written. Where the increase is less than
*  the old decrease the old 'inactive' INCI record may have to
*  be split into two; the first, with the lower sequence
*  number representing the active portion, and the second,
*  with the higher sequence number representing the still
*  inactive portion of the previous premium level.
*
*  Decreases:
*
*  Where a decrease has occurred the decreased portion will be
*  represented by de-activating existing INCI records from the
*  highest 'active' sequence downwards. Where the decrease is
*  less than a whole INCI record the record will be split into
*  two, the higher sequence number holding the de-activated
*  portion and the lower sequence number holding the now
*  active portion.
*
*  The following example should serve to illustrate this
*  procedure. Including the contract issue there are four
*  changes to the policy: issue, first increase, a decrease
*  below the original amount and an increase to an amount
*  above the recent highest level.
*
*      Risk Commencement     Premium
*           Date
*      1.   1/1/88             100
*      2.   1/1/89             150       (increase)
*      3.   1/7/89              80       (decrease)
*      4.   1/1/90             200       (increase).
*
*  The INCI records will be as follows:
*
*          Seq.  R.C.D.  Premium  Dormant Flag
*      1.   1    1/1/88    100        'N'
*      2.   2    1/1/89     50        'N'
*           1    1/1/88    100        'N'
*      3.   3    1/1/89     50        'Y'
*           2    1/1/88     20        'Y'
*           1    1/1/88     80        'N'
*      4.   4    1/1/90     50        'N'
*           3    1/7/89     50        'N'
*           2    1/7/88     20        'N'
*           1    1/1/88     80        'N'
*
*  At position #2 a new INCI was created to account for the
*  increased portion of 50 pounds with a Risk Commencement
*  Date of 1/1/89, one year after the original R.C.D.
*
*  At position #3 that INCI is now de-activated by setting its
*  DORMANT-FLAG to 'Y'. The original INCI is split into two
*  records, the higher sequence number, (#2), being of 20
*  pounds and inactive, and the lower sequence number, (#1)
*  being for the remaining 80 pounds which is still active.
*  At position #4 the two inactive INCI records are
*  re-activated, and as the new premium is 50 pounds greater
*  than the old highest total premium a new INCI record is
*  created, (#4) which reflects the remainder of the increase
*  of 50 pounds.
*
*  Creating New INCI Records.
*
*  When creating new INCI records data from table T5536 will
*  be required. First read T5540 with the CRTABLE. From there
*  obtain the Basis Select code. Concatenate this code, the
*  Life Assured's sex code and the Transaction code to form a
*  key to read T5537. Match the term and age and select the
*  appropriate Unit Allocation Basis Code. This will be used
*  to read T5536. Note that if no matching record was found on
*  T5537 then attempt to read again with the sex code set as
*  an asterisk.
*
*  T5536 shows the percentage of the premium allocated to
*  buying units and then how much of that will be allocated to
*  Initial Units
*
*  The first premium required at the start of the period is
*  the Current Premium * the number of premiums in the first
*  period. The second premium required at the start of the
*  period is the Current Premium * the number of premiums in
*  the second period and so on. The other percentage figures
*  are placed on the INCI record directly from the table.
*
*  Notes:
*
*  Use of the ISUA-NEW-TRANNO field:
*    IF this field is set to zeros, use CHDRMJA-TRANNO when
*      writing INCI records.
*    IF this field is non-zero, use when writing INCI records.
*
*    This field will only ever be non-zero when we are performing
*     a Unit-linked Component Modify Reversal.
*
*  Add the field SEQNO to the physical file INCIPF. Create a
*  new logical view - INCIMJA which has this field placed
*  immediatley after INCINUM. Make it part of the key, the
*  last field in the key.
*  Tables:
*
*  T5536 - Unit Allocation Basis Details     Key: Unit Allocation  asis
*                                                 code from T5537
*  T5537 - Unit Allocation Basis Select      Key: Basis Select fro  T5540
*                                                 Sex Code
*                                                 Transaction Code
*  T5540 - General Unit Linked Details       Key: CRTABLE
*
*****************************************************************
* </pre>
*/
public class Unlchg extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLCHG";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5537Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBasisSelect = new FixedLengthStringData(3).isAPartOf(wsaaT5537Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Key, 3).init(SPACES);
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5537Key, 4).init(SPACES);
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(13, 2).init(0);
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZ2 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaX2 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaIfIncrease = new FixedLengthStringData(1);
	private Validator wsaaIncrease = new Validator(wsaaIfIncrease, "Y");
	private PackedDecimalData wsaaNofPeriods = new PackedDecimalData(5, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTotalInci = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSplitPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaInstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSingPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaPremiumPaid = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaLastSeqno = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaLastVf2Seqno = new PackedDecimalData(2, 0);
	private ZonedDecimalData wsaaI = new ZonedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaFactor = new PackedDecimalData(18, 16);
	private PackedDecimalData wsaaCurrPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDiscontPrem = new PackedDecimalData(17, 2);
		/* WSAA-ORIGINAL-PREM-TABLE */
	private PackedDecimalData[] wsaaOriginalPremst = PDInittedArray(4, 17, 2);
	private PackedDecimalData[] wsaaOriginalPremcurr = PDInittedArray(4, 17, 2);
	private ZonedDecimalData wsaaUnitAllocAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaCovrRiskAge = new ZonedDecimalData(3, 0);
	private FixedLengthStringData wsaaT5537KeyAfterCont = new FixedLengthStringData(8);
	private PackedDecimalData wsaaAddPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaLessPrem = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaProcessCompleteFlag = new FixedLengthStringData(1);
	private Validator processComplete = new Validator(wsaaProcessCompleteFlag, "Y");

	private FixedLengthStringData wsaaIfSplitReqd = new FixedLengthStringData(1);
	private Validator wsaaSplitNotReqd = new Validator(wsaaIfSplitReqd, "N");
	private Validator wsaaSplitReqd = new Validator(wsaaIfSplitReqd, "Y");

		/* WSAA-T5536-VALUES */
	private FixedLengthStringData wsaaMaxPeriods = new FixedLengthStringData(28);
	private ZonedDecimalData[] wsaaMaxPeriod = ZDArrayPartOfStructure(7, 4, 0, wsaaMaxPeriods, 0);

	private FixedLengthStringData wsaaPcUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaPcUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcUnits, 0);

	private FixedLengthStringData wsaaPcInitUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaPcInitUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcInitUnits, 0);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
	private PackedDecimalData wsaaPremRcvd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
		/* ERRORS */
	private static final String e616 = "E616";
	private static final String e706 = "E706";
	private static final String e707 = "E707";
		/* TABLES */
	private static final String t5540 = "T5540";
	private static final String t5537 = "T5537";
	private static final String t5536 = "T5536";
	private static final String t1693 = "T1693";
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private InciTableDAM inciIO = new InciTableDAM();
	private Incimj2TableDAM incimj2IO = new Incimj2TableDAM();
	private IncimjaTableDAM incimjaIO = new IncimjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T5540rec t5540rec = new T5540rec();
	private T5537rec t5537rec = new T5537rec();
	private T5536rec t5536rec = new T5536rec();
	private T1693rec t1693rec = new T1693rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Isuallrec isuallrec = new Isuallrec();
	private FormatsInner formatsInner = new FormatsInner();
	private ExternalisedRules er = new ExternalisedRules();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont10320, 
		yOrdinate10340, 
		increment10360, 
		xOrdinate10380, 
		increment10385, 
		nextFreq10440, 
		endLoop10450, 
		exit10490, 
		seqLvlTot19120, 
		next19180, 
		exit19190, 
		seqLvlTot19152, 
		next19158, 
		exit19159, 
		exit19290, 
		exit19359, 
		next19362, 
		exit19369, 
		next31800, 
		nextPeriod32200
	}

	public Unlchg() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		isuallrec.statuz.set(varcom.oK);
		obtainInfo10000();
		/* If it is a single premium transaction, no processing required.*/
		if (isNE(wsaaInstprem, 0)) {
			inciProcess19000();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainInfo10000()
	{
		/*PARA*/
		varcom.vrcmTime.set(getCobolTime());
		getFiles10100();
		getT554010200();
		wsaaLastVf2Seqno.set(ZERO);
		/*EXIT*/
	}

protected void getFiles10100()
	{
		para10110();
	}

protected void para10110()
	{
		/* Retrieve contract header details.*/
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setChdrcoy(isuallrec.company);
		chdrmjaIO.setChdrnum(isuallrec.chdrnum);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			dbError8100();
		}
		/* Retrieve LIFE details.*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(isuallrec.company);
		lifemjaIO.setChdrnum(isuallrec.chdrnum);
		lifemjaIO.setLife(isuallrec.life);
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			syserrrec.params.set(lifemjaIO.getParams());
			dbError8100();
		}
		/*  Read Coverage/Rider record.*/
		covrmjaIO.setChdrcoy(isuallrec.company);
		covrmjaIO.setChdrnum(isuallrec.chdrnum);
		covrmjaIO.setLife(isuallrec.life);
		covrmjaIO.setCoverage(isuallrec.coverage);
		covrmjaIO.setRider(isuallrec.rider);
		covrmjaIO.setPlanSuffix(isuallrec.planSuffix);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			dbError8100();
		}
		/* Calculate the premium for use within the processing.*/
		wsaaInstprem.set(covrmjaIO.getInstprem());
		if (isNE(covrmjaIO.getInstprem(), ZERO)) {
			compute(wsaaPremiumPaid, 5).set(mult(covrmjaIO.getInstprem(), isuallrec.freqFactor));
		}
		else {
			wsaaPremiumPaid.set(covrmjaIO.getSingp());
		}
	}

protected void getT554010200()
	{
		para10210();
	}

protected void para10210()
	{
		/*  Read T5540 for general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getDataArea());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t5540))
		|| (isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getDataArea());
			dbError8100();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT553710300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10310();
				case cont10320: 
					cont10320();
				case yOrdinate10340: 
					yOrdinate10340();
				case increment10360: 
					increment10360();
				case xOrdinate10380: 
					xOrdinate10380();
				case increment10385: 
					increment10385();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10310()
	{
		/* Read T5537 for unit allocation basis.*/
		wsaaBasisSelect.set(t5540rec.allbas);
		wsaaTranCode.set(isuallrec.batctrcde);
		wsaaT5537Sex.set(lifemjaIO.getCltsex());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(t5537);
		itemIO.setItemitem(wsaaT5537Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont10320);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaTranCode.fill("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont10320);
		}
		/* If no match is found, try again with the global sex*/
		/* code.*/
		wsaaT5537Sex.fill("*");
		wsaaTranCode.set(isuallrec.batctrcde);
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont10320);
		}
		/* If no match is found, try again with the global transaction*/
		/* code and global sex code.*/
		wsaaT5537Sex.fill("*");
		wsaaTranCode.fill("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
	}

protected void cont10320()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaT5537KeyAfterCont.set(wsaaT5537Key);
		/* If it is an increase calculate the age between effective*/
		/* date (Paid to date) and LIfe commence date*/
		/*     IF WSAA-INCREASE                                            */
		/*        MOVE LIFEMJA-CLTDOB     TO DTC3-INT-DATE-1               */
		/*        MOVE CHDRMJA-PTDATE     TO DTC3-INT-DATE-2               */
		/*        MOVE '01'               TO DTC3-FREQUENCY                */
		/*        CALL 'DATCON3' USING DTC3-DATCON3-REC                    */
		/*        IF DTC3-STATUZ              = O-K                        */
		/* Round up the age.*/
		/*           ADD .99999 DTC3-FREQ-FACTOR                           */
		/*                      GIVING WSAA-UNIT-ALLOC-AGE.                */
		if (wsaaIncrease.isTrue()) {
			calcAge18000();
		}
		datcon3rec.intDate1.set(chdrmjaIO.getPtdate());
		datcon3rec.intDate2.set(covrmjaIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz, varcom.oK)) {
			/* Round up the age.*/
			compute(wsaaCovrRiskAge, 5).set(add(.99999, datcon3rec.freqFactor));
		}
		wsaaZ2.set(1);
		wsaaX2.set(1);
	}

	/**
	* <pre>
	*  Work out the array's Y index based on ANB-AT-CCD.
	* </pre>
	*/
protected void yOrdinate10340()
	{
		wsaaZ.set(0);
		wsaaZ2.subtract(1);
	}

protected void increment10360()
	{
		wsaaZ.add(1);
		wsaaZ2.add(1);
		/*  Read the age continuation item, if all entries in age table*/
		/*  is checked. If no continuation item is specified, terminate*/
		/*  the program with a system-error "No Age cont. Item - T5537".*/
		if (isGT(wsaaZ, 10)
		&& isNE(t5537rec.agecont, SPACES)) {
			readAgecont10500();
			goTo(GotoLabel.yOrdinate10340);
		}
		else {
			if (isGT(wsaaZ, 10)
			&& isEQ(t5537rec.agecont, SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e706);
				dbError8100();
			}
		}
		if (wsaaIncrease.isTrue()) {
			/*       IF WSAA-UNIT-ALLOC-AGE > T5537-TOAGE(WSAA-Z)              */
			if (isGT(wsaaUnitAllocAge, t5537rec.ageIssageTo[wsaaZ.toInt()])) {
				goTo(GotoLabel.increment10360);
			}
		}
		else {
			/*    IF LIFEMJA-ANB-AT-CCD > T5537-TOAGE(WSAA-Z)                  */
			if (isGT(lifemjaIO.getAnbAtCcd(), t5537rec.ageIssageTo[wsaaZ.toInt()])) {
				goTo(GotoLabel.increment10360);
			}
		}
		/* This is performed here to prevent the continuation item (if any)*/
		/*  from being over written with the original*/
		readOriginalT553710700();
	}

	/**
	* <pre>
	*  Work out the array's X index based on TERM.
	* </pre>
	*/
protected void xOrdinate10380()
	{
		/*Read the original record from T5537.*/
		wsaaZ.set(0);
		wsaaX2.subtract(1);
	}

protected void increment10385()
	{
		wsaaZ.add(1);
		wsaaX2.add(1);
		/*  Read the term continuation item, if all entries in term table*/
		/*  is checked. If no continuation item is specified, terminate*/
		/*  the program with a system-error "No Term cont. Item - T5537".*/
		if (isGT(wsaaZ, 9)
		&& isNE(t5537rec.trmcont, SPACES)) {
			readTermcont10600();
			wsaaX2.set(1);
			goTo(GotoLabel.xOrdinate10380);
		}
		else {
			if (isGT(wsaaZ, 9)
			&& isEQ(t5537rec.trmcont, SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e707);
				dbError8100();
			}
		}
		wsaaTerm.set(wsaaCovrRiskAge);
		if (isGT(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])) {
			goTo(GotoLabel.increment10385);
		}
		wsaaX.set(wsaaZ);
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaZ2.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaZ2, 9));
		wsaaIndex.add(wsaaX2);
		/* Make sure to read the right T5537-record (calculateted) from*/
		/* wsaa-z2 and wsaa-x2. To look-up allocation basis it is always*/
		/* the Age continuation Item, that is used.*/
		/* It is not necessary to re-read the original item as the current*/
		/* one is the one on which processing must be done.*/
		while ( !(isLT(wsaaIndex, 100))) {
			readAgecont10500();
		}
		
		wsaaAllocBasis.set(t5537rec.actAllocBasis[wsaaIndex.toInt()]);
	}

protected void getT553610400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10410();
					gotRecord10420();
				case nextFreq10440: 
					nextFreq10440();
				case endLoop10450: 
					endLoop10450();
				case exit10490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10410()
	{
		/* Read T5536 for unit allocation basis details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t5536);
		itdmIO.setItemitem(wsaaAllocBasis);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getDataArea());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t5536))
		|| (isNE(itdmIO.getItemitem(), wsaaAllocBasis))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getDataArea());
			dbError8100();
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	* There are 6 sets of details in the table, each set is led by
	* the billing frequency. So to find the correct set of details,
	* match each of these frequencies with the billing frequency
	* specified in the contract header record.
	* </pre>
	*/
protected void gotRecord10420()
	{
		wsaaZ.set(0);
	}

protected void nextFreq10440()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ, 6)) {
			goTo(GotoLabel.endLoop10450);
		}
		if (isEQ(t5536rec.billfreq[wsaaZ.toInt()], SPACES)) {
			goTo(GotoLabel.nextFreq10440);
		}
		if (isNE(t5536rec.billfreq[wsaaZ.toInt()], chdrmjaIO.getBillfreq())) {
			goTo(GotoLabel.nextFreq10440);
		}
		/* The appropriate allocation basis details found.*/
		if (isEQ(wsaaZ, 1)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodas);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaPcUnits.set(t5536rec.pcUnitsas);
			goTo(GotoLabel.exit10490);
		}
		if (isEQ(wsaaZ, 2)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodbs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsbs);
			wsaaPcUnits.set(t5536rec.pcUnitsbs);
			goTo(GotoLabel.exit10490);
		}
		if (isEQ(wsaaZ, 3)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodcs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitscs);
			wsaaPcUnits.set(t5536rec.pcUnitscs);
			goTo(GotoLabel.exit10490);
		}
		if (isEQ(wsaaZ, 4)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodds);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsds);
			wsaaPcUnits.set(t5536rec.pcUnitsds);
			goTo(GotoLabel.exit10490);
		}
		if (isEQ(wsaaZ, 5)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodes);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitses);
			wsaaPcUnits.set(t5536rec.pcUnitses);
			goTo(GotoLabel.exit10490);
		}
		if (isEQ(wsaaZ, 6)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodfs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsfs);
			wsaaPcUnits.set(t5536rec.pcUnitsfs);
			goTo(GotoLabel.exit10490);
		}
	}

protected void endLoop10450()
	{
		isuallrec.statuz.set(e616);
		exit090();
	}

protected void readAgecont10500()
	{
		readAgecont10510();
	}

protected void readAgecont10510()
	{
		/* Read Age continuation Item.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(t5537);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(t5537rec.agecont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaIndex.subtract(99);
	}

protected void readTermcont10600()
	{
		readTermcont10610();
	}

protected void readTermcont10610()
	{
		/* Read Term continuation Item.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(t5537);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(t5537rec.trmcont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
	}

protected void readOriginalT553710700()
	{
		readOriginalT553710710();
	}

protected void readOriginalT553710710()
	{
		/* Read Original T5537-record.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(t5537);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(wsaaT5537KeyAfterCont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
	}

protected void calcAge18000()
	{
		para18010();
	}

protected void para18010()
	{
		/*  Identify FSU Company                                           */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(isuallrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(isuallrec.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.intDate1.set(lifemjaIO.getCltdob());
		/* The date passed will either be the paid to date for non flex    */
		/* premium contracts or the target from date for flex prems        */
		/*    MOVE CHDRMJA-PTDATE         TO AGEC-INT-DATE-2.      <D9604> */
		agecalcrec.intDate2.set(isuallrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError8000();
		}
		wsaaUnitAllocAge.set(agecalcrec.agerating);
	}

protected void inciProcess19000()
	{
		para19010();
		read19010();
	}

protected void para19010()
	{
		wsaaTotalInci.set(ZERO);
		incimjaIO.setDataArea(SPACES);
		incimjaIO.setStatuz(varcom.oK);
		incimjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		incimjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incimjaIO.setLife(covrmjaIO.getLife());
		incimjaIO.setCoverage(covrmjaIO.getCoverage());
		incimjaIO.setRider(covrmjaIO.getRider());
		incimjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incimjaIO.setInciNum(ZERO);
		incimjaIO.setSeqno(ZERO);
		incimjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		incimjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incimjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
	}

protected void read19010()
	{
		while ( !(isEQ(incimjaIO.getStatuz(), varcom.endp))) {
			totPremium19400();
		}
		
		if (isEQ(wsaaPremiumPaid, wsaaTotalInci)) {
			return ;
		}
		if (isLTE(wsaaPremiumPaid, wsaaTotalInci)) {
			wsaaIfIncrease.set("N");
		}
		else {
			wsaaIfIncrease.set("Y");
		}
		/* You need to know if the change is an increase or decrease to*/
		/*  process T5537*/
		getT553710300();
		//getT553610400();
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models starts
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VALCL") && er.isExternalized(chdrmjaIO.cnttype.toString(), covrmjaIO.crtable.toString())))
		{
			getT553610400();
		}
		else
		{
			Untallrec untallrec = new Untallrec();
			untallrec.untaCompany.set(isuallrec.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(chdrmjaIO.occdate);
			untallrec.untaBillfreq.set(chdrmjaIO.billfreq);	
			
			callProgram("VALCL", untallrec.untallrec,chdrmjaIO);
			
			wsaaMaxPeriod[1].set(untallrec.untaMaxPeriod01);	
			wsaaPcInitUnit[1].set(untallrec.untaPcInitUnit01);
			wsaaPcUnit[1].set(untallrec.untaPcUnit01);	
			
			wsaaMaxPeriod[2].set(untallrec.untaMaxPeriod02);	
			wsaaPcInitUnit[2].set(untallrec.untaPcInitUnit02);
			wsaaPcUnit[2].set(untallrec.untaPcUnit02);
			
			wsaaMaxPeriod[3].set(untallrec.untaMaxPeriod03);	
			wsaaPcInitUnit[3].set(untallrec.untaPcInitUnit03);
			wsaaPcUnit[3].set(untallrec.untaPcUnit03);
			
			wsaaMaxPeriod[4].set(untallrec.untaMaxPeriod04);	
			wsaaPcInitUnit[4].set(untallrec.untaPcInitUnit04);
			wsaaPcUnit[4].set(untallrec.untaPcUnit04);
			
			wsaaMaxPeriod[5].set(untallrec.untaMaxPeriod05);	
			wsaaPcInitUnit[5].set(untallrec.untaPcInitUnit05);
			wsaaPcUnit[5].set(untallrec.untaPcUnit05);
			

			wsaaMaxPeriod[6].set(untallrec.untaMaxPeriod06);	
			wsaaPcInitUnit[6].set(untallrec.untaPcInitUnit06);
			wsaaPcUnit[6].set(untallrec.untaPcUnit06);

			wsaaMaxPeriod[7].set(untallrec.untaMaxPeriod07);	
			wsaaPcInitUnit[7].set(untallrec.untaPcInitUnit07);
			wsaaPcUnit[7].set(untallrec.untaPcUnit07);
		}
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models end
		if (isLTE(wsaaPremiumPaid, wsaaTotalInci)) {
			compute(wsaaLessPrem, 2).set(sub(wsaaTotalInci, wsaaPremiumPaid));
			splitReqd19100();
			resequenceSeqno19200();
			reducePrem20000();
		}
		else {
			compute(wsaaAddPrem, 2).set(sub(wsaaPremiumPaid, wsaaTotalInci));
			splitReqd19150();
			resequenceSeqno19350();
			wsaaIfIncrease.set("Y");
			increasePrem21000();
			if (isGT(wsaaAddPrem, ZERO)) {
				inciDetails30000();
			}
		}
	}

protected void splitReqd19100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupKey19110();
				case seqLvlTot19120: 
					seqLvlTot19120();
				case next19180: 
					next19180();
				case exit19190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey19110()
	{
		wsaaSplitPrem.set(wsaaLessPrem);
		wsaaIfSplitReqd.set("Y");
		incimjaIO.setDataArea(SPACES);
		incimjaIO.setStatuz(varcom.oK);
		incimjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		incimjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incimjaIO.setLife(covrmjaIO.getLife());
		incimjaIO.setCoverage(covrmjaIO.getCoverage());
		incimjaIO.setRider(covrmjaIO.getRider());
		incimjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incimjaIO.setSeqno(99);
		incimjaIO.setInciNum(ZERO);
		incimjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		incimjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incimjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
				
	}

protected void seqLvlTot19120()
	{
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)
		&& isNE(incimjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), incimjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), incimjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), incimjaIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), incimjaIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), incimjaIO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), incimjaIO.getPlanSuffix())
		|| isEQ(incimjaIO.getStatuz(), varcom.endp)) {
			incimjaIO.setStatuz(varcom.endp);
			wsaaIfSplitReqd.set("N");
			goTo(GotoLabel.exit19190);
		}
		/* Descending Read valid flag '1's to determine whether split is*/
		/* necessary. ie. is the reduction exact enough to deactivate*/
		/* a whole INCI.*/
		/* NOTE : If we hit a DORMANT-FLAG 'Y' record, we want to read the*/
		/*        next INCIMJA record not jump out of the section*/
		/*        without reading any more records.*/
		if (isEQ(incimjaIO.getDormantFlag(), "Y")) {
			goTo(GotoLabel.next19180);
		}
		compute(wsaaSplitPrem, 2).set(sub(wsaaSplitPrem, incimjaIO.getCurrPrem()));
		if (isEQ(wsaaSplitPrem, ZERO)) {
			wsaaIfSplitReqd.set("N");
			goTo(GotoLabel.exit19190);
		}
		if (isLT(wsaaSplitPrem, ZERO)) {
			goTo(GotoLabel.exit19190);
		}
	}

protected void next19180()
	{
		incimjaIO.setFunction(varcom.nextr);
		goTo(GotoLabel.seqLvlTot19120);
	}

protected void splitReqd19150()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupKey19151();
				case seqLvlTot19152: 
					seqLvlTot19152();
				case next19158: 
					next19158();
				case exit19159: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey19151()
	{
		wsaaSplitPrem.set(wsaaAddPrem);
		wsaaIfSplitReqd.set("Y");
		incimj2IO.setDataArea(SPACES);
		incimj2IO.setStatuz(varcom.oK);
		incimj2IO.setChdrcoy(chdrmjaIO.getChdrcoy());
		incimj2IO.setChdrnum(chdrmjaIO.getChdrnum());
		incimj2IO.setLife(covrmjaIO.getLife());
		incimj2IO.setCoverage(covrmjaIO.getCoverage());
		incimj2IO.setRider(covrmjaIO.getRider());
		incimj2IO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incimj2IO.setSeqno(ZERO);
		incimj2IO.setInciNum(ZERO);
		incimj2IO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		incimj2IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incimj2IO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
        
	}

protected void seqLvlTot19152()
	{
		SmartFileCode.execute(appVars, incimj2IO);
		if (isNE(incimj2IO.getStatuz(), varcom.oK)
		&& isNE(incimj2IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimj2IO.getParams());
			dbError8100();
		}
		/*   If end of file reached at this point we either have no*/
		/*   DORMANT-FLAG 2 INCI's to reinstate and so we need to write a*/
		/*   new one to cater for the premium increase or else all the*/
		/*   validflag 2 INCI's are not enough to cover the increase so*/
		/*   we are going to reinstate all of them and write a new INCI*/
		/*   aswell. Therefore remember to set WSAA-SPLIT-REQD to 'N'*/
		/*   as in both of these situations no split is required.*/
		if (isNE(chdrmjaIO.getChdrcoy(), incimj2IO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), incimj2IO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), incimj2IO.getLife())
		|| isNE(covrmjaIO.getCoverage(), incimj2IO.getCoverage())
		|| isNE(covrmjaIO.getRider(), incimj2IO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), incimj2IO.getPlanSuffix())
		|| isEQ(incimj2IO.getStatuz(), varcom.endp)) {
			wsaaIfSplitReqd.set("N");
			incimj2IO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit19159);
		}
		/*   Ascending read DORMANT-FLAG 'Y' to determine whether split is*/
		/*   neccessary. I.E Can we cater for the increase by reactivating*/
		/*   a whole INCI.*/
		if (isEQ(incimj2IO.getDormantFlag(), "N")) {
			goTo(GotoLabel.next19158);
		}
		wsaaLastVf2Seqno.set(incimj2IO.getSeqno());
		compute(wsaaSplitPrem, 2).set(sub(wsaaSplitPrem, incimj2IO.getCurrPrem()));
		if (isEQ(wsaaSplitPrem, ZERO)) {
			wsaaIfSplitReqd.set("N");
			goTo(GotoLabel.exit19159);
		}
		if (isLT(wsaaSplitPrem, ZERO)) {
			goTo(GotoLabel.exit19159);
		}
	}

protected void next19158()
	{
		incimj2IO.setFunction(varcom.nextr);
		goTo(GotoLabel.seqLvlTot19152);
	}

protected void resequenceSeqno19200()
	{
		try {
			setupKey19210();
			read19210();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void setupKey19210()
	{
		if (!wsaaSplitReqd.isTrue()) {
			goTo(GotoLabel.exit19290);
		}
		incimjaIO.setDataArea(SPACES);
		incimjaIO.setStatuz(varcom.oK);
		incimjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		incimjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incimjaIO.setLife(covrmjaIO.getLife());
		incimjaIO.setCoverage(covrmjaIO.getCoverage());
		incimjaIO.setRider(covrmjaIO.getRider());
		incimjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incimjaIO.setInciNum(ZERO);
		incimjaIO.setSeqno(99);
		incimjaIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		incimjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incimjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		incimjaIO.setFormat(formatsInner.incimjarec);
	}

protected void read19210()
	{
		while ( !(isEQ(incimjaIO.getStatuz(), varcom.endp))) {
			resequenceDflgY19300();
		}
		
	}

protected void resequenceDflgY19300()
	{
		call19310();
		next19320();
	}

protected void call19310()
	{
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)
		&& isNE(incimjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), incimjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), incimjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), incimjaIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), incimjaIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), incimjaIO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), incimjaIO.getPlanSuffix())
		|| isEQ(incimjaIO.getStatuz(), varcom.endp)) {
			incimjaIO.setStatuz(varcom.endp);
		}
		if (isNE(incimjaIO.getDormantFlag(), "Y")) {
			return ;
		}
		if (isEQ(incimjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		incimjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)
		&& isNE(incimjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		if (wsaaSplitReqd.isTrue()) {
			incimjaIO.setFunction(varcom.delet);
		}
		else {
			incimjaIO.setFunction(varcom.rewrt);
		}
		incimjaIO.setFormat(formatsInner.incimjarec);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		if (wsaaSplitReqd.isTrue()) {
			setPrecision(incimjaIO.getSeqno(), 0);
			incimjaIO.setSeqno(add(incimjaIO.getSeqno(), 1));
			incimjaIO.setFunction(varcom.writr);
		}
		else {
			return ;
		}
		incimjaIO.setFormat(formatsInner.incimjarec);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
	}

protected void next19320()
	{
		incimjaIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void resequenceSeqno19350()
	{
		try {
			setupKey19351();
			setupKey219351();
			read19352();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void setupKey19351()
	{
		if (!wsaaSplitReqd.isTrue()) {
			goTo(GotoLabel.exit19359);
		}
	}

protected void setupKey219351()
	{
		incimjaIO.setDataArea(SPACES);
		incimjaIO.setStatuz(varcom.oK);
		incimjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		incimjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incimjaIO.setLife(covrmjaIO.getLife());
		incimjaIO.setCoverage(covrmjaIO.getCoverage());
		incimjaIO.setRider(covrmjaIO.getRider());
		incimjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incimjaIO.setInciNum(ZERO);
		incimjaIO.setSeqno(99);
		incimjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		incimjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incimjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		incimjaIO.setFormat(formatsInner.incimjarec);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
	}

protected void read19352()
	{
		while ( !(isEQ(incimjaIO.getStatuz(), varcom.endp))) {
			resequenceDflgY19360();
		}
		
	}

protected void resequenceDflgY19360()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call19361();
				case next19362: 
					next19362();
				case exit19369: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call19361()
	{
		if (isNE(chdrmjaIO.getChdrcoy(), incimjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), incimjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), incimjaIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), incimjaIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), incimjaIO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), incimjaIO.getPlanSuffix())
		|| isEQ(incimjaIO.getStatuz(), varcom.endp)) {
			incimjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit19369);
		}
		if (isEQ(incimjaIO.getDormantFlag(), "N")) {
			goTo(GotoLabel.next19362);
		}
		if (isLTE(incimjaIO.getSeqno(), wsaaLastVf2Seqno)) {
			incimjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit19369);
		}
		incimjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		incimjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		setPrecision(incimjaIO.getSeqno(), 0);
		incimjaIO.setSeqno(add(incimjaIO.getSeqno(), 1));
		incimjaIO.setFunction(varcom.writr);
		incimjaIO.setFormat(formatsInner.incimjarec);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
	}

protected void next19362()
	{
		incimjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)
		&& isNE(incimjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
	}

protected void totPremium19400()
	{
		call19410();
	}

protected void call19410()
	{
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)
		&& isNE(incimjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), incimjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), incimjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), incimjaIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), incimjaIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), incimjaIO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), incimjaIO.getPlanSuffix())
		|| isEQ(incimjaIO.getStatuz(), varcom.endp)) {
			incimjaIO.setStatuz(varcom.endp);
			return ;
		}
		incimjaIO.setFunction(varcom.nextr);
		if (isEQ(incimjaIO.getDormantFlag(), "Y")) {
			return ;
		}
		wsaaTotalInci.add(incimjaIO.getCurrPrem());
	}

protected void reducePrem20000()
	{
		setupKey20010();
	}

protected void setupKey20010()
	{
		incimjaIO.setDataArea(SPACES);
		incimjaIO.setStatuz(varcom.oK);
		incimjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		incimjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incimjaIO.setLife(covrmjaIO.getLife());
		incimjaIO.setCoverage(covrmjaIO.getCoverage());
		incimjaIO.setRider(covrmjaIO.getRider());
		incimjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incimjaIO.setSeqno(99);
		incimjaIO.setInciNum(ZERO);
		incimjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		incimjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incimjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		incimjaIO.setFormat(formatsInner.incimjarec);
		while ( !(isEQ(incimjaIO.getStatuz(), varcom.endp))) {
			updateInci20200();
		}
		
	}

protected void updateInci20200()
	{
		call20210();
	}

protected void call20210()
	{
		if (isLTE(wsaaLessPrem, 0)) {
			incimjaIO.setStatuz(varcom.endp);
			return ;
		}
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)
		&& isNE(incimjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), incimjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), incimjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), incimjaIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), incimjaIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), incimjaIO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), incimjaIO.getPlanSuffix())
		|| isEQ(incimjaIO.getStatuz(), varcom.endp)) {
			incimjaIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(incimjaIO.getDormantFlag(), "N")) {
			/*   This is a change to stop the program looping if we begin on*/
			/*     a dormant flg Y record the program will just return to*/
			/*     the previous section and begin the file again*/
			incimjaIO.setFunction(varcom.nextr);
			return ;
		}
		incimjaIO.setFunction(varcom.nextr);
		if (isLT(wsaaLessPrem, incimjaIO.getCurrPrem())) {
			dormantFlagToN20300();
			dormantFlagToY20400();
		}
		else {
			dormantFlagToY20500();
		}
		incimjaIO.setFunction(varcom.nextr);
	}

protected void dormantFlagToN20300()
	{
		setupKey20310();
		factor20310();
		factorEnd20310();
		rewriteVflg120320();
	}

protected void setupKey20310()
	{
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		/*  Calculate the dividing factor. Which is then used to calc.*/
		/*  the new PREMST + PREMCURR fields.*/
		compute(wsaaCurrPrem, 2).set(sub(incimjaIO.getCurrPrem(), wsaaLessPrem));
		compute(wsaaFactor, 17).setRounded(div(wsaaCurrPrem, incimjaIO.getCurrPrem()));
		wsaaI.set(ZERO);
		for (int loopVar1 = 0; !(loopVar1 == 3); loopVar1 += 1){
			factor20310();
		}
	}

protected void factor20310()
	{
		wsaaI.add(1);
		/* Store original values of PREMST & PREMCURR for later*/
		/* computation of discontinued values.*/
		wsaaOriginalPremst[wsaaI.toInt()].set(incimjaIO.getPremst(wsaaI));
		wsaaOriginalPremcurr[wsaaI.toInt()].set(incimjaIO.getPremcurr(wsaaI));
		setPrecision(incimjaIO.getPremst(wsaaI), 17);
		incimjaIO.setPremst(wsaaI, mult(incimjaIO.getPremst(wsaaI), wsaaFactor), true);
		setPrecision(incimjaIO.getPremcurr(wsaaI), 17);
		incimjaIO.setPremcurr(wsaaI, mult(incimjaIO.getPremcurr(wsaaI), wsaaFactor), true);
		zrdecplrec.amountIn.set(incimjaIO.getPremst(wsaaI));
		a000CallRounding();
		incimjaIO.setPremst(wsaaI, zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(incimjaIO.getPremcurr(wsaaI));
		a000CallRounding();
		incimjaIO.setPremcurr(wsaaI, zrdecplrec.amountOut);
	}

protected void factorEnd20310()
	{
		notFlexiblePremiumContract.setTrue();
		readT572940000();
		if (flexiblePremiumContract.isTrue()) {
			flexpremProcess41000();
		}
		zrdecplrec.amountIn.set(wsaaNonInvestPrem);
		a000CallRounding();
		wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		setPrecision(incimjaIO.getCurrPrem(), 2);
		incimjaIO.setCurrPrem(sub(incimjaIO.getCurrPrem(), wsaaLessPrem));
		incimjaIO.setFunction(varcom.rewrt);
	}

protected void rewriteVflg120320()
	{
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void dormantFlagToY20400()
	{
		setupKey20410();
		factor20410();
		factorEnd20410();
	}

protected void setupKey20410()
	{
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		wsaaI.set(ZERO);
		for (int loopVar2 = 0; !(loopVar2 == 3); loopVar2 += 1){
			factor20410();
		}
	}

protected void factor20410()
	{
		wsaaI.add(1);
		/* Because we have performed a READH on the record previously*/
		/* re-written by 20300-DORMANT-FLAG-TO-N to set PREMST & PREMCURR*/
		/* to the correct values for a type '1' record, we need to*/
		/* subtract to reducing amount from thr original amount to*/
		/* obtain the discontinued values.*/
		compute(wsaaDiscontPrem, 2).set(sub(wsaaOriginalPremst[wsaaI.toInt()], incimjaIO.getPremst(wsaaI)));
		incimjaIO.setPremst(wsaaI, wsaaDiscontPrem);
		/*   COMPUTE WSAA-DISCONT-PREM =                                  */
		/*       WSAA-ORIGINAL-PREMCURR(WSAA-I) -                         */
		/*       INCIMJA-PREMCURR(WSAA-I).                                */
		/*   MOVE WSAA-DISCONT-PREM TO INCIMJA-PREMCURR(WSAA-I).          */
		if (flexiblePremiumContract.isTrue()
		&& isNE(incimjaIO.getPremcurr(wsaaI), ZERO)) {
			incimjaIO.setPremcurr(wsaaI, incimjaIO.getPremst(wsaaI));
		}
		else {
			compute(wsaaDiscontPrem, 2).set(sub(wsaaOriginalPremcurr[wsaaI.toInt()], incimjaIO.getPremcurr(wsaaI)));
			incimjaIO.setPremcurr(wsaaI, wsaaDiscontPrem);
		}
	}

protected void factorEnd20410()
	{
		incimjaIO.setCurrPrem(wsaaLessPrem);
		wsaaLessPrem.subtract(incimjaIO.getCurrPrem());
		incimjaIO.setDormantFlag("Y");
		setPrecision(incimjaIO.getSeqno(), 0);
		incimjaIO.setSeqno(add(incimjaIO.getSeqno(), 1));
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.writr);
		/*WRITE-VLDFG2*/
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void dormantFlagToY20500()
	{
		setupKey20510();
		writeVldfg220520();
	}

protected void setupKey20510()
	{
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		incimjaIO.setDormantFlag("Y");
		if (wsaaSplitReqd.isTrue()) {
			setPrecision(incimjaIO.getSeqno(), 0);
			incimjaIO.setSeqno(add(incimjaIO.getSeqno(), 1));
		}
		wsaaLessPrem.subtract(incimjaIO.getCurrPrem());
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.writr);
	}

protected void writeVldfg220520()
	{
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void increasePrem21000()
	{
		setupKey21010();
	}

protected void setupKey21010()
	{
		incimj2IO.setDataArea(SPACES);
		incimj2IO.setStatuz(varcom.oK);
		incimj2IO.setChdrcoy(chdrmjaIO.getChdrcoy());
		incimj2IO.setChdrnum(chdrmjaIO.getChdrnum());
		incimj2IO.setLife(covrmjaIO.getLife());
		incimj2IO.setCoverage(covrmjaIO.getCoverage());
		incimj2IO.setRider(covrmjaIO.getRider());
		incimj2IO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incimj2IO.setSeqno(ZERO);
		incimj2IO.setInciNum(ZERO);
		incimj2IO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		incimj2IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incimj2IO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		wsaaLastSeqno.set(ZERO);
		while ( !(isEQ(incimj2IO.getStatuz(), varcom.endp))) {
			updateInci21200();
		}
		
	}

protected void updateInci21200()
	{
		call21210();
	}

protected void call21210()
	{
		if (isLTE(wsaaAddPrem, ZERO)) {
			incimj2IO.setStatuz(varcom.endp);
			return ;
		}
		incimj2IO.setFormat(formatsInner.incimj2rec);
		SmartFileCode.execute(appVars, incimj2IO);
		if (isNE(incimj2IO.getStatuz(), varcom.oK)
		&& isNE(incimj2IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incimj2IO.getParams());
			dbError8100();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), incimj2IO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), incimj2IO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), incimj2IO.getLife())
		|| isNE(covrmjaIO.getCoverage(), incimj2IO.getCoverage())
		|| isNE(covrmjaIO.getRider(), incimj2IO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), incimj2IO.getPlanSuffix())
		|| isEQ(incimj2IO.getStatuz(), varcom.endp)) {
			incimj2IO.setStatuz(varcom.endp);
			return ;
		}
		incimj2IO.setFunction(varcom.nextr);
		if (isGT(incimj2IO.getSeqno(), wsaaLastSeqno)) {
			wsaaLastSeqno.set(incimj2IO.getSeqno());
		}
		if (isEQ(incimj2IO.getDormantFlag(), "N")) {
			return ;
		}
		if (isGTE(wsaaAddPrem, incimj2IO.getCurrPrem())) {
			dormantFlagToN23000();
			wsaaAddPrem.subtract(incimj2IO.getCurrPrem());
		}
		else {
			dormantFlagToY24000();
			dormantFlagToN25000();
			wsaaAddPrem.set(ZERO);
		}
		incimj2IO.setFunction(varcom.nextr);
	}

protected void dormantFlagToN23000()
	{
		setupKey23010();
		rewriteVldfg123020();
	}

protected void setupKey23010()
	{
		incimj2IO.setFormat(formatsInner.incimj2rec);
		incimj2IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incimj2IO);
		if (isNE(incimj2IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimj2IO.getParams());
			dbError8100();
		}
		incimj2IO.setDormantFlag("N");
		incimj2IO.setFunction(varcom.rewrt);
	}

protected void rewriteVldfg123020()
	{
		SmartFileCode.execute(appVars, incimj2IO);
		if (isNE(incimj2IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimj2IO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void dormantFlagToY24000()
	{
		validflagTo2Para24100();
		factor24200();
		factorEnd24200();
	}

protected void validflagTo2Para24100()
	{
		incimjaIO.setDataArea(SPACES);
		incimjaIO.setChdrcoy(incimj2IO.getChdrcoy());
		incimjaIO.setChdrnum(incimj2IO.getChdrnum());
		incimjaIO.setLife(incimj2IO.getLife());
		incimjaIO.setCoverage(incimj2IO.getCoverage());
		incimjaIO.setRider(incimj2IO.getRider());
		incimjaIO.setPlanSuffix(incimj2IO.getPlanSuffix());
		incimjaIO.setSeqno(incimj2IO.getSeqno());
		incimjaIO.setInciNum(incimj2IO.getInciNum());
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		/*  Calculate the dividing factor which is then used to calc.*/
		/*  the new PREMST and PREMCURR fields.*/
		compute(wsaaCurrPrem, 2).set(sub(incimjaIO.getCurrPrem(), wsaaAddPrem));
		compute(wsaaFactor, 17).setRounded(div(wsaaCurrPrem, incimjaIO.getCurrPrem()));
		wsaaI.set(ZERO);
		for (int loopVar3 = 0; !(loopVar3 == 3); loopVar3 += 1){
			factor24200();
		}
	}

protected void factor24200()
	{
		wsaaI.add(1);
		/*  Store inactive portions of PREMST and PREMCURR for later*/
		/*  computation of active portion values. When writing VALIDFLAG*/
		/*  1 record.*/
		setPrecision(incimjaIO.getPremst(wsaaI), 17);
		incimjaIO.setPremst(wsaaI, mult(incimjaIO.getPremst(wsaaI), wsaaFactor), true);
		setPrecision(incimjaIO.getPremcurr(wsaaI), 17);
		incimjaIO.setPremcurr(wsaaI, mult(incimjaIO.getPremcurr(wsaaI), wsaaFactor), true);
		zrdecplrec.amountIn.set(incimjaIO.getPremst(wsaaI));
		a000CallRounding();
		incimjaIO.setPremst(wsaaI, zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(incimjaIO.getPremcurr(wsaaI));
		a000CallRounding();
		incimjaIO.setPremcurr(wsaaI, zrdecplrec.amountOut);
		wsaaOriginalPremst[wsaaI.toInt()].set(incimjaIO.getPremst(wsaaI));
		wsaaOriginalPremcurr[wsaaI.toInt()].set(incimjaIO.getPremcurr(wsaaI));
	}

protected void factorEnd24200()
	{
		setPrecision(incimjaIO.getCurrPrem(), 2);
		incimjaIO.setCurrPrem(sub(incimjaIO.getCurrPrem(), wsaaAddPrem));
		incimjaIO.setDormantFlag("Y");
		setPrecision(incimjaIO.getSeqno(), 0);
		incimjaIO.setSeqno(add(incimjaIO.getSeqno(), 1));
		incimjaIO.setFunction(varcom.writr);
		/*WRITE-DORMFLAG-Y*/
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		/*VALIDFLAG-TO-2-EXIT*/
	}

protected void dormantFlagToN25000()
	{
		validflagTo1Para25100();
		factor25200();
		factorEnd25200();
	}

protected void validflagTo1Para25100()
	{
		incimjaIO.setDataArea(SPACES);
		incimjaIO.setChdrcoy(incimj2IO.getChdrcoy());
		incimjaIO.setChdrnum(incimj2IO.getChdrnum());
		incimjaIO.setLife(incimj2IO.getLife());
		incimjaIO.setCoverage(incimj2IO.getCoverage());
		incimjaIO.setRider(incimj2IO.getRider());
		incimjaIO.setPlanSuffix(incimj2IO.getPlanSuffix());
		incimjaIO.setSeqno(incimj2IO.getSeqno());
		incimjaIO.setInciNum(incimj2IO.getInciNum());
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		wsaaI.set(ZERO);
		for (int loopVar4 = 0; !(loopVar4 == 3); loopVar4 += 1){
			factor25200();
		}
	}

protected void factor25200()
	{
		wsaaI.add(1);
		/*  Because we have performed a READH on the record previously*/
		/*  re-written by 20300-DORMANT-FLAG-TO-N to set PREMST and PREMCUR*/
		/*  to the correct values for a type '1' record, we need*/
		/*  to subtract a reducing amount from the original amount to*/
		/*  obtain the discounted values.*/
		compute(wsaaDiscontPrem, 2).set(sub(incimjaIO.getPremst(wsaaI), wsaaOriginalPremst[wsaaI.toInt()]));
		incimjaIO.setPremst(wsaaI, wsaaDiscontPrem);
		compute(wsaaDiscontPrem, 2).set(sub(incimjaIO.getPremcurr(wsaaI), wsaaOriginalPremcurr[wsaaI.toInt()]));
		incimjaIO.setPremcurr(wsaaI, wsaaDiscontPrem);
	}

protected void factorEnd25200()
	{
		incimjaIO.setCurrPrem(wsaaAddPrem);
		wsaaAddPrem.subtract(incimjaIO.getCurrPrem());
		incimjaIO.setDormantFlag("N");
		incimjaIO.setFormat(formatsInner.incimjarec);
		incimjaIO.setFunction(varcom.rewrt);
		/*REWRT-VLDFLG1*/
		SmartFileCode.execute(appVars, incimjaIO);
		if (isNE(incimjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incimjaIO.getParams());
			dbError8100();
		}
		/*VALIDFLAG-TO-1-EXIT*/
	}

protected void inciDetails30000()
	{
		/*PARA*/
		initialiseInci31000();
		setUpInciDetails32000();
		writeInci39000();
		/*EXIT*/
	}

protected void initialiseInci31000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para31010();
				case next31800: 
					next31800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para31010()
	{
		inciIO.setChdrcoy(isuallrec.company);
		inciIO.setChdrnum(isuallrec.chdrnum);
		inciIO.setLife(isuallrec.life);
		inciIO.setCoverage(isuallrec.coverage);
		inciIO.setRider(isuallrec.rider);
		inciIO.setPlanSuffix(isuallrec.planSuffix);
		setPrecision(inciIO.getSeqno(), 0);
		inciIO.setSeqno(add(wsaaLastSeqno, 1));
		inciIO.setInciNum("001");
		inciIO.setValidflag("1");
		inciIO.setDormantFlag("N");
		if (isNE(isuallrec.newTranno, ZERO)) {
			inciIO.setTranno(isuallrec.newTranno);
		}
		else {
			inciIO.setTranno(chdrmjaIO.getTranno());
		}
		inciIO.setRcdate(isuallrec.effdate);
		inciIO.setPremCessDate(covrmjaIO.getPremCessDate());
		inciIO.setCrtable(covrmjaIO.getCrtable());
		inciIO.setOrigPrem(wsaaAddPrem);
		inciIO.setCurrPrem(wsaaAddPrem);
		inciIO.setUser(isuallrec.user);
		inciIO.setTermid(isuallrec.termid);
		inciIO.setTransactionDate(isuallrec.transactionDate);
		inciIO.setTransactionTime(isuallrec.transactionTime);
		wsaaZ.set(ZERO);
	}

protected void next31800()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ, 4)) {
			return ;
		}
		inciIO.setPremst(wsaaZ, ZERO);
		inciIO.setPremcurr(wsaaZ, ZERO);
		inciIO.setPcunit(wsaaZ, ZERO);
		inciIO.setUsplitpc(wsaaZ, ZERO);
		goTo(GotoLabel.next31800);
	}

protected void setUpInciDetails32000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para32010();
				case nextPeriod32200: 
					nextPeriod32200();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para32010()
	{
		wsaaZ.set(0);
	}

protected void nextPeriod32200()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ, 7)) {
			return ;
		}
		inciIO.setPcunit(wsaaZ, wsaaPcUnit[wsaaZ.toInt()]);
		inciIO.setUsplitpc(wsaaZ, wsaaPcInitUnit[wsaaZ.toInt()]);
		if (isEQ(wsaaMaxPeriod[wsaaZ.toInt()], 0)) {
			wsaaMaxPeriod[wsaaZ.toInt()].set(9999);
			inciIO.setPremst(wsaaZ, 0);
			inciIO.setPremcurr(wsaaZ, 0);
			goTo(GotoLabel.nextPeriod32200);
		}
		/* Work out the length of the present period.(4 periods in total)*/
		if (isGT(wsaaZ, 1)) {
			compute(wsaaX, 0).set(sub(wsaaZ, 1));
			compute(wsaaNofPeriods, 0).set(sub(wsaaMaxPeriod[wsaaZ.toInt()], wsaaMaxPeriod[wsaaX.toInt()]));
		}
		else {
			wsaaNofPeriods.set(wsaaMaxPeriod[wsaaZ.toInt()]);
		}
		/* Work out the the total premiums to pay for the present period.*/
		/* At this stage this total should be the same as the total*/
		/* premiums needed for the present period since no instalment has*/
		/* been paid yet(not at this point anyway!).*/
		setPrecision(inciIO.getPremcurr(wsaaZ), 2);
		inciIO.setPremcurr(wsaaZ, mult(wsaaAddPrem, wsaaNofPeriods));
		inciIO.setPremst(wsaaZ, inciIO.getPremcurr(wsaaZ));
		goTo(GotoLabel.nextPeriod32200);
	}

protected void writeInci39000()
	{
		/*WRITE-REC*/
		inciIO.setFunction(varcom.writr);
		inciIO.setFormat(formatsInner.incirec);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void readT572940000()
	{
		para40000();
	}

protected void para40000()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(isuallrec.company, SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(chdrmjaIO.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), t5729)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())
		|| isGT(itdmIO.getItmfrm(), chdrmjaIO.getOccdate())
		|| isLT(itdmIO.getItmto(), chdrmjaIO.getOccdate())) {
			return ;
		}
		flexiblePremiumContract.setTrue();
	}

protected void flexpremProcess41000()
	{
		/*PARA*/
		readFpco41001();
		wsaaPremRcvd.set(fpcoIO.getPremRecPer());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3)
		|| isEQ(wsaaPremRcvd, ZERO)); wsaaSub.add(1)){
			calcInciprem41002();
		}
		/*EXIT*/
	}

protected void readFpco41001()
	{
		para41001();
	}

protected void para41001()
	{
		fpcoIO.setDataArea(SPACES);
		fpcoIO.setChdrcoy(covrmjaIO.getChdrcoy());
		fpcoIO.setChdrnum(covrmjaIO.getChdrnum());
		fpcoIO.setLife(covrmjaIO.getLife());
		fpcoIO.setCoverage(covrmjaIO.getCoverage());
		fpcoIO.setRider(covrmjaIO.getRider());
		fpcoIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		fpcoIO.setTargfrom(0);
		fpcoIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		fpcoIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcoIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			dbError8100();
		}
		if (isNE(covrmjaIO.getChdrcoy(), fpcoIO.getChdrcoy())
		&& isNE(covrmjaIO.getChdrnum(), fpcoIO.getChdrnum())
		&& isNE(covrmjaIO.getLife(), fpcoIO.getLife())
		&& isNE(covrmjaIO.getCoverage(), fpcoIO.getCoverage())
		&& isNE(covrmjaIO.getRider(), fpcoIO.getRider())
		&& isNE(covrmjaIO.getPlanSuffix(), fpcoIO.getPlanSuffix())) {
			syserrrec.params.set(fpcoIO.getParams());
			dbError8100();
		}
	}

protected void calcInciprem41002()
	{
		/*PARA*/
		if (isEQ(incimjaIO.getPremcurr(wsaaSub), ZERO)) {
			return ;
		}
		if (isGTE(wsaaPremRcvd, incimjaIO.getPremst(wsaaSub))) {
			incimjaIO.setPremcurr(wsaaSub, ZERO);
			compute(wsaaPremRcvd, 2).set(sub(wsaaPremRcvd, incimjaIO.getPremst(wsaaSub)));
			return ;
		}
		if (isGT(incimjaIO.getPremst(wsaaSub), wsaaPremRcvd)) {
			setPrecision(incimjaIO.getPremcurr(wsaaSub), 2);
			incimjaIO.setPremcurr(wsaaSub, sub(incimjaIO.getPremst(wsaaSub), wsaaPremRcvd));
			wsaaPremRcvd.set(ZERO);
			return ;
		}
		/*EXIT*/
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		db8100();
		dbExit8190();
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(isuallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(isuallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData incirec = new FixedLengthStringData(10).init("INCIREC");
	private FixedLengthStringData incimjarec = new FixedLengthStringData(10).init("INCIMJAREC");
	private FixedLengthStringData incimj2rec = new FixedLengthStringData(10).init("INCIMJ2REC");
}
}
