package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:50
 * Description:
 * Copybook name: UTRNRGWKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnrgwkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnrgwFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnrgwKey = new FixedLengthStringData(64).isAPartOf(utrnrgwFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnrgwChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnrgwKey, 0);
  	public FixedLengthStringData utrnrgwChdrnum = new FixedLengthStringData(8).isAPartOf(utrnrgwKey, 1);
  	public PackedDecimalData utrnrgwTranno = new PackedDecimalData(5, 0).isAPartOf(utrnrgwKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(utrnrgwKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnrgwFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnrgwFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}