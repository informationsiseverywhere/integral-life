package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5691
 * @version 1.0 generated on 30/08/09 06:49
 * @author Quipoz
 */
public class S5691ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData inifeeamn = DD.inifeeamn.copyToZonedDecimal().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,12);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,20);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,28);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,36);
	public ZonedDecimalData perfeeamn = DD.perfeeamn.copyToZonedDecimal().isAPartOf(dataFields,66);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,77);
	public ZonedDecimalData zradmnpc = DD.zradmnpc.copyToZonedDecimal().isAPartOf(dataFields,82);
	public ZonedDecimalData zrtupadpc = DD.zrtupadpc.copyToZonedDecimal().isAPartOf(dataFields,87);
	public ZonedDecimalData zrtupfee = DD.zrtupfee.copyToZonedDecimal().isAPartOf(dataFields,92);
	public ZonedDecimalData advfeepc = DD.advfeepc.copyToZonedDecimal().isAPartOf(dataFields,103);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData inifeeamnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData perfeeamnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData zradmnpcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData zrtupadpcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData zrtupfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData advfeepcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] inifeeamnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] perfeeamnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] zradmnpcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] zrtupadpcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] zrtupfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] advfeepcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5691screenWritten = new LongData(0);
	public LongData S5691protectWritten = new LongData(0);

	public static int[] screenPfInds = new int[] {1, 2, 3, 4, 5, 6, 12, 15, 16, 17, 18, 20, 21, 22, 23, 24, 60, 61, 62};
	
	public boolean hasSubfile() {
		return false;
	}


	public S5691ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(advfeepcOut,new String[] {null, null, null, "12", null, null, null, null, null, null, null, null});
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5691screen.class;
		protectRecord = S5691protect.class;
	}
	
	public int getDataAreaSize()
	{
		return 300;
	}
	

	public int getDataFieldsSize()
	{
		return 108;
	}
	

	public int getErrorIndicatorSize()
	{
		return 48;
	}
	
	public int getOutputFieldSize()
	{
		return 144;
	}
	
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, inifeeamn, perfeeamn, zradmnpc, zrtupfee, zrtupadpc,advfeepc};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, inifeeamnOut, perfeeamnOut, zradmnpcOut, zrtupfeeOut, zrtupadpcOut,advfeepcOut};	
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, inifeeamnErr, perfeeamnErr, zradmnpcErr, zrtupfeeErr, zrtupadpcErr,advfeepcErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {itmfrm, itmto};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {itmfrmDisp, itmtoDisp};
	}
	

	public BaseData[] getscreenDateErrFields()
	{
		return  new BaseData[] {itmfrmErr, itmtoErr};
	}
	

	public static int[] getScreenPfInds()
	{
		return screenPfInds;
	}

}
