/*
 * File: Br501.java
 * Date: 29 August 2009 22:09:24
 * Author: Quipoz Limited
 * 
 * Class transformed from BR501.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.unitlinkedprocessing.reports.Rr501Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*                    Suspense Printing
*    This report shows contracts having money in suspense.
*
*****************************************************************
* </pre>
*/
public class Br501 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rr501Report printerFile = new Rr501Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR501");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaCode = "LP";
	private String wsaaType = "S ";
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String covtlnbrec = "COVTLNBREC";
	private String covrlnbrec = "COVRLNBREC";
	private String acblrec = "ACBLREC";
		/* TABLES */
	private String t1692 = "T1692";
	private String t1693 = "T1693";
	private String t5688 = "T5688";
	private String t5687 = "T5687";
		/* CONTROL-TOTALS */
	private int ct01 = 1;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rr501H01 = new FixedLengthStringData(83);
	private FixedLengthStringData rr501h01O = new FixedLengthStringData(83).isAPartOf(rr501H01, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rr501h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr501h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr501h01O, 11);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr501h01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr501h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr501h01O, 53);

	private FixedLengthStringData rr501D02 = new FixedLengthStringData(80);
	private FixedLengthStringData rr501d02O = new FixedLengthStringData(80).isAPartOf(rr501D02, 0);
	private FixedLengthStringData crcode = new FixedLengthStringData(4).isAPartOf(rr501d02O, 0);
	private FixedLengthStringData descrip02 = new FixedLengthStringData(30).isAPartOf(rr501d02O, 4);
	private FixedLengthStringData statcode02 = new FixedLengthStringData(2).isAPartOf(rr501d02O, 34);
	private FixedLengthStringData crrcd = new FixedLengthStringData(10).isAPartOf(rr501d02O, 36);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(rr501d02O, 46);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2).isAPartOf(rr501d02O, 63);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Rr501D01Inner rr501D01Inner = new Rr501D01Inner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextAcbl2550, 
		exit2550, 
		exit2600, 
		exit2750, 
		nextCovr2850, 
		exit2850
	}

	public Br501() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		rh01Branch.set(bsprIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(bsprIO.getCompany());
		acblIO.setSacscode(wsaaCode);
		acblIO.setSacstyp(wsaaType);
		acblIO.setFormat(acblrec);
		acblIO.setFunction(varcom.begn);
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		while ( !(isEQ(acblIO.getStatuz(),varcom.endp))) {
			readAcblRecords2550();
		}
		
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void readAcblRecords2550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					edit2550();
				case nextAcbl2550: 
					nextAcbl2550();
				case exit2550: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2550()
	{
		/*  Check record is required for processing.*/
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(acblIO.getStatuz());
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(),varcom.endp)
		|| isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			acblIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2550);
		}
		if (isNE(bsprIO.getCompany(),acblIO.getRldgcoy())
		|| isNE(wsaaCode,acblIO.getSacscode())) {
			acblIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2550);
		}
		if (isNE(wsaaCode,acblIO.getSacscode())) {
			acblIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2550);
		}
		if (isNE(wsaaType,acblIO.getSacstyp())) {
			goTo(GotoLabel.nextAcbl2550);
		}
		if (isGTE(acblIO.getSacscurbal(),ZERO)) {
			goTo(GotoLabel.nextAcbl2550);
		}
		obtainChdrDetails2600();
		if (isEQ(chdrlifIO.getStatcode(),"XX")) {
			goTo(GotoLabel.nextAcbl2550);
		}
		/*    IF CHDRLIF-STATCODE          = 'PR'                          */
		if (isEQ(chdrlifIO.getStatcode(),"PS")) {
			obtainCovtlnbDetails2700();
		}
		else {
			obtainCovrlnbDetails2800();
		}
	}

protected void nextAcbl2550()
	{
		acblIO.setFunction(varcom.nextr);
	}

protected void obtainChdrDetails2600()
	{
		try {
			begin2600();
			readTableT56882600();
			writeDetail12600();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begin2600()
	{
		/* Read and hold the CHDRLIF record.*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(acblIO.getRldgcoy());
		chdrlifIO.setChdrnum(subString(acblIO.getRldgacct(), 1, 8));
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			chdrlifIO.setStatcode("XX");
			goTo(GotoLabel.exit2600);
		}
	}

protected void readTableT56882600()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlifIO.getCnttype());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rr501D01Inner.rr501D01.set(SPACES);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			rr501D01Inner.descrip01.fill("?");
		}
		else {
			rr501D01Inner.descrip01.set(descIO.getLongdesc());
		}
	}

protected void writeDetail12600()
	{
		rr501D01Inner.chdrnum.set(chdrlifIO.getChdrnum());
		rr501D01Inner.cnttype.set(chdrlifIO.getCnttype());
		rr501D01Inner.statcode01.set(chdrlifIO.getStatcode());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(chdrlifIO.getOccdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rr501D01Inner.date01.set(datcon1rec.extDate);
		rr501D01Inner.cownnum.set(chdrlifIO.getCownnum());
		rr501D01Inner.agntnum.set(chdrlifIO.getAgntnum());
		rr501D01Inner.billfreq.set(chdrlifIO.getBillfreq());
		wsaaFreq.set(chdrlifIO.getBillfreq());
		rr501D01Inner.billchnl.set(chdrlifIO.getBillchnl());
		rr501D01Inner.cntcurr.set(chdrlifIO.getCntcurr());
		rr501D01Inner.sacscurbal.set(acblIO.getSacscurbal());
		writeDetail12650();
	}

protected void writeDetail12650()
	{
		/*START*/
		/* If first page/overflow - write standard headings*/
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRr501h01(rr501H01, indicArea);
			wsaaOverflow.set("N");
		}
		/*  Write detail, checking for page overflow*/
		printerFile.printRr501d01(rr501D01Inner.rr501D01, indicArea);
		/*EXIT*/
	}

protected void obtainCovtlnbDetails2700()
	{
		/*BEGIN*/
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setGenDate(ZERO);
		covtlnbIO.setGenTime(ZERO);
		covtlnbIO.setRrn(ZERO);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setChdrcoy(bsprIO.getCompany());
		covtlnbIO.setChdrnum(chdrlifIO.getChdrnum());
		covtlnbIO.setFormat(covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
		while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
			readCovtlnbDetails2750();
		}
		
		/*EXIT*/
	}

protected void readCovtlnbDetails2750()
	{
		try {
			begin2750();
			setUpDetails22750();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begin2750()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			/*       GO TO 2700-EXIT                                         */
			goTo(GotoLabel.exit2750);
		}
		if (isNE(bsprIO.getCompany(),covtlnbIO.getChdrcoy())
		|| isNE(chdrlifIO.getChdrnum(),covtlnbIO.getChdrnum())) {
			covtlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2750);
		}
		initialize(rr501D02);
		descIO.setDataArea(SPACES);
		descIO.setDescitem(covtlnbIO.getCrtable());
		readTableT56872900();
	}

protected void setUpDetails22750()
	{
		if (isEQ(wsaaFreq,ZERO)) {
			singp.set(covtlnbIO.getSingp());
		}
		else {
			compute(singp, 2).set(mult(covtlnbIO.getInstprem(),wsaaFreq));
		}
		crcode.set(covtlnbIO.getCrtable());
		statcode02.set(SPACES);
		crrcd.set(SPACES);
		sumins.set(covtlnbIO.getSumins());
		writeDetail22950();
		/*NEXT-COVT*/
		covtlnbIO.setFunction(varcom.nextr);
	}

protected void obtainCovrlnbDetails2800()
	{
		/*BEGIN*/
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setGenDate(ZERO);
		covrlnbIO.setGenTime(ZERO);
		covrlnbIO.setRrn(ZERO);
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setTranno(ZERO);
		covrlnbIO.setChdrcoy(bsprIO.getCompany());
		covrlnbIO.setChdrnum(chdrlifIO.getChdrnum());
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.begn);
		while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
			readCovrlnbDetails2850();
		}
		
		/*EXIT*/
	}

protected void readCovrlnbDetails2850()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin2850();
					accesTableT56872850();
					setUpDetails22850();
				case nextCovr2850: 
					nextCovr2850();
				case exit2850: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin2850()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covrlnbIO.getStatuz(),varcom.endp)) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2850);
		}
		if (isNE(bsprIO.getCompany(),covrlnbIO.getChdrcoy())
		|| isNE(chdrlifIO.getChdrnum(),covrlnbIO.getChdrnum())) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2850);
		}
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			//ilife-3037 starts
			covrlnbIO.setFunction(varcom.nextr);
			// ilife-3037 ends
			goTo(GotoLabel.nextCovr2850);
		}
		initialize(rr501D02);
	}

protected void accesTableT56872850()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescitem(covrlnbIO.getCrtable());
		readTableT56872900();
	}

protected void setUpDetails22850()
	{
		if (isEQ(wsaaFreq,ZERO)) {
			singp.set(covrlnbIO.getSingp());
		}
		else {
			compute(singp, 2).set(mult(covrlnbIO.getInstprem(),wsaaFreq));
		}
		crcode.set(covrlnbIO.getCrtable());
		statcode02.set(covrlnbIO.getStatcode());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrlnbIO.getCrrcd());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		crrcd.set(datcon1rec.extDate);
		sumins.set(covrlnbIO.getSumins());
		writeDetail22950();
	}

protected void nextCovr2850()
	{
		covrlnbIO.setFunction(varcom.nextr);
	}

protected void readTableT56872900()
	{
		start2901();
	}

protected void start2901()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5687);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			descrip02.set(descIO.getLongdesc());
		}
		else {
			descrip02.fill("?");
		}
	}

protected void writeDetail22950()
	{
		/* If first page/overflow - write standard headings*/
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRr501h01(rr501H01, indicArea);
			wsaaOverflow.set("N");
		}
		/*  Write detail, checking for page overflow*/
		printerFile.printRr501d02(rr501D02, indicArea);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure RR501-D01--INNER
 */
private static final class Rr501D01Inner { 

	private FixedLengthStringData rr501D01 = new FixedLengthStringData(93);
	private FixedLengthStringData rr501d01O = new FixedLengthStringData(93).isAPartOf(rr501D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr501d01O, 0);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rr501d01O, 8);
	private FixedLengthStringData descrip01 = new FixedLengthStringData(30).isAPartOf(rr501d01O, 11);
	private FixedLengthStringData statcode01 = new FixedLengthStringData(2).isAPartOf(rr501d01O, 41);
	private FixedLengthStringData date01 = new FixedLengthStringData(10).isAPartOf(rr501d01O, 43);
	private FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(rr501d01O, 53);
	private FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(rr501d01O, 61);
	private FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(rr501d01O, 69);
	private FixedLengthStringData billchnl = new FixedLengthStringData(2).isAPartOf(rr501d01O, 71);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rr501d01O, 73);
	private ZonedDecimalData sacscurbal = new ZonedDecimalData(17, 2).isAPartOf(rr501d01O, 76);
}
}
