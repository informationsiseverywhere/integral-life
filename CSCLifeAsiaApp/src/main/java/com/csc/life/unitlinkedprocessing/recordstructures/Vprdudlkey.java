package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:08
 * Description:
 * Copybook name: VPRDUDLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprdudlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprdudlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprdudlKey = new FixedLengthStringData(256).isAPartOf(vprdudlFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprdudlCompany = new FixedLengthStringData(1).isAPartOf(vprdudlKey, 0);
  	public FixedLengthStringData vprdudlUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprdudlKey, 1);
  	public FixedLengthStringData vprdudlUnitType = new FixedLengthStringData(1).isAPartOf(vprdudlKey, 5);
  	public PackedDecimalData vprdudlEffdate = new PackedDecimalData(8, 0).isAPartOf(vprdudlKey, 6);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(vprdudlKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprdudlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprdudlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}