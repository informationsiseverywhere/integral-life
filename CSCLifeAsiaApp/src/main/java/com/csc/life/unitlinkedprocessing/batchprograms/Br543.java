/*
 * File: Br543.java
 * Date: 29 August 2009 22:18:30
 * Author: Quipoz Limited
 * 
 * Class transformed from BR543.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.Acblbk2TableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.unitlinkedprocessing.reports.Rr543Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This program prints the 'Contract Suspense Non-active policies'
*
*   The records are selected from ACBL file and CHDR file when
*   1. Account balance (ACBL-SACSCURBAL) <> 0
*   2. Sub-account code and type are similar to those on the
*      relevant ITEM on Table T5645.
*   4. Contract status not in-force (CHDR-STATCODE)
*
*   The SQL records are sorted on Contract status, Contract no.
*   and Billing frequency.
*
*   The report is break by Contract status.
*
*   Control totals:
*     01  -  Number of records selected via SQL
*     02  -  Number of pages printed
*
*****************************************************************
* </pre>
*/
public class Br543 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlchdrpf1rs = null;
	private java.sql.PreparedStatement sqlchdrpf1ps = null;
	private java.sql.Connection sqlchdrpf1conn = null;
	private String sqlchdrpf1 = "";
	private Rr543Report printerFile = new Rr543Report();
	private SortFileDAM sortFile = new SortFileDAM("LU00");
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR543");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaDate1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDate1Yy = new FixedLengthStringData(4).isAPartOf(wsaaDate1, 0);
	private FixedLengthStringData wsaaDate1Mm = new FixedLengthStringData(2).isAPartOf(wsaaDate1, 4);
	private FixedLengthStringData wsaaDate1Dd = new FixedLengthStringData(2).isAPartOf(wsaaDate1, 6);

	private FixedLengthStringData wsaaDate2 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaDate2Dd = new FixedLengthStringData(2).isAPartOf(wsaaDate2, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaDate2, 2, FILLER).init("/");
	private FixedLengthStringData wsaaDate2Mm = new FixedLengthStringData(2).isAPartOf(wsaaDate2, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaDate2, 5, FILLER).init("/");
	private FixedLengthStringData wsaaDate2Yy = new FixedLengthStringData(4).isAPartOf(wsaaDate2, 6);
	private ZonedDecimalData wsaaDate3 = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaSinstamt = new ZonedDecimalData(15, 2);

	private FixedLengthStringData wsaaChdrstatus = new FixedLengthStringData(2);
		/*   88  PROPOSAL                 VALUE 'PS'.             <LFA1059>*/
	private Validator proposal = new Validator(wsaaChdrstatus, "PS", "UW");
	private ZonedDecimalData wsaaT5645Index = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaLastContract = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastCurrency = new FixedLengthStringData(3);

		/* WSAA-SUMMARY-TABLE */
	private FixedLengthStringData[] wsaaTotals = FLSInittedArray (45, 20);
	private FixedLengthStringData[] wsaaSubaccount = FLSDArrayPartOfArrayStructure(4, wsaaTotals, 0);
	private FixedLengthStringData[] wsaaCurrency = FLSDArrayPartOfArrayStructure(3, wsaaTotals, 4);
	private PackedDecimalData[] wsaaAmount = PDArrayPartOfArrayStructure(17, 2, wsaaTotals, 7);
	private FixedLengthStringData[] wsaaSacscode = FLSDArrayPartOfArrayStructure(2, wsaaTotals, 16);
	private FixedLengthStringData[] wsaaSacstyp = FLSDArrayPartOfArrayStructure(2, wsaaTotals, 18);
	private ZonedDecimalData wsaaSumIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaIf = new FixedLengthStringData(2).init("IF");
	private FixedLengthStringData wsaaLp = new FixedLengthStringData(2).init("LP");
	private FixedLengthStringData wsaaLastBranch = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaIfPrintSummary = new FixedLengthStringData(1).init("N");
	private Validator wsaaPrintSummary = new Validator(wsaaIfPrintSummary, "Y");

	private FixedLengthStringData wsaaIfFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator wsaaFirstTime = new Validator(wsaaIfFirstTime, "Y");

		/*01  WSAA-SACSCODE-TYPE.                                          */
	private FixedLengthStringData wsaaIfEntryFound = new FixedLengthStringData(1).init("N");
	private Validator wsaaEntryFound = new Validator(wsaaIfEntryFound, "Y");

	private FixedLengthStringData wsaaIfRecordReqd = new FixedLengthStringData(1).init("N");
	private Validator wsaaRecordReqd = new Validator(wsaaIfRecordReqd, "Y");
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler3, 5);

		/*01  NONLPF.
		 COPY DDS-ALL-FORMATS OF NONLPF.*/
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaEofSort = new FixedLengthStringData(1).init("N");
	private Validator endOfSortFile = new Validator(wsaaEofSort, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* FORMATS */
	private static final String covtlnbrec = "COVTLNBREC";
	private static final String acblbk2rec = "ACBLBK2REC";
	private static final String descrec = "DESCREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3623 = "T3623";
	private static final String t5645 = "T5645";
	private static final int ct02 = 2;

	private FixedLengthStringData rr543H01 = new FixedLengthStringData(73);
	private FixedLengthStringData rr543h01O = new FixedLengthStringData(73).isAPartOf(rr543H01, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr543h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr543h01O, 1);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr543h01O, 31);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr543h01O, 41);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr543h01O, 43);

	private FixedLengthStringData rr543H02 = new FixedLengthStringData(32);
	private FixedLengthStringData rr543h02O = new FixedLengthStringData(32).isAPartOf(rr543H02, 0);
	private FixedLengthStringData rh02Statcode = new FixedLengthStringData(2).isAPartOf(rr543h02O, 0);
	private FixedLengthStringData rh02Statdets = new FixedLengthStringData(30).isAPartOf(rr543h02O, 2);

	private FixedLengthStringData rr543D02 = new FixedLengthStringData(24);
	private FixedLengthStringData rr543d02O = new FixedLengthStringData(24).isAPartOf(rr543D02, 0);
	private FixedLengthStringData rd02Sacscode = new FixedLengthStringData(2).isAPartOf(rr543d02O, 0);
	private FixedLengthStringData rd02Sacstyp = new FixedLengthStringData(2).isAPartOf(rr543d02O, 2);
	private FixedLengthStringData rd02Origcurr = new FixedLengthStringData(3).isAPartOf(rr543d02O, 4);
	private ZonedDecimalData rd02Sacscurbal = new ZonedDecimalData(17, 2).isAPartOf(rr543d02O, 7);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Acblbk2TableDAM acblbk2IO = new Acblbk2TableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Rr543D01Inner rr543D01Inner = new Rr543D01Inner();
	private SortRecInner sortRecInner = new SortRecInner();
	private SqlChdrpfInner sqlChdrpfInner = new SqlChdrpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2198, 
		exit2199, 
		nextr2298, 
		exit2299, 
		print3150
	}

	public Br543() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		/* IF BSSC-LANGUAGE             = 'E'                           */
		indOn.setTrue(21);
	}

	/**
	* <pre>
	**** END-IF.                                                      
	* </pre>
	*/
protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		/* MOVE BPRD-DEFAULT-BRANCH    TO DESC-DESCITEM.                */
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		/* MOVE BPRD-DEFAULT-BRANCH    TO RH01-BRANCH.                  */
		rh01Branch.set(bsprIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSumIndex.set(1); !(isGT(wsaaSumIndex,45)); wsaaSumIndex.add(1)){
			initSumTable4100();
		}
		wsaaCompany.set(bsprIO.getCompany());
		sqlchdrpf1 = " SELECT  CHDRCOY, CHDRNUM, COWNNUM, STATCODE, CNTCURR, SINSTAMT06, OCCDATE, BILLFREQ, BILLCURR, CNTBRANCH" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE STATCODE <> ?" +
" AND SERVUNIT = ?" +
" AND CHDRCOY = ?" +
" AND VALIDFLAG <> '2'";
		/*   Run the query*/
		sqlerrorflag = false;
		try {
			sqlchdrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrpf1ps = getAppVars().prepareStatementEmbeded(sqlchdrpf1conn, sqlchdrpf1, "CHDRPF");
			getAppVars().setDBString(sqlchdrpf1ps, 1, wsaaIf);
			getAppVars().setDBString(sqlchdrpf1ps, 2, wsaaLp);
			getAppVars().setDBString(sqlchdrpf1ps, 3, wsaaCompany);
			sqlchdrpf1rs = getAppVars().executeQuery(sqlchdrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		sortFile.openOutput();
		retrieveData2050();
		sortFile.close();
		FileSort fs1 = new FileSort(sortFile);
		fs1.addSortKey(sortRecInner.sortCntbranch, true);
		fs1.addSortKey(sortRecInner.sortStatcode, true);
		fs1.addSortKey(sortRecInner.sortBillfreq, true);
		fs1.addSortKey(sortRecInner.sortChdrnum, true);
		fs1.addSortKey(sortRecInner.sortOrigcurr, true);
		fs1.sort();
		sortFile.openInput();
		retrieveSorted2550();
		sortFile.close();
		/*EXIT*/
	}

protected void retrieveData2050()
	{
		/*RETRIEVAL*/
		wsaaEof.set("N");
		while ( !(endOfFile.isTrue())) {
			releaseToSort2100();
		}
		
		/*EXIT*/
	}

protected void releaseToSort2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					release2101();
				case eof2198: 
					eof2198();
				case exit2199: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void release2101()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlchdrpf1rs)) {
				getAppVars().getDBObject(sqlchdrpf1rs, 1, sqlChdrpfInner.sqlChdrcoy);
				getAppVars().getDBObject(sqlchdrpf1rs, 2, sqlChdrpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlchdrpf1rs, 3, sqlChdrpfInner.sqlCownnum);
				getAppVars().getDBObject(sqlchdrpf1rs, 4, sqlChdrpfInner.sqlStatcode);
				getAppVars().getDBObject(sqlchdrpf1rs, 5, sqlChdrpfInner.sqlCntcurr);
				getAppVars().getDBObject(sqlchdrpf1rs, 6, sqlChdrpfInner.sqlSinstamt06);
				getAppVars().getDBObject(sqlchdrpf1rs, 7, sqlChdrpfInner.sqlOccdate);
				getAppVars().getDBObject(sqlchdrpf1rs, 8, sqlChdrpfInner.sqlBillfreq);
				getAppVars().getDBObject(sqlchdrpf1rs, 9, sqlChdrpfInner.sqlBillcurr);
				getAppVars().getDBObject(sqlchdrpf1rs, 10, sqlChdrpfInner.sqlCntbranch);
			}
			else {
				goTo(GotoLabel.eof2198);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		wsspEdterror.set(varcom.oK);
		acblbk2IO.setParams(SPACES);
		acblbk2IO.setRldgcoy(sqlChdrpfInner.sqlChdrcoy);
		acblbk2IO.setRldgacct(sqlChdrpfInner.sqlChdrnum);
		acblbk2IO.setFormat(acblbk2rec);
		acblbk2IO.setFunction(varcom.begn);
		while ( !(isEQ(acblbk2IO.getStatuz(),varcom.endp))) {
			readThroughAcbl2200();
		}
		
		goTo(GotoLabel.exit2199);
	}

protected void eof2198()
	{
		wsaaEof.set("Y");
		wsspEdterror.set(varcom.endp);
	}

protected void readThroughAcbl2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					acbl2201();
				case nextr2298: 
					nextr2298();
				case exit2299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void acbl2201()
	{
		SmartFileCode.execute(appVars, acblbk2IO);
		if (isNE(acblbk2IO.getStatuz(),varcom.oK)
		&& isNE(acblbk2IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acblbk2IO.getParams());
			syserrrec.statuz.set(acblbk2IO.getStatuz());
			fatalError600();
		}
		if (isEQ(acblbk2IO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2299);
		}
		if (isNE(acblbk2IO.getRldgcoy(), sqlChdrpfInner.sqlChdrcoy)
		|| isNE(acblbk2IO.getRldgacct(), sqlChdrpfInner.sqlChdrnum)) {
			acblbk2IO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2299);
		}
		if (isEQ(acblbk2IO.getSacscurbal(),0)) {
			goTo(GotoLabel.nextr2298);
		}
		wsaaIfRecordReqd.set("N");
		for (wsaaT5645Index.set(1); !(isGT(wsaaT5645Index,15)
		|| wsaaRecordReqd.isTrue()); wsaaT5645Index.add(1)){
			checkIfReqd4300();
		}
		if (!wsaaRecordReqd.isTrue()) {
			goTo(GotoLabel.nextr2298);
		}
		fillValues2300();
	}

protected void nextr2298()
	{
		acblbk2IO.setFunction(varcom.nextr);
	}

protected void fillValues2300()
	{
		fill2301();
	}

protected void fill2301()
	{
		sortRecInner.sortChdrcoy.set(sqlChdrpfInner.sqlChdrcoy);
		sortRecInner.sortChdrnum.set(sqlChdrpfInner.sqlChdrnum);
		sortRecInner.sortCownnum.set(sqlChdrpfInner.sqlCownnum);
		sortRecInner.sortStatcode.set(sqlChdrpfInner.sqlStatcode);
		sortRecInner.sortCntcurr.set(sqlChdrpfInner.sqlCntcurr);
		sortRecInner.sortSinstamt06.set(sqlChdrpfInner.sqlSinstamt06);
		sortRecInner.sortOccdate.set(sqlChdrpfInner.sqlOccdate);
		sortRecInner.sortBillfreq.set(sqlChdrpfInner.sqlBillfreq);
		sortRecInner.sortBillcurr.set(sqlChdrpfInner.sqlBillcurr);
		sortRecInner.sortCntbranch.set(sqlChdrpfInner.sqlCntbranch);
		sortRecInner.sortOrigcurr.set(acblbk2IO.getOrigcurr());
		sortRecInner.sortSacscurbal.set(acblbk2IO.getSacscurbal());
		sortRecInner.sortSacscode.set(acblbk2IO.getSacscode());
		sortRecInner.sortSacstyp.set(acblbk2IO.getSacstyp());
		sortFile.write(sortRecInner.sortRec);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void retrieveSorted2550()
	{
		/*STARTED*/
		wsaaEofSort.set("N");
		while ( !(endOfSortFile.isTrue())) {
			printReport2600();
		}
		
		/*EXIT*/
	}

protected void printReport2600()
	{
			print2601();
		}

protected void print2601()
	{
		sortFile.read(sortRecInner.sortRec);
		if (sortFile.isAtEnd()) {
			wsaaEofSort.set("Y");
			return ;
		}
		chkStatusChg2650();
		rr543D01Inner.rd01Chdrnum.set(sortRecInner.sortChdrnum);
		rr543D01Inner.rd01Cownnum.set(sortRecInner.sortCownnum);
		rr543D01Inner.rd01Freq.set(sortRecInner.sortBillfreq);
		rr543D01Inner.rd01Cntcurr.set(sortRecInner.sortCntcurr);
		wsaaDate3.set(sortRecInner.sortOccdate);
		wsaaDate1.set(wsaaDate3);
		wsaaDate2Dd.set(wsaaDate1Dd);
		wsaaDate2Mm.set(wsaaDate1Mm);
		wsaaDate2Yy.set(wsaaDate1Yy);
		rr543D01Inner.rd01Date.set(wsaaDate2);
		rr543D01Inner.rd01Sacscode.set(sortRecInner.sortSacscode);
		rr543D01Inner.rd01Sacstyp.set(sortRecInner.sortSacstyp);
		rr543D01Inner.rd01Billcurr.set(sortRecInner.sortBillcurr);
		rr543D01Inner.rd01Origcurr.set(sortRecInner.sortOrigcurr);
		wsaaChdrstatus.set(sortRecInner.sortStatcode);
		compute(sortRecInner.sortSacscurbal, 0).set(mult(sortRecInner.sortSacscurbal, -1));
		rr543D01Inner.rd01Sacscurbal.set(sortRecInner.sortSacscurbal);
		if (proposal.isTrue()) {
			getSinstamt2660();
		}
		if (isEQ(sortRecInner.sortBillcurr, sortRecInner.sortCntcurr)) {
			if (proposal.isTrue()) {
				rr543D01Inner.rd01Sinstamt.set(wsaaSinstamt);
			}
			else {
				rr543D01Inner.rd01Sinstamt.set(sortRecInner.sortSinstamt06);
			}
		}
		else {
			if (proposal.isTrue()) {
				conlinkrec.amountIn.set(wsaaSinstamt);
			}
			else {
				conlinkrec.amountIn.set(sortRecInner.sortSinstamt06);
			}
			conlinkrec.currIn.set(sortRecInner.sortCntcurr);
			conlinkrec.currOut.set(sortRecInner.sortBillcurr);
			callXcvrt2800();
			rr543D01Inner.rd01Sinstamt.set(conlinkrec.amountOut);
		}
		writeDetails3200();
	}

protected void chkStatusChg2650()
	{
			start2651();
		}

protected void start2651()
	{
		if (isEQ(rh02Statcode, sortRecInner.sortStatcode)) {
			return ;
		}
		/*  Get description of contract status from table T3623*/
		rh02Statcode.set(sortRecInner.sortStatcode);
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setDesctabl(t3623);
		descIO.setDescitem(sortRecInner.sortStatcode);
		descIO.setItemseq(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			rh02Statdets.set("???");
		}
		else {
			rh02Statdets.set(descIO.getLongdesc());
		}
		if (wsaaFirstTime.isTrue()) {
			wsaaIfFirstTime.set("N");
			return ;
		}
		writeStatusHeader3300();
	}

protected void getSinstamt2660()
	{
		/*START*/
		wsaaSinstamt.set(ZERO);
		covtlnbIO.setDataKey(SPACES);
		covtlnbIO.setChdrcoy(sortRecInner.sortChdrcoy);
		covtlnbIO.setChdrnum(sortRecInner.sortChdrnum);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFormat(covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
		covtlnbIO.setStatuz(varcom.oK);
		while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
			readNextCovt2700();
		}
		
		/*EXIT*/
	}

protected void readNextCovt2700()
	{
		/*START*/
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(sortRecInner.sortChdrcoy, covtlnbIO.getChdrcoy())
		|| isNE(sortRecInner.sortChdrnum, covtlnbIO.getChdrnum())
		|| isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			covtlnbIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaSinstamt.add(covtlnbIO.getInstprem());
		covtlnbIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void callXcvrt2800()
	{
		start2801();
	}

protected void start2801()
	{
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(sortRecInner.sortChdrcoy);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT    TO ZRDP-AMOUNT-IN.                   */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT    TO CLNK-AMOUNT-OUT.                  */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

protected void writeHeader3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3101();
				case print3150: 
					print3150();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3101()
	{
		if (isNE(sortRecInner.sortCntbranch, wsaaLastBranch)) {
			wsaaLastBranch.set(sortRecInner.sortCntbranch);
		}
		else {
			goTo(GotoLabel.print3150);
		}
		rh01Branch.set(wsaaLastBranch);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(wsaaLastBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
	}

protected void print3150()
	{
		if (wsaaPrintSummary.isTrue()) {
			rh01Branch.set(SPACES);
			rh01Branchnm.set("ALL BRANCHES");
		}
		else {
			if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
				rh01Branchnm.fill("?");
			}
			else {
				rh01Branchnm.set(descIO.getLongdesc());
			}
		}
		printerFile.printRr543h01(rr543H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void writeDetails3200()
	{
		start3201();
	}

protected void start3201()
	{
		if (newPageReq.isTrue()) {
			writeHeader3100();
			writeStatusHeader3300();
			wsaaOverflow.set("N");
		}
		if (isNE(sortRecInner.sortCntbranch, wsaaLastBranch)) {
			writeHeader3100();
			writeStatusHeader3300();
			wsaaOverflow.set("N");
		}
		/*  Write detail, checking for page overflow*/
		printerFile.printRr543d01(rr543D01Inner.rr543D01, indicArea);
		wsaaIfEntryFound.set("N");
		for (wsaaSumIndex.set(1); !(isGT(wsaaSumIndex,45)
		|| wsaaEntryFound.isTrue()); wsaaSumIndex.add(1)){
			accumulateTotals4200();
		}
	}

protected void writeStatusHeader3300()
	{
		/*START*/
		printerFile.printRr543h02(rr543H02, indicArea);
		printerFile.printRr543h03(printerRec, indicArea);
		/*EXIT*/
	}

protected void writeEndMessage3400()
	{
		/*START*/
		wsaaIfPrintSummary.set("Y");
		writeHeader3100();
		printerFile.printRr543h04(printerRec, indicArea);
		for (wsaaSumIndex.set(1); !(isGT(wsaaSumIndex,45)
		|| isEQ(wsaaCurrency[wsaaSumIndex.toInt()],SPACES)); wsaaSumIndex.add(1)){
			printSummary3500();
		}
		printerFile.printRr543h05(printerRec, indicArea);
		/*EXIT*/
	}

protected void printSummary3500()
	{
		/*START*/
		rd02Sacscode.set(SPACES);
		rd02Sacstyp.set(SPACES);
		rd02Origcurr.set(SPACES);
		rd02Sacscurbal.set(0);
		rd02Sacscode.set(wsaaSacscode[wsaaSumIndex.toInt()]);
		rd02Sacstyp.set(wsaaSacstyp[wsaaSumIndex.toInt()]);
		rd02Origcurr.set(wsaaCurrency[wsaaSumIndex.toInt()]);
		rd02Sacscurbal.set(wsaaAmount[wsaaSumIndex.toInt()]);
		printerFile.printRr543d02(rr543D02, indicArea);
		/*EXIT*/
	}

protected void initSumTable4100()
	{
		/*START*/
		wsaaCurrency[wsaaSumIndex.toInt()].set(SPACES);
		wsaaAmount[wsaaSumIndex.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void accumulateTotals4200()
	{
		/*START*/
		if (isEQ(wsaaCurrency[wsaaSumIndex.toInt()],SPACES)) {
			wsaaCurrency[wsaaSumIndex.toInt()].set(sortRecInner.sortOrigcurr);
			wsaaSacscode[wsaaSumIndex.toInt()].set(sortRecInner.sortSacscode);
			wsaaSacstyp[wsaaSumIndex.toInt()].set(sortRecInner.sortSacstyp);
			wsaaAmount[wsaaSumIndex.toInt()].set(sortRecInner.sortSacscurbal);
			wsaaIfEntryFound.set("Y");
			return ;
		}
		if (isEQ(wsaaCurrency[wsaaSumIndex.toInt()], sortRecInner.sortOrigcurr)
		&& isEQ(wsaaSacscode[wsaaSumIndex.toInt()], sortRecInner.sortSacscode)
		&& isEQ(wsaaSacstyp[wsaaSumIndex.toInt()], sortRecInner.sortSacstyp)) {
			compute(wsaaAmount[wsaaSumIndex.toInt()], 2).set(add(wsaaAmount[wsaaSumIndex.toInt()], sortRecInner.sortSacscurbal));
			wsaaIfEntryFound.set("Y");
		}
		/*EXIT*/
	}

protected void checkIfReqd4300()
	{
		/*START*/
		if (isEQ(acblbk2IO.getSacscode(),t5645rec.sacscode[wsaaT5645Index.toInt()])
		&& isEQ(acblbk2IO.getSacstyp(),t5645rec.sacstype[wsaaT5645Index.toInt()])) {
			wsaaIfRecordReqd.set("Y");
		}
		/*EXIT*/
	}

protected void finished9000()
	{
		/*CLOSE-FILES*/
		getAppVars().freeDBConnectionIgnoreErr(sqlchdrpf1conn, sqlchdrpf1ps, sqlchdrpf1rs);
		writeEndMessage3400();
		printerFile.close();
		/*EXIT*/
	}

protected void r543SqlError()
	{
		/*R543-CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.statuz.set(sqlStatuz);
		fatalError600();
	}

protected void update3000()
	{
		/*UPDATE*/
		/** Update database records.*/
		/*WRITE-DETAIL*/
		/** If first page/overflow - write standard headings*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		finished9000();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sortRecInner.sortBillcurr);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure SORT-REC--INNER
 */
private static final class SortRecInner { 

	private FixedLengthStringData sortRec = new FixedLengthStringData(57);
	private FixedLengthStringData sortChdrcoy = new FixedLengthStringData(1).isAPartOf(sortRec, 0);
	private FixedLengthStringData sortChdrnum = new FixedLengthStringData(8).isAPartOf(sortRec, 1);
	private FixedLengthStringData sortStatcode = new FixedLengthStringData(2).isAPartOf(sortRec, 9);
	private FixedLengthStringData sortCntcurr = new FixedLengthStringData(3).isAPartOf(sortRec, 11);
	private PackedDecimalData sortSinstamt06 = new PackedDecimalData(13, 2).isAPartOf(sortRec, 14);
	private PackedDecimalData sortOccdate = new PackedDecimalData(8, 0).isAPartOf(sortRec, 21);
	private FixedLengthStringData sortBillfreq = new FixedLengthStringData(2).isAPartOf(sortRec, 26);
	private FixedLengthStringData sortBillcurr = new FixedLengthStringData(3).isAPartOf(sortRec, 28);
	private FixedLengthStringData sortCntbranch = new FixedLengthStringData(2).isAPartOf(sortRec, 31);
	private FixedLengthStringData sortCownnum = new FixedLengthStringData(8).isAPartOf(sortRec, 33);
	private FixedLengthStringData sortOrigcurr = new FixedLengthStringData(3).isAPartOf(sortRec, 41);
	private PackedDecimalData sortSacscurbal = new PackedDecimalData(17, 2).isAPartOf(sortRec, 44);
	private FixedLengthStringData sortSacscode = new FixedLengthStringData(2).isAPartOf(sortRec, 53);
	private FixedLengthStringData sortSacstyp = new FixedLengthStringData(2).isAPartOf(sortRec, 55);
}
/*
 * Class transformed  from Data Structure SQL-CHDRPF--INNER
 */
private static final class SqlChdrpfInner { 

		/* SQL-CHDRPF */
	private FixedLengthStringData sqlChdrrec1 = new FixedLengthStringData(41);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlChdrrec1, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlChdrrec1, 1);
	private FixedLengthStringData sqlCownnum = new FixedLengthStringData(8).isAPartOf(sqlChdrrec1, 9);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlChdrrec1, 17);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlChdrrec1, 19);
	private PackedDecimalData sqlSinstamt06 = new PackedDecimalData(13, 2).isAPartOf(sqlChdrrec1, 22);
	private PackedDecimalData sqlOccdate = new PackedDecimalData(8, 0).isAPartOf(sqlChdrrec1, 29);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlChdrrec1, 34);
	private FixedLengthStringData sqlBillcurr = new FixedLengthStringData(3).isAPartOf(sqlChdrrec1, 36);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlChdrrec1, 39);
}
/*
 * Class transformed  from Data Structure RR543-D01--INNER
 */
private static final class Rr543D01Inner { 

	private FixedLengthStringData rr543D01 = new FixedLengthStringData(75);
	private FixedLengthStringData rr543d01O = new FixedLengthStringData(75).isAPartOf(rr543D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rr543d01O, 0);
	private FixedLengthStringData rd01Cownnum = new FixedLengthStringData(8).isAPartOf(rr543d01O, 8);
	private FixedLengthStringData rd01Freq = new FixedLengthStringData(2).isAPartOf(rr543d01O, 16);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(rr543d01O, 18);
	private FixedLengthStringData rd01Billcurr = new FixedLengthStringData(3).isAPartOf(rr543d01O, 21);
	private ZonedDecimalData rd01Sinstamt = new ZonedDecimalData(17, 2).isAPartOf(rr543d01O, 24);
	private FixedLengthStringData rd01Date = new FixedLengthStringData(10).isAPartOf(rr543d01O, 41);
	private FixedLengthStringData rd01Sacscode = new FixedLengthStringData(2).isAPartOf(rr543d01O, 51);
	private FixedLengthStringData rd01Sacstyp = new FixedLengthStringData(2).isAPartOf(rr543d01O, 53);
	private FixedLengthStringData rd01Origcurr = new FixedLengthStringData(3).isAPartOf(rr543d01O, 55);
	private ZonedDecimalData rd01Sacscurbal = new ZonedDecimalData(17, 2).isAPartOf(rr543d01O, 58);
}
}
