package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5551screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5551ScreenVars sv = (S5551ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5551screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5551ScreenVars screenVars = (S5551ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.ageIssageFrm01.setClassString("");
		screenVars.ageIssageTo01.setClassString("");
		screenVars.ageIssageFrm02.setClassString("");
		screenVars.ageIssageTo02.setClassString("");
		screenVars.ageIssageFrm03.setClassString("");
		screenVars.ageIssageTo03.setClassString("");
		screenVars.ageIssageFrm04.setClassString("");
		screenVars.ageIssageTo04.setClassString("");
		screenVars.ageIssageFrm05.setClassString("");
		screenVars.ageIssageTo05.setClassString("");
		screenVars.ageIssageFrm06.setClassString("");
		screenVars.ageIssageTo06.setClassString("");
		screenVars.ageIssageFrm07.setClassString("");
		screenVars.ageIssageTo07.setClassString("");
		screenVars.ageIssageFrm08.setClassString("");
		screenVars.ageIssageTo08.setClassString("");
		screenVars.premCessageFrom01.setClassString("");
		screenVars.premCessageTo01.setClassString("");
		screenVars.premCessageFrom02.setClassString("");
		screenVars.premCessageTo02.setClassString("");
		screenVars.premCessageFrom03.setClassString("");
		screenVars.premCessageTo03.setClassString("");
		screenVars.premCessageFrom04.setClassString("");
		screenVars.premCessageTo04.setClassString("");
		screenVars.premCessageFrom05.setClassString("");
		screenVars.premCessageTo05.setClassString("");
		screenVars.premCessageFrom06.setClassString("");
		screenVars.premCessageTo06.setClassString("");
		screenVars.premCessageFrom07.setClassString("");
		screenVars.premCessageTo07.setClassString("");
		screenVars.premCessageFrom08.setClassString("");
		screenVars.premCessageTo08.setClassString("");
		screenVars.maturityAgeFrom01.setClassString("");
		screenVars.maturityAgeTo01.setClassString("");
		screenVars.maturityAgeFrom02.setClassString("");
		screenVars.maturityAgeTo02.setClassString("");
		screenVars.maturityAgeFrom03.setClassString("");
		screenVars.maturityAgeTo03.setClassString("");
		screenVars.maturityAgeFrom04.setClassString("");
		screenVars.maturityAgeTo04.setClassString("");
		screenVars.maturityAgeFrom05.setClassString("");
		screenVars.maturityAgeTo05.setClassString("");
		screenVars.maturityAgeFrom06.setClassString("");
		screenVars.maturityAgeTo06.setClassString("");
		screenVars.maturityAgeFrom07.setClassString("");
		screenVars.maturityAgeTo07.setClassString("");
		screenVars.maturityAgeFrom08.setClassString("");
		screenVars.maturityAgeTo08.setClassString("");
		screenVars.eaage.setClassString("");
		screenVars.termIssageFrm01.setClassString("");
		screenVars.termIssageTo01.setClassString("");
		screenVars.termIssageFrm02.setClassString("");
		screenVars.termIssageTo02.setClassString("");
		screenVars.termIssageFrm03.setClassString("");
		screenVars.termIssageTo03.setClassString("");
		screenVars.termIssageFrm04.setClassString("");
		screenVars.termIssageTo04.setClassString("");
		screenVars.termIssageFrm05.setClassString("");
		screenVars.termIssageTo05.setClassString("");
		screenVars.termIssageFrm06.setClassString("");
		screenVars.termIssageTo06.setClassString("");
		screenVars.termIssageFrm07.setClassString("");
		screenVars.termIssageTo07.setClassString("");
		screenVars.termIssageFrm08.setClassString("");
		screenVars.termIssageTo08.setClassString("");
		screenVars.premCesstermFrom01.setClassString("");
		screenVars.premCesstermTo01.setClassString("");
		screenVars.premCesstermFrom02.setClassString("");
		screenVars.premCesstermTo02.setClassString("");
		screenVars.premCesstermFrom03.setClassString("");
		screenVars.premCesstermTo03.setClassString("");
		screenVars.premCesstermFrom04.setClassString("");
		screenVars.premCesstermTo04.setClassString("");
		screenVars.premCesstermFrom05.setClassString("");
		screenVars.premCesstermTo05.setClassString("");
		screenVars.premCesstermFrom06.setClassString("");
		screenVars.premCesstermTo06.setClassString("");
		screenVars.premCesstermFrom07.setClassString("");
		screenVars.premCesstermTo07.setClassString("");
		screenVars.premCesstermFrom08.setClassString("");
		screenVars.premCesstermTo08.setClassString("");
		screenVars.maturityTermFrom01.setClassString("");
		screenVars.maturityTermTo01.setClassString("");
		screenVars.maturityTermFrom02.setClassString("");
		screenVars.maturityTermTo02.setClassString("");
		screenVars.maturityTermFrom03.setClassString("");
		screenVars.maturityTermTo03.setClassString("");
		screenVars.maturityTermFrom04.setClassString("");
		screenVars.maturityTermTo04.setClassString("");
		screenVars.maturityTermFrom05.setClassString("");
		screenVars.maturityTermTo05.setClassString("");
		screenVars.maturityTermFrom06.setClassString("");
		screenVars.maturityTermTo06.setClassString("");
		screenVars.maturityTermFrom07.setClassString("");
		screenVars.maturityTermTo07.setClassString("");
		screenVars.maturityTermFrom08.setClassString("");
		screenVars.maturityTermTo08.setClassString("");
		screenVars.rsvflg.setClassString("");
		screenVars.sumInsMin.setClassString("");
		screenVars.mortcls01.setClassString("");
		screenVars.mortcls02.setClassString("");
		screenVars.mortcls03.setClassString("");
		screenVars.mortcls04.setClassString("");
		screenVars.mortcls05.setClassString("");
		screenVars.mortcls06.setClassString("");
		screenVars.sumInsMax.setClassString("");
		screenVars.liencd01.setClassString("");
		screenVars.liencd02.setClassString("");
		screenVars.liencd03.setClassString("");
		screenVars.liencd04.setClassString("");
		screenVars.liencd05.setClassString("");
		screenVars.liencd06.setClassString("");
		screenVars.minLumpSum.setClassString("");
		screenVars.maxLumpSum.setClassString("");
		screenVars.alfnds.setClassString("");
		screenVars.specind.setClassString("");
		screenVars.svcmeth.setClassString("");
	}

/**
 * Clear all the variables in S5551screen
 */
	public static void clear(VarModel pv) {
		S5551ScreenVars screenVars = (S5551ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.ageIssageFrm01.clear();
		screenVars.ageIssageTo01.clear();
		screenVars.ageIssageFrm02.clear();
		screenVars.ageIssageTo02.clear();
		screenVars.ageIssageFrm03.clear();
		screenVars.ageIssageTo03.clear();
		screenVars.ageIssageFrm04.clear();
		screenVars.ageIssageTo04.clear();
		screenVars.ageIssageFrm05.clear();
		screenVars.ageIssageTo05.clear();
		screenVars.ageIssageFrm06.clear();
		screenVars.ageIssageTo06.clear();
		screenVars.ageIssageFrm07.clear();
		screenVars.ageIssageTo07.clear();
		screenVars.ageIssageFrm08.clear();
		screenVars.ageIssageTo08.clear();
		screenVars.premCessageFrom01.clear();
		screenVars.premCessageTo01.clear();
		screenVars.premCessageFrom02.clear();
		screenVars.premCessageTo02.clear();
		screenVars.premCessageFrom03.clear();
		screenVars.premCessageTo03.clear();
		screenVars.premCessageFrom04.clear();
		screenVars.premCessageTo04.clear();
		screenVars.premCessageFrom05.clear();
		screenVars.premCessageTo05.clear();
		screenVars.premCessageFrom06.clear();
		screenVars.premCessageTo06.clear();
		screenVars.premCessageFrom07.clear();
		screenVars.premCessageTo07.clear();
		screenVars.premCessageFrom08.clear();
		screenVars.premCessageTo08.clear();
		screenVars.maturityAgeFrom01.clear();
		screenVars.maturityAgeTo01.clear();
		screenVars.maturityAgeFrom02.clear();
		screenVars.maturityAgeTo02.clear();
		screenVars.maturityAgeFrom03.clear();
		screenVars.maturityAgeTo03.clear();
		screenVars.maturityAgeFrom04.clear();
		screenVars.maturityAgeTo04.clear();
		screenVars.maturityAgeFrom05.clear();
		screenVars.maturityAgeTo05.clear();
		screenVars.maturityAgeFrom06.clear();
		screenVars.maturityAgeTo06.clear();
		screenVars.maturityAgeFrom07.clear();
		screenVars.maturityAgeTo07.clear();
		screenVars.maturityAgeFrom08.clear();
		screenVars.maturityAgeTo08.clear();
		screenVars.eaage.clear();
		screenVars.termIssageFrm01.clear();
		screenVars.termIssageTo01.clear();
		screenVars.termIssageFrm02.clear();
		screenVars.termIssageTo02.clear();
		screenVars.termIssageFrm03.clear();
		screenVars.termIssageTo03.clear();
		screenVars.termIssageFrm04.clear();
		screenVars.termIssageTo04.clear();
		screenVars.termIssageFrm05.clear();
		screenVars.termIssageTo05.clear();
		screenVars.termIssageFrm06.clear();
		screenVars.termIssageTo06.clear();
		screenVars.termIssageFrm07.clear();
		screenVars.termIssageTo07.clear();
		screenVars.termIssageFrm08.clear();
		screenVars.termIssageTo08.clear();
		screenVars.premCesstermFrom01.clear();
		screenVars.premCesstermTo01.clear();
		screenVars.premCesstermFrom02.clear();
		screenVars.premCesstermTo02.clear();
		screenVars.premCesstermFrom03.clear();
		screenVars.premCesstermTo03.clear();
		screenVars.premCesstermFrom04.clear();
		screenVars.premCesstermTo04.clear();
		screenVars.premCesstermFrom05.clear();
		screenVars.premCesstermTo05.clear();
		screenVars.premCesstermFrom06.clear();
		screenVars.premCesstermTo06.clear();
		screenVars.premCesstermFrom07.clear();
		screenVars.premCesstermTo07.clear();
		screenVars.premCesstermFrom08.clear();
		screenVars.premCesstermTo08.clear();
		screenVars.maturityTermFrom01.clear();
		screenVars.maturityTermTo01.clear();
		screenVars.maturityTermFrom02.clear();
		screenVars.maturityTermTo02.clear();
		screenVars.maturityTermFrom03.clear();
		screenVars.maturityTermTo03.clear();
		screenVars.maturityTermFrom04.clear();
		screenVars.maturityTermTo04.clear();
		screenVars.maturityTermFrom05.clear();
		screenVars.maturityTermTo05.clear();
		screenVars.maturityTermFrom06.clear();
		screenVars.maturityTermTo06.clear();
		screenVars.maturityTermFrom07.clear();
		screenVars.maturityTermTo07.clear();
		screenVars.maturityTermFrom08.clear();
		screenVars.maturityTermTo08.clear();
		screenVars.rsvflg.clear();
		screenVars.sumInsMin.clear();
		screenVars.mortcls01.clear();
		screenVars.mortcls02.clear();
		screenVars.mortcls03.clear();
		screenVars.mortcls04.clear();
		screenVars.mortcls05.clear();
		screenVars.mortcls06.clear();
		screenVars.sumInsMax.clear();
		screenVars.liencd01.clear();
		screenVars.liencd02.clear();
		screenVars.liencd03.clear();
		screenVars.liencd04.clear();
		screenVars.liencd05.clear();
		screenVars.liencd06.clear();
		screenVars.minLumpSum.clear();
		screenVars.maxLumpSum.clear();
		screenVars.alfnds.clear();
		screenVars.specind.clear();
		screenVars.svcmeth.clear();
	}
}
