package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:22
 * Description:
 * Copybook name: ZRPWKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrpwkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrpwFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrpwKey = new FixedLengthStringData(64).isAPartOf(zrpwFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrpwChdrcoy = new FixedLengthStringData(1).isAPartOf(zrpwKey, 0);
  	public FixedLengthStringData zrpwChdrnum = new FixedLengthStringData(8).isAPartOf(zrpwKey, 1);
  	public PackedDecimalData zrpwTranno = new PackedDecimalData(5, 0).isAPartOf(zrpwKey, 9);
  	public FixedLengthStringData zrpwLife = new FixedLengthStringData(2).isAPartOf(zrpwKey, 12);
  	public FixedLengthStringData zrpwCoverage = new FixedLengthStringData(2).isAPartOf(zrpwKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(zrpwKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrpwFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrpwFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}