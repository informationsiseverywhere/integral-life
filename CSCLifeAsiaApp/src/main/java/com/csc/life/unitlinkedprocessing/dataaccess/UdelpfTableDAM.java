package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UdelpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:39
 * Class transformed from UDELPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UdelpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 258;
	public FixedLengthStringData udelrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData udelpfRecord = udelrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(udelrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(udelrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(udelrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(udelrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(udelrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(udelrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(udelrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(udelrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(udelrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(udelrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(udelrec);
	public PackedDecimalData jobnoPrice = DD.jctljnumpr.copy().isAPartOf(udelrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(udelrec);
	public FixedLengthStringData unitSubAccount = DD.unitsa.copy().isAPartOf(udelrec);
	public PackedDecimalData strpdate = DD.strpdate.copy().isAPartOf(udelrec);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(udelrec);
	public PackedDecimalData nofUnits = DD.nofunt.copy().isAPartOf(udelrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(udelrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(udelrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(udelrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(udelrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(udelrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(udelrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(udelrec);
	public PackedDecimalData priceDateUsed = DD.pricedt.copy().isAPartOf(udelrec);
	public PackedDecimalData moniesDate = DD.moniesdt.copy().isAPartOf(udelrec);
	public PackedDecimalData priceUsed = DD.priceused.copy().isAPartOf(udelrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(udelrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(udelrec);
	public PackedDecimalData nofDunits = DD.nofdunt.copy().isAPartOf(udelrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(udelrec);
	public PackedDecimalData unitBarePrice = DD.ubrepr.copy().isAPartOf(udelrec);
	public PackedDecimalData inciNum = DD.incinum.copy().isAPartOf(udelrec);
	public PackedDecimalData inciPerd = DD.inciperd.copy().isAPartOf(udelrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(udelrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(udelrec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(udelrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(udelrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(udelrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(udelrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(udelrec);
	public FixedLengthStringData genlcde = DD.genlcde.copy().isAPartOf(udelrec);
	public FixedLengthStringData allInd = DD.allind.copy().isAPartOf(udelrec);
	public FixedLengthStringData contractType = DD.contyp.copy().isAPartOf(udelrec);
	public FixedLengthStringData triggerModule = DD.triger.copy().isAPartOf(udelrec);
	public FixedLengthStringData triggerKey = DD.trigky.copy().isAPartOf(udelrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(udelrec);
	public FixedLengthStringData switchIndicator = DD.swchind.copy().isAPartOf(udelrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(udelrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(udelrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(udelrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UdelpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UdelpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UdelpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UdelpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UdelpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UdelpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UdelpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UDELPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"JCTLJNUMPR, " +
							"VRTFND, " +
							"UNITSA, " +
							"STRPDATE, " +
							"NDFIND, " +
							"NOFUNT, " +
							"UNITYP, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"PRICEDT, " +
							"MONIESDT, " +
							"PRICEUSED, " +
							"CRTABLE, " +
							"CNTCURR, " +
							"NOFDUNT, " +
							"FDBKIND, " +
							"UBREPR, " +
							"INCINUM, " +
							"INCIPERD, " +
							"USTMNO, " +
							"CNTAMNT, " +
							"FNDCURR, " +
							"FUNDAMNT, " +
							"FUNDRATE, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"GENLCDE, " +
							"ALLIND, " +
							"CONTYP, " +
							"TRIGER, " +
							"TRIGKY, " +
							"PRCSEQ, " +
							"SWCHIND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     jobnoPrice,
                                     unitVirtualFund,
                                     unitSubAccount,
                                     strpdate,
                                     nowDeferInd,
                                     nofUnits,
                                     unitType,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     priceDateUsed,
                                     moniesDate,
                                     priceUsed,
                                     crtable,
                                     cntcurr,
                                     nofDunits,
                                     feedbackInd,
                                     unitBarePrice,
                                     inciNum,
                                     inciPerd,
                                     ustmno,
                                     contractAmount,
                                     fundCurrency,
                                     fundAmount,
                                     fundRate,
                                     sacscode,
                                     sacstyp,
                                     genlcde,
                                     allInd,
                                     contractType,
                                     triggerModule,
                                     triggerKey,
                                     procSeqNo,
                                     switchIndicator,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		jobnoPrice.clear();
  		unitVirtualFund.clear();
  		unitSubAccount.clear();
  		strpdate.clear();
  		nowDeferInd.clear();
  		nofUnits.clear();
  		unitType.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		priceDateUsed.clear();
  		moniesDate.clear();
  		priceUsed.clear();
  		crtable.clear();
  		cntcurr.clear();
  		nofDunits.clear();
  		feedbackInd.clear();
  		unitBarePrice.clear();
  		inciNum.clear();
  		inciPerd.clear();
  		ustmno.clear();
  		contractAmount.clear();
  		fundCurrency.clear();
  		fundAmount.clear();
  		fundRate.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		genlcde.clear();
  		allInd.clear();
  		contractType.clear();
  		triggerModule.clear();
  		triggerKey.clear();
  		procSeqNo.clear();
  		switchIndicator.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUdelrec() {
  		return udelrec;
	}

	public FixedLengthStringData getUdelpfRecord() {
  		return udelpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUdelrec(what);
	}

	public void setUdelrec(Object what) {
  		this.udelrec.set(what);
	}

	public void setUdelpfRecord(Object what) {
  		this.udelpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(udelrec.getLength());
		result.set(udelrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}