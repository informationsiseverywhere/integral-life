package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UlnkpfDAOImpl extends BaseDAOImpl<Ulnkpf> implements UlnkpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UlnkpfDAOImpl.class);

	public Map<String, List<Ulnkpf>> searchUlnkrnlRecordByChdrnum(List<String> chdrnumList) {
		StringBuilder sqlUlnkSelect1 = new StringBuilder(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,UALFND01,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,PRCAMTIND,CURRTO,PTOPUP,PLNSFX,FUNDPOOL01,FUNDPOOL02,FUNDPOOL03,FUNDPOOL04,FUNDPOOL05,FUNDPOOL06,FUNDPOOL07,FUNDPOOL08,FUNDPOOL09,FUNDPOOL10 ");
		sqlUlnkSelect1.append("FROM ULNKRNL WHERE ");
		sqlUlnkSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlUlnkSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		
		PreparedStatement psUlnkSelect = getPrepareStatement(sqlUlnkSelect1.toString());
		ResultSet sqlulnkpf1rs = null;
		Map<String, List<Ulnkpf>> resultMap = new HashMap<>();
		try {
			sqlulnkpf1rs = executeQuery(psUlnkSelect);
			while (sqlulnkpf1rs.next()) {
				Ulnkpf u = new Ulnkpf();
	
				u.setUniqueNumber(sqlulnkpf1rs.getLong("UNIQUE_NUMBER"));
				u.setChdrcoy(sqlulnkpf1rs.getString("Chdrcoy"));
				u.setChdrnum(sqlulnkpf1rs.getString("Chdrnum"));
				u.setLife(sqlulnkpf1rs.getString("Life"));
				u.setJlife(sqlulnkpf1rs.getString("Jlife"));
				u.setCoverage(sqlulnkpf1rs.getString("Coverage"));
				u.setRider(sqlulnkpf1rs.getString("Rider"));
				u.setUnitAllocFund01(sqlulnkpf1rs.getString("Ualfnd01"));
				u.setUnitAllocFund02(sqlulnkpf1rs.getString("Ualfnd02"));
				u.setUnitAllocFund03(sqlulnkpf1rs.getString("Ualfnd03"));
				u.setUnitAllocFund04(sqlulnkpf1rs.getString("Ualfnd04"));
				u.setUnitAllocFund05(sqlulnkpf1rs.getString("Ualfnd05"));
				u.setUnitAllocFund06(sqlulnkpf1rs.getString("Ualfnd06"));
				u.setUnitAllocFund07(sqlulnkpf1rs.getString("Ualfnd07"));
				u.setUnitAllocFund08(sqlulnkpf1rs.getString("Ualfnd08"));
				u.setUnitAllocFund09(sqlulnkpf1rs.getString("Ualfnd09"));
				u.setUnitAllocFund10(sqlulnkpf1rs.getString("Ualfnd10"));
				u.setUnitAllocPercAmt01(sqlulnkpf1rs.getBigDecimal("Ualprc01"));
				u.setUnitAllocPercAmt02(sqlulnkpf1rs.getBigDecimal("Ualprc02"));
				u.setUnitAllocPercAmt03(sqlulnkpf1rs.getBigDecimal("Ualprc03"));
				u.setUnitAllocPercAmt04(sqlulnkpf1rs.getBigDecimal("Ualprc04"));
				u.setUnitAllocPercAmt05(sqlulnkpf1rs.getBigDecimal("Ualprc05"));
				u.setUnitAllocPercAmt06(sqlulnkpf1rs.getBigDecimal("Ualprc06"));
				u.setUnitAllocPercAmt07(sqlulnkpf1rs.getBigDecimal("Ualprc07"));
				u.setUnitAllocPercAmt08(sqlulnkpf1rs.getBigDecimal("Ualprc08"));
				u.setUnitAllocPercAmt09(sqlulnkpf1rs.getBigDecimal("Ualprc09"));
				u.setUnitAllocPercAmt10(sqlulnkpf1rs.getBigDecimal("Ualprc10"));
				
				//san
				
				u.setFundPool01(sqlulnkpf1rs.getString("FUNDPOOL01"));
				u.setFundPool02(sqlulnkpf1rs.getString("FUNDPOOL02"));
				u.setFundPool03(sqlulnkpf1rs.getString("FUNDPOOL03"));
				u.setFundPool04(sqlulnkpf1rs.getString("FUNDPOOL04"));
				u.setFundPool05(sqlulnkpf1rs.getString("FUNDPOOL05"));
				u.setFundPool06(sqlulnkpf1rs.getString("FUNDPOOL06"));
				u.setFundPool07(sqlulnkpf1rs.getString("FUNDPOOL07"));
				u.setFundPool08(sqlulnkpf1rs.getString("FUNDPOOL08"));
				u.setFundPool09(sqlulnkpf1rs.getString("FUNDPOOL09"));
				u.setFundPool10(sqlulnkpf1rs.getString("FUNDPOOL10"));
				//san
				u.setPercOrAmntInd(sqlulnkpf1rs.getString("Prcamtind"));
				u.setCurrto(sqlulnkpf1rs.getInt("Currto"));
				u.setPremTopupInd(sqlulnkpf1rs.getString("Ptopup"));
				u.setPlanSuffix(sqlulnkpf1rs.getInt("Plnsfx"));
				if (resultMap.containsKey(u.getChdrnum())) {
					resultMap.get(u.getChdrnum()).add(u);
				} else {
					List<Ulnkpf> ulnkpfList = new ArrayList<>();
					ulnkpfList.add(u);
					resultMap.put(u.getChdrnum(), ulnkpfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchUlnkrnlRecordByChdrnum()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psUlnkSelect, sqlulnkpf1rs);
		}
		return resultMap;

	}
	public int checkUlnkpf(Ulnkpf ulnkpf){
		int intRetVal = 0;
		StringBuilder sql = new StringBuilder("SELECT  COUNT(*) FROM ULNKPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, ulnkpf.getChdrcoy());
			ps.setString(2, ulnkpf.getChdrnum());
			rs = ps.executeQuery();
			while(rs.next()){
				intRetVal = rs.getInt(1);
				
			}
		} catch (SQLException e) {
			LOGGER.error("readUlnkpf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,rs);			
		}	
		
		return intRetVal;
		

	}
	//ILIFE-5140
	public List<Ulnkpf> getUlnkpfByChdrnum(Ulnkpf ulnkpf){
		StringBuilder sql = new StringBuilder("SELECT * FROM ULNKPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG=? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Ulnkpf> ulnkpfList = new ArrayList<Ulnkpf>(); 
		try{
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, ulnkpf.getChdrcoy());
			ps.setString(2, ulnkpf.getChdrnum());
			ps.setString(3, "1");
			rs = ps.executeQuery();
			while(rs.next()){
				Ulnkpf ulnkpfdata = new Ulnkpf();
				ulnkpfdata.setUnitAllocFund01(rs.getString("Ualfnd01"));
				ulnkpfdata.setUnitAllocFund02(rs.getString("Ualfnd02"));
				ulnkpfdata.setUnitAllocFund03(rs.getString("Ualfnd03"));
				ulnkpfdata.setUnitAllocFund04(rs.getString("Ualfnd04"));
				ulnkpfdata.setUnitAllocFund05(rs.getString("Ualfnd05"));
				ulnkpfdata.setUnitAllocFund06(rs.getString("Ualfnd06"));
				ulnkpfdata.setUnitAllocFund07(rs.getString("Ualfnd07"));
				ulnkpfdata.setUnitAllocFund08(rs.getString("Ualfnd08"));
				ulnkpfdata.setUnitAllocFund09(rs.getString("Ualfnd09"));
				ulnkpfdata.setUnitAllocFund10(rs.getString("Ualfnd10"));
				
				ulnkpfdata.setUnitAllocPercAmt01(rs.getBigDecimal("Ualprc01"));
				ulnkpfdata.setUnitAllocPercAmt02(rs.getBigDecimal("Ualprc02"));
				ulnkpfdata.setUnitAllocPercAmt03(rs.getBigDecimal("Ualprc03"));
				ulnkpfdata.setUnitAllocPercAmt04(rs.getBigDecimal("Ualprc04"));
				ulnkpfdata.setUnitAllocPercAmt05(rs.getBigDecimal("Ualprc05"));
				ulnkpfdata.setUnitAllocPercAmt06(rs.getBigDecimal("Ualprc06"));
				ulnkpfdata.setUnitAllocPercAmt07(rs.getBigDecimal("Ualprc07"));
				ulnkpfdata.setUnitAllocPercAmt08(rs.getBigDecimal("Ualprc08"));
				ulnkpfdata.setUnitAllocPercAmt09(rs.getBigDecimal("Ualprc09"));
				ulnkpfdata.setUnitAllocPercAmt10(rs.getBigDecimal("Ualprc10"));
				ulnkpfList.add(ulnkpfdata);
			}
		} catch (SQLException e) {
			LOGGER.error("getUlnkpfByChdrnum()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,rs);			
		}	
		return ulnkpfList;
	}//ILIFE-5140
	
	public List<Ulnkpf> searchUlnkrevRecord(String coy, String chdrnum) {
		StringBuilder sqlUlnkSelect1 = new StringBuilder(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,UALFND01,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,PRCAMTIND,CURRTO,PTOPUP,PLNSFX ");
		sqlUlnkSelect1.append(" FROM ULNKREV WHERE CHDRCOY=? and CHDRNUM=? ");
		sqlUlnkSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC,TRANNO ASC, UNIQUE_NUMBER ASC ");
		PreparedStatement psUlnkSelect = getPrepareStatement(sqlUlnkSelect1.toString());
		ResultSet sqlulnkpf1rs = null;
		List<Ulnkpf> ulnkpfList = new ArrayList<>();
		try {
			psUlnkSelect.setString(1, coy);
			psUlnkSelect.setString(2, chdrnum);
			sqlulnkpf1rs = executeQuery(psUlnkSelect);
			while (sqlulnkpf1rs.next()) {
				Ulnkpf u = new Ulnkpf();
	
				u.setUniqueNumber(sqlulnkpf1rs.getLong("UNIQUE_NUMBER"));
				u.setChdrcoy(sqlulnkpf1rs.getString("Chdrcoy"));
				u.setChdrnum(sqlulnkpf1rs.getString("Chdrnum"));
				u.setLife(sqlulnkpf1rs.getString("Life"));
				u.setJlife(sqlulnkpf1rs.getString("Jlife"));
				u.setCoverage(sqlulnkpf1rs.getString("Coverage"));
				u.setRider(sqlulnkpf1rs.getString("Rider"));
				u.setUnitAllocFund01(sqlulnkpf1rs.getString("Ualfnd01"));
				u.setUnitAllocFund02(sqlulnkpf1rs.getString("Ualfnd02"));
				u.setUnitAllocFund03(sqlulnkpf1rs.getString("Ualfnd03"));
				u.setUnitAllocFund04(sqlulnkpf1rs.getString("Ualfnd04"));
				u.setUnitAllocFund05(sqlulnkpf1rs.getString("Ualfnd05"));
				u.setUnitAllocFund06(sqlulnkpf1rs.getString("Ualfnd06"));
				u.setUnitAllocFund07(sqlulnkpf1rs.getString("Ualfnd07"));
				u.setUnitAllocFund08(sqlulnkpf1rs.getString("Ualfnd08"));
				u.setUnitAllocFund09(sqlulnkpf1rs.getString("Ualfnd09"));
				u.setUnitAllocFund10(sqlulnkpf1rs.getString("Ualfnd10"));
				u.setUnitAllocPercAmt01(sqlulnkpf1rs.getBigDecimal("Ualprc01"));
				u.setUnitAllocPercAmt02(sqlulnkpf1rs.getBigDecimal("Ualprc02"));
				u.setUnitAllocPercAmt03(sqlulnkpf1rs.getBigDecimal("Ualprc03"));
				u.setUnitAllocPercAmt04(sqlulnkpf1rs.getBigDecimal("Ualprc04"));
				u.setUnitAllocPercAmt05(sqlulnkpf1rs.getBigDecimal("Ualprc05"));
				u.setUnitAllocPercAmt06(sqlulnkpf1rs.getBigDecimal("Ualprc06"));
				u.setUnitAllocPercAmt07(sqlulnkpf1rs.getBigDecimal("Ualprc07"));
				u.setUnitAllocPercAmt08(sqlulnkpf1rs.getBigDecimal("Ualprc08"));
				u.setUnitAllocPercAmt09(sqlulnkpf1rs.getBigDecimal("Ualprc09"));
				u.setUnitAllocPercAmt10(sqlulnkpf1rs.getBigDecimal("Ualprc10"));
				u.setPercOrAmntInd(sqlulnkpf1rs.getString("Prcamtind"));
				u.setCurrto(sqlulnkpf1rs.getInt("Currto"));
				u.setPremTopupInd(sqlulnkpf1rs.getString("Ptopup"));
				u.setPlanSuffix(sqlulnkpf1rs.getInt("Plnsfx"));
				ulnkpfList.add(u);
			}
		} catch (SQLException e) {
			LOGGER.error("searchUlnkrevRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psUlnkSelect, sqlulnkpf1rs);
		}
		return ulnkpfList;
	}
	
	public void deleteUlnkpfRecord(List<Ulnkpf> ulnkpfList){
		String sql = "DELETE FROM ULNKPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Ulnkpf l:ulnkpfList){
				ps.setLong(1, l.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteUlnkpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public int isExistUlnk(String chdrcoy, String chdrnum){
		int intRetVal = 0;
		StringBuilder sql = new StringBuilder("SELECT  COUNT(*) FROM ULNKPF WHERE PTOPUP=' ' AND  CHDRCOY=? AND CHDRNUM=?"); //ILIFE-8421
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum.trim());//ILIFE-8421
			rs = ps.executeQuery();
			if(rs.next()){
				intRetVal = rs.getInt(1);
				
			}
		} catch (SQLException e) {
			LOGGER.error("isExistUlnk()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,rs);			
		}	
		
		return intRetVal;
		

	}
 //ILIFE-8786 start
	@Override
	public Ulnkpf searchUlnk(String chdrcoy, String chdrnum, String life, String jlife, String coverage, String rider, int plnsfx) {
		Ulnkpf ulnkpf = null;
		PreparedStatement psUlnkSelect = null;
		ResultSet sqlulnkpf1rs = null;
		StringBuilder sqlUlnkSelectBuilder = new StringBuilder();

		sqlUlnkSelectBuilder.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,UALFND01,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,PRCAMTIND,CURRTO,PTOPUP,PLNSFX ");
		sqlUlnkSelectBuilder.append(" FROM ULNKPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE=? AND JLIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?");
		sqlUlnkSelectBuilder.append(" ORDER BY ");
		sqlUlnkSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		psUlnkSelect = getPrepareStatement(sqlUlnkSelectBuilder.toString());
		
		try {
			psUlnkSelect.setString(1, chdrcoy);
			psUlnkSelect.setString(2, chdrnum);
			psUlnkSelect.setString(3, life);
			psUlnkSelect.setString(4, jlife);
			psUlnkSelect.setString(5, coverage);
			psUlnkSelect.setString(6, rider);
			psUlnkSelect.setInt(7, plnsfx);
			sqlulnkpf1rs = executeQuery(psUlnkSelect);
			while (sqlulnkpf1rs.next()) {
				ulnkpf = new Ulnkpf();
				
				ulnkpf.setUniqueNumber(sqlulnkpf1rs.getLong("UNIQUE_NUMBER"));
				ulnkpf.setChdrcoy(sqlulnkpf1rs.getString("Chdrcoy"));
				ulnkpf.setChdrnum(sqlulnkpf1rs.getString("Chdrnum"));
				ulnkpf.setLife(sqlulnkpf1rs.getString("Life"));
				ulnkpf.setJlife(sqlulnkpf1rs.getString("Jlife"));
				ulnkpf.setCoverage(sqlulnkpf1rs.getString("Coverage"));
				ulnkpf.setRider(sqlulnkpf1rs.getString("Rider"));
				ulnkpf.setUnitAllocFund01(sqlulnkpf1rs.getString("Ualfnd01"));
				ulnkpf.setUnitAllocFund02(sqlulnkpf1rs.getString("Ualfnd02"));
				ulnkpf.setUnitAllocFund03(sqlulnkpf1rs.getString("Ualfnd03"));
				ulnkpf.setUnitAllocFund04(sqlulnkpf1rs.getString("Ualfnd04"));
				ulnkpf.setUnitAllocFund05(sqlulnkpf1rs.getString("Ualfnd05"));
				ulnkpf.setUnitAllocFund06(sqlulnkpf1rs.getString("Ualfnd06"));
				ulnkpf.setUnitAllocFund07(sqlulnkpf1rs.getString("Ualfnd07"));
				ulnkpf.setUnitAllocFund08(sqlulnkpf1rs.getString("Ualfnd08"));
				ulnkpf.setUnitAllocFund09(sqlulnkpf1rs.getString("Ualfnd09"));
				ulnkpf.setUnitAllocFund10(sqlulnkpf1rs.getString("Ualfnd10"));
				ulnkpf.setUnitAllocPercAmt01(sqlulnkpf1rs.getBigDecimal("Ualprc01"));
				ulnkpf.setUnitAllocPercAmt02(sqlulnkpf1rs.getBigDecimal("Ualprc02"));
				ulnkpf.setUnitAllocPercAmt03(sqlulnkpf1rs.getBigDecimal("Ualprc03"));
				ulnkpf.setUnitAllocPercAmt04(sqlulnkpf1rs.getBigDecimal("Ualprc04"));
				ulnkpf.setUnitAllocPercAmt05(sqlulnkpf1rs.getBigDecimal("Ualprc05"));
				ulnkpf.setUnitAllocPercAmt06(sqlulnkpf1rs.getBigDecimal("Ualprc06"));
				ulnkpf.setUnitAllocPercAmt07(sqlulnkpf1rs.getBigDecimal("Ualprc07"));
				ulnkpf.setUnitAllocPercAmt08(sqlulnkpf1rs.getBigDecimal("Ualprc08"));
				ulnkpf.setUnitAllocPercAmt09(sqlulnkpf1rs.getBigDecimal("Ualprc09"));
				ulnkpf.setUnitAllocPercAmt10(sqlulnkpf1rs.getBigDecimal("Ualprc10"));
				ulnkpf.setPercOrAmntInd(sqlulnkpf1rs.getString("Prcamtind"));
				ulnkpf.setCurrto(sqlulnkpf1rs.getInt("Currto"));
				ulnkpf.setPremTopupInd(sqlulnkpf1rs.getString("Ptopup"));
				ulnkpf.setPlanSuffix(sqlulnkpf1rs.getInt("Plnsfx"));
			}
		}catch (SQLException e) {
			LOGGER.error("searchUlnk()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(psUlnkSelect, sqlulnkpf1rs);
		}
		return ulnkpf;
	}
 //ILIFE-8786 end
}