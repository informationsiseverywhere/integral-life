/*
 * File: Br542.java
 * Date: 29 August 2009 22:18:04
 * Author: Quipoz Limited
 * 
 * Class transformed from BR542.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.Iterator;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.enquiries.dataaccess.AgntenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.UtrnrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.unitlinkedprocessing.dataaccess.ChdrulsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfstmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstnannTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZstaTableDAM;
import com.csc.life.unitlinkedprocessing.reports.Rr541Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrncpy;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*
* B5105 - Unit statement - Anniversary Report
* This is a clone from B5105
*
*    This  is  one of the  modules in the Regular processing run
* suite  of programs.  It uses OPNQRYF in the calling CL program
* to  select  all  contracts  which  are  due for unit statement
* processing.  Any  contracts  due are then processed by calling
* the generic routine as determined by the method held in T6647.
*
*    Control totals maintained by this program are:
*
*      1. Total CHDRs read
*      2. CHDRs processed
*      3. Payment outstanding
*      4. Unit trans outstanding
*      5. Statements produced
*
*    The  calling  CL  program will use OPNQRYF to select into a
* file  details  from the contract header (CHDRPF) file based on
* the following selection criteria:
*
* Keyed on : Contract
*
* Select   : IF   CHDR-STATEMENT-DATE < (parm date - 3 days) *
*            and  CHDR-VALIDFLAG = 1
* Omit     : IF   CHDR-STATEMENT-DATE = 0
*
* Parm date for the purpose of the selection should be reduced by
* three days for this selection to allow any imminent unit trans
* to go through unit dealing processing.
*
* 1.) Initialise.
*
*    Reset   the   control  parameters  if  the  program  is  in
* recovery/restart  mode  by  performing  004-UPDATE-REQD  until
* UPDATE-FLAG  is  yes.  This  is  necessary as OPNQRYF will not
* select records updated previously.
*
* 2.) Process Contracts.
*
*    The  records from the passed file are read sequentially and
* for each:
*
*    Read   the   transaction  status  codes  from  T5679  using
* parameter  1  (tran  code) from the jobstream details for this
* program. If the CHDR status fields do not match the table read
* the next sequential record.
*
*    Read  the CHDRPF with the logical view CHDRLIF and softlock
* the record.
*
*    Add 1 to control total no.1.
*
*    Read T6647 with transaction code/CHDR-CNTTYPE as the key to
* get  the  unit  statement  method.  If no item is found reread
* with  the  type  set to all '*' to pick up any global item. If
* there is still no item found this is a fatal error.
*
*    Read  T6659  (unit statement table) with this method as the
* key to get the unit statement details.
*
*  Checks for statement processing:
*
* Anniversary or Payment date?
*
*    If the  anniversary  or  payment date indicator on T6659 is
* not set to payment  (P)  go  to the next check. If the paid to
* date on the  contract is less than the unit statement date add
* 1 to control total no.3 and process the next record.
*
*
* What about outstanding unit transactions?
*
*
*    If the outstanding UTRN indicator on T6659 is set to 'Y' we
* need to  read  the  unit  transaction  file  (UTRNPF) with the
* logical view  UTRNRNL  and  a  key  of  company/contract. If a
* record is  found (view only selects outstanding records) we do
* not process this contract -  add one to control total no.4 and
* process next record.
*
* Process Statement.
*
*    Add one  to  control  total  no.5  and dynamically call the
* subroutine from  T6659 with the following parameters set up in
* ANNPROCREC:
*
*      - function
*      - status
*      - rider key
*      - plan suffix
*      - effective date (parm date)
*      - batchkey
*
*    If  the   status  returned  is  O-K  recalculate  the  next
* CHDR-STATEMENT-DATE  as CHDR-STATEMENT-DATE incremented by the
* frequency as held on T6659. Update the CHDR record and rewrite
* to the database. Add 1 to control total no.2.
*
*    If status is not O-K this is a fatal error.
*
* The records selected by the OPNQRYF are in the following order:-
*
*     Company
*     Contract
*
* The selected records are being put into CHDRPF by the CLP. So
* this program will only need to read this file.
*
*****************************************************************
* </pre>
*/
public class Br542 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ChdrpfTableDAM chdrpf = new ChdrpfTableDAM();
	private Rr541Report printFile = new Rr541Report();
	private ChdrpfTableDAM chdrpfRec = new ChdrpfTableDAM();
	private FixedLengthStringData printRecord = new FixedLengthStringData(350);
		/* Pls note that BR542 used is to enable the trx a/c rules will
		 be the same as the statement of account. In the event BR542
		 is needed in stead of BR542 pls create a duplicate item for
		 BR542.*/
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR542");
	private ZonedDecimalData wsaaInCount = new ZonedDecimalData(9, 0).setUnsigned();
	private ZonedDecimalData sub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private ZonedDecimalData wsaaLinecount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPagenum = new ZonedDecimalData(4, 0).setUnsigned();
	private static final int wsaaMaxLines = 37;

		/* WSAA-SUMMARY-TABLE */
	private FixedLengthStringData wsaaFundTable = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaFund = FLSArrayPartOfStructure(50, 4, wsaaFundTable, 0);
	private FixedLengthStringData[] wsaaFundType = FLSArrayPartOfStructure(50, 4, wsaaFundTable, 200);

	private FixedLengthStringData wsaaDunitsTable = new FixedLengthStringData(450);
	private PackedDecimalData[] wsaaClDunits = PDArrayPartOfStructure(50, 16, 5, wsaaDunitsTable, 0);
		/* WSAA-STORE-COMPONENT */
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaStoreLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreRider = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaZstaStoreKey = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaZstaStChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaZstaStoreKey, 0);
	private FixedLengthStringData wsaaZstaStChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaZstaStoreKey, 1);
	private PackedDecimalData wsaaZstaStUstmno = new PackedDecimalData(5, 0).isAPartOf(wsaaZstaStoreKey, 9);
	private PackedDecimalData wsaaZstaStPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaZstaStoreKey, 12);
	private FixedLengthStringData wsaaZstaStLife = new FixedLengthStringData(2).isAPartOf(wsaaZstaStoreKey, 15);
	private FixedLengthStringData wsaaZstaStCoverage = new FixedLengthStringData(2).isAPartOf(wsaaZstaStoreKey, 17);
	private FixedLengthStringData wsaaZstaStRider = new FixedLengthStringData(2).isAPartOf(wsaaZstaStoreKey, 19);
	private FixedLengthStringData wsaaZstaStUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(wsaaZstaStoreKey, 21);
	private FixedLengthStringData wsaaZstaStUnitType = new FixedLengthStringData(1).isAPartOf(wsaaZstaStoreKey, 25);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);

	private FixedLengthStringData wsspNameTab = new FixedLengthStringData(47).isAPartOf(wsspLongconfname, 0, REDEFINE);
	private FixedLengthStringData[] wsspName = FLSArrayPartOfStructure(47, 1, wsspNameTab, 0);
	private PackedDecimalData wsaaSummaryCount = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaUnits = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaFundCash = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaContCash = new PackedDecimalData(16, 5);
	private ZonedDecimalData wsaaTotalUnits = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaCloBalUnits = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaTotnofunts = new ZonedDecimalData(13, 2);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(13, 2).setUnsigned();
	private PackedDecimalData wsaaPrevTranno = new PackedDecimalData(5, 0);
	private static final int wsaaTextfieldLen = 13;

	private FixedLengthStringData wsaaExcptRecord = new FixedLengthStringData(1);
	private Validator wsaaExcptFound = new Validator(wsaaExcptRecord, "Y");
	private Validator wsaaExcptNotFound = new Validator(wsaaExcptRecord, "N");
	private PackedDecimalData wsaaTotalCash = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaUstnannContractAmt = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(20).init(SPACES);
	private PackedDecimalData wsaaFees = new PackedDecimalData(12, 5);
	private ZonedDecimalData wsaaStmtOpDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaStmtOpDunits = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsaaStmtClDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaStmtClDunits = new ZonedDecimalData(16, 5);

	private FixedLengthStringData wsaaValidRecord = new FixedLengthStringData(1);
	private Validator validRecord = new Validator(wsaaValidRecord, "Y");
	private Validator invalidRecord = new Validator(wsaaValidRecord, "N");

	private FixedLengthStringData wsaaZstaRecord = new FixedLengthStringData(1);
	private Validator wsaaZstaFound = new Validator(wsaaZstaRecord, "Y");
	private Validator wsaaZstaNotFound = new Validator(wsaaZstaRecord, "N");

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRunparm1TranCode = new FixedLengthStringData(4).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaRunparm1Rest = new FixedLengthStringData(6).isAPartOf(wsaaRunparm1, 4);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647TranCode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
	private String endOfFile = "N";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaZrstFound = new FixedLengthStringData(1);
	private static final String h965 = "H965";
	private static final String g029 = "G029";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;

		/* INDIC-AREA */
//	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	/*bug #ILIFE-764 start*/
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	/*bug #ILIFE-764 end*/
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/* COPY TR386REC.                                       <LA3235>*/
	private FixedLengthStringData rr541h01Record = new FixedLengthStringData(162);
	private FixedLengthStringData rr541h01O = new FixedLengthStringData(162).isAPartOf(rr541h01Record, 0);
	private FixedLengthStringData stmtdate = new FixedLengthStringData(10).isAPartOf(rr541h01O, 0);
	private ZonedDecimalData pagenum = new ZonedDecimalData(4, 0).isAPartOf(rr541h01O, 10);
	private FixedLengthStringData asgname1 = new FixedLengthStringData(30).isAPartOf(rr541h01O, 14);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr541h01O, 44);
	private FixedLengthStringData asgname2 = new FixedLengthStringData(30).isAPartOf(rr541h01O, 52);
	private FixedLengthStringData name1 = new FixedLengthStringData(30).isAPartOf(rr541h01O, 82);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rr541h01O, 112);
	private FixedLengthStringData name2 = new FixedLengthStringData(30).isAPartOf(rr541h01O, 115);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(rr541h01O, 145);

	private FixedLengthStringData rr541h02Record = new FixedLengthStringData(30+DD.cltaddr.length*5); //Starts ILIFE-3212
	private FixedLengthStringData rr541h02O = new FixedLengthStringData(30+DD.cltaddr.length*5).isAPartOf(rr541h02Record, 0);
	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr541h02O, 0);
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr541h02O, DD.cltaddr.length*1);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr541h02O, DD.cltaddr.length*2);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr541h02O, DD.cltaddr.length*3);
	private FixedLengthStringData addr5 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr541h02O, DD.cltaddr.length*4);
	private FixedLengthStringData ctypedes = new FixedLengthStringData(30).isAPartOf(rr541h02O, DD.cltaddr.length*5); //End ILIFE-3212

	private FixedLengthStringData rr541h03Record = new FixedLengthStringData(30);
	private FixedLengthStringData rr541h03O = new FixedLengthStringData(30).isAPartOf(rr541h03Record, 0);
	private FixedLengthStringData vrtfnddsc = new FixedLengthStringData(30).isAPartOf(rr541h03O, 0);

	private FixedLengthStringData rr541h04Record = new FixedLengthStringData(24);
	private FixedLengthStringData rr541h04O = new FixedLengthStringData(24).isAPartOf(rr541h04Record, 0);
	private FixedLengthStringData stopdt = new FixedLengthStringData(10).isAPartOf(rr541h04O, 0);
	private ZonedDecimalData zrtotnun = new ZonedDecimalData(14, 3).isAPartOf(rr541h04O, 10);

	private FixedLengthStringData rr541h05Record = new FixedLengthStringData(42);
	private FixedLengthStringData rr541h05O = new FixedLengthStringData(42).isAPartOf(rr541h05Record, 0);
	private FixedLengthStringData stopdt1 = new FixedLengthStringData(10).isAPartOf(rr541h05O, 0);
	private ZonedDecimalData uoffpr02 = new ZonedDecimalData(9, 5).isAPartOf(rr541h05O, 10);
	private ZonedDecimalData ubidpr02 = new ZonedDecimalData(9, 5).isAPartOf(rr541h05O, 19);
	private ZonedDecimalData zrtotnun1 = new ZonedDecimalData(14, 3).isAPartOf(rr541h05O, 28);

	private FixedLengthStringData rr541d01Record = new FixedLengthStringData(68);
	private FixedLengthStringData rr541d01O = new FixedLengthStringData(68).isAPartOf(rr541d01Record, 0);
	private FixedLengthStringData moniesdt = new FixedLengthStringData(10).isAPartOf(rr541d01O, 0);
	private ZonedDecimalData zrordpay = new ZonedDecimalData(9, 2).isAPartOf(rr541d01O, 10);
	private FixedLengthStringData textfield = new FixedLengthStringData(13).isAPartOf(rr541d01O, 19);
	private ZonedDecimalData uoffpr = new ZonedDecimalData(9, 5).isAPartOf(rr541d01O, 32);
	private ZonedDecimalData ubidpr = new ZonedDecimalData(9, 5).isAPartOf(rr541d01O, 41);
	private ZonedDecimalData zrbonprd1 = new ZonedDecimalData(9, 3).isAPartOf(rr541d01O, 50);
	private ZonedDecimalData zrbonprd2 = new ZonedDecimalData(9, 3).isAPartOf(rr541d01O, 59);

	private FixedLengthStringData rr541s01Record = new FixedLengthStringData(23);
	private FixedLengthStringData rr541s01O = new FixedLengthStringData(23).isAPartOf(rr541s01Record, 0);
	private FixedLengthStringData stmtdate1 = new FixedLengthStringData(10).isAPartOf(rr541s01O, 0);
	private ZonedDecimalData zrtotvlu = new ZonedDecimalData(13, 2).isAPartOf(rr541s01O, 10);

	private FixedLengthStringData rr541s02Record = new FixedLengthStringData(110);
	private FixedLengthStringData rr541s02O = new FixedLengthStringData(110).isAPartOf(rr541s02Record, 0);
	private FixedLengthStringData despname1 = new FixedLengthStringData(47).isAPartOf(rr541s02O, 0);
	private FixedLengthStringData cltphone = new FixedLengthStringData(16).isAPartOf(rr541s02O, 47);
	private FixedLengthStringData despname2 = new FixedLengthStringData(47).isAPartOf(rr541s02O, 63);

	private FixedLengthStringData rr541s03Record = new FixedLengthStringData(1);
	private AgntenqTableDAM agntenqIO = new AgntenqTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrulsTableDAM chdrulsIO = new ChdrulsTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private UstfTableDAM ustfIO = new UstfTableDAM();
	private UstfstmTableDAM ustfstmIO = new UstfstmTableDAM();
	private UstnannTableDAM ustnannIO = new UstnannTableDAM();
	private UstsTableDAM ustsIO = new UstsTableDAM();
	private UtrnrnlTableDAM utrnrnlIO = new UtrnrnlTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private ZstaTableDAM zstaIO = new ZstaTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	private Varcom varcom = new Varcom();
	private Dbcstrncpy dbcstrncpy = new Dbcstrncpy();
	private T5645rec t5645rec = new T5645rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5679rec t5679rec = new T5679rec();
	private T6647rec t6647rec = new T6647rec();
	private T6659rec t6659rec = new T6659rec();
	private T5515rec t5515rec = new T5515rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private Iterator<Chdrpf> chdrpfIterator;
	private Chdrpf chdrpfEntity;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		tryAgain1450, 
		readT66591500, 
		outstandUnitTrans1600, 
		processStatement1700, 
		exit1900, 
		para3100, 
		updateUstnann33a0, 
		callUstnann33a0, 
		nextCovr3600a, 
		exit3600a, 
		nextZsta3910a, 
		exit3910a, 
		b280Nextr, 
		b290Exit
	}

	public Br542() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		chdrpfIterator = (Iterator<Chdrpf>) parmArray[1];
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		open110();
		openBatch120();
		mainProcess130();
		closeBatch170();
		out185();
	}

protected void open110()
	{
		chdrpf.openInput();
		printFile.openOutput();
	}

protected void openBatch120()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.runparm1);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
		initialise200();
		b300ReadT5645();
	}

	/**
	* <pre>
	**** PERFORM B310-READ-TR386.                             <LA3235>
	* </pre>
	*/
protected void mainProcess130()
	{
		while ( !(isEQ(endOfFile, "Y"))) {
			mainProcessing1000();
		}
		
	}

protected void closeBatch170()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.runparm1);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
	}

protected void out185()
	{
		chdrpf.close();
		printFile.close();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		wsaaIndex.set(0);
		endOfFile = "N";
		/* Read T5679 for valid statii.*/
		wsaaItemkey.set(SPACES);
		wsaaRunparm1.set(runparmrec.runparm1);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t5679);
		wsaaItemkey.itemItemitem.set(wsaaRunparm1TranCode);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					process1100();
					readT66471400();
				case tryAgain1450: 
					tryAgain1450();
				case readT66591500: 
					readT66591500();
					checkT6659Details1550();
				case outstandUnitTrans1600: 
					outstandUnitTrans1600();
				case processStatement1700: 
					processStatement1700();
					nearExit1800();
				case exit1900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void process1100()
	{
		/* Read the input file seuentially until end of file.*/
		updateReqd004();
		if (isEQ(controlrec.flag, "N")) {
			goTo(GotoLabel.exit1900);
		}	
		//smalchi2 for ILIFE-1099 STARTS and changed Chdrpfrec wherever used to chdrpfEntity
		if (chdrpfIterator.hasNext()) {
			chdrpfEntity = chdrpfIterator.next();
		} else {
			endOfFile = "Y";
			goTo(GotoLabel.exit1900);
		}		
		//ENDS
		/* Validate the contract status against T5679.*/
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)); wsaaIndex.add(1)){
			validateChdrStatus2000();
		}
		if (isEQ(wsaaValidStatus, "N")) {
			goTo(GotoLabel.exit1900);
		}
		softlockReadChdr3900();
		if (isEQ(sftlockrec.statuz, "LOCK")) {
				getAppVars().addDiagnostic(chdrpfEntity.getChdrnum() +"ALREADY LOCKED - PROCESSING BYPASSED");/* IJTI-1523 */
			goTo(GotoLabel.exit1900);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void readT66471400()
	{
		/* Use transaction code/contract type to access T6647 to get the*/
		/* unit statement method.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaT6647TranCode.set(wsaaRunparm1TranCode);
		wsaaT6647Cnttype.set(chdrpfEntity.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			conerrrec.statuz.set(itdmIO.getStatuz());
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(), runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Key))) {
			goTo(GotoLabel.tryAgain1450);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		goTo(GotoLabel.readT66591500);
	}

protected void tryAgain1450()
	{
		wsaaT6647TranCode.set(wsaaRunparm1TranCode);
		wsaaT6647Cnttype.set("***");
		itdmIO.setItemitem(wsaaT6647Key);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			conerrrec.statuz.set(itdmIO.getStatuz());
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(), runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Key))) {
			itdmIO.setItemitem(wsaaT6647Key);
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(h965);
			databaseError006();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void readT66591500()
	{
		/* Use unit statement method to access T6659 to get generic*/
		/* processing subroutine and all the relevant details.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			conerrrec.statuz.set(itdmIO.getStatuz());
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(), runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6659))
		|| (isNE(itdmIO.getItemitem(), t6647rec.unitStatMethod))) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(g029);
			databaseError006();
		}
		t6659rec.t6659Rec.set(itdmIO.getGenarea());
		if (isEQ(t6659rec.subprog, SPACES)) {
			goTo(GotoLabel.exit1900);
		}
	}

protected void checkT6659Details1550()
	{
		/* Check the details and call the generic subroutine from T6659.*/
		if (isNE(t6659rec.annOrPayInd, "P")) {
			goTo(GotoLabel.outstandUnitTrans1600);
		}
		if (isLT(chdrpfEntity.getPtdate(), chdrpfEntity.getStmdte())) {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			unlockRewriteChdr4000();
			goTo(GotoLabel.exit1900);
		}
	}

protected void outstandUnitTrans1600()
	{
		if (isEQ(t6659rec.osUtrnInd, "Y")) {
			goTo(GotoLabel.processStatement1700);
		}
		utrnrnlIO.setParams(SPACES);
		utrnrnlIO.setChdrnum(chdrpfEntity.getChdrnum());
		utrnrnlIO.setChdrcoy(chdrpfEntity.getChdrcoy());
		utrnrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnrnlIO);
		if ((isNE(utrnrnlIO.getStatuz(), varcom.oK))
		&& (isNE(utrnrnlIO.getStatuz(), varcom.mrnf))) {
			conerrrec.statuz.set(utrnrnlIO.getStatuz());
			conerrrec.params.set(utrnrnlIO.getParams());
			databaseError006();
		}
		if (isNE(utrnrnlIO.getStatuz(), varcom.mrnf)) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
			unlockRewriteChdr4000();
			goTo(GotoLabel.exit1900);
		}
	}

protected void processStatement1700()
	{
		contotrec.totno.set(ct05);
		contotrec.totval.set(1);
		callContot001();
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(chdrpfEntity.getChdrcoy());
		annprocrec.chdrnum.set(chdrpfEntity.getChdrnum());
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(runparmrec.effdate);
		annprocrec.batctrcde.set(runparmrec.runparm1);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(runparmrec.user);
		annprocrec.batcactyr.set(runparmrec.acctyear);
		annprocrec.batcactmn.set(runparmrec.acctmonth);
		annprocrec.batccoy.set(runparmrec.company);
		annprocrec.batcbrn.set(runparmrec.batcbranch);
		annprocrec.batcbatch.set(batcdorrec.batch);
		annprocrec.batcpfx.set(batcdorrec.prefix);
		/* Print Unit statment for select record*/
		printStatement2100();
		/* Calculate CHDRLIF-STATEMENT-DATE as CHDRLIF-STATEMENT-DATE*/
		/* incremented by the frequency in T6659.*/
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set(t6659rec.freq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(chdrlifIO.getStatementDate());
		datcon2rec.intDate2.set(0);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		chdrlifIO.setStatementDate(datcon2rec.intDate2);
	}

protected void nearExit1800()
	{
		unlockRewriteChdr4000();
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void validateChdrStatus2000()
	{
		/*PARA*/
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()], chdrpfEntity.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)); wsaaIndex.add(1)){
				validatePremStatus2500();
			}
		}
		/*EXIT*/
	}

protected void printStatement2100()
	{
		processStatement2100();
	}

protected void processStatement2100()
	{
		/* Read CHDR record.*/
		chdrIO.setDataArea(SPACES);
		chdrIO.setChdrpfx(chdrpfEntity.getChdrpfx());
		chdrIO.setChdrcoy(chdrpfEntity.getChdrcoy());
		chdrIO.setChdrnum(chdrpfEntity.getChdrnum());
		chdrIO.setValidflag(chdrpfEntity.getValidflag());
		chdrIO.setCurrfrom(chdrpfEntity.getCurrfrom());
		chdrIO.setFunction(varcom.readr);
		chdrIO.setFormat(formatsInner.chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(chdrIO.getStatuz());
			conerrrec.params.set(chdrIO.getParams());
			databaseError006();
		}
		summaryStatement2200();
		detailStatement3000();
	}

	/**
	* <pre>
	*    The Summary Statement is always not printed as in the old
	*    format of unit statement printing, however, the Summary
	*    section is retain to retrieve infor for header; so that
	*    the structure of this program remains similar to the old
	*    statement printing program
	* </pre>
	*/
protected void summaryStatement2200()
	{
		para2000();
	}

protected void para2000()
	{
		/*   Read the first USTF record for the contract with the statement*/
		/*   number obtained from the Trigger Record (USTJ).*/
		ustfIO.setDataArea(SPACES);
		ustfIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ustfIO.setChdrnum(chdrlifIO.getChdrnum());
		ustfIO.setUstmno(99999);
		ustfIO.setPlanSuffix(ZERO);
		ustfIO.setFunction(varcom.begn);
		ustfIO.setFormat(formatsInner.ustfrec);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(), varcom.oK)
		&& isNE(ustfIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
		/*    The data for the Statement Header are set up here and the*/
		/*    header record is printed.*/
		chdrnum.set(chdrlifIO.getChdrnum());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(chdrpfEntity.getStmdte());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		stmtdate.set(datcon1rec.extDate);
		stmtdate1.set(datcon1rec.extDate);
		chdrulsIO.setDataKey(SPACES);
		chdrulsIO.setChdrcoy(runparmrec.company);
		chdrulsIO.setChdrnum(chdrlifIO.getChdrnum());
		chdrulsIO.setFormat(formatsInner.chdrulsrec);
		chdrulsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrulsIO);
		if (isNE(chdrulsIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(chdrulsIO.getParams());
			databaseError006();
		}
		/*    Obtain the Contract Type description from T3681.*/
		/*    - Modified to obtain the contract type desc from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrulsIO.getCnttype());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			ctypedes.fill("?");
		}
		else {
			ctypedes.set(descIO.getLongdesc());
		}
		cntcurr.set(chdrulsIO.getCntcurr());
		/* Obtain the Life Assured client number and name.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrulsIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrulsIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(lifeenqIO.getParams());
			databaseError006();
		}
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(runparmrec.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(cltsIO.getParams());
			databaseError006();
		}
		plainname();
		/* Lifename on statement provide for 2 lines of 30 bytes length*/
		if (isEQ(wsspName[31], SPACES)
		&& isEQ(wsspName[32], SPACES)) {
			name1.set(wsspLongconfname);
		}
		else {
			if (isEQ(cltsIO.getEthorig(), "1")) {
				name1.set(cltsIO.getSurname());
				name2.set(cltsIO.getGivname());
			}
			else {
				name1.set(cltsIO.getGivname());
				name2.set(cltsIO.getSurname());
			}
		}
		/* to get additional information for new format of sttm printing*/
		extractDetails3500a();
		wsaaFundTable.set(SPACES);
		sub.set(1);
		while ( !(isNE(ustfIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(), varcom.endp))) {
			buildFundTable2100();
		}
		
		sub.set(1);
		while ( !(isEQ(wsaaFund[sub.toInt()], SPACES))) {
			printSummaryLine2300();
		}
		
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspLongconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspLongconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspLongconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspLongconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void buildFundTable2100()
	{
		/*PARA*/
		wsaaFund[sub.toInt()].set(ustfIO.getUnitVirtualFund());
		wsaaFundType[sub.toInt()].set(ustfIO.getUnitType());
		sub.add(1);
		ustfIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(), varcom.oK)
		&& isNE(ustfIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void printSummaryLine2300()
	{
		para2300();
	}

protected void para2300()
	{
		/*    The fund summary detail lines are built here.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrulsIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItemitem(wsaaFund[sub.toInt()]);
		itdmIO.setItmfrm(chdrulsIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(), chdrulsIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), wsaaFund[sub.toInt()])) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		/*    MOVE T5515-CURRCODE         TO FNDCURR OF RR541D02-O.*/
		/*    Obtain the Fund description from T5515.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(tablesInner.t5515);
		descIO.setDescitem(wsaaFund[sub.toInt()]);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		/*    Obtain the Unit Type description from T6649.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(tablesInner.t6649);
		descIO.setDescitem(wsaaFundType[sub.toInt()]);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(tablesInner.t3629);
		descIO.setDescitem(t5515rec.currcode);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		printRecord.set(SPACES);
		sub.add(1);
	}

protected void summaryHeadings2400()
	{
		/*PARA*/
		wsaaOverflow.set("N");
		wsaaLinecount.set(8);
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		printRecord.set(SPACES);
//		printFile.printRr541h01(rr541h01Record);
		/*bug #ILIFE-764 start*/
		printFile.printRr541h01(rr541h01Record, indicArea);
		/*bug #ILIFE-764 end*/
		/*                              INDICATORS INDIC-AREA.   <S19FIX>*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		printRecord.set(SPACES);
		/*EXIT*/
	}

protected void detailStatement3000()
	{
		para3000();
	}

protected void para3000()
	{
		/*    The statement header for the contract will be written again.*/
		wsaaLinecount.set(ZERO);
		wsaaTotnofunts.set(ZERO);
		wsaaTotalUnits.set(ZERO);
		wsaaPagenum.set(ZERO);
		wsaaSummaryCount.set(ZERO);
		/*   Read the first USTF record for the contract with the most*/
		/*   recent statement number, ie. the latest.*/
		ustfIO.setDataArea(SPACES);
		ustfIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ustfIO.setChdrnum(chdrlifIO.getChdrnum());
		ustfIO.setUstmno(99999);
		ustfIO.setPlanSuffix(ZERO);
		ustfIO.setFormat(formatsInner.ustfrec);
		ustfIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(), varcom.oK)
		&& isNE(ustfIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
		while ( !(isNE(ustfIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(), varcom.endp))) {
			processUstfHeaders3100();
		}
		
	}

protected void processUstfHeaders3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para3100: 
					para3100();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3100()
	{
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		if (isEQ(ustfIO.getPlanSuffix(), ZERO)
		&& isNE(chdrulsIO.getPolsum(), ZERO)) {
			wsaaSummaryCount.add(1);
		}
		printRecord.set(SPACES);
//		printFile.printRr541h01(rr541h01Record);
		/*bug #ILIFE-764 start*/
		printFile.printRr541h01(rr541h01Record, indicArea);
		/*bug #ILIFE-764 end*/
		printRecord.set(SPACES);
		printFile.printRr541h02(rr541h02Record);
		wsaaOverflow.set("N");
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		wsaaLinecount.set(21);
		wsaaStorePlanSuffix.set(ustfIO.getPlanSuffix());
		/*    A new page will be started for each new component.*/
		/*    If the transactions for a single fund go across a page*/
		/*    boundary then the Fund/Currency sub-heading is re-printed*/
		/*    but the Opening Balance details are only printed on change*/
		/*    of Fund.*/
		/*    Do not check for change in sttmt no., Life, Coverage, rider,*/
		/*    plan suffix*/
		while ( !(isNE(ustfIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isEQ(ustfIO.getStatuz(), varcom.endp))) {
			processUstfComponent3200();
		}
		
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		/* To print the bottom portion of the statment after all the*/
		/* details line have been printed*/
		zrtotvlu.set(wsaaTotnofunts);
		printRecord.set(SPACES);
		printFile.printRr541s01(rr541s01Record);
		getAgent3700a();
		printRecord.set(SPACES);
		printFile.printRr541s02(rr541s02Record);
		/*   If we are still processing the summary records then re-read*/
		/*   the first USTF record for the contract with the most and*/
		/*   re-process all the USTF records for the summary.*/
		if (isEQ(wsaaStorePlanSuffix, ZERO)
		&& isNE(chdrulsIO.getPolsum(), ZERO)
		&& isLT(wsaaSummaryCount, chdrulsIO.getPolsum())) {
			ustfIO.setDataArea(SPACES);
			ustfIO.setChdrcoy(chdrlifIO.getChdrcoy());
			ustfIO.setChdrnum(chdrlifIO.getChdrnum());
			ustfIO.setUstmno(99999);
			ustfIO.setPlanSuffix(ZERO);
			ustfIO.setFormat(formatsInner.ustfrec);
			ustfIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, ustfIO);
			if (isNE(ustfIO.getStatuz(), varcom.oK)
			&& isNE(ustfIO.getStatuz(), varcom.endp)) {
				conerrrec.dbparams.set(ustfIO.getParams());
				databaseError006();
			}
			else {
				goTo(GotoLabel.para3100);
			}
		}
	}

protected void processUstfComponent3200()
	{
		para3200();
	}

protected void para3200()
	{
		/* To initialize total units at new or change of fund*/
		wsaaTotalUnits.set(ZERO);
		wsaaCloBalUnits.set(ZERO);
		wsaaStmtOpDate.set(ZERO);
		wsaaStmtOpDunits.set(ZERO);
		wsaaStmtClDate.set(ZERO);
		wsaaStmtClDunits.set(ZERO);
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrulsIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItemitem(ustfIO.getUnitVirtualFund());
		itdmIO.setItmfrm(chdrulsIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(), chdrulsIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), ustfIO.getUnitVirtualFund())) {
			conerrrec.dbparams.set(itdmIO.getParams());
			databaseError006();
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		/*    Obtain the Fund description from T5515.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(tablesInner.t5515);
		descIO.setDescitem(ustfIO.getUnitVirtualFund());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			vrtfnddsc.fill("?");
		}
		else {
			vrtfnddsc.set(descIO.getLongdesc());
		}
		if (isGT(wsaaLinecount, wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr541s03(rr541s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		printRecord.set(SPACES);
		printFile.printRr541h03(rr541h03Record);
		wsaaLinecount.add(3);
		/*    Obtain the Unit Type description from T6649.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(tablesInner.t6649);
		descIO.setDescitem(ustfIO.getUnitType());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		/*    Obtain the Fund Currency description from T3629.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(tablesInner.t3629);
		descIO.setDescitem(t5515rec.currcode);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		/* Set up Opening Details from USTF.*/
		/* To obtain opening and closing information from ZSTA.*/
		readZsta3900a();
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(wsaaStmtOpDate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		stopdt.set(datcon1rec.extDate);
		if (isEQ(ustfIO.getPlanSuffix(), ZERO)
		&& isNE(chdrulsIO.getPolsum(), ZERO)) {
			compute(wsaaUnits, 5).set(div(ustfIO.getStmtOpDunits(), chdrulsIO.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustfIO.getStmtOpFundCash(), chdrulsIO.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustfIO.getStmtOpContCash(), chdrulsIO.getPolsum()));
			zrtotnun.set(wsaaUnits);
			wsaaTotalUnits.set(wsaaUnits);
		}
		else {
			wsaaTotalUnits.set(wsaaStmtOpDunits);
			zrtotnun.setRounded(wsaaStmtOpDunits);
		}
		/* MOVE WSAA-FUND-CASH          TO ZRDP-AMOUNT-IN.              */
		/* MOVE T5515-CURRCODE          TO ZRDP-CURRENCY.               */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO WSAA-FUND-CASH.              */
		if (isNE(wsaaFundCash, 0)) {
			zrdecplrec.amountIn.set(wsaaFundCash);
			zrdecplrec.currency.set(t5515rec.currcode);
			callRounding5000();
			wsaaFundCash.set(zrdecplrec.amountOut);
		}
		/* MOVE WSAA-CONT-CASH          TO ZRDP-AMOUNT-IN.              */
		/* MOVE CHDRULS-CNTCURR         TO ZRDP-CURRENCY.               */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO WSAA-CONT-CASH.              */
		if (isNE(wsaaContCash, 0)) {
			zrdecplrec.amountIn.set(wsaaContCash);
			zrdecplrec.currency.set(chdrulsIO.getCntcurr());
			callRounding5000();
			wsaaContCash.set(zrdecplrec.amountOut);
		}
		if (isGT(wsaaLinecount, wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr541s03(rr541s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		printRecord.set(SPACES);
		printFile.printRr541h04(rr541h04Record);
		wsaaLinecount.add(1);
		/*    Set up the USTS Key.*/
		ustsIO.setChdrcoy(ustfIO.getChdrcoy());
		ustsIO.setChdrnum(ustfIO.getChdrnum());
		ustsIO.setUstmno(ZERO);
		ustsIO.setPlanSuffix(ustfIO.getPlanSuffix());
		ustsIO.setLife(ustfIO.getLife());
		ustsIO.setCoverage(ustfIO.getCoverage());
		ustsIO.setRider(ustfIO.getRider());
		ustsIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		ustsIO.setUnitType(ustfIO.getUnitType());
		ustsIO.setMoniesDate(chdrlifIO.getStatementDate());
		ustsIO.setTranno(ZERO);
		ustsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ustsIO);
		if (isNE(ustsIO.getStatuz(), varcom.oK)
		&& isNE(ustsIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustsIO.getParams());
			databaseError006();
		}
		while ( !((isNE(ustsIO.getChdrcoy(), ustfIO.getChdrcoy()))
		|| (isNE(ustsIO.getChdrnum(), ustfIO.getChdrnum()))
		|| isEQ(ustsIO.getStatuz(), varcom.endp))) {
			processUstsTransactions3300();
		}
		
		/*    Set up the USTNANN Key.                                      */
		ustnannIO.setParams(SPACES);
		ustnannIO.setChdrcoy(ustfIO.getChdrcoy());
		ustnannIO.setChdrnum(ustfIO.getChdrnum());
		ustnannIO.setPlanSuffix(ustfIO.getPlanSuffix());
		ustnannIO.setLife(ustfIO.getLife());
		ustnannIO.setCoverage(ustfIO.getCoverage());
		ustnannIO.setRider(ustfIO.getRider());
		ustnannIO.setUnitVirtualFund(SPACES);
		ustnannIO.setTranno(ZERO);
		ustnannIO.setFormat(formatsInner.ustnannrec);
		ustnannIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, ustnannIO);
		wsaaTotalCash.set(ZERO);
		wsaaExcptRecord.set("N");
		while ( !((isNE(ustnannIO.getChdrcoy(), ustfIO.getChdrcoy()))
		|| (isNE(ustnannIO.getChdrnum(), ustfIO.getChdrnum()))
		|| isEQ(ustnannIO.getStatuz(), varcom.endp))) {
			processUstnannTrans3300();
		}
		
		if (wsaaExcptFound.isTrue()) {
			b600UpdateUstfstm();
		}
		/* To obtain bid price and offer price                             */
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(ustfIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(ustfIO.getUnitType());
		vprnudlIO.setEffdate(chdrpfEntity.getStmdte());
		vprnudlIO.setFunction(varcom.begn);
		vprnudlIO.setFormat(formatsInner.vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(vprnudlIO.getParams());
			databaseError006();
		}
		uoffpr02.set(vprnudlIO.getUnitOfferPrice());
		ubidpr02.set(vprnudlIO.getUnitBidPrice());
		/*    Set up Closing Details from USTF.*/
		wsaaStmtClDate.set(chdrpfEntity.getStmdte());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(chdrpfEntity.getStmdte());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		stopdt1.set(datcon1rec.extDate);
		if (isEQ(ustfIO.getPlanSuffix(), ZERO)
		&& isNE(chdrulsIO.getPolsum(), ZERO)) {
			/*    COMPUTE WSAA-UNITS       = USTF-STMT-CL-DUNITS            */
			/*                       /   CHDRULS-POLSUM                     */
			/*    COMPUTE WSAA-FUND-CASH   = USTF-STMT-CL-FUND-CASH         */
			/*                       /   CHDRULS-POLSUM                     */
			/*    COMPUTE WSAA-CONT-CASH   = USTF-STMT-CL-CONT-CASH         */
			/*                       /   CHDRULS-POLSUM                     */
			if (wsaaExcptFound.isTrue()) {
				compute(wsaaUnits, 5).set(div(ustfstmIO.getStmtClDunits(), chdrulsIO.getPolsum()));
				compute(wsaaFundCash, 5).set(div(ustfstmIO.getStmtClFundCash(), chdrulsIO.getPolsum()));
				compute(wsaaContCash, 5).set(div(ustfstmIO.getStmtClContCash(), chdrulsIO.getPolsum()));
			}
			else {
				compute(wsaaUnits, 5).set(div(ustfIO.getStmtClDunits(), chdrulsIO.getPolsum()));
				compute(wsaaFundCash, 5).set(div(ustfIO.getStmtClFundCash(), chdrulsIO.getPolsum()));
				compute(wsaaContCash, 5).set(div(ustfIO.getStmtClContCash(), chdrulsIO.getPolsum()));
			}
			zrtotnun1.set(wsaaUnits);
			wsaaCloBalUnits.set(wsaaUnits);
			wsaaStmtClDunits.set(wsaaUnits);
		}
		else {
			/*    MOVE USTF-STMT-CL-DUNITS TO WSAA-CLO-BAL-UNITS            */
			/*                                WSAA-STMT-CL-DUNITS           */
			if (wsaaExcptFound.isTrue()) {
				wsaaCloBalUnits.set(ustfstmIO.getStmtClDunits());
				wsaaStmtClDunits.set(ustfstmIO.getStmtClDunits());
			}
			else {
				wsaaCloBalUnits.set(ustfIO.getStmtClDunits());
				wsaaStmtClDunits.set(ustfIO.getStmtClDunits());
			}
			zrtotnun1.setRounded(wsaaStmtClDunits);
		}
		/* MOVE WSAA-FUND-CASH          TO ZRDP-AMOUNT-IN.              */
		/* MOVE T5515-CURRCODE          TO ZRDP-CURRENCY.               */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO WSAA-FUND-CASH.              */
		if (isNE(wsaaFundCash, 0)) {
			zrdecplrec.amountIn.set(wsaaFundCash);
			zrdecplrec.currency.set(t5515rec.currcode);
			callRounding5000();
			wsaaFundCash.set(zrdecplrec.amountOut);
		}
		/* MOVE WSAA-CONT-CASH          TO ZRDP-AMOUNT-IN.              */
		/* MOVE CHDRULS-CNTCURR         TO ZRDP-CURRENCY.               */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO WSAA-CONT-CASH.              */
		if (isNE(wsaaContCash, 0)) {
			zrdecplrec.amountIn.set(wsaaContCash);
			zrdecplrec.currency.set(chdrulsIO.getCntcurr());
			callRounding5000();
			wsaaContCash.set(zrdecplrec.amountOut);
		}
		/* To Update opening and closing information from ZSTA.*/
		if (wsaaZstaNotFound.isTrue()) {
			createZsta3950a();
		}
		else {
			rewrtZsta3970a();
		}
		/* Convert units to dollar value.*/
		unitsValue3800a();
		/* Print the closing balance of the fund.*/
		if (isGT(wsaaLinecount, wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr541s03(rr541s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		printRecord.set(SPACES);
		printFile.printRr541h05(rr541h05Record);
		wsaaLinecount.add(1);
		/*    Read the next USTF record.*/
		ustfIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(), varcom.oK)
		&& isNE(ustfIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustfIO.getParams());
			databaseError006();
		}
	}

protected void processUstsTransactions3300()
	{
		para3300();
		callUsts33a0();
	}

protected void para3300()
	{
		/* Read the next record if the unit sttmt transaction if a*/
		/* non-investment transaction*/
		if (isEQ(ustfIO.getChdrnum(), ustsIO.getChdrnum())) {
			if ((isEQ(ustsIO.getUnitVirtualFund(), SPACES)
			&& isEQ(ustsIO.getFundAmount(), ZERO))|| isEQ(ustsIO.getPriceDateUsed(),ZERO)) {
				return ;
			}
		}
		/* Read next record if unit type of unit virtual fund differ*/
		if ((isNE(ustsIO.getUnitVirtualFund(), ustfIO.getUnitVirtualFund()))
		|| (isNE(ustsIO.getUnitType(), ustfIO.getUnitType()))) {
			return ;
		}
		/* Read next record if component differ                            */
		if (isNE(ustsIO.getLife(), ustfIO.getLife())
		|| isNE(ustsIO.getCoverage(), ustfIO.getCoverage())
		|| isNE(ustsIO.getRider(), ustfIO.getRider())) {
			return ;
		}
		/* Read next record if transaction is not within printing range*/
		/* for subsequent anniversary year after the 1st anniv printing*/
		/* COMPUTE WSAA-PREV-DATE = CHDRLIF-STATEMENT-DATE.             */
		/* COMPUTE WSAA-PREV-CCYY = WSAA-PREV-CCYY - 1.                 */
		/* This does not work if anniversary is not printed yearly.        */
		/* Thus, we should check against ZSTA opening date instead.        */
		if (wsaaZstaFound.isTrue()) {
			/*    IF (USTS-MONIES-DATE NOT > WSAA-PREV-DATE) OR             */
			/*    IF (USTS-MONIES-DATE NOT > ZSTA-STMT-OP-DATE) OR <LIFE2.1>*/
			if ((isLTE(ustsIO.getMoniesDate(), zstaIO.getStmtClDate()))
			|| (isGT(ustsIO.getMoniesDate(), chdrlifIO.getStatementDate()))) {
				return ;
			}
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		/*    Set up Transaction Details and write details line D01.*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ustsIO.getMoniesDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		moniesdt.set(datcon1rec.extDate);
		/* To obtain bid price and offer price*/
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(ustfIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(ustfIO.getUnitType());
		vprnudlIO.setEffdate(ustsIO.getPriceDateUsed());
		vprnudlIO.setFunction(varcom.readr);
		vprnudlIO.setFormat(formatsInner.vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(vprnudlIO.getParams());
			databaseError006();
		}
		uoffpr.set(vprnudlIO.getUnitOfferPrice());
		ubidpr.set(vprnudlIO.getUnitBidPrice());
		if (isEQ(ustfIO.getPlanSuffix(), ZERO)
		&& isNE(chdrulsIO.getPolsum(), ZERO)) {
			compute(wsaaUnits, 5).set(div(ustsIO.getNofDunits(), chdrulsIO.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustsIO.getFundAmount(), chdrulsIO.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustsIO.getContractAmount(), chdrulsIO.getPolsum()));
			if (isNE(wsaaFundCash, 0)) {
				zrdecplrec.amountIn.set(wsaaFundCash);
				zrdecplrec.currency.set(t5515rec.currcode);
				callRounding5000();
				wsaaFundCash.set(zrdecplrec.amountOut);
			}
			if (isNE(wsaaContCash, 0)) {
				zrdecplrec.amountIn.set(wsaaContCash);
				zrdecplrec.currency.set(chdrulsIO.getCntcurr());
				callRounding5000();
				wsaaContCash.set(zrdecplrec.amountOut);
			}
			zrbonprd1.setRounded(wsaaUnits);
			wsaaTotalUnits.add(wsaaUnits);
			zrbonprd2.setRounded(wsaaTotalUnits);
			zrordpay.set(wsaaFundCash);
		}
		else {
			/* Splitting mortality charges and fees*/
			if (isEQ(ustsIO.getBatctrcde(), "B633")) {
				zrstIO.setParams(SPACES);
				zrstIO.setChdrcoy(ustfIO.getChdrcoy());
				zrstIO.setChdrnum(ustfIO.getChdrnum());
				zrstIO.setLife(ustfIO.getLife());
				zrstIO.setCoverage(ustfIO.getCoverage());
				zrstIO.setJlife(SPACES);
				/*                                   ZRST-RIDER                    */
				zrstIO.setRider(ustfIO.getRider());
				zrstIO.setFormat(formatsInner.zrstrec);
				zrstIO.setFunction(varcom.begn);
				wsaaZrstFound.set(SPACES);
				while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
					b100ReadZrst();
				}
				
				if (isEQ(wsaaZrstFound, "Y")) {
					return ;
				}
			}
		}
		if (isNE(ustsIO.getBatctrcde(), "B633")
		|| isEQ(wsaaZrstFound, SPACES)) {
			/*    ELSE                                                         */
			if (isEQ(chdrulsIO.getBillfreq(), "00")) {
				wsaaSacstype.set(t5645rec.sacstype01);
			}
			else {
				wsaaSacstype.set(t5645rec.sacstype02);
			}
			if (isEQ(ustsIO.getSacstyp(), "EN")) {
				wsaaSacstype.set(ustsIO.getSacstyp());
			}
			b400ReadT3695();
			/*    MOVE WSAA-LONGDESC         TO TEXTFIELD OF RR541D01-O     */
			dbcstrncpy.dbcsInputString.set(wsaaLongdesc);
			dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
			dbcstrncpy.dbcsStatuz.set(SPACES);
			dbcsTrnc(dbcstrncpy.rec);
			if (isNE(dbcstrncpy.dbcsStatuz, "****")) {
				dbcstrncpy.dbcsOutputString.set(SPACES);
			}
			textfield.set(dbcstrncpy.dbcsOutputString);
			zrordpay.set(ustsIO.getContractAmount());
			zrbonprd1.setRounded(ustsIO.getNofDunits());
			wsaaTotalUnits.add(ustsIO.getNofDunits());
			zrbonprd2.setRounded(wsaaTotalUnits);
		}
		if (isGT(wsaaLinecount, wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr541s03(rr541s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		wsaaLinecount.add(1);
		printRecord.set(SPACES);
		printFile.printRr541d01(rr541d01Record);
	}

	/**
	* <pre>
	*    Read the next USTS record.
	* </pre>
	*/
protected void callUsts33a0()
	{
		ustsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustsIO);
		if (isNE(ustsIO.getStatuz(), varcom.oK)
		&& isNE(ustsIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustsIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void processUstnannTrans3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para13300();
				case updateUstnann33a0: 
					updateUstnann33a0();
				case callUstnann33a0: 
					callUstnann33a0();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para13300()
	{
		if (isNE(ustnannIO.getFeedbackInd(), "Y")) {
			goTo(GotoLabel.callUstnann33a0);
		}
		/* non-investment transaction                                      */
		if (isEQ(ustnannIO.getChdrnum(), ustfIO.getChdrnum())) {
			if (isEQ(ustnannIO.getUnitVirtualFund(), SPACES)
			&& isEQ(ustnannIO.getFundAmount(), ZERO)) {
				goTo(GotoLabel.updateUstnann33a0);
			}
		}
		/* Read next record if unit type of unit virtual fund differ       */
		if ((isNE(ustnannIO.getUnitVirtualFund(), ustfIO.getUnitVirtualFund()))
		|| (isNE(ustnannIO.getUnitType(), ustfIO.getUnitType()))) {
			goTo(GotoLabel.callUstnann33a0);
		}
		/* Read next record if component differ                            */
		if (isNE(ustnannIO.getLife(), ustfIO.getLife())
		|| isNE(ustnannIO.getCoverage(), ustfIO.getCoverage())
		|| isNE(ustnannIO.getRider(), ustfIO.getRider())) {
			goTo(GotoLabel.callUstnann33a0);
		}
		/* Read next record if transaction is not within printing range    */
		/* for subsequent anniversary year after the 1st anniv printing    */
		if (wsaaZstaFound.isTrue()) {
			if ((isLTE(ustnannIO.getMoniesDate(), zstaIO.getStmtOpDate()))
			|| (isGT(ustnannIO.getMoniesDate(), chdrlifIO.getStatementDate()))) {
				goTo(GotoLabel.callUstnann33a0);
			}
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		/*ILIFE-7870 starts*/
		if ((isEQ(ustfIO.getUnitVirtualFund(), SPACES)
				&& isEQ(ustfIO.getUnitType(), ZERO))|| isEQ(ustnannIO.getPriceDateUsed(),ZERO)) {
					return ;
				}
		/*ILIFE-7870 ends*/
		/*    Set up Transaction Details and write details line D01.       */
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ustnannIO.getMoniesDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		moniesdt.set(datcon1rec.extDate);
		/* To obtain bid price and offer price                             */
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(ustfIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(ustfIO.getUnitType());
		vprnudlIO.setEffdate(ustnannIO.getPriceDateUsed());
		vprnudlIO.setFunction(varcom.readr);
		vprnudlIO.setFormat(formatsInner.vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(vprnudlIO.getParams());
			databaseError006();
		}
		uoffpr.set(vprnudlIO.getUnitOfferPrice());
		ubidpr.set(vprnudlIO.getUnitBidPrice());
		if (isEQ(ustfIO.getPlanSuffix(), ZERO)
		&& isNE(chdrulsIO.getPolsum(), ZERO)) {
			compute(wsaaUnits, 5).set(div(ustnannIO.getNofDunits(), chdrulsIO.getPolsum()));
			compute(wsaaFundCash, 5).set(div(ustnannIO.getFundAmount(), chdrulsIO.getPolsum()));
			compute(wsaaContCash, 5).set(div(ustnannIO.getContractAmount(), chdrulsIO.getPolsum()));
			if (isNE(wsaaFundCash, 0)) {
				zrdecplrec.amountIn.set(wsaaFundCash);
				zrdecplrec.currency.set(t5515rec.currcode);
				callRounding5000();
				wsaaFundCash.set(zrdecplrec.amountOut);
			}
			if (isNE(wsaaContCash, 0)) {
				zrdecplrec.amountIn.set(wsaaContCash);
				zrdecplrec.currency.set(chdrulsIO.getCntcurr());
				callRounding5000();
				wsaaContCash.set(zrdecplrec.amountOut);
			}
			zrbonprd1.setRounded(wsaaUnits);
			wsaaTotalUnits.add(wsaaUnits);
			zrbonprd2.setRounded(wsaaTotalUnits);
			zrordpay.set(wsaaFundCash);
			wsaaTotalCash.add(wsaaFundCash);
		}
		else {
			/* Splitting mortality charges and fees                            */
			if (isEQ(ustnannIO.getBatctrcde(), "B633")) {
				zrstIO.setParams(SPACES);
				zrstIO.setChdrcoy(ustfIO.getChdrcoy());
				zrstIO.setChdrnum(ustfIO.getChdrnum());
				zrstIO.setLife(ustfIO.getLife());
				zrstIO.setCoverage(ustfIO.getCoverage());
				zrstIO.setJlife(SPACES);
				/*                                   ZRST-RIDER            <LA4286>*/
				zrstIO.setRider(ustfIO.getRider());
				zrstIO.setFormat(formatsInner.zrstrec);
				zrstIO.setFunction(varcom.begnh);
				while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
					b200ReadZrst();
				}
				
				goTo(GotoLabel.updateUstnann33a0);
			}
			else {
				if (isEQ(chdrulsIO.getBillfreq(), "00")) {
					wsaaSacstype.set(t5645rec.sacstype01);
				}
				else {
					wsaaSacstype.set(t5645rec.sacstype02);
				}
				if (isEQ(ustnannIO.getSacstyp(), "EN")) {
					wsaaSacstype.set(ustnannIO.getSacstyp());
				}
				b400ReadT3695();
				/*    MOVE WSAA-LONGDESC      TO TEXTFIELD OF RR541D01-O<LIF2.1>*/
				dbcstrncpy.dbcsInputString.set(wsaaLongdesc);
				dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
				dbcstrncpy.dbcsStatuz.set(SPACES);
				dbcsTrnc(dbcstrncpy.rec);
				if (isNE(dbcstrncpy.dbcsStatuz, "****")) {
					dbcstrncpy.dbcsOutputString.set(SPACES);
				}
				textfield.set(dbcstrncpy.dbcsOutputString);
				zrordpay.set(ustnannIO.getContractAmount());
				wsaaTotalCash.add(ustnannIO.getContractAmount());
				zrbonprd1.setRounded(ustnannIO.getNofDunits());
				wsaaTotalUnits.add(ustnannIO.getNofDunits());
				zrbonprd2.setRounded(wsaaTotalUnits);
			}
		}
		if (isGT(wsaaLinecount, wsaaMaxLines)) {
			wsaaOverflow.set("Y");
			printFile.printRr541s03(rr541s03Record);
		}
		if (pageOverflow.isTrue()) {
			detailHeading3400();
		}
		wsaaLinecount.add(1);
		printRecord.set(SPACES);
		printFile.printRr541d01(rr541d01Record);
	}

protected void updateUstnann33a0()
	{
		wsaaExcptRecord.set("Y");
		setPrecision(ustnannIO.getUstmno(), 0);
		ustnannIO.setUstmno(add(1, ustfIO.getUstmno()));
		ustnannIO.setStrpdate(runparmrec.effdate);
		ustnannIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ustnannIO);
		if (isNE(ustnannIO.getStatuz(), varcom.oK)
		&& isNE(ustnannIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustnannIO.getParams());
			databaseError006();
		}
	}

	/**
	* <pre>
	*    Read the next USTNANN record.                                
	* </pre>
	*/
protected void callUstnann33a0()
	{
		ustnannIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ustnannIO);
		if (isNE(ustnannIO.getStatuz(), varcom.oK)
		&& isNE(ustnannIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(ustnannIO.getParams());
			databaseError006();
		}
		/*EXIT1*/
	}

protected void detailHeading3400()
	{
		para3400();
	}

protected void para3400()
	{
		wsaaOverflow.set("N");
		wsaaLinecount.set(21);
		wsaaPagenum.add(1);
		pagenum.set(wsaaPagenum);
		printRecord.set(SPACES);
//		printFile.printRr541h01(rr541h01Record);
		/*bug #ILIFE-764 start*/
		printFile.printRr541h01(rr541h01Record, indicArea);
		/*bug #ILIFE-764 end*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
		printRecord.set(SPACES);
		printFile.printRr541h02(rr541h02Record);
		printRecord.set(SPACES);
		printFile.printRr541h03(rr541h03Record);
	}

protected void extractDetails3500a()
	{
		extrDetails3500a();
	}

protected void extrDetails3500a()
	{
		/* address*/
		clntIO.setClntpfx(cltsIO.getClntpfx());
		clntIO.setClntcoy(cltsIO.getClntcoy());
		clntIO.setClntnum(lifeenqIO.getLifcnum());
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(formatsInner.clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(clntIO.getStatuz());
			conerrrec.params.set(clntIO.getParams());
			databaseError006();
		}
		addr1.set(SPACES);
		addr2.set(SPACES);
		addr3.set(SPACES);
		addr4.set(SPACES);
		addr5.set(SPACES);
		/* Write first line of address.*/
		addr1.set(clntIO.getCltaddr01());
		/* Write second line of address.*/
		if (isEQ(addr1, SPACES)) {
			addr1.set(clntIO.getCltaddr02());
		}
		else {
			addr2.set(clntIO.getCltaddr02());
		}
		/* Write third line of address.*/
		if (isEQ(addr1, SPACES)) {
			addr1.set(clntIO.getCltaddr03());
		}
		else {
			if (isEQ(addr2, SPACES)) {
				addr2.set(clntIO.getCltaddr03());
			}
			else {
				addr3.set(clntIO.getCltaddr03());
			}
		}
		/* Write fourth line of address.*/
		if (isEQ(addr1, SPACES)) {
			addr1.set(clntIO.getCltaddr04());
		}
		else {
			if (isEQ(addr2, SPACES)) {
				addr2.set(clntIO.getCltaddr04());
			}
			else {
				if (isEQ(addr3, SPACES)) {
					addr3.set(clntIO.getCltaddr04());
				}
				else {
					addr4.set(clntIO.getCltaddr04());
				}
			}
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(cltsIO.getClntcoy());
		descIO.setDesctabl(tablesInner.t3645);
		descIO.setDescitem(clntIO.getCtrycode());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		/* Write fifth line of address.*/
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			addr5.fill("?");
		}
		else {
			if (isEQ(addr1, SPACES)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(descIO.getLongdesc(), "  ");
				stringVariable1.addExpression(" ");
				stringVariable1.addExpression(clntIO.getCltpcode(), " ");
				stringVariable1.setStringInto(addr1);
			}
			else {
				if (isEQ(addr2, SPACES)) {
					StringUtil stringVariable2 = new StringUtil();
					stringVariable2.addExpression(descIO.getLongdesc(), "  ");
					stringVariable2.addExpression(" ");
					stringVariable2.addExpression(clntIO.getCltpcode(), " ");
					stringVariable2.setStringInto(addr2);
				}
				else {
					if (isEQ(addr3, SPACES)) {
						StringUtil stringVariable3 = new StringUtil();
						stringVariable3.addExpression(descIO.getLongdesc(), "  ");
						stringVariable3.addExpression(" ");
						stringVariable3.addExpression(clntIO.getCltpcode(), " ");
						stringVariable3.setStringInto(addr3);
					}
					else {
						if (isEQ(addr4, SPACES)) {
							StringUtil stringVariable4 = new StringUtil();
							stringVariable4.addExpression(descIO.getLongdesc(), "  ");
							stringVariable4.addExpression(" ");
							stringVariable4.addExpression(clntIO.getCltpcode(), " ");
							stringVariable4.setStringInto(addr4);
						}
						else {
							StringUtil stringVariable5 = new StringUtil();
							stringVariable5.addExpression(descIO.getLongdesc(), "  ");
							stringVariable5.addExpression(" ");
							stringVariable5.addExpression(clntIO.getCltpcode(), " ");
							stringVariable5.setStringInto(addr5);
						}
					}
				}
			}
		}
		/*obtain name of assured or assignee*/
		cltsIO.setClntnum(chdrulsIO.getCownnum());
		cltsIO.setClntcoy(runparmrec.fsuco);
		if(isEQ(chdrulsIO.getCownpfx(), SPACES)){
			cltsIO.setClntpfx(clntIO.getClntpfx());
		}else{
			cltsIO.setClntpfx(chdrulsIO.getCownpfx());
		}
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(cltsIO.getParams());
			databaseError006();
		}
		plainname();
		if (isEQ(wsspName[31], SPACES)
		&& isEQ(wsspName[32], SPACES)) {
			asgname1.set(wsspLongconfname);
		}
		else {
			if (isEQ(cltsIO.getEthorig(), "1")) {
				asgname1.set(cltsIO.getSurname());
				asgname2.set(cltsIO.getGivname());
			}
			else {
				asgname1.set(cltsIO.getGivname());
				asgname2.set(cltsIO.getSurname());
			}
		}
		/*To obtain mode description*/
		if (isEQ(chdrulsIO.getBillfreq(), "00")) {
			/*    MOVE 'SINGLE PREMIUM'     TO LNGDSC  OF RR541H01-O        */
			/*    MOVE TR386-PROGDESC-1     TO LNGDSC  OF RR541H01-O        */
			/*    MOVE TR386-PROGDESC-01    TO LNGDSC  OF RR541H01-O<LA3235>*/
			indOn.setTrue(1);
		}
		else {
			/*    MOVE 'INSTALMENT PREMIUM' TO LNGDSC  OF RR541H01-O        */
			/*    MOVE TR386-PROGDESC-2     TO LNGDSC  OF RR541H01-O        */
			/*    MOVE TR386-PROGDESC-02    TO LNGDSC  OF RR541H01-O<LA3235>*/
			indOff.setTrue(1);
		}
		/*To obtain sum assured*/
		wsaaValidRecord.set("N");
		wsaaSumins.set(ZERO);
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrulsIO.getChdrcoy());
		covrIO.setChdrnum(chdrulsIO.getChdrnum());
		covrIO.setLife(ustfIO.getLife());
		covrIO.setCoverage(ustfIO.getCoverage());
		covrIO.setRider(ustfIO.getRider());
		covrIO.setPlanSuffix(ustfIO.getPlanSuffix());
		covrIO.setFunction(varcom.begn);
		covrIO.setFormat(formatsInner.covrrec);
		while ( !((isEQ(covrIO.getStatuz(), varcom.endp)
		|| validRecord.isTrue()))) {
			readCovr3600a();
		}
		
		if (validRecord.isTrue()) {
			sumins.set(wsaaSumins);
		}
	}

protected void readCovr3600a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCovrRec3600a();
				case nextCovr3600a: 
					nextCovr3600a();
				case exit3600a: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCovrRec3600a()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(covrIO.getParams());
			databaseError006();
		}
		if ((isNE(chdrulsIO.getChdrcoy(), covrIO.getChdrcoy()))
		|| (isNE(chdrulsIO.getChdrnum(), covrIO.getChdrnum()))
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3600a);
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			goTo(GotoLabel.nextCovr3600a);
		}
		wsaaValidRecord.set("Y");
		wsaaSumins.set(covrIO.getSumins());
	}

protected void nextCovr3600a()
	{
		covrIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*To obtain Agent information
	* </pre>
	*/
protected void getAgent3700a()
	{
		readAgntenqRec3700a();
	}

protected void readAgntenqRec3700a()
	{
	
	if (isNE(chdrulsIO.getAgntcoy(),SPACES) && isNE(chdrulsIO.getAgntnum(),SPACES))
	{		
		agntenqIO.setParams(SPACES);
		agntenqIO.setAgntcoy(chdrulsIO.getAgntcoy());
		agntenqIO.setAgntnum(chdrulsIO.getAgntnum());
		agntenqIO.setFunction(varcom.readr);
		agntenqIO.setFormat(formatsInner.agntenqrec);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(agntenqIO.getParams());
			databaseError006();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntcoy(agntenqIO.getClntcoy());
		cltsIO.setClntpfx(chdrulsIO.getCownpfx());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(cltsIO.getParams());
			databaseError006();
		}
		plainname();
		despname1.set(wsspLongconfname);
		despname2.set(wsspLongconfname);
		cltphone.set(cltsIO.getCltphone01());
	}
	}

	/**
	* <pre>
	* Total value of Units = total units * bid price at closing
	* </pre>
	*/
protected void unitsValue3800a()
	{
		unitsValueProcess3800a();
	}

protected void unitsValueProcess3800a()
	{
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(ustfIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(ustfIO.getUnitType());
		vprnudlIO.setEffdate(chdrpfEntity.getStmdte());
		vprnudlIO.setFunction(varcom.begn);
		vprnudlIO.setFormat(formatsInner.vprnudlrec);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(), varcom.oK)
		&& isNE(vprnudlIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(vprnudlIO.getParams());
			databaseError006();
		}
		compute(wsaaTotnofunts, 6).setRounded(add(wsaaTotnofunts, (mult(wsaaCloBalUnits, vprnudlIO.getUnitBidPrice()))));
		/* MOVE WSAA-TOTNOFUNTS     TO ZRDP-AMOUNT-IN.                  */
		/* MOVE T5515-CURRCODE      TO ZRDP-CURRENCY.                   */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT     TO WSAA-TOTNOFUNTS.                 */
		if (isNE(wsaaTotnofunts, 0)) {
			zrdecplrec.amountIn.set(wsaaTotnofunts);
			zrdecplrec.currency.set(t5515rec.currcode);
			callRounding5000();
			wsaaTotnofunts.set(zrdecplrec.amountOut);
		}
	}

protected void readZsta3900a()
	{
		beginZsta3900a();
	}

protected void beginZsta3900a()
	{
		wsaaZstaRecord.set("N");
		zstaIO.setChdrcoy(ustfIO.getChdrcoy());
		zstaIO.setChdrnum(ustfIO.getChdrnum());
		zstaIO.setUstmno(99999);
		zstaIO.setPlanSuffix(ustfIO.getPlanSuffix());
		zstaIO.setLife(ustfIO.getLife());
		zstaIO.setCoverage(ustfIO.getCoverage());
		zstaIO.setRider(ustfIO.getRider());
		zstaIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		zstaIO.setUnitType(ustfIO.getUnitType());
		zstaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, zstaIO);
		if (isNE(zstaIO.getStatuz(), varcom.oK)
		&& isNE(zstaIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(zstaIO.getParams());
			databaseError006();
		}
		while ( !((isNE(ustfIO.getChdrcoy(), zstaIO.getChdrcoy()))
		|| (isNE(ustfIO.getChdrnum(), zstaIO.getChdrnum()))
		|| wsaaZstaFound.isTrue()
		|| isEQ(zstaIO.getStatuz(), varcom.endp))) {
			processZsta3910a();
		}
		
		if (wsaaZstaNotFound.isTrue()) {
			wsaaStmtOpDate.set(chdrlifIO.getOccdate());
			wsaaStmtOpDunits.set(ZERO);
		}
	}

protected void processZsta3910a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					procZsta3910a();
				case nextZsta3910a: 
					nextZsta3910a();
				case exit3910a: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void procZsta3910a()
	{
		if ((isNE(zstaIO.getUnitVirtualFund(), ustfIO.getUnitVirtualFund()))
		|| (isNE(zstaIO.getUnitType(), ustfIO.getUnitType()))) {
			goTo(GotoLabel.nextZsta3910a);
		}
		if (isNE(zstaIO.getLife(), ustfIO.getLife())
		|| isNE(zstaIO.getCoverage(), ustfIO.getCoverage())
		|| isNE(zstaIO.getRider(), ustfIO.getRider())) {
			goTo(GotoLabel.nextZsta3910a);
		}
		wsaaZstaRecord.set("Y");
		wsaaStmtOpDate.set(zstaIO.getStmtClDate());
		wsaaStmtOpDunits.set(zstaIO.getStmtClDunits());
		wsaaZstaStoreKey.set(zstaIO.getDataKey());
		goTo(GotoLabel.exit3910a);
	}

protected void nextZsta3910a()
	{
		zstaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, zstaIO);
		if (isNE(zstaIO.getStatuz(), varcom.oK)
		&& isNE(zstaIO.getStatuz(), varcom.endp)) {
			conerrrec.dbparams.set(zstaIO.getParams());
			databaseError006();
		}
	}

protected void createZsta3950a()
	{
		createReocrd3950a();
	}

protected void createReocrd3950a()
	{
		zstaIO.setChdrcoy(ustfIO.getChdrcoy());
		zstaIO.setChdrnum(ustfIO.getChdrnum());
		zstaIO.setUstmno(ustfIO.getUstmno());
		zstaIO.setPlanSuffix(ustfIO.getPlanSuffix());
		zstaIO.setLife(ustfIO.getLife());
		zstaIO.setCoverage(ustfIO.getCoverage());
		zstaIO.setRider(ustfIO.getRider());
		zstaIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		zstaIO.setUnitType(ustfIO.getUnitType());
		zstaIO.setStmtOpDate(wsaaStmtOpDate);
		zstaIO.setStmtOpDunits(wsaaStmtOpDunits);
		zstaIO.setStmtClDate(wsaaStmtClDate);
		zstaIO.setStmtClDunits(wsaaStmtClDunits);
		zstaIO.setFunction(varcom.writr);
		zstaIO.setFormat(formatsInner.zstarec);
		SmartFileCode.execute(appVars, zstaIO);
		if (isNE(zstaIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(zstaIO.getStatuz());
			conerrrec.params.set(zstaIO.getParams());
			databaseError006();
		}
	}

protected void rewrtZsta3970a()
	{
		rewrtRecord3970a();
	}

protected void rewrtRecord3970a()
	{
		zstaIO.setChdrcoy(wsaaZstaStChdrcoy);
		zstaIO.setChdrnum(wsaaZstaStChdrnum);
		zstaIO.setUstmno(wsaaZstaStUstmno);
		zstaIO.setPlanSuffix(wsaaZstaStPlanSuffix);
		zstaIO.setLife(wsaaZstaStLife);
		zstaIO.setCoverage(wsaaZstaStCoverage);
		zstaIO.setRider(wsaaZstaStRider);
		zstaIO.setUnitVirtualFund(wsaaZstaStUnitVirtualFund);
		zstaIO.setUnitType(wsaaZstaStUnitType);
		zstaIO.setStmtOpDate(wsaaStmtOpDate);
		zstaIO.setStmtOpDunits(wsaaStmtOpDunits);
		zstaIO.setStmtClDate(wsaaStmtClDate);
		zstaIO.setStmtClDunits(wsaaStmtClDunits);
		zstaIO.setFunction(varcom.updat);
		zstaIO.setFormat(formatsInner.zstarec);
		SmartFileCode.execute(appVars, zstaIO);
		if (isNE(zstaIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(zstaIO.getStatuz());
			conerrrec.params.set(zstaIO.getParams());
			databaseError006();
		}
	}

protected void b100ReadZrst()
	{
		b110ReadZrst();
	}

protected void b110ReadZrst()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(zrstIO.getParams());
			databaseError006();
		}
		if (isNE(zrstIO.getChdrcoy(), ustfIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), ustfIO.getChdrnum())
		|| isNE(zrstIO.getLife(), ustfIO.getLife())
		|| isNE(zrstIO.getCoverage(), ustfIO.getCoverage())
		|| isNE(zrstIO.getRider(), ustfIO.getRider())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			zrstIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstIO.getXtranno(), ustsIO.getTranno())
		&& isEQ(zrstIO.getFeedbackInd(), "Y")) {
			wsaaZrstFound.set("Y");
			/* To get description from T5687 if Mortality Charge is for Riders*/
			if (isNE(zrstIO.getRider(), "00")
			&& isEQ(zrstIO.getSacstyp(), "MC")) {
				b500ReadT5687();
			}
			else {
				wsaaSacstype.set(zrstIO.getSacstyp());
				b400ReadT3695();
			}
			/*     MOVE WSAA-LONGDESC      TO TEXTFIELD OF RR541D01-O       */
			dbcstrncpy.dbcsInputString.set(wsaaLongdesc);
			dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
			dbcstrncpy.dbcsStatuz.set(SPACES);
			dbcsTrnc(dbcstrncpy.rec);
			if (isNE(dbcstrncpy.dbcsStatuz, "****")) {
				dbcstrncpy.dbcsOutputString.set(SPACES);
			}
			textfield.set(dbcstrncpy.dbcsOutputString);
			compute(wsaaFees, 6).setRounded((div((mult(ustsIO.getContractAmount(), zrstIO.getZramount01())), zrstIO.getZramount02())));
			if (isNE(wsaaFees, 0)) {
				zrdecplrec.amountIn.set(wsaaFees);
				zrdecplrec.currency.set(chdrulsIO.getCntcurr());
				callRounding5000();
				wsaaFees.set(zrdecplrec.amountOut);
			}
			zrordpay.setRounded(wsaaFees);
			compute(wsaaUnits, 6).setRounded((div(wsaaFees, vprnudlIO.getUnitBidPrice())));
			zrbonprd1.setRounded(wsaaUnits);
			wsaaTotalUnits.add(wsaaUnits);
			zrbonprd2.setRounded(wsaaTotalUnits);
			if (isGT(wsaaLinecount, wsaaMaxLines)) {
				wsaaOverflow.set("Y");
				printFile.printRr541s03(rr541s03Record);
			}
			if (pageOverflow.isTrue()) {
				detailHeading3400();
			}
			printFile.printRr541d01(rr541d01Record);
			wsaaLinecount.add(1);
		}
		zrstIO.setFunction(varcom.nextr);
	}

protected void b200ReadZrst()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b210ReadZrst();
				case b280Nextr: 
					b280Nextr();
				case b290Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b210ReadZrst()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(zrstIO.getParams());
			databaseError006();
		}
		if (isNE(zrstIO.getChdrcoy(), ustfIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), ustfIO.getChdrnum())
		|| isNE(zrstIO.getLife(), ustfIO.getLife())
		|| isNE(zrstIO.getCoverage(), ustfIO.getCoverage())
		|| isNE(zrstIO.getRider(), ustfIO.getRider())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			zrstIO.setStatuz(varcom.endp);
			goTo(GotoLabel.b290Exit);
		}
		if (isNE(zrstIO.getUstmno(), ZERO)) {
			goTo(GotoLabel.b280Nextr);
		}
		/*    IF  ZRST-XTRANNO            = USTS-TRANNO            <LA4286>*/
		/*    IF  ZRST-TRANNO             < USTNANN-TRANNO                 */
		/*    AND ZRST-TRANNO             > WSAA-PREV-TRANNO               */
		if (isEQ(zrstIO.getXtranno(), ustnannIO.getTranno())
		&& isEQ(zrstIO.getFeedbackInd(), "Y")) {
			/* To get description from T5687 if Mortality Charge is for Riders */
			if (isNE(zrstIO.getRider(), "00")
			&& isEQ(zrstIO.getSacstyp(), "MC")) {
				b500ReadT5687();
			}
			else {
				wsaaSacstype.set(zrstIO.getSacstyp());
				b400ReadT3695();
			}
			/*     MOVE WSAA-LONGDESC     TO TEXTFIELD OF RR541D01-O<LIF2.1>*/
			dbcstrncpy.dbcsInputString.set(wsaaLongdesc);
			dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
			dbcstrncpy.dbcsStatuz.set(SPACES);
			dbcsTrnc(dbcstrncpy.rec);
			if (isNE(dbcstrncpy.dbcsStatuz, "****")) {
				dbcstrncpy.dbcsOutputString.set(SPACES);
			}
			textfield.set(dbcstrncpy.dbcsOutputString);
			compute(wsaaFees, 6).setRounded((div((mult(ustnannIO.getContractAmount(), zrstIO.getZramount01())), zrstIO.getZramount02())));
			if (isNE(wsaaFees, 0)) {
				zrdecplrec.amountIn.set(wsaaFees);
				zrdecplrec.currency.set(chdrulsIO.getCntcurr());
				callRounding5000();
				wsaaFees.set(zrdecplrec.amountOut);
			}
			zrordpay.setRounded(wsaaFees);
			compute(wsaaUnits, 6).setRounded((div(wsaaFees, vprnudlIO.getUnitBidPrice())));
			zrbonprd1.setRounded(wsaaUnits);
			wsaaTotalUnits.add(wsaaUnits);
			wsaaTotalCash.add(wsaaFees);
			wsaaUstnannContractAmt.add(ustnannIO.getContractAmount());
			zrbonprd2.setRounded(wsaaTotalUnits);
			if (isGT(wsaaLinecount, wsaaMaxLines)) {
				wsaaOverflow.set("Y");
				printFile.printRr541s03(rr541s03Record);
			}
			if (pageOverflow.isTrue()) {
				detailHeading3400();
			}
			printFile.printRr541d01(rr541d01Record);
			wsaaLinecount.add(1);
			/* This is to cater for multiple funds. There is only one record   */
			/* in ZRST regardless on the number of funds for a contract.       */
			/* So, the USTMNO should only be updated if there is no more B633  */
			/* in UTRN.                                                        */
			if ((setPrecision(wsaaUstnannContractAmt, 2)
			&& isEQ(wsaaUstnannContractAmt, (mult(zrstIO.getZramount02(), -1))))) {
				setPrecision(zrstIO.getUstmno(), 0);
				zrstIO.setUstmno(add(1, ustfIO.getUstmno()));
				zrstIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, zrstIO);
				if (isNE(zrstIO.getStatuz(), varcom.oK)
				&& isNE(zrstIO.getStatuz(), varcom.endp)) {
					conerrrec.params.set(zrstIO.getParams());
					databaseError006();
				}
				wsaaUstnannContractAmt.set(ZERO);
			}
		}
	}

protected void b280Nextr()
	{
		zrstIO.setFunction(varcom.nextr);
	}

protected void b300ReadT5645()
	{
		b300Para();
	}

protected void b300Para()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.dbparams.set(itemIO.getParams());
			databaseError006();
		}
		if (isNE(itemIO.getItemcoy(), runparmrec.company)
		|| isNE(itemIO.getItemtabl(), tablesInner.t5645)
		|| isNE(itemIO.getItemitem(), wsaaProg)) {
			conerrrec.dbparams.set(itemIO.getParams());
			databaseError006();
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
	}

	/**
	* <pre>
	*B310-READ-TR386 SECTION.                                 <LA3235>
	*B310-PARA.                                               <LA3235>
	**** MOVE SPACES                 TO ITEM-DATA-KEY.        <LA3235>
	**** MOVE 'IT'                   TO ITEM-ITEMPFX.         <LA3235>
	**** MOVE PARM-COMPANY           TO ITEM-ITEMCOY.         <LA3235>
	**** MOVE TR386                  TO ITEM-ITEMTABL.        <LA3235>
	**** MOVE PARM-LANGUAGE          TO ITEM-ITEMITEM.        <LA3235>
	**** MOVE WSAA-PROG              TO ITEM-ITEMITEM(2:5).   <LA3235>
	**** MOVE READR                  TO ITEM-FUNCTION.        <LA3235>
	****                                                      <LA3235>
	**** CALL 'ITEMIO'               USING ITEM-PARAMS.       <LA3235>
	**** IF  ITEM-STATUZ             NOT = O-K                <LA3235>
	****     PERFORM 006-DATABASE-ERROR                       <LA3235>
	**** END-IF.                                              <LA3235>
	****                                                      <LA3235>
	**** MOVE ITEM-GENAREA           TO TR386-TR386-REC.      <LA3235>
	****                                                      <LA3235>
	*B310-EXIT.                                               <LA3235>
	**** EXIT.                                                <LA3235>
	* </pre>
	*/
protected void b400ReadT3695()
	{
		b400Para();
	}

protected void b400Para()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setLanguage(runparmrec.language);
		descIO.setDesctabl(tablesInner.t3695);
		descIO.setDescitem(wsaaSacstype);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isNE(descIO.getDesccoy(), runparmrec.company)
		|| isNE(descIO.getDesctabl(), tablesInner.t3695)
		|| isNE(descIO.getDescitem(), wsaaSacstype)) {
			wsaaLongdesc.set("NOT FOUND IN T3695");
		}
		else {
			wsaaLongdesc.set(descIO.getShortdesc());
		}
	}

protected void b500ReadT5687()
	{
		b510Para();
	}

protected void b510Para()
	{
		/* Read COVR to find out the Rider Code.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(zrstIO.getChdrcoy());
		covrIO.setChdrnum(zrstIO.getChdrnum());
		covrIO.setLife(zrstIO.getLife());
		covrIO.setCoverage(zrstIO.getCoverage());
		covrIO.setRider(zrstIO.getRider());
		covrIO.setPlanSuffix(ustfIO.getPlanSuffix());
		covrIO.setFunction(varcom.readr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isEQ(covrIO.getStatuz(), varcom.mrnf)) {
			conerrrec.dbparams.set(covrIO.getParams());
			databaseError006();
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setLanguage(runparmrec.language);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrIO.getCrtable());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaLongdesc.set(SPACES);
		}
		else {
			wsaaLongdesc.set(descIO.getShortdesc());
		}
	}

protected void b600UpdateUstfstm()
	{
		b610Para();
	}

protected void b610Para()
	{
		/* Delete the old USTF record.                                     */
		ustfstmIO.setParams(SPACES);
		ustfstmIO.setChdrcoy(ustfIO.getChdrcoy());
		ustfstmIO.setChdrnum(ustfIO.getChdrnum());
		ustfstmIO.setUstmno(99999);
		ustfstmIO.setPlanSuffix(ustfIO.getPlanSuffix());
		ustfstmIO.setLife(ustfIO.getLife());
		ustfstmIO.setCoverage(ustfIO.getCoverage());
		ustfstmIO.setRider(ustfIO.getRider());
		ustfstmIO.setUnitVirtualFund(ustfIO.getUnitVirtualFund());
		ustfstmIO.setUnitType(ustfIO.getUnitType());
		ustfstmIO.setFormat(formatsInner.ustfrec);
		ustfstmIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, ustfstmIO);
		if (isNE(ustfstmIO.getStatuz(), varcom.oK)
		&& isNE(ustfstmIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(ustfstmIO.getParams());
			databaseError006();
		}
		ustfstmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ustfstmIO);
		if (isNE(ustfstmIO.getStatuz(), varcom.oK)
		&& isNE(ustfstmIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(ustfstmIO.getParams());
			databaseError006();
		}
		/* Create a new USTF with the new USTMNO.                          */
		setPrecision(ustfstmIO.getUstmno(), 0);
		ustfstmIO.setUstmno(add(1, ustfIO.getUstmno()));
		ustfstmIO.setStmtClUnits(wsaaTotalUnits);
		ustfstmIO.setStmtClDunits(wsaaTotalUnits);
		ustfstmIO.setStmtTrnUnits(wsaaTotalUnits);
		ustfstmIO.setStmtTrnDunits(wsaaTotalUnits);
		setPrecision(ustfstmIO.getStmtClFundCash(), 5);
		ustfstmIO.setStmtClFundCash(add(ustfstmIO.getStmtClFundCash(), wsaaTotalCash));
		setPrecision(ustfstmIO.getStmtClContCash(), 5);
		ustfstmIO.setStmtClContCash(add(ustfstmIO.getStmtClContCash(), wsaaTotalCash));
		setPrecision(ustfstmIO.getStmtTrFundCash(), 5);
		ustfstmIO.setStmtTrFundCash(add(ustfstmIO.getStmtTrFundCash(), wsaaTotalCash));
		setPrecision(ustfstmIO.getStmtTrContCash(), 5);
		ustfstmIO.setStmtTrContCash(add(ustfstmIO.getStmtTrContCash(), wsaaTotalCash));
		ustfstmIO.setStmtClDate(runparmrec.effdate);
		ustfstmIO.setFormat(formatsInner.ustfrec);
		ustfstmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ustfstmIO);
		if (isNE(ustfstmIO.getStatuz(), varcom.oK)
		&& isNE(ustfstmIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(ustfstmIO.getParams());
			databaseError006();
		}
	}

protected void validatePremStatus2500()
	{
		/*PARA*/
		if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()], chdrpfEntity.getPstcde())) {
			wsaaIndex.set(13);
			wsaaValidStatus.set("Y");
		}
		/*EXIT*/
	}

protected void softlockReadChdr3900()
	{
		para3910();
	}

protected void para3910()
	{
		/* Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(runparmrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpfEntity.getChdrnum());
		sftlockrec.transaction.set(wsaaRunparm1TranCode);
		sftlockrec.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz, varcom.oK))
		&& (isNE(sftlockrec.statuz, "LOCK"))) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			return ;
		}
		/* Read and hold the CHDRLIF record.*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(chdrpfEntity.getChdrcoy());
		chdrlifIO.setChdrnum(chdrpfEntity.getChdrnum());
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
	}

protected void unlockRewriteChdr4000()
	{
		para4010();
	}

protected void para4010()
	{
		/* Rewrite CHDRLIF record.*/
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		/* Undone soft lock.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(runparmrec.company);
		sftlockrec.entity.set(chdrpfEntity.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(runparmrec.user);
		sftlockrec.transaction.set(wsaaRunparm1TranCode);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(sftlockrec.statuz);
			conerrrec.params.set(sftlockrec.sftlockRec);
			systemError005();
		}
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(runparmrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(runparmrec.runparm1);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(zrdecplrec.statuz);
			conerrrec.params.set(zrdecplrec.zrdecplRec);
			systemError005();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t6649 = new FixedLengthStringData(5).init("T6649");
	private FixedLengthStringData t3645 = new FixedLengthStringData(5).init("T3645");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t3695 = new FixedLengthStringData(5).init("T3695");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 

	private FixedLengthStringData formats = new FixedLengthStringData(177);
	private FixedLengthStringData agntenqrec = new FixedLengthStringData(10).isAPartOf(formats, 0).init("AGNTREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).isAPartOf(formats, 10).init("CHDRLIFREC");
	private FixedLengthStringData chdrulsrec = new FixedLengthStringData(10).isAPartOf(formats, 20).init("CHDRULSREC");
	private FixedLengthStringData chdrrec = new FixedLengthStringData(10).isAPartOf(formats, 30).init("CHDRREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).isAPartOf(formats, 40).init("CLTSREC");
	private FixedLengthStringData clntrec = new FixedLengthStringData(10).isAPartOf(formats, 50).init("CLNTREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).isAPartOf(formats, 60).init("COVRREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).isAPartOf(formats, 70).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).isAPartOf(formats, 80).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).isAPartOf(formats, 87).init("ITDMREC");
	private FixedLengthStringData ustfrec = new FixedLengthStringData(10).isAPartOf(formats, 97).init("USTFREC");
	private FixedLengthStringData ustsrec = new FixedLengthStringData(10).isAPartOf(formats, 107).init("USTSREC");
	private FixedLengthStringData zstarec = new FixedLengthStringData(10).isAPartOf(formats, 117).init("ZSTAREC");
	private FixedLengthStringData utrnrnlrec = new FixedLengthStringData(10).isAPartOf(formats, 127).init("UTRNRNLREC");
	private FixedLengthStringData vprnudlrec = new FixedLengthStringData(10).isAPartOf(formats, 137).init("VPRNUDLREC");
	private FixedLengthStringData sftlockrec = new FixedLengthStringData(10).isAPartOf(formats, 147).init("SFTLOCKREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).isAPartOf(formats, 157).init("ZRSTREC");
	private FixedLengthStringData ustnannrec = new FixedLengthStringData(10).isAPartOf(formats, 167).init("USTNANNREC");
}
}
