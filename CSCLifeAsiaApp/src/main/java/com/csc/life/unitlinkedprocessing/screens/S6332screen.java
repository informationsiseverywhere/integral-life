package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6332screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6332ScreenVars sv = (S6332ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6332screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6332ScreenVars screenVars = (S6332ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.sumInsMin.setClassString("");
		screenVars.premmult.setClassString("");
		screenVars.sumInsMax.setClassString("");
		screenVars.rsvflg.setClassString("");
		screenVars.liencd01.setClassString("");
		screenVars.liencd02.setClassString("");
		screenVars.liencd03.setClassString("");
		screenVars.liencd04.setClassString("");
		screenVars.liencd05.setClassString("");
		screenVars.liencd06.setClassString("");
	}

/**
 * Clear all the variables in S6332screen
 */
	public static void clear(VarModel pv) {
		S6332ScreenVars screenVars = (S6332ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.sumInsMin.clear();
		screenVars.premmult.clear();
		screenVars.sumInsMax.clear();
		screenVars.rsvflg.clear();
		screenVars.liencd01.clear();
		screenVars.liencd02.clear();
		screenVars.liencd03.clear();
		screenVars.liencd04.clear();
		screenVars.liencd05.clear();
		screenVars.liencd06.clear();
	}
}
