package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:32
 * Description:
 * Copybook name: T5537REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5537rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5537Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(30).isAPartOf(t5537Rec, 0);
  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(10, 3, 0, ageIssageTos, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData ageIssageTo09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData ageIssageTo10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public FixedLengthStringData actAllocBasiss = new FixedLengthStringData(360).isAPartOf(t5537Rec, 30);
  	public FixedLengthStringData[] actAllocBasis = FLSArrayPartOfStructure(90, 4, actAllocBasiss, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(360).isAPartOf(actAllocBasiss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData actAllocBasis01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData actAllocBasis02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData actAllocBasis03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData actAllocBasis04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData actAllocBasis05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData actAllocBasis06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData actAllocBasis07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData actAllocBasis08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData actAllocBasis09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData actAllocBasis10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData actAllocBasis11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData actAllocBasis12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData actAllocBasis13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
  	public FixedLengthStringData actAllocBasis14 = new FixedLengthStringData(4).isAPartOf(filler1, 52);
  	public FixedLengthStringData actAllocBasis15 = new FixedLengthStringData(4).isAPartOf(filler1, 56);
  	public FixedLengthStringData actAllocBasis16 = new FixedLengthStringData(4).isAPartOf(filler1, 60);
  	public FixedLengthStringData actAllocBasis17 = new FixedLengthStringData(4).isAPartOf(filler1, 64);
  	public FixedLengthStringData actAllocBasis18 = new FixedLengthStringData(4).isAPartOf(filler1, 68);
  	public FixedLengthStringData actAllocBasis19 = new FixedLengthStringData(4).isAPartOf(filler1, 72);
  	public FixedLengthStringData actAllocBasis20 = new FixedLengthStringData(4).isAPartOf(filler1, 76);
  	public FixedLengthStringData actAllocBasis21 = new FixedLengthStringData(4).isAPartOf(filler1, 80);
  	public FixedLengthStringData actAllocBasis22 = new FixedLengthStringData(4).isAPartOf(filler1, 84);
  	public FixedLengthStringData actAllocBasis23 = new FixedLengthStringData(4).isAPartOf(filler1, 88);
  	public FixedLengthStringData actAllocBasis24 = new FixedLengthStringData(4).isAPartOf(filler1, 92);
  	public FixedLengthStringData actAllocBasis25 = new FixedLengthStringData(4).isAPartOf(filler1, 96);
  	public FixedLengthStringData actAllocBasis26 = new FixedLengthStringData(4).isAPartOf(filler1, 100);
  	public FixedLengthStringData actAllocBasis27 = new FixedLengthStringData(4).isAPartOf(filler1, 104);
  	public FixedLengthStringData actAllocBasis28 = new FixedLengthStringData(4).isAPartOf(filler1, 108);
  	public FixedLengthStringData actAllocBasis29 = new FixedLengthStringData(4).isAPartOf(filler1, 112);
  	public FixedLengthStringData actAllocBasis30 = new FixedLengthStringData(4).isAPartOf(filler1, 116);
  	public FixedLengthStringData actAllocBasis31 = new FixedLengthStringData(4).isAPartOf(filler1, 120);
  	public FixedLengthStringData actAllocBasis32 = new FixedLengthStringData(4).isAPartOf(filler1, 124);
  	public FixedLengthStringData actAllocBasis33 = new FixedLengthStringData(4).isAPartOf(filler1, 128);
  	public FixedLengthStringData actAllocBasis34 = new FixedLengthStringData(4).isAPartOf(filler1, 132);
  	public FixedLengthStringData actAllocBasis35 = new FixedLengthStringData(4).isAPartOf(filler1, 136);
  	public FixedLengthStringData actAllocBasis36 = new FixedLengthStringData(4).isAPartOf(filler1, 140);
  	public FixedLengthStringData actAllocBasis37 = new FixedLengthStringData(4).isAPartOf(filler1, 144);
  	public FixedLengthStringData actAllocBasis38 = new FixedLengthStringData(4).isAPartOf(filler1, 148);
  	public FixedLengthStringData actAllocBasis39 = new FixedLengthStringData(4).isAPartOf(filler1, 152);
  	public FixedLengthStringData actAllocBasis40 = new FixedLengthStringData(4).isAPartOf(filler1, 156);
  	public FixedLengthStringData actAllocBasis41 = new FixedLengthStringData(4).isAPartOf(filler1, 160);
  	public FixedLengthStringData actAllocBasis42 = new FixedLengthStringData(4).isAPartOf(filler1, 164);
  	public FixedLengthStringData actAllocBasis43 = new FixedLengthStringData(4).isAPartOf(filler1, 168);
  	public FixedLengthStringData actAllocBasis44 = new FixedLengthStringData(4).isAPartOf(filler1, 172);
  	public FixedLengthStringData actAllocBasis45 = new FixedLengthStringData(4).isAPartOf(filler1, 176);
  	public FixedLengthStringData actAllocBasis46 = new FixedLengthStringData(4).isAPartOf(filler1, 180);
  	public FixedLengthStringData actAllocBasis47 = new FixedLengthStringData(4).isAPartOf(filler1, 184);
  	public FixedLengthStringData actAllocBasis48 = new FixedLengthStringData(4).isAPartOf(filler1, 188);
  	public FixedLengthStringData actAllocBasis49 = new FixedLengthStringData(4).isAPartOf(filler1, 192);
  	public FixedLengthStringData actAllocBasis50 = new FixedLengthStringData(4).isAPartOf(filler1, 196);
  	public FixedLengthStringData actAllocBasis51 = new FixedLengthStringData(4).isAPartOf(filler1, 200);
  	public FixedLengthStringData actAllocBasis52 = new FixedLengthStringData(4).isAPartOf(filler1, 204);
  	public FixedLengthStringData actAllocBasis53 = new FixedLengthStringData(4).isAPartOf(filler1, 208);
  	public FixedLengthStringData actAllocBasis54 = new FixedLengthStringData(4).isAPartOf(filler1, 212);
  	public FixedLengthStringData actAllocBasis55 = new FixedLengthStringData(4).isAPartOf(filler1, 216);
  	public FixedLengthStringData actAllocBasis56 = new FixedLengthStringData(4).isAPartOf(filler1, 220);
  	public FixedLengthStringData actAllocBasis57 = new FixedLengthStringData(4).isAPartOf(filler1, 224);
  	public FixedLengthStringData actAllocBasis58 = new FixedLengthStringData(4).isAPartOf(filler1, 228);
  	public FixedLengthStringData actAllocBasis59 = new FixedLengthStringData(4).isAPartOf(filler1, 232);
  	public FixedLengthStringData actAllocBasis60 = new FixedLengthStringData(4).isAPartOf(filler1, 236);
  	public FixedLengthStringData actAllocBasis61 = new FixedLengthStringData(4).isAPartOf(filler1, 240);
  	public FixedLengthStringData actAllocBasis62 = new FixedLengthStringData(4).isAPartOf(filler1, 244);
  	public FixedLengthStringData actAllocBasis63 = new FixedLengthStringData(4).isAPartOf(filler1, 248);
  	public FixedLengthStringData actAllocBasis64 = new FixedLengthStringData(4).isAPartOf(filler1, 252);
  	public FixedLengthStringData actAllocBasis65 = new FixedLengthStringData(4).isAPartOf(filler1, 256);
  	public FixedLengthStringData actAllocBasis66 = new FixedLengthStringData(4).isAPartOf(filler1, 260);
  	public FixedLengthStringData actAllocBasis67 = new FixedLengthStringData(4).isAPartOf(filler1, 264);
  	public FixedLengthStringData actAllocBasis68 = new FixedLengthStringData(4).isAPartOf(filler1, 268);
  	public FixedLengthStringData actAllocBasis69 = new FixedLengthStringData(4).isAPartOf(filler1, 272);
  	public FixedLengthStringData actAllocBasis70 = new FixedLengthStringData(4).isAPartOf(filler1, 276);
  	public FixedLengthStringData actAllocBasis71 = new FixedLengthStringData(4).isAPartOf(filler1, 280);
  	public FixedLengthStringData actAllocBasis72 = new FixedLengthStringData(4).isAPartOf(filler1, 284);
  	public FixedLengthStringData actAllocBasis73 = new FixedLengthStringData(4).isAPartOf(filler1, 288);
  	public FixedLengthStringData actAllocBasis74 = new FixedLengthStringData(4).isAPartOf(filler1, 292);
  	public FixedLengthStringData actAllocBasis75 = new FixedLengthStringData(4).isAPartOf(filler1, 296);
  	public FixedLengthStringData actAllocBasis76 = new FixedLengthStringData(4).isAPartOf(filler1, 300);
  	public FixedLengthStringData actAllocBasis77 = new FixedLengthStringData(4).isAPartOf(filler1, 304);
  	public FixedLengthStringData actAllocBasis78 = new FixedLengthStringData(4).isAPartOf(filler1, 308);
  	public FixedLengthStringData actAllocBasis79 = new FixedLengthStringData(4).isAPartOf(filler1, 312);
  	public FixedLengthStringData actAllocBasis80 = new FixedLengthStringData(4).isAPartOf(filler1, 316);
  	public FixedLengthStringData actAllocBasis81 = new FixedLengthStringData(4).isAPartOf(filler1, 320);
  	public FixedLengthStringData actAllocBasis82 = new FixedLengthStringData(4).isAPartOf(filler1, 324);
  	public FixedLengthStringData actAllocBasis83 = new FixedLengthStringData(4).isAPartOf(filler1, 328);
  	public FixedLengthStringData actAllocBasis84 = new FixedLengthStringData(4).isAPartOf(filler1, 332);
  	public FixedLengthStringData actAllocBasis85 = new FixedLengthStringData(4).isAPartOf(filler1, 336);
  	public FixedLengthStringData actAllocBasis86 = new FixedLengthStringData(4).isAPartOf(filler1, 340);
  	public FixedLengthStringData actAllocBasis87 = new FixedLengthStringData(4).isAPartOf(filler1, 344);
  	public FixedLengthStringData actAllocBasis88 = new FixedLengthStringData(4).isAPartOf(filler1, 348);
  	public FixedLengthStringData actAllocBasis89 = new FixedLengthStringData(4).isAPartOf(filler1, 352);
  	public FixedLengthStringData actAllocBasis90 = new FixedLengthStringData(4).isAPartOf(filler1, 356);
  	public FixedLengthStringData agecont = new FixedLengthStringData(8).isAPartOf(t5537Rec, 390);
  	public FixedLengthStringData toterms = new FixedLengthStringData(27).isAPartOf(t5537Rec, 398);
  	public ZonedDecimalData[] toterm = ZDArrayPartOfStructure(9, 3, 0, toterms, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(27).isAPartOf(toterms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData toterm01 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData toterm02 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 3);
  	public ZonedDecimalData toterm03 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData toterm04 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 9);
  	public ZonedDecimalData toterm05 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData toterm06 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 15);
  	public ZonedDecimalData toterm07 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 18);
  	public ZonedDecimalData toterm08 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 21);
  	public ZonedDecimalData toterm09 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 24);
  	public FixedLengthStringData trmcont = new FixedLengthStringData(8).isAPartOf(t5537Rec, 425);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(67).isAPartOf(t5537Rec, 433, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5537Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5537Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}