package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:20
 * Description:
 * Copybook name: T5514REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5514rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5514Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData fcodes = new FixedLengthStringData(44).isAPartOf(t5514Rec, 0);
  	public FixedLengthStringData[] fcode = FLSArrayPartOfStructure(11, 4, fcodes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(fcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData fcode01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData fcode02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData fcode03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData fcode04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData fcode05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData fcode06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData fcode07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData fcode08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData fcode09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData fcode10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData fcode11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData fpercs = new FixedLengthStringData(55).isAPartOf(t5514Rec, 44);
  	public ZonedDecimalData[] fperc = ZDArrayPartOfStructure(11, 5, 2, fpercs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(55).isAPartOf(fpercs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData fperc01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData fperc02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData fperc03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData fperc04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData fperc05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData fperc06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData fperc07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData fperc08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData fperc09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData fperc10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public ZonedDecimalData fperc11 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 50);
  	public ZonedDecimalData fromdate = new ZonedDecimalData(8, 0).isAPartOf(t5514Rec, 99);
  	public FixedLengthStringData vfundesc = new FixedLengthStringData(30).isAPartOf(t5514Rec, 107);
  	public FixedLengthStringData unitVirtualFund = new FixedLengthStringData(4).isAPartOf(t5514Rec, 137);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(359).isAPartOf(t5514Rec, 141, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5514Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5514Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}