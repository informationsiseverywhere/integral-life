package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5535screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5535ScreenVars sv = (S5535ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5535screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5535ScreenVars screenVars = (S5535ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.unitAfterDur01.setClassString("");
		screenVars.unitExtPcAlloc01.setClassString("");
		screenVars.percOrAmtInd01.setClassString("");
		screenVars.unitAfterDur02.setClassString("");
		screenVars.unitExtPcAlloc02.setClassString("");
		screenVars.percOrAmtInd02.setClassString("");
		screenVars.unitAfterDur03.setClassString("");
		screenVars.unitExtPcAlloc03.setClassString("");
		screenVars.percOrAmtInd03.setClassString("");
		screenVars.unitAfterDur04.setClassString("");
		screenVars.unitExtPcAlloc04.setClassString("");
		screenVars.percOrAmtInd04.setClassString("");
		screenVars.unitAfterDur05.setClassString("");
		screenVars.unitExtPcAlloc05.setClassString("");
		screenVars.percOrAmtInd05.setClassString("");
		screenVars.unitAfterDur06.setClassString("");
		screenVars.unitExtPcAlloc06.setClassString("");
		screenVars.percOrAmtInd06.setClassString("");
		screenVars.unitAfterDur07.setClassString("");
		screenVars.unitExtPcAlloc07.setClassString("");
		screenVars.percOrAmtInd07.setClassString("");
		screenVars.unitAfterDur08.setClassString("");
		screenVars.unitExtPcAlloc08.setClassString("");
		screenVars.percOrAmtInd08.setClassString("");
		screenVars.unitAfterDur09.setClassString("");
		screenVars.unitExtPcAlloc09.setClassString("");
		screenVars.percOrAmtInd09.setClassString("");
		screenVars.unitAfterDur10.setClassString("");
		screenVars.unitExtPcAlloc10.setClassString("");
		screenVars.percOrAmtInd10.setClassString("");
		screenVars.unitAfterDur11.setClassString("");
		screenVars.unitExtPcAlloc11.setClassString("");
		screenVars.percOrAmtInd11.setClassString("");
		screenVars.unitAfterDur12.setClassString("");
		screenVars.unitExtPcAlloc12.setClassString("");
		screenVars.percOrAmtInd12.setClassString("");
		screenVars.unitAfterDur13.setClassString("");
		screenVars.unitExtPcAlloc13.setClassString("");
		screenVars.percOrAmtInd13.setClassString("");
		screenVars.unitAfterDur14.setClassString("");
		screenVars.unitExtPcAlloc14.setClassString("");
		screenVars.percOrAmtInd14.setClassString("");
		screenVars.unitAfterDur15.setClassString("");
		screenVars.unitExtPcAlloc15.setClassString("");
		screenVars.percOrAmtInd15.setClassString("");
	}

/**
 * Clear all the variables in S5535screen
 */
	public static void clear(VarModel pv) {
		S5535ScreenVars screenVars = (S5535ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.unitAfterDur01.clear();
		screenVars.unitExtPcAlloc01.clear();
		screenVars.percOrAmtInd01.clear();
		screenVars.unitAfterDur02.clear();
		screenVars.unitExtPcAlloc02.clear();
		screenVars.percOrAmtInd02.clear();
		screenVars.unitAfterDur03.clear();
		screenVars.unitExtPcAlloc03.clear();
		screenVars.percOrAmtInd03.clear();
		screenVars.unitAfterDur04.clear();
		screenVars.unitExtPcAlloc04.clear();
		screenVars.percOrAmtInd04.clear();
		screenVars.unitAfterDur05.clear();
		screenVars.unitExtPcAlloc05.clear();
		screenVars.percOrAmtInd05.clear();
		screenVars.unitAfterDur06.clear();
		screenVars.unitExtPcAlloc06.clear();
		screenVars.percOrAmtInd06.clear();
		screenVars.unitAfterDur07.clear();
		screenVars.unitExtPcAlloc07.clear();
		screenVars.percOrAmtInd07.clear();
		screenVars.unitAfterDur08.clear();
		screenVars.unitExtPcAlloc08.clear();
		screenVars.percOrAmtInd08.clear();
		screenVars.unitAfterDur09.clear();
		screenVars.unitExtPcAlloc09.clear();
		screenVars.percOrAmtInd09.clear();
		screenVars.unitAfterDur10.clear();
		screenVars.unitExtPcAlloc10.clear();
		screenVars.percOrAmtInd10.clear();
		screenVars.unitAfterDur11.clear();
		screenVars.unitExtPcAlloc11.clear();
		screenVars.percOrAmtInd11.clear();
		screenVars.unitAfterDur12.clear();
		screenVars.unitExtPcAlloc12.clear();
		screenVars.percOrAmtInd12.clear();
		screenVars.unitAfterDur13.clear();
		screenVars.unitExtPcAlloc13.clear();
		screenVars.percOrAmtInd13.clear();
		screenVars.unitAfterDur14.clear();
		screenVars.unitExtPcAlloc14.clear();
		screenVars.percOrAmtInd14.clear();
		screenVars.unitAfterDur15.clear();
		screenVars.unitExtPcAlloc15.clear();
		screenVars.percOrAmtInd15.clear();
	}
}
