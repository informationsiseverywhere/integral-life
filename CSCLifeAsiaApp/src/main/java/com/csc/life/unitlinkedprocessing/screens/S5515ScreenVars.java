package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5515
 * @version 1.0 generated on 30/08/09 06:41
 * @author Quipoz
 */
public class S5515ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(442);
	public FixedLengthStringData dataFields = new FixedLengthStringData(106).isAPartOf(dataArea, 0);
	public ZonedDecimalData acumbof = DD.acumbof.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData accumRounding = DD.acumrnd.copyToZonedDecimal().isAPartOf(dataFields,8);
	public FixedLengthStringData btobid = DD.btobid.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData btodisc = DD.btodisc.copy().isAPartOf(dataFields,12);
	public ZonedDecimalData discountOfferPercent = DD.btodiscpc.copyToZonedDecimal().isAPartOf(dataFields,13);
	public FixedLengthStringData btooff = DD.btooff.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,20);
	public ZonedDecimalData initBidOffer = DD.initbof.copyToZonedDecimal().isAPartOf(dataFields,23);
	public ZonedDecimalData initialRounding = DD.initrnd.copyToZonedDecimal().isAPartOf(dataFields,31);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,34);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,42);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,50);
	public FixedLengthStringData initAccumSame = DD.iuacum.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,59);
	public ZonedDecimalData managementCharge = DD.mgmtchrg.copyToZonedDecimal().isAPartOf(dataFields,89);
	public FixedLengthStringData otoff = DD.otoff.copy().isAPartOf(dataFields,94);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,95);
	public ZonedDecimalData tolerance = DD.tolerance.copyToZonedDecimal().isAPartOf(dataFields,100);
	public FixedLengthStringData unitType = DD.ultype.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData zfundtyp = DD.zfundtyp.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 106);
	public FixedLengthStringData acumbofErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acumrndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btobidErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData btodiscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData btodiscpcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData btooffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData initbofErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData initrndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData iuacumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mgmtchrgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData otoffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData toleranceErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ultypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData zfundtypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 190);
	public FixedLengthStringData[] acumbofOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acumrndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btobidOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] btodiscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] btodiscpcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] btooffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] initbofOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] initrndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] iuacumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mgmtchrgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] otoffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] toleranceOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ultypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] zfundtypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5515screenWritten = new LongData(0);
	public LongData S5515protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5515ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(currcodeOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mgmtchrgOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ultypeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toleranceOut,new String[] {"55",null, "-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(initrndOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acumrndOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(initbofOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acumbofOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(iuacumOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btobidOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btooffOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btodiscOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otoffOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btodiscpcOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, currcode, managementCharge, unitType, tolerance, initialRounding, accumRounding, initBidOffer, acumbof, initAccumSame, btobid, btooff, btodisc, otoff, discountOfferPercent, zfundtyp};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, currcodeOut, mgmtchrgOut, ultypeOut, toleranceOut, initrndOut, acumrndOut, initbofOut, acumbofOut, iuacumOut, btobidOut, btooffOut, btodiscOut, otoffOut, btodiscpcOut, zfundtypOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, currcodeErr, mgmtchrgErr, ultypeErr, toleranceErr, initrndErr, acumrndErr, initbofErr, acumbofErr, iuacumErr, btobidErr, btooffErr, btodiscErr, otoffErr, btodiscpcErr, zfundtypErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5515screen.class;
		protectRecord = S5515protect.class;
	}

}
