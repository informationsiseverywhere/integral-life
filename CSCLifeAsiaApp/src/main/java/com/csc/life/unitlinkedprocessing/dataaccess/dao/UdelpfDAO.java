package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Udelpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UdelpfDAO extends BaseDAO<Udelpf> {
	public int deleteUdelpfRecord(String chdrFrom, String chdrTo,String chdrcoy);
	public void insertUdelpfRecord(List<Udelpf> udelpfList);
	public void deleteUdelpfRecordByUdel(List<Udelpf> udelpfList);
	public boolean isExistUdelRecord(String chdrcoy, String chdrnum);
}