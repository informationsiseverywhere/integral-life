package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6518
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6518ScreenVars extends SmartVarModel { 

	//MIBT-114 STARTS
//	public FixedLengthStringData dataArea = new FixedLengthStringData(197);
//	public FixedLengthStringData dataFields = new FixedLengthStringData(53).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataArea = new FixedLengthStringData(265);
	public FixedLengthStringData dataFields = new FixedLengthStringData(73).isAPartOf(dataArea, 0);
	//MIBT-114 ENDS
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,19);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,27);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,35);
	public ZonedDecimalData stmEffdte = DD.stmeffd.copyToZonedDecimal().isAPartOf(dataFields,45);
	//MIBT-114 STARTS
	public FixedLengthStringData jobname = DD.jobname.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData runid = DD.runid.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData runlib = DD.runlib.copy().isAPartOf(dataFields,63);
//	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 53);
	//MIBT-114 ENDS
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 73);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData stmeffdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	//MIBT-114 STARTS
	public FixedLengthStringData jobnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData runidErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData runlibErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
//	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 89);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 121);
	//MIBT-114 ENDS
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] stmeffdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	//MIBT-114 STARTS
	public FixedLengthStringData[] jobnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] runidOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] runlibOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	//MIBT-114 ENDS
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData stmEffdteDisp = new FixedLengthStringData(10);

	public LongData S6518screenWritten = new LongData(0);
	public LongData S6518protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6518ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(stmeffdOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		//MIBT-114 STARTS
		screenFields = new BaseData[] {effdate, acctmonth, jobq, acctyear, scheduleName, scheduleNumber, bbranch, bcompany, stmEffdte,jobname,runid,runlib};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, jobqOut, acctyearOut, bschednamOut, bschednumOut, bbranchOut, bcompanyOut,stmeffdOut,jobnameOut,runidOut,runlibOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, jobqErr, acctyearErr, bschednamErr, bschednumErr, bbranchErr, bcompanyErr,stmeffdErr,jobnameErr,runidErr,runlibErr};
		//MIBT-114 ENDS
		screenDateFields = new BaseData[] {effdate, stmEffdte};
		screenDateErrFields = new BaseData[] {effdateErr, stmeffdErr};
		screenDateDispFields = new BaseData[] {effdateDisp, stmEffdteDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6518screen.class;
		protectRecord = S6518protect.class;
	}

}
