/*
 * File: P5145.java
 * Date: 30 August 2009 0:13:59
 * Author: Quipoz Limited
 * 
 * Class transformed from P5145.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UswdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswhTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsswhTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnunlTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S5145ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.Th608rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
*    P5145 - Unit Switching Source Funds.
*
*    This program will be used to select the funds from which
*    switches will be made. All the existing fund holdings are
*    displayed along with their estimated values - the system
*    will take the most recent price to work out these figures.
*    The user will then elect to switch monies from funds under
*    this component by either percentage or amount and will
*    select the funds from which monies are to be switched by
*    placing a figure against the funds.
*
*    Percentages and amounts may not be mixed for the funds
*    within a component.
*
*    If the whole plan has been selected - Plan Suffix on
*    COVRMJA = '0000', then the program will check that there
*    are no pending switches for the selected component on any
*    policy within the plan. If there are then an error message
*    will be displayed and the screen displayed fully protected
*    with no variable data. This validation will be carried out
*    in the Initialise section.
*
*    If an individual policy has been selected then there must
*    be no pending switches for the selected component within
*    the selected policy. This validation will be carried out in
*    the Initialise section and the screen displayed blank and
*    protected if an error is detected.
*
*    When all the selected funds have been validated control
*    will pass to the next program in the stack so that the user
*    may identify which funds are to receive the monies. Control
*    will only be returned to this program from the next one
*    down the stack if the entire plan has been selected for
*    fund switching.
*
*    If the whole plan is selected then this screen will not be
*    displayed. It will be assumed that all funds for the
*    selected component for all policies have been selected
*    and this program will direct control to the target funds
*    program for each such component, control being returned
*    here after each component has been processed.
*
*    If a selected policy is being processed then control will
*    not be returned to this program unless the user has
*    selected another component in the previous program.
*
*   Initialise
*   ----------
*    Skip this section if returning from a program further down
*    the stack, (current stack position action flag = '*').
*
*    RETRV the COVRMJA record from the COVRMJAIO module.
*
*    If the whole plan was selected the Plan Suffix in the
*    COVRMJA record will be '0000'. If this is the case then
*    ensure that no USWD record exists for this Company,
*    Contract Number, Life, Coverage and Rider combination, (any
*    Plan Suffix). Use a generic key of Company, Contract
*    Number, Life, Coverage and Rider from COVRMJA to perform a
*    BEGN on USWD. If a matching record is returned then set up
*    an error message informing the user that there are pending
*    switches for the Plan. The screen will be displayed blank
*    and protected and the processing following the screen for
*    an 'Enter' key will be the same as for 'CF11'.
*
*    If the whole plan was selected then skip the rest of this
*    section.
*
*    If an individual policy was selected then there may be no
*    pending switches for the selected component. Use the full
*    USWD key, obtained from COVRMJA, to perform a READR on
*    USWD. If a matching record is returned then set up an error
*    message informing the user that there are pending switches
*    for the selected component. The screen will be displayed
*    blank and protected and the processing following the screen
*    for an 'Enter' key will be the same as for 'CF11'.
*
*    Clear the subfile ready for loading.
*
*    The  details  of  the  contract  being processed will be
*    stored  in  the  CHDRMJA I/O module. Retrieve the details
*    and set up the header portion of the screen.
*
*    Look up the following descriptions and names:
*
*         Contract Type, (CNTTYPE) - long description from T5688,
*
*         Contract  Status,  (STATCODE)  -  short description
*         from T3623,
*
*         Premium  Status,  (PSTATCODE)  -  short description
*         from T3588,
*
*         Servicing branch, - long description from T1692,
*
*         The owner's client (CLTS) details.
*
*         The first Life Assured details from the LIFE dataset.
*
*         The Joint Life Assured details from the LIFE dataset
*         if they exist.
*
*         All the client numbers should have their names
*         displayed using CONFNAME. Obtain the confirmation
*         names.
*
*
*    Set the Switch date to the current system date and the
*    Percent/Amount indicator field to 'P'.
*
*    The details of all funds held for the selected component
*    will be displayed from the UTRS records. The generic key
*    for the UTRS record will be obtained from the COVRMJA
*    record. If the selected policy was a summarised policy then
*    the Plan Suffix in the key of the USWD record will be a
*    'notional' number, ie. that policy will not actually exist.
*    The program will be able to determine this by comparing
*    this number to the Number of Policies Summarised, (POLSUM),
*    from the Contract Header. If the Plan Suffix is less than
*    or equal to POLSUM then a notional policy is being
*    processed which will require a logical breakout of the
*    values from UTRS. If this is the case then the UTRS file
*    must be read with a Plan Suffix of '0000' but the notional
*    policy must still be retained for the USWD record and for
*    calculations.
*
*    The figure obtained from UTRS will be the Current Deemed
*    Unit Balance. If a summarised policy is being processed
*    this figure must be re-calculated as follows:
*
*    If the plan suffix is '0001' use the following calculation:
*
*        Value To Display = TOTAL - (TOTAL * (POLSUM - 1))
*                                                POLSUM
*
*    otherwise simply divide the Current Balance by the number
*    of policies summarised.
*
*    For example if the current balance is 100 and the number of
*    policies summarised  is 3, the balance for notional
*    policies #2 and #3 will  be  calculated  and  displayed as
*    33. For policy #1 it will be:
*
*                           100 - (100 * (3 - 1)
*                                            3
*
*                         = 100 - (100 * 2/3)
*
*                        =  34
*
*
*    Load the subfile as follows:
*
*    Use the Company, Contract Number, Plan Suffix, Life,
*    Coverage and Rider from the COVRMJA record to set up a
*    partial key for UTRS. If a summarised policy is being
*    processed replace the Plan Suffix in the UTRS key with
*    '0000'.
*
*    Perform a BEGN on UTRS and process all records using NEXTR
*    until any portion of the key up to Rider Number changes.
*    This will give all the funds for the selected component.
*
*    Display the fund and fund type and the current deemed unit
*    balance - use the logical break-out processing described
*    above if a summarised policy is being processed.
*
*    Read T5515 using a key of Fund to pick up the Fund Currency.
*
*    Read VPRNUNL to obtain the 'Now' price for the fund and
*    calculate the Estimated Fund Value. VPRUNL holds three
*    different prices: Bare, Bid and Offer. The program will use
*    either the Bid or Offer price in its calculations. It will
*    determine which one to use in the following way:
*
*    Read table T5544 with a key of Switch Rule, (from T6647)
*    concatenated with the contract currency. This will indicate
*    which basis is to be used in calculating the fund values
*    during the switch, the options will be either:
*
*         Bid to Bid
*         Bid to Offer
*         Bid to Discounted Offer
*         Bid to Fund Basis
*         Offer to Offer.
*
*    The first one marked with a 'Y' is the chosen method, so
*    use either the Bid or Offer price as appropriate.
*
*    Load all pages  required in the subfile and set the subfile
*    more indicator to no.
*   Validation
*   ----------
*    Skip this section if returning from a program further down
*    the stack, (current stack position action flag = '*').
*
*    Skip this section if the whole plan was selected and there
*    are no pending switches for the component.
*
*    Display the screen.
*
*    If the whole plan was selected and pending switches exist
*    for this component then process the rest of this section as
*    if 'CF11' was pressed.
*
*    Check that the Switch Date is not less than today's date.
*
*    Check that the 'Percent/Amount' field is 'P' or 'A'. If it
*    is neither give an appropriate error message.
*
*    If it is 'P' for percentage check that none of the amounts
*    entered are above 100.
*
*   Updating
*   --------
*
*    If the whole plan was selected the Plan Suffix in the first
*    COVRMJA record will be '0000'. If this is the case the
*    updating will be as follows:
*
*         If the first COVRMJA Plan Suffix was '0000' then this
*         screen will not be displayed and all the fund holdings
*         on the matching component on each policy will be
*         deemed to be included in the source fund switching.
*         The program will create one USWD record for each
*         matching  component within each 'real' plan suffix,
*         (ie. no logical breakouts), within the contract. After
*         each one is created control will be passed to the
*         Target Funds program further down the stack to create
*         the corresponding target switching details. Control
*         will then be returned here and this program will
*         process the next COVRMJA and its associated group of
*         UTRS records to create the next USWD record. When
*         processing an entire plan the program will need some
*         mechanism of telling whether it is processing the
*         first UTRS group or one of the subsequent ones. The
*         current stack position action field - WSSP-STACK-ACTN
*         (WSSP-PROGRAM-POINTER) - may be used for this as it
*         will only be set to '*' when the program is dealing
*         with the whole plan.
*
*         On the first time through use the Company and Contract
*         Number from the COVRMJA record to perform a BEGN on
*         UTRS.
*
*         Store the Company, Contract Number, Plan Suffix, Life,
*         Coverage and Rider number and then process all the
*         UTRS records using NEXTR until one of these fields
*         changes or end of file is reached. For this group
*         perform a KEEPS for a USWD record with all the funds
*         found from the UTRS group and all their amounts set to
*         100 and the Percent/Amount indicator set to 'P' Also
*         obtain each fund's currency code from T5515. Read
*         VPRNUNL to obtain the 'Now' price for the fund and
*         calculate the Estimated Fund Value. By-pass the rest
*         of this section.
*
*         On subsequent passes through this section process the
*         next COVRMJA and its UTRS group and KEEPS the next
*         USWD record. This will continue until end of file or
*         the Company, Contract Number, Life, Coverage or Rider
*         on COVRMJA changes.
*
*
*    If the COVRMJA Plan Suffix is not '0000' then take the
*    details from the screen and perform a KEEPS on the USWD
*    record using a key obtained from the COVRMJA record. The
*    extra details will be the Percent/Amount indicator, all the
*    source funds that have an amount or percentage against them
*    along with their amounts or percentages and estimated
*    values. All other fields will be spaces or zeroes as
*    appropriate.
*
*   Next Program
*   ------------
*    If "CF11" was requested move spaces to  the  current
*    program position and action field and exit.
*
*    If the whole plan was selected  move an asterisk to the
*    current stack action field and add 1 to the program pointer
*    and exit. If it is not the case just add 1 to the current
*    program pointer and exit.
*
*   Notes.
*   ------
*   Tables Used:
*   T3588 - Contract Premium Status           Key: PSTATCODE  (CHD MJA)
*   T3623 - Contract Risk Status              Key: STATCODE   (CHD MJA)
*   T5515 - Virtual Funds                     Key: FUND Code
*   T5681 - Coverage Premium Status           Key: PSTATCODE  (COV UNL)
*   T5682 - Coverage Risk Status              Key: STATCODE   (COV UNL)
*   T5688 - Contract Structure                Key: CNTTYPE
*
*****************************************************************
* </pre>
*/
public class P5145 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5145");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaUswdExists = new FixedLengthStringData(1);
	private String wsaaUswhExists = "";
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaAmtExists = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAmtTooBig = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaReinputFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaNoMatch = new FixedLengthStringData(1);
	protected PackedDecimalData wsaaReinputSuffix = new PackedDecimalData(4, 0);
	protected PackedDecimalData wsaaCheckSuffix = new PackedDecimalData(4, 0);
	protected String wsaaFirstTime = "";
	private PackedDecimalData wsaaSuffixZero = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaWholeUnits = new PackedDecimalData(15, 0);
	private ZonedDecimalData wsaaRemUnits = new ZonedDecimalData(2, 2);

	protected FixedLengthStringData wsaaFundKey = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).isAPartOf(wsaaFundKey, 0);

	private FixedLengthStringData wsaaUswdSrcfunds = new FixedLengthStringData(80);
	private FixedLengthStringData[] wsaaUswdSrcfund = FLSArrayPartOfStructure(20, 4, wsaaUswdSrcfunds, 0);

	private FixedLengthStringData wsaaUswdScfndtyps = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaUswdScfndtyp = FLSArrayPartOfStructure(20, 1, wsaaUswdScfndtyps, 0);

	protected FixedLengthStringData wsaaT5544Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaUsmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 0);
	protected FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 4);
	protected PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private String wsaaValidStatus = "";
	protected PackedDecimalData wsaaUnitBal = new PackedDecimalData(16, 5);
	protected FixedLengthStringData wsaaIndPolicy = new FixedLengthStringData(1);
	protected String wsaaNoUtrs = "";
		/* WSAA-KEY-FIELDS */
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	protected PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4).init(SPACES);

	private FixedLengthStringData wsaaEffDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaEffYy = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffDate, 0).setUnsigned();
	private ZonedDecimalData wsaaEffMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffDate, 4).setUnsigned();
	private ZonedDecimalData wsaaEffDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffDate, 6).setUnsigned();

	private FixedLengthStringData wsaaTranDates = new FixedLengthStringData(372);
	private FixedLengthStringData[] wsaaTranDay = FLSArrayPartOfStructure(372, 1, wsaaTranDates, 0);
	private String wsaaCalcPressed = "N";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private UswdTableDAM uswdIO = new UswdTableDAM();
	private UswhTableDAM uswhIO = new UswhTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrsswhTableDAM utrsswhIO = new UtrsswhTableDAM();
	protected VprnunlTableDAM vprnunlIO = new VprnunlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected T5515rec t5515rec = new T5515rec();
	protected T6647rec t6647rec = new T6647rec();
	protected T5544rec t5544rec = new T5544rec();
	private T5679rec t5679rec = new T5679rec();
	private Th506rec th506rec = new Th506rec();
	private Th608rec th608rec = new Th608rec();
	private Wssplife wssplife = new Wssplife();
	protected S5145ScreenVars sv = ScreenProgram.getScreenVars( S5145ScreenVars.class);
	protected ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	protected TablesInner tablesInner = new TablesInner();
	//ILIFE-8137 
    protected Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	protected Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;

/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		uswd1030, 
		readUtrs1060, 
		hits1080, 
		readHits1085, 
		exit1090, 
		vprnunl1440, 
		redisplay1447, 
		addLine1450, 
		nextUtrsswh1460, 
		percAmtInd2020, 
		exit2090, 
		updateErrorIndicators2670, 
		readNextModifiedRecord2680, 
		para3007, 
		process3010, 
		exit3090, 
		nextCovr3120, 
		begnUtrsswh3130, 
		zeroise3140, 
		formatUswd3150, 
		nextrUtrsswh3175, 
		keepsUswd3185, 
		exit3190, 
		zeroise3220, 
		uswd3230, 
		readSubfile3235, 
		keeps3240, 
		read3810, 
		exit4090, 
		data5130, 
		exit5190, 
		utrnLoop6020, 
		next6050, 
		exit6090, 
		a150AddLine, 
		a160NextHits
	}

	public P5145() {
		super();
		screenVars = sv;
		new ScreenModel("S5145", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1005();
					retrvChdr1010();
					retrCovr1015();
					proc1020();
				case uswd1030: 
					uswd1030();
					heads1040();
					subfile1050();
				case readUtrs1060: 
					readUtrs1060();
				case hits1080: 
					hits1080();
				case readHits1085: 
					readHits1085();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1005()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(wsspcomn.flag, "F")) {
			wsspcomn.flag.set("M");
		}
		wsaaIndPolicy.set(SPACES);
		wssplife.planPolicy.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaUswdSrcfunds.set(SPACES);
		wsaaUswdScfndtyps.set(SPACES);
		wsaaUswdExists.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.estval.set(ZERO);
		sv.nofDunits.set(ZERO);
		sv.pcntamt.set(ZERO);
		/*                                S5145-ESTVAL-01               */
		/*                                S5145-NOF-DUNITS-01           */
		/*                                S5145-PCNTAMT-01.             */
		scrnparams.function.set(varcom.sclr);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvChdr1010()
	{
		/*    Retrieve CHDRMJA header*/
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		wsaaPolsum.set(chdrpf.getPolsum());
	}

protected void retrCovr1015()
	{
		//ILIFE-8137
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		wsaaChdrcoy.set(covrpf.getChdrcoy());
		wsaaChdrnum.set(covrpf.getChdrnum());
		wsaaLife.set(covrpf.getLife());
		wsaaCoverage.set(covrpf.getCoverage());
		wsaaRider.set(covrpf.getRider());
	}

protected void proc1020()
	{
		/*    Determine what we are processing - if suffix not = zero*/
		/*    then processing individual policy*/
		if (isNE(covrpf.getPlanSuffix(), ZERO)) {
			wsaaIndPolicy.set("Y");
			goTo(GotoLabel.uswd1030);
		}
		/*    If the number of summarised policies = no. of policies in*/
		/*    plan this should be treated in the same way as individual*/
		/*    policies (ie display source fund screen)*/
		if (isEQ(chdrpf.getPolsum(), chdrpf.getPolinc())) {
			wsaaIndPolicy.set("Y");
		}
		if (isEQ(chdrpf.getPolsum(), ZERO)
		&& isEQ(chdrpf.getPolinc(), 1)) {
			wsaaIndPolicy.set("Y");
		}
		/*    If not all policies are summarised, then whole plan*/
		/*    processing comes into force - the source fund screen*/
		/*    is not displayed*/
		if (isGT(chdrpf.getPolinc(), chdrpf.getPolsum())) {
			rlseCovr1600();
			covrpf.setPlanSuffix(9999);
			begnCovr1700();
			
			wsaaPlanSuffix.set(covrpf.getPlanSuffix());
			if (isNE(wsaaFirstTime, "N")) {
				compute(wsaaReinputSuffix, 0).set(add(1, covrpf.getPlanSuffix()));
				wsaaFirstTime = "N";
			}
		
		}
	}

protected void uswd1030()
	{
		if (isEQ(wsaaIndPolicy, "Y")) {
			indvPolicy1200();
		}
		if (isNE(wsaaIndPolicy, "Y")) {
			wholePlan1100();
			if (isEQ(wsaaUswdExists, "Y")) {
				screenHeads1300();
			}
		}
		if (isEQ(wsaaReinputFlag, "Y")) {
			wsaaUswdSrcfunds.set(uswdIO.getSrcfunds());
			wsaaUswdScfndtyps.set(uswdIO.getScfndtyps());
		}
	}

protected void heads1040()
	{
		/*    If whole plan processing & no pending switches exist, then*/
		/*    source funds screen is not displayed*/
		if (isNE(wsaaIndPolicy, "Y")
		&& isNE(wsaaUswdExists, "Y")) {
			goTo(GotoLabel.exit1090);
		}
		screenHeads1300();
		/*    Protect screen if pending switches exist*/
		if (isNE(sv.chdrnumErr, SPACES)) {
			scrnparams.function.set(varcom.prot);
			goTo(GotoLabel.exit1090);
		}
	}

protected void subfile1050()
	{
		wsaaNoUtrs = "N";
		/*    BEGN UTRSSWH*/
		utrsswhIO.setDataKey(SPACES);
		utrsswhIO.setChdrcoy(covrpf.getChdrcoy());
		utrsswhIO.setChdrnum(covrpf.getChdrnum());
		utrsswhIO.setLife(covrpf.getLife());
		utrsswhIO.setCoverage(covrpf.getCoverage());
		utrsswhIO.setRider(covrpf.getRider());
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			utrsswhIO.setPlanSuffix(ZERO);
		}
		else {
			utrsswhIO.setPlanSuffix(covrpf.getPlanSuffix());
		}
		utrsswhIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		utrsswhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsswhIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE");
		//End;
		
	}

protected void readUtrs1060()
	{
		SmartFileCode.execute(appVars, utrsswhIO);
		if (isNE(utrsswhIO.getStatuz(), varcom.oK)
		&& isNE(utrsswhIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsswhIO.getParams());
			fatalError600();
		}
		if (isEQ(utrsswhIO.getStatuz(), varcom.endp)) {
			wsaaNoUtrs = "Y";
			goTo(GotoLabel.hits1080);
		}
		/*    MOVE H387                TO S5145-CHDRNUM-ERR             */
		/*    MOVE 'N'                 TO SCRN-SUBFILE-MORE             */
		/*    GO 1090-EXIT.                                             */
		if (isEQ(utrsswhIO.getChdrcoy(), covrpf.getChdrcoy())
		&& isEQ(utrsswhIO.getChdrnum(), covrpf.getChdrnum())
		&& isEQ(utrsswhIO.getLife(), covrpf.getLife())
		&& isEQ(utrsswhIO.getCoverage(), covrpf.getCoverage())) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaNoUtrs = "Y";
			goTo(GotoLabel.hits1080);
			/*****    MOVE H387                TO S5145-CHDRNUM-ERR             */
			/*****    MOVE 'N'                 TO SCRN-SUBFILE-MORE             */
			/*****    GO 1090-EXIT.                                             */
		}
		if (isLTE(utrsswhIO.getCurrentUnitBal(), ZERO)) {
			utrsswhIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readUtrs1060);
		}
		/* MOVE ZERO                   TO WSAA-SUB.                     */
		checkUswd5200();
		wsaaSub.set(1);
		while ( !(isEQ(scrnparams.subfileMore, "N"))) {
			loadSubfile1400();
		}
		
	}

protected void hits1080()
	{
		scrnparams.subfileMore.set(SPACES);
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(covrpf.getChdrcoy());
		hitsIO.setChdrnum(covrpf.getChdrnum());
		hitsIO.setLife(covrpf.getLife());
		hitsIO.setCoverage(covrpf.getCoverage());
		hitsIO.setRider(covrpf.getRider());
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			hitsIO.setPlanSuffix(ZERO);
		}
		else {
			hitsIO.setPlanSuffix(covrpf.getPlanSuffix());
		}
		hitsIO.setFormat(formatsInner.hitsrec);
		hitsIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE");
		//End;
	
	}

protected void readHits1085()
	{
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		if (isEQ(hitsIO.getStatuz(), varcom.endp)) {
			if (isEQ(wsaaNoUtrs, "Y")) {
				sv.chdrnumErr.set(errorsInner.h387);
			}
			scrnparams.subfileMore.set("N");
			return ;
		}
		if (isEQ(hitsIO.getChdrcoy(), covrpf.getChdrcoy())
		&& isEQ(hitsIO.getChdrnum(), covrpf.getChdrnum())
		&& isEQ(hitsIO.getLife(), covrpf.getLife())
		&& isEQ(hitsIO.getCoverage(), covrpf.getCoverage())) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(wsaaNoUtrs, "Y")) {
				sv.chdrnumErr.set(errorsInner.h387);
			}
			scrnparams.subfileMore.set("N");
			return ;
		}
		if (isLTE(hitsIO.getZcurprmbal(), ZERO)) {
			hitsIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readHits1085);
		}
		checkUswd5200();
		if (isEQ(wsaaNoUtrs, "Y")) {
			wsaaSub.set(1);
		}
		while ( !(isEQ(scrnparams.subfileMore, "N"))) {
			a100LoadSubfile();
		}
		
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void wholePlan1100()
	{
		begn1100();
	}

protected void begn1100()
	{
		/*    Checks to see if already outstanding UWSD record present*/
		wsaaReinputFlag.set(SPACES);
		compute(wsaaTranno, 0).set(add(1, chdrpf.getTranno()));
		wsaaSuffixZero.set(ZERO);
		uswdIO.setDataKey(SPACES);
		uswdIO.setFunction(varcom.begn);
		//start Life Performance atiwari
		uswdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		uswdIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE", "RIDER");
		//end;
		

	
		uswdIO.setChdrcoy(covrpf.getChdrcoy());
		uswdIO.setChdrnum(covrpf.getChdrnum());
		uswdIO.setLife(covrpf.getLife());
		uswdIO.setCoverage(covrpf.getCoverage());
		uswdIO.setRider(covrpf.getRider());
		uswdIO.setPlanSuffix(ZERO);
		uswdIO.setTranno(ZERO);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		if (isEQ(uswdIO.getStatuz(), varcom.oK)
		&& isEQ(uswdIO.getChdrcoy(), covrpf.getChdrcoy())
		&& isEQ(uswdIO.getChdrnum(), covrpf.getChdrnum())
		&& isEQ(uswdIO.getLife(), covrpf.getLife())
		&& isEQ(uswdIO.getCoverage(), covrpf.getCoverage())
		&& isEQ(uswdIO.getRider(), covrpf.getRider())
		&& isNE(uswdIO.getTranno(), wsaaTranno)) {
			sv.chdrnumErr.set(errorsInner.h385);
			wsaaUswdExists.set("Y");
		}
		/*    If redisplay of previous transaction*/
		if (isEQ(uswdIO.getStatuz(), varcom.oK)
		&& isEQ(uswdIO.getTranno(), wsaaTranno)) {
			/*NEXT_SENTENCE*/
		}
		else {
			return ;
		}
		wsaaReinputSuffix.subtract(1);
		if (isLTE(wsaaReinputSuffix, chdrpf.getPolsum())) {
			wsaaReinputSuffix.set(ZERO);
		}
		uswdIO.setPlanSuffix(wsaaReinputSuffix);
		uswdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		wsaaReinputFlag.set("Y");
	}

protected void indvPolicy1200()
	{
		uswd1210();
	}

protected void uswd1210()
	{
		/*   READR on USWD to check if pending switches exist*/
		wsaaReinputFlag.set(SPACES);
		compute(wsaaTranno, 0).set(add(1, chdrpf.getTranno()));
		scrnparams.subfileMore.set(SPACES);
		uswdIO.setChdrnum(covrpf.getChdrnum());
		uswdIO.setChdrcoy(covrpf.getChdrcoy());
		uswdIO.setPlanSuffix(covrpf.getPlanSuffix());
		uswdIO.setLife(covrpf.getLife());
		uswdIO.setCoverage(covrpf.getCoverage());
		uswdIO.setRider(covrpf.getRider());
		uswdIO.setTranno(ZERO);
		uswdIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		// ILIFE-5020
		//uswdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//uswdIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE", "RIDER","TRANNO");
		//End;
		
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		if (isEQ(uswdIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(uswdIO.getStatuz(), varcom.oK)
		&& isEQ(uswdIO.getChdrcoy(), covrpf.getChdrcoy())
		&& isEQ(uswdIO.getChdrnum(), covrpf.getChdrnum())
		&& isEQ(uswdIO.getLife(), covrpf.getLife())
		&& isEQ(uswdIO.getCoverage(), covrpf.getCoverage())
		&& isEQ(uswdIO.getRider(), covrpf.getRider())
		&& isNE(uswdIO.getTranno(), wsaaTranno)) {
			sv.chdrnumErr.set(errorsInner.h385);
			wsaaUswdExists.set("Y");
		}
		/*    If redisplay of previous transaction*/
		if (isEQ(uswdIO.getStatuz(), varcom.oK)
		&& isEQ(uswdIO.getChdrcoy(), covrpf.getChdrcoy())
		&& isEQ(uswdIO.getChdrnum(), covrpf.getChdrnum())
		&& isEQ(uswdIO.getPlanSuffix(), covrpf.getPlanSuffix())
		&& isEQ(uswdIO.getLife(), covrpf.getLife())
		&& isEQ(uswdIO.getCoverage(), covrpf.getCoverage())
		&& isEQ(uswdIO.getRider(), covrpf.getRider())
		&& isEQ(uswdIO.getTranno(), wsaaTranno)) {
			wsaaReinputFlag.set("Y");
		}
		wsaaSuffixZero.fill("9");
	}

protected void screenHeads1300()
	{
		accessT56881310();
		contStatus1320();
		premStatus1330();
		life1340();
		jlife1350();
		swDate1360();
		furtherFields1370();
		t56871380();
	}

protected void accessT56881310()
	{
		/*    Look up Contract  type description on table T5688*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
	}

protected void contStatus1320()
	{
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
	}

protected void premStatus1330()
	{
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void life1340()
	{
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeIO.setChdrnum(chdrpf.getChdrnum());
		lifeIO.setLife("01");
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
//		lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		lifeIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","JLIFE");
		//End;
		
		
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if (isNE(lifeIO.getChdrnum(), chdrpf.getChdrnum())
		&& isNE(lifeIO.getChdrcoy(), chdrpf.getChdrcoy())
		&& isNE(lifeIO.getLife(), "01")
		&& isNE(lifeIO.getJlife(), "00")) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeIO.getLifcnum());
		cltsIO.setClntnum(lifeIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jlife1350()
	{
		/* Check for the existence of Joint Life details.*/
		lifeIO.setJlife("01");
		lifeIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeIO);
		/*  IF   LIFE-STATUZ            NOT = O-K                        */
		/*       MOVE 'NONE'            TO S5145-JLIFE                   */
		/*                                 S5145-JLIFENAME               */
		/*  ELSE                                                         */
		if (isEQ(lifeIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeIO.getLifcnum());
			cltsIO.setClntnum(lifeIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

protected void swDate1360()
	{
		if (isEQ(wsaaReinputFlag, "Y")) {
			sv.effdate.set(uswdIO.getEffdate());
		}
		else {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			sv.effdate.set(datcon1rec.intDate);
		}
		sv.percentAmountInd.set("P");
	}

	/**
	* <pre>
	*****MOVE 'N'                    TO S5145-OVERRIDE-FEE.      <007>
	* </pre>
	*/
protected void furtherFields1370()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.planSuffix.set(covrpf.getPlanSuffix());
		sv.numpols.set(chdrpf.getPolinc());
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.crtable.set(covrpf.getCrtable());
	}

protected void t56871380()
	{
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		else {
			sv.crtabdesc.fill("?");
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(tablesInner.th506);
		itemIO.setItemitem(chdrpf.getCnttype());
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(errorsInner.hl62);
			fatalError600();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		if (isEQ(th506rec.ind, "Y")) {
			sv.effdate.set(varcom.vrcmMaxDate);
			validateEffdate2500();
		}
	}

protected void loadSubfile1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fundType1420();
					t55051430();
				case vprnunl1440: 
					vprnunl1440();
					t66471443();
					t55441445();
				case redisplay1447: 
					redisplay1447();
				case addLine1450: 
					addLine1450();
				case nextUtrsswh1460: 
					nextUtrsswh1460();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fundType1420()
	{
		/*    Set up subfile fields for fund, fund type & current*/
		/*    deemed unit balance*/
		/* IF UTRSSWH-PLAN-SUFFIX      = ZERO                           */
		/*PERFORM 1500-CALC-UNIT-BALANCE.                      <001>*/
		/*    PERFORM 1500-CALC-UNIT-BALANCE                       <001>*/
		/* ELSE                                                    <001>*/
		/*    MOVE UTRSSWH-CURRENT-UNIT-BAL TO WSAA-UNIT-BAL.      <001>*/
		/* MOVE UTRSSWH-UNIT-VIRTUAL-FUND                               */
		/*              TO S5145-UNIT-VIRTUAL-FUND-01.                  */
		/* MOVE UTRSSWH-UNIT-TYPE      TO S5145-FNDTYP-01.              */
		/*MOVE WSAA-UNIT-BAL          TO S5145-CURRCY-01.              */
		/*MOVE UTRSSWH-CURRENT-DUNIT-BAL                               */
		/*                            TO S5145-NOF-DUNITS-01.          */
		/* MOVE WSAA-UNIT-BAL          TO S5145-NOF-DUNITS-01.          */
		/* MOVE UTRSSWH-CURRENT-UNIT-BAL TO S5145-NOF-DUNITS-01.   <004>*/
		if (isEQ(utrsswhIO.getPlanSuffix(), ZERO)
		&& isNE(covrpf.getPlanSuffix(), ZERO)) {
			calcUnitBalance1500();
		}
		else {
			wsaaUnitBal.set(utrsswhIO.getCurrentUnitBal());
		}
		sv.unitVirtualFund.set(utrsswhIO.getUnitVirtualFund());
		/*                        TO S5145-UNIT-VIRTUAL-FUND-01.   <009>*/
		sv.fndtyp.set(utrsswhIO.getUnitType());
		/* MOVE UTRSSWH-UNIT-TYPE      TO S5145-FNDTYP-01.      <SM0404>*/
		sv.nofDunits.set(wsaaUnitBal);
	}

	/**
	* <pre>
	**** MOVE WSAA-UNIT-BAL          TO S5145-NOF-DUNITS-01.  <SM0404>
	* </pre>
	*/
protected void t55051430()
	{
		/*    Read T5515 to pick up Fund Currency*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5515");
		itdmIO.setItmfrm(sv.effdate);
		wsaaVirtualFund.set(utrsswhIO.getUnitVirtualFund());
		itdmIO.setItemitem(wsaaFundKey);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.mrnf)) {
			sv.vrtfndErr.set(errorsInner.h386);
			/*    MOVE H386                 TO S5145-VRTFND01-ERR           */
			goTo(GotoLabel.vprnunl1440);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		/* MOVE T5515-CURRCODE          TO S5145-CURRCY-01.             */
		sv.currcy.set(t5515rec.currcode);
	}

protected void vprnunl1440()
	{
		/*    Read VPRNUNL to obtain 'NOW' price*/
		vprnunlIO.setCompany(covrpf.getChdrcoy());
		vprnunlIO.setUnitVirtualFund(utrsswhIO.getUnitVirtualFund());
		vprnunlIO.setUnitType(utrsswhIO.getUnitType());
		/*MOVE ALL '9'                 TO VPRNUNL-EFFDATE.             */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		vprnunlIO.setEffdate(datcon1rec.intDate);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		vprnunlIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		vprnunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnunlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		//End;
		
		SmartFileCode.execute(appVars, vprnunlIO);
		if (isNE(vprnunlIO.getStatuz(), varcom.oK)
		&& isNE(vprnunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vprnunlIO.getParams());
			fatalError600();
		}
		if (isEQ(vprnunlIO.getStatuz(), varcom.endp)
		|| isNE(covrpf.getChdrcoy(), vprnunlIO.getCompany())
		|| isNE(utrsswhIO.getUnitVirtualFund(), vprnunlIO.getUnitVirtualFund())
		|| isNE(utrsswhIO.getUnitType(), vprnunlIO.getUnitType())) {
			sv.estvalErr.set(errorsInner.g227);
			sv.estval.set(ZERO);
			/*    MOVE G227                 TO S5145-ESTVAL01-ERR           */
			/*    MOVE ZERO                 TO S5145-ESTVAL-01              */
			goTo(GotoLabel.addLine1450);
		}
	}

protected void t66471443()
	{
		/*    Reads table T6647 to get switch rule to access T5544 table*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTranscode.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrpf.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(sv.effdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void t55441445()
	{
		/*    Reads table T5544 which indicates whether Bid or Offer price*/
		/*    is to be used*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5544);
		wsaaUsmeth.set(t6647rec.swmeth);
		wsaaCurrcode.set(t5515rec.currcode);
		itdmIO.setItemitem(wsaaT5544Key);
		itdmIO.setItmfrm(sv.effdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5544rec.t5544Rec.set(itdmIO.getGenarea());
		if (isEQ(t5544rec.btobid, "Y")
		|| isEQ(t5544rec.btodisc, "Y")
		|| isEQ(t5544rec.btooff, "Y")) {
			compute(sv.estval, 5).set(mult(vprnunlIO.getUnitBidPrice(), wsaaUnitBal));
			/*                     GIVING S5145-ESTVAL-01                   */
			zrdecplrec.amountIn.set(sv.estval);
			callRounding8000();
			sv.estval.set(zrdecplrec.amountOut);
			goTo(GotoLabel.redisplay1447);
		}
		/*ELSE                                                         */
		if (isEQ(t5544rec.otooff, "Y")) {
			compute(sv.estval, 5).set(mult(vprnunlIO.getUnitOfferPrice(), wsaaUnitBal));
			/*                     GIVING S5145-ESTVAL-01                   */
			zrdecplrec.amountIn.set(sv.estval);
			callRounding8000();
			sv.estval.set(zrdecplrec.amountOut);
			goTo(GotoLabel.redisplay1447);
		}
		/* Must be Bid to Fund then. So use details from T5515 to decide   */
		/* which price to use to calculate the estimate.                   */
		if ((isEQ(t5515rec.btobid, "Y"))
		|| (isEQ(t5515rec.btodisc, "Y"))
		|| (isEQ(t5515rec.btooff, "Y"))) {
			compute(sv.estval, 5).set(mult(vprnunlIO.getUnitBidPrice(), wsaaUnitBal));
			/*                     GIVING S5145-ESTVAL-01           <SM0404>*/
			zrdecplrec.amountIn.set(sv.estval);
			callRounding8000();
			sv.estval.set(zrdecplrec.amountOut);
		}
		else {
			compute(sv.estval, 5).set(mult(vprnunlIO.getUnitOfferPrice(), wsaaUnitBal));
			zrdecplrec.amountIn.set(sv.estval);
			callRounding8000();
			sv.estval.set(zrdecplrec.amountOut);
		}
	}

	/**
	* <pre>
	****                     GIVING S5145-ESTVAL.             <V76F06>
	****                     GIVING S5145-ESTVAL-01.          <SM0404>
	* </pre>
	*/
protected void redisplay1447()
	{
		/*    If redisplay of previous input, format percentage*/
		/*    previously input (from USWD record)*/
		if (isNE(wsaaReinputFlag, "Y")) {
			goTo(GotoLabel.addLine1450);
		}
		/* PERFORM 5200-CHECK-USWD.                                     */
		if (isEQ(wsaaNoMatch, "Y")) {
			goTo(GotoLabel.addLine1450);
		}
		/* IF USWD-SRCFUND (WSAA-SUB) NOT =                     <SM0404>*/
		/*                            S5145-UNIT-VIRTUAL-FUND-01<SM0404>*/
		/*    MOVE ZEROES                 TO S5145-PCNTAMT-01   <SM0404>*/
		/* ELSE                                                 <SM0404>*/
		/*    MOVE USWD-SCPRCAMT (WSAA-SUB) TO S5145-PCNTAMT-01 <SM0404>*/
		/* END-IF.                                              <SM0404>*/
		if (isNE(uswdIO.getSrcfund(wsaaSub), sv.unitVirtualFund)) {
			sv.pcntamt.set(ZERO);
			goTo(GotoLabel.addLine1450);
		}
		else {
			sv.pcntamt.set(uswdIO.getScprcamt(wsaaSub));
		}
		wsaaSub.add(1);
	}

	/**
	* <pre>
	**** IF USWD-SCPRCAMT(WSAA-SUB)     > ZERO                        
	****    MOVE USWD-SCPRCAMT(WSAA-SUB) TO S5145-PCNTAMT-01          
	**** ELSE                                                         
	****    MOVE ZERO                   TO S5145-PCNTAMT-01.          
	* </pre>
	*/
protected void addLine1450()
	{
		/*    Write a line to subfile*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextUtrsswh1460()
	{
		/*    Read next UTRSSWH*/
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			wsaaCheckSuffix.set(ZERO);
		}
		else {
			wsaaCheckSuffix.set(covrpf.getPlanSuffix());
		}
		utrsswhIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsswhIO);
		if (isNE(utrsswhIO.getStatuz(), varcom.oK)
		&& isNE(utrsswhIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsswhIO.getParams());
			fatalError600();
		}
		if (isNE(utrsswhIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(utrsswhIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(utrsswhIO.getPlanSuffix(), wsaaCheckSuffix)
		|| isNE(utrsswhIO.getLife(), covrpf.getLife())
		|| isNE(utrsswhIO.getCoverage(), covrpf.getCoverage())
		|| isEQ(utrsswhIO.getStatuz(), varcom.endp)) {
			scrnparams.subfileMore.set("N");
			return ;
		}
		if (isLTE(utrsswhIO.getCurrentUnitBal(), ZERO)) {
			goTo(GotoLabel.nextUtrsswh1460);
		}
	}

protected void calcUnitBalance1500()
	{
		/*CALC*/
		/*    Calculates Unit Balance Depending on value of plan suffix*/
		/* IF USWD-PLAN-SUFFIX         = 1                              */
		if (isEQ(covrpf.getPlanSuffix(), 1)) {
			compute(wsaaUnitBal, 5).set(sub(utrsswhIO.getCurrentUnitBal(), (mult(div((sub(chdrpf.getPolsum(), 1)), chdrpf.getPolsum()), utrsswhIO.getCurrentUnitBal()))));
		}
		else {
			/*DIVIDE UTRSSWH-CURRENT-UNIT-BAL BY CHDRMJA-POLSUM         */
			/* IF USWD-PLAN-SUFFIX          > 1                        <001>*/
			if (isGT(covrpf.getPlanSuffix(), 1)) {
				compute(wsaaUnitBal, 5).set(div(utrsswhIO.getCurrentUnitBal(), chdrpf.getPolsum()));
			}
			else {
				wsaaUnitBal.set(utrsswhIO.getCurrentUnitBal());
			}
		}
		/*EXIT*/
	}

protected void rlseCovr1600()
	{
		/*PARA*/
		/*    Releases COVR record for whole plan processing*/
		covrpfDAO.deleteCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		/*EXIT*/
	}

protected void begnCovr1700()
	{
		/*PARA*/
		/*    Begins COVR for plan suffix (whole plan processing)*/
		covrpfList=covrpfDAO.getCovrmjaByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr : covrpfList ) {
			if (covrpfList.isEmpty()) {
				fatalError600();
			}
			if (isNE(covr.getChdrcoy(), chdrpf.getChdrcoy())
			&& isNE(covr.getChdrnum(), chdrpf.getChdrnum())) {
				//syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}else {
				covrpf=covr;
				return;
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/*2000-PARA.                                               <D509CS>*/
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*IF WSAA-USWD-EXISTS         = 'Y' OR                 <D509CS>*/
		/*   WSSP-FLAG                = 'I'                    <D509CS>*/
		/*   MOVE 'PROT'              TO SCRN-FUNCTION         <D509CS>*/
		/*   GO 2010-SCREEN-IO.                                <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF   WSAA-IND-POLICY        NOT = 'Y'                <D509CS>*/
		/*     MOVE O-K               TO WSSP-EDTERROR         <D509CS>*/
		/*     GO 2090-EXIT.                                   <D509CS>*/
		if (isEQ(wsaaUswdExists, "Y")
		|| isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set("PROT");
		}
		else {
			if (isNE(wsaaIndPolicy, "Y")) {
				wsspcomn.edterror.set(varcom.oK);
				wsspcomn.sectionno.set("3000");
				return ;
			}
		}
		/*2010-SCREEN-IO.                                          <D509CS>*/
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
				case percAmtInd2020: 
					percAmtInd2020();
					validateSubfile2030();
					checkForErrors2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5145IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   S5145-DATA-AREA               */
		/*                                   S5145-SUBFILE-AREA.           */
		/* If user has abandoned the transaction, then prevent *           */
		/* program P5133 from continuing to  Fund Redirection. *           */
		if (isEQ(scrnparams.statuz, "SUBM ")) {
			wsspcomn.flag.set("K");
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")
		|| isEQ(wsspcomn.flag, "I")
		|| isEQ(wsaaUswdExists, "Y")) {
			goTo(GotoLabel.exit2090);
		}
		/* CF05 - Screen Refresh requested                                 */
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsaaCalcPressed = "Y";
		}
	}

protected void validateScreen2010()
	{
		/*    Check switch date is not < todays date*/
		if (isEQ(sv.effdate, ZERO)) {
			goTo(GotoLabel.percAmtInd2020);
		}
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)
		&& isNE(th506rec.ind, "Y")) {
			sv.effdateErr.set(errorsInner.f665);
			goTo(GotoLabel.percAmtInd2020);
		}
		if (isLT(sv.effdate, covrpf.getCrrcd())) {
			sv.effdateErr.set(errorsInner.t044);
		}
		/*   MOVE H997                TO S5145-EFFDATE-ERR.            */
		if (isEQ(th506rec.ind, "Y")) {
			validateEffdate2500();
		}
	}

protected void percAmtInd2020()
	{
		/*    Percentage/Amount indicator must = 'A' or 'P'*/
		/*    Cater for option 'U' to switch units between funds.          */
		if (isNE(sv.percentAmountInd, "A")
		&& isNE(sv.percentAmountInd, "P")
		&& isNE(sv.percentAmountInd, "U")) {
			sv.pcamtindErr.set(errorsInner.rl27);
		}
		/* IF S5145-PERCENT-AMOUNT-IND NOT = 'P' AND                    */
		/*                             NOT = 'A'                        */
		/*    MOVE G347                TO S5145-PCAMTIND-ERR.           */
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

	/**
	* <pre>
	*2025-OVERRIDE-FEE.                                          <007>
	*******                                                      <007>
	*****Override Switch Fee must be  = 'Y' or 'N'               <007>
	*******                                                      <007>
	*****IF S5145-OVERRIDE-FEE NOT = 'Y' AND                     <007>
	*******                    NOT = 'N'                         <007>
	******* MOVE H503                TO S5145-ORSWCHFE-ERR.      <007>
	*******                                                      <007>
	*****IF S5145-ERROR-INDICATORS NOT = SPACES                  <007>
	******* MOVE 'Y'                 TO WSSP-EDTERROR            <007>
	******* GO 2090-EXIT.                                        <007>
	*********                                                    <007>
	* </pre>
	*/
protected void validateSubfile2030()
	{
		/*    Move to top of subfile*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaAmtExists.set(SPACES);
		wsaaAmtTooBig.set(SPACES);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isNE(wsaaAmtExists, "Y")) {
			sv.pcntamtErr.set(errorsInner.h998);
			/*    MOVE H998              TO S5145-PCNTAMT01-ERR             */
			scrnparams.errorCode.set(errorsInner.h998);
			sv.pcntamtOut[varcom.ri.toInt()].set("Y");
		}
		/*    MOVE 'Y'               TO S5145-PCNTAMT01-OUT(RI).        */
		if (isEQ(wsaaAmtTooBig, "Y")) {
			sv.pcntamtErr.set(errorsInner.f348);
			/*    MOVE F348              TO S5145-PCNTAMT01-ERR             */
			scrnparams.errorCode.set(errorsInner.f348);
			sv.pcntamtOut[varcom.ri.toInt()].set("Y");
		}
		/*    MOVE 'Y'               TO S5145-PCNTAMT01-OUT(RI).        */
		/* If CF05 pressed for screen refresh, then prevent further        */
		/* processing (i.e. until user presses enter to continue)          */
		if (isEQ(wsaaCalcPressed, "Y")) {
			wsspcomn.edterror.set("Y");
			wsaaCalcPressed = "N";
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)
		|| isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.subfileRrn.set(1);
	}

protected void validateEffdate2500()
	{
		validateDate2510();
	}

protected void validateDate2510()
	{
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			wsaaEffDate.set(datcon1rec.intDate);
		}
		else {
			wsaaEffDate.set(sv.effdate);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.th608);
		itemIO.setItemitem(wsaaEffYy);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		th608rec.th608Rec.set(itemIO.getGenarea());
		wsaaTranDates.set(th608rec.th608Rec);
		compute(wsaaSub, 0).set(add((mult((sub(wsaaEffMm, 1)), 31)), wsaaEffDd));
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			while ( !(isEQ(wsaaTranDay[wsaaSub.toInt()], "T")
			|| isEQ(wsaaSub, 372))) {
				wsaaSub.add(1);
			}
			
			compute(wsaaEffMm, 0).setDivide(wsaaSub, (31));
			wsaaEffDd.setRemainder(wsaaEffMm);
			if (isEQ(wsaaEffDd, 0)) {
				wsaaEffDd.set(31);
			}
			else {
				wsaaEffMm.add(1);
			}
			sv.effdate.set(wsaaEffDate);
		}
		if (isNE(wsaaTranDay[wsaaSub.toInt()], "T")) {
			sv.effdateErr.set(errorsInner.hl61);
		}
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
				case readNextModifiedRecord2680: 
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		/*    If percent ind = 'P' check that amount not > 100*/
		/* IF S5145-PCNTAMT-01         > ZERO                           */
		if (isGT(sv.pcntamt, ZERO)) {
			wsaaAmtExists.set("Y");
		}
		if (isNE(sv.percentAmountInd, "P")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* IF S5145-PCNTAMT-01         > 100                            */
		if (isGT(sv.pcntamt, 100)) {
			wsaaAmtTooBig.set("Y");
	    }	
	}

protected void updateErrorIndicators2670()
	{
		/* Check, if switching Units, that number available is             */
		if (isEQ(sv.percentAmountInd, "U")) {
			/*    IF S5145-PCNTAMT-01       > S5145-NOF-DUNITS-01   <SM0404>*/
			/*       MOVE F070             TO S5145-PCNTAMT01-ERR   <SM0404>*/
			if (isGT(sv.pcntamt, sv.nofDunits)) {
				sv.pcntamtErr.set(errorsInner.f070);
			}
			else {
				/*       DIVIDE S5145-PCNTAMT-01  BY 1                  <SM0404>*/
				compute(wsaaWholeUnits, 2).setDivide(sv.pcntamt, (1));
				wsaaRemUnits.setRemainder(wsaaWholeUnits);
				if (isGT(wsaaRemUnits, ZERO)) {
					/*          MOVE RL28          TO S5145-PCNTAMT01-ERR   <SM0404>*/
					sv.pcntamtErr.set(errorsInner.rl28);
				}
			}
		}
		
		if ((isEQ(sv.percentAmountInd, "A") && (isGT(sv.pcntamt, sv.estval)))) {
			sv.pcntamtErr.set(errorsInner.RUJX);
		}
		/* Check, if Amount, conduct the currency rounding rule.           */
		if (isEQ(sv.percentAmountInd, "A")) {
			if (isNE(sv.pcntamt, ZERO)) {
				zrdecplrec.amountIn.set(sv.pcntamt);
				callRounding8000();
				if (isNE(zrdecplrec.amountOut, sv.pcntamt)) {
					sv.pcntamtErr.set(errorsInner.rfik);
				}
			}
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			/*      MOVE O-K               TO WSSP-EDTERROR                 */
			goTo(GotoLabel.readNextModifiedRecord2680);
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	**** IF   S5145-ERROR-SUBFILE NOT = SPACES                        
	****      MOVE 'Y'               TO WSSP-EDTERROR.                
	* </pre>
	*/
protected void readNextModifiedRecord2680()
	{
		/* MOVE SRNCH                  TO SCRN-FUNCTION.                */
		scrnparams.function.set(varcom.srdn);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3005();
				case para3007: 
					para3007();
				case process3010: 
					process3010();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3005()
	{
		wssplife.effdate.set(sv.effdate);
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.flag.set("K");
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		&& isEQ(wsaaIndPolicy, "Y")) {
			goTo(GotoLabel.exit3090);
		}
		/*    If redisplaying previous transaction's input*/
		if (isEQ(wsaaReinputFlag, "Y")) {
			deleteUtrns6000();
		}
		if (isNE(wsaaReinputFlag, "Y")) {
			goTo(GotoLabel.para3007);
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		&& isNE(wsaaIndPolicy, "Y")) {
			keepsUswd3500();
			keepsCovr3900();
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaIndPolicy, "Y")
		&& isNE(wsaaNoMatch, "Y")) {
			indPolRewrite5100();
			keepsUswd3500();
			keepsCovr3900();
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaIndPolicy, "Y")
		&& isEQ(wsaaNoMatch, "Y")) {
			goTo(GotoLabel.para3007);
		}
		readUswd5000();
		if (isNE(wsaaSuffixZero, ZERO)) {
			goTo(GotoLabel.exit3090);
		}
		keepsUswd3500();
		keepsCovr3900();
		goTo(GotoLabel.exit3090);
	}

protected void para3007()
	{
		/*    If pending switches exist then no updating to be done*/
		if (isEQ(wsaaUswdExists, "Y")) {
			goTo(GotoLabel.exit3090);
		}
		/*    No need to write USWH if one already exists*/
		/*IF WSAA-USWH-EXISTS          = 'Y'                           */
		/*GO 3010-PROCESS.                                          */
		if (isNE(chdrpf.getChdrnum(), wsaaPrevChdrnum)
		&& isNE(wsaaPrevChdrnum, SPACES)) {
			uswhIO.setFormat(formatsInner.uswhrec);
			uswhIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, uswhIO);
			if (isNE(uswhIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(uswhIO.getParams());
				fatalError600();
			}
		}
		wsaaPrevChdrnum.set(chdrpf.getChdrnum());
		/*    Check if USWH exists (if more than one policy has been*/
		/*    selected from selection screen, a USWH will exist on reentry*/
		/*    but the above flag will have been initialised.)*/
		uswhIO.setDataKey(SPACES);
		uswhIO.setChdrnum(chdrpf.getChdrnum());
		uswhIO.setChdrcoy(wsspcomn.company);
		setPrecision(uswhIO.getTranno(), 0);
		uswhIO.setTranno(add(1, chdrpf.getTranno()));
		uswhIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)
		&& isNE(uswhIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError600();
		}
		if (isEQ(uswhIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.process3010);
		}
		/*    WRITR on USWH record*/
		uswhIO.setDataArea(SPACES);
		uswhIO.setChdrnum(chdrpf.getChdrnum());
		uswhIO.setChdrcoy(wsspcomn.company);
		setPrecision(uswhIO.getTranno(), 0);
		uswhIO.setTranno(add(1, chdrpf.getTranno()));
		/*    Access T6647 for Switch Rule*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T6647");
		itdmIO.setItmfrm(sv.effdate);
		wsaaTranscode.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrpf.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		uswhIO.setSwitchRule(t6647rec.swmeth);
		uswhIO.setFormat(formatsInner.uswhrec);
		uswhIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError600();
		}
		/*    KEEPS on USWH record*/
		uswhIO.setFormat(formatsInner.uswhrec);
		uswhIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError600();
		}
	}

protected void process3010()
	{
		/*    Determine if Whole Plan or Individual Policy has been*/
		/*    selected & process accordingly*/
		if (isNE(wsaaIndPolicy, "Y")) {
			wholePlan3100();
		}
		else {
			indPolicy3200();
		}
	}

protected void wholePlan3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkActn3110();
				case nextCovr3120: 
					nextCovr3120();
				case begnUtrsswh3130: 
					begnUtrsswh3130();
				case zeroise3140: 
					zeroise3140();
				case formatUswd3150: 
					formatUswd3150();
					currcode3160();
					vprnunl3170();
				case nextrUtrsswh3175: 
					nextrUtrsswh3175();
				case keepsUswd3185: 
					keepsUswd3185();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkActn3110()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.begnUtrsswh3130);
		}
	}

protected void nextCovr3120()
	{
		/*    Retrieve next COVRMJA if Whole Plan & not first time thru*/
		if (isEQ(wsaaPlanSuffix, ZERO)) {
			wsaaSuffixZero.fill("9");
			goTo(GotoLabel.exit3190);
		}
		if (isLTE(wsaaPlanSuffix, chdrpf.getPolsum())) {
			goTo(GotoLabel.begnUtrsswh3130);
		}
		nextCovr3800();
		if(covrpf != null) {
			wsaaSuffixZero.fill("9");
			goTo(GotoLabel.exit3190);
		}
		/*if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			wsaaSuffixZero.fill("9");
			goTo(GotoLabel.exit3190);
		}*/
		if (isNE(covrpf.getPlanSuffix(), ZERO)) {
			wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		}
	}

protected void begnUtrsswh3130()
	{
		begnUtrsswh3300();
		/*    Exit if all summarised policies have been processed*/
		if (isEQ(utrsswhIO.getStatuz(), varcom.endp)
		&& isEQ(covrpf.getPlanSuffix(), ZERO)) {
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(utrsswhIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.nextCovr3120);
		}
		wsaaSub.set(1);
	}

protected void zeroise3140()
	{
		/*    Zeroise fields on USWD*/
		uswdIO.setScprcamt(wsaaSub, ZERO);
		uswdIO.setScactval(wsaaSub, ZERO);
		uswdIO.setScestval(wsaaSub, ZERO);
		uswdIO.setSrcfund(wsaaSub, SPACES);
		uswdIO.setScfndtyp(wsaaSub, SPACES);
		uswdIO.setScfndcur(wsaaSub, SPACES);
		if (isLT(wsaaSub, 11)) {
			uswdIO.setTgtprcnt(wsaaSub, ZERO);
			uswdIO.setTgtfund(wsaaSub, SPACES);
			uswdIO.setTgfndcur(wsaaSub, SPACES);
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 20)) {
			goTo(GotoLabel.zeroise3140);
		}
		uswdIO.setPercentAmountInd("P");
		wsaaSub.set(1);
	}

protected void formatUswd3150()
	{
		/*    Store source funds , types & amounts*/
		if (isLTE(utrsswhIO.getCurrentUnitBal(), ZERO)) {
			goTo(GotoLabel.nextrUtrsswh3175);
		}
		uswdIO.setSrcfund(wsaaSub, utrsswhIO.getUnitVirtualFund());
		uswdIO.setScfndtyp(wsaaSub, utrsswhIO.getUnitType());
		uswdIO.setScprcamt(wsaaSub, 100);
	}

protected void currcode3160()
	{
		/*    Read T5515 to pick up Fund Currency*/
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5515");
		itdmIO.setItmfrm(sv.effdate);
		wsaaVirtualFund.set(utrsswhIO.getUnitVirtualFund());
		itdmIO.setItemitem(wsaaFundKey);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		uswdIO.setScfndcur(wsaaSub, t5515rec.currcode);
	}

protected void vprnunl3170()
	{
		/*    Read VPRNUNL to obtain 'NOW' price*/
		vprnunlIO.setCompany(covrpf.getChdrcoy());
		vprnunlIO.setUnitVirtualFund(utrsswhIO.getUnitVirtualFund());
		vprnunlIO.setUnitType(utrsswhIO.getUnitType());
		/*MOVE ALL '9'                 TO VPRNUNL-EFFDATE.             */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		vprnunlIO.setEffdate(datcon1rec.intDate);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		vprnunlIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		
		vprnunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnunlIO.setFitKeysSearch("VRTFND","ULTYPE");
		//end;
		
		SmartFileCode.execute(appVars, vprnunlIO);
		if (isNE(vprnunlIO.getStatuz(), varcom.oK)
		|| isNE(vprnunlIO.getUnitVirtualFund(), utrsswhIO.getUnitVirtualFund())
		|| isNE(vprnunlIO.getUnitType(), utrsswhIO.getUnitType())) {
			syserrrec.params.set(vprnunlIO.getParams());
			fatalError600();
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5544);
		wsaaUsmeth.set(t6647rec.swmeth);
		wsaaCurrcode.set(t5515rec.currcode);
		itdmIO.setItemitem(wsaaT5544Key);
		itdmIO.setItmfrm(sv.effdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5544rec.t5544Rec.set(itdmIO.getGenarea());
		if (isEQ(t5544rec.btobid, "Y")
		|| isEQ(t5544rec.btodisc, "Y")
		|| isEQ(t5544rec.btooff, "Y")) {
			setPrecision(uswdIO.getScestval(wsaaSub), 5);
			uswdIO.setScestval(wsaaSub, mult(vprnunlIO.getUnitBidPrice(), utrsswhIO.getCurrentUnitBal()));
		}
		else {
			setPrecision(uswdIO.getScestval(wsaaSub), 5);
			uswdIO.setScestval(wsaaSub, mult(vprnunlIO.getUnitOfferPrice(), utrsswhIO.getCurrentUnitBal()));
		}
		zrdecplrec.amountIn.set(uswdIO.getScestval(wsaaSub));
		callRounding8000();
		uswdIO.setScestval(wsaaSub, zrdecplrec.amountOut);
	}

protected void nextrUtrsswh3175()
	{
		/*    Read next UTRSSWH record*/
		nextrUtrsswh3400();
		if (isEQ(utrsswhIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.keepsUswd3185);
		}
		/*INC-SUB*/
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 20)) {
			goTo(GotoLabel.formatUswd3150);
		}
	}

protected void keepsUswd3185()
	{
		/*    Perform KEEPS on USWD record*/
		uswdIO.setChdrcoy(covrpf.getChdrcoy());
		uswdIO.setChdrnum(covrpf.getChdrnum());
		uswdIO.setPlanSuffix(wsaaPlanSuffix);
		/* MOVE ZERO                    TO USWD-TRANNO.                 */
		uswdIO.setTranno(wsaaTranno);
		uswdIO.setLife(covrpf.getLife());
		uswdIO.setCoverage(covrpf.getCoverage());
		uswdIO.setRider(covrpf.getRider());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		uswdIO.setEffdate(datcon1rec.intDate);
		keepsUswd3500();
		keepsCovr3900();
	}

protected void indPolicy3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					startSubfile3210();
				case zeroise3220: 
					zeroise3220();
				case uswd3230: 
					uswd3230();
				case readSubfile3235: 
					readSubfile3235();
				case keeps3240: 
					keeps3240();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void startSubfile3210()
	{
		/*    Move to top of subfile*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaSub.set(1);
	}

protected void zeroise3220()
	{
		uswdIO.setScprcamt(wsaaSub, ZERO);
		uswdIO.setScactval(wsaaSub, ZERO);
		uswdIO.setScestval(wsaaSub, ZERO);
		uswdIO.setSrcfund(wsaaSub, SPACES);
		uswdIO.setScfndtyp(wsaaSub, SPACES);
		uswdIO.setScfndcur(wsaaSub, SPACES);
		if (isLT(wsaaSub, 11)) {
			uswdIO.setTgtprcnt(wsaaSub, ZERO);
			uswdIO.setTgtfund(wsaaSub, SPACES);
			uswdIO.setTgfndcur(wsaaSub, SPACES);
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 20)) {
			goTo(GotoLabel.zeroise3220);
		}
		wsaaSub.set(1);
		uswdIO.setPercentAmountInd(sv.percentAmountInd);
	}

	/**
	* <pre>
	*****MOVE S5145-OVERRIDE-FEE       TO USWD-OVERRIDE-FEE.     <007>
	* </pre>
	*/
protected void uswd3230()
	{
		/* IF S5145-PCNTAMT-01          NOT > ZERO                      */
		if (isLTE(sv.pcntamt, ZERO)) {
			goTo(GotoLabel.readSubfile3235);
		}
		formatUswd3600();
		wsaaSub.add(1);
		if (isGT(wsaaSub, 20)) {
			goTo(GotoLabel.keeps3240);
		}
	}

protected void readSubfile3235()
	{
		readSubfile3700();
		if (isNE(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.uswd3230);
		}
	}

protected void keeps3240()
	{
		uswdIO.setChdrcoy(covrpf.getChdrcoy());
		uswdIO.setChdrnum(covrpf.getChdrnum());
		uswdIO.setPlanSuffix(covrpf.getPlanSuffix());
		/* MOVE ZERO                   TO USWD-TRANNO.                  */
		uswdIO.setTranno(wsaaTranno);
		uswdIO.setLife(covrpf.getLife());
		uswdIO.setCoverage(covrpf.getCoverage());
		uswdIO.setRider(covrpf.getRider());
		uswdIO.setEffdate(sv.effdate);
		keepsUswd3500();
		keepsCovr3900();
		/*EXIT*/
	}

protected void begnUtrsswh3300()
	{
		begn3310();
	}

protected void begn3310()
	{
		/*    BEGN UTRSSWH (first time thru only)*/
		utrsswhIO.setDataKey(SPACES);
		utrsswhIO.setFunction(varcom.begn);
		utrsswhIO.setChdrcoy(chdrpf.getChdrcoy());
		utrsswhIO.setChdrnum(chdrpf.getChdrnum());
		
		//Start Life Performance atiwari23
		utrsswhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		  utrsswhIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		  //End;
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			wsaaPlanSuffix.set(ZERO);
		}
		utrsswhIO.setPlanSuffix(wsaaPlanSuffix);
		SmartFileCode.execute(appVars, utrsswhIO);
		if (isEQ(utrsswhIO.getStatuz(), varcom.oK)
		&& isEQ(utrsswhIO.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(utrsswhIO.getChdrnum(), chdrpf.getChdrnum())
		&& isEQ(utrsswhIO.getPlanSuffix(), wsaaPlanSuffix)) {
			return ;
		}
		if (isNE(utrsswhIO.getStatuz(), varcom.oK)
		&& isNE(utrsswhIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsswhIO.getParams());
			fatalError600();
		}
		utrsswhIO.setStatuz(varcom.endp);
	}

protected void nextrUtrsswh3400()
	{
		/*NEXTR*/
		utrsswhIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsswhIO);
		if (isNE(utrsswhIO.getStatuz(), varcom.oK)
		&& isNE(utrsswhIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsswhIO.getParams());
			fatalError600();
		}
		if (isEQ(utrsswhIO.getChdrcoy(), wsaaChdrcoy)
		&& isEQ(utrsswhIO.getChdrnum(), wsaaChdrnum)
		&& isEQ(utrsswhIO.getPlanSuffix(), wsaaPlanSuffix)) {
			return ;
		}
		utrsswhIO.setStatuz(varcom.endp);
		/*EXIT*/
	}

protected void keepsUswd3500()
	{
		/*KEEP*/
		uswdIO.setTranno(wsaaTranno);
		uswdIO.setFormat(formatsInner.uswdrec);
		uswdIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void formatUswd3600()
	{
		/*MOVE*/
		/*    Stores subfile details in USWD record (only where plan*/
		/*    suffix not = zero)*/
		/* MOVE S5145-CURRCY-01        TO USWD-SCFNDCUR(WSAA-SUB).      */
		/* MOVE S5145-ESTVAL-01        TO USWD-SCESTVAL(WSAA-SUB).      */
		/* MOVE S5145-FNDTYP-01        TO USWD-SCFNDTYP(WSAA-SUB).      */
		/* MOVE S5145-PCNTAMT-01       TO USWD-SCPRCAMT(WSAA-SUB).      */
		/* MOVE S5145-UNIT-VIRTUAL-FUND-01                              */
		/*                             TO USWD-SRCFUND(WSAA-SUB).       */
		uswdIO.setScfndcur(wsaaSub, sv.currcy);
		uswdIO.setScestval(wsaaSub, sv.estval);
		uswdIO.setScfndtyp(wsaaSub, sv.fndtyp);
		uswdIO.setScprcamt(wsaaSub, sv.pcntamt);
		uswdIO.setSrcfund(wsaaSub, sv.unitVirtualFund);
		/*EXIT*/
	}

protected void readSubfile3700()
	{
		/*SRDN*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void nextCovr3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case read3810: 
					read3810();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read3810()
	{
		/*   Reads next COVRMJA record if Whole Plan & not first time thru*/
		//ILIFE-8137
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
			if (isEQ(covrpf.getChdrcoy(), wsaaChdrcoy)
					&& isEQ(covrpf.getChdrnum(), wsaaChdrnum)
					&& isEQ(covrpf.getLife(), wsaaLife)
					&& isEQ(covrpf.getCoverage(), wsaaCoverage)
					&& isEQ(covrpf.getRider(), wsaaRider)) {
						wsaaPlanSuffix.set(covrpf.getPlanSuffix());
						/*****    GO 3890-EXIT                                              */
					}
		}
	/*	covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrpf.getChdrcoy(), wsaaChdrcoy)
		&& isEQ(covrpf.getChdrnum(), wsaaChdrnum)
		&& isEQ(covrpf.getLife(), wsaaLife)
		&& isEQ(covrpf.getCoverage(), wsaaCoverage)
		&& isEQ(covrpf.getRider(), wsaaRider)) {
			wsaaPlanSuffix.set(covrpf.getPlanSuffix());
			*//*****    GO 3890-EXIT                                              *//*
		}
		else {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
		/* Read the valid statii from table T5679.                         */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			headerStatuzCheck3850();
		}
		if (isNE(wsaaValidStatus, "Y")) {
			goTo(GotoLabel.read3810);
		}
	}

protected void headerStatuzCheck3850()
	{
		/*HEADER-STATUZ-CHECK*/
		if (isEQ(covrpf.getRider(), ZERO)) {
			/* IF T5679-COV-RISK-STAT(WSAA-INDEX) NOT = SPACE         <011>*/
			/*   IF (T5679-COV-RISK-STAT(WSAA-INDEX) = COVRMJA-STATCODE)<01*/
			/*  AND (T5679-COV-PREM-STAT(WSAA-INDEX) = COVRMJA-PSTATCODE)<0*/
			/*        MOVE 'Y'            TO WSAA-VALID-STATUS           <0*/
			/*           GO TO 3859-EXIT.                             <011>*/
			if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
				for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
				|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode())) {
						wsaaValidStatus = "Y";
					}
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			/* IF T5679-RID-RISK-STAT(WSAA-INDEX) NOT = SPACE         <011>*/
			/*   IF (T5679-RID-RISK-STAT(WSAA-INDEX) = COVRMJA-STATCODE)<01*/
			/*  AND (T5679-RID-PREM-STAT(WSAA-INDEX) = COVRMJA-PSTATCODE)<0*/
			/*        MOVE 'Y'            TO WSAA-VALID-STATUS           <0*/
			/*           GO TO 3859-EXIT.                             <011>*/
			if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
				for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
				|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
					if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrpf.getPstatcode())) {
						wsaaValidStatus = "Y";
					}
				}
			}
		}
		/*EXIT*/
	}

protected void keepsCovr3900()
	{
		/*KEEPS*/
		covrpfDAO.setCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		try {
			para4000();
			nextProgram4080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")
		|| isEQ(wsaaUswdExists, "Y")) {
			wsaaUswdExists.set(SPACES);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
	}

protected void nextProgram4080()
	{
		/*    Individual policy processed*/
		if (isEQ(wsaaIndPolicy, "Y")) {
			wsaaIndPolicy.set(SPACES);
			wsspcomn.programPtr.add(1);
			return ;
		}
		if (isEQ(wsaaSuffixZero, ZERO)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
			return ;
		}
		/*    Whole Plan processed & end of displays*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
	}

protected void readUswd5000()
	{
		read5010();
	}

protected void read5010()
	{
		/*    Read USWD for redisplaying of previous changes*/
		wsaaReinputSuffix.subtract(1);
		if (isLT(wsaaReinputSuffix, ZERO)) {
			wsaaSuffixZero.fill("9");
			return ;
		}
		if (isLTE(wsaaReinputSuffix, wsaaPolsum)) {
			wsaaReinputSuffix.set(ZERO);
		}
		uswdIO.setPlanSuffix(wsaaReinputSuffix);
		uswdIO.setChdrcoy(wsaaChdrcoy);
		uswdIO.setChdrnum(wsaaChdrnum);
		uswdIO.setLife(wsaaLife);
		uswdIO.setCoverage(wsaaCoverage);
		uswdIO.setRider(wsaaRider);
		uswdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, uswdIO);
		if (isEQ(uswdIO.getStatuz(), varcom.mrnf)) {
			wsaaSuffixZero.fill("9");
			return ;
		}
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
	}

protected void indPolRewrite5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					sub5110();
				case data5130: 
					data5130();
					rewrite5150();
				case exit5190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    If redisplay of previous input, rewrite USWD with new
	*    changes.
	* </pre>
	*/
protected void sub5110()
	{
		wsaaSub.set(1);
		/*SUBFILE*/
		/*    Move to top of subfile*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit5190);
		}
	}

protected void data5130()
	{
		/* IF S5145-PCNTAMT-01      > ZERO                              */
		if (isGT(sv.pcntamt, ZERO)) {
			formatUswd3600();
			wsaaSub.add(1);
		}
		/*NEXT-SUBFILE*/
		readSubfile3700();
		if (isNE(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.data5130);
		}
	}

protected void rewrite5150()
	{
		uswdIO.setFormat(formatsInner.uswdrec);
		uswdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
	}

protected void checkUswd5200()
	{
		readh5210();
	}

	/**
	* <pre>
	*    Check to see if a USWD record already exists for the
	*    selected plan suffix.
	* </pre>
	*/
protected void readh5210()
	{
		uswdIO.setDataKey(SPACES);
		uswdIO.setTranno(wsaaTranno);
		uswdIO.setChdrnum(sv.chdrnum);
		uswdIO.setChdrcoy(wsspcomn.company);
		uswdIO.setPlanSuffix(sv.planSuffix);
		uswdIO.setLife(sv.life);
		uswdIO.setCoverage(sv.coverage);
		uswdIO.setRider(sv.rider);
		uswdIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		uswdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		if (isEQ(uswdIO.getStatuz(), varcom.mrnf)) {
			wsaaNoMatch.set("Y");
		}
		else {
			wsaaNoMatch.set(SPACES);
		}
	}

protected void deleteUtrns6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					delete6010();
				case utrnLoop6020: 
					utrnLoop6020();
				case next6050: 
					next6050();
				case exit6090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void delete6010()
	{
		/*    Delete UTRN's previously written by this transactiion.       */
		/*    This only happens during modify mode.                        */
		utrnIO.setDataKey(SPACES);
		utrnIO.setChdrcoy(uswdIO.getChdrcoy());
		utrnIO.setChdrnum(uswdIO.getChdrnum());
		utrnIO.setPlanSuffix(uswdIO.getPlanSuffix());
		utrnIO.setLife(uswdIO.getLife());
		utrnIO.setCoverage(uswdIO.getCoverage());
		utrnIO.setRider(uswdIO.getRider());
		utrnIO.setTranno(wsaaTranno);
		hitrIO.setDataKey(SPACES);
		hitrIO.setChdrcoy(uswdIO.getChdrcoy());
		hitrIO.setChdrnum(uswdIO.getChdrnum());
		hitrIO.setPlanSuffix(uswdIO.getPlanSuffix());
		hitrIO.setLife(uswdIO.getLife());
		hitrIO.setCoverage(uswdIO.getCoverage());
		hitrIO.setRider(uswdIO.getRider());
		hitrIO.setTranno(wsaaTranno);
		wsaaSub.set(1);
	}

protected void utrnLoop6020()
	{
		if (isGT(wsaaSub, 20)) {
			goTo(GotoLabel.exit6090);
		}
		readT55156100();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			hitrIO.setFunction(varcom.readh);
			hitrIO.setZintbfnd(wsaaUswdSrcfund[wsaaSub.toInt()]);
			SmartFileCode.execute(appVars, hitrIO);
			if (isNE(hitrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hitrIO.getParams());
				fatalError600();
			}
			hitrIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, hitrIO);
			if (isNE(hitrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hitrIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.next6050);
		}
		utrnIO.setFunction(varcom.readh);
		utrnIO.setUnitVirtualFund(wsaaUswdSrcfund[wsaaSub.toInt()]);
		utrnIO.setUnitType(wsaaUswdScfndtyp[wsaaSub.toInt()]);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError600();
		}
		utrnIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError600();
		}
	}

protected void next6050()
	{
		wsaaSub.add(1);
		if (isEQ(wsaaUswdSrcfund[wsaaSub.toInt()], SPACES)) {
			wsaaSub.set(1);
			return ;
		}
		goTo(GotoLabel.utrnLoop6020);
	}

protected void readT55156100()
	{
		t55156110();
	}

protected void t55156110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(wsaaUswdSrcfund[wsaaSub.toInt()]);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM");
		//end;
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.endp)
		&& isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), wsaaUswdSrcfund[wsaaSub.toInt()])
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.currcy);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a100LoadSubfile()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a110Load();
				case a150AddLine: 
					a150AddLine();
				case a160NextHits: 
					a160NextHits();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Load()
	{
		/* MOVE HITS-ZINTBFND   TO S5145-UNIT-VIRTUAL-FUND-01.   <INTBR>*/
		/* MOVE 'D'                    TO S5145-FNDTYP-01.      <SM0404>*/
		/* MOVE ZEROES                 TO S5145-NOF-DUNITS-01.  <SM0404>*/
		/* MOVE HITS-ZCURPRMBAL        TO S5145-ESTVAL-01.      <SM0404>*/
		sv.unitVirtualFund.set(hitsIO.getZintbfnd());
		sv.fndtyp.set("D");
		sv.nofDunits.set(ZERO);
		sv.estval.set(hitsIO.getZcurprmbal());
		/*    Read T5515 to pick up Fund Currency                          */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		//end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(sv.effdate);
		wsaaVirtualFund.set(hitsIO.getZintbfnd());
		itdmIO.setItemitem(wsaaFundKey);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), wsaaFundKey)) {
			sv.vrtfndErr.set(errorsInner.h386);
			/*****    MOVE H386                TO S5145-VRTFND01-ERR    <SM0404>*/
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
			sv.currcy.set(t5515rec.currcode);
			/*****    MOVE T5515-CURRCODE      TO S5145-CURRCY-01       <SM0404>*/
		}
		/*    Read table T6647 to get switch rule to access T5544 table    */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		//end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTranscode.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrpf.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(sv.effdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6647)
		|| isNE(itdmIO.getItemitem(), wsaaT6647Key)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		/*    If redisplay of previous input, format percentage            */
		/*    previously input (from USWD record)                          */
		if (isNE(wsaaReinputFlag, "Y")) {
			goTo(GotoLabel.a150AddLine);
		}
		if (isEQ(wsaaNoMatch, "Y")) {
			goTo(GotoLabel.a150AddLine);
		}
		/* IF USWD-SRCFUND (WSAA-SUB)                           <SM0404>*/
		/*                  NOT = S5145-UNIT-VIRTUAL-FUND-01    <SM0404>*/
		/*    MOVE ZEROES              TO S5145-PCNTAMT-01      <SM0404>*/
		if (isNE(uswdIO.getSrcfund(wsaaSub), sv.unitVirtualFund)) {
			sv.pcntamt.set(ZERO);
			goTo(GotoLabel.a150AddLine);
		}
		else {
			/*    MOVE USWD-SCPRCAMT (WSAA-SUB) TO S5145-PCNTAMT-01 <SM0404>*/
			sv.pcntamt.set(uswdIO.getScprcamt(wsaaSub));
		}
		wsaaSub.add(1);
	}

protected void a150AddLine()
	{
		/*    Write a line to subfile                                      */
		scrnparams.function.set(varcom.sadd);
		processScreen("S5145", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void a160NextHits()
	{
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			wsaaCheckSuffix.set(ZERO);
		}
		else {
			wsaaCheckSuffix.set(covrpf.getPlanSuffix());
		}
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		if (isNE(hitsIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(hitsIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(hitsIO.getPlanSuffix(), wsaaCheckSuffix)
		|| isNE(hitsIO.getLife(), covrpf.getLife())
		|| isNE(hitsIO.getCoverage(), covrpf.getCoverage())
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			scrnparams.subfileMore.set("N");
			return ;
		}
		if (isLTE(hitsIO.getZcurprmbal(), ZERO)) {
			goTo(GotoLabel.a160NextHits);
		}
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData f070 = new FixedLengthStringData(4).init("F070");
	private FixedLengthStringData f348 = new FixedLengthStringData(4).init("F348");
	private FixedLengthStringData f665 = new FixedLengthStringData(4).init("F665");
	private FixedLengthStringData g227 = new FixedLengthStringData(4).init("G227");
	private FixedLengthStringData h385 = new FixedLengthStringData(4).init("H385");
	private FixedLengthStringData h386 = new FixedLengthStringData(4).init("H386");
	private FixedLengthStringData h387 = new FixedLengthStringData(4).init("H387");
	private FixedLengthStringData h998 = new FixedLengthStringData(4).init("H998");
	private FixedLengthStringData t044 = new FixedLengthStringData(4).init("T044");
	private FixedLengthStringData rl27 = new FixedLengthStringData(4).init("RL27");
	private FixedLengthStringData rl28 = new FixedLengthStringData(4).init("RL28");
	private FixedLengthStringData hl61 = new FixedLengthStringData(4).init("HL61");
	private FixedLengthStringData hl62 = new FixedLengthStringData(4).init("HL62");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
    private FixedLengthStringData RUJX = new FixedLengthStringData(4).init("RUJX");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
public static final class TablesInner { 
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	public FixedLengthStringData t5544 = new FixedLengthStringData(5).init("T5544");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData th506 = new FixedLengthStringData(5).init("TH506");
	private FixedLengthStringData th608 = new FixedLengthStringData(5).init("TH608");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData uswdrec = new FixedLengthStringData(10).init("USWDREC");
	private FixedLengthStringData uswhrec = new FixedLengthStringData(10).init("USWHREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitsrec = new FixedLengthStringData(10).init("HITSREC");
}
}
