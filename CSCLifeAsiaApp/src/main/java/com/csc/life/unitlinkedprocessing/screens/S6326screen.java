package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6326screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6326ScreenVars sv = (S6326ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6326screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6326ScreenVars screenVars = (S6326ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.zbinstprem.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.matage.setClassString("");
		screenVars.mattrm.setClassString("");
		screenVars.mattcessDisp.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.premcessDisp.setClassString("");
		screenVars.reserveUnitsInd.setClassString("");
		screenVars.reserveUnitsDateDisp.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.singlePremium.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.lumpContrib.setClassString("");
		screenVars.polinc.setClassString("");
		screenVars.optextind.setClassString("");
		screenVars.numavail.setClassString("");
		screenVars.numapp.setClassString("");
		screenVars.ratypind.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.select.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.taxind.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		screenVars.optsmode.setClassString("");
		screenVars.optind.setClassString("");//BRD-NBP-012 
		screenVars.singpremtype.setClassString(""); // ILIFE-7805
		screenVars.zstpduty01.setClassString("");//ILIFE-8537
	}

/**
 * Clear all the variables in S6326screen
 */
	public static void clear(VarModel pv) {
		S6326ScreenVars screenVars = (S6326ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.zbinstprem.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifenum.clear();
		screenVars.linsname.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.sumin.clear();
		screenVars.currcd.clear();
		screenVars.matage.clear();
		screenVars.mattrm.clear();
		screenVars.mattcessDisp.clear();
		screenVars.mattcess.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		screenVars.premcessDisp.clear();
		screenVars.premcess.clear();
		screenVars.reserveUnitsInd.clear();
		screenVars.reserveUnitsDateDisp.clear();
		screenVars.reserveUnitsDate.clear();
		screenVars.liencd.clear();
		screenVars.singlePremium.clear();
		screenVars.mortcls.clear();
		screenVars.lumpContrib.clear();
		screenVars.polinc.clear();
		screenVars.optextind.clear();
		screenVars.numavail.clear();
		screenVars.numapp.clear();
		screenVars.ratypind.clear();
		screenVars.zlinstprem.clear();
		screenVars.select.clear();
		screenVars.taxamt.clear();
		screenVars.taxind.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		screenVars.optsmode.clear();
		screenVars.optind.clear();//BRD-NBP-012 
		screenVars.singpremtype.clear();// ILIFE-7805
		screenVars.zstpduty01.clear();//ILIFE-8537
	}
}
