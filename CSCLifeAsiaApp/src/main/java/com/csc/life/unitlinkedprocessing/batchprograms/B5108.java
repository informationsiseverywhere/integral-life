/*
 * File: B5108.java
 * Date: 29 August 2009 20:57:04
 * Author: Quipoz Limited
 * 
 * Class transformed from B5108.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdelpfDAO;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  UDEL DELETION FOLLOWING UNIT DEALING.
*  =====================================
*
*  OVERVIEW.
*
*  This program is the first in the Unitdeal suite.  It will
*  delete all the UDELs created in the last run.  These UDELs
*  are for contracts whose UTRNs were not all processed in the
*  last run.
*
*  Control totals used in this program:
*
*  01 - No. of UDEL recs read
*  02 - UDEL contracts locked (No longer used - A07095)
*  03 - No. of UDEL recs deleted
*
*****************************************************************
* </pre>
*/
public class B5108 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5108");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);


		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct03 = 3;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private UdelpfDAO udelpfDAO = getApplicationContext().getBean("udelpfDAO", UdelpfDAO.class);
	private P6671par p6671par = new P6671par();

	public B5108() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
	}

/**
 * don't need to read first, just delete.
 */
protected void readFile2000()
	{
		return;
	}
protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	* <pre>
	*2600-SOFTLOCK-CONTRACT SECTION.                                  
	*2610-START.                                                      
	***  MOVE SPACES                    TO SFTL-SFTLOCK-REC.          
	***  MOVE 'CH'                      TO SFTL-ENTTYP.               
	***  MOVE BSPR-COMPANY              TO SFTL-COMPANY.              
	***  MOVE 99999                     TO SFTL-USER.                 
	***  MOVE BPRD-AUTH-CODE            TO SFTL-TRANSACTION.          
	***  MOVE 'LOCK'                    TO SFTL-FUNCTION.             
	***  MOVE UDEL-CHDRNUM              TO SFTL-ENTITY.               
	***  MOVE O-K                       TO SFTL-STATUZ.               
	***  CALL 'SFTLOCK'                 USING SFTL-SFTLOCK-REC.       
	***  IF SFTL-STATUZ          NOT = O-K AND                        
	***     SFTL-STATUZ          NOT = 'LOCK'                         
	***     MOVE SFTL-SFTLOCK-REC         TO SYSR-PARAMS              
	***     MOVE SFTL-STATUZ              TO SYSR-STATUZ              
	***     PERFORM 600-FATAL-ERROR                                   
	***  END-IF.                                                      
	*2199-EXIT.                                                       
	**   EXIT.                                                        
	* </pre>
	*/
protected void update3000()
 {
		int result = udelpfDAO.deleteUdelpfRecord(p6671par.chdrnum.toString(), p6671par.chdrnum1.toString(), bsprIO
				.getCompany().toString());
		/* Log number of UDEL records. */
		contotrec.totno.set(ct01);
		contotrec.totval.set(result);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(result);
		callContot001();
		wsspEdterror.set(varcom.endp);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLL*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
