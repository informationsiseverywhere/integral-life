package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:28
 * Description:
 * Copybook name: COVRUDLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrudlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrudlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrudlKey = new FixedLengthStringData(64).isAPartOf(covrudlFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrudlChdrcoy = new FixedLengthStringData(1).isAPartOf(covrudlKey, 0);
  	public FixedLengthStringData covrudlChdrnum = new FixedLengthStringData(8).isAPartOf(covrudlKey, 1);
  	public FixedLengthStringData covrudlLife = new FixedLengthStringData(2).isAPartOf(covrudlKey, 9);
  	public FixedLengthStringData covrudlCoverage = new FixedLengthStringData(2).isAPartOf(covrudlKey, 11);
  	public FixedLengthStringData covrudlRider = new FixedLengthStringData(2).isAPartOf(covrudlKey, 13);
  	public PackedDecimalData covrudlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrudlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrudlKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrudlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrudlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}