package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Covupf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CovupfDAO extends BaseDAO<Covupf>{
	public List<Covupf> searchCovuTempRecord(String tableName,String memName);
}
