/*
 * File: B5100.java
 * Date: 29 August 2009 20:53:35
 * Author: Quipoz Limited
 * 
 * Class transformed from B5100.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrxpfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdelpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UderpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdivpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrxpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Udelpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Udivpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrxpf;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This is a Transaction Splitter Program for use within the new
* multi-thread UnitDeal batch job.
*
* It has three functions which will invoke separate SQL Select
* statements. The functions are found in system param 01.
*
* DEAL: This function will extract all undealt UTRNs of any type.
*       (i.e any UTRN with a feedback indicator equal to spaces.)
*
* DEBT: This function will extract only unprocessed UTRNs which
*       have been created by the new Coverage Debt Settlement
*       (B5104) program. (i.e. feedback indicator equal to spaces
*       and a coverage debt indicator set to "D".)
*
* SWCH: This function will pick up undealt UTRNs which have been
*       created as part of a fund switch.(i.e. feedback indicator
*       equal to spaces and switch indicator equal to 'Y'.
*
* Thus a typical UnitDeal configuration which has debt processing
* as a separate transaction type would look like this:
*
*  B5100  Unit Dealing Splitter (with the DEAL function)
*  B5101  The Fund Movement Report
*  B5102  Dealing
*  B5100  Unit Dealing Splitter (with the SWCH function)
*  B5102  Dealing
*  B5103  Coverage Debt Splitter
*  B5104  Coverage Debt Settlement
*  B5100  Unit Dealing Splitter (with the DEBT function)
*  B5102  Dealing
*  B5106  Dealt Transaction Report
*  B5107  Dealing & Debt Settlement Errors Report
*
* Note that many more combinations are possible and reference
* should be made to the appropriate reference manual for further
* combinations.
*
* As a 'splitter' program it will divide the relevant records
* from UTRN to be processed.
*
* The temporary file for this UTRN extract program is UTRXPF with
* many UTRX members. The splitter program writes to as many of
* these output members as threads specified in 'No. of subsequent
* threads' parameter on the process definition for this program.
*
* Each UTRN record is written to a different UTRX member, unless
* there are many UTRNS for the same contract, in which case all
* UTRNs for the same contract will be written to the same UTRX
* member.
*
* For each break of COMPANY/CHDRNUM for the UTRX records, a
* record is written to the UDEL file for use in processing
* reversals.
*
***********************************************************************
* </pre>
*/
public class B5100 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

		/* Change the record length to that of the temporary file. This
		 can be found by doing a DSPFD of the file being duplicated
		 by the CRTTMPF process.*/
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5100");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaPrevTranno = new PackedDecimalData(5, 0).init(ZERO);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
		/* Various Counters                                                */
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSlot = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCurtot = new ZonedDecimalData(15, 0).init(ZERO).setUnsigned();
		/* WSAA-COUNT */
	private ZonedDecimalData[] wsaaRcdcnt = ZDInittedArray(20, 15, 0, ZERO, UNSIGNED_TRUE);

		/* UTRX member parameters*/
	private FixedLengthStringData wsaaUtrxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaUtrxFn, 0, FILLER).init("UTRX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaUtrxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUtrxFn, 6).setUnsigned();
		/* Host variables*/
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* These host variables are used in the SQL-SELECT. The character
		 values e.g. 'Y' cannot be used in their naked state and must be
		 replaced by working storage containing identical information.*/
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSpaces = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaD = new FixedLengthStringData(1).init("D");
	private FixedLengthStringData wsaaY = new FixedLengthStringData(1).init("Y");

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*  Pointers to point to the member to read (IY) and write (IZ).*/
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String invf = "INVF";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private P6671par p6671par = new P6671par();
	private Utrnpf wsaaFetchArrayInner;
	private UtrxpfTableDAM utrxpf = new UtrxpfTableDAM();
	private DiskFileDAM utrx01 = new DiskFileDAM("UTRX01");
	private DiskFileDAM utrx02 = new DiskFileDAM("UTRX02");
	private DiskFileDAM utrx03 = new DiskFileDAM("UTRX03");
	private DiskFileDAM utrx04 = new DiskFileDAM("UTRX04");
	private DiskFileDAM utrx05 = new DiskFileDAM("UTRX05");
	private DiskFileDAM utrx06 = new DiskFileDAM("UTRX06");
	private DiskFileDAM utrx07 = new DiskFileDAM("UTRX07");
	private DiskFileDAM utrx08 = new DiskFileDAM("UTRX08");
	private DiskFileDAM utrx09 = new DiskFileDAM("UTRX09");
	private DiskFileDAM utrx10 = new DiskFileDAM("UTRX10");
	private DiskFileDAM utrx11 = new DiskFileDAM("UTRX11");
	private DiskFileDAM utrx12 = new DiskFileDAM("UTRX12");
	private DiskFileDAM utrx13 = new DiskFileDAM("UTRX13");
	private DiskFileDAM utrx14 = new DiskFileDAM("UTRX14");
	private DiskFileDAM utrx15 = new DiskFileDAM("UTRX15");
	private DiskFileDAM utrx16 = new DiskFileDAM("UTRX16");
	private DiskFileDAM utrx17 = new DiskFileDAM("UTRX17");
	private DiskFileDAM utrx18 = new DiskFileDAM("UTRX18");
	private DiskFileDAM utrx19 = new DiskFileDAM("UTRX19");
	private DiskFileDAM utrx20 = new DiskFileDAM("UTRX20");
	private UtrxpfTableDAM utrxpfData = new UtrxpfTableDAM();
	private int ct02Value = 0;
	private int ct03Value = 0;
    private UdivpfDAO udivpfDAO = getApplicationContext().getBean("udivpfDAO", UdivpfDAO.class);
    private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
    private UdelpfDAO udelpfDAO = getApplicationContext().getBean("udelpfDAO", UdelpfDAO.class);
    private UderpfDAO uderpfDAO = getApplicationContext().getBean("uderpfDAO", UderpfDAO.class);
    private UtrxpfDAO utrxpfDAO = getApplicationContext().getBean("utrxpfDAO", UtrxpfDAO.class);
    
	private List<Udivpf> udivpfList = null;
	private List<Udelpf> udelpfList = null;
	private List<String> chdrnumList = null;
	private String QUERY_TABLE = "UTRNEXT";
	private String JOIN_UTRN_ZFMC_PAYR = "(UTRNPF U INNER JOIN ZFMCPF Z ON U.CHDRNUM=Z.CHDRNUM) INNER JOIN PAYRPF P ON U.CHDRNUM=P.CHDRNUM";
	private static final String  FMC_FEATURE_ID = "BTPRO026";
	private boolean fmcOnFlag = false;
	private static final String FMC = "FMC";
	private Datcon4rec datcon4rec = new Datcon4rec();
	private ZonedDecimalData wsaaNextdate = new ZonedDecimalData(8, 0).init(0);
	private int intBatchID = 0;
	private int intBatchExtractSize;
	private Iterator<Utrnpf> iteratorDealList;
	private Iterator<Utrnpf> iteratorDebtList;
	private Iterator<Utrnpf> iteratorSwchList;
	private List<Utrnpf> utrnpfList;
	private List<Utrxpf> insertUtrxpfList;
	private Map<String, List<Utrnpf>> utrnpfMap;
	private int iy;
	public B5100() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** The  restart section is being handled in section 1100.*/
		/** This is because the code is applicable to every run,*/
		/** not just restarts.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*    Check that the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Do ovrdbf for each temporary file member. Note that we have*/
		/*    allowed for a maximum of 20.*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Open the required number of temporary files, depending*/
		/*    on IZ and determined by the number of threads specified*/
		/*    for the subsequent process.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		bupaIO.setDataArea(lsaaBuparec);
		fmcOnFlag = FeaConfg.isFeatureExist(wsaaCompany.toString().trim(), FMC_FEATURE_ID, appVars, "IT");
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){	
			datcon4rec.frequency.set("12");
			datcon4rec.freqFactor.set(1);
			datcon4rec.intDate1.set(bsscIO.getEffectiveDate());
			datcon4rec.intDate2.set(0);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaNextdate.set(datcon4rec.intDate2.toInt());
		}
		if (isEQ(bprdIO.getSystemParam01(),"DEAL")) {
			sqlDeal1200ChunkRead();
		}
		else {
			if (isEQ(bprdIO.getSystemParam01(),"DEBT")) {
				sqlDebt1300ChunkRead();
			}
			else {
				if (isEQ(bprdIO.getSystemParam01(),"SWCH")) {
					sqlSwch1400ChunkRead();
				}
				else {
					syserrrec.syserrType.set("2");
					syserrrec.params.set(bprdIO.getSystemParam01());
					syserrrec.statuz.set(invf);
					fatalError600();
				}
			}
		}
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as*/
		/*    the array is indexed).*/
		iy = 1;
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			wsaaRcdcnt[wsaaSub.toInt()].set(ZERO);
		}
		/* Where parameter 3 on the schedule definition is not          */
		/* spaces, clear the UFNS file.                                 */
		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
			clearUfns1200();
		}
	}


protected void clearUfns1200()
	{
		/*START*/
		/* Check for multi-thread processing. If it is, clear down      */
		/* the UFNSPF file.                                             */
//		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
//			fileprcrec.function.set("CLRF");
//			fileprcrec.file1.set("UFNSPF");
//			noSource("FILEPROC", fileprcrec.params);
//			if (isNE(fileprcrec.statuz, varcom.oK)) {
//				syserrrec.statuz.set(fileprcrec.statuz);
//				syserrrec.params.set(fileprcrec.params);
//				fatalError600();
//			}
//		}
		/*EXIT*/
	if (isNE(bprdIO.getSystemParam03(),SPACES)) {
		wsaaQcmdexc.set(SPACES);
		wsaaQcmdexc.set("CLRPFM FILE(UFNSPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
	}
	}

protected void openThreadMember1100()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaUtrxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(UTRX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaUtrxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 50)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			utrx01.openOutput();
		}
		if (isEQ(iz,2)) {
			utrx02.openOutput();
		}
		if (isEQ(iz,3)) {
			utrx03.openOutput();
		}
		if (isEQ(iz,4)) {
			utrx04.openOutput();
		}
		if (isEQ(iz,5)) {
			utrx05.openOutput();
		}
		if (isEQ(iz,6)) {
			utrx06.openOutput();
		}
		if (isEQ(iz,7)) {
			utrx07.openOutput();
		}
		if (isEQ(iz,8)) {
			utrx08.openOutput();
		}
		if (isEQ(iz,9)) {
			utrx09.openOutput();
		}
		if (isEQ(iz,10)) {
			utrx10.openOutput();
		}
		if (isEQ(iz,11)) {
			utrx11.openOutput();
		}
		if (isEQ(iz,12)) {
			utrx12.openOutput();
		}
		if (isEQ(iz,13)) {
			utrx13.openOutput();
		}
		if (isEQ(iz,14)) {
			utrx14.openOutput();
		}
		if (isEQ(iz,15)) {
			utrx15.openOutput();
		}
		if (isEQ(iz,16)) {
			utrx16.openOutput();
		}
		if (isEQ(iz,17)) {
			utrx17.openOutput();
		}
		if (isEQ(iz,18)) {
			utrx18.openOutput();
		}
		if (isEQ(iz,19)) {
			utrx19.openOutput();
		}
		if (isEQ(iz,20)) {
			utrx20.openOutput();
		}
	}

	private void initList() {

		if (chdrnumList == null) {// yy
			chdrnumList = new ArrayList<>();
		} else {
			chdrnumList.clear();
		}
		if(utrnpfMap!=null){
			utrnpfMap.clear();
		}
		if (utrnpfList != null && !utrnpfList.isEmpty()) {
			Set<String> chdrnumSet = new HashSet<String>();
			for (Utrnpf u : utrnpfList) {
				chdrnumSet.add(u.getChdrnum());
			}
			chdrnumList.addAll(chdrnumSet);
			utrnpfMap = utrnpfDAO.checkUtrnRecordByChdrnum(chdrnumList);
		}
		
	}
protected void sqlDeal1200ChunkRead()
	{
		/*SQL-DEAL*/
		String tableId = getAppVars().getTableNameOverriden(QUERY_TABLE);
		utrnpfList = utrnpfDAO.sqlDeal1200ChunkRead(tableId, intBatchExtractSize, intBatchID, wsaaCompany.toString(), wsaaSpaces.toString(), wsaaChdrnumFrom.toString(), wsaaChdrnumTo.toString());
		iteratorDealList = utrnpfList.iterator();
		initList();
	}

protected void sqlDebt1300ChunkRead()
	{
		if(!(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC))){
			String tableId = getAppVars().getTableNameOverriden(QUERY_TABLE);
			utrnpfList = utrnpfDAO.sqlDebt1300ChunkRead(tableId, intBatchExtractSize, intBatchID, wsaaCompany.toString(), wsaaSpaces.toString(), wsaaD.toString(), wsaaChdrnumFrom.toString(), wsaaChdrnumTo.toString());
		}
		else {
			String tableId = JOIN_UTRN_ZFMC_PAYR;
			utrnpfList = utrnpfDAO.sqlDebt1300ChunkReadUtrnZfmcPayr(tableId, intBatchExtractSize, intBatchID, wsaaCompany.toString(), wsaaSpaces.toString(), wsaaD.toString(), wsaaChdrnumFrom.toString(), wsaaChdrnumTo.toString(), wsaaNextdate.toInt());
		}
		iteratorDebtList = utrnpfList.iterator();
		initList();
	}

protected void sqlSwch1400ChunkRead()
	{
		String tableId = getAppVars().getTableNameOverriden(QUERY_TABLE);
		utrnpfList = utrnpfDAO.sqlSwch1400ChunkRead(tableId, intBatchExtractSize, intBatchID, wsaaCompany.toString(), wsaaSpaces.toString(), wsaaY.toString(), wsaaChdrnumFrom.toString(), wsaaChdrnumTo.toString());
		iteratorSwchList = utrnpfList.iterator();
		initList();
	}

protected void readFile2000()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}

		if (isEQ(bprdIO.getSystemParam01(),"DEAL")) {
			
			if (iteratorDealList != null && iteratorDealList.hasNext()) {
				wsaaFetchArrayInner = iteratorDealList.next();
			} else {
				intBatchID++;
				sqlDeal1200ChunkRead();
				if (iteratorDealList != null && iteratorDealList.hasNext()) {
					wsaaFetchArrayInner = iteratorDealList.next();			
				} else {
					wsspEdterror.set(varcom.endp);
					return ;
				}
			}
		}
		if (isEQ(bprdIO.getSystemParam01(),"DEBT")) {
			if (iteratorDebtList != null && iteratorDebtList.hasNext()) {
				wsaaFetchArrayInner = iteratorDebtList.next();
			} else {
				intBatchID++;
				sqlDebt1300ChunkRead();
				if (iteratorDebtList != null && iteratorDebtList.hasNext()) {
					wsaaFetchArrayInner = iteratorDebtList.next();			
				} else {
					wsspEdterror.set(varcom.endp);
					return ;
				}
			}
		}
		if (isEQ(bprdIO.getSystemParam01(),"SWCH")) {
			if (iteratorSwchList != null && iteratorSwchList.hasNext()) {
				wsaaFetchArrayInner = iteratorSwchList.next();
			} else {
				intBatchID++;
				sqlSwch1400ChunkRead();
				if (iteratorSwchList != null && iteratorSwchList.hasNext()) {
					wsaaFetchArrayInner = iteratorSwchList.next();			
				} else {
					wsspEdterror.set(varcom.endp);
					return ;
				}
			}
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  If either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/

		if (firstTime.isTrue()) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.getChdrnum());
			wsaaPrevTranno.set(wsaaFetchArrayInner.getTranno());
			wsaaFirstTime.set("N");
		}
	}


protected void edit2500()
	{
		/*READ*/
		/*  Check record is required for processsing but do not do any*/
		/*  other reads to any other file.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*  If the CHDRNUM being processed is not equal to the previous*/
		/*  one we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last CHDRNUM*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next UTRX member to write to if they have changed.*/
		/* IF WSAA-CHDRNUM (WSAA-IND) NOT = WSAA-PREV-CHDRNUM           */
		/*    MOVE                                                      */
		/*    WSAA-CHDRNUM (WSAA-IND)    TO WSAA-PREV-CHDRNUM           */
		/*       ADD  1                  TO IY                          */
		/* END-IF.                                                      */
		/* Additional checking added here to ensure that                   */
		/*   a) only contracts that have at least 1 unprocessed UTRN       */
		/*       with a trigger module get extracted into thread #1        */
		/*   b) all other contracts with unprocessed UTRNs, but with       */
		/*       no associated trigger modules are evenly spread across    */
		/*       the other threads                                         */
		if (isNE(wsaaFetchArrayInner.getChdrnum(), wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.getChdrnum());
			wsaaPrevTranno.set(wsaaFetchArrayInner.getTranno());
			/* if we are only running 1 thread, no need to split UTRNs         */
			if (isGT(bprdIO.getThreadsSubsqntProc(), 1)) {
				checkForTrigger3300();
			}
			else {
				iy = 1;
			}
		}
		if (isEQ(wsaaFetchArrayInner.getChdrnum(), wsaaPrevChdrnum)
		&& isNE(wsaaFetchArrayInner.getTranno(), wsaaPrevTranno)) {
			wsaaPrevTranno.set(wsaaFetchArrayInner.getTranno());
		}
		/*  Delete the UDIV record only for dealing processing.            */
		if (isEQ(bprdIO.getSystemParam04(), "UD")) {
			deleteUdiv5000();
		}
		/*  Load from storage all UTRX data for the same contract until*/
		/*  the CHDRNUM on UTRX has changed or until the end of an*/
		/*  incomplete block is reached.*/
		loadSameContracts3200();
	}

	private String getFileName() {
		if (iy < 10) {
			return "UTRX0" + iy;
		} else {
			return "UTRX" + iy;
		}
	}

protected void loadSameContracts3200()
	{
		Utrxpf utrxpfRecord = new Utrxpf();
		utrxpfRecord.setChdrcoy(wsaaFetchArrayInner.getChdrcoy());
		utrxpfRecord.setChdrnum(wsaaFetchArrayInner.getChdrnum());
		utrxpfRecord.setLife(wsaaFetchArrayInner.getLife());
		utrxpfRecord.setCoverage(wsaaFetchArrayInner.getCoverage());
		utrxpfRecord.setRider(wsaaFetchArrayInner.getRider());
		utrxpfRecord.setPlanSuffix(wsaaFetchArrayInner.getPlanSuffix());
		utrxpfRecord.setUnitVirtualFund(wsaaFetchArrayInner.getUnitVirtualFund());
		utrxpfRecord.setUnitType(wsaaFetchArrayInner.getUnitType());
		utrxpfRecord.setTranno(wsaaFetchArrayInner.getTranno());
		utrxpfRecord.setBatctrcde(wsaaFetchArrayInner.getBatctrcde());
		utrxpfRecord.setUnitSubAccount(wsaaFetchArrayInner.getUnitSubAccount());
		utrxpfRecord.setNowDeferInd(wsaaFetchArrayInner.getNowDeferInd());
		utrxpfRecord.setNofUnits(wsaaFetchArrayInner.getNofUnits());
		utrxpfRecord.setNofDunits(wsaaFetchArrayInner.getNofDunits());
		utrxpfRecord.setMoniesDate(wsaaFetchArrayInner.getMoniesDate().intValue());
		utrxpfRecord.setCntcurr(wsaaFetchArrayInner.getCntcurr());
		utrxpfRecord.setFeedbackInd(wsaaFetchArrayInner.getFeedbackInd());
		utrxpfRecord.setContractAmount(wsaaFetchArrayInner.getContractAmount());
		utrxpfRecord.setFundAmount(wsaaFetchArrayInner.getFundAmount());
		utrxpfRecord.setContractType(wsaaFetchArrayInner.getCnttyp());
		utrxpfRecord.setProcSeqNo(wsaaFetchArrayInner.getProcSeqNo());
		utrxpfRecord.setSurrenderPercent(wsaaFetchArrayInner.getSurrenderPercent());
		utrxpfRecord.setFundRate(wsaaFetchArrayInner.getFundRate());
		utrxpfRecord.setMemberName(utrxpfData.getMember(getFileName()));
		if(insertUtrxpfList == null){
			insertUtrxpfList = new ArrayList<>();
		}
		insertUtrxpfList.add(utrxpfRecord);
		/* The following IF was used to set the thread to '1' if the Num   */
		/*   of threads has been exceeded, thus ensuring that this process */
		/*   loops around all of the extract files populating them         */
		/*   equally with unprocessed UTRNs.                               */
		/*  This has been changed because the thread extract file number   */
		/*   is set up in start of the 3100-LOAD-THREADS section.          */
		/* IF IY > BPRD-THREADS-SUBSQNT-PROC                            */
		/*     MOVE 1                  TO IY                            */
		/* END-IF.                                                      */
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		
		/*    Add 1 to the count of how many records have gone into each   */
		/*     thread                                                      */
		wsaaRcdcnt[iy].add(1);
		/*    Log the number of extracted records.*/
		ct02Value++;
		/*  Set up the array for the next block of records.*/
		/*  Check for an incomplete block retrieved.*/
		/*    Write to the UDEL file if specified in the process*/
		/*    definition.*/
		if (isEQ(bprdIO.getSystemParam01(), "DEAL")
		&& isEQ(bprdIO.getSystemParam02(), "UDEL")
		&& isNE(wsaaFetchArrayInner.getChdrnum(), wsaaPrevChdrnum)) {
			if(udelpfList==null){
				udelpfList = new ArrayList<>();
			}
			Udelpf udelIO = new Udelpf();
			udelIO.setChdrcoy(wsaaCompany.toString());
			udelIO.setChdrnum(wsaaPrevChdrnum.toString());
			udelpfList.add(udelIO);
			ct03Value++;
		}

	}

protected void checkForTrigger3300()
	{
		/* Read UTRNUTM logical to see if contract has any unprocessed     */
		/*   UTRN records with a Trigger module attached                   */
		/* If the return STATUZ is MRNF, record not found, then this       */
		/*   contract has NO unprocessed UTRNs with a trigger module       */
		/*   attached, so we will add the UTRNs to another thread, other   */
		/*   than #1 - we will add to the thread with the least number     */
		/*   of extracted records in it                                    */
		/* If the return STATUZ is OK, then an unprocessed UTRN has been   */
		/*   found WITH a trigger module attached, so for this contract    */
		/*   put ALL of its unprocessed UTRNa into thread #1               */
		boolean flag = false;
		if(utrnpfMap.containsKey(wsaaPrevChdrnum.toString())){
			List<Utrnpf> utrnpfList = utrnpfMap.get(wsaaPrevChdrnum.toString());
			for(Utrnpf u:utrnpfList){
				if(isEQ(u.getChdrcoy(),wsaaFetchArrayInner.getChdrcoy())){
					flag = true;
				}
			}
		}
		if(flag){
			iy = 1;
			return ;
		}else{
			wsaaCurtot.set(wsaaRcdcnt[2]);
			wsaaSlot.set(2);
			for (wsaaSub.set(2); !(isEQ(wsaaSub,bprdIO.getThreadsSubsqntProc())); wsaaSub.add(1)){
				compute(wsaaSub2, 0).set(add(1,wsaaSub));
				if (isGT(wsaaCurtot,wsaaRcdcnt[wsaaSub2.toInt()])) {
					wsaaCurtot.set(wsaaRcdcnt[wsaaSub2.toInt()]);
					wsaaSlot.set(wsaaSub2);
				}
			}
			iy = wsaaSlot.toInt();
		}
	}


protected void commit3500()
 {
		contotrec.totval.set(ct02Value);
		contotrec.totno.set(ct02);
		callContot001();
		ct02Value = 0;
		contotrec.totval.set(ct03Value);
		contotrec.totno.set(ct03);
		callContot001();
		ct03Value = 0;
		if (udivpfList != null) {
			udivpfDAO.deleteUdivpfRecord(udivpfList);
			udivpfList.clear();
		}
		if (udelpfList != null) {
			udelpfDAO.insertUdelpfRecord(udelpfList);
			udelpfList.clear();
		}
		if(insertUtrxpfList!=null){
			utrxpfDAO.insertUtrxRecoed(insertUtrxpfList, wsaaUtrxFn.toString());
			insertUtrxpfList.clear();
		}
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		clearUdErrorFile4010();
		closeFiles4010();
	}

protected void clearUdErrorFile4010()
	{
		/*    Clear the Unit Dealing Error file of all previous errors.*/
		if (isEQ(bprdIO.getSystemParam01(), "DEAL")) {
			uderpfDAO.deleteUderpfUTRNRecord();
		}
	}

protected void closeFiles4010()
	{
		/*  Close the open files and remove the overrides.*/
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}


protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void deleteUdiv5000()
	{
		
		Udivpf udivIO = new Udivpf();
		udivIO.setChdrcoy(wsaaCompany.toString());
		udivIO.setChdrnum(wsaaPrevChdrnum.toString());
		udivIO.setTranno(wsaaPrevTranno.toInt());
		
		if(udivpfList==null){
			udivpfList = new ArrayList<>();
		}
		udivpfList.add(udivIO);
	}
}
