package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:50
 * Description:
 * Copybook name: UTRNSURKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnsurkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnsurFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnsurKey = new FixedLengthStringData(64).isAPartOf(utrnsurFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnsurChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnsurKey, 0);
  	public FixedLengthStringData utrnsurChdrnum = new FixedLengthStringData(8).isAPartOf(utrnsurKey, 1);
  	public PackedDecimalData utrnsurTranno = new PackedDecimalData(5, 0).isAPartOf(utrnsurKey, 9);
  	public PackedDecimalData utrnsurPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnsurKey, 12);
  	public FixedLengthStringData utrnsurCoverage = new FixedLengthStringData(2).isAPartOf(utrnsurKey, 15);
  	public FixedLengthStringData utrnsurRider = new FixedLengthStringData(2).isAPartOf(utrnsurKey, 17);
  	public FixedLengthStringData utrnsurLife = new FixedLengthStringData(2).isAPartOf(utrnsurKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(utrnsurKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnsurFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnsurFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}