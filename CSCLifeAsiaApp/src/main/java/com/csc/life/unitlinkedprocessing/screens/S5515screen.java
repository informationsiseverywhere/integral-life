package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5515screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5515ScreenVars sv = (S5515ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5515screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5515ScreenVars screenVars = (S5515ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.managementCharge.setClassString("");
		screenVars.unitType.setClassString("");
		screenVars.tolerance.setClassString("");
		screenVars.initialRounding.setClassString("");
		screenVars.accumRounding.setClassString("");
		screenVars.initBidOffer.setClassString("");
		screenVars.acumbof.setClassString("");
		screenVars.initAccumSame.setClassString("");
		screenVars.btobid.setClassString("");
		screenVars.btooff.setClassString("");
		screenVars.btodisc.setClassString("");
		screenVars.otoff.setClassString("");
		screenVars.discountOfferPercent.setClassString("");
		screenVars.zfundtyp.setClassString("");
	}

/**
 * Clear all the variables in S5515screen
 */
	public static void clear(VarModel pv) {
		S5515ScreenVars screenVars = (S5515ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.currcode.clear();
		screenVars.managementCharge.clear();
		screenVars.unitType.clear();
		screenVars.tolerance.clear();
		screenVars.initialRounding.clear();
		screenVars.accumRounding.clear();
		screenVars.initBidOffer.clear();
		screenVars.acumbof.clear();
		screenVars.initAccumSame.clear();
		screenVars.btobid.clear();
		screenVars.btooff.clear();
		screenVars.btodisc.clear();
		screenVars.otoff.clear();
		screenVars.discountOfferPercent.clear();
		screenVars.zfundtyp.clear();
	}
}
