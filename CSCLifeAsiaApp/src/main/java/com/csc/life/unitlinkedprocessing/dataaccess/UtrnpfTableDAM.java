package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UtrnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:55
 * Class transformed from UTRNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UtrnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 311;
	public FixedLengthStringData utrnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData utrnpfRecord = utrnrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(utrnrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(utrnrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(utrnrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(utrnrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(utrnrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(utrnrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(utrnrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(utrnrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(utrnrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(utrnrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(utrnrec);
	public PackedDecimalData jobnoPrice = DD.jctljnumpr.copy().isAPartOf(utrnrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(utrnrec);
	public FixedLengthStringData unitSubAccount = DD.unitsa.copy().isAPartOf(utrnrec);
	public PackedDecimalData strpdate = DD.strpdate.copy().isAPartOf(utrnrec);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(utrnrec);
	public PackedDecimalData nofUnits = DD.nofunt.copy().isAPartOf(utrnrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(utrnrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(utrnrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(utrnrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(utrnrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(utrnrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(utrnrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(utrnrec);
	public PackedDecimalData priceDateUsed = DD.pricedt.copy().isAPartOf(utrnrec);
	public PackedDecimalData moniesDate = DD.moniesdt.copy().isAPartOf(utrnrec);
	public PackedDecimalData priceUsed = DD.priceused.copy().isAPartOf(utrnrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(utrnrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(utrnrec);
	public PackedDecimalData nofDunits = DD.nofdunt.copy().isAPartOf(utrnrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(utrnrec);
	public FixedLengthStringData covdbtind = DD.covdbtind.copy().isAPartOf(utrnrec);
	public PackedDecimalData unitBarePrice = DD.ubrepr.copy().isAPartOf(utrnrec);
	public PackedDecimalData inciNum = DD.incinum.copy().isAPartOf(utrnrec);
	public PackedDecimalData inciPerd01 = DD.inciperd.copy().isAPartOf(utrnrec);
	public PackedDecimalData inciPerd02 = DD.inciperd.copy().isAPartOf(utrnrec);
	public PackedDecimalData inciprm01 = DD.inciprm.copy().isAPartOf(utrnrec);
	public PackedDecimalData inciprm02 = DD.inciprm.copy().isAPartOf(utrnrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(utrnrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(utrnrec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(utrnrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(utrnrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(utrnrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(utrnrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(utrnrec);
	public FixedLengthStringData genlcde = DD.genlcde.copy().isAPartOf(utrnrec);
	public FixedLengthStringData contractType = DD.contyp.copy().isAPartOf(utrnrec);
	public FixedLengthStringData triggerModule = DD.triger.copy().isAPartOf(utrnrec);
	public FixedLengthStringData triggerKey = DD.trigky.copy().isAPartOf(utrnrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(utrnrec);
	public PackedDecimalData svp = DD.svp.copy().isAPartOf(utrnrec);
	public PackedDecimalData discountFactor = DD.discfa.copy().isAPartOf(utrnrec);
	public PackedDecimalData surrenderPercent = DD.persur.copy().isAPartOf(utrnrec);
	public PackedDecimalData crComDate = DD.crcdte.copy().isAPartOf(utrnrec);
	public FixedLengthStringData canInitUnitInd = DD.ciuind.copy().isAPartOf(utrnrec);
	public FixedLengthStringData switchIndicator = DD.swchind.copy().isAPartOf(utrnrec);
	public FixedLengthStringData fundpool = DD.fundpool.copy().isAPartOf(utrnrec);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(utrnrec);
	public PackedDecimalData scheduleNumber = DD.bschednum.copy().isAPartOf(utrnrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(utrnrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(utrnrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(utrnrec);
	public FixedLengthStringData fundPool = DD.fundpool.copy().isAPartOf(utrnrec);
	 

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UtrnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UtrnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UtrnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UtrnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UtrnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UtrnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UtrnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UTRNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"JCTLJNUMPR, " +
							"VRTFND, " +
							"UNITSA, " +
							"STRPDATE, " +
							"NDFIND, " +
							"NOFUNT, " +
							"UNITYP, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"PRICEDT, " +
							"MONIESDT, " +
							"PRICEUSED, " +
							"CRTABLE, " +
							"CNTCURR, " +
							"NOFDUNT, " +
							"FDBKIND, " +
							"COVDBTIND, " +
							"UBREPR, " +
							"INCINUM, " +
							"INCIPERD01, " +
							"INCIPERD02, " +
							"INCIPRM01, " +
							"INCIPRM02, " +
							"USTMNO, " +
							"CNTAMNT, " +
							"FNDCURR, " +
							"FUNDAMNT, " +
							"FUNDRATE, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"GENLCDE, " +
							"CONTYP, " +
							"TRIGER, " +
							"TRIGKY, " +
							"PRCSEQ, " +
							"SVP, " +
							"DISCFA, " +
							"PERSUR, " +
							"CRCDTE, " +
							"CIUIND, " +
							"SWCHIND, " +
							"BSCHEDNAM, " +
							"BSCHEDNUM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER," +
							"FUNDPOOL";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     jobnoPrice,
                                     unitVirtualFund,
                                     unitSubAccount,
                                     strpdate,
                                     nowDeferInd,
                                     nofUnits,
                                     unitType,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     priceDateUsed,
                                     moniesDate,
                                     priceUsed,
                                     crtable,
                                     cntcurr,
                                     nofDunits,
                                     feedbackInd,
                                     covdbtind,
                                     unitBarePrice,
                                     inciNum,
                                     inciPerd01,
                                     inciPerd02,
                                     inciprm01,
                                     inciprm02,
                                     ustmno,
                                     contractAmount,
                                     fundCurrency,
                                     fundAmount,
                                     fundRate,
                                     sacscode,
                                     sacstyp,
                                     genlcde,
                                     contractType,
                                     triggerModule,
                                     triggerKey,
                                     procSeqNo,
                                     svp,
                                     discountFactor,
                                     surrenderPercent,
                                     crComDate,
                                     canInitUnitInd,
                                     switchIndicator,
                                     scheduleName,
                                     scheduleNumber,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number,
                                     fundPool
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		jobnoPrice.clear();
  		unitVirtualFund.clear();
  		unitSubAccount.clear();
  		strpdate.clear();
  		nowDeferInd.clear();
  		nofUnits.clear();
  		unitType.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		priceDateUsed.clear();
  		moniesDate.clear();
  		priceUsed.clear();
  		crtable.clear();
  		cntcurr.clear();
  		nofDunits.clear();
  		feedbackInd.clear();
  		covdbtind.clear();
  		unitBarePrice.clear();
  		inciNum.clear();
  		inciPerd01.clear();
  		inciPerd02.clear();
  		inciprm01.clear();
  		inciprm02.clear();
  		ustmno.clear();
  		contractAmount.clear();
  		fundCurrency.clear();
  		fundAmount.clear();
  		fundRate.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		genlcde.clear();
  		contractType.clear();
  		triggerModule.clear();
  		triggerKey.clear();
  		procSeqNo.clear();
  		svp.clear();
  		discountFactor.clear();
  		surrenderPercent.clear();
  		crComDate.clear();
  		canInitUnitInd.clear();
  		switchIndicator.clear();
  		scheduleName.clear();
  		scheduleNumber.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
 
  		fundPool.clear();
 
   
 
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUtrnrec() {
  		return utrnrec;
	}

	public FixedLengthStringData getUtrnpfRecord() {
  		return utrnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUtrnrec(what);
	}

	public void setUtrnrec(Object what) {
  		this.utrnrec.set(what);
	}

	public void setUtrnpfRecord(Object what) {
  		this.utrnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(utrnrec.getLength());
		result.set(utrnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}