package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6647screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6647ScreenVars sv = (S6647ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6647screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6647ScreenVars screenVars = (S6647ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.swmeth.setClassString("");
		screenVars.enhall.setClassString("");
		screenVars.efdcode.setClassString("");
		screenVars.aloind.setClassString("");
		screenVars.dealin.setClassString("");
		screenVars.procSeqNo.setClassString("");
		screenVars.unitStatMethod.setClassString("");
		screenVars.monthsNegUnits.setClassString("");
		screenVars.monthsDebt.setClassString("");
		screenVars.monthsLapse.setClassString("");
		screenVars.monthsError.setClassString("");
		screenVars.bidoffer.setClassString("");
		screenVars.moniesDate.setClassString("");
		
	}

/**
 * Clear all the variables in S6647screen
 */
	public static void clear(VarModel pv) {
		S6647ScreenVars screenVars = (S6647ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.swmeth.clear();
		screenVars.enhall.clear();
		screenVars.efdcode.clear();
		screenVars.aloind.clear();
		screenVars.dealin.clear();
		screenVars.procSeqNo.clear();
		screenVars.unitStatMethod.clear();
		screenVars.monthsNegUnits.clear();
		screenVars.monthsDebt.clear();
		screenVars.monthsLapse.clear();
		screenVars.monthsError.clear();
		screenVars.bidoffer.clear();
		screenVars.moniesDate.clear();
	}
}
