package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5106.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5106Report extends SMARTReportLayout { 

	private FixedLengthStringData batctrcde = new FixedLengthStringData(4);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(1);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData nofdunts = new ZonedDecimalData(16, 5);
	private ZonedDecimalData nofrunts = new ZonedDecimalData(16, 5);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData pricedt = new FixedLengthStringData(10);
	private ZonedDecimalData priceused = new ZonedDecimalData(9, 5);
	private FixedLengthStringData schalph = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totdunts = new ZonedDecimalData(16, 5);
	private ZonedDecimalData totfndamt = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totrunts = new ZonedDecimalData(16, 5);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5106Report() {
		super();
	}


	/**
	 * Print the XML for R5106d01
	 */
	public void printR5106d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(10).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		fundamnt.setFieldName("fundamnt");
		fundamnt.setInternal(subString(recordData, 9, 17));
		priceused.setFieldName("priceused");
		priceused.setInternal(subString(recordData, 26, 9));
		nofrunts.setFieldName("nofrunts");
		nofrunts.setInternal(subString(recordData, 35, 16));
		pricedt.setFieldName("pricedt");
		pricedt.setInternal(subString(recordData, 51, 10));
		batctrcde.setFieldName("batctrcde");
		batctrcde.setInternal(subString(recordData, 61, 4));
		nofdunts.setFieldName("nofdunts");
		nofdunts.setInternal(subString(recordData, 65, 16));
		printLayout("R5106d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				fundamnt,
				priceused,
				nofrunts,
				pricedt,
				batctrcde,
				nofdunts
			}
			, new Object[] {			// indicators
				new Object[]{"ind10", indicArea.charAt(10)}
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5106e01
	 */
	public void printR5106e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R5106e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5106h01
	 */
	public void printR5106h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		vrtfnd.setFieldName("vrtfnd");
		vrtfnd.setInternal(subString(recordData, 32, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 36, 30));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 66, 3));
		curdesc.setFieldName("curdesc");
		curdesc.setInternal(subString(recordData, 69, 30));
		fundtyp.setFieldName("fundtyp");
		fundtyp.setInternal(subString(recordData, 99, 1));
		printLayout("R5106h01",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr,
				vrtfnd,
				longdesc,
				fndcurr,
				curdesc,
				fundtyp
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for R5106h02
	 */
	public void printR5106h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("R5106h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5106n01
	 */
	public void printR5106n01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(20).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		schalph.setFieldName("schalph");
		schalph.setInternal(subString(recordData, 1, 10));
		printLayout("R5106n01",			// Record name
			new BaseData[]{			// Fields:
				schalph
			}
			, new Object[] {			// indicators
				new Object[]{"ind20", indicArea.charAt(20)}
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for R5106t01
	 */
	public void printR5106t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		totfndamt.setFieldName("totfndamt");
		totfndamt.setInternal(subString(recordData, 1, 18));
		totrunts.setFieldName("totrunts");
		totrunts.setInternal(subString(recordData, 19, 16));
		totdunts.setFieldName("totdunts");
		totdunts.setInternal(subString(recordData, 35, 16));
		printLayout("R5106t01",			// Record name
			new BaseData[]{			// Fields:
				totfndamt,
				totrunts,
				totdunts
			}
		);

		currentPrintLine.add(2);
	}


}
