package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:36
 * Description:
 * Copybook name: UNLTMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Unltmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData unltmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData unltmjaKey = new FixedLengthStringData(64).isAPartOf(unltmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData unltmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(unltmjaKey, 0);
  	public FixedLengthStringData unltmjaChdrnum = new FixedLengthStringData(8).isAPartOf(unltmjaKey, 1);
  	public FixedLengthStringData unltmjaLife = new FixedLengthStringData(2).isAPartOf(unltmjaKey, 9);
  	public FixedLengthStringData unltmjaCoverage = new FixedLengthStringData(2).isAPartOf(unltmjaKey, 11);
  	public FixedLengthStringData unltmjaRider = new FixedLengthStringData(2).isAPartOf(unltmjaKey, 13);
  	public PackedDecimalData unltmjaSeqnbr = new PackedDecimalData(3, 0).isAPartOf(unltmjaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(unltmjaKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(unltmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		unltmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}