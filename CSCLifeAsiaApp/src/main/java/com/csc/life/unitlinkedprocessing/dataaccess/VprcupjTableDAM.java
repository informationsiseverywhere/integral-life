package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: VprcupjTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:34
 * Class transformed from VPRCUPJ.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class VprcupjTableDAM extends VprcpfTableDAM {

	public VprcupjTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("VPRCUPJ");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "COMPANY"
		             + ", EFFDATE"
		             + ", JOBNO";
		
		QUALIFIEDCOLUMNS = 
		            "COMPANY, " +
		            "VRTFND, " +
		            "ULTYPE, " +
		            "EFFDATE, " +
		            "JOBNO, " +
		            "UBREPR, " +
		            "UBIDPR, " +
		            "UOFFPR, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "UPDDTE, " +
		            "PROCFLAG, " +
		            "TRTM, " +
		            //ILIFE-2745-STARTS
		            "USRPRF, " +  
					"JOBNM, " +
					"DATIME, " +
					//ILIFE-2745-ENDS
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "COMPANY ASC, " +
		            "EFFDATE DESC, " +
		            "JOBNO DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "COMPANY DESC, " +
		            "EFFDATE ASC, " +
		            "JOBNO ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               company,
                               unitVirtualFund,
                               unitType,
                               effdate,
                               jobno,
                               unitBarePrice,
                               unitBidPrice,
                               unitOfferPrice,
                               validflag,
                               tranno,
                               updateDate,
                               procflag,
                               transactionTime,
                               //ILIFE-2745-STARTS
                               userProfile,
                               jobName,
                               datime,
                             //ILIFE-2745-ENDS
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(245);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getCompany().toInternal()
					+ getEffdate().toInternal()
					+ getJobno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, jobno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(company.toInternal());
	nonKeyFiller4.setInternal(effdate.toInternal());
	nonKeyFiller5.setInternal(jobno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(92);//ILIFE-2745
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ getUnitBarePrice().toInternal()
					+ getUnitBidPrice().toInternal()
					+ getUnitOfferPrice().toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getUpdateDate().toInternal()
					+ getProcflag().toInternal()
					+ getTransactionTime().toInternal()
					//ILIFE-2745-STARTS
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
					//ILIFE-2745-ENDS
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, unitBarePrice);
			what = ExternalData.chop(what, unitBidPrice);
			what = ExternalData.chop(what, unitOfferPrice);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, updateDate);
			what = ExternalData.chop(what, procflag);
			what = ExternalData.chop(what, transactionTime);		
			//ILIFE-2745-STARTS
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);
			//ILIFE-2745-ENDS
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public PackedDecimalData getJobno() {
		return jobno;
	}
	public void setJobno(Object what) {
		setJobno(what, false);
	}
	public void setJobno(Object what, boolean rounded) {
		if (rounded)
			jobno.setRounded(what);
		else
			jobno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}	
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}	
	public PackedDecimalData getUnitBarePrice() {
		return unitBarePrice;
	}
	public void setUnitBarePrice(Object what) {
		setUnitBarePrice(what, false);
	}
	public void setUnitBarePrice(Object what, boolean rounded) {
		if (rounded)
			unitBarePrice.setRounded(what);
		else
			unitBarePrice.set(what);
	}	
	public PackedDecimalData getUnitBidPrice() {
		return unitBidPrice;
	}
	public void setUnitBidPrice(Object what) {
		setUnitBidPrice(what, false);
	}
	public void setUnitBidPrice(Object what, boolean rounded) {
		if (rounded)
			unitBidPrice.setRounded(what);
		else
			unitBidPrice.set(what);
	}	
	public PackedDecimalData getUnitOfferPrice() {
		return unitOfferPrice;
	}
	public void setUnitOfferPrice(Object what) {
		setUnitOfferPrice(what, false);
	}
	public void setUnitOfferPrice(Object what, boolean rounded) {
		if (rounded)
			unitOfferPrice.setRounded(what);
		else
			unitOfferPrice.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Object what) {
		setUpdateDate(what, false);
	}
	public void setUpdateDate(Object what, boolean rounded) {
		if (rounded)
			updateDate.setRounded(what);
		else
			updateDate.set(what);
	}	
	public FixedLengthStringData getProcflag() {
		return procflag;
	}
	public void setProcflag(Object what) {
		procflag.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	//ILIFE-2745-STARTS
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//ILIFE-2745-ENDS

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		company.clear();
		effdate.clear();
		jobno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		unitVirtualFund.clear();
		unitType.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		unitBarePrice.clear();
		unitBidPrice.clear();
		unitOfferPrice.clear();
		validflag.clear();
		tranno.clear();
		updateDate.clear();
		procflag.clear();
		transactionTime.clear();		
		//ILIFE-2745-STARTS
		userProfile.clear();
		jobName.clear();
		datime.clear();
		//ILIFE-2745-ENDS
	}


}