package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:43
 * Description:
 * Copybook name: USTFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustfkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustfFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustfKey = new FixedLengthStringData(64).isAPartOf(ustfFileKey, 0, REDEFINE);
  	public FixedLengthStringData ustfChdrcoy = new FixedLengthStringData(1).isAPartOf(ustfKey, 0);
  	public FixedLengthStringData ustfChdrnum = new FixedLengthStringData(8).isAPartOf(ustfKey, 1);
  	public PackedDecimalData ustfUstmno = new PackedDecimalData(5, 0).isAPartOf(ustfKey, 9);
  	public PackedDecimalData ustfPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ustfKey, 12);
  	public FixedLengthStringData ustfLife = new FixedLengthStringData(2).isAPartOf(ustfKey, 15);
  	public FixedLengthStringData ustfCoverage = new FixedLengthStringData(2).isAPartOf(ustfKey, 17);
  	public FixedLengthStringData ustfRider = new FixedLengthStringData(2).isAPartOf(ustfKey, 19);
  	public FixedLengthStringData ustfUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(ustfKey, 21);
  	public FixedLengthStringData ustfUnitType = new FixedLengthStringData(1).isAPartOf(ustfKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(ustfKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustfFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustfFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}