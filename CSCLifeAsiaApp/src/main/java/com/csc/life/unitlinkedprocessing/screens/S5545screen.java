package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5545screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5545ScreenVars sv = (S5545ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5545screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5545ScreenVars screenVars = (S5545ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.billfreq01.setClassString("");
		screenVars.enhanaPrm01.setClassString("");
		screenVars.enhanaPc01.setClassString("");
		screenVars.enhanaPrm02.setClassString("");
		screenVars.enhanaPc02.setClassString("");
		screenVars.enhanaPrm03.setClassString("");
		screenVars.enhanaPc03.setClassString("");
		screenVars.enhanaPrm04.setClassString("");
		screenVars.enhanaPc04.setClassString("");
		screenVars.enhanaPrm05.setClassString("");
		screenVars.enhanaPc05.setClassString("");
		screenVars.billfreq02.setClassString("");
		screenVars.enhanbPrm01.setClassString("");
		screenVars.enhanbPc01.setClassString("");
		screenVars.enhanbPrm02.setClassString("");
		screenVars.enhanbPc02.setClassString("");
		screenVars.enhanbPrm03.setClassString("");
		screenVars.enhanbPc03.setClassString("");
		screenVars.enhanbPrm04.setClassString("");
		screenVars.enhanbPc04.setClassString("");
		screenVars.enhanbPrm05.setClassString("");
		screenVars.enhanbPc05.setClassString("");
		screenVars.billfreq03.setClassString("");
		screenVars.enhancPrm01.setClassString("");
		screenVars.enhancPc01.setClassString("");
		screenVars.enhancPrm02.setClassString("");
		screenVars.enhancPc02.setClassString("");
		screenVars.enhancPrm03.setClassString("");
		screenVars.enhancPc03.setClassString("");
		screenVars.enhancPrm04.setClassString("");
		screenVars.enhancPc04.setClassString("");
		screenVars.enhancPrm05.setClassString("");
		screenVars.enhancPc05.setClassString("");
		screenVars.billfreq04.setClassString("");
		screenVars.enhandPrm01.setClassString("");
		screenVars.enhandPc01.setClassString("");
		screenVars.enhandPrm02.setClassString("");
		screenVars.enhandPc02.setClassString("");
		screenVars.enhandPrm03.setClassString("");
		screenVars.enhandPc03.setClassString("");
		screenVars.enhandPrm04.setClassString("");
		screenVars.enhandPc04.setClassString("");
		screenVars.enhandPrm05.setClassString("");
		screenVars.enhandPc05.setClassString("");
		screenVars.billfreq05.setClassString("");
		screenVars.enhanePrm01.setClassString("");
		screenVars.enhanePc01.setClassString("");
		screenVars.enhanePrm02.setClassString("");
		screenVars.enhanePc02.setClassString("");
		screenVars.enhanePrm03.setClassString("");
		screenVars.enhanePc03.setClassString("");
		screenVars.enhanePrm04.setClassString("");
		screenVars.enhanePc04.setClassString("");
		screenVars.enhanePrm05.setClassString("");
		screenVars.enhanePc05.setClassString("");
		screenVars.billfreq06.setClassString("");
		screenVars.enhanfPrm01.setClassString("");
		screenVars.enhanfPc01.setClassString("");
		screenVars.enhanfPrm02.setClassString("");
		screenVars.enhanfPc02.setClassString("");
		screenVars.enhanfPrm03.setClassString("");
		screenVars.enhanfPc03.setClassString("");
		screenVars.enhanfPrm04.setClassString("");
		screenVars.enhanfPc04.setClassString("");
		screenVars.enhanfPrm05.setClassString("");
		screenVars.enhanfPc05.setClassString("");
	}

/**
 * Clear all the variables in S5545screen
 */
	public static void clear(VarModel pv) {
		S5545ScreenVars screenVars = (S5545ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.billfreq01.clear();
		screenVars.enhanaPrm01.clear();
		screenVars.enhanaPc01.clear();
		screenVars.enhanaPrm02.clear();
		screenVars.enhanaPc02.clear();
		screenVars.enhanaPrm03.clear();
		screenVars.enhanaPc03.clear();
		screenVars.enhanaPrm04.clear();
		screenVars.enhanaPc04.clear();
		screenVars.enhanaPrm05.clear();
		screenVars.enhanaPc05.clear();
		screenVars.billfreq02.clear();
		screenVars.enhanbPrm01.clear();
		screenVars.enhanbPc01.clear();
		screenVars.enhanbPrm02.clear();
		screenVars.enhanbPc02.clear();
		screenVars.enhanbPrm03.clear();
		screenVars.enhanbPc03.clear();
		screenVars.enhanbPrm04.clear();
		screenVars.enhanbPc04.clear();
		screenVars.enhanbPrm05.clear();
		screenVars.enhanbPc05.clear();
		screenVars.billfreq03.clear();
		screenVars.enhancPrm01.clear();
		screenVars.enhancPc01.clear();
		screenVars.enhancPrm02.clear();
		screenVars.enhancPc02.clear();
		screenVars.enhancPrm03.clear();
		screenVars.enhancPc03.clear();
		screenVars.enhancPrm04.clear();
		screenVars.enhancPc04.clear();
		screenVars.enhancPrm05.clear();
		screenVars.enhancPc05.clear();
		screenVars.billfreq04.clear();
		screenVars.enhandPrm01.clear();
		screenVars.enhandPc01.clear();
		screenVars.enhandPrm02.clear();
		screenVars.enhandPc02.clear();
		screenVars.enhandPrm03.clear();
		screenVars.enhandPc03.clear();
		screenVars.enhandPrm04.clear();
		screenVars.enhandPc04.clear();
		screenVars.enhandPrm05.clear();
		screenVars.enhandPc05.clear();
		screenVars.billfreq05.clear();
		screenVars.enhanePrm01.clear();
		screenVars.enhanePc01.clear();
		screenVars.enhanePrm02.clear();
		screenVars.enhanePc02.clear();
		screenVars.enhanePrm03.clear();
		screenVars.enhanePc03.clear();
		screenVars.enhanePrm04.clear();
		screenVars.enhanePc04.clear();
		screenVars.enhanePrm05.clear();
		screenVars.enhanePc05.clear();
		screenVars.billfreq06.clear();
		screenVars.enhanfPrm01.clear();
		screenVars.enhanfPc01.clear();
		screenVars.enhanfPrm02.clear();
		screenVars.enhanfPc02.clear();
		screenVars.enhanfPrm03.clear();
		screenVars.enhanfPc03.clear();
		screenVars.enhanfPrm04.clear();
		screenVars.enhanfPc04.clear();
		screenVars.enhanfPrm05.clear();
		screenVars.enhanfPc05.clear();
	}
}
