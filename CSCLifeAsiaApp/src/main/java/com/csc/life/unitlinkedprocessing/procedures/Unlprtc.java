/*
 * File: Unlprtc.java
 * Date: 30 August 2009 2:51:15
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLPRTC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.CovrpenTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
* UNLPRTC - Part Surrender Calculation Method #1.
* -----------------------------------------------
*
* This  program  is  an  item  entry  on  T6598,  the  surrender  claim
* subroutine   method  table.  This  is  the  calculation  process  for
* obtaining the Bid value of the investments, the amount is
*  returned in the currency of the fund.
*
*     The  following  is  the  linkage  information  passed   to   this
* subroutine:-
*
*            - company
*            - contract header number
*            - suffix
*            - life number
*            - joint-life number
*            - coverage
*            - rider
*            - crtable
*            - effective date
*            - language
*            - estimated value
*            - actual value
*            - currency
*            - element code
*            - description
*            - type code
*            - status
*
* PROCESSING.
* ----------
*
* If it is the first time through this routine:
*
*     NB.   -   accumulate  all  records/amounts  for  the  same  fund,
* including initial and accumulation.
*
*     - set-up the key and BEGN option  and read fund UTRS
*         keyed:  Coy, Chdr, Life, Jlife, Coverage, Rider
*
*     - for each fund on component get the now bid price
*         read the VPRC (prices file), keyed on:
*         - Virtual fund, Unit type and Effective-date.
*
* If "INITIAL" units
*
*
*     The no. of initial deemed units are multiplied by the
*      initial bid price. This result is the surrender value
*     amount of the initial units.
* If "ACCUMULATION" units
*
*     The no. of accumulated deemed units are multiplied by the
*     accumulation bid price.This result is the surrender value
*     amount of the accumulated units.
*
*     Return one line for each fund.
*
*
*
*
*     Sum the amount for Policy or Plan.
*     Calculate the fee amount
*
*     Read table T5687 with a key of CRTABLE and read T5542
*     with a composite key of Surrender Calculation Method +
*     Currency Code.
*
*     Perform 5 times or until SURC-BILLFREQ = table frequency
*     If correct frequency  If fee amount      NOT = 0
*           Move  fee amount to SURC-ACTUAL-VAL
*           exit section
*
*       If correct frequency
*          If fee percentage   = 0
*             Move 0 to SURC-ACTUAL-VAL
*          Else
*             Move fee percentage to  SURC-ACTUAL-VAL
*
*       If correct frequency
*          Compute estimated total amount = (estimated total
*             amount * fee percentage) / 100
*          If estimated total amount < minimum fee
*             Move minimum fee to SURC-ACTUAL-VAL
*          Else
*             If estimated total amount > maximum fee
*                Move maximum fee     to SURC-ACTUAL-VAL
*             Else
*                Move estimated total amount to SURC-ACTUAL-VAL
*     End perform.
*
*     If minimum withdrawal amount > previous estimated total
*        Move 'Y'    to SURC-NE-UNITS
*
*     If maximum withdrawal amount < previous estimated total
*        Move 'Y'    to SURC-TM-UNITS
*
*
*     Return Amount,  Description, Type
*
* else
*
*     set status = ENDP.
*****************************************************************
* </pre>
*/
public class Unlprtc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected final String wsaaSubr = "UNLPRTC";
	protected PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);

	protected ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator policy1 = new Validator(wsaaPlnsfx, "1");

	protected ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator wholePlan = new Validator(wsaaPlanSwitch, "1");
	private Validator partPlan = new Validator(wsaaPlanSwitch, "2");
	protected Validator summaryPart = new Validator(wsaaPlanSwitch, "3");
	protected ZonedDecimalData wsaaSurcCcdate = new ZonedDecimalData(8, 0).setUnsigned();
	protected ZonedDecimalData wsaaSurcCesdte = new ZonedDecimalData(8, 0).setUnsigned();
	protected FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	protected PackedDecimalData wsaaItmfrm = new PackedDecimalData(8, 0);
	protected ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();

	protected FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT5551Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5551Item, 0);
	protected FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(1).isAPartOf(wsaaT5551Item, 4);
	protected FixedLengthStringData wsaaT5551Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);
	protected static final String h141 = "H141";
	protected static final String g094 = "G094";
		/* TABLES */
	protected static final String t5515 = "T5515";
	protected static final String t5542 = "T5542";
	protected static final String t5687 = "T5687";
	protected static final String t6644 = "T6644";
	protected static final String t6598 = "T6598";
	protected static final String t5551 = "T5551";
	protected static final String descrec = "DESCREC";
	protected PackedDecimalData wsaaInitialAmount = new PackedDecimalData(17, 5).init(0);
	protected PackedDecimalData wsaaAccumAmount = new PackedDecimalData(17, 5).init(0);
	protected PackedDecimalData wsaaInitialBidPrice = new PackedDecimalData(9, 5).init(0);
	protected PackedDecimalData wsaaAccumBidPrice = new PackedDecimalData(9, 5).init(0);
	protected FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
	protected FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	protected FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	protected ZonedDecimalData wsaaInd = new ZonedDecimalData(2, 0).setUnsigned();

	protected FixedLengthStringData wsaaNoMore = new FixedLengthStringData(1);
	protected Validator endOfFund = new Validator(wsaaNoMore, "Y");

	protected FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	protected Validator gotFreq = new Validator(wsaaGotFreq, "Y");
		/* WSAA-FREQUENCIES */
	private FixedLengthStringData wsaaFreq1 = new FixedLengthStringData(10).init("0001020412");
	protected PackedDecimalData wsaaEstimateTotAmt = new PackedDecimalData(18, 5).init(0);
	protected PackedDecimalData wsaaEstimateTotAmt1 = new PackedDecimalData(18, 5).init(0);
	protected PackedDecimalData wsaaPercentage = new PackedDecimalData(5, 2).init(0);

	protected FixedLengthStringData wsaaUtype = new FixedLengthStringData(1);
	protected Validator initialUnits = new Validator(wsaaUtype, "I");

	protected FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	protected Validator firstTime = new Validator(wsaaSwitch, "Y");

	private FixedLengthStringData wsaaNoT6656Entry = new FixedLengthStringData(1);
	private Validator noT6656Entry = new Validator(wsaaNoT6656Entry, "Y");

	protected FixedLengthStringData wsaaT5542Key = new FixedLengthStringData(7);
	protected FixedLengthStringData wsaaT5542Meth = new FixedLengthStringData(4).isAPartOf(wsaaT5542Key, 0);
	protected FixedLengthStringData wsaaT5542Curr = new FixedLengthStringData(3).isAPartOf(wsaaT5542Key, 4);

	protected FixedLengthStringData wsaaReadTable = new FixedLengthStringData(1);
	private Validator haveTabDets = new Validator(wsaaReadTable, "Y");
	protected CovrpenTableDAM covrpenIO = new CovrpenTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected HitsTableDAM hitsIO = new HitsTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
	protected VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	protected Datcon3rec datcon3rec = new Datcon3rec();
	protected Syserrrec syserrrec = new Syserrrec();
	protected T5515rec t5515rec = new T5515rec();
	protected T5542rec t5542rec = new T5542rec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	protected T5551rec t5551rec = new T5551rec();
	protected Varcom varcom = new Varcom();
	private Srcalcpy srcalcpy = new Srcalcpy();
  	public PackedDecimalData wsaaNewactualVal = new PackedDecimalData(17, 2).init(0);
  	private ExternalisedRules er = new ExternalisedRules();
  	private static final String UNLPRTCVPM = "UNLPRTCVPM";
  	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private Utrspf utrspf=new Utrspf();
	private List<Utrspf> utrspfList=new ArrayList<>();
	Iterator<Utrspf> iterator ;
	boolean status = false;
	boolean endOfFile = false ;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	public enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readAccum152, 
		partSurr550, 
		setType570, 
		exit590, 
		next750, 
		exit790, 
		setType870
	}

	public Unlprtc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		srcalcpy.status.set("****");
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(srcalcpy.endf, "J")) {
			calcSurrCharge800();
			return ;
		}
		if (isEQ(srcalcpy.endf, "Y")) {
			/*    IF (UTRSSUR-STATUZ           = 'ENDP') OR                    */
			/*       (UTRSCLM-STATUZ           = 'ENDP')                       */
			calcSurrFeeVal500();
			return ;
		}
		if (firstTime.isTrue()) {
			wsaaChdrcoy.set(srcalcpy.chdrChdrcoy);
			wsaaChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaCoverage.set(srcalcpy.covrCoverage);
			wsaaRider.set(srcalcpy.covrRider);
			wsaaLife.set(srcalcpy.lifeLife);
			wsaaPlanSuffix.set(srcalcpy.planSuffix);
			wsaaSurcCcdate.set(srcalcpy.crrcd);
			wsaaSurcCesdte.set(srcalcpy.convUnits);
			/*    MOVE SURC-CURRCODE       TO WSAA-T5542-CURR               */
			wsaaT5542Curr.set(srcalcpy.chdrCurr);
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			wsaaEstimateTotAmt.set(ZERO);
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			wsaaVirtualFund.set(SPACES);
			wsaaUnitType.set(SPACES);
			wsaaReadTable.set(SPACES);
			srcalcpy.description.set(SPACES);
			srcalcpy.neUnits.set(SPACES);
			srcalcpy.tmUnits.set(SPACES);
			srcalcpy.psNotAllwd.set(SPACES);
			/* decide which part of the plan is being surrended*/
			if (isEQ(srcalcpy.planSuffix, ZERO)) {
				wsaaPlanSwitch.set(1);
			}
			else {
				if ((isLTE(srcalcpy.planSuffix, srcalcpy.polsum))
				&& (isNE(srcalcpy.polsum, 1))) {
					wsaaPlanSuffix.set(0);
					wsaaPlanSwitch.set(3);
					if (isEQ(srcalcpy.planSuffix, 1)) {
						wsaaPlnsfx.set(1);
					}
					else {
						wsaaPlnsfx.set(0);
					}
				}
				else {
					wsaaPlanSwitch.set(2);
				}
			}
		}
		if (summaryPart.isTrue()) {
			srcalcpy.planSuffix.set(0);
		}
		wsaaNoMore.set("N");
		if (isEQ(srcalcpy.endf, "I")) {
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
		}
		if (isEQ(srcalcpy.status, "****")) {
			if (isEQ(srcalcpy.endf, "I")) {
				a100ReadInterestBearing();
			}
			else {
				if (wholePlan.isTrue()) {
					readFundsWholePlan100();
				}
				else {
					readFundsPartPlan130();
				}
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readFundsWholePlan100()
	{
		para101();
	}

protected void para101()
	{
		/* MOVE SPACES                 TO UTRSSUR-PARAMS.               */
		/* MOVE WSAA-CHDRCOY           TO UTRSSUR-CHDRCOY.              */
		/* MOVE WSAA-CHDRNUM           TO UTRSSUR-CHDRNUM.              */
		/* MOVE WSAA-LIFE              TO UTRSSUR-LIFE.                 */
		/* MOVE WSAA-COVERAGE          TO UTRSSUR-COVERAGE.             */
		/* MOVE WSAA-RIDER             TO UTRSSUR-RIDER.                */
		/* MOVE ZERO                   TO UTRSSUR-PLAN-SUFFIX.          */
		/* MOVE WSAA-VIRTUAL-FUND      TO UTRSSUR-UNIT-VIRTUAL-FUND.    */
		/* MOVE WSAA-UNIT-TYPE         TO UTRSSUR-UNIT-TYPE.            */
		/* MOVE 'BEGN'                 TO UTRSSUR-FUNCTION.             */
		/* CALL 'UTRSSURIO'            USING UTRSSUR-PARAMS.            */
		/* IF (UTRSSUR-STATUZ       NOT = '****') AND                   */
		/*    (UTRSSUR-STATUZ       NOT = 'ENDP')                       */
		/*    MOVE UTRSSUR-PARAMS      TO SYSR-PARAMS                   */
		/* IF (UTRSSUR-CHDRCOY      NOT = SURC-CHDR-CHDRCOY)  OR        */
		/*    (UTRSSUR-CHDRNUM      NOT = SURC-CHDR-CHDRNUM)  OR        */
		/*    (UTRSSUR-LIFE         NOT = SURC-LIFE-LIFE)     OR        */
		/*    (UTRSSUR-COVERAGE     NOT = SURC-COVR-COVERAGE) OR        */
		/*    (UTRSSUR-RIDER        NOT = SURC-COVR-RIDER)    OR        */
		/*    (UTRSSUR-STATUZ           = 'ENDP')                       */
	
	if(firstTime.isTrue()){                            //IBPLIFE-6795 
	utrspf.setChdrcoy(wsaaChdrcoy.toString());
	utrspf.setChdrnum(wsaaChdrnum.toString());
	utrspf.setLife(wsaaLife.toString());
	utrspf.setCoverage(wsaaCoverage.toString());
	utrspf.setRider(wsaaRider.toString());
	utrspf.setUnitVirtualFund(wsaaVirtualFund.toString());
	utrspf.setUnitType(wsaaUnitType.toString());
	utrspfList=utrspfDAO.searchUtrsRecord(utrspf);

	if(null==utrspfList){
		syserrrec.params.set(wsaaChdrcoy.toString()+wsaaChdrnum.toString());
		fatalError9000();	
	}
	else{
		iterator= utrspfList.iterator();
	}
			/*    MOVE 'Y'  TO  SURC-ENDF                                   */
	if(iterator.hasNext()){
		utrspf = iterator.next();
	}
	else{
			srcalcpy.endf.set("I");
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaCoverage.set(srcalcpy.covrCoverage);
			wsaaRider.set(srcalcpy.covrRider);
			wsaaLife.set(srcalcpy.lifeLife);
			/*    MOVE 'ENDP'              TO    UTRSSUR-STATUZ             */
			return ;
		}
	}                                                                         
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		/* MOVE UTRSSUR-UNIT-VIRTUAL-FUND TO WSAA-VIRTUAL-FUND.         */
		/* MOVE UTRSSUR-UNIT-TYPE         TO WSAA-UNIT-TYPE.            */
	wsaaVirtualFund.set(utrspf.getUnitVirtualFund());
	wsaaUnitType.set(utrspf.getUnitType());
		while ( !(endOfFund.isTrue()
			|| isNE(utrspf.getUnitVirtualFund(), wsaaVirtualFund)
			|| isNE(utrspf.getUnitType(), wsaaUnitType))) {
			accumAmtWholePlan120();
		}
		
		/* IF (UTRSSUR-STATUZ          = 'ENDP') OR                     */
		/*    (UTRSSUR-STATUZ          = '****')                        */

	if(endOfFile || status){
			
			//ILIFE-9394
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(UNLPRTCVPM) && 
					er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))) {
				readVprnunl150();
				checkEof200();
			}
			else {
				calcFundValues();
			}

			/*      MOVE UTRSSUR-UNIT-VIRTUAL-FUND TO WSAA-VIRTUAL-FUND     */
			/*      MOVE UTRSSUR-UNIT-TYPE         TO WSAA-UNIT-TYPE        */
		wsaaVirtualFund.set(utrspf.getUnitVirtualFund());
		wsaaUnitType.set(utrspf.getUnitType());
			/*      MOVE UTRSSUR-LIFE              TO WSAA-LIFE             */
			/*      MOVE UTRSSUR-RIDER             TO WSAA-RIDER            */
			/*      MOVE UTRSSUR-COVERAGE          TO WSAA-COVERAGE         */
			/*      MOVE UTRSSUR-CHDRNUM           TO WSAA-CHDRNUM          */
		wsaaLife.set(utrspf.getLife());
		wsaaRider.set(utrspf.getRider());
		wsaaCoverage.set(utrspf.getCoverage());
		wsaaChdrnum.set(utrspf.getChdrnum());
			if (isEQ(srcalcpy.endf, "I")) {
				wsaaVirtualFund.set(SPACES);
				wsaaChdrnum.set(srcalcpy.chdrChdrnum);
				wsaaCoverage.set(srcalcpy.covrCoverage);
				wsaaRider.set(srcalcpy.covrRider);
				wsaaLife.set(srcalcpy.lifeLife);
			}
			wsaaSwitch.set("N");
		}
	}

protected void calcFundValues()
{
	srcalcpy.estimatedVal.set(ZERO);
	Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
	vpxsurcrec.curduntbal.set(wsaaAccumAmount);
	vpxsurcrec.chargeCalc.set(srcalcpy.surrCalcMeth);
	srcalcpy.fund.set(wsaaVirtualFund);
	srcalcpy.type.set(utrspf.getUnitType());
	
	callProgram(UNLPRTCVPM, srcalcpy.surrenderRec, vpxsurcrec);//VPMS call
	if (isNE(srcalcpy.status,varcom.oK)
	&& isNE(srcalcpy.status,varcom.endp)) {
		fatalError9000();
	}
	wsaaEstimateTotAmt.add(srcalcpy.estimatedVal);
}

protected void accumAmtWholePlan120()
	{
		para121();
		readNextUtrs123();
	}

protected void para121()
	{
		/* IF UTRSSUR-UNIT-TYPE        = 'I'                            */
		if (isEQ(utrspf.getUnitType(), "I")) {
			/*     ADD UTRSSUR-CURRENT-UNIT-BAL TO WSAA-INITIAL-AMOUNT      */
			wsaaInitialAmount.add(PackedDecimalData.parseObject(utrspf.getCurrentUnitBal()));
			wsaaUtype.set("I");
		}
		else {
			/*     ADD UTRSSUR-CURRENT-UNIT-BAL TO WSAA-ACCUM-AMOUNT        */
			wsaaAccumAmount.add(PackedDecimalData.parseObject(utrspf.getCurrentUnitBal()));
			wsaaUtype.set("A");
		}
	}

	/**
	* <pre>
	**** MOVE 'NEXTR'                TO UTRSSUR-FUNCTION.             
	**** CALL 'UTRSSURIO'            USING UTRSSUR-PARAMS.            
	**** IF (UTRSSUR-STATUZ          NOT = '****') AND                
	****    (UTRSSUR-STATUZ          NOT = 'ENDP')                    
	****    MOVE UTRSSUR-PARAMS         TO SYSR-PARAMS                
	* </pre>
	*/
protected void readNextUtrs123()
	{

	if (iterator.hasNext()){
		utrspf=iterator.next();
		status = true;

	}
	else
	{
		endOfFile = true;
		}
		/* IF UTRSSUR-CHDRCOY             NOT = SURC-CHDR-CHDRCOY OR    */
		/*    UTRSSUR-CHDRNUM             NOT = SURC-CHDR-CHDRNUM OR    */
		/*    UTRSSUR-LIFE                NOT = SURC-LIFE-LIFE OR       */
		/*    UTRSSUR-COVERAGE            NOT = SURC-COVR-COVERAGE OR   */
		/*    UTRSSUR-RIDER               NOT = SURC-COVR-RIDER OR      */
		/*    UTRSSUR-STATUZ              = 'ENDP'                      */
	if (isNE(utrspf.getChdrcoy(), srcalcpy.chdrChdrcoy)
			|| isNE(utrspf.getChdrnum(), srcalcpy.chdrChdrnum)
			|| isNE(utrspf.getLife(), srcalcpy.lifeLife)
			|| isNE(utrspf.getCoverage(), srcalcpy.covrCoverage)
			|| isNE(utrspf.getRider(), srcalcpy.covrRider)
			|| (endOfFile)) {
			/* A new coverage (or end of file) has been read, therefore,       */
			/* set ENDF processing indicator to "Y". This will be picked       */
			/* up immediately next time through and the PENALTY value          */
			/* will be then calculated. Avoids a needless call to the          */
			/* program between these two 'modes' of processing and hence       */
			/* eliminates the situation whereby, control is passed back        */
			/* to the calling program but there is no record to write to       */
			/* the subfile.                                                    */
			/*    MOVE 'Y'                    TO SURC-ENDF          <INTBR> */
			srcalcpy.endf.set("I");
			wsaaNoMore.set("Y");
		}
		/*EXIT*/
	}

protected void readFundsPartPlan130()
	{
		para131();
	}

protected void para131()
	{
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setChdrcoy(wsaaChdrcoy);
		utrsclmIO.setChdrnum(wsaaChdrnum);
		utrsclmIO.setLife(wsaaLife);
		utrsclmIO.setCoverage(wsaaCoverage);
		utrsclmIO.setRider(wsaaRider);
		utrsclmIO.setPlanSuffix(wsaaPlanSuffix);
		utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsclmIO.setUnitType(wsaaUnitType);
		utrsclmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, utrsclmIO);
		if ((isNE(utrsclmIO.getStatuz(), "****"))
		&& (isNE(utrsclmIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if ((isNE(utrsclmIO.getChdrcoy(), srcalcpy.chdrChdrcoy))
		|| (isNE(utrsclmIO.getChdrnum(), srcalcpy.chdrChdrnum))
		|| (isNE(utrsclmIO.getLife(), srcalcpy.lifeLife))
		|| (isNE(utrsclmIO.getCoverage(), srcalcpy.covrCoverage))
		|| (isNE(utrsclmIO.getRider(), srcalcpy.covrRider))
		|| (isNE(utrsclmIO.getPlanSuffix(), srcalcpy.planSuffix))
		|| (isEQ(utrsclmIO.getStatuz(), "ENDP"))) {
			/*    MOVE 'Y'  TO  SURC-ENDF                                   */
			srcalcpy.endf.set("I");
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaCoverage.set(srcalcpy.covrCoverage);
			wsaaRider.set(srcalcpy.covrRider);
			wsaaLife.set(srcalcpy.lifeLife);
			utrsclmIO.setStatuz("ENDP");
			return ;
		}
		wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsclmIO.getUnitType());
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		while ( !(endOfFund.isTrue()
		|| isNE(utrsclmIO.getUnitVirtualFund(), wsaaVirtualFund)
		|| isNE(utrsclmIO.getUnitType(), wsaaUnitType))) {
			accumAmtPartPlan140();
		}
		
		if ((isEQ(utrsclmIO.getStatuz(), "****"))
		|| (isEQ(utrsclmIO.getStatuz(), "ENDP"))) {
			readVprnunl150();
			checkEof200();
			wsaaChdrnum.set(utrsclmIO.getChdrnum());
			wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
			wsaaUnitType.set(utrsclmIO.getUnitType());
			wsaaLife.set(utrsclmIO.getLife());
			wsaaRider.set(utrsclmIO.getRider());
			wsaaCoverage.set(utrsclmIO.getCoverage());
			wsaaPlanSuffix.set(utrsclmIO.getPlanSuffix());
			if (isEQ(srcalcpy.endf, "I")) {
				wsaaVirtualFund.set(SPACES);
				wsaaChdrnum.set(srcalcpy.chdrChdrnum);
				wsaaCoverage.set(srcalcpy.covrCoverage);
				wsaaRider.set(srcalcpy.covrRider);
				wsaaLife.set(srcalcpy.lifeLife);
			}
			wsaaSwitch.set("N");
		}
	}

protected void accumAmtPartPlan140()
	{
		para141();
		readNextUtrsclm143();
	}

protected void para141()
	{
		if (isEQ(utrsclmIO.getUnitType(), "I")) {
			wsaaInitialAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("A");
		}
	}

protected void readNextUtrsclm143()
	{
		utrsclmIO.setFunction("NEXTR");
		SmartFileCode.execute(appVars, utrsclmIO);
		if ((isNE(utrsclmIO.getStatuz(), "****"))
		&& (isNE(utrsclmIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if ((isNE(utrsclmIO.getChdrcoy(), srcalcpy.chdrChdrcoy))
		|| (isNE(utrsclmIO.getChdrnum(), srcalcpy.chdrChdrnum))
		|| (isNE(utrsclmIO.getLife(), srcalcpy.lifeLife))
		|| (isNE(utrsclmIO.getCoverage(), srcalcpy.covrCoverage))
		|| (isNE(utrsclmIO.getRider(), srcalcpy.covrRider))
		|| (isNE(utrsclmIO.getPlanSuffix(), srcalcpy.planSuffix))
		|| (isEQ(utrsclmIO.getStatuz(), "ENDP"))) {
			/* A new coverage/plan suffix (or end of file) has been read,      */
			/* set ENDF processing indicator to "Y". This will be picked       */
			/* up immediately next time through and the PENALTY value          */
			/* will be then calculated. Avoids a needless call to the          */
			/* program between these two 'modes' of processing and hence       */
			/* eliminates the situation whereby, control is passed back        */
			/* to the calling program but there is no record to write to       */
			/* the subfile.                                                    */
			/*    MOVE 'Y'                    TO SURC-ENDF          <INTBR> */
			srcalcpy.endf.set("I");
			wsaaNoMore.set("Y");
		}
		/*EXIT*/
	}

protected void readVprnunl150()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read151();
				case readAccum152: 
					readAccum152();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read151()
	{
		/*  read the vprc file for each fund type*/
		if (isEQ(wsaaInitialAmount, ZERO)) {
			goTo(GotoLabel.readAccum152);
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(srcalcpy.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("I");
		vprnudlIO.setEffdate(srcalcpy.effdate);
		vprnudlIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, vprnudlIO);
		if ((isNE(vprnudlIO.getStatuz(), "****"))
		&& (isNE(vprnudlIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if ((isNE(wsaaVirtualFund, vprnudlIO.getUnitVirtualFund()))
		|| (isNE(vprnudlIO.getUnitType(), "I"))
		|| (isNE(srcalcpy.chdrChdrcoy, vprnudlIO.getCompany()))
		|| isEQ(vprnudlIO.getStatuz(), "ENDP")) {
			srcalcpy.status.set("MRNF");
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		wsaaInitialBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void readAccum152()
	{
		if (isEQ(wsaaAccumAmount, ZERO)) {
			return ;
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(srcalcpy.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("A");
		vprnudlIO.setEffdate(srcalcpy.effdate);
		vprnudlIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, vprnudlIO);
		if ((isNE(vprnudlIO.getStatuz(), "****"))
		&& (isNE(vprnudlIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if ((isNE(wsaaVirtualFund, vprnudlIO.getUnitVirtualFund()))
		|| (isNE(vprnudlIO.getUnitType(), "A"))
		|| (isNE(srcalcpy.chdrChdrcoy, vprnudlIO.getCompany()))
		|| isEQ(vprnudlIO.getStatuz(), "ENDP")) {
			srcalcpy.status.set("MRNF");
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		wsaaAccumBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void checkEof200()
	{
		eof210();
	}

protected void eof210()
	{
		if (isEQ(srcalcpy.type, "P")) {
			if (initialUnits.isTrue()) {
				compute(wsaaInitialAmount, 5).set(mult(wsaaInitialAmount, wsaaInitialBidPrice));
			}
			else {
				compute(wsaaAccumAmount, 5).set(mult(wsaaAccumAmount, wsaaAccumBidPrice));
			}
		}
		if (initialUnits.isTrue()) {
			/*     MOVE WSAA-INITIAL-AMOUNT TO SURC-ESTIMATED-VAL           */
			compute(srcalcpy.estimatedVal, 6).setRounded(mult(wsaaInitialAmount, 1));
			srcalcpy.type.set("I");
		}
		else {
			srcalcpy.type.set("A");
			compute(srcalcpy.estimatedVal, 6).setRounded(mult(wsaaAccumAmount, 1));
			/*****     MOVE WSAA-ACCUM-AMOUNT   TO SURC-ESTIMATED-VAL           */
		}
		wsaaEstimateTotAmt.add(srcalcpy.estimatedVal);
		srcalcpy.fund.set(wsaaVirtualFund);
		readTable300();
	}

protected void getSurrenderMeth230()
	{
		read231();
	}

protected void read231()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(wsaaItmfrm);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), "****"))
		&& (isNE(itdmIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(), srcalcpy.chdrChdrcoy))
		|| (isNE(itdmIO.getItemtabl(), t5687))
		|| (isNE(itdmIO.getItemitem(), wsaaItemitem))
		|| (isEQ(itdmIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableT5542280()
	{
		go281();
	}

protected void go281()
	{
		if (isEQ(t5687rec.partsurr, SPACES)) {
			srcalcpy.psNotAllwd.set("T");
			return ;
		}
		wsaaReadTable.set("Y");
		wsaaT5542Meth.set(t5687rec.partsurr);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5542);
		itdmIO.setItemitem(wsaaT5542Key);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), "****"))
		&& (isNE(itdmIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(), srcalcpy.chdrChdrcoy))
		|| (isNE(itdmIO.getItemtabl(), t5542))
		|| (isNE(itdmIO.getItemitem(), wsaaT5542Key))
		|| (isEQ(itdmIO.getStatuz(), "ENDP"))) {
			srcalcpy.psNotAllwd.set("T");
		}
		else {
			t5542rec.t5542Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTable300()
	{
		go300();
	}

protected void go300()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), "****"))
		&& (isNE(descIO.getStatuz(), "MRNF"))) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(), "****")) {
			srcalcpy.description.set(descIO.getLongdesc());
		}
		else {
			srcalcpy.description.set(SPACES);
		}
		
		Itempf itempf = itemDAO.findItemByItem(srcalcpy.chdrChdrcoy.toString(), t5515, wsaaVirtualFund.toString());
		if(null  ==  itempf){
			syserrrec.params.set(srcalcpy.chdrChdrcoy.toString()+t5515+wsaaVirtualFund.toString());
			fatalError9000();
		}
		else if (itempf.getItemitem().trim().equals("")){
			srcalcpy.currcode.set(SPACES);
			return ;
		}
		else {
			t5515rec.t5515Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			srcalcpy.currcode.set(t5515rec.currcode);
		}
		srcalcpy.element.set(wsaaVirtualFund);
	}

protected void calcSurrFeeVal500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para500();
				case partSurr550: 
					partSurr550();
				case setType570: 
					setType570();
				case exit590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para500()
	{
		srcalcpy.actualVal.set(ZERO);
		if (summaryPart.isTrue()) {
			if (policy1.isTrue()) {
				compute(srcalcpy.singp, 2).set((sub(srcalcpy.singp, (div(mult(srcalcpy.singp, (sub(srcalcpy.polsum, 1))), srcalcpy.polsum)))));
			}
			else {
				compute(srcalcpy.singp, 3).setRounded(div(srcalcpy.singp, srcalcpy.polsum));
			}
		}
		if (isEQ(srcalcpy.type, "P") || isEQ(srcalcpy.type, "J")) {
			srcalcpy.actualVal.set(ZERO);
			goTo(GotoLabel.partSurr550);
		}
		else {
			srcalcpy.actualVal.set(ZERO);
			goTo(GotoLabel.setType570);
		}
	}

protected void partSurr550()
	{
		/* The frequency codes on the table T5542 have been hard coded*/
		/* and are as follows 1) single 2) annual 3) half yearly*/
		/* 4) quarterly 5) monthly.*/
		/* The penalty can be one of either A) Flat fee or B) % of surr.*/
		/* which is checked against a minimum and maximum value also*/
		/* held on the table.*/
		wsaaItemitem.set(ZERO);
		wsaaItmfrm.set(ZERO);
		wsaaItemitem.set(srcalcpy.crtable);
		wsaaItmfrm.set(srcalcpy.crrcd);
		getSurrenderMeth230();
		/* IF SURC-CURRCODE NOT = SPACES                                */
		/*    MOVE SURC-CURRCODE       TO WSAA-T5542-CURR               */
		/* ELSE                                                         */
		/*    MOVE SURC-CHDR-CURR      TO WSAA-T5542-CURR               */
		/* END-IF.                                                      */
		wsaaT5542Curr.set(srcalcpy.chdrCurr);
		readTableT5542280();
		if (isEQ(srcalcpy.psNotAllwd, "T")) {
			srcalcpy.status.set("ENDP");
			goTo(GotoLabel.exit590);
		}
		wsaaEstimateTotAmt1.set(wsaaEstimateTotAmt);
		wsaaInd.set(ZERO);
		wsaaGotFreq.set("N");
		/* PERFORM 560-GET-FEE UNTIL WSAA-IND = 5                       */
		/*                     OR GOT-FREQ.                             */
		getT5542Freq700();
		if (!gotFreq.isTrue()) {
			wsaaGotFreq.set("Y");
			srcalcpy.actualVal.set(ZERO);
			goTo(GotoLabel.setType570);
		}
		if ((isNE(t5542rec.wdlFreq[wsaaInd.toInt()], ZERO))
		&& (isNE(srcalcpy.effdate, ZERO))
		&& (isNE(wsaaSurcCcdate, ZERO))) {
			checkDatesFrmRcd600();
		}
		if (gotFreq.isTrue()) {
			if (isNE(t5542rec.wdlAmount[wsaaInd.toInt()], ZERO)) {
				if (isGT(t5542rec.wdlAmount[wsaaInd.toInt()], wsaaEstimateTotAmt)) {
					srcalcpy.neUnits.set("Y");
				}
			}
		}
		if (gotFreq.isTrue()) {
			if (isNE(t5542rec.wdrem[wsaaInd.toInt()], ZERO)) {
				if (isLT(t5542rec.wdrem[wsaaInd.toInt()], wsaaEstimateTotAmt1)) {
					srcalcpy.tmUnits.set("Y");
				}
			}
		}
	}

	/**
	* <pre>
	**** MOVE T5542-FEEPC(WSAA-IND) TO SURC-ACTUAL-VAL.               
	**** MOVE ZERO                  TO SURC-ACTUAL-VAL.       <LA4505>
	* </pre>
	*/
protected void setType570()
	{
		srcalcpy.status.set("ENDP");
		wsaaSwitch.set("Y");
		srcalcpy.type.set("C");
		srcalcpy.description.set(SPACES);
		/*    MOVE 'FEE'   TO SURC-DESCRIPTION.                            */
		/* Access Table T6644 to retrieve description.                    */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t6644);
		/* MOVE 'E'                    TO DESC-LANGUAGE.        <LA4976>*/
		descIO.setLanguage(srcalcpy.language);
		descIO.setDescitem("FEE");
		descIO.setFunction("READR");
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), "****")) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		srcalcpy.description.set(descIO.getShortdesc());
		srcalcpy.currcode.set(SPACES);
		srcalcpy.estimatedVal.set(ZERO);
		/* MOVE SPACES  TO UTRSSUR-STATUZ SURC-ENDF SURC-FUND           */
		srcalcpy.endf.set(SPACES);
		srcalcpy.fund.set(SPACES);
		utrsclmIO.setStatuz(SPACES);
		covrpenIO.setStatuz(SPACES);
		vprnudlIO.setStatuz(SPACES);
	}

protected void getFee560()
	{
	}

protected void checkDatesFrmRcd600()
	{
		strt600();
	}

protected void strt600()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		/* MOVE     'M'              TO DTC3-FREQUENCY.                 */
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(wsaaSurcCcdate);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, "****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		if (isLT(datcon3rec.freqFactor, t5542rec.wdlFreq[wsaaInd.toInt()])) {
			srcalcpy.psNotAllwd.set("Y");
		}
	}

protected void getT5542Freq700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start710();
				case next750: 
					next750();
				case exit790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start710()
	{
		/* Check frequency with those on T5542.                            */
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			if (isEQ(srcalcpy.billfreq, t5542rec.billfreq[wsaaSub.toInt()])) {
				wsaaGotFreq.set("Y");
				wsaaInd.set(wsaaSub);
				wsaaSub.set(10);
				goTo(GotoLabel.next750);
			}
		}
		/*                                                         <LA4555>*/
		/* IF WSAA-SUB   > 9                                    <LA4555>*/
		if (isGT(wsaaSub, 6)) {
			syserrrec.statuz.set(h141);
			goTo(GotoLabel.exit790);
		}
	}

protected void next750()
	{
		if (gotFreq.isTrue()) {
			if (isNE(t5542rec.ffamt[wsaaInd.toInt()], ZERO)) {
				//srcalcpy.actualVal.set(t5542rec.ffamt[wsaaInd.toInt()]);	/*ILIFE-6709*/
				srcalcpy.ffamt.set(t5542rec.ffamt[wsaaInd.toInt()]);
				return ;
			}
		}
		if (gotFreq.isTrue()) {
			if (isEQ(t5542rec.feepc[wsaaInd.toInt()], 0)) {
				//srcalcpy.actualVal.set(ZERO);	/*ILIFE-6709*/
				srcalcpy.ffamt.set(ZERO);
				return ;
			}
			else {
				//srcalcpy.actualVal.set(t5542rec.feepc[wsaaInd.toInt()]);	/*ILIFE-6709*/
				srcalcpy.feepc.set(t5542rec.feepc[wsaaInd.toInt()]);
			}
		}
		if (gotFreq.isTrue()) {
			compute(wsaaEstimateTotAmt, 5).set(div((mult(wsaaEstimateTotAmt, t5542rec.feepc[wsaaInd.toInt()])), 100));
			if (isLT(wsaaEstimateTotAmt, t5542rec.feemin[wsaaInd.toInt()])) {
				//srcalcpy.actualVal.set(t5542rec.feemin[wsaaInd.toInt()]);	/*ILIFE-6709*/
				srcalcpy.feemin.set(t5542rec.feemin[wsaaInd.toInt()]);
			}
			else {
				if ((isGT(wsaaEstimateTotAmt, t5542rec.feemax[wsaaInd.toInt()]))
				&& (isNE(t5542rec.feemax[wsaaInd.toInt()], ZERO))) {
					//srcalcpy.actualVal.set(t5542rec.feemax[wsaaInd.toInt()]);	/*ILIFE-6709*/
					srcalcpy.feemax.set(t5542rec.feemax[wsaaInd.toInt()]);
				}
				else {
					//srcalcpy.actualVal.set(wsaaEstimateTotAmt);	/*ILIFE-6709*/
					srcalcpy.feemax.set(t5542rec.feemax[wsaaInd.toInt()]);
				}
			}
		}
	}

protected void calcSurrCharge800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para800();
					readT5551810();
					readT6598820();
					prepareToCalc530();
				case setType870: 
					setType870();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para800()
	{
		srcalcpy.actualVal.set(ZERO);
		wsaaT5551Key.set(srcalcpy.language);
		if (isNE(srcalcpy.type, "P")
		|| isEQ(wsaaEstimateTotAmt, ZERO)) {
			goTo(GotoLabel.setType870);
		}
		if (summaryPart.isTrue()) {
			if (policy1.isTrue()) {
				compute(srcalcpy.singp, 2).set((sub(srcalcpy.singp, (div(mult(srcalcpy.singp, (sub(srcalcpy.polsum, 1))), srcalcpy.polsum)))));
			}
			else {
				compute(srcalcpy.singp, 3).setRounded(div(srcalcpy.singp, srcalcpy.polsum));
			}
		}
	}

protected void readT5551810()
	{
		/* Reading T5551 to obtain surrender penalty/charge method.        */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5551);
		wsaaT5551Crtable.set(srcalcpy.crtable);
		/* MOVE SURC-CURRCODE          TO WSAA-T5551-CURRCODE.  <LA4976>*/
		wsaaT5551Currcode.set(srcalcpy.chdrCurr);
		itdmIO.setItemitem(wsaaT5551Item);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemitem(), wsaaT5551Item)
		&& isEQ(itdmIO.getStatuz(), varcom.oK)) {
			wsaaT5551Key.set("*");
			readT5551810();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(), srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5551)
		|| isNE(itdmIO.getItemitem(), wsaaT5551Item)
		|| isNE(itdmIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.setType870);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5551rec.svcmeth, SPACES)) {
			goTo(GotoLabel.setType870);
		}
	}

protected void readT6598820()
	{
		/* Reading T6598 using surrender penalty/charge method from T5551  */
		/* to obtain surrender penalty/charge calc routine.                */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5551rec.svcmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
		if (isEQ(t6598rec.calcprog, SPACES)) {
			goTo(GotoLabel.setType870);
		}
	}

protected void prepareToCalc530()
	{
		/* Here, we make use of the output fields as input for surrender   */
		/* penalty/charge calc routine. These fields will then move to     */
		/* WS fields in individual calc routine for further processing.    */
		srcalcpy.type.set(wsaaPlanSwitch);
		srcalcpy.estimatedVal.set(wsaaEstimateTotAmt);
		callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		if (isNE(srcalcpy.status, varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		
		// ILIFE-8117 START
		if (isLT(wsaaEstimateTotAmt,srcalcpy.actualVal))  
		{
			wsaaNewactualVal.set(wsaaEstimateTotAmt);  
			compute(wsaaPercentage, 6).setRounded(mult(div(wsaaNewactualVal, wsaaEstimateTotAmt), 100));
			srcalcpy.actualVal.set(wsaaPercentage);
		} 
		else
		{		
		     compute(wsaaPercentage, 6).setRounded(mult(div(srcalcpy.actualVal, wsaaEstimateTotAmt), 100));
		     srcalcpy.actualVal.set(wsaaPercentage);
		}
		// ILIFE-8117 END
	}

protected void setType870()
	{
		/* Access table T6644 for description                              */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t6644);
		/* MOVE 'E'                    TO DESC-LANGUAGE.        <LA4976>*/
		descIO.setLanguage(srcalcpy.language);
		descIO.setDescitem("PEN");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setShortdesc("??????????");
		}
		srcalcpy.fund.set(SPACES);
		srcalcpy.endf.set("Y");
		srcalcpy.type.set("J");
		srcalcpy.description.set(descIO.getShortdesc());
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.currcode.set(srcalcpy.chdrCurr);
	}

protected void a100ReadInterestBearing()
	{
		a110Para();
	}

protected void a110Para()
	{
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(wsaaChdrcoy);
		hitsIO.setChdrnum(wsaaChdrnum);
		hitsIO.setLife(wsaaLife);
		hitsIO.setCoverage(wsaaCoverage);
		hitsIO.setRider(wsaaRider);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setZintbfnd(wsaaVirtualFund);
		hitsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(), srcalcpy.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(), srcalcpy.chdrChdrnum)
		|| isNE(hitsIO.getLife(), srcalcpy.lifeLife)
		|| isNE(hitsIO.getCoverage(), srcalcpy.covrCoverage)
		|| isNE(hitsIO.getRider(), srcalcpy.covrRider)
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			/*    MOVE 'Y'                 TO SURC-ENDF             <V5L012>*/
			srcalcpy.endf.set("J");
			hitsIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.fund.set(SPACES);
		wsaaVirtualFund.set(hitsIO.getZintbfnd());
		while ( !(endOfFund.isTrue()
		|| isNE(hitsIO.getZintbfnd(), wsaaVirtualFund))) {
			a120AccumInterestBearing();
		}
		
		if (isEQ(hitsIO.getStatuz(), varcom.endp)
		|| isEQ(hitsIO.getStatuz(), varcom.oK)) {
			

			//ILIFE-9394
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(UNLPRTCVPM) && 
					er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))) {
				a200CheckEof();
			}
			else {
				calcInterestBearing();
			}
			
			wsaaVirtualFund.set(hitsIO.getZintbfnd());
			wsaaLife.set(hitsIO.getLife());
			wsaaRider.set(hitsIO.getRider());
			wsaaCoverage.set(hitsIO.getCoverage());
			wsaaChdrnum.set(hitsIO.getChdrnum());
			wsaaSwitch.set("N");
		}
	}

protected void calcInterestBearing() {
	srcalcpy.estimatedVal.set(ZERO);
	Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
	vpxsurcrec.curduntbal.set(wsaaAccumAmount);
	vpxsurcrec.chargeCalc.set(srcalcpy.surrCalcMeth);
	srcalcpy.fund.set(wsaaVirtualFund);
	srcalcpy.type.set("D");
	
	callProgram(UNLPRTCVPM, srcalcpy.surrenderRec, vpxsurcrec);//VPMS call
	if (isNE(srcalcpy.status,varcom.oK)
	&& isNE(srcalcpy.status,varcom.endp)) {
		fatalError9000();
	}
	wsaaEstimateTotAmt.add(srcalcpy.estimatedVal);
}

protected void a120AccumInterestBearing()
	{
		/*A121-PARA*/
		wsaaAccumAmount.add(hitsIO.getZcurprmbal());
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(), srcalcpy.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(), srcalcpy.chdrChdrnum)
		|| isNE(hitsIO.getLife(), srcalcpy.lifeLife)
		|| isNE(hitsIO.getCoverage(), srcalcpy.covrCoverage)
		|| isNE(hitsIO.getRider(), srcalcpy.covrRider)
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			/* A new coverage (or end of file) has been read, therefore,       */
			/* set ENDF processing indicator to "Y". This will be picked       */
			/* up immediately next time through and the PENALTY value          */
			/* will be then calculated. Avoids a needless call to the          */
			/* program between these two 'modes' of processing and hence       */
			/* eliminates the situation whereby, control is passed back        */
			/* to the calling program but there is no record to write to       */
			/* the subfile.                                                    */
			/*    MOVE 'Y'                 TO SURC-ENDF             <V5L012>*/
			srcalcpy.endf.set("J");
			wsaaNoMore.set("Y");
		}
		/*A129-EXIT*/
	}

protected void a200CheckEof()
	{
		/*A210-CHECK*/
		/* MOVE WSAA-ACCUM-AMOUNT      TO SURC-ESTIMATED-VAL.   <V76F06>*/
		compute(srcalcpy.estimatedVal, 6).setRounded(mult(wsaaAccumAmount, 1));
		wsaaEstimateTotAmt.add(srcalcpy.estimatedVal);
		srcalcpy.fund.set(wsaaVirtualFund);
		srcalcpy.type.set("D");
		readTable300();
		/*A290-EXIT*/
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		srcalcpy.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
