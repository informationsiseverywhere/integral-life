package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Covupf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZutrpfDAOImpl extends BaseDAOImpl<Zutrpf> implements ZutrpfDAO {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(ZutrpfDAOImpl.class);

	@Override
	public void insertZutrpfRecord(Zutrpf zutrpf) {		
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ZUTRPF(CHDRPFX,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,TRANNO,RSUNIN,RUNDTE,BATCTRCDE,TRDT,TRTM,USER_T,EFFDATE,VALIDFLAG,DATESUB,CRTUSER,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		try {
				int i = 1;
				ps.setString(i++, zutrpf.getChdrpfx());
				ps.setString(i++, zutrpf.getChdrcoy());
				ps.setString(i++, zutrpf.getChdrnum());
				ps.setString(i++, zutrpf.getLife());
				ps.setString(i++, zutrpf.getCoverage());
				ps.setString(i++, zutrpf.getRider());
				ps.setInt(i++, zutrpf.getPlanSuffix());
				ps.setInt(i++, zutrpf.getTranno());
				ps.setString(i++, zutrpf.getReserveUnitsInd());
				ps.setInt(i++, zutrpf.getReserveUnitsDate());
				ps.setString(i++, zutrpf.getBatctrcde());
				ps.setInt(i++, zutrpf.getTransactionDate());
				ps.setInt(i++, zutrpf.getTransactionTime());
				ps.setInt(i++, zutrpf.getUser());
				ps.setInt(i++, zutrpf.getEffdate());
				ps.setString(i++, zutrpf.getValidflag());
				ps.setInt(i++, zutrpf.getDatesub());
				ps.setString(i++, zutrpf.getCrtuser());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.executeUpdate();
				ps.getConnection().commit();
		} catch (SQLException e) {
			LOGGER.error("insertZutrpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	@Override
	public List<Zutrpf> readReservePricing(Zutrpf zutrpf) {
		StringBuilder sql = new StringBuilder("SELECT * FROM ZUTRPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? ORDER BY DATIME DESC");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Zutrpf> zutrpfList = new ArrayList<Zutrpf>(); 
		try{
			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(1, zutrpf.getChdrcoy());
			ps.setString(2, zutrpf.getChdrnum());
			ps.setInt(3, zutrpf.getTranno());
			rs = ps.executeQuery();
			while(rs.next()){
				Zutrpf zutrpfdata = new Zutrpf();
				zutrpfdata.setUniqueNumber(rs.getInt(1));
				zutrpfdata.setChdrpfx(rs.getString(2));
				zutrpfdata.setChdrcoy(rs.getString(3));
				zutrpfdata.setChdrnum(rs.getString(4));
				zutrpfdata.setLife(rs.getString(5));
				zutrpfdata.setCoverage(rs.getString(6));
				zutrpfdata.setRider(rs.getString(7));
				zutrpfdata.setPlanSuffix(rs.getInt(8));
				zutrpfdata.setTranno(rs.getInt(9));
				zutrpfdata.setReserveUnitsInd(rs.getString(10));
				zutrpfdata.setReserveUnitsDate(rs.getInt(11));
				zutrpfdata.setBatctrcde(rs.getString(12));
				zutrpfdata.setTransactionDate(rs.getInt(13));
				zutrpfdata.setTransactionTime(rs.getInt(14));
				zutrpfdata.setUser(rs.getInt(15));
				zutrpfdata.setEffdate(rs.getInt(16));
				zutrpfdata.setValidflag(rs.getString(17)); 
				zutrpfdata.setDatesub(rs.getInt(18));
				zutrpfdata.setCrtuser(rs.getString(19));
				zutrpfdata.setUserProfile(rs.getString(20));
				zutrpfdata.setJobName(rs.getString(21));
				zutrpfdata.setDatime(rs.getString(22));
				zutrpfList.add(zutrpfdata);
			}
			
		} catch (SQLException e) {
			LOGGER.error("readReservePricing()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,rs);			
		}	
		return zutrpfList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}