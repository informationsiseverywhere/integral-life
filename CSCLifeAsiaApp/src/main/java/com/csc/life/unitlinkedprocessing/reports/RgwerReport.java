package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RGWER.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class RgwerReport extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData currcd = new FixedLengthStringData(3);
	private ZonedDecimalData ffamt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private FixedLengthStringData npaydate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private FixedLengthStringData pnames = new FixedLengthStringData(20);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData seqnumb = new ZonedDecimalData(3, 0);
	private ZonedDecimalData standamt = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public RgwerReport() {
		super();
	}


	/**
	 * Print the XML for Rgwerd01
	 */
	public void printRgwerd01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		seqnumb.setFieldName("seqnumb");
		seqnumb.setInternal(subString(recordData, 9, 3));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 12, 4));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 16, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 18, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 20, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 22, 4));
		standamt.setFieldName("standamt");
		standamt.setInternal(subString(recordData, 26, 17));
		ffamt.setFieldName("ffamt");
		ffamt.setInternal(subString(recordData, 43, 17));
		currcd.setFieldName("currcd");
		currcd.setInternal(subString(recordData, 60, 3));
		npaydate.setFieldName("npaydate");
		npaydate.setInternal(subString(recordData, 63, 10));
		pnames.setFieldName("pnames");
		pnames.setInternal(subString(recordData, 73, 20));
		printLayout("Rgwerd01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				seqnumb,
				crtable,
				life,
				coverage,
				rider,
				plnsfx,
				standamt,
				ffamt,
				currcd,
				npaydate,
				pnames
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for Rgwerh01
	 */
	public void printRgwerh01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 42, 10));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rgwerh01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				company,
				companynm,
				sdate,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(11);
	}


}
