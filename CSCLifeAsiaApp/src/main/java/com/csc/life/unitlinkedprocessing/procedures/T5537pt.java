/*
 * File: T5537pt.java
 * Date: 30 August 2009 2:21:51
 * Author: Quipoz Limited
 * 
 * Class transformed from T5537PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5537.
*
*
*
****************************************************************** ****
* </pre>
*/
public class T5537pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5537rec t5537rec = new T5537rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5537pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5537rec.t5537Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(t5537rec.toterm01);
		generalCopyLinesInner.fieldNo006.set(t5537rec.toterm02);
		generalCopyLinesInner.fieldNo007.set(t5537rec.toterm03);
		generalCopyLinesInner.fieldNo008.set(t5537rec.toterm04);
		generalCopyLinesInner.fieldNo009.set(t5537rec.toterm05);
		generalCopyLinesInner.fieldNo010.set(t5537rec.toterm06);
		generalCopyLinesInner.fieldNo011.set(t5537rec.toterm07);
		generalCopyLinesInner.fieldNo012.set(t5537rec.toterm08);
		generalCopyLinesInner.fieldNo013.set(t5537rec.toterm09);
		generalCopyLinesInner.fieldNo014.set(t5537rec.ageIssageTo01);
		generalCopyLinesInner.fieldNo015.set(t5537rec.actAllocBasis01);
		generalCopyLinesInner.fieldNo016.set(t5537rec.actAllocBasis02);
		generalCopyLinesInner.fieldNo017.set(t5537rec.actAllocBasis03);
		generalCopyLinesInner.fieldNo018.set(t5537rec.actAllocBasis04);
		generalCopyLinesInner.fieldNo019.set(t5537rec.actAllocBasis05);
		generalCopyLinesInner.fieldNo020.set(t5537rec.actAllocBasis06);
		generalCopyLinesInner.fieldNo021.set(t5537rec.actAllocBasis07);
		generalCopyLinesInner.fieldNo022.set(t5537rec.actAllocBasis08);
		generalCopyLinesInner.fieldNo023.set(t5537rec.actAllocBasis09);
		generalCopyLinesInner.fieldNo024.set(t5537rec.ageIssageTo02);
		generalCopyLinesInner.fieldNo025.set(t5537rec.actAllocBasis10);
		generalCopyLinesInner.fieldNo026.set(t5537rec.actAllocBasis11);
		generalCopyLinesInner.fieldNo027.set(t5537rec.actAllocBasis12);
		generalCopyLinesInner.fieldNo028.set(t5537rec.actAllocBasis13);
		generalCopyLinesInner.fieldNo029.set(t5537rec.actAllocBasis14);
		generalCopyLinesInner.fieldNo030.set(t5537rec.actAllocBasis15);
		generalCopyLinesInner.fieldNo031.set(t5537rec.actAllocBasis16);
		generalCopyLinesInner.fieldNo032.set(t5537rec.actAllocBasis17);
		generalCopyLinesInner.fieldNo033.set(t5537rec.actAllocBasis18);
		generalCopyLinesInner.fieldNo034.set(t5537rec.ageIssageTo03);
		generalCopyLinesInner.fieldNo035.set(t5537rec.actAllocBasis19);
		generalCopyLinesInner.fieldNo036.set(t5537rec.actAllocBasis20);
		generalCopyLinesInner.fieldNo037.set(t5537rec.actAllocBasis21);
		generalCopyLinesInner.fieldNo038.set(t5537rec.actAllocBasis22);
		generalCopyLinesInner.fieldNo039.set(t5537rec.actAllocBasis23);
		generalCopyLinesInner.fieldNo040.set(t5537rec.actAllocBasis24);
		generalCopyLinesInner.fieldNo041.set(t5537rec.actAllocBasis25);
		generalCopyLinesInner.fieldNo042.set(t5537rec.actAllocBasis26);
		generalCopyLinesInner.fieldNo043.set(t5537rec.actAllocBasis27);
		generalCopyLinesInner.fieldNo044.set(t5537rec.ageIssageTo04);
		generalCopyLinesInner.fieldNo045.set(t5537rec.actAllocBasis28);
		generalCopyLinesInner.fieldNo046.set(t5537rec.actAllocBasis29);
		generalCopyLinesInner.fieldNo047.set(t5537rec.actAllocBasis30);
		generalCopyLinesInner.fieldNo048.set(t5537rec.actAllocBasis31);
		generalCopyLinesInner.fieldNo049.set(t5537rec.actAllocBasis32);
		generalCopyLinesInner.fieldNo050.set(t5537rec.actAllocBasis33);
		generalCopyLinesInner.fieldNo051.set(t5537rec.actAllocBasis34);
		generalCopyLinesInner.fieldNo052.set(t5537rec.actAllocBasis35);
		generalCopyLinesInner.fieldNo053.set(t5537rec.actAllocBasis36);
		generalCopyLinesInner.fieldNo054.set(t5537rec.ageIssageTo05);
		generalCopyLinesInner.fieldNo055.set(t5537rec.actAllocBasis37);
		generalCopyLinesInner.fieldNo056.set(t5537rec.actAllocBasis38);
		generalCopyLinesInner.fieldNo057.set(t5537rec.actAllocBasis39);
		generalCopyLinesInner.fieldNo058.set(t5537rec.actAllocBasis40);
		generalCopyLinesInner.fieldNo059.set(t5537rec.actAllocBasis41);
		generalCopyLinesInner.fieldNo060.set(t5537rec.actAllocBasis42);
		generalCopyLinesInner.fieldNo061.set(t5537rec.actAllocBasis43);
		generalCopyLinesInner.fieldNo062.set(t5537rec.actAllocBasis44);
		generalCopyLinesInner.fieldNo063.set(t5537rec.actAllocBasis45);
		generalCopyLinesInner.fieldNo064.set(t5537rec.ageIssageTo06);
		generalCopyLinesInner.fieldNo065.set(t5537rec.actAllocBasis46);
		generalCopyLinesInner.fieldNo066.set(t5537rec.actAllocBasis47);
		generalCopyLinesInner.fieldNo067.set(t5537rec.actAllocBasis48);
		generalCopyLinesInner.fieldNo068.set(t5537rec.actAllocBasis49);
		generalCopyLinesInner.fieldNo069.set(t5537rec.actAllocBasis50);
		generalCopyLinesInner.fieldNo070.set(t5537rec.actAllocBasis51);
		generalCopyLinesInner.fieldNo071.set(t5537rec.actAllocBasis52);
		generalCopyLinesInner.fieldNo072.set(t5537rec.actAllocBasis53);
		generalCopyLinesInner.fieldNo073.set(t5537rec.actAllocBasis54);
		generalCopyLinesInner.fieldNo074.set(t5537rec.ageIssageTo07);
		generalCopyLinesInner.fieldNo075.set(t5537rec.actAllocBasis55);
		generalCopyLinesInner.fieldNo076.set(t5537rec.actAllocBasis56);
		generalCopyLinesInner.fieldNo077.set(t5537rec.actAllocBasis57);
		generalCopyLinesInner.fieldNo078.set(t5537rec.actAllocBasis58);
		generalCopyLinesInner.fieldNo079.set(t5537rec.actAllocBasis59);
		generalCopyLinesInner.fieldNo080.set(t5537rec.actAllocBasis60);
		generalCopyLinesInner.fieldNo081.set(t5537rec.actAllocBasis61);
		generalCopyLinesInner.fieldNo082.set(t5537rec.actAllocBasis62);
		generalCopyLinesInner.fieldNo083.set(t5537rec.actAllocBasis63);
		generalCopyLinesInner.fieldNo084.set(t5537rec.ageIssageTo08);
		generalCopyLinesInner.fieldNo085.set(t5537rec.actAllocBasis64);
		generalCopyLinesInner.fieldNo086.set(t5537rec.actAllocBasis65);
		generalCopyLinesInner.fieldNo087.set(t5537rec.actAllocBasis66);
		generalCopyLinesInner.fieldNo088.set(t5537rec.actAllocBasis67);
		generalCopyLinesInner.fieldNo089.set(t5537rec.actAllocBasis68);
		generalCopyLinesInner.fieldNo090.set(t5537rec.actAllocBasis69);
		generalCopyLinesInner.fieldNo091.set(t5537rec.actAllocBasis70);
		generalCopyLinesInner.fieldNo092.set(t5537rec.actAllocBasis71);
		generalCopyLinesInner.fieldNo093.set(t5537rec.actAllocBasis72);
		generalCopyLinesInner.fieldNo094.set(t5537rec.ageIssageTo09);
		generalCopyLinesInner.fieldNo095.set(t5537rec.actAllocBasis73);
		generalCopyLinesInner.fieldNo096.set(t5537rec.actAllocBasis74);
		generalCopyLinesInner.fieldNo097.set(t5537rec.actAllocBasis75);
		generalCopyLinesInner.fieldNo098.set(t5537rec.actAllocBasis76);
		generalCopyLinesInner.fieldNo099.set(t5537rec.actAllocBasis77);
		generalCopyLinesInner.fieldNo100.set(t5537rec.actAllocBasis78);
		generalCopyLinesInner.fieldNo101.set(t5537rec.actAllocBasis79);
		generalCopyLinesInner.fieldNo102.set(t5537rec.actAllocBasis80);
		generalCopyLinesInner.fieldNo103.set(t5537rec.actAllocBasis81);
		generalCopyLinesInner.fieldNo104.set(t5537rec.ageIssageTo10);
		generalCopyLinesInner.fieldNo105.set(t5537rec.actAllocBasis82);
		generalCopyLinesInner.fieldNo106.set(t5537rec.actAllocBasis83);
		generalCopyLinesInner.fieldNo107.set(t5537rec.actAllocBasis84);
		generalCopyLinesInner.fieldNo108.set(t5537rec.actAllocBasis85);
		generalCopyLinesInner.fieldNo109.set(t5537rec.actAllocBasis86);
		generalCopyLinesInner.fieldNo110.set(t5537rec.actAllocBasis87);
		generalCopyLinesInner.fieldNo111.set(t5537rec.actAllocBasis88);
		generalCopyLinesInner.fieldNo112.set(t5537rec.actAllocBasis89);
		generalCopyLinesInner.fieldNo113.set(t5537rec.actAllocBasis90);
		generalCopyLinesInner.fieldNo114.set(t5537rec.agecont);
		generalCopyLinesInner.fieldNo115.set(t5537rec.trmcont);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(56).isAPartOf(wsaaPrtLine001, 20, FILLER).init("Premium Allocation Basis - Age and Term            S5537");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(70);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 10, FILLER).init(" Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 18);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 23, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 31);
	private FixedLengthStringData filler6 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 40);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(37);
	private FixedLengthStringData filler7 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine003, 0, FILLER).init("Up to                      Up to Term");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(3);
	private FixedLengthStringData filler8 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 0, FILLER).init("Age");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(69);
	private FixedLengthStringData filler9 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 10).setPattern("ZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 17).setPattern("ZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 24).setPattern("ZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 31).setPattern("ZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 38).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 45).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 52).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 59).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 66).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(69);
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 1).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 9);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 16);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 23);
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 30);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 37);
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 44);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 51);
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 58);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 65);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(69);
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 1).setPattern("ZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 9);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 16);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 23);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 30);
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 37);
	private FixedLengthStringData filler34 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 44);
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 58);
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 65);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(69);
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 1).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 9);
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 16);
	private FixedLengthStringData filler41 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 23);
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 30);
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 37);
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 44);
	private FixedLengthStringData filler45 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 51);
	private FixedLengthStringData filler46 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 58);
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 65);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(69);
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 1).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 9);
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 16);
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 23);
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 30);
	private FixedLengthStringData filler53 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 37);
	private FixedLengthStringData filler54 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 44);
	private FixedLengthStringData filler55 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 51);
	private FixedLengthStringData filler56 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 58);
	private FixedLengthStringData filler57 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 65);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(69);
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 1).setPattern("ZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 9);
	private FixedLengthStringData filler60 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 16);
	private FixedLengthStringData filler61 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 23);
	private FixedLengthStringData filler62 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 30);
	private FixedLengthStringData filler63 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 37);
	private FixedLengthStringData filler64 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 44);
	private FixedLengthStringData filler65 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler66 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 58);
	private FixedLengthStringData filler67 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 65);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(69);
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 1).setPattern("ZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 9);
	private FixedLengthStringData filler70 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 16);
	private FixedLengthStringData filler71 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 23);
	private FixedLengthStringData filler72 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 30);
	private FixedLengthStringData filler73 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 37);
	private FixedLengthStringData filler74 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 44);
	private FixedLengthStringData filler75 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 51);
	private FixedLengthStringData filler76 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 58);
	private FixedLengthStringData filler77 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 65);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(69);
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 1).setPattern("ZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 9);
	private FixedLengthStringData filler80 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 16);
	private FixedLengthStringData filler81 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 23);
	private FixedLengthStringData filler82 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 30);
	private FixedLengthStringData filler83 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 37);
	private FixedLengthStringData filler84 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 44);
	private FixedLengthStringData filler85 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 51);
	private FixedLengthStringData filler86 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 58);
	private FixedLengthStringData filler87 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 65);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(69);
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 1).setPattern("ZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 9);
	private FixedLengthStringData filler90 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 16);
	private FixedLengthStringData filler91 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 23);
	private FixedLengthStringData filler92 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 30);
	private FixedLengthStringData filler93 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 37);
	private FixedLengthStringData filler94 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 44);
	private FixedLengthStringData filler95 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 51);
	private FixedLengthStringData filler96 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 58);
	private FixedLengthStringData filler97 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo093 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 65);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(69);
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 1).setPattern("ZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 9);
	private FixedLengthStringData filler100 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 16);
	private FixedLengthStringData filler101 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 23);
	private FixedLengthStringData filler102 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 30);
	private FixedLengthStringData filler103 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 37);
	private FixedLengthStringData filler104 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 44);
	private FixedLengthStringData filler105 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo101 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 51);
	private FixedLengthStringData filler106 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo102 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 58);
	private FixedLengthStringData filler107 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo103 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 65);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(69);
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 1).setPattern("ZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo105 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 9);
	private FixedLengthStringData filler110 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo106 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 16);
	private FixedLengthStringData filler111 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo107 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 23);
	private FixedLengthStringData filler112 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo108 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 30);
	private FixedLengthStringData filler113 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo109 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 37);
	private FixedLengthStringData filler114 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo110 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 44);
	private FixedLengthStringData filler115 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo111 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 51);
	private FixedLengthStringData filler116 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo112 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 58);
	private FixedLengthStringData filler117 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo113 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 65);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(65);
	private FixedLengthStringData filler118 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Age Continuation:");
	private FixedLengthStringData fieldNo114 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 19);
	private FixedLengthStringData filler119 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 27, FILLER).init(SPACES);
	private FixedLengthStringData filler120 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine016, 38, FILLER).init("Term Continuation:");
	private FixedLengthStringData fieldNo115 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 57);
}
}
