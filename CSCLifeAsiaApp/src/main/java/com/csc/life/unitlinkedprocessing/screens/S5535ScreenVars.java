package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5535
 * @version 1.0 generated on 30/08/09 06:41
 * @author Quipoz
 */
public class S5535ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(918);
	public FixedLengthStringData dataFields = new FixedLengthStringData(210).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData percOrAmtInds = new FixedLengthStringData(15).isAPartOf(dataFields, 55);
	public FixedLengthStringData[] percOrAmtInd = FLSArrayPartOfStructure(15, 1, percOrAmtInds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(15).isAPartOf(percOrAmtInds, 0, FILLER_REDEFINE);
	public FixedLengthStringData percOrAmtInd01 = DD.poaind.copy().isAPartOf(filler,0);
	public FixedLengthStringData percOrAmtInd02 = DD.poaind.copy().isAPartOf(filler,1);
	public FixedLengthStringData percOrAmtInd03 = DD.poaind.copy().isAPartOf(filler,2);
	public FixedLengthStringData percOrAmtInd04 = DD.poaind.copy().isAPartOf(filler,3);
	public FixedLengthStringData percOrAmtInd05 = DD.poaind.copy().isAPartOf(filler,4);
	public FixedLengthStringData percOrAmtInd06 = DD.poaind.copy().isAPartOf(filler,5);
	public FixedLengthStringData percOrAmtInd07 = DD.poaind.copy().isAPartOf(filler,6);
	public FixedLengthStringData percOrAmtInd08 = DD.poaind.copy().isAPartOf(filler,7);
	public FixedLengthStringData percOrAmtInd09 = DD.poaind.copy().isAPartOf(filler,8);
	public FixedLengthStringData percOrAmtInd10 = DD.poaind.copy().isAPartOf(filler,9);
	public FixedLengthStringData percOrAmtInd11 = DD.poaind.copy().isAPartOf(filler,10);
	public FixedLengthStringData percOrAmtInd12 = DD.poaind.copy().isAPartOf(filler,11);
	public FixedLengthStringData percOrAmtInd13 = DD.poaind.copy().isAPartOf(filler,12);
	public FixedLengthStringData percOrAmtInd14 = DD.poaind.copy().isAPartOf(filler,13);
	public FixedLengthStringData percOrAmtInd15 = DD.poaind.copy().isAPartOf(filler,14);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData unitAfterDurs = new FixedLengthStringData(60).isAPartOf(dataFields, 75);
	public ZonedDecimalData[] unitAfterDur = ZDArrayPartOfStructure(15, 4, 0, unitAfterDurs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(unitAfterDurs, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitAfterDur01 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData unitAfterDur02 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,4);
	public ZonedDecimalData unitAfterDur03 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,8);
	public ZonedDecimalData unitAfterDur04 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData unitAfterDur05 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,16);
	public ZonedDecimalData unitAfterDur06 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,20);
	public ZonedDecimalData unitAfterDur07 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData unitAfterDur08 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,28);
	public ZonedDecimalData unitAfterDur09 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,32);
	public ZonedDecimalData unitAfterDur10 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,36);
	public ZonedDecimalData unitAfterDur11 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,40);
	public ZonedDecimalData unitAfterDur12 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,44);
	public ZonedDecimalData unitAfterDur13 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,48);
	public ZonedDecimalData unitAfterDur14 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,52);
	public ZonedDecimalData unitAfterDur15 = DD.uafdur.copyToZonedDecimal().isAPartOf(filler1,56);
	public FixedLengthStringData unitExtPcAllocs = new FixedLengthStringData(75).isAPartOf(dataFields, 135);
	public ZonedDecimalData[] unitExtPcAlloc = ZDArrayPartOfStructure(15, 5, 2, unitExtPcAllocs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(75).isAPartOf(unitExtPcAllocs, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitExtPcAlloc01 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData unitExtPcAlloc02 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,5);
	public ZonedDecimalData unitExtPcAlloc03 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,10);
	public ZonedDecimalData unitExtPcAlloc04 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,15);
	public ZonedDecimalData unitExtPcAlloc05 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,20);
	public ZonedDecimalData unitExtPcAlloc06 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,25);
	public ZonedDecimalData unitExtPcAlloc07 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,30);
	public ZonedDecimalData unitExtPcAlloc08 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,35);
	public ZonedDecimalData unitExtPcAlloc09 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,40);
	public ZonedDecimalData unitExtPcAlloc10 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,45);
	public ZonedDecimalData unitExtPcAlloc11 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,50);
	public ZonedDecimalData unitExtPcAlloc12 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,55);
	public ZonedDecimalData unitExtPcAlloc13 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,60);
	public ZonedDecimalData unitExtPcAlloc14 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,65);
	public ZonedDecimalData unitExtPcAlloc15 = DD.uextpc.copyToZonedDecimal().isAPartOf(filler2,70);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 210);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData poaindsErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] poaindErr = FLSArrayPartOfStructure(15, 4, poaindsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(60).isAPartOf(poaindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData poaind01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData poaind02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData poaind03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData poaind04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData poaind05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData poaind06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData poaind07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData poaind08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData poaind09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData poaind10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData poaind11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData poaind12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData poaind13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData poaind14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData poaind15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData uafdursErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData[] uafdurErr = FLSArrayPartOfStructure(15, 4, uafdursErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(uafdursErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData uafdur01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData uafdur02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData uafdur03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData uafdur04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData uafdur05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData uafdur06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData uafdur07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData uafdur08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData uafdur09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData uafdur10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData uafdur11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData uafdur12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData uafdur13Err = new FixedLengthStringData(4).isAPartOf(filler4, 48);
	public FixedLengthStringData uafdur14Err = new FixedLengthStringData(4).isAPartOf(filler4, 52);
	public FixedLengthStringData uafdur15Err = new FixedLengthStringData(4).isAPartOf(filler4, 56);
	public FixedLengthStringData uextpcsErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData[] uextpcErr = FLSArrayPartOfStructure(15, 4, uextpcsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(60).isAPartOf(uextpcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData uextpc01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData uextpc02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData uextpc03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData uextpc04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData uextpc05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData uextpc06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData uextpc07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData uextpc08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData uextpc09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData uextpc10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData uextpc11Err = new FixedLengthStringData(4).isAPartOf(filler5, 40);
	public FixedLengthStringData uextpc12Err = new FixedLengthStringData(4).isAPartOf(filler5, 44);
	public FixedLengthStringData uextpc13Err = new FixedLengthStringData(4).isAPartOf(filler5, 48);
	public FixedLengthStringData uextpc14Err = new FixedLengthStringData(4).isAPartOf(filler5, 52);
	public FixedLengthStringData uextpc15Err = new FixedLengthStringData(4).isAPartOf(filler5, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(504).isAPartOf(dataArea, 414);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData poaindsOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] poaindOut = FLSArrayPartOfStructure(15, 12, poaindsOut, 0);
	public FixedLengthStringData[][] poaindO = FLSDArrayPartOfArrayStructure(12, 1, poaindOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(180).isAPartOf(poaindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] poaind01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] poaind02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] poaind03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] poaind04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] poaind05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] poaind06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] poaind07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] poaind08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] poaind09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] poaind10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] poaind11Out = FLSArrayPartOfStructure(12, 1, filler6, 120);
	public FixedLengthStringData[] poaind12Out = FLSArrayPartOfStructure(12, 1, filler6, 132);
	public FixedLengthStringData[] poaind13Out = FLSArrayPartOfStructure(12, 1, filler6, 144);
	public FixedLengthStringData[] poaind14Out = FLSArrayPartOfStructure(12, 1, filler6, 156);
	public FixedLengthStringData[] poaind15Out = FLSArrayPartOfStructure(12, 1, filler6, 168);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData uafdursOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] uafdurOut = FLSArrayPartOfStructure(15, 12, uafdursOut, 0);
	public FixedLengthStringData[][] uafdurO = FLSDArrayPartOfArrayStructure(12, 1, uafdurOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(180).isAPartOf(uafdursOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] uafdur01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] uafdur02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] uafdur03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] uafdur04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] uafdur05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] uafdur06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] uafdur07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] uafdur08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] uafdur09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] uafdur10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] uafdur11Out = FLSArrayPartOfStructure(12, 1, filler7, 120);
	public FixedLengthStringData[] uafdur12Out = FLSArrayPartOfStructure(12, 1, filler7, 132);
	public FixedLengthStringData[] uafdur13Out = FLSArrayPartOfStructure(12, 1, filler7, 144);
	public FixedLengthStringData[] uafdur14Out = FLSArrayPartOfStructure(12, 1, filler7, 156);
	public FixedLengthStringData[] uafdur15Out = FLSArrayPartOfStructure(12, 1, filler7, 168);
	public FixedLengthStringData uextpcsOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] uextpcOut = FLSArrayPartOfStructure(15, 12, uextpcsOut, 0);
	public FixedLengthStringData[][] uextpcO = FLSDArrayPartOfArrayStructure(12, 1, uextpcOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(180).isAPartOf(uextpcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] uextpc01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] uextpc02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] uextpc03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] uextpc04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] uextpc05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] uextpc06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] uextpc07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] uextpc08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] uextpc09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] uextpc10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] uextpc11Out = FLSArrayPartOfStructure(12, 1, filler8, 120);
	public FixedLengthStringData[] uextpc12Out = FLSArrayPartOfStructure(12, 1, filler8, 132);
	public FixedLengthStringData[] uextpc13Out = FLSArrayPartOfStructure(12, 1, filler8, 144);
	public FixedLengthStringData[] uextpc14Out = FLSArrayPartOfStructure(12, 1, filler8, 156);
	public FixedLengthStringData[] uextpc15Out = FLSArrayPartOfStructure(12, 1, filler8, 168);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5535screenWritten = new LongData(0);
	public LongData S5535protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5535ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itmfrmOut,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itmtoOut,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind01Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc02Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind02Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc03Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind03Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind04Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind05Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc06Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind06Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc07Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind07Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc08Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind08Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc09Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind09Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc10Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind10Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc11Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind11Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc12Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind12Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur13Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc13Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind13Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur14Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc14Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind14Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uafdur15Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uextpc15Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poaind15Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, unitAfterDur01, unitExtPcAlloc01, percOrAmtInd01, unitAfterDur02, unitExtPcAlloc02, percOrAmtInd02, unitAfterDur03, unitExtPcAlloc03, percOrAmtInd03, unitAfterDur04, unitExtPcAlloc04, percOrAmtInd04, unitAfterDur05, unitExtPcAlloc05, percOrAmtInd05, unitAfterDur06, unitExtPcAlloc06, percOrAmtInd06, unitAfterDur07, unitExtPcAlloc07, percOrAmtInd07, unitAfterDur08, unitExtPcAlloc08, percOrAmtInd08, unitAfterDur09, unitExtPcAlloc09, percOrAmtInd09, unitAfterDur10, unitExtPcAlloc10, percOrAmtInd10,
				unitAfterDur11, unitExtPcAlloc11, percOrAmtInd11, unitAfterDur12, unitExtPcAlloc12, percOrAmtInd12, unitAfterDur13, unitExtPcAlloc13, percOrAmtInd13, 
				unitAfterDur14, unitExtPcAlloc14, percOrAmtInd14, unitAfterDur15, unitExtPcAlloc15, percOrAmtInd15 };
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, uafdur01Out, uextpc01Out, poaind01Out, uafdur02Out, uextpc02Out, poaind02Out, uafdur03Out, uextpc03Out, poaind03Out, uafdur04Out, uextpc04Out, poaind04Out, uafdur05Out, uextpc05Out, poaind05Out, uafdur06Out, uextpc06Out, poaind06Out, uafdur07Out, uextpc07Out, poaind07Out, uafdur08Out, uextpc08Out, poaind08Out, uafdur09Out, uextpc09Out, poaind09Out, uafdur10Out, uextpc10Out, poaind10Out,
				uafdur11Out, uextpc11Out, poaind11Out, uafdur12Out, uextpc12Out, poaind12Out, uafdur13Out, uextpc13Out, poaind13Out, uafdur14Out, uextpc14Out, poaind14Out,
				uafdur15Out, uextpc15Out, poaind15Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, uafdur01Err, uextpc01Err, poaind01Err, uafdur02Err, uextpc02Err, poaind02Err, uafdur03Err, uextpc03Err, poaind03Err, uafdur04Err, uextpc04Err, poaind04Err, uafdur05Err, uextpc05Err, poaind05Err, uafdur06Err, uextpc06Err, poaind06Err, uafdur07Err, uextpc07Err, poaind07Err, uafdur08Err, uextpc08Err, poaind08Err, uafdur09Err, uextpc09Err, poaind09Err, uafdur10Err, uextpc10Err, poaind10Err,
				uafdur11Err, uextpc11Err, poaind11Err, uafdur12Err, uextpc12Err, poaind12Err, uafdur13Err, uextpc13Err, poaind13Err, uafdur14Err, uextpc14Err, poaind14Err,
				uafdur15Err, uextpc15Err, poaind15Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5535screen.class;
		protectRecord = S5535protect.class;
	}

}
