package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:08
 * Description:
 * Copybook name: XUDLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Xudlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData xudlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData xudlUdelKey = new FixedLengthStringData(64).isAPartOf(xudlFileKey, 0, REDEFINE);
  	public FixedLengthStringData xudlUdelChdrcoy = new FixedLengthStringData(1).isAPartOf(xudlUdelKey, 0);
  	public FixedLengthStringData xudlUdelUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(xudlUdelKey, 1);
  	public FixedLengthStringData xudlUdelUnitType = new FixedLengthStringData(1).isAPartOf(xudlUdelKey, 5);
  	public FixedLengthStringData xudlUdelChdrnum = new FixedLengthStringData(8).isAPartOf(xudlUdelKey, 6);
  	public FixedLengthStringData xudlUdelLife = new FixedLengthStringData(2).isAPartOf(xudlUdelKey, 14);
  	public FixedLengthStringData xudlUdelCoverage = new FixedLengthStringData(2).isAPartOf(xudlUdelKey, 16);
  	public FixedLengthStringData xudlUdelRider = new FixedLengthStringData(2).isAPartOf(xudlUdelKey, 18);
  	public FixedLengthStringData xudlUdelBatctrcde = new FixedLengthStringData(4).isAPartOf(xudlUdelKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(xudlUdelKey, 24, FILLER);
  
  	public FixedLengthStringData xudlUfndKey = new FixedLengthStringData(64).isAPartOf(xudlFileKey, 0, REDEFINE);
  	public FixedLengthStringData xudlUfndCompany = new FixedLengthStringData(1).isAPartOf(xudlUfndKey, 0);
  	public FixedLengthStringData xudlUfndVirtualFund = new FixedLengthStringData(4).isAPartOf(xudlUfndKey, 1);
  	public FixedLengthStringData xudlUfndUnitType = new FixedLengthStringData(1).isAPartOf(xudlUfndKey, 5);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(xudlUfndKey, 6, FILLER);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(xudlUfndKey, 14, FILLER);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(xudlUfndKey, 16, FILLER);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(xudlUfndKey, 18, FILLER);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(4).isAPartOf(xudlUfndKey, 20, FILLER);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(xudlUfndKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(xudlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		xudlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}