package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UstfstmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:51:33
 * Class transformed from USTFSTM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UstfstmTableDAM extends UstfpfTableDAM {

	public UstfstmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("USTFSTM");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", PLNSFX"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", VRTFND"
		             + ", UNITYP"
		             + ", USTMNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "PLNSFX, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "VRTFND, " +
		            "UNITYP, " +
		            "USTMNO, " +
		            "USTMTFLAG, " +
		            "STOPDT, " +
		            "STOPUN, " +
		            "STOPDUN, " +
		            "STOPFUCA, " +
		            "STOPCOCA, " +
		            "STCLDT, " +
		            "STCLUN, " +
		            "STCLDUN, " +
		            "STCLFUCA, " +
		            "STCLCOCA, " +
		            "STTRUN, " +
		            "STTRDUN, " +
		            "STTRFUCA, " +
		            "STTRCOCA, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "PLNSFX ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "VRTFND ASC, " +
		            "UNITYP ASC, " +
		            "USTMNO DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "PLNSFX DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "VRTFND DESC, " +
		            "UNITYP DESC, " +
		            "USTMNO ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               planSuffix,
                               life,
                               coverage,
                               rider,
                               unitVirtualFund,
                               unitType,
                               ustmno,
                               unitStmtFlag,
                               stmtOpDate,
                               stmtOpUnits,
                               stmtOpDunits,
                               stmtOpFundCash,
                               stmtOpContCash,
                               stmtClDate,
                               stmtClUnits,
                               stmtClDunits,
                               stmtClFundCash,
                               stmtClContCash,
                               stmtTrnUnits,
                               stmtTrnDunits,
                               stmtTrFundCash,
                               stmtTrContCash,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(38);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getUstfrecKeyData() {
		return getRecKeyData();
	}
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getPlanSuffix().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ getUstmno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setUstfrecKeyData(Object obj) {
		return setRecKeyData(obj);
	}
	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, ustmno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller8 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller9 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(planSuffix.toInternal());
	nonKeyFiller4.setInternal(life.toInternal());
	nonKeyFiller5.setInternal(coverage.toInternal());
	nonKeyFiller6.setInternal(rider.toInternal());
	nonKeyFiller7.setInternal(unitVirtualFund.toInternal());
	nonKeyFiller8.setInternal(unitType.toInternal());
	nonKeyFiller9.setInternal(ustmno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getUstfrecNonKeyData() {
		return getRecNonKeyData();
	}
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(191);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ nonKeyFiller8.toInternal()
					+ nonKeyFiller9.toInternal()
					+ getUnitStmtFlag().toInternal()
					+ getStmtOpDate().toInternal()
					+ getStmtOpUnits().toInternal()
					+ getStmtOpDunits().toInternal()
					+ getStmtOpFundCash().toInternal()
					+ getStmtOpContCash().toInternal()
					+ getStmtClDate().toInternal()
					+ getStmtClUnits().toInternal()
					+ getStmtClDunits().toInternal()
					+ getStmtClFundCash().toInternal()
					+ getStmtClContCash().toInternal()
					+ getStmtTrnUnits().toInternal()
					+ getStmtTrnDunits().toInternal()
					+ getStmtTrFundCash().toInternal()
					+ getStmtTrContCash().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setUstfrecNonKeyData(Object obj) {
		return setRecNonKeyData(obj);
	}
	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, nonKeyFiller8);
			what = ExternalData.chop(what, nonKeyFiller9);
			what = ExternalData.chop(what, unitStmtFlag);
			what = ExternalData.chop(what, stmtOpDate);
			what = ExternalData.chop(what, stmtOpUnits);
			what = ExternalData.chop(what, stmtOpDunits);
			what = ExternalData.chop(what, stmtOpFundCash);
			what = ExternalData.chop(what, stmtOpContCash);
			what = ExternalData.chop(what, stmtClDate);
			what = ExternalData.chop(what, stmtClUnits);
			what = ExternalData.chop(what, stmtClDunits);
			what = ExternalData.chop(what, stmtClFundCash);
			what = ExternalData.chop(what, stmtClContCash);
			what = ExternalData.chop(what, stmtTrnUnits);
			what = ExternalData.chop(what, stmtTrnDunits);
			what = ExternalData.chop(what, stmtTrFundCash);
			what = ExternalData.chop(what, stmtTrContCash);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}
	public PackedDecimalData getUstmno() {
		return ustmno;
	}
	public void setUstmno(Object what) {
		setUstmno(what, false);
	}
	public void setUstmno(Object what, boolean rounded) {
		if (rounded)
			ustmno.setRounded(what);
		else
			ustmno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getUnitStmtFlag() {
		return unitStmtFlag;
	}
	public void setUnitStmtFlag(Object what) {
		unitStmtFlag.set(what);
	}	
	public PackedDecimalData getStmtOpDate() {
		return stmtOpDate;
	}
	public void setStmtOpDate(Object what) {
		setStmtOpDate(what, false);
	}
	public void setStmtOpDate(Object what, boolean rounded) {
		if (rounded)
			stmtOpDate.setRounded(what);
		else
			stmtOpDate.set(what);
	}	
	public PackedDecimalData getStmtOpUnits() {
		return stmtOpUnits;
	}
	public void setStmtOpUnits(Object what) {
		setStmtOpUnits(what, false);
	}
	public void setStmtOpUnits(Object what, boolean rounded) {
		if (rounded)
			stmtOpUnits.setRounded(what);
		else
			stmtOpUnits.set(what);
	}	
	public PackedDecimalData getStmtOpDunits() {
		return stmtOpDunits;
	}
	public void setStmtOpDunits(Object what) {
		setStmtOpDunits(what, false);
	}
	public void setStmtOpDunits(Object what, boolean rounded) {
		if (rounded)
			stmtOpDunits.setRounded(what);
		else
			stmtOpDunits.set(what);
	}	
	public PackedDecimalData getStmtOpFundCash() {
		return stmtOpFundCash;
	}
	public void setStmtOpFundCash(Object what) {
		setStmtOpFundCash(what, false);
	}
	public void setStmtOpFundCash(Object what, boolean rounded) {
		if (rounded)
			stmtOpFundCash.setRounded(what);
		else
			stmtOpFundCash.set(what);
	}	
	public PackedDecimalData getStmtOpContCash() {
		return stmtOpContCash;
	}
	public void setStmtOpContCash(Object what) {
		setStmtOpContCash(what, false);
	}
	public void setStmtOpContCash(Object what, boolean rounded) {
		if (rounded)
			stmtOpContCash.setRounded(what);
		else
			stmtOpContCash.set(what);
	}	
	public PackedDecimalData getStmtClDate() {
		return stmtClDate;
	}
	public void setStmtClDate(Object what) {
		setStmtClDate(what, false);
	}
	public void setStmtClDate(Object what, boolean rounded) {
		if (rounded)
			stmtClDate.setRounded(what);
		else
			stmtClDate.set(what);
	}	
	public PackedDecimalData getStmtClUnits() {
		return stmtClUnits;
	}
	public void setStmtClUnits(Object what) {
		setStmtClUnits(what, false);
	}
	public void setStmtClUnits(Object what, boolean rounded) {
		if (rounded)
			stmtClUnits.setRounded(what);
		else
			stmtClUnits.set(what);
	}	
	public PackedDecimalData getStmtClDunits() {
		return stmtClDunits;
	}
	public void setStmtClDunits(Object what) {
		setStmtClDunits(what, false);
	}
	public void setStmtClDunits(Object what, boolean rounded) {
		if (rounded)
			stmtClDunits.setRounded(what);
		else
			stmtClDunits.set(what);
	}	
	public PackedDecimalData getStmtClFundCash() {
		return stmtClFundCash;
	}
	public void setStmtClFundCash(Object what) {
		setStmtClFundCash(what, false);
	}
	public void setStmtClFundCash(Object what, boolean rounded) {
		if (rounded)
			stmtClFundCash.setRounded(what);
		else
			stmtClFundCash.set(what);
	}	
	public PackedDecimalData getStmtClContCash() {
		return stmtClContCash;
	}
	public void setStmtClContCash(Object what) {
		setStmtClContCash(what, false);
	}
	public void setStmtClContCash(Object what, boolean rounded) {
		if (rounded)
			stmtClContCash.setRounded(what);
		else
			stmtClContCash.set(what);
	}	
	public PackedDecimalData getStmtTrnUnits() {
		return stmtTrnUnits;
	}
	public void setStmtTrnUnits(Object what) {
		setStmtTrnUnits(what, false);
	}
	public void setStmtTrnUnits(Object what, boolean rounded) {
		if (rounded)
			stmtTrnUnits.setRounded(what);
		else
			stmtTrnUnits.set(what);
	}	
	public PackedDecimalData getStmtTrnDunits() {
		return stmtTrnDunits;
	}
	public void setStmtTrnDunits(Object what) {
		setStmtTrnDunits(what, false);
	}
	public void setStmtTrnDunits(Object what, boolean rounded) {
		if (rounded)
			stmtTrnDunits.setRounded(what);
		else
			stmtTrnDunits.set(what);
	}	
	public PackedDecimalData getStmtTrFundCash() {
		return stmtTrFundCash;
	}
	public void setStmtTrFundCash(Object what) {
		setStmtTrFundCash(what, false);
	}
	public void setStmtTrFundCash(Object what, boolean rounded) {
		if (rounded)
			stmtTrFundCash.setRounded(what);
		else
			stmtTrFundCash.set(what);
	}	
	public PackedDecimalData getStmtTrContCash() {
		return stmtTrContCash;
	}
	public void setStmtTrContCash(Object what) {
		setStmtTrContCash(what, false);
	}
	public void setStmtTrContCash(Object what, boolean rounded) {
		if (rounded)
			stmtTrContCash.setRounded(what);
		else
			stmtTrContCash.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		planSuffix.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		unitVirtualFund.clear();
		unitType.clear();
		ustmno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		nonKeyFiller8.clear();
		nonKeyFiller9.clear();
		unitStmtFlag.clear();
		stmtOpDate.clear();
		stmtOpUnits.clear();
		stmtOpDunits.clear();
		stmtOpFundCash.clear();
		stmtOpContCash.clear();
		stmtClDate.clear();
		stmtClUnits.clear();
		stmtClDunits.clear();
		stmtClFundCash.clear();
		stmtClContCash.clear();
		stmtTrnUnits.clear();
		stmtTrnDunits.clear();
		stmtTrFundCash.clear();
		stmtTrContCash.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}