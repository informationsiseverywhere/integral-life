/*
 * File: P6251at.java
 * Date: 30 August 2009 0:39:43
 * Author: Quipoz Limited
 * 
 * Class transformed from P6251AT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.unitlinkedprocessing.dataaccess.RdirTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*       FUND REDIRECTION - AT MODULE
*       ----------------------------
*
* This AT module program for Fund Redirection.
*
*************************************************************
* </pre>
*/
public class P6251at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P6251AT");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(ZERO);
		/* FORMATS */
	private static final String ulnkrec = "ULNKREC";
	private static final String hitdrec = "HITDREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String th510 = "TH510";
	private HitdTableDAM hitdIO = new HitdTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private RdirTableDAM rdirIO = new RdirTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private T5515rec t5515rec = new T5515rec();
	private Th510rec th510rec = new Th510rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atmodrec atmodrec = new Atmodrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		begnUlnk3020, 
		rewrtUlnk3030, 
		deleteRdir3050, 
		initial3110, 
		a110Fund, 
		a180NextFund
	}

	public P6251at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		updateUlnk3000();
		endOfDrivingFile4000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		readRdir1100();
		callBreakout1120();
		/*EXIT*/
	}

	/**
	* <pre>
	* Read of RDIR file                                           *
	* </pre>
	*/
protected void readRdir1100()
	{
		/*READ-RDIR*/
		rdirIO.setParams(SPACES);
		rdirIO.setChdrcoy(atmodrec.company);
		rdirIO.setChdrnum(atmodrec.primaryKey);
		rdirIO.setPlanSuffix(ZERO);
		rdirIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, rdirIO);
		if (isNE(rdirIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(rdirIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Call the BRKOUT subroutine to breakout COVR and ULNK        *
	* </pre>
	*/
protected void callBreakout1120()
	{
		go1121();
	}

protected void go1121()
	{
		/*  call the breakout routine*/
		wsaaTransactionRec.set(atmodrec.transArea);
		wsaaBatckey.set(atmodrec.batchKey);
		if (isGT(rdirIO.getPlanSuffix(), wsaaPolsum)) {
			return ;
		}
		if (isEQ(rdirIO.getPlanSuffix(), 1)
		&& isEQ(wsaaPolsum, 1)) {
			return ;
		}
		brkoutrec.brkOldSummary.set(wsaaPolsum);
		compute(brkoutrec.brkNewSummary, 0).set(sub(rdirIO.getPlanSuffix(), 1));
		brkoutrec.brkChdrnum.set(atmodrec.primaryKey);
		brkoutrec.brkChdrcoy.set(atmodrec.company);
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(SPACES);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, "****")) {
			syserrrec.params.set(brkoutrec.brkStatuz);
			xxxxFatalError();
		}
	}

protected void updateUlnk3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begnRdir3010();
				case begnUlnk3020: 
					begnUlnk3020();
				case rewrtUlnk3030: 
					rewrtUlnk3030();
					newUlnk3040();
				case deleteRdir3050: 
					deleteRdir3050();
					nextRdir3060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Update the broken out records from records on RDIR          *
	* </pre>
	*/
protected void begnRdir3010()
	{
		/*    Begn RDIR file*/
		rdirIO.setChdrcoy(atmodrec.company);
		rdirIO.setChdrnum(atmodrec.primaryKey);
		rdirIO.setPlanSuffix(0);
		rdirIO.setFunction("BEGNH");
		SmartFileCode.execute(appVars, rdirIO);
		if (isEQ(rdirIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rdirIO.getParams());
			xxxxFatalError();
		}
		wsaaChdrnum.set(rdirIO.getChdrnum());
	}

protected void begnUlnk3020()
	{
		/*    Read corresponding ULNK record*/
		ulnkIO.setChdrcoy(rdirIO.getChdrcoy());
		ulnkIO.setChdrnum(rdirIO.getChdrnum());
		ulnkIO.setLife(rdirIO.getLife());
		ulnkIO.setJlife(ZERO);
		ulnkIO.setCoverage(rdirIO.getCoverage());
		ulnkIO.setRider(rdirIO.getRider());
		ulnkIO.setPlanSuffix(rdirIO.getPlanSuffix());
		ulnkIO.setFunction("BEGNH");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isEQ(ulnkIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.deleteRdir3050);
		}
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(ulnkIO.getChdrcoy(), rdirIO.getChdrcoy())
		&& isEQ(ulnkIO.getChdrnum(), rdirIO.getChdrnum())
		&& isEQ(ulnkIO.getPlanSuffix(), rdirIO.getPlanSuffix())
		&& isEQ(ulnkIO.getLife(), rdirIO.getLife())
		&& isEQ(ulnkIO.getCoverage(), rdirIO.getCoverage())
		&& isEQ(ulnkIO.getRider(), rdirIO.getRider())) {
			goTo(GotoLabel.rewrtUlnk3030);
		}
		else {
			goTo(GotoLabel.deleteRdir3050);
		}
	}

protected void rewrtUlnk3030()
	{
		/*    Rewrite ULNK record with valid flag of 2*/
		ulnkIO.setValidflag("2");
		datcon2rec.intDate1.set(rdirIO.getCurrfrom());
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		ulnkIO.setCurrto(datcon2rec.intDate2);
		ulnkIO.setFormat(ulnkrec);
		ulnkIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			xxxxFatalError();
		}
	}

protected void newUlnk3040()
	{
		/*    Write new ULNK record from RDIR details*/
		ulnkIO.setParams(SPACES);
		ulnkIO.setChdrcoy(rdirIO.getChdrcoy());
		ulnkIO.setChdrnum(rdirIO.getChdrnum());
		ulnkIO.setLife(rdirIO.getLife());
		ulnkIO.setJlife(ZERO);
		ulnkIO.setCoverage(rdirIO.getCoverage());
		ulnkIO.setRider(rdirIO.getRider());
		ulnkIO.setPlanSuffix(rdirIO.getPlanSuffix());
		wsaaSub.set(1);
		initialise3100();
		ulnkIO.setUalfnds(rdirIO.getUalfnds());
		ulnkIO.setUalprcs(rdirIO.getUalprcs());
		ulnkIO.setTranno(rdirIO.getTranno());
		ulnkIO.setPercOrAmntInd(rdirIO.getPercOrAmntInd());
		ulnkIO.setCurrfrom(rdirIO.getCurrfrom());
		ulnkIO.setCurrto(rdirIO.getCurrto());
		ulnkIO.setValidflag("1");
		ulnkIO.setFormat(ulnkrec);
		ulnkIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			xxxxFatalError();
		}
		wsaaSub.set(1);
		a100CheckHitd();
	}

protected void deleteRdir3050()
	{
		/*    Delete the RDIR record*/
		rdirIO.setFunction("DELET");
		SmartFileCode.execute(appVars, rdirIO);
		if (isNE(rdirIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(rdirIO.getParams());
			xxxxFatalError();
		}
	}

protected void nextRdir3060()
	{
		/*    Read next RDIR record for processing*/
		rdirIO.setFunction("NEXTR");
		SmartFileCode.execute(appVars, rdirIO);
		if (isEQ(rdirIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(rdirIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(rdirIO.getParams());
			xxxxFatalError();
		}
		goTo(GotoLabel.begnUlnk3020);
	}

protected void initialise3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case initial3110: 
					initial3110();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Initialise output fields on ULNK
	* </pre>
	*/
protected void initial3110()
	{
		ulnkIO.setUspcpr(wsaaSub, ZERO);
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.initial3110);
		}
		/*EXIT*/
	}

protected void endOfDrivingFile4000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		releaseSoftlock4100();
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock4100()
	{
		releaseSoftlock4101();
	}

protected void releaseSoftlock4101()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(wsaaChdrnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void a100CheckHitd()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case a110Fund: 
					a110Fund();
				case a180NextFund: 
					a180NextFund();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Fund()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(ulnkIO.getCurrfrom());
		itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub));
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.endp)
		&& isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), t5515)
		|| isNE(itdmIO.getItemitem(), ulnkIO.getUalfnd(wsaaSub))
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isNE(t5515rec.zfundtyp, "D")) {
			goTo(GotoLabel.a180NextFund);
		}
		hitdIO.setParams(SPACES);
		hitdIO.setChdrcoy(rdirIO.getChdrcoy());
		hitdIO.setChdrnum(rdirIO.getChdrnum());
		hitdIO.setLife(rdirIO.getLife());
		hitdIO.setCoverage(rdirIO.getCoverage());
		hitdIO.setRider(rdirIO.getRider());
		hitdIO.setPlanSuffix(rdirIO.getPlanSuffix());
		hitdIO.setZintbfnd(ulnkIO.getUalfnd(wsaaSub));
		hitdIO.setFormat(hitdrec);
		hitdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)
		&& isNE(hitdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitdIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(hitdIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.a180NextFund);
		}
		hitdIO.setZlstintdte(varcom.vrcmMaxDate);
		hitdIO.setZlstsmtdt(varcom.vrcmMaxDate);
		hitdIO.setZlstfndval(ZERO);
		hitdIO.setZlstsmtno(ZERO);
		hitdIO.setZlstsmtbal(ZERO);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(th510);
		itdmIO.setItmfrm(ulnkIO.getCurrfrom());
		itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub));
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), th510)
		|| isNE(itdmIO.getItemitem(), ulnkIO.getUalfnd(wsaaSub))
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemtabl(th510);
			itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub));
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		th510rec.th510Rec.set(itdmIO.getGenarea());
		a300GetNextFreq();
		hitdIO.setZnxtintdte(datcon2rec.intDate2);
		hitdIO.setTranno(ulnkIO.getTranno());
		hitdIO.setValidflag("1");
		hitdIO.setEffdate(ulnkIO.getCurrfrom());
		hitdIO.setFormat(hitdrec);
		hitdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			xxxxFatalError();
		}
	}

protected void a180NextFund()
	{
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.a110Fund);
		}
		/*A190-EXIT*/
	}

protected void a300GetNextFreq()
	{
		a310Next();
	}

protected void a310Next()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(ulnkIO.getCurrfrom());
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(th510rec.zintalofrq);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		if (isNE(th510rec.zintfixdd, ZERO)) {
			/*     MOVE TH510-ZINTFIXDD    TO DTC2-INT-DATE-2 (5:2) <LA4737>*/
			datcon2rec.intDate2.setSub1String(7, 2, th510rec.zintfixdd);
		}
	}
}
