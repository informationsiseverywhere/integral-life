/*
 * File: P6220.java
 * Date: 30 August 2009 0:36:36
 * Author: Quipoz Limited
 * 
 * Class transformed from P6220.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupjTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S6220ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Unit Prices Job Scroll Selection Program
*
* This program uses the effective date and action passed from
* the submenu screen(S5412) to determine which job numbers to
* be displayed. So only the valid and relevant job numbers will
* be shown on the screen. After each selection has been processed,
* it is updated with a '*' shown next to the job number on the
* screen and the control is always passed back to this program
* after all the selections have been processed. To exit and return
* to the submenu screen, press ENTER without selecting. The last
* processed job number and date is also returned on the submenu
* screen.
*
*****************************************************************
* </pre>
*/
public class P6220 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6220");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private BinaryData wsaaCount = new BinaryData(3, 0);
	private BinaryData wsaaSubfchg = new BinaryData(3, 0);
	private BinaryData wsaaPtr = new BinaryData(3, 0);
	private BinaryData wsaaRrn = new BinaryData(5, 0);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaJobno = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
		/* TABLES */
	private String tr386 = "TR386";
		/* FORMATS */
	private String vprcupjrec = "VPRCUPJREC";
	private String itemrec = "ITEMREC";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr386rec tr386rec = new Tr386rec();
		/*Unot Price File, Effdate/Jobno as key*/
	private VprcupjTableDAM vprcupjIO = new VprcupjTableDAM();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S6220ScreenVars sv = ScreenProgram.getScreenVars( S6220ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1900, 
		preExit, 
		validateSubfile2600, 
		exit2090, 
		call4100, 
		exit4900, 
		nextRecord5200, 
		exit5900
	}

	public P6220() {
		super();
		screenVars = sv;
		new ScreenModel("S6220", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1100();
			load1300();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1100()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1900);
		}
		if (isNE(wsaaSubfchg,0)) {
			wsaaSubfchg.set(0);
			scrnparams.subfileRrn.set(wsaaRrn);
			goTo(GotoLabel.exit1900);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6220", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void load1300()
	{
		vprcupjIO.setUnitVirtualFund(SPACES);
		vprcupjIO.setUnitType(SPACES);
		vprcupjIO.setJobno(ZERO);
		vprcupjIO.setEffdate(ZERO);
		wsaaJobno.set(ZERO);
		wsaaEffdate.set(ZERO);
		wsaaSubfchg.set(ZERO);
		vprcupjIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupjIO.setJobno(99999999);
		vprcupjIO.setCompany(wsspcomn.company);
		vprcupjIO.setFunction("BEGN");
		vprcupjIO.setFormat(vprcupjrec);
		SmartFileCode.execute(appVars, vprcupjIO);
		if (isNE(vprcupjIO.getStatuz(),varcom.oK)
		&& isNE(vprcupjIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupjIO.getParams());
			fatalError600();
		}
		wsaaCount.set(1);
		if (isEQ(vprcupjIO.getStatuz(),varcom.endp)) {
			wsaaCount.set(18);
		}
		wsaaEffdate.set(vprcupjIO.getEffdate());
		wsaaJobno.set(vprcupjIO.getJobno());
		while ( !(isEQ(wsaaCount,18))) {
			loadSubfile5000();
		}
		
		wsaaRrn.set(scrnparams.subfileRrn);
		if (isEQ(vprcupjIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					rollUp2100();
				}
				case validateSubfile2600: {
					validateSubfile2600();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void rollUp2100()
	{
		if ((isNE(scrnparams.statuz,varcom.rolu))) {
			scrnparams.function.set(varcom.srnch);
			goTo(GotoLabel.validateSubfile2600);
		}
		sv.subfileArea.set(SPACES);
		vprcupjIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprcupjIO);
		if (isNE(vprcupjIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vprcupjIO.getParams());
			fatalError600();
		}
		wsaaCount.set(1);
		wsaaEffdate.set(vprcupjIO.getEffdate());
		wsaaJobno.set(vprcupjIO.getJobno());
		while ( !(isEQ(wsaaCount,18))) {
			loadSubfile5000();
		}
		
		if (isEQ(vprcupjIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void validateSubfile2600()
	{
		processScreen("S6220", sv);
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.validateSubfile2600);
		}
		wsaaSubfchg.add(1);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4010();
				}
				case call4100: {
					call4100();
					selection4200();
				}
				case exit4900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4010()
	{
		if (isEQ(wsaaSubfchg,0)) {
			compute(wsaaPtr, 0).set(add(wsspcomn.programPtr,1));
			wsspcomn.secProg[wsaaPtr.toInt()].set(SPACES);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4900);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.function.set(varcom.srdn);
		}
		else {
			scrnparams.function.set(varcom.sstrt);
		}
	}

protected void call4100()
	{
		processScreen("S6220", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4900);
		}
	}

protected void selection4200()
	{
		if (isEQ(sv.select,SPACES)) {
			scrnparams.function.set(varcom.srdn);
			goTo(GotoLabel.call4100);
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspuprc.uprcEffdate.set(sv.effdate);
		wsspuprc.uprcJobno.set(sv.jobno);
		wsspcomn.programPtr.add(1);
		sv.select.set(SPACES);
		sv.change.set("*");
		scrnparams.function.set(varcom.supd);
		processScreen("S6220", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void loadSubfile5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5100();
				}
				case nextRecord5200: {
					nextRecord5200();
				}
				case exit5900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5100()
	{
		if (isNE(vprcupjIO.getCompany(),wsspcomn.company)) {
			vprcupjIO.setStatuz(varcom.endp);
			wsaaCount.set(18);
			goTo(GotoLabel.exit5900);
		}
		if (isEQ(wsspcomn.flag,"B")) {
			if (isNE(vprcupjIO.getValidflag(),"2")) {
				goTo(GotoLabel.nextRecord5200);
			}
		}
		if (isEQ(wsspcomn.flag,"C")) {
			if (isNE(vprcupjIO.getValidflag(),"2")) {
				goTo(GotoLabel.nextRecord5200);
			}
		}
		if (isEQ(wsspcomn.flag,"D")) {
			if (isNE(vprcupjIO.getValidflag(),"3")) {
				goTo(GotoLabel.nextRecord5200);
			}
		}
		if (isEQ(wsspcomn.flag,"G")) {
			if (isNE(vprcupjIO.getValidflag(),"3")) {
				goTo(GotoLabel.nextRecord5200);
			}
		}
		sv.jobno.set(vprcupjIO.getJobno());
		sv.effdate.set(vprcupjIO.getEffdate());
		sv.change.set(SPACES);
		sv.validtype.set(SPACES);
		if (isEQ(vprcupjIO.getValidflag(),1)) {
			sv.validtype.set(tr386rec.progdesc01);
		}
		if (isEQ(vprcupjIO.getValidflag(),2)) {
			sv.validtype.set(tr386rec.progdesc02);
		}
		if (isEQ(vprcupjIO.getValidflag(),3)) {
			sv.validtype.set(tr386rec.progdesc02);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6220", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
	}

protected void nextRecord5200()
	{
		vprcupjIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vprcupjIO);
		if (isNE(vprcupjIO.getStatuz(),varcom.oK)
		&& isNE(vprcupjIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupjIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupjIO.getStatuz(),varcom.endp)) {
			wsaaCount.set(18);
			goTo(GotoLabel.exit5900);
		}
		if (isEQ(vprcupjIO.getJobno(),wsaaJobno)
		&& isEQ(vprcupjIO.getEffdate(),wsaaEffdate)) {
			goTo(GotoLabel.nextRecord5200);
		}
		else {
			wsaaEffdate.set(vprcupjIO.getEffdate());
			wsaaJobno.set(vprcupjIO.getJobno());
		}
	}
}
