package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:45
 * Description:
 * Copybook name: USTSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustsKey = new FixedLengthStringData(64).isAPartOf(ustsFileKey, 0, REDEFINE);
  	public FixedLengthStringData ustsChdrcoy = new FixedLengthStringData(1).isAPartOf(ustsKey, 0);
  	public FixedLengthStringData ustsChdrnum = new FixedLengthStringData(8).isAPartOf(ustsKey, 1);
  	public PackedDecimalData ustsPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ustsKey, 9);
  	public PackedDecimalData ustsUstmno = new PackedDecimalData(5, 0).isAPartOf(ustsKey, 12);
  	public FixedLengthStringData ustsLife = new FixedLengthStringData(2).isAPartOf(ustsKey, 15);
  	public FixedLengthStringData ustsCoverage = new FixedLengthStringData(2).isAPartOf(ustsKey, 17);
  	public FixedLengthStringData ustsRider = new FixedLengthStringData(2).isAPartOf(ustsKey, 19);
  	public FixedLengthStringData ustsUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(ustsKey, 21);
  	public FixedLengthStringData ustsUnitType = new FixedLengthStringData(1).isAPartOf(ustsKey, 25);
  	public PackedDecimalData ustsMoniesDate = new PackedDecimalData(8, 0).isAPartOf(ustsKey, 26);
  	public PackedDecimalData ustsTranno = new PackedDecimalData(5, 0).isAPartOf(ustsKey, 31);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ustsKey, 34, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}