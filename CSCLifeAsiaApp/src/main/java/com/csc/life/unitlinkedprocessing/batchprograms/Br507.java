/*
 * File: Br507.java
 * Date: 29 August 2009 22:10:52
 * Author: Quipoz Limited
 * 
 * Class transformed from BR507.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.recordstructures.Srvunitcpy;
import com.csc.life.anticipatedendowment.recordstructures.Pr507par;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.reports.Rr507Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* BR507 - Fund Valuation New Business Cancellation Report
*
*
*****************************************************************
* </pre>
*/
public class Br507 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlchdrpf1rs = null;
	private java.sql.PreparedStatement sqlchdrpf1ps = null;
	private java.sql.Connection sqlchdrpf1conn = null;
	private String sqlchdrpf1 = "";
	private Rr507Report printerFile = new Rr507Report();
	private SortFileDAM sortFile = new SortFileDAM("BR507SRT");
	private FixedLengthStringData printerRec = new FixedLengthStringData(320);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR507");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaContractTot = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaPremTot = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaFundTot = new ZonedDecimalData(11, 2);
	private FixedLengthStringData wsaaUalfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private ZonedDecimalData sub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaDateFrm = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateTo = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private FixedLengthStringData wsaaValidFlag3 = new FixedLengthStringData(1).init("3");

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-CHDRPF */
	private FixedLengthStringData sqlChdrrec = new FixedLengthStringData(18);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlChdrrec, 0);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlChdrrec, 8);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlChdrrec, 11);
	private PackedDecimalData sqlOccdate = new PackedDecimalData(8, 0).isAPartOf(sqlChdrrec, 13);
		/* ERRORS */
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3629 = "T3629";
	private static final String t5515 = "T5515";
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEofSort = new FixedLengthStringData(1).init("N");
	private Validator endOfSortFile = new Validator(wsaaEofSort, "Y");
	private FixedLengthStringData wsaaSrvuLife = new FixedLengthStringData(2);

	private FixedLengthStringData rr507H01 = new FixedLengthStringData(110);
	private FixedLengthStringData rr507h01O = new FixedLengthStringData(110).isAPartOf(rr507H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr507h01O, 0);
	private FixedLengthStringData ualfnd = new FixedLengthStringData(4).isAPartOf(rr507h01O, 10);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rr507h01O, 14);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rr507h01O, 44);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30).isAPartOf(rr507h01O, 47);
	private FixedLengthStringData ultype = new FixedLengthStringData(1).isAPartOf(rr507h01O, 77);
	private FixedLengthStringData statcode = new FixedLengthStringData(2).isAPartOf(rr507h01O, 78);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rr507h01O, 80);

	private FixedLengthStringData rr507D01 = new FixedLengthStringData(93);
	private FixedLengthStringData rr507d01O = new FixedLengthStringData(93).isAPartOf(rr507D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr507d01O, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rr507d01O, 8);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2).isAPartOf(rr507d01O, 12);
	private FixedLengthStringData rundte = new FixedLengthStringData(10).isAPartOf(rr507d01O, 29);
	private ZonedDecimalData ualprc = new ZonedDecimalData(17, 2).isAPartOf(rr507d01O, 39);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(rr507d01O, 56);
	private FixedLengthStringData ptrneff = new FixedLengthStringData(10).isAPartOf(rr507d01O, 66);
	private ZonedDecimalData sacscurbal = new ZonedDecimalData(17, 2).isAPartOf(rr507d01O, 76);

	private FixedLengthStringData rr507S01 = new FixedLengthStringData(40);
	private FixedLengthStringData rr507s01O = new FixedLengthStringData(40).isAPartOf(rr507S01, 0);
	private ZonedDecimalData totccount = new ZonedDecimalData(9, 0).isAPartOf(rr507s01O, 0);
	private ZonedDecimalData totprm = new ZonedDecimalData(13, 2).isAPartOf(rr507s01O, 9);
	private ZonedDecimalData totfundval = new ZonedDecimalData(18, 5).isAPartOf(rr507s01O, 22);
		/* COPY TR386REC.                                       <LA3235>*/
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pr507par pr507par = new Pr507par();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private Srvunitcpy srvunitcpy = new Srvunitcpy();
	private FormatsInner formatsInner = new FormatsInner();
	private SortRecInner sortRecInner = new SortRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2108, 
		exit2109
	}

	public Br507() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		pr507par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaSrvuLife.set(srvunitcpy.lifeVal);
		printerFile.openOutput();
		wsaaUalfnd.set(SPACES);
		if (isEQ(pr507par.datefrm,varcom.maxdate)
		|| isEQ(pr507par.datefrm,varcom.vrcmMaxDate)) {
			wsaaDateFrm.set(0);
		}
		else {
			wsaaDateFrm.set(pr507par.datefrm);
		}
		wsaaDateTo.set(pr507par.dateto);
		sqlchdrpf1 = " SELECT  CHDRNUM, CNTCURR, STATCODE, OCCDATE" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE VALIDFLAG = ?" +
" AND SERVUNIT = ?";
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlchdrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrpf1ps = getAppVars().prepareStatementEmbeded(sqlchdrpf1conn, sqlchdrpf1, "CHDRPF");
			getAppVars().setDBString(sqlchdrpf1ps, 1, wsaaValidFlag3);
			getAppVars().setDBString(sqlchdrpf1ps, 2, wsaaSrvuLife);
			sqlchdrpf1rs = getAppVars().executeQuery(sqlchdrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		/* Read T5645 for sub-account code and type (for reading ACBLs    .*/
		/* for suspense)*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T5679 for valid statii for contract header.               .*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*--- Read TR386 table to get screen literals                      
	**** MOVE ITEMREC                TO ITEM-FORMAT.          <LA3235>
	**** MOVE 'IT'                   TO ITEM-ITEMPFX.         <LA3235>
	**** MOVE BSPR-COMPANY           TO ITEM-ITEMCOY.         <LA3235>
	**** MOVE TR386                  TO ITEM-ITEMTABL.        <LA3235>
	**** MOVE BSSC-LANGUAGE          TO ITEM-ITEMITEM.        <LA3235>
	**** MOVE WSAA-PROG              TO ITEM-ITEMITEM(2:5).   <LA3235>
	**** MOVE READR                  TO ITEM-FUNCTION.        <LA3235>
	****                                                      <LA3235>
	**** CALL 'ITEMIO'               USING ITEM-PARAMS.       <LA3235>
	****                                                      <LA3235>
	**** IF  ITEM-STATUZ             NOT = O-K                <LA3235>
	****     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <LA3235>
	****     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <LA3235>
	****     PERFORM 600-FATAL-ERROR                          <LA3235>
	**** END-IF.                                              <LA3235>
	****                                                      <LA3235>
	**** MOVE ITEM-GENAREA        TO TR386-TR386-REC.         <LA3235>
	*1090-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void readFile2000()
	{
		/*READ-FILE*/
		sortFile.openOutput();
		retrieveData2050();
		sortFile.close();
		FileSort fs1 = new FileSort(sortFile);
		fs1.addSortKey(sortRecInner.sortUalfnd, true);
		fs1.addSortKey(sortRecInner.sortStatcode, true);
		fs1.addSortKey(sortRecInner.sortChdrnum, true);
		fs1.sort();
		sortFile.openInput();
		retrieveSorted2200();
		sortFile.close();
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void retrieveData2050()
	{
		/*RETRIEVAL*/
		wsaaEofSort.set("N");
		while ( !(endOfSortFile.isTrue())) {
			releaseToSort2100();
		}
		
		/*EXIT*/
	}

protected void releaseToSort2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					release2101();
				case eof2108: 
					eof2108();
				case exit2109: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void release2101()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlchdrpf1rs)) {
				getAppVars().getDBObject(sqlchdrpf1rs, 1, sqlChdrnum);
				getAppVars().getDBObject(sqlchdrpf1rs, 2, sqlCntcurr);
				getAppVars().getDBObject(sqlchdrpf1rs, 3, sqlStatcode);
				getAppVars().getDBObject(sqlchdrpf1rs, 4, sqlOccdate);
			}
			else {
				goTo(GotoLabel.eof2108);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* Check record is required for processing.*/
		wsaaValidContract.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| validContract.isTrue()); wsaaIndex.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],sqlStatcode)) {
				wsaaValidContract.set("Y");
			}
		}
		if (validContract.isTrue()) {
			readAcbl2600();
			if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.exit2109);
			}
			ptrnrevIO.setParams(SPACES);
			ptrnrevIO.setChdrcoy(bsprIO.getCompany());
			ptrnrevIO.setChdrnum(sqlChdrnum);
			ptrnrevIO.setTranno(99999);
			ptrnrevIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, ptrnrevIO);
			if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
			|| isNE(ptrnrevIO.getChdrcoy(),bsprIO.getCompany())
			|| isNE(ptrnrevIO.getChdrnum(),sqlChdrnum)) {
				syserrrec.params.set(ptrnrevIO.getParams());
				fatalError600();
			}
			if (isLT(ptrnrevIO.getPtrneff(),wsaaDateFrm)
			|| isGT(ptrnrevIO.getPtrneff(),wsaaDateTo)) {
				goTo(GotoLabel.exit2109);
			}
			sortRecInner.sortPtrneff.set(ptrnrevIO.getPtrneff());
			unltunlIO.setParams(SPACES);
			unltunlIO.setChdrcoy(bsprIO.getCompany());
			unltunlIO.setChdrnum(sqlChdrnum);
			unltunlIO.setLife(SPACES);
			unltunlIO.setCoverage(SPACES);
			unltunlIO.setRider(SPACES);
			unltunlIO.setSeqnbr(ZERO);
			unltunlIO.setFormat(formatsInner.unltunlrec);
			unltunlIO.setFunction(varcom.begn);
			while ( !(isEQ(unltunlIO.getStatuz(),varcom.endp))) {
				readUnltunl2700();
			}
			
		}
		goTo(GotoLabel.exit2109);
	}

protected void eof2108()
	{
		wsaaEofSort.set("Y");
	}

protected void retrieveSorted2200()
	{
		/*STARTED*/
		wsaaContractTot.set(0);
		wsaaEofSort.set("N");
		while ( !(endOfSortFile.isTrue())) {
			printReport2250();
		}
		
		/* To print Summary total of details*/
		if (isGT(wsaaContractTot,0)) {
			printSummary2900();
		}
		/*EXIT*/
	}

protected void printReport2250()
	{
			print2251();
		}

protected void print2251()
	{
		sortFile.read(sortRecInner.sortRec);
		if (sortFile.isAtEnd()) {
			wsaaEofSort.set("Y");
			return ;
		}
		/* Check for change in Fund Code*/
		if (isNE(wsaaUalfnd,SPACES)) {
			if (isNE(wsaaUalfnd, sortRecInner.sortUalfnd)) {
				printSummary2900();
			}
		}
		/* Check for change in Fund Code, Status code or New page*/
		if (!(isEQ(wsaaUalfnd, sortRecInner.sortUalfnd)
		&& isEQ(wsaaStatcode, sortRecInner.sortStatcode))
		|| newPageReq.isTrue()) {
			headerDetails2800();
		}
		/* MOVE SQL-CHDRNUM            TO CHDRNUM OF RR507D01-O.        */
		chdrnum.set(sortRecInner.sortChdrnum);
		crtable.set(sortRecInner.sortCrtable);
		singp.set(sortRecInner.sortSingp);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sortRecInner.sortRundte);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rundte.set(datcon1rec.extDate);
		ualprc.set(sortRecInner.sortUalprc);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sqlOccdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		occdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sortRecInner.sortPtrneff);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		ptrneff.set(datcon1rec.extDate);
		sacscurbal.set(sortRecInner.sortSacscurbal);
		printerFile.printRr507d01(rr507D01);
		/* Add detail total to Summary total.*/
		wsaaContractTot.add(1);
		wsaaPremTot.add(sortRecInner.sortSingp);
		wsaaFundTot.add(sortRecInner.sortUalprc);
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void readAcbl2600()
	{
		readAcbl2610();
	}

protected void readAcbl2610()
	{
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(bsprIO.getCompany());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setOrigcurr(sqlCntcurr);
		acblIO.setRldgacct(sqlChdrnum);
		acblIO.setFormat(formatsInner.acblrec);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(),varcom.oK)) {
			compute(sortRecInner.sortSacscurbal, 2).set(mult(acblIO.getSacscurbal(), -1));
		}
	}

protected void readUnltunl2700()
	{
			readUnltunl2710();
		}

protected void readUnltunl2710()
	{
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(),varcom.oK)
		&& isNE(unltunlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		if (isNE(bsprIO.getCompany(),unltunlIO.getChdrcoy())
		|| isNE(sqlChdrnum,unltunlIO.getChdrnum())
		|| isEQ(unltunlIO.getStatuz(),varcom.endp)) {
			unltunlIO.setStatuz(varcom.endp);
			return ;
		}
		covtunlIO.setParams(SPACES);
		covtunlIO.setChdrcoy(unltunlIO.getChdrcoy());
		covtunlIO.setChdrnum(unltunlIO.getChdrnum());
		covtunlIO.setLife(unltunlIO.getLife());
		covtunlIO.setCoverage(unltunlIO.getCoverage());
		covtunlIO.setRider(unltunlIO.getRider());
		covtunlIO.setSeqnbr(ZERO);
		covtunlIO.setFunction(varcom.begn);
		covtunlIO.setFormat(formatsInner.covtunlrec);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(),varcom.oK)
		|| isNE(covtunlIO.getChdrcoy(),unltunlIO.getChdrcoy())
		|| isNE(covtunlIO.getChdrnum(),unltunlIO.getChdrnum())
		|| isNE(covtunlIO.getLife(),unltunlIO.getLife())
		|| isNE(covtunlIO.getCoverage(),unltunlIO.getCoverage())
		|| isNE(covtunlIO.getRider(),unltunlIO.getRider())) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		sortRecInner.sortCrtable.set(covtunlIO.getCrtable());
		sortRecInner.sortRundte.set(covtunlIO.getReserveUnitsDate());
		if (isEQ(covtunlIO.getBillfreq(),"00")) {
			sortRecInner.sortSingp.set(covtunlIO.getSingp());
		}
		else {
			sortRecInner.sortSingp.set(covtunlIO.getInstprem());
		}
		/* Check Fund Code and Transaction Value.*/
		for (sub.set(1); !(isGT(sub,10)); sub.add(1)){
			if (isNE(unltunlIO.getUalfnd(sub),SPACES)) {
				sortRecInner.sortChdrnum.set(sqlChdrnum);
				sortRecInner.sortOccdate.set(sqlOccdate);
				sortRecInner.sortStatcode.set(sqlStatcode);
				sortRecInner.sortCntcurr.set(sqlCntcurr);
				sortRecInner.sortUalfnd.set(unltunlIO.getUalfnd(sub));
				if (isEQ(unltunlIO.getPercOrAmntInd(),"A")) {
					sortRecInner.sortUalprc.set(unltunlIO.getUalprc(sub));
				}
				else {
					compute(sortRecInner.sortUalprc, 0).set(div(mult(sortRecInner.sortSingp, unltunlIO.getUalprc(sub)), 100));
				}
				sortFile.write(sortRecInner.sortRec);
			}
		}
		unltunlIO.setFunction(varcom.nextr);
	}

protected void headerDetails2800()
	{
		headerDetails2810();
	}

protected void headerDetails2810()
	{
		initialize(indicArea);
		/* Read T5515 with Fund Code to retrieve the Fund Currency and*/
		/* Unit Type*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(sortRecInner.sortUalfnd);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		/* Read T5515 again to get the Fund Code Description*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5515);
		descIO.setDescitem(sortRecInner.sortUalfnd);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		ualfnd.set(sortRecInner.sortUalfnd);
		wsaaUalfnd.set(sortRecInner.sortUalfnd);
		descrip.set(descIO.getLongdesc());
		/* Read T3629 to get the Currency description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(t5515rec.currcode);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		currcode.set(t5515rec.currcode);
		currencynm.set(descIO.getLongdesc());
		ultype.set(t5515rec.unitType);
		if (isEQ(t5515rec.unitType,"I")) {
			/*    MOVE 'Initial'           TO LNGDSC     OF RR507H01-O      */
			/*    MOVE TR386-PROGDESC-1    TO LNGDSC     OF RR507H01-O      */
			/*    MOVE TR386-PROGDESC-01   TO LNGDSC     OF RR507H01-O      */
			indicTable[1].set("1");
			indicTable[2].set("0");
			indicTable[3].set("0");
		}
		else {
			if (isEQ(t5515rec.unitType,"A")) {
				/*    MOVE 'Accumulation'      TO LNGDSC     OF RR507H01-O      */
				/*    MOVE TR386-PROGDESC-2    TO LNGDSC     OF RR507H01-O      */
				/*    MOVE TR386-PROGDESC-02   TO LNGDSC     OF RR507H01-O      */
				indicTable[2].set("1");
				indicTable[1].set("0");
				indicTable[3].set("0");
			}
			else {
				if (isEQ(t5515rec.unitType,"B")) {
					/*    MOVE 'Both'              TO LNGDSC     OF RR507H01-O      */
					/*    MOVE TR386-PROGDESC-3    TO LNGDSC     OF RR507H01-O      */
					/*    MOVE TR386-PROGDESC-03   TO LNGDSC     OF RR507H01-O      */
					indicTable[3].set("1");
					indicTable[1].set("0");
					indicTable[2].set("0");
				}
			}
		}
		/* Read T3623 to get the Risk Status description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3623);
		descIO.setDescitem(sortRecInner.sortStatcode);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		statcode.set(sortRecInner.sortStatcode);
		wsaaStatcode.set(sortRecInner.sortStatcode);
		longdesc.set(descIO.getLongdesc());
		printerFile.printRr507h01(rr507H01);
		/*                              INDICATORS INDIC-AREA.   <S19FIX>*/
		wsaaOverflow.set("N");
	}

protected void printSummary2900()
	{
		/*PRINT-SUNNARY*/
		totccount.set(wsaaContractTot);
		totprm.set(wsaaPremTot);
		totfundval.set(wsaaFundTot);
		printerFile.printRr507s01(rr507S01);
		initialize(wsaaContractTot);
		initialize(wsaaPremTot);
		initialize(wsaaFundTot);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		printerFile.close();
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure SORT-REC--INNER
 */
private static final class SortRecInner { 

	private FixedLengthStringData sortRec = new FixedLengthStringData(63);
	private FixedLengthStringData sortChdrnum = new FixedLengthStringData(8).isAPartOf(sortRec, 0);
	private PackedDecimalData sortOccdate = new PackedDecimalData(8, 0).isAPartOf(sortRec, 8);
	private PackedDecimalData sortRundte = new PackedDecimalData(8, 0).isAPartOf(sortRec, 13);
	private PackedDecimalData sortSingp = new PackedDecimalData(17, 2).isAPartOf(sortRec, 18);
	private PackedDecimalData sortSacscurbal = new PackedDecimalData(17, 2).isAPartOf(sortRec, 27);
	private FixedLengthStringData sortCrtable = new FixedLengthStringData(4).isAPartOf(sortRec, 36);
	private FixedLengthStringData sortStatcode = new FixedLengthStringData(2).isAPartOf(sortRec, 40);
	private FixedLengthStringData sortCntcurr = new FixedLengthStringData(3).isAPartOf(sortRec, 42);
	private FixedLengthStringData sortUalfnd = new FixedLengthStringData(4).isAPartOf(sortRec, 45);
	private PackedDecimalData sortUalprc = new PackedDecimalData(17, 2).isAPartOf(sortRec, 49);
	private PackedDecimalData sortPtrneff = new PackedDecimalData(8, 0).isAPartOf(sortRec, 58);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData unltunlrec = new FixedLengthStringData(10).init("UNLTUNLREC");
	private FixedLengthStringData covtunlrec = new FixedLengthStringData(10).init("COVTUNLREC");
}
}
