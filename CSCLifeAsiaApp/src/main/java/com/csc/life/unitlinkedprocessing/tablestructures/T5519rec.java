package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:22
 * Description:
 * Copybook name: T5519REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5519rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5519Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData annchg = new ZonedDecimalData(17, 2).isAPartOf(t5519Rec, 0);
  	public FixedLengthStringData diffprice = new FixedLengthStringData(1).isAPartOf(t5519Rec, 17);
  	public ZonedDecimalData fixdtrm = new ZonedDecimalData(3, 0).isAPartOf(t5519Rec, 18);
  	public FixedLengthStringData initUnitChargeFreq = new FixedLengthStringData(2).isAPartOf(t5519Rec, 21);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(t5519Rec, 23);
  	public FixedLengthStringData unitadj = new FixedLengthStringData(1).isAPartOf(t5519Rec, 33);
  	public FixedLengthStringData unitdeduc = new FixedLengthStringData(1).isAPartOf(t5519Rec, 34);
  	public FixedLengthStringData filler = new FixedLengthStringData(465).isAPartOf(t5519Rec, 35, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5519Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5519Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}