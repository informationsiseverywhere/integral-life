package com.csc.life.unitlinkedprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:53
 * Description:
 * Copybook name: WSSPUPRC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Wsspuprc extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData userArea = new FixedLengthStringData(765);
  	public FixedLengthStringData uprcUserArea = new FixedLengthStringData(765).isAPartOf(userArea, 0);
  	public FixedLengthStringData uprckey = new FixedLengthStringData(18).isAPartOf(uprcUserArea, 0);
  	public PackedDecimalData uprcEffdate = new PackedDecimalData(8, 0).isAPartOf(uprckey, 0);
  	public ZonedDecimalData uprcJobno = new ZonedDecimalData(8, 0).isAPartOf(uprckey, 5).setUnsigned();
  	public FixedLengthStringData uprcVrtfnd = new FixedLengthStringData(4).isAPartOf(uprckey, 13);
  	public FixedLengthStringData uprcFundType = new FixedLengthStringData(1).isAPartOf(uprckey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(747).isAPartOf(uprcUserArea, 18);


	public void initialize() {
		COBOLFunctions.initialize(userArea);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		userArea.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}