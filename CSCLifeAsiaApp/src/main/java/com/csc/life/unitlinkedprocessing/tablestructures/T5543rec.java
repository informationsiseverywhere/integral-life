package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:44
 * Description:
 * Copybook name: T5543REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5543rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5543Rec = new FixedLengthStringData(536);
  	public FixedLengthStringData vfundescs = new FixedLengthStringData(360).isAPartOf(t5543Rec, 0);
  	public FixedLengthStringData[] vfundesc = FLSArrayPartOfStructure(12, 30, vfundescs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(360).isAPartOf(vfundescs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData vfundesc1 = new FixedLengthStringData(30).isAPartOf(filler, 0);
  	public FixedLengthStringData vfundesc2 = new FixedLengthStringData(30).isAPartOf(filler, 30);     
  	public FixedLengthStringData vfundesc3 = new FixedLengthStringData(30).isAPartOf(filler, 60);
  	public FixedLengthStringData vfundesc4 = new FixedLengthStringData(30).isAPartOf(filler, 90);
  	public FixedLengthStringData vfundesc5 = new FixedLengthStringData(30).isAPartOf(filler, 120);
  	public FixedLengthStringData vfundesc6 = new FixedLengthStringData(30).isAPartOf(filler, 150);
  	public FixedLengthStringData vfundesc7 = new FixedLengthStringData(30).isAPartOf(filler, 180);
  	public FixedLengthStringData vfundesc8 = new FixedLengthStringData(30).isAPartOf(filler, 210);
  	public FixedLengthStringData vfundesc9 = new FixedLengthStringData(30).isAPartOf(filler, 240);
  	public FixedLengthStringData vfundesc10 = new FixedLengthStringData(30).isAPartOf(filler, 270);
  	public FixedLengthStringData vfundesc11= new FixedLengthStringData(30).isAPartOf(filler, 300);
  	public FixedLengthStringData vfundesc12 = new FixedLengthStringData(30).isAPartOf(filler, 330);
  	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(48).isAPartOf(t5543Rec, 360);
  	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(12, 4, unitVirtualFunds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(48).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData unitVirtualFund1 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData unitVirtualFund2 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData unitVirtualFund3 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData unitVirtualFund4 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData unitVirtualFund5 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData unitVirtualFund6 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData unitVirtualFund7 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData unitVirtualFund8 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData unitVirtualFund9 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData unitVirtualFund10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData unitVirtualFund11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData unitVirtualFund12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(92).isAPartOf(t5543Rec, 408, FILLER);
  	public FixedLengthStringData allowperiods = new FixedLengthStringData(36).isAPartOf(t5543Rec, 500);
  	public FixedLengthStringData[] allowperiod = FLSArrayPartOfStructure(12, 3, allowperiods, 0, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(36).isAPartOf(allowperiods, 0, FILLER);
	public FixedLengthStringData allowperiod01 = new FixedLengthStringData(3).isAPartOf(filler6, 0);
	public FixedLengthStringData allowperiod02 = new FixedLengthStringData(3).isAPartOf(filler6, 3);
	public FixedLengthStringData allowperiod03 = new FixedLengthStringData(3).isAPartOf(filler6, 6);
	public FixedLengthStringData allowperiod04 = new FixedLengthStringData(3).isAPartOf(filler6, 9);
	public FixedLengthStringData allowperiod05 = new FixedLengthStringData(3).isAPartOf(filler6, 12);
	public FixedLengthStringData allowperiod06 = new FixedLengthStringData(3).isAPartOf(filler6, 15);
	public FixedLengthStringData allowperiod07 = new FixedLengthStringData(3).isAPartOf(filler6, 18);
	public FixedLengthStringData allowperiod08 = new FixedLengthStringData(3).isAPartOf(filler6, 21);
	public FixedLengthStringData allowperiod09 = new FixedLengthStringData(3).isAPartOf(filler6, 24);
	public FixedLengthStringData allowperiod10 = new FixedLengthStringData(3).isAPartOf(filler6, 27);
	public FixedLengthStringData allowperiod11 = new FixedLengthStringData(3).isAPartOf(filler6, 30);
	public FixedLengthStringData allowperiod12 = new FixedLengthStringData(3).isAPartOf(filler6, 33); 


	public void initialize() {
		COBOLFunctions.initialize(t5543Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5543Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}