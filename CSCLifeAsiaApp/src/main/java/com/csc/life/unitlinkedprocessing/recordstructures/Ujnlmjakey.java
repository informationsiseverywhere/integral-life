package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:25
 * Description:
 * Copybook name: UJNLMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ujnlmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ujnlmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ujnlmjaKey = new FixedLengthStringData(64).isAPartOf(ujnlmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData ujnlmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(ujnlmjaKey, 0);
  	public FixedLengthStringData ujnlmjaChdrnum = new FixedLengthStringData(8).isAPartOf(ujnlmjaKey, 1);
  	public PackedDecimalData ujnlmjaSeqno = new PackedDecimalData(2, 0).isAPartOf(ujnlmjaKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(ujnlmjaKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ujnlmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ujnlmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}