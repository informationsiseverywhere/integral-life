package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R6230.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R6230Report extends SMARTReportLayout { 

	private ZonedDecimalData abidpr = new ZonedDecimalData(9, 5);
	private ZonedDecimalData abrepr = new ZonedDecimalData(9, 5);
	private FixedLengthStringData action = new FixedLengthStringData(1);
	private ZonedDecimalData aoffpr = new ZonedDecimalData(9, 5);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private FixedLengthStringData expiry = new FixedLengthStringData(8);
	private ZonedDecimalData ibidpr = new ZonedDecimalData(9, 5);
	private ZonedDecimalData ibrepr = new ZonedDecimalData(9, 5);
	private ZonedDecimalData ioffpr = new ZonedDecimalData(9, 5);
	private ZonedDecimalData jobno = new ZonedDecimalData(8, 0);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R6230Report() {
		super();
	}


	/**
	 * Print the XML for R6230d01
	 */
	public void printR6230d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		vrtfnd.setFieldName("vrtfnd");
		vrtfnd.setInternal(subString(recordData, 1, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 5, 30));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 35, 10));
		jobno.setFieldName("jobno");
		jobno.setInternal(subString(recordData, 45, 8));
		ibrepr.setFieldName("ibrepr");
		ibrepr.setInternal(subString(recordData, 53, 9));
		ibidpr.setFieldName("ibidpr");
		ibidpr.setInternal(subString(recordData, 62, 9));
		ioffpr.setFieldName("ioffpr");
		ioffpr.setInternal(subString(recordData, 71, 9));
		abrepr.setFieldName("abrepr");
		abrepr.setInternal(subString(recordData, 80, 9));
		abidpr.setFieldName("abidpr");
		abidpr.setInternal(subString(recordData, 89, 9));
		aoffpr.setFieldName("aoffpr");
		aoffpr.setInternal(subString(recordData, 98, 9));
		expiry.setFieldName("expiry");
		expiry.setInternal(subString(recordData, 107, 8));
		printLayout("R6230d01",			// Record name
			new BaseData[]{			// Fields:
				vrtfnd,
				longdesc,
				effdate,
				jobno,
				ibrepr,
				ibidpr,
				ioffpr,
				abrepr,
				abidpr,
				aoffpr,
				expiry
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R6230h01
	 */
	public void printR6230h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(10).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		action.setFieldName("action");
		action.setInternal(subString(recordData, 42, 1));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R6230h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				company,
				companynm,
				action,
				dateReportVariable,
				time,
				pagnbr
			}
			, new Object[] {			// indicators
				new Object[]{"ind10", indicArea.charAt(10)}
			}
		);

		currentPrintLine.set(13);
	}

	/**
	 * Print the XML for R6230t01
	 */
	public void printR6230t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R6230t01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}


}
