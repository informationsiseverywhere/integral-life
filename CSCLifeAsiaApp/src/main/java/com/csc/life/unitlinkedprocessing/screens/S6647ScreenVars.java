package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6647
 * @version 1.0 generated on 30/08/09 06:55
 * @author Quipoz
 */
public class S6647ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(397);
	public FixedLengthStringData dataFields = new FixedLengthStringData(93).isAPartOf(dataArea, 0);
	public FixedLengthStringData aloind = DD.aloind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bidoffer = DD.bidoffer.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData dealin = DD.dealin.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData efdcode = DD.efdcode.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData enhall = DD.enhall.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,10);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,18);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,26);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,34);
	public ZonedDecimalData monthsDebt = DD.mondet.copyToZonedDecimal().isAPartOf(dataFields,64);
	public ZonedDecimalData monthsError = DD.monerr.copyToZonedDecimal().isAPartOf(dataFields,67);
	public ZonedDecimalData monthsLapse = DD.monlap.copyToZonedDecimal().isAPartOf(dataFields,70);
	public ZonedDecimalData monthsNegUnits = DD.monneg.copyToZonedDecimal().isAPartOf(dataFields,73);
	public ZonedDecimalData procSeqNo = DD.prcseq.copyToZonedDecimal().isAPartOf(dataFields,76);
	public FixedLengthStringData unitStatMethod = DD.stmeth.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData swmeth = DD.swmeth.copy().isAPartOf(dataFields,83);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData moniesDate = DD.moniesDate.copy().isAPartOf(dataFields,92);//ILIFE-5462
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 93);
	public FixedLengthStringData aloindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bidofferErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dealinErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData efdcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData enhallErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData mondetErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData monerrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData monlapErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData monnegErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData prcseqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData stmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData swmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData moniesdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);//ILIFE-5462
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 169);
	public FixedLengthStringData[] aloindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bidofferOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dealinOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] efdcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] enhallOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] mondetOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] monerrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] monlapOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] monnegOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] prcseqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] stmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] swmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] moniesdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);//ILIFE-5462
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6647screenWritten = new LongData(0);
	public LongData S6647protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6647ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(swmethOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(enhallOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(efdcodeOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aloindOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dealinOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcseqOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stmethOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(monnegOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mondetOut,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(monlapOut,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(monerrOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bidofferOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, swmeth, enhall, efdcode, aloind, dealin, procSeqNo, unitStatMethod, monthsNegUnits, monthsDebt, monthsLapse, monthsError, bidoffer,moniesDate};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, swmethOut, enhallOut, efdcodeOut, aloindOut, dealinOut, prcseqOut, stmethOut, monnegOut, mondetOut, monlapOut, monerrOut, bidofferOut,moniesdtOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, swmethErr, enhallErr, efdcodeErr, aloindErr, dealinErr, prcseqErr, stmethErr, monnegErr, mondetErr, monlapErr, monerrErr, bidofferErr,moniesdtErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6647screen.class;
		protectRecord = S6647protect.class;
	}

}
