/*
 * File: P5102at.java
 * Date: 30 August 2009 0:07:39
 * Author: Quipoz Limited
 * 
 * Class transformed from P5102AT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS
*
*  P5102AT - Regular Withdrawals AT Program.
*  -----------------------------------------
*
*  Overview.
*  ---------
*
*  This module will be run  under  AT  and  it  will  call  the
*  breakout  routine  to  physically breakout the contract from
*  its existing summarised form.
*
*  It will only  be  called  for  the  Creation  of  a  regular
*  Withdrawal.
*
*
*  Processing.
*  -----------
*
*  Call  the  breakout  routine  using the Contract Number, old
*  Plan Suffix and new Plan Suffix passed in the AT parameters.
*
*  Call softlock to release the contract.
*
*
****************************************************************** *
* </pre>
*/
public class P5102at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5102AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(19);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private PackedDecimalData wsaaBrkoutPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTransArea, 16);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atmodrec atmodrec = new Atmodrec();

	public P5102at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialize1000();
		finish3000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialize1000()
	{
		/*PROC*/
		readContractHeader1050();
		callBreakout1300();
		/*EXIT*/
	}

protected void readContractHeader1050()
	{
		readContract1051();
	}

protected void readContract1051()
	{
		/*  Retrieve the contract header and store the transaction number*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		chdrrgpIO.setChdrcoy(atmodrec.company);
		chdrrgpIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrrgpIO.setDataArea(SPACES);
		chdrrgpIO.setChdrcoy(atmodrec.company);
		chdrrgpIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrrgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Call the BRKOUT subroutine to breakout COVR and AGCM        *
	* </pre>
	*/
protected void callBreakout1300()
	{
		go1321();
	}

protected void go1321()
	{
		/*  Call the breakout routine in order to breakout the COVR*/
		/*  and the AGCM records - this is done only once for the*/
		/*  first record read from the Surrender header file*/
		if ((isGT(wsaaBrkoutPlanSuffix, chdrrgpIO.getPolsum()))
		|| (isEQ(chdrrgpIO.getPolsum(), 1))
		|| (isEQ(wsaaBrkoutPlanSuffix, 0))) {
			return ;
		}
		brkoutrec.brkOldSummary.set(chdrrgpIO.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaBrkoutPlanSuffix, 1));
		brkoutrec.brkChdrnum.set(chdrrgpIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrrgpIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(SPACES);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
			syserrrec.params.set(brkoutrec.brkStatuz);
			xxxxFatalError();
		}
	}

protected void finish3000()
	{
		/*GO*/
		releaseSoftlock3600();
		/*EXIT*/
	}

protected void releaseSoftlock3600()
	{
		releaseSoftlock3601();
	}

protected void releaseSoftlock3601()
	{
		/* Release Soft locked record.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}
}
