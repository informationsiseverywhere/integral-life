/*
 * File: P6519.java
 * Date: 30 August 2009 0:46:37
 * Author: Quipoz Limited
 * 
 * Class transformed from P6519.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.enquiries.dataaccess.AgntenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.unitlinkedprocessing.dataaccess.ChdrulsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstmTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S6519ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            UNIT STATEMENT PRINT REQUEST
*
* Initialise
* ----------
*
*
*     The details of the contract being dislayed will be  stored in
*     the CHDRULS I/O module.  Retrieve the details.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist.
*
*
*          The agent (AGNT) to get the client number, and hence the
*          client (agent's) name.
*
*     Read the USTF  data-set  with  a  key  of  Contract  Company,
*     Contract Number and  Unit  Statement  Number  set ot all 9's.
*     This  will  return  the  most  recent  USTF  record  for  the
*     contract.  Display   the   Unit  Statement  Number  and  Last
*     Statement Date on the  screen.  If  no  USTF record was found
*     matching  the  Contract  Company  and  Contract  Number  then
*     display zero and blanks in the appropriate screen fields.
*
*     Set the  statement  level  field  on  the  screen  to 'S' for
*     Summary and set the Statement Date field to today's date.
*
* Validation
* ----------
*
*
*     If  the  'KILL'   function  key  was  pressed  skip  all  the
*     validation.
*
*     Check that  the  Statement  Date  is  greater  then  the last
*     Statement  Date   and   not   less  than  the  contract  risk
*     commencement date.
*
*     The Statement Level  field may only be 'S' for Summary or 'D'
*     for Detail.
*
* Updating
* --------
*
*     If "KILL"  was not selected then create a USTM Trigger record
*     with  all  the  relevant information, including the statement
*     level.
*
*
* Next Program
* ------------
*
*     Add 1 to the current program pointer and exit.
*
*
* Notes.
* ------
*
*
*     Create a new view  of  CHDRPF  called CHDRULS which uses only
*     those fields required for this program.
*
*     Table Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
*
*****************************************************************
* </pre>
*/
public class P6519 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6519");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* WSAA-MISCELLANEOUS */
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanProc = new FixedLengthStringData(1);
	private Validator plan = new Validator(wsaaPlanProc, "Y");
	private Validator nonplan = new Validator(wsaaPlanProc, "N");

		/*Fields for use with the CLRF logical.                            */
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private static final String ustmrec = "USTMREC";
	private static final String ustfrec = "USTFREC";
	private static final String clrfrec = "CLRFREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
		/* ERRORS */
	private static final String e848 = "E848";
	private static final String g425 = "G425";
	private static final String h359 = "H359";
	private AgntenqTableDAM agntenqIO = new AgntenqTableDAM();
	private ChdrulsTableDAM chdrulsIO = new ChdrulsTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private UstfTableDAM ustfIO = new UstfTableDAM();
	private UstmTableDAM ustmIO = new UstmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	private S6519ScreenVars sv = ScreenProgram.getScreenVars( S6519ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		jointOwnerName1030, 
		payerName1040, 
		agentName1050
	}

	public P6519() {
		super();
		screenVars = sv;
		new ScreenModel("S6519", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
					ownerName1020();
				case jointOwnerName1030: 
					jointOwnerName1030();
				case payerName1040: 
					payerName1040();
				case agentName1050: 
					agentName1050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		wsaaToday.set(0);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.numpols.set(ZERO);
		sv.ustmno.set(ZERO);
		sv.stmtdate.set(wsaaToday);
		sv.stmtlevel.set("S");
		sv.lststmdte.set(varcom.vrcmMaxDate);
		/* Set screen fields*/
		/* Read CHDRULS (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrulsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrulsIO);
		if (isNE(chdrulsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrulsIO.getParams());
			fatalError600();
		}
		/*    If the 'Number of Policies in Plan' field on the Contract*/
		/*    Header is not zeros then Plan Processing is applicable.*/
		if (isEQ(chdrulsIO.getPolinc(), ZERO)
		|| isEQ(chdrulsIO.getPolinc(), 1)) {
			wsaaPlanProc.set("N");
		}
		else {
			wsaaPlanProc.set("Y");
		}
		/*    If Plan Processing is applicable, protect and non-display*/
		/*    the Plan Processing fields (number available, number*/
		/*    applicable and total number of policies in plan).*/
		if (plan.isTrue()) {
			sv.numpols.set(chdrulsIO.getPolinc());
		}
		else {
			sv.numpolsOut[varcom.pr.toInt()].set("Y");
			sv.numpolsOut[varcom.nd.toInt()].set("Y");
		}
		sv.chdrnum.set(chdrulsIO.getChdrnum());
		sv.cnttype.set(chdrulsIO.getCnttype());
		sv.cntcurr.set(chdrulsIO.getCntcurr());
		sv.register.set(chdrulsIO.getRegister());
		sv.cownnum.set(chdrulsIO.getCownnum());
		if (isNE(chdrulsIO.getJownnum(), SPACES)) {
			sv.jowner.set(chdrulsIO.getJownnum());
		}
		/*  IF   CHDRULS-PAYRNUM        NOT = SPACES*/
		/*       MOVE CHDRULS-PAYRNUM   TO S6519-PAYER.*/
		sv.servagnt.set(chdrulsIO.getAgntnum());
		sv.servbr.set(chdrulsIO.getCntbranch());
		sv.currfrom.set(chdrulsIO.getOccdate());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrulsIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrulsIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set("NONE");
			sv.jlifename.set("NONE");
		}
		else {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

	/**
	* <pre>
	*    Obtain the names of the other lives on the contract header.
	* </pre>
	*/
protected void ownerName1020()
	{
		if (isEQ(sv.cownnum, SPACES)) {
			goTo(GotoLabel.jointOwnerName1030);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
	}

protected void jointOwnerName1030()
	{
		if (isEQ(sv.jowner, SPACES)) {
			goTo(GotoLabel.payerName1040);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.jowner);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.jownername.set(wsspcomn.longconfname);
	}

protected void payerName1040()
	{
		/* Read the Client Role file CLRF logical to find details of    */
		/* the Payer.                                                   */
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(chdrulsIO.getChdrcoy());
		wsaaChdrnum.set(chdrulsIO.getChdrnum());
		wsaaPayrseqno.set("1");
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		clrfIO.setFormat(clrfrec);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)
		&& isNE(clrfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clrfIO.getStatuz(), varcom.mrnf)) {
			sv.payer.set(SPACES);
		}
		else {
			sv.payer.set(clrfIO.getClntnum());
		}
		if (isEQ(sv.payer, SPACES)) {
			goTo(GotoLabel.agentName1050);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.payer);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.payername.set(wsspcomn.longconfname);
	}

protected void agentName1050()
	{
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(chdrulsIO.getAgntcoy());
		agntenqIO.setAgntnum(chdrulsIO.getAgntnum());
		agntenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(), varcom.oK)
		&& isNE(agntenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.servagnam.set(wsspcomn.longconfname);
		/*    Obtain the Branch name from T1692.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrulsIO.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.brchname.fill("?");
		}
		else {
			sv.brchname.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrulsIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrulsIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrulsIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Latest statement number for the contract from     */
		/*    the previous triger record.                                  */
		ustfIO.setDataArea(SPACES);
		ustfIO.setChdrcoy(chdrulsIO.getChdrcoy());
		ustfIO.setChdrnum(chdrulsIO.getChdrnum());
		ustfIO.setUstmno(99999);
		ustfIO.setPlanSuffix(ZERO);
		ustfIO.setLife("01");
		ustfIO.setCoverage("01");
		ustfIO.setRider("00");
		ustfIO.setFunction(varcom.begn);
		ustfIO.setFormat(ustfrec);
		SmartFileCode.execute(appVars, ustfIO);
		if (isNE(ustfIO.getStatuz(), varcom.oK)
		&& isNE(ustfIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ustfIO.getParams());
			fatalError600();
		}
		if (isEQ(ustfIO.getStatuz(), varcom.endp)
		|| isNE(ustfIO.getChdrcoy(), chdrulsIO.getChdrcoy())
		|| isNE(ustfIO.getChdrnum(), chdrulsIO.getChdrnum())) {
			/*     MOVE ZERO              TO USTF-USTMNO              <002>*/
			/*                               USTF-STMT-CL-DATE.       <002>*/
			ustfIO.setUstmno(ZERO);
			ustfIO.setStmtClDate(varcom.vrcmMaxDate);
		}
		sv.ustmno.set(ustfIO.getUstmno());
		sv.lststmdte.set(ustfIO.getStmtClDate());
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2001();
	}

protected void screenIo2001()
	{
		/*    CALL 'S6519IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6519-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*IF   S6519-STMTDATE      NOT > S6519-LSTSTMDTE               */
		if (isLTE(sv.stmtdate, sv.lststmdte)
		&& isNE(sv.lststmdte, varcom.vrcmMaxDate)) {
			sv.stmtdateErr.set(g425);
			wsspcomn.edterror.set("Y");
		}
		if (isLT(sv.stmtdate, sv.currfrom)) {
			sv.stmtdateErr.set(h359);
			wsspcomn.edterror.set("Y");
		}
		/*    The Statement Level Field amy be either 'S' or 'D'.*/
		if (isEQ(sv.stmtlevel, "S")
		|| isEQ(sv.stmtlevel, "D")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.stmtlevelErr.set(e848);
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		para3200();
	}

protected void para3200()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		ustmIO.setDataArea(SPACES);
		ustmIO.setChdrcoy(chdrulsIO.getChdrcoy());
		ustmIO.setChdrnum(chdrulsIO.getChdrnum());
		ustmIO.setEffdate(sv.stmtdate);
		ustmIO.setStmtType("P");
		/*    The statement may be at either Detail, (policy) level or at*/
		/*    Summary, (plan) level.*/
		ustmIO.setUnitStmtFlag(sv.stmtlevel);
		/*    MOVE ZERO                   TO USTM-STRPDATE                 */
		/*                                   USTM-USTMNO                   */
		/*                                   USTM-TRANNO                   */
		/*                                   USTM-JOBNO-STMT.              */
		ustmIO.setTranno(ZERO);
		ustmIO.setJobnoStmt(ZERO);
		ustmIO.setUstmno(sv.ustmno);
		ustmIO.setStrpdate(sv.lststmdte);
		wsaaBatckey.set(wsspcomn.batchkey);
		ustmIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ustmIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ustmIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ustmIO.setFormat(ustmrec);
		ustmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ustmIO);
		if (isNE(ustmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ustmIO.getParams());
			/*      GO TO 600-FATAL-ERROR.                                  */
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
