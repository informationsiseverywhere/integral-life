package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.life.newbusiness.dataaccess.CovrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovrrgwTableDAM.java
 * Date: Sun, 30 Aug 2009 03:35:20
 * Class transformed from COVRRGW.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovrrgwTableDAM extends CovrpfTableDAM {

	public CovrrgwTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("COVRRGW");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "JLIFE, " +
		            "STATCODE, " +
		            "PSTATCODE, " +
		            "STATREASN, " +
		            "CRRCD, " +
		            "ANBCCD, " +
		            "SEX, " +
		            "PRMCUR, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "STFUND, " +
		            "STSECT, " +
		            "CBCVIN, " +
		            "STSSECT, " +
		            "CRTABLE, " +
		            "RCESDTE, " +
		            "PCESDTE, " +
		            "BCESDTE, " +
		            "NXTDTE, " +
		            "SUMINS, " +
		            "SICURR, " +
		            "VARSI, " +
		            "MORTCLS, " +
		            "LIENCD, " +
		            "RATCLS, " +
		            "INDXIN, " +
		            "BNUSIN, " +
		            "DPCD, " +
		            "DPAMT, " +
		            "DPIND, " +
		            "TMBEN, " +
		            "SINGP, " +
		            "INSTPREM, " +
		            "CAMPAIGN, " +
		            "STSMIN, " +
		            "RTRNYRS, " +
		            "PCAMTH, " +
		            "PCADAY, " +
		            "PCTMTH, " +
		            "PCTDAY, " +
		            "RCAMTH, " +
		            "RCADAY, " +
		            "RCTMTH, " +
		            "RCTDAY, " +
		            "JLLSID, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "ZSTPDUTY01, " +
			      	"ZCLSTATE, " + 
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               validflag,
                               tranno,
                               currfrom,
                               currto,
                               jlife,
                               statcode,
                               pstatcode,
                               statreasn,
                               crrcd,
                               anbAtCcd,
                               sex,
                               premCurrency,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               statFund,
                               statSect,
                               convertInitialUnits,
                               statSubsect,
                               crtable,
                               riskCessDate,
                               premCessDate,
                               benCessDate,
                               nextActDate,
                               sumins,
                               sicurr,
                               varSumInsured,
                               mortcls,
                               liencd,
                               ratingClass,
                               indexationInd,
                               bonusInd,
                               deferPerdCode,
                               deferPerdAmt,
                               deferPerdInd,
                               totMthlyBenefit,
                               singp,
                               instprem,
                               campaign,
                               statSumins,
                               rtrnyrs,
                               premCessAgeMth,
                               premCessAgeDay,
                               premCessTermMth,
                               premCessTermDay,
                               riskCessAgeMth,
                               riskCessAgeDay,
                               riskCessTermMth,
                               riskCessTermDay,
                               jlLsInd,
                               userProfile,
                               jobName,
                               datime,
                               zstpduty01,
                               zclstate,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(270);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getJlife().toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ getStatreasn().toInternal()
					+ getCrrcd().toInternal()
					+ getAnbAtCcd().toInternal()
					+ getSex().toInternal()
					+ getPremCurrency().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getConvertInitialUnits().toInternal()
					+ getStatSubsect().toInternal()
					+ getCrtable().toInternal()
					+ getRiskCessDate().toInternal()
					+ getPremCessDate().toInternal()
					+ getBenCessDate().toInternal()
					+ getNextActDate().toInternal()
					+ getSumins().toInternal()
					+ getSicurr().toInternal()
					+ getVarSumInsured().toInternal()
					+ getMortcls().toInternal()
					+ getLiencd().toInternal()
					+ getRatingClass().toInternal()
					+ getIndexationInd().toInternal()
					+ getBonusInd().toInternal()
					+ getDeferPerdCode().toInternal()
					+ getDeferPerdAmt().toInternal()
					+ getDeferPerdInd().toInternal()
					+ getTotMthlyBenefit().toInternal()
					+ getSingp().toInternal()
					+ getInstprem().toInternal()
					+ getCampaign().toInternal()
					+ getStatSumins().toInternal()
					+ getRtrnyrs().toInternal()
					+ getPremCessAgeMth().toInternal()
					+ getPremCessAgeDay().toInternal()
					+ getPremCessTermMth().toInternal()
					+ getPremCessTermDay().toInternal()
					+ getRiskCessAgeMth().toInternal()
					+ getRiskCessAgeDay().toInternal()
					+ getRiskCessTermMth().toInternal()
					+ getRiskCessTermDay().toInternal()
					+ getJlLsInd().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getZstpduty01().toString()
			        + getZclstate().toString());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, statreasn);
			what = ExternalData.chop(what, crrcd);
			what = ExternalData.chop(what, anbAtCcd);
			what = ExternalData.chop(what, sex);
			what = ExternalData.chop(what, premCurrency);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, convertInitialUnits);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, riskCessDate);
			what = ExternalData.chop(what, premCessDate);
			what = ExternalData.chop(what, benCessDate);
			what = ExternalData.chop(what, nextActDate);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, sicurr);
			what = ExternalData.chop(what, varSumInsured);
			what = ExternalData.chop(what, mortcls);
			what = ExternalData.chop(what, liencd);
			what = ExternalData.chop(what, ratingClass);
			what = ExternalData.chop(what, indexationInd);
			what = ExternalData.chop(what, bonusInd);
			what = ExternalData.chop(what, deferPerdCode);
			what = ExternalData.chop(what, deferPerdAmt);
			what = ExternalData.chop(what, deferPerdInd);
			what = ExternalData.chop(what, totMthlyBenefit);
			what = ExternalData.chop(what, singp);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, campaign);
			what = ExternalData.chop(what, statSumins);
			what = ExternalData.chop(what, rtrnyrs);
			what = ExternalData.chop(what, premCessAgeMth);
			what = ExternalData.chop(what, premCessAgeDay);
			what = ExternalData.chop(what, premCessTermMth);
			what = ExternalData.chop(what, premCessTermDay);
			what = ExternalData.chop(what, riskCessAgeMth);
			what = ExternalData.chop(what, riskCessAgeDay);
			what = ExternalData.chop(what, riskCessTermMth);
			what = ExternalData.chop(what, riskCessTermDay);
			what = ExternalData.chop(what, jlLsInd);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, zstpduty01);
			what = ExternalData.chop(what, zclstate);
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getStatreasn() {
		return statreasn;
	}
	public void setStatreasn(Object what) {
		statreasn.set(what);
	}	
	public PackedDecimalData getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Object what) {
		setCrrcd(what, false);
	}
	public void setCrrcd(Object what, boolean rounded) {
		if (rounded)
			crrcd.setRounded(what);
		else
			crrcd.set(what);
	}	
	public PackedDecimalData getAnbAtCcd() {
		return anbAtCcd;
	}
	public void setAnbAtCcd(Object what) {
		setAnbAtCcd(what, false);
	}
	public void setAnbAtCcd(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd.setRounded(what);
		else
			anbAtCcd.set(what);
	}	
	public FixedLengthStringData getSex() {
		return sex;
	}
	public void setSex(Object what) {
		sex.set(what);
	}	
	public FixedLengthStringData getPremCurrency() {
		return premCurrency;
	}
	public void setPremCurrency(Object what) {
		premCurrency.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}	
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}	
	public PackedDecimalData getConvertInitialUnits() {
		return convertInitialUnits;
	}
	public void setConvertInitialUnits(Object what) {
		setConvertInitialUnits(what, false);
	}
	public void setConvertInitialUnits(Object what, boolean rounded) {
		if (rounded)
			convertInitialUnits.setRounded(what);
		else
			convertInitialUnits.set(what);
	}	
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getRiskCessDate() {
		return riskCessDate;
	}
	public void setRiskCessDate(Object what) {
		setRiskCessDate(what, false);
	}
	public void setRiskCessDate(Object what, boolean rounded) {
		if (rounded)
			riskCessDate.setRounded(what);
		else
			riskCessDate.set(what);
	}	
	public PackedDecimalData getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(Object what) {
		setPremCessDate(what, false);
	}
	public void setPremCessDate(Object what, boolean rounded) {
		if (rounded)
			premCessDate.setRounded(what);
		else
			premCessDate.set(what);
	}	
	public PackedDecimalData getBenCessDate() {
		return benCessDate;
	}
	public void setBenCessDate(Object what) {
		setBenCessDate(what, false);
	}
	public void setBenCessDate(Object what, boolean rounded) {
		if (rounded)
			benCessDate.setRounded(what);
		else
			benCessDate.set(what);
	}	
	public PackedDecimalData getNextActDate() {
		return nextActDate;
	}
	public void setNextActDate(Object what) {
		setNextActDate(what, false);
	}
	public void setNextActDate(Object what, boolean rounded) {
		if (rounded)
			nextActDate.setRounded(what);
		else
			nextActDate.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	public FixedLengthStringData getSicurr() {
		return sicurr;
	}
	public void setSicurr(Object what) {
		sicurr.set(what);
	}	
	public PackedDecimalData getVarSumInsured() {
		return varSumInsured;
	}
	public void setVarSumInsured(Object what) {
		setVarSumInsured(what, false);
	}
	public void setVarSumInsured(Object what, boolean rounded) {
		if (rounded)
			varSumInsured.setRounded(what);
		else
			varSumInsured.set(what);
	}	
	public FixedLengthStringData getMortcls() {
		return mortcls;
	}
	public void setMortcls(Object what) {
		mortcls.set(what);
	}	
	public FixedLengthStringData getLiencd() {
		return liencd;
	}
	public void setLiencd(Object what) {
		liencd.set(what);
	}	
	public FixedLengthStringData getRatingClass() {
		return ratingClass;
	}
	public void setRatingClass(Object what) {
		ratingClass.set(what);
	}	
	public FixedLengthStringData getIndexationInd() {
		return indexationInd;
	}
	public void setIndexationInd(Object what) {
		indexationInd.set(what);
	}	
	public FixedLengthStringData getBonusInd() {
		return bonusInd;
	}
	public void setBonusInd(Object what) {
		bonusInd.set(what);
	}	
	public FixedLengthStringData getDeferPerdCode() {
		return deferPerdCode;
	}
	public void setDeferPerdCode(Object what) {
		deferPerdCode.set(what);
	}	
	public PackedDecimalData getDeferPerdAmt() {
		return deferPerdAmt;
	}
	public void setDeferPerdAmt(Object what) {
		setDeferPerdAmt(what, false);
	}
	public void setDeferPerdAmt(Object what, boolean rounded) {
		if (rounded)
			deferPerdAmt.setRounded(what);
		else
			deferPerdAmt.set(what);
	}	
	public FixedLengthStringData getDeferPerdInd() {
		return deferPerdInd;
	}
	public void setDeferPerdInd(Object what) {
		deferPerdInd.set(what);
	}	
	public PackedDecimalData getTotMthlyBenefit() {
		return totMthlyBenefit;
	}
	public void setTotMthlyBenefit(Object what) {
		setTotMthlyBenefit(what, false);
	}
	public void setTotMthlyBenefit(Object what, boolean rounded) {
		if (rounded)
			totMthlyBenefit.setRounded(what);
		else
			totMthlyBenefit.set(what);
	}	
	public PackedDecimalData getSingp() {
		return singp;
	}
	public void setSingp(Object what) {
		setSingp(what, false);
	}
	public void setSingp(Object what, boolean rounded) {
		if (rounded)
			singp.setRounded(what);
		else
			singp.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public FixedLengthStringData getCampaign() {
		return campaign;
	}
	public void setCampaign(Object what) {
		campaign.set(what);
	}	
	public PackedDecimalData getStatSumins() {
		return statSumins;
	}
	public void setStatSumins(Object what) {
		setStatSumins(what, false);
	}
	public void setStatSumins(Object what, boolean rounded) {
		if (rounded)
			statSumins.setRounded(what);
		else
			statSumins.set(what);
	}	
	public PackedDecimalData getRtrnyrs() {
		return rtrnyrs;
	}
	public void setRtrnyrs(Object what) {
		setRtrnyrs(what, false);
	}
	public void setRtrnyrs(Object what, boolean rounded) {
		if (rounded)
			rtrnyrs.setRounded(what);
		else
			rtrnyrs.set(what);
	}	
	public PackedDecimalData getPremCessAgeMth() {
		return premCessAgeMth;
	}
	public void setPremCessAgeMth(Object what) {
		setPremCessAgeMth(what, false);
	}
	public void setPremCessAgeMth(Object what, boolean rounded) {
		if (rounded)
			premCessAgeMth.setRounded(what);
		else
			premCessAgeMth.set(what);
	}	
	public PackedDecimalData getPremCessAgeDay() {
		return premCessAgeDay;
	}
	public void setPremCessAgeDay(Object what) {
		setPremCessAgeDay(what, false);
	}
	public void setPremCessAgeDay(Object what, boolean rounded) {
		if (rounded)
			premCessAgeDay.setRounded(what);
		else
			premCessAgeDay.set(what);
	}	
	public PackedDecimalData getPremCessTermMth() {
		return premCessTermMth;
	}
	public void setPremCessTermMth(Object what) {
		setPremCessTermMth(what, false);
	}
	public void setPremCessTermMth(Object what, boolean rounded) {
		if (rounded)
			premCessTermMth.setRounded(what);
		else
			premCessTermMth.set(what);
	}	
	public PackedDecimalData getPremCessTermDay() {
		return premCessTermDay;
	}
	public void setPremCessTermDay(Object what) {
		setPremCessTermDay(what, false);
	}
	public void setPremCessTermDay(Object what, boolean rounded) {
		if (rounded)
			premCessTermDay.setRounded(what);
		else
			premCessTermDay.set(what);
	}	
	public PackedDecimalData getRiskCessAgeMth() {
		return riskCessAgeMth;
	}
	public void setRiskCessAgeMth(Object what) {
		setRiskCessAgeMth(what, false);
	}
	public void setRiskCessAgeMth(Object what, boolean rounded) {
		if (rounded)
			riskCessAgeMth.setRounded(what);
		else
			riskCessAgeMth.set(what);
	}	
	public PackedDecimalData getRiskCessAgeDay() {
		return riskCessAgeDay;
	}
	public void setRiskCessAgeDay(Object what) {
		setRiskCessAgeDay(what, false);
	}
	public void setRiskCessAgeDay(Object what, boolean rounded) {
		if (rounded)
			riskCessAgeDay.setRounded(what);
		else
			riskCessAgeDay.set(what);
	}	
	public PackedDecimalData getRiskCessTermMth() {
		return riskCessTermMth;
	}
	public void setRiskCessTermMth(Object what) {
		setRiskCessTermMth(what, false);
	}
	public void setRiskCessTermMth(Object what, boolean rounded) {
		if (rounded)
			riskCessTermMth.setRounded(what);
		else
			riskCessTermMth.set(what);
	}	
	public PackedDecimalData getRiskCessTermDay() {
		return riskCessTermDay;
	}
	public void setRiskCessTermDay(Object what) {
		setRiskCessTermDay(what, false);
	}
	public void setRiskCessTermDay(Object what, boolean rounded) {
		if (rounded)
			riskCessTermDay.setRounded(what);
		else
			riskCessTermDay.set(what);
	}	
	public FixedLengthStringData getJlLsInd() {
		return jlLsInd;
	}
	public void setJlLsInd(Object what) {
		jlLsInd.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getZstpduty01() {
		return zstpduty01;
	}
	public void setZstpduty01 (Object zstpduty01) {
		this.zstpduty01.set(zstpduty01);
	}
	public FixedLengthStringData getZclstate() {
		return zclstate;
	}
	public void setZclstate(Object what) {
		this.zclstate.set(what);
	}
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		validflag.clear();
		tranno.clear();
		currfrom.clear();
		currto.clear();
		jlife.clear();
		statcode.clear();
		pstatcode.clear();
		statreasn.clear();
		crrcd.clear();
		anbAtCcd.clear();
		sex.clear();
		premCurrency.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		statFund.clear();
		statSect.clear();
		convertInitialUnits.clear();
		statSubsect.clear();
		crtable.clear();
		riskCessDate.clear();
		premCessDate.clear();
		benCessDate.clear();
		nextActDate.clear();
		sumins.clear();
		sicurr.clear();
		varSumInsured.clear();
		mortcls.clear();
		liencd.clear();
		ratingClass.clear();
		indexationInd.clear();
		bonusInd.clear();
		deferPerdCode.clear();
		deferPerdAmt.clear();
		deferPerdInd.clear();
		totMthlyBenefit.clear();
		singp.clear();
		instprem.clear();
		campaign.clear();
		statSumins.clear();
		rtrnyrs.clear();
		premCessAgeMth.clear();
		premCessAgeDay.clear();
		premCessTermMth.clear();
		premCessTermDay.clear();
		riskCessAgeMth.clear();
		riskCessAgeDay.clear();
		riskCessTermMth.clear();
		riskCessTermDay.clear();
		jlLsInd.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}