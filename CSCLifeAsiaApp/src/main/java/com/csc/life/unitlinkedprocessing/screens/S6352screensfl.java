package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6352screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 10;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {30, 32, 31, 29, 5, 1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 21, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6352ScreenVars sv = (S6352ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6352screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6352screensfl, 
			sv.S6352screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6352ScreenVars sv = (S6352ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6352screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6352ScreenVars sv = (S6352ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6352screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6352screensflWritten.gt(0))
		{
			sv.s6352screensfl.setCurrentIndex(0);
			sv.S6352screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6352ScreenVars sv = (S6352ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6352screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6352ScreenVars screenVars = (S6352ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.unitType.setFieldName("unitType");
				screenVars.nofUnits.setFieldName("nofUnits");
				screenVars.nofDunits.setFieldName("nofDunits");
				screenVars.currentUnitBal.setFieldName("currentUnitBal");
				screenVars.currentDunitBal.setFieldName("currentDunitBal");
				screenVars.hseqno.setFieldName("hseqno");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.updteflag.set(dm.getField("updteflag"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.unitType.set(dm.getField("unitType"));
			screenVars.nofUnits.set(dm.getField("nofUnits"));
			screenVars.nofDunits.set(dm.getField("nofDunits"));
			screenVars.currentUnitBal.set(dm.getField("currentUnitBal"));
			screenVars.currentDunitBal.set(dm.getField("currentDunitBal"));
			screenVars.hseqno.set(dm.getField("hseqno"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6352ScreenVars screenVars = (S6352ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.unitType.setFieldName("unitType");
				screenVars.nofUnits.setFieldName("nofUnits");
				screenVars.nofDunits.setFieldName("nofDunits");
				screenVars.currentUnitBal.setFieldName("currentUnitBal");
				screenVars.currentDunitBal.setFieldName("currentDunitBal");
				screenVars.hseqno.setFieldName("hseqno");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("updteflag").set(screenVars.updteflag);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("unitType").set(screenVars.unitType);
			dm.getField("nofUnits").set(screenVars.nofUnits);
			dm.getField("nofDunits").set(screenVars.nofDunits);
			dm.getField("currentUnitBal").set(screenVars.currentUnitBal);
			dm.getField("currentDunitBal").set(screenVars.currentDunitBal);
			dm.getField("hseqno").set(screenVars.hseqno);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6352screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6352ScreenVars screenVars = (S6352ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.updteflag.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.unitType.clearFormatting();
		screenVars.nofUnits.clearFormatting();
		screenVars.nofDunits.clearFormatting();
		screenVars.currentUnitBal.clearFormatting();
		screenVars.currentDunitBal.clearFormatting();
		screenVars.hseqno.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6352ScreenVars screenVars = (S6352ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.updteflag.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.unitType.setClassString("");
		screenVars.nofUnits.setClassString("");
		screenVars.nofDunits.setClassString("");
		screenVars.currentUnitBal.setClassString("");
		screenVars.currentDunitBal.setClassString("");
		screenVars.hseqno.setClassString("");
	}

/**
 * Clear all the variables in S6352screensfl
 */
	public static void clear(VarModel pv) {
		S6352ScreenVars screenVars = (S6352ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.updteflag.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.unitType.clear();
		screenVars.nofUnits.clear();
		screenVars.nofDunits.clear();
		screenVars.currentUnitBal.clear();
		screenVars.currentDunitBal.clear();
		screenVars.hseqno.clear();
	}
}
