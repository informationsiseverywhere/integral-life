/*
 * File: Unlsc02.java
 * Date: 30 August 2009 2:51:56
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLSC02.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6656rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Full Surrender Charge/Penalty Calculation Method #2.
* ----------------------------------------------------
*
* PROCESSING.
* ----------
*   Look-up the  "Surrender Value Penalty (SVP)"  factor table  T6656,
*   keyed on  surrender charge  method plus  component premium status.
*   We use the 'Nth' factor on  table T6656  which has  50 occurences.
*   The occurence we use is the age of policy (RCD till Surrender Eff.
*   Date). The  factor obtained  is then divided  by the  value in the
*   divisor field to give a calculated factor. This new factor is used
*   in the charge/penalty calculations.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Unlsc02 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLSC02";
	private PackedDecimalData wsaaEstimateTotAmt = new PackedDecimalData(18, 5).init(0);

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, 1);
	private Validator partPlan = new Validator(wsaaPlanSwitch, 2);
	private Validator summaryPart = new Validator(wsaaPlanSwitch, 3);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(13, 9);

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(1).isAPartOf(wsaaT5551Item, 4);
	private FixedLengthStringData wsaaT5551Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);
	private ZonedDecimalData wsaaSvpFactor = new ZonedDecimalData(14, 9);
	private ZonedDecimalData wsaaSrYears = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaNoT6656Entry = new FixedLengthStringData(1);
	private Validator noT6656Entry = new Validator(wsaaNoT6656Entry, "Y");

	private FixedLengthStringData wsaaT6656Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6656Meth = new FixedLengthStringData(4).isAPartOf(wsaaT6656Key, 0);
	private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(4).isAPartOf(wsaaT6656Key, 4);
		/* TABLES */
	private static final String t5551 = "T5551";
	private static final String t6656 = "T6656";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5551rec t5551rec = new T5551rec();
	private T6656rec t6656rec = new T6656rec();
	private Srcalcpy srcalcpy = new Srcalcpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readT5551400
	}

	public Unlsc02() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		initialzation100();
		surrChargeCalc200();
		/*EXIT*/
		exitProgram();
	}

protected void initialzation100()
	{
		/*INIT*/
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaPlanSwitch.set(srcalcpy.type);
		wsaaEstimateTotAmt.set(srcalcpy.estimatedVal);
		wsaaSvpFactor.set(ZERO);
		wsaaSrYears.set(ZERO);
		/*EXIT*/
	}

protected void surrChargeCalc200()
	{
			para200();
		}

protected void para200()
	{
		readTableT5551400();
		readTableT6656500();
		if (noT6656Entry.isTrue()) {
			srcalcpy.actualVal.set(ZERO);
			return ;
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		wsaaSrYears.set(datcon3rec.freqFactor);
		wsaaSrYears.add(1);
		if (isLT(wsaaSrYears,1)) {
			wsaaSrYears.set(1);
		}
		/* IF  WSAA-SR-YEARS           > 50                             */
		if (isGT(wsaaSrYears, 49)) {
			wsaaSrYears.set(50);
		}
		wsaaSvpFactor.set(t6656rec.svpFactor[wsaaSrYears.toInt()]);
		compute(wsaaFactor, 9).set(div(wsaaSvpFactor,t6656rec.divisor));
		compute(srcalcpy.actualVal, 10).setRounded(mult(wsaaEstimateTotAmt,wsaaFactor));
		if (isLT(wsaaEstimateTotAmt,srcalcpy.actualVal)) {
			srcalcpy.actualVal.set(wsaaEstimateTotAmt);
		}
	}

protected void readTableT5551400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initLang400();
				case readT5551400: 
					readT5551400();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initLang400()
	{
		wsaaT5551Key.set(srcalcpy.language);
	}

protected void readT5551400()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5551);
		wsaaT5551Crtable.set(srcalcpy.crtable);
		wsaaT5551Currcode.set(srcalcpy.currcode);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setItemitem(wsaaT5551Item);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemitem(),wsaaT5551Item)
		&& isEQ(itdmIO.getStatuz(),varcom.oK)) {
			wsaaT5551Key.set("*");
			goTo(GotoLabel.readT5551400);
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5551)
		|| isNE(itdmIO.getItemitem(),wsaaT5551Item)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableT6656500()
	{
		para500();
	}

protected void para500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6656);
		wsaaT6656Meth.set(t5551rec.svcmeth);
		wsaaPremStat.set(srcalcpy.pstatcode);
		itdmIO.setItemitem(wsaaT6656Key);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6656)
		|| isNE(itdmIO.getItemitem(),wsaaT6656Key)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			wsaaNoT6656Entry.set("Y");
		}
		else {
			wsaaNoT6656Entry.set(SPACES);
			t6656rec.t6656Rec.set(itdmIO.getGenarea());
		}
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		srcalcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
