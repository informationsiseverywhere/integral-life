package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:23
 * Description:
 * Copybook name: CHDRTRNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrtrnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrtrnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrtrnKey = new FixedLengthStringData(64).isAPartOf(chdrtrnFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrtrnChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrtrnKey, 0);
  	public FixedLengthStringData chdrtrnChdrnum = new FixedLengthStringData(8).isAPartOf(chdrtrnKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrtrnKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrtrnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrtrnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}