package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:43
 * Description:
 * Copybook name: USTFBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustfbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustfbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustfbrkKey = new FixedLengthStringData(64).isAPartOf(ustfbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData ustfbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(ustfbrkKey, 0);
  	public FixedLengthStringData ustfbrkChdrnum = new FixedLengthStringData(8).isAPartOf(ustfbrkKey, 1);
  	public FixedLengthStringData ustfbrkCoverage = new FixedLengthStringData(2).isAPartOf(ustfbrkKey, 9);
  	public FixedLengthStringData ustfbrkRider = new FixedLengthStringData(2).isAPartOf(ustfbrkKey, 11);
  	public PackedDecimalData ustfbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ustfbrkKey, 13);
  	public PackedDecimalData ustfbrkUstmno = new PackedDecimalData(5, 0).isAPartOf(ustfbrkKey, 16);
  	public FixedLengthStringData ustfbrkLife = new FixedLengthStringData(2).isAPartOf(ustfbrkKey, 19);
  	public FixedLengthStringData ustfbrkUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(ustfbrkKey, 21);
  	public FixedLengthStringData ustfbrkUnitType = new FixedLengthStringData(1).isAPartOf(ustfbrkKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(ustfbrkKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustfbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustfbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}