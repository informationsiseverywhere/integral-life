package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UtrxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:56
 * Class transformed from UTRXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UtrxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 98;
	public FixedLengthStringData utrxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData utrxpfRecord = utrxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(utrxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(utrxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(utrxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(utrxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(utrxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(utrxrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(utrxrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(utrxrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(utrxrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(utrxrec);
	public FixedLengthStringData unitSubAccount = DD.unitsa.copy().isAPartOf(utrxrec);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(utrxrec);
	public PackedDecimalData nofUnits = DD.nofunt.copy().isAPartOf(utrxrec);
	public PackedDecimalData moniesDate = DD.moniesdt.copy().isAPartOf(utrxrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(utrxrec);
	public PackedDecimalData nofDunits = DD.nofdunt.copy().isAPartOf(utrxrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(utrxrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(utrxrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(utrxrec);
	public FixedLengthStringData contractType = DD.contyp.copy().isAPartOf(utrxrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(utrxrec);
	public PackedDecimalData surrenderPercent = DD.persur.copy().isAPartOf(utrxrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(utrxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UtrxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for UtrxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UtrxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UtrxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UtrxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UtrxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UtrxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UTRXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VRTFND, " +
							"UNITYP, " +
							"TRANNO, " +
							"BATCTRCDE, " +
							"UNITSA, " +
							"NDFIND, " +
							"NOFUNT, " +
							"MONIESDT, " +
							"CNTCURR, " +
							"NOFDUNT, " +
							"FDBKIND, " +
							"CNTAMNT, " +
							"FUNDAMNT, " +
							"CONTYP, " +
							"PRCSEQ, " +
							"PERSUR, " +
							"FUNDRATE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     unitVirtualFund,
                                     unitType,
                                     tranno,
                                     batctrcde,
                                     unitSubAccount,
                                     nowDeferInd,
                                     nofUnits,
                                     moniesDate,
                                     cntcurr,
                                     nofDunits,
                                     feedbackInd,
                                     contractAmount,
                                     fundAmount,
                                     contractType,
                                     procSeqNo,
                                     surrenderPercent,
                                     fundRate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		unitVirtualFund.clear();
  		unitType.clear();
  		tranno.clear();
  		batctrcde.clear();
  		unitSubAccount.clear();
  		nowDeferInd.clear();
  		nofUnits.clear();
  		moniesDate.clear();
  		cntcurr.clear();
  		nofDunits.clear();
  		feedbackInd.clear();
  		contractAmount.clear();
  		fundAmount.clear();
  		contractType.clear();
  		procSeqNo.clear();
  		surrenderPercent.clear();
  		fundRate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUtrxrec() {
  		return utrxrec;
	}

	public FixedLengthStringData getUtrxpfRecord() {
  		return utrxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUtrxrec(what);
	}

	public void setUtrxrec(Object what) {
  		this.utrxrec.set(what);
	}

	public void setUtrxpfRecord(Object what) {
  		this.utrxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(utrxrec.getLength());
		result.set(utrxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}