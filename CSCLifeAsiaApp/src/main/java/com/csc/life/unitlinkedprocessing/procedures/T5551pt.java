/*
 * File: T5551pt.java
 * Date: 30 August 2009 2:22:42
 * Author: Quipoz Limited
 * 
 * Class transformed from T5551PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5551.
*
*
*
***********************************************************************
* </pre>
*/
public class T5551pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5551rec t5551rec = new T5551rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5551pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5551rec.t5551Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5551rec.ageIssageFrm01);
		generalCopyLinesInner.fieldNo008.set(t5551rec.ageIssageTo01);
		generalCopyLinesInner.fieldNo009.set(t5551rec.ageIssageFrm02);
		generalCopyLinesInner.fieldNo010.set(t5551rec.ageIssageTo02);
		generalCopyLinesInner.fieldNo011.set(t5551rec.ageIssageFrm03);
		generalCopyLinesInner.fieldNo012.set(t5551rec.ageIssageTo03);
		generalCopyLinesInner.fieldNo013.set(t5551rec.ageIssageFrm04);
		generalCopyLinesInner.fieldNo014.set(t5551rec.ageIssageTo04);
		generalCopyLinesInner.fieldNo015.set(t5551rec.ageIssageFrm05);
		generalCopyLinesInner.fieldNo016.set(t5551rec.ageIssageTo05);
		generalCopyLinesInner.fieldNo017.set(t5551rec.ageIssageFrm06);
		generalCopyLinesInner.fieldNo018.set(t5551rec.ageIssageTo06);
		generalCopyLinesInner.fieldNo019.set(t5551rec.ageIssageFrm07);
		generalCopyLinesInner.fieldNo020.set(t5551rec.ageIssageTo07);
		generalCopyLinesInner.fieldNo021.set(t5551rec.ageIssageFrm08);
		generalCopyLinesInner.fieldNo022.set(t5551rec.ageIssageTo08);
		generalCopyLinesInner.fieldNo023.set(t5551rec.premCessageFrom01);
		generalCopyLinesInner.fieldNo024.set(t5551rec.premCessageTo01);
		generalCopyLinesInner.fieldNo025.set(t5551rec.premCessageFrom02);
		generalCopyLinesInner.fieldNo026.set(t5551rec.premCessageTo02);
		generalCopyLinesInner.fieldNo027.set(t5551rec.premCessageFrom03);
		generalCopyLinesInner.fieldNo028.set(t5551rec.premCessageTo03);
		generalCopyLinesInner.fieldNo029.set(t5551rec.premCessageFrom04);
		generalCopyLinesInner.fieldNo030.set(t5551rec.premCessageTo04);
		generalCopyLinesInner.fieldNo031.set(t5551rec.premCessageFrom05);
		generalCopyLinesInner.fieldNo032.set(t5551rec.premCessageTo05);
		generalCopyLinesInner.fieldNo033.set(t5551rec.premCessageFrom06);
		generalCopyLinesInner.fieldNo034.set(t5551rec.premCessageTo06);
		generalCopyLinesInner.fieldNo035.set(t5551rec.premCessageFrom07);
		generalCopyLinesInner.fieldNo036.set(t5551rec.premCessageTo07);
		generalCopyLinesInner.fieldNo037.set(t5551rec.premCessageFrom08);
		generalCopyLinesInner.fieldNo038.set(t5551rec.premCessageTo08);
		generalCopyLinesInner.fieldNo039.set(t5551rec.maturityAgeFrom01);
		generalCopyLinesInner.fieldNo040.set(t5551rec.maturityAgeTo01);
		generalCopyLinesInner.fieldNo041.set(t5551rec.maturityAgeFrom02);
		generalCopyLinesInner.fieldNo042.set(t5551rec.maturityAgeTo02);
		generalCopyLinesInner.fieldNo043.set(t5551rec.maturityAgeFrom03);
		generalCopyLinesInner.fieldNo044.set(t5551rec.maturityAgeTo03);
		generalCopyLinesInner.fieldNo045.set(t5551rec.maturityAgeFrom04);
		generalCopyLinesInner.fieldNo046.set(t5551rec.maturityAgeTo04);
		generalCopyLinesInner.fieldNo047.set(t5551rec.maturityAgeFrom05);
		generalCopyLinesInner.fieldNo048.set(t5551rec.maturityAgeTo05);
		generalCopyLinesInner.fieldNo049.set(t5551rec.maturityAgeFrom06);
		generalCopyLinesInner.fieldNo050.set(t5551rec.maturityAgeTo06);
		generalCopyLinesInner.fieldNo051.set(t5551rec.maturityAgeFrom07);
		generalCopyLinesInner.fieldNo052.set(t5551rec.maturityAgeTo07);
		generalCopyLinesInner.fieldNo053.set(t5551rec.maturityAgeFrom08);
		generalCopyLinesInner.fieldNo054.set(t5551rec.maturityAgeTo08);
		generalCopyLinesInner.fieldNo055.set(t5551rec.eaage);
		generalCopyLinesInner.fieldNo056.set(t5551rec.termIssageFrm01);
		generalCopyLinesInner.fieldNo057.set(t5551rec.termIssageTo01);
		generalCopyLinesInner.fieldNo058.set(t5551rec.termIssageFrm02);
		generalCopyLinesInner.fieldNo059.set(t5551rec.termIssageTo02);
		generalCopyLinesInner.fieldNo060.set(t5551rec.termIssageFrm03);
		generalCopyLinesInner.fieldNo061.set(t5551rec.termIssageTo03);
		generalCopyLinesInner.fieldNo062.set(t5551rec.termIssageFrm04);
		generalCopyLinesInner.fieldNo063.set(t5551rec.termIssageTo04);
		generalCopyLinesInner.fieldNo064.set(t5551rec.termIssageFrm05);
		generalCopyLinesInner.fieldNo065.set(t5551rec.termIssageTo05);
		generalCopyLinesInner.fieldNo066.set(t5551rec.termIssageFrm06);
		generalCopyLinesInner.fieldNo067.set(t5551rec.termIssageTo06);
		generalCopyLinesInner.fieldNo068.set(t5551rec.termIssageFrm07);
		generalCopyLinesInner.fieldNo069.set(t5551rec.termIssageTo07);
		generalCopyLinesInner.fieldNo070.set(t5551rec.termIssageFrm08);
		generalCopyLinesInner.fieldNo071.set(t5551rec.termIssageTo08);
		generalCopyLinesInner.fieldNo072.set(t5551rec.premCesstermFrom01);
		generalCopyLinesInner.fieldNo073.set(t5551rec.premCesstermTo01);
		generalCopyLinesInner.fieldNo074.set(t5551rec.premCesstermFrom02);
		generalCopyLinesInner.fieldNo075.set(t5551rec.premCesstermTo02);
		generalCopyLinesInner.fieldNo076.set(t5551rec.premCesstermFrom03);
		generalCopyLinesInner.fieldNo077.set(t5551rec.premCesstermTo03);
		generalCopyLinesInner.fieldNo078.set(t5551rec.premCesstermFrom04);
		generalCopyLinesInner.fieldNo079.set(t5551rec.premCesstermTo04);
		generalCopyLinesInner.fieldNo080.set(t5551rec.premCesstermFrom05);
		generalCopyLinesInner.fieldNo081.set(t5551rec.premCesstermTo05);
		generalCopyLinesInner.fieldNo082.set(t5551rec.premCesstermFrom06);
		generalCopyLinesInner.fieldNo083.set(t5551rec.premCesstermTo06);
		generalCopyLinesInner.fieldNo084.set(t5551rec.premCesstermFrom07);
		generalCopyLinesInner.fieldNo085.set(t5551rec.premCesstermTo07);
		generalCopyLinesInner.fieldNo086.set(t5551rec.premCesstermFrom08);
		generalCopyLinesInner.fieldNo087.set(t5551rec.premCesstermTo08);
		generalCopyLinesInner.fieldNo088.set(t5551rec.maturityTermFrom01);
		generalCopyLinesInner.fieldNo089.set(t5551rec.maturityTermTo01);
		generalCopyLinesInner.fieldNo090.set(t5551rec.maturityTermFrom02);
		generalCopyLinesInner.fieldNo091.set(t5551rec.maturityTermTo02);
		generalCopyLinesInner.fieldNo092.set(t5551rec.maturityTermFrom03);
		generalCopyLinesInner.fieldNo093.set(t5551rec.maturityTermTo03);
		generalCopyLinesInner.fieldNo094.set(t5551rec.maturityTermFrom04);
		generalCopyLinesInner.fieldNo095.set(t5551rec.maturityTermTo04);
		generalCopyLinesInner.fieldNo096.set(t5551rec.maturityTermFrom05);
		generalCopyLinesInner.fieldNo097.set(t5551rec.maturityTermTo05);
		generalCopyLinesInner.fieldNo098.set(t5551rec.maturityTermFrom06);
		generalCopyLinesInner.fieldNo099.set(t5551rec.maturityTermTo06);
		generalCopyLinesInner.fieldNo100.set(t5551rec.maturityTermFrom07);
		generalCopyLinesInner.fieldNo101.set(t5551rec.maturityTermTo07);
		generalCopyLinesInner.fieldNo102.set(t5551rec.maturityTermFrom08);
		generalCopyLinesInner.fieldNo103.set(t5551rec.maturityTermTo08);
		generalCopyLinesInner.fieldNo105.set(t5551rec.rsvflg);
		generalCopyLinesInner.fieldNo106.set(t5551rec.sumInsMin);
		generalCopyLinesInner.fieldNo107.set(t5551rec.mortcls01);
		generalCopyLinesInner.fieldNo108.set(t5551rec.mortcls02);
		generalCopyLinesInner.fieldNo109.set(t5551rec.mortcls03);
		generalCopyLinesInner.fieldNo110.set(t5551rec.mortcls04);
		generalCopyLinesInner.fieldNo111.set(t5551rec.mortcls05);
		generalCopyLinesInner.fieldNo112.set(t5551rec.mortcls06);
		generalCopyLinesInner.fieldNo113.set(t5551rec.sumInsMax);
		generalCopyLinesInner.fieldNo114.set(t5551rec.liencd01);
		generalCopyLinesInner.fieldNo115.set(t5551rec.liencd02);
		generalCopyLinesInner.fieldNo116.set(t5551rec.liencd03);
		generalCopyLinesInner.fieldNo118.set(t5551rec.liencd04);
		generalCopyLinesInner.fieldNo119.set(t5551rec.liencd05);
		generalCopyLinesInner.fieldNo120.set(t5551rec.liencd06);
		generalCopyLinesInner.fieldNo117.set(t5551rec.minLumpSum);
		generalCopyLinesInner.fieldNo121.set(t5551rec.maxLumpSum);
		generalCopyLinesInner.fieldNo122.set(t5551rec.alfnds);
		generalCopyLinesInner.fieldNo123.set(t5551rec.specind);
		generalCopyLinesInner.fieldNo104.set(t5551rec.svcmeth);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Non-Traditional Edit Rules                  S5551");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 10, FILLER).init(" Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 18);
	private FixedLengthStringData filler5 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23, FILLER).init(SPACES);
	private FixedLengthStringData filler6 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine002, 28, FILLER).init("Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler7 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler8 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine003, 0, FILLER).init("Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 18);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 28, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(78);
	private FixedLengthStringData filler10 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(61).isAPartOf(wsaaPrtLine004, 17, FILLER).init("F---T   F---TF---T   F---T   F---T   F---T   F---T   F---T");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler12 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine005, 0, FILLER).init("Issue Age:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 16).setPattern("ZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 20).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 24).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 28).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 32).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 36).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 40).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 44).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 48).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 52).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 56).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 60).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 64).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 68).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 72).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 75, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(79);
	private FixedLengthStringData filler28 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 0, FILLER).init("Premium  Age:");
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 16).setPattern("ZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 20).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 24).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 28).setPattern("ZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 32).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 36).setPattern("ZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 40).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 44).setPattern("ZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 48).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 52).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 56).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 60).setPattern("ZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 64).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 68).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 72).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 75, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(79);
	private FixedLengthStringData filler44 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 0, FILLER).init("Maturity Age:");
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 16).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 20).setPattern("ZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 24).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 28).setPattern("ZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 32).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 36).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 40).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 44).setPattern("ZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 48).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 52).setPattern("ZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 56).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 60).setPattern("ZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 64).setPattern("ZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 68).setPattern("ZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 72).setPattern("ZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 75, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(41);
	private FixedLengthStringData filler60 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine008, 0, FILLER).init("Cess date, Anniversary or Exact");
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler61 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 34, FILLER).init("  (A/E)");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(78);
	private FixedLengthStringData filler62 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler63 = new FixedLengthStringData(61).isAPartOf(wsaaPrtLine009, 17, FILLER).init("F---T   F---TF---T   F---T   F---T   F---T   F---T   F---T");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(79);
	private FixedLengthStringData filler64 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 0, FILLER).init("Issue Age:");
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 16).setPattern("ZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 20).setPattern("ZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 24).setPattern("ZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 28).setPattern("ZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 32).setPattern("ZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 36).setPattern("ZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 40).setPattern("ZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 44).setPattern("ZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 48).setPattern("ZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 52).setPattern("ZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 56).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 60).setPattern("ZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 64).setPattern("ZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 68).setPattern("ZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 72).setPattern("ZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 75, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(79);
	private FixedLengthStringData filler80 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine011, 0, FILLER).init("Premium  Term:");
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 16).setPattern("ZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 20).setPattern("ZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 24).setPattern("ZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 28).setPattern("ZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 32).setPattern("ZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 36).setPattern("ZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 40).setPattern("ZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 44).setPattern("ZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 48).setPattern("ZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 52).setPattern("ZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 56).setPattern("ZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 60).setPattern("ZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 64).setPattern("ZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 68).setPattern("ZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 72).setPattern("ZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 75, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(79);
	private FixedLengthStringData filler96 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine012, 0, FILLER).init("Maturity Term:");
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 16).setPattern("ZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 20).setPattern("ZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 24).setPattern("ZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 28).setPattern("ZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 32).setPattern("ZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 36).setPattern("ZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 40).setPattern("ZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 44).setPattern("ZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 48).setPattern("ZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 52).setPattern("ZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 56).setPattern("ZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 60).setPattern("ZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 64).setPattern("ZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 68).setPattern("ZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 72).setPattern("ZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 75, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(29);
	private FixedLengthStringData filler112 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine013, 0, FILLER).init("Surrender Charge Method:");
	private FixedLengthStringData fieldNo104 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 25);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(76);
	private FixedLengthStringData filler113 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine014, 0, FILLER).init("Unit Reserve Flag:");
	private FixedLengthStringData fieldNo105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 20);
	private FixedLengthStringData filler114 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine014, 21, FILLER).init(SPACES);
	private FixedLengthStringData filler115 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine014, 42, FILLER).init("Sum Insured  Min:");
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine014, 61).setPattern("ZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(76);
	private FixedLengthStringData filler116 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine015, 0, FILLER).init("Mortality Class:");
	private FixedLengthStringData fieldNo107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 17);
	private FixedLengthStringData filler117 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 22);
	private FixedLengthStringData filler118 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 27);
	private FixedLengthStringData filler119 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 32);
	private FixedLengthStringData filler120 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 37);
	private FixedLengthStringData filler121 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 42);
	private FixedLengthStringData filler122 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 43, FILLER).init(SPACES);
	private FixedLengthStringData filler123 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 55, FILLER).init("Max:");
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine015, 61).setPattern("ZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(76);
	private FixedLengthStringData filler124 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine016, 0, FILLER).init("Lien Codes:");
	private FixedLengthStringData fieldNo114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 17);
	private FixedLengthStringData filler125 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 22);
	private FixedLengthStringData filler126 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 27);
	private FixedLengthStringData filler127 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 29, FILLER).init(SPACES);
	private FixedLengthStringData filler128 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine016, 38, FILLER).init("Lump Sum @ Issue Min:");
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine016, 61).setPattern("ZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(76);
	private FixedLengthStringData filler129 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 17);
	private FixedLengthStringData filler130 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo119 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 22);
	private FixedLengthStringData filler131 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 27);
	private FixedLengthStringData filler132 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine017, 29, FILLER).init(SPACES);
	private FixedLengthStringData filler133 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 55, FILLER).init("Max:");
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine017, 61).setPattern("ZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(58);
	private FixedLengthStringData filler134 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine018, 0, FILLER).init("Funds:  Available Fund List:");
	private FixedLengthStringData fieldNo122 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 32);
	private FixedLengthStringData filler135 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 36, FILLER).init(SPACES);
	private FixedLengthStringData filler136 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine018, 42, FILLER).init("Special Terms:");
	private FixedLengthStringData fieldNo123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 57);
}
}
