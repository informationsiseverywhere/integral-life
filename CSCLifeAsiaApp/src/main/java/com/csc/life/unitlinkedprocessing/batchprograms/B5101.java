/*
 * File: B5101.java
 * Date: 29 August 2009 20:53:55
 * Author: Quipoz Limited
 * 
 * Class transformed from B5101.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrxpfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UfndpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ufndpf;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.reports.R5101Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   FUND MOVEMENT REPORT.
*   =====================
*
*   This program is part of the Multi-Thread UnitDeal batch job.
*   If reports are required, it must be run directly after the
*   Unit Deal Extract splitter program B5100. The program has two
*   connected functions, namely:
*
*   - to print the total value of the funds extracted
*
*   - to print the outstanding transactions extracted grouped by
*     fund/type
*
*    The program is run single thread and processes all the
*    records in all the members created by the previous splitter
*    program.
*
*    SQL will be used for the fetching of records using these
*    criteria:
*      -  COMPANY = BSPR-COMPANY
*         ORDER BY COMPANY, VRTFND, UNITYP, CHDRNUM, LIFE,
*         COVERAGE, RIDER, PLNSFX.
*
*    In order to improve the program's performace block fetching
*    has been employed. This merits some explanation (see also
*    comments in sections).
*
*    The blocks (SQL-FIELDS storage) are large enough to store a
*    thousand sets of records at a time. The program loops round
*    the 2000- section fetching sets of records into the current
*    block until the block is full or until all the records have
*    been fetched in, leaving a partially full block. These
*    records are then referenced by the pointer SQL-I for use in
*    the 3000- section.
*
*    Two kinds of record are extracted for inclusion in the
*    reports: UTRNs and UFNDs. These are extracted at the same
*    time from their respective physical files by the use of an
*    SQL UNION statement. The field RECTYPE has been created to
*    allow easy identification of the origin of records and will
*    contain either 'UFND' or 'UTRN'.
*
*    UTRN records are extracted from the threads created by the
*    splitter program B5100. Since the parameters used are thus
*    variable (since there will be more than one member from
*    which to extract records) dynamic SQL is used.
*
*    The threads from B5100 are overriden to the temporary file
*    UTRXxxnnnn, where xx are the two characters on BPRD system
*    parameter 4 and nnnn are the last four numbers of the job no.
*
*    The 3000- section runs using control break logic. A control
*    break is triggered on a change of virtual fund or unit type.
*    This causes a page throw and header lines are printed. When
*    the first record is detected, the FIRST-REC flag, set on at
*    the start, will still br on and the headers will be set up
*    as above.
*
*    Detail lines are printed when records are extracted which
*    have the same VRTFND and UNITYP as the last record; totals
*    lines are produced when there is a change, and are set up
*    prior to the printing of a new header line.
*
*   Control totals:
*     01  -  Number of outstanding transactions read
*     02  -  No. of outstanding transactions processed
*     03  -  No. of o/s transactions with no fund
*     04  -  Number of non-invested transactions
*     05  -  Number of funds with no transactions
*     06  -  Number of report 1 pages printed
*     07  -  Number of report 2 pages printed
*
*
*
*****************************************************************
* </pre>
*/
public class B5101 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlutrxpf1rs = null;
	private java.sql.PreparedStatement sqlutrxpf1ps = null;
	private java.sql.Connection sqlutrxpf1conn = null;
	private R5101Report printerFile1 = new R5101Report();
	private R5101Report printerFile2 = new R5101Report();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5101");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData ap = new FixedLengthStringData(1).init("'");
	private ZonedDecimalData iz = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData lastPriceDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private PackedDecimalData wsaaCurrentFundPrice = new PackedDecimalData(9, 5);
	private FixedLengthStringData wsaaVrtfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaUnityp = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30).init("?", true);
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastChdrcoy = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaBeforeTotVal = new ZonedDecimalData(18, 5);
	private ZonedDecimalData wsaaBeforeTotUnits = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsaaNewTotValue = new ZonedDecimalData(18, 5);
	private ZonedDecimalData wsaaNewTotUnits = new ZonedDecimalData(16, 5);
	private PackedDecimalData wsaaPrcntDiff = new PackedDecimalData(6, 3);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaSqlSelect = new FixedLengthStringData(8192).init("##");
	private String wsaaInd = "N";
	private String wsaaOnceDet = "";
	private String wsaaOnceSum = "";

	private FixedLengthStringData wsaaFundArray = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaFund = FLSArrayPartOfStructure(100, 4, wsaaFundArray, 0);
		/* WSAA-UNIT-TYPE */
	private FixedLengthStringData[] wsaaUnit = FLSInittedArray(100, 1);
	private ZonedDecimalData indc = new ZonedDecimalData(4, 0).setUnsigned();
	private String ufndPrintOnly = "N";

		/*  88  UFND-REC                VALUE 'UFND'.                    
		  88  UTRN-REC                VALUE 'UTRN'.                    */
	private FixedLengthStringData wsaaUfndEmpty = new FixedLengthStringData(1).init("N");
	private Validator ufndEmpty = new Validator(wsaaUfndEmpty, "Y");

	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private Validator firstRec = new Validator(wsaaFirstRecord, "Y");

	private FixedLengthStringData wsaaReportType = new FixedLengthStringData(1);
	private Validator summaryOnlyReport = new Validator(wsaaReportType, "S");

	private FixedLengthStringData wsaaUtrxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaUtrxFn, 0, FILLER).init("UTRX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaUtrxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUtrxFn, 6).setUnsigned();
		/* SQLA-CODES */
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler1 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler1, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler1, 8);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
	private PackedDecimalData isql = new PackedDecimalData(5, 0).init(ZERO);
	private int sqlerrd = 0;
	
	private static final String ufndudlrec = "UFNDUDLREC";
		/* TABLES */
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String t5515 = "T5515";
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
	private static final String g255 = "G255";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;
	private static final int ct09 = 9;
	private static final int ct10 = 10;

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5101sh0Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5101sh0O = new FixedLengthStringData(31).isAPartOf(r5101sh0Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5101sh0O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5101sh0O, 1);

	private FixedLengthStringData r5101dh0Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5101dh0O = new FixedLengthStringData(31).isAPartOf(r5101dh0Record, 0);
	private FixedLengthStringData company1 = new FixedLengthStringData(1).isAPartOf(r5101dh0O, 0);
	private FixedLengthStringData companynm1 = new FixedLengthStringData(30).isAPartOf(r5101dh0O, 1);

	private FixedLengthStringData r5101dh1Record = new FixedLengthStringData(68);
	private FixedLengthStringData r5101dh1O = new FixedLengthStringData(68).isAPartOf(r5101dh1Record, 0);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(r5101dh1O, 0);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(r5101dh1O, 4);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(r5101dh1O, 34);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30).isAPartOf(r5101dh1O, 37);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(1).isAPartOf(r5101dh1O, 67);

	private FixedLengthStringData r5101dt1Record = new FixedLengthStringData(90);
	private FixedLengthStringData r5101dt1O = new FixedLengthStringData(90).isAPartOf(r5101dt1Record, 0);
	private ZonedDecimalData fundvalb = new ZonedDecimalData(18, 5).isAPartOf(r5101dt1O, 0);
	private ZonedDecimalData nofuntsb = new ZonedDecimalData(16, 5).isAPartOf(r5101dt1O, 18);
	private ZonedDecimalData fundvalc = new ZonedDecimalData(18, 5).isAPartOf(r5101dt1O, 34);
	private ZonedDecimalData nofuntsc = new ZonedDecimalData(16, 5).isAPartOf(r5101dt1O, 52);
	private ZonedDecimalData untsdiff = new ZonedDecimalData(16, 5).isAPartOf(r5101dt1O, 68);
	private ZonedDecimalData prcntdiff = new ZonedDecimalData(6, 3).isAPartOf(r5101dt1O, 84);

	private FixedLengthStringData r5101sh1Record = new FixedLengthStringData(2);

	private FixedLengthStringData r5101sd1Record = new FixedLengthStringData(97);
	private FixedLengthStringData r5101sd1O = new FixedLengthStringData(97).isAPartOf(r5101sd1Record, 0);
	private FixedLengthStringData vrtfnd1 = new FixedLengthStringData(4).isAPartOf(r5101sd1O, 0);
	private FixedLengthStringData longdesc1 = new FixedLengthStringData(30).isAPartOf(r5101sd1O, 4);
	private FixedLengthStringData unityp = new FixedLengthStringData(1).isAPartOf(r5101sd1O, 34);
	private ZonedDecimalData totfundval = new ZonedDecimalData(18, 5).isAPartOf(r5101sd1O, 35);
	private FixedLengthStringData fndcurr1 = new FixedLengthStringData(3).isAPartOf(r5101sd1O, 53);
	private ZonedDecimalData pricenow = new ZonedDecimalData(9, 5).isAPartOf(r5101sd1O, 56);
	private FixedLengthStringData pricedt = new FixedLengthStringData(10).isAPartOf(r5101sd1O, 65);
	private ZonedDecimalData totnofunts = new ZonedDecimalData(16, 5).isAPartOf(r5101sd1O, 75);
	private ZonedDecimalData prcntdiff1 = new ZonedDecimalData(6, 3).isAPartOf(r5101sd1O, 91);

	private FixedLengthStringData r5101e01Record = new FixedLengthStringData(2);

	private FixedLengthStringData r5101e02Record = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private R5101dd1RecordInner r5101dd1RecordInner = new R5101dd1RecordInner();
	private SqlUtrxpfInner sqlUtrxpfInner = new SqlUtrxpfInner();
	
	private UfndpfDAO ufndpfDAO = getApplicationContext().getBean("ufndpfDAO", UfndpfDAO.class);
	private Map<String, List<Ufndpf>> ufndpfMap = null;
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private int ct06Value = 0;
	private int ct07Value = 0;
	private int ct08Value = 0;
	private int ct09Value = 0;
	private int ct10Value = 0;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfCursor2080, 
		exit2090, 
		fundDescription3022, 
		fundType3025, 
		sameDate3085, 
		exit3089, 
		readUfnd6050
	}

	public B5101() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** There is no additional restart processing to be done.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		indicArea.set("0");
		wsaaFirstRecord.set("Y");
		ufndPrintOnly = "N";
		indc.set(0);
		wsaaFundArray.set(SPACES);
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		printerFile1.openOutput();
		printerFile2.openOutput();
		wsaaReportType.set(bprdIO.getSystemParam01());
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		/* This SQL command uses a UNION statement to extract records*/
		/* from two discrete physical files, UFNDPF and UTRXPF. The ""*/
		/* (spaces) and 0 (for numerics) are used in the one line where*/
		/* corresponding fields are not to be found in the other file.*/
		/*  Previous comment is no longer valid.                           */
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*   The following lines were commented out, due to the fact that  */
		/*   UFND is no longer read using SQL.                             */
		/* STRING WSAA-SQL-SELECT      DELIMITED BY '##'                */
		/*   'SELECT ALL COMPANY,' AP AP ',' AP AP ','                  */
		/*          AP AP ',' AP AP ',-0, VRTFUND,UNITYP,-0,'           */
		/*          AP AP ',' AP AP ',' AP AP ',NOFUNTS,-0,-0,'         */
		/* DELIMITED BY SIZE INTO WSAA-SQL-SELECT.                      */
		/* STRING WSAA-SQL-SELECT DELIMITED BY '##'                     */
		/*       AP AP ',' AP AP ',-0,-0,'                              */
		/*       AP AP ',-0,-0,-0,' AP 'UFND' AP                        */
		/*      ' FROM UFNDPF '                                         */
		/*      'WHERE COMPANY = ' AP BSPR-COMPANY AP ' ##'             */
		/* DELIMITED BY SIZE INTO WSAA-SQL-SELECT.                      */
		/*  STRING WSAA-SQL-SELECT      DELIMITED BY '##'                */
		/*       'ORDER BY  7, 8, 2, 3, 4, 5, 6, 10'                     */
		/*      DELIMITED BY SIZE     INTO WSAA-SQL-SELECT.              */
		/* This DISPLAY statement allows us to examine active data from*/
		/* the SELECT statment.*/
		getAppVars().addDiagnostic(wsaaSqlSelect);
		/* The PREPARE statement has to be used here since we wish to run*/
		/* the WSAA-SQL-SELECT several times within the program using*/
		/* varying parameters.*/
		String sqlutrxpf1 = wsaaSqlSelect.toString();
		sqlerrorflag = false;
		try {
			sqlutrxpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new UtrxpfTableDAM());
			sqlutrxpf1ps = getAppVars().prepareStatementEmbeded(sqlutrxpf1conn, sqlutrxpf1);
			sqlutrxpf1rs = getAppVars().executeQuery(sqlutrxpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		isql.set(wsaaRowsInBlock);
		//getAppVars().setNumberOfRowsProcessed(wsaaRowsInBlock);
		sqlerrd = wsaaRowsInBlock.toInt();
		/*    Get company name.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(bsprIO.getCompany());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			wsaaCompanynm.fill("?");
		}
		else {
			wsaaCompanynm.set(getdescrec.longdesc);
		}
		/*The purpose of the following is to print the first               */
		/*part of Heading: Company, Title, Date, Time and Page             */
		/*for both reports                                                 */
		company.set(bsprIO.getCompany());
		company1.set(bsprIO.getCompany());
		companynm.set(wsaaCompanynm);
		companynm1.set(wsaaCompanynm);
		printerFile2.printR5101sh0(r5101sh0Record, indicArea);
		printerFile2.printR5101sh1(r5101sh1Record, indicArea);
		/*    Control Total on number of summary report pages written.     */
		ct07Value++;
		printerFile1.printR5101dh0(r5101dh0Record, indicArea);
		wsaaOnceDet = "Y";
		wsaaOnceSum = "Y";
		/*  The following read of the UFND file with BEGN is to check      */
		/*  whether the file is empty or not. In case it is empty, the     */
		/*  program will still carry on.                                   */
		/*  This only happens when run in a new level where UFND has no    */
		/*  data.                                                          */
		wsaaUfndEmpty.set("N");
		int countResult = ufndpfDAO.countUfndRecord();
		if(countResult == 0){
			wsaaUfndEmpty.set("Y");
		}
	}


protected void openThreadMember1100()
	{
		/*OPEN-THREAD-MEMBER*/
		/* This SQL command takes as many UTRXnn members as have been     t*/
		/* created in B5100, and overrides them all to the temporary file **/
		/* UTRXxxnnnn (where xx are the two characters on BPRD sysparam 4*/
		/* and nnnn are the last four numbers of the job no.)*/
		iy.set(iz);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(UTRX");
		stringVariable1.addExpression(iy);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaUtrxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(THREAD");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*  STRING WSAA-SQL-SELECT      DELIMITED BY '##'                */
		/*  'SELECT ALL CHDRCOY, CHDRNUM, LIFE, '                        */
		/*           'COVERAGE, RIDER, PLNSFX, VRTFND, UNITYP, TRANNO, ' */
		/*           'BATCTRCDE,UNITSA,NDFIND,NOFUNT,NOFDUNT, '          */
		/*           'CONTYP,PRCSEQ,PERSUR,FUNDRATE,' AP 'UTRN' AP ' '   */
		/*           'FROM UTRX' IY ' '                                  */
		/*       'WHERE CHDRCOY = ' AP BSPR-COMPANY AP                   */
		/*    ' UNION ALL ##'                                            */
		/*  DELIMITED BY SIZE           INTO WSAA-SQL-SELECT.            */
		if (isLT(iy, bprdIO.getThreadsSubsqntProc())) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(wsaaSqlSelect, "##");
			stringVariable2.addExpression("SELECT ALL CHDRCOY, CHDRNUM, LIFE, ");
			stringVariable2.addExpression("COVERAGE, RIDER, PLNSFX, VRTFND, UNITYP, TRANNO, ");
			stringVariable2.addExpression("BATCTRCDE,UNITSA,NDFIND,NOFUNT,NOFDUNT, ");
			stringVariable2.addExpression("MONIESDT,CNTCURR,FDBKIND,CNTAMNT,FUNDAMNT,");
			stringVariable2.addExpression("CONTYP,PRCSEQ,PERSUR,FUNDRATE,");
			stringVariable2.addExpression(ap);
			stringVariable2.addExpression("UTRN");
			stringVariable2.addExpression(ap);
			stringVariable2.addExpression(" ");
//			stringVariable2.addExpression("FROM UTRX");
//			stringVariable2.addExpression(iy);
			stringVariable2.addExpression("FROM " + wsaaUtrxFn.toString());
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression("WHERE CHDRCOY = ");
			stringVariable2.addExpression(ap);
			stringVariable2.addExpression(bsprIO.getCompany());
			stringVariable2.addExpression(ap);
			stringVariable2.addExpression(" UNION ALL ##");
			stringVariable2.setStringInto(wsaaSqlSelect);
		}
		else {
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression(wsaaSqlSelect, "##");
			stringVariable3.addExpression("SELECT ALL CHDRCOY, CHDRNUM, LIFE, ");
			stringVariable3.addExpression("COVERAGE, RIDER, PLNSFX, VRTFND, UNITYP, TRANNO, ");
			stringVariable3.addExpression("BATCTRCDE,UNITSA,NDFIND,NOFUNT,NOFDUNT, ");
			stringVariable3.addExpression("MONIESDT,CNTCURR,FDBKIND,CNTAMNT,FUNDAMNT,");
			stringVariable3.addExpression("CONTYP,PRCSEQ,PERSUR,FUNDRATE,");
			stringVariable3.addExpression(ap);
			stringVariable3.addExpression("UTRN");
			stringVariable3.addExpression(ap);
			stringVariable3.addExpression(" ");
//			stringVariable3.addExpression("FROM UTRX");
//			stringVariable3.addExpression(iy);
			stringVariable3.addExpression("FROM " + wsaaUtrxFn.toString());
			stringVariable3.addExpression(" ");
			stringVariable3.addExpression("WHERE CHDRCOY = ");
			stringVariable3.addExpression(ap);
			stringVariable3.addExpression(bsprIO.getCompany());
			stringVariable3.addExpression(ap);
			stringVariable3.addExpression(" ORDER BY 7, 8, 2, 3, 4, 5, 6,  14");
			stringVariable3.setStringInto(wsaaSqlSelect);
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfCursor2080: 
					endOfCursor2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		/*    In the 1000 section we have set the SQLERRD(3) to the maximum*/
		/*    The first thing we do is set the pointer up by one and then*/
		/*    compare it to SQLERRD(3) which contains the number of rows*/
		/*    that were actually fetched.*/
		isql.add(1);
		if (isGTE(sqlerrd, isql.toInt())) {
			/*        Still processing a block*/
			ct01Value++;
			goTo(GotoLabel.exit2090);
		}
		else {
			if (isEQ(sqlerrd, wsaaRowsInBlock.toInt())) {
				/*        The current block was full, get another one*/
				/*NEXT_SENTENCE*/
			}
			else {
				/*        The last block contained the last record, no point doing*/
				/*        further fetches, all done*/
				goTo(GotoLabel.endOfCursor2080);
			}
		}
		sqlerrorflag = false;
		int utrxpf1LoopIndex = 1;
		try {
			for (; isLT(utrxpf1LoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlutrxpf1rs); utrxpf1LoopIndex++ ){
				getAppVars().getDBObject(sqlutrxpf1rs, 1, sqlUtrxpfInner.sqlChdrcoy[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 2, sqlUtrxpfInner.sqlChdrnum[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 3, sqlUtrxpfInner.sqlLife[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 4, sqlUtrxpfInner.sqlCoverage[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 5, sqlUtrxpfInner.sqlRider[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 6, sqlUtrxpfInner.sqlPlnsfx[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 7, sqlUtrxpfInner.sqlVrtfnd[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 8, sqlUtrxpfInner.sqlUnityp[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 9, sqlUtrxpfInner.sqlTranno[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 10, sqlUtrxpfInner.sqlBatctrcde[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 11, sqlUtrxpfInner.sqlUnitsa[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 12, sqlUtrxpfInner.sqlNdfind[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 13, sqlUtrxpfInner.sqlNofunt[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 14, sqlUtrxpfInner.sqlNofdunt[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 15, sqlUtrxpfInner.sqlMoniesdt[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 16, sqlUtrxpfInner.sqlCntcurr[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 17, sqlUtrxpfInner.sqlFdbkind[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 18, sqlUtrxpfInner.sqlCntamnt[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 19, sqlUtrxpfInner.sqlFundamnt[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 20, sqlUtrxpfInner.sqlContyp[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 21, sqlUtrxpfInner.sqlPrcseq[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 22, sqlUtrxpfInner.sqlPersur[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 23, sqlUtrxpfInner.sqlFundrate[utrxpf1LoopIndex]);
				getAppVars().getDBObject(sqlutrxpf1rs, 24, sqlUtrxpfInner.sqlRectype[utrxpf1LoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		sqlerrd = utrxpf1LoopIndex - 1;
		if (sqlerrd == 0) {
			goTo(GotoLabel.endOfCursor2080);
		}
		isql.set(1);
		ct01Value++;
		if (ufndpfMap == null) {
			ufndpfMap = new HashMap<>();
		} else {
			ufndpfMap.clear();
		}
		List<String> vrtfndList = new ArrayList<>();
		for (int i = 1; i < utrxpf1LoopIndex; i++) {
			vrtfndList.add(sqlUtrxpfInner.sqlVrtfnd[i].toString());
		}
		ufndpfMap = ufndpfDAO.searchUfndRecord(vrtfndList);

		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		getAppVars().freeDBConnectionIgnoreErr(sqlutrxpf1conn, sqlutrxpf1ps, sqlutrxpf1rs);
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		/* The UNION command performed earlier means that we have brought i*/
		/* two different types of record: UFNDs and UTRNs. These are easily*/
		/* identifiable by use of the RECTYPE field which will contain a*/
		/* reference to one or the other.*/
		/* Previous comment is no longer valid.                            */
		/* MOVE SQL-RECTYPE (ISQL)     TO WSAA-REC-TYPE.                */
		/* It is conceivable, though highly improbable, that the user may w*/
		/* to process transactions not actually connected to any fund. In s*/
		/* cases, we ignore that UTRN until such a time as there may be a f*/
		/* to which it can be attached.*/
		wsaaInd = "Y";
		if (isEQ(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()], SPACES)) {
			/* AND UTRN-REC                                                 */
			ct04Value++;
			wsspEdterror.set(SPACES);
			return ;
		}
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		if (ufndEmpty.isTrue()) {
			return ;
		}
		Ufndpf ufndpf = readUfnd5000();
		if (firstRec.isTrue()) {
			wsaaFirstRecord.set("N");
			setControlBreak3010();
			setUpFundDetails3020(ufndpf);
			/*      PERFORM 3030-SUMMARY-PAGE-HEADING                        */
			detailPageHeading3040();
		}
		if (isNE(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()], wsaaVrtfnd)
		|| isNE(sqlUtrxpfInner.sqlUnityp[isql.toInt()], wsaaUnityp)) {
			detailFooting3050();
			summaryLine3060();
			zeroiseAccumulations3070();
			setUpFundDetails3020(ufndpf);
			detailPageHeading3040();
		}
		/* IF UTRN-REC                                                  */
		/*     PERFORM 3080-GET-PRICE-VIA-UFPRICE                       */
		/*     PERFORM 3110-ACCUMULATE                                  */
		/*     IF NOT SUMMARY-ONLY-REPORT                               */
		/*         PERFORM 3100-PRINT-DETAIL-LINE                       */
		/*     END-IF                                                   */
		/* END-IF.                                                      */
		getPriceViaUfprice3080();
		accumulate3110();
		if (!summaryOnlyReport.isTrue()) {
			printDetailLine3100();
		}
		/* IF  UFND-REC                                                 */
		/* AND WSAA-LAST-REC-TYPE      = 'UFND'                         */
		/*        Clock up number of funds without trans (utrns)*/
		/*     MOVE CT05                   TO CONT-TOTNO                */
		/*     MOVE 1                      TO CONT-TOTVAL               */
		/*     PERFORM 001-CALL-CONTOT                                  */
		/* END-IF.                                                      */
		setControlBreak3010();
		wsaaOnceDet = "N";
		wsaaOnceSum = "N";
	}


protected void setControlBreak3010()
	{
		/*SET-CONTROL-BREAK*/
		/* MOVE SQL-RECTYPE (ISQL)     TO WSAA-LAST-REC-TYPE.           */
		wsaaVrtfnd.set(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
		wsaaUnityp.set(sqlUtrxpfInner.sqlUnityp[isql.toInt()]);
		indOn.setTrue(10);
		/*EXIT*/
	}

protected void setUpFundDetails3020(Ufndpf ufndpf)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setUpFundDetails3021();
				case fundDescription3022: 
					fundDescription3022(ufndpf);
				case fundType3025: 
					fundType3025(ufndpf);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setUpFundDetails3021()
	{
		/* If the unit type has changed but not the fund there is no*/
		/* need to set up the fund: just do the UFPRICE subroutine.*/
		if (isEQ(ufndPrintOnly, "Y")) {
			goTo(GotoLabel.fundDescription3022);
		}
		if (isEQ(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()], wsaaVrtfnd)
		&& isNE(sqlUtrxpfInner.sqlUnityp[isql.toInt()], wsaaUnityp)) {
			goTo(GotoLabel.fundType3025);
		}
	}

	/**
	* <pre>
	* Get fund description
	* </pre>
	*/
protected void fundDescription3022(Ufndpf ufndpf)
	{
		/*  SQL-VRTFND and SQL-VRTFND has been commented out, because 1 is */
		/*  added to ISQL when there are no ITRX records at the start of   */
		/*  2000, so the value is 1001. It skips the instruction which     */
		/*  resets ISQL to 1 WHENEVER NOT FOUND, making the program fail   */
		/*  fail because Subscript ISQL is out of range.                   */
		itemIO.setItempfx("IT");
		/*  MOVE SQL-CHDRCOY (ISQL)     TO ITEM-ITEMCOY                  */
		itemIO.setItemtabl(t5515);
		/*  MOVE SQL-VRTFND  (ISQL)     TO ITEM-ITEMITEM.                */
		if (isEQ(ufndPrintOnly, "Y")) {
			itemIO.setItemcoy(ufndpf.getCompany());
			itemIO.setItemitem(ufndpf.getVirtualFund());
		}
		else {
			itemIO.setItemcoy(sqlUtrxpfInner.sqlChdrcoy[isql.toInt()]);
			itemIO.setItemitem(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
		}
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			longdesc.fill("?");
			longdesc1.fill("?");
		}
		else {
			longdesc.set(getdescrec.longdesc);
			longdesc1.set(getdescrec.longdesc);
		}
		/* Get the fund currency from table T5515.*/
		itemIO.setFunction(varcom.readr);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5515);
		/*  MOVE SQL-VRTFND (ISQL)      TO ITEM-ITEMITEM.                */
		if (isEQ(ufndPrintOnly, "Y")) {
			itemIO.setItemitem(ufndpf.getVirtualFund());
		}
		else {
			itemIO.setItemitem(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		fndcurr.set(t5515rec.currcode);
		fndcurr1.set(t5515rec.currcode);
		if (isEQ(ufndPrintOnly, "Y")) {
			goTo(GotoLabel.fundType3025);
		}
		/*    Look up currency description*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(t5515rec.currcode);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			curdesc.fill("?");
		}
		else {
			curdesc.set(getdescrec.longdesc);
		}
	}

protected void fundType3025(Ufndpf ufndpf)
	{
		/* It is possible that we have arrived in this section having*/
		/* fetched a UTRX record and not a UFND record as expected. In*/
		/* this situation we can be certain that there will be no units*/
		/* for that particular fund as it has not yet been set up. We can*/
		/* therefore set the number of units to zero.*/
		/* Previous comment is no longer valid.                            */
		/* IF  UTRN-REC                                                 */
		/*     MOVE ZEROS              TO WSAA-BEFORE-TOT-UNITS         */
		/*                                WSAA-NEW-TOT-UNITS            */
		/* ELSE                                                         */
		/*     MOVE SQL-NOFUNT (ISQL)  TO WSAA-BEFORE-TOT-UNITS         */
		/*                                WSAA-NEW-TOT-UNITS            */
		/* END-IF.                                                      */
		wsaaBeforeTotUnits.set(ufndpf.getNofunts());
		wsaaNewTotUnits.set(ufndpf.getNofunts());
		/*    The now/deferred indicator is here set to 'N' since we*/
		/*    are setting up a fund and prices will always be of the*/
		/*    'now' variety.*/
		ufpricerec.function.set("PRICE");
		ufpricerec.mode.set("BATCH");
		ufpricerec.company.set(bsprIO.getCompany());
		/*  MOVE  SQL-VRTFND (ISQL)     TO UFPR-UNIT-VIRTUAL-FUND        */
		/*  MOVE  SQL-UNITYP (ISQL)     TO UFPR-UNIT-TYPE                */
		if (isEQ(ufndPrintOnly, "Y")) {
			ufpricerec.unitVirtualFund.set(ufndpf.getVirtualFund());
			ufpricerec.unitType.set(ufndpf.getUnitType());
		}
		else {
			ufpricerec.unitVirtualFund.set(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
			ufpricerec.unitType.set(sqlUtrxpfInner.sqlUnityp[isql.toInt()]);
		}
		ufpricerec.effdate.set(bsscIO.getEffectiveDate());
		ufpricerec.nowDeferInd.set("N");
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)
		&& isNE(ufpricerec.statuz, "ESTM")
		&& isNE(ufpricerec.statuz, varcom.endp)) {
			syserrrec.params.set(ufpricerec.ufpriceRec);
			syserrrec.statuz.set(ufpricerec.statuz);
			fatalError600();
		}
		wsaaCurrentFundPrice.set(ufpricerec.barePrice);
		if (isEQ(ufpricerec.statuz, varcom.endp)) {
			pricedt.set("NO PRICE");
		}
		else {
			datcon1rec.intDate.set(ufpricerec.priceDate);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			pricedt.set(datcon1rec.extDate);
		}
		compute(wsaaBeforeTotVal, 6).setRounded(mult(ufpricerec.barePrice, wsaaBeforeTotUnits));
		/*  MOVE WSAA-BEFORE-TOT-VAL    TO ZRDP-AMOUNT-IN.              */
		/*  PERFORM 8000-CALL-ROUNDING.                                 */
		/*  MOVE ZRDP-AMOUNT-OUT        TO WSAA-BEFORE-TOT-VAL.         */
		if (isNE(wsaaBeforeTotVal, 0)) {
			zrdecplrec.amountIn.set(wsaaBeforeTotVal);
			callRounding8000();
			wsaaBeforeTotVal.set(zrdecplrec.amountOut);
		}
	}

protected void summaryPageHeading3030()
	{
		/*SUMMARY-PAGE-HEADING*/
		/* MOVE BSPR-COMPANY           TO COMPANY OF R5101SH1-O.        */
		/* MOVE WSAA-COMPANYNM         TO COMPANYNM OF R5101SH1-O.      */
		if (isNE(wsaaOnceSum, "Y")) {
			wsaaOnceSum = "N";
			company.set(bsprIO.getCompany());
			companynm.set(wsaaCompanynm);
			printerFile2.printR5101sh0(r5101sh0Record, indicArea);
		}
		printerFile2.printR5101sh1(r5101sh1Record, indicArea);
		/*    Control Total on number of summary report pages written.*/
		ct07Value++;
		/*EXIT*/
	}

protected void detailPageHeading3040()
	{
		detailPageHeading3041();
		printR5101dh13042();
	}

protected void detailPageHeading3041()
	{
		/* MOVE BSPR-COMPANY           TO COMPANY   OF R5101DH1-O.      */
		/* MOVE WSAA-COMPANYNM         TO COMPANYNM OF R5101DH1-O.      */
		vrtfnd.set(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
		fundtyp.set(sqlUtrxpfInner.sqlUnityp[isql.toInt()]);
		if (isNE(wsaaOnceDet, "Y")) {
			wsaaOnceDet = "N";
			company.set(bsprIO.getCompany());
			companynm.set(wsaaCompanynm);
			printerFile1.printR5101dh0(r5101dh0Record, indicArea);
		}
	}

protected void printR5101dh13042()
	{
		printerFile1.printR5101dh1(r5101dh1Record, indicArea);
		ct06Value++;
		/*EXIT*/
	}

protected void detailFooting3050()
	{
		detailFooting3051();
	}

protected void detailFooting3051()
	{
		fundvalb.set(wsaaBeforeTotVal);
		nofuntsb.set(wsaaBeforeTotUnits);
		wsaaNewTotValue.add(wsaaBeforeTotVal);
		fundvalc.set(wsaaNewTotValue);
		nofuntsc.set(wsaaNewTotUnits);
		/* Calculate the difference between the total number of units*/
		/* BEFORE the transaction and AFTER.*/
		compute(untsdiff, 6).setRounded(sub(wsaaNewTotUnits, wsaaBeforeTotUnits));
		/* Calculate the difference as a percentage change.*/
		if (isNE(wsaaBeforeTotUnits, ZERO)
		&& isNE(wsaaNewTotUnits, ZERO)) {
			compute(wsaaPrcntDiff, 6).setRounded(mult((div(wsaaBeforeTotUnits, wsaaNewTotUnits)), 100));
		}
		else {
			wsaaPrcntDiff.set(100);
		}
		prcntdiff.set(wsaaPrcntDiff);
		prcntdiff1.set(wsaaPrcntDiff);
		printerFile1.printR5101dt1(r5101dt1Record, indicArea);
		/*        AT EOP PERFORM 3040-DETAIL-PAGE-HEADING.               */
		/* Control Total on number of total lines printed.*/
		ct03Value++;
	}

protected void summaryLine3060()
	{
		summaryLine3061();
	}

protected void summaryLine3061()
	{
		indc.add(1);
		vrtfnd1.set(wsaaVrtfnd);
		wsaaFund[indc.toInt()].set(wsaaVrtfnd);
		unityp.set(wsaaUnityp);
		wsaaUnit[indc.toInt()].set(wsaaUnityp);
		totfundval.set(wsaaNewTotValue);
		pricenow.set(wsaaCurrentFundPrice);
		totnofunts.set(wsaaNewTotUnits);
		printerFile2.printR5101sd1(r5101sd1Record, indicArea);
		/*        AT EOP PERFORM 3030-SUMMARY-PAGE-HEADING.*/
		ct10Value++;
	}

protected void zeroiseAccumulations3070()
	{
		/*ZEROISE-ACCUMULATIONS*/
		wsaaBeforeTotVal.set(ZERO);
		wsaaBeforeTotUnits.set(ZERO);
		wsaaNewTotValue.set(ZERO);
		wsaaNewTotUnits.set(ZERO);
		wsaaPrcntDiff.set(ZERO);
		/*EXIT*/
	}

protected void getPriceViaUfprice3080()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					getPriceViaUfprice3081();
				case sameDate3085: 
					sameDate3085();
				case exit3089: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getPriceViaUfprice3081()
	{
		r5101dd1RecordInner.estflag.set(SPACES);
		ufpricerec.function.set("PRICE");
		ufpricerec.mode.set("BATCH");
		ufpricerec.company.set(bsprIO.getCompany());
		ufpricerec.unitVirtualFund.set(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
		ufpricerec.unitType.set(sqlUtrxpfInner.sqlUnityp[isql.toInt()]);
		ufpricerec.effdate.set(sqlUtrxpfInner.sqlMoniesdt[isql.toInt()]);
		ufpricerec.nowDeferInd.set(sqlUtrxpfInner.sqlNdfind[isql.toInt()]);
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)
		&& isNE(ufpricerec.statuz, "ESTM")
		&& isNE(ufpricerec.statuz, varcom.endp)) {
			syserrrec.params.set(ufpricerec.ufpriceRec);
			syserrrec.statuz.set(ufpricerec.statuz);
			fatalError600();
		}
		if (isEQ(ufpricerec.statuz, varcom.endp)) {
			r5101dd1RecordInner.estflag.set("*");
			r5101dd1RecordInner.pricedt.set("NO PRICE");
			goTo(GotoLabel.exit3089);
		}
		if (isEQ(ufpricerec.statuz, "ESTM")) {
			r5101dd1RecordInner.estflag.set("*");
		}
		if (isEQ(ufpricerec.priceDate, lastPriceDate)) {
			goTo(GotoLabel.sameDate3085);
		}
		datcon1rec.intDate.set(ufpricerec.priceDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
	}

protected void sameDate3085()
	{
		r5101dd1RecordInner.pricedt.set(datcon1rec.extDate);
		lastPriceDate.set(ufpricerec.priceDate);
	}

protected void printDetailLine3100()
	{
		printDetailLine3101();
	}

protected void printDetailLine3101()
	{
		if (isNE(sqlUtrxpfInner.sqlChdrnum[isql.toInt()], wsaaLastChdrnum)
		|| isNE(sqlUtrxpfInner.sqlChdrcoy[isql.toInt()], wsaaLastChdrcoy)) {
			wsaaLastChdrnum.set(sqlUtrxpfInner.sqlChdrnum[isql.toInt()]);
			wsaaLastChdrcoy.set(sqlUtrxpfInner.sqlChdrcoy[isql.toInt()]);
			indOn.setTrue(10);
		}
		else {
			indOff.setTrue(10);
		}
		r5101dd1RecordInner.chdrnum.set(sqlUtrxpfInner.sqlChdrnum[isql.toInt()]);
		r5101dd1RecordInner.life.set(sqlUtrxpfInner.sqlLife[isql.toInt()]);
		r5101dd1RecordInner.coverage.set(sqlUtrxpfInner.sqlCoverage[isql.toInt()]);
		r5101dd1RecordInner.rider.set(sqlUtrxpfInner.sqlRider[isql.toInt()]);
		r5101dd1RecordInner.plnsfx.set(sqlUtrxpfInner.sqlPlnsfx[isql.toInt()]);
		r5101dd1RecordInner.batctrcde.set(sqlUtrxpfInner.sqlBatctrcde[isql.toInt()]);
		r5101dd1RecordInner.prcseq.set(sqlUtrxpfInner.sqlPrcseq[isql.toInt()]);
		r5101dd1RecordInner.ndfind.set(sqlUtrxpfInner.sqlNdfind[isql.toInt()]);
		r5101dd1RecordInner.unitsa.set(sqlUtrxpfInner.sqlUnitsa[isql.toInt()]);
		printerFile1.printR5101dd1(r5101dd1RecordInner.r5101dd1Record, indicArea);
		/*        AT EOP PERFORM 3040-DETAIL-PAGE-HEADING.*/
		ct02Value++;
	}

protected void accumulate3110()
	{
		r5101dd1RecordInner.nofunits.set(ZERO);
		r5101dd1RecordInner.tranval.set(ZERO);
		r5101dd1RecordInner.convrate.set(ZERO);
		if (isNE(t5515rec.currcode, sqlUtrxpfInner.sqlCntcurr[isql.toInt()])) {
			r5101dd1RecordInner.cntcurr.set(sqlUtrxpfInner.sqlCntcurr[isql.toInt()]);
		}
		else {
			r5101dd1RecordInner.cntcurr.set(SPACES);
		}
		/*    Firstly check if we have a "complete" UTRN. This will be*/
		/*    the case for reversals as the original UTRN will have been*/
		/*    through unit dealing and will have completed amounts.*/
		if (isNE(sqlUtrxpfInner.sqlNofunt[isql.toInt()], ZERO)
		&& isNE(sqlUtrxpfInner.sqlNofdunt[isql.toInt()], ZERO)
		&& isNE(sqlUtrxpfInner.sqlFundamnt[isql.toInt()], ZERO)
		&& isNE(sqlUtrxpfInner.sqlCntamnt[isql.toInt()], ZERO)) {
			r5101dd1RecordInner.nofunits.set(sqlUtrxpfInner.sqlNofdunt[isql.toInt()]);
			r5101dd1RecordInner.tranval.set(sqlUtrxpfInner.sqlFundamnt[isql.toInt()]);
			if (isNE(t5515rec.currcode, sqlUtrxpfInner.sqlCntcurr[isql.toInt()])) {
				r5101dd1RecordInner.convrate.setRounded(sqlUtrxpfInner.sqlFundrate[isql.toInt()]);
			}
		}
		/*    In the normal case of events the cash amount to be bought or*/
		/*    sold and the unit price will be known but not the number of*/
		/*    units involved. Consequently these have to be calculated.*/
		/*    Note that the cash amount will be in the contract currency so*/
		/*    we may need to convert the cash amount into the fund currency*/
		if (isEQ(sqlUtrxpfInner.sqlNofunt[isql.toInt()], ZERO)
		&& isNE(sqlUtrxpfInner.sqlCntamnt[isql.toInt()], ZERO)
		&& isNE(ufpricerec.barePrice, ZERO)) {
			if (isEQ(t5515rec.currcode, sqlUtrxpfInner.sqlCntcurr[isql.toInt()])) {
				r5101dd1RecordInner.tranval.set(sqlUtrxpfInner.sqlCntamnt[isql.toInt()]);
				r5101dd1RecordInner.convrate.set(ZERO);
				compute(r5101dd1RecordInner.nofunits, 6).setRounded(div(sqlUtrxpfInner.sqlCntamnt[isql.toInt()], ufpricerec.barePrice));
			}
			else {
				convertToFundCurr3130();
				r5101dd1RecordInner.convrate.setRounded(conlinkrec.rateUsed);
				if (isNE(conlinkrec.amountOut, 0)) {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					callRounding8000();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
				}
				r5101dd1RecordInner.tranval.set(conlinkrec.amountOut);
				compute(r5101dd1RecordInner.nofunits, 6).setRounded(div(conlinkrec.amountOut, ufpricerec.barePrice));
			}
		}
		/*    In the situation where both the number of units and the cash*/
		/*    amount are zero then we can only assume that this is a surren*/
		if (isEQ(sqlUtrxpfInner.sqlNofunt[isql.toInt()], ZERO)
		&& isEQ(sqlUtrxpfInner.sqlCntamnt[isql.toInt()], ZERO)
		&& isNE(sqlUtrxpfInner.sqlPersur[isql.toInt()], ZERO)) {
			calcSurrenderValue3120();
		}
		/*    If we still don't have a tranval but we do have a price and*/
		/*    some units ....... NB if we are dealing with a unit*/
		/*    journal and all we have is the number of units, move this*/
		/*    amount in, not worrying about values.*/
		if (isEQ(r5101dd1RecordInner.tranval, ZERO)
		&& isNE(sqlUtrxpfInner.sqlNofunt[isql.toInt()], ZERO)) {
			if (isNE(ufpricerec.barePrice, ZERO)) {
				compute(zrdecplrec.amountIn, 5).set(mult(ufpricerec.barePrice, sqlUtrxpfInner.sqlNofunt[isql.toInt()]));
				callRounding8000();
				r5101dd1RecordInner.tranval.set(zrdecplrec.amountOut);
				/*****        COMPUTE TRANVAL OF R5101DD1-O ROUNDED =               */
				/*****F            UFPR-BARE-PRICE * SQL-NOFUNT (ISQL)              */
			}
			r5101dd1RecordInner.nofunits.set(sqlUtrxpfInner.sqlNofunt[isql.toInt()]);
		}
		wsaaNewTotValue.add(r5101dd1RecordInner.tranval);
		wsaaNewTotUnits.add(r5101dd1RecordInner.nofunits);

	}


protected void calcSurrenderValue3120()
	{
		/* If number of units equal to zero AND the Contract Amount for the*/
		/* transaction equal to zero and we have a Surrender Percentage....*/
		/* Then: read UTRS for the Current Unit Balance and calculate the*/
		/* transaction value.*/
		/* There is no need to do a currency conversion and initialise CONV*/
		/* here since all UTRS amounts are in the fund currency.*/
		utrsIO.setChdrcoy(sqlUtrxpfInner.sqlChdrcoy[isql.toInt()]);
		utrsIO.setChdrnum(sqlUtrxpfInner.sqlChdrnum[isql.toInt()]);
		utrsIO.setLife(sqlUtrxpfInner.sqlLife[isql.toInt()]);
		utrsIO.setCoverage(sqlUtrxpfInner.sqlCoverage[isql.toInt()]);
		utrsIO.setRider(sqlUtrxpfInner.sqlRider[isql.toInt()]);
		utrsIO.setPlanSuffix(sqlUtrxpfInner.sqlPlnsfx[isql.toInt()]);
		utrsIO.setUnitVirtualFund(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
		utrsIO.setUnitType(sqlUtrxpfInner.sqlUnityp[isql.toInt()]);
		utrsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)
		&& isNE(utrsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isEQ(utrsIO.getStatuz(), varcom.mrnf)) {
			conlogrec.error.set(g255);
			conlogrec.params.set(utrsIO.getParams());
			callConlog003();
			utrsIO.setCurrentUnitBal(ZERO);
		}
		compute(r5101dd1RecordInner.tranval, 6).setRounded(mult(mult(utrsIO.getCurrentUnitBal(), (div(sqlUtrxpfInner.sqlPersur[isql.toInt()], 100))), ufpricerec.barePrice));
		/* MOVE TRANVAL OF R5101DD1-O  TO ZRDP-AMOUNT-IN.               */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO TRANVAL OF R5101DD1-O.        */
		if (isNE(r5101dd1RecordInner.tranval, 0)) {
			zrdecplrec.amountIn.set(r5101dd1RecordInner.tranval);
			callRounding8000();
			r5101dd1RecordInner.tranval.set(zrdecplrec.amountOut);
		}
		compute(r5101dd1RecordInner.tranval, 0).set(mult(r5101dd1RecordInner.tranval, -1));
		if (isEQ(r5101dd1RecordInner.tranval, 0)
		|| isEQ(ufpricerec.barePrice, 0)) {
			r5101dd1RecordInner.nofunits.set(ZERO);
		}
		else {
			compute(r5101dd1RecordInner.nofunits, 6).setRounded(div(r5101dd1RecordInner.tranval, ufpricerec.barePrice));
		}
	}


protected void convertToFundCurr3130()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.currIn.set(sqlUtrxpfInner.sqlCntcurr[isql.toInt()]);
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.currOut.set(t5515rec.currcode);
		conlinkrec.amountIn.set(sqlUtrxpfInner.sqlCntamnt[isql.toInt()]);
		conlinkrec.amountOut.set(ZERO);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
	}


protected void commit3500()
 {
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callContot001();
		ct01Value = 0;
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callContot001();
		ct02Value = 0;
		contotrec.totno.set(ct03);
		contotrec.totval.set(ct03Value);
		callContot001();
		ct03Value = 0;
		contotrec.totno.set(ct04);
		contotrec.totval.set(ct04Value);
		callContot001();
		ct04Value = 0;
		contotrec.totno.set(ct05);
		contotrec.totval.set(ct05Value);
		callContot001();
		ct05Value = 0;
		contotrec.totno.set(ct06);
		contotrec.totval.set(ct06Value);
		callContot001();
		ct06Value = 0;
		contotrec.totno.set(ct07);
		contotrec.totval.set(ct07Value);
		callContot001();
		ct07Value = 0;
		contotrec.totno.set(ct08);
		contotrec.totval.set(ct08Value);
		callContot001();
		ct08Value = 0;
		contotrec.totno.set(ct09);
		contotrec.totval.set(ct09Value);
		callContot001();
		ct09Value = 0;
		contotrec.totno.set(ct10);
		contotrec.totval.set(ct10Value);
		callContot001();
		ct10Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** No additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/* PERFORM 3050-DETAIL-FOOTING                                  */
		/* PERFORM 3060-SUMMARY-LINE                                    */
		if (isEQ(wsaaInd, "Y")) {
			detailFooting3050();
			summaryLine3060();
		}
		/* The purpose for the following PERFORM is to print all the FUNDs */
		/* from the UFND file, except those already previously written.    */
		if (!ufndEmpty.isTrue()) {
			indc.set(0);
			List<Ufndpf> ufndpfList = ufndpfDAO.searchAllUfndRecord();
			ufndPrintOnly = "Y";
			zeroiseAccumulations3070();
			for(Ufndpf u:ufndpfList){
				ct09Value++;
				if(!checkFund6010(u)){
					continue;
				}
				moveDataToPrintRec6030(u);
				printSummaryLine6040();
			}
		}
		printerFile1.printR5101e01(r5101e01Record, indicArea);
		printerFile2.printR5101e02(r5101e02Record, indicArea);
		printerFile1.close();
		printerFile2.close();
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);

	}


protected Ufndpf readUfnd5000()
	{
		Ufndpf ufndpf = null;
		boolean foundFlag = false;
		if(ufndpfMap!=null && ufndpfMap.containsKey(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()])){
			List<Ufndpf> ufndpfList = ufndpfMap.get(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()]);
			for(Ufndpf u:ufndpfList){
				if(isEQ(sqlUtrxpfInner.sqlChdrcoy[isql.toInt()], u.getCompany())&&isEQ(sqlUtrxpfInner.sqlUnityp[isql.toInt()],u.getUnitType())){
					foundFlag=true;
					ufndpf = u;
				}
			}
		}
		
		if(!foundFlag){
			wsaaBeforeTotVal.set(0);
			wsaaBeforeTotUnits.set(0);
			wsaaNewTotValue.set(0);
			wsaaNewTotUnits.set(0);
			/*        Clock up number of trans without funds                   */
			ct05Value++;
			ufndpf = new Ufndpf();
			ufndpf.setCompany(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()].toString());
			ufndpf.setUnitType(sqlUtrxpfInner.sqlUnityp[isql.toInt()].toString());
			ufndpf.setVirtualFund(sqlUtrxpfInner.sqlVrtfnd[isql.toInt()].toString());
			ufndpf.setNofunts(BigDecimal.ZERO);
		}
		ct08Value++;
		return ufndpf;
	}


protected boolean checkFund6010(Ufndpf u)
	{
		/* Find out whether the fund read has already been printed. In     */
		/* case it has, skip it.                                           */
		for (indc.set(1); !(isEQ(wsaaFund[indc.toInt()], SPACES)
		|| isGT(indc, 100)); indc.add(1)){
			if (isEQ(u.getVirtualFund(), wsaaFund[indc.toInt()])
			&& isEQ(u.getUnitType(), wsaaUnit[indc.toInt()])) {
				return false;
			}
		}
		return true;
	}

protected void moveDataToPrintRec6030(Ufndpf u)
	{
		vrtfnd1.set(u.getVirtualFund());
		unityp.set(u.getUnitType());
		totfundval.set(wsaaBeforeTotVal);
		pricenow.set(wsaaCurrentFundPrice);
		totnofunts.set(u.getNofunts());
		/* Calculate the difference as a percentage change.                */
		if (isNE(wsaaBeforeTotUnits, ZERO)
		&& isNE(wsaaNewTotUnits, ZERO)) {
			compute(wsaaPrcntDiff, 6).setRounded(mult((div(wsaaBeforeTotUnits, wsaaNewTotUnits)), 100));
		}
		else {
			wsaaPrcntDiff.set(100);
		}
		prcntdiff1.set(wsaaPrcntDiff);
	}

protected void printSummaryLine6040()
	{
		printerFile2.printR5101sd1(r5101sd1Record, indicArea);
		ct10Value++;
	}


protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(t5515rec.currcode);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure SQL-UTRXPF--INNER
 */
private static final class SqlUtrxpfInner { 

		/* SQL-UTRXPF 
		 02 SQL-DATA          OCCURS 1000 TIMES.                      */
	private FixedLengthStringData[] sqlData = FLSInittedArray (2000, 102);
	private FixedLengthStringData[] sqlChdrcoy = FLSDArrayPartOfArrayStructure(1, sqlData, 0);
	private FixedLengthStringData[] sqlChdrnum = FLSDArrayPartOfArrayStructure(8, sqlData, 1);
	private FixedLengthStringData[] sqlLife = FLSDArrayPartOfArrayStructure(2, sqlData, 9);
	private FixedLengthStringData[] sqlCoverage = FLSDArrayPartOfArrayStructure(2, sqlData, 11);
	private FixedLengthStringData[] sqlRider = FLSDArrayPartOfArrayStructure(2, sqlData, 13);
	private PackedDecimalData[] sqlPlnsfx = PDArrayPartOfArrayStructure(4, 0, sqlData, 15);
	private FixedLengthStringData[] sqlVrtfnd = FLSDArrayPartOfArrayStructure(4, sqlData, 18);
	private FixedLengthStringData[] sqlUnityp = FLSDArrayPartOfArrayStructure(1, sqlData, 22);
	private PackedDecimalData[] sqlTranno = PDArrayPartOfArrayStructure(5, 0, sqlData, 23);
	private FixedLengthStringData[] sqlBatctrcde = FLSDArrayPartOfArrayStructure(4, sqlData, 26);
	private FixedLengthStringData[] sqlUnitsa = FLSDArrayPartOfArrayStructure(4, sqlData, 30);
	private FixedLengthStringData[] sqlNdfind = FLSDArrayPartOfArrayStructure(1, sqlData, 34);
	private PackedDecimalData[] sqlNofunt = PDArrayPartOfArrayStructure(16, 5, sqlData, 35);
	private PackedDecimalData[] sqlNofdunt = PDArrayPartOfArrayStructure(16, 5, sqlData, 44);
	private PackedDecimalData[] sqlMoniesdt = PDArrayPartOfArrayStructure(8, 0, sqlData, 53);
	private FixedLengthStringData[] sqlCntcurr = FLSDArrayPartOfArrayStructure(3, sqlData, 58);
	private FixedLengthStringData[] sqlFdbkind = FLSDArrayPartOfArrayStructure(1, sqlData, 61);
	private PackedDecimalData[] sqlCntamnt = PDArrayPartOfArrayStructure(17, 2, sqlData, 62);
	private PackedDecimalData[] sqlFundamnt = PDArrayPartOfArrayStructure(17, 2, sqlData, 71);
	private FixedLengthStringData[] sqlContyp = FLSDArrayPartOfArrayStructure(3, sqlData, 80);
	private PackedDecimalData[] sqlPrcseq = PDArrayPartOfArrayStructure(3, 0, sqlData, 83);
	private PackedDecimalData[] sqlPersur = PDArrayPartOfArrayStructure(5, 2, sqlData, 85);
	private PackedDecimalData[] sqlFundrate = PDArrayPartOfArrayStructure(18, 9, sqlData, 88);
	private FixedLengthStringData[] sqlRectype = FLSDArrayPartOfArrayStructure(4, sqlData, 98);
}
/*
 * Class transformed  from Data Structure R5101DD1-RECORD--INNER
 */
private static final class R5101dd1RecordInner { 

	private FixedLengthStringData r5101dd1Record = new FixedLengthStringData(91);
	private FixedLengthStringData r5101dd1O = new FixedLengthStringData(91).isAPartOf(r5101dd1Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5101dd1O, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5101dd1O, 8);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5101dd1O, 10);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5101dd1O, 12);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5101dd1O, 14);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5101dd1O, 18);
	private ZonedDecimalData prcseq = new ZonedDecimalData(3, 0).isAPartOf(r5101dd1O, 22);
	private FixedLengthStringData pricedt = new FixedLengthStringData(10).isAPartOf(r5101dd1O, 25);
	private FixedLengthStringData ndfind = new FixedLengthStringData(1).isAPartOf(r5101dd1O, 35);
	private FixedLengthStringData unitsa = new FixedLengthStringData(4).isAPartOf(r5101dd1O, 36);
	private ZonedDecimalData tranval = new ZonedDecimalData(18, 5).isAPartOf(r5101dd1O, 40);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(r5101dd1O, 58);
	private ZonedDecimalData convrate = new ZonedDecimalData(13, 4).isAPartOf(r5101dd1O, 61);
	private ZonedDecimalData nofunits = new ZonedDecimalData(16, 5).isAPartOf(r5101dd1O, 74);
	private FixedLengthStringData estflag = new FixedLengthStringData(1).isAPartOf(r5101dd1O, 90);
}
}
