/*
 * File: P6520.java
 * Date: 30 August 2009 0:46:44
 * Author: Quipoz Limited
 * 
 * Class transformed from P6520.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.unitlinkedprocessing.dataaccess.ChdrulsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UstmTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S6520ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            OUTSTANDING UNIT STATEMENT REQUESTS
*
* This program lists details of Unit Statements requested
* and not yet processed.
*
* Initialise
* ----------
*
*
*     The details of the  contract being dislayed will be stored in
*     the CHDRULS I/O module.  Retrieve the details.
*
*     Load the subfile as follows:
*
*          Perform a BEGN  on  USTM  using  the  Contract  Company,
*          Contract Number and an Effective Date of zero.
*
*          If no matching  record  has  been  found inform the user
*          that no  details  exist.  Otherwise continue loading the
*          subfile unitl  either  the  Contract Company or Contract
*          Number changes, end  of file,  or  15  lines  have  been
*          written.
*
*          Display the details  from the USTM record and decode the
*          Transaction Code,  BATCTRCDE, from table T1688 using the
*          long description.
*
* Validation
* ----------
*
*     If  the  'KILL'   function  key  was  pressed  skip  all  the
*     validation.
*
*     If 'Roll Up'  has been selected then continue displaying USTM
*     details as desribed above.
*
*
* Updating
* --------
*
*          There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Add 1 to the current program pointer and exit.
*
*
* Notes.
* ------
*
* Tables Used:
*
* T1688 - Transaction Codes                 Key: BATCTRCDE
*
*
*
*****************************************************************
* </pre>
*/
public class P6520 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6520");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* WSAA-MISCELLANEOUS */
	private PackedDecimalData wsaaLinecount = new PackedDecimalData(3, 0);
	private String ustmrec = "USTMREC";
		/* ERRORS */
	private String e040 = "E040";
		/* TABLES */
	private String t1688 = "T1688";
		/*Contract Header Information for Unit Sta*/
	private ChdrulsTableDAM chdrulsIO = new ChdrulsTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Unit Statement Trigger Records no Statem*/
	private UstmTableDAM ustmIO = new UstmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6520ScreenVars sv = ScreenProgram.getScreenVars( S6520ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		para1100, 
		setMoreSign1150, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P6520() {
		super();
		screenVars = sv;
		new ScreenModel("S6520", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1100();
			loadSubfile1200();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		ustmIO.setFormat(ustmrec);
		chdrulsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrulsIO);
		if (isNE(chdrulsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrulsIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrulsIO.getChdrnum());
		sv.cownnum.set(chdrulsIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		sv.effdate.set(ZERO);
		sv.ustmno.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6520", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void loadSubfile1200()
	{
		ustmIO.setChdrcoy(chdrulsIO.getChdrcoy());
		ustmIO.setChdrnum(chdrulsIO.getChdrnum());
		ustmIO.setEffdate(ZERO);
		ustmIO.setFunction(varcom.begn);
		ustmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ustmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, ustmIO);
		if (isNE(ustmIO.getStatuz(),varcom.oK)
		&& isNE(ustmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ustmIO.getParams());
			fatalError600();
		}
		if (isNE(ustmIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustmIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustmIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(e040);
			goTo(GotoLabel.exit1090);
		}
		wsaaLinecount.set(ZERO);
		ustmIO.setFunction(varcom.nextr);
		while ( !(isNE(ustmIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustmIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustmIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLinecount,15))) {
			writeSubfile1100();
		}
		
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void writeSubfile1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case para1100: {
					para1100();
				}
				case setMoreSign1150: {
					setMoreSign1150();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1100()
	{
		sv.subfileArea.set(SPACES);
		sv.effdate.set(ustmIO.getEffdate());
		sv.stmtlevel.set(ustmIO.getUnitStmtFlag());
		sv.stmtType.set(ustmIO.getStmtType());
		sv.ustmno.set(ustmIO.getUstmno());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(ustmIO.getBatctrcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.trandesc.fill("?");
		}
		else {
			sv.trandesc.set(descIO.getLongdesc());
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6520", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		SmartFileCode.execute(appVars, ustmIO);
		if (isNE(ustmIO.getStatuz(),varcom.oK)
		&& isNE(ustmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ustmIO.getParams());
			fatalError600();
		}
		if (isNE(ustmIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustmIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustmIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLinecount,15)) {
			goTo(GotoLabel.setMoreSign1150);
		}
		goTo(GotoLabel.para1100);
	}

protected void setMoreSign1150()
	{
		if (isNE(ustmIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustmIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustmIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			rollUp2100();
			goTo(GotoLabel.exit2090);
		}
	}

protected void rollUp2100()
	{
		/*PARA*/
		wsaaLinecount.set(ZERO);
		while ( !(isNE(ustmIO.getChdrcoy(),chdrulsIO.getChdrcoy())
		|| isNE(ustmIO.getChdrnum(),chdrulsIO.getChdrnum())
		|| isEQ(ustmIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaLinecount,15))) {
			writeSubfile1100();
		}
		
		wsspcomn.edterror.set(SPACES);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			para3000();
		}
		catch (GOTOException e){
		}
	}

protected void para3000()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		/*PARA*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
