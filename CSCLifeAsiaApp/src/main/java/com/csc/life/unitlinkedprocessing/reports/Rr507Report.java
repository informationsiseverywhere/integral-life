package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR507.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rr507Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private FixedLengthStringData occdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData ptrneff = new FixedLengthStringData(10);
	private FixedLengthStringData rundte = new FixedLengthStringData(10);
	private ZonedDecimalData sacscurbal = new ZonedDecimalData(17, 2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2);
	private FixedLengthStringData statcode = new FixedLengthStringData(2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totccount = new ZonedDecimalData(9, 0);
	private ZonedDecimalData totfundval = new ZonedDecimalData(18, 5);
	private ZonedDecimalData totprm = new ZonedDecimalData(13, 2);
	private FixedLengthStringData ualfnd = new FixedLengthStringData(4);
	private ZonedDecimalData ualprc = new ZonedDecimalData(17, 2);
	private FixedLengthStringData ultype = new FixedLengthStringData(1);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr507Report() {
		super();
	}


	/**
	 * Print the XML for Rr507d01
	 */
	public void printRr507d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 9, 4));
		singp.setFieldName("singp");
		singp.setInternal(subString(recordData, 13, 17));
		rundte.setFieldName("rundte");
		rundte.setInternal(subString(recordData, 30, 10));
		ualprc.setFieldName("ualprc");
		ualprc.setInternal(subString(recordData, 40, 17));
		occdate.setFieldName("occdate");
		occdate.setInternal(subString(recordData, 57, 10));
		ptrneff.setFieldName("ptrneff");
		ptrneff.setInternal(subString(recordData, 67, 10));
		sacscurbal.setFieldName("sacscurbal");
		sacscurbal.setInternal(subString(recordData, 77, 17));
		printLayout("Rr507d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				crtable,
				singp,
				rundte,
				ualprc,
				occdate,
				ptrneff,
				sacscurbal
			}
		);

	}

	/**
	 * Print the XML for Rr507h01
	 */
	public void printRr507h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		ualfnd.setFieldName("ualfnd");
		ualfnd.setInternal(subString(recordData, 11, 4));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 15, 30));
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 45, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 48, 30));
		ultype.setFieldName("ultype");
		ultype.setInternal(subString(recordData, 78, 1));
		statcode.setFieldName("statcode");
		statcode.setInternal(subString(recordData, 79, 2));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 81, 30));
		printLayout("Rr507h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				time,
				pagnbr,
				ualfnd,
				descrip,
				currcode,
				currencynm,
				ultype,
				statcode,
				longdesc
			}
//		ILIFE-1165(Batch L2FNDVALRP aborted)		
//			, new Object[] {			// indicators
//				new Object[]{"ind01", indicArea.charAt(1)},
//				new Object[]{"ind02", indicArea.charAt(2)},
//				new Object[]{"ind03", indicArea.charAt(3)}
//			}
		);

		currentPrintLine.set(16);
	}

	/**
	 * Print the XML for Rr507s01
	 */
	public void printRr507s01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		totccount.setFieldName("totccount");
		totccount.setInternal(subString(recordData, 1, 9));
		totprm.setFieldName("totprm");
		totprm.setInternal(subString(recordData, 10, 13));
		totfundval.setFieldName("totfundval");
		totfundval.setInternal(subString(recordData, 23, 18));
		printLayout("Rr507s01",			// Record name
			new BaseData[]{			// Fields:
				totccount,
				totprm,
				totfundval
			}
		);

		currentPrintLine.add(4);
	}


}
