/*
 * File: Rgwtrig.java
 * Date: 30 August 2009 2:12:27
 * Author: Quipoz Limited
 * 
 * Class transformed from RGWTRIG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpudlTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnrgwTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnupdTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.life.unitlinkedprocessing.reports.RgwerReport;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrncpy;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*          ADDITIONAL PROCESSING FOR REGULAR WITHDRAWALS.
*
*            RGWTRIG - Regular Withdrawals Trigger Module.
*            ---------------------------------------------
*
*  This program will be called by  Unit  Deal  whenever  it  is
*  processing  a  UTRN record for a Regular Withdrawal. It will
*  read all the other UTRN's that were created for the  Regular
*  Withdrawal  accumulating  the  amount from each UTRN record.
*  If all can  be  processed  then  it  will  then  create  the
*  postings  for  the  actual Regular Withdrawal and create the
*  payment requisition.
*  This program may be found as an entry on table T6598  -  the
*  Surrender  Claim Method table. It is the Trigger Module held
*  under Additional Processing.
*
*  Processing.
*  -----------
*  Using the contract number and TRANNO the program  will  read
*  all  the  UTRN records created for the transaction. For each
*  record it will accumulate the  amount.  If  it  reaches  any
*  UTRN  record for the transaction that has not been processed
*  by UNITDEAL then it will cease processing and exit program.
*  If all the UTRN records  have  been  processed  by  UNITDEAL
*  then   it   will   perform  the  remainder  of  the  Regular
*  Withdrawal processing, which is to make the  necessary  ACMV
*  postings,  create  the  cheque  requisition if indicated and
*  the RTRN posting.
*
*  Read T5645  with  a  key  of  RGWTRIG. There will be four
*  entries  here, as up to 4 ACMV's may  be  written.  For  all
*  withdrawals  post  3 ACMV's (to separate the fee and balance
*  into  different  accounts)  using  lines  1/2/3  for   Total
*  Pending, Fee, and Balance respectively.
*
*  To  find  out  if a fee is required read T5540, keyed on the
*  CRTABLE of the component from which the withdrawal is  being
*  made.   Use   the   Regular  Withdrawal  Method  from  there
*  concatenated with the withdrawal  currency  to  read  T5542.
*  This  will  provide a matrix of frequencies and rules. Match
*  the Withdrawal Frequency with one on the table. If  none  is
*  found  then  report  an  error  condition and do not proceed
*  with  processing  this  withdrawal.   Otherwise   take   the
*  associated  Fee  details. These will be either a flat fee or
*  a percentage with  maximum  and  minimum  parameters.  If  a
*  value  is  found  in  the  flat fee field then this is to be
*  used. Otherwise if a value is found in the percentage  field
*  use  this  to calculate the fee as a percentage of the total
*  withdrawal amount. If  this  is  less  than  the  associated
*  minimum,  use  the  minimum,  if  it  is  greater  than  the
*  associated maximum use the maximum. Note that both the  flat
*  fee  and  the  percentage  may  be zero, in which case there
*  will be no fee.
*
*
*  Post the total accumulated  value  to  the  first  entry  on
*  T5645 if the fee is non-zero, (do not test for greater
*  than  zero), then post the fee to the second entry, and post
*  the net value (accumulated value - fee) to the third entry.
*
*  If the transaction is an internal transfer, a 'From'  and  a
*  'To' ACMV will be required. The 'From' record will post the
*  net  value using details from the 4th entry on T5645 and the
*  'To' record will be  posted  using  details  from  the  REGP
*  record.
*
*  The LIFACMV values are as follows:
*             Function               - PSTW
*             Batch key              - AT linkage
*             Document number        - contract number
*             Transaction number     - transaction no.
*             Journal sequence no.   - 0
*             Sub-account code       - from applicable T5645
*                                                entry
*             Sub-account type       - ditto
*             Sub-account GL map     - ditto
*             Sub-account GL sign    - ditto
*             S/acct control total   - ditto
*             Cmpy code (sub ledger) - batch company
*             Cmpy code (GL)         - batch company
*             Subsidiary ledger      - contract number
*             Original currency code - currency payable in
*             Original currency amt  - as stated above
*             Accounting curr code   - T3629 ledger currency
*             Accounting curr amount - as stated above
*             Exchange rate          - zero (handled by
*                                            subroutine)
*             Trans reference        - Policy Number
*             Trans description      - from transaction code
*                                                description
*             Posting month and year - defaulted
*             Effective date         - UTRN Monies Date
*             Reconciliation amount  - zero
*             Reconciliation date    - Max date
*             Transaction Id         - Job Parameters
*             Substitution code 1    - contract type
*
*  If  the  payment  is  via  bank  or  cheque,  the subroutine
*  'PAYREQ' will be called with the following details:
*
*             Batchkey               - Job Parameters
*             Tranno                 - from UTRN
*             Effective Date         - UTRN monies date
*             Currency Code          - from REGP
*             Contract Type          - from CHDR
*             Payment Amount         - Calculated Net Value
*             CHEQPFX                - PRFX-BANK-TAPE /
*                                       PRFX-SEMI-CHEQ
*             Trandesc               - T5645 description
*             Bankkey                - from REGP
*             Bankacckey             - from REGP
*             Sacscode               - 4th entry on T5645
*             Sacstype               - ditto
*             Glmap                  - ditto
*             Sign                   - ditto
*             Rldgpfx                - PRFX-CLNT
*             Rldgcoy                - Payee Company from REGP
*             Rldgacct               - Payee No. from REGP
*
*  If an invalid status  is  returned  from  PAYREQ  report  an
*  error and exit the subroutine.
*
*****************************************************************
* </pre>
*/
public class Rgwtrig extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private RgwerReport printerFile = new RgwerReport();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private final String wsaaSubr = "RGWTRIG";
	private PackedDecimalData wsaaLineCount = new PackedDecimalData(3, 0).init(99);
	private static final int wsaaPageSize = 60;
	private FixedLengthStringData wsaaPayeename = new FixedLengthStringData(47);
	private PackedDecimalData wsaaTotalWdlAmount = new PackedDecimalData(18, 3).init(0);
	private PackedDecimalData wsaaNetVal = new PackedDecimalData(18, 3).init(0);
	private PackedDecimalData wsaaTotFee = new PackedDecimalData(18, 3).init(0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private static final int wsaaPnamesLen = 20;

	private FixedLengthStringData wsaaT5542Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5542Wdmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5542Key, 0);
	private FixedLengthStringData wsaaT5542Currcd = new FixedLengthStringData(3).isAPartOf(wsaaT5542Key, 4);
	private FixedLengthStringData wsaaErrorFormat = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();

		/* WSAA-GENERAL-FIELDS */
	private FixedLengthStringData wsaaIfAllUtrnProcessed = new FixedLengthStringData(1);
	private Validator wsaaAllUtrnProcessed = new Validator(wsaaIfAllUtrnProcessed, "Y");

	private FixedLengthStringData wsaaIfFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator wsaaFirstTime = new Validator(wsaaIfFirstTime, "Y");
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLastOrigCcy = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();
		/* WSAA-T5542-INFO */
	private PackedDecimalData wsaaFeemax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFeemin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFeepc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaFfamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWdlAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWdrem = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaParams = new FixedLengthStringData(37);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String g450 = "G450";
	private static final String h388 = "H388";
		/* TABLES */
	private static final String t1693 = "T1693";
	private static final String t5645 = "T5645";
	private static final String t5540 = "T5540";
	private static final String t5542 = "T5542";
	private static final String t3629 = "T3629";
	private static final String t5688 = "T5688";
	private static final String t6694 = "T6694";
		/* FORMATS */
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String regpudlrec = "REGPUDLREC";

	private FixedLengthStringData rgwerh01Record = new FixedLengthStringData(51);
	private FixedLengthStringData rgwerh01O = new FixedLengthStringData(51).isAPartOf(rgwerh01Record, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(rgwerh01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rgwerh01O, 10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rgwerh01O, 11);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(rgwerh01O, 41);
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RegpudlTableDAM regpudlIO = new RegpudlTableDAM();
	private UtrnrgwTableDAM utrnrgwIO = new UtrnrgwTableDAM();
	private UtrnupdTableDAM utrnupdIO = new UtrnupdTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5645rec t5645rec = new T5645rec();
	private T3629rec t3629rec = new T3629rec();
	private T5540rec t5540rec = new T5540rec();
	private T5542rec t5542rec = new T5542rec();
	private T5688rec t5688rec = new T5688rec();
	private T6694rec t6694rec = new T6694rec();
	private Payreqrec payreqrec = new Payreqrec();
	private Subcoderec subcoderec = new Subcoderec();
	private Dbcstrncpy dbcstrncpy = new Dbcstrncpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Udtrigrec udtrigrec = new Udtrigrec();
	private Rgwerd01RecordInner rgwerd01RecordInner = new Rgwerd01RecordInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNextUtrn2110, 
		exit2190, 
		exit7190
	}

	public Rgwtrig() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			mainLine100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine100()
	{
		start100();
		exit190();
	}

protected void start100()
	{
		udtrigrec.statuz.set(varcom.oK);
		initialize1000();
		chkIfAllUtrnProcessed2000();
		/* There are still outstanding funds to be dealt, so no need to*/
		/* process*/
		if (!wsaaAllUtrnProcessed.isTrue()) {
			return ;
		}
		openBatch1600();
		compute(wsaaTotalWdlAmount, 3).set(mult(wsaaTotalWdlAmount, -1));
		/* Update the total amount on the REGP record if payment is by*/
		/* percentage, as before now the correct value could not have been*/
		/* determined.*/
		/* IF REGPUDL-PRCNT         NOT = ZEROES                        */
		/*    ADD WSAA-TOTAL-WDL-AMOUNT TO REGPUDL-TOTAMNT              */
		/*    MOVE REGPUDLREC          TO REGPUDL-FORMAT                */
		/*    MOVE REWRT               TO REGPUDL-FUNCTION              */
		/*    CALL 'REGPUDLIO'      USING REGPUDL-PARAMS                */
		/*    IF REGPUDL-STATUZ     NOT = O-K                           */
		/*       MOVE REGPUDL-PARAMS   TO SYSR-PARAMS                   */
		/*       MOVE REGPUDL-STATUZ   TO SYSR-STATUZ                   */
		/*       MOVE REGPUDLREC       TO WSAA-ERROR-FORMAT             */
		/*       PERFORM 9000-FATAL-ERROR                               */
		/*    END-IF                                                    */
		/* END-IF.                                                      */
		setupWithdrawalFee4000();
		/*  Subtract total fee from total value of funds giving amount to*/
		/*  be posted*/
		compute(wsaaNetVal, 3).set(sub(wsaaTotalWdlAmount, wsaaTotFee));
		/* Get Currency details from T3629*/
		getCurrencyRate7000();
		/* Post to Pending Withdrawal Account*/
		postPending3000();
		/* Post to Withdrawal fee account*/
		if (isNE(wsaaTotFee, ZERO)) {
			postWdlFee3100();
		}
		/* Post to Withdrawal Balance Account*/
		postWdlBal3200();
		/*  Perform Payment Processing*/
		if (isNE(regpudlIO.getDestkey(), SPACES)) {
			intTrans3500();
		}
		else {
			callPayreq3400();
		}
		printReport5000();
		/* Update the total amount on all REGP records if payment is by    */
		/* percentage, as before now the correct value could not have been */
		/* determined.                                                     */
		if (isNE(regpudlIO.getPrcnt(), ZERO)) {
			while ( !(isEQ(regpudlIO.getStatuz(), varcom.endp)
			|| isNE(udtrigrec.tk1Chdrcoy, regpudlIO.getChdrcoy())
			|| isNE(udtrigrec.tk1Chdrnum, regpudlIO.getChdrnum())
			|| isNE(udtrigrec.life, regpudlIO.getLife())
			|| isNE(udtrigrec.coverage, regpudlIO.getCoverage())
			|| isNE(udtrigrec.rider, regpudlIO.getRider())
			|| isNE(udtrigrec.planSuffix, regpudlIO.getPlanSuffix())
			|| isNE(udtrigrec.tk1Seqnumb, regpudlIO.getRgpynum()))) {
				setPrecision(regpudlIO.getTotamnt(), 3);
				regpudlIO.setTotamnt(add(regpudlIO.getTotamnt(), wsaaTotalWdlAmount));
				regpudlIO.setFormat(regpudlrec);
				regpudlIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, regpudlIO);
				if (isNE(regpudlIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(regpudlIO.getParams());
					syserrrec.statuz.set(regpudlIO.getStatuz());
					fatalError9000();
				}
				regpudlIO.setFormat(regpudlrec);
				regpudlIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, regpudlIO);
				if (isNE(regpudlIO.getStatuz(), varcom.oK)
				&& isNE(regpudlIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(regpudlIO.getParams());
					syserrrec.statuz.set(regpudlIO.getStatuz());
					fatalError9000();
				}
			}
			
		}
		closeBatch1700();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialize1000()
	{
		start1010();
	}

protected void start1010()
	{
		wsaaFfamt.set(ZERO);
		wsaaFeepc.set(ZERO);
		wsaaFeemax.set(ZERO);
		wsaaFeemin.set(ZERO);
		if (wsaaFirstTime.isTrue()) {
			wsaaIfFirstTime.set("N");
			printerFile.openOutput();
		}
		syserrrec.subrname.set(wsaaSubr);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		readRegp1200();
		readT66946000();
		readChdrrgp1300();
		if (isNE(regpudlIO.getPayclt(), SPACES)) {
			setupPayeeName5200();
		}
		else {
			wsaaPayeename.set(regpudlIO.getDestkey());
		}
		/*    Get company name.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		/* MOVE PARM-COMPANY           TO DESC-DESCITEM.           <002>*/
		descIO.setDescitem(udtrigrec.company);
		/*MOVE WSAA-ITEMKEY           TO GTDS-ITEMKEY.                 */
		/* MOVE PARM-LANGUAGE          TO DESC-LANGUAGE.           <002>*/
		descIO.setLanguage(udtrigrec.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			wsaaCompanynm.set(SPACES);
		}
		else {
			wsaaCompanynm.set(descIO.getLongdesc());
		}
		/* MOVE WSAA-UTRNUPD-DATA-KEY  TO UTRNUPD-DATA-KEY.             */
		utrnupdIO.setDataKey(udtrigrec.utrnKey);
		utrnupdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnupdIO);
		if (isNE(utrnupdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnupdIO.getParams());
			syserrrec.statuz.set(utrnupdIO.getStatuz());
			fatalError9000();
		}
		readT36291900();
		readT56451800();
		readT55401500();
		readT56881400();
		validateGlmap1995();
	}

protected void readRegp1200()
	{
		start1200();
	}

protected void start1200()
	{
		/*  Set up Header rec. key.*/
		regpudlIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO REGPUDL-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO REGPUDL-CHDRNUM.              */
		/* MOVE WSAA-UTRNUPD-LIFE      TO REGPUDL-LIFE.            <009>*/
		/* MOVE WSAA-UTRNUPD-COVERAGE  TO REGPUDL-COVERAGE.        <009>*/
		/* MOVE WSAA-UTRNUPD-RIDER     TO REGPUDL-RIDER.           <009>*/
		/* MOVE WSAA-UTRNUPD-PLAN-SUFFIX TO REGPUDL-PLAN-SUFFIX.   <009>*/
		/* MOVE WSAA-TRIGGER-TRANNO    TO REGPUDL-TRANNO.               */
		/* MOVE WSAA-TRIGGER-SEQNUMB   TO REGPUDL-RGPYNUM.              */
		regpudlIO.setChdrcoy(udtrigrec.tk1Chdrcoy);
		regpudlIO.setChdrnum(udtrigrec.tk1Chdrnum);
		regpudlIO.setLife(udtrigrec.life);
		regpudlIO.setCoverage(udtrigrec.coverage);
		regpudlIO.setRider(udtrigrec.rider);
		regpudlIO.setPlanSuffix(udtrigrec.planSuffix);
		regpudlIO.setTranno(udtrigrec.tk1Tranno);
		regpudlIO.setRgpynum(udtrigrec.tk1Seqnumb);
		/* MOVE READH                  TO REGPUDL-FUNCTION.             */
		regpudlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		regpudlIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, regpudlIO);
		if (isNE(regpudlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpudlIO.getParams());
			syserrrec.statuz.set(regpudlIO.getStatuz());
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY  NOT = REGPUDL-CHDRCOY OR       <009>*/
		/*    WSAA-TRIGGER-CHDRNUM  NOT = REGPUDL-CHDRNUM OR       <009>*/
		/*    WSAA-UTRNUPD-LIFE     NOT = REGPUDL-LIFE OR          <009>*/
		/*    WSAA-UTRNUPD-COVERAGE NOT = REGPUDL-COVERAGE OR      <009>*/
		/*    WSAA-UTRNUPD-RIDER    NOT = REGPUDL-RIDER OR         <009>*/
		/*    WSAA-UTRNUPD-PLAN-SUFFIX NOT = REGPUDL-PLAN-SUFFIX OR<009>*/
		/*    WSAA-TRIGGER-SEQNUMB  NOT = REGPUDL-RGPYNUM OR       <009>*/
		/*    WSAA-TRIGGER-TRANNO  NOT =  REGPUDL-TRANNO           <009>*/
		/*    MOVE REGPUDL-PARAMS      TO SYSR-PARAMS              <009>*/
		/*    MOVE REGPUDL-STATUZ      TO SYSR-STATUZ              <009>*/
		/*    MOVE REGPUDLREC          TO WSAA-ERROR-FORMAT        <009>*/
		/*    PERFORM 9000-FATAL-ERROR                             <009>*/
		/* END-IF.                                                 <009>*/
		if (isNE(udtrigrec.tk1Chdrcoy, regpudlIO.getChdrcoy())
		|| isNE(udtrigrec.tk1Chdrnum, regpudlIO.getChdrnum())
		|| isNE(udtrigrec.life, regpudlIO.getLife())
		|| isNE(udtrigrec.coverage, regpudlIO.getCoverage())
		|| isNE(udtrigrec.rider, regpudlIO.getRider())
		|| isNE(udtrigrec.planSuffix, regpudlIO.getPlanSuffix())
		|| isNE(udtrigrec.tk1Seqnumb, regpudlIO.getRgpynum())
		|| isNE(udtrigrec.tk1Tranno, regpudlIO.getTranno())) {
			regpudlIO.setStatuz(varcom.endp);
			regpudlIO.setChdrcoy(udtrigrec.tk1Chdrcoy);
			regpudlIO.setChdrnum(udtrigrec.tk1Chdrnum);
			regpudlIO.setLife(udtrigrec.life);
			regpudlIO.setCoverage(udtrigrec.coverage);
			regpudlIO.setRider(udtrigrec.rider);
			regpudlIO.setPlanSuffix(udtrigrec.planSuffix);
			regpudlIO.setTranno(udtrigrec.tk1Tranno);
			regpudlIO.setRgpynum(udtrigrec.tk1Seqnumb);
			syserrrec.params.set(regpudlIO.getParams());
			syserrrec.statuz.set(regpudlIO.getStatuz());
			fatalError9000();
		}
	}

protected void readChdrrgp1300()
	{
		/*START*/
		chdrrgpIO.setDataArea(SPACES);
		chdrrgpIO.setChdrcoy(regpudlIO.getChdrcoy());
		chdrrgpIO.setChdrnum(regpudlIO.getChdrnum());
		chdrrgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			syserrrec.statuz.set(chdrrgpIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void readT56881400()
	{
		read1410();
	}

protected void read1410()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(regpudlIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrrgpIO.getCnttype());
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), regpudlIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrrgpIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrrgpIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT55401500()
	{
		go1500();
	}

protected void go1500()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(regpudlIO.getChdrcoy());
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(utrnupdIO.getCrtable());
		/* IF  PARM-EFFDATE             = ZEROS                         */
		if (isEQ(udtrigrec.effdate, ZERO)) {
			itdmIO.setItmfrm(varcom.vrcmMaxDate);
		}
		else {
			/*     MOVE PARM-EFFDATE       TO ITDM-ITMFRM                   */
			itdmIO.setItmfrm(udtrigrec.effdate);
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(), regpudlIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), t5540))
		|| (isNE(itdmIO.getItemitem(), utrnupdIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemitem(utrnupdIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void openBatch1600()
	{
		/*START*/
		batcdorrec.batcdorRec.set(SPACES);
		batcdorrec.function.set("AUTO");
		/* MOVE COMN-PARM-BATCPFX      TO BATD-PREFIX.                  */
		/* MOVE PARM-TRANID            TO BATD-TRANID.                  */
		/* MOVE PARM-COMPANY           TO BATD-COMPANY.                 */
		/* MOVE PARM-BATCBRANCH        TO BATD-BRANCH.                  */
		/* MOVE PARM-ACCTYEAR          TO BATD-ACTYEAR.                 */
		/* MOVE PARM-ACCTMONTH         TO BATD-ACTMONTH.                */
		/* MOVE UTRNUPD-BATCTRCDE      TO BATD-TRCDE.                   */
		batcdorrec.batchkey.set(udtrigrec.batchkey);
		batcdorrec.trcde.set(utrnupdIO.getBatctrcde());
		batcdorrec.batch.set(SPACES);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void closeBatch1700()
	{
		start1700();
	}

protected void start1700()
	{
		batcuprec.function.set(varcom.writs);
		batcuprec.batchkey.set(batcdorrec.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError9000();
		}
		batcdorrec.function.set("CLOSE");
		/* MOVE PARM-TRANID            TO BATD-TRANID.                  */
		batcdorrec.batchkey.set(batcdorrec.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
	}

protected void readT56451800()
	{
		read1810();
	}

protected void read1810()
	{
		itemIO.setItemcoy(regpudlIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(regpudlIO.getChdrcoy());
		/* MOVE PARM-LANGUAGE          TO DESC-LANGUAGE.                */
		descIO.setLanguage(udtrigrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
	}

protected void readT36291900()
	{
		read1910();
	}

protected void read1910()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(regpudlIO.getChdrcoy());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(regpudlIO.getCurrcd());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			/*MOVE ITDM-PARAMS         TO SYSR-PARAMS                   */
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
	}

protected void validateGlmap1995()
	{
		para1996();
	}

protected void para1996()
	{
		/*  Ensure that the sacscode,type, GL account, sign and control    */
		/*  all exist on T5645. They are vital for the Payment request.    */
		/*  If the payment is at component level test line 5 of T5645 else */
		/*  test line 1.                                                   */
		/*  If they have not been entered, let the user know, ie error.    */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			if (isEQ(t5645rec.sacscode[5], SPACES)
			|| isEQ(t5645rec.sacstype[5], SPACES)
			|| isEQ(t5645rec.glmap[5], SPACES)
			|| isEQ(t5645rec.sign[5], SPACES)
			|| isEQ(t5645rec.cnttot[5], ZERO)) {
				syserrrec.statuz.set(g450);
				wsaaParams.set(SPACES);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("Item RGWTRIG", " ");
				stringVariable1.addExpression(" ", " ");
				stringVariable1.addExpression(t5645rec.sacscode[5], " ");
				stringVariable1.addExpression(" ", " ");
				stringVariable1.addExpression(t5645rec.sacstype[5], " ");
				stringVariable1.addExpression(" ", " ");
				stringVariable1.addExpression(t5645rec.glmap[5], " ");
				stringVariable1.addExpression(" ", " ");
				stringVariable1.addExpression(t5645rec.sign[5], " ");
				stringVariable1.addExpression(" ", " ");
				stringVariable1.addExpression(t5645rec.cnttot[5], " ");
				stringVariable1.setStringInto(wsaaParams);
				syserrrec.params.set(wsaaParams);
				fatalError9000();
			}
		}
		else {
			if (isEQ(t5645rec.sacscode[1], SPACES)
			|| isEQ(t5645rec.sacstype[1], SPACES)
			|| isEQ(t5645rec.glmap[1], SPACES)
			|| isEQ(t5645rec.sign[1], SPACES)
			|| isEQ(t5645rec.cnttot[1], ZERO)) {
				syserrrec.statuz.set(g450);
				wsaaParams.set(SPACES);
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression("Item RGWTRIG", " ");
				stringVariable2.addExpression(" ", " ");
				stringVariable2.addExpression(t5645rec.sacscode[1], " ");
				stringVariable2.addExpression(" ", " ");
				stringVariable2.addExpression(t5645rec.sacstype[1], " ");
				stringVariable2.addExpression(" ", " ");
				stringVariable2.addExpression(t5645rec.glmap[1], " ");
				stringVariable2.addExpression(" ", " ");
				stringVariable2.addExpression(t5645rec.sign[1], " ");
				stringVariable2.addExpression(" ", " ");
				stringVariable2.addExpression(t5645rec.cnttot[1], " ");
				stringVariable2.setStringInto(wsaaParams);
				syserrrec.params.set(wsaaParams);
				fatalError9000();
			}
		}
	}

protected void chkIfAllUtrnProcessed2000()
	{
		/*PARA*/
		wsaaIfAllUtrnProcessed.set("Y");
		wsaaTotalWdlAmount.set(0);
		utrnrgwIO.setParams(SPACES);
		utrnrgwIO.setChdrnum(regpudlIO.getChdrnum());
		utrnrgwIO.setChdrcoy(regpudlIO.getChdrcoy());
		utrnrgwIO.setTranno(utrnupdIO.getTranno());
		utrnrgwIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		utrnrgwIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		readUtrn2700();
		while ( !(!wsaaAllUtrnProcessed.isTrue()
		|| isEQ(utrnrgwIO.getStatuz(), varcom.endp))) {
			checkFeedbackIndUtrn2100();
		}
		
		/*EXIT*/
	}

protected void checkFeedbackIndUtrn2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2100();
				case readNextUtrn2110: 
					readNextUtrn2110();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2100()
	{
		if (isNE(utrnrgwIO.getTriggerModule(), wsaaSubr)) {
			goTo(GotoLabel.readNextUtrn2110);
		}
		if (isNE(utrnrgwIO.getFeedbackInd(), "Y")) {
			wsaaIfAllUtrnProcessed.set("N");
			goTo(GotoLabel.exit2190);
		}
		wsaaTotalWdlAmount.add(utrnrgwIO.getContractAmount());
	}

protected void readNextUtrn2110()
	{
		utrnrgwIO.setFunction(varcom.nextr);
		readUtrn2700();
	}

protected void readUtrn2700()
	{
		/*READ*/
		SmartFileCode.execute(appVars, utrnrgwIO);
		if ((isNE(utrnrgwIO.getStatuz(), varcom.oK))
		&& (isNE(utrnrgwIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(utrnrgwIO.getParams());
			syserrrec.statuz.set(utrnrgwIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(regpudlIO.getChdrnum(), utrnrgwIO.getChdrnum()))
		|| (isNE(regpudlIO.getChdrcoy(), utrnrgwIO.getChdrcoy()))
		|| (isNE(utrnupdIO.getTranno(), utrnrgwIO.getTranno()))
		|| (isEQ(utrnrgwIO.getStatuz(), varcom.endp))) {
			utrnrgwIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void postPending3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* MOVE SPACES                 TO LIFA-LIFACMV-REC.             */
		wsaaJrnseq.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.rdocnum.set(regpudlIO.getChdrnum());
		lifacmvrec1.tranno.set(utrnupdIO.getTranno());
		/* MOVE T5645-SACSCODE-01      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-01      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-01         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-01          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-01        TO LIFA-CONTOT.                  */
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		lifacmvrec1.rldgcoy.set(regpudlIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(regpudlIO.getChdrcoy());
		/* MOVE REGPUDL-CHDRNUM        TO LIFA-RLDGACCT.                */
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaRldgChdrnum.set(regpudlIO.getChdrnum());
			wsaaRldgLife.set(regpudlIO.getLife());
			wsaaRldgCoverage.set(regpudlIO.getCoverage());
			wsaaRldgRider.set(regpudlIO.getRider());
			wsaaPlan.set(regpudlIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.sacscode.set(t5645rec.sacscode05);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec1.glcode.set(t5645rec.glmap05);
			lifacmvrec1.glsign.set(t5645rec.sign05);
			lifacmvrec1.contot.set(t5645rec.cnttot05);
			lifacmvrec1.substituteCode[6].set(regpudlIO.getCrtable());
		}
		else {
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(regpudlIO.getChdrnum());
			lifacmvrec1.sacscode.set(t5645rec.sacscode01);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec1.glcode.set(t5645rec.glmap01);
			lifacmvrec1.glsign.set(t5645rec.sign01);
			lifacmvrec1.contot.set(t5645rec.cnttot01);
			lifacmvrec1.substituteCode[6].set(SPACES);
		}
		lifacmvrec1.origcurr.set(regpudlIO.getCurrcd());
		lifacmvrec1.origamt.set(wsaaTotalWdlAmount);
		lifacmvrec1.genlcur.set(wsaaLedgerCcy);
		compute(lifacmvrec1.acctamt, 9).set(mult(wsaaTotalWdlAmount, wsaaNominalRate));
		zrdecplrec.amountIn.set(lifacmvrec1.acctamt);
		zrdecplrec.currency.set(wsaaLedgerCcy);
		a000CallRounding();
		lifacmvrec1.acctamt.set(zrdecplrec.amountOut);
		lifacmvrec1.crate.set(wsaaNominalRate);
		lifacmvrec1.postyear.set(utrnupdIO.getBatcactyr());
		lifacmvrec1.postmonth.set(utrnupdIO.getBatcactmn());
		lifacmvrec1.tranref.set(regpudlIO.getChdrnum());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(utrnupdIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrrgpIO.getCnttype());
		/* MOVE PARM-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.                  */
		/* MOVE VRCM-USER              TO LIFA-USER.                    */
		/* MOVE VRCM-TIME              TO LIFA-TRANSACTION-TIME.        */
		/* MOVE VRCM-DATE              TO LIFA-TRANSACTION-DATE.        */
		lifacmvrec1.user.set(999999);
		lifacmvrec1.transactionTime.set(getCobolTime());
		lifacmvrec1.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
	}

protected void postWdlFee3100()
	{
		start3110();
	}

protected void start3110()
	{
		/* MOVE SPACES                 TO LIFA-LIFACMV-REC.             */
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.rdocnum.set(regpudlIO.getChdrnum());
		lifacmvrec1.tranno.set(utrnupdIO.getTranno());
		/* MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-02         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-02          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.                  */
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		lifacmvrec1.rldgcoy.set(regpudlIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(regpudlIO.getChdrcoy());
		/* MOVE REGPUDL-CHDRNUM        TO LIFA-RLDGACCT.                */
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaRldgChdrnum.set(regpudlIO.getChdrnum());
			wsaaRldgLife.set(regpudlIO.getLife());
			wsaaRldgCoverage.set(regpudlIO.getCoverage());
			wsaaRldgRider.set(regpudlIO.getRider());
			wsaaPlan.set(regpudlIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.sacscode.set(t5645rec.sacscode06);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec1.glcode.set(t5645rec.glmap06);
			lifacmvrec1.glsign.set(t5645rec.sign06);
			lifacmvrec1.contot.set(t5645rec.cnttot06);
			lifacmvrec1.substituteCode[6].set(regpudlIO.getCrtable());
		}
		else {
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(regpudlIO.getChdrnum());
			lifacmvrec1.sacscode.set(t5645rec.sacscode02);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec1.glcode.set(t5645rec.glmap02);
			lifacmvrec1.glsign.set(t5645rec.sign02);
			lifacmvrec1.contot.set(t5645rec.cnttot02);
			lifacmvrec1.substituteCode[6].set(SPACES);
		}
		lifacmvrec1.origcurr.set(regpudlIO.getCurrcd());
		lifacmvrec1.origamt.set(wsaaTotFee);
		lifacmvrec1.genlcur.set(wsaaLedgerCcy);
		compute(lifacmvrec1.acctamt, 9).set(mult(wsaaTotFee, wsaaNominalRate));
		zrdecplrec.amountIn.set(lifacmvrec1.acctamt);
		zrdecplrec.currency.set(wsaaLedgerCcy);
		a000CallRounding();
		lifacmvrec1.acctamt.set(zrdecplrec.amountOut);
		lifacmvrec1.crate.set(wsaaNominalRate);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(regpudlIO.getChdrnum());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(utrnupdIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrrgpIO.getCnttype());
		/* MOVE PARM-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.                  */
		/* MOVE VRCM-USER              TO LIFA-USER.                    */
		/* MOVE VRCM-TIME              TO LIFA-TRANSACTION-TIME.        */
		/* MOVE VRCM-DATE              TO LIFA-TRANSACTION-DATE.        */
		lifacmvrec1.user.set(999999);
		lifacmvrec1.transactionTime.set(getCobolTime());
		lifacmvrec1.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
	}

protected void postWdlBal3200()
	{
		start3210();
	}

protected void start3210()
	{
		/* MOVE SPACES                 TO LIFA-LIFACMV-REC.             */
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.rdocnum.set(regpudlIO.getChdrnum());
		lifacmvrec1.tranno.set(utrnupdIO.getTranno());
		/* MOVE T5645-SACSCODE-03      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-03      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-03         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-03          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-03        TO LIFA-CONTOT.                  */
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		lifacmvrec1.rldgcoy.set(regpudlIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(regpudlIO.getChdrcoy());
		/* MOVE REGPUDL-CHDRNUM        TO LIFA-RLDGACCT.                */
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaRldgChdrnum.set(regpudlIO.getChdrnum());
			wsaaRldgLife.set(regpudlIO.getLife());
			wsaaRldgCoverage.set(regpudlIO.getCoverage());
			wsaaRldgRider.set(regpudlIO.getRider());
			wsaaPlan.set(regpudlIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.sacscode.set(t5645rec.sacscode07);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec1.glcode.set(t5645rec.glmap07);
			lifacmvrec1.glsign.set(t5645rec.sign07);
			lifacmvrec1.contot.set(t5645rec.cnttot07);
			lifacmvrec1.substituteCode[6].set(regpudlIO.getCrtable());
		}
		else {
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(regpudlIO.getChdrnum());
			lifacmvrec1.sacscode.set(t5645rec.sacscode03);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec1.glcode.set(t5645rec.glmap03);
			lifacmvrec1.glsign.set(t5645rec.sign03);
			lifacmvrec1.contot.set(t5645rec.cnttot03);
			lifacmvrec1.substituteCode[6].set(SPACES);
		}
		lifacmvrec1.origcurr.set(regpudlIO.getCurrcd());
		lifacmvrec1.origamt.set(wsaaNetVal);
		lifacmvrec1.genlcur.set(wsaaLedgerCcy);
		compute(lifacmvrec1.acctamt, 9).set(mult(wsaaNetVal, wsaaNominalRate));
		zrdecplrec.amountIn.set(lifacmvrec1.acctamt);
		zrdecplrec.currency.set(wsaaLedgerCcy);
		a000CallRounding();
		lifacmvrec1.acctamt.set(zrdecplrec.amountOut);
		lifacmvrec1.crate.set(wsaaNominalRate);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(regpudlIO.getChdrnum());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(utrnupdIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrrgpIO.getCnttype());
		/* MOVE PARM-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.                  */
		/* MOVE VRCM-TIME              TO LIFA-TRANSACTION-TIME.        */
		/* MOVE VRCM-DATE              TO LIFA-TRANSACTION-DATE.        */
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
	}

protected void callPayreq3400()
	{
		setUp3400();
	}

protected void setUp3400()
	{
		payreqrec.rec.set(SPACES);
		payreqrec.function.set("NPSTW");
		/*MOVE BATD-BATCHKEY          TO PAYREQ-BATCKEY.               */
		/* MOVE BATD-PREFIX            TO PAYREQ-BATCPFX.               */
		/* MOVE PARM-COMPANY           TO PAYREQ-BATCCOY.          <007>*/
		/* MOVE PARM-BRANCH            TO PAYREQ-BATCBRN.          <007>*/
		/* MOVE PARM-ACCTYEAR          TO PAYREQ-BATCACTYR.        <007>*/
		/* MOVE PARM-ACCTMONTH         TO PAYREQ-BATCACTMN.        <007>*/
		payreqrec.batccoy.set(batcdorrec.company);
		payreqrec.batcbrn.set(batcdorrec.branch);
		payreqrec.batcactyr.set(batcdorrec.actyear);
		payreqrec.batcactmn.set(batcdorrec.actmonth);
		payreqrec.batctrcde.set(batcdorrec.trcde);
		payreqrec.batcbatch.set(batcdorrec.batch);
		/*MOVE PARM-COMPANY           TO PAYREQ-COMPANY.          <002>*/
		/*MOVE PARM-BRANCH            TO PAYREQ-BRANCH.           <002>*/
		payreqrec.effdate.set(utrnupdIO.getMoniesDate());
		payreqrec.paycurr.set(regpudlIO.getCurrcd());
		payreqrec.bankcode.set(t3629rec.bankcode);
		payreqrec.pymt.set(wsaaNetVal);
		/*MOVE CHDRRGP-CNTTYPE        TO PAYREQ-CNTTYPE.               */
		/*MOVE UTRNUPD-TRANNO         TO PAYREQ-TRANNO.                */
		/*IF REGPUDL-BANKKEY       NOT = SPACES                        */
		/*   MOVE PRFX-BANK-TAPE      TO PAYREQ-CHEQPFX                */
		/*ELSE                                                         */
		/*   MOVE PRFX-SEMI-CHEQ      TO PAYREQ-CHEQPFX                */
		/*END-IF.                                                      */
		payreqrec.trandesc.set(descIO.getLongdesc());
		payreqrec.bankkey.set(regpudlIO.getBankkey());
		payreqrec.bankacckey.set(regpudlIO.getBankacckey());
		/*  MOVE T5645-SACSCODE-04      TO PAYREQ-SACSCODE.              */
		/*  MOVE T5645-SACSTYPE-04      TO PAYREQ-SACSTYPE.              */
		/*  MOVE T5645-GLMAP-04         TO PAYREQ-GLCODE.                */
		/*  MOVE T5645-SIGN-04          TO PAYREQ-SIGN.                  */
		/* MOVE REGPUDL-CHDRNUM        TO PAYREQ-FRM-RLDGACCT.     <002>*/
		/*MOVE VRCM-USER              TO PAYREQ-USER.             <002>*/
		/* MOVE 999999                 TO PAYREQ-USER.          <LA1189>*/
		payreqrec.user.set(udtrigrec.user);
		/* MOVE VRCM-TERMID            TO PAYREQ-TERMID.           <002>*/
		payreqrec.termid.set(SPACES);
		/*MOVE PRFX-CLNT              TO PAYREQ-TO-RLDGPFX.            */
		/*MOVE REGPUDL-PAYCOY         TO PAYREQ-TO-RLDGCOY.            */
		/*MOVE PARM-ACCTMONTH         TO PAYREQ-POSTMONTH.        <002>*/
		/*MOVE PARM-ACCTYEAR          TO PAYREQ-POSTYEAR.         <002)*/
		/*MOVE REGPUDL-RGPYMOP        TO PAYREQ-REQNTYPE.         <002>*/
		payreqrec.reqntype.set(t6694rec.reqntype);
		/* MOVE PARM-FSUCO             TO PAYREQ-CLNTCOY.          <002>*/
		payreqrec.clntcoy.set(udtrigrec.fsuco);
		payreqrec.clntnum.set(regpudlIO.getPayclt());
		payreqrec.language.set(udtrigrec.language);
		/* Set up Component parameters for PAYREQ if Component level       */
		/*  accounting is required.                                        */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaRldgacct.set(SPACES);
			payreqrec.frmRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(regpudlIO.getChdrnum());
			wsaaRldgLife.set(regpudlIO.getLife());
			wsaaRldgCoverage.set(regpudlIO.getCoverage());
			wsaaRldgRider.set(regpudlIO.getRider());
			wsaaPlan.set(regpudlIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			payreqrec.frmRldgacct.set(wsaaRldgacct);
			/* Use new procedure division copybook SUBCODE to perform the      */
			/*  substitution in the GLCODE field before it is passed to        */
			/*  PAYREQ and written to the PREQ file.                           */
			subcoderec.codeRec.set(SPACES);
			subcoderec.code[1].set(chdrrgpIO.getCnttype());
			subcoderec.code[6].set(regpudlIO.getCrtable());
			subcoderec.glcode.set(t5645rec.glmap08);
			subcode();
			payreqrec.glcode.set(subcoderec.glcode);
			payreqrec.sacscode.set(t5645rec.sacscode08);
			payreqrec.sacstype.set(t5645rec.sacstype08);
			payreqrec.sign.set(t5645rec.sign08);
			payreqrec.cnttot.set(t5645rec.cnttot08);
		}
		else {
			wsaaRldgacct.set(SPACES);
			payreqrec.frmRldgacct.set(SPACES);
			payreqrec.frmRldgacct.set(regpudlIO.getChdrnum());
			/* Use new procedure division copybook SUBCODE to perform the      */
			/*  substitution in the GLCODE field before it is passed to        */
			/*  PAYREQ and written to the PREQ file.                           */
			subcoderec.codeRec.set(SPACES);
			subcoderec.code[1].set(chdrrgpIO.getCnttype());
			subcoderec.glcode.set(t5645rec.glmap04);
			subcode();
			payreqrec.glcode.set(subcoderec.glcode);
			payreqrec.sacscode.set(t5645rec.sacscode04);
			payreqrec.sacstype.set(t5645rec.sacstype04);
			payreqrec.sign.set(t5645rec.sign04);
			payreqrec.cnttot.set(t5645rec.cnttot04);
		}
		payreqrec.tranref.set(regpudlIO.getChdrnum());
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(payreqrec.statuz);
			/*MOVE 'PAYREQ'            TO WSAA-ERROR-FORMAT             */
			fatalError9000();
		}
	}

protected void subcode()
	{
		sbcd100Start();
	}

protected void sbcd100Start()
	{
		if (isNE(subcoderec.code[1], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[1]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[2], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[2]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[3], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[3]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[4], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[4]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[5], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[5]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[6], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[6]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
		}
	}

protected void intTrans3500()
	{
		fromAcmv3510();
		toAcmv3530();
	}

protected void fromAcmv3510()
	{
		/* MOVE SPACES                 TO LIFA-LIFACMV-REC.             */
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.tranno.set(utrnupdIO.getTranno());
		/* MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-SIGN-04          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-GLMAP-04         TO LIFA-GLCODE.                  */
		lifacmvrec1.jrnseq.set(1);
		lifacmvrec1.rldgcoy.set(regpudlIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(regpudlIO.getChdrcoy());
		/* MOVE REGPUDL-CHDRNUM        TO LIFA-RLDGACCT                 */
		/*                                LIFA-RDOCNUM.                 */
		lifacmvrec1.rdocnum.set(regpudlIO.getChdrnum());
		/* Set up Component accounting parameters if required              */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaRldgChdrnum.set(regpudlIO.getChdrnum());
			wsaaRldgLife.set(regpudlIO.getLife());
			wsaaRldgCoverage.set(regpudlIO.getCoverage());
			wsaaRldgRider.set(regpudlIO.getRider());
			wsaaPlan.set(regpudlIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.sacscode.set(t5645rec.sacscode08);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec1.glcode.set(t5645rec.glmap08);
			lifacmvrec1.glsign.set(t5645rec.sign08);
			lifacmvrec1.contot.set(t5645rec.cnttot08);
			lifacmvrec1.substituteCode[6].set(regpudlIO.getCrtable());
		}
		else {
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(regpudlIO.getChdrnum());
			lifacmvrec1.sacscode.set(t5645rec.sacscode04);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec1.glcode.set(t5645rec.glmap04);
			lifacmvrec1.glsign.set(t5645rec.sign04);
			lifacmvrec1.contot.set(t5645rec.cnttot04);
			lifacmvrec1.substituteCode[6].set(SPACES);
		}
		lifacmvrec1.origcurr.set(regpudlIO.getCurrcd());
		lifacmvrec1.origamt.set(wsaaNetVal);
		lifacmvrec1.genlcur.set(wsaaLedgerCcy);
		compute(lifacmvrec1.acctamt, 9).set(mult(wsaaNetVal, wsaaNominalRate));
		zrdecplrec.amountIn.set(lifacmvrec1.acctamt);
		zrdecplrec.currency.set(wsaaLedgerCcy);
		a000CallRounding();
		lifacmvrec1.acctamt.set(zrdecplrec.amountOut);
		lifacmvrec1.crate.set(wsaaNominalRate);
		lifacmvrec1.postyear.set(utrnupdIO.getBatcactyr());
		lifacmvrec1.postmonth.set(utrnupdIO.getBatcactmn());
		lifacmvrec1.tranref.set(regpudlIO.getChdrnum());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(utrnupdIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrrgpIO.getCnttype());
		/* MOVE PARM-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.                  */
		/* MOVE VRCM-USER              TO LIFA-USER.                    */
		/* ACCEPT LIFA-TRANSACTION-TIME FROM TIME.                      */
		/* ACCEPT LIFA-TRANSACTION-DATE FROM DATE.                      */
		lifacmvrec1.user.set(999999);
		lifacmvrec1.transactionTime.set(getCobolTime());
		lifacmvrec1.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
	}

protected void toAcmv3530()
	{
		/* MOVE SPACES                 TO LIFA-LIFACMV-REC.             */
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.rdocnum.set(regpudlIO.getChdrnum());
		lifacmvrec1.tranno.set(utrnupdIO.getTranno());
		lifacmvrec1.sacscode.set(regpudlIO.getSacscode());
		lifacmvrec1.sacstyp.set(regpudlIO.getSacstype());
		lifacmvrec1.glsign.set(regpudlIO.getDebcred());
		lifacmvrec1.glcode.set(regpudlIO.getGlact());
		lifacmvrec1.jrnseq.set(1);
		lifacmvrec1.rldgcoy.set(regpudlIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(regpudlIO.getChdrcoy());
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(regpudlIO.getDestkey());
		lifacmvrec1.origcurr.set(regpudlIO.getCurrcd());
		lifacmvrec1.origamt.set(wsaaNetVal);
		lifacmvrec1.genlcur.set(wsaaLedgerCcy);
		compute(lifacmvrec1.acctamt, 9).set(mult(wsaaNetVal, wsaaNominalRate));
		lifacmvrec1.crate.set(wsaaNominalRate);
		/* MOVE PARM-ACCTYEAR          TO LIFA-POSTYEAR.                */
		/* MOVE PARM-ACCTMONTH         TO LIFA-POSTMONTH.               */
		lifacmvrec1.postyear.set(udtrigrec.actyear);
		lifacmvrec1.postmonth.set(udtrigrec.actmonth);
		lifacmvrec1.tranref.set(regpudlIO.getChdrnum());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(utrnupdIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrrgpIO.getCnttype());
		lifacmvrec1.substituteCode[6].set(SPACES);
		/* MOVE PARM-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.                  */
		/* MOVE VRCM-USER              TO LIFA-USER.                    */
		/* ACCEPT LIFA-TRANSACTION-TIME FROM TIME.                      */
		/* ACCEPT LIFA-TRANSACTION-DATE FROM DATE.                      */
		lifacmvrec1.user.set(999999);
		lifacmvrec1.transactionTime.set(getCobolTime());
		lifacmvrec1.transactionDate.set(getCobolDate());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
	}

protected void setupWithdrawalFee4000()
	{
		start4000();
	}

protected void start4000()
	{
		wsaaTotFee.set(0);
		readT55424100();
		if (isNE(wsaaFfamt, ZERO)) {
			wsaaTotFee.set(wsaaFfamt);
			return ;
		}
		if (isGT(wsaaFeepc, 0)) {
			compute(wsaaTotFee, 3).set(div(mult(wsaaTotalWdlAmount, wsaaFeepc), 100));
		}
		zrdecplrec.amountIn.set(wsaaTotFee);
		zrdecplrec.currency.set(regpudlIO.getCurrcd());
		a000CallRounding();
		wsaaTotFee.set(zrdecplrec.amountOut);
		if (isGT(wsaaTotFee, wsaaFeemax)) {
			wsaaTotFee.set(wsaaFeemax);
		}
		else {
			if (isLT(wsaaTotFee, wsaaFeemin)) {
				wsaaTotFee.set(wsaaFeemin);
			}
		}
	}

protected void readT55424100()
	{
		start4100();
	}

protected void start4100()
	{
		/* Read the Table T5542 to obtain the withdrawal information*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(regpudlIO.getChdrcoy());
		itdmIO.setItemtabl(t5542);
		wsaaT5542Wdmeth.set(t5540rec.wdmeth);
		wsaaT5542Currcd.set(regpudlIO.getCurrcd());
		itdmIO.setItemitem(wsaaT5542Key);
		/* MOVE PARM-EFFDATE           TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(udtrigrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(), regpudlIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), t5542))
		|| (isNE(itdmIO.getItemitem(), wsaaT5542Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemitem(wsaaT5542Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		else {
			t5542rec.t5542Rec.set(itdmIO.getGenarea());
		}
		/* IF  REGPUDL-REGPAYFREQ = '00'*/
		/*     MOVE 1 TO SUB1*/
		/* END-IF.*/
		/* IF  REGPUDL-REGPAYFREQ = '01'*/
		/*     MOVE 2 TO SUB1*/
		/* END-IF.*/
		/* IF  REGPUDL-REGPAYFREQ = '02'*/
		/*     MOVE 3 TO SUB1*/
		/* END-IF.*/
		/* IF  REGPUDL-REGPAYFREQ = '04'*/
		/*     MOVE 4 TO SUB1*/
		/* END-IF.*/
		/* IF REGPUDL-REGPAYFREQ  = '12'*/
		/*     MOVE 5 TO SUB1*/
		/* END-IF.*/
		/* MOVE T5542-FEEMAX (SUB1)    TO WSAA-FEEMAX.*/
		/* MOVE T5542-FEEMIN (SUB1)    TO WSAA-FEEMIN.*/
		/* MOVE T5542-FEEPC  (SUB1)    TO WSAA-FEEPC.*/
		/* MOVE T5542-FFAMT  (SUB1)    TO WSAA-FFAMT.*/
		/* MOVE T5542-WDL-AMOUNT(SUB1) TO WSAA-WDL-AMOUNT.*/
		/* MOVE T5542-WDREM (SUB1)     TO WSAA-WDREM.*/
		/* Check frequency with those on T5542.                            */
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			if (isEQ(regpudlIO.getRegpayfreq(), t5542rec.billfreq[wsaaSub.toInt()])) {
				wsaaFeemax.set(t5542rec.feemax[wsaaSub.toInt()]);
				wsaaFeemin.set(t5542rec.feemin[wsaaSub.toInt()]);
				wsaaFeepc.set(t5542rec.feepc[wsaaSub.toInt()]);
				wsaaFfamt.set(t5542rec.ffamt[wsaaSub.toInt()]);
				wsaaSub.set(10);
				return ;
			}
		}
		/* IF WSAA-SUB   > 9                                    <LA4555>*/
		if (isGT(wsaaSub, 6)) {
			syserrrec.statuz.set(h388);
			fatalError9000();
		}
	}

protected void printReport5000()
	{
		start5000();
	}

protected void start5000()
	{
		if (isGT(wsaaLineCount, wsaaPageSize)) {
			printHeading5100();
		}
		rgwerd01RecordInner.chdrnum.set(regpudlIO.getChdrnum());
		rgwerd01RecordInner.seqnumb.set(regpudlIO.getRgpynum());
		rgwerd01RecordInner.crtable.set(utrnupdIO.getCrtable());
		rgwerd01RecordInner.life.set(regpudlIO.getLife());
		rgwerd01RecordInner.coverage.set(regpudlIO.getCoverage());
		rgwerd01RecordInner.rider.set(regpudlIO.getRider());
		rgwerd01RecordInner.plnsfx.set(regpudlIO.getPlanSuffix());
		rgwerd01RecordInner.standamt.set(wsaaNetVal);
		rgwerd01RecordInner.ffamt.set(wsaaTotFee);
		rgwerd01RecordInner.currcd.set(regpudlIO.getCurrcd());
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(utrnupdIO.getMoniesDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		rgwerd01RecordInner.npaydate.set(datcon1rec.extDate);
		/* MOVE WSAA-PAYEENAME         TO PNAMES   OF RGWERD01-O.       */
		dbcstrncpy.dbcsInputString.set(wsaaPayeename);
		dbcstrncpy.dbcsOutputLength.set(wsaaPnamesLen);
		dbcstrncpy.dbcsStatuz.set(SPACES);
		dbcsTrnc(dbcstrncpy.rec);
		if (isNE(dbcstrncpy.dbcsStatuz, "****")) {
			dbcstrncpy.dbcsOutputString.set(SPACES);
		}
		rgwerd01RecordInner.pnames.set(dbcstrncpy.dbcsOutputString);
		printerFile.printRgwerd01(rgwerd01RecordInner.rgwerd01Record);
		wsaaLineCount.add(1);
	}

protected void printHeading5100()
	{
		start5100();
	}

protected void start5100()
	{
		datcon1rec.function.set("CONV");
		/* MOVE PARM-EFFDATE           TO DTC1-INT-DATE.                */
		datcon1rec.intDate.set(udtrigrec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		repdate.set(datcon1rec.extDate);
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(wsaaToday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		sdate.set(datcon1rec.extDate);
		/* MOVE PARM-COMPANY           TO COMPANY   OF RGWERH01-O       */
		company.set(udtrigrec.company);
		companynm.set(wsaaCompanynm);
		printerFile.printRgwerh01(rgwerh01Record);
		wsaaLineCount.set(12);
	}

protected void setupPayeeName5200()
	{
		para5200();
	}

protected void para5200()
	{
		wsaaPayeename.set(SPACES);
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(regpudlIO.getPayclt());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(regpudlIO.getPaycoy());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(), varcom.oK))
		&& (isNE(cltsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError9000();
		}
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(), "1"))) {
			wsaaPayeename.set(SPACES);
			return ;
		}
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname5300();
			return ;
		}
		wsaaPayeename.set(SPACES);
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsaaPayeename);
		}
		else {
			wsaaPayeename.set(cltsIO.getSurname());
		}
	}

protected void corpname5300()
	{
		/*START*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getSurname());
		stringVariable1.addExpression(cltsIO.getGivname(), "  ");
		stringVariable1.setStringInto(wsaaPayeename);
		/*EXIT*/
	}

protected void readT66946000()
	{
		read6000();
	}

protected void read6000()
	{
		itdmIO.setParams(SPACES);
		/* MOVE PARM-COMPANY           TO ITDM-ITEMCOY.            <004>*/
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(regpudlIO.getRgpymop());
		itdmIO.setItmfrm("99999999");
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isEQ(itdmIO.getStatuz(), varcom.endp))
		|| (isNE(itdmIO.getItemcoy(), udtrigrec.company))
		|| (isNE(itdmIO.getItempfx(), "IT"))
		|| (isNE(itdmIO.getItemtabl(), t6694))
		|| (isNE(itdmIO.getItemitem(), regpudlIO.getRgpymop()))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		else {
			t6694rec.t6694Rec.set(itdmIO.getGenarea());
		}
	}

protected void getCurrencyRate7000()
	{
		try {
			start7000();
			reRead7120();
			findRate7150();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start7000()
	{
		itemIO.setItempfx("IT");
		/* MOVE PARM-COMPANY           TO ITEM-ITEMCOY.                 */
		itemIO.setItemcoy(udtrigrec.company);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(regpudlIO.getCurrcd());
	}

protected void reRead7120()
	{
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !((isNE(wsaaNominalRate, ZERO))
		|| (isGT(wsaaX, 7)))) {
			findRate7150();
		}
		
		if (isEQ(wsaaNominalRate, ZERO)) {
			if (isNE(t3629rec.contitem, SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				reRead7120();
				return ;
			}
			else {
				syserrrec.statuz.set("MRNF");
				fatalError9000();
			}
		}
		else {
			goTo(GotoLabel.exit7190);
		}
	}

protected void findRate7150()
	{
		/* IF (PARM-EFFDATE         NOT < T3629-FRMDATE(WSAA-X)) AND    */
		/*    (PARM-EFFDATE         NOT > T3629-TODATE(WSAA-X))         */
		if ((isGTE(udtrigrec.effdate, t3629rec.frmdate[wsaaX.toInt()]))
		&& (isLTE(udtrigrec.effdate, t3629rec.todate[wsaaX.toInt()]))) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
		}
		else {
			wsaaX.add(1);
		}
	}

protected void fatalError9000()
	{
		/*ERROR*/
		callProgram(Syserr.class, syserrrec.syserrRec);
		udtrigrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(udtrigrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserrrec.statuz.set(zrdecplrec.statuz);
			fatalError9000();
		}
		/*A000-EXIT*/
	}
/*
 * Class transformed  from Data Structure RGWERD01-RECORD--INNER
 */
private static final class Rgwerd01RecordInner { 

	private FixedLengthStringData rgwerd01Record = new FixedLengthStringData(92);
	private FixedLengthStringData rgwerd01O = new FixedLengthStringData(92).isAPartOf(rgwerd01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rgwerd01O, 0);
	private ZonedDecimalData seqnumb = new ZonedDecimalData(3, 0).isAPartOf(rgwerd01O, 8);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rgwerd01O, 11);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rgwerd01O, 15);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rgwerd01O, 17);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rgwerd01O, 19);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(rgwerd01O, 21);
	private ZonedDecimalData standamt = new ZonedDecimalData(17, 2).isAPartOf(rgwerd01O, 25);
	private ZonedDecimalData ffamt = new ZonedDecimalData(17, 2).isAPartOf(rgwerd01O, 42);
	private FixedLengthStringData currcd = new FixedLengthStringData(3).isAPartOf(rgwerd01O, 59);
	private FixedLengthStringData npaydate = new FixedLengthStringData(10).isAPartOf(rgwerd01O, 62);
	private FixedLengthStringData pnames = new FixedLengthStringData(20).isAPartOf(rgwerd01O, 72);
}
}
