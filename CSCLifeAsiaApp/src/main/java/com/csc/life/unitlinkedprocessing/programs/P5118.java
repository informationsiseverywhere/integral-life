/*
 * File: P5118.java
 * Date: 30 August 2009 0:08:59
 * Author: Quipoz Limited
 * 
 * Class transformed from P5118.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgpTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S5118ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*  P5118 - Regular Withdrawal Plan/Policy Selection.
*  -------------------------------------------------
*
*
*  This screen will be used to select the  whole  plan  or  the
*  policy  within  the  plan  that will be used for the Regular
*  Withdrawal.  At this stage the user will be able  to  choose
*  to  make  the  withdrawal from either the whole plan or from
*  one of the policies within the plan. Only one may be chosen.
*
*  If there is only  one  policy  within  the  plan  then  this
*  screen  will  be by-passed and processing will proceed as if
*  that one policy was selected.
*
*  If the transaction is not creation then this screen will  be
*  skipped  altogether, as the program list obtained from T5671
*  in the submenu program will not contain this program.
*
*
*  Initialise.
*  -----------
*
*  Clear the subfile ready for loading.
*
*  Perform a RETRV on CHDRRGP.
*
*  Perform a RETRV on REGP.
*
*  Display  the  screen  heading  details  and  look   up   the
*  following descriptions and names:
*
*     Contract Type, (CNTTYPE) - long description from T5688,
*
*     Contract  Status,  (STATCODE)  -  short description
*     from T3623,
*
*     Premium  Status,  (PSTATCODE)  -  short description from
*     T3588,
*
*     The owner's client (CLTS) details.
*
*     The Life Assured client (CLTS) details.
*
*
*  If  the  number of policies in plan is one or zero then plan
*  processing does not apply. If this is the  case  then  place
*  the  value  of  the  plan suffix from the contract header in
*  REGP and skip the rest of this section.
*
*  If plan processing is applicable then  the  policies  within
*  the  plan  will  be  displayed  in  descending sequence. The
*  first line of the subfile will indicate  the whole plan  and
*  this  will  be  followed  by  the highest plan suffix in the
*  plan and then, subsequently, all the  others  in  descending
*  order.  In  the  Status Description field for the first line
*  place the literal 'Whole  Plan'.  On  the  other  lines  the
*  Risk  Status Code and Premium Status Code will be displayed.
*  The Risk Status code will be used to read  table  T5682  and
*  the long description displayed against it.
*
*  In  the  case  of a plan with summarised details the program
*  will logically 'break out' the summarised policies.
*
*  In all cases, load all pages required in the subfile .
*
*
*  Validation.
*  -----------
*
*  If plan processing is not  applicable  or  only  one  record
*  written to the subfile skip this section.
*
*  Display the screen using the screen I/O module.
*
*  If  SCRN-STATUZ  equals  anything  other  than  O-K  or CALC
*  redisplay  the  screen  with  an  error  message.  Read  all
*  modified  subfile  records.  Only one selection may be made.
*  If  there  are  more  than  one  highlight  the  second  and
*  subsequent ones as being in error.
*
*  Updating.
*  ---------
*
*  No update is required in this section.
*
*
*  Next Program.
*  -------------
*
*  If  only one policy, perform a KEEPS on the REGP record, add
*  1 to the program pointer and exit the program.
*
*  If a record has been selected, perform a KEEPS on  the  REGP
*  record.
*
*  Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5118 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5118");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaPoliciesProtected = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaSelected = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaActualWritten = new PackedDecimalData(3, 0);
	private String wsaaCovStatExit = "";
	private String wsaaPremStatExit = "";
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0);
	private String wsaaFirstTime = "";

	private FixedLengthStringData wsaaPlan = new FixedLengthStringData(1);
	private Validator planNotSelected = new Validator(wsaaPlan, " ");
	private Validator planSelected = new Validator(wsaaPlan, "Y");

	private FixedLengthStringData wsaaEofFlag = new FixedLengthStringData(1);
	private Validator wsaaEofExit = new Validator(wsaaEofFlag, "Y");
		/* ERRORS */
	private String e304 = "E304";
	private String f388 = "F388";
	private String g634 = "G634";
	private String h080 = "H080";
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5679 = "T5679";
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5688 = "T5688";
		/* FORMATS */
	private String chdrrgprec = "CHDRRGPREC";
	private String regprec = "REGPREC   ";
		/*Regular Payments View.*/
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Components for Regular Withdrawals.*/
	private CovrrgpTableDAM covrrgpIO = new CovrrgpTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Regular Payments File*/
	private RegpTableDAM regpIO = new RegpTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5118ScreenVars sv = ScreenProgram.getScreenVars( S5118ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		exit1190, 
		exit1390, 
		exit1490, 
		preExit, 
		updateErrorIndicators2640, 
		exit2690, 
		exit3090, 
		exit4090
	}

	public P5118() {
		super();
		screenVars = sv;
		new ScreenModel("S5118", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{	
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars,"IT");
		if(cntDteFlag)	{
		sv.iljCntDteFlag.set("Y");
		} else {
		sv.iljCntDteFlag.set("N");
		}
		//ILJ-49 End 
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaPlanSuffix.set(ZERO);
		wsaaPoliciesProtected.set(ZERO);
		wsaaSelected.set(ZERO);
		wsaaActualWritten.set(ZERO);
		wsaaPlan.set(SPACES);
		wsaaEofFlag.set(SPACES);
		wsaaFirstTime = "Y";
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5118", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.polinc.set(ZERO);
		chdrrgpIO.setFunction(varcom.retrv);
		chdrrgpIO.setFormat(chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrrgpIO.getPolinc(),1)) {
			goTo(GotoLabel.exit1090);
		}
		scrnparams.subfileRrn.set(1);
		sv.planSuffix.set(ZERO);
		sv.rstatdesc.set("Whole Plan Selected");
		scrnparams.function.set(varcom.sadd);
		processScreen("S5118", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.selectOut[varcom.pr.toInt()].set("N");
		wsaaEofFlag.set("N");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		covrrgpIO.setDataArea(SPACES);
		covrrgpIO.setChdrcoy(regpIO.getChdrcoy());
		covrrgpIO.setChdrnum(regpIO.getChdrnum());
		covrrgpIO.setLife(regpIO.getLife());
		covrrgpIO.setCoverage(regpIO.getCoverage());
		covrrgpIO.setRider(regpIO.getRider());
		covrrgpIO.setPlanSuffix(9999);
		covrrgpIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		covrrgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrrgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		
		while ( !(wsaaEofExit.isTrue())) {
			loadCovrSubfile1100();
		}
		
		sv.crtable.set(regpIO.getCrtable());
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		descIO.setDescitem(chdrrgpIO.getCnttype());
		descIO.setDesctabl(t5688);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.polinc.set(chdrrgpIO.getPolinc());
		sv.currcd.set(chdrrgpIO.getCntcurr());
		sv.occdate.set(chdrrgpIO.getOccdate());
		sv.ptdate.set(chdrrgpIO.getPtdate());
		sv.btdate.set(chdrrgpIO.getBtdate());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrrgpIO.getStatcode());
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrrgpIO.getPstatcode());
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrnum(chdrrgpIO.getChdrnum());
		lifemjaIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK))
		|| (isNE(lifemjaIO.getChdrcoy(),chdrrgpIO.getChdrcoy()))
		|| (isNE(lifemjaIO.getChdrnum(),chdrrgpIO.getChdrnum()))) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		callCltsio1600();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),"1"))) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		cltsIO.setClntnum(chdrrgpIO.getCownnum());
		sv.cownnum.set(chdrrgpIO.getCownnum());
		callCltsio1600();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),"1"))) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void loadCovrSubfile1100()
	{
		try {
			readPlanRecs1110();
		}
		catch (GOTOException e){
		}
	}

protected void readPlanRecs1110()
	{
		SmartFileCode.execute(appVars, covrrgpIO);
		if ((isNE(covrrgpIO.getStatuz(),varcom.oK))
		&& (isNE(covrrgpIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrrgpIO.getParams());
			fatalError600();
		}
		if ((isNE(covrrgpIO.getChdrcoy(),regpIO.getChdrcoy()))
		|| (isNE(covrrgpIO.getChdrnum(),regpIO.getChdrnum()))
		|| (isNE(covrrgpIO.getLife(),regpIO.getLife()))
		|| (isNE(covrrgpIO.getCoverage(),regpIO.getCoverage()))
		|| (isNE(covrrgpIO.getRider(),regpIO.getRider()))
		|| (isEQ(covrrgpIO.getStatuz(),varcom.endp))) {
			wsaaEofFlag.set("Y");
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(covrrgpIO.getPlanSuffix(),ZERO)) {
			wsaaPlanSuffix.set(chdrrgpIO.getPolsum());
			wsaaEofFlag.set("Y");
			for (wsaaPlanSuffix.set(chdrrgpIO.getPolsum()); !(isEQ(wsaaPlanSuffix,ZERO)); wsaaPlanSuffix.add(-1)){
				statiiToSubfile1200();
			}
		}
		else {
			wsaaPlanSuffix.set(covrrgpIO.getPlanSuffix());
			statiiToSubfile1200();
		}
		covrrgpIO.setFunction(varcom.nextr);
	}

protected void statiiToSubfile1200()
	{
		covrStatusDesc1210();
	}

protected void covrStatusDesc1210()
	{
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		wsaaCovStatExit = "N";
		wsaaPremStatExit = "N";
		sv.planSuffix.set(wsaaPlanSuffix);
		wsaaSub.set(ZERO);
		while ( !(isEQ(wsaaCovStatExit,"Y"))) {
			checkStatusCoverage1300();
		}
		
		sv.planSuffix.set(wsaaPlanSuffix);
		descIO.setDescitem(covrrgpIO.getStatcode());
		descIO.setDesctabl(t5682);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rstatdesc.fill("?");
		}
		descIO.setDescitem(covrrgpIO.getPstatcode());
		descIO.setDesctabl(t5681);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pstatdesc.fill("?");
		}
		wsaaSub.set(ZERO);
		while ( !(isEQ(wsaaPremStatExit,"Y"))) {
			checkStatusPremium1400();
		}
		
		scrnparams.function.set(varcom.sadd);
		processScreen("S5118", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaActualWritten.add(1);
		}
	}

protected void checkStatusCoverage1300()
	{
		try {
			search1310();
		}
		catch (GOTOException e){
		}
	}

protected void search1310()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaCovStatExit = "Y";
		}
		else {
			if (isNE(covrrgpIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.exit1390);
			}
			else {
				sv.covRiskStat.set(t5679rec.covRiskStat[wsaaSub.toInt()]);
				wsaaCovStatExit = "Y";
			}
		}
	}

protected void checkStatusPremium1400()
	{
		try {
			search1410();
		}
		catch (GOTOException e){
		}
	}

protected void search1410()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaPremStatExit = "Y";
		}
		else {
			if (isNE(covrrgpIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.exit1490);
			}
			else {
				sv.covPremStat.set(t5679rec.covPremStat[wsaaSub.toInt()]);
				wsaaPremStatExit = "Y";
			}
		}
	}

protected void callCltsio1600()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callDescio1700()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsaaSelected.set(0);
		wsaaPlan.set(SPACES);
		scrnparams.subfileRrn.set(1);
		if (isEQ(chdrrgpIO.getPolinc(),1)) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		checkForErrors2050();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5118", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		wsaaFirstTime = "N";
		if ((planNotSelected.isTrue())
		&& (isEQ(wsaaSelected,0))) {
			scrnparams.errorCode.set(g634);
			wsspcomn.edterror.set("Y");
		}
		compute(wsaaPoliciesProtected, 0).set(sub(chdrrgpIO.getPolinc(),wsaaActualWritten));
		if ((isGT(chdrrgpIO.getPolinc(),1))
		&& (isEQ(wsaaPoliciesProtected,wsaaActualWritten))) {
			wsaaPlan.set("Y");
		}
	}

protected void checkForErrors2050()
	{
		if ((isNE(sv.errorIndicators,SPACES))) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					checkNextRecord2610();
					validation2620();
				}
				case updateErrorIndicators2640: {
					updateErrorIndicators2640();
					readNext2660();
				}
				case exit2690: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkNextRecord2610()
	{
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2690);
		}
		if (isEQ(sv.select,SPACES)) {
			sv.selectErr.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2640);
		}
	}

protected void validation2620()
	{
		if (isNE(sv.select,SPACES)) {
			wsaaSelected.add(1);
		}
		if ((isNE(sv.select,SPACES))
		&& (isLTE(chdrrgpIO.getPolinc(),1))) {
			scrnparams.errorCode.set(h080);
			wsspcomn.edterror.set("Y");
		}
		if (isGT(wsaaSelected,1)) {
			sv.selectErr.set(f388);
		}
	}

protected void updateErrorIndicators2640()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5118", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNext2660()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			scrnparams.function.set(varcom.srnch);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		processScreen("S5118", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		goTo(GotoLabel.exit3090);
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(chdrrgpIO.getPolinc(),1)) {
			regpIO.setPlanSuffix(ZERO);
			keepsRegp4200();
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5118", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		while ( !(isNE(sv.select,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			processScreen("S5118", sv);
			if ((isNE(scrnparams.statuz,varcom.oK))
			&& (isNE(scrnparams.statuz,varcom.endp))) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isNE(sv.select,SPACES)) {
			regpIO.setPlanSuffix(sv.planSuffix);
			keepsRegp4200();
			wsspcomn.programPtr.add(1);
		}
	}

protected void keepsRegp4200()
	{
		/*GO*/
		regpIO.setFunction(varcom.keeps);
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
}
