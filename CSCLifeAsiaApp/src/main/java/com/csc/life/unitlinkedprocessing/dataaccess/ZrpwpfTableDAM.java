package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZrpwpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:01
 * Class transformed from ZRPWPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZrpwpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 67;
	public FixedLengthStringData zrpwrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zrpwpfRecord = zrpwrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zrpwrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zrpwrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zrpwrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zrpwrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zrpwrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zrpwrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(zrpwrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zrpwrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zrpwrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zrpwrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZrpwpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZrpwpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZrpwpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZrpwpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrpwpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZrpwpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrpwpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZRPWPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZrpwrec() {
  		return zrpwrec;
	}

	public FixedLengthStringData getZrpwpfRecord() {
  		return zrpwpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZrpwrec(what);
	}

	public void setZrpwrec(Object what) {
  		this.zrpwrec.set(what);
	}

	public void setZrpwpfRecord(Object what) {
  		this.zrpwpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zrpwrec.getLength());
		result.set(zrpwrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}