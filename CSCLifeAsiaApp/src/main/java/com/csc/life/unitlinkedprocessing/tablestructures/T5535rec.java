package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:25
 * Description:
 * Copybook name: T5535REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5535rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5535Rec = new FixedLengthStringData(550);
  	public FixedLengthStringData percOrAmtInds = new FixedLengthStringData(15).isAPartOf(t5535Rec, 0);
  	public FixedLengthStringData[] percOrAmtInd = FLSArrayPartOfStructure(15, 1, percOrAmtInds, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(15).isAPartOf(percOrAmtInds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData percOrAmtInd01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData percOrAmtInd02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData percOrAmtInd03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData percOrAmtInd04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData percOrAmtInd05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData percOrAmtInd06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData percOrAmtInd07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData percOrAmtInd08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData percOrAmtInd09 = new FixedLengthStringData(1).isAPartOf(filler, 8);
  	public FixedLengthStringData percOrAmtInd10 = new FixedLengthStringData(1).isAPartOf(filler, 9);
  	public FixedLengthStringData percOrAmtInd11 = new FixedLengthStringData(1).isAPartOf(filler, 10);
  	public FixedLengthStringData percOrAmtInd12 = new FixedLengthStringData(1).isAPartOf(filler, 11);
  	public FixedLengthStringData percOrAmtInd13 = new FixedLengthStringData(1).isAPartOf(filler, 12);
  	public FixedLengthStringData percOrAmtInd14 = new FixedLengthStringData(1).isAPartOf(filler, 13);
  	public FixedLengthStringData percOrAmtInd15 = new FixedLengthStringData(1).isAPartOf(filler, 14);
  	public FixedLengthStringData unitAfterDurs = new FixedLengthStringData(60).isAPartOf(t5535Rec, 15);
  	public ZonedDecimalData[] unitAfterDur = ZDArrayPartOfStructure(15, 4, 0, unitAfterDurs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(unitAfterDurs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData unitAfterDur01 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData unitAfterDur02 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData unitAfterDur03 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData unitAfterDur04 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData unitAfterDur05 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData unitAfterDur06 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 20);
  	public ZonedDecimalData unitAfterDur07 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData unitAfterDur08 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 28);
  	public ZonedDecimalData unitAfterDur09 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 32);
  	public ZonedDecimalData unitAfterDur10 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 36);
  	public ZonedDecimalData unitAfterDur11 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 40);
  	public ZonedDecimalData unitAfterDur12 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 44);
  	public ZonedDecimalData unitAfterDur13 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 48);
  	public ZonedDecimalData unitAfterDur14 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 52);
  	public ZonedDecimalData unitAfterDur15 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 56);
  	public FixedLengthStringData unitExtPcAllocs = new FixedLengthStringData(75).isAPartOf(t5535Rec, 75);
  	public ZonedDecimalData[] unitExtPcAlloc = ZDArrayPartOfStructure(15, 5, 2, unitExtPcAllocs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(75).isAPartOf(unitExtPcAllocs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData unitExtPcAlloc01 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData unitExtPcAlloc02 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 5);
  	public ZonedDecimalData unitExtPcAlloc03 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 10);
  	public ZonedDecimalData unitExtPcAlloc04 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 15);
  	public ZonedDecimalData unitExtPcAlloc05 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 20);
  	public ZonedDecimalData unitExtPcAlloc06 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 25);
  	public ZonedDecimalData unitExtPcAlloc07 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 30);
  	public ZonedDecimalData unitExtPcAlloc08 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 35);
  	public ZonedDecimalData unitExtPcAlloc09 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 40);
  	public ZonedDecimalData unitExtPcAlloc10 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 45);
  	public ZonedDecimalData unitExtPcAlloc11 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 50);
  	public ZonedDecimalData unitExtPcAlloc12 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 55);
  	public ZonedDecimalData unitExtPcAlloc13 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 60);
  	public ZonedDecimalData unitExtPcAlloc14 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 65);
  	public ZonedDecimalData unitExtPcAlloc15 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 70);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(400).isAPartOf(t5535Rec, 150, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5535Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5535Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}