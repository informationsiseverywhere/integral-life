/*
 * File: B5102.java
 * Date: 29 August 2009 20:54:18
 * Author: Quipoz Limited
 * 
 * Class transformed from B5102.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.BwrkTableDAM;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Td5f6rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdelpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UderpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdivpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UfnspfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UreppfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrxpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Udelpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Uderpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Udivpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ufnspf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ureppf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrxpf;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.database.QPBaseDataSource;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.IntegralDBProperties;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  UNIT DEALING.
*  =============
*
*  OVERVIEW.
*
*  This program will read UTRX records, UTRXs being undealt UTRNs
*  created by the Unit Dealing Splitter program B5100, and "deal"
*  each record found. Each "deal" involves buying and selling units
*  in the fund identified on the UTRX.
*
*  B5100 has three functions and each function will return
*  different types of UTRNs. A typical schedule will look like this:
*
*  B5100  Unit Dealing Splitter (with the DEAL function)
*  B5101  The Fund Movement Report
*  B5102  Dealing
*  B5100  Unit Dealing Splitter (with the SWCH function)
*  B5102  Dealing
*  B5103  Coverage Debt Splitter
*  B5104  Coverage Debt Settlement
*  B5100  Unit Dealing Splitter (with the DEBT function)
*  B5102  Dealing
*  B5106  Dealt Transaction Report
*  B5107  Dealing & Debt Settlement Errors Report
*
*  Note that more combinations are possible and reference should
*  be made to the appropriate reference manual for further
*  combinations.
*
*  Thus this program could be run three times in a Unit Dealing
*  job. The first time to pick up all outstanding deals, the second
*  time to pick up fund switch "buy ins" and the third time to pick
*  up "sells" to settle coverage debt.
*
*  DETAILED DESCRIPTION.
*
*  Initialisation Section.
*
*  a) Check the restart method is 3
*  b) Find the name of the UTRX temporary file. UTRX is a temporary
*     file which has the name of "UTRX" + first two characters of
*     param 4 + the last four digits of the job number. The problem
*     for this program is that the last four digits of the job number
*     may not be this job's job number - i.e. where this program is
*     to deal only those transactions which were listed on the Fund
*     Movement Report (B5101) then param 1 will need to contain the
*     name of the schedule in which B5100 and B5101 ran. Using the
*     schedule name the last run can be found and hence the last four
*     digits of the run no. Note that if param 1 is blank then the
*     assumption is that B5100 has been run immediately previous to
*     this program and the current schedule number can be used.
*  c) Pre-load constantly referenced Tables into working storage.
*     These are: T5645, T5688, T5544, T5515, T6647, T5687, T6597
*  d) Set up the LifeAcmv and UTRNUP fields which will not change.
*
*  Read Section.
*
*  a) Read a UTRX.
*
*  Edit Section.
*
*  a) Softlock the contract if this UTRX is for a new contract.
*     Note that if a softlock exists we could further corrupt a
*     policy if we process its UTRNs.
*  b) If the UTRX has no fund then we will still process it
*     although there will be no fund movements. Log it.
*  c) Get the price for this UTRN. If we don't find one then we
*     can't process it now so ignore it. Prices are found by
*     calling UFPRICE which has buffering logic to avoid reading
*     the same VPRC records over and over again.
*
*  Update Section.
*
*  a) Read and hold the UTRN using UTRNALO. It is conceivable
*     although unlikely that the UTRN extracted by B5100 may have
*     been removed. If so log its disappearance and skip all
*     further processing. If the UDEL switch is on (SYS PARAM02),
*     check for break of contract and if all UTRNs for this
*     contract have been processed, delete the UDEL record.
*  b) Search WSAA-T5688 for the accounting level for this contract
*     type. It will be either coverage or contract level accounted.
*  c) If the fund is spaces, account for the non-invested amounts,
*     flag the UTRN as processed and skip all further processing.
*  d) Read and hold the coverage level summary record (UTRS) as
*     we are about to increase or decrease the number of units
*     attributed to this coverage.
*  e) If the modify units indicator is set on then we skip to m)
*     as this is a modify transaction which requires little
*     processing
*  f) Now we need the Buy or Sell price. This is primarily determined
*     by whether we are buying or selling units and whether this is
*     a fund switch. Search WSAA-T6647 using the UTRNs trans
*     code and contract type (if not found try *** as the contract
*     type) to get the bid or offer indicator). If this is not a fund
*     switch UTRN then simply set the price to either bid or offer
*     price as determined by T6647. If this is a fund switch then
*     we search WSAA-T5544 using the T6647 switch method to find
*     various bid-to-bid, bid-to-offer, offer-to-offer, etc
*     pricing rules. finally... We have a price!
*  g) If this is a surrender and we have units to sell then
*     we need to calculate various UTRN values based upon the
*     surrender percentage.
*  h) Now we need to complete the important fields on the UTRN. This
*     needs to be done as different values will be filled in by
*     different transactions - e.g. we may have only number of units
*     but no monetary amount or vice versa.
*  i) If we have a UTRN with no fund amount or units then this UTRN
*     requires only n) processing. Flag the UTRN as processed and
*     then skip to n).
*  j) If this is a sell UTRN then we need to check that we have
*     sufficient units. If not we look up the forfeiture rules
*     and take the appropriate action. Four options are available
*     1. Do nothing. 2. Create a negative units transaction. 3.
*     Increase the coverage debt (but not if the UTRN is actually
*     being sold to satisfy a coverage debt) 4. Lapse the contract.
*     If 3 or 4, log this as a error by writing a record to the
*     UDER file, update the UTRN as processed and skip to n).
*  k) Now we need to write ACMVs to reflect the ledger changes
*     for this almost complete UTRN. Before we do this however
*     we need to round the various posting from five decimal
*     places to two. As this can result in rounding errors we
*     sum the rounded amounts up to ensure they balance to zero.
*     If they do not balance to zero we add the balance to a
*     non-zero posting, thus balancing the posted ACMVs. Then
*     we post the ACMVs.
*  l) Next check that the effective date of the UTRN which was
*     used to get the price has not resulted in fluctuations from
*     today's price. If it has, ensure we reflect this by posting
*     extra ACMVs to recognise the fluctuations.
*  m) Now update the relevant fund records. Rewrite the now
*     completed UTRN as processed, create or update the coverage
*     summary record (UTRS) with the traded number of units, and
*     finally update the fund summary record with the traded
*     number of units. (This last bit is done by storing the
*     units for each fund in a working storage table keyed by
*     fund and fund type and then flushing the table to UFND
*     immediately before a commit in the 3500 section.)
*  n) And finally ... if the trigger module on the UTRN is not
*     blank call the trigger routine. (e.g FUNDSWCH).
*
*  Finalise Section.
*
*  a) Flush any fund totals to UFND.
*  b) Close the temporary file.
*  c) Remove the override
*  d) Set the parameter statuz to O-K
*  e) All done.
*
*****************************************************************
* </pre>
*/
public class B5102 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5102");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSaveBsprParams = new FixedLengthStringData(1024);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaUtrxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaUtrxFn, 0, FILLER).init("UTRX");
	private FixedLengthStringData wsaaUtrxRunid = new FixedLengthStringData(2).isAPartOf(wsaaUtrxFn, 4);
	private ZonedDecimalData wsaaUtrxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUtrxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaPrevItemitem = new FixedLengthStringData(8);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaPrice = new PackedDecimalData(9, 5);
	private PackedDecimalData wsaaUfprBarePrice = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaUfprPriceDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaWithinRange = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaInsuffUnitsMeth = new FixedLengthStringData(1);
	private PackedDecimalData wsaaDebt = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaFund = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaBidoffer = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaBarebid = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaCharge = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaPostFund = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBidoffer = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBarebid = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostCharge = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBalanceCheck = new PackedDecimalData(13, 2);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaReversalUtrn = new FixedLengthStringData(1);
	private Validator reversalUtrn = new Validator(wsaaReversalUtrn, "Y");

	private FixedLengthStringData wsaaBwrkData = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaUdelChdrcoy = new FixedLengthStringData(2).isAPartOf(wsaaBwrkData, 0).init(SPACES);
	private FixedLengthStringData wsaaUdelChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaBwrkData, 2).init(SPACES);
	private FixedLengthStringData wsaaDeleteUdel = new FixedLengthStringData(1).isAPartOf(wsaaBwrkData, 10);
	private Validator prevUnprocUtrnExists = new Validator(wsaaDeleteUdel, "N");
	private Validator udelToBeDeleted = new Validator(wsaaDeleteUdel, "Y");
	private PackedDecimalData wsaaPostFundAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBidofferAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBarebidAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostChargeAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBalanceCheckAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaUtrnaloFundAmount = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaX = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTotalUnits = new PackedDecimalData(16, 5);

	private FixedLengthStringData wsaaDatimeInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDatimeDate = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 0);
	private FixedLengthStringData wsaaDatimeTime = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 10);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaDatimeInit, 20);

		/* WSAA-UFND-ARRAY */
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private FixedLengthStringData[] wsaaUfndRec = FLSInittedArray (100, 14);
	private FixedLengthStringData[] wsaaUfndFund = FLSDArrayPartOfArrayStructure(4, wsaaUfndRec, 0);
	private FixedLengthStringData[] wsaaUfndType = FLSDArrayPartOfArrayStructure(1, wsaaUfndRec, 4);
	private PackedDecimalData[] wsaaUfndNofUnits = PDArrayPartOfArrayStructure(16, 5, wsaaUfndRec, 5);
	private static final int wsaaUfndSize = 100;

		/* WSAA-T5645-ARRAY */
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private static final int wsaaT5645Size = 1000;
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);

		/* WSAA-T5688-ARRAY */
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 9);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(8, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Key, 3);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 8);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private static final int wsaaT5688Size = 1000;

		/* WSAA-T5544-ARRAY */
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private FixedLengthStringData[] wsaaT5544Rec = FLSInittedArray (1000, 15);
	private FixedLengthStringData[] wsaaT5544Key = FLSDArrayPartOfArrayStructure(8, wsaaT5544Rec, 0);
	private FixedLengthStringData[] wsaaT5544Swmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5544Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5544Currcode = FLSDArrayPartOfArrayStructure(4, wsaaT5544Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5544Data = FLSDArrayPartOfArrayStructure(7, wsaaT5544Rec, 8);
	private FixedLengthStringData[] wsaaT5544BidToFund = FLSDArrayPartOfArrayStructure(1, wsaaT5544Data, 0);
	private FixedLengthStringData[] wsaaT5544Btobid = FLSDArrayPartOfArrayStructure(1, wsaaT5544Data, 1);
	private FixedLengthStringData[] wsaaT5544Btodisc = FLSDArrayPartOfArrayStructure(1, wsaaT5544Data, 2);
	private FixedLengthStringData[] wsaaT5544Btooff = FLSDArrayPartOfArrayStructure(1, wsaaT5544Data, 3);
	private PackedDecimalData[] wsaaT5544DiscOfferPercent = PDArrayPartOfArrayStructure(5, 2, wsaaT5544Data, 4);
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private static final int wsaaT5544Size = 1000;

		/* WSAA-T5515-ARRAY */
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private FixedLengthStringData[] wsaaT5515Rec = FLSInittedArray (1000, 11);
	private FixedLengthStringData[] wsaaT5515Key = FLSDArrayPartOfArrayStructure(4, wsaaT5515Rec, 0);
	private FixedLengthStringData[] wsaaT5515Fund = FLSDArrayPartOfArrayStructure(4, wsaaT5515Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5515Data = FLSDArrayPartOfArrayStructure(7, wsaaT5515Rec, 4);
	private FixedLengthStringData[] wsaaT5515Btobid = FLSDArrayPartOfArrayStructure(1, wsaaT5515Data, 0);
	private FixedLengthStringData[] wsaaT5515Btodisc = FLSDArrayPartOfArrayStructure(1, wsaaT5515Data, 1);
	private FixedLengthStringData[] wsaaT5515Btooff = FLSDArrayPartOfArrayStructure(1, wsaaT5515Data, 2);
	private FixedLengthStringData[] wsaaT5515Otoff = FLSDArrayPartOfArrayStructure(1, wsaaT5515Data, 3);
	private PackedDecimalData[] wsaaT5515DiscOfferPercent = PDArrayPartOfArrayStructure(5, 2, wsaaT5515Data, 4);
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private static final int wsaaT5515Size = 1000;
	//private static final int wsaaT6647Size = 500;//MIBT-289, commented
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
		private static final int wsaaT6647Size = 2050;//MIBT-289, size increased   //ILIFE-6190
		/* WSAA-T5687-ARRAY 
		 03 WSAA-T5687-REC                OCCURS 60                   */
	//private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (100, 24);
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (1000, 24);//ILIFE-1975
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(20, wsaaT5687Rec, 4);
	private FixedLengthStringData[] wsaaT5687NonForfeitMeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 0);
	private FixedLengthStringData[] wsaaT6597Statuz = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 4);
	private FixedLengthStringData[] wsaaT6597Premsubr04 = FLSDArrayPartOfArrayStructure(8, wsaaT5687Data, 8);
	private FixedLengthStringData[] wsaaT6597Cpstat04 = FLSDArrayPartOfArrayStructure(2, wsaaT5687Data, 16);
	private FixedLengthStringData[] wsaaT6597Crstat04 = FLSDArrayPartOfArrayStructure(2, wsaaT5687Data, 18);
	//private static final int wsaaT5687Size = 100;
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private static final int wsaaT5687Size = 1000;//ILIFE-1975
	//ILIFE-2605 fixed--Array size increased for each smart table arrays
	private static final int wsaaT3629Size = 1000;
	/* WSAA-T3629-ARRAY */
	private FixedLengthStringData[] wsaaT3629Rec = FLSInittedArray (1000, 503);
	private FixedLengthStringData[] wsaaT3629Key = FLSDArrayPartOfArrayStructure(3, wsaaT3629Rec, 0);
	private FixedLengthStringData[] wsaaT3629Billcurr = FLSDArrayPartOfArrayStructure(3, wsaaT3629Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT3629Data = FLSDArrayPartOfArrayStructure(500, wsaaT3629Rec, 3);
	private FixedLengthStringData[] wsaaT3629T3629Rec = FLSDArrayPartOfArrayStructure(500, wsaaT3629Data, 0);
		/*       07  WSAA-T3629-FRMDATES.                               
		                                 OCCURS 07 .                  
		       07  WSAA-T3629-TODATES.                                
		                                 OCCURS 07 .                  
		       07  WSAA-T3629-SCRATES.                                
		                                 OCCURS 07 .                  */
	private FixedLengthStringData wsaaT3629StoreCurr = new FixedLengthStringData(3);
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5515 = "T5515";
	private static final String t5544 = "T5544";
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6597 = "T6597";
	private static final String t6647 = "T6647";
	private FixedLengthStringData t3629 = new FixedLengthStringData(6).init("T3629");
		/* ERRORS */
	private static final String f070 = "F070";
	private static final String f294 = "F294";
	private static final String ivrm = "IVRM";
	private static final String h072 = "H072";
	private static final String h791 = "H791";
	private static final String g418 = "G418";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData t5544Ix = new IntegerData();
	private IntegerData t5515Ix = new IntegerData();
	private IntegerData t6647Ix = new IntegerData();
	private IntegerData t5687Ix = new IntegerData();
	private BwrkTableDAM bwrkIO = new BwrkTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private T5544rec t5544rec = new T5544rec();
	private T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6597rec t6597rec = new T6597rec();
	private T6647rec t6647rec = new T6647rec();
	private T3629rec t3629rec = new T3629rec();
	private Td5f6rec td5f6rec = new Td5f6rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private Udtrigrec udtrigrec = new Udtrigrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT6647ArrayInner wsaaT6647ArrayInner = new WsaaT6647ArrayInner();

	private Map<String, List<Itempf>> t5645Map = null;
	private Map<String, List<Itempf>> t5688Map = null;
	private Map<String, List<Itempf>> t5544Map = null;
	private Map<String, List<Itempf>> t5515Map = null;
	private Map<String, List<Itempf>> t6647Map = null;
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Itempf>> t6597Map = null;
	private Map<String, List<Itempf>> t3629Map = null;
	private Map<String, List<Itempf>> Td5f6Map = null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private UdelpfDAO udelpfDAO = getApplicationContext().getBean("udelpfDAO", UdelpfDAO.class);
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private PayrpfDAO payrpfDAO	 = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private UreppfDAO ureppfDAO	 = getApplicationContext().getBean("ureppfDAO", UreppfDAO.class);
	private CovrpfDAO covrpfDAO	 = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private UderpfDAO uderpfDAO	 = getApplicationContext().getBean("uderpfDAO", UderpfDAO.class);
	private UdivpfDAO udivpfDAO	 = getApplicationContext().getBean("udivpfDAO", UdivpfDAO.class);
	private UfnspfDAO ufnspfDAO	 = getApplicationContext().getBean("ufnspfDAO", UfnspfDAO.class);
	private UtrxpfDAO utrxpfDAO	 = getApplicationContext().getBean("utrxpfDAO", UtrxpfDAO.class);
	
	private Map<String, List<Utrnpf>> utrnaloMap = null;
	private Map<String, List<Utrspf>> utrsMap = null;
	private Map<String, List<Payrpf>> payerMap = null;
	private Map<String, List<Ureppf>> ureppfMap = null;
	private List<Udelpf> deleteUdelpfList = null;
	private List<Utrnpf> updateUtrnpfList = null;
	private List<Utrspf> insertUtrspfList = null;
	private List<Utrspf> updateUtrspfList = null;
	private List<Covrpf> updateCovrpfDebtList = null;
	private List<Covrpf> updateCovrpfList = null;
	private List<Uderpf> insertUderpfList = null;
	private List<Udivpf> insertUdivpfList = null;
	private List<Ureppf> insertUreppfList = null;
	private List<Ufnspf> insertUfnspfList = null;
	private int wsaaT3629Ix = 1;
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Utrspf utrspf = null;
	private Utrxpf utrxpfRec;
	private Iterator<Utrxpf> iteratorList;
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private int ct06Value = 0;
	private int ct07Value = 0;
	private int ct08Value = 0;
	private int ct09Value = 0;
	private int ct10Value = 0;
	private int ct11Value = 0;
	private int ct12Value = 0;
	private int ct13Value = 0;
	private int ct14Value = 0;
	private int ct15Value = 0;
	private int ct16Value = 0;
	private int ct17Value = 0;
	private int ct20Value = 0;
	private BigDecimal ct21Value = BigDecimal.ZERO;
	private BigDecimal ct22Value = BigDecimal.ZERO;
	private BigDecimal ct23Value = BigDecimal.ZERO;
	private BigDecimal ct24Value = BigDecimal.ZERO;
	private BigDecimal ct25Value = BigDecimal.ZERO;
	private BigDecimal ct26Value = BigDecimal.ZERO;
	private BigDecimal ct27Value = BigDecimal.ZERO;
	private BigDecimal ct28Value = BigDecimal.ZERO;
	private BigDecimal ct29Value = BigDecimal.ZERO;
	private BigDecimal ct30Value = BigDecimal.ZERO;
	private BigDecimal ct31Value = BigDecimal.ZERO;
	private BigDecimal ct32Value = BigDecimal.ZERO;
	private BigDecimal ct33Value = BigDecimal.ZERO;
	private BigDecimal ct34Value = BigDecimal.ZERO;
	private int ct35Value = 0;
	private int ct36Value = 0;
	private int ct37Value = 0;
	private int intBatchID = 0;
	private int intBatchExtractSize;
	private Utrnpf utrnalo = null;
	List<Long> uniqueNum=new ArrayList<Long>();//ILIFE-9312
	private static final String FMC = "FMC";
	private static final String MF = "MF";
	private static final String  FMC_FEATURE_ID = "BTPRO026";
	private boolean fmcOnFlag = false;
	List<Long> uniqueNumUtrs=new ArrayList<Long>();// IBPLIFE-6876
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		updates3030, 
		triggerProcessing3070, 
		exit3090, 
		exit5539, 
		unitsToSellForDebt5750, 
		exit5790, 
		exit5809, 
		exit5819, 
		exit5829, 
		exit5990, 
		x2020CallPayrlifio
	}

	public B5102() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		bwrkIO.setFunction(varcom.readh);
		bwrkIO.setDataKey(bsprIO.getDataKey());
		bwrkIO.setFormat(formatsInner.bwrkrec);
		SmartFileCode.execute(appVars, bwrkIO);
		if (isEQ(bwrkIO.getStatuz(), varcom.mrnf)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(bwrkIO.getStatuz(), varcom.oK)) {
				wsaaBwrkData.set(bwrkIO.getWorkAreaData());
			}
			else {
				syserrrec.params.set(bwrkIO.getParams());
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void initialise1000()
	{
		wsspEdterror.set(varcom.oK);
		wsaaDatimeInit.set(bsscIO.getDatimeInit());
		wsaaTotalUnits.set(ZERO);
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		fmcOnFlag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString().trim(), FMC_FEATURE_ID, appVars, "IT");
		tempFileOverride1100();
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		readChunkRecord();
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Load All Accounting Rules.*/
		String coy = bsprIO.getCompany().toString();
		t5645Map = itemDAO.loadSmartTable("IT", coy, "T5645");
		iy.set(1);
		loadT56451200();
		
		/* Load All Contract Processing Rules.*/
		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
		loadT56881300();
		
		/* Load Fund Switching Rules for job effective date.*/
		t5544Map = itemDAO.loadSmartTable("IT", coy, "T5544");
		loadT55441400();
		
		/* Load Unit Linked Fund Details for job effective date.*/
		t5515Map = itemDAO.loadSmartTable("IT", coy, "T5515");
		loadT55151500();
		
		/* Load Unit Linked Contract Details for job effective date.*/
		t6647Map = itemDAO.loadSmartTable("IT", coy, "T6647");
		loadT66471600();
		
		/* Load General Coverage/Rider Details for job effective date.*/
		/* Note the we also load the Non-fortfeiture table (T6597) at*/
		/* the same time into the same table as this is a one-to-one*/
		/* relationship between non-forteiture method and T6597 items.*/
		t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
		loadT56871700();
		
		t6597Map = itemDAO.loadSmartTable("IT", coy, "T6597");
		loadT6597IntoT56871800();
	
		/*  Load Bank codes.                                               */
		t3629Map = itemDAO.loadSmartTable("IT", coy, "T3629");
		loadT36291900();
		
		
		Td5f6Map = itemDAO.loadSmartTable("IT", coy, "TD5F6");
		
		
		
		/*    Fields which require initialisation only once.*/
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(bprdIO.getAuthCode());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
		wsaaDeleteUdel.set(" ");
	}

private void readChunkRecord() {
	List<Utrxpf> utrxpfList = utrxpfDAO.searchUtrxpfRecord(wsaaUtrxFn.toString(), wsaaThreadMember.toString(),
			intBatchExtractSize, intBatchID);
	iteratorList = utrxpfList.iterator();
	if (utrxpfList != null && !utrxpfList.isEmpty()) {
		List<String> chdrnumSet = new ArrayList<>();
		for (Utrxpf u : utrxpfList) {
			chdrnumSet.add(u.getChdrnum());
		}
		utrnaloMap = utrnpfDAO.searchUtrnRecordByFdbkind(chdrnumSet);
		utrsMap = utrspfDAO.searchUtrsRecordByChdrnum(chdrnumSet);
		payerMap  = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrnumSet);
		ureppfMap  = ureppfDAO.searchSeqByChdrnum(chdrnumSet);
	}
}
protected void tempFileOverride1100()
	{
		tempFileOverride1101();
		override1105();
	}

protected void tempFileOverride1101()
	{
		if (isEQ(bprdIO.getSystemParam01(), SPACES)) {
			wsaaUtrxJobno.set(bsprIO.getScheduleNumber());
			return ;
		}
		/*    This multi-thread batch program is slightly different from*/
		/*    most in that we need to find the JOB NUMBER of the LAST*/
		/*    Fund Movement Report which was run to assign ourselves to*/
		/*    the temporary file. We need to do this because the Fund*/
		/*    Movement Report includes our splitter program which creates*/
		/*    the UTRX files and because the Fund Manager will have*/
		/*    have set his/her fund prices based upon a FINITE set of*/
		/*    transations (i.e those extracted via the splitter program*/
		/*    B5100).*/
		/*    Firstly save this process's BSPR-Params.*/
		wsaaSaveBsprParams.set(bsprIO.getParams());
		bsprIO.setScheduleName(bprdIO.getSystemParam01());
		bsprIO.setScheduleNumber(99999999);
		bsprIO.setCompany(SPACES);
		bsprIO.setProcessName(SPACES);
		bsprIO.setProcessOccNum(ZERO);
		bsprIO.setFormat(formatsInner.bsprrec);
		bsprIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, bsprIO);
		if (isNE(bsprIO.getStatuz(), varcom.oK)
		|| isNE(bsprIO.getScheduleName(), bprdIO.getSystemParam01())
		|| isNE(bsprIO.getProcessStatus(), "90")) {
			bsprIO.setScheduleName(bprdIO.getSystemParam01());
			bsprIO.setScheduleNumber(99999999);
			bsprIO.setCompany(SPACES);
			bsprIO.setProcessName(SPACES);
			bsprIO.setProcessOccNum(ZERO);
			bsprIO.setFormat(formatsInner.bsprrec);
			bsprIO.setFunction(varcom.endr);
			bsprIO.setStatuz(varcom.endp);
			syserrrec.params.set(bsprIO.getParams());
			fatalError600();
		}
		/*    Set up our temporary file job number as that of the last*/
		/*    Fund Movement Report job number.*/
		wsaaUtrxJobno.set(bsprIO.getScheduleNumber());
		/*    Restore the BSPR-Params.*/
		bsprIO.setParams(wsaaSaveBsprParams);
	}

protected void override1105()
	{
		/*    Now set up the rest of the temporary file params.*/
		wsaaUtrxRunid.set(bprdIO.getSystemParam04());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*EXIT*/
	}

protected void loadT56451200()
 {
		if (t5645Map.containsKey(wsaaProg)) {
			List<Itempf> itemList = t5645Map.get(wsaaProg);
			if (itemList.size() > wsaaT5645Size) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t5645);
				fatalError600();
			}
			for (Itempf item : itemList) {
				t5645rec.t5645Rec.set(StringUtil.rawToString(item.getGenarea()));
				int i = 1;
				while (i <= 15) {
					wsaaT5645Cnttot[iy.toInt()].set(t5645rec.cnttot[15]);
					wsaaT5645Glmap[iy.toInt()].set(t5645rec.glmap[i]);
					wsaaT5645Sacscode[iy.toInt()].set(t5645rec.sacscode[i]);
					wsaaT5645Sacstype[iy.toInt()].set(t5645rec.sacstype[i]);
					wsaaT5645Sign[iy.toInt()].set(t5645rec.sign[i]);
					i++;
					iy.add(1);
				}
			}
		} else {
			syserrrec.params.set("t5645:" + wsaaProg);
			fatalError600();
		}
	}

protected void loadT56881300()
 {
		if (t5688Map == null || t5688Map.size() == 0) {
			syserrrec.params.set("t5688");
			fatalError600();
		}
		if (t5688Map.size() > wsaaT5688Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5688);
			fatalError600();
		}
		int i = 1;
		for (List<Itempf> itemList : t5688Map.values()) {
			for (Itempf item : itemList) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t5688rec.t5688Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5688Cnttype[i].set(item.getItemitem());
					wsaaT5688Itmfrm[i].set(item.getItmfrm());
					wsaaT5688Comlvlacc[i].set(t5688rec.comlvlacc);
					i++;
				}
			}
		}
	}


protected void loadT55441400()
 {
		int i = 1;
		for (List<Itempf> itempfList : t5544Map.values()) {
			Itempf itempf = itempfList.get(0);
			if (itempf.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
				t5544rec.t5544Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaT5544Swmeth[i].set(subString(itempf.getItemitem(), 1, 4));
				wsaaT5544Currcode[i].set(subString(itempf.getItemitem(), 5, 3));
				wsaaT5544BidToFund[i].set(t5544rec.bidToFund);
				wsaaT5544Btobid[i].set(t5544rec.btobid);
				wsaaT5544Btodisc[i].set(t5544rec.btodisc);
				wsaaT5544Btooff[i].set(t5544rec.btooff);
				wsaaT5544DiscOfferPercent[i].set(t5544rec.discountOfferPercent);
				i++;
			}
			if (i > wsaaT5544Size) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t5688);
				fatalError600();
			}
		}
	}

protected void loadT55151500()
	{
		int i = 1;
		for (List<Itempf> itempfList : t5515Map.values()) {
			Itempf itempf = itempfList.get(0);
			if (itempf.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
				t5515rec.t5515Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaT5515Fund[i].set(itempf.getItemitem());
				wsaaT5515Btobid[i].set(t5515rec.btobid);
				wsaaT5515Btodisc[i].set(t5515rec.btodisc);
				wsaaT5515Btooff[i].set(t5515rec.btooff);
				wsaaT5515Otoff[i].set(t5515rec.otoff);
				wsaaT5515DiscOfferPercent[i].set(t5515rec.discountOfferPercent);
				i++;
			}
			if (i > wsaaT5515Size) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t5515);
				fatalError600();
			}
		}
	}

protected void loadT66471600()
 {
		int i = 1;
		for (List<Itempf> itempfList : t6647Map.values()) {
			Itempf itempf = itempfList.get(0);
			if (itempf.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
				t6647rec.t6647Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaT6647ArrayInner.wsaaT6647Batctrcde[i].set(subString(itempf.getItemitem(), 1, 4));
				wsaaT6647ArrayInner.wsaaT6647Cnttype[i].set(subString(itempf.getItemitem(), 5, 3));
				wsaaT6647ArrayInner.wsaaT6647Swmeth[i].set(t6647rec.swmeth);
				wsaaT6647ArrayInner.wsaaT6647Bidoffer[i].set(t6647rec.bidoffer);
				wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[i].set(t6647rec.monthsNegUnits);
				wsaaT6647ArrayInner.wsaaT6647MonthsDebt[i].set(t6647rec.monthsDebt);
				wsaaT6647ArrayInner.wsaaT6647MonthsLapse[i].set(t6647rec.monthsLapse);
				wsaaT6647ArrayInner.wsaaT6647MonthsError[i].set(t6647rec.monthsError);
				i++;
			}
			if (i > wsaaT6647Size) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t6647);
				fatalError600();
			}
		}
	}

protected void loadT56871700()
	{
		int i = 1;
		for (List<Itempf> itempfList : t5687Map.values()) {
			Itempf itempf = itempfList.get(0);
			if (itempf.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaT5687Crtable[i].set(itempf.getItemitem());
				wsaaT5687NonForfeitMeth[i].set(t5687rec.nonForfeitMethod);
				i++;
			}
			if (i > wsaaT5687Size) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t5687);
				fatalError600();
			}
		}
	}

protected void loadT6597IntoT56871800()
 {
		int i = 1;
		while (!(i > wsaaT5687Size || isEQ(wsaaT5687Crtable[i], HIVALUES))) {
			if (isEQ(wsaaT5687NonForfeitMeth[i], SPACES)) {
				i++;
				continue;
			}
			if (t6597Map.containsKey(wsaaT5687NonForfeitMeth[i])) {
				for (Itempf itempf : t6597Map.get(wsaaT5687NonForfeitMeth[i])) {
					if (itempf.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
						wsaaT6597Statuz[i].set(varcom.oK);
						t6597rec.t6597Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						wsaaT6597Premsubr04[i].set(t6597rec.premsubr04);
						wsaaT6597Cpstat04[i].set(t6597rec.cpstat04);
						wsaaT6597Crstat04[i].set(t6597rec.crstat04);
						break;
					}
				}
			} else {
				wsaaT6597Statuz[i].set(varcom.endp);
			}
			i++;
		}
	}

protected void loadT36291900()
	{
	/* MOVE T3629-BANKCODE  TO WSAA-T3629-BANKCODE (WSAA-T3629-IX). */
	/* MOVE T3629-CONTITEM  TO WSAA-T3629-CONTITEM (WSAA-T3629-IX). */
	/* PERFORM VARYING IX FROM 1 BY 1 UNTIL IX > 7                  */
	/*     MOVE T3629-SCRATE (IX) TO                                */
	/*                      WSAA-T3629-SCRATE (WSAA-T3629 IX)       */
	/*     MOVE T3629-FRMDATE (IX) TO                               */
	/*                      WSAA-T3629-FRMDATE (WSAA-T3629 IX)      */
	/*     MOVE T3629-TODATE (IX) TO                                */
	/*                      WSAA-T3629-TODATE (WSAA-T3629 IX)       */
	/* END-PERFORM.                                                 */
		for (List<Itempf> itemList : t3629Map.values()) {
			for (Itempf itempf : itemList) {
				if (wsaaT3629Ix > wsaaT3629Size) {
					syserrrec.statuz.set(h791);
					syserrrec.params.set(t3629);
					fatalError600();
				}
				wsaaT3629Billcurr[wsaaT3629Ix].set(itempf.getItemitem());
				wsaaT3629T3629Rec[wsaaT3629Ix].set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaT3629Ix++;
			}
		}
	}

protected boolean loadTD5F61500(Utrnpf utrnaloIO)
	{
	
 
	
	 String keyitem=batcdorrec.trcde.toString()+utrnaloIO.getCnttyp();
		if (Td5f6Map.containsKey(keyitem)) 
		{
			 
			return true;
		} 
		else 
		{
			 return false;
		}
}

	/**
	* <pre>
	*1A00-CLEAR-UFNS SECTION.                                 <LA4271>
	*1A10-START.                                              <LA4271>
	**** Check for multi-thread processing. If it is, clear down      
	**** the UFNSPF file.                                             
	*    IF BPRD-SYSTEM-PARAM03 NOT = SPACES                  <LA4271>
	*        MOVE 'CLRF'           TO FILEPR-FUNCTION         <LA4271>
	*        MOVE 'UFNSPF'         TO FILEPR-FILE1            <LA4271>
	*        CALL 'FILEPROC'         USING FILEPR-PARAMS      <LA4271>
	*        IF FILEPR-STATUZ   NOT = O-K                     <LA4271>
	*            MOVE FILEPR-STATUZ TO SYSR-STATUZ            <LA4271>
	*            MOVE FILEPR-PARAMS TO SYSR-PARAMS            <LA4271>
	*            PERFORM 600-FATAL-ERROR                      <LA4271>
	*        END-IF                                           <LA4271>
	*    END-IF.                                              <LA4271>
	*                                                         <LA4271>
	*1A99-EXIT.                                               <LA4271>
	*     EXIT.                                               <LA4271>
	*                                                         <LA4271>
	* </pre>
	*/
protected void readFile2000()
	{
		/*READ-FILE*/
		if (iteratorList != null && iteratorList.hasNext()) {
			utrxpfRec = iteratorList.next();
			ct01Value++;
		} else {
			intBatchID++;
			clearList2100();
			readChunkRecord();
			if (iteratorList != null && iteratorList.hasNext()) {
				utrxpfRec = iteratorList.next();			
				ct01Value++;
			} else {
				wsspEdterror.set(varcom.endp);
				return ;
			}
		}
		/*EXIT*/
	}

private void clearList2100() {
	if (utrnaloMap != null) {
		utrnaloMap.clear();
	}
	if (utrsMap != null) {
		utrsMap.clear();
	}
	if (payerMap != null) {
		payerMap.clear();
	}
	if (ureppfMap != null) {
		ureppfMap.clear();
	}
}
protected void edit2500()
	{
		wsspEdterror.set(SPACES);
		if (isEQ(utrxpfRec.getChdrnum(), wsaaALockedChdrnum)) {
			ct03Value++;
			return ;
		}
		if (isNE(utrxpfRec.getChdrnum(), lastChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz, "LOCK")) {
				wsaaALockedChdrnum.set(utrxpfRec.getChdrnum());
				return ;
			}
			else {
				wsaaALockedChdrnum.set(SPACES);
			}
			lastChdrnum.set(utrxpfRec.getChdrnum());
			wsaaJrnseq.set(ZERO);
		}
		/*    If we don't have a fund, we will do no 'dealing' for this*/
		/*    UTRN. However, we do still need to process it.*/
		if (isEQ(utrxpfRec.getUnitVirtualFund(), SPACES)) {
			ct04Value++;
			wsspEdterror.set(varcom.oK);
			return ;
		}
		/*    Get the unit price for this fund. If no price is to be*/
		/*    found there isn't much we can do with this UTRN.*/
		getFundPrice2700();
		if (isEQ(ufpricerec.statuz, varcom.endp)) {
			ct05Value++;
			return ;
		}
		wsspEdterror.set(varcom.oK);
	}


protected void softlockPolicy2600()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(utrxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(utrxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(utrxpfRec.getChdrcoy());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(utrxpfRec.getChdrnum());
			stringVariable1.setStringInto(conlogrec.params);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++;
			ct02Value++;
		}
		else {
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
	}


protected void getFundPrice2700()
	{
		ufpricerec.function.set("DEALP");
		ufpricerec.mode.set("BATCH");
		ufpricerec.company.set(utrxpfRec.getChdrcoy());
		ufpricerec.unitVirtualFund.set(utrxpfRec.getUnitVirtualFund());
		ufpricerec.unitType.set(utrxpfRec.getUnitType());
		ufpricerec.effdate.set(utrxpfRec.getMoniesDate());
		ufpricerec.bsscIODate.set(bsscIO.getEffectiveDate());
		ufpricerec.nowDeferInd.set(utrxpfRec.getNowDeferInd());
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)
		&& isNE(ufpricerec.statuz, varcom.endp)) {
			syserrrec.params.set(ufpricerec.ufpriceRec);
			syserrrec.statuz.set(ufpricerec.statuz);
			fatalError600();
		}
		wsaaUfprPriceDate.set(ufpricerec.priceDate);
		wsaaUfprBarePrice.set(ufpricerec.barePrice);
	}


protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
					dealTheUtrn3020();
				case updates3030: 
					updates3030();
				case triggerProcessing3070: 
					triggerProcessing3070();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected Utrnpf update3010()
	{
		lifacmvrec.acctamt.set(0);
		lifacmvrec.origamt.set(0);
		utrnalo = readhUtrn5100();
		if(utrnalo == null){
			goTo(GotoLabel.exit3090);
		}
			
		/*  If the UDEL is to be included in the schedule, delete*/
		/*  the UDEL if all the UTRNs for the previous contract*/
		/*  have been processed (WSAA-DELETE-UDEL = 'Y' for every*/
		/*  contract.  THIS FLAG WILL BE INITIALISED TO ' ' FOR*/
		/*  EVERY UTRX and the final value checked here.*/
		if (isEQ(bprdIO.getSystemParam02(), "UDEL")) {
			/*  This is effectively detecting whether the previous*/
			/*  contract (WSAA-UDEL-CHDRNUM) was processed in 6100-*/
			/*  REWRT-UTRN section.*/
			if (isEQ(wsaaDeleteUdel, " ")) {
				wsaaDeleteUdel.set("N");
			}
			/*  We have read another contract, so should the UDEL*/
			/*  for the previous one be deleted?  Then reset all*/
			/*  variables.*/
			if (isNE(utrnalo.getChdrcoy(), wsaaUdelChdrcoy)
			|| isNE(utrnalo.getChdrnum(), wsaaUdelChdrnum)) {
				if (udelToBeDeleted.isTrue()) {
					deleteUdel3200();
				}
				wsaaUdelChdrcoy.set(utrnalo.getChdrcoy());
				wsaaUdelChdrnum.set(utrnalo.getChdrnum());
				wsaaDeleteUdel.set(" ");
			}
			else {
				/*  If we are still processing the same contract, reset*/
				/*  the UDEL flag.*/
				if (isEQ(wsaaDeleteUdel, "Y")) {
					wsaaDeleteUdel.set(" ");
				}
			}
		}
		setAcctLvlForPostings5200(utrnalo);
		if (isEQ(utrnalo.getUnitVirtualFund(), SPACES)) {
			processUtrnWithoutFund5300(utrnalo);
			rewrtUtrn6100(utrnalo);
			goTo(GotoLabel.triggerProcessing3070);
		}
		return utrnalo;
	}

protected void dealTheUtrn3020()
	{
		Utrnpf utrnaloIO = utrnalo;
		if(loadTD5F61500(utrnaloIO))
			readhUtrs5401(utrnaloIO);
		else			
			readhUtrs5400(utrnaloIO);
		/*    If the modify units indicator is on then we do not require*/
		/*    any other processing as we are only completing the UTRN and*/
		/*    updating the Unit Summary record (UTRS).*/
		if (utrnaloIO.getCanInitUnitInd()!=null && isEQ(utrnaloIO.getCanInitUnitInd(), "Y")) {
			ct07Value++;
			goTo(GotoLabel.updates3030);
		}
		/*    The object for the next section is to set the field WSAA-PRIC*/
		/*    which will be used for all subsequent processing for this*/
		/*    buy/sell UTRN.*/
		/*    If all the following UTRNALO fields have entries in them we*/
		/*    can be confident that we are dealing with a completed UTRN. I*/
		/*    the case of a reversal, we most definitely do not want to get*/
		/*    a new price: we want to reverse the existing amounts, no othe*/
		if (utrnaloIO.getNofUnits().compareTo(BigDecimal.ZERO)!=0
		&& utrnaloIO.getNofDunits().compareTo(BigDecimal.ZERO)!=0
		&& utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO)!=0
		&& utrnaloIO.getContractAmount().compareTo(BigDecimal.ZERO)!=0
		&& utrnaloIO.getPriceUsed().compareTo(BigDecimal.ZERO)!=0
		&& utrnaloIO.getPriceDateUsed()!=0
		&& utrnaloIO.getUnitBarePrice().compareTo(BigDecimal.ZERO)!=0) {
			wsaaReversalUtrn.set("Y");
		}
		else {
			wsaaReversalUtrn.set("N");
		}
		if (reversalUtrn.isTrue()) {
			wsaaPrice.set(utrnaloIO.getPriceUsed());
			wsaaUfprPriceDate.set(utrnaloIO.getPriceDateUsed());
			wsaaUfprBarePrice.set(utrnaloIO.getUnitBarePrice());
		}
		else {
			getBuySellPrice5500(utrnaloIO);
		}
		if (utrnaloIO.getSurrenderPercent().compareTo(BigDecimal.ZERO)!=0
		&& utrspf.isUpdateFlag()
		&& !reversalUtrn.isTrue()) {
			calcSurrenderPrcnt5600(utrnaloIO);
		}
		if (!reversalUtrn.isTrue()) {
			completeUtrnDetails5700(utrnaloIO);
		}
		/*    If the UTRNPF rec has got this far and still has no amount*/
		/*    or units then it must have dropped through the above*/
		/*    processing. This can happen for 'dummy' trans which are sent*/
		/*    through with a 'trigger' module only for delayed processing.*/
		if (utrnaloIO.getNofUnits().compareTo(BigDecimal.ZERO)==0
		&& utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO)==0
		&& isNE(utrnaloIO.getTriggerModule(), SPACES)) {
			utrnaloIO.setFeedbackInd("Y");
			rewrtUtrn6100(utrnaloIO);
			ct08Value++;
			goTo(GotoLabel.triggerProcessing3070);
		}
		else {
			ct09Value++;
		}
		/*    In the situation where the incoming transaction is negative*/
		/*    (i.e. units are being sold) and the surrender percentage is*/
		/*    zero (i.e. a specific amount is surrendered) we need to check*/
		/*    against existing funds to ensure the coverage has sufficient*/
		/*    units to satisfy the sell. If not take the appropriate action*/
		if (utrnaloIO.getSurrenderPercent().compareTo(BigDecimal.ZERO)<=0
		&& utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO)<0
		&& !reversalUtrn.isTrue()) {
			wsaaInsuffUnitsMeth.set(SPACES);
			checkSuffCovUnits5800(utrnaloIO);
			if (isEQ(wsaaInsuffUnitsMeth, "E")) {
				x7000InsuffValueProcess(utrnaloIO);
				goTo(GotoLabel.exit3090);
			}
			if (isEQ(wsaaInsuffUnitsMeth, "L")) {
				/*     OR  WSAA-INSUFF-UNITS-METH = 'E'                         */
				/*          If we are dealing with Paid-Up. We fill up the rest    */
				/*          of fields that where left blanks.                      */
				if (isEQ(utrnaloIO.getUnitType(), "A")) {
					utrnaloIO.setPriceUsed(wsaaPrice.getbigdata());
					utrnaloIO.setFeedbackInd("Y");
					utrnaloIO.setPriceDateUsed(wsaaUfprPriceDate.toLong());
					utrnaloIO.setUnitBarePrice(wsaaUfprBarePrice.getbigdata());
				}
				rewrtUtrn6100(utrnaloIO);
				goTo(GotoLabel.triggerProcessing3070);
			}
		}
		accountingPostings5900(utrnaloIO);
		/*    There may be situations where the monies date is in the past*/
		/*    and the  price  used  has  changed since that date. In this*/
		/*    case a fluctuation will arise as the company will have*/
		/*    allocated units at the monies date and yet had to buy at*/
		/*    todays price.  The price used can never be amended as this*/
		/*    could lead to potential fraud situations and would make*/
		/*    the ledger even more difficult to audit.*/
		if (isNE(utrnaloIO.getMoniesDate(), datcon1rec.intDate)) {
			fluctuationCheck6000(utrnaloIO);
		}
	}

protected void updates3030()
	{
		Utrnpf utrnaloIO = utrnalo; 
		utrnaloIO.setPriceDateUsed(wsaaUfprPriceDate.toLong());
		utrnaloIO.setPriceUsed(wsaaPrice.getbigdata());
		utrnaloIO.setUnitBarePrice(wsaaUfprBarePrice.getbigdata());
		utrnaloIO.setFeedbackInd("Y");
		rewrtUtrn6100(utrnaloIO);
		/*    We must now create the UREP record for reporting purposes    */
		/*    in B5106. This file will be cleared prior to the first run   */
		/*    of B5102 on the next run of Unit Deal. This will enable the  */
		/*    report to be produced independantly of the Unit Deal         */
		/*    overnight batch schedule - particularly if overrunning       */
		/*    the batch window.                                            */
		createUrep6150(utrnaloIO);
		utrspf.setCurrentUnitBal(utrspf.getCurrentUnitBal().add(utrnaloIO.getNofUnits()));
		utrspf.setCurrentDunitBal(utrspf.getCurrentDunitBal().add(utrnaloIO.getNofDunits()));
		/*    We must now update the UFND which has a total of all*/
		/*    units in a fund. Instead of performing reads and rewrites*/
		/*    for each UTRN we process, we store the values in working*/
		/*    storage. When MainB is ready to commit the 3500 section*/
		/*    will be performed where we will flush the working storage*/
		/*    values to UFND.*/
		int i=1;
		for (; !(isGT(i, wsaaUfndSize)
		|| (isEQ(wsaaUfndFund[i], utrnaloIO.getUnitVirtualFund())
		&& isEQ(wsaaUfndType[i], utrnaloIO.getUnitType()))
		|| isEQ(wsaaUfndType[i], SPACES)); i++)
{
			/*CONTINUE_STMT*/
		}
		if (i> wsaaUfndSize) {
			syserrrec.params.set(utrnaloIO.toString());
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		if (isEQ(wsaaUfndType[i], SPACES)) {
			wsaaUfndFund[i].set(utrnaloIO.getUnitVirtualFund());
			wsaaUfndType[i].set(utrnaloIO.getUnitType());
			wsaaUfndNofUnits[i].set(utrnaloIO.getNofUnits());
		}
		else {
			wsaaUfndNofUnits[i].add(new PackedDecimalData(16,5).init(utrnaloIO.getNofUnits()));
		}
	}

protected void triggerProcessing3070()
	{
		if (isNE(utrnalo.getTriggerModule(), SPACES)) 
		{
			
			callTrigger6300(utrnalo);
		}
	}
protected void updateutrn100()
{
	if (updateUtrnpfList != null) {
		utrnpfDAO.updateUtrnpfRecord(updateUtrnpfList);				
	}
}

protected void deleteUdel3200()
 {
		if (deleteUdelpfList == null) {
			deleteUdelpfList = new ArrayList<>();
		}
		Udelpf udelIO = new Udelpf();
		udelIO.setChdrcoy(wsaaUdelChdrcoy.toString());
		udelIO.setChdrnum(wsaaUdelChdrnum.toString());
		deleteUdelpfList.add(udelIO);
		ct36Value++;
	}

protected void commit3500()
 {
		/* In the pre-commit section we simply loop through the accumulated */
		/* totals of units and update the respective fund totals. Firtsly, */
		/* get a lock on the Process so that don't run into a "deadly */
		/* embrace" problem with the other copies of B5102 which may */
		/* trying to update UFND at the same time. */
		/* Since the updating of UFND is deferred to the next step of */
		/* unit deal processing, no need to hold the process. We do */
		/* not want to wait --- try to enhance the performance. */
		/* MOVE ZERO TO WSAA-DLY-COUNT. <V65L15> */
		/* <V65L15> */
		/* 3520-GET-A-PROCESS-LOCK. <V65L15> */
		/* <V65L15> */
		/* MOVE 'LOCK' TO SFTL-FUNCTION <V65L15> */
		/* MOVE BSPR-COMPANY TO SFTL-COMPANY <V65L15> */
		/* MOVE 'PR' TO SFTL-ENTTYP <V65L15> */
		/* MOVE WSAA-PROG TO SFTL-ENTITY <V65L15> */
		/* MOVE BPRD-AUTH-CODE TO SFTL-TRANSACTION <V65L15> */
		/* MOVE 999999 TO SFTL-USER <V65L15> */
		/* <V65L15> */
		/* CALL 'SFTLOCK' USING SFTL-SFTLOCK-REC. <V65L15> */
		/* <V65L15> */
		/* IF SFTL-STATUZ = 'LOCK' <V65L15> */
		/* IF WSAA-DLY-COUNT = 40 <V65L15> */
		/* MOVE SFTL-SFTLOCK-REC TO SYSR-PARAMS <V65L15> */
		/* MOVE PLCK TO SYSR-STATUZ <V65L15> */
		/* PERFORM 600-FATAL-ERROR <V65L15> */
		/* ELSE <V65L15> */
		/* ADD 1 TO WSAA-DLY-COUNT <V65L15> */
		/* MOVE 'DLYJOB DLY(3)' TO WSAA-QCMDEXC <V65L15> */
		/* CALL 'QCMDEXC' USING WSAA-QCMDEXC <V65L15> */
		/* WSAA-QCMDEXC-LENGTH <V65L15> */
		/* GO TO 3520-GET-A-PROCESS-LOCK <V65L15> */
		/* ELSE <V65L15> */
		/* IF SFTL-STATUZ NOT = O-K <V65L15> */
		/* MOVE SFTL-SFTLOCK-REC TO SYSR-PARAMS <V65L15> */
		/* MOVE SFTL-STATUZ TO SYSR-STATUZ <V65L15> */
		/* PERFORM 600-FATAL-ERROR. <V65L15> */
		int i = 1;
		for (; !(isGT(i, wsaaUfndSize) || isEQ(wsaaUfndType[i], SPACES)); i++) {
			Ufnspf ufnsIO = new Ufnspf();
			ufnsIO.setCompany(bsprIO.getCompany().toString());
			ufnsIO.setVrtfund(wsaaUfndFund[i].toString());
			ufnsIO.setUnityp(wsaaUfndType[i].toString());
			ufnsIO.setNofunts(wsaaUfndNofUnits[i].getbigdata());
			wsaaUfndFund[i].set(SPACES);
			wsaaUfndType[i].set(SPACES);
			wsaaUfndNofUnits[i].set(ZERO);
			ufnsIO.setJobno(bsscIO.getScheduleNumber().toInt());
			if (insertUfnspfList == null) {
				insertUfnspfList = new ArrayList<>();
			}
			insertUfnspfList.add(ufnsIO);
		}
		bwrkIO.setDataKey(bsprIO.getDataKey());
		bwrkIO.setWorkAreaData(wsaaBwrkData);
		bwrkIO.setFormat(formatsInner.bwrkrec);
		bwrkIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, bwrkIO);
		if (isNE(bwrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bwrkIO.getParams());
			fatalError600();
		}
		if (deleteUdelpfList != null) {
			udelpfDAO.deleteUdelpfRecordByUdel(deleteUdelpfList);
			deleteUdelpfList.clear();
		}
		if (updateUtrnpfList != null) {
			utrnpfDAO.updateUtrnpfRecord(updateUtrnpfList);
			updateUtrnpfList.clear();
		}
		if (insertUtrspfList != null) {
			utrspfDAO.insertUtrspfRecord(insertUtrspfList);
			insertUtrspfList.clear();
		}
		if (updateUtrspfList != null) {
			utrspfDAO.updateUtrspfRecord(updateUtrspfList);
			updateUtrspfList.clear();
		}
		if (updateCovrpfDebtList != null) {
			covrpfDAO.updateCovrDebtRecord(updateCovrpfDebtList);
			updateCovrpfDebtList.clear();
		}
		if (updateCovrpfList != null) {
			covrpfDAO.updateCovrStatRecord(updateCovrpfList);
			updateCovrpfList.clear();
		}
		if (insertUderpfList != null) {
			uderpfDAO.insertUderpfRecord(insertUderpfList);
			insertUderpfList.clear();
		}
		if (insertUdivpfList != null) {
			udivpfDAO.insertUdivpfRecord(insertUdivpfList);
			insertUdivpfList.clear();
		}
		if (insertUreppfList != null) {
			ureppfDAO.insertUreppfRecord(insertUreppfList);
			insertUreppfList.clear();
		}
		if (insertUfnspfList != null) {
			ufnspfDAO.insertUfnspfRecord(insertUfnspfList);
			insertUfnspfList.clear();
		}
		contotrec.totval.set(ct01Value);
		contotrec.totno.set(1);
		callContot001();
		ct01Value = 0;
		contotrec.totval.set(ct02Value);
		contotrec.totno.set(2);
		callContot001();
		ct02Value = 0;
		contotrec.totval.set(ct03Value);
		contotrec.totno.set(3);
		callContot001();
		ct03Value = 0;
		contotrec.totval.set(ct04Value);
		contotrec.totno.set(4);
		callContot001();
		ct04Value = 0;
		contotrec.totval.set(ct05Value);
		contotrec.totno.set(5);
		callContot001();
		ct05Value = 0;
		contotrec.totval.set(ct06Value);
		contotrec.totno.set(6);
		callContot001();
		ct06Value = 0;
		contotrec.totval.set(ct07Value);
		contotrec.totno.set(7);
		callContot001();
		ct07Value = 0;
		contotrec.totval.set(ct08Value);
		contotrec.totno.set(8);
		callContot001();
		ct08Value = 0;
		contotrec.totval.set(ct09Value);
		contotrec.totno.set(9);
		callContot001();
		ct09Value = 0;
		contotrec.totval.set(ct10Value);
		contotrec.totno.set(10);
		callContot001();
		ct10Value = 0;
		contotrec.totval.set(ct11Value);
		contotrec.totno.set(11);
		callContot001();
		ct11Value = 0;
		contotrec.totval.set(ct12Value);
		contotrec.totno.set(12);
		callContot001();
		ct12Value = 0;
		contotrec.totval.set(ct13Value);
		contotrec.totno.set(13);
		callContot001();
		ct13Value = 0;
		contotrec.totval.set(ct14Value);
		contotrec.totno.set(14);
		callContot001();
		ct14Value = 0;
		contotrec.totval.set(ct15Value);
		contotrec.totno.set(15);
		callContot001();
		ct15Value = 0;
		contotrec.totval.set(ct16Value);
		contotrec.totno.set(16);
		callContot001();
		ct16Value = 0;
		contotrec.totval.set(ct17Value);
		contotrec.totno.set(17);
		callContot001();
		ct17Value = 0;
		contotrec.totval.set(ct20Value);
		contotrec.totno.set(20);
		callContot001();
		ct20Value = 0;
		contotrec.totval.set(ct21Value);
		contotrec.totno.set(21);
		callContot001();
		ct21Value = BigDecimal.ZERO;
		contotrec.totval.set(ct22Value);
		contotrec.totno.set(22);
		callContot001();
		ct22Value = BigDecimal.ZERO;
		contotrec.totval.set(ct23Value);
		contotrec.totno.set(23);
		callContot001();
		ct23Value = BigDecimal.ZERO;
		contotrec.totval.set(ct24Value);
		contotrec.totno.set(24);
		callContot001();
		ct24Value = BigDecimal.ZERO;
		contotrec.totval.set(ct25Value);
		contotrec.totno.set(25);
		callContot001();
		ct25Value = BigDecimal.ZERO;
		contotrec.totval.set(ct26Value);
		contotrec.totno.set(26);
		callContot001();
		ct26Value = BigDecimal.ZERO;
		contotrec.totval.set(ct27Value);
		contotrec.totno.set(27);
		callContot001();
		ct27Value = BigDecimal.ZERO;
		contotrec.totval.set(ct28Value);
		contotrec.totno.set(28);
		callContot001();
		ct28Value = BigDecimal.ZERO;
		contotrec.totval.set(ct29Value);
		contotrec.totno.set(29);
		callContot001();
		ct29Value = BigDecimal.ZERO;
		contotrec.totval.set(ct30Value);
		contotrec.totno.set(30);
		callContot001();
		ct30Value = BigDecimal.ZERO;
		contotrec.totval.set(ct31Value);
		contotrec.totno.set(31);
		callContot001();
		ct31Value = BigDecimal.ZERO;
		contotrec.totval.set(ct32Value);
		contotrec.totno.set(32);
		callContot001();
		ct32Value = BigDecimal.ZERO;
		contotrec.totval.set(ct33Value);
		contotrec.totno.set(33);
		callContot001();
		ct33Value = BigDecimal.ZERO;
		contotrec.totval.set(ct34Value);
		contotrec.totno.set(34);
		callContot001();
		ct34Value = BigDecimal.ZERO;
		contotrec.totval.set(ct35Value);
		contotrec.totno.set(35);
		callContot001();
		ct35Value = 0;
		contotrec.totval.set(ct36Value);
		contotrec.totno.set(36);
		callContot001();
		ct36Value = 0;
		contotrec.totval.set(ct37Value);
		contotrec.totno.set(37);
		callContot001();
		ct37Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		/*  If the UDEL is to be included in the schedule, delete*/
		/*  the UDEL if all the UTRNs for that contract have been*/
		/*  processed. The flag WSAA-DELETE-UDEL for the first*/
		/*  UTRN for the contract will be set to spaces then set*/
		/*  to 'N' if not processed and 'Y' (in DELETE-UDEL*/
		/*  section) if processed.  Once the flag is set to 'N'*/
		/*  once for the contract, it will remain at 'N' for*/
		/*  the rest of the processing for that contract.*/
		if (isEQ(bprdIO.getSystemParam02(), "UDEL") && udelToBeDeleted.isTrue()) {
				deleteUdel3200();
		}
		/*    Ensure we flush the UFND accumulations.*/
		/*  The following perform is done out of MAINB and is not       */
		/*  reqired here.                                               */
		/*PERFORM 3500-COMMIT.                                         */
		/* Delete the Batch working area record.                        */
		bwrkIO.setDataKey(bsprIO.getDataKey());
		bwrkIO.setFormat(formatsInner.bwrkrec);
		bwrkIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, bwrkIO);
		if (isNE(bwrkIO.getStatuz(), varcom.oK)
		&& isNE(bwrkIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(bwrkIO.getStatuz());
			syserrrec.params.set(bwrkIO.getParams());
			fatalError600();
		}
		if (isEQ(bwrkIO.getStatuz(), varcom.oK)) {
			bwrkIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, bwrkIO);
			if (isNE(bwrkIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(bwrkIO.getStatuz());
				syserrrec.params.set(bwrkIO.getParams());
				fatalError600();
			}
		}
		lsaaStatuz.set(varcom.oK);
	}

protected Utrnpf readhUtrn5100()
	{
		boolean foundFlag = false;
		Utrnpf resultUtrnpf = null;
		if (utrnaloMap != null && utrnaloMap.containsKey(utrxpfRec.getChdrnum())) {
			for (Utrnpf u : utrnaloMap.get(utrxpfRec.getChdrnum())) {
				if (u.getChdrcoy().equals(utrxpfRec.getChdrcoy()) && u.getLife().equals(utrxpfRec.getLife())
						&& u.getCoverage().equals(utrxpfRec.getCoverage()) && u.getRider().equals(utrxpfRec.getRider())
						&& (u.getPlanSuffix() == utrxpfRec.getPlanSuffix())
						&& u.getUnitType().equals(utrxpfRec.getUnitType())
						&& u.getUnitVirtualFund().equals(utrxpfRec.getUnitVirtualFund())
						&& (u.getProcSeqNo() == utrxpfRec.getProcSeqNo())
						&& (u.getTranno() == utrxpfRec.getTranno())
						&& (u.getMoniesDate().intValue() == utrxpfRec.getMoniesDate())
						&& !(uniqueNum.contains(u.getUniqueNumber()))) {//ILIFE-9312
				
					foundFlag = true;
					resultUtrnpf = u;
					uniqueNum.add(u.getUniqueNumber());
					break;
				}
			}
		}
		if(!foundFlag){
		/*    It is possible, although very unlikely, that a reversal may*/
		/*    have been done against the contract which could result in*/
		/*    the UTRN disappearing. Log this fact and skip the UTRN.*/
			ct06Value++;
		}
		return resultUtrnpf;
	}


protected void setAcctLvlForPostings5200(Utrnpf utrnaloIO)
 {
		int i = 1;
		for (; !(isGT(i, wsaaT5688Size) || isEQ(wsaaT5688Cnttype[i], SPACES) || (isEQ(wsaaT5688Cnttype[i],
				utrnaloIO.getCnttyp()) && isLTE(wsaaT5688Itmfrm[i], utrnaloIO.getMoniesDate()))); i++) {
			/* CONTINUE_STMT */
		}
		if (isGT(i, wsaaT5688Size) || isEQ(wsaaT5688Cnttype[i], SPACES)) {
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5688);
			itdmIO.setItemitem(utrnaloIO.getCnttyp());
			itdmIO.setItmfrm(utrnaloIO.getMoniesDate());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		wsaaComponLevelAccounted.set(wsaaT5688Comlvlacc[i]);

	}


protected void processUtrnWithoutFund5300(Utrnpf utrnaloIO)
	{
		lifacmvrec.origcurr.set(utrnaloIO.getCntcurr());
		lifacmvrec.origamt.set(utrnaloIO.getContractAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(5);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
			if(contotal!=null){
				ct21Value = ct21Value.add(contotal);
			}
			t5645Ix.set(29);
			wsaaT5645Sacscode[29].set(utrnaloIO.getSacscode());
			wsaaT5645Sacstype[29].set(utrnaloIO.getSacstyp());
			wsaaT5645Glmap[29].set(utrnaloIO.getGenlcde());
			contotal = x1000CallLifacmv(utrnaloIO);
			if(contotal!=null){
				ct22Value = ct22Value.add(contotal);
			}
		}
		else {
			t5645Ix.set(22);
			lifacmvrec.substituteCode[6].set(SPACES);
			BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
			if(contotal!=null){
				ct21Value = ct21Value.add(contotal);
			}
			t5645Ix.set(16);
			wsaaT5645Sacscode[16].set(utrnaloIO.getSacscode());
			wsaaT5645Sacstype[16].set(utrnaloIO.getSacstyp());
			wsaaT5645Glmap[16].set(utrnaloIO.getGenlcde());
			contotal = x1000CallLifacmv(utrnaloIO);
			if(contotal!=null){
				ct22Value = ct22Value.add(contotal);
			}
		}
		utrnaloIO.setFeedbackInd("Y");

	}


protected void readhUtrs5401(Utrnpf utrnaloIO)
	{
		Utrspf resultUtrspf = null;
		if (utrsMap != null && utrsMap.containsKey(utrxpfRec.getChdrnum())) {
			for (Utrspf u : utrsMap.get(utrxpfRec.getChdrnum())) {
				if (u.getChdrcoy().equals(utrxpfRec.getChdrcoy()) && u.getLife().equals(utrxpfRec.getLife())
						&& u.getCoverage().equals(utrxpfRec.getCoverage()) && u.getRider().equals(utrxpfRec.getRider())
						&& (u.getPlanSuffix() == utrxpfRec.getPlanSuffix())
						&& u.getUnitType().equals(utrxpfRec.getUnitType())
						&& u.getUnitVirtualFund().equals(utrxpfRec.getUnitVirtualFund())
						&& u.getFundPool().equals(utrxpfRec.getFundPool())
						&& !(uniqueNumUtrs.contains(u.getUniqueNumber())))  //IBPLIFE-6876
					{
							resultUtrspf = u;
							uniqueNumUtrs.add(u.getUniqueNumber());          //IBPLIFE-6876     
					break;
					}
			}
		}
		if(resultUtrspf == null){
			String vfund="";
			Utrspf utrsIO = new Utrspf();
			utrsIO.setChdrcoy(utrnaloIO.getChdrcoy());
			utrsIO.setChdrnum(utrnaloIO.getChdrnum());
			utrsIO.setLife(utrnaloIO.getLife());
			utrsIO.setCoverage(utrnaloIO.getCoverage());
			utrsIO.setRider(utrnaloIO.getRider());
			utrsIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
			utrsIO.setUnitVirtualFund(utrnaloIO.getUnitVirtualFund());
			utrsIO.setUnitType(utrnaloIO.getUnitType());
			vfund=utrnaloIO.getUnitVirtualFund();
			utrsIO.setCurrentUnitBal(BigDecimal.ZERO);
			utrsIO.setCurrentDunitBal(BigDecimal.ZERO);
			utrsIO.setFundPool(utrnaloIO.getFundPool());
			if(insertUtrspfList == null){
				insertUtrspfList = new ArrayList<>();
			}
			boolean found=false;
			if (insertUtrspfList!=null && insertUtrspfList.size()>0)
			{
				for (Utrspf u : insertUtrspfList) 
				{
					if (u.getUnitVirtualFund()==vfund)
					{
						found=true;
					}					
				}
			}
			if (found==false)
			{
				insertUtrspfList.add(utrsIO);
			}
			utrspf = utrsIO;
			ct17Value++;
			return;
		}
		if(updateUtrspfList == null){
			updateUtrspfList = new ArrayList<>();
		}
		resultUtrspf.setUpdateFlag(true);
		updateUtrspfList.add(resultUtrspf);
		utrspf = resultUtrspf;
		ct17Value++;
	}


protected void readhUtrs5400(Utrnpf utrnaloIO)
{
	Utrspf resultUtrspf = null;
	if (utrsMap != null && utrsMap.containsKey(utrxpfRec.getChdrnum())) {
		for (Utrspf u : utrsMap.get(utrxpfRec.getChdrnum())) {
			if (u.getChdrcoy().equals(utrxpfRec.getChdrcoy()) && u.getLife().equals(utrxpfRec.getLife())
					&& u.getCoverage().equals(utrxpfRec.getCoverage()) && u.getRider().equals(utrxpfRec.getRider())
					&& (u.getPlanSuffix() == utrxpfRec.getPlanSuffix())
					&& u.getUnitType().equals(utrxpfRec.getUnitType())
					&& u.getUnitVirtualFund().equals(utrxpfRec.getUnitVirtualFund())
					&& !(uniqueNumUtrs.contains(u.getUniqueNumber()))){     //IBPLIFE-6876
				resultUtrspf = u;
				uniqueNumUtrs.add(u.getUniqueNumber());                     //IBPLIFE-6876
				break;
			}
		}
	}
	if(resultUtrspf == null){
		Utrspf utrsIO = new Utrspf();
		utrsIO.setChdrcoy(utrnaloIO.getChdrcoy());
		utrsIO.setChdrnum(utrnaloIO.getChdrnum());
		utrsIO.setLife(utrnaloIO.getLife());
		utrsIO.setCoverage(utrnaloIO.getCoverage());
		utrsIO.setRider(utrnaloIO.getRider());
		utrsIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
		utrsIO.setUnitVirtualFund(utrnaloIO.getUnitVirtualFund());
		utrsIO.setUnitType(utrnaloIO.getUnitType());
		
		utrsIO.setCurrentUnitBal(BigDecimal.ZERO);
		utrsIO.setCurrentDunitBal(BigDecimal.ZERO);
		
		if(insertUtrspfList == null){
			insertUtrspfList = new ArrayList<>();
		}
		insertUtrspfList.add(utrsIO);
		utrspf = utrsIO;
		ct17Value++;
		return;
	}
	if(updateUtrspfList == null){
		updateUtrspfList = new ArrayList<>();
	}
	resultUtrspf.setUpdateFlag(true);
	updateUtrspfList.add(resultUtrspf);
	utrspf = resultUtrspf;
	ct17Value++;

}

protected void getBuySellPrice5500(Utrnpf utrnaloIO)
	{
		/*GET-BUY-SELL-PRICE*/
		wsaaPrice.set(ZERO);
		/*    Search our pre-loaded T6647 to establish whether the*/
		/*    pricing is going to be Bid or Offer Pricing. If a fund*/
		/*    switch has occured use the switch code to look up the*/
		/*    fund switch details from T5544.*/
		getT66475510(utrnaloIO);
		/*    If we have a fund switch scurry off and get the price*/
		/*    for switching.*/
		if (isEQ(utrnaloIO.getSwitchIndicator(), "Y")) {
			getFundSwitchPrice5520(utrnaloIO);
		}
		/*    A relatively ordinary buy/sell UTRN (i.e. not a fund switch).*/
		/*    Log the number of non-fund-switching UTRNs.*/
		if (isNE(utrnaloIO.getSwitchIndicator(), "Y")) {
			if (isEQ(wsaaT6647ArrayInner.wsaaT6647Bidoffer[t6647Ix.toInt()], "B")) {
				wsaaPrice.set(ufpricerec.bidPrice);
			}
			else {
				wsaaPrice.set(ufpricerec.offerPrice);
			}
		}
		/*EXIT*/
	}

protected void getT66475510(Utrnpf utrnaloIO)
	{
		itdmIO.getItemitem().setSub1String(1, 4, utrnaloIO.getBatctrcde());
		itdmIO.getItemitem().setSub1String(5, 4, utrnaloIO.getCnttyp());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT6647ArrayInner.wsaaT6647Rec);
		as1.setIndices(t6647Ix);
		as1.addSearchKey(wsaaT6647ArrayInner.wsaaT6647Key, itdmIO.getItemitem(), true);
		String wsaaNotFound = "";
		if (as1.binarySearch()) {
			wsaaNotFound = "N";
		}
		else {
			wsaaNotFound = "Y";
		}
		if (isEQ(wsaaNotFound, "Y")) {
			itdmIO.getItemitem().setSub1String(5, 4, "***");
			ArraySearch as2 = ArraySearch.getInstance(wsaaT6647ArrayInner.wsaaT6647Rec);
			as2.setIndices(t6647Ix);
			as2.addSearchKey(wsaaT6647ArrayInner.wsaaT6647Key, itdmIO.getItemitem(), true);
			if (as2.binarySearch()) {
				/*CONTINUE_STMT*/
			}
			else {
				itdmIO.setItemcoy(utrnaloIO.getChdrcoy());
				itdmIO.setItemtabl(t6647);
				itdmIO.setItmfrm(bsscIO.getEffectiveDate());
				itdmIO.setFunction(varcom.begn);
				itdmIO.setStatuz(varcom.endp);
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
		}
	}


protected void getFundSwitchPrice5520(Utrnpf utrnaloIO)
	{
		/*    Get the Fund switching rule from T5544 using the switching*/
		/*    method and the contract currency.*/
		itdmIO.getItemitem().setSub1String(1, 4, wsaaT6647ArrayInner.wsaaT6647Swmeth[t6647Ix.toInt()]);
		itdmIO.getItemitem().setSub1String(5, 4, utrnaloIO.getCntcurr());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5544Rec);
		as1.setIndices(t5544Ix);
		as1.addSearchKey(wsaaT5544Key, itdmIO.getItemitem(), true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			itdmIO.setItemcoy(utrnaloIO.getChdrcoy());
			itdmIO.setItemtabl(t5544);
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isGT(utrnaloIO.getContractAmount(), ZERO)
		|| isLT(utrnaloIO.getSurrenderPercent(), ZERO)) {
			fundSwitchBuyPrice5530(utrnaloIO);
		}
		else {
			fundSwitchSellPrice5540(utrnaloIO);
		}
	}

protected void fundSwitchBuyPrice5530(Utrnpf utrnaloIO)
 {
		/* Start with T5544 flags. */
		if (isEQ(wsaaT5544Btobid[t5544Ix.toInt()], "Y")) {
			wsaaPrice.set(ufpricerec.bidPrice);
			return;
		}
		if (isEQ(wsaaT5544Btooff[t5544Ix.toInt()], "Y")) {
			wsaaPrice.set(ufpricerec.offerPrice);
			return;
		}
		if (isEQ(wsaaT5544Btodisc[t5544Ix.toInt()], "Y")) {
			compute(wsaaPrice, 6).setRounded(
					div(mult(ufpricerec.offerPrice, (sub(100, wsaaT5544DiscOfferPercent[t5544Ix.toInt()]))), 100));
			return;
		}
		/* No joy so far. Try T5515 flags. */
		itdmIO.setItemitem(utrnaloIO.getUnitVirtualFund());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5515Rec);
		as1.setIndices(t5515Ix);
		as1.addSearchKey(wsaaT5515Key, itdmIO.getItemitem(), true);
		if (as1.binarySearch()) {
			/* CONTINUE_STMT */
		} else {
			itdmIO.setItemcoy(utrnaloIO.getChdrcoy());
			itdmIO.setItemtabl(t5515);
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaT5515Btobid[t5515Ix.toInt()], "Y")) {
			wsaaPrice.set(ufpricerec.bidPrice);
			return;
		}
		if (isEQ(wsaaT5515Btooff[t5515Ix.toInt()], "Y") || isEQ(wsaaT5515Otoff[t5515Ix.toInt()], "Y")) {
			wsaaPrice.set(ufpricerec.offerPrice);
			return;
		}
		if (isEQ(wsaaT5515Btodisc[t5515Ix.toInt()], "Y")) {
			compute(wsaaPrice, 6).setRounded(
					div(mult(ufpricerec.offerPrice, (sub(100, wsaaT5515DiscOfferPercent[t5515Ix.toInt()]))), 100));
			return;
		}
		ct15Value++;
	}


protected void fundSwitchSellPrice5540(Utrnpf utrnaloIO)
	{
		/* Start with T5544 flags.*/
		if (isNE(wsaaT5544BidToFund[t5544Ix.toInt()], "Y")) {
			if (isEQ(wsaaT5544Btobid[t5544Ix.toInt()], "Y")
			|| isEQ(wsaaT5544Btodisc[t5544Ix.toInt()], "Y")
			|| isEQ(wsaaT5544Btooff[t5544Ix.toInt()], "Y")) {
				wsaaPrice.set(ufpricerec.bidPrice);
			}
			else {
				wsaaPrice.set(ufpricerec.offerPrice);
			}
			return ;
		}
		/*    No joy so far. Try T5515 flags.*/
		itdmIO.setItemitem(utrnaloIO.getUnitVirtualFund());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5515Rec);
		as1.setIndices(t5515Ix);
		as1.addSearchKey(wsaaT5515Key, itdmIO.getItemitem(), true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			itdmIO.setItemcoy(utrnaloIO.getChdrcoy());
			itdmIO.setItemtabl(t5515);
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaT5515Btobid[t5515Ix.toInt()], "Y")
		|| isEQ(wsaaT5515Btodisc[t5515Ix.toInt()], "Y")
		|| isEQ(wsaaT5515Btooff[t5515Ix.toInt()], "Y")) {
			wsaaPrice.set(ufpricerec.bidPrice);
		}
		else {
			wsaaPrice.set(ufpricerec.offerPrice);
		}
	}


protected void calcSurrenderPrcnt5600(Utrnpf utrnaloIO)
	{
		if (isEQ(utrnaloIO.getSwitchIndicator(), "Y")
		&& utrspf.getCurrentDunitBal().compareTo(BigDecimal.ZERO)==0
		&& utrspf.getCurrentUnitBal().compareTo(BigDecimal.ZERO)==0) {
			wsaaPrice.set(ZERO);
			return ;
		}
		//ILIFE-7007 starts
		 if (IntegralDBProperties.getType().equals(QPBaseDataSource.DATABASE_SQLSERVER))
		 {
				utrnaloIO.setNofDunits(mult(mult(div(utrnaloIO.getSurrenderPercent(), 100), utrspf.getCurrentDunitBal()), -1).getbigdata());
				utrnaloIO.setNofUnits(mult(mult(div(utrnaloIO.getSurrenderPercent(), 100), utrspf.getCurrentUnitBal()), -1).getbigdata());
		 }
		 else
		 {
			PackedDecimalData surrper = new PackedDecimalData(10,2);
			surrper.set(utrnaloIO.getSurrenderPercent());
			utrnaloIO.setNofDunits(mult(mult(div(surrper, 100), utrspf.getCurrentDunitBal()), -1).getbigdata());
			utrnaloIO.setNofUnits(mult(mult(div(surrper, 100), utrspf.getCurrentUnitBal()), -1).getbigdata());
		 }
		 //ILIFE-7007 ends
		utrnaloIO.setFundAmount(div(mult(utrnaloIO.getNofUnits(), wsaaPrice), utrnaloIO.getDiscountFactor()).getbigdata());
		/*  Apply Surrender value penalty if one exists.                   */
		if (utrnaloIO.getSvp().compareTo(BigDecimal.ONE)!=0) {
			utrnaloIO.setFundAmount(mult(utrnaloIO.getFundAmount(), (sub(1, utrnaloIO.getSvp()))).getbigdata());
		}
		/* MOVE UTRNALO-FUND-CURRENCY  TO ZRDP-CURRENCY.                */
		/* MOVE UTRNALO-FUND-AMOUNT    TO ZRDP-AMOUNT-IN.               */
		/* PERFORM X8000-CALL-ROUNDING.                                 */
		/* MOVE ZRDP-AMOUNT-OUT        TO UTRNALO-FUND-AMOUNT.          */
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) != 0) {
			zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
			zrdecplrec.amountIn.set(utrnaloIO.getFundAmount());
			x8000CallRounding();
			utrnaloIO.setFundAmount(zrdecplrec.amountOut.getbigdata());
		}

	}

protected void completeUtrnDetails5700(Utrnpf utrnaloIO)
	{
		if(completeUtrnDetails5710(utrnaloIO)){
			unitsToSellForDebt5750(utrnaloIO);
		}
	}

protected boolean completeUtrnDetails5710(Utrnpf utrnaloIO)
 {
		/* Firstly, check if we have an amount in the Contract * */
		/* Currency, but no amount in the Fund Currency. * */
		/* If yes, then move the Contract Amount to the Fund Amount,* */
		/* applying currency conversion if the Fund Currency is not * */
		/* equal to the Contract Currency. * */
		if (utrnaloIO.getContractAmount().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) == 0) {
			if (isEQ(utrnaloIO.getFundCurrency(), utrnaloIO.getCntcurr())) {
				utrnaloIO.setFundAmount(utrnaloIO.getContractAmount());
				utrnaloIO.setFundRate(BigDecimal.ONE);
			} else {
				conlinkrec.amountIn.set(utrnaloIO.getContractAmount());
				conlinkrec.currIn.set(utrnaloIO.getCntcurr());
				conlinkrec.currOut.set(utrnaloIO.getFundCurrency());
				/* MOVE UFPR-PRICE-DATE TO CLNK-CASHDATE */
				conlinkrec.cashdate.set(utrnaloIO.getMoniesDate());
				if (utrnaloIO.getContractAmount().compareTo(BigDecimal.ZERO) < 0) {
					conlinkrec.function.set("SURR");
				} else {
					conlinkrec.function.set("CVRT");
				}
				zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
				x3000CallXcvrt(utrnaloIO);
				utrnaloIO.setFundAmount(conlinkrec.amountOut.getbigdata());
				utrnaloIO.setFundRate(conlinkrec.rateUsed.getbigdata());
			}
		}
		/* Next, check if we have an amount in the Fund * */
		/* Currency, but no amount in the Contract Currency. * */
		/* If yes, then move the Fund Amount to the Contract Amount,* */
		/* applying currenct conversion if the Fund Currency is not * */
		/* equal to the Contract Currency. * */
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getContractAmount().compareTo(BigDecimal.ZERO) == 0) {
			if (isEQ(utrnaloIO.getFundCurrency(), utrnaloIO.getCntcurr())) {
				utrnaloIO.setContractAmount(utrnaloIO.getFundAmount());
				utrnaloIO.setFundRate(BigDecimal.ONE);
			} else {
				conlinkrec.amountIn.set(utrnaloIO.getFundAmount());
				conlinkrec.currIn.set(utrnaloIO.getFundCurrency());
				conlinkrec.currOut.set(utrnaloIO.getCntcurr());
				/* MOVE UFPR-PRICE-DATE TO CLNK-CASHDATE */
				conlinkrec.cashdate.set(utrnaloIO.getMoniesDate());
				conlinkrec.function.set("CVRT");
				zrdecplrec.currency.set(utrnaloIO.getCntcurr());
				x3000CallXcvrt(utrnaloIO);
				utrnaloIO.setContractAmount(conlinkrec.amountOut.getbigdata());
				utrnaloIO.setFundRate(conlinkrec.rateUsed.getbigdata());
			}
		}
		/* Ensure the Discount Factor is non-zero. * */
		if (utrnaloIO.getDiscountFactor().compareTo(BigDecimal.ZERO) == 0) {
			utrnaloIO.setDiscountFactor(BigDecimal.ONE);
		}
		/* If we have re-entered this program after Debt */
		/* Processing to satisfy a debt, the debt already */
		/* written to the UTRN must be paid off from real */
		/* units then deemed units are calculated from these. */
		/* This calculation is required for if we are selling */
		/* only some of the units on the coverage. */
		if (utrnaloIO.getCovdbtind()!=null&&"D".equals(utrnaloIO.getCovdbtind())) {
			return true;
		}
		/* We only get here if the original amounts in the Fund * */
		/* Currency and the Contract Currency were both zero. * */
		/* If yes, then attempt to calculate the Fund Amount using * */
		/* the Real Units. If this value is zero, then use the * */
		/* Deemed Units. * */
		if (utrnaloIO.getNofUnits().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) == 0) {
			utrnaloIO.setFundAmount(div(mult(utrnaloIO.getNofUnits(), wsaaPrice), utrnaloIO.getDiscountFactor())
					.getbigdata());
			if (isNE(utrnaloIO.getSvp(), 1)) {
				utrnaloIO.setFundAmount(mult(utrnaloIO.getFundAmount(), (sub(1, utrnaloIO.getSvp()))).getbigdata());
			}
		}
		if (utrnaloIO.getNofDunits().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) == 0) {
			utrnaloIO.setFundAmount(mult(utrnaloIO.getNofDunits(), wsaaPrice).getbigdata());
			if (isNE(utrnaloIO.getSvp(), 1)) {
				utrnaloIO.setFundAmount(mult(utrnaloIO.getFundAmount(), (sub(1, utrnaloIO.getSvp()))).getbigdata());
			}
		}
		/* MOVE UTRNALO-FUND-CURRENCY TO ZRDP-CURRENCY. */
		/* MOVE UTRNALO-FUND-AMOUNT TO ZRDP-AMOUNT-IN. */
		/* PERFORM X8000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO UTRNALO-FUND-AMOUNT. */
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) != 0) {
			zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
			zrdecplrec.amountIn.set(utrnaloIO.getFundAmount());
			x8000CallRounding();
			utrnaloIO.setFundAmount(zrdecplrec.amountOut.getbigdata());
		}
		/* We get here if the original amounts in the Fund Currency * */
		/* and the Contract Currency were both zero and we * */
		/* calculated the Fund Amount. * */
		/* We need to calculate the Contract Amount. If Fund * */
		/* Currency equals Contract currency, no problem. Else we * */
		/* need to carry out currency conversion * */
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getContractAmount().compareTo(BigDecimal.ZERO) == 0) {
			if (isEQ(utrnaloIO.getFundCurrency(), utrnaloIO.getCntcurr())) {
				utrnaloIO.setContractAmount(utrnaloIO.getFundAmount());
				utrnaloIO.setFundRate(BigDecimal.ONE);
			} else {
				conlinkrec.amountIn.set(utrnaloIO.getFundAmount());
				conlinkrec.currIn.set(utrnaloIO.getFundCurrency());
				conlinkrec.currOut.set(utrnaloIO.getCntcurr());
				/* MOVE UFPR-PRICE-DATE TO CLNK-CASHDATE */
				conlinkrec.cashdate.set(utrnaloIO.getMoniesDate());
				conlinkrec.function.set("CVRT");
				zrdecplrec.currency.set(utrnaloIO.getCntcurr());
				x3000CallXcvrt(utrnaloIO);
				utrnaloIO.setContractAmount(conlinkrec.amountOut.getbigdata());
				utrnaloIO.setFundRate(conlinkrec.rateUsed.getbigdata());
			}
		}
		/* Finally, we need to fill in the number of deemed and/or * */
		/* real units if either (or both) of these fields are * */
		/* zero. * */
		/* Note how initial units surrender calculates from the * */
		/* real units then pro-rata's the deemed from this. * */
		if (isEQ(utrnaloIO.getUnitType(), "I") && utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) < 0
				&& utrnaloIO.getNofUnits().compareTo(BigDecimal.ZERO) == 0) {
			utrnaloIO.setNofUnits(mult(div(utrnaloIO.getFundAmount(), wsaaPrice), utrnaloIO.getDiscountFactor())
					.getbigdata());
			if (isNE(utrnaloIO.getSvp(), 1)) {
				utrnaloIO.setNofUnits(mult(utrnaloIO.getNofUnits(), (add(1, utrnaloIO.getSvp()))).getbigdata());
			}
			utrnaloIO.setNofDunits(mult(utrnaloIO.getNofUnits(),
					(div(utrspf.getCurrentDunitBal(), utrspf.getCurrentUnitBal()))).getbigdata());
			return false;
		}
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getNofDunits().compareTo(BigDecimal.ZERO) == 0) {
			utrnaloIO.setNofDunits(div(utrnaloIO.getFundAmount(), wsaaPrice).getbigdata());
			if (isNE(utrnaloIO.getSvp(), 1)) {
				utrnaloIO.setNofUnits(mult(utrnaloIO.getNofUnits(), (add(1, utrnaloIO.getSvp()))).getbigdata());
			}
		}
		if (utrnaloIO.getNofDunits().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getNofUnits().compareTo(BigDecimal.ZERO) == 0) {
			utrnaloIO.setNofUnits(mult(utrnaloIO.getNofDunits(), utrnaloIO.getDiscountFactor()).getbigdata());
		}
		if (utrnaloIO.getNofUnits().compareTo(BigDecimal.ZERO) != 0
				&& utrnaloIO.getNofDunits().compareTo(BigDecimal.ZERO) == 0) {
			utrnaloIO.setNofDunits(div(utrnaloIO.getNofUnits(), utrnaloIO.getDiscountFactor()).getbigdata());
		}
		return false;
	}

protected void unitsToSellForDebt5750(Utrnpf utrnaloIO)
	{
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO)!=0
		&& utrnaloIO.getNofUnits().compareTo(BigDecimal.ZERO)==0) {
			utrnaloIO.setNofUnits(mult(div(utrnaloIO.getFundAmount(), wsaaPrice), utrnaloIO.getDiscountFactor()).getbigdata());
			if (isNE(utrnaloIO.getSvp(), 1)) {
				setPrecision(utrnaloIO.getNofUnits(), 5);
				utrnaloIO.setNofUnits(mult(utrnaloIO.getNofUnits(), (add(1, utrnaloIO.getSvp()))).getbigdata());
			}
		}
		/* Let's see if there is still any negative units left once        */
		/* they have been sold to clear a debt.                            */
		compute(wsaaTotalUnits, 5).set(sub(utrnaloIO.getNofUnits(), (mult(utrspf.getCurrentUnitBal(), -1))));
		if (isLT(wsaaTotalUnits, 0)) {
			setPrecision(utrnaloIO.getNofUnits(), 5);
			utrnaloIO.setNofUnits(utrnaloIO.getNofUnits().subtract(wsaaTotalUnits.getbigdata()));
		}
		if (isEQ(utrnaloIO.getUnitType(), "A")) {
			utrnaloIO.setNofDunits(utrnaloIO.getNofUnits());
		}
		else {
			if (isNE(utrnaloIO.getNofUnits(), ZERO)
			&& isEQ(utrnaloIO.getNofDunits(), ZERO)) {
				setPrecision(utrnaloIO.getNofDunits(), 6);
				utrnaloIO.setNofDunits(utrnaloIO.getNofUnits().divide(utrnaloIO.getDiscountFactor()));
			}
		}
	}

protected void checkSuffCovUnits5800(Utrnpf utrnaloIO)
	{
		if(checkSuffCovUnits5801(utrnaloIO)){
			moreUtrnUnitsThanUtrs5802(utrnaloIO);
			findCancellationOption5809(utrnaloIO);
		}
	}

protected boolean checkSuffCovUnits5801(Utrnpf utrnaloIO)
	{
		/*    If this is a switch and we are using deferred pricing it*/
		/*    is possible that tomorrow's price was more than today's price*/
		/*    which, if all the units were switched as amounts rather than*/
		/*    as units, could result in the amount being higher than actual*/
		/*    it should be. One up to the punter! Log the event and*/
		/*    continue on.*/
		if (isEQ(utrnaloIO.getSwitchIndicator(), "Y")
		&& isEQ(utrnaloIO.getNowDeferInd(), "D")
		&& utrspf.getCurrentUnitBal().multiply(wsaaPrice.getbigdata()).compareTo(BigDecimal.ZERO.subtract(utrnaloIO.getFundAmount()))<0) {
			conlogrec.params.set(utrspf.toString());
			conlogrec.error.set(f070);
			callConlog003();
		}
		/*    If we have sufficient units against the coverage to process*/
		/*    the number on the UTRN then - no further problem.*/
		if (utrnaloIO.getNofUnits().compareTo(utrspf.getCurrentUnitBal().multiply(new BigDecimal(-1)))>=0) {
			return false;
		}
		ct10Value++;
		return true;
	}

protected void moreUtrnUnitsThanUtrs5802(Utrnpf utrnaloIO)
	{
		/*    If we have got this far then we have to check which method*/
		/*    is to be used to deal with the possible debt against the*/
		/*    fund. This information is held in table T6647 which has*/
		/*    already been read for the bid/offer indicator earlier in*/
		/*    the module.*/
		/*    Call the subroutine DATCON3 to establish how many months*/
		/*    have elapsed since coverage commence date.*/
		/* MOVE 'CMDF'                 TO DTC3-FUNCTION.                */
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(utrnaloIO.getCrComDate());
		/*  MOVE UTRNALO-MONIES-DATE    TO DTC3-INT-DATE-2.              */
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
	}

protected void findCancellationOption5809(Utrnpf utrnaloIO)
	{
		/*    Read through the four options in the cancellation order set*/
		/*    out in T6647. Ignoring any items which are spaces or less*/
		/*    than the frequency factor returned from DATCON3, find the*/
		/*    one which is the closest to that number. Depending on the*/
		/*    option found carry out the following processing.*/
		/* MOVE ZERO                   TO WSAA-WITHIN-RANGE.            */
		wsaaWithinRange.set(1000);
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[t6647Ix.toInt()], datcon3rec.freqFactor)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[t6647Ix.toInt()], 0)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[t6647Ix.toInt()]);
			wsaaInsuffUnitsMeth.set("N");
		}
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix.toInt()], datcon3rec.freqFactor)
		&& isLT(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix.toInt()], wsaaWithinRange)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix.toInt()], 0)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix.toInt()]);
			wsaaInsuffUnitsMeth.set("D");
		}
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix.toInt()], datcon3rec.freqFactor)
		&& isLT(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix.toInt()], wsaaWithinRange)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix.toInt()], 0)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix.toInt()]);
			wsaaInsuffUnitsMeth.set("L");
		}
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix.toInt()], datcon3rec.freqFactor)
		&& isLT(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix.toInt()], wsaaWithinRange)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix.toInt()], ZERO)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix.toInt()]);
			wsaaInsuffUnitsMeth.set("E");
		}
		if (isEQ(wsaaInsuffUnitsMeth, " ")){
			return ;
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "N")){
			wsaaInsuffUnitsMeth.set(SPACES);
			return ;
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "D")){
			increaseCoverageDebt5810(utrnaloIO);
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "L")){
			lapseCoverage5820(utrnaloIO);
			errorOnCoverage5830(utrnaloIO);
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "E")){
			errorOnCoverage5830(utrnaloIO);
		}
	}

protected void increaseCoverageDebt5810(Utrnpf utrnaloIO)
	{
		//uderIO.setLapind("D");
		computeNewUtrnValues5812(utrnaloIO);
		postCovDebtAcmvDebit5813(utrnaloIO);
		postCovDebtAcmvCredit5814(utrnaloIO);
		updateCovrDebt5815(utrnaloIO);
		/* PERFORM 5816-CREATE-UTRN-MOVEMENT.                           */
		ct11Value++;
	}

protected void computeNewUtrnValues5812(Utrnpf utrnaloIO)
	{
		utrnaloIO.setNofDunits(BigDecimal.ZERO.subtract(utrspf.getCurrentDunitBal()));
		utrnaloIO.setNofUnits(BigDecimal.ZERO.subtract(utrspf.getCurrentUnitBal()));
		utrnaloIO.setFundAmount(mult(mult(utrnaloIO.getNofDunits(), wsaaPrice), utrnaloIO.getDiscountFactor()).getbigdata());
		/* MOVE UTRNALO-FUND-CURRENCY  TO ZRDP-CURRENCY.                */
		/* MOVE UTRNALO-FUND-AMOUNT    TO ZRDP-AMOUNT-IN.               */
		/* PERFORM X8000-CALL-ROUNDING.                                 */
		/* MOVE ZRDP-AMOUNT-OUT        TO UTRNALO-FUND-AMOUNT.          */
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO)!=0) {
			zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
			zrdecplrec.amountIn.set(utrnaloIO.getFundAmount());
			x8000CallRounding();
			utrnaloIO.setFundAmount(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE UTRNALO-CONTRACT-AMOUNT    TO WSAA-DEBT.                */
		compute(wsaaDebt, 2).set(mult(utrnaloIO.getContractAmount(), (-1)));
		if (utrnaloIO.getCntcurr().compareTo(utrnaloIO.getFundCurrency())==0) {
			utrnaloIO.setContractAmount(utrnaloIO.getFundAmount());
			utrnaloIO.setFundRate(BigDecimal.ONE);
		}
		else {
			conlinkrec.currIn.set(utrnaloIO.getFundCurrency());
			conlinkrec.currOut.set(utrnaloIO.getCntcurr());
			/*    MOVE UFPR-PRICE-DATE         TO CLNK-CASHDATE             */
			conlinkrec.cashdate.set(utrnaloIO.getMoniesDate());
			conlinkrec.amountIn.set(utrnaloIO.getFundAmount());
			conlinkrec.function.set("SURR");
			zrdecplrec.currency.set(utrnaloIO.getCntcurr());
			x3000CallXcvrt(utrnaloIO);
			utrnaloIO.setContractAmount(conlinkrec.amountOut.getbigdata());
			utrnaloIO.setFundRate(conlinkrec.rateUsed.getbigdata());
		}
		/*    Calculate the debt as the new contract amount (we have*/
		/*    just set it to the fund amount) less the original contract*/
		/*    amount.*/
		compute(wsaaDebt, 3).setRounded(add(wsaaDebt, utrnaloIO.getContractAmount()));
	}

protected void postCovDebtAcmvDebit5813(Utrnpf utrnaloIO)
	{
		lifacmvrec.origcurr.set(utrnaloIO.getCntcurr());
		if (utrnaloIO.getContractAmount().compareTo(BigDecimal.ZERO)<0) {
			compute(lifacmvrec.origamt, 2).set(sub(ZERO, wsaaDebt));
		}
		else {
			lifacmvrec.origamt.set(wsaaDebt);
		}
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(8);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(25);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct23Value = ct23Value.add(contotal);
		}
	}

protected void postCovDebtAcmvCredit5814(Utrnpf utrnaloIO)
	{
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(11);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(28);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct24Value = ct24Value.add(contotal);
		}
	}

protected void updateCovrDebt5815(Utrnpf utrnaloIO)
	{
		/*    Update the coverage debt balance on the component file*/
		/*    (COVRPF). Read and hold  the COVRPF file using the logical*/
		/*    view COVRUDL. Add the amount written to the ledger to the*/
		/*    coverage debt balance field and rewrite the record back to*/
		/*    the database.*/
		Covrpf covrudlIO = new Covrpf();
		covrudlIO.setChdrcoy(utrnaloIO.getChdrcoy());
		covrudlIO.setChdrnum(utrnaloIO.getChdrnum());
		covrudlIO.setLife(utrnaloIO.getLife());
		covrudlIO.setCoverage(utrnaloIO.getCoverage());
		covrudlIO.setRider(utrnaloIO.getRider());
		covrudlIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
		covrudlIO.setCoverageDebt(wsaaDebt.getbigdata());
		//covrudlIO.setCoverageDebt(add(covrudlIO.getCoverageDebt(), wsaaDebt));
		if(updateCovrpfDebtList==null){
			updateCovrpfDebtList = new ArrayList<>();
		}
		updateCovrpfDebtList.add(covrudlIO);
	}

protected void lapseCoverage5820(Utrnpf utrnaloIO)
	{
			if(lapseCoverage5821(utrnaloIO)){
				callSubroutine5822(utrnaloIO);
			}
	}

protected boolean lapseCoverage5821(Utrnpf utrnaloIO)
	{
		ct12Value++;
		/*    Using CRTABLE from UTRNPF as the key, look up the T5687*/
		/*    non-forfeiture method.*/
		itdmIO.setItemitem(utrnaloIO.getCrtable());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5687Rec);
		as1.setIndices(t5687Ix);
		as1.addSearchKey(wsaaT5687Key, itdmIO.getItemitem(), true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			itdmIO.setItemcoy(utrnaloIO.getChdrcoy());
			itdmIO.setItemtabl(t5687);
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			conlogrec.params.set(itdmIO.getParams());
			conlogrec.error.set(f294);
			callConlog003();
			return false;
		}
		if (isEQ(wsaaT5687NonForfeitMeth[t5687Ix.toInt()], SPACES)) {
			return false;
		}
		if (isEQ(wsaaT6597Statuz[t5687Ix.toInt()], varcom.endp)) {
			itdmIO.setItemitem(wsaaT5687NonForfeitMeth[t5687Ix.toInt()]);
			itdmIO.setItemcoy(utrnaloIO.getChdrcoy());
			itdmIO.setItemtabl(t6597);
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			conlogrec.params.set(itdmIO.getParams());
			conlogrec.error.set(h072);
			callConlog003();
			return false;
		}
		if (isEQ(wsaaT6597Premsubr04[t5687Ix.toInt()], SPACES)) {
			return false;
		}
		return true;
	}

protected void callSubroutine5822(Utrnpf utrnaloIO)
	{
		ovrduerec.language.set(bsscIO.getLanguage());
		ovrduerec.chdrcoy.set(utrnaloIO.getChdrcoy());
		ovrduerec.chdrnum.set(utrnaloIO.getChdrnum());
		ovrduerec.life.set(utrnaloIO.getLife());
		ovrduerec.coverage.set(utrnaloIO.getCoverage());
		ovrduerec.rider.set(utrnaloIO.getRider());
		ovrduerec.planSuffix.set(utrnaloIO.getPlanSuffix());
		ovrduerec.tranno.set(utrnaloIO.getTranno());
		ovrduerec.cntcurr.set(utrnaloIO.getCntcurr());
		ovrduerec.effdate.set(utrnaloIO.getMoniesDate());
		ovrduerec.outstamt.set(utrnaloIO.getFundAmount());
		ovrduerec.aloind.set("N");
		ovrduerec.cnttype.set(utrnaloIO.getCnttyp());
		ovrduerec.crtable.set(utrnaloIO.getCrtable());
		ovrduerec.company.set(batcdorrec.company);
		ovrduerec.batcbrn.set(batcdorrec.branch);
		ovrduerec.acctyear.set(batcdorrec.actyear);
		ovrduerec.acctmonth.set(batcdorrec.actmonth);
		ovrduerec.trancode.set(batcdorrec.trcde);
		int ptdate = x2000GetPayr(utrnaloIO);
		ovrduerec.ptdate.set(ptdate);
		ovrduerec.btdate.set(ZERO);
		ovrduerec.occdate.set(ZERO);
		ovrduerec.agntnum.set(ZERO);
		ovrduerec.cownnum.set(ZERO);
		ovrduerec.statcode.set(ZERO);
		ovrduerec.billfreq.set(ZERO);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.termid.set(SPACES);
		ovrduerec.tranDate.set(bsscIO.getEffectiveDate());
		ovrduerec.tranTime.set(varcom.vrcmTime);
		ovrduerec.tranDate.set(bsscIO.getEffectiveDate());
		ovrduerec.runDate.set(bsscIO.getEffectiveDate());
		ovrduerec.user.set(ZERO);
		ovrduerec.sumins.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.instprem.set(ZERO);
		ovrduerec.premCessDate.set(ZERO);
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.pumeth.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.pstatcode.set(SPACES);
		ovrduerec.occdate.set(utrnaloIO.getCrComDate());
		callProgram(wsaaT6597Premsubr04[t5687Ix.toInt()], ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK)
		&& isNE(ovrduerec.statuz, "OMIT")) {
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		if (isEQ(ovrduerec.statuz, "OMIT")) {
			return ;
		}
		if (isEQ(wsaaT6597Cpstat04[t5687Ix.toInt()], SPACES)
		&& isEQ(wsaaT6597Crstat04[t5687Ix.toInt()], SPACES)) {
			return ;
		}
		
		Covrpf covrudlIO = new Covrpf();
		covrudlIO.setChdrcoy(utrnaloIO.getChdrcoy());
		covrudlIO.setChdrnum(utrnaloIO.getChdrnum());
		covrudlIO.setLife(utrnaloIO.getLife());
		covrudlIO.setCoverage(utrnaloIO.getCoverage());
		covrudlIO.setRider(utrnaloIO.getRider());
		covrudlIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
		
		covrudlIO.setPstatcode(wsaaT6597Cpstat04[t5687Ix.toInt()].toString());
		covrudlIO.setStatcode(wsaaT6597Crstat04[t5687Ix.toInt()].toString());
		//covrudlIO.setCoverageDebt(add(covrudlIO.getCoverageDebt(), wsaaDebt));
		if(updateCovrpfList==null){
			updateCovrpfList = new ArrayList<>();
		}
		updateCovrpfList.add(covrudlIO);
	}

protected void errorOnCoverage5830(Utrnpf utrnaloIO)
	{
		Uderpf uderIO = new Uderpf();
		uderIO.setLapind("L");
		uderIO.setRecordtype("UTRN");
		uderIO.setRelativeRecordNo(utrnaloIO.getUniqueNumber());
		uderIO.setChdrcoy(utrnaloIO.getChdrcoy());
		uderIO.setChdrnum(utrnaloIO.getChdrnum());
		uderIO.setLife(utrnaloIO.getLife());
		uderIO.setCoverage(utrnaloIO.getCoverage());
		uderIO.setRider(utrnaloIO.getRider());
		uderIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
		uderIO.setUnitVirtualFund(utrnaloIO.getUnitVirtualFund());
		uderIO.setUnitType(utrnaloIO.getUnitType());
		uderIO.setTranno(utrnaloIO.getTranno());
		uderIO.setBatctrcde(utrnaloIO.getBatctrcde());
		uderIO.setContractAmount(utrnaloIO.getContractAmount());
		uderIO.setCntcurr(utrnaloIO.getCntcurr());
		uderIO.setFundAmount(utrnaloIO.getFundAmount());
		uderIO.setFundCurrency(utrnaloIO.getFundCurrency());
		uderIO.setFundRate(utrnaloIO.getFundRate());
		uderIO.setAge(datcon3rec.freqFactor.toInt());
		uderIO.setTriggerModule(utrnaloIO.getTriggerModule());
		if(insertUderpfList == null){
			insertUderpfList = new ArrayList<>();
		}
		insertUderpfList.add(uderIO);
		ct13Value++;
	}

protected void accountingPostings5900(Utrnpf utrnaloIO)
	{
		wsaaPostFundAcct.set(0);
		wsaaPostBidofferAcct.set(0);
		wsaaPostBarebidAcct.set(0);
		wsaaPostChargeAcct.set(0);
		wsaaUtrnaloFundAmount.set(0);
		wsaaNominalRate.set(0);
		wsaaX.set(0);
		postedAmountCalcs5920(utrnaloIO);
		if (isNE(utrnaloIO.getCntcurr(), utrnaloIO.getFundCurrency())) {
			acctForCurrConversion5930(utrnaloIO);
		}
		fundPostings5940(utrnaloIO);
		if (isNE(wsaaPostFund, ZERO)) {
			fundInvestment5950(utrnaloIO);
		}
		if (isNE(wsaaPostBidoffer, ZERO)) {
			bidOfferSpread5960(utrnaloIO);
		}
		if (isNE(wsaaPostBarebid, ZERO)) {
			bidBareSpread5970(utrnaloIO);
		}
		if (isNE(wsaaPostCharge, ZERO)) {
			chargesCosts5980(utrnaloIO);
		}
	}


protected void postedAmountCalcs5920(Utrnpf utrnaloIO)
	{
		/* This paragraph has two primary functions. Firstly to calculate*/
		/* the amounts to be posted and secondly to ensure the rounded*/
		/* WSAA-POST-xxxx fields balance to zero.*/
		compute(wsaaFund, 6).setRounded(mult(utrnaloIO.getNofUnits(), ufpricerec.barePrice));
		/* MOVE UTRNALO-FUND-CURRENCY  TO ZRDP-CURRENCY.                */
		/* MOVE WSAA-FUND              TO ZRDP-AMOUNT-IN.               */
		/* PERFORM X8000-CALL-ROUNDING.                                 */
		/* MOVE ZRDP-AMOUNT-OUT        TO WSAA-FUND.                    */
		if (isNE(wsaaFund, 0)) {
			zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
			zrdecplrec.amountIn.set(wsaaFund);
			x8000CallRounding();
			wsaaFund.set(zrdecplrec.amountOut);
		}
		wsaaPostFund.setRounded(wsaaFund);
		compute(wsaaBidoffer, 6).setRounded(mult(utrnaloIO.getNofUnits(), (sub(wsaaPrice, ufpricerec.bidPrice))));
		wsaaPostBidoffer.setRounded(wsaaBidoffer);
		/* MOVE UTRNALO-FUND-CURRENCY  TO ZRDP-CURRENCY.                */
		/* MOVE WSAA-BIDOFFER          TO ZRDP-AMOUNT-IN.               */
		/* PERFORM X8000-CALL-ROUNDING.                                 */
		/* MOVE ZRDP-AMOUNT-OUT        TO WSAA-BIDOFFER.                */
		if (isNE(wsaaBidoffer, 0)) {
			zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
			zrdecplrec.amountIn.set(wsaaBidoffer);
			x8000CallRounding();
			wsaaBidoffer.set(zrdecplrec.amountOut);
		}
		if (isEQ(utrnaloIO.getUnitType(), "A")) {
			compute(wsaaBarebid, 6).setRounded(sub(sub(utrnaloIO.getFundAmount(), wsaaBidoffer), wsaaFund));
		}
		else {
			compute(wsaaBarebid, 6).setRounded(mult(utrnaloIO.getNofUnits(), (sub(ufpricerec.bidPrice, ufpricerec.barePrice))));
		}
		wsaaPostBarebid.setRounded(wsaaBarebid);
		compute(wsaaCharge, 6).setRounded(sub(sub(sub(utrnaloIO.getFundAmount(), wsaaFund), wsaaBidoffer), wsaaBarebid));
		wsaaPostCharge.setRounded(wsaaCharge);
		compute(wsaaBalanceCheck, 2).set(sub(add(add(add(wsaaPostFund, wsaaPostBidoffer), wsaaPostBarebid), wsaaPostCharge), utrnaloIO.getFundAmount()));
		/*    This perform will calculate the acct. amount to be posted    */
		/*    just to make sure before it is written in ACMV file that     */
		/*    it balances out, otherwise it will make it balance.          */
		x6000PostedAcctAmountCalcs(utrnaloIO);
		/*    If the balance check is non-zero add the balance check*/
		/*    to the first non-zero posting we find. (Note we check*/
		/*    the postings in reverse order to maintain compatability*/
		/*    with the previous version).*/
		if (isEQ(wsaaBalanceCheck, ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isNE(wsaaPostCharge, ZERO)) {
				wsaaPostCharge.subtract(wsaaBalanceCheck);
			}
			else {
				if (isNE(wsaaPostBarebid, ZERO)) {
					wsaaPostBarebid.subtract(wsaaBalanceCheck);
				}
				else {
					if (isNE(wsaaPostBidoffer, ZERO)) {
						wsaaPostBidoffer.subtract(wsaaBalanceCheck);
					}
					else {
						if (isNE(wsaaPostFund, ZERO)) {
							wsaaPostFund.subtract(wsaaBalanceCheck);
						}
					}
				}
			}
		}
	}

protected void acctForCurrConversion5930(Utrnpf utrnaloIO)
	{
		/*    Post the fund amount in its currency. (LIFACMV will look*/
		/*    T3629 to get the rate to convert the ORIGAMT into the*/
		/*    ACCTAMT.)*/
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		lifacmvrec.origamt.set(utrnaloIO.getFundAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(10);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(27);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct25Value = ct25Value.add(contotal);
		}
		/*    Post the Contract amount in its currency. (ditto).*/
		lifacmvrec.origcurr.set(utrnaloIO.getCntcurr());
		lifacmvrec.origamt.set(utrnaloIO.getContractAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(9);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(26);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct26Value = ct26Value.add(contotal);
		}
	}

protected void fundPostings5940(Utrnpf utrnaloIO)
	{
		lifacmvrec.origcurr.set(utrnaloIO.getCntcurr());
		lifacmvrec.origamt.set(utrnaloIO.getContractAmount());
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC) && isEQ(bprdIO.getSystemParam04(),MF)){
				t5645Ix.set(12);
			}
			else{
				t5645Ix.set(29);
				wsaaT5645Sacscode[29].set(utrnaloIO.getSacscode());
				wsaaT5645Sacstype[29].set(utrnaloIO.getSacstyp());
				wsaaT5645Glmap[29].set(utrnaloIO.getGenlcde());
			}
		}
		else {
			lifacmvrec.substituteCode[6].set(SPACES);
			if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC) && isEQ(bprdIO.getSystemParam04(),MF)){
				t5645Ix.set(13);
			}
			else{
				t5645Ix.set(16);
				wsaaT5645Sacscode[16].set(utrnaloIO.getSacscode());
				wsaaT5645Sacstype[16].set(utrnaloIO.getSacstyp());
				wsaaT5645Glmap[16].set(utrnaloIO.getGenlcde());
			}
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct27Value = ct27Value.add(contotal);
		}
	}

protected void fundInvestment5950(Utrnpf utrnaloIO)
	{
		lifacmvrec.origamt.set(wsaaPostFund);
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(30);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(17);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostFundAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostFundAcct);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct28Value = ct28Value.add(contotal);
		}
	}

protected void bidOfferSpread5960(Utrnpf utrnaloIO)
	{
		lifacmvrec.origamt.set(wsaaPostBidoffer);
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(18);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostBidofferAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostBidofferAcct);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct29Value = ct29Value.add(contotal);
		}
	}

protected void bidBareSpread5970(Utrnpf utrnaloIO)
	{
		lifacmvrec.origamt.set(wsaaPostBarebid);
		if ((setPrecision(ZERO, 5)
		&& isLT((sub(ufpricerec.bidPrice, ufpricerec.barePrice)), ZERO))) {
			compute(lifacmvrec.origamt, 2).set(sub(ZERO, lifacmvrec.origamt));
		}
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(2);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(19);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostBarebidAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostBarebidAcct);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct30Value = ct30Value.add(contotal);
		}
	}

protected void chargesCosts5980(Utrnpf utrnaloIO)
	{
		lifacmvrec.origamt.set(wsaaPostCharge);
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (isGT(wsaaPostCharge, ZERO)) {
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(3);
				lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			}
			else {
				t5645Ix.set(20);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
		}
		else {
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(4);
				lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			}
			else {
				t5645Ix.set(21);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
		}

		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostChargeAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostChargeAcct);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct30Value = ct30Value.add(contotal);
			if (isGT(wsaaPostCharge, ZERO)) {
				ct31Value = ct31Value.add(contotal);
			}
			else {
				ct32Value = ct32Value.add(contotal);
			}
		}
	}

protected void fluctuationCheck6000(Utrnpf utrnaloIO)
	{
		/*    Get Deferred or Now price applicable "today". If there is*/
		/*    no available deferred price use the now price.*/
		ufpricerec.function.set("FLUCT");
		ufpricerec.mode.set("BATCH");
		ufpricerec.effdate.set(datcon1rec.intDate);
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)) {
			syserrrec.params.set(ufpricerec.ufpriceRec);
			syserrrec.statuz.set(ufpricerec.statuz);
			fatalError600();
		}
		if (isEQ(ufpricerec.barePrice, wsaaUfprBarePrice)) {
			return ;
		}
		compute(lifacmvrec.origamt, 6).setRounded(mult(utrnaloIO.getNofUnits(), (sub(ufpricerec.barePrice, wsaaUfprBarePrice))));
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		/*  Round fluctuation amount                                       */
		if (isNE(lifacmvrec.origamt, ZERO)) {
			zrdecplrec.currency.set(lifacmvrec.origcurr);
			zrdecplrec.amountIn.set(lifacmvrec.origamt);
			x8000CallRounding();
			lifacmvrec.origamt.set(zrdecplrec.amountOut);
		}
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(6);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(23);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		BigDecimal contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct33Value = ct33Value.add(contotal);
		}
		/*    Update Negative Fluctuation ledger posting.*/
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(7);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(24);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		contotal = x1000CallLifacmv(utrnaloIO);
		if(contotal!=null){
			ct34Value = ct34Value.add(contotal);
		}
		/*    Control total for number of Fluctuations.*/
		ct14Value++;

	}


protected void rewrtUtrn6100(Utrnpf u)
	{
		u.setScheduleName(bsscIO.getScheduleName().toString());
		u.setScheduleNumber(bsscIO.getScheduleNumber().toInt());
		// FeedbackInd
		if(updateUtrnpfList == null){
			updateUtrnpfList = new ArrayList<>();
		}
		updateUtrnpfList.add(u);
		utrnpfDAO.updateUtrnpfRecord(updateUtrnpfList);
		updateUtrnpfList = null;
		/*  If ever the WSAA-DELETE-UDEL flag gets set to 'N'*/
		/*  in 3000-UPDATE as a result of not entering this*/
		/*  section for the current UTRX, the flag must NEVER*/
		/*  be updated again for the rest of the contract even*/
		/*  if further UTRXs are processed successfully here.*/
		updateutrn100();
		if (prevUnprocUtrnExists.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaDeleteUdel.set("Y");
		}
		ct16Value++;
	}

protected void createUrep6150(Utrnpf utrnaloIO)
	{
		/*  Read the UREP file to  get the last used Sequence Number       */
		/*  for the given company, contract.                               */
		int wsaaSeqno = x4000GetSeqnum(utrnaloIO);
		Ureppf urepIO = new Ureppf();
		urepIO.setChdrcoy(utrnaloIO.getChdrcoy());
		urepIO.setChdrnum(utrnaloIO.getChdrnum());
		urepIO.setSeqnum(wsaaSeqno);
		urepIO.setLife(utrnaloIO.getLife());
		urepIO.setCoverage(utrnaloIO.getCoverage());
		urepIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
		urepIO.setUnitVirtualFund(utrnaloIO.getUnitVirtualFund());
		urepIO.setUnitType(utrnaloIO.getUnitType());
		urepIO.setTranno(utrnaloIO.getTranno());
		urepIO.setBatctrcde(utrnaloIO.getBatctrcde());
		urepIO.setNofUnits(utrnaloIO.getNofUnits());
		urepIO.setNofDunits(utrnaloIO.getNofDunits());
		urepIO.setMoniesDate(utrnaloIO.getMoniesDate().intValue());
		urepIO.setPriceDateUsed(utrnaloIO.getPriceDateUsed().intValue());
		urepIO.setPriceUsed(utrnaloIO.getPriceUsed());
		urepIO.setFundCurrency(utrnaloIO.getFundCurrency());
		urepIO.setFundAmount(utrnaloIO.getFundAmount());
		urepIO.setScheduleNumber(bsscIO.getScheduleNumber().toInt());
		urepIO.setScheduleName(bsscIO.getScheduleName().toString());
		urepIO.setProcflg("");
		if(insertUreppfList == null){
			insertUreppfList = new ArrayList<>();
		}
		insertUreppfList.add(urepIO);
		ct37Value++;
	}
protected void callTrigger6300(Utrnpf utrnaloIO)
	{
		udtrigrec.function.set(wsaaProg);
		udtrigrec.statuz.set(varcom.oK);
		udtrigrec.mode.set("BATCH");
		udtrigrec.callingProg.set(wsaaProg);
		udtrigrec.fsuco.set(bsprIO.getFsuco());
		udtrigrec.language.set(bsscIO.getLanguage());
		udtrigrec.batchkey.set(batcdorrec.batchkey);
		udtrigrec.effdate.set(bsscIO.getEffectiveDate());
		udtrigrec.chdrcoy.set(utrnaloIO.getChdrcoy());
		udtrigrec.chdrnum.set(utrnaloIO.getChdrnum());
		udtrigrec.life.set(utrnaloIO.getLife());
		udtrigrec.coverage.set(utrnaloIO.getCoverage());
		udtrigrec.rider.set(utrnaloIO.getRider());
		udtrigrec.planSuffix.set(utrnaloIO.getPlanSuffix());
		udtrigrec.unitVirtualFund.set(utrnaloIO.getUnitVirtualFund());
		udtrigrec.unitType.set(utrnaloIO.getUnitType());
		udtrigrec.tranno.set(utrnaloIO.getTranno());
		udtrigrec.procSeqNo.set(utrnaloIO.getProcSeqNo());
		udtrigrec.triggerKey.set(utrnaloIO.getTriggerKey());
		udtrigrec.user.set(wsaaUser);
		callProgram(utrnaloIO.getTriggerModule(), udtrigrec.udtrigrecRec);
		if (isNE(udtrigrec.statuz, varcom.oK)) {
			syserrrec.params.set(udtrigrec.udtrigrecRec);
			syserrrec.statuz.set(udtrigrec.statuz);
			fatalError600();
		}
		if (isNE(utrnaloIO.getTriggerKey(), udtrigrec.triggerKey)) {
			getAppVars().addDiagnostic("UTRN RECORD FOR TRIGGER CALL "+utrnaloIO.toString());
			getAppVars().addDiagnostic("UTRN TRIGGER "+utrnaloIO.getTriggerModule() +" "+udtrigrec.triggerKey);
			syserrrec.params.set(udtrigrec.udtrigrecRec);
			syserrrec.statuz.set(udtrigrec.statuz);
			fatalError600();
		}
		/*    Control total for number of Trigger Processing Utrns*/
		/*  MOVE CT23                   TO CONT-TOTNO.                   */
		ct35Value++;
	}


protected BigDecimal x1000CallLifacmv(Utrnpf utrnaloIO)
	{
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return null;
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(utrnaloIO.getPlanSuffix());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(utrnaloIO.getChdrnum());
			stringVariable1.addExpression(utrnaloIO.getLife());
			stringVariable1.addExpression(utrnaloIO.getCoverage());
			stringVariable1.addExpression(utrnaloIO.getRider());
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
		}
		else {
			lifacmvrec.rldgacct.set(utrnaloIO.getChdrnum());
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		lifacmvrec.rdocnum.set(utrnaloIO.getChdrnum());
		if(fmcOnFlag && isNE(bprdIO.getSystemParam05(),FMC) && 
				(isEQ(utrnaloIO.getBatctrcde(), "T642") || isEQ(utrnaloIO.getBatctrcde(), "B522"))){
			lifacmvrec.tranno.set(ZERO);
		}
		else{
			lifacmvrec.tranno.set(utrnaloIO.getTranno());
		}
		lifacmvrec.tranref.set(utrnaloIO.getTranno());
		lifacmvrec.effdate.set(utrnaloIO.getMoniesDate());
		lifacmvrec.substituteCode[1].set(utrnaloIO.getCnttyp());
		lifacmvrec.substituteCode[2].set(utrnaloIO.getUnitVirtualFund());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaJrnseq.add(1);
		/*    Each invocation of this section will have set the control*/
		/*    total which will be used to log the accounting amount. Simply*/
		/*    set the appropriate sign and call contot.*/
		if (isEQ(lifacmvrec.glsign, "-")) {
			compute(contotrec.totval, 2).set(sub(ZERO, lifacmvrec.origamt));
		}
		else {
			contotrec.totval.set(lifacmvrec.origamt);
		}
		/*    Log number of ACMVs written.*/
		ct20Value++;
		return contotrec.totval.getbigdata();

	}


protected int x2000GetPayr(Utrnpf utrnaloIO)
 {
		Payrpf payrlifIO = null;
		if (payerMap != null && payerMap.containsKey(utrnaloIO.getChdrnum())) {
			for (Payrpf p : payerMap.get(utrnaloIO.getChdrnum())) {
				if (utrnaloIO.getChdrcoy().equals(p.getChdrcoy())
						&& "1".equals(p.getPayrseqno())
						&& "1".equals(p.getValidflag())) {
					payrlifIO = p;
					break;
				}
			}
		}
		if (payrlifIO == null) {
			syserrrec.params.set(utrnaloIO.getChdrnum());
			fatalError600();
		} else {//IJTI-320 START
			return payrlifIO.getPtdate();
		}
		return 0;
		//IJTI-320 END
	}

protected void x3000CallXcvrt(Utrnpf utrnaloIO)
	{
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(bsprIO.getCompany());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			getAppVars().addDiagnostic("CURRENCY CONVERSION FAILED "+ utrnaloIO.toString());
			fatalError600();
		}
		/* MOVE UTRNALO-FUND-CURRENCY  TO ZRDP-CURRENCY.                */
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* PERFORM X8000-CALL-ROUNDING.                                 */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			x8000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}

	}

protected int x4000GetSeqnum(Utrnpf utrnaloIO)
	{
		Ureppf urepIO = null;
		if(ureppfMap != null && ureppfMap.containsKey(utrnaloIO.getChdrnum())){
			for(Ureppf u:ureppfMap.get(utrnaloIO.getChdrnum())){
				if(u.getChdrcoy().equals(utrnaloIO.getChdrcoy())){
					urepIO = u;
					break;
				}
			}
		}
		if(urepIO == null){
			return 1;
		}else{
			return urepIO.getSeqnum()+1;
		}
	}


protected void x6000PostedAcctAmountCalcs(Utrnpf utrnaloIO)
	{
		x6010GetRate(utrnaloIO);
		x6020CheckBalance();
	}

protected void x6010GetRate(Utrnpf utrnaloIO)
	{
		wsaaT3629StoreCurr.set(utrnaloIO.getFundCurrency());
		x6100GetRate(utrnaloIO);
		if (isEQ(wsaaNominalRate, ZERO)) {
			if (isNE(t3629rec.contitem, SPACES)) {
				/*          MOVE T3629-CONTITEM   TO ITEM-ITEMITEM         <V65L18>*/
				wsaaT3629StoreCurr.set(t3629rec.contitem);
				x6100GetRate(utrnaloIO);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
		if (utrnaloIO.getFundAmount().compareTo(BigDecimal.ZERO)!=0) {
			compute(wsaaUtrnaloFundAmount, 10).setRounded(mult(wsaaNominalRate, utrnaloIO.getFundAmount()));
			if (isNE(wsaaUtrnaloFundAmount, 0)) {
				zrdecplrec.currency.set(t3629rec.ledgcurr);
				zrdecplrec.amountIn.set(wsaaUtrnaloFundAmount);
				x8000CallRounding();
				wsaaUtrnaloFundAmount.set(zrdecplrec.amountOut);
			}
		}
		if (isNE(wsaaPostFund, ZERO)) {
			compute(wsaaPostFundAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostFund));
			if (isNE(wsaaPostFundAcct, 0)) {
				zrdecplrec.currency.set(t3629rec.ledgcurr);
				zrdecplrec.amountIn.set(wsaaPostFundAcct);
				x8000CallRounding();
				wsaaPostFundAcct.set(zrdecplrec.amountOut);
			}
		}
		if (isNE(wsaaPostBidoffer, ZERO)) {
			compute(wsaaPostBidofferAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostBidoffer));
			if (isNE(wsaaPostBidofferAcct, 0)) {
				zrdecplrec.currency.set(t3629rec.ledgcurr);
				zrdecplrec.amountIn.set(wsaaPostBidofferAcct);
				x8000CallRounding();
				wsaaPostBidofferAcct.set(zrdecplrec.amountOut);
			}
		}
		if (isNE(wsaaPostBarebid, ZERO)) {
			compute(wsaaPostBarebidAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostBarebid));
			if (isNE(wsaaPostBarebidAcct, 0)) {
				zrdecplrec.currency.set(t3629rec.ledgcurr);
				zrdecplrec.amountIn.set(wsaaPostBarebidAcct);
				x8000CallRounding();
				wsaaPostBarebidAcct.set(zrdecplrec.amountOut);
			}
		}
		if (isNE(wsaaPostCharge, ZERO)) {
			compute(wsaaPostChargeAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostCharge));
			if (isNE(wsaaPostChargeAcct, 0)) {
				zrdecplrec.currency.set(t3629rec.ledgcurr);
				zrdecplrec.amountIn.set(wsaaPostChargeAcct);
				x8000CallRounding();
				wsaaPostChargeAcct.set(zrdecplrec.amountOut);
			}
		}
	}

protected void x6020CheckBalance()
	{
		compute(wsaaBalanceCheckAcct, 2).set(sub(add(add(add(wsaaPostFundAcct, wsaaPostBidofferAcct), wsaaPostBarebidAcct), wsaaPostChargeAcct), wsaaUtrnaloFundAmount));
		if (isEQ(wsaaBalanceCheckAcct, ZERO)) {
			wsaaPostFundAcct.set(0);
			wsaaPostBidofferAcct.set(0);
			wsaaPostBarebidAcct.set(0);
			wsaaPostChargeAcct.set(0);
			wsaaUtrnaloFundAmount.set(0);
			wsaaNominalRate.set(0);
			wsaaX.set(0);
		}
		else {
			if (isNE(wsaaPostChargeAcct, ZERO)) {
				wsaaPostChargeAcct.subtract(wsaaBalanceCheckAcct);
				wsaaPostFundAcct.set(0);
				wsaaPostBidofferAcct.set(0);
				wsaaPostBarebidAcct.set(0);
			}
			else {
				if (isNE(wsaaPostBarebidAcct, ZERO)) {
					wsaaPostBarebidAcct.subtract(wsaaBalanceCheckAcct);
					wsaaPostFundAcct.set(0);
					wsaaPostBidofferAcct.set(0);
					wsaaPostChargeAcct.set(0);
				}
				else {
					if (isNE(wsaaPostBidofferAcct, ZERO)) {
						wsaaPostBidofferAcct.subtract(wsaaBalanceCheckAcct);
						wsaaPostFundAcct.set(0);
						wsaaPostBarebidAcct.set(0);
						wsaaPostChargeAcct.set(0);
					}
					else {
						if (isNE(wsaaPostFundAcct, ZERO)) {
							wsaaPostFundAcct.subtract(wsaaBalanceCheckAcct);
							wsaaPostChargeAcct.set(0);
							wsaaPostBidofferAcct.set(0);
							wsaaPostBarebidAcct.set(0);
						}
					}
				}
			}
		}
	}

protected void x6100GetRate(Utrnpf utrnaloIO)
	{
		/* Search the T3629 array to get the rate                          */
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3629Rec);
		wsaaT3629Ix = 1;
		as1.setIndices(new IntegerData(wsaaT3629Ix));
		as1.addSearchKey(wsaaT3629Key, wsaaT3629StoreCurr, true);
		if (as1.binarySearch()) {
			/*       MOVE WSAA-T3629-T3629-REC TO T3629-T3629-REC      <LA4271>*/
			t3629rec.t3629Rec.set(wsaaT3629T3629Rec[wsaaT3629Ix]);
		}
		else {
			syserrrec.statuz.set(varcom.mrnf);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t3629);
			stringVariable1.addExpression(wsaaT3629StoreCurr);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.         <V65L18>*/
		/* MOVE BATD-COMPANY           TO ITEM-ITEMCOY.         <V65L18>*/
		/* MOVE T3629                  TO ITEM-ITEMTABL.        <V65L18>*/
		/* MOVE UTRNALO-FUND-CURRENCY  TO ITEM-ITEMITEM.        <V65L18>*/
		/*                                                      <V65L18>*/
		/* MOVE READR                  TO ITEM-FUNCTION.        <V65L18>*/
		/*                                                      <V65L18>*/
		/* CALL 'ITEMIO'               USING ITEM-PARAMS.       <V65L18>*/
		/*                                                      <V65L18>*/
		/* IF ITEM-STATUZ              NOT = O-K                <V65L18>*/
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <V65L18>*/
		/*     PERFORM 600-FATAL-ERROR                          <V65L18>*/
		/* END-IF                                               <V65L18>*/
		/*                                                      <V65L18>*/
		/* MOVE ITEM-GENAREA           TO T3629-T3629-REC.      <V65L18>*/
		/*                                                      <V65L18>*/
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaNominalRate, ZERO)
		|| isGT(wsaaX, 7))) {
			if (isGTE(utrnaloIO.getMoniesDate(), t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(utrnaloIO.getMoniesDate(), t3629rec.todate[wsaaX.toInt()])) {
				wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}
	}


protected void x7000InsuffValueProcess(Utrnpf utrnaloIO)
	{
		/* Write a UDIV (Unit Deal Insufficient Value) record.             */
		if (isNE(bprdIO.getSystemParam04(), "UD")) {
			return ;
		}
		Udivpf udivIO = new Udivpf();
		udivIO.setChdrcoy(utrnaloIO.getChdrcoy());
		udivIO.setChdrnum(utrnaloIO.getChdrnum());
		udivIO.setTranno(utrnaloIO.getTranno());
		if(insertUdivpfList==null){
			insertUdivpfList = new ArrayList<>();
		}
		insertUdivpfList.add(udivIO);
	}


protected void x8000CallRounding()
	{
		/*X8100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*X8900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-T6647-ARRAY--INNER
 */
private static final class WsaaT6647ArrayInner { 

		/* WSAA-T6647-ARRAY */
	//private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (500, 20); //MIBT-289, commented
		//private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (1000, 20);	//MIBT-289, size increased
		private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (2050, 20);	//ILIFE-6190
	private FixedLengthStringData[] wsaaT6647Key = FLSDArrayPartOfArrayStructure(7, wsaaT6647Rec, 0);
	private FixedLengthStringData[] wsaaT6647Batctrcde = FLSDArrayPartOfArrayStructure(4, wsaaT6647Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6647Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6647Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT6647Data = FLSDArrayPartOfArrayStructure(13, wsaaT6647Rec, 7);
	private FixedLengthStringData[] wsaaT6647Swmeth = FLSDArrayPartOfArrayStructure(4, wsaaT6647Data, 0);
	private FixedLengthStringData[] wsaaT6647Bidoffer = FLSDArrayPartOfArrayStructure(1, wsaaT6647Data, 4);
	private PackedDecimalData[] wsaaT6647MonthsNegUnits = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 5);
	private PackedDecimalData[] wsaaT6647MonthsDebt = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 7);
	private PackedDecimalData[] wsaaT6647MonthsLapse = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 9);
	private PackedDecimalData[] wsaaT6647MonthsError = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 11);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData bsprrec = new FixedLengthStringData(10).init("BSPRREC");
	private FixedLengthStringData utrnupdrec = new FixedLengthStringData(10).init("UTRNUPDREC");
	private FixedLengthStringData utrsrec = new FixedLengthStringData(10).init("UTRSREC");
	private FixedLengthStringData uderrec = new FixedLengthStringData(10).init("UDERREC");
	private FixedLengthStringData udelrec = new FixedLengthStringData(10).init("UDELREC");
	private FixedLengthStringData bwrkrec = new FixedLengthStringData(10).init("BWRKREC");
	private FixedLengthStringData ureprec = new FixedLengthStringData(10).init("UREPREC");
	private FixedLengthStringData ufnsrec = new FixedLengthStringData(10).init("UFNSREC");
	private FixedLengthStringData udivrec = new FixedLengthStringData(10).init("UDIVREC");
}
}