package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Ufndpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UfndpfDAO extends BaseDAO<Ufndpf> {
	public Map<String, List<Ufndpf>> searchUfndRecord(List<String> virtualFundList);
	public int countUfndRecord();
	public List<Ufndpf> searchAllUfndRecord();
}