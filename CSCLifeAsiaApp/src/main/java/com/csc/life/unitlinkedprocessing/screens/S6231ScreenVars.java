package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6231
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6231ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(65);
	public FixedLengthStringData dataFields = new FixedLengthStringData(17).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,1);
	public ZonedDecimalData jobno = DD.jobno.copyToZonedDecimal().isAPartOf(dataFields,9);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 17);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData jobnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 29);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] jobnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(223);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(77).isAPartOf(subfileArea, 0);
	public ZonedDecimalData accBidPrice = DD.abidpr.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData accBarePrice = DD.abrepr.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public ZonedDecimalData accOfferPrice = DD.aoffpr.copyToZonedDecimal().isAPartOf(subfileFields,20);
	public ZonedDecimalData initBidPrice = DD.ibidpr.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData initBarePrice = DD.ibrepr.copyToZonedDecimal().isAPartOf(subfileFields,40);
	public ZonedDecimalData initOfferPrice = DD.ioffpr.copyToZonedDecimal().isAPartOf(subfileFields,50);
	public FixedLengthStringData update = DD.update.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,68);
	public ZonedDecimalData xtranno = DD.xtranno.copyToZonedDecimal().isAPartOf(subfileFields,72);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 77);
	public FixedLengthStringData abidprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData abreprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData aoffprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData ibidprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData ibreprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData ioffprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData updateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData xtrannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 113);
	public FixedLengthStringData[] abidprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] abreprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] aoffprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] ibidprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] ibreprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] ioffprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] updateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] xtrannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 221);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S6231screensflWritten = new LongData(0);
	public LongData S6231screenctlWritten = new LongData(0);
	public LongData S6231screenWritten = new LongData(0);
	public LongData S6231protectWritten = new LongData(0);
	public GeneralTable s6231screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6231screensfl;
	}

	public S6231ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vrtfndOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ibreprOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ibidprOut,new String[] {"03","11","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ioffprOut,new String[] {"04","12","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(abreprOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(abidprOut,new String[] {"06","13","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aoffprOut,new String[] {"07","14","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(updateOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jobnoOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {xtranno, unitVirtualFund, initBarePrice, initBidPrice, initOfferPrice, accBarePrice, accBidPrice, accOfferPrice, update};
		screenSflOutFields = new BaseData[][] {xtrannoOut, vrtfndOut, ibreprOut, ibidprOut, ioffprOut, abreprOut, abidprOut, aoffprOut, updateOut};
		screenSflErrFields = new BaseData[] {xtrannoErr, vrtfndErr, ibreprErr, ibidprErr, ioffprErr, abreprErr, abidprErr, aoffprErr, updateErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {company, effdate, jobno};
		screenOutFields = new BaseData[][] {companyOut, effdateOut, jobnoOut};
		screenErrFields = new BaseData[] {companyErr, effdateErr, jobnoErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6231screen.class;
		screenSflRecord = S6231screensfl.class;
		screenCtlRecord = S6231screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6231protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6231screenctl.lrec.pageSubfile);
	}
}
