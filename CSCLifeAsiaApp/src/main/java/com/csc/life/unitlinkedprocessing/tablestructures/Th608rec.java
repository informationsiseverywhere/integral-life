package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:36
 * Description:
 * Copybook name: TH608REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th608rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th608Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData daywhs = new FixedLengthStringData(372).isAPartOf(th608Rec, 0);
  	public FixedLengthStringData[] daywh = FLSArrayPartOfStructure(12, 31, daywhs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(372).isAPartOf(daywhs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData daywh01 = new FixedLengthStringData(31).isAPartOf(filler, 0);
  	public FixedLengthStringData daywh02 = new FixedLengthStringData(31).isAPartOf(filler, 31);
  	public FixedLengthStringData daywh03 = new FixedLengthStringData(31).isAPartOf(filler, 62);
  	public FixedLengthStringData daywh04 = new FixedLengthStringData(31).isAPartOf(filler, 93);
  	public FixedLengthStringData daywh05 = new FixedLengthStringData(31).isAPartOf(filler, 124);
  	public FixedLengthStringData daywh06 = new FixedLengthStringData(31).isAPartOf(filler, 155);
  	public FixedLengthStringData daywh07 = new FixedLengthStringData(31).isAPartOf(filler, 186);
  	public FixedLengthStringData daywh08 = new FixedLengthStringData(31).isAPartOf(filler, 217);
  	public FixedLengthStringData daywh09 = new FixedLengthStringData(31).isAPartOf(filler, 248);
  	public FixedLengthStringData daywh10 = new FixedLengthStringData(31).isAPartOf(filler, 279);
  	public FixedLengthStringData daywh11 = new FixedLengthStringData(31).isAPartOf(filler, 310);
  	public FixedLengthStringData daywh12 = new FixedLengthStringData(31).isAPartOf(filler, 341);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(128).isAPartOf(th608Rec, 372, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th608Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th608Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}