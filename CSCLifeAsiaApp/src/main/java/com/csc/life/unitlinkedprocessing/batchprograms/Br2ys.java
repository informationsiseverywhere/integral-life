package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ZfmcpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.CovxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*REMARKS.
*
* This program is the splitter program for Benefit Billing.
*
* The efficiency of this program relies on its ability to find
* potential transactions quickly.  It  does this by having its
* own logical file over COVR. This logical, as supplied in the
* base system, may not be ideal for your site.  The logical is
* called COVRBBL. Use of Interactive SQL when Debug  is active
* will  identify if the COVRBBL access path is being used.  If
* not, the COVRBBL logical file should be customised  until it
* is being used. This program is the only program  which  uses
* the COVRBBL logical, not by calling the IO module  directly,
* but  by  utilising  the  access  path which the logical file
* uses.
*
* Batch System Parameters:
*
*    Param 1: Transaction Code to look up T5679. (4 Chars)
*    Param 4: Run Id for temporary file name.   (2 Chars)
*
* Control Totals:
*
*    01  Number of Threads used.
*    02  Number of records returned from SQL Select.
*    03  Number of rejections: Invalid T5679 Statuses.
*    04  Number of rejections: T5687/T5534 Editing.
*    05  Number of COVX records written.
*
* Notes of coding changes for this splitter program.
* -------------------------------------------------
*
* A Splitter Program's purpose is to find pending transactions
* from the database and  create  multiple  extract  files  for
* processing by multiple  copies  of  the  subsequent program,
* each running in its own  thread.  Each  subsequent  program,
* therefore, processes a discrete  portion of the total trans-
* actions.
*
* Splitter programs should be simple. They  should  only  deal
* with a single file. If there  is insufficient  data  on  the
* primary file to completely identify  a transaction, then the
* further editing should be done  in  the  subsequent process.
* The objective of a Splitter is  to rapidly isolate potential
* transactions, not to process the  transaction.  The  primary
* concern for Splitter program  design  is  elapsed time speed
* and this is achieved by minimising disk IO.
*
* Before the Splitter process is invoked, the program CRTTMPF,
* Create Temporary File, should be run.   CRTTMPF will  create
* a duplicate of a physical  file  (created  under  Smart  and
* defined as a Query file) which will have as many members  as
* are defined in that process's   'subsequent threads'  field.
* The temporary file will have the name XXXXDD9999, where XXXX
* is  the   first  four  characters  of  the  file  which  was
* duplicated, DD is the first two characters of the 4th system
* parameter and 9999 is the schedule run number.  Each  member
* added  to  the temporary file will be called THREAD999 where
* 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read  records  from  a primary file
* and, for each record selected, will  write  a  record  to  a
* temporary file member. Exactly which  temporary  file member
* is written to is decided by a  working  storage  count.  The
* objective of the count is to spread  the transaction records
* evenly amongst each thread member.
*
* The Splitter program should always have  a restart method of
* '1' indicating a re-run. In the  event  of  a  restart,  the
* Splitter program will always start with empty file  members.
* Apart from writing records to the  temporary  file  members,
* Splitter programs should not be doing any further updates.
 *
 *****************************************************************
 * </pre>
 */
public class Br2ys extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private DiskFileDAM covx01 = new DiskFileDAM("COVX01");
	private DiskFileDAM covx02 = new DiskFileDAM("COVX02");
	private DiskFileDAM covx03 = new DiskFileDAM("COVX03");
	private DiskFileDAM covx04 = new DiskFileDAM("COVX04");
	private DiskFileDAM covx05 = new DiskFileDAM("COVX05");
	private DiskFileDAM covx06 = new DiskFileDAM("COVX06");
	private DiskFileDAM covx07 = new DiskFileDAM("COVX07");
	private DiskFileDAM covx08 = new DiskFileDAM("COVX08");
	private DiskFileDAM covx09 = new DiskFileDAM("COVX09");
	private DiskFileDAM covx10 = new DiskFileDAM("COVX10");
	private DiskFileDAM covx11 = new DiskFileDAM("COVX11");
	private DiskFileDAM covx12 = new DiskFileDAM("COVX12");
	private DiskFileDAM covx13 = new DiskFileDAM("COVX13");
	private DiskFileDAM covx14 = new DiskFileDAM("COVX14");
	private DiskFileDAM covx15 = new DiskFileDAM("COVX15");
	private DiskFileDAM covx16 = new DiskFileDAM("COVX16");
	private DiskFileDAM covx17 = new DiskFileDAM("COVX17");
	private DiskFileDAM covx18 = new DiskFileDAM("COVX18");
	private DiskFileDAM covx19 = new DiskFileDAM("COVX19");
	private DiskFileDAM covx20 = new DiskFileDAM("COVX20");
	private CovxpfTableDAM covxpfData = new CovxpfTableDAM();
	/*
	 * Change the record length to that of the temporary file. This can be found
	 * by doing a DSPFD of the file being duplicated by the CRTTMPF process.
	 */
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR2YS");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaCovxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCovxFn, 0, FILLER).init("COVX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaCovxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCovxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER)
			.init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private PackedDecimalData iw = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0).init(1);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private PackedDecimalData wsaaEffectiveDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1);
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaGotOne = new FixedLengthStringData(1);
	private Validator gotOne = new Validator(wsaaGotOne, "Y");
	private static final int WSAAMAXINDEX = 1000;

	/*
	 * WSAA-T5687-T5534-TABLE 03 WSAA-T5687-T5534 OCCURS 256.
	 */
	private FixedLengthStringData[] wsaaT5687T5534 = FLSInittedArray(1000, 41);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 0, HIVALUE);
	private PackedDecimalData[] wsaaCrrcd = PDArrayPartOfArrayStructure(8, 0, wsaaT5687T5534, 4);
	private FixedLengthStringData[] wsaaBbmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 9);
	private FixedLengthStringData[] wsaaAdfeemth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 13);
	private FixedLengthStringData[] wsaaJlPremMeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 17);
	private FixedLengthStringData[] wsaaPremmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 21);
	private FixedLengthStringData[] wsaaSubprog = FLSDArrayPartOfArrayStructure(10, wsaaT5687T5534, 25);
	private FixedLengthStringData[] wsaaSvMethod = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 35);
	private FixedLengthStringData[] wsaaUnitFreq = FLSDArrayPartOfArrayStructure(2, wsaaT5687T5534, 39);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler4 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler4, 5);
	/* ERRORS */
	private static final String IVRM = "IVRM";
	private static final String H791 = "H791";
	/* FORMATS */

	/* TABLES */
	private static final String T5534 = "T5534";
	private static final String T5679 = "T5679";
	private static final String T5687 = "T5687";
	/* CONTROL-TOTALS */
	private static final int CT01 = 1;
	private static final int CT02 = 2;
	private static final int CT03 = 3;
	private static final int CT04 = 4;
	private static final int CT05 = 5;

	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");

	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private T5534rec t5534rec = new T5534rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private SqlZfmcpfInner sqlZfmcpfInner = new SqlZfmcpfInner();
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private int ix;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ZfmcpfDAO zfmcpfDAO = getApplicationContext().getBean("zfmcpfDAO", ZfmcpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
		
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Itempf>> t5534Map = null;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private Datcon1rec datcon1rec = new Datcon1rec();
	private ResultSet sqlzfmcpf1rs = null;
	private P6671par p6671par = new P6671par();
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	/**
	 * Contains all possible labels used by goTo action.
	 */
	public Br2ys() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void sqlError500() {
		/* CALL-SYSTEM-ERROR */
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

	protected void restart0900() {
		/* RESTART */
		/** This section is invoked in restart mode. Note section 1100 must */
		/** clear each temporary file member as the members are not under */
		/** commitment control. Note also that Splitter programs must have */
		/** a Restart Method of 1 (re-run). Thus no updating should occur */
		/** which would result in different records being returned from the */
		/** primary file in a restart! */
		/* EXIT */
	}

	protected void initialise1000() {
		/* Check the restart method is compatible with the program. */
		/* This program is restartable but will always re-run from */
		/* the beginning. This is because the temporary file members */
		/* are not under commitment control. */
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(IVRM);
			fatalError600();
		}
		/* The next thing we must do is construct the name of the */
		/* temporary file which we will be working with. */
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		/* Do a ovrdbf for each temporary file member. (Note we have */
		/* allowed for a maximum of 20.) */
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)) {
			openThreadMember1100();
		}
		/* Log the number of threads being used */
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(CT01);
		callContot001();
		/* Set up the details for the SQL select. */
		getT56791300();
		wsaaCompany.set(bprdIO.getCompany());
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);

		String sqlzfmcpf1 = " SELECT  Z.CHDRCOY, Z.CHDRNUM, Z.LIFE, Z.COVERAGE, Z.RIDER, Z.PLNSFX, Z.JLIFE, Z.PRMCUR, Z.ZFMCDAT, Z.SUMINS, Z.PCESDTE, Z.CRTABLE, Z.MORTCLS"
				+ " FROM   " + getAppVars().getTableNameOverriden("ZFMCPF Z INNER JOIN PAYRPF P ON Z.CHDRNUM=P.CHDRNUM") + " " + " WHERE Z.CHDRCOY = ?"
				+ " AND Z.ZFMCDAT <> 0" + " AND Z.VALIDFLAG = ?" + " AND Z.ZFMCDAT <= ? AND Z.CHDRNUM BETWEEN ? AND ? AND P.BTDATE=P.PTDATE AND P.VALIDFLAG = ? " 
				+ " ORDER BY Z.CHDRCOY, Z.CHDRNUM";
		sqlerrorflag = false;
		try {

			PreparedStatement sqlzfmcpf1ps = getAppVars().prepareStatementEmbeded(zfmcpfDAO.getConnection(), sqlzfmcpf1, "ZFMCPF");
			getAppVars().setDBString(sqlzfmcpf1ps, 1, wsaaCompany);
			getAppVars().setDBString(sqlzfmcpf1ps, 2, wsaa1);
			getAppVars().setDBNumber(sqlzfmcpf1ps, 3, wsaaEffectiveDate);
			getAppVars().setDBString(sqlzfmcpf1ps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlzfmcpf1ps, 5, wsaaChdrnumTo);
			getAppVars().setDBString(sqlzfmcpf1ps, 6, wsaa1);
			sqlzfmcpf1rs = zfmcpfDAO.executeQuery(sqlzfmcpf1ps);
		} catch (SQLException ex) {
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}

		if (sqlerrorflag) {
			sqlError500();
		}
		/* Load T5687 and T5534 into working storage for fast access. */
		String coy = bsprIO.getCompany().toString();
		t5687Map = itemDAO.loadSmartTable("IT", coy, T5687);
		t5534Map = itemDAO.loadSmartTable("IT", coy, T5534);
		ix = 1;
		loadT5687T55341200();
	}

	protected void openThreadMember1100() {
		/* Clear the member for this thread in case we are in restart */
		/* mode in which case it might already contain data. */
		/* MOVE IZ TO WSAA-THREAD-NUMBER. */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaCovxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/* Do the override. */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(COVX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaCovxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Open the file. */
		if (isEQ(iz, 1)) {
			covx01.openOutput();
		}
		if (isEQ(iz, 2)) {
			covx02.openOutput();
		}
		if (isEQ(iz, 3)) {
			covx03.openOutput();
		}
		if (isEQ(iz, 4)) {
			covx04.openOutput();
		}
		if (isEQ(iz, 5)) {
			covx05.openOutput();
		}
		if (isEQ(iz, 6)) {
			covx06.openOutput();
		}
		if (isEQ(iz, 7)) {
			covx07.openOutput();
		}
		if (isEQ(iz, 8)) {
			covx08.openOutput();
		}
		if (isEQ(iz, 9)) {
			covx09.openOutput();
		}
		if (isEQ(iz, 10)) {
			covx10.openOutput();
		}
		if (isEQ(iz, 11)) {
			covx11.openOutput();
		}
		if (isEQ(iz, 12)) {
			covx12.openOutput();
		}
		if (isEQ(iz, 13)) {
			covx13.openOutput();
		}
		if (isEQ(iz, 14)) {
			covx14.openOutput();
		}
		if (isEQ(iz, 15)) {
			covx15.openOutput();
		}
		if (isEQ(iz, 16)) {
			covx16.openOutput();
		}
		if (isEQ(iz, 17)) {
			covx17.openOutput();
		}
		if (isEQ(iz, 18)) {
			covx18.openOutput();
		}
		if (isEQ(iz, 19)) {
			covx19.openOutput();
		}
		if (isEQ(iz, 20)) {
			covx20.openOutput();
		}

	}

	protected void loadT5687T55341200() {
		/* Firstly get the T5687 dated item. */
		/* Get the T5534 record and see whether there is a */
		/* generic subroutine. If so we load the details from both */
		/* T5687 and T5534 into the table for fast search access. */
		if (t5687Map != null && !t5687Map.isEmpty()) {
			for (List<Itempf> t5687items : t5687Map.values()) {
				for (Itempf i : t5687items) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(i.getGenarea()));
					if (t5534Map != null && t5534Map.containsKey(t5687rec.bbmeth.toString())) {
						List<Itempf> itempfList = t5534Map.get(t5687rec.bbmeth.toString());
						t5534rec.t5534Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
						/* IF IX > 256 */
						if (isGT(ix, WSAAMAXINDEX)) {
							syserrrec.statuz.set(H791);
							syserrrec.params.set("T5687 & T5534");
							fatalError600();
						}
						wsaaCrtable[ix].set(i.getItemitem());
						wsaaCrrcd[ix].set(i.getItmfrm());
						wsaaAdfeemth[ix].set(t5534rec.adfeemth);
						wsaaJlPremMeth[ix].set(t5534rec.jlPremMeth);
						wsaaPremmeth[ix].set(t5534rec.premmeth);
						wsaaSubprog[ix].set(t5534rec.subprog);
						wsaaSvMethod[ix].set(t5534rec.svMethod);
						wsaaUnitFreq[ix].set(t5534rec.unitFreq);
						ix++;
					} else {
						t5534rec.t5534Rec.set(SPACES);
					}
				}
			}
		}
	}

	protected void getT56791300() {
		Itempf itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(bsprIO.getCompany().toString().trim());
		itempf.setItemtabl(T5679);
		itempf.setItemitem(bprdIO.getSystemParam01().toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.params.set(bsprIO.getCompany().toString()+T5679+bprdIO.getSystemParam01().toString());
			fatalError600();
		}
		else
			t5679rec.t5679Rec.set(itempf.getGenarea());
	}

	protected void readFile2000() {
		readFiles2010();
	}

	protected void readFiles2010() {

		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlzfmcpf1rs)) {
				getAppVars().getDBObject(sqlzfmcpf1rs, 1, sqlZfmcpfInner.sqlChdrcoy);
				getAppVars().getDBObject(sqlzfmcpf1rs, 2, sqlZfmcpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlzfmcpf1rs, 3, sqlZfmcpfInner.sqlLife);
				getAppVars().getDBObject(sqlzfmcpf1rs, 4, sqlZfmcpfInner.sqlCoverage);
				getAppVars().getDBObject(sqlzfmcpf1rs, 5, sqlZfmcpfInner.sqlRider);
				getAppVars().getDBObject(sqlzfmcpf1rs, 6, sqlZfmcpfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlzfmcpf1rs, 7, sqlZfmcpfInner.sqlJlife);
				getAppVars().getDBObject(sqlzfmcpf1rs, 8, sqlZfmcpfInner.sqlPrmcur);
				getAppVars().getDBObject(sqlzfmcpf1rs, 9, sqlZfmcpfInner.sqlZfmcdat);
				getAppVars().getDBObject(sqlzfmcpf1rs, 10, sqlZfmcpfInner.sqlSumins);
				getAppVars().getDBObject(sqlzfmcpf1rs, 11, sqlZfmcpfInner.sqlPcesdte);
				getAppVars().getDBObject(sqlzfmcpf1rs, 12, sqlZfmcpfInner.sqlCrtable);
				getAppVars().getDBObject(sqlzfmcpf1rs, 13, sqlZfmcpfInner.sqlMortcls);
			} else {
				eof2080();
			}
		} catch (SQLException ex) {
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		ct02Value++;
		return;
	}

	protected void eof2080() {
		wsspEdterror.set(varcom.endp);
	}

	protected void edit2500() {
		checkCollection4200();
		statcodeChecks2520();
		t5687T5534Checks2530();
		gotOne2580();
	}

	protected void statcodeChecks2520() {
		covrpf.setChdrcoy(sqlZfmcpfInner.sqlChdrcoy.toString()); 
		covrpf.setChdrnum(sqlZfmcpfInner.sqlChdrnum.toString());
		covrpf.setLife(sqlZfmcpfInner.sqlLife.toString());
		covrpf.setPlanSuffix(sqlZfmcpfInner.sqlPlnsfx.toInt());
		covrpf.setCoverage(sqlZfmcpfInner.sqlCoverage.toString());
		covrpf.setRider(sqlZfmcpfInner.sqlRider.toString());
		covrpf = covrpfDAO.readCovrenqData(covrpf);
		if(null == covrpf){
			syserrrec.params.set(sqlZfmcpfInner.sqlChdrcoy.toString() + sqlZfmcpfInner.sqlChdrnum.toString()+sqlZfmcpfInner.sqlLife.toString()+sqlZfmcpfInner.sqlPlnsfx.toInt()+sqlZfmcpfInner.sqlCoverage.toString()+sqlZfmcpfInner.sqlRider.toString());
			fatalError600();
		}
		wsaaValidCoverage.set("N");

		if (isNE(sqlZfmcpfInner.sqlCoverage, SPACES) && isEQ(sqlZfmcpfInner.sqlRider, SPACES)
				|| isEQ(sqlZfmcpfInner.sqlRider, "00")) {
			for (ix = 1; ix < 13; ix++) {
				if (isEQ(t5679rec.covRiskStat[ix], covrpf.getStatcode())) {
					ix = 13;
					wsaaValidCoverage.set("Y");
				}
			}
		}
		/* If we have a rider, check we have the correct status */
		if (isNE(sqlZfmcpfInner.sqlCoverage, SPACES) && isNE(sqlZfmcpfInner.sqlRider, SPACES)
				|| isNE(sqlZfmcpfInner.sqlRider, "00")) {
			for (ix = 1; ix < 13; ix++) {
				if (isEQ(t5679rec.ridRiskStat[ix], covrpf.getStatcode())) {
					ix = 13;
					wsaaValidCoverage.set("Y");
				}
			}
		}

		/* If we do not have a valid coverage or rider, skip to */
		/* the next record. */
		if (!validCoverage.isTrue()) {
			ct03Value++;
			wsspEdterror.set(SPACES);
			return;
		}
	}

	protected void t5687T5534Checks2530() {
		wsaaGotOne.set("N");
		/* PERFORM VARYING IX FROM 1 BY 1 UNTIL IX > 256 */
		for (ix = 1; !(isGT(ix, WSAAMAXINDEX) || isEQ(wsaaCrtable[ix], HIVALUE) || gotOne.isTrue()); ix++) {
			if (isEQ(wsaaCrtable[ix], sqlZfmcpfInner.sqlCrtable) && isLTE(wsaaCrrcd[ix], sqlZfmcpfInner.sqlPrmcur)) {
				if (isNE(wsaaSubprog[ix], SPACES)) {
					iw.set(ix);
					wsaaGotOne.set("Y");
				} else {
					/* MOVE 256 TO IX */
					ix = WSAAMAXINDEX;
				}
			}
		}
		if (!gotOne.isTrue()) {
			ct04Value++;
			wsspEdterror.set(SPACES);
			return;
		}
	}

	protected void gotOne2580() {
		wsspEdterror.set(varcom.oK);
	}

	protected void update3000() {
		covxpfData.chdrcoy.set(sqlZfmcpfInner.sqlChdrcoy);
		covxpfData.chdrnum.set(sqlZfmcpfInner.sqlChdrnum);
		covxpfData.life.set(covrpf.getLife());
		covxpfData.coverage.set(covrpf.getCoverage());
		covxpfData.rider.set(covrpf.getRider());
		covxpfData.planSuffix.set(covrpf.getPlanSuffix());
		covxpfData.jlife.set(covrpf.getJlife());
		covxpfData.premCurrency.set(sqlZfmcpfInner.sqlPrmcur);
		covxpfData.benBillDate.set(sqlZfmcpfInner.sqlZfmcdat);
		covxpfData.sumins.set(covrpf.getSumins());
		covxpfData.premCessDate.set(covrpf.getPremCessDate());
		covxpfData.crtable.set(covrpf.getCrtable());
		covxpfData.mortcls.set(covrpf.getMortcls());
		covxpfData.subprog.set(wsaaSubprog[iw.toInt()]);
		covxpfData.unitFreq.set(wsaaUnitFreq[iw.toInt()]);
		covxpfData.premmeth.set(wsaaPremmeth[iw.toInt()]);
		covxpfData.jlPremMeth.set(wsaaJlPremMeth[iw.toInt()]);
		covxpfData.svMethod.set(wsaaSvMethod[iw.toInt()]);
		covxpfData.adfeemth.set(wsaaAdfeemth[iw.toInt()]);
		if (isEQ(sqlZfmcpfInner.sqlChdrnum, lastChdrnum)) {
			/* NEXT_SENTENCE */
		}

		else {
			lastChdrnum.set(sqlZfmcpfInner.sqlChdrnum);
			iy.add(1);
			if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
				iy.set(1);
			}
		}
		if (isEQ(iy, 1)) {
			covx01.write(covxpfData);
		}
		if (isEQ(iy, 2)) {
			covx02.write(covxpfData);
		}
		if (isEQ(iy, 3)) {
			covx03.write(covxpfData);
		}
		if (isEQ(iy, 4)) {
			covx04.write(covxpfData);
		}
		if (isEQ(iy, 5)) {
			covx05.write(covxpfData);
		}
		if (isEQ(iy, 6)) {
			covx06.write(covxpfData);
		}
		if (isEQ(iy, 7)) {
			covx07.write(covxpfData);
		}
		if (isEQ(iy, 8)) {
			covx08.write(covxpfData);
		}
		if (isEQ(iy, 9)) {
			covx09.write(covxpfData);
		}
		if (isEQ(iy, 10)) {
			covx10.write(covxpfData);
		}
		if (isEQ(iy, 11)) {
			covx11.write(covxpfData);
		}
		if (isEQ(iy, 12)) {
			covx12.write(covxpfData);
		}
		if (isEQ(iy, 13)) {
			covx13.write(covxpfData);
		}
		if (isEQ(iy, 14)) {
			covx14.write(covxpfData);
		}
		if (isEQ(iy, 15)) {
			covx15.write(covxpfData);
		}
		if (isEQ(iy, 16)) {
			covx16.write(covxpfData);
		}
		if (isEQ(iy, 17)) {
			covx17.write(covxpfData);
		}
		if (isEQ(iy, 18)) {
			covx18.write(covxpfData);
		}
		if (isEQ(iy, 19)) {
			covx19.write(covxpfData);
		}
		if (isEQ(iy, 20)) {
			covx20.write(covxpfData);
		}
		/* Log the number of extacted records. */
		ct05Value++;
	}

	protected void commit3500() {
		contotrec.totno.set(CT02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct02Value = 0;
		contotrec.totno.set(CT03);
		contotrec.totval.set(ct03Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct03Value = 0;
		contotrec.totno.set(CT04);
		contotrec.totval.set(ct04Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct04Value = 0;
		contotrec.totno.set(CT05);
		contotrec.totval.set(ct05Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct05Value = 0;
	}

	protected void rollback3600() {
		/* ROLLBACK */
		/** There is no pre-rollback processing to be done */
		/* EXIT */
	}

	protected void close4000() {
		/* CLOSE-FILES */
		/* Close the open files and remove the overrides. */
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)) {
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/* EXIT */
	}

	protected void close4100() {
		if (isEQ(iz, 1)) {
			covx01.close();
		}
		if (isEQ(iz, 2)) {
			covx02.close();
		}
		if (isEQ(iz, 3)) {
			covx03.close();
		}
		if (isEQ(iz, 4)) {
			covx04.close();
		}
		if (isEQ(iz, 5)) {
			covx05.close();
		}
		if (isEQ(iz, 6)) {
			covx06.close();
		}
		if (isEQ(iz, 7)) {
			covx07.close();
		}
		if (isEQ(iz, 8)) {
			covx08.close();
		}
		if (isEQ(iz, 9)) {
			covx09.close();
		}
		if (isEQ(iz, 10)) {
			covx10.close();
		}
		if (isEQ(iz, 11)) {
			covx11.close();
		}
		if (isEQ(iz, 12)) {
			covx12.close();
		}
		if (isEQ(iz, 13)) {
			covx13.close();
		}
		if (isEQ(iz, 14)) {
			covx14.close();
		}
		if (isEQ(iz, 15)) {
			covx15.close();
		}
		if (isEQ(iz, 16)) {
			covx16.close();
		}
		if (isEQ(iz, 17)) {
			covx17.close();
		}
		if (isEQ(iz, 18)) {
			covx18.close();
		}
		if (isEQ(iz, 19)) {
			covx19.close();
		}
		if (isEQ(iz, 20)) {
			covx20.close();
		}

	}

	protected void checkCollection4200() {

		chdrpf = chdrpfDAO.getChdrpf(sqlZfmcpfInner.sqlChdrcoy.toString(), sqlZfmcpfInner.sqlChdrnum.toString());
		if(null==chdrpf) {
			syserrrec.params.set(sqlZfmcpfInner.sqlChdrcoy.toString()+ sqlZfmcpfInner.sqlChdrnum.toString());
			fatalError600();
		}
	}


	/*
	 * Class transformed from Data Structure SQL-COVRPF--INNER
	 */

	private static final class SqlZfmcpfInner {

		private FixedLengthStringData sqlZfmcpf = new FixedLengthStringData(48);
		private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlZfmcpf, 0);
		private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlZfmcpf, 1);
		private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlZfmcpf, 9);
		private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlZfmcpf, 11);
		private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlZfmcpf, 13);
		private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlZfmcpf, 15);
		private FixedLengthStringData sqlJlife = new FixedLengthStringData(2).isAPartOf(sqlZfmcpf, 18);
		private FixedLengthStringData sqlPrmcur = new FixedLengthStringData(3).isAPartOf(sqlZfmcpf, 20);
		private PackedDecimalData sqlZfmcdat = new PackedDecimalData(8, 0).isAPartOf(sqlZfmcpf, 23);
		private PackedDecimalData sqlSumins = new PackedDecimalData(17, 2).isAPartOf(sqlZfmcpf, 28);
		private PackedDecimalData sqlPcesdte = new PackedDecimalData(8, 0).isAPartOf(sqlZfmcpf, 38);
		private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlZfmcpf, 43);
		private FixedLengthStringData sqlMortcls = new FixedLengthStringData(1).isAPartOf(sqlZfmcpf, 47);

	}
}