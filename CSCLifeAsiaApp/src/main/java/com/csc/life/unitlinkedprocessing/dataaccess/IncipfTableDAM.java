package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IncipfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:37
 * Class transformed from INCIPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IncipfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 349; //217
	public FixedLengthStringData incirec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData incipfRecord = incirec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(incirec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(incirec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(incirec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(incirec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(incirec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(incirec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(incirec);
	public PackedDecimalData rcdate = DD.rcdate.copy().isAPartOf(incirec);
	public PackedDecimalData premCessDate = DD.pcesdte.copy().isAPartOf(incirec);
	public PackedDecimalData origPrem = DD.origprm.copy().isAPartOf(incirec);
	public PackedDecimalData currPrem = DD.currprm.copy().isAPartOf(incirec);
	
	public PackedDecimalData premStart01 = DD.premst.copy().isAPartOf(incirec);
	public PackedDecimalData premStart02 = DD.premst.copy().isAPartOf(incirec);
	public PackedDecimalData premStart03 = DD.premst.copy().isAPartOf(incirec);
	public PackedDecimalData premStart04 = DD.premst.copy().isAPartOf(incirec);	
	public PackedDecimalData premStart05 = DD.premst.copy().isAPartOf(incirec);
	public PackedDecimalData premStart06 = DD.premst.copy().isAPartOf(incirec);
	public PackedDecimalData premStart07 = DD.premst.copy().isAPartOf(incirec);
	
	
	public PackedDecimalData premCurr01 = DD.premcurr.copy().isAPartOf(incirec);
	public PackedDecimalData premCurr02 = DD.premcurr.copy().isAPartOf(incirec);
	public PackedDecimalData premCurr03 = DD.premcurr.copy().isAPartOf(incirec);
	public PackedDecimalData premCurr04 = DD.premcurr.copy().isAPartOf(incirec);	
	public PackedDecimalData premCurr05 = DD.premcurr.copy().isAPartOf(incirec);
	public PackedDecimalData premCurr06 = DD.premcurr.copy().isAPartOf(incirec);
	public PackedDecimalData premCurr07 = DD.premcurr.copy().isAPartOf(incirec);
	
	public PackedDecimalData pcUnits01 = DD.pcunit.copy().isAPartOf(incirec);
	public PackedDecimalData pcUnits02 = DD.pcunit.copy().isAPartOf(incirec);
	public PackedDecimalData pcUnits03 = DD.pcunit.copy().isAPartOf(incirec);
	public PackedDecimalData pcUnits04 = DD.pcunit.copy().isAPartOf(incirec);	
	public PackedDecimalData pcUnits05 = DD.pcunit.copy().isAPartOf(incirec);
	public PackedDecimalData pcUnits06 = DD.pcunit.copy().isAPartOf(incirec);
	public PackedDecimalData pcUnits07 = DD.pcunit.copy().isAPartOf(incirec);
	
	
	public PackedDecimalData unitSplit01 = DD.usplitpc.copy().isAPartOf(incirec);
	public PackedDecimalData unitSplit02 = DD.usplitpc.copy().isAPartOf(incirec);
	public PackedDecimalData unitSplit03 = DD.usplitpc.copy().isAPartOf(incirec);
	public PackedDecimalData unitSplit04 = DD.usplitpc.copy().isAPartOf(incirec);
	public PackedDecimalData unitSplit05 = DD.usplitpc.copy().isAPartOf(incirec);
	public PackedDecimalData unitSplit06 = DD.usplitpc.copy().isAPartOf(incirec);
	public PackedDecimalData unitSplit07 = DD.usplitpc.copy().isAPartOf(incirec);
	
	public PackedDecimalData inciNum = DD.incinum.copy().isAPartOf(incirec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(incirec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(incirec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(incirec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(incirec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(incirec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(incirec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(incirec);
	public FixedLengthStringData dormantFlag = DD.dormflag.copy().isAPartOf(incirec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(incirec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(incirec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(incirec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public IncipfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for IncipfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public IncipfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for IncipfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public IncipfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for IncipfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public IncipfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("INCIPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"RCDATE, " +
							"PCESDTE, " +
							"ORIGPRM, " +
							"CURRPRM, " +
							"PREMST01, " +
							"PREMST02, " +
							"PREMST03, " +
							"PREMST04, " +
							"PREMST05, " +
							"PREMST06, " +
							"PREMST07, " +							
							
							"PREMCURR01, " +
							"PREMCURR02, " +
							"PREMCURR03, " +
							"PREMCURR04, " +
							"PREMCURR05, " +
							"PREMCURR06, " +
							"PREMCURR07, " +
							
							"PCUNIT01, " +
							"PCUNIT02, " +
							"PCUNIT03, " +
							"PCUNIT04, " +
							"PCUNIT05, " +
							"PCUNIT06, " +
							"PCUNIT07, " +
							
							
							"USPLITPC01, " +
							"USPLITPC02, " +
							"USPLITPC03, " +
							"USPLITPC04, " +
							"USPLITPC05, " +
							"USPLITPC06, " +
							"USPLITPC07, " +
							
							"INCINUM, " +
							"CRTABLE, " +
							"USER_T, " +
							"TRTM, " +
							"TRDT, " +
							"TERMID, " +
							"PLNSFX, " +
							"SEQNO, " +
							"DORMFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     validflag,
                                     tranno,
                                     rcdate,
                                     premCessDate,
                                     origPrem,
                                     currPrem,
                                     premStart01,
                                     premStart02,
                                     premStart03,
                                     premStart04,
                                     premStart05,
                                     premStart06,
                                     premStart07,
                                     
                                     premCurr01,
                                     premCurr02,
                                     premCurr03,
                                     premCurr04,
                                     premCurr05,
                                     premCurr06,
                                     premCurr07,
                                     
                                     pcUnits01,
                                     pcUnits02,
                                     pcUnits03,
                                     pcUnits04,
                                     pcUnits05,
                                     pcUnits06,
                                     pcUnits07,
                                     
                                     unitSplit01,
                                     unitSplit02,
                                     unitSplit03,
                                     unitSplit04,
                                     unitSplit05,
                                     unitSplit06,
                                     unitSplit07,
                                     
                                     inciNum,
                                     crtable,
                                     user,
                                     transactionTime,
                                     transactionDate,
                                     termid,
                                     planSuffix,
                                     seqno,
                                     dormantFlag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		validflag.clear();
  		tranno.clear();
  		rcdate.clear();
  		premCessDate.clear();
  		origPrem.clear();
  		currPrem.clear();
  		premStart01.clear();
  		premStart02.clear();
  		premStart03.clear();
  		premStart04.clear();
  		premStart05.clear();
  		premStart06.clear();
  		premStart07.clear();
  		
  		premCurr01.clear();
  		premCurr02.clear();
  		premCurr03.clear();
  		premCurr04.clear();
  		premCurr05.clear();
  		premCurr06.clear();
  		premCurr07.clear();
  		
  		
  		pcUnits01.clear();
  		pcUnits02.clear();
  		pcUnits03.clear();
  		pcUnits04.clear();
  		pcUnits05.clear();
  		pcUnits06.clear();
  		pcUnits07.clear();
  		
  		unitSplit01.clear();
  		unitSplit02.clear();
  		unitSplit03.clear();
  		unitSplit04.clear();
  		unitSplit05.clear();
  		unitSplit06.clear();
  		unitSplit07.clear();
  		
  		inciNum.clear();
  		crtable.clear();
  		user.clear();
  		transactionTime.clear();
  		transactionDate.clear();
  		termid.clear();
  		planSuffix.clear();
  		seqno.clear();
  		dormantFlag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getIncirec() {
  		return incirec;
	}

	public FixedLengthStringData getIncipfRecord() {
  		return incipfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setIncirec(what);
	}

	public void setIncirec(Object what) {
  		this.incirec.set(what);
	}

	public void setIncipfRecord(Object what) {
  		this.incipfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(incirec.getLength());
		result.set(incirec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}