/*
 * File: Br610.java
 * Date: 29 August 2009 22:23:26
 * Author: Quipoz Limited
 * 
 * Class transformed from BR610.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br610TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UstfpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UstmDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ustfpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *             UNIT STATEMENTS - EXTRACT NEW TRANSACTION RECORDS
 *
 *
 * This  program reads  USTJ the Unit Statement trigger records
 * that are being processed this run. For each contract all
 * transactions that have been processed thru the Feedback Loop
 * and not printed on a statement are tagged with the next
 * statement number.
 *
 * If there were no transactions to be printed since the last
 * statement a message is given.
 *
 * If transactions are available to be printed update the Trigger record
 * with the statement number that was calculated earlier.
 *
 * The most recent USTF record for the contract is read. The last
 * Statement Number from here is incremented by 1 for use on the new
 * statements. All the USTN records for the contract, life, component &
 * fund from the USTF record are read and the details accumulated for
 * updating the totals in the USTF header record.  <SEE MOD NOTE 1>
 *
 * The USTN record is then re-written with the new Statement Number and
 * this then removes it from the USTN logical view and adds it to the
 * USTS logical view.
 *
 * If there have been no previous statements for this contract then set
 * all the details to zero.
 *
 * USTN records that have not been through the Unit Dealing process are
 * dropped.
 *
 * USTN records that have Effective Dates after the statement Trigger
 * Date are dropped.
 *
 * USTN records that are for charges, uninvested or debt are not
 * accumulated nor printed on the statement. However their Statement
 * number is updated with the Statement Number that would have
 * appeared on them and this stops them forever hanging around as
 * new transactions
 * and clogging up the access path. If they are required on the
 * Statement in future it will be possible to include them.
 *
 * In restart mode the standard restart procedure causes USTJ
 * records already processed to be ignored.
 *
 *
 *#
 *    CONTROL TOTALS
 *    ==============
 *    01   TRIGGER RECORDS READ
 *    02   NEW TRANSACTIONS SELECTED
 *    03   FUND HEADER RECORDS WRITTEN
 *    04   NON-UNIT TRANSACTIONS READ
 *    05   TRANS NOT PROCESSED THRU FEEDBACK LOOP
 *    06   TRANS AFTER RUN DATE
 *    07   NO TRANS SINCE LAST STMT
 *
 ***********************************************************************
 * </pre>
 */
public class Br610 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR610");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaUstmno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaNofTrans = new PackedDecimalData(9, 0);

	/* WSAA-FUND-ACCUMULATION */
	private FixedLengthStringData wsaaFundTotals = new FixedLengthStringData(62);
	private ZonedDecimalData wsaaUnits = new ZonedDecimalData(16, 5).isAPartOf(wsaaFundTotals, 0);
	private ZonedDecimalData wsaaDunits = new ZonedDecimalData(16, 5).isAPartOf(wsaaFundTotals, 16);
	private ZonedDecimalData wsaaFundCash = new ZonedDecimalData(17, 2).isAPartOf(wsaaFundTotals, 32);
	private ZonedDecimalData wsaaContCash = new ZonedDecimalData(13, 2).isAPartOf(wsaaFundTotals, 49);

	private FixedLengthStringData wsaaCreateFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaCreate = new Validator(wsaaCreateFlag, "Y");
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaInCount = new ZonedDecimalData(9, 0).init(0).setUnsigned();
	/* ERRORS */
	private String e228 = "E228";
	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	/*Logical File: Extra data screen*/

	/*Unit Statements - Fund details*/

	/*Unit Statement Trigger Records with Stat*/


	/*ZRST for Unit Statement*/


	//private Iterator<UstmData> ustmIter; 

	List<UstmData> ustmList;
	Map<String,List<UstmData>> ustmMap ;  
	private UstmData ustmData ;
	List<UstmData> ustjList;

	List<UstmData> updateList = null;
	private UstmDAO ustmDAO = getApplicationContext().getBean("ustmDAO", UstmDAO.class);

	List<String> chdrnumList = null;
	private Utrnpf utrnpf;

	private Br610TempDAO br610TempDAO = getApplicationContext().getBean("br610TempDAO", Br610TempDAO.class);
	Map<String,List<Br610DTO>> dataMap = new HashMap<String,List<Br610DTO>>();  
	Map<String,List<Br610DTO>> tempMap = new HashMap<String,List<Br610DTO>>();
	private Iterator<Br610DTO> br610DTOIter; 
	private Br610DTO br610DTO = null;

	private Ustfpf ustfpf;
	List<Ustfpf> ustfstmInsertList = null;
	List<Ustfpf> ustfstmTempList = null;
	List<Utrnpf> ustnUpdateList = null;
	List<Zrstpf> zrstUpdateList = null;

	private Zrstpf zrstpf;
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	Map<String,List<Zrstpf>> zrstMap ; 
	List<Zrstpf> zrstpfList;
	private Iterator<Zrstpf> zrstpfIter; 

	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private UstfpfDAO ustfpfDAO = getApplicationContext().getBean("ustfpfDAO", UstfpfDAO.class);
	int partitionId;
	private int rowCount;

	ArrayList<Integer> deleteIndex = new ArrayList<Integer>();

	private int ctrCT01; 
	private int ctrCT02;
	private int ctrCT03;
	private int ctrCT04; 
	private int ctrCT05;
	private int ctrCT06;
	private int ctrCT07; 
	private int minRecord = 1;



	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		a190Exit
	}

	public Br610() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

	protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		wsaaInCount.set(ZERO);
		wsaaFundTotals.fill("0");
		wsaaCreateFlag.set("N");

		/*EXIT*/	

		rowCount = bprdIO.cyclesPerCommit.toInt();
		if(rowCount <= 0)
			rowCount = 1000; //default size set as 1000  to ensure minimal speed
		partitionId = 0;

		ctrCT01 = 0;
		ctrCT02 = 0;
		ctrCT03 = 0;
		ctrCT04 = 0;
		ctrCT05 = 0;
		ctrCT06 = 0;
		ctrCT07 = 0;
	}

	protected void readFile2000()
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	}

	protected void readFile2010()
	{
		ustmMap = ustmDAO.readUstjData(bsscIO.getScheduleNumber().toInt(),bprdIO.getCompany().toString(),minRecord,minRecord+rowCount);
		if(ustmMap==null ||(ustmMap !=null && ustmMap.size()==0)){
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}

		Set<String> keys = ustmMap.keySet();
		chdrnumList = new ArrayList<String>();
		for (String key:keys)
		{
			chdrnumList.add(key.substring(0,8));
		}
	}

	protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

	protected void update3000()
	{
		try {
			update3010();
			minRecord +=rowCount;
		}
		catch (GOTOException e){
		}
	}

	protected void update3010()
	{
		dataMap.clear();
		ustfpf = new Ustfpf();
		zrstpf = new Zrstpf();
		ustfstmInsertList = new ArrayList<Ustfpf>();
		ustfstmTempList = new ArrayList<Ustfpf>();
		ustnUpdateList = new ArrayList<Utrnpf>() ;
		zrstUpdateList = new ArrayList<Zrstpf>() ;
		updateList = new ArrayList<UstmData>();
		// change for ticket ILIFE-7774
		if (chdrnumList.size() > 1000) {
			List<String> splitValueList = new ArrayList<String>();
			splitValueList.addAll(chdrnumList.subList(0, 1000));
            int count = chdrnumList.size() / 1000 + 1;
            for (int i = 1; i < count; i++) {
                int endIndex = 1000 * (i + 1);
                if (endIndex > chdrnumList.size()) {
                    endIndex = chdrnumList.size();
                }
                splitValueList.addAll(chdrnumList.subList(1000 * i, endIndex));
                Map<String, List<Br610DTO>> tempMap  = br610TempDAO.searchRecord(bprdIO.getCompany().toString(), splitValueList,rowCount,partitionId);
                tempMap.forEach((key, list) -> {
                	if (dataMap.containsKey(key)) {
    					dataMap.get(key).addAll(list);
    				} else {
    					dataMap.put(key, list);
    				}
                });
                
            }
		} else {
			//dataMap = br610TempDAO.searchRecord(bprdIO.getCompany().toString(), chdrnumList,rowCount,partitionId);
			int temppartitionId = 0;
			do {
			tempMap  = br610TempDAO.searchRecord(bprdIO.getCompany().toString(), chdrnumList,rowCount,temppartitionId);
			for (Map.Entry<String,List<Br610DTO>> entry : tempMap.entrySet()) {
				if (dataMap.containsKey(entry.getKey())) {
					dataMap.get(entry.getKey()).addAll(entry.getValue());
				} else {
					dataMap.put(entry.getKey(), entry.getValue());
				}
			}
            temppartitionId++;
			} while(!tempMap.isEmpty());
		}
		
		zrstMap = zrstpfDAO.searchZrststmByChdrnum(chdrnumList, bprdIO.getCompany().toString());
		partitionId++;

		if(dataMap == null || dataMap.isEmpty()){
			wsspEdterror.set(Varcom.endp);	
			return;
		}else{
			OuterLoop: for(Map.Entry<String, List<Br610DTO>> entry : dataMap.entrySet()) {
				String key = entry.getKey();
				String key1 = key.substring(0, 8);
				List<Br610DTO> value = entry.getValue();
				br610DTOIter = value.iterator();
				ustjList = ustmMap.get(key);
				if(null== ustjList || ustjList.isEmpty()){
					continue OuterLoop ;
				}
				else {
				ustmData = ustjList.get(0);
				}
				while(br610DTOIter.hasNext()){
					br610DTO = br610DTOIter.next();
					wsaaNofTrans.set(ZERO);
					wsaaTranno.set(ZERO);
					processUstnRecord3100();
				}	
				if (isEQ(wsaaNofTrans,0)) {
					/*contotrec.totval.set(1);
					contotrec.totno.set(ct07);
					callContot001();*/
					ctrCT07++;
					a200UpdateUstm();
					continue OuterLoop ;
				}
				for(Ustfpf u: ustfstmTempList){
					tagUstfRecord3300(u);
				}

				ustfstmTempList.clear();
				zrstpfList = zrstMap.get(key1);
				if(null == zrstpfList || zrstpfList.isEmpty()){
					continue OuterLoop;//IBPLIFE-3329
				}
				zrstpfIter = zrstpfList.iterator();

				while(zrstpfIter.hasNext()){
					zrstpf = zrstpfIter.next();
					a100UpdateZrststm();
				}
				setPrecision(ustmData.getUstmno(), 0);
				ustmData.setUstmno(ustmData.getUstmno()+1);
				updateList.add(ustmData);
				chdrnumList.clear();

			}
		}
	}

	protected void processUstnRecord3100(){
		if (isNE(br610DTO.getFdbkind(),"Y")) {
			ctrCT05++;
			return;
		}
		if (isGT(br610DTO.getMoniesdt(),ustmData.getStrpdate())) {
			ctrCT06++;
			return;
		}
		wsaaFundTotals.fill("0");
		if (isGT(br610DTO.getTranno(),wsaaTranno)) {
			wsaaTranno.set(br610DTO.getTranno());
		}
		if (isEQ(br610DTO.getVrtfnd(),SPACES)) {
			ctrCT04++;
			rewriteUstn3170();
			return;
		}
		wsaaNofTrans.add(1);
		ctrCT02++;
		wsaaFundCash.set(br610DTO.getFundamnt());
		wsaaContCash.set(br610DTO.getCntamnt());
		wsaaUnits.set(br610DTO.getNofunts());
		wsaaDunits.set(br610DTO.getNofdunts());
		ustfpf = new Ustfpf();
		processUstfs3150();
		rewriteUstn3170();
	}

	protected void processUstfs3150()
	{

		if(ustfstmInsertList.size() > 0){
			Ustfpf ustfpfTemp = ustfstmInsertList.get(ustfstmInsertList.size()-1);
			if   (  br610DTO.getChdrnum().equals( ustfpfTemp.getChdrnum())
					&& br610DTO.getPlnsfx() == ustfpfTemp.getPlnsfx()
					&& br610DTO.getLife().equals(ustfpfTemp.getLife())
					&& br610DTO.getCoverage().equals(ustfpfTemp.getCoverage())
					&& br610DTO.getRider().equals(ustfpfTemp.getRider())
					&& br610DTO.getVrtfnd().equals(ustfpfTemp.getVrtfnd())
					&& br610DTO.getUnityp().equals(ustfpfTemp.getUnityp())){

				ustfpf = ustfpfTemp;
			}
			else
			{
				findUstfRecord3500();
			}
		}
		else
		{
			findUstfRecord3500();
		}	

		processUstfRecord3700();
	}

	protected void rewriteUstn3170()
	{
		wsaaInCount.set(1);

		utrnpf = new Utrnpf();
		utrnpf.setUniqueNumber(br610DTO.getUstn_Unique_number());
		utrnpf.setStrpdate(ustmData.getStrpdate());
		setPrecision(utrnpf.getUstmno(), 0);
		utrnpf.setUstmno((int)(1 + ustmData.getUstmno()));
		ustnUpdateList.add(utrnpf);
	}

	protected void next3180()
	{
		//todo
	}
	protected void tagUstfRecord3300(Ustfpf u)
	{
		if ((setPrecision(u.getUstmno(), 0)
				&& isNE(u.getUstmno(),add(ustmData.getUstmno(),1)))) {
			newUstf3350();
		}
	}

	protected void newUstf3350()
	{
		para3350();
	}

	protected void para3350()
	{
		ustfpf.setUstmno((int)(1+ ustmData.getUstmno()));
		ustfpf.setStopdt((int)ustfpf.getStcldt());
		ustfpf.setStcldt(ustmData.getStrpdate());
		ustfpf.setUstmtflag(ustmData.getUnitStmtFlag());
		ustfpf.setStopun(ustfpf.getStclun());
		ustfpf.setStopdun(ustfpf.getStcldun());
		ustfpf.setStofuca(ustfpf.getStclfuca());
		ustfpf.setStopcoca(ustfpf.getStclcoca());
	}
	protected void findUstfRecord3500(){
		if (br610DTO.getUstfChdrcoy() != br610DTO.getCompany()
				|| br610DTO.getUstfChdrnum() != br610DTO.getChdrnum()
				|| br610DTO.getUstfPlnsfx() != br610DTO.getPlnsfx()
				|| br610DTO.getUstfLife() != br610DTO.getLife()
				|| br610DTO.getUstfCoverage() != br610DTO.getCoverage()
				|| br610DTO.getUstfRider()!=br610DTO.getRider()
				|| br610DTO.getUstfVrtfnd() != br610DTO.getVrtfnd()
				|| br610DTO.getUstfUnityp() != br610DTO.getUnityp())
		{
			wsaaCreateFlag.set("Y");
			ustfpf.setChdrcoy(br610DTO.getCompany());
			ustfpf.setChdrnum(br610DTO.getChdrnum());
			ustfpf.setPlnsfx(br610DTO.getPlnsfx());
			ustfpf.setLife(br610DTO.getLife());
			ustfpf.setCoverage(br610DTO.getCoverage());
			ustfpf.setRider(br610DTO.getRider());
			ustfpf.setVrtfnd(br610DTO.getVrtfnd());
			ustfpf.setUnityp(br610DTO.getUnityp());
			//ustfpf.setFormat(ustfrec);
			ustfpf.setUstmno(0);
			ustfpf.setStopdt(0);
			ustfpf.setStopun(BigDecimal.ZERO);
			ustfpf.setStopdun(BigDecimal.ZERO);
			ustfpf.setStofuca(BigDecimal.ZERO);
			ustfpf.setStopcoca(BigDecimal.ZERO);
			ustfpf.setStcldt(0);
			ustfpf.setStclun(BigDecimal.ZERO);
			ustfpf.setStcldun(BigDecimal.ZERO);
			ustfpf.setStclfuca(BigDecimal.ZERO);
			ustfpf.setStclcoca(BigDecimal.ZERO);
			ustfpf.setSttrun(BigDecimal.ZERO);
			ustfpf.setSttrdun(BigDecimal.ZERO);
			ustfpf.setSttrfuca(BigDecimal.ZERO);
			ustfpf.setSttrcoca(BigDecimal.ZERO);
		}
	}

	protected void processUstfRecord3700(){
		//ustfstmIO.setFormat(ustfrec);
		if (!wsaaCreate.isTrue()) {

			deleteIndex.add(ustfstmInsertList.size()-1);
		}
		wsaaCreateFlag.set("N");
		compute(wsaaUstmno, 0).set(add(1,ustmData.getUstmno()));
		if (isNE(ustfpf.getUstmno(),wsaaUstmno)) {
			ustfpf.setSttrfuca(BigDecimal.ZERO);
			ustfpf.setStofuca(BigDecimal.ZERO);
			ustfpf.setSttrcoca(BigDecimal.ZERO);
			ustfpf.setSttrun(BigDecimal.ZERO);
			ustfpf.setSttrdun(BigDecimal.ZERO);
			ustfpf.setStopdt((int)ustfpf.getStcldt());
			ustfpf.setStcldt(ustmData.getStrpdate());
			ustfpf.setUstmtflag(ustmData.getUnitStmtFlag());
			ustfpf.setStopun(ustfpf.getStclun());
			ustfpf.setStopdun(ustfpf.getStcldun());
			ustfpf.setStofuca(ustfpf.getStclfuca());
			ustfpf.setStopcoca(ustfpf.getStclcoca());
		}
		setPrecision(ustfpf.getSttrfuca(), 2);
		ustfpf.setSttrfuca(add(ustfpf.getSttrfuca(),wsaaFundCash).getbigdata());
		setPrecision(ustfpf.getSttrcoca(), 2);
		ustfpf.setSttrcoca(add(ustfpf.getSttrcoca(),wsaaContCash).getbigdata());
		setPrecision(ustfpf.getSttrdun(), 5);
		ustfpf.setSttrdun(add(ustfpf.getSttrdun(),wsaaDunits).getbigdata());
		setPrecision(ustfpf.getSttrun(), 5);
		ustfpf.setSttrun(add(ustfpf.getSttrun(),wsaaUnits).getbigdata());
		setPrecision(ustfpf.getStclfuca(), 2);
		ustfpf.setStclfuca(add(ustfpf.getStofuca(),ustfpf.getSttrfuca()).getbigdata());;
		setPrecision(ustfpf.getStclcoca(), 2);
		ustfpf.setStclcoca(add(ustfpf.getStopcoca(),ustfpf.getSttrcoca()).getbigdata());
		setPrecision(ustfpf.getStclun(), 5);
		ustfpf.setStclun(add(ustfpf.getStopun(),ustfpf.getSttrun()).getbigdata());
		setPrecision(ustfpf.getStcldun(), 5);
		ustfpf.setStcldun(add(ustfpf.getStopdun(),ustfpf.getSttrdun()).getbigdata());
		setPrecision(ustfpf.getUstmno(), 0);
		ustfpf.setUstmno((int)(1 + ustmData.getUstmno()));

		ustfstmInsertList.add(ustfpf);
		ustfstmTempList.add(ustfpf);
		ctrCT03++;
	}

	protected void a100UpdateZrststm()
	{
		try {
			a110UpdateZrststm();
		}
		catch (GOTOException e){
		}
	}

	protected void a110UpdateZrststm()
	{
		setPrecision(zrstpf.getUstmno(), 0);
		zrstpf.setUstmno((int)(1 + ustmData.getUstmno()));

		zrstUpdateList.add(zrstpf);	
	}

	protected void a200UpdateUstm()
	{
		a200Start();
	}
	protected void a200Start()
	{	
		ustmData.setJobnoStmt(0);
		updateList.add(ustmData);
	}

	protected void commitControlTotals(){
		//ct01
		contotrec.totno.set(ct01);
		contotrec.totval.set(ctrCT01);
		callProgram(Contot.class, contotrec.contotRec);		
		ctrCT01 = 0;

		//ct02
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT02 = 0;
		//ct03
		contotrec.totno.set(ct03);
		contotrec.totval.set(ctrCT03);
		callProgram(Contot.class, contotrec.contotRec);		
		ctrCT03 = 0;

		//ct04
		contotrec.totno.set(ct04);
		contotrec.totval.set(ctrCT04);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT02 = 4;

		//ct05
		contotrec.totno.set(ct05);
		contotrec.totval.set(ctrCT05);
		callProgram(Contot.class, contotrec.contotRec);		
		ctrCT05 = 0;

		//ct06
		contotrec.totno.set(ct06);
		contotrec.totval.set(ctrCT06);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT06 = 0;

		//ct07
		contotrec.totno.set(ct07);
		contotrec.totval.set(ctrCT07);
		callProgram(Contot.class, contotrec.contotRec);	
		ctrCT07 = 0;

	}

	protected void commit3500()
	{
		/*COMMIT*/	
		Collections.sort(deleteIndex, Collections.reverseOrder());
		// IBPLIFE-2145 start
		if (!(ustfstmInsertList == null || ustfstmInsertList.isEmpty())) {
			for (int i : deleteIndex) {
				if (i < ustfstmInsertList.size() && i >= 0) {
					ustfstmInsertList.remove(i);
				}
			}
		}
		// IBPLIFE-2145 end
		commitControlTotals();
		if (ustfstmInsertList != null && !ustfstmInsertList.isEmpty()) ustfpfDAO.insertUstfstmRecord(ustfstmInsertList);
		if (ustnUpdateList != null && !ustnUpdateList.isEmpty()) utrnpfDAO.insertUstnRecord(ustnUpdateList);
		if (zrstUpdateList != null && !zrstUpdateList.isEmpty()) zrstpfDAO.updateZrststm(zrstUpdateList);
		if (updateList != null && !updateList.isEmpty()) ustmDAO.updateUstjData(updateList);
		deleteIndex.clear();
		/*EXIT*/
	}

	protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

	protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);

		if(ustfstmInsertList != null && !ustfstmInsertList.isEmpty()){
		ustfstmInsertList.clear();
		ustfstmInsertList = null;
		}
		if (ustnUpdateList != null && !ustnUpdateList.isEmpty()){
		ustnUpdateList.clear();
		ustnUpdateList = null;
		}
		if (zrstUpdateList != null && !zrstUpdateList.isEmpty()){
		zrstUpdateList.clear();
		zrstUpdateList = null;
		}
		if (updateList != null && !updateList.isEmpty()){
		updateList.clear();
		updateList = null ;
		}
		if (isEQ(wsaaInCount,0)) {
			conlogrec.params.set(SPACES);
			conlogrec.error.set(e228);
			callConlog003();
		}
		/*EXIT*/
	}
}