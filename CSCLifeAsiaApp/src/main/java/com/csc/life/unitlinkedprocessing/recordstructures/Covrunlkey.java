package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:28
 * Description:
 * Copybook name: COVRUNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrunlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrunlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covrunlKey = new FixedLengthStringData(256).isAPartOf(covrunlFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrunlChdrcoy = new FixedLengthStringData(1).isAPartOf(covrunlKey, 0);
  	public FixedLengthStringData covrunlChdrnum = new FixedLengthStringData(8).isAPartOf(covrunlKey, 1);
  	public FixedLengthStringData covrunlLife = new FixedLengthStringData(2).isAPartOf(covrunlKey, 9);
  	public FixedLengthStringData covrunlCoverage = new FixedLengthStringData(2).isAPartOf(covrunlKey, 11);
  	public FixedLengthStringData covrunlRider = new FixedLengthStringData(2).isAPartOf(covrunlKey, 13);
  	public PackedDecimalData covrunlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrunlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(covrunlKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrunlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrunlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}