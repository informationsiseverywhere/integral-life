package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6326
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6326ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,113);
	public ZonedDecimalData lumpContrib = DD.lmpcnt.copyToZonedDecimal().isAPartOf(dataFields,160);
	public ZonedDecimalData matage = DD.matage.copyToZonedDecimal().isAPartOf(dataFields,177);
	public ZonedDecimalData mattcess = DD.mattcess.copyToZonedDecimal().isAPartOf(dataFields,180);
	public ZonedDecimalData mattrm = DD.mattrm.copyToZonedDecimal().isAPartOf(dataFields,188);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,191);
	public ZonedDecimalData numapp = DD.numapp.copyToZonedDecimal().isAPartOf(dataFields,192);
	public ZonedDecimalData numavail = DD.numavail.copyToZonedDecimal().isAPartOf(dataFields,196);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,200);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,201);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,204);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,207);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,211);
	public FixedLengthStringData ratypind = DD.ratypind.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData reserveUnitsInd = DD.rsunin.copy().isAPartOf(dataFields,222);
	public ZonedDecimalData reserveUnitsDate = DD.rundte.copyToZonedDecimal().isAPartOf(dataFields,223);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,231);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,232);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,249);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,250);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,252);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,256);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,271);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,288);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,289);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,302);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,319);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 336);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 339);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 369);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 386);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 403);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 420);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 437);
	
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development */
	public FixedLengthStringData optsmode =DD.optsmode.copy().isAPartOf(dataFields,454);
	public FixedLengthStringData optind =DD.optind.copy().isAPartOf(dataFields,455);//BRD-NBP-012 
	
	public FixedLengthStringData singpremtype = DD.sngprmtyp.copy().isAPartOf(dataFields,456);//ILIFE-7805
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,459);
	

	/*BRD-306 END */
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lmpcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData matageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mattcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mattrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData numappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData numavailErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData ratypindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rsuninErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData rundteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development */
	public FixedLengthStringData optsmodeErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,180);
	public FixedLengthStringData optindErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,184);//BRD-NBP-012 
	public FixedLengthStringData singpremtypeErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,188);//ILIFE-7805
	public FixedLengthStringData zstpduty01Err =new FixedLengthStringData(4).isAPartOf(errorIndicators,192);

	/*BRD-306 END */
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea,getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lmpcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] matageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mattcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mattrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] numappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] numavailOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] ratypindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rsuninOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] rundteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development */
	public FixedLengthStringData[]optsmodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[]optindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);//BRD-NBP-012 
	public FixedLengthStringData[] singpremtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);//ILIFE-7805
	public FixedLengthStringData[] zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	
	/*BRD-306 END */
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData mattcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData reserveUnitsDateDisp = new FixedLengthStringData(10);

	public LongData S6326screenWritten = new LongData(0);
	public LongData S6326protectWritten = new LongData(0);

	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 6, 15, 16, 17, 18, 20, 21, 22, 23, 24, 60, 61, 62}; 	
	
	public boolean hasSubfile() {
		return false;
	}


	public S6326ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"03","01","-03","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(matageOut,new String[] {"14","13","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattrmOut,new String[] {"16","15","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattcessOut,new String[] {"17","18","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"07","06","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"09","08","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premcessOut,new String[] {"10","11","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rsuninOut,new String[] {"20","19","-20","45",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rundteOut,new String[] {"22","21","-22","46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"24","23","-24","40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(singprmOut,new String[] {"26","25","-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"34","36","-34","35",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lmpcntOut,new String[] {"37","39","-37","38",null, null, null, null, null, null, null, null});
		fieldIndMap.put(polincOut,new String[] {null, null, null, "27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"32","42","-32","42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numavailOut,new String[] {null, null, null, "27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numappOut,new String[] {"30","28","-30","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratypindOut,new String[] {"31","33","-31","41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"48","50","-49","51",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, "70", null, "61",null, null, null, null, null, null, null, null});	//ILIFE-3188
		fieldIndMap.put(taxindOut,new String[] {"58","60","-59","61",null, null, null, null, null, null, null, null});
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts */
		fieldIndMap.put(optsmodeOut,new String[] {"56","57","-56","57",null, null, null, null, null, null, null, null});
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends */
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "59", null, null, null, null, null, null, null, null});
		fieldIndMap.put(optindOut,new String[] {"62","63","-62","64",null, null, null, null, null, null, null, null});//BRD-NBP-012 
		/*BRD-306 END */
		fieldIndMap.put(singpremtypeOut,new String[] {"65","66","-65","67",null, null, null, null, null, null, null, null});//ILIFE-7805
		
		fieldIndMap.put(zstpduty01Out,new String[] {"68","69","-70","71",null, null, null, null, null, null, null, null});
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6326screen.class;
		protectRecord = S6326protect.class;
	}

	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	
	public int getDataAreaSize() {
		return 1260;
	}
	

	public int getDataFieldsSize(){
		return 476;  
	}
	public int getErrorIndicatorSize(){
		return 196; 
	}
	
	public int getOutputFieldSize(){
		return 588; 
	}


	public BaseData[] getscreenFields(){
		return new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifenum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, sumin, currcd, matage, mattrm, mattcess, premCessAge, premCessTerm, premcess, reserveUnitsInd, reserveUnitsDate, liencd, singlePremium, mortcls, lumpContrib, polinc, optextind, numavail, numapp, ratypind, zlinstprem, select, taxamt, taxind, loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt,optsmode,optind,singpremtype,zstpduty01};   //IFSU-2036
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, suminOut, currcdOut, matageOut, mattrmOut, mattcessOut, pcessageOut, pcesstrmOut, premcessOut, rsuninOut, rundteOut, liencdOut, singprmOut, mortclsOut, lmpcntOut, polincOut, optextindOut, numavailOut, numappOut, ratypindOut, zlinstpremOut, selectOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut,optsmodeOut,optindOut,singpremtypeOut,zstpduty01Out};
	}
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, suminErr, currcdErr, matageErr, mattrmErr, mattcessErr, pcessageErr, pcesstrmErr, premcessErr, rsuninErr, rundteErr, liencdErr, singprmErr, mortclsErr, lmpcntErr, polincErr, optextindErr, numavailErr, numappErr, ratypindErr, zlinstpremErr, selectErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr,optsmodeErr,optindErr,singpremtypeErr,zstpduty01Err};  
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {mattcess, premcess, reserveUnitsDate};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {mattcessDisp, premcessDisp, reserveUnitsDateDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {mattcessErr, premcessErr, rundteErr};
	}
	
	
	
	
	
}
