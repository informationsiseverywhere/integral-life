package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZstapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:02
 * Class transformed from ZSTAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZstapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 100;
	public FixedLengthStringData zstarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zstapfRecord = zstarec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zstarec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zstarec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(zstarec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(zstarec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zstarec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zstarec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zstarec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(zstarec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(zstarec);
	public PackedDecimalData stmtOpDate = DD.stopdt.copy().isAPartOf(zstarec);
	public PackedDecimalData stmtOpDunits = DD.stopdun.copy().isAPartOf(zstarec);
	public PackedDecimalData stmtClDate = DD.stcldt.copy().isAPartOf(zstarec);
	public PackedDecimalData stmtClDunits = DD.stcldun.copy().isAPartOf(zstarec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zstarec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zstarec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zstarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZstapfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZstapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZstapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZstapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZstapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZstapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZstapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZSTAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"USTMNO, " +
							"PLNSFX, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"VRTFND, " +
							"UNITYP, " +
							"STOPDT, " +
							"STOPDUN, " +
							"STCLDT, " +
							"STCLDUN, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     ustmno,
                                     planSuffix,
                                     life,
                                     coverage,
                                     rider,
                                     unitVirtualFund,
                                     unitType,
                                     stmtOpDate,
                                     stmtOpDunits,
                                     stmtClDate,
                                     stmtClDunits,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		ustmno.clear();
  		planSuffix.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		unitVirtualFund.clear();
  		unitType.clear();
  		stmtOpDate.clear();
  		stmtOpDunits.clear();
  		stmtClDate.clear();
  		stmtClDunits.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZstarec() {
  		return zstarec;
	}

	public FixedLengthStringData getZstapfRecord() {
  		return zstapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZstarec(what);
	}

	public void setZstarec(Object what) {
  		this.zstarec.set(what);
	}

	public void setZstapfRecord(Object what) {
  		this.zstapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zstarec.getLength());
		result.set(zstarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}