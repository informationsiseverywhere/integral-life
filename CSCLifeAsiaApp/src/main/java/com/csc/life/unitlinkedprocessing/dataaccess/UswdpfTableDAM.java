package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UswdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:54
 * Class transformed from USWDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UswdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 886;
	public FixedLengthStringData uswdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData uswdpfRecord = uswdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(uswdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(uswdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(uswdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(uswdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(uswdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(uswdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(uswdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(uswdrec);
	public FixedLengthStringData percentAmountInd = DD.pcamtind.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund01 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund02 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund03 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund04 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund05 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund06 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund07 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund08 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund09 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund10 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund11 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund12 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund13 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund14 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund15 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund16 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund17 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund18 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund19 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFund20 = DD.srcfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType01 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType02 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType03 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType04 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType05 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType06 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType07 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType08 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType09 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType10 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType11 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType12 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType13 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType14 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType15 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType16 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType17 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType18 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType19 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundType20 = DD.scfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency01 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency02 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency03 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency04 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency05 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency06 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency07 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency08 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency09 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency10 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency11 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency12 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency13 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency14 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency15 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency16 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency17 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency18 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency19 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData sourceFundCurrency20 = DD.scfndcur.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount01 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount02 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount03 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount04 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount05 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount06 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount07 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount08 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount09 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount10 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount11 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount12 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount13 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount14 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount15 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount16 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount17 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount18 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount19 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourcePercentAmount20 = DD.scprcamt.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue01 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue02 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue03 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue04 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue05 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue06 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue07 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue08 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue09 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue10 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue11 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue12 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue13 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue14 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue15 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue16 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue17 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue18 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue19 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceEstimatedValue20 = DD.scestval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue01 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue02 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue03 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue04 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue05 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue06 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue07 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue08 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue09 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue10 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue11 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue12 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue13 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue14 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue15 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue16 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue17 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue18 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue19 = DD.scactval.copy().isAPartOf(uswdrec);
	public PackedDecimalData sourceActualValue20 = DD.scactval.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund01 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund02 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund03 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund04 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund05 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund06 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund07 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund08 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund09 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFund10 = DD.tgtfund.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType01 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType02 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType03 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType04 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType05 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType06 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType07 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType08 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType09 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundType10 = DD.tgfndtyp.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency01 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency02 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency03 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency04 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency05 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency06 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency07 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency08 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency09 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public FixedLengthStringData targetFundCurrency10 = DD.tgfndcur.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent01 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent02 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent03 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent04 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent05 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent06 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent07 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent08 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent09 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData targetPercent10 = DD.tgtprcnt.copy().isAPartOf(uswdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(uswdrec);
	public FixedLengthStringData overrideFee = DD.orswchfe.copy().isAPartOf(uswdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(uswdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(uswdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(uswdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UswdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UswdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UswdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UswdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UswdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UswdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UswdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("USWDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PLNSFX, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"TRANNO, " +
							"PCAMTIND, " +
							"SRCFUND01, " +
							"SRCFUND02, " +
							"SRCFUND03, " +
							"SRCFUND04, " +
							"SRCFUND05, " +
							"SRCFUND06, " +
							"SRCFUND07, " +
							"SRCFUND08, " +
							"SRCFUND09, " +
							"SRCFUND10, " +
							"SRCFUND11, " +
							"SRCFUND12, " +
							"SRCFUND13, " +
							"SRCFUND14, " +
							"SRCFUND15, " +
							"SRCFUND16, " +
							"SRCFUND17, " +
							"SRCFUND18, " +
							"SRCFUND19, " +
							"SRCFUND20, " +
							"SCFNDTYP01, " +
							"SCFNDTYP02, " +
							"SCFNDTYP03, " +
							"SCFNDTYP04, " +
							"SCFNDTYP05, " +
							"SCFNDTYP06, " +
							"SCFNDTYP07, " +
							"SCFNDTYP08, " +
							"SCFNDTYP09, " +
							"SCFNDTYP10, " +
							"SCFNDTYP11, " +
							"SCFNDTYP12, " +
							"SCFNDTYP13, " +
							"SCFNDTYP14, " +
							"SCFNDTYP15, " +
							"SCFNDTYP16, " +
							"SCFNDTYP17, " +
							"SCFNDTYP18, " +
							"SCFNDTYP19, " +
							"SCFNDTYP20, " +
							"SCFNDCUR01, " +
							"SCFNDCUR02, " +
							"SCFNDCUR03, " +
							"SCFNDCUR04, " +
							"SCFNDCUR05, " +
							"SCFNDCUR06, " +
							"SCFNDCUR07, " +
							"SCFNDCUR08, " +
							"SCFNDCUR09, " +
							"SCFNDCUR10, " +
							"SCFNDCUR11, " +
							"SCFNDCUR12, " +
							"SCFNDCUR13, " +
							"SCFNDCUR14, " +
							"SCFNDCUR15, " +
							"SCFNDCUR16, " +
							"SCFNDCUR17, " +
							"SCFNDCUR18, " +
							"SCFNDCUR19, " +
							"SCFNDCUR20, " +
							"SCPRCAMT01, " +
							"SCPRCAMT02, " +
							"SCPRCAMT03, " +
							"SCPRCAMT04, " +
							"SCPRCAMT05, " +
							"SCPRCAMT06, " +
							"SCPRCAMT07, " +
							"SCPRCAMT08, " +
							"SCPRCAMT09, " +
							"SCPRCAMT10, " +
							"SCPRCAMT11, " +
							"SCPRCAMT12, " +
							"SCPRCAMT13, " +
							"SCPRCAMT14, " +
							"SCPRCAMT15, " +
							"SCPRCAMT16, " +
							"SCPRCAMT17, " +
							"SCPRCAMT18, " +
							"SCPRCAMT19, " +
							"SCPRCAMT20, " +
							"SCESTVAL01, " +
							"SCESTVAL02, " +
							"SCESTVAL03, " +
							"SCESTVAL04, " +
							"SCESTVAL05, " +
							"SCESTVAL06, " +
							"SCESTVAL07, " +
							"SCESTVAL08, " +
							"SCESTVAL09, " +
							"SCESTVAL10, " +
							"SCESTVAL11, " +
							"SCESTVAL12, " +
							"SCESTVAL13, " +
							"SCESTVAL14, " +
							"SCESTVAL15, " +
							"SCESTVAL16, " +
							"SCESTVAL17, " +
							"SCESTVAL18, " +
							"SCESTVAL19, " +
							"SCESTVAL20, " +
							"SCACTVAL01, " +
							"SCACTVAL02, " +
							"SCACTVAL03, " +
							"SCACTVAL04, " +
							"SCACTVAL05, " +
							"SCACTVAL06, " +
							"SCACTVAL07, " +
							"SCACTVAL08, " +
							"SCACTVAL09, " +
							"SCACTVAL10, " +
							"SCACTVAL11, " +
							"SCACTVAL12, " +
							"SCACTVAL13, " +
							"SCACTVAL14, " +
							"SCACTVAL15, " +
							"SCACTVAL16, " +
							"SCACTVAL17, " +
							"SCACTVAL18, " +
							"SCACTVAL19, " +
							"SCACTVAL20, " +
							"TGTFUND01, " +
							"TGTFUND02, " +
							"TGTFUND03, " +
							"TGTFUND04, " +
							"TGTFUND05, " +
							"TGTFUND06, " +
							"TGTFUND07, " +
							"TGTFUND08, " +
							"TGTFUND09, " +
							"TGTFUND10, " +
							"TGFNDTYP01, " +
							"TGFNDTYP02, " +
							"TGFNDTYP03, " +
							"TGFNDTYP04, " +
							"TGFNDTYP05, " +
							"TGFNDTYP06, " +
							"TGFNDTYP07, " +
							"TGFNDTYP08, " +
							"TGFNDTYP09, " +
							"TGFNDTYP10, " +
							"TGFNDCUR01, " +
							"TGFNDCUR02, " +
							"TGFNDCUR03, " +
							"TGFNDCUR04, " +
							"TGFNDCUR05, " +
							"TGFNDCUR06, " +
							"TGFNDCUR07, " +
							"TGFNDCUR08, " +
							"TGFNDCUR09, " +
							"TGFNDCUR10, " +
							"TGTPRCNT01, " +
							"TGTPRCNT02, " +
							"TGTPRCNT03, " +
							"TGTPRCNT04, " +
							"TGTPRCNT05, " +
							"TGTPRCNT06, " +
							"TGTPRCNT07, " +
							"TGTPRCNT08, " +
							"TGTPRCNT09, " +
							"TGTPRCNT10, " +
							"EFFDATE, " +
							"ORSWCHFE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     planSuffix,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     tranno,
                                     percentAmountInd,
                                     sourceFund01,
                                     sourceFund02,
                                     sourceFund03,
                                     sourceFund04,
                                     sourceFund05,
                                     sourceFund06,
                                     sourceFund07,
                                     sourceFund08,
                                     sourceFund09,
                                     sourceFund10,
                                     sourceFund11,
                                     sourceFund12,
                                     sourceFund13,
                                     sourceFund14,
                                     sourceFund15,
                                     sourceFund16,
                                     sourceFund17,
                                     sourceFund18,
                                     sourceFund19,
                                     sourceFund20,
                                     sourceFundType01,
                                     sourceFundType02,
                                     sourceFundType03,
                                     sourceFundType04,
                                     sourceFundType05,
                                     sourceFundType06,
                                     sourceFundType07,
                                     sourceFundType08,
                                     sourceFundType09,
                                     sourceFundType10,
                                     sourceFundType11,
                                     sourceFundType12,
                                     sourceFundType13,
                                     sourceFundType14,
                                     sourceFundType15,
                                     sourceFundType16,
                                     sourceFundType17,
                                     sourceFundType18,
                                     sourceFundType19,
                                     sourceFundType20,
                                     sourceFundCurrency01,
                                     sourceFundCurrency02,
                                     sourceFundCurrency03,
                                     sourceFundCurrency04,
                                     sourceFundCurrency05,
                                     sourceFundCurrency06,
                                     sourceFundCurrency07,
                                     sourceFundCurrency08,
                                     sourceFundCurrency09,
                                     sourceFundCurrency10,
                                     sourceFundCurrency11,
                                     sourceFundCurrency12,
                                     sourceFundCurrency13,
                                     sourceFundCurrency14,
                                     sourceFundCurrency15,
                                     sourceFundCurrency16,
                                     sourceFundCurrency17,
                                     sourceFundCurrency18,
                                     sourceFundCurrency19,
                                     sourceFundCurrency20,
                                     sourcePercentAmount01,
                                     sourcePercentAmount02,
                                     sourcePercentAmount03,
                                     sourcePercentAmount04,
                                     sourcePercentAmount05,
                                     sourcePercentAmount06,
                                     sourcePercentAmount07,
                                     sourcePercentAmount08,
                                     sourcePercentAmount09,
                                     sourcePercentAmount10,
                                     sourcePercentAmount11,
                                     sourcePercentAmount12,
                                     sourcePercentAmount13,
                                     sourcePercentAmount14,
                                     sourcePercentAmount15,
                                     sourcePercentAmount16,
                                     sourcePercentAmount17,
                                     sourcePercentAmount18,
                                     sourcePercentAmount19,
                                     sourcePercentAmount20,
                                     sourceEstimatedValue01,
                                     sourceEstimatedValue02,
                                     sourceEstimatedValue03,
                                     sourceEstimatedValue04,
                                     sourceEstimatedValue05,
                                     sourceEstimatedValue06,
                                     sourceEstimatedValue07,
                                     sourceEstimatedValue08,
                                     sourceEstimatedValue09,
                                     sourceEstimatedValue10,
                                     sourceEstimatedValue11,
                                     sourceEstimatedValue12,
                                     sourceEstimatedValue13,
                                     sourceEstimatedValue14,
                                     sourceEstimatedValue15,
                                     sourceEstimatedValue16,
                                     sourceEstimatedValue17,
                                     sourceEstimatedValue18,
                                     sourceEstimatedValue19,
                                     sourceEstimatedValue20,
                                     sourceActualValue01,
                                     sourceActualValue02,
                                     sourceActualValue03,
                                     sourceActualValue04,
                                     sourceActualValue05,
                                     sourceActualValue06,
                                     sourceActualValue07,
                                     sourceActualValue08,
                                     sourceActualValue09,
                                     sourceActualValue10,
                                     sourceActualValue11,
                                     sourceActualValue12,
                                     sourceActualValue13,
                                     sourceActualValue14,
                                     sourceActualValue15,
                                     sourceActualValue16,
                                     sourceActualValue17,
                                     sourceActualValue18,
                                     sourceActualValue19,
                                     sourceActualValue20,
                                     targetFund01,
                                     targetFund02,
                                     targetFund03,
                                     targetFund04,
                                     targetFund05,
                                     targetFund06,
                                     targetFund07,
                                     targetFund08,
                                     targetFund09,
                                     targetFund10,
                                     targetFundType01,
                                     targetFundType02,
                                     targetFundType03,
                                     targetFundType04,
                                     targetFundType05,
                                     targetFundType06,
                                     targetFundType07,
                                     targetFundType08,
                                     targetFundType09,
                                     targetFundType10,
                                     targetFundCurrency01,
                                     targetFundCurrency02,
                                     targetFundCurrency03,
                                     targetFundCurrency04,
                                     targetFundCurrency05,
                                     targetFundCurrency06,
                                     targetFundCurrency07,
                                     targetFundCurrency08,
                                     targetFundCurrency09,
                                     targetFundCurrency10,
                                     targetPercent01,
                                     targetPercent02,
                                     targetPercent03,
                                     targetPercent04,
                                     targetPercent05,
                                     targetPercent06,
                                     targetPercent07,
                                     targetPercent08,
                                     targetPercent09,
                                     targetPercent10,
                                     effdate,
                                     overrideFee,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		planSuffix.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		tranno.clear();
  		percentAmountInd.clear();
  		sourceFund01.clear();
  		sourceFund02.clear();
  		sourceFund03.clear();
  		sourceFund04.clear();
  		sourceFund05.clear();
  		sourceFund06.clear();
  		sourceFund07.clear();
  		sourceFund08.clear();
  		sourceFund09.clear();
  		sourceFund10.clear();
  		sourceFund11.clear();
  		sourceFund12.clear();
  		sourceFund13.clear();
  		sourceFund14.clear();
  		sourceFund15.clear();
  		sourceFund16.clear();
  		sourceFund17.clear();
  		sourceFund18.clear();
  		sourceFund19.clear();
  		sourceFund20.clear();
  		sourceFundType01.clear();
  		sourceFundType02.clear();
  		sourceFundType03.clear();
  		sourceFundType04.clear();
  		sourceFundType05.clear();
  		sourceFundType06.clear();
  		sourceFundType07.clear();
  		sourceFundType08.clear();
  		sourceFundType09.clear();
  		sourceFundType10.clear();
  		sourceFundType11.clear();
  		sourceFundType12.clear();
  		sourceFundType13.clear();
  		sourceFundType14.clear();
  		sourceFundType15.clear();
  		sourceFundType16.clear();
  		sourceFundType17.clear();
  		sourceFundType18.clear();
  		sourceFundType19.clear();
  		sourceFundType20.clear();
  		sourceFundCurrency01.clear();
  		sourceFundCurrency02.clear();
  		sourceFundCurrency03.clear();
  		sourceFundCurrency04.clear();
  		sourceFundCurrency05.clear();
  		sourceFundCurrency06.clear();
  		sourceFundCurrency07.clear();
  		sourceFundCurrency08.clear();
  		sourceFundCurrency09.clear();
  		sourceFundCurrency10.clear();
  		sourceFundCurrency11.clear();
  		sourceFundCurrency12.clear();
  		sourceFundCurrency13.clear();
  		sourceFundCurrency14.clear();
  		sourceFundCurrency15.clear();
  		sourceFundCurrency16.clear();
  		sourceFundCurrency17.clear();
  		sourceFundCurrency18.clear();
  		sourceFundCurrency19.clear();
  		sourceFundCurrency20.clear();
  		sourcePercentAmount01.clear();
  		sourcePercentAmount02.clear();
  		sourcePercentAmount03.clear();
  		sourcePercentAmount04.clear();
  		sourcePercentAmount05.clear();
  		sourcePercentAmount06.clear();
  		sourcePercentAmount07.clear();
  		sourcePercentAmount08.clear();
  		sourcePercentAmount09.clear();
  		sourcePercentAmount10.clear();
  		sourcePercentAmount11.clear();
  		sourcePercentAmount12.clear();
  		sourcePercentAmount13.clear();
  		sourcePercentAmount14.clear();
  		sourcePercentAmount15.clear();
  		sourcePercentAmount16.clear();
  		sourcePercentAmount17.clear();
  		sourcePercentAmount18.clear();
  		sourcePercentAmount19.clear();
  		sourcePercentAmount20.clear();
  		sourceEstimatedValue01.clear();
  		sourceEstimatedValue02.clear();
  		sourceEstimatedValue03.clear();
  		sourceEstimatedValue04.clear();
  		sourceEstimatedValue05.clear();
  		sourceEstimatedValue06.clear();
  		sourceEstimatedValue07.clear();
  		sourceEstimatedValue08.clear();
  		sourceEstimatedValue09.clear();
  		sourceEstimatedValue10.clear();
  		sourceEstimatedValue11.clear();
  		sourceEstimatedValue12.clear();
  		sourceEstimatedValue13.clear();
  		sourceEstimatedValue14.clear();
  		sourceEstimatedValue15.clear();
  		sourceEstimatedValue16.clear();
  		sourceEstimatedValue17.clear();
  		sourceEstimatedValue18.clear();
  		sourceEstimatedValue19.clear();
  		sourceEstimatedValue20.clear();
  		sourceActualValue01.clear();
  		sourceActualValue02.clear();
  		sourceActualValue03.clear();
  		sourceActualValue04.clear();
  		sourceActualValue05.clear();
  		sourceActualValue06.clear();
  		sourceActualValue07.clear();
  		sourceActualValue08.clear();
  		sourceActualValue09.clear();
  		sourceActualValue10.clear();
  		sourceActualValue11.clear();
  		sourceActualValue12.clear();
  		sourceActualValue13.clear();
  		sourceActualValue14.clear();
  		sourceActualValue15.clear();
  		sourceActualValue16.clear();
  		sourceActualValue17.clear();
  		sourceActualValue18.clear();
  		sourceActualValue19.clear();
  		sourceActualValue20.clear();
  		targetFund01.clear();
  		targetFund02.clear();
  		targetFund03.clear();
  		targetFund04.clear();
  		targetFund05.clear();
  		targetFund06.clear();
  		targetFund07.clear();
  		targetFund08.clear();
  		targetFund09.clear();
  		targetFund10.clear();
  		targetFundType01.clear();
  		targetFundType02.clear();
  		targetFundType03.clear();
  		targetFundType04.clear();
  		targetFundType05.clear();
  		targetFundType06.clear();
  		targetFundType07.clear();
  		targetFundType08.clear();
  		targetFundType09.clear();
  		targetFundType10.clear();
  		targetFundCurrency01.clear();
  		targetFundCurrency02.clear();
  		targetFundCurrency03.clear();
  		targetFundCurrency04.clear();
  		targetFundCurrency05.clear();
  		targetFundCurrency06.clear();
  		targetFundCurrency07.clear();
  		targetFundCurrency08.clear();
  		targetFundCurrency09.clear();
  		targetFundCurrency10.clear();
  		targetPercent01.clear();
  		targetPercent02.clear();
  		targetPercent03.clear();
  		targetPercent04.clear();
  		targetPercent05.clear();
  		targetPercent06.clear();
  		targetPercent07.clear();
  		targetPercent08.clear();
  		targetPercent09.clear();
  		targetPercent10.clear();
  		effdate.clear();
  		overrideFee.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUswdrec() {
  		return uswdrec;
	}

	public FixedLengthStringData getUswdpfRecord() {
  		return uswdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUswdrec(what);
	}

	public void setUswdrec(Object what) {
  		this.uswdrec.set(what);
	}

	public void setUswdpfRecord(Object what) {
  		this.uswdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(uswdrec.getLength());
		result.set(uswdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}