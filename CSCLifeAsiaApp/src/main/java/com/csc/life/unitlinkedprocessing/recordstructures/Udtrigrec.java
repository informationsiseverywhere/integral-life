package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:21
 * Description:
 * Copybook name: UDTRIGREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Udtrigrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData udtrigrecRec = new FixedLengthStringData(104);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(udtrigrecRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(udtrigrecRec, 5);
  	public FixedLengthStringData mode = new FixedLengthStringData(5).isAPartOf(udtrigrecRec, 9);
  	public FixedLengthStringData callingProg = new FixedLengthStringData(10).isAPartOf(udtrigrecRec, 14);
  	public FixedLengthStringData fsuco = new FixedLengthStringData(1).isAPartOf(udtrigrecRec, 24);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(udtrigrecRec, 25);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(udtrigrecRec, 26).setUnsigned();
  	public FixedLengthStringData batchkey = new FixedLengthStringData(19).isAPartOf(udtrigrecRec, 32);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batchkey, 0);
  	public FixedLengthStringData prefix = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData actyear = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData actmonth = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData trcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(udtrigrecRec, 51);
  	public FixedLengthStringData utrnKey = new FixedLengthStringData(28).isAPartOf(udtrigrecRec, 56);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(utrnKey, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(utrnKey, 1);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(utrnKey, 9);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(utrnKey, 11);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(utrnKey, 13);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnKey, 15);
  	public FixedLengthStringData unitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnKey, 18);
  	public FixedLengthStringData unitType = new FixedLengthStringData(1).isAPartOf(utrnKey, 22);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(utrnKey, 23);
  	public PackedDecimalData procSeqNo = new PackedDecimalData(3, 0).isAPartOf(utrnKey, 26);
  	public FixedLengthStringData triggerKey = new FixedLengthStringData(20).isAPartOf(udtrigrecRec, 84);
  	public FixedLengthStringData tk1Key = new FixedLengthStringData(20).isAPartOf(triggerKey, 0, REDEFINE);
  	public FixedLengthStringData tk1Chdrcoy = new FixedLengthStringData(1).isAPartOf(tk1Key, 0);
  	public FixedLengthStringData tk1Chdrnum = new FixedLengthStringData(8).isAPartOf(tk1Key, 1);
  	public PackedDecimalData tk1Suffix = new PackedDecimalData(4, 0).isAPartOf(tk1Key, 9);
  	public PackedDecimalData tk1Tranno = new PackedDecimalData(5, 0).isAPartOf(tk1Key, 12);
  	public PackedDecimalData tk1Seqnumb = new PackedDecimalData(3, 0).isAPartOf(tk1Key, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(tk1Key, 17, FILLER);
  	public FixedLengthStringData tk2Key = new FixedLengthStringData(20).isAPartOf(triggerKey, 0, REDEFINE);
  	public FixedLengthStringData tk2Chdrcoy = new FixedLengthStringData(1).isAPartOf(tk2Key, 0);
  	public FixedLengthStringData tk2Chdrnum = new FixedLengthStringData(8).isAPartOf(tk2Key, 1);
  	public PackedDecimalData tk2Suffix = new PackedDecimalData(4, 0).isAPartOf(tk2Key, 9);
  	public PackedDecimalData tk2Tranno = new PackedDecimalData(5, 0).isAPartOf(tk2Key, 12);
  	public FixedLengthStringData tk2Coverage = new FixedLengthStringData(2).isAPartOf(tk2Key, 15);
  	public FixedLengthStringData tk2Rider = new FixedLengthStringData(2).isAPartOf(tk2Key, 17);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(tk2Key, 19, FILLER);
  	public FixedLengthStringData tk3Key = new FixedLengthStringData(20).isAPartOf(triggerKey, 0, REDEFINE);
  	public FixedLengthStringData tk3Chdrcoy = new FixedLengthStringData(1).isAPartOf(tk3Key, 0);
  	public FixedLengthStringData tk3Chdrnum = new FixedLengthStringData(8).isAPartOf(tk3Key, 1);
  	public FixedLengthStringData tk3Life = new FixedLengthStringData(2).isAPartOf(tk3Key, 9);
  	public FixedLengthStringData tk3Coverage = new FixedLengthStringData(2).isAPartOf(tk3Key, 11);
  	public FixedLengthStringData tk3Rider = new FixedLengthStringData(2).isAPartOf(tk3Key, 13);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(5).isAPartOf(tk3Key, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(udtrigrecRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		udtrigrecRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}