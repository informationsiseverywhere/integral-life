package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:28
 * Description:
 * Copybook name: COVTBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtbrkKey = new FixedLengthStringData(64).isAPartOf(covtbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(covtbrkKey, 0);
  	public FixedLengthStringData covtbrkChdrnum = new FixedLengthStringData(8).isAPartOf(covtbrkKey, 1);
  	public PackedDecimalData covtbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covtbrkKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(covtbrkKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}