/*
 * File: T5536pt.java
 * Date: 30 August 2009 2:21:43
 * Author: Quipoz Limited
 * 
 * Class transformed from T5536PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5536.
*
*
*****************************************************************
* </pre>
*/
public class T5536pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Actual Premium Allocation Basis                S5536");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 14);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 24, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 29, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(74);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Frequency:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 18);
	private FixedLengthStringData filler12 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 20, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 34, FILLER).init("Frequency:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 45);
	private FixedLengthStringData filler14 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 47, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 61, FILLER).init("Frequency:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 72);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(71);
	private FixedLengthStringData filler16 = new FixedLengthStringData(71).isAPartOf(wsaaPrtLine005, 0, FILLER).init("   To    %%             To    %      %             To    %      %");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(72);
	private FixedLengthStringData filler17 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(65).isAPartOf(wsaaPrtLine006, 7, FILLER).init("Units  Init           Units  Init                Units  Init");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(66);
	private FixedLengthStringData filler19 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(59).isAPartOf(wsaaPrtLine007, 7, FILLER).init("/Prem           /Prem                      /Prem");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(74);
	private FixedLengthStringData filler57 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler58 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 7, FILLER).init("Frequency:");
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 18);
	private FixedLengthStringData filler59 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private FixedLengthStringData filler60 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 34, FILLER).init("Frequency:");
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 45);
	private FixedLengthStringData filler61 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private FixedLengthStringData filler62 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 61, FILLER).init("Frequency:");
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 72);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(73);
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(73);
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(73);
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(73);
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 6).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 12, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 13).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 28).setPattern("ZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 33).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 55).setPattern("ZZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 60).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(54);
	private FixedLengthStringData filler99 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler100 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine017, 17, FILLER).init("Over Target Allocation Item Key");
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 50);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5536rec t5536rec = new T5536rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5536pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5536rec.t5536Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5536rec.billfreq01);
		fieldNo008.set(t5536rec.billfreq02);
		fieldNo009.set(t5536rec.billfreq03);
		fieldNo010.set(t5536rec.maxPerioda01);
		fieldNo011.set(t5536rec.pcUnitsa01);
		fieldNo012.set(t5536rec.pcInitUnitsa01);
		fieldNo013.set(t5536rec.maxPeriodb01);
		fieldNo014.set(t5536rec.pcUnitsb01);
		fieldNo015.set(t5536rec.pcInitUnitsb01);
		fieldNo016.set(t5536rec.maxPeriodc01);
		fieldNo017.set(t5536rec.pcUnitsc01);
		fieldNo018.set(t5536rec.pcInitUnitsc01);
		fieldNo019.set(t5536rec.maxPerioda02);
		fieldNo020.set(t5536rec.pcUnitsa02);
		fieldNo021.set(t5536rec.pcInitUnitsa02);
		fieldNo022.set(t5536rec.maxPeriodb02);
		fieldNo023.set(t5536rec.pcUnitsb02);
		fieldNo024.set(t5536rec.pcInitUnitsb02);
		fieldNo025.set(t5536rec.maxPeriodc02);
		fieldNo026.set(t5536rec.pcUnitsc02);
		fieldNo027.set(t5536rec.pcInitUnitsc02);
		fieldNo028.set(t5536rec.maxPerioda03);
		fieldNo029.set(t5536rec.pcUnitsa03);
		fieldNo030.set(t5536rec.pcInitUnitsa03);
		fieldNo031.set(t5536rec.maxPeriodb03);
		fieldNo032.set(t5536rec.pcUnitsb03);
		fieldNo033.set(t5536rec.pcInitUnitsb03);
		fieldNo034.set(t5536rec.maxPeriodc03);
		fieldNo035.set(t5536rec.pcUnitsc03);
		fieldNo036.set(t5536rec.pcInitUnitsc03);
		fieldNo037.set(t5536rec.maxPerioda04);
		fieldNo038.set(t5536rec.pcUnitsa04);
		fieldNo039.set(t5536rec.pcInitUnitsa04);
		fieldNo040.set(t5536rec.maxPeriodb04);
		fieldNo041.set(t5536rec.pcUnitsb04);
		fieldNo042.set(t5536rec.pcInitUnitsb04);
		fieldNo043.set(t5536rec.maxPeriodc04);
		fieldNo044.set(t5536rec.pcUnitsc04);
		fieldNo045.set(t5536rec.pcInitUnitsc04);
		fieldNo046.set(t5536rec.billfreq04);
		fieldNo047.set(t5536rec.billfreq05);
		fieldNo048.set(t5536rec.billfreq06);
		fieldNo049.set(t5536rec.maxPeriodd01);
		fieldNo050.set(t5536rec.pcUnitsd01);
		fieldNo051.set(t5536rec.pcInitUnitsd01);
		fieldNo052.set(t5536rec.maxPeriode01);
		fieldNo053.set(t5536rec.pcUnitse01);
		fieldNo054.set(t5536rec.pcInitUnitse01);
		fieldNo055.set(t5536rec.maxPeriodf01);
		fieldNo056.set(t5536rec.pcUnitsf01);
		fieldNo057.set(t5536rec.pcInitUnitsf01);
		fieldNo058.set(t5536rec.maxPeriodd02);
		fieldNo059.set(t5536rec.pcUnitsd02);
		fieldNo060.set(t5536rec.pcInitUnitsd02);
		fieldNo061.set(t5536rec.maxPeriode02);
		fieldNo062.set(t5536rec.pcUnitse02);
		fieldNo063.set(t5536rec.pcInitUnitse02);
		fieldNo064.set(t5536rec.maxPeriodf02);
		fieldNo065.set(t5536rec.pcUnitsf02);
		fieldNo066.set(t5536rec.pcInitUnitsf02);
		fieldNo067.set(t5536rec.maxPeriodd03);
		fieldNo068.set(t5536rec.pcUnitsd03);
		fieldNo069.set(t5536rec.pcInitUnitsd03);
		fieldNo070.set(t5536rec.maxPeriode03);
		fieldNo071.set(t5536rec.pcUnitse03);
		fieldNo072.set(t5536rec.pcInitUnitse03);
		fieldNo073.set(t5536rec.maxPeriodf03);
		fieldNo074.set(t5536rec.pcUnitsf03);
		fieldNo075.set(t5536rec.pcInitUnitsf03);
		fieldNo076.set(t5536rec.maxPeriodd04);
		fieldNo077.set(t5536rec.pcUnitsd04);
		fieldNo078.set(t5536rec.pcInitUnitsd04);
		fieldNo079.set(t5536rec.maxPeriode04);
		fieldNo080.set(t5536rec.pcUnitse04);
		fieldNo081.set(t5536rec.pcInitUnitse04);
		fieldNo082.set(t5536rec.maxPeriodf04);
		fieldNo083.set(t5536rec.pcUnitsf04);
		fieldNo084.set(t5536rec.pcInitUnitsf04);
		fieldNo085.set(t5536rec.yotalmth);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
