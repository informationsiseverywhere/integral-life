package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:44
 * Description:
 * Copybook name: USTJKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustjkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustjFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustjKey = new FixedLengthStringData(64).isAPartOf(ustjFileKey, 0, REDEFINE);
  	public PackedDecimalData ustjJobnoStmt = new PackedDecimalData(8, 0).isAPartOf(ustjKey, 0);
  	public FixedLengthStringData ustjChdrcoy = new FixedLengthStringData(1).isAPartOf(ustjKey, 5);
  	public FixedLengthStringData ustjChdrnum = new FixedLengthStringData(8).isAPartOf(ustjKey, 6);
  	public PackedDecimalData ustjEffdate = new PackedDecimalData(8, 0).isAPartOf(ustjKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(45).isAPartOf(ustjKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustjFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustjFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}