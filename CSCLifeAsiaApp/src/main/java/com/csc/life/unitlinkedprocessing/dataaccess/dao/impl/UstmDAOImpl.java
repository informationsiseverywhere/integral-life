package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UstmDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UstmDAOImpl extends BaseDAOImpl<UstmData> implements UstmDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(UstmDAOImpl.class);


	public Map<String, List<UstmData>> getUstmDetails(String stmtType, String company, Integer effDate, int extractSize, int partitionID){
		StringBuilder sqlstmt = new StringBuilder();	 
		sqlstmt.append("SELECT * FROM(SELECT U.UNIQUE_NUMBER, U.CHDRCOY,U.CHDRNUM, U.EFFDATE,U.USTMTYP, U.USTMNO,"); 
		sqlstmt.append(" U.STRPDATE, U.CNTCURR, C.COWNPFX, C.COWNCOY, C.COWNNUM, C.DESPCOY, C.DESPNUM, C.DESPPFX, ");
		sqlstmt.append(" FLOOR((ROW_NUMBER()OVER(ORDER BY U.CHDRCOY, U.CHDRNUM, U.EFFDATE)-1)/?) AS BATCHNUM");
		sqlstmt.append(" FROM USTM U INNER JOIN CHDRPF C ON U.CHDRCOY = C.CHDRCOY AND U.CHDRNUM = C.CHDRNUM");
		sqlstmt.append(" AND U.CHDRCOY = ? AND U.USTMTYP = ? AND U.EFFDATE <= ? ");
		sqlstmt.append(" ) MAIN WHERE BATCHNUM = ?");
		sqlstmt.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, EFFDATE ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sqlstmt.toString());
		ResultSet rs = null;
		;
		Map<String, List<UstmData>> ustmMap = new LinkedHashMap<String, List<UstmData>>();
		try {
			ps.setInt(1, extractSize);
			ps.setString(2, company );
			ps.setString(3,stmtType );
			ps.setInt(4, effDate);
			ps.setInt(5, partitionID);
			rs = executeQuery(ps);
			while(rs.next()){
				UstmData ustmData =  new UstmData();
				ustmData.setUniqueNumber(rs.getLong(1));
				ustmData.setChdrcoy(rs.getString(2));
				ustmData.setChdrnum(rs.getString(3));
				ustmData.setEffdate(rs.getInt(4));
				ustmData.setStmtType(rs.getString(5));
				ustmData.setUstmno(rs.getInt(6));
				ustmData.setStrpdate(rs.getLong(7));
				ustmData.setCntcurr(rs.getString(8));
				ustmData.setOwnerPfx(rs.getString(9));
				ustmData.setOwnerCoy(rs.getString(10));
				ustmData.setOwnerNum(rs.getString(11));
				ustmData.setDescCoy(rs.getString(12));
				ustmData.setDescNum(rs.getString(13));
				ustmData.setDescPfx(rs.getString(14));
				if (ustmMap.containsKey(ustmData.getChdrnum())) {
					ustmMap.get(ustmData.getChdrnum()).add(ustmData);
				} else {
					List<UstmData> ustmList = new ArrayList<UstmData>();
					ustmList.add(ustmData);
					ustmMap.put(ustmData.getChdrcoy().trim()+ ustmData.getChdrnum().trim()+ustmData.getUniqueNumber(), ustmList);
				}
			}


		}catch(SQLException e) {
			LOGGER.error("UstmPfDAOImpl:getUstmDetails() threw SQLException :", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return ustmMap;
	}


	public void updateUstmRecord(List<UstmData> ustmList) {
		if (ustmList != null && ustmList.size() > 0) {
			String SQL_USTM_UPDATE = "UPDATE USTMPF SET JCTLJNUMST=?, STRPDATE=?, CLNTPFX=?, CLNTCOY=?, CLNTNUM=?, CNTCURR=?, JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement ps = getPrepareStatement(SQL_USTM_UPDATE);
			try {
				for (UstmData ustm : ustmList) {
					ps.setLong(1,ustm.getJobnoStmt());
					ps.setLong(2,ustm.getStrpdate());
					ps.setString(3, ustm.getClntpfx());
					ps.setString(4, ustm.getClntcoy());
					ps.setString(5, ustm.getClntnum());
					ps.setString(6, ustm.getCntcurr());
					ps.setString(7, getJobnm());
					ps.setString(8, getUsrprf());
					ps.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
					ps.setLong(10, ustm.getUniqueNumber());					
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("UstmPfDAOImpl:updateUstmRecord() threw SQLException: ", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}

	}
	public Map<String, List<UstmData>> readUstjData(int scheduleNumber,String company,int extractSize, int partitionID) {
		StringBuilder sqlstmt = new StringBuilder();	 
		sqlstmt.append("SELECT * FROM (SELECT UNIQUE_NUMBER, JCTLJNUMST, CHDRCOY, CHDRNUM, EFFDATE, USTMTFLAG, TRANNO, BATCPFX, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, VALIDFLAG, CNTCURR, USTMNO, STRPDATE, CLNTCOY, CLNTNUM, USTMTYP, JOBNM, USRPRF, DATIME , FLOOR((ROW_NUMBER()OVER(ORDER BY CHDRCOY, CHDRNUM)-1)/?) AS BATCHNUM FROM USTJ  "
				+ "WHERE JCTLJNUMST = ? AND CHDRCOY = ? ) MAIN WHERE BATCHNUM = ? " ); 
		sqlstmt.append(" ORDER by JCTLJNUMST ASC, CHDRCOY ASC, CHDRNUM ASC, EFFDATE ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement ps = getPrepareStatement(sqlstmt.toString());
		ResultSet rs = null;
		List<UstmData> ustjList = new ArrayList();
		List<String> chdrnumList = new ArrayList();
		Map<String, List<UstmData>> ustmMap = new LinkedHashMap<String, List<UstmData>>();
		try {
			ps.setInt(1, extractSize);
			ps.setLong(2,scheduleNumber);
			ps.setString(3,company);
			ps.setInt(4, partitionID);
			LOGGER.info(sqlstmt.toString());
			LOGGER.info("Statement readUstj");
			rs = executeQuery(ps);
			while(rs.next()){
				UstmData ustmData =  new UstmData();
				ustmData.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				ustmData.setChdrcoy(rs.getString("CHDRCOY"));
				ustmData.setChdrnum(rs.getString("CHDRNUM"));
				ustmData.setEffdate(rs.getInt("EFFDATE"));
				ustmData.setStmtType(rs.getString("USTMTYP"));
				ustmData.setUstmno(rs.getInt("USTMNO"));
				ustmData.setStrpdate(rs.getLong("STRPDATE"));
				ustmData.setCntcurr(rs.getString("CNTCURR"));
				ustmData.setUnitStmtFlag(rs.getString("USTMTFLAG"));
				ustmData.setJobnoStmt(rs.getLong("JCTLJNUMST"));
				ustmData.setTranno(rs.getLong("TRANNO"));
				ustjList.add(ustmData);
				if (ustmMap.containsKey(ustmData.getChdrnum())) {
					ustmMap.get(ustmData.getChdrnum()).add(ustmData);
				} else {
					List<UstmData> ustmList = new ArrayList<UstmData>();
					ustmList.add(ustmData);
					ustmMap.put(ustmData.getChdrnum().trim(), ustmList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return ustmMap;

	}


	public void updateUstjData(List<UstmData> ustmList) {
		if (ustmList != null && ustmList.size() > 0) {
			String SQL_USTM_UPDATE = "UPDATE USTJ SET JCTLJNUMST=? , USTMNO = ?,USRPRF=? ,DATIME = ? WHERE UNIQUE_NUMBER=? ";



			PreparedStatement psChdrUpdate = getPrepareStatement(SQL_USTM_UPDATE);
			try {
				for (UstmData ustm : ustmList) {
					psChdrUpdate.setLong(1, ustm.getJobnoStmt());
					psChdrUpdate.setLong(2, ustm.getUstmno());
					psChdrUpdate.setString(3, getUsrprf());
					psChdrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
					psChdrUpdate.setLong(5, ustm.getUniqueNumber());					
					psChdrUpdate.addBatch();
				}
				psChdrUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateChdrRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psChdrUpdate, null);
			}
		}
	}


	public List<UstmData> getUstjData(UstmData ustmpf) {
		StringBuilder sqlstmt = new StringBuilder();	 
		sqlstmt.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,EFFDATE,USTMTYP,USTMNO,STRPDATE,CNTCURR,USTMTFLAG,JCTLJNUMST,TRANNO FROM USTJ WHERE JCTLJNUMST = ? AND CHDRCOY = ?  "); 
		PreparedStatement ps = getPrepareStatement(sqlstmt.toString());
		ResultSet rs = null;
		List<UstmData> ustjList = new ArrayList();
		try {
			ps.setLong(1, ustmpf.getJobnoStmt());
			ps.setString(2,ustmpf.getChdrcoy());

			rs = executeQuery(ps);
			while(rs.next()){
				UstmData ustmData =  new UstmData();
				ustmData.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				ustmData.setChdrcoy(rs.getString("CHDRCOY"));
				ustmData.setChdrnum(rs.getString("CHDRNUM"));
				ustmData.setEffdate(rs.getInt("EFFDATE"));
				ustmData.setStmtType(rs.getString("USTMTYP"));
				ustmData.setUstmno(rs.getInt("USTMNO"));
				ustmData.setStrpdate(rs.getLong("STRPDATE"));
				ustmData.setCntcurr(rs.getString("CNTCURR"));
				ustmData.setUnitStmtFlag(rs.getString("USTMTFLAG"));
				ustmData.setJobnoStmt(rs.getLong("JCTLJNUMST"));
				ustmData.setTranno(rs.getLong("TRANNO"));
				ustjList.add(ustmData);
			}
		} catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return ustjList;
	}

}
