/*
 * File: Th608pt.java
 * Date: 30 August 2009 2:36:54
 * Author: Quipoz Limited
 * 
 * Class transformed from TH608PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.Th608rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH608.
*
*
*****************************************************************
* </pre>
*/
public class Th608pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(71).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine001, 71, FILLER).init("SH608");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(50);
	private FixedLengthStringData filler7 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine003, 29, FILLER).init("1         2         3");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(51);
	private FixedLengthStringData filler9 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine004, 20, FILLER).init("1234567890123456789012345678901");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(68);
	private FixedLengthStringData filler11 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 7, FILLER).init("January");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine005, 20);
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 56, FILLER).init("WE = Weekend");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(51);
	private FixedLengthStringData filler15 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 7, FILLER).init("February");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine006, 20);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(68);
	private FixedLengthStringData filler17 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 7, FILLER).init("March");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine007, 20);
	private FixedLengthStringData filler19 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 57, FILLER).init("H = Holiday");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(51);
	private FixedLengthStringData filler21 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 7, FILLER).init("April");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine008, 20);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(69);
	private FixedLengthStringData filler23 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 7, FILLER).init("May");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine009, 20);
	private FixedLengthStringData filler25 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 57, FILLER).init("D = Half Day");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(51);
	private FixedLengthStringData filler27 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 7, FILLER).init("June");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine010, 20);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler29 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 7, FILLER).init("July");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine011, 20);
	private FixedLengthStringData filler31 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine011, 57, FILLER).init("T = Transaction Date");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(51);
	private FixedLengthStringData filler33 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler34 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 7, FILLER).init("August");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine012, 20);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(72);
	private FixedLengthStringData filler35 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler36 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 7, FILLER).init("September");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine013, 20);
	private FixedLengthStringData filler37 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 51, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 57, FILLER).init("X = Unavailable");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(51);
	private FixedLengthStringData filler39 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler40 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 7, FILLER).init("October");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine014, 20);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(67);
	private FixedLengthStringData filler41 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler42 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine015, 7, FILLER).init("November");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine015, 20);
	private FixedLengthStringData filler43 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 51, FILLER).init(SPACES);
	private FixedLengthStringData filler44 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 57, FILLER).init(". = Normal");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(72);
	private FixedLengthStringData filler45 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler46 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine016, 7, FILLER).init("December");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine016, 20);
	private FixedLengthStringData filler47 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 51, FILLER).init(SPACES);
	private FixedLengthStringData filler48 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 61, FILLER).init("Working Day");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th608rec th608rec = new Th608rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th608pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th608rec.th608Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(th608rec.daywh01);
		fieldNo006.set(th608rec.daywh02);
		fieldNo007.set(th608rec.daywh03);
		fieldNo008.set(th608rec.daywh04);
		fieldNo009.set(th608rec.daywh05);
		fieldNo010.set(th608rec.daywh06);
		fieldNo011.set(th608rec.daywh07);
		fieldNo012.set(th608rec.daywh08);
		fieldNo013.set(th608rec.daywh09);
		fieldNo014.set(th608rec.daywh10);
		fieldNo015.set(th608rec.daywh11);
		fieldNo016.set(th608rec.daywh12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
