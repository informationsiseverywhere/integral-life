package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br608TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br608DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br608TempDAOImpl extends BaseDAOImpl<Br608DTO> implements Br608TempDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Br608TempDAOImpl.class);
    
	private final static String TABLE_NAME = "BR608DATA";  //ILB-475
	
	private void deleteTempData() {
		String sqlStr = "DELETE FROM " + TABLE_NAME;
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	private void insertTempData() {
        StringBuilder sqlStr = new StringBuilder("INSERT INTO ");
        sqlStr.append(TABLE_NAME);
        sqlStr.append(" (COMPANY,VRTFUND,UNITYP,NOFUNTS)");
        sqlStr.append(" SELECT ");
        sqlStr.append("COMPANY,VRTFUND,UNITYP,NOFUNTS");
        sqlStr.append(" FROM UFNXPF");
        sqlStr.append(" ORDER BY COMPANY, VRTFUND, UNITYP");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

	}
	
	@Override
	public void buildTempData() {
		deleteTempData();
		insertTempData();
	}

	@Override
	public List<Br608DTO> findTempDataResult(int batchExtractSize, int batchID) {
        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.COMPANY, TE.VRTFUND, TE.UNITYP, TE.NOFUNTS)-1)/?) ROWNM, TE.* from BR608DATA TE)cc where cc.rownm=? "); //ILB-475
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		List<Br608DTO> dtoList = new LinkedList<Br608DTO>();
		ResultSet rs = null;
        try {
        	ps.setInt(1, batchExtractSize);
        	ps.setInt(2, batchID);
        	rs = ps.executeQuery();
            while (rs.next()) {
            	Br608DTO dto = new Br608DTO();
            	dto.setCompany(rs.getString("COMPANY"));
            	dto.setVrtfund(rs.getString("VRTFUND"));
            	dto.setUnityp(rs.getString("UNITYP"));
            	dto.setNofunts(rs.getBigDecimal("NOFUNTS"));
                dtoList.add(dto);
            }

        } catch (SQLException e) {
            LOGGER.error("findTempDataResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return dtoList;

	}

}