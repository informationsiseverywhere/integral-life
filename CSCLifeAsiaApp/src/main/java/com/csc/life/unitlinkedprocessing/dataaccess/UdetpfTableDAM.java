package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UdetpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:40
 * Class transformed from UDETPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UdetpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 258;
	public FixedLengthStringData udetrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData udetpfRecord = udetrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(udetrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(udetrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(udetrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(udetrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(udetrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(udetrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(udetrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(udetrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(udetrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(udetrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(udetrec);
	public PackedDecimalData jobnoPrice = DD.jctljnumpr.copy().isAPartOf(udetrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(udetrec);
	public FixedLengthStringData unitSubAccount = DD.unitsa.copy().isAPartOf(udetrec);
	public PackedDecimalData strpdate = DD.strpdate.copy().isAPartOf(udetrec);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(udetrec);
	public PackedDecimalData nofUnits = DD.nofunt.copy().isAPartOf(udetrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(udetrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(udetrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(udetrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(udetrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(udetrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(udetrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(udetrec);
	public PackedDecimalData priceDateUsed = DD.pricedt.copy().isAPartOf(udetrec);
	public PackedDecimalData moniesDate = DD.moniesdt.copy().isAPartOf(udetrec);
	public PackedDecimalData priceUsed = DD.priceused.copy().isAPartOf(udetrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(udetrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(udetrec);
	public PackedDecimalData nofDunits = DD.nofdunt.copy().isAPartOf(udetrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(udetrec);
	public PackedDecimalData unitBarePrice = DD.ubrepr.copy().isAPartOf(udetrec);
	public PackedDecimalData inciNum = DD.incinum.copy().isAPartOf(udetrec);
	public PackedDecimalData inciPerd = DD.inciperd.copy().isAPartOf(udetrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(udetrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(udetrec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(udetrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(udetrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(udetrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(udetrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(udetrec);
	public FixedLengthStringData genlcde = DD.genlcde.copy().isAPartOf(udetrec);
	public FixedLengthStringData allInd = DD.allind.copy().isAPartOf(udetrec);
	public FixedLengthStringData contractType = DD.contyp.copy().isAPartOf(udetrec);
	public FixedLengthStringData triggerModule = DD.triger.copy().isAPartOf(udetrec);
	public FixedLengthStringData triggerKey = DD.trigky.copy().isAPartOf(udetrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(udetrec);
	public FixedLengthStringData switchIndicator = DD.swchind.copy().isAPartOf(udetrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(udetrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(udetrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(udetrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UdetpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UdetpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UdetpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UdetpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UdetpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UdetpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UdetpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UDETPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"JCTLJNUMPR, " +
							"VRTFND, " +
							"UNITSA, " +
							"STRPDATE, " +
							"NDFIND, " +
							"NOFUNT, " +
							"UNITYP, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"PRICEDT, " +
							"MONIESDT, " +
							"PRICEUSED, " +
							"CRTABLE, " +
							"CNTCURR, " +
							"NOFDUNT, " +
							"FDBKIND, " +
							"UBREPR, " +
							"INCINUM, " +
							"INCIPERD, " +
							"USTMNO, " +
							"CNTAMNT, " +
							"FNDCURR, " +
							"FUNDAMNT, " +
							"FUNDRATE, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"GENLCDE, " +
							"ALLIND, " +
							"CONTYP, " +
							"TRIGER, " +
							"TRIGKY, " +
							"PRCSEQ, " +
							"SWCHIND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     jobnoPrice,
                                     unitVirtualFund,
                                     unitSubAccount,
                                     strpdate,
                                     nowDeferInd,
                                     nofUnits,
                                     unitType,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     priceDateUsed,
                                     moniesDate,
                                     priceUsed,
                                     crtable,
                                     cntcurr,
                                     nofDunits,
                                     feedbackInd,
                                     unitBarePrice,
                                     inciNum,
                                     inciPerd,
                                     ustmno,
                                     contractAmount,
                                     fundCurrency,
                                     fundAmount,
                                     fundRate,
                                     sacscode,
                                     sacstyp,
                                     genlcde,
                                     allInd,
                                     contractType,
                                     triggerModule,
                                     triggerKey,
                                     procSeqNo,
                                     switchIndicator,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		jobnoPrice.clear();
  		unitVirtualFund.clear();
  		unitSubAccount.clear();
  		strpdate.clear();
  		nowDeferInd.clear();
  		nofUnits.clear();
  		unitType.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		priceDateUsed.clear();
  		moniesDate.clear();
  		priceUsed.clear();
  		crtable.clear();
  		cntcurr.clear();
  		nofDunits.clear();
  		feedbackInd.clear();
  		unitBarePrice.clear();
  		inciNum.clear();
  		inciPerd.clear();
  		ustmno.clear();
  		contractAmount.clear();
  		fundCurrency.clear();
  		fundAmount.clear();
  		fundRate.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		genlcde.clear();
  		allInd.clear();
  		contractType.clear();
  		triggerModule.clear();
  		triggerKey.clear();
  		procSeqNo.clear();
  		switchIndicator.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUdetrec() {
  		return udetrec;
	}

	public FixedLengthStringData getUdetpfRecord() {
  		return udetpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUdetrec(what);
	}

	public void setUdetrec(Object what) {
  		this.udetrec.set(what);
	}

	public void setUdetpfRecord(Object what) {
  		this.udetpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(udetrec.getLength());
		result.set(udetrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}