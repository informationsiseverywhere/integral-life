package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;

public interface ZutrpfDAO {
	
	public void insertZutrpfRecord(Zutrpf zutrpf);

	public List<Zutrpf> readReservePricing(Zutrpf zutrpf);
}
