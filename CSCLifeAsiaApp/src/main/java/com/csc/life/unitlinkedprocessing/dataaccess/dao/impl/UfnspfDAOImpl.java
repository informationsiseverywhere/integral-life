package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UfnspfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ufnspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UfnspfDAOImpl extends BaseDAOImpl<Ufnspf> implements UfnspfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(UfnspfDAOImpl.class);
	public void insertUfnspfRecord(List<Ufnspf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO Ufnspf(company,vrtfund,unityp,nofunts,jobno,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		
		try {
			for (Ufnspf u : urList) {
				int i = 1;
				ps.setString(i++, u.getCompany());
				ps.setString(i++, u.getVrtfund());
				ps.setString(i++, u.getUnityp());
				ps.setBigDecimal(i++, u.getNofunts());
				ps.setInt(i++, u.getJobno());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertUfnspfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
}