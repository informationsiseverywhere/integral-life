/*
 * File: T6646pt.java
 * Date: 30 August 2009 2:28:41
 * Author: Quipoz Limited
 * 
 * Class transformed from T6646PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6646.
*
*
*****************************************************************
* </pre>
*/
public class T6646pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Discount Factors - Whole of Life               S6646");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(70);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 10, FILLER).init(" Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 18);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 23, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 31);
	private FixedLengthStringData filler6 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 40);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(76);
	private FixedLengthStringData filler7 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine003, 12, FILLER).init("1      2      3     4      5      6      7      8      9     10");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler9 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" 0");
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine004, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(76);
	private FixedLengthStringData filler19 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 0, FILLER).init("10");
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(76);
	private FixedLengthStringData filler29 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 0, FILLER).init("20");
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(76);
	private FixedLengthStringData filler39 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init("30");
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(76);
	private FixedLengthStringData filler49 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init("40");
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(76);
	private FixedLengthStringData filler59 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 0, FILLER).init("50");
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(76);
	private FixedLengthStringData filler69 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init("60");
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(76);
	private FixedLengthStringData filler79 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 0, FILLER).init("70");
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine011, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(76);
	private FixedLengthStringData filler89 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 0, FILLER).init("80");
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine012, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(69);
	private FixedLengthStringData filler99 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 0, FILLER).init("90");
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine013, 64).setPattern("ZZZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6646rec t6646rec = new T6646rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6646pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6646rec.t6646Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t6646rec.dfact01);
		fieldNo006.set(t6646rec.dfact02);
		fieldNo007.set(t6646rec.dfact03);
		fieldNo008.set(t6646rec.dfact04);
		fieldNo009.set(t6646rec.dfact05);
		fieldNo010.set(t6646rec.dfact06);
		fieldNo011.set(t6646rec.dfact07);
		fieldNo012.set(t6646rec.dfact08);
		fieldNo013.set(t6646rec.dfact09);
		fieldNo014.set(t6646rec.dfact10);
		fieldNo015.set(t6646rec.dfact11);
		fieldNo016.set(t6646rec.dfact12);
		fieldNo017.set(t6646rec.dfact13);
		fieldNo018.set(t6646rec.dfact14);
		fieldNo019.set(t6646rec.dfact15);
		fieldNo020.set(t6646rec.dfact16);
		fieldNo021.set(t6646rec.dfact17);
		fieldNo022.set(t6646rec.dfact18);
		fieldNo023.set(t6646rec.dfact19);
		fieldNo024.set(t6646rec.dfact20);
		fieldNo025.set(t6646rec.dfact21);
		fieldNo026.set(t6646rec.dfact22);
		fieldNo027.set(t6646rec.dfact23);
		fieldNo028.set(t6646rec.dfact24);
		fieldNo029.set(t6646rec.dfact25);
		fieldNo030.set(t6646rec.dfact26);
		fieldNo031.set(t6646rec.dfact27);
		fieldNo032.set(t6646rec.dfact28);
		fieldNo033.set(t6646rec.dfact29);
		fieldNo034.set(t6646rec.dfact30);
		fieldNo035.set(t6646rec.dfact31);
		fieldNo036.set(t6646rec.dfact32);
		fieldNo037.set(t6646rec.dfact33);
		fieldNo038.set(t6646rec.dfact34);
		fieldNo039.set(t6646rec.dfact35);
		fieldNo040.set(t6646rec.dfact36);
		fieldNo041.set(t6646rec.dfact37);
		fieldNo042.set(t6646rec.dfact38);
		fieldNo043.set(t6646rec.dfact39);
		fieldNo044.set(t6646rec.dfact40);
		fieldNo045.set(t6646rec.dfact41);
		fieldNo046.set(t6646rec.dfact42);
		fieldNo047.set(t6646rec.dfact43);
		fieldNo048.set(t6646rec.dfact44);
		fieldNo049.set(t6646rec.dfact45);
		fieldNo050.set(t6646rec.dfact46);
		fieldNo051.set(t6646rec.dfact47);
		fieldNo052.set(t6646rec.dfact48);
		fieldNo053.set(t6646rec.dfact49);
		fieldNo054.set(t6646rec.dfact50);
		fieldNo055.set(t6646rec.dfact51);
		fieldNo056.set(t6646rec.dfact52);
		fieldNo057.set(t6646rec.dfact53);
		fieldNo058.set(t6646rec.dfact54);
		fieldNo059.set(t6646rec.dfact55);
		fieldNo060.set(t6646rec.dfact56);
		fieldNo061.set(t6646rec.dfact57);
		fieldNo062.set(t6646rec.dfact58);
		fieldNo063.set(t6646rec.dfact59);
		fieldNo064.set(t6646rec.dfact60);
		fieldNo065.set(t6646rec.dfact61);
		fieldNo066.set(t6646rec.dfact62);
		fieldNo067.set(t6646rec.dfact63);
		fieldNo068.set(t6646rec.dfact64);
		fieldNo069.set(t6646rec.dfact65);
		fieldNo070.set(t6646rec.dfact66);
		fieldNo071.set(t6646rec.dfact67);
		fieldNo072.set(t6646rec.dfact68);
		fieldNo073.set(t6646rec.dfact69);
		fieldNo074.set(t6646rec.dfact70);
		fieldNo075.set(t6646rec.dfact71);
		fieldNo076.set(t6646rec.dfact72);
		fieldNo077.set(t6646rec.dfact73);
		fieldNo078.set(t6646rec.dfact74);
		fieldNo079.set(t6646rec.dfact75);
		fieldNo080.set(t6646rec.dfact76);
		fieldNo081.set(t6646rec.dfact77);
		fieldNo082.set(t6646rec.dfact78);
		fieldNo083.set(t6646rec.dfact79);
		fieldNo084.set(t6646rec.dfact80);
		fieldNo085.set(t6646rec.dfact81);
		fieldNo086.set(t6646rec.dfact82);
		fieldNo087.set(t6646rec.dfact83);
		fieldNo088.set(t6646rec.dfact84);
		fieldNo089.set(t6646rec.dfact85);
		fieldNo090.set(t6646rec.dfact86);
		fieldNo091.set(t6646rec.dfact87);
		fieldNo092.set(t6646rec.dfact88);
		fieldNo093.set(t6646rec.dfact89);
		fieldNo094.set(t6646rec.dfact90);
		fieldNo095.set(t6646rec.dfact91);
		fieldNo096.set(t6646rec.dfact92);
		fieldNo097.set(t6646rec.dfact93);
		fieldNo098.set(t6646rec.dfact94);
		fieldNo099.set(t6646rec.dfact95);
		fieldNo100.set(t6646rec.dfact96);
		fieldNo101.set(t6646rec.dfact97);
		fieldNo102.set(t6646rec.dfact98);
		fieldNo103.set(t6646rec.dfact99);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
