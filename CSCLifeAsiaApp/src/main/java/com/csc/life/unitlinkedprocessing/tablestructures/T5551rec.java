package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:50
 * Description:
 * Copybook name: T5551REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5551rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5551Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(24).isAPartOf(t5551Rec, 0);
  	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(8, 3, 0, ageIssageFrms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData ageIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData ageIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData ageIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData ageIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData ageIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData ageIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData ageIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(24).isAPartOf(t5551Rec, 24);
  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(8, 3, 0, ageIssageTos, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public FixedLengthStringData alfnds = new FixedLengthStringData(4).isAPartOf(t5551Rec, 48);
  	public FixedLengthStringData eaage = new FixedLengthStringData(1).isAPartOf(t5551Rec, 52);
  	public FixedLengthStringData liencds = new FixedLengthStringData(12).isAPartOf(t5551Rec, 53);
  	public FixedLengthStringData[] liencd = FLSArrayPartOfStructure(6, 2, liencds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(liencds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData liencd01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData liencd02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData liencd03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData liencd04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData liencd05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData liencd06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public ZonedDecimalData maxLumpSum = new ZonedDecimalData(15, 0).isAPartOf(t5551Rec, 65);
  	public ZonedDecimalData minLumpSum = new ZonedDecimalData(15, 0).isAPartOf(t5551Rec, 80);
  	public FixedLengthStringData maturityAgeTos = new FixedLengthStringData(24).isAPartOf(t5551Rec, 95);
  	public ZonedDecimalData[] maturityAgeTo = ZDArrayPartOfStructure(8, 3, 0, maturityAgeTos, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(maturityAgeTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maturityAgeTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData maturityAgeTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData maturityAgeTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData maturityAgeTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData maturityAgeTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData maturityAgeTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData maturityAgeTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
  	public ZonedDecimalData maturityAgeTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
  	public FixedLengthStringData maturityAgeFroms = new FixedLengthStringData(24).isAPartOf(t5551Rec, 119);
  	public ZonedDecimalData[] maturityAgeFrom = ZDArrayPartOfStructure(8, 3, 0, maturityAgeFroms, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(maturityAgeFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maturityAgeFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData maturityAgeFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 3);
  	public ZonedDecimalData maturityAgeFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 6);
  	public ZonedDecimalData maturityAgeFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 9);
  	public ZonedDecimalData maturityAgeFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 12);
  	public ZonedDecimalData maturityAgeFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 15);
  	public ZonedDecimalData maturityAgeFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 18);
  	public ZonedDecimalData maturityAgeFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 21);
  	public FixedLengthStringData maturityTermFroms = new FixedLengthStringData(24).isAPartOf(t5551Rec, 143);
  	public ZonedDecimalData[] maturityTermFrom = ZDArrayPartOfStructure(8, 3, 0, maturityTermFroms, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(maturityTermFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maturityTermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData maturityTermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 3);
  	public ZonedDecimalData maturityTermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 6);
  	public ZonedDecimalData maturityTermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 9);
  	public ZonedDecimalData maturityTermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 12);
  	public ZonedDecimalData maturityTermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 15);
  	public ZonedDecimalData maturityTermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 18);
  	public ZonedDecimalData maturityTermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 21);
  	public FixedLengthStringData maturityTermTos = new FixedLengthStringData(24).isAPartOf(t5551Rec, 167);
  	public ZonedDecimalData[] maturityTermTo = ZDArrayPartOfStructure(8, 3, 0, maturityTermTos, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(maturityTermTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maturityTermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 0);
  	public ZonedDecimalData maturityTermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 3);
  	public ZonedDecimalData maturityTermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 6);
  	public ZonedDecimalData maturityTermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 9);
  	public ZonedDecimalData maturityTermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 12);
  	public ZonedDecimalData maturityTermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 15);
  	public ZonedDecimalData maturityTermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 18);
  	public ZonedDecimalData maturityTermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 21);
  	public FixedLengthStringData mortclss = new FixedLengthStringData(6).isAPartOf(t5551Rec, 191);
  	public FixedLengthStringData[] mortcls = FLSArrayPartOfStructure(6, 1, mortclss, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(6).isAPartOf(mortclss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mortcls01 = new FixedLengthStringData(1).isAPartOf(filler7, 0);
  	public FixedLengthStringData mortcls02 = new FixedLengthStringData(1).isAPartOf(filler7, 1);
  	public FixedLengthStringData mortcls03 = new FixedLengthStringData(1).isAPartOf(filler7, 2);
  	public FixedLengthStringData mortcls04 = new FixedLengthStringData(1).isAPartOf(filler7, 3);
  	public FixedLengthStringData mortcls05 = new FixedLengthStringData(1).isAPartOf(filler7, 4);
  	public FixedLengthStringData mortcls06 = new FixedLengthStringData(1).isAPartOf(filler7, 5);
  	public FixedLengthStringData premCessageFroms = new FixedLengthStringData(24).isAPartOf(t5551Rec, 197);
  	public ZonedDecimalData[] premCessageFrom = ZDArrayPartOfStructure(8, 3, 0, premCessageFroms, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(premCessageFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 0);
  	public ZonedDecimalData premCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 3);
  	public ZonedDecimalData premCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 6);
  	public ZonedDecimalData premCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 9);
  	public ZonedDecimalData premCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 12);
  	public ZonedDecimalData premCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 15);
  	public ZonedDecimalData premCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 18);
  	public ZonedDecimalData premCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 21);
  	public FixedLengthStringData premCessageTos = new FixedLengthStringData(24).isAPartOf(t5551Rec, 221);
  	public ZonedDecimalData[] premCessageTo = ZDArrayPartOfStructure(8, 3, 0, premCessageTos, 0);
  	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(premCessageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 0);
  	public ZonedDecimalData premCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 3);
  	public ZonedDecimalData premCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 6);
  	public ZonedDecimalData premCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 9);
  	public ZonedDecimalData premCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 12);
  	public ZonedDecimalData premCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 15);
  	public ZonedDecimalData premCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 18);
  	public ZonedDecimalData premCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 21);
  	public FixedLengthStringData premCesstermFroms = new FixedLengthStringData(24).isAPartOf(t5551Rec, 245);
  	public ZonedDecimalData[] premCesstermFrom = ZDArrayPartOfStructure(8, 3, 0, premCesstermFroms, 0);
  	public FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(premCesstermFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCesstermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 0);
  	public ZonedDecimalData premCesstermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 3);
  	public ZonedDecimalData premCesstermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 6);
  	public ZonedDecimalData premCesstermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 9);
  	public ZonedDecimalData premCesstermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 12);
  	public ZonedDecimalData premCesstermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 15);
  	public ZonedDecimalData premCesstermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 18);
  	public ZonedDecimalData premCesstermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 21);
  	public FixedLengthStringData premCesstermTos = new FixedLengthStringData(24).isAPartOf(t5551Rec, 269);
  	public ZonedDecimalData[] premCesstermTo = ZDArrayPartOfStructure(8, 3, 0, premCesstermTos, 0);
  	public FixedLengthStringData filler11 = new FixedLengthStringData(24).isAPartOf(premCesstermTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 0);
  	public ZonedDecimalData premCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 3);
  	public ZonedDecimalData premCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 6);
  	public ZonedDecimalData premCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 9);
  	public ZonedDecimalData premCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 12);
  	public ZonedDecimalData premCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 15);
  	public ZonedDecimalData premCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 18);
  	public ZonedDecimalData premCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 21);
  	public FixedLengthStringData rsvflg = new FixedLengthStringData(1).isAPartOf(t5551Rec, 293);
  	public FixedLengthStringData specind = new FixedLengthStringData(1).isAPartOf(t5551Rec, 294);
  	public ZonedDecimalData sumInsMax = new ZonedDecimalData(15, 0).isAPartOf(t5551Rec, 295);
  	public ZonedDecimalData sumInsMin = new ZonedDecimalData(15, 0).isAPartOf(t5551Rec, 310);
  	public FixedLengthStringData svcmeth = new FixedLengthStringData(4).isAPartOf(t5551Rec, 325);
  	public FixedLengthStringData termIssageFrms = new FixedLengthStringData(24).isAPartOf(t5551Rec, 329);
  	public ZonedDecimalData[] termIssageFrm = ZDArrayPartOfStructure(8, 3, 0, termIssageFrms, 0);
  	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(termIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData termIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 0);
  	public ZonedDecimalData termIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 3);
  	public ZonedDecimalData termIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 6);
  	public ZonedDecimalData termIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 9);
  	public ZonedDecimalData termIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 12);
  	public ZonedDecimalData termIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 15);
  	public ZonedDecimalData termIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 18);
  	public ZonedDecimalData termIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 21);
  	public FixedLengthStringData termIssageTos = new FixedLengthStringData(24).isAPartOf(t5551Rec, 353);
  	public ZonedDecimalData[] termIssageTo = ZDArrayPartOfStructure(8, 3, 0, termIssageTos, 0);
  	public FixedLengthStringData filler13 = new FixedLengthStringData(24).isAPartOf(termIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData termIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 0);
  	public ZonedDecimalData termIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 3);
  	public ZonedDecimalData termIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 6);
  	public ZonedDecimalData termIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 9);
  	public ZonedDecimalData termIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 12);
  	public ZonedDecimalData termIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 15);
  	public ZonedDecimalData termIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 18);
  	public ZonedDecimalData termIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 21);
  	public FixedLengthStringData filler14 = new FixedLengthStringData(123).isAPartOf(t5551Rec, 377, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5551Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5551Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}