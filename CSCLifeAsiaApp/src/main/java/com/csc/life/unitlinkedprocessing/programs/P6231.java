/*
 * File: P6231.java
 * Date: 30 August 2009 0:37:20
 * Author: Quipoz Limited
 * 
 * Class transformed from P6231.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.unitlinkedprocessing.dataaccess.VprcupeTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcuprTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S6231ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P6231 - Direct Input of Bid/Offer Price.
*
*  This program provides a means for the direct input of Bid
*  prices. The Bare price is set to the Bid price and the Offer
*  price is calculated online. The user may then amend the Offer
*  price on the screen.
*
*  The user can press CF9 to ask the system to calculate the
*  offer price and display it on the screen.
*  If CF9 is not pressed and ENTER instead, calculation will
*  be performed at updating stage.
*
*  Matching also available by entering 1, 2 ,3  or 4
*  characters. Consequently the program will display the fund
*  that matches these characters as the first fund in the
*  subfile.
*
*  It is called from the Unit Linked Entry submenu(P5412) by
*  means of selecting the appropriate option.
*
*  The program flow is as follows:-
*
*      Initialisation.
*
*      Read VPRCUPR and load subfile.
*      Display screen and read screen.
*
*      Validate changes until there is no error.
*      Default bare price to bid price.
*      If CF9 and no error occurs on the subfile record
*          Calculate the offer price and display on the screen.
*          Calculation only required if offer price has not been
*          entered.
*      If no error and ENTER is pressed
*          Accept the modifications and calculate the offer price
*          if the offer price field is empty.
*
*      Update VPRCUPR.
*
*      Return control to calling program.
*
*
*****************************************************************
* </pre>
*/
public class P6231 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6231");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	//private int wsaaSubfileSize = 200;//ILIFE-7549
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();
	private BinaryData wsaaRrn = new BinaryData(5, 0);
	private String wsaaNormError = "";

	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Validator wsaaRolu = new Validator(wsaaFunctionKey, "ROLU");
	private Validator wsaaCalc = new Validator(wsaaFunctionKey, "CALC");

	private FixedLengthStringData wsaaPartFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPartF1 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 0);
	private FixedLengthStringData wsaaPartF2 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 1);
	private FixedLengthStringData wsaaPartF3 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 2);
	private FixedLengthStringData wsaaPartF4 = new FixedLengthStringData(1).isAPartOf(wsaaPartFund, 3);

	private FixedLengthStringData wsaaItemFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaItemF1 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 0);
	private FixedLengthStringData wsaaItemF2 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 1);
	private FixedLengthStringData wsaaItemF3 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 2);
	private FixedLengthStringData wsaaItemF4 = new FixedLengthStringData(1).isAPartOf(wsaaItemFund, 3);
	private FixedLengthStringData wsaaSearchFund = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaTdayIntDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaTdayExtDate = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaAcumPerc = new ZonedDecimalData(8, 5);
	private ZonedDecimalData wsaaInitPerc = new ZonedDecimalData(8, 5);
	private ZonedDecimalData wsaaAcumOffer = new ZonedDecimalData(9, 5);
	private ZonedDecimalData wsaaInitOffer = new ZonedDecimalData(9, 5);
	private ZonedDecimalData wsaaTolerance = new ZonedDecimalData(8, 5).setUnsigned();
	private String wsaaToleranceFlag = "";
	private FixedLengthStringData wsaaPrevItem = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaLastItem = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private String f351 = "F351";
	private String g037 = "G037";
	private String t004 = "T004";
	private String h025 = "H025";
	private String h094 = "H094";
	private String h095 = "H095";
	private String vprcuprrec = "VPRCUPRREC";
	private String t5515 = "T5515";
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5515rec t5515rec = new T5515rec();
		/*Unit Price File (For checking tolerance)*/
	private VprcupeTableDAM vprcupeIO = new VprcupeTableDAM();
		/*Unit Price File*/
	private VprcuprTableDAM vprcuprIO = new VprcuprTableDAM();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S6231ScreenVars sv = ScreenProgram.getScreenVars( S6231ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		writeToSubfile1300, 
		nextRecord1400, 
		setMoreSign1400, 
		preExit, 
		validateSubfile2100, 
		updateSubfile2400, 
		rollUp2600, 
		writeToSubfile2700, 
		setMoreSign2750, 
		exit2090, 
		moreUpdateRec3200, 
		update3700, 
		exit3900, 
		call5110, 
		exit5190, 
		accumUnit7200, 
		exit7900, 
		exit8900, 
		checkInitUnit14200, 
		exit14900
	}

	public P6231() {
		super();
		screenVars = sv;
		new ScreenModel("S6231", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1100();
					startSubfile1200();
				}
				case writeToSubfile1300: {
					writeToSubfile1300();
				}
				case nextRecord1400: {
					nextRecord1400();
				}
				case setMoreSign1400: {
					setMoreSign1400();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaNormError = "Y";
		wsaaToleranceFlag = "N";
		sv.xtranno.set(0);
		sv.initBarePrice.set(0);
		sv.initBidPrice.set(0);
		sv.initOfferPrice.set(0);
		sv.accBarePrice.set(0);
		sv.accBidPrice.set(0);
		sv.accOfferPrice.set(0);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTdayIntDate.set(datcon1rec.intDate);
		sv.company.set(wsspcomn.company);
		sv.effdate.set(wsspuprc.uprcEffdate);
		sv.jobno.set(wsspuprc.uprcJobno);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6231", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		if (isEQ(wsspuprc.uprcVrtfnd,SPACES)) {
			wsaaSearchFund.set("N");
		}
		else {
			wsaaPartFund.set(wsspuprc.uprcVrtfnd);
			wsaaSearchFund.set("Y");
		}
	}

protected void startSubfile1200()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");
		/*LOAD-SUBFILE*/
		callItdmIo5100();
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1400);
		}
	}

protected void writeToSubfile1300()
	{
		if (isEQ(t5515rec.zfundtyp,"D")) {
			goTo(GotoLabel.nextRecord1400);
		}
		//ILIFE-7549-- Start
	    if (isNE(itdmIO.getItemcoy(),wsspcomn.company))
	    {
	    	goTo(GotoLabel.nextRecord1400);
	    }
	  //ILIFE-7549-- END
		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		processScreen("S6231", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
	}

protected void nextRecord1400()
	{
		itdmIO.setFunction(varcom.nextr);
		callItdmIo5100();
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.setMoreSign1400);
		}
		//ILIFE-7549-- Start
		/*if (isEQ(scrnparams.subfileRrn,wsaaSubfileSize)) {
			wsaaRem.set(0);
			goTo(GotoLabel.setMoreSign1400);
		}*/
		//ILIFE-7549-- End
		goTo(GotoLabel.writeToSubfile1300);
	}

protected void setMoreSign1400()
	{
		if (isNE(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
			wsaaLastItem.set(itdmIO.getItemitem());
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if ((isEQ(wsaaNormError,"N"))
		&& (isEQ(wsspcomn.edterror,"Y"))
		&& (!wsaaRolu.isTrue())
		&& (isEQ(wsaaToleranceFlag,"N"))
		&& (!wsaaCalc.isTrue())) {
			scrnparams.errorCode.set(h094);
			wsaaToleranceFlag = "Y";
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case validateSubfile2100: {
					validateSubfile2100();
				}
				case updateSubfile2400: {
					updateSubfile2400();
				}
				case rollUp2600: {
					rollUp2600();
				}
				case writeToSubfile2700: {
					writeToSubfile2700();
				}
				case setMoreSign2750: {
					setMoreSign2750();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaFunctionKey.set(scrnparams.statuz);
		if (isEQ(scrnparams.function,varcom.prot)) {
			goTo(GotoLabel.exit2090);
		}
		wsaaNormError = "N";
		if (wsaaCalc.isTrue()) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
	    scrnparams.function.set(varcom.srnch);
		processScreen("S6231", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.rollUp2600);
		}
		if ((isEQ(sv.accBidPrice,ZERO))
		&& (isEQ(sv.accOfferPrice,ZERO))
		&& (isEQ(sv.initBidPrice,ZERO))
		&& (isEQ(sv.initOfferPrice,ZERO))) {
			sv.initBarePrice.set(sv.initBidPrice);
			sv.accBarePrice.set(sv.accBidPrice);
			goTo(GotoLabel.updateSubfile2400);
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(sv.unitVirtualFund,itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.unitType,"I")) {
			if ((isNE(sv.accBidPrice,ZERO)
			|| isNE(sv.accOfferPrice,ZERO))) {
				sv.abidprErr.set(h025);
				sv.aoffprErr.set(h025);
				wsaaNormError = "Y";
				wsspcomn.edterror.set("Y");
			}
		}
		else {
			if (isEQ(t5515rec.unitType,"A")) {
				if ((isNE(sv.initBidPrice,ZERO)
				|| isNE(sv.initOfferPrice,ZERO))) {
					sv.ibidprErr.set(h025);
					sv.ioffprErr.set(h025);
					wsaaNormError = "Y";
					wsspcomn.edterror.set("Y");
				}
			}
			else {
				if ((isEQ(sv.accBidPrice,ZERO)
				|| isEQ(sv.accOfferPrice,ZERO))
				&& (isNE(sv.initBidPrice,ZERO)
				|| isNE(sv.initOfferPrice,ZERO))) {
					sv.abidprErr.set(f351);
					sv.ibidprErr.set(f351);
					sv.aoffprErr.set(f351);
					sv.ioffprErr.set(f351);
					wsaaNormError = "Y";
					wsspcomn.edterror.set("Y");
				}
			}
		}
		if ((isNE(wsaaNormError,"Y"))
		&& (isEQ(sv.accBidPrice,ZERO)
		&& isNE(sv.accOfferPrice,ZERO))) {
			sv.abidprErr.set(t004);
			wsaaNormError = "Y";
			wsspcomn.edterror.set("Y");
		}
		if ((isNE(wsaaNormError,"Y"))
		&& (isEQ(sv.initBidPrice,ZERO)
		&& isNE(sv.initOfferPrice,ZERO))) {
			sv.ibidprErr.set(t004);
			wsaaNormError = "Y";
			wsspcomn.edterror.set("Y");
		}
		sv.initBarePrice.set(sv.initBidPrice);
		sv.accBarePrice.set(sv.accBidPrice);
		if (isEQ(wsaaNormError,"Y")) {
			goTo(GotoLabel.updateSubfile2400);
		}
		if (wsaaCalc.isTrue()) {
			calculateOfferPrice8000();
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsaaToleranceFlag,"N")) {
			calculateOfferPrice8000();
			checkTolerances14000();
		}
	}

protected void updateSubfile2400()
	{
		sv.xtranno.add(1);
		scrnparams.function.set(varcom.supd);
		processScreen("S6231", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*NEXT-SUBFILE-RECORD*/
		goTo(GotoLabel.validateSubfile2100);
	}

protected void rollUp2600()
	{
		if ((!wsaaRolu.isTrue())
		|| (isNE(wsspcomn.edterror,varcom.oK))) {
			goTo(GotoLabel.exit2090);
		}
		wsspcomn.edterror.set("N");
		itdmIO.setItemitem(wsaaLastItem);
		itdmIO.setFunction(varcom.readr);
	}

protected void writeToSubfile2700()
	{
		callItdmIo5100();
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaLastItem.set(itdmIO.getItemitem());
			goTo(GotoLabel.setMoreSign2750);
		}
		//ILIFE-7549-- Start
		/*if (isGTE(wsaaRem,wsaaSubfileSize)) {
			wsaaRem.set(0);
			wsaaLastItem.set(itdmIO.getItemitem());
			goTo(GotoLabel.setMoreSign2750);
		}*/
		//ILIFE-7549-- End
		itdmIO.setFunction(varcom.nextr);
		if (isEQ(t5515rec.zfundtyp,"D")) {
			goTo(GotoLabel.writeToSubfile2700);
		}
		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		callScreenIo5000();
		wsaaRem.add(1);
		goTo(GotoLabel.writeToSubfile2700);
	}

protected void setMoreSign2750()
	{
		if (isNE(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		wsspcomn.edterror.set("Y");
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateDatabase3100();
				}
				case moreUpdateRec3200: {
					moreUpdateRec3200();
				}
				case update3700: {
					update3700();
				}
				case exit3900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3100()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void moreUpdateRec3200()
	{
		processScreen("S6231", sv);
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		if (isNE(sv.initBidPrice,0)) {
			if (isEQ(sv.initOfferPrice,0)) {
				calculateOfferPrice8000();
				goTo(GotoLabel.update3700);
			}
		}
		if (isNE(sv.accBidPrice,0)) {
			if (isEQ(sv.accOfferPrice,0)) {
				calculateOfferPrice8000();
			}
		}
	}

protected void update3700()
	{
		writeToVprcuprFile7000();
		goTo(GotoLabel.moreUpdateRec3200);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callScreenIo5000()
	{
		/*PARA*/
		processScreen("S6231", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callItdmIo5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case call5110: {
					call5110();
				}
				case exit5190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call5110()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemtabl(),"T5515")) {
			itdmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5190);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(itdmIO.getItemitem(),"*ALL ")) {
			itdmIO.setFunction(varcom.nextr);
			goTo(GotoLabel.call5110);
		}
		if (isEQ(itdmIO.getItemitem(),wsaaPrevItem)
		&& isNE(itdmIO.getFunction(),varcom.readr)) {
			itdmIO.setFunction(varcom.nextr);
			goTo(GotoLabel.call5110);
		}
		wsaaPrevItem.set(itdmIO.getItemitem());
		if (isEQ(wsaaSearchFund,"N")) {
			goTo(GotoLabel.exit5190);
		}
		wsaaItemFund.set(itdmIO.getItemitem());
		if (isNE(wsaaPartF1,wsaaItemF1)) {
			if (isLT(wsaaPartF1,wsaaItemF1)) {
				/*NEXT_SENTENCE*/
			}
			else {
				itdmIO.setFunction(varcom.nextr);
				goTo(GotoLabel.call5110);
			}
		}
		else {
			if (isNE(wsaaPartF2,SPACES)) {
				if (isNE(wsaaPartF2,wsaaItemF2)) {
					if (isLT(wsaaPartF2,wsaaItemF2)) {
						/*NEXT_SENTENCE*/
					}
					else {
						itdmIO.setFunction(varcom.nextr);
						goTo(GotoLabel.call5110);
					}
				}
				else {
					if (isNE(wsaaPartF3,SPACES)) {
						if (isNE(wsaaPartF3,wsaaItemF3)) {
							if (isLT(wsaaPartF3,wsaaItemF3)) {
								/*NEXT_SENTENCE*/
							}
							else {
								itdmIO.setFunction(varcom.nextr);
								goTo(GotoLabel.call5110);
							}
						}
						else {
							if (isNE(wsaaPartF4,SPACES)) {
								if (isNE(wsaaPartF4,wsaaItemF4)) {
									if (isLT(wsaaPartF4,wsaaItemF4)) {
										/*NEXT_SENTENCE*/
									}
									else {
										itdmIO.setFunction(varcom.nextr);
										goTo(GotoLabel.call5110);
									}
								}
							}
						}
					}
				}
			}
		}
		wsaaSearchFund.set("N");
	}

protected void moveFieldsToSubfile6000()
	{
		para6000();
	}

protected void para6000()
	{
	
		sv.subfileArea.set(SPACES);
		if (isEQ(t5515rec.unitType,"A")
		|| isEQ(t5515rec.unitType,"I")
		|| isEQ(t5515rec.unitType,"B")) {
			if (isEQ(t5515rec.unitType,"A")) {
				sv.ibidprOut[varcom.pr.toInt()].set("Y");
				sv.ioffprOut[varcom.pr.toInt()].set("Y");
			}
			if (isEQ(t5515rec.unitType,"I")) {
				sv.abidprOut[varcom.pr.toInt()].set("Y");
				sv.aoffprOut[varcom.pr.toInt()].set("Y");
			}
			if (isEQ(t5515rec.unitType,"B")) {
				sv.ibidprOut[varcom.pr.toInt()].set(SPACES);
				sv.ioffprOut[varcom.pr.toInt()].set(SPACES);
				sv.abidprOut[varcom.pr.toInt()].set(SPACES);
				sv.aoffprOut[varcom.pr.toInt()].set(SPACES);
			}
		}
		else {
			sv.ibidprOut[varcom.pr.toInt()].set(SPACES);
			sv.ioffprOut[varcom.pr.toInt()].set(SPACES);
			sv.abidprOut[varcom.pr.toInt()].set(SPACES);
			sv.aoffprOut[varcom.pr.toInt()].set(SPACES);
		}
		sv.unitVirtualFund.set(itdmIO.getItemitem());
		datcon1rec.intDate.set(wsaaTdayIntDate);
		datcon1rec.function.set("CONU");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.update.set(datcon1rec.extDate);
		sv.xtranno.set(0);
		sv.initBarePrice.set(0);
		sv.initBidPrice.set(0);
		sv.initOfferPrice.set(0);
		sv.accBarePrice.set(0);
		sv.accBidPrice.set(0);
		sv.accOfferPrice.set(0);
	}

protected void writeToVprcuprFile7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initUnit7000();
				}
				case accumUnit7200: {
					accumUnit7200();
				}
				case exit7900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initUnit7000()
	{
		if ((isEQ(sv.initBidPrice,0))
		&& (isEQ(sv.accBidPrice,0))) {
			goTo(GotoLabel.exit7900);
		}
		vprcuprIO.setFunction(varcom.writr);
		vprcuprIO.setFormat(vprcuprrec);
		vprcuprIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcuprIO.setJobno(wsspuprc.uprcJobno);
		vprcuprIO.setEffdate(wsspuprc.uprcEffdate);
		vprcuprIO.setCompany(wsspcomn.company);
		if (isEQ(sv.initBidPrice,0)
		&& isEQ(sv.initOfferPrice,0)) {
			goTo(GotoLabel.accumUnit7200);
		}
		vprcuprIO.setUnitType("I");
		vprcuprIO.setUnitBarePrice(sv.initBarePrice);
		vprcuprIO.setUnitBidPrice(sv.initBidPrice);
		vprcuprIO.setUnitOfferPrice(sv.initOfferPrice);
		vprcuprIO.setValidflag("3");
		vprcuprIO.setProcflag("Y");
		vprcuprIO.setTranno(1);
		vprcuprIO.setUpdateDate(wsaaTdayIntDate);
		vprcuprIO.setTransactionTime(0);
		SmartFileCode.execute(appVars, vprcuprIO);
		if (isNE(vprcuprIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcuprIO.getStatuz());
			fatalError600();
		}
	}

protected void accumUnit7200()
	{
		if (isEQ(sv.accBidPrice,0)
		&& isEQ(sv.accOfferPrice,0)) {
			goTo(GotoLabel.exit7900);
		}
		vprcuprIO.setUnitType("A");
		vprcuprIO.setUnitBarePrice(sv.accBarePrice);
		vprcuprIO.setUnitBidPrice(sv.accBidPrice);
		vprcuprIO.setUnitOfferPrice(sv.accOfferPrice);
		vprcuprIO.setValidflag("3");
		vprcuprIO.setProcflag("Y");
		vprcuprIO.setTranno(1);
		vprcuprIO.setUpdateDate(wsaaTdayIntDate);
		vprcuprIO.setTransactionTime(0);
		SmartFileCode.execute(appVars, vprcuprIO);
		if (isNE(vprcuprIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(vprcuprIO.getStatuz());
			fatalError600();
		}
	}

protected void calculateOfferPrice8000()
	{
		try {
			para8000();
		}
		catch (GOTOException e){
		}
	}

protected void para8000()
	{
		if ((isEQ(sv.accBidPrice,0))
		&& (isEQ(sv.initBidPrice,0))) {
			goTo(GotoLabel.exit8900);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");
		if (isNE(itdmIO.getItemtabl(),"T5515")) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		t5515rec.t5515Rec.set(SPACES);
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		wsaaAcumPerc.set(t5515rec.acumbof);
		wsaaInitPerc.set(t5515rec.initBidOffer);
		if (isNE(sv.initBidPrice,0)) {
			if (isEQ(sv.initOfferPrice,0)) {
				compute(wsaaInitOffer, 6).setRounded(div((mult(sv.initBidPrice,100)),(sub(100,wsaaInitPerc))));
				sv.initOfferPrice.set(wsaaInitOffer);
			}
		}
		if (isNE(sv.accBidPrice,0)) {
			if (isEQ(sv.accOfferPrice,0)) {
				compute(wsaaAcumOffer, 6).setRounded(div((mult(sv.accBidPrice,100)),(sub(100,wsaaAcumPerc))));
				sv.accOfferPrice.set(wsaaAcumOffer);
			}
		}
	}

protected void checkTolerances14000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start14100();
					checkAccUnit14100();
				}
				case checkInitUnit14200: {
					checkInitUnit14200();
				}
				case exit14900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start14100()
	{
		if ((isEQ(sv.accBidPrice,0))
		&& (isEQ(sv.initBidPrice,0))) {
			goTo(GotoLabel.exit14900);
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(sv.unitVirtualFund,itdmIO.getItemitem())) {
			sv.vrtfndErr.set(g037);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.tolerance,0)) {
			goTo(GotoLabel.exit14900);
		}
		vprcupeIO.setCompany(wsspcomn.company);
		vprcupeIO.setValidflag("1");
		vprcupeIO.setEffdate(varcom.vrcmMaxDate);
		vprcupeIO.setJobno(99999999);
		vprcupeIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupeIO.setUnitType("A");
		vprcupeIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		vprcupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcupeIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.checkInitUnit14200);
		}
		if (isNE(wsspcomn.company,vprcupeIO.getCompany())
		|| isNE(sv.unitVirtualFund,vprcupeIO.getUnitVirtualFund())
		|| isNE(vprcupeIO.getUnitType(),"A")) {
			goTo(GotoLabel.checkInitUnit14200);
		}
	}

protected void checkAccUnit14100()
	{
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBidPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.accBidPrice, 5)
		&& isGT(sv.accBidPrice,(add(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))
		|| (setPrecision(sv.accBidPrice, 5)
		&& isLT(sv.accBidPrice,(sub(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))) {
			sv.abidprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitOfferPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.accOfferPrice, 5)
		&& isGT(sv.accOfferPrice,(add(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))
		|| (setPrecision(sv.accOfferPrice, 5)
		&& isLT(sv.accOfferPrice,(sub(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))) {
			sv.aoffprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkInitUnit14200()
	{
		vprcupeIO.setCompany(wsspcomn.company);
		vprcupeIO.setValidflag("1");
		vprcupeIO.setEffdate(varcom.vrcmMaxDate);
		vprcupeIO.setJobno(99999999);
		vprcupeIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupeIO.setUnitType("I");
		vprcupeIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		vprcupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcupeIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		SmartFileCode.execute(appVars, vprcupeIO);
		if (isNE(vprcupeIO.getStatuz(),varcom.oK)
		&& isNE(vprcupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprcupeIO.getParams());
			fatalError600();
		}
		if (isEQ(vprcupeIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit14900);
		}
		if (isNE(wsspcomn.company,vprcupeIO.getCompany())
		|| isNE(sv.unitVirtualFund,vprcupeIO.getUnitVirtualFund())
		|| isNE(vprcupeIO.getUnitType(),"I")) {
			goTo(GotoLabel.exit14900);
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitBidPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.initBidPrice, 5)
		&& isGT(sv.initBidPrice,(add(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))
		|| (setPrecision(sv.initBidPrice, 5)
		&& isLT(sv.initBidPrice,(sub(vprcupeIO.getUnitBidPrice(),wsaaTolerance))))) {
			sv.ibidprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
		compute(wsaaTolerance, 5).set(div((mult(vprcupeIO.getUnitOfferPrice(),t5515rec.tolerance)),100));
		if ((setPrecision(sv.initOfferPrice, 5)
		&& isGT(sv.initOfferPrice,(add(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))
		|| (setPrecision(sv.initOfferPrice, 5)
		&& isLT(sv.initOfferPrice,(sub(vprcupeIO.getUnitOfferPrice(),wsaaTolerance))))) {
			sv.ioffprErr.set(h095);
			wsspcomn.edterror.set("Y");
		}
	}
}
