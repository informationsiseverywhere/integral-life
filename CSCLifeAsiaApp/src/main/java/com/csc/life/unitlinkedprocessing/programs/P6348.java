/*
 * File: P6348.java
 * Date: 30 August 2009 0:43:55
 * Author: Quipoz Limited
 *
 * Class transformed from P6348.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtbrkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UjnlmjaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnmjaTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.utility.Validator;
import com.csc.diary.procedures.Dryproces;

//ILIFE-8137
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import java.util.List;

/**
* <pre>
*REMARKS.
*         P6348 - UNIT JOURNALS END PROCESSING.
*         ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*****************************************************************
* </pre>
*/
public class P6348 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6348");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaEndUjnl = "";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler1 = new FixedLengthStringData(181).isAPartOf(wsaaTransactionRec, 19, FILLER).init(SPACES);

		/* WSAA-MESSAGE-AREA */
	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaMessage = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 0).init("AT request submitted for ");
	private FixedLengthStringData wsaaMsgnum = new FixedLengthStringData(8).isAPartOf(wsaaMsgarea, 25);
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String ptrnrec = "PTRNREC   ";
	private String utrnmjarec = "UTRNMJAREC";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Atreqrec atreqrec = new Atreqrec();
	private Batcuprec batcuprec = new Batcuprec();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage temporary Breakout file*/
	private CovtbrkTableDAM covtbrkIO = new CovtbrkTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
		/*Unit Journal Temporary file - Major Alts*/
	private UjnlmjaTableDAM ujnlmjaIO = new UjnlmjaTableDAM();
		/*Unit Transactions - Major Alts Journals*/
	private UtrnmjaTableDAM utrnmjaIO = new UtrnmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	//ILPI-100
	
		private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
		private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
		private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		
		private ItemTableDAM itemIO = new ItemTableDAM();
		private T7508rec t7508rec = new T7508rec();
		private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
		private TablesInner tablesInner = new TablesInner();

		//ILIFE-8137 
	    private Chdrpf chdrpf = new Chdrpf();
		private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
		
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit3290
	}

	public P6348() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}

protected void screenEdit2000()
	{
		/*EDIT*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		retrieveHeader3010();
		readHold3030();
		updateHeader3040();
		batchUpdate3050();
		ptrn3060();
		breakoutCheck3070();
		//Diary ILPI-100 - Implementing L2Unitdeal diary for Unit Journal transaction 
		diaryProcessing3080(); 
		
	}

protected void retrieveHeader3010()
	{	
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/*RELEASE-HEADER*/
		chdrpfDAO.deleteCacheObject(chdrpf);
		/*chdrmjaIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void readHold3030()
	{
		//ILIFE-8137 ILIFE-8218
		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (chdrpf == null) {
			fatalError600();
		}
		/*chdrmjaIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void updateHeader3040()
	{
		//ILIFE-8137
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(),1).toInt());
		
		chdrpfDAO.updatebyTran(chdrpf);
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void batchUpdate3050()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}

protected void ptrn3060()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(chdrpf.getChdrcoy());
		ptrnIO.setChdrnum(chdrpf.getChdrnum());
		ptrnIO.setTranno(chdrpf.getTranno());
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid); //PINNACLE-2931
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void breakoutCheck3070()
	{
		covtbrkIO.setDataArea(SPACES);
		covtbrkIO.setChdrcoy(wsspcomn.company);
		covtbrkIO.setChdrnum(chdrpf.getChdrnum());
		covtbrkIO.setPlanSuffix(ZERO);
		covtbrkIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtbrkIO);
		if (isNE(covtbrkIO.getStatuz(),varcom.oK)
		&& isNE(covtbrkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtbrkIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,covtbrkIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),covtbrkIO.getChdrnum())) {
			covtbrkIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtbrkIO.getStatuz(),varcom.oK)) {
			sftlckCallAt3100();
		}
		else {
			ujnlmjaIO.setChdrcoy(chdrpf.getChdrcoy());
			ujnlmjaIO.setChdrnum(chdrpf.getChdrnum());
			ujnlmjaIO.setSeqno(ZERO);
			/*ILIFE-2248 start by slakkala */		
			ujnlmjaIO.setFunction("BEGN");
			/*ILIFE-2248 ends */	
			wsaaEndUjnl = "N";
			while ( !(isEQ(wsaaEndUjnl,"Y"))) {
				createUtrnLoop3200();
			}

			rlseSftlock3300();
		}
	}

protected void sftlckCallAt3100()
	{
		softlock3110();
	}

protected void softlock3110()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P6348AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.company.set(wsspcomn.company);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryKey.set(SPACES);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaPolsum.set(chdrpf.getPolsum());
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
		wsaaMsgnum.set(chdrpf.getChdrnum());
		wsspcomn.msgarea.set(wsaaMsgarea);
	}

protected void createUtrnLoop3200()
	{
		try {
			findUjnlmja3210();
			createUtrn3220();
/*ILIFE-2248 start by slakkala */			
//			deleteUjnlmja3230();
			ujnlmjaIO.setFunction(varcom.nextr);
/*ILIFE-2248 ends */	
		}
		catch (GOTOException e){
		}
	}

protected void findUjnlmja3210()
	{
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(ujnlmjaIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(ujnlmjaIO.getStatuz(),varcom.endp)) {
			ujnlmjaIO.setStatuz(varcom.endp);
			wsaaEndUjnl = "Y";
			goTo(GotoLabel.exit3290);
		}
	}

protected void createUtrn3220()
	{
		utrnmjaIO.setDataArea(SPACES);
		utrnmjaIO.setChdrcoy(ujnlmjaIO.getChdrcoy());
		utrnmjaIO.setChdrnum(ujnlmjaIO.getChdrnum());
		utrnmjaIO.setLife(ujnlmjaIO.getLife());
		utrnmjaIO.setCoverage(ujnlmjaIO.getCoverage());
		utrnmjaIO.setRider(ujnlmjaIO.getRider());
		utrnmjaIO.setPlanSuffix(ujnlmjaIO.getPlanSuffix());
		utrnmjaIO.setUnitVirtualFund(ujnlmjaIO.getUnitVirtualFund());
		utrnmjaIO.setUnitType(ujnlmjaIO.getUnitType());
		utrnmjaIO.setTranno(ujnlmjaIO.getTranno());
		utrnmjaIO.setTermid(ujnlmjaIO.getTermid());
		utrnmjaIO.setTransactionDate(ujnlmjaIO.getTransactionDate());
		utrnmjaIO.setTransactionTime(ujnlmjaIO.getTransactionTime());
		utrnmjaIO.setUser(ujnlmjaIO.getUser());
		utrnmjaIO.setBatccoy(ujnlmjaIO.getBatccoy());
		utrnmjaIO.setBatcbrn(ujnlmjaIO.getBatcbrn());
		utrnmjaIO.setBatcactyr(ujnlmjaIO.getBatcactyr());
		utrnmjaIO.setBatcactmn(ujnlmjaIO.getBatcactmn());
		utrnmjaIO.setBatctrcde(ujnlmjaIO.getBatctrcde());
		utrnmjaIO.setBatcbatch(SPACES);
		utrnmjaIO.setJobnoPrice(ZERO);
		utrnmjaIO.setStrpdate(ZERO);
		utrnmjaIO.setInciNum(ZERO);
		utrnmjaIO.setInciPerd01(ZERO);
		utrnmjaIO.setInciPerd02(ZERO);
		utrnmjaIO.setInciprm01(ZERO);
		utrnmjaIO.setInciprm02(ZERO);
		utrnmjaIO.setSurrenderPercent(ZERO);
		utrnmjaIO.setUnitSubAccount(ujnlmjaIO.getUnitSubAccount());
		utrnmjaIO.setNowDeferInd("N");
		utrnmjaIO.setNofUnits(ujnlmjaIO.getNofUnits());
		utrnmjaIO.setNofDunits(ujnlmjaIO.getNofDunits());
		utrnmjaIO.setMoniesDate(ujnlmjaIO.getMoniesDate());
		utrnmjaIO.setPriceDateUsed(ujnlmjaIO.getPriceDateUsed());
		utrnmjaIO.setPriceUsed(ujnlmjaIO.getPriceUsed());
		utrnmjaIO.setUnitBarePrice(ujnlmjaIO.getPriceUsed());
		utrnmjaIO.setCrtable(ujnlmjaIO.getCrtable());
		utrnmjaIO.setCntcurr(ujnlmjaIO.getCntcurr());
		utrnmjaIO.setFeedbackInd(SPACES);
		utrnmjaIO.setContractAmount(ZERO);
		utrnmjaIO.setFundRate(1);
		utrnmjaIO.setFundCurrency(ujnlmjaIO.getFundCurrency());
		utrnmjaIO.setFundAmount(ZERO);
		utrnmjaIO.setSacscode(ujnlmjaIO.getSacscode());
		utrnmjaIO.setSacstyp(ujnlmjaIO.getSacstyp());
		utrnmjaIO.setGenlcde(ujnlmjaIO.getGenlcde());
		utrnmjaIO.setContractType(ujnlmjaIO.getContractType());
		utrnmjaIO.setTriggerModule(SPACES);
		utrnmjaIO.setTriggerKey(SPACES);
		utrnmjaIO.setProcSeqNo(ujnlmjaIO.getProcSeqNo());
		utrnmjaIO.setSvp(1);
		utrnmjaIO.setDiscountFactor(1);
		utrnmjaIO.setCrComDate(ujnlmjaIO.getCrComDate());
		utrnmjaIO.setFormat(utrnmjarec);
		utrnmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnmjaIO);
		if (isNE(utrnmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnmjaIO.getParams());
			fatalError600();
		}
	}

protected void deleteUjnlmja3230()
	{
		ujnlmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ujnlmjaIO.getParams());
			fatalError600();
		}
		ujnlmjaIO.setFunction(varcom.nextr);
	}

protected void rlseSftlock3300()
	{
		unlockContract3310();
	}

protected void unlockContract3310()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
//Diary ILPI-100 - Implementing L2Unitdeal diary for Unit Journal transaction - asood8
protected void diaryProcessing3080()
{
	/* This section will determine if the DIARY system is present      */
	/* If so, the appropriate parameters are filled and the            */
	/* diary processor is called.                                      */
	wsaaT7508Cnttype.set(chdrpf.getCnttype());
	wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
	readT75085100();
	/* If item not found try other types of contract.                  */
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		wsaaT7508Cnttype.set("***");
		readT75085100();
	}
	/* If item not found no Diary Update Processing is Required.       */
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		return ;
	}
	t7508rec.t7508Rec.set(itemIO.getGenarea());
	drypDryprcRecInner.drypStatuz.set(varcom.oK);
	drypDryprcRecInner.onlineMode.setTrue();
	drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
	drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
	drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
	drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
	drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
	drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
	drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
	drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum());
	drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
	drypDryprcRecInner.drypEffectiveTime.set(ZERO);
	drypDryprcRecInner.drypFsuCompany.set("9");
	drypDryprcRecInner.drypProcSeqNo.set(100);
	drypDryprcRecInner.drypAplsupto.set(ZERO);
	drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
	/* Fill the parameters.                                            */
	drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
	drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
	drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
	drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
	drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
	drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
	drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
	drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
	drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
	drypDryprcRecInner.drypCpiDate.set(varcom.vrcmDate);
	drypDryprcRecInner.drypBbldate.set(varcom.vrcmDate);
	drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
	drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
	drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
	drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
	callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
	if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
		syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
		syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
		fatalError600();
	}
	
	
	
}
protected void readT75085100()
{
	start5110();
}

protected void start5110()
{
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setItemtabl(tablesInner.t7508);
	itemIO.setItemitem(wsaaT7508Key);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(itemIO.getStatuz());
		fatalError600();
	}
}

private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}

private static final class TablesInner {
	/* TABLES */
private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
}
//Diary ILPI-100 - Implementing L2Unitdeal diary for Unit Journal transaction - End


}

