/*
 * File: Br512.java
 * Date: 29 August 2009 22:13:14
 * Author: Quipoz Limited
 * 
 * Class transformed from BR512.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5646rec;
import com.csc.life.productdefinition.tablestructures.Tr52rrec;
import com.csc.life.terminationclaims.dataaccess.dao.PtsdpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Ptsdpf;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrpwpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrpwpf;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*               Partial Withdrawals Batch Process
*               ---------------------------------
*
*  This batch program is part of the UNITDEAL batch job. It will
*  read all records from ZRPW that are created in the trigger
*  program, UNLPRTT. For each ZRPW record, it will read through
*  PTSDCLM and accumulate the actual value for the coverage. This
*  accumulated amount is the Withdrawal Amount.
*  For each coverage, the COVR is read to retrieve the current
*  Sum Assured & all COVR records with TRANNO >= ZRPW-TRANNO
*  will be updated with the new Sum Assured.
*
*  The new Sum Assured is base on the following calculation
*  method (A or B) whichever is higher:
*
*  A)  NEW SA = OLD SA - (WITHDRAWAL AMT * PERCENTAGE)
*      where PERCENTAGE = LEXT-ZNADJPERC if LEXT exists or
*                       = T5646-FACTORSA
*
*  B)  New SA = Remaining units after surrender * Bid value
*               UTRS-CURRENT-UNIT-BAL * BID PRICE
*
*  The ZRPW records will be deleted once its processed.
*
*****************************************************************
* </pre>
*/
public class Br512 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR512");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* ERRORS */
	private static final String e031 = "E031";
	private static final String e563 = "E563";
		/* TABLES */
	private static final String t5646 = "T5646";
	private static final String tr52r = "TR52R";

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaLextFound = new FixedLengthStringData(1).init("N");
	private Validator lextFound = new Validator(wsaaLextFound, "Y");

	private FixedLengthStringData wsaaFactorFound = new FixedLengthStringData(1).init("N");
	private Validator factorFound = new Validator(wsaaFactorFound, "Y");
	private PackedDecimalData wsaaPercRate = new PackedDecimalData(7, 4);
	private PackedDecimalData wsaaWithdrawalAmt = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNewSumins1 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNewSumins2 = new PackedDecimalData(13, 2);
	private ZonedDecimalData indx = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTerm = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaTr52rKey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr52rCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52rKey, 0);
	private FixedLengthStringData wsaaTr52rBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaTr52rKey, 4);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	// private CovrTableDAM covrIO = new CovrTableDAM(); // ILIFE-4349
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	//private PtsdclmTableDAM ptsdclmIO = new PtsdclmTableDAM(); // ILIFE-4349
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	//private ZrpwTableDAM zrpwIO = new ZrpwTableDAM(); // ILIFE-4349
	private T5646rec t5646rec = new T5646rec();
	private Tr52rrec tr52rrec = new Tr52rrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	
	// Start: ILIFE-4349
	private ZrpwpfDAO zrpwpfDao =getApplicationContext().getBean("zrpwpfDao",ZrpwpfDAO.class);
	private Iterator<Zrpwpf> zrpwpfIter = null;
	private Zrpwpf zrpwpfRec = null;
	private PtsdpfDAO ptsdDao = getApplicationContext().getBean("ptsdpfDAO",PtsdpfDAO.class);
	private Iterator<Ptsdpf> ptsdpfIter=null;
	private Ptsdpf ptsdObj=null;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAOImpl.class);
	private LinkedList<Covrpf> covrpfLinkedList=null;
	private Covrpf covrObj=null;
	// Ends: ILIFE-4349
	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr2620, 
		exit2620, 
		exit2640
	}

	public Br512() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		// Start: ILIFE-4349
		/*zrpwIO.setParams(SPACES);
		zrpwIO.setChdrcoy(SPACES);
		zrpwIO.setChdrnum(SPACES);
		zrpwIO.setTranno(ZERO);
		zrpwIO.setFunction(varcom.begnh);
		zrpwIO.setFormat(formatsInner.zrpwrec);*/
		Zrpwpf zrpwpf = new Zrpwpf();
		List<Zrpwpf> zrpwpfList = zrpwpfDao.searchZrpwpfRecord(zrpwpf);
		zrpwpfIter = zrpwpfList.iterator();
		// Ends: ILIFE-4349
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		// Start: ILIFE-4349
		if(zrpwpfIter.hasNext()){
			zrpwpfRec = zrpwpfIter.next();
		}else{
			wsspEdterror.set(varcom.endp);
		}
		// Ends: ILIFE-4349
		/*EXIT*/
	}

protected void edit2500()
	{
		wsspEdterror.set(varcom.oK);
		wsaaWithdrawalAmt.set(ZERO);
		wsaaPercRate.set(ZERO);
		wsaaNewSumins1.set(ZERO);
		wsaaNewSumins2.set(ZERO);
		// Start: ILIFE-4349
		Ptsdpf ptsdpf = new Ptsdpf();
		ptsdpf.setChdrcoy(zrpwpfRec.getChdrcoy());
		ptsdpf.setChdrnum(zrpwpfRec.getChdrnum());
		ptsdpf.setTranno(zrpwpfRec.getTranno());
		ptsdpf.setPlanSuffix(zrpwpfRec.getPlnsfx());
		ptsdpf.setCoverage(zrpwpfRec.getCoverage());
		ptsdpf.setRider(zrpwpfRec.getRider());
		ptsdpf.setLife(zrpwpfRec.getLife());
		List<Ptsdpf> ptsdpfList = ptsdDao.serachPtsdRecord(ptsdpf);
		ptsdpfIter = ptsdpfList.iterator();
		while(ptsdpfIter.hasNext()){
			accumulateAmount2600();
		}
		
		List<Covrpf> covrpfList = covrpfDAO.searchCovrrnlRecord(zrpwpfRec.getChdrcoy(), zrpwpfRec.getChdrnum(), 
				"01", zrpwpfRec.getCoverage(), zrpwpfRec.getRider(), zrpwpfRec.getPlnsfx().doubleValue());
		covrpfLinkedList = new LinkedList<>(covrpfList);
		while(covrpfLinkedList.size() > 0){
			processCovr2700();
		}
		
		int count = zrpwpfDao.deleteZrpwpf(zrpwpfRec);
		if(count == 0){
			syserrrec.params.set(zrpwpfRec.getChdrnum());
			fatalError600();
		}
		// Ends: ILIFE-4349
	}

protected void accumulateAmount2600()
	{
		// Start: ILIFE-4349
		ptsdObj = ptsdpfIter.next();
		if("C".equalsIgnoreCase(ptsdObj.getFieldType())
				|| "J".equalsIgnoreCase(ptsdObj.getFieldType())){
			return;
		}
		PackedDecimalData actValue = new PackedDecimalData(17, 2);
		actValue.set(ptsdObj.getActvalue());
		wsaaWithdrawalAmt.add(actValue);
		readUtrs2630();
	}

protected void readUtrs2630()
	{
		utrsIO.setParams(SPACES);
		utrsIO.setChdrcoy(ptsdObj.getChdrcoy());
		utrsIO.setChdrnum(ptsdObj.getChdrnum());
		utrsIO.setLife(ptsdObj.getLife());
		utrsIO.setCoverage(ptsdObj.getCoverage());
		utrsIO.setRider(ptsdObj.getRider());
		utrsIO.setPlanSuffix(ptsdObj.getPlanSuffix());
		utrsIO.setUnitVirtualFund(ptsdObj.getVirtualFund());
		utrsIO.setUnitType(ptsdObj.getFieldType());
		utrsIO.setFunction(varcom.readr);
		utrsIO.setFormat(formatsInner.utrsrec);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isEQ(utrsIO.getStatuz(),varcom.mrnf)) {
			readHits2650();
			return ;
		}
		/* IF UTRS-STATUZ              = ENDP                           */
		/* OR (UTRS-CHDRCOY        NOT = PTSDCLM-CHDRCOY)               */
		/* OR (UTRS-CHDRNUM        NOT = PTSDCLM-CHDRNUM)               */
		/* OR (UTRS-RIDER          NOT = PTSDCLM-RIDER)                 */
		/* OR (UTRS-COVERAGE       NOT = PTSDCLM-COVERAGE)              */
		/* OR (UTRS-LIFE           NOT = PTSDCLM-LIFE)                  */
		/* OR (UTRS-UNIT-VIRTUAL-FUND                                   */
		/*                         NOT = PTSDCLM-VIRTUAL-FUND)          */
		/* OR (UTRS-UNIT-TYPE      NOT = PTSDCLM-FIELD-TYPE)            */
		/*    MOVE ENDP                TO UTRS-STATUZ                   */
		/*    GO TO 2630-EXIT                                           */
		/* END-IF.                                                      */
		/* GET BID PRICE USING UTRS-UNIT-VIRTUAL-FUND AND UTRS-UNIT-TYPE*/
		nowPrice2640();
		compute(wsaaNewSumins2, 5).set(add((mult(utrsIO.getCurrentUnitBal(),vprnudlIO.getUnitBidPrice())),wsaaNewSumins2));
	}

protected void nowPrice2640()
	{
		try {
			parmdtePrice2640();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void parmdtePrice2640()
	{
		/*If the transaction is Priced normally, read Price.*/
		vprnudlIO.setDataKey(SPACES);
		vprnudlIO.setCompany(utrsIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(utrsIO.getUnitType());
		vprnudlIO.setEffdate(bsscIO.getEffectiveDate());
		vprnudlIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError600();
		}
		if ((isNE(vprnudlIO.getCompany(),ptsdObj.getChdrcoy()))
		|| (isNE(vprnudlIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund()))
		|| (isNE(vprnudlIO.getUnitType(),utrsIO.getUnitType()))
		|| (isEQ(vprnudlIO.getStatuz(),varcom.endp))) {
			vprnudlIO.setFunction(varcom.nextp);
			lastPrice2640();
		}
		goTo(GotoLabel.exit2640);
	}

protected void lastPrice2640()
	{
		/*If the transaction is Priced normally, read Price.*/
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError600();
		}
		if ((isNE(vprnudlIO.getCompany(),ptsdObj.getChdrcoy()))
		|| (isNE(vprnudlIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund()))
		|| (isNE(vprnudlIO.getUnitType(),utrsIO.getUnitType()))
		|| (isEQ(vprnudlIO.getStatuz(),varcom.endp))) {
			vprnudlIO.setStatuz(varcom.endp);
		}
	}

protected void readHits2650()
	{
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(ptsdObj.getChdrcoy());
		hitsIO.setChdrnum(ptsdObj.getChdrnum());
		hitsIO.setLife(ptsdObj.getLife());
		hitsIO.setCoverage(ptsdObj.getCoverage());
		hitsIO.setRider(ptsdObj.getRider());
		hitsIO.setPlanSuffix(ptsdObj.getPlanSuffix());
		hitsIO.setZintbfnd(ptsdObj.getVirtualFund());
		hitsIO.setFunction(varcom.readr);
		hitsIO.setFormat(formatsInner.hitsrec);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		compute(wsaaNewSumins2, 2).set(add(hitsIO.getZcurprmbal(),wsaaNewSumins2));
	}

protected void processCovr2700()
	{
		covrObj = covrpfLinkedList.poll();
		/* Read LEXT file to see if any Special Terms are entered. If yes,*/
		/* use LEXT-ZNADJPERC to calculate the new Sum Assured. Otherwise,*/
		/* read T5646 to get the FACTORSA & used it for the calculation.*/
		readLext2800();
		if (!lextFound.isTrue()) {
			readT56462900();
			if (!factorFound.isTrue()) {
				readTr52r2950();
			}
		}
		/* Update the old SUMINS with the new calculated SUMINS.*/
		while (covrObj != null) {
			compute(wsaaNewSumins1, 5).setRounded(sub(covrObj.getSumins(),(mult(wsaaWithdrawalAmt,wsaaPercRate))));
			if (isGT(wsaaNewSumins1,wsaaNewSumins2)) {
				covrObj.setSumins(wsaaNewSumins1.getbigdata());
			}
			else {
				covrObj.setSumins(wsaaNewSumins2.getbigdata());
			}
			if (isNE(covrObj.getSumins(), 0)) {
				zrdecplrec.amountIn.set(covrObj.getSumins());
				callRounding8000();
				covrObj.setSumins(zrdecplrec.amountOut.getbigdata());
			}
			boolean updateStatus = covrpfDAO.updateSumins(covrObj);
			if(!updateStatus){
				syserrrec.params.set(covrObj.getChdrnum());
				fatalError600();
			}
			
			/* There is a possibility that the UNITDEAL may be run after*/
			/* other transactions. Thus, we have to update the SUMINS of all*/
			/* the COVR records until the COVR-TRANNO = ZRPW-TRANNO.*/
			covrObj = covrpfLinkedList.peek();
			if(covrObj == null || isLT(covrObj.getTranno(), zrpwpfRec.getTranno())){
				break;
			}else{
				covrObj = covrpfLinkedList.poll();
			}
		}
		
	}

protected void readLext2800()
	{
		wsaaLextFound.set("N");
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(zrpwpfRec.getChdrcoy());
		lextIO.setChdrnum(zrpwpfRec.getChdrnum());
		lextIO.setLife(covrObj.getLife());
		lextIO.setRider(zrpwpfRec.getRider());
		lextIO.setCoverage(zrpwpfRec.getCoverage());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFunction(varcom.begn);
		lextIO.setFormat(formatsInner.lextrec);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)
		|| isNE(lextIO.getChdrcoy(),zrpwpfRec.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),zrpwpfRec.getChdrnum())
		|| isNE(lextIO.getRider(),zrpwpfRec.getRider())
		|| isNE(lextIO.getCoverage(),zrpwpfRec.getCoverage())
		|| isNE(lextIO.getLife(),covrObj.getLife())
		|| isEQ(lextIO.getZnadjperc(),0)) {
			lextIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaLextFound.set("Y");
		compute(wsaaPercRate, 4).set(div(lextIO.getZnadjperc(),100));
	}

protected void readT56462900()
	{
		wsaaFactorFound.set("N");
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5646);
		itemIO.setItemitem(covrObj.getCrtable());
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t5646rec.t5646Rec.set(itemIO.getGenarea());
		for (indx.set(1); !(isGT(indx,12)
		|| factorFound.isTrue()); indx.add(1)){
			if (isGTE(covrObj.getAnbAtCcd(), t5646rec.ageIssageFrm[indx.toInt()])
			&& isLTE(covrObj.getAnbAtCcd(), t5646rec.ageIssageTo[indx.toInt()])) {
				wsaaFactorFound.set("Y");
				wsaaPercRate.set(t5646rec.factorsa[indx.toInt()]);
			}
		}
	}

protected void readTr52r2950()
	{
		readChdrenq2951();
		readTr52r2952();
	}

protected void readChdrenq2951()
	{
		/* Check CHDRENQ to get billfrequence                              */
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(bsprIO.getCompany());
		chdrenqIO.setChdrnum(zrpwpfRec.getChdrnum());
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
	}

protected void readTr52r2952()
	{
		wsaaFactorFound.set("N");
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(tr52r);
		wsaaTr52rCrtable.set(covrObj.getCrtable());
		wsaaTr52rBillfreq.set(chdrenqIO.getBillfreq());
		itdmIO.setItemitem(wsaaTr52rKey);
		itdmIO.setItmfrm(chdrenqIO.getBtdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaTr52rKey, itdmIO.getItemitem())
		|| isNE(bsprIO.getCompany(), itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tr52r)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e031);
			fatalError600();
		}
		else {
			tr52rrec.tr52rRec.set(itdmIO.getGenarea());
		}
		compute(wsaaTerm, 0).set(sub(tr52rrec.agelimit, covrObj.getAnbAtCcd()));
		if (isGT(wsaaTerm, covrObj.getPremCessTerm())) {
			wsaaTerm.set(covrObj.getPremCessTerm());
		}
		for (indx.set(1); !(isGT(indx, 12)
		|| factorFound.isTrue()); indx.add(1)){
			if (isLTE(wsaaTerm, tr52rrec.toterm[indx.toInt()])) {
				wsaaFactorFound.set("Y");
				/*  WSAA-FACTOR = Max (TR52R-FACTSAMN, Min (TR52R-FACTSAMX,*/
				/*  TR52R-FACTORSA * WSAA-TERM)*/
				compute(wsaaPercRate, 4).set(mult(tr52rrec.factorsa[indx.toInt()], wsaaTerm));
				if (isEQ(tr52rrec.factsamx[indx.toInt()], ZERO)) {
					tr52rrec.factsamx[indx.toInt()].set(999.9999);
				}
				if (isGT(wsaaPercRate, tr52rrec.factsamx[indx.toInt()])) {
					wsaaPercRate.set(tr52rrec.factsamx[indx.toInt()]);
				}
				if (isLT(wsaaPercRate, tr52rrec.factsamn[indx.toInt()])) {
					wsaaPercRate.set(tr52rrec.factsamn[indx.toInt()]);
				}
			}
		}
		if (isGT(indx, 12)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e563);
			fatalError600();
		}
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		String currency = " ";
		if(ptsdObj != null){
			currency = ptsdObj.getCurrcd();
		}
		zrdecplrec.currency.set(currency);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData zrpwrec = new FixedLengthStringData(10).init("ZRPWREC");
	private FixedLengthStringData ptsdclmrec = new FixedLengthStringData(10).init("PTSDCLMREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData utrsrec = new FixedLengthStringData(10).init("UTRSREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData hitsrec = new FixedLengthStringData(10).init("HITSREC");
}
}
