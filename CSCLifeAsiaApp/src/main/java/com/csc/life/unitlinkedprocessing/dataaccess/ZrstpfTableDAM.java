package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZrstpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:02
 * Class transformed from ZRSTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZrstpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 104;
	public FixedLengthStringData zrstrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zrstpfRecord = zrstrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zrstrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zrstrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zrstrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(zrstrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zrstrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zrstrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(zrstrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zrstrec);
	public PackedDecimalData xtranno = DD.xtranno.copy().isAPartOf(zrstrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(zrstrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(zrstrec);
	public PackedDecimalData zramount01 = DD.zramount.copy().isAPartOf(zrstrec);
	public PackedDecimalData zramount02 = DD.zramount.copy().isAPartOf(zrstrec);
	public PackedDecimalData trandate = DD.trandate.copy().isAPartOf(zrstrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(zrstrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(zrstrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zrstrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zrstrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zrstrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZrstpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZrstpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZrstpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZrstpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrstpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZrstpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrstpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZRSTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"SEQNO, " +
							"TRANNO, " +
							"XTRANNO, " +
							"BATCTRCDE, " +
							"SACSTYP, " +
							"ZRAMOUNT01, " +
							"ZRAMOUNT02, " +
							"TRANDATE, " +
							"USTMNO, " +
							"FDBKIND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     seqno,
                                     tranno,
                                     xtranno,
                                     batctrcde,
                                     sacstyp,
                                     zramount01,
                                     zramount02,
                                     trandate,
                                     ustmno,
                                     feedbackInd,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		seqno.clear();
  		tranno.clear();
  		xtranno.clear();
  		batctrcde.clear();
  		sacstyp.clear();
  		zramount01.clear();
  		zramount02.clear();
  		trandate.clear();
  		ustmno.clear();
  		feedbackInd.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZrstrec() {
  		return zrstrec;
	}

	public FixedLengthStringData getZrstpfRecord() {
  		return zrstpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZrstrec(what);
	}

	public void setZrstrec(Object what) {
  		this.zrstrec.set(what);
	}

	public void setZrstpfRecord(Object what) {
  		this.zrstpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zrstrec.getLength());
		result.set(zrstrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}