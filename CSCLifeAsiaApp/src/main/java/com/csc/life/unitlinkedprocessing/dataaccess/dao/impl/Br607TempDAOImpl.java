package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br607TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br607DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br607TempDAOImpl extends BaseDAOImpl<Br607DTO> implements Br607TempDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Br607TempDAOImpl.class);
    
	@Override
	public int buildTempData(Br607DTO dto) {
		deleteTempData(dto.getTableName());
		return insertTempData(dto);
	}

	private void deleteTempData(String tableName) {
		String sqlStr = "DELETE FROM " + tableName;
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	private int insertTempData(Br607DTO dto) {
		StringBuilder sqlStr = new StringBuilder("INSERT INTO ");
		sqlStr.append(dto.getTableName());
		sqlStr.append(" (COMPANY,VRTFUND,UNITYP,NOFUNTS) ");
		sqlStr.append("SELECT COMPANY, VRTFUND, UNITYP, NOFUNTS from UFNSPF ");
		sqlStr.append("where COMPANY = ? AND JOBNO = ? ORDER BY COMPANY, VRTFUND, UNITYP");
		
		PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		try {
			ps.setString(1, dto.getCompany());
			ps.setInt(2, dto.getJobNo());
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

}
