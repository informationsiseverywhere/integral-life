package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UswdTableDAM.java
 * Date: Sun, 30 Aug 2009 03:51:48
 * Class transformed from USWD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UswdTableDAM extends UswdpfTableDAM {

	public UswdTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("USWD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", PLNSFX"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "PLNSFX, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "TRANNO, " +
		            "EFFDATE, " +
		            "PCAMTIND, " +
		            "ORSWCHFE, " +
		            "SRCFUND01, " +
		            "SRCFUND02, " +
		            "SRCFUND03, " +
		            "SRCFUND04, " +
		            "SRCFUND05, " +
		            "SRCFUND06, " +
		            "SRCFUND07, " +
		            "SRCFUND08, " +
		            "SRCFUND09, " +
		            "SRCFUND10, " +
		            "SRCFUND11, " +
		            "SRCFUND12, " +
		            "SRCFUND13, " +
		            "SRCFUND14, " +
		            "SRCFUND15, " +
		            "SRCFUND16, " +
		            "SRCFUND17, " +
		            "SRCFUND18, " +
		            "SRCFUND19, " +
		            "SRCFUND20, " +
		            "SCFNDTYP01, " +
		            "SCFNDTYP02, " +
		            "SCFNDTYP03, " +
		            "SCFNDTYP04, " +
		            "SCFNDTYP05, " +
		            "SCFNDTYP06, " +
		            "SCFNDTYP07, " +
		            "SCFNDTYP08, " +
		            "SCFNDTYP09, " +
		            "SCFNDTYP10, " +
		            "SCFNDTYP11, " +
		            "SCFNDTYP12, " +
		            "SCFNDTYP13, " +
		            "SCFNDTYP14, " +
		            "SCFNDTYP15, " +
		            "SCFNDTYP16, " +
		            "SCFNDTYP17, " +
		            "SCFNDTYP18, " +
		            "SCFNDTYP19, " +
		            "SCFNDTYP20, " +
		            "SCFNDCUR01, " +
		            "SCFNDCUR02, " +
		            "SCFNDCUR03, " +
		            "SCFNDCUR04, " +
		            "SCFNDCUR05, " +
		            "SCFNDCUR06, " +
		            "SCFNDCUR07, " +
		            "SCFNDCUR08, " +
		            "SCFNDCUR09, " +
		            "SCFNDCUR10, " +
		            "SCFNDCUR11, " +
		            "SCFNDCUR12, " +
		            "SCFNDCUR13, " +
		            "SCFNDCUR14, " +
		            "SCFNDCUR15, " +
		            "SCFNDCUR16, " +
		            "SCFNDCUR17, " +
		            "SCFNDCUR18, " +
		            "SCFNDCUR19, " +
		            "SCFNDCUR20, " +
		            "SCPRCAMT01, " +
		            "SCPRCAMT02, " +
		            "SCPRCAMT03, " +
		            "SCPRCAMT04, " +
		            "SCPRCAMT05, " +
		            "SCPRCAMT06, " +
		            "SCPRCAMT07, " +
		            "SCPRCAMT08, " +
		            "SCPRCAMT09, " +
		            "SCPRCAMT10, " +
		            "SCPRCAMT11, " +
		            "SCPRCAMT12, " +
		            "SCPRCAMT13, " +
		            "SCPRCAMT14, " +
		            "SCPRCAMT15, " +
		            "SCPRCAMT16, " +
		            "SCPRCAMT17, " +
		            "SCPRCAMT18, " +
		            "SCPRCAMT19, " +
		            "SCPRCAMT20, " +
		            "SCESTVAL01, " +
		            "SCESTVAL02, " +
		            "SCESTVAL03, " +
		            "SCESTVAL04, " +
		            "SCESTVAL05, " +
		            "SCESTVAL06, " +
		            "SCESTVAL07, " +
		            "SCESTVAL08, " +
		            "SCESTVAL09, " +
		            "SCESTVAL10, " +
		            "SCESTVAL11, " +
		            "SCESTVAL12, " +
		            "SCESTVAL13, " +
		            "SCESTVAL14, " +
		            "SCESTVAL15, " +
		            "SCESTVAL16, " +
		            "SCESTVAL17, " +
		            "SCESTVAL18, " +
		            "SCESTVAL19, " +
		            "SCESTVAL20, " +
		            "SCACTVAL01, " +
		            "SCACTVAL02, " +
		            "SCACTVAL03, " +
		            "SCACTVAL04, " +
		            "SCACTVAL05, " +
		            "SCACTVAL06, " +
		            "SCACTVAL07, " +
		            "SCACTVAL08, " +
		            "SCACTVAL09, " +
		            "SCACTVAL10, " +
		            "SCACTVAL11, " +
		            "SCACTVAL12, " +
		            "SCACTVAL13, " +
		            "SCACTVAL14, " +
		            "SCACTVAL15, " +
		            "SCACTVAL16, " +
		            "SCACTVAL17, " +
		            "SCACTVAL18, " +
		            "SCACTVAL19, " +
		            "SCACTVAL20, " +
		            "TGTFUND01, " +
		            "TGTFUND02, " +
		            "TGTFUND03, " +
		            "TGTFUND04, " +
		            "TGTFUND05, " +
		            "TGTFUND06, " +
		            "TGTFUND07, " +
		            "TGTFUND08, " +
		            "TGTFUND09, " +
		            "TGTFUND10, " +
		            "TGFNDTYP01, " +
		            "TGFNDTYP02, " +
		            "TGFNDTYP03, " +
		            "TGFNDTYP04, " +
		            "TGFNDTYP05, " +
		            "TGFNDTYP06, " +
		            "TGFNDTYP07, " +
		            "TGFNDTYP08, " +
		            "TGFNDTYP09, " +
		            "TGFNDTYP10, " +
		            "TGFNDCUR01, " +
		            "TGFNDCUR02, " +
		            "TGFNDCUR03, " +
		            "TGFNDCUR04, " +
		            "TGFNDCUR05, " +
		            "TGFNDCUR06, " +
		            "TGFNDCUR07, " +
		            "TGFNDCUR08, " +
		            "TGFNDCUR09, " +
		            "TGFNDCUR10, " +
		            "TGTPRCNT01, " +
		            "TGTPRCNT02, " +
		            "TGTPRCNT03, " +
		            "TGTPRCNT04, " +
		            "TGTPRCNT05, " +
		            "TGTPRCNT06, " +
		            "TGTPRCNT07, " +
		            "TGTPRCNT08, " +
		            "TGTPRCNT09, " +
		            "TGTPRCNT10, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "PLNSFX ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "PLNSFX DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               planSuffix,
                               life,
                               coverage,
                               rider,
                               tranno,
                               effdate,
                               percentAmountInd,
                               overrideFee,
                               sourceFund01,
                               sourceFund02,
                               sourceFund03,
                               sourceFund04,
                               sourceFund05,
                               sourceFund06,
                               sourceFund07,
                               sourceFund08,
                               sourceFund09,
                               sourceFund10,
                               sourceFund11,
                               sourceFund12,
                               sourceFund13,
                               sourceFund14,
                               sourceFund15,
                               sourceFund16,
                               sourceFund17,
                               sourceFund18,
                               sourceFund19,
                               sourceFund20,
                               sourceFundType01,
                               sourceFundType02,
                               sourceFundType03,
                               sourceFundType04,
                               sourceFundType05,
                               sourceFundType06,
                               sourceFundType07,
                               sourceFundType08,
                               sourceFundType09,
                               sourceFundType10,
                               sourceFundType11,
                               sourceFundType12,
                               sourceFundType13,
                               sourceFundType14,
                               sourceFundType15,
                               sourceFundType16,
                               sourceFundType17,
                               sourceFundType18,
                               sourceFundType19,
                               sourceFundType20,
                               sourceFundCurrency01,
                               sourceFundCurrency02,
                               sourceFundCurrency03,
                               sourceFundCurrency04,
                               sourceFundCurrency05,
                               sourceFundCurrency06,
                               sourceFundCurrency07,
                               sourceFundCurrency08,
                               sourceFundCurrency09,
                               sourceFundCurrency10,
                               sourceFundCurrency11,
                               sourceFundCurrency12,
                               sourceFundCurrency13,
                               sourceFundCurrency14,
                               sourceFundCurrency15,
                               sourceFundCurrency16,
                               sourceFundCurrency17,
                               sourceFundCurrency18,
                               sourceFundCurrency19,
                               sourceFundCurrency20,
                               sourcePercentAmount01,
                               sourcePercentAmount02,
                               sourcePercentAmount03,
                               sourcePercentAmount04,
                               sourcePercentAmount05,
                               sourcePercentAmount06,
                               sourcePercentAmount07,
                               sourcePercentAmount08,
                               sourcePercentAmount09,
                               sourcePercentAmount10,
                               sourcePercentAmount11,
                               sourcePercentAmount12,
                               sourcePercentAmount13,
                               sourcePercentAmount14,
                               sourcePercentAmount15,
                               sourcePercentAmount16,
                               sourcePercentAmount17,
                               sourcePercentAmount18,
                               sourcePercentAmount19,
                               sourcePercentAmount20,
                               sourceEstimatedValue01,
                               sourceEstimatedValue02,
                               sourceEstimatedValue03,
                               sourceEstimatedValue04,
                               sourceEstimatedValue05,
                               sourceEstimatedValue06,
                               sourceEstimatedValue07,
                               sourceEstimatedValue08,
                               sourceEstimatedValue09,
                               sourceEstimatedValue10,
                               sourceEstimatedValue11,
                               sourceEstimatedValue12,
                               sourceEstimatedValue13,
                               sourceEstimatedValue14,
                               sourceEstimatedValue15,
                               sourceEstimatedValue16,
                               sourceEstimatedValue17,
                               sourceEstimatedValue18,
                               sourceEstimatedValue19,
                               sourceEstimatedValue20,
                               sourceActualValue01,
                               sourceActualValue02,
                               sourceActualValue03,
                               sourceActualValue04,
                               sourceActualValue05,
                               sourceActualValue06,
                               sourceActualValue07,
                               sourceActualValue08,
                               sourceActualValue09,
                               sourceActualValue10,
                               sourceActualValue11,
                               sourceActualValue12,
                               sourceActualValue13,
                               sourceActualValue14,
                               sourceActualValue15,
                               sourceActualValue16,
                               sourceActualValue17,
                               sourceActualValue18,
                               sourceActualValue19,
                               sourceActualValue20,
                               targetFund01,
                               targetFund02,
                               targetFund03,
                               targetFund04,
                               targetFund05,
                               targetFund06,
                               targetFund07,
                               targetFund08,
                               targetFund09,
                               targetFund10,
                               targetFundType01,
                               targetFundType02,
                               targetFundType03,
                               targetFundType04,
                               targetFundType05,
                               targetFundType06,
                               targetFundType07,
                               targetFundType08,
                               targetFundType09,
                               targetFundType10,
                               targetFundCurrency01,
                               targetFundCurrency02,
                               targetFundCurrency03,
                               targetFundCurrency04,
                               targetFundCurrency05,
                               targetFundCurrency06,
                               targetFundCurrency07,
                               targetFundCurrency08,
                               targetFundCurrency09,
                               targetFundCurrency10,
                               targetPercent01,
                               targetPercent02,
                               targetPercent03,
                               targetPercent04,
                               targetPercent05,
                               targetPercent06,
                               targetPercent07,
                               targetPercent08,
                               targetPercent09,
                               targetPercent10,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(43);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getPlanSuffix().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(planSuffix.toInternal());
	nonKeyFiller4.setInternal(life.toInternal());
	nonKeyFiller5.setInternal(coverage.toInternal());
	nonKeyFiller6.setInternal(rider.toInternal());
	nonKeyFiller7.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(884);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ getEffdate().toInternal()
					+ getPercentAmountInd().toInternal()
					+ getOverrideFee().toInternal()
					+ getSourceFund01().toInternal()
					+ getSourceFund02().toInternal()
					+ getSourceFund03().toInternal()
					+ getSourceFund04().toInternal()
					+ getSourceFund05().toInternal()
					+ getSourceFund06().toInternal()
					+ getSourceFund07().toInternal()
					+ getSourceFund08().toInternal()
					+ getSourceFund09().toInternal()
					+ getSourceFund10().toInternal()
					+ getSourceFund11().toInternal()
					+ getSourceFund12().toInternal()
					+ getSourceFund13().toInternal()
					+ getSourceFund14().toInternal()
					+ getSourceFund15().toInternal()
					+ getSourceFund16().toInternal()
					+ getSourceFund17().toInternal()
					+ getSourceFund18().toInternal()
					+ getSourceFund19().toInternal()
					+ getSourceFund20().toInternal()
					+ getSourceFundType01().toInternal()
					+ getSourceFundType02().toInternal()
					+ getSourceFundType03().toInternal()
					+ getSourceFundType04().toInternal()
					+ getSourceFundType05().toInternal()
					+ getSourceFundType06().toInternal()
					+ getSourceFundType07().toInternal()
					+ getSourceFundType08().toInternal()
					+ getSourceFundType09().toInternal()
					+ getSourceFundType10().toInternal()
					+ getSourceFundType11().toInternal()
					+ getSourceFundType12().toInternal()
					+ getSourceFundType13().toInternal()
					+ getSourceFundType14().toInternal()
					+ getSourceFundType15().toInternal()
					+ getSourceFundType16().toInternal()
					+ getSourceFundType17().toInternal()
					+ getSourceFundType18().toInternal()
					+ getSourceFundType19().toInternal()
					+ getSourceFundType20().toInternal()
					+ getSourceFundCurrency01().toInternal()
					+ getSourceFundCurrency02().toInternal()
					+ getSourceFundCurrency03().toInternal()
					+ getSourceFundCurrency04().toInternal()
					+ getSourceFundCurrency05().toInternal()
					+ getSourceFundCurrency06().toInternal()
					+ getSourceFundCurrency07().toInternal()
					+ getSourceFundCurrency08().toInternal()
					+ getSourceFundCurrency09().toInternal()
					+ getSourceFundCurrency10().toInternal()
					+ getSourceFundCurrency11().toInternal()
					+ getSourceFundCurrency12().toInternal()
					+ getSourceFundCurrency13().toInternal()
					+ getSourceFundCurrency14().toInternal()
					+ getSourceFundCurrency15().toInternal()
					+ getSourceFundCurrency16().toInternal()
					+ getSourceFundCurrency17().toInternal()
					+ getSourceFundCurrency18().toInternal()
					+ getSourceFundCurrency19().toInternal()
					+ getSourceFundCurrency20().toInternal()
					+ getSourcePercentAmount01().toInternal()
					+ getSourcePercentAmount02().toInternal()
					+ getSourcePercentAmount03().toInternal()
					+ getSourcePercentAmount04().toInternal()
					+ getSourcePercentAmount05().toInternal()
					+ getSourcePercentAmount06().toInternal()
					+ getSourcePercentAmount07().toInternal()
					+ getSourcePercentAmount08().toInternal()
					+ getSourcePercentAmount09().toInternal()
					+ getSourcePercentAmount10().toInternal()
					+ getSourcePercentAmount11().toInternal()
					+ getSourcePercentAmount12().toInternal()
					+ getSourcePercentAmount13().toInternal()
					+ getSourcePercentAmount14().toInternal()
					+ getSourcePercentAmount15().toInternal()
					+ getSourcePercentAmount16().toInternal()
					+ getSourcePercentAmount17().toInternal()
					+ getSourcePercentAmount18().toInternal()
					+ getSourcePercentAmount19().toInternal()
					+ getSourcePercentAmount20().toInternal()
					+ getSourceEstimatedValue01().toInternal()
					+ getSourceEstimatedValue02().toInternal()
					+ getSourceEstimatedValue03().toInternal()
					+ getSourceEstimatedValue04().toInternal()
					+ getSourceEstimatedValue05().toInternal()
					+ getSourceEstimatedValue06().toInternal()
					+ getSourceEstimatedValue07().toInternal()
					+ getSourceEstimatedValue08().toInternal()
					+ getSourceEstimatedValue09().toInternal()
					+ getSourceEstimatedValue10().toInternal()
					+ getSourceEstimatedValue11().toInternal()
					+ getSourceEstimatedValue12().toInternal()
					+ getSourceEstimatedValue13().toInternal()
					+ getSourceEstimatedValue14().toInternal()
					+ getSourceEstimatedValue15().toInternal()
					+ getSourceEstimatedValue16().toInternal()
					+ getSourceEstimatedValue17().toInternal()
					+ getSourceEstimatedValue18().toInternal()
					+ getSourceEstimatedValue19().toInternal()
					+ getSourceEstimatedValue20().toInternal()
					+ getSourceActualValue01().toInternal()
					+ getSourceActualValue02().toInternal()
					+ getSourceActualValue03().toInternal()
					+ getSourceActualValue04().toInternal()
					+ getSourceActualValue05().toInternal()
					+ getSourceActualValue06().toInternal()
					+ getSourceActualValue07().toInternal()
					+ getSourceActualValue08().toInternal()
					+ getSourceActualValue09().toInternal()
					+ getSourceActualValue10().toInternal()
					+ getSourceActualValue11().toInternal()
					+ getSourceActualValue12().toInternal()
					+ getSourceActualValue13().toInternal()
					+ getSourceActualValue14().toInternal()
					+ getSourceActualValue15().toInternal()
					+ getSourceActualValue16().toInternal()
					+ getSourceActualValue17().toInternal()
					+ getSourceActualValue18().toInternal()
					+ getSourceActualValue19().toInternal()
					+ getSourceActualValue20().toInternal()
					+ getTargetFund01().toInternal()
					+ getTargetFund02().toInternal()
					+ getTargetFund03().toInternal()
					+ getTargetFund04().toInternal()
					+ getTargetFund05().toInternal()
					+ getTargetFund06().toInternal()
					+ getTargetFund07().toInternal()
					+ getTargetFund08().toInternal()
					+ getTargetFund09().toInternal()
					+ getTargetFund10().toInternal()
					+ getTargetFundType01().toInternal()
					+ getTargetFundType02().toInternal()
					+ getTargetFundType03().toInternal()
					+ getTargetFundType04().toInternal()
					+ getTargetFundType05().toInternal()
					+ getTargetFundType06().toInternal()
					+ getTargetFundType07().toInternal()
					+ getTargetFundType08().toInternal()
					+ getTargetFundType09().toInternal()
					+ getTargetFundType10().toInternal()
					+ getTargetFundCurrency01().toInternal()
					+ getTargetFundCurrency02().toInternal()
					+ getTargetFundCurrency03().toInternal()
					+ getTargetFundCurrency04().toInternal()
					+ getTargetFundCurrency05().toInternal()
					+ getTargetFundCurrency06().toInternal()
					+ getTargetFundCurrency07().toInternal()
					+ getTargetFundCurrency08().toInternal()
					+ getTargetFundCurrency09().toInternal()
					+ getTargetFundCurrency10().toInternal()
					+ getTargetPercent01().toInternal()
					+ getTargetPercent02().toInternal()
					+ getTargetPercent03().toInternal()
					+ getTargetPercent04().toInternal()
					+ getTargetPercent05().toInternal()
					+ getTargetPercent06().toInternal()
					+ getTargetPercent07().toInternal()
					+ getTargetPercent08().toInternal()
					+ getTargetPercent09().toInternal()
					+ getTargetPercent10().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, percentAmountInd);
			what = ExternalData.chop(what, overrideFee);
			what = ExternalData.chop(what, sourceFund01);
			what = ExternalData.chop(what, sourceFund02);
			what = ExternalData.chop(what, sourceFund03);
			what = ExternalData.chop(what, sourceFund04);
			what = ExternalData.chop(what, sourceFund05);
			what = ExternalData.chop(what, sourceFund06);
			what = ExternalData.chop(what, sourceFund07);
			what = ExternalData.chop(what, sourceFund08);
			what = ExternalData.chop(what, sourceFund09);
			what = ExternalData.chop(what, sourceFund10);
			what = ExternalData.chop(what, sourceFund11);
			what = ExternalData.chop(what, sourceFund12);
			what = ExternalData.chop(what, sourceFund13);
			what = ExternalData.chop(what, sourceFund14);
			what = ExternalData.chop(what, sourceFund15);
			what = ExternalData.chop(what, sourceFund16);
			what = ExternalData.chop(what, sourceFund17);
			what = ExternalData.chop(what, sourceFund18);
			what = ExternalData.chop(what, sourceFund19);
			what = ExternalData.chop(what, sourceFund20);
			what = ExternalData.chop(what, sourceFundType01);
			what = ExternalData.chop(what, sourceFundType02);
			what = ExternalData.chop(what, sourceFundType03);
			what = ExternalData.chop(what, sourceFundType04);
			what = ExternalData.chop(what, sourceFundType05);
			what = ExternalData.chop(what, sourceFundType06);
			what = ExternalData.chop(what, sourceFundType07);
			what = ExternalData.chop(what, sourceFundType08);
			what = ExternalData.chop(what, sourceFundType09);
			what = ExternalData.chop(what, sourceFundType10);
			what = ExternalData.chop(what, sourceFundType11);
			what = ExternalData.chop(what, sourceFundType12);
			what = ExternalData.chop(what, sourceFundType13);
			what = ExternalData.chop(what, sourceFundType14);
			what = ExternalData.chop(what, sourceFundType15);
			what = ExternalData.chop(what, sourceFundType16);
			what = ExternalData.chop(what, sourceFundType17);
			what = ExternalData.chop(what, sourceFundType18);
			what = ExternalData.chop(what, sourceFundType19);
			what = ExternalData.chop(what, sourceFundType20);
			what = ExternalData.chop(what, sourceFundCurrency01);
			what = ExternalData.chop(what, sourceFundCurrency02);
			what = ExternalData.chop(what, sourceFundCurrency03);
			what = ExternalData.chop(what, sourceFundCurrency04);
			what = ExternalData.chop(what, sourceFundCurrency05);
			what = ExternalData.chop(what, sourceFundCurrency06);
			what = ExternalData.chop(what, sourceFundCurrency07);
			what = ExternalData.chop(what, sourceFundCurrency08);
			what = ExternalData.chop(what, sourceFundCurrency09);
			what = ExternalData.chop(what, sourceFundCurrency10);
			what = ExternalData.chop(what, sourceFundCurrency11);
			what = ExternalData.chop(what, sourceFundCurrency12);
			what = ExternalData.chop(what, sourceFundCurrency13);
			what = ExternalData.chop(what, sourceFundCurrency14);
			what = ExternalData.chop(what, sourceFundCurrency15);
			what = ExternalData.chop(what, sourceFundCurrency16);
			what = ExternalData.chop(what, sourceFundCurrency17);
			what = ExternalData.chop(what, sourceFundCurrency18);
			what = ExternalData.chop(what, sourceFundCurrency19);
			what = ExternalData.chop(what, sourceFundCurrency20);
			what = ExternalData.chop(what, sourcePercentAmount01);
			what = ExternalData.chop(what, sourcePercentAmount02);
			what = ExternalData.chop(what, sourcePercentAmount03);
			what = ExternalData.chop(what, sourcePercentAmount04);
			what = ExternalData.chop(what, sourcePercentAmount05);
			what = ExternalData.chop(what, sourcePercentAmount06);
			what = ExternalData.chop(what, sourcePercentAmount07);
			what = ExternalData.chop(what, sourcePercentAmount08);
			what = ExternalData.chop(what, sourcePercentAmount09);
			what = ExternalData.chop(what, sourcePercentAmount10);
			what = ExternalData.chop(what, sourcePercentAmount11);
			what = ExternalData.chop(what, sourcePercentAmount12);
			what = ExternalData.chop(what, sourcePercentAmount13);
			what = ExternalData.chop(what, sourcePercentAmount14);
			what = ExternalData.chop(what, sourcePercentAmount15);
			what = ExternalData.chop(what, sourcePercentAmount16);
			what = ExternalData.chop(what, sourcePercentAmount17);
			what = ExternalData.chop(what, sourcePercentAmount18);
			what = ExternalData.chop(what, sourcePercentAmount19);
			what = ExternalData.chop(what, sourcePercentAmount20);
			what = ExternalData.chop(what, sourceEstimatedValue01);
			what = ExternalData.chop(what, sourceEstimatedValue02);
			what = ExternalData.chop(what, sourceEstimatedValue03);
			what = ExternalData.chop(what, sourceEstimatedValue04);
			what = ExternalData.chop(what, sourceEstimatedValue05);
			what = ExternalData.chop(what, sourceEstimatedValue06);
			what = ExternalData.chop(what, sourceEstimatedValue07);
			what = ExternalData.chop(what, sourceEstimatedValue08);
			what = ExternalData.chop(what, sourceEstimatedValue09);
			what = ExternalData.chop(what, sourceEstimatedValue10);
			what = ExternalData.chop(what, sourceEstimatedValue11);
			what = ExternalData.chop(what, sourceEstimatedValue12);
			what = ExternalData.chop(what, sourceEstimatedValue13);
			what = ExternalData.chop(what, sourceEstimatedValue14);
			what = ExternalData.chop(what, sourceEstimatedValue15);
			what = ExternalData.chop(what, sourceEstimatedValue16);
			what = ExternalData.chop(what, sourceEstimatedValue17);
			what = ExternalData.chop(what, sourceEstimatedValue18);
			what = ExternalData.chop(what, sourceEstimatedValue19);
			what = ExternalData.chop(what, sourceEstimatedValue20);
			what = ExternalData.chop(what, sourceActualValue01);
			what = ExternalData.chop(what, sourceActualValue02);
			what = ExternalData.chop(what, sourceActualValue03);
			what = ExternalData.chop(what, sourceActualValue04);
			what = ExternalData.chop(what, sourceActualValue05);
			what = ExternalData.chop(what, sourceActualValue06);
			what = ExternalData.chop(what, sourceActualValue07);
			what = ExternalData.chop(what, sourceActualValue08);
			what = ExternalData.chop(what, sourceActualValue09);
			what = ExternalData.chop(what, sourceActualValue10);
			what = ExternalData.chop(what, sourceActualValue11);
			what = ExternalData.chop(what, sourceActualValue12);
			what = ExternalData.chop(what, sourceActualValue13);
			what = ExternalData.chop(what, sourceActualValue14);
			what = ExternalData.chop(what, sourceActualValue15);
			what = ExternalData.chop(what, sourceActualValue16);
			what = ExternalData.chop(what, sourceActualValue17);
			what = ExternalData.chop(what, sourceActualValue18);
			what = ExternalData.chop(what, sourceActualValue19);
			what = ExternalData.chop(what, sourceActualValue20);
			what = ExternalData.chop(what, targetFund01);
			what = ExternalData.chop(what, targetFund02);
			what = ExternalData.chop(what, targetFund03);
			what = ExternalData.chop(what, targetFund04);
			what = ExternalData.chop(what, targetFund05);
			what = ExternalData.chop(what, targetFund06);
			what = ExternalData.chop(what, targetFund07);
			what = ExternalData.chop(what, targetFund08);
			what = ExternalData.chop(what, targetFund09);
			what = ExternalData.chop(what, targetFund10);
			what = ExternalData.chop(what, targetFundType01);
			what = ExternalData.chop(what, targetFundType02);
			what = ExternalData.chop(what, targetFundType03);
			what = ExternalData.chop(what, targetFundType04);
			what = ExternalData.chop(what, targetFundType05);
			what = ExternalData.chop(what, targetFundType06);
			what = ExternalData.chop(what, targetFundType07);
			what = ExternalData.chop(what, targetFundType08);
			what = ExternalData.chop(what, targetFundType09);
			what = ExternalData.chop(what, targetFundType10);
			what = ExternalData.chop(what, targetFundCurrency01);
			what = ExternalData.chop(what, targetFundCurrency02);
			what = ExternalData.chop(what, targetFundCurrency03);
			what = ExternalData.chop(what, targetFundCurrency04);
			what = ExternalData.chop(what, targetFundCurrency05);
			what = ExternalData.chop(what, targetFundCurrency06);
			what = ExternalData.chop(what, targetFundCurrency07);
			what = ExternalData.chop(what, targetFundCurrency08);
			what = ExternalData.chop(what, targetFundCurrency09);
			what = ExternalData.chop(what, targetFundCurrency10);
			what = ExternalData.chop(what, targetPercent01);
			what = ExternalData.chop(what, targetPercent02);
			what = ExternalData.chop(what, targetPercent03);
			what = ExternalData.chop(what, targetPercent04);
			what = ExternalData.chop(what, targetPercent05);
			what = ExternalData.chop(what, targetPercent06);
			what = ExternalData.chop(what, targetPercent07);
			what = ExternalData.chop(what, targetPercent08);
			what = ExternalData.chop(what, targetPercent09);
			what = ExternalData.chop(what, targetPercent10);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getPercentAmountInd() {
		return percentAmountInd;
	}
	public void setPercentAmountInd(Object what) {
		percentAmountInd.set(what);
	}	
	public FixedLengthStringData getOverrideFee() {
		return overrideFee;
	}
	public void setOverrideFee(Object what) {
		overrideFee.set(what);
	}	
	public FixedLengthStringData getSourceFund01() {
		return sourceFund01;
	}
	public void setSourceFund01(Object what) {
		sourceFund01.set(what);
	}	
	public FixedLengthStringData getSourceFund02() {
		return sourceFund02;
	}
	public void setSourceFund02(Object what) {
		sourceFund02.set(what);
	}	
	public FixedLengthStringData getSourceFund03() {
		return sourceFund03;
	}
	public void setSourceFund03(Object what) {
		sourceFund03.set(what);
	}	
	public FixedLengthStringData getSourceFund04() {
		return sourceFund04;
	}
	public void setSourceFund04(Object what) {
		sourceFund04.set(what);
	}	
	public FixedLengthStringData getSourceFund05() {
		return sourceFund05;
	}
	public void setSourceFund05(Object what) {
		sourceFund05.set(what);
	}	
	public FixedLengthStringData getSourceFund06() {
		return sourceFund06;
	}
	public void setSourceFund06(Object what) {
		sourceFund06.set(what);
	}	
	public FixedLengthStringData getSourceFund07() {
		return sourceFund07;
	}
	public void setSourceFund07(Object what) {
		sourceFund07.set(what);
	}	
	public FixedLengthStringData getSourceFund08() {
		return sourceFund08;
	}
	public void setSourceFund08(Object what) {
		sourceFund08.set(what);
	}	
	public FixedLengthStringData getSourceFund09() {
		return sourceFund09;
	}
	public void setSourceFund09(Object what) {
		sourceFund09.set(what);
	}	
	public FixedLengthStringData getSourceFund10() {
		return sourceFund10;
	}
	public void setSourceFund10(Object what) {
		sourceFund10.set(what);
	}	
	public FixedLengthStringData getSourceFund11() {
		return sourceFund11;
	}
	public void setSourceFund11(Object what) {
		sourceFund11.set(what);
	}	
	public FixedLengthStringData getSourceFund12() {
		return sourceFund12;
	}
	public void setSourceFund12(Object what) {
		sourceFund12.set(what);
	}	
	public FixedLengthStringData getSourceFund13() {
		return sourceFund13;
	}
	public void setSourceFund13(Object what) {
		sourceFund13.set(what);
	}	
	public FixedLengthStringData getSourceFund14() {
		return sourceFund14;
	}
	public void setSourceFund14(Object what) {
		sourceFund14.set(what);
	}	
	public FixedLengthStringData getSourceFund15() {
		return sourceFund15;
	}
	public void setSourceFund15(Object what) {
		sourceFund15.set(what);
	}	
	public FixedLengthStringData getSourceFund16() {
		return sourceFund16;
	}
	public void setSourceFund16(Object what) {
		sourceFund16.set(what);
	}	
	public FixedLengthStringData getSourceFund17() {
		return sourceFund17;
	}
	public void setSourceFund17(Object what) {
		sourceFund17.set(what);
	}	
	public FixedLengthStringData getSourceFund18() {
		return sourceFund18;
	}
	public void setSourceFund18(Object what) {
		sourceFund18.set(what);
	}	
	public FixedLengthStringData getSourceFund19() {
		return sourceFund19;
	}
	public void setSourceFund19(Object what) {
		sourceFund19.set(what);
	}	
	public FixedLengthStringData getSourceFund20() {
		return sourceFund20;
	}
	public void setSourceFund20(Object what) {
		sourceFund20.set(what);
	}	
	public FixedLengthStringData getSourceFundType01() {
		return sourceFundType01;
	}
	public void setSourceFundType01(Object what) {
		sourceFundType01.set(what);
	}	
	public FixedLengthStringData getSourceFundType02() {
		return sourceFundType02;
	}
	public void setSourceFundType02(Object what) {
		sourceFundType02.set(what);
	}	
	public FixedLengthStringData getSourceFundType03() {
		return sourceFundType03;
	}
	public void setSourceFundType03(Object what) {
		sourceFundType03.set(what);
	}	
	public FixedLengthStringData getSourceFundType04() {
		return sourceFundType04;
	}
	public void setSourceFundType04(Object what) {
		sourceFundType04.set(what);
	}	
	public FixedLengthStringData getSourceFundType05() {
		return sourceFundType05;
	}
	public void setSourceFundType05(Object what) {
		sourceFundType05.set(what);
	}	
	public FixedLengthStringData getSourceFundType06() {
		return sourceFundType06;
	}
	public void setSourceFundType06(Object what) {
		sourceFundType06.set(what);
	}	
	public FixedLengthStringData getSourceFundType07() {
		return sourceFundType07;
	}
	public void setSourceFundType07(Object what) {
		sourceFundType07.set(what);
	}	
	public FixedLengthStringData getSourceFundType08() {
		return sourceFundType08;
	}
	public void setSourceFundType08(Object what) {
		sourceFundType08.set(what);
	}	
	public FixedLengthStringData getSourceFundType09() {
		return sourceFundType09;
	}
	public void setSourceFundType09(Object what) {
		sourceFundType09.set(what);
	}	
	public FixedLengthStringData getSourceFundType10() {
		return sourceFundType10;
	}
	public void setSourceFundType10(Object what) {
		sourceFundType10.set(what);
	}	
	public FixedLengthStringData getSourceFundType11() {
		return sourceFundType11;
	}
	public void setSourceFundType11(Object what) {
		sourceFundType11.set(what);
	}	
	public FixedLengthStringData getSourceFundType12() {
		return sourceFundType12;
	}
	public void setSourceFundType12(Object what) {
		sourceFundType12.set(what);
	}	
	public FixedLengthStringData getSourceFundType13() {
		return sourceFundType13;
	}
	public void setSourceFundType13(Object what) {
		sourceFundType13.set(what);
	}	
	public FixedLengthStringData getSourceFundType14() {
		return sourceFundType14;
	}
	public void setSourceFundType14(Object what) {
		sourceFundType14.set(what);
	}	
	public FixedLengthStringData getSourceFundType15() {
		return sourceFundType15;
	}
	public void setSourceFundType15(Object what) {
		sourceFundType15.set(what);
	}	
	public FixedLengthStringData getSourceFundType16() {
		return sourceFundType16;
	}
	public void setSourceFundType16(Object what) {
		sourceFundType16.set(what);
	}	
	public FixedLengthStringData getSourceFundType17() {
		return sourceFundType17;
	}
	public void setSourceFundType17(Object what) {
		sourceFundType17.set(what);
	}	
	public FixedLengthStringData getSourceFundType18() {
		return sourceFundType18;
	}
	public void setSourceFundType18(Object what) {
		sourceFundType18.set(what);
	}	
	public FixedLengthStringData getSourceFundType19() {
		return sourceFundType19;
	}
	public void setSourceFundType19(Object what) {
		sourceFundType19.set(what);
	}	
	public FixedLengthStringData getSourceFundType20() {
		return sourceFundType20;
	}
	public void setSourceFundType20(Object what) {
		sourceFundType20.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency01() {
		return sourceFundCurrency01;
	}
	public void setSourceFundCurrency01(Object what) {
		sourceFundCurrency01.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency02() {
		return sourceFundCurrency02;
	}
	public void setSourceFundCurrency02(Object what) {
		sourceFundCurrency02.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency03() {
		return sourceFundCurrency03;
	}
	public void setSourceFundCurrency03(Object what) {
		sourceFundCurrency03.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency04() {
		return sourceFundCurrency04;
	}
	public void setSourceFundCurrency04(Object what) {
		sourceFundCurrency04.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency05() {
		return sourceFundCurrency05;
	}
	public void setSourceFundCurrency05(Object what) {
		sourceFundCurrency05.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency06() {
		return sourceFundCurrency06;
	}
	public void setSourceFundCurrency06(Object what) {
		sourceFundCurrency06.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency07() {
		return sourceFundCurrency07;
	}
	public void setSourceFundCurrency07(Object what) {
		sourceFundCurrency07.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency08() {
		return sourceFundCurrency08;
	}
	public void setSourceFundCurrency08(Object what) {
		sourceFundCurrency08.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency09() {
		return sourceFundCurrency09;
	}
	public void setSourceFundCurrency09(Object what) {
		sourceFundCurrency09.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency10() {
		return sourceFundCurrency10;
	}
	public void setSourceFundCurrency10(Object what) {
		sourceFundCurrency10.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency11() {
		return sourceFundCurrency11;
	}
	public void setSourceFundCurrency11(Object what) {
		sourceFundCurrency11.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency12() {
		return sourceFundCurrency12;
	}
	public void setSourceFundCurrency12(Object what) {
		sourceFundCurrency12.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency13() {
		return sourceFundCurrency13;
	}
	public void setSourceFundCurrency13(Object what) {
		sourceFundCurrency13.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency14() {
		return sourceFundCurrency14;
	}
	public void setSourceFundCurrency14(Object what) {
		sourceFundCurrency14.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency15() {
		return sourceFundCurrency15;
	}
	public void setSourceFundCurrency15(Object what) {
		sourceFundCurrency15.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency16() {
		return sourceFundCurrency16;
	}
	public void setSourceFundCurrency16(Object what) {
		sourceFundCurrency16.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency17() {
		return sourceFundCurrency17;
	}
	public void setSourceFundCurrency17(Object what) {
		sourceFundCurrency17.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency18() {
		return sourceFundCurrency18;
	}
	public void setSourceFundCurrency18(Object what) {
		sourceFundCurrency18.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency19() {
		return sourceFundCurrency19;
	}
	public void setSourceFundCurrency19(Object what) {
		sourceFundCurrency19.set(what);
	}	
	public FixedLengthStringData getSourceFundCurrency20() {
		return sourceFundCurrency20;
	}
	public void setSourceFundCurrency20(Object what) {
		sourceFundCurrency20.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount01() {
		return sourcePercentAmount01;
	}
	public void setSourcePercentAmount01(Object what) {
		setSourcePercentAmount01(what, false);
	}
	public void setSourcePercentAmount01(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount01.setRounded(what);
		else
			sourcePercentAmount01.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount02() {
		return sourcePercentAmount02;
	}
	public void setSourcePercentAmount02(Object what) {
		setSourcePercentAmount02(what, false);
	}
	public void setSourcePercentAmount02(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount02.setRounded(what);
		else
			sourcePercentAmount02.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount03() {
		return sourcePercentAmount03;
	}
	public void setSourcePercentAmount03(Object what) {
		setSourcePercentAmount03(what, false);
	}
	public void setSourcePercentAmount03(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount03.setRounded(what);
		else
			sourcePercentAmount03.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount04() {
		return sourcePercentAmount04;
	}
	public void setSourcePercentAmount04(Object what) {
		setSourcePercentAmount04(what, false);
	}
	public void setSourcePercentAmount04(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount04.setRounded(what);
		else
			sourcePercentAmount04.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount05() {
		return sourcePercentAmount05;
	}
	public void setSourcePercentAmount05(Object what) {
		setSourcePercentAmount05(what, false);
	}
	public void setSourcePercentAmount05(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount05.setRounded(what);
		else
			sourcePercentAmount05.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount06() {
		return sourcePercentAmount06;
	}
	public void setSourcePercentAmount06(Object what) {
		setSourcePercentAmount06(what, false);
	}
	public void setSourcePercentAmount06(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount06.setRounded(what);
		else
			sourcePercentAmount06.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount07() {
		return sourcePercentAmount07;
	}
	public void setSourcePercentAmount07(Object what) {
		setSourcePercentAmount07(what, false);
	}
	public void setSourcePercentAmount07(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount07.setRounded(what);
		else
			sourcePercentAmount07.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount08() {
		return sourcePercentAmount08;
	}
	public void setSourcePercentAmount08(Object what) {
		setSourcePercentAmount08(what, false);
	}
	public void setSourcePercentAmount08(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount08.setRounded(what);
		else
			sourcePercentAmount08.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount09() {
		return sourcePercentAmount09;
	}
	public void setSourcePercentAmount09(Object what) {
		setSourcePercentAmount09(what, false);
	}
	public void setSourcePercentAmount09(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount09.setRounded(what);
		else
			sourcePercentAmount09.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount10() {
		return sourcePercentAmount10;
	}
	public void setSourcePercentAmount10(Object what) {
		setSourcePercentAmount10(what, false);
	}
	public void setSourcePercentAmount10(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount10.setRounded(what);
		else
			sourcePercentAmount10.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount11() {
		return sourcePercentAmount11;
	}
	public void setSourcePercentAmount11(Object what) {
		setSourcePercentAmount11(what, false);
	}
	public void setSourcePercentAmount11(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount11.setRounded(what);
		else
			sourcePercentAmount11.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount12() {
		return sourcePercentAmount12;
	}
	public void setSourcePercentAmount12(Object what) {
		setSourcePercentAmount12(what, false);
	}
	public void setSourcePercentAmount12(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount12.setRounded(what);
		else
			sourcePercentAmount12.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount13() {
		return sourcePercentAmount13;
	}
	public void setSourcePercentAmount13(Object what) {
		setSourcePercentAmount13(what, false);
	}
	public void setSourcePercentAmount13(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount13.setRounded(what);
		else
			sourcePercentAmount13.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount14() {
		return sourcePercentAmount14;
	}
	public void setSourcePercentAmount14(Object what) {
		setSourcePercentAmount14(what, false);
	}
	public void setSourcePercentAmount14(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount14.setRounded(what);
		else
			sourcePercentAmount14.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount15() {
		return sourcePercentAmount15;
	}
	public void setSourcePercentAmount15(Object what) {
		setSourcePercentAmount15(what, false);
	}
	public void setSourcePercentAmount15(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount15.setRounded(what);
		else
			sourcePercentAmount15.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount16() {
		return sourcePercentAmount16;
	}
	public void setSourcePercentAmount16(Object what) {
		setSourcePercentAmount16(what, false);
	}
	public void setSourcePercentAmount16(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount16.setRounded(what);
		else
			sourcePercentAmount16.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount17() {
		return sourcePercentAmount17;
	}
	public void setSourcePercentAmount17(Object what) {
		setSourcePercentAmount17(what, false);
	}
	public void setSourcePercentAmount17(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount17.setRounded(what);
		else
			sourcePercentAmount17.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount18() {
		return sourcePercentAmount18;
	}
	public void setSourcePercentAmount18(Object what) {
		setSourcePercentAmount18(what, false);
	}
	public void setSourcePercentAmount18(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount18.setRounded(what);
		else
			sourcePercentAmount18.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount19() {
		return sourcePercentAmount19;
	}
	public void setSourcePercentAmount19(Object what) {
		setSourcePercentAmount19(what, false);
	}
	public void setSourcePercentAmount19(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount19.setRounded(what);
		else
			sourcePercentAmount19.set(what);
	}	
	public PackedDecimalData getSourcePercentAmount20() {
		return sourcePercentAmount20;
	}
	public void setSourcePercentAmount20(Object what) {
		setSourcePercentAmount20(what, false);
	}
	public void setSourcePercentAmount20(Object what, boolean rounded) {
		if (rounded)
			sourcePercentAmount20.setRounded(what);
		else
			sourcePercentAmount20.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue01() {
		return sourceEstimatedValue01;
	}
	public void setSourceEstimatedValue01(Object what) {
		setSourceEstimatedValue01(what, false);
	}
	public void setSourceEstimatedValue01(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue01.setRounded(what);
		else
			sourceEstimatedValue01.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue02() {
		return sourceEstimatedValue02;
	}
	public void setSourceEstimatedValue02(Object what) {
		setSourceEstimatedValue02(what, false);
	}
	public void setSourceEstimatedValue02(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue02.setRounded(what);
		else
			sourceEstimatedValue02.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue03() {
		return sourceEstimatedValue03;
	}
	public void setSourceEstimatedValue03(Object what) {
		setSourceEstimatedValue03(what, false);
	}
	public void setSourceEstimatedValue03(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue03.setRounded(what);
		else
			sourceEstimatedValue03.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue04() {
		return sourceEstimatedValue04;
	}
	public void setSourceEstimatedValue04(Object what) {
		setSourceEstimatedValue04(what, false);
	}
	public void setSourceEstimatedValue04(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue04.setRounded(what);
		else
			sourceEstimatedValue04.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue05() {
		return sourceEstimatedValue05;
	}
	public void setSourceEstimatedValue05(Object what) {
		setSourceEstimatedValue05(what, false);
	}
	public void setSourceEstimatedValue05(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue05.setRounded(what);
		else
			sourceEstimatedValue05.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue06() {
		return sourceEstimatedValue06;
	}
	public void setSourceEstimatedValue06(Object what) {
		setSourceEstimatedValue06(what, false);
	}
	public void setSourceEstimatedValue06(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue06.setRounded(what);
		else
			sourceEstimatedValue06.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue07() {
		return sourceEstimatedValue07;
	}
	public void setSourceEstimatedValue07(Object what) {
		setSourceEstimatedValue07(what, false);
	}
	public void setSourceEstimatedValue07(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue07.setRounded(what);
		else
			sourceEstimatedValue07.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue08() {
		return sourceEstimatedValue08;
	}
	public void setSourceEstimatedValue08(Object what) {
		setSourceEstimatedValue08(what, false);
	}
	public void setSourceEstimatedValue08(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue08.setRounded(what);
		else
			sourceEstimatedValue08.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue09() {
		return sourceEstimatedValue09;
	}
	public void setSourceEstimatedValue09(Object what) {
		setSourceEstimatedValue09(what, false);
	}
	public void setSourceEstimatedValue09(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue09.setRounded(what);
		else
			sourceEstimatedValue09.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue10() {
		return sourceEstimatedValue10;
	}
	public void setSourceEstimatedValue10(Object what) {
		setSourceEstimatedValue10(what, false);
	}
	public void setSourceEstimatedValue10(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue10.setRounded(what);
		else
			sourceEstimatedValue10.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue11() {
		return sourceEstimatedValue11;
	}
	public void setSourceEstimatedValue11(Object what) {
		setSourceEstimatedValue11(what, false);
	}
	public void setSourceEstimatedValue11(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue11.setRounded(what);
		else
			sourceEstimatedValue11.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue12() {
		return sourceEstimatedValue12;
	}
	public void setSourceEstimatedValue12(Object what) {
		setSourceEstimatedValue12(what, false);
	}
	public void setSourceEstimatedValue12(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue12.setRounded(what);
		else
			sourceEstimatedValue12.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue13() {
		return sourceEstimatedValue13;
	}
	public void setSourceEstimatedValue13(Object what) {
		setSourceEstimatedValue13(what, false);
	}
	public void setSourceEstimatedValue13(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue13.setRounded(what);
		else
			sourceEstimatedValue13.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue14() {
		return sourceEstimatedValue14;
	}
	public void setSourceEstimatedValue14(Object what) {
		setSourceEstimatedValue14(what, false);
	}
	public void setSourceEstimatedValue14(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue14.setRounded(what);
		else
			sourceEstimatedValue14.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue15() {
		return sourceEstimatedValue15;
	}
	public void setSourceEstimatedValue15(Object what) {
		setSourceEstimatedValue15(what, false);
	}
	public void setSourceEstimatedValue15(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue15.setRounded(what);
		else
			sourceEstimatedValue15.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue16() {
		return sourceEstimatedValue16;
	}
	public void setSourceEstimatedValue16(Object what) {
		setSourceEstimatedValue16(what, false);
	}
	public void setSourceEstimatedValue16(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue16.setRounded(what);
		else
			sourceEstimatedValue16.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue17() {
		return sourceEstimatedValue17;
	}
	public void setSourceEstimatedValue17(Object what) {
		setSourceEstimatedValue17(what, false);
	}
	public void setSourceEstimatedValue17(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue17.setRounded(what);
		else
			sourceEstimatedValue17.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue18() {
		return sourceEstimatedValue18;
	}
	public void setSourceEstimatedValue18(Object what) {
		setSourceEstimatedValue18(what, false);
	}
	public void setSourceEstimatedValue18(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue18.setRounded(what);
		else
			sourceEstimatedValue18.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue19() {
		return sourceEstimatedValue19;
	}
	public void setSourceEstimatedValue19(Object what) {
		setSourceEstimatedValue19(what, false);
	}
	public void setSourceEstimatedValue19(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue19.setRounded(what);
		else
			sourceEstimatedValue19.set(what);
	}	
	public PackedDecimalData getSourceEstimatedValue20() {
		return sourceEstimatedValue20;
	}
	public void setSourceEstimatedValue20(Object what) {
		setSourceEstimatedValue20(what, false);
	}
	public void setSourceEstimatedValue20(Object what, boolean rounded) {
		if (rounded)
			sourceEstimatedValue20.setRounded(what);
		else
			sourceEstimatedValue20.set(what);
	}	
	public PackedDecimalData getSourceActualValue01() {
		return sourceActualValue01;
	}
	public void setSourceActualValue01(Object what) {
		setSourceActualValue01(what, false);
	}
	public void setSourceActualValue01(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue01.setRounded(what);
		else
			sourceActualValue01.set(what);
	}	
	public PackedDecimalData getSourceActualValue02() {
		return sourceActualValue02;
	}
	public void setSourceActualValue02(Object what) {
		setSourceActualValue02(what, false);
	}
	public void setSourceActualValue02(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue02.setRounded(what);
		else
			sourceActualValue02.set(what);
	}	
	public PackedDecimalData getSourceActualValue03() {
		return sourceActualValue03;
	}
	public void setSourceActualValue03(Object what) {
		setSourceActualValue03(what, false);
	}
	public void setSourceActualValue03(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue03.setRounded(what);
		else
			sourceActualValue03.set(what);
	}	
	public PackedDecimalData getSourceActualValue04() {
		return sourceActualValue04;
	}
	public void setSourceActualValue04(Object what) {
		setSourceActualValue04(what, false);
	}
	public void setSourceActualValue04(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue04.setRounded(what);
		else
			sourceActualValue04.set(what);
	}	
	public PackedDecimalData getSourceActualValue05() {
		return sourceActualValue05;
	}
	public void setSourceActualValue05(Object what) {
		setSourceActualValue05(what, false);
	}
	public void setSourceActualValue05(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue05.setRounded(what);
		else
			sourceActualValue05.set(what);
	}	
	public PackedDecimalData getSourceActualValue06() {
		return sourceActualValue06;
	}
	public void setSourceActualValue06(Object what) {
		setSourceActualValue06(what, false);
	}
	public void setSourceActualValue06(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue06.setRounded(what);
		else
			sourceActualValue06.set(what);
	}	
	public PackedDecimalData getSourceActualValue07() {
		return sourceActualValue07;
	}
	public void setSourceActualValue07(Object what) {
		setSourceActualValue07(what, false);
	}
	public void setSourceActualValue07(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue07.setRounded(what);
		else
			sourceActualValue07.set(what);
	}	
	public PackedDecimalData getSourceActualValue08() {
		return sourceActualValue08;
	}
	public void setSourceActualValue08(Object what) {
		setSourceActualValue08(what, false);
	}
	public void setSourceActualValue08(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue08.setRounded(what);
		else
			sourceActualValue08.set(what);
	}	
	public PackedDecimalData getSourceActualValue09() {
		return sourceActualValue09;
	}
	public void setSourceActualValue09(Object what) {
		setSourceActualValue09(what, false);
	}
	public void setSourceActualValue09(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue09.setRounded(what);
		else
			sourceActualValue09.set(what);
	}	
	public PackedDecimalData getSourceActualValue10() {
		return sourceActualValue10;
	}
	public void setSourceActualValue10(Object what) {
		setSourceActualValue10(what, false);
	}
	public void setSourceActualValue10(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue10.setRounded(what);
		else
			sourceActualValue10.set(what);
	}	
	public PackedDecimalData getSourceActualValue11() {
		return sourceActualValue11;
	}
	public void setSourceActualValue11(Object what) {
		setSourceActualValue11(what, false);
	}
	public void setSourceActualValue11(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue11.setRounded(what);
		else
			sourceActualValue11.set(what);
	}	
	public PackedDecimalData getSourceActualValue12() {
		return sourceActualValue12;
	}
	public void setSourceActualValue12(Object what) {
		setSourceActualValue12(what, false);
	}
	public void setSourceActualValue12(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue12.setRounded(what);
		else
			sourceActualValue12.set(what);
	}	
	public PackedDecimalData getSourceActualValue13() {
		return sourceActualValue13;
	}
	public void setSourceActualValue13(Object what) {
		setSourceActualValue13(what, false);
	}
	public void setSourceActualValue13(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue13.setRounded(what);
		else
			sourceActualValue13.set(what);
	}	
	public PackedDecimalData getSourceActualValue14() {
		return sourceActualValue14;
	}
	public void setSourceActualValue14(Object what) {
		setSourceActualValue14(what, false);
	}
	public void setSourceActualValue14(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue14.setRounded(what);
		else
			sourceActualValue14.set(what);
	}	
	public PackedDecimalData getSourceActualValue15() {
		return sourceActualValue15;
	}
	public void setSourceActualValue15(Object what) {
		setSourceActualValue15(what, false);
	}
	public void setSourceActualValue15(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue15.setRounded(what);
		else
			sourceActualValue15.set(what);
	}	
	public PackedDecimalData getSourceActualValue16() {
		return sourceActualValue16;
	}
	public void setSourceActualValue16(Object what) {
		setSourceActualValue16(what, false);
	}
	public void setSourceActualValue16(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue16.setRounded(what);
		else
			sourceActualValue16.set(what);
	}	
	public PackedDecimalData getSourceActualValue17() {
		return sourceActualValue17;
	}
	public void setSourceActualValue17(Object what) {
		setSourceActualValue17(what, false);
	}
	public void setSourceActualValue17(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue17.setRounded(what);
		else
			sourceActualValue17.set(what);
	}	
	public PackedDecimalData getSourceActualValue18() {
		return sourceActualValue18;
	}
	public void setSourceActualValue18(Object what) {
		setSourceActualValue18(what, false);
	}
	public void setSourceActualValue18(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue18.setRounded(what);
		else
			sourceActualValue18.set(what);
	}	
	public PackedDecimalData getSourceActualValue19() {
		return sourceActualValue19;
	}
	public void setSourceActualValue19(Object what) {
		setSourceActualValue19(what, false);
	}
	public void setSourceActualValue19(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue19.setRounded(what);
		else
			sourceActualValue19.set(what);
	}	
	public PackedDecimalData getSourceActualValue20() {
		return sourceActualValue20;
	}
	public void setSourceActualValue20(Object what) {
		setSourceActualValue20(what, false);
	}
	public void setSourceActualValue20(Object what, boolean rounded) {
		if (rounded)
			sourceActualValue20.setRounded(what);
		else
			sourceActualValue20.set(what);
	}	
	public FixedLengthStringData getTargetFund01() {
		return targetFund01;
	}
	public void setTargetFund01(Object what) {
		targetFund01.set(what);
	}	
	public FixedLengthStringData getTargetFund02() {
		return targetFund02;
	}
	public void setTargetFund02(Object what) {
		targetFund02.set(what);
	}	
	public FixedLengthStringData getTargetFund03() {
		return targetFund03;
	}
	public void setTargetFund03(Object what) {
		targetFund03.set(what);
	}	
	public FixedLengthStringData getTargetFund04() {
		return targetFund04;
	}
	public void setTargetFund04(Object what) {
		targetFund04.set(what);
	}	
	public FixedLengthStringData getTargetFund05() {
		return targetFund05;
	}
	public void setTargetFund05(Object what) {
		targetFund05.set(what);
	}	
	public FixedLengthStringData getTargetFund06() {
		return targetFund06;
	}
	public void setTargetFund06(Object what) {
		targetFund06.set(what);
	}	
	public FixedLengthStringData getTargetFund07() {
		return targetFund07;
	}
	public void setTargetFund07(Object what) {
		targetFund07.set(what);
	}	
	public FixedLengthStringData getTargetFund08() {
		return targetFund08;
	}
	public void setTargetFund08(Object what) {
		targetFund08.set(what);
	}	
	public FixedLengthStringData getTargetFund09() {
		return targetFund09;
	}
	public void setTargetFund09(Object what) {
		targetFund09.set(what);
	}	
	public FixedLengthStringData getTargetFund10() {
		return targetFund10;
	}
	public void setTargetFund10(Object what) {
		targetFund10.set(what);
	}	
	public FixedLengthStringData getTargetFundType01() {
		return targetFundType01;
	}
	public void setTargetFundType01(Object what) {
		targetFundType01.set(what);
	}	
	public FixedLengthStringData getTargetFundType02() {
		return targetFundType02;
	}
	public void setTargetFundType02(Object what) {
		targetFundType02.set(what);
	}	
	public FixedLengthStringData getTargetFundType03() {
		return targetFundType03;
	}
	public void setTargetFundType03(Object what) {
		targetFundType03.set(what);
	}	
	public FixedLengthStringData getTargetFundType04() {
		return targetFundType04;
	}
	public void setTargetFundType04(Object what) {
		targetFundType04.set(what);
	}	
	public FixedLengthStringData getTargetFundType05() {
		return targetFundType05;
	}
	public void setTargetFundType05(Object what) {
		targetFundType05.set(what);
	}	
	public FixedLengthStringData getTargetFundType06() {
		return targetFundType06;
	}
	public void setTargetFundType06(Object what) {
		targetFundType06.set(what);
	}	
	public FixedLengthStringData getTargetFundType07() {
		return targetFundType07;
	}
	public void setTargetFundType07(Object what) {
		targetFundType07.set(what);
	}	
	public FixedLengthStringData getTargetFundType08() {
		return targetFundType08;
	}
	public void setTargetFundType08(Object what) {
		targetFundType08.set(what);
	}	
	public FixedLengthStringData getTargetFundType09() {
		return targetFundType09;
	}
	public void setTargetFundType09(Object what) {
		targetFundType09.set(what);
	}	
	public FixedLengthStringData getTargetFundType10() {
		return targetFundType10;
	}
	public void setTargetFundType10(Object what) {
		targetFundType10.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency01() {
		return targetFundCurrency01;
	}
	public void setTargetFundCurrency01(Object what) {
		targetFundCurrency01.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency02() {
		return targetFundCurrency02;
	}
	public void setTargetFundCurrency02(Object what) {
		targetFundCurrency02.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency03() {
		return targetFundCurrency03;
	}
	public void setTargetFundCurrency03(Object what) {
		targetFundCurrency03.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency04() {
		return targetFundCurrency04;
	}
	public void setTargetFundCurrency04(Object what) {
		targetFundCurrency04.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency05() {
		return targetFundCurrency05;
	}
	public void setTargetFundCurrency05(Object what) {
		targetFundCurrency05.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency06() {
		return targetFundCurrency06;
	}
	public void setTargetFundCurrency06(Object what) {
		targetFundCurrency06.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency07() {
		return targetFundCurrency07;
	}
	public void setTargetFundCurrency07(Object what) {
		targetFundCurrency07.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency08() {
		return targetFundCurrency08;
	}
	public void setTargetFundCurrency08(Object what) {
		targetFundCurrency08.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency09() {
		return targetFundCurrency09;
	}
	public void setTargetFundCurrency09(Object what) {
		targetFundCurrency09.set(what);
	}	
	public FixedLengthStringData getTargetFundCurrency10() {
		return targetFundCurrency10;
	}
	public void setTargetFundCurrency10(Object what) {
		targetFundCurrency10.set(what);
	}	
	public PackedDecimalData getTargetPercent01() {
		return targetPercent01;
	}
	public void setTargetPercent01(Object what) {
		setTargetPercent01(what, false);
	}
	public void setTargetPercent01(Object what, boolean rounded) {
		if (rounded)
			targetPercent01.setRounded(what);
		else
			targetPercent01.set(what);
	}	
	public PackedDecimalData getTargetPercent02() {
		return targetPercent02;
	}
	public void setTargetPercent02(Object what) {
		setTargetPercent02(what, false);
	}
	public void setTargetPercent02(Object what, boolean rounded) {
		if (rounded)
			targetPercent02.setRounded(what);
		else
			targetPercent02.set(what);
	}	
	public PackedDecimalData getTargetPercent03() {
		return targetPercent03;
	}
	public void setTargetPercent03(Object what) {
		setTargetPercent03(what, false);
	}
	public void setTargetPercent03(Object what, boolean rounded) {
		if (rounded)
			targetPercent03.setRounded(what);
		else
			targetPercent03.set(what);
	}	
	public PackedDecimalData getTargetPercent04() {
		return targetPercent04;
	}
	public void setTargetPercent04(Object what) {
		setTargetPercent04(what, false);
	}
	public void setTargetPercent04(Object what, boolean rounded) {
		if (rounded)
			targetPercent04.setRounded(what);
		else
			targetPercent04.set(what);
	}	
	public PackedDecimalData getTargetPercent05() {
		return targetPercent05;
	}
	public void setTargetPercent05(Object what) {
		setTargetPercent05(what, false);
	}
	public void setTargetPercent05(Object what, boolean rounded) {
		if (rounded)
			targetPercent05.setRounded(what);
		else
			targetPercent05.set(what);
	}	
	public PackedDecimalData getTargetPercent06() {
		return targetPercent06;
	}
	public void setTargetPercent06(Object what) {
		setTargetPercent06(what, false);
	}
	public void setTargetPercent06(Object what, boolean rounded) {
		if (rounded)
			targetPercent06.setRounded(what);
		else
			targetPercent06.set(what);
	}	
	public PackedDecimalData getTargetPercent07() {
		return targetPercent07;
	}
	public void setTargetPercent07(Object what) {
		setTargetPercent07(what, false);
	}
	public void setTargetPercent07(Object what, boolean rounded) {
		if (rounded)
			targetPercent07.setRounded(what);
		else
			targetPercent07.set(what);
	}	
	public PackedDecimalData getTargetPercent08() {
		return targetPercent08;
	}
	public void setTargetPercent08(Object what) {
		setTargetPercent08(what, false);
	}
	public void setTargetPercent08(Object what, boolean rounded) {
		if (rounded)
			targetPercent08.setRounded(what);
		else
			targetPercent08.set(what);
	}	
	public PackedDecimalData getTargetPercent09() {
		return targetPercent09;
	}
	public void setTargetPercent09(Object what) {
		setTargetPercent09(what, false);
	}
	public void setTargetPercent09(Object what, boolean rounded) {
		if (rounded)
			targetPercent09.setRounded(what);
		else
			targetPercent09.set(what);
	}	
	public PackedDecimalData getTargetPercent10() {
		return targetPercent10;
	}
	public void setTargetPercent10(Object what) {
		setTargetPercent10(what, false);
	}
	public void setTargetPercent10(Object what, boolean rounded) {
		if (rounded)
			targetPercent10.setRounded(what);
		else
			targetPercent10.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getTgtprcnts() {
		return new FixedLengthStringData(targetPercent01.toInternal()
										+ targetPercent02.toInternal()
										+ targetPercent03.toInternal()
										+ targetPercent04.toInternal()
										+ targetPercent05.toInternal()
										+ targetPercent06.toInternal()
										+ targetPercent07.toInternal()
										+ targetPercent08.toInternal()
										+ targetPercent09.toInternal()
										+ targetPercent10.toInternal());
	}
	public void setTgtprcnts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTgtprcnts().getLength()).init(obj);
	
		what = ExternalData.chop(what, targetPercent01);
		what = ExternalData.chop(what, targetPercent02);
		what = ExternalData.chop(what, targetPercent03);
		what = ExternalData.chop(what, targetPercent04);
		what = ExternalData.chop(what, targetPercent05);
		what = ExternalData.chop(what, targetPercent06);
		what = ExternalData.chop(what, targetPercent07);
		what = ExternalData.chop(what, targetPercent08);
		what = ExternalData.chop(what, targetPercent09);
		what = ExternalData.chop(what, targetPercent10);
	}
	public PackedDecimalData getTgtprcnt(BaseData indx) {
		return getTgtprcnt(indx.toInt());
	}
	public PackedDecimalData getTgtprcnt(int indx) {

		switch (indx) {
			case 1 : return targetPercent01;
			case 2 : return targetPercent02;
			case 3 : return targetPercent03;
			case 4 : return targetPercent04;
			case 5 : return targetPercent05;
			case 6 : return targetPercent06;
			case 7 : return targetPercent07;
			case 8 : return targetPercent08;
			case 9 : return targetPercent09;
			case 10 : return targetPercent10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTgtprcnt(BaseData indx, Object what) {
		setTgtprcnt(indx, what, false);
	}
	public void setTgtprcnt(BaseData indx, Object what, boolean rounded) {
		setTgtprcnt(indx.toInt(), what, rounded);
	}
	public void setTgtprcnt(int indx, Object what) {
		setTgtprcnt(indx, what, false);
	}
	public void setTgtprcnt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setTargetPercent01(what, rounded);
					 break;
			case 2 : setTargetPercent02(what, rounded);
					 break;
			case 3 : setTargetPercent03(what, rounded);
					 break;
			case 4 : setTargetPercent04(what, rounded);
					 break;
			case 5 : setTargetPercent05(what, rounded);
					 break;
			case 6 : setTargetPercent06(what, rounded);
					 break;
			case 7 : setTargetPercent07(what, rounded);
					 break;
			case 8 : setTargetPercent08(what, rounded);
					 break;
			case 9 : setTargetPercent09(what, rounded);
					 break;
			case 10 : setTargetPercent10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTgtfunds() {
		return new FixedLengthStringData(targetFund01.toInternal()
										+ targetFund02.toInternal()
										+ targetFund03.toInternal()
										+ targetFund04.toInternal()
										+ targetFund05.toInternal()
										+ targetFund06.toInternal()
										+ targetFund07.toInternal()
										+ targetFund08.toInternal()
										+ targetFund09.toInternal()
										+ targetFund10.toInternal());
	}
	public void setTgtfunds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTgtfunds().getLength()).init(obj);
	
		what = ExternalData.chop(what, targetFund01);
		what = ExternalData.chop(what, targetFund02);
		what = ExternalData.chop(what, targetFund03);
		what = ExternalData.chop(what, targetFund04);
		what = ExternalData.chop(what, targetFund05);
		what = ExternalData.chop(what, targetFund06);
		what = ExternalData.chop(what, targetFund07);
		what = ExternalData.chop(what, targetFund08);
		what = ExternalData.chop(what, targetFund09);
		what = ExternalData.chop(what, targetFund10);
	}
	public FixedLengthStringData getTgtfund(BaseData indx) {
		return getTgtfund(indx.toInt());
	}
	public FixedLengthStringData getTgtfund(int indx) {

		switch (indx) {
			case 1 : return targetFund01;
			case 2 : return targetFund02;
			case 3 : return targetFund03;
			case 4 : return targetFund04;
			case 5 : return targetFund05;
			case 6 : return targetFund06;
			case 7 : return targetFund07;
			case 8 : return targetFund08;
			case 9 : return targetFund09;
			case 10 : return targetFund10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTgtfund(BaseData indx, Object what) {
		setTgtfund(indx.toInt(), what);
	}
	public void setTgtfund(int indx, Object what) {

		switch (indx) {
			case 1 : setTargetFund01(what);
					 break;
			case 2 : setTargetFund02(what);
					 break;
			case 3 : setTargetFund03(what);
					 break;
			case 4 : setTargetFund04(what);
					 break;
			case 5 : setTargetFund05(what);
					 break;
			case 6 : setTargetFund06(what);
					 break;
			case 7 : setTargetFund07(what);
					 break;
			case 8 : setTargetFund08(what);
					 break;
			case 9 : setTargetFund09(what);
					 break;
			case 10 : setTargetFund10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTgfndtyps() {
		return new FixedLengthStringData(targetFundType01.toInternal()
										+ targetFundType02.toInternal()
										+ targetFundType03.toInternal()
										+ targetFundType04.toInternal()
										+ targetFundType05.toInternal()
										+ targetFundType06.toInternal()
										+ targetFundType07.toInternal()
										+ targetFundType08.toInternal()
										+ targetFundType09.toInternal()
										+ targetFundType10.toInternal());
	}
	public void setTgfndtyps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTgfndtyps().getLength()).init(obj);
	
		what = ExternalData.chop(what, targetFundType01);
		what = ExternalData.chop(what, targetFundType02);
		what = ExternalData.chop(what, targetFundType03);
		what = ExternalData.chop(what, targetFundType04);
		what = ExternalData.chop(what, targetFundType05);
		what = ExternalData.chop(what, targetFundType06);
		what = ExternalData.chop(what, targetFundType07);
		what = ExternalData.chop(what, targetFundType08);
		what = ExternalData.chop(what, targetFundType09);
		what = ExternalData.chop(what, targetFundType10);
	}
	public FixedLengthStringData getTgfndtyp(BaseData indx) {
		return getTgfndtyp(indx.toInt());
	}
	public FixedLengthStringData getTgfndtyp(int indx) {

		switch (indx) {
			case 1 : return targetFundType01;
			case 2 : return targetFundType02;
			case 3 : return targetFundType03;
			case 4 : return targetFundType04;
			case 5 : return targetFundType05;
			case 6 : return targetFundType06;
			case 7 : return targetFundType07;
			case 8 : return targetFundType08;
			case 9 : return targetFundType09;
			case 10 : return targetFundType10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTgfndtyp(BaseData indx, Object what) {
		setTgfndtyp(indx.toInt(), what);
	}
	public void setTgfndtyp(int indx, Object what) {

		switch (indx) {
			case 1 : setTargetFundType01(what);
					 break;
			case 2 : setTargetFundType02(what);
					 break;
			case 3 : setTargetFundType03(what);
					 break;
			case 4 : setTargetFundType04(what);
					 break;
			case 5 : setTargetFundType05(what);
					 break;
			case 6 : setTargetFundType06(what);
					 break;
			case 7 : setTargetFundType07(what);
					 break;
			case 8 : setTargetFundType08(what);
					 break;
			case 9 : setTargetFundType09(what);
					 break;
			case 10 : setTargetFundType10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTgfndcurs() {
		return new FixedLengthStringData(targetFundCurrency01.toInternal()
										+ targetFundCurrency02.toInternal()
										+ targetFundCurrency03.toInternal()
										+ targetFundCurrency04.toInternal()
										+ targetFundCurrency05.toInternal()
										+ targetFundCurrency06.toInternal()
										+ targetFundCurrency07.toInternal()
										+ targetFundCurrency08.toInternal()
										+ targetFundCurrency09.toInternal()
										+ targetFundCurrency10.toInternal());
	}
	public void setTgfndcurs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTgfndcurs().getLength()).init(obj);
	
		what = ExternalData.chop(what, targetFundCurrency01);
		what = ExternalData.chop(what, targetFundCurrency02);
		what = ExternalData.chop(what, targetFundCurrency03);
		what = ExternalData.chop(what, targetFundCurrency04);
		what = ExternalData.chop(what, targetFundCurrency05);
		what = ExternalData.chop(what, targetFundCurrency06);
		what = ExternalData.chop(what, targetFundCurrency07);
		what = ExternalData.chop(what, targetFundCurrency08);
		what = ExternalData.chop(what, targetFundCurrency09);
		what = ExternalData.chop(what, targetFundCurrency10);
	}
	public FixedLengthStringData getTgfndcur(BaseData indx) {
		return getTgfndcur(indx.toInt());
	}
	public FixedLengthStringData getTgfndcur(int indx) {

		switch (indx) {
			case 1 : return targetFundCurrency01;
			case 2 : return targetFundCurrency02;
			case 3 : return targetFundCurrency03;
			case 4 : return targetFundCurrency04;
			case 5 : return targetFundCurrency05;
			case 6 : return targetFundCurrency06;
			case 7 : return targetFundCurrency07;
			case 8 : return targetFundCurrency08;
			case 9 : return targetFundCurrency09;
			case 10 : return targetFundCurrency10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTgfndcur(BaseData indx, Object what) {
		setTgfndcur(indx.toInt(), what);
	}
	public void setTgfndcur(int indx, Object what) {

		switch (indx) {
			case 1 : setTargetFundCurrency01(what);
					 break;
			case 2 : setTargetFundCurrency02(what);
					 break;
			case 3 : setTargetFundCurrency03(what);
					 break;
			case 4 : setTargetFundCurrency04(what);
					 break;
			case 5 : setTargetFundCurrency05(what);
					 break;
			case 6 : setTargetFundCurrency06(what);
					 break;
			case 7 : setTargetFundCurrency07(what);
					 break;
			case 8 : setTargetFundCurrency08(what);
					 break;
			case 9 : setTargetFundCurrency09(what);
					 break;
			case 10 : setTargetFundCurrency10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSrcfunds() {
		return new FixedLengthStringData(sourceFund01.toInternal()
										+ sourceFund02.toInternal()
										+ sourceFund03.toInternal()
										+ sourceFund04.toInternal()
										+ sourceFund05.toInternal()
										+ sourceFund06.toInternal()
										+ sourceFund07.toInternal()
										+ sourceFund08.toInternal()
										+ sourceFund09.toInternal()
										+ sourceFund10.toInternal()
										+ sourceFund11.toInternal()
										+ sourceFund12.toInternal()
										+ sourceFund13.toInternal()
										+ sourceFund14.toInternal()
										+ sourceFund15.toInternal()
										+ sourceFund16.toInternal()
										+ sourceFund17.toInternal()
										+ sourceFund18.toInternal()
										+ sourceFund19.toInternal()
										+ sourceFund20.toInternal());
	}
	public void setSrcfunds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSrcfunds().getLength()).init(obj);
	
		what = ExternalData.chop(what, sourceFund01);
		what = ExternalData.chop(what, sourceFund02);
		what = ExternalData.chop(what, sourceFund03);
		what = ExternalData.chop(what, sourceFund04);
		what = ExternalData.chop(what, sourceFund05);
		what = ExternalData.chop(what, sourceFund06);
		what = ExternalData.chop(what, sourceFund07);
		what = ExternalData.chop(what, sourceFund08);
		what = ExternalData.chop(what, sourceFund09);
		what = ExternalData.chop(what, sourceFund10);
		what = ExternalData.chop(what, sourceFund11);
		what = ExternalData.chop(what, sourceFund12);
		what = ExternalData.chop(what, sourceFund13);
		what = ExternalData.chop(what, sourceFund14);
		what = ExternalData.chop(what, sourceFund15);
		what = ExternalData.chop(what, sourceFund16);
		what = ExternalData.chop(what, sourceFund17);
		what = ExternalData.chop(what, sourceFund18);
		what = ExternalData.chop(what, sourceFund19);
		what = ExternalData.chop(what, sourceFund20);
	}
	public FixedLengthStringData getSrcfund(BaseData indx) {
		return getSrcfund(indx.toInt());
	}
	public FixedLengthStringData getSrcfund(int indx) {

		switch (indx) {
			case 1 : return sourceFund01;
			case 2 : return sourceFund02;
			case 3 : return sourceFund03;
			case 4 : return sourceFund04;
			case 5 : return sourceFund05;
			case 6 : return sourceFund06;
			case 7 : return sourceFund07;
			case 8 : return sourceFund08;
			case 9 : return sourceFund09;
			case 10 : return sourceFund10;
			case 11 : return sourceFund11;
			case 12 : return sourceFund12;
			case 13 : return sourceFund13;
			case 14 : return sourceFund14;
			case 15 : return sourceFund15;
			case 16 : return sourceFund16;
			case 17 : return sourceFund17;
			case 18 : return sourceFund18;
			case 19 : return sourceFund19;
			case 20 : return sourceFund20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSrcfund(BaseData indx, Object what) {
		setSrcfund(indx.toInt(), what);
	}
	public void setSrcfund(int indx, Object what) {

		switch (indx) {
			case 1 : setSourceFund01(what);
					 break;
			case 2 : setSourceFund02(what);
					 break;
			case 3 : setSourceFund03(what);
					 break;
			case 4 : setSourceFund04(what);
					 break;
			case 5 : setSourceFund05(what);
					 break;
			case 6 : setSourceFund06(what);
					 break;
			case 7 : setSourceFund07(what);
					 break;
			case 8 : setSourceFund08(what);
					 break;
			case 9 : setSourceFund09(what);
					 break;
			case 10 : setSourceFund10(what);
					 break;
			case 11 : setSourceFund11(what);
					 break;
			case 12 : setSourceFund12(what);
					 break;
			case 13 : setSourceFund13(what);
					 break;
			case 14 : setSourceFund14(what);
					 break;
			case 15 : setSourceFund15(what);
					 break;
			case 16 : setSourceFund16(what);
					 break;
			case 17 : setSourceFund17(what);
					 break;
			case 18 : setSourceFund18(what);
					 break;
			case 19 : setSourceFund19(what);
					 break;
			case 20 : setSourceFund20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getScprcamts() {
		return new FixedLengthStringData(sourcePercentAmount01.toInternal()
										+ sourcePercentAmount02.toInternal()
										+ sourcePercentAmount03.toInternal()
										+ sourcePercentAmount04.toInternal()
										+ sourcePercentAmount05.toInternal()
										+ sourcePercentAmount06.toInternal()
										+ sourcePercentAmount07.toInternal()
										+ sourcePercentAmount08.toInternal()
										+ sourcePercentAmount09.toInternal()
										+ sourcePercentAmount10.toInternal()
										+ sourcePercentAmount11.toInternal()
										+ sourcePercentAmount12.toInternal()
										+ sourcePercentAmount13.toInternal()
										+ sourcePercentAmount14.toInternal()
										+ sourcePercentAmount15.toInternal()
										+ sourcePercentAmount16.toInternal()
										+ sourcePercentAmount17.toInternal()
										+ sourcePercentAmount18.toInternal()
										+ sourcePercentAmount19.toInternal()
										+ sourcePercentAmount20.toInternal());
	}
	public void setScprcamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getScprcamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, sourcePercentAmount01);
		what = ExternalData.chop(what, sourcePercentAmount02);
		what = ExternalData.chop(what, sourcePercentAmount03);
		what = ExternalData.chop(what, sourcePercentAmount04);
		what = ExternalData.chop(what, sourcePercentAmount05);
		what = ExternalData.chop(what, sourcePercentAmount06);
		what = ExternalData.chop(what, sourcePercentAmount07);
		what = ExternalData.chop(what, sourcePercentAmount08);
		what = ExternalData.chop(what, sourcePercentAmount09);
		what = ExternalData.chop(what, sourcePercentAmount10);
		what = ExternalData.chop(what, sourcePercentAmount11);
		what = ExternalData.chop(what, sourcePercentAmount12);
		what = ExternalData.chop(what, sourcePercentAmount13);
		what = ExternalData.chop(what, sourcePercentAmount14);
		what = ExternalData.chop(what, sourcePercentAmount15);
		what = ExternalData.chop(what, sourcePercentAmount16);
		what = ExternalData.chop(what, sourcePercentAmount17);
		what = ExternalData.chop(what, sourcePercentAmount18);
		what = ExternalData.chop(what, sourcePercentAmount19);
		what = ExternalData.chop(what, sourcePercentAmount20);
	}
	public PackedDecimalData getScprcamt(BaseData indx) {
		return getScprcamt(indx.toInt());
	}
	public PackedDecimalData getScprcamt(int indx) {

		switch (indx) {
			case 1 : return sourcePercentAmount01;
			case 2 : return sourcePercentAmount02;
			case 3 : return sourcePercentAmount03;
			case 4 : return sourcePercentAmount04;
			case 5 : return sourcePercentAmount05;
			case 6 : return sourcePercentAmount06;
			case 7 : return sourcePercentAmount07;
			case 8 : return sourcePercentAmount08;
			case 9 : return sourcePercentAmount09;
			case 10 : return sourcePercentAmount10;
			case 11 : return sourcePercentAmount11;
			case 12 : return sourcePercentAmount12;
			case 13 : return sourcePercentAmount13;
			case 14 : return sourcePercentAmount14;
			case 15 : return sourcePercentAmount15;
			case 16 : return sourcePercentAmount16;
			case 17 : return sourcePercentAmount17;
			case 18 : return sourcePercentAmount18;
			case 19 : return sourcePercentAmount19;
			case 20 : return sourcePercentAmount20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setScprcamt(BaseData indx, Object what) {
		setScprcamt(indx, what, false);
	}
	public void setScprcamt(BaseData indx, Object what, boolean rounded) {
		setScprcamt(indx.toInt(), what, rounded);
	}
	public void setScprcamt(int indx, Object what) {
		setScprcamt(indx, what, false);
	}
	public void setScprcamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSourcePercentAmount01(what, rounded);
					 break;
			case 2 : setSourcePercentAmount02(what, rounded);
					 break;
			case 3 : setSourcePercentAmount03(what, rounded);
					 break;
			case 4 : setSourcePercentAmount04(what, rounded);
					 break;
			case 5 : setSourcePercentAmount05(what, rounded);
					 break;
			case 6 : setSourcePercentAmount06(what, rounded);
					 break;
			case 7 : setSourcePercentAmount07(what, rounded);
					 break;
			case 8 : setSourcePercentAmount08(what, rounded);
					 break;
			case 9 : setSourcePercentAmount09(what, rounded);
					 break;
			case 10 : setSourcePercentAmount10(what, rounded);
					 break;
			case 11 : setSourcePercentAmount11(what, rounded);
					 break;
			case 12 : setSourcePercentAmount12(what, rounded);
					 break;
			case 13 : setSourcePercentAmount13(what, rounded);
					 break;
			case 14 : setSourcePercentAmount14(what, rounded);
					 break;
			case 15 : setSourcePercentAmount15(what, rounded);
					 break;
			case 16 : setSourcePercentAmount16(what, rounded);
					 break;
			case 17 : setSourcePercentAmount17(what, rounded);
					 break;
			case 18 : setSourcePercentAmount18(what, rounded);
					 break;
			case 19 : setSourcePercentAmount19(what, rounded);
					 break;
			case 20 : setSourcePercentAmount20(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getScfndtyps() {
		return new FixedLengthStringData(sourceFundType01.toInternal()
										+ sourceFundType02.toInternal()
										+ sourceFundType03.toInternal()
										+ sourceFundType04.toInternal()
										+ sourceFundType05.toInternal()
										+ sourceFundType06.toInternal()
										+ sourceFundType07.toInternal()
										+ sourceFundType08.toInternal()
										+ sourceFundType09.toInternal()
										+ sourceFundType10.toInternal()
										+ sourceFundType11.toInternal()
										+ sourceFundType12.toInternal()
										+ sourceFundType13.toInternal()
										+ sourceFundType14.toInternal()
										+ sourceFundType15.toInternal()
										+ sourceFundType16.toInternal()
										+ sourceFundType17.toInternal()
										+ sourceFundType18.toInternal()
										+ sourceFundType19.toInternal()
										+ sourceFundType20.toInternal());
	}
	public void setScfndtyps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getScfndtyps().getLength()).init(obj);
	
		what = ExternalData.chop(what, sourceFundType01);
		what = ExternalData.chop(what, sourceFundType02);
		what = ExternalData.chop(what, sourceFundType03);
		what = ExternalData.chop(what, sourceFundType04);
		what = ExternalData.chop(what, sourceFundType05);
		what = ExternalData.chop(what, sourceFundType06);
		what = ExternalData.chop(what, sourceFundType07);
		what = ExternalData.chop(what, sourceFundType08);
		what = ExternalData.chop(what, sourceFundType09);
		what = ExternalData.chop(what, sourceFundType10);
		what = ExternalData.chop(what, sourceFundType11);
		what = ExternalData.chop(what, sourceFundType12);
		what = ExternalData.chop(what, sourceFundType13);
		what = ExternalData.chop(what, sourceFundType14);
		what = ExternalData.chop(what, sourceFundType15);
		what = ExternalData.chop(what, sourceFundType16);
		what = ExternalData.chop(what, sourceFundType17);
		what = ExternalData.chop(what, sourceFundType18);
		what = ExternalData.chop(what, sourceFundType19);
		what = ExternalData.chop(what, sourceFundType20);
	}
	public FixedLengthStringData getScfndtyp(BaseData indx) {
		return getScfndtyp(indx.toInt());
	}
	public FixedLengthStringData getScfndtyp(int indx) {

		switch (indx) {
			case 1 : return sourceFundType01;
			case 2 : return sourceFundType02;
			case 3 : return sourceFundType03;
			case 4 : return sourceFundType04;
			case 5 : return sourceFundType05;
			case 6 : return sourceFundType06;
			case 7 : return sourceFundType07;
			case 8 : return sourceFundType08;
			case 9 : return sourceFundType09;
			case 10 : return sourceFundType10;
			case 11 : return sourceFundType11;
			case 12 : return sourceFundType12;
			case 13 : return sourceFundType13;
			case 14 : return sourceFundType14;
			case 15 : return sourceFundType15;
			case 16 : return sourceFundType16;
			case 17 : return sourceFundType17;
			case 18 : return sourceFundType18;
			case 19 : return sourceFundType19;
			case 20 : return sourceFundType20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setScfndtyp(BaseData indx, Object what) {
		setScfndtyp(indx.toInt(), what);
	}
	public void setScfndtyp(int indx, Object what) {

		switch (indx) {
			case 1 : setSourceFundType01(what);
					 break;
			case 2 : setSourceFundType02(what);
					 break;
			case 3 : setSourceFundType03(what);
					 break;
			case 4 : setSourceFundType04(what);
					 break;
			case 5 : setSourceFundType05(what);
					 break;
			case 6 : setSourceFundType06(what);
					 break;
			case 7 : setSourceFundType07(what);
					 break;
			case 8 : setSourceFundType08(what);
					 break;
			case 9 : setSourceFundType09(what);
					 break;
			case 10 : setSourceFundType10(what);
					 break;
			case 11 : setSourceFundType11(what);
					 break;
			case 12 : setSourceFundType12(what);
					 break;
			case 13 : setSourceFundType13(what);
					 break;
			case 14 : setSourceFundType14(what);
					 break;
			case 15 : setSourceFundType15(what);
					 break;
			case 16 : setSourceFundType16(what);
					 break;
			case 17 : setSourceFundType17(what);
					 break;
			case 18 : setSourceFundType18(what);
					 break;
			case 19 : setSourceFundType19(what);
					 break;
			case 20 : setSourceFundType20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getScfndcurs() {
		return new FixedLengthStringData(sourceFundCurrency01.toInternal()
										+ sourceFundCurrency02.toInternal()
										+ sourceFundCurrency03.toInternal()
										+ sourceFundCurrency04.toInternal()
										+ sourceFundCurrency05.toInternal()
										+ sourceFundCurrency06.toInternal()
										+ sourceFundCurrency07.toInternal()
										+ sourceFundCurrency08.toInternal()
										+ sourceFundCurrency09.toInternal()
										+ sourceFundCurrency10.toInternal()
										+ sourceFundCurrency11.toInternal()
										+ sourceFundCurrency12.toInternal()
										+ sourceFundCurrency13.toInternal()
										+ sourceFundCurrency14.toInternal()
										+ sourceFundCurrency15.toInternal()
										+ sourceFundCurrency16.toInternal()
										+ sourceFundCurrency17.toInternal()
										+ sourceFundCurrency18.toInternal()
										+ sourceFundCurrency19.toInternal()
										+ sourceFundCurrency20.toInternal());
	}
	public void setScfndcurs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getScfndcurs().getLength()).init(obj);
	
		what = ExternalData.chop(what, sourceFundCurrency01);
		what = ExternalData.chop(what, sourceFundCurrency02);
		what = ExternalData.chop(what, sourceFundCurrency03);
		what = ExternalData.chop(what, sourceFundCurrency04);
		what = ExternalData.chop(what, sourceFundCurrency05);
		what = ExternalData.chop(what, sourceFundCurrency06);
		what = ExternalData.chop(what, sourceFundCurrency07);
		what = ExternalData.chop(what, sourceFundCurrency08);
		what = ExternalData.chop(what, sourceFundCurrency09);
		what = ExternalData.chop(what, sourceFundCurrency10);
		what = ExternalData.chop(what, sourceFundCurrency11);
		what = ExternalData.chop(what, sourceFundCurrency12);
		what = ExternalData.chop(what, sourceFundCurrency13);
		what = ExternalData.chop(what, sourceFundCurrency14);
		what = ExternalData.chop(what, sourceFundCurrency15);
		what = ExternalData.chop(what, sourceFundCurrency16);
		what = ExternalData.chop(what, sourceFundCurrency17);
		what = ExternalData.chop(what, sourceFundCurrency18);
		what = ExternalData.chop(what, sourceFundCurrency19);
		what = ExternalData.chop(what, sourceFundCurrency20);
	}
	public FixedLengthStringData getScfndcur(BaseData indx) {
		return getScfndcur(indx.toInt());
	}
	public FixedLengthStringData getScfndcur(int indx) {

		switch (indx) {
			case 1 : return sourceFundCurrency01;
			case 2 : return sourceFundCurrency02;
			case 3 : return sourceFundCurrency03;
			case 4 : return sourceFundCurrency04;
			case 5 : return sourceFundCurrency05;
			case 6 : return sourceFundCurrency06;
			case 7 : return sourceFundCurrency07;
			case 8 : return sourceFundCurrency08;
			case 9 : return sourceFundCurrency09;
			case 10 : return sourceFundCurrency10;
			case 11 : return sourceFundCurrency11;
			case 12 : return sourceFundCurrency12;
			case 13 : return sourceFundCurrency13;
			case 14 : return sourceFundCurrency14;
			case 15 : return sourceFundCurrency15;
			case 16 : return sourceFundCurrency16;
			case 17 : return sourceFundCurrency17;
			case 18 : return sourceFundCurrency18;
			case 19 : return sourceFundCurrency19;
			case 20 : return sourceFundCurrency20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setScfndcur(BaseData indx, Object what) {
		setScfndcur(indx.toInt(), what);
	}
	public void setScfndcur(int indx, Object what) {

		switch (indx) {
			case 1 : setSourceFundCurrency01(what);
					 break;
			case 2 : setSourceFundCurrency02(what);
					 break;
			case 3 : setSourceFundCurrency03(what);
					 break;
			case 4 : setSourceFundCurrency04(what);
					 break;
			case 5 : setSourceFundCurrency05(what);
					 break;
			case 6 : setSourceFundCurrency06(what);
					 break;
			case 7 : setSourceFundCurrency07(what);
					 break;
			case 8 : setSourceFundCurrency08(what);
					 break;
			case 9 : setSourceFundCurrency09(what);
					 break;
			case 10 : setSourceFundCurrency10(what);
					 break;
			case 11 : setSourceFundCurrency11(what);
					 break;
			case 12 : setSourceFundCurrency12(what);
					 break;
			case 13 : setSourceFundCurrency13(what);
					 break;
			case 14 : setSourceFundCurrency14(what);
					 break;
			case 15 : setSourceFundCurrency15(what);
					 break;
			case 16 : setSourceFundCurrency16(what);
					 break;
			case 17 : setSourceFundCurrency17(what);
					 break;
			case 18 : setSourceFundCurrency18(what);
					 break;
			case 19 : setSourceFundCurrency19(what);
					 break;
			case 20 : setSourceFundCurrency20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getScestvals() {
		return new FixedLengthStringData(sourceEstimatedValue01.toInternal()
										+ sourceEstimatedValue02.toInternal()
										+ sourceEstimatedValue03.toInternal()
										+ sourceEstimatedValue04.toInternal()
										+ sourceEstimatedValue05.toInternal()
										+ sourceEstimatedValue06.toInternal()
										+ sourceEstimatedValue07.toInternal()
										+ sourceEstimatedValue08.toInternal()
										+ sourceEstimatedValue09.toInternal()
										+ sourceEstimatedValue10.toInternal()
										+ sourceEstimatedValue11.toInternal()
										+ sourceEstimatedValue12.toInternal()
										+ sourceEstimatedValue13.toInternal()
										+ sourceEstimatedValue14.toInternal()
										+ sourceEstimatedValue15.toInternal()
										+ sourceEstimatedValue16.toInternal()
										+ sourceEstimatedValue17.toInternal()
										+ sourceEstimatedValue18.toInternal()
										+ sourceEstimatedValue19.toInternal()
										+ sourceEstimatedValue20.toInternal());
	}
	public void setScestvals(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getScestvals().getLength()).init(obj);
	
		what = ExternalData.chop(what, sourceEstimatedValue01);
		what = ExternalData.chop(what, sourceEstimatedValue02);
		what = ExternalData.chop(what, sourceEstimatedValue03);
		what = ExternalData.chop(what, sourceEstimatedValue04);
		what = ExternalData.chop(what, sourceEstimatedValue05);
		what = ExternalData.chop(what, sourceEstimatedValue06);
		what = ExternalData.chop(what, sourceEstimatedValue07);
		what = ExternalData.chop(what, sourceEstimatedValue08);
		what = ExternalData.chop(what, sourceEstimatedValue09);
		what = ExternalData.chop(what, sourceEstimatedValue10);
		what = ExternalData.chop(what, sourceEstimatedValue11);
		what = ExternalData.chop(what, sourceEstimatedValue12);
		what = ExternalData.chop(what, sourceEstimatedValue13);
		what = ExternalData.chop(what, sourceEstimatedValue14);
		what = ExternalData.chop(what, sourceEstimatedValue15);
		what = ExternalData.chop(what, sourceEstimatedValue16);
		what = ExternalData.chop(what, sourceEstimatedValue17);
		what = ExternalData.chop(what, sourceEstimatedValue18);
		what = ExternalData.chop(what, sourceEstimatedValue19);
		what = ExternalData.chop(what, sourceEstimatedValue20);
	}
	public PackedDecimalData getScestval(BaseData indx) {
		return getScestval(indx.toInt());
	}
	public PackedDecimalData getScestval(int indx) {

		switch (indx) {
			case 1 : return sourceEstimatedValue01;
			case 2 : return sourceEstimatedValue02;
			case 3 : return sourceEstimatedValue03;
			case 4 : return sourceEstimatedValue04;
			case 5 : return sourceEstimatedValue05;
			case 6 : return sourceEstimatedValue06;
			case 7 : return sourceEstimatedValue07;
			case 8 : return sourceEstimatedValue08;
			case 9 : return sourceEstimatedValue09;
			case 10 : return sourceEstimatedValue10;
			case 11 : return sourceEstimatedValue11;
			case 12 : return sourceEstimatedValue12;
			case 13 : return sourceEstimatedValue13;
			case 14 : return sourceEstimatedValue14;
			case 15 : return sourceEstimatedValue15;
			case 16 : return sourceEstimatedValue16;
			case 17 : return sourceEstimatedValue17;
			case 18 : return sourceEstimatedValue18;
			case 19 : return sourceEstimatedValue19;
			case 20 : return sourceEstimatedValue20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setScestval(BaseData indx, Object what) {
		setScestval(indx, what, false);
	}
	public void setScestval(BaseData indx, Object what, boolean rounded) {
		setScestval(indx.toInt(), what, rounded);
	}
	public void setScestval(int indx, Object what) {
		setScestval(indx, what, false);
	}
	public void setScestval(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSourceEstimatedValue01(what, rounded);
					 break;
			case 2 : setSourceEstimatedValue02(what, rounded);
					 break;
			case 3 : setSourceEstimatedValue03(what, rounded);
					 break;
			case 4 : setSourceEstimatedValue04(what, rounded);
					 break;
			case 5 : setSourceEstimatedValue05(what, rounded);
					 break;
			case 6 : setSourceEstimatedValue06(what, rounded);
					 break;
			case 7 : setSourceEstimatedValue07(what, rounded);
					 break;
			case 8 : setSourceEstimatedValue08(what, rounded);
					 break;
			case 9 : setSourceEstimatedValue09(what, rounded);
					 break;
			case 10 : setSourceEstimatedValue10(what, rounded);
					 break;
			case 11 : setSourceEstimatedValue11(what, rounded);
					 break;
			case 12 : setSourceEstimatedValue12(what, rounded);
					 break;
			case 13 : setSourceEstimatedValue13(what, rounded);
					 break;
			case 14 : setSourceEstimatedValue14(what, rounded);
					 break;
			case 15 : setSourceEstimatedValue15(what, rounded);
					 break;
			case 16 : setSourceEstimatedValue16(what, rounded);
					 break;
			case 17 : setSourceEstimatedValue17(what, rounded);
					 break;
			case 18 : setSourceEstimatedValue18(what, rounded);
					 break;
			case 19 : setSourceEstimatedValue19(what, rounded);
					 break;
			case 20 : setSourceEstimatedValue20(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getScactvals() {
		return new FixedLengthStringData(sourceActualValue01.toInternal()
										+ sourceActualValue02.toInternal()
										+ sourceActualValue03.toInternal()
										+ sourceActualValue04.toInternal()
										+ sourceActualValue05.toInternal()
										+ sourceActualValue06.toInternal()
										+ sourceActualValue07.toInternal()
										+ sourceActualValue08.toInternal()
										+ sourceActualValue09.toInternal()
										+ sourceActualValue10.toInternal()
										+ sourceActualValue11.toInternal()
										+ sourceActualValue12.toInternal()
										+ sourceActualValue13.toInternal()
										+ sourceActualValue14.toInternal()
										+ sourceActualValue15.toInternal()
										+ sourceActualValue16.toInternal()
										+ sourceActualValue17.toInternal()
										+ sourceActualValue18.toInternal()
										+ sourceActualValue19.toInternal()
										+ sourceActualValue20.toInternal());
	}
	public void setScactvals(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getScactvals().getLength()).init(obj);
	
		what = ExternalData.chop(what, sourceActualValue01);
		what = ExternalData.chop(what, sourceActualValue02);
		what = ExternalData.chop(what, sourceActualValue03);
		what = ExternalData.chop(what, sourceActualValue04);
		what = ExternalData.chop(what, sourceActualValue05);
		what = ExternalData.chop(what, sourceActualValue06);
		what = ExternalData.chop(what, sourceActualValue07);
		what = ExternalData.chop(what, sourceActualValue08);
		what = ExternalData.chop(what, sourceActualValue09);
		what = ExternalData.chop(what, sourceActualValue10);
		what = ExternalData.chop(what, sourceActualValue11);
		what = ExternalData.chop(what, sourceActualValue12);
		what = ExternalData.chop(what, sourceActualValue13);
		what = ExternalData.chop(what, sourceActualValue14);
		what = ExternalData.chop(what, sourceActualValue15);
		what = ExternalData.chop(what, sourceActualValue16);
		what = ExternalData.chop(what, sourceActualValue17);
		what = ExternalData.chop(what, sourceActualValue18);
		what = ExternalData.chop(what, sourceActualValue19);
		what = ExternalData.chop(what, sourceActualValue20);
	}
	public PackedDecimalData getScactval(BaseData indx) {
		return getScactval(indx.toInt());
	}
	public PackedDecimalData getScactval(int indx) {

		switch (indx) {
			case 1 : return sourceActualValue01;
			case 2 : return sourceActualValue02;
			case 3 : return sourceActualValue03;
			case 4 : return sourceActualValue04;
			case 5 : return sourceActualValue05;
			case 6 : return sourceActualValue06;
			case 7 : return sourceActualValue07;
			case 8 : return sourceActualValue08;
			case 9 : return sourceActualValue09;
			case 10 : return sourceActualValue10;
			case 11 : return sourceActualValue11;
			case 12 : return sourceActualValue12;
			case 13 : return sourceActualValue13;
			case 14 : return sourceActualValue14;
			case 15 : return sourceActualValue15;
			case 16 : return sourceActualValue16;
			case 17 : return sourceActualValue17;
			case 18 : return sourceActualValue18;
			case 19 : return sourceActualValue19;
			case 20 : return sourceActualValue20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setScactval(BaseData indx, Object what) {
		setScactval(indx, what, false);
	}
	public void setScactval(BaseData indx, Object what, boolean rounded) {
		setScactval(indx.toInt(), what, rounded);
	}
	public void setScactval(int indx, Object what) {
		setScactval(indx, what, false);
	}
	public void setScactval(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSourceActualValue01(what, rounded);
					 break;
			case 2 : setSourceActualValue02(what, rounded);
					 break;
			case 3 : setSourceActualValue03(what, rounded);
					 break;
			case 4 : setSourceActualValue04(what, rounded);
					 break;
			case 5 : setSourceActualValue05(what, rounded);
					 break;
			case 6 : setSourceActualValue06(what, rounded);
					 break;
			case 7 : setSourceActualValue07(what, rounded);
					 break;
			case 8 : setSourceActualValue08(what, rounded);
					 break;
			case 9 : setSourceActualValue09(what, rounded);
					 break;
			case 10 : setSourceActualValue10(what, rounded);
					 break;
			case 11 : setSourceActualValue11(what, rounded);
					 break;
			case 12 : setSourceActualValue12(what, rounded);
					 break;
			case 13 : setSourceActualValue13(what, rounded);
					 break;
			case 14 : setSourceActualValue14(what, rounded);
					 break;
			case 15 : setSourceActualValue15(what, rounded);
					 break;
			case 16 : setSourceActualValue16(what, rounded);
					 break;
			case 17 : setSourceActualValue17(what, rounded);
					 break;
			case 18 : setSourceActualValue18(what, rounded);
					 break;
			case 19 : setSourceActualValue19(what, rounded);
					 break;
			case 20 : setSourceActualValue20(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		planSuffix.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		effdate.clear();
		percentAmountInd.clear();
		overrideFee.clear();
		sourceFund01.clear();
		sourceFund02.clear();
		sourceFund03.clear();
		sourceFund04.clear();
		sourceFund05.clear();
		sourceFund06.clear();
		sourceFund07.clear();
		sourceFund08.clear();
		sourceFund09.clear();
		sourceFund10.clear();
		sourceFund11.clear();
		sourceFund12.clear();
		sourceFund13.clear();
		sourceFund14.clear();
		sourceFund15.clear();
		sourceFund16.clear();
		sourceFund17.clear();
		sourceFund18.clear();
		sourceFund19.clear();
		sourceFund20.clear();
		sourceFundType01.clear();
		sourceFundType02.clear();
		sourceFundType03.clear();
		sourceFundType04.clear();
		sourceFundType05.clear();
		sourceFundType06.clear();
		sourceFundType07.clear();
		sourceFundType08.clear();
		sourceFundType09.clear();
		sourceFundType10.clear();
		sourceFundType11.clear();
		sourceFundType12.clear();
		sourceFundType13.clear();
		sourceFundType14.clear();
		sourceFundType15.clear();
		sourceFundType16.clear();
		sourceFundType17.clear();
		sourceFundType18.clear();
		sourceFundType19.clear();
		sourceFundType20.clear();
		sourceFundCurrency01.clear();
		sourceFundCurrency02.clear();
		sourceFundCurrency03.clear();
		sourceFundCurrency04.clear();
		sourceFundCurrency05.clear();
		sourceFundCurrency06.clear();
		sourceFundCurrency07.clear();
		sourceFundCurrency08.clear();
		sourceFundCurrency09.clear();
		sourceFundCurrency10.clear();
		sourceFundCurrency11.clear();
		sourceFundCurrency12.clear();
		sourceFundCurrency13.clear();
		sourceFundCurrency14.clear();
		sourceFundCurrency15.clear();
		sourceFundCurrency16.clear();
		sourceFundCurrency17.clear();
		sourceFundCurrency18.clear();
		sourceFundCurrency19.clear();
		sourceFundCurrency20.clear();
		sourcePercentAmount01.clear();
		sourcePercentAmount02.clear();
		sourcePercentAmount03.clear();
		sourcePercentAmount04.clear();
		sourcePercentAmount05.clear();
		sourcePercentAmount06.clear();
		sourcePercentAmount07.clear();
		sourcePercentAmount08.clear();
		sourcePercentAmount09.clear();
		sourcePercentAmount10.clear();
		sourcePercentAmount11.clear();
		sourcePercentAmount12.clear();
		sourcePercentAmount13.clear();
		sourcePercentAmount14.clear();
		sourcePercentAmount15.clear();
		sourcePercentAmount16.clear();
		sourcePercentAmount17.clear();
		sourcePercentAmount18.clear();
		sourcePercentAmount19.clear();
		sourcePercentAmount20.clear();
		sourceEstimatedValue01.clear();
		sourceEstimatedValue02.clear();
		sourceEstimatedValue03.clear();
		sourceEstimatedValue04.clear();
		sourceEstimatedValue05.clear();
		sourceEstimatedValue06.clear();
		sourceEstimatedValue07.clear();
		sourceEstimatedValue08.clear();
		sourceEstimatedValue09.clear();
		sourceEstimatedValue10.clear();
		sourceEstimatedValue11.clear();
		sourceEstimatedValue12.clear();
		sourceEstimatedValue13.clear();
		sourceEstimatedValue14.clear();
		sourceEstimatedValue15.clear();
		sourceEstimatedValue16.clear();
		sourceEstimatedValue17.clear();
		sourceEstimatedValue18.clear();
		sourceEstimatedValue19.clear();
		sourceEstimatedValue20.clear();
		sourceActualValue01.clear();
		sourceActualValue02.clear();
		sourceActualValue03.clear();
		sourceActualValue04.clear();
		sourceActualValue05.clear();
		sourceActualValue06.clear();
		sourceActualValue07.clear();
		sourceActualValue08.clear();
		sourceActualValue09.clear();
		sourceActualValue10.clear();
		sourceActualValue11.clear();
		sourceActualValue12.clear();
		sourceActualValue13.clear();
		sourceActualValue14.clear();
		sourceActualValue15.clear();
		sourceActualValue16.clear();
		sourceActualValue17.clear();
		sourceActualValue18.clear();
		sourceActualValue19.clear();
		sourceActualValue20.clear();
		targetFund01.clear();
		targetFund02.clear();
		targetFund03.clear();
		targetFund04.clear();
		targetFund05.clear();
		targetFund06.clear();
		targetFund07.clear();
		targetFund08.clear();
		targetFund09.clear();
		targetFund10.clear();
		targetFundType01.clear();
		targetFundType02.clear();
		targetFundType03.clear();
		targetFundType04.clear();
		targetFundType05.clear();
		targetFundType06.clear();
		targetFundType07.clear();
		targetFundType08.clear();
		targetFundType09.clear();
		targetFundType10.clear();
		targetFundCurrency01.clear();
		targetFundCurrency02.clear();
		targetFundCurrency03.clear();
		targetFundCurrency04.clear();
		targetFundCurrency05.clear();
		targetFundCurrency06.clear();
		targetFundCurrency07.clear();
		targetFundCurrency08.clear();
		targetFundCurrency09.clear();
		targetFundCurrency10.clear();
		targetPercent01.clear();
		targetPercent02.clear();
		targetPercent03.clear();
		targetPercent04.clear();
		targetPercent05.clear();
		targetPercent06.clear();
		targetPercent07.clear();
		targetPercent08.clear();
		targetPercent09.clear();
		targetPercent10.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}