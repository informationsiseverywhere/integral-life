package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datadictionarydatatype.PackedDateDDObj;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5121
 * @version 1.0 generated on 30/08/09 06:35
 * @author Quipoz
 */
public class S5121ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(462);
	public FixedLengthStringData dataFields = new FixedLengthStringData(206).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,111);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,158);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,166);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,170);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,184);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,187);
	public ZonedDecimalData fundAmount = DD.fundamnt.copyToZonedDecimal().isAPartOf(dataFields,189);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 206);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData fundAmountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 270);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] fundAmountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	//ILIFE-1402 STARTS
	public FixedLengthStringData subfileArea = new FixedLengthStringData(274);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(112).isAPartOf(subfileArea, 0);
	public ZonedDecimalData currentDunitBal = DD.curduntbal.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData currentUnitBal = DD.curuntbal.copyToZonedDecimal().isAPartOf(subfileFields,16);
	public FixedLengthStringData fundShortDesc = DD.fndshrtdsc.copy().isAPartOf(subfileFields,32);
	public FixedLengthStringData fundTypeShortDesc = DD.fndtypldsc.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData fundtype = DD.fundtype.copy().isAPartOf(subfileFields,72);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,73);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,74);
	public ZonedDecimalData uoffpr = DD.uoffpr.copyToZonedDecimal().isAPartOf(subfileFields,78);
	public ZonedDecimalData estval = DD.estval.copyToZonedDecimal().isAPartOf(subfileFields,87);
	public ZonedDecimalData effDate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,104);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 112);
	
	public FixedLengthStringData curduntbalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData curuntbalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData fndshrtdscErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData fndtypsdscErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fundtypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData uofprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData estvlErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData effdtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 152);
	public FixedLengthStringData[] curduntbalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] curuntbalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] fndshrtdscOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] fndtypsdscOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fundtypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] uofprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] estvlOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] effdtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 272);
	//ILIFE-1402 ENDS
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5121screensflWritten = new LongData(0);
	public LongData S5121screenctlWritten = new LongData(0);
	public LongData S5121screenWritten = new LongData(0);
	public LongData S5121protectWritten = new LongData(0);
	public GeneralTable s5121screensfl = new GeneralTable(AppVars.getInstance());
	
	public FixedLengthStringData effDateDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5121screensfl;
	}

	public S5121ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "80",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, unitVirtualFund, fundShortDesc, fundtype, fundTypeShortDesc, currentUnitBal, currentDunitBal, estval, uoffpr,
				effDate};
		screenSflOutFields = new BaseData[][] {selectOut, vrtfndOut, fndshrtdscOut, fundtypeOut, fndtypsdscOut, curuntbalOut, curduntbalOut, estvlOut, uofprOut,
				effdtOut};
		screenSflErrFields = new BaseData[] {selectErr, vrtfndErr, fndshrtdscErr, fundtypeErr, fndtypsdscErr, curuntbalErr, curduntbalErr, estvlErr, uofprErr,
				effdtErr};
		screenSflDateFields = new BaseData[] {effDate};
		screenSflDateErrFields = new BaseData[] {effdtErr};
		screenSflDateDispFields = new BaseData[] {effDateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, coverage, rider};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, coverageOut, riderOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, coverageErr, riderErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5121screen.class;
		screenSflRecord = S5121screensfl.class;
		screenCtlRecord = S5121screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5121protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5121screenctl.lrec.pageSubfile);
	}
}
