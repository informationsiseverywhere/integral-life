/*
 * File: P2478low.java
 * Date: 29 August 2009 23:32:51
 * Author: Quipoz Limited
 * 
 * Class transformed from P2478LOW.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;

import com.csc.fsu.general.recordstructures.Hcltrolcpy;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This subroutine is used to 'KEEPS' CHDR for subsequent inquiry
* screen.
*
* Functions:
*   There are no functions for this subroutine.
*
* Statii:
*      BOMB - System error occurred
*
* Linkage Area:
*        FUNCTION           PIC X(05).
*        STATUZ             PIC X(04).
*        HCLT-KEY           PIC X(10).  OCCURS 10 TIMES.
*  (These fields are contained in HCLTROLCPY)
*
*****************************************************
* </pre>
*/
public class P2478low extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
		/* ERRORS */
	private String e704 = "E704";
		/* FORMATS */
	private String chdrenqrec = "CHDRENQREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Hcltrolcpy hcltrolcpy = new Hcltrolcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Wssplife wssplife = new Wssplife();
	private Chdrpf chdrpf = new Chdrpf();
	private Chdrpf chdrpfData = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit000
	}

	public P2478low() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		hcltrolcpy.clntRoleInqRec = convertAndSetParam(hcltrolcpy.clntRoleInqRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			start000();
		}
		catch (GOTOException e){
		}
		finally{
			exit000();
		}
	}

protected void start000()
	{
		hcltrolcpy.statuz.set(varcom.oK);
		
	
		
		
		//chdrenqIO.setFunction(varcom.rlse);
	//	SmartFileCode.execute(appVars, chdrenqIO);
	//	if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
	//		syserrrec.params.set(chdrenqIO.getParams());
	//		databaseError900();
	//	}
		
		
	//	chdrenqIO.setParams(SPACES);
		//chdrenqIO.setChdrcoy(subString(hcltrolcpy.key[1], 1, 1));
		//chdrenqIO.setChdrnum(subString(hcltrolcpy.key[2], 1, 8));
		//chdrenqIO.setFormat(chdrenqrec);
		//chdrenqIO.setFunction(varcom.readr);
	//	SmartFileCode.execute(appVars, chdrenqIO);
	//	if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
	//		syserrrec.statuz.set(chdrenqIO.getStatuz());
	//		syserrrec.params.set(chdrenqIO.getParams());
	//		databaseError900();
	//	}
		
		
	//	chdrenqIO.setFunction(varcom.keeps);
	//	SmartFileCode.execute(appVars, chdrenqIO);
	//	if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
	//		syserrrec.statuz.set(chdrenqIO.getStatuz());
	//		syserrrec.params.set(chdrenqIO.getParams());
	//		databaseError900();
	//	}
		
		
		
		chdrpfDAO.deleteCacheObject(chdrpf);
		chdrpf.setChdrcoy(subString(hcltrolcpy.key[1], 1, 1).toString().charAt(0));
		chdrpf.setChdrnum(subString(hcltrolcpy.key[2], 1, 8).toString());
		chdrpf = chdrpfDAO.getchdrRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (isNE(chdrpf.getValidflag(),"1")) {
			hcltrolcpy.statuz.set(e704);
			goTo(GotoLabel.exit000);
		}
	
		chdrpfDAO.setCacheObject(chdrpf);
	}

protected void exit000()
	{
		exitProgram();
	}

protected void databaseError900()
	{
		/*DATABASE-ERROR*/
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		hcltrolcpy.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError1000()
	{
		/*SYSTEM-ERROR*/
		syserrrec.syserrStatuz.set(hcltrolcpy.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		hcltrolcpy.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
