/*
 * File: Crtundwrt.java
 * Date: 29 August 2009 22:42:40
 * Author: Quipoz Limited
 * 
 * Class transformed from CRTUNDWRT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.life.enquiries.dataaccess.UndrTableDAM;
import com.csc.life.enquiries.dataaccess.UndrchrTableDAM;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is a subprogram with 3 main functions, namely
*
* 1) ADD - To create record(s) in enquiry file, UNDRPF
* 2) MOD - To modify the Sum Assured in enquiry file, UNDRPF
* 3) DEL - To delete related record(s) in enquiry file, UNDRPF
*          depending on content of CRTABLE (ie. to delete all
*          related records when CRTABLE is '****' or to delete
*          specific record when CRTABLE is a unique code
*
*****************************************************************
* </pre>
*/
public class Crtundwrt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private BinaryData x = new BinaryData(5, 0);
		/* FORMATS */
	private String undrrec = "UNDRREC";
	private String undrchrrec = "UNDRCHRREC";

		/* ERRORS */
	private String invf = "INVF";
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private Th618rec th618rec = new Th618rec();
		/*U/W Logical File*/
	private UndrTableDAM undrIO = new UndrTableDAM();
		/*U/W Logical File By Contract Number*/
	private UndrchrTableDAM undrchrIO = new UndrchrTableDAM();
	private Varcom varcom = new Varcom();

	//ILIFE-6587 by wii31	
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t5446ListMap;
	private Map<String, List<Itempf>> t5448ListMap;
	private Map<String, List<Itempf>> th618ListMap;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1000, 
		exit1200, 
		exit2000, 
		exit2200, 
		exit2400, 
		exit3200, 
		exit3210
	}

	public Crtundwrt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		crtundwrec.parmRec = convertAndSetParam(crtundwrec.parmRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main1000()
	{
		try {
			ctrl1000();
		}
		catch (GOTOException e){
		}
		finally{
			exit1000();
		}
	}

protected void initSmartTable(){
	String coy = crtundwrec.coy.toString().trim();
	String pfx = smtpfxcpy.item.toString().trim();
	t5446ListMap = itemDAO.loadSmartTable(pfx, coy, "T5446");	
	t5448ListMap = itemDAO.loadSmartTable(pfx, coy, "T5448");
	th618ListMap = itemDAO.loadSmartTable(pfx, coy, "TH618");	
}
protected void ctrl1000()
	{
		crtundwrec.status.set(varcom.oK);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isEQ(crtundwrec.function,"DEL")
		&& crtundwrec.crtable.containsOnly("*")) {
			delSect2200();
			goTo(GotoLabel.exit1000);
		}
		initSmartTable();
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(crtundwrec.cnttyp.toString());
		stringVariable1.append(crtundwrec.crtable.toString());
		
		//ILIFE-6587 wli31
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5448ListMap.containsKey(stringVariable1.toString().trim())){	
			itempfList = t5448ListMap.get(stringVariable1.toString().trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(itempf.getItmfrm().compareTo(new BigDecimal(datcon1rec.intDate.toString())) < 1){
					t5448rec.t5448Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
				}else{
					crtundwrec.status.set(varcom.endp);
					goTo(GotoLabel.exit1000);
				}			
			}		
		}else{
			crtundwrec.status.set(varcom.endp);
			goTo(GotoLabel.exit1000);
		}
		
		
		String keyItemitem = t5448rec.rrsktyp.toString().trim();
		if (th618ListMap.containsKey(keyItemitem)) {
			itempfList = th618ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				th618rec.th618Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
		if (isEQ(th618rec.lrkclss,SPACES)) {
			crtundwrec.status.set(varcom.mrnf);
			goTo(GotoLabel.exit1000);
		}
		for (x.set(1); !(isGT(x,5)); x.add(1)){
			eachTh618Lrkcls1200();
		}
	}

protected void exit1000()
	{
		exitProgram();
	}

protected void eachTh618Lrkcls1200()
	{
		try {
			ctrl1200();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl1200()
	{
		if (isEQ(th618rec.lrkcls[x.toInt()],SPACES)) {
			goTo(GotoLabel.exit1200);
		}
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5446ListMap.containsKey(th618rec.lrkcls[x.toInt()].toString().trim())){	
			itempfList = t5446ListMap.get(th618rec.lrkcls[x.toInt()].toString().trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(itempf.getItmfrm().compareTo(new BigDecimal(datcon1rec.intDate.toString())) < 1){
					t5446rec.t5446Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
				}else{
					goTo(GotoLabel.exit1200);
				}			
			}		
		}else{
			goTo(GotoLabel.exit1200);
		}
		
		if (isEQ(t5446rec.lrkcls,SPACES)) {
			t5446rec.lrkcls.set(th618rec.lrkcls[x.toInt()]);
		}
		if (isEQ(crtundwrec.function,"ADD")){
			addSect2000();
		}
		else if (isEQ(crtundwrec.function,"DEL")){
			delSect2200();
		}
		else if (isEQ(crtundwrec.function,"MOD")){
			undrIO.setClntnum(crtundwrec.clntnum);
			undrIO.setCoy(crtundwrec.coy);
			undrIO.setCurrcode(crtundwrec.currcode);
			undrIO.setChdrnum(crtundwrec.chdrnum);
			undrIO.setCrtable(crtundwrec.crtable);
			undrIO.setLrkcls01(th618rec.lrkcls[x.toInt()]);
			undrIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			undrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			undrIO.setFitKeysSearch("CLNTNUM", "COY", "CURRCODE", "CHDRNUM", "CRTABLE");
			undrIO.setStatuz(varcom.oK);
			while ( !(isNE(undrIO.getStatuz(),varcom.oK))) {
				modSect2400();
			}
			
			crtundwrec.status.set(varcom.oK);
		}
		else{
			crtundwrec.status.set(invf);
			x.set(99);
		}
	}

protected void addSect2000()
	{
		try {
			ctrl2000();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl2000()
	{
		undrIO.setClntnum(crtundwrec.clntnum);
		undrIO.setCoy(crtundwrec.coy);
		undrIO.setCurrcode(crtundwrec.currcode);
		undrIO.setChdrnum(crtundwrec.chdrnum);
		undrIO.setCrtable(crtundwrec.crtable);
		undrIO.setLrkcls01(th618rec.lrkcls[x.toInt()]);
		undrIO.setLrkcls02(t5446rec.lrkcls);
		undrIO.setCnttyp(crtundwrec.cnttyp);
		undrIO.setLife(crtundwrec.life);
		undrIO.setSumins(crtundwrec.sumins);
		undrIO.setEffdate(datcon1rec.intDate);
		undrIO.setAdsc(SPACES);
		undrIO.setFunction(varcom.writr);
		callUndrio9200();
		if (isNE(undrIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit2000);
		}
	}

protected void delSect2200()
	{
		try {
			ctrl2200();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl2200()
	{
		if (isEQ(crtundwrec.clntnum,SPACES)) {
			undrchrIO.setRecKeyData(SPACES);
			undrchrIO.setStatuz(varcom.oK);
			undrchrIO.setCoy(crtundwrec.coy);
			undrchrIO.setChdrnum(crtundwrec.chdrnum);
			undrchrIO.setFormat(undrchrrec);
			undrchrIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			undrchrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
			while ( !(isNE(undrchrIO.getStatuz(),varcom.oK))) {
				delAllCrtable3210();
			}
			
			goTo(GotoLabel.exit2200);
		}
		undrIO.setRecKeyData(SPACES);
		undrIO.setClntnum(crtundwrec.clntnum);
		undrIO.setCoy(crtundwrec.coy);
		undrIO.setCurrcode(crtundwrec.currcode);
		undrIO.setChdrnum(crtundwrec.chdrnum);
		if (!crtundwrec.crtable.containsOnly("*")) {
			undrIO.setCrtable(crtundwrec.crtable);
			undrIO.setLrkcls01(th618rec.lrkcls[x.toInt()]);
		}
		undrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undrIO.setFitKeysSearch("CLNTNUM", "COY", "CURRCODE", "CHDRNUM");
		undrIO.setStatuz(varcom.oK);
		while ( !(isNE(undrIO.getStatuz(),varcom.oK))) {
			delAllCrtable3200();
		}
		
	}

protected void modSect2400()
	{
		try {
			ctrl2400();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl2400()
	{
		callUndrio9200();
		if (isNE(undrIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit2400);
		}
		undrIO.setFunction(varcom.readh);
		callUndrio9200();
		undrIO.setSumins(crtundwrec.sumins);
		undrIO.setFunction(varcom.rewrt);
		callUndrio9200();
		if (isNE(undrIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit2400);
		}
		undrIO.setFunction(varcom.nextr);
	}

protected void delAllCrtable3200()
	{
		try {
			ctrl3200();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl3200()
	{
		callUndrio9200();
		if (isNE(undrIO.getStatuz(),varcom.oK)) {
			crtundwrec.status.set(varcom.oK);
			goTo(GotoLabel.exit3200);
		}
		undrIO.setFunction(varcom.readh);
		callUndrio9200();
		undrIO.setFunction(varcom.delet);
		callUndrio9200();
		if (isNE(undrIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit3200);
		}
		undrIO.setFunction(varcom.begn);
	}

protected void delAllCrtable3210()
	{
		try {
			start3210();
		}
		catch (GOTOException e){
		}
	}

protected void start3210()
	{
		SmartFileCode.execute(appVars, undrchrIO);
		if (isNE(undrchrIO.getStatuz(),varcom.oK)
		&& isNE(undrchrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(undrchrIO.getParams());
			syserrrec.statuz.set(undrchrIO.getStatuz());
			xxxxDbError();
		}
		if (isEQ(undrchrIO.getStatuz(),varcom.endp)
		|| isNE(undrchrIO.getCoy(),crtundwrec.coy)
		|| isNE(undrchrIO.getChdrnum(),crtundwrec.chdrnum)) {
			crtundwrec.status.set(varcom.oK);
			undrchrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3210);
		}
		undrchrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undrchrIO);
		if (isNE(undrchrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(undrchrIO.getParams());
			syserrrec.statuz.set(undrchrIO.getStatuz());
			xxxxDbError();
		}
		undrchrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, undrchrIO);
		if (isNE(undrchrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(undrchrIO.getParams());
			syserrrec.statuz.set(undrchrIO.getStatuz());
			xxxxDbError();
		}
		undrchrIO.setFunction(varcom.begn);
	}


protected void callUndrio9200()
	{
		ctrl9200();
	}

protected void ctrl9200()
	{
		undrIO.setFormat(undrrec);
		SmartFileCode.execute(appVars, undrIO);
		if (isNE(undrIO.getStatuz(),varcom.oK)
		&& isNE(undrIO.getStatuz(),varcom.mrnf)
		&& isNE(undrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(undrIO.getParams());
			syserrrec.statuz.set(undrIO.getStatuz());
			xxxxDbError();
		}
		if (!(isEQ(undrIO.getStatuz(),varcom.oK)
		&& isEQ(crtundwrec.clntnum,undrIO.getClntnum())
		&& isEQ(crtundwrec.coy,undrIO.getCoy())
		&& isEQ(crtundwrec.currcode,undrIO.getCurrcode())
		&& isEQ(crtundwrec.chdrnum,undrIO.getChdrnum())
		&& (isEQ(crtundwrec.crtable,undrIO.getCrtable())
		|| crtundwrec.crtable.containsOnly("*"))
		&& (crtundwrec.crtable.containsOnly("*")
		|| isEQ(th618rec.lrkcls[x.toInt()],undrIO.getLrkcls01())))) {
			undrIO.setStatuz(varcom.endp);
		}
		if (isNE(undrIO.getStatuz(),varcom.oK)) {
			crtundwrec.parmRec.set(undrIO.getParams());
			crtundwrec.status.set(undrIO.getStatuz());
			undrIO.setRecNonKeyData(SPACES);
		}
	}

protected void xxxxDbError()
	{
		/*XXXX-START*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		crtundwrec.status.set(varcom.bomb);
		exitProgram();
	}
}
