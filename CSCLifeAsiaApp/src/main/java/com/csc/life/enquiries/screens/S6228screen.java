package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6228screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21, 20}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {13, 23, 4, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6228ScreenVars sv = (S6228ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6228screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6228ScreenVars screenVars = (S6228ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.name.setClassString("");
		screenVars.cltaddr01.setClassString("");
		screenVars.cltaddr02.setClassString("");
		screenVars.cltaddr03.setClassString("");
		screenVars.cltaddr04.setClassString("");
		screenVars.cltaddr05.setClassString("");
		screenVars.asgnCodeDesc.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.commfromDisp.setClassString("");
		screenVars.commtoDisp.setClassString("");
		screenVars.asgnnum.setClassString("");
		screenVars.asgnflag.setClassString("");
		screenVars.cltpcode.setClassString("");
	}

/**
 * Clear all the variables in S6228screen
 */
	public static void clear(VarModel pv) {
		S6228ScreenVars screenVars = (S6228ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.name.clear();
		screenVars.cltaddr01.clear();
		screenVars.cltaddr02.clear();
		screenVars.cltaddr03.clear();
		screenVars.cltaddr04.clear();
		screenVars.cltaddr05.clear();
		screenVars.asgnCodeDesc.clear();
		screenVars.reasoncd.clear();
		screenVars.commfromDisp.clear();
		screenVars.commfrom.clear();
		screenVars.commtoDisp.clear();
		screenVars.commto.clear();
		screenVars.asgnnum.clear();
		screenVars.asgnflag.clear();
		screenVars.cltpcode.clear();
	}
}
