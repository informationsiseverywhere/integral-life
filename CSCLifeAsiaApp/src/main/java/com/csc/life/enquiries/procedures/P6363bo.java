/*
 * File: P6363bo.java
 * Date: 30 August 2009 0:45:33
 * Author: Quipoz Limited
 * 
 * Class transformed from P6363BO.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;

import com.csc.fsu.general.recordstructures.Sdartnrec;
import com.csc.life.enquiries.programs.P6363;
import com.csc.life.enquiries.screens.S6363ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Msgdta;
import com.csc.smart.recordstructures.Sessiono;
import com.csc.smart.recordstructures.Sverrrec;
import com.csc.smart400framework.parent.Mainx;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*              P6363 Business Object
*
*  This object processes only one method
*
*         INQ - Inquiry
*
****************************************************************** ****
* </pre>
*/
public class P6363bo extends Mainx {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(12).init("P6363BO     ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaInKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaInChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaInKey, 0);
	private ZonedDecimalData wsaaInCcdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaInKey, 8);
	private Msgdta wsaaRequest = new Msgdta();
	private Msgdta wsaaResponse = new Msgdta();
	private Sessiono sessiono = new Sessiono();
	private Sverrrec validationError = new Sverrrec();
	private Wssplife wssplife = new Wssplife();
	private Sdartnrec sdartnrec = new Sdartnrec();
	private S6363ScreenVars sv1 = ScreenProgram.getScreenVars( S6363ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		aExit
	}

	public P6363bo() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sdartnrec.rec = convertAndSetParam(sdartnrec.rec, parmArray, 2);
		responseData = convertAndSetParam(responseData, parmArray, 1);
		ldrhdr.header = convertAndSetParam(ldrhdr.header, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		/*START*/
		scrnparams.statuz.set(varcom.oK);
		ldrhdr.errlvl.set("0");
		/*EXIT*/
	}

protected void callPrograms2000()
	{
		/*CALL*/
		wsspcomn.edterror.set(varcom.oK);
		wsspcomn.company.set(sdartnrec.sdarCompany);
		aInquiry();
		/*EXIT*/
	}

protected void aInquiry()
	{
		try {
			aP6363Start();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void aP6363Start()
	{
		wsspcomn.sbmaction.set("A");
		scrnparams.action.set("A");
		wsspcomn.sectionno.set("1000");
		callProgram(P6363.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		wsspcomn.sectionno.set("PRE");
		callProgram(P6363.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		wsaaInKey.set(sdartnrec.sdarInKey);
		sv1.chdrsel.set(wsaaInChdrnum);
		wsspcomn.sectionno.set("2000");
		callProgram(P6363.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		if (isNE(wsspcomn.edterror, varcom.oK)
		&& isNE(ldrhdr.opmode, "2")) {
			validationError.errorIndicators.set(sv1.errorIndicators);
			validationError.sverrLanguage.set(wsspcomn.language);
			validationError.msgid.set("SVERRREC");
			noSource("SVERR", validationError);
			validationError.msgcnt.set(1);
			compute(validationError.msglng, 0).set(length(validationError) - length(validationError.header));
			ldrhdr.totmsglng.set(length(validationError));
			responseData.set(validationError);
			ldrhdr.errlvl.set("1");
			goTo(GotoLabel.aExit);
		}
		wsspcomn.sectionno.set("3000");
		callProgram(P6363.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		sdartnrec.sdarBatchkey.set(wsspcomn.batchkey);
	}

protected void updateWsspTemp8000()
	{
		/*PARA*/
		sessiono.wssp.set(wsspcomn.commonArea);
		wsaaResponse.set(sessiono.rec);
		wsaaLdrhdr.set(ldrhdr.header);
		wsaaLdrhdr.objid.set("SESSION");
		wsaaLdrhdr.vrbid.set("UPD  ");
		noSource("SESSION", wsaaLdrhdr, wsaaRequest, wsaaResponse);
		/*EXIT*/
	}

protected void updateWssp9000()
	{
		/*PASS*/
		/*EXIT*/
	}
}
