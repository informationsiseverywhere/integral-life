package com.csc.life.enquiries.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:14
 * Description:
 * Copybook name: TR596REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr596rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr596Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData zrsacprd = new ZonedDecimalData(4, 0).isAPartOf(tr596Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(tr596Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr596Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr596Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}