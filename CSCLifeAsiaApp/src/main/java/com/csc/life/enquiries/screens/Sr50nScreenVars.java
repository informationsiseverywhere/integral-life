package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50N
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50nScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(805);
	public FixedLengthStringData dataFields = new FixedLengthStringData(373).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData optdscs = new FixedLengthStringData(120).isAPartOf(dataFields, 164);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(8, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optdsc05 = DD.optdsc.copy().isAPartOf(filler,60);
	public FixedLengthStringData optdsc06 = DD.optdsc.copy().isAPartOf(filler,75);
	public FixedLengthStringData optdsc07 = DD.optdsc.copy().isAPartOf(filler,90);
	public FixedLengthStringData optdsc08 = DD.optdsc.copy().isAPartOf(filler,105);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,284);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,294);
	// ILB-326:Starts
	public ZonedDecimalData trannosearch = DD.tranno.copyToZonedDecimal().isAPartOf(dataFields,297);
	public ZonedDecimalData datesubsearch = DD.datesub.copyToZonedDecimal().isAPartOf(dataFields,302);
	public ZonedDecimalData effdatesearch = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,310);
	public FixedLengthStringData trcodesearch = DD.trcode.copy().isAPartOf(dataFields,318);
	public FixedLengthStringData trandescsearch = DD.trandesc.copy().isAPartOf(dataFields,322);
	public FixedLengthStringData crtusersearch = DD.crtuser.copy().isAPartOf(dataFields,352);
	public FixedLengthStringData rdocnumsearch = DD.rdocnum.copy().isAPartOf(dataFields,362);
	public FixedLengthStringData statzsearch = DD.statz.copy().isAPartOf(dataFields,371);
	// ILB-326:Ends
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 373);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(8, 4, optdscsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData optdsc05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData optdsc06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData optdsc07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData optdsc08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	// ILB-326:Starts
	public FixedLengthStringData trannosearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData datesubsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData effdatesearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData trcodesearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData trandescsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData crtusersearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rdocnumsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData statzsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	// ILB-326:Ends
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(324).isAPartOf(dataArea, 481);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(8, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(96).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] optdsc05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] optdsc06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] optdsc07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] optdsc08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	// ILB-326:Starts
	public FixedLengthStringData[] trannosearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] datesubsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] effdatesearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] trcodesearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] trandescsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] crtusersearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rdocnumsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] statzsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	// ILB-326:Ends
		
	public FixedLengthStringData subfileArea = new FixedLengthStringData(289);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(111).isAPartOf(subfileArea, 0);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData datesub = DD.datesub.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public FixedLengthStringData descr = DD.descr.copy().isAPartOf(subfileFields,18);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,48);
	public FixedLengthStringData rdocnum = DD.rdocnum.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,69);
	public FixedLengthStringData statz = DD.statz.copy().isAPartOf(subfileFields,70);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(subfileFields,72);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,102);
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(subfileFields,107);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 111);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData datesubErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData descrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData rdocnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData statzErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 155);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] datesubOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] descrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] rdocnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] statzOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 287);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData datesubDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	
	// ILB-326:Starts
	public FixedLengthStringData datesubsearchDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdatesearchDisp = new FixedLengthStringData(10);
	// ILB-326:Ends

	public LongData Sr50nscreensflWritten = new LongData(0);
	public LongData Sr50nscreenctlWritten = new LongData(0);
	public LongData Sr50nscreenWritten = new LongData(0);
	public LongData Sr50nprotectWritten = new LongData(0);
	public GeneralTable sr50nscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50nscreensfl;
	}

	public Sr50nScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, tranno, effdate, trcode, trandesc, crtuser, datesub, rdocnum, statz, reasoncd, descr};
		screenSflOutFields = new BaseData[][] {selectOut, trannoOut, effdateOut, trcodeOut, trandescOut, crtuserOut, datesubOut, rdocnumOut, statzOut, reasoncdOut, descrOut};
		screenSflErrFields = new BaseData[] {selectErr, trannoErr, effdateErr, trcodeErr, trandescErr, crtuserErr, datesubErr, rdocnumErr, statzErr, reasoncdErr, descrErr};
		screenSflDateFields = new BaseData[] {effdate, datesub};
		screenSflDateErrFields = new BaseData[] {effdateErr, datesubErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp, datesubDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, optdsc01, optdsc02, optdsc03, optdsc04, optdsc05, optdsc06, optdsc07, optdsc08, trannosearch, datesubsearch, effdatesearch, trcodesearch, trandescsearch, crtusersearch, rdocnumsearch, statzsearch};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, optdsc01Out, optdsc02Out, optdsc03Out, optdsc04Out, optdsc05Out, optdsc06Out, optdsc07Out, optdsc08Out, trannosearchOut, datesubsearchOut, effdatesearchOut, trcodesearchOut, trandescsearchOut, crtusersearchOut, rdocnumsearchOut, statzsearchOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, optdsc01Err, optdsc02Err, optdsc03Err, optdsc04Err, optdsc05Err, optdsc06Err, optdsc07Err, optdsc08Err, trannosearchErr, datesubsearchErr, effdatesearchErr, trcodesearchErr, trandescsearchErr, crtusersearchErr, rdocnumsearchErr, statzsearchErr};
		screenDateFields = new BaseData[] {datesubsearch, effdatesearch};
		screenDateErrFields = new BaseData[] {datesubsearchErr, effdatesearchErr};
		screenDateDispFields = new BaseData[] {datesubsearchDisp, effdatesearchDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50nscreen.class;
		screenSflRecord = Sr50nscreensfl.class;
		screenCtlRecord = Sr50nscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50nprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50nscreenctl.lrec.pageSubfile);
	}
}
