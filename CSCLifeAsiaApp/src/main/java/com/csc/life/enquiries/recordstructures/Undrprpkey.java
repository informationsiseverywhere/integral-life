package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:35
 * Description:
 * Copybook name: UNDRPRPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undrprpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undrprpFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData undrprpKey = new FixedLengthStringData(64).isAPartOf(undrprpFileKey, 0, REDEFINE);
  	public FixedLengthStringData undrprpClntnum = new FixedLengthStringData(8).isAPartOf(undrprpKey, 0);
  	public FixedLengthStringData undrprpCoy = new FixedLengthStringData(1).isAPartOf(undrprpKey, 8);
  	public FixedLengthStringData undrprpCurrcode = new FixedLengthStringData(3).isAPartOf(undrprpKey, 9);
  	public FixedLengthStringData undrprpLrkcls02 = new FixedLengthStringData(4).isAPartOf(undrprpKey, 12);
  	public FixedLengthStringData undrprpChdrnum = new FixedLengthStringData(8).isAPartOf(undrprpKey, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(undrprpKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undrprpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undrprpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}