package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6222screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	//public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 16;
	public static int nextChangeIndicator = 94;
	//public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 21, 6, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6222ScreenVars sv = (S6222ScreenVars) pv;
	/*	if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6222screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6222screensfl, 
			sv.S6222screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();*/
		
		Subfile.write(av, pv, ind2, ind3, sv.s6222screensfl, maxRecords, sv.S6222screensflWritten, ROUTINE, sv.getScreenSflPfInds(), nextChangeIndicator);
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6222ScreenVars sv = (S6222ScreenVars) pv;
		/*TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6222screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();*/
		
		Subfile.update(av, pv, ind2, ROUTINE, sv.s6222screensfl, nextChangeIndicator);
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6222ScreenVars sv = (S6222ScreenVars) pv;
		/*DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6222screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6222screensflWritten.gt(0))
		{
			sv.s6222screensfl.setCurrentIndex(0);
			sv.S6222screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);*/
		Subfile.readNextChangedRecord(av, pv, ind2, ind3, sflIndex, sv.s6222screensfl, ROUTINE, sv.S6222screensflWritten, sv.getScreenSflAffectedInds());
	
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6222ScreenVars sv = (S6222ScreenVars) pv;
		/*DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6222screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);*/
		Subfile.chain(av, pv, record, ind2, ind3, sv.s6222screensfl, ROUTINE, sv.getScreenSflAffectedInds());
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	/*public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6222ScreenVars screenVars = (S6222ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.company.setFieldName("company");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.cnttype.setFieldName("cnttype");
				screenVars.crtable.setFieldName("crtable");
				screenVars.sicurr.setFieldName("sicurr");
				screenVars.sumin.setFieldName("sumin");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.company.set(dm.getField("company"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.cnttype.set(dm.getField("cnttype"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.sicurr.set(dm.getField("sicurr"));
			screenVars.sumin.set(dm.getField("sumin"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6222ScreenVars screenVars = (S6222ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.company.setFieldName("company");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.cnttype.setFieldName("cnttype");
				screenVars.crtable.setFieldName("crtable");
				screenVars.sicurr.setFieldName("sicurr");
				screenVars.sumin.setFieldName("sumin");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("company").set(screenVars.company);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("cnttype").set(screenVars.cnttype);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("sicurr").set(screenVars.sicurr);
			dm.getField("sumin").set(screenVars.sumin);
		}
	}*/

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6222screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
	/*	gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);*/
		
		S6222ScreenVars sv = (S6222ScreenVars)pv;
		Subfile.set1stScreenRow(gt, appVars, pv, sv.getScreenSflAffectedInds());
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		/*gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);*/
		
		S6222ScreenVars sv = (S6222ScreenVars)pv;
		Subfile.setNextScreenRow(gt, appVars, pv, sv.getScreenSflAffectedInds());
	}

	public static void clearFormatting(VarModel pv) {
		/*S6222ScreenVars screenVars = (S6222ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.company.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.cnttype.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.sicurr.clearFormatting();
		screenVars.sumin.clearFormatting();
		clearClassString(pv);*/
		
		Subfile.clearFormatting(pv);
	}

	/*public static void clearClassString(VarModel pv) {
		S6222ScreenVars screenVars = (S6222ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.company.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.sicurr.setClassString("");
		screenVars.sumin.setClassString("");
	}*/

/**
 * Clear all the variables in S6222screensfl
 */
	public static void clear(VarModel pv) {
		S6222ScreenVars screenVars = (S6222ScreenVars) pv;
		/*screenVars.screenIndicArea.clear();
		screenVars.company.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.crtable.clear();
		screenVars.sicurr.clear();
		screenVars.sumin.clear();*/
		ScreenRecord.clear(pv);
	}
}
