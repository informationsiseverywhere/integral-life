package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6240screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 22, 3, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6240ScreenVars sv = (S6240ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6240screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6240screensfl, 
			sv.S6240screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6240ScreenVars sv = (S6240ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6240screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6240ScreenVars sv = (S6240ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6240screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6240screensflWritten.gt(0))
		{
			sv.s6240screensfl.setCurrentIndex(0);
			sv.S6240screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6240ScreenVars sv = (S6240ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6240screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6240ScreenVars screenVars = (S6240ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.clntname.setFieldName("clntname");
				screenVars.clrole.setFieldName("clrole");
				screenVars.clrskind.setFieldName("clrskind");
				screenVars.delFlag.setFieldName("delFlag");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.clntnum.set(dm.getField("clntnum"));
			screenVars.clntname.set(dm.getField("clntname"));
			screenVars.clrole.set(dm.getField("clrole"));
			screenVars.clrskind.set(dm.getField("clrskind"));
			screenVars.delFlag.set(dm.getField("delFlag"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6240ScreenVars screenVars = (S6240ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.clntname.setFieldName("clntname");
				screenVars.clrole.setFieldName("clrole");
				screenVars.clrskind.setFieldName("clrskind");
				screenVars.delFlag.setFieldName("delFlag");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("clntnum").set(screenVars.clntnum);
			dm.getField("clntname").set(screenVars.clntname);
			dm.getField("clrole").set(screenVars.clrole);
			dm.getField("clrskind").set(screenVars.clrskind);
			dm.getField("delFlag").set(screenVars.delFlag);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6240screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6240ScreenVars screenVars = (S6240ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.clntnum.clearFormatting();
		screenVars.clntname.clearFormatting();
		screenVars.clrole.clearFormatting();
		screenVars.clrskind.clearFormatting();
		screenVars.delFlag.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6240ScreenVars screenVars = (S6240ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.clntname.setClassString("");
		screenVars.clrole.setClassString("");
		screenVars.clrskind.setClassString("");
		screenVars.delFlag.setClassString("");
	}

/**
 * Clear all the variables in S6240screensfl
 */
	public static void clear(VarModel pv) {
		S6240ScreenVars screenVars = (S6240ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.clntnum.clear();
		screenVars.clntname.clear();
		screenVars.clrole.clear();
		screenVars.clrskind.clear();
		screenVars.delFlag.clear();
	}
}
