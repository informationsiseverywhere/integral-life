package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5121screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 80}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 22, 4, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5121ScreenVars sv = (S5121ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5121screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5121screensfl, 
			sv.S5121screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5121ScreenVars sv = (S5121ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5121screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5121ScreenVars sv = (S5121ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5121screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5121screensflWritten.gt(0))
		{
			sv.s5121screensfl.setCurrentIndex(0);
			sv.S5121screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5121ScreenVars sv = (S5121ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5121screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5121ScreenVars screenVars = (S5121ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.fundShortDesc.setFieldName("fundShortDesc");
				screenVars.fundtype.setFieldName("fundtype");
				screenVars.fundTypeShortDesc.setFieldName("fundTypeShortDesc");
				screenVars.currentUnitBal.setFieldName("currentUnitBal");
				screenVars.currentDunitBal.setFieldName("currentDunitBal");
				screenVars.estval.setFieldName("estval");
				screenVars.effDateDisp.setFieldName("effDateDisp");				
				screenVars.uoffpr.setFieldName("uoffpr");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.fundShortDesc.set(dm.getField("fundShortDesc"));
			screenVars.fundtype.set(dm.getField("fundtype"));
			screenVars.fundTypeShortDesc.set(dm.getField("fundTypeShortDesc"));
			screenVars.currentUnitBal.set(dm.getField("currentUnitBal"));
			screenVars.currentDunitBal.set(dm.getField("currentDunitBal"));
			screenVars.estval.set(dm.getField("estval"));
			screenVars.effDateDisp.set(dm.getField("effDateDisp"));			
			screenVars.uoffpr.set(dm.getField("uoffpr"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5121ScreenVars screenVars = (S5121ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.fundShortDesc.setFieldName("fundShortDesc");
				screenVars.fundtype.setFieldName("fundtype");
				screenVars.fundTypeShortDesc.setFieldName("fundTypeShortDesc");
				screenVars.currentUnitBal.setFieldName("currentUnitBal");
				screenVars.currentDunitBal.setFieldName("currentDunitBal");
				screenVars.estval.setFieldName("estval");
				screenVars.effDateDisp.setFieldName("effDateDisp");
				screenVars.uoffpr.setFieldName("uoffpr");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("fundShortDesc").set(screenVars.fundShortDesc);
			dm.getField("fundtype").set(screenVars.fundtype);
			dm.getField("fundTypeShortDesc").set(screenVars.fundTypeShortDesc);
			dm.getField("currentUnitBal").set(screenVars.currentUnitBal);
			dm.getField("currentDunitBal").set(screenVars.currentDunitBal);
			dm.getField("estval").set(screenVars.estval);
			dm.getField("effDateDisp").set(screenVars.effDateDisp);
			dm.getField("uoffpr").set(screenVars.uoffpr);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5121screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5121ScreenVars screenVars = (S5121ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.fundShortDesc.clearFormatting();
		screenVars.fundtype.clearFormatting();
		screenVars.fundTypeShortDesc.clearFormatting();
		screenVars.currentUnitBal.clearFormatting();
		screenVars.currentDunitBal.clearFormatting();
		screenVars.estval.clearFormatting();
		screenVars.effDateDisp.clearFormatting();		
		screenVars.uoffpr.clearFormatting();		
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5121ScreenVars screenVars = (S5121ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.fundShortDesc.setClassString("");
		screenVars.fundtype.setClassString("");
		screenVars.fundTypeShortDesc.setClassString("");
		screenVars.currentUnitBal.setClassString("");
		screenVars.currentDunitBal.setClassString("");
		screenVars.estval.setClassString("");
		screenVars.effDateDisp.setClassString("");
		screenVars.uoffpr.setClassString("");
	}

/**
 * Clear all the variables in S5121screensfl
 */
	public static void clear(VarModel pv) {
		S5121ScreenVars screenVars = (S5121ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.fundShortDesc.clear();
		screenVars.fundtype.clear();
		screenVars.fundTypeShortDesc.clear();
		screenVars.currentUnitBal.clear();
		screenVars.currentDunitBal.clear();
		screenVars.estval.clear();
		screenVars.effDateDisp.clear();		
		screenVars.uoffpr.clear();
	}
}
