package com.csc.life.enquiries.dataaccess.model;

public class Asgnenqpf {
	
	public long	unique_number;
	public String	chdrcoy;
	public String	chdrnum;
	public String	asgnpfx;
	public String	asgnnum;
	public String	reasoncd;
	public int	commfrom;
	public int	commto;
	public int	tranno;
	public int	seqno;
	public String	trancde;
	
	
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getAsgnpfx() {
		return asgnpfx;
	}
	public void setAsgnpfx(String asgnpfx) {
		this.asgnpfx = asgnpfx;
	}
	public String getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(String asgnnum) {
		this.asgnnum = asgnnum;
	}
	public String getReasoncd() {
		return reasoncd;
	}
	public void setReasoncd(String reasoncd) {
		this.reasoncd = reasoncd;
	}
	public int getCommfrom() {
		return commfrom;
	}
	public void setCommfrom(int commfrom) {
		this.commfrom = commfrom;
	}
	public int getCommto() {
		return commto;
	}
	public void setCommto(int commto) {
		this.commto = commto;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getTrancde() {
		return trancde;
	}
	public void setTrancde(String trancde) {
		this.trancde = trancde;
	}
	

}
