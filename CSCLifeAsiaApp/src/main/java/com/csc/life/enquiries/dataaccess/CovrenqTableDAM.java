package com.csc.life.enquiries.dataaccess;

import com.csc.life.newbusiness.dataaccess.CovrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovrenqTableDAM.java
 * Date: Sun, 30 Aug 2009 03:34:53
 * Class transformed from COVRENQ.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovrenqTableDAM extends CovrpfTableDAM {

	public CovrenqTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("COVRENQ");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", PLNSFX"
		             + ", COVERAGE"
		             + ", RIDER";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "PLNSFX, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "CRTABLE, " +
		            "STATCODE, " +
		            "PSTATCODE, " +
		            "SUMINS, " +
		            "MORTCLS, " +
		            "LIENCD, " +
		            "JLIFE, " +
		            "RSUNIN, " +
		            "RUNDTE, " +
		            "SINGP, " +
		            "INSTPREM, " +
		            "ZLINSTPREM, " +
		            "ANBCCD, " +
		            "CBANPR, " +
		            "CRRCD, " +
		            "CBRVPR, " +
		            "CBUNST, " +
		            "NXTDTE, " +
		            "BBLDAT, " +
		            "CRDEBT, " +
		            "PCESDTE, " +
		            "RCESDTE, " +
		            "BCESDTE, " +
		            "RRTDAT, " +
		            "RRTFRM, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "PAYRSEQNO, " +
		            "BNUSIN, " +
		            "BAPPMETH, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "ZBINSTPREM, " + //TSD-306
		            "LOADPER, " + //TSD-306
			      	"RATEADJ, " + //TSD-306
			      	"FLTMORT, " + //TSD-306
			      	"PREMADJ, " + //TSD-306
			      	"AGEADJ, " + //TSD-306
			      	 "RCESAGE, " +
		            "PCESAGE, " +
		            "BCESAGE, " +
		            "RCESTRM, " +
		            "PCESTRM, " +
		            "BCESTRM, " +
		            "ZSTPDUTY01, " +
			      	"ZCLSTATE, "+
			      	"GMIB, "+
			      	"GMDB, "+
			      	"GMWB, "+
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "PLNSFX ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "PLNSFX DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               planSuffix,
                               coverage,
                               rider,
                               crtable,
                               statcode,
                               pstatcode,
                               sumins,
                               mortcls,
                               liencd,
                               jlife,
                               reserveUnitsInd,
                               reserveUnitsDate,
                               singp,
                               instprem,
                               zlinstprem,
                               anbAtCcd,
                               annivProcDate,
                               crrcd,
                               reviewProcessing,
                               unitStatementDate,
                               nextActDate,
                               benBillDate,
                               coverageDebt,
                               premCessDate,
                               riskCessDate,
                               benCessDate,
                               rerateDate,
                               rerateFromDate,
                               statFund,
                               statSect,
                               statSubsect,
                               validflag,
                               tranno,
                               payrseqno,
                               bonusInd,
                               bappmeth,
                               userProfile,
                               jobName,
                               datime,
                               zbinstprem, //TSD-306
                               loadper, //TSD-306
                               rateadj, //TSD-306
                               fltmort, //TSD-306
                               premadj, //TSD-306
                               ageadj, //TSD-306
                               riskCessAge,
                               premCessAge,
                               benCessAge,
                               riskCessTerm,
                               premCessTerm,
                               benCessTerm,
                               zstpduty01,
                               zclstate,
                               gmib,
                               gmdb,
                               gmwb,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getPlanSuffix().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(planSuffix.toInternal());
	nonKeyFiller5.setInternal(coverage.toInternal());
	nonKeyFiller6.setInternal(rider.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(322);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getCrtable().toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ getSumins().toInternal()
					+ getMortcls().toInternal()
					+ getLiencd().toInternal()
					+ getJlife().toInternal()
					+ getReserveUnitsInd().toInternal()
					+ getReserveUnitsDate().toInternal()
					+ getSingp().toInternal()
					+ getInstprem().toInternal()
					+ getZlinstprem().toInternal()
					+ getAnbAtCcd().toInternal()
					+ getAnnivProcDate().toInternal()
					+ getCrrcd().toInternal()
					+ getReviewProcessing().toInternal()
					+ getUnitStatementDate().toInternal()
					+ getNextActDate().toInternal()
					+ getBenBillDate().toInternal()
					+ getCoverageDebt().toInternal()
					+ getPremCessDate().toInternal()
					+ getRiskCessDate().toInternal()
					+ getBenCessDate().toInternal()
					+ getRerateDate().toInternal()
					+ getRerateFromDate().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getPayrseqno().toInternal()
					+ getBonusInd().toInternal()
					+ getBappmeth().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()				
					+ getZbinstprem().toString() //TSD-306
					+ getLoadper().toString() //TSD-306
					+ getRateadj().toString() //TSD-306
					+ getFltmort().toString() //TSD-306
					+ getPremadj().toString()
					+ getAgeadj().toString()
					+ getRiskCessAge().toInternal()
					+ getPremCessAge().toInternal()
					+ getBenCessAge().toInternal()
					+ getRiskCessTerm().toInternal()
					+ getPremCessTerm().toInternal()
					+ getBenCessTerm().toInternal()//TSD-306
					+ getZstpduty01().toString()
					+ getZclstate().toString()
					+ getGmib().toString()
					+ getGmdb().toString()
					+ getGmwb().toString());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, mortcls);
			what = ExternalData.chop(what, liencd);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, reserveUnitsInd);
			what = ExternalData.chop(what, reserveUnitsDate);
			what = ExternalData.chop(what, singp);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, zlinstprem);
			what = ExternalData.chop(what, anbAtCcd);
			what = ExternalData.chop(what, annivProcDate);
			what = ExternalData.chop(what, crrcd);
			what = ExternalData.chop(what, reviewProcessing);
			what = ExternalData.chop(what, unitStatementDate);
			what = ExternalData.chop(what, nextActDate);
			what = ExternalData.chop(what, benBillDate);
			what = ExternalData.chop(what, coverageDebt);
			what = ExternalData.chop(what, premCessDate);
			what = ExternalData.chop(what, riskCessDate);
			what = ExternalData.chop(what, benCessDate);
			what = ExternalData.chop(what, rerateDate);
			what = ExternalData.chop(what, rerateFromDate);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, payrseqno);
			what = ExternalData.chop(what, bonusInd);
			what = ExternalData.chop(what, bappmeth);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, zbinstprem); //TSD-306
			what = ExternalData.chop(what, loadper); //TSD-306
			what = ExternalData.chop(what, rateadj); //TSD-306
			what = ExternalData.chop(what, fltmort); //TSD-306
			what = ExternalData.chop(what, premadj); //TSD-306
			what = ExternalData.chop(what, ageadj); //TSD-306
			what = ExternalData.chop(what, riskCessAge);
			what = ExternalData.chop(what, premCessAge);
			what = ExternalData.chop(what, benCessAge);
			what = ExternalData.chop(what, riskCessTerm);
			what = ExternalData.chop(what, premCessTerm);
			what = ExternalData.chop(what, benCessTerm);
			what = ExternalData.chop(what, zstpduty01);
			what = ExternalData.chop(what, zclstate);
			what = ExternalData.chop(what, gmib);
			what = ExternalData.chop(what, gmdb);
			what = ExternalData.chop(what, gmwb);
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	
	public PackedDecimalData getZstpduty01() {
		return zstpduty01;
	}
	public void setZstpduty01 (Object zstpduty01) {
		this.zstpduty01.set(zstpduty01);
	}
	public FixedLengthStringData getZclstate() {
		return zclstate;
	}
	public void setZclstate(Object what) {
		this.zclstate.set(what);
	}
	public FixedLengthStringData getMortcls() {
		return mortcls;
	}
	public void setMortcls(Object what) {
		mortcls.set(what);
	}	
	public FixedLengthStringData getLiencd() {
		return liencd;
	}
	public void setLiencd(Object what) {
		liencd.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getReserveUnitsInd() {
		return reserveUnitsInd;
	}
	public void setReserveUnitsInd(Object what) {
		reserveUnitsInd.set(what);
	}	
	public PackedDecimalData getReserveUnitsDate() {
		return reserveUnitsDate;
	}
	public void setReserveUnitsDate(Object what) {
		setReserveUnitsDate(what, false);
	}
	public void setReserveUnitsDate(Object what, boolean rounded) {
		if (rounded)
			reserveUnitsDate.setRounded(what);
		else
			reserveUnitsDate.set(what);
	}	
	public PackedDecimalData getSingp() {
		return singp;
	}
	public void setSingp(Object what) {
		setSingp(what, false);
	}
	public void setSingp(Object what, boolean rounded) {
		if (rounded)
			singp.setRounded(what);
		else
			singp.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public PackedDecimalData getZlinstprem() {
		return zlinstprem;
	}
	public void setZlinstprem(Object what) {
		setZlinstprem(what, false);
	}
	public void setZlinstprem(Object what, boolean rounded) {
		if (rounded)
			zlinstprem.setRounded(what);
		else
			zlinstprem.set(what);
	}	
	public PackedDecimalData getAnbAtCcd() {
		return anbAtCcd;
	}
	public void setAnbAtCcd(Object what) {
		setAnbAtCcd(what, false);
	}
	public void setAnbAtCcd(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd.setRounded(what);
		else
			anbAtCcd.set(what);
	}	
	public PackedDecimalData getAnnivProcDate() {
		return annivProcDate;
	}
	public void setAnnivProcDate(Object what) {
		setAnnivProcDate(what, false);
	}
	public void setAnnivProcDate(Object what, boolean rounded) {
		if (rounded)
			annivProcDate.setRounded(what);
		else
			annivProcDate.set(what);
	}	
	public PackedDecimalData getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Object what) {
		setCrrcd(what, false);
	}
	public void setCrrcd(Object what, boolean rounded) {
		if (rounded)
			crrcd.setRounded(what);
		else
			crrcd.set(what);
	}	
	public PackedDecimalData getReviewProcessing() {
		return reviewProcessing;
	}
	public void setReviewProcessing(Object what) {
		setReviewProcessing(what, false);
	}
	public void setReviewProcessing(Object what, boolean rounded) {
		if (rounded)
			reviewProcessing.setRounded(what);
		else
			reviewProcessing.set(what);
	}	
	public PackedDecimalData getUnitStatementDate() {
		return unitStatementDate;
	}
	public void setUnitStatementDate(Object what) {
		setUnitStatementDate(what, false);
	}
	public void setUnitStatementDate(Object what, boolean rounded) {
		if (rounded)
			unitStatementDate.setRounded(what);
		else
			unitStatementDate.set(what);
	}	
	public PackedDecimalData getNextActDate() {
		return nextActDate;
	}
	public void setNextActDate(Object what) {
		setNextActDate(what, false);
	}
	public void setNextActDate(Object what, boolean rounded) {
		if (rounded)
			nextActDate.setRounded(what);
		else
			nextActDate.set(what);
	}	
	public PackedDecimalData getBenBillDate() {
		return benBillDate;
	}
	public void setBenBillDate(Object what) {
		setBenBillDate(what, false);
	}
	public void setBenBillDate(Object what, boolean rounded) {
		if (rounded)
			benBillDate.setRounded(what);
		else
			benBillDate.set(what);
	}	
	public PackedDecimalData getCoverageDebt() {
		return coverageDebt;
	}
	public void setCoverageDebt(Object what) {
		setCoverageDebt(what, false);
	}
	public void setCoverageDebt(Object what, boolean rounded) {
		if (rounded)
			coverageDebt.setRounded(what);
		else
			coverageDebt.set(what);
	}	
	public PackedDecimalData getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(Object what) {
		setPremCessDate(what, false);
	}
	public void setPremCessDate(Object what, boolean rounded) {
		if (rounded)
			premCessDate.setRounded(what);
		else
			premCessDate.set(what);
	}	
	public PackedDecimalData getRiskCessDate() {
		return riskCessDate;
	}
	public void setRiskCessDate(Object what) {
		setRiskCessDate(what, false);
	}
	public void setRiskCessDate(Object what, boolean rounded) {
		if (rounded)
			riskCessDate.setRounded(what);
		else
			riskCessDate.set(what);
	}	
	public PackedDecimalData getBenCessDate() {
		return benCessDate;
	}
	public void setBenCessDate(Object what) {
		setBenCessDate(what, false);
	}
	public void setBenCessDate(Object what, boolean rounded) {
		if (rounded)
			benCessDate.setRounded(what);
		else
			benCessDate.set(what);
	}
	public PackedDecimalData getRerateDate() {
		return rerateDate;
	}
	public void setRerateDate(Object what) {
		setRerateDate(what, false);
	}
	public void setRerateDate(Object what, boolean rounded) {
		if (rounded)
			rerateDate.setRounded(what);
		else
			rerateDate.set(what);
	}	
	public PackedDecimalData getRerateFromDate() {
		return rerateFromDate;
	}
	public void setRerateFromDate(Object what) {
		setRerateFromDate(what, false);
	}
	public void setRerateFromDate(Object what, boolean rounded) {
		if (rounded)
			rerateFromDate.setRounded(what);
		else
			rerateFromDate.set(what);
	}	
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}	
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}	
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(Object what) {
		setPayrseqno(what, false);
	}
	public void setPayrseqno(Object what, boolean rounded) {
		if (rounded)
			payrseqno.setRounded(what);
		else
			payrseqno.set(what);
	}	
	public FixedLengthStringData getBonusInd() {
		return bonusInd;
	}
	public void setBonusInd(Object what) {
		bonusInd.set(what);
	}	
	public FixedLengthStringData getBappmeth() {
		return bappmeth;
	}
	public void setBappmeth(Object what) {
		bappmeth.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	//TSD-306 Start
	public PackedDecimalData getLoadper() {
		return loadper;
	}
	public void setLoadper (Object loadper) {
		this.loadper.set(loadper);
	}
	public PackedDecimalData getRateadj() {
		return rateadj;
	}
	public void setRateadj (Object rateadj) {
		this.rateadj.set(rateadj);
	}
	public PackedDecimalData getFltmort() {
		return fltmort;
	}
	public void setFltmort (Object fltmort) {
		this.fltmort.set(fltmort);
	}
	public PackedDecimalData getPremadj() {
		return premadj;
	}
	public void setPremadj (Object premadj) {
		this.premadj.set(premadj);
	}
	public PackedDecimalData getAgeadj() {
		return ageadj;
	}
	public void setAgeadj (Object ageadj) {
		this.ageadj.set(ageadj);
	}
	public PackedDecimalData getZbinstprem() {
		return zbinstprem;
	}
	public void setZbinstprem (Object zbinstprem) {
		this.zbinstprem.set(zbinstprem);
	}
	public PackedDecimalData getRiskCessAge() {
		return riskCessAge;
	}
	public void setRiskCessAge(Object what) {
		setRiskCessAge(what, false);
	}
	public void setRiskCessAge(Object what, boolean rounded) {
		if (rounded)
			riskCessAge.setRounded(what);
		else
			riskCessAge.set(what);
	}	
	public PackedDecimalData getPremCessAge() {
		return premCessAge;
	}
	public void setPremCessAge(Object what) {
		setPremCessAge(what, false);
	}
	public void setPremCessAge(Object what, boolean rounded) {
		if (rounded)
			premCessAge.setRounded(what);
		else
			premCessAge.set(what);
	}	
	public PackedDecimalData getBenCessAge() {
		return benCessAge;
	}
	public void setBenCessAge(Object what) {
		setBenCessAge(what, false);
	}
	public void setBenCessAge(Object what, boolean rounded) {
		if (rounded)
			benCessAge.setRounded(what);
		else
			benCessAge.set(what);
	}	
	public PackedDecimalData getRiskCessTerm() {
		return riskCessTerm;
	}
	public void setRiskCessTerm(Object what) {
		setRiskCessTerm(what, false);
	}
	public void setRiskCessTerm(Object what, boolean rounded) {
		if (rounded)
			riskCessTerm.setRounded(what);
		else
			riskCessTerm.set(what);
	}	
	public PackedDecimalData getPremCessTerm() {
		return premCessTerm;
	}
	public void setPremCessTerm(Object what) {
		setPremCessTerm(what, false);
	}
	public void setPremCessTerm(Object what, boolean rounded) {
		if (rounded)
			premCessTerm.setRounded(what);
		else
			premCessTerm.set(what);
	}	
	public PackedDecimalData getBenCessTerm() {
		return benCessTerm;
	}
	public void setBenCessTerm(Object what) {
		setBenCessTerm(what, false);
	}
	public void setBenCessTerm(Object what, boolean rounded) {
		if (rounded)
			benCessTerm.setRounded(what);
		else
			benCessTerm.set(what);
	}	
	//TSD-306 End
	public FixedLengthStringData getGmib() {
		return gmib;
	}
	public void setGmib(Object what) {
		gmib.set(what);
	}	
	public FixedLengthStringData getGmdb() {
		return gmdb;
	}
	public void setGmdb(Object what) {
		gmdb.set(what);
	}
	public FixedLengthStringData getGmwb() {
		return gmwb;
	}
	public void setGmwb(Object what) {
		gmdb.set(what);
	}
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		planSuffix.clear();
		coverage.clear();
		rider.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		crtable.clear();
		statcode.clear();
		pstatcode.clear();
		sumins.clear();
		mortcls.clear();
		liencd.clear();
		jlife.clear();
		reserveUnitsInd.clear();
		reserveUnitsDate.clear();
		singp.clear();
		instprem.clear();
		zlinstprem.clear();
		anbAtCcd.clear();
		annivProcDate.clear();
		crrcd.clear();
		reviewProcessing.clear();
		unitStatementDate.clear();
		nextActDate.clear();
		benBillDate.clear();
		coverageDebt.clear();
		premCessDate.clear();
		riskCessDate.clear();
		benCessDate.clear();
		rerateDate.clear();
		rerateFromDate.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		validflag.clear();
		tranno.clear();
		payrseqno.clear();
		bonusInd.clear();
		bappmeth.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		//TSD-306 Start
		zbinstprem.clear();
		loadper.clear();
		rateadj.clear();
		fltmort.clear();
		premadj.clear();
		ageadj.clear();
		riskCessAge.clear();
		premCessAge.clear();
		benCessAge.clear();
		riskCessTerm.clear();
		premCessTerm.clear();
		benCessTerm.clear();
		//TSD-306 End
		zstpduty01.clear();
		zclstate.clear();
		gmib.clear();
		gmdb.clear();
		gmwb.clear();
	}


}