/*
 * File: P6240.java
 * Date: 30 August 2009 0:38:31
 * Author: Quipoz Limited
 * 
 * Class transformed from P6240.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClprTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Cltskey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S6240ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selectio 
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The details of the  contract  being  enquired  upon  will  b 
*     stored in the CHDRENQ  I/O  module.  Retrieve the details an 
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description fro 
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description fro 
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist 
*
*
*     Load the subfile as follows:
*
*          Perform BEGN on CLRF with  key of "CH", Contract Compan 
*          (FORECOY) and Contract  Number (FORENUM) and Client Rol 
*          set to spaces.
*
*          Continue Reading the  CLRF data-set until end of file o 
*          the Prefix, Contract Company or Contract number changes 
*
*          Display the Client number  and  decode  the  name in th 
*          normal way.  The  role  itself  will  be decoded agains 
*          table T3632.  Where  the  same Client Number is repeate 
*          blank out the Client  Cumber  and  Name so that only th 
*          role is displayed on the subsequent lines.
*
*     Load all pages required  in  the  subfile and set the subfil 
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation in this program.
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was requested  add  1  to  the  program pointer an 
*     exit.
*
*     Release any CLTS record that may have been stored.
*
*     At this point the  program  will  be either searching for th 
*     FIRST selected record in  order to pass control to the Clien 
*     Enquiry program for the  selected client number or it will b 
*     returning from the  Client  Enquiry  program after displayin 
*     some details and searching for the NEXT selected record.
*
*     It will be able to determine  which of these two states it i 
*     in by examining the Stack Action Flag.
*
*     If not returning from a Client display, (Stack Action Flag i 
*     blank), perform a start  on  the subfile to position the fil 
*     pointer at the beginning.
*
*     Each time it returns  to  this  program  after  processing   
*     previous seelction its position in the subfile will have bee 
*     retained and it will be  able  to continue from where it lef 
*     off.
*
*     Processing from here is  the same for either state. After th 
*     Start or after returning  to  the  program after processing  
*     previous selection read the next record from the subfile.  I 
*     this is not selected  (Select is blank), continue reading th 
*     next subfile record until  one  is  found  with  a  non-blan 
*     Select field or end of file  is reached. Do NOT use the 'Rea 
*     Next Changed Subfile Record' function.
*
*     If nothing was selected  or  there  are no more selections t 
*     process, continue by  moving  blanks  to  the  current  Stac 
*     Action field, and Program position and exit.
*
*     If a selection has  been  found the corresponding CLTS recor 
*     should be read and stored  in  the  I/O  module ready for th 
*     next program. Use the  selected  Client  number  to perform  
*     READS on CLTS. If no record is found report a system error.
*
*
* Notes.
* ------
*
*     Create a new view  of  PCDDPF  called PCDDENQ which uses onl 
*     those fields required for this program.
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T3639 - Client Roles                      Key: Client Role
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6240 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6240");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t3639 = "T3639";
	private static final String t5688 = "T5688";
	private static final String clprrec = "CLPRREC";
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ClprTableDAM clprIO = new ClprTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Cltskey wsaaCltskey = new Cltskey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S6240ScreenVars sv = ScreenProgram.getScreenVars( S6240ScreenVars.class);
	private FixedLengthStringData action = new FixedLengthStringData(1);
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class); 
  	private Clrrpf clrfIO = new Clrrpf();
  	private FixedLengthStringData longconfname1 = new FixedLengthStringData(122);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		bypassStart4010, 
		exit4090
	}

	public P6240() {
		super();
		screenVars = sv;
		new ScreenModel("S6240", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
	/*bug ILIFE-1008 start (Save action value for inquiring the client detail )*/
	   if(isNE(action,SPACE)){
	    wsspcomn.sbmaction.set(action);
	    action.set(SPACE);
	   }
	 /*bug ILIFE-1008 end*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6240", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(longconfname1);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO S6240-JLIFE                   */
		/*                                 S6240-JLIFENAME               */
		/*  ELSE                                                         */
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(longconfname1);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*    Read the first CLRF record for the contract.*/
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(wsspcomn.company.toString());
		clrfIO.setForenum(chdrpf.getChdrnum());
		List<Clrrpf> clrrpf = clrrpfDAO.getClrfRecordByForenum(clrfIO);
		if(clrrpf.isEmpty()){
			syserrrec.params.set(clrfIO.getForenum());
			fatalError600();
		}
		Iterator<Clrrpf> itClrrpf = clrrpf.iterator();
		while(itClrrpf.hasNext()){
			clrfIO = itClrrpf.next();
			processClrr1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*      (including subfile load section)
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		longconfname1.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		longconfname1.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		longconfname1.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
			stringVariable1.setStringInto(longconfname1);
		}
		else {
			longconfname1.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		longconfname1.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(longconfname1);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(longconfname1);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		longconfname1.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(longconfname1);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processClrr1100()
	{
		para1100();
	}

protected void para1100()
	{
		/*    Set up the Transaction details from the CLRF record.         */
		sv.subfileFields.set(SPACES);
		sv.clntnum.set(clrfIO.getClntnum());
		cltsIO.setClntnum(clrfIO.getClntnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.clntname.set(longconfname1);
		/*    Obtain the short description T3639.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		/* MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 */
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3639);
		descIO.setDescitem(clrfIO.getClrrrole());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.clrole.fill("?");
		}
		else {
			sv.clrole.set(descIO.getLongdesc());
		}
		if (isNE(clrfIO.getUsed2b(), SPACES)) {
			sv.delFlag.set("*");
		}
		else {
			sv.delFlag.set(SPACES);
		}
		/* Read CLPR to get risk indicator                                 */
		clprIO.setParams(SPACES);
		clprIO.setClntpfx(fsupfxcpy.clnt);
		clprIO.setClntcoy(wsspcomn.fsuco);
		clprIO.setClntnum(sv.clntnum);
		clprIO.setFunction(varcom.readr);
		clprIO.setFormat(clprrec);
		SmartFileCode.execute(appVars, clprIO);
		if (isNE(clprIO.getStatuz(), varcom.oK)
		&& isNE(clprIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(clprIO.getStatuz());
			syserrrec.params.set(clprIO.getParams());
			fatalError600();
		}
		if (isEQ(clprIO.getStatuz(), varcom.mrnf)) {
			sv.clrskind.set(SPACES);
		}
		else {
			sv.clrskind.set(clprIO.getClrskind());
		}
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6240", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO1*/
		/*    CALL 'S6240IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6240-DATA-AREA                         */
		/*                         S6240-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
				case bypassStart4010: 
					bypassStart4010();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*    Release any CLTS record that may have been stored.*/
		cltsIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    If returning from a program further down the stack then*/
		/*    bypass the start on the subfile.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.bypassStart4010);
		}
		/*    Re-start the subfile.*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6240", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypassStart4010()
	{
		if (isEQ(sv.select, SPACES)) {
			while ( !(isNE(sv.select, SPACES)
			|| isEQ(scrnparams.statuz, varcom.endp))) {
				readSubfile4100();
			}
			
		}
		/*  Nothing pressed at all, end working*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/* All requests services,*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			return ;
		}
		/* Blank out action to ensure it is not processed again*/
		sv.select.set(SPACES);
		/*    Read and store the selected CLTS record.*/
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntpfx("CN");
		wsaaCltskey.cltsClntpfx.set("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		wsaaCltskey.cltsClntcoy.set(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.clntnum);
		wsaaCltskey.cltsClntnum.set(sv.clntnum);
		cltsIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		/* ADD 1                       TO WSSP-PROGRAM-PTR.             */
		if (isEQ(cltsIO.getClttype(), "P")) {
			wsspcomn.programPtr.add(1);			
		}
		else {
			wsspcomn.programPtr.add(3);
		}
		
		/*bug ILIFE-1008 start (The action D is calling inquiry mode of P2465)*/
		if(isEQ(wsspcomn.secProg[wsspcomn.programPtr.toInt()],"P2465")){
		  action.set(wsspcomn.sbmaction);
		  wsspcomn.sbmaction.set("D");
		}
		/*bug ILIFE-1008 end*/
		
		wsspcomn.clntkey.set(wsaaCltskey);
	}

protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S6240", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
