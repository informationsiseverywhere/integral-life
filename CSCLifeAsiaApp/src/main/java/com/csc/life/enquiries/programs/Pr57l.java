/*
 * File: Pr57l.java
 * Date: 31 August 2009 11:47:41
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr57l.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;


import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;


import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifesurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.enquiries.screens.Sr57lScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
public class Pr57l extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5019");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");	
	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();	
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, "1");	
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);		
	private FixedLengthStringData wsaaFirstTimeFlag = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeThrough = new Validator(wsaaFirstTimeFlag, "Y");	
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LifesurTableDAM lifesurIO = new LifesurTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr57lScreenVars sv = ScreenProgram.getScreenVars( Sr57lScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();		
	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String e304 = "E304";
	private static final String surhclmrec = "SURHCLMREC";
	private Wssplife wssplife = new Wssplife();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 







	public Pr57l() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57l", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000()
	{	
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		processScreen("Sr57l", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);

		scrnparams.subfileRrn.set(1);
		sv.clamant.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.estimateTotalValue.set(ZERO);
		sv.netOfSvDebt.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.tdbtamt.set(ZERO);
		sv.zrcshamt.set(ZERO);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.planSuffix.set(ZERO);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrsurIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrsurIO);
		if(isNE(chdrsurIO.getStatuz(),varcom.oK)){
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrsurIO.getChdrnum());
		sv.cnttype.set(chdrsurIO.getCnttype());

		descIO.setDescitem(chdrsurIO.getCnttype());

		descIO.setDesctabl(t5688);
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.ctypedes.set(descIO.getLongdesc());
		}else{
			sv.ctypedes.set(SPACE);
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrsurIO.getStatcode());
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.rstate.set(descIO.getShortdesc());
		}else{
			sv.rstate.set(SPACE);
		}
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrsurIO.getPstatcode());
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.pstate.set(descIO.getShortdesc());
		}else{
			sv.pstate.set(SPACE);
		}
		sv.occdate.set(chdrsurIO.getOccdate());
		sv.cownnum.set(chdrsurIO.getCownnum());
		cltsIO.setClntnum(chdrsurIO.getCownnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else{
			//.set(SPACES);
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrsurIO.getBtdate());
		sv.ptdate.set(chdrsurIO.getPtdate());
		//initialize(surhclmIO.getParams());
		surhclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surhclmIO.setTranno(chdrsurIO.getTranno());
		/*surhclmIO.setTranno(0);*/
		surhclmIO.setPlanSuffix(0);
		surhclmIO.setFunction(varcom.readr);	
		surhclmIO.setFormat(surhclmrec);


		SmartFileCode.execute(appVars, surhclmIO);

		//add not in file
		if (isNE(surhclmIO.getStatuz(), varcom.oK)
				&& isEQ(surhclmIO.getStatuz(), varcom.mrnf)) {
			/*if(isNE(surhclmIO.getStatuz(),varcom.endp)&&isNE(surhclmIO.getStatuz(),varcom.oK)){*/
			syserrrec.statuz.set(surhclmIO.getStatuz());
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		if(isNE(chdrsurIO.getChdrcoy(),surhclmIO.getChdrcoy())
				||isNE(chdrsurIO.getChdrnum(),surhclmIO.getChdrnum())
				||isEQ(surhclmIO.getStatuz(),varcom.endp)){
			syserrrec.params.set(surdclmIO.getParams());
			fatalError600();
		}


		sv.effdate.set(surhclmIO.getEffdate());
		sv.currcd.set(surhclmIO.getCurrcd());
		sv.policyloan.set(surhclmIO.getPolicyloan());
		sv.tdbtamt.set(surhclmIO.getTdbtamt());
		sv.zrcshamt.set(surhclmIO.getZrcshamt());
		sv.otheradjst.set(surhclmIO.getOtheradjst());
		sv.taxamt.set(surhclmIO.getTaxamt());
		sv.reasoncd.set(surhclmIO.getReasoncd());
		sv.resndesc.set(surhclmIO.getResndesc());

		lifesurIO.setDataArea(SPACES);
		lifesurIO.setChdrcoy(surhclmIO.getChdrcoy());
		lifesurIO.setChdrnum(surhclmIO.getChdrnum());
		lifesurIO.setLife(surhclmIO.getLife());
		lifesurIO.setJlife(SPACES);
		lifesurIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifesurIO);
		if(isNE(lifesurIO.getStatuz(),varcom.oK)&&isNE(lifesurIO.getStatuz(),varcom.endp)){
			syserrrec.statuz.set(lifesurIO.getStatuz());
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}	
		if((isNE(surhclmIO.getChdrcoy(),lifesurIO.getChdrcoy()))
				|| (isNE(surhclmIO.getChdrnum(),lifesurIO.getChdrnum()))
				|| (isNE(surhclmIO.getLife(),lifesurIO.getLife()))
				||(isNE(lifesurIO.getJlife(),"00"))
				||(isEQ(lifesurIO.getStatuz(),varcom.endp))){


			syserrrec.statuz.set(lifesurIO.getStatuz());
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);}
		else{		
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		lifesurIO.setJlife("01");
		lifesurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifesurIO);
		if(isNE(lifesurIO.getStatuz(),varcom.oK)&&isNE(lifesurIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if(isNE(lifesurIO.getStatuz(),varcom.mrnf)){
			lifesurIO.setJlife(SPACES);
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);			
			continue1030();

		}
		//else{
			surdclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
			surdclmIO.setChdrnum(chdrsurIO.getChdrnum());
			surdclmIO.setTranno(surhclmIO.getTranno());
			surdclmIO.setPlanSuffix(surhclmIO.getPlanSuffix());
			surdclmIO.setLife(surhclmIO.getLife());
			surdclmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, surdclmIO);
			if (isNE(surdclmIO.getStatuz(), varcom.oK)
					&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(surdclmIO.getParams());
				fatalError600();
			}
			if (isNE(chdrsurIO.getChdrnum(), surdclmIO.getChdrnum())
					|| isNE(chdrsurIO.getChdrcoy(), surdclmIO.getChdrcoy())
					|| isEQ(surdclmIO.getStatuz(), varcom.endp)) { 
				surdclmIO.setStatuz(varcom.endp);
			}
			while(isNE(surdclmIO.getStatuz(),varcom.endp)){
				processSurdclm1500();
			}
			sv.estimateTotalValue.set(wsaaEstimateTot);
			if(isEQ(sv.estimateTotalValue,0)){
				//COMPUTE SZ001-ESTIMATE-TOTAL-VALUE = WSAA-ACTUAL-TOT   - ( SZ001-POLICYLOAN + SZ001-TDBTAMT + SZ001-TAXAMT) + SZ001-ZRCSHAMT + SZ001-OTHERADJST;
				compute(sv.estimateTotalValue, 2).set(sub(add(add(add(add(sv.policyloan,sv.tdbtamt),surhclmIO.getTaxamt()),sv.zrcshamt),sv.otheradjst),wsaaActualTot));

			}
	//	}

	}


	protected void continue1030()
	{
		sv.jlifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else{	
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}

	}


	protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
	private void findDesc1300(){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if(isNE(descIO.getStatuz(),varcom.oK)&&isNE(descIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

	private void getClientDetails1400(){
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if(isNE(cltsIO.getStatuz(),varcom.oK)&&isNE(cltsIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}






	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}





	private void processSurdclm1500(){

		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
				&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrsurIO.getChdrnum(), surdclmIO.getChdrnum())
				|| isNE(chdrsurIO.getChdrcoy(), surdclmIO.getChdrcoy())
				|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
			surdclmIO.setStatuz(varcom.endp);
			return;//ILIFE-7721
		}

		if (isEQ(chdrsurIO.getChdrnum(), surdclmIO.getChdrnum())){


			sv.estMatValue.set(ZERO);
			sv.hemv.set(ZERO);
			sv.hactval.set(ZERO);
			sv.actvalue.set(ZERO);
			sv.life.set(surdclmIO.getLife());
			sv.coverage.set(surdclmIO.getCoverage());
			sv.hcover.set(surdclmIO.getCoverage());
			if(isEQ(surdclmIO.getRider(),00)){
				sv.rider.set(SPACES);
			}else{
				sv.rider.set(surdclmIO.getRider());
			}
			if(isNE(surdclmIO.getRider(),00)){
				sv.coverage.set(SPACES);
			}
			sv.fund.set(surdclmIO.getCrtable());
			sv.fieldType.set(surdclmIO.getFieldType());
			sv.shortds.set(surdclmIO.getShortds());
			sv.cnstcur.set(surdclmIO.getCurrcd());
			sv.hcnstcur.set(surdclmIO.getCurrcd());
			sv.estMatValue.set(surdclmIO.getEstMatValue());
			sv.hemv.set(surdclmIO.getEstMatValue());
			sv.actvalue.set(surdclmIO.getActvalue());
			/*if(isNE(sz001scr.fieldType,c)){
				wsaaEstimateTot.add(SURDCLM-EST-MAT-VALUE);
				wsaaActualTot.add(SURDCLM-ACTVALUE);
			}
			if(isEQ(sz001scr.fieldType,c  subtract surdclmEstMatValue  from wsaaEstimateTot subtract surdclmActvalue       from wsaaActualTot)){
			}*/
			if (isNE(sv.fieldType, "C")) {
				wsaaEstimateTot.add(surdclmIO.getEstMatValue());
				wsaaActualTot.add(surdclmIO.getActvalue());
			}
			else {				
				compute(wsaaEstimateTot, 2).set(sub(wsaaEstimateTot, surdclmIO.getEstMatValue()));
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, surdclmIO.getActvalue()));
			}
			scrnparams.function.set(varcom.sadd);
			processScreen("Sr57l", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		surdclmIO.setFunction(varcom.nextr);
	}



	protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6234IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6234-DATA-AREA                         */
		/*                         S6234-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  No updates are required.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		} 
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.set(wsaaProg);;
		scrnparams.function.set("HIDEW"); 
		if(isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.edterror.set("Y");
		}

		/*EXIT*/	
	}


	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		/* ERRORS */	
		private FixedLengthStringData h960 = new FixedLengthStringData(4).init("H960");

	}
}
