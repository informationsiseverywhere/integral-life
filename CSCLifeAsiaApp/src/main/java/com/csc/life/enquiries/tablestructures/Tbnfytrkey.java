package com.csc.life.enquiries.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:37
 * Description:
 * Copybook name: TBNFYTRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tbnfytrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData tbnfytrFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData tbnfytrKey = new FixedLengthStringData(256).isAPartOf(tbnfytrFileKey, 0, REDEFINE);
  	public FixedLengthStringData tbnfytrChdrcoy = new FixedLengthStringData(1).isAPartOf(tbnfytrKey, 0);
  	public FixedLengthStringData tbnfytrChdrnum = new FixedLengthStringData(8).isAPartOf(tbnfytrKey, 1);
  	public FixedLengthStringData tbnfytrValidflag = new FixedLengthStringData(1).isAPartOf(tbnfytrKey, 9);
  	public PackedDecimalData tbnfytrTranno = new PackedDecimalData(5, 0).isAPartOf(tbnfytrKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(tbnfytrKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tbnfytrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tbnfytrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}