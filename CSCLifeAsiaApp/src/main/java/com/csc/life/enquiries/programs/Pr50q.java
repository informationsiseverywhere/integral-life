/*
 * File: Pr50q.java
 * Date: 30 August 2009 1:33:08
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50Q.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrtrnTableDAM;
import com.csc.life.enquiries.dataaccess.LifetrnTableDAM;
import com.csc.life.enquiries.screens.Sr50qScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* PR50Q - Policy Components
* The function provides a list of all the components that go to
* make up a contract at policy level. The details shown in the
* screen is the position of the contract as of the transaction
* time correct.
* The first line of the sub-file is the Life Assured , the sub-file
* program will allow user to go into the Life Assured screen (SR50R),
* view the detailed info. If a particular component is selected, user
* will be allowed to view the Component Details screen (SR50O).
*
* 1000-section
* - Initialize the screen sub-file
* - Call option switch using 'INIT' function
* - Retrieve CHDRTRX and PTRNENQ files using RETRV function
* - Set the screen header fields
* - Load sub-file from COVRENQ file
*
* 2000-section
* - If SR50Q-SELECT not spaces, call option switch using 'CHCK'
*   function
*
* 4000-section
* - Where the line selection is selected
*   Release LIFETRN and COVRTRN files using RLSE function
*   Keep LIFETRN file when LINETYPE= 'L' and keep COVRTRN when
*   LINETYPE='C'
* - Call option switch using 'STCK' function
*
***********************************************************************
* </pre>
*/
public class Pr50q extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50Q");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSubfileRrn = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaSubfileEnd = new ZonedDecimalData(5, 0).setUnsigned();
	private final String lineCoverage = "C";
	private final String lineLife = "L";

	private FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
	private FixedLengthStringData wsaaCovrenqLife = new FixedLengthStringData(2);
	private static final int wsaaToday = 0;
	private ZonedDecimalData ix = new ZonedDecimalData(5, 0).setUnsigned();
	private static final int wsaaMaxOpt = 8;
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
		/* WSAA-CLNT */
	private FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidLife = new FixedLengthStringData(1);
	private Validator validLife = new Validator(wsaaValidLife, "Y");

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1);
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5681 = "T5681";
	private static final String t5682 = "T5682";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String covrenqrec = "COVRENQREC";
	private static final String covrtrnrec = "COVRTRNREC";
	private static final String descrec = "DESCREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String lifetrnrec = "LIFETRNREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Header by Transaction Number*/
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Coverage by Tranno*/
	private CovrtrnTableDAM covrtrnIO = new CovrtrnTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Life by Tranno*/
	private LifetrnTableDAM lifetrnIO = new LifetrnTableDAM();
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
	private Desckey wsaaDesckey = new Desckey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Optswchrec optswchrec = new Optswchrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sr50qScreenVars sv = ScreenProgram.getScreenVars( Sr50qScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next4200, 
		exit4200, 
		coverage4220
	}

	public Pr50q() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50q", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		scrnparams.subfileRrn.set(1);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.trandate.set(varcom.vrcmMaxDate);
		sv.tranno.set(ZERO);
		initOptswch1100();
		retrvFiles1200();
		setScreenHeader1300();
		/* Load subfile.*/
		covrenqIO.setDataArea(SPACES);
		covrenqIO.setStatuz(varcom.oK);
		covrenqIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		covrenqIO.setChdrnum(chdrtrxIO.getChdrnum());
		covrenqIO.setLife(SPACES);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setCoverage(SPACES);
		covrenqIO.setRider(SPACES);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		|| isNE(covrenqIO.getChdrcoy(),chdrtrxIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),chdrtrxIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
		}
		wsaaCovrenqLife.set(SPACES);
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			loadSubfile1500();
		}
		
	}

protected void initOptswch1100()
	{
		begin1100();
	}

protected void begin1100()
	{
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		for (ix.set(1); !(isGT(ix,wsaaMaxOpt)
		|| isEQ(optswchrec.optsType[ix.toInt()],SPACES)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")) {
				sv.optdsc[ix.toInt()].set(optswchrec.optsDsc[ix.toInt()]);
			}
		}
	}

protected void retrvFiles1200()
	{
		begin1200();
	}

protected void begin1200()
	{
		chdrtrxIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		ptrnenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
	}

protected void setScreenHeader1300()
	{
		begin1300();
	}

protected void begin1300()
	{
		sv.chdrnum.set(chdrtrxIO.getChdrnum());
		sv.cnttype.set(chdrtrxIO.getCnttype());
		sv.currency.set(chdrtrxIO.getCntcurr());
		sv.register.set(chdrtrxIO.getRegister());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(lifelnbIO.getLifcnum());
		a1000CallNamadrs();
		sv.lifedesc.set(namadrsrec.name);
		/* Check for the existence of Joint Life details.*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isEQ(lifelnbIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifelnbIO.getLifcnum());
			wsaaClntPrefix.set(fsupfxcpy.clnt);
			wsaaClntCompany.set(wsspcomn.fsuco);
			wsaaClntNumber.set(lifelnbIO.getLifcnum());
			a1000CallNamadrs();
			sv.jlifedesc.set(namadrsrec.name);
		}
		/* Obtain the Contract Type description from T5688.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5688);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getCnttype());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypdesc.fill("?");
		}
		else {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3623);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getStatcode());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3588);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getPstatcode());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		sv.tranno.set(ptrnenqIO.getTranno());
		sv.trancd.set(ptrnenqIO.getBatctrcde());
		sv.trandate.set(ptrnenqIO.getDatesub());
		sv.effdate.set(ptrnenqIO.getPtrneff());
		if (isNE(ptrnenqIO.getCrtuser(),SPACES)) {
			sv.crtuser.set(ptrnenqIO.getCrtuser());
		}
		else {
			sv.crtuser.set(ptrnenqIO.getUserProfile());
		}
		/* Obtain the Transaction code description from T1688.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t1688);
		wsaaDesckey.descDescitem.set(ptrnenqIO.getBatctrcde());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.trandesc.fill("?");
		}
		else {
			/*    MOVE DESC-SHORTDESC      TO SR50Q-TRANDESC                */
			sv.trandesc.set(descIO.getLongdesc());
		}
	}

protected void loadSubfile1500()
	{
					begin1500();
					nextCovrenq1500();
				}

protected void begin1500()
	{
		if (isNE(covrenqIO.getLife(),wsaaCovrenqLife)) {
			wsaaValidLife.set("Y");
			checkValidLife1600();
			wsaaCovrenqLife.set(covrenqIO.getLife());
		}
		if (!validLife.isTrue()) {
			return ;
		}
		covrtrnIO.setDataArea(SPACES);
		covrtrnIO.setStatuz(varcom.oK);
		covrtrnIO.setChdrcoy(covrenqIO.getChdrcoy());
		covrtrnIO.setChdrnum(covrenqIO.getChdrnum());
		covrtrnIO.setLife(covrenqIO.getLife());
		covrtrnIO.setPlanSuffix(covrenqIO.getPlanSuffix());
		covrtrnIO.setCoverage(covrenqIO.getCoverage());
		covrtrnIO.setRider(covrenqIO.getRider());
		covrtrnIO.setTranno(ptrnenqIO.getTranno());
		covrtrnIO.setFormat(covrtrnrec);
		covrtrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrtrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrtrnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)
		&& isNE(covrtrnIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrtrnIO.getStatuz());
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)
		|| isNE(covrtrnIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(covrtrnIO.getChdrnum(),covrenqIO.getChdrnum())
		|| isNE(covrtrnIO.getLife(),covrenqIO.getLife())
		|| isNE(covrtrnIO.getPlanSuffix(),covrenqIO.getPlanSuffix())
		|| isNE(covrtrnIO.getCoverage(),covrenqIO.getCoverage())
		|| isNE(covrtrnIO.getRider(),covrenqIO.getRider())
		|| isLT(covrtrnIO.getTranno(),ptrnenqIO.getTranno())) {
			return ;
		}
		sv.subfileFields.set(SPACES);
		sv.linetype.set(lineCoverage);
		sv.select.set(SPACES);
		sv.hjlife.set(SPACES);
		sv.hsuffix.set(covrtrnIO.getPlanSuffix());
		sv.hlife.set(covrtrnIO.getLife());
		if (isEQ(covrtrnIO.getRider(),"00")) {
			wsaaCoverage.set(covrtrnIO.getCrtable());
			sv.component.set(wsaaCovrComponent);
			sv.cmpntnum.set(covrtrnIO.getCoverage());
		}
		else {
			wsaaRider.set(covrtrnIO.getCrtable());
			sv.component.set(wsaaRidrComponent);
			sv.cmpntnum.set(covrtrnIO.getRider());
		}
		sv.hcoverage.set(covrtrnIO.getCoverage());
		sv.hrider.set(covrtrnIO.getRider());
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5687);
		wsaaDesckey.descDescitem.set(covrtrnIO.getCrtable());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.compdesc.fill("?");
		}
		else {
			sv.compdesc.set(descIO.getLongdesc());
		}
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5681);
		wsaaDesckey.descDescitem.set(covrenqIO.getPstatcode());//ILIFE-7837
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatcode.fill("?");
		}
		else {
			sv.pstatcode.set(descIO.getShortdesc());
		}
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5682);
		wsaaDesckey.descDescitem.set(covrenqIO.getStatcode());//ILIFE-7837
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.statcode.fill("?");
		}
		else {
			sv.statcode.set(descIO.getShortdesc());
		}
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}

protected void nextCovrenq1500()
	{
		covrenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		|| isNE(covrenqIO.getChdrcoy(),chdrtrxIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),chdrtrxIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkValidLife1600()
	{
			begin1600();
		}

protected void begin1600()
	{
		/* Check valid life exists.*/
		lifetrnIO.setDataArea(SPACES);
		lifetrnIO.setStatuz(varcom.oK);
		lifetrnIO.setChdrcoy(covrenqIO.getChdrcoy());
		lifetrnIO.setChdrnum(covrenqIO.getChdrnum());
		lifetrnIO.setLife(covrenqIO.getLife());
		lifetrnIO.setJlife("00");
		lifetrnIO.setTranno(ptrnenqIO.getTranno());
		lifetrnIO.setFormat(lifetrnrec);
		lifetrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifetrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifetrnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE"); 
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)
		&& isNE(lifetrnIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			fatalError600();
		}
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)
		|| isNE(lifetrnIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(lifetrnIO.getChdrnum(),covrenqIO.getChdrnum())
		|| isNE(lifetrnIO.getLife(),covrenqIO.getLife())
		|| isNE(lifetrnIO.getJlife(),"00")) {
			wsaaValidLife.set("N");
			return ;
		}
		addLifeSubfile1700();
		/* Check valid joint life exists.*/
		lifetrnIO.setDataArea(SPACES);
		lifetrnIO.setStatuz(varcom.oK);
		lifetrnIO.setChdrcoy(covrenqIO.getChdrcoy());
		lifetrnIO.setChdrnum(covrenqIO.getChdrnum());
		lifetrnIO.setLife(covrenqIO.getLife());
		lifetrnIO.setJlife("01");
		lifetrnIO.setTranno(ptrnenqIO.getTranno());
		lifetrnIO.setFormat(lifetrnrec);
		lifetrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifetrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifetrnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE");
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)
		&& isNE(lifetrnIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			fatalError600();
		}
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)
		|| isNE(lifetrnIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(lifetrnIO.getChdrnum(),covrenqIO.getChdrnum())
		|| isNE(lifetrnIO.getLife(),covrenqIO.getLife())
		|| isNE(lifetrnIO.getJlife(),"01")) {
			return ;
		}
		addLifeSubfile1700();
	}

protected void addLifeSubfile1700()
	{
		begin1700();
	}

protected void begin1700()
	{
		sv.subfileFields.set(SPACES);
		sv.linetype.set(lineLife);
		sv.hcoverage.set(SPACES);
		sv.hrider.set(SPACES);
		sv.hsuffix.set(ZERO);
		sv.select.set(SPACES);
		if (isNE(lifetrnIO.getJlife(),"00")) {
			sv.cmpntnum.set("+");
			sv.hlife.set(lifetrnIO.getLife());
			sv.hjlife.set(lifetrnIO.getJlife());
		}
		else {
			sv.cmpntnum.set(lifetrnIO.getLife());
			sv.hlife.set(lifetrnIO.getLife());
			sv.hjlife.set(lifetrnIO.getJlife());
		}
		sv.component.set(lifetrnIO.getLifcnum());
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(lifetrnIO.getLifcnum());
		a1000CallNamadrs();
		sv.compdesc.set(namadrsrec.name);
		sv.pstatcode.set(SPACES);
		sv.statcode.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
		if (isNE(sv.select,SPACES)) {
			chckOptswch2700();
		}
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void chckOptswch2700()
	{
		begin2700();
	}

protected void begin2700()
	{
		optswchrec.optsStatuz.set(varcom.oK);
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelCode.set(SPACES);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
		stckOpts4010();
	}

protected void nextProgram4010()
	{
		scrnparams.statuz.set(varcom.oK);
		wsaaFoundSelection.set("N");
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz,varcom.endp)
		|| foundSelection.isTrue())) {
			lineSels4200();
		}
		
	}

protected void stckOpts4010()
	{
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(wsaaScrnStatuz,varcom.oK)) {
				scrnparams.subfileRrn.set(wsaaSubfileRrn);
				scrnparams.subfileEnd.set(wsaaSubfileEnd);
			}
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void lineSels4200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin4200();
				case next4200: 
					next4200();
				case exit4200: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin4200()
	{
		screenIo9000();
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit4200);
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.next4200);
		}
		for (ix.set(1); !(isGT(ix,wsaaMaxOpt)
		|| foundSelection.isTrue()); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")
			&& isEQ(optswchrec.optsCode[ix.toInt()],sv.select)) {
				optswchrec.optsInd[ix.toInt()].set("X");
				wsaaFoundSelection.set("Y");
				rlseFiles4210();
				keepsFiles4220();
			}
		}
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelCode.set(SPACES);
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		wsaaSubfileRrn.set(scrnparams.subfileRrn);
		wsaaSubfileEnd.set(scrnparams.subfileEnd);
	}

protected void next4200()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void rlseFiles4210()
	{
		begin4210();
	}

protected void begin4210()
	{
		lifetrnIO.setFormat(lifetrnrec);
		lifetrnIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			fatalError600();
		}
		covrtrnIO.setFormat(covrtrnrec);
		covrtrnIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrtrnIO.getStatuz());
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
	}

protected void keepsFiles4220()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin4220();
				case coverage4220: 
					coverage4220();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin4220()
	{
		if (isNE(sv.linetype,lineLife)) {
			goTo(GotoLabel.coverage4220);
		}
		lifetrnIO.setDataArea(SPACES);
		lifetrnIO.setStatuz(varcom.oK);
		lifetrnIO.setChdrcoy(wsspcomn.company);
		lifetrnIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifetrnIO.setLife(sv.hlife);
		lifetrnIO.setJlife(sv.hjlife);
		lifetrnIO.setTranno(sv.tranno);
		lifetrnIO.setFormat(lifetrnrec);
		lifetrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifetrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifetrnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE");
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)
		&& isNE(lifetrnIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			fatalError600();
		}
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)
		|| isNE(lifetrnIO.getChdrcoy(),wsspcomn.company)
		|| isNE(lifetrnIO.getChdrnum(),chdrtrxIO.getChdrnum())
		|| isNE(lifetrnIO.getLife(),sv.hlife)
		|| isNE(lifetrnIO.getJlife(),sv.hjlife)) {
			lifetrnIO.setDataArea(SPACES);
			lifetrnIO.setStatuz(varcom.oK);
			lifetrnIO.setChdrcoy(wsspcomn.company);
			lifetrnIO.setChdrnum(chdrtrxIO.getChdrnum());
			lifetrnIO.setLife(sv.hlife);
			lifetrnIO.setJlife(sv.hjlife);
			lifetrnIO.setTranno(sv.tranno);
			lifetrnIO.setFormat(lifetrnrec);
			lifetrnIO.setFunction(varcom.endr);
			SmartFileCode.execute(appVars, lifetrnIO);
			if (isNE(lifetrnIO.getStatuz(),varcom.oK)
			&& isNE(lifetrnIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(lifetrnIO.getStatuz());
				syserrrec.params.set(lifetrnIO.getParams());
				fatalError600();
			}
			if (isNE(lifetrnIO.getStatuz(),varcom.oK)
			|| isNE(lifetrnIO.getChdrcoy(),wsspcomn.company)
			|| isNE(lifetrnIO.getChdrnum(),chdrtrxIO.getChdrnum())
			|| isNE(lifetrnIO.getLife(),sv.hlife)
			|| isNE(lifetrnIO.getJlife(),sv.hjlife)) {
				syserrrec.statuz.set(lifetrnIO.getStatuz());
				syserrrec.params.set(lifetrnIO.getParams());
				fatalError600();
			}
		}
		lifetrnIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			fatalError600();
		}
	}

protected void coverage4220()
	{
		if (isNE(sv.linetype,lineCoverage)) {
			return ;
		}
		covrtrnIO.setDataArea(SPACES);
		covrtrnIO.setStatuz(varcom.oK);
		covrtrnIO.setChdrcoy(wsspcomn.company);
		covrtrnIO.setChdrnum(chdrtrxIO.getChdrnum());
		covrtrnIO.setLife(sv.hlife);
		covrtrnIO.setPlanSuffix(sv.hsuffix);
		covrtrnIO.setCoverage(sv.hcoverage);
		covrtrnIO.setRider(sv.hrider);
		covrtrnIO.setTranno(sv.tranno);
		covrtrnIO.setFormat(covrtrnrec);
		covrtrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrtrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrtrnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrtrnIO.getStatuz());
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)
		|| isNE(covrtrnIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrtrnIO.getChdrnum(),chdrtrxIO.getChdrnum())
		|| isNE(covrtrnIO.getLife(),sv.hlife)
		|| isNE(covrtrnIO.getPlanSuffix(),sv.hsuffix)
		|| isNE(covrtrnIO.getCoverage(),sv.hcoverage)
		|| isNE(covrtrnIO.getRider(),sv.hrider)) {
			covrtrnIO.setStatuz(varcom.oK);
			covrtrnIO.setChdrcoy(wsspcomn.company);
			covrtrnIO.setChdrnum(chdrtrxIO.getChdrnum());
			covrtrnIO.setLife(sv.hlife);
			covrtrnIO.setPlanSuffix(sv.hsuffix);
			covrtrnIO.setCoverage(sv.hcoverage);
			covrtrnIO.setRider(sv.hrider);
			covrtrnIO.setTranno(sv.tranno);
			covrtrnIO.setFormat(covrtrnrec);
			covrtrnIO.setFunction(varcom.endr);
			SmartFileCode.execute(appVars, covrtrnIO);
			if (isNE(covrtrnIO.getStatuz(),varcom.oK)
			&& isNE(covrtrnIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(covrtrnIO.getStatuz());
				syserrrec.params.set(covrtrnIO.getParams());
				fatalError600();
			}
			if (isNE(covrtrnIO.getStatuz(),varcom.oK)
			|| isNE(covrtrnIO.getChdrcoy(),wsspcomn.company)
			|| isNE(covrtrnIO.getChdrnum(),chdrtrxIO.getChdrnum())
			|| isNE(covrtrnIO.getLife(),sv.hlife)
			|| isNE(covrtrnIO.getPlanSuffix(),sv.hsuffix)
			|| isNE(covrtrnIO.getCoverage(),sv.hcoverage)
			|| isNE(covrtrnIO.getRider(),sv.hrider)) {
				syserrrec.statuz.set(covrtrnIO.getStatuz());
				syserrrec.params.set(covrtrnIO.getParams());
				fatalError600();
			}
		}
		covrtrnIO.setFormat(covrtrnrec);
		covrtrnIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrtrnIO.getStatuz());
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
	}

protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen("SR50Q", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a1000CallNamadrs()
	{
		a1000Begin();
	}

protected void a1000Begin()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void a2000GetDesc()
	{
		a2000Begin();
	}

protected void a2000Begin()
	{
		descIO.setDataArea(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsaaDesckey.descDesccoy);
		descIO.setDesctabl(wsaaDesckey.descDesctabl);
		descIO.setDescitem(wsaaDesckey.descDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
}
