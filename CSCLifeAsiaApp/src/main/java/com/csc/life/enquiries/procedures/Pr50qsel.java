/*
 * File: Pr50qsel.java
 * Date: 30 August 2009 1:33:21
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50QSEL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.dataaccess.CovrtrnTableDAM;
import com.csc.life.enquiries.dataaccess.LifetrnTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*    This is a Variable Dialogue subroutine. It is called
*    by OPTSWCH.
*
***********************************************************************
* </pre>
*/
public class Pr50qsel extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("PR50QSE");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String covrtrnrec = "COVRTRNREC";
	private String lifetrnrec = "LIFETRNREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaPrograms = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaProgram = FLSArrayPartOfStructure(4, 5, wsaaPrograms, 0);
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Coverage by Tranno*/
	private CovrtrnTableDAM covrtrnIO = new CovrtrnTableDAM();
		/*Life by Tranno*/
	private LifetrnTableDAM lifetrnIO = new LifetrnTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit0090, 
		errorProg620
	}

	public Pr50qsel() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 2);
		wsaaPrograms = convertAndSetParam(wsaaPrograms, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			begin0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit0090();
		}
	}

protected void begin0010()
	{
		wsaaChkpStatuz.set(varcom.oK);
		wsaaPrograms.set(SPACES);
		covrtrnIO.setFunction(varcom.retrv);
		covrtrnIO.setFormat(covrtrnrec);
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)
		&& isNE(covrtrnIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
		if (isEQ(covrtrnIO.getStatuz(),varcom.oK)) {
			wsaaProgram[1].set("PR50O");
			wsaaProgram[2].set(SPACES);
			wsaaProgram[3].set(SPACES);
			wsaaProgram[4].set(SPACES);
			goTo(GotoLabel.exit0090);
		}
		lifetrnIO.setFunction(varcom.retrv);
		lifetrnIO.setFormat(lifetrnrec);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)
		&& isNE(lifetrnIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifetrnIO.getParams());
			fatalError600();
		}
		if (isEQ(lifetrnIO.getStatuz(),varcom.oK)) {
			wsaaProgram[1].set("PR50R");
			wsaaProgram[2].set(SPACES);
			wsaaProgram[3].set(SPACES);
			wsaaProgram[4].set(SPACES);
			goTo(GotoLabel.exit0090);
		}
		else {
			wsaaProgram[1].set(SPACES);
			wsaaProgram[2].set(SPACES);
			wsaaProgram[3].set(SPACES);
			wsaaProgram[4].set(SPACES);
		}
	}

protected void exit0090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError610();
				}
				case errorProg620: {
					errorProg620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg620);
		}
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg620()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
