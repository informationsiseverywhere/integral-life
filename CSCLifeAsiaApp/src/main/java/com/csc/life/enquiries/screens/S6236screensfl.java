package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6236screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 13;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {20, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 22, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6236ScreenVars sv = (S6236ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6236screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6236screensfl, 
			sv.S6236screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6236ScreenVars sv = (S6236ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6236screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6236ScreenVars sv = (S6236ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6236screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6236screensflWritten.gt(0))
		{
			sv.s6236screensfl.setCurrentIndex(0);
			sv.S6236screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6236ScreenVars sv = (S6236ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6236screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6236ScreenVars screenVars = (S6236ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hcurrcd.setFieldName("hcurrcd");
				screenVars.hacccurr.setFieldName("hacccurr");
				screenVars.trcode.setFieldName("trcode");
				screenVars.bankcode.setFieldName("bankcode");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.tranno.setFieldName("tranno");
				screenVars.glcode.setFieldName("glcode");
				screenVars.hmhii.setFieldName("hmhii");
				screenVars.hmhli.setFieldName("hmhli");
				screenVars.rdocnum.setFieldName("rdocnum");
				screenVars.statz.setFieldName("statz");
				screenVars.select.setFieldName("select");
				screenVars.datesubDisp.setFieldName("datesubDisp");//ILJ-388
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hcurrcd.set(dm.getField("hcurrcd"));
			screenVars.hacccurr.set(dm.getField("hacccurr"));
			screenVars.trcode.set(dm.getField("trcode"));
			screenVars.bankcode.set(dm.getField("bankcode"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.tranno.set(dm.getField("tranno"));
			screenVars.glcode.set(dm.getField("glcode"));
			screenVars.hmhii.set(dm.getField("hmhii"));
			screenVars.hmhli.set(dm.getField("hmhli"));
			screenVars.rdocnum.set(dm.getField("rdocnum"));
			screenVars.statz.set(dm.getField("statz"));
			screenVars.select.set(dm.getField("select"));
			screenVars.datesubDisp.set(dm.getField("datesubDisp"));//ILJ-388
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6236ScreenVars screenVars = (S6236ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hcurrcd.setFieldName("hcurrcd");
				screenVars.hacccurr.setFieldName("hacccurr");
				screenVars.trcode.setFieldName("trcode");
				screenVars.bankcode.setFieldName("bankcode");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.tranno.setFieldName("tranno");
				screenVars.glcode.setFieldName("glcode");
				screenVars.hmhii.setFieldName("hmhii");
				screenVars.hmhli.setFieldName("hmhli");
				screenVars.rdocnum.setFieldName("rdocnum");
				screenVars.statz.setFieldName("statz");
				screenVars.select.setFieldName("select");
				screenVars.datesubDisp.setFieldName("datesubDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hcurrcd").set(screenVars.hcurrcd);
			dm.getField("hacccurr").set(screenVars.hacccurr);
			dm.getField("trcode").set(screenVars.trcode);
			dm.getField("bankcode").set(screenVars.bankcode);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("glcode").set(screenVars.glcode);
			dm.getField("hmhii").set(screenVars.hmhii);
			dm.getField("hmhli").set(screenVars.hmhli);
			dm.getField("rdocnum").set(screenVars.rdocnum);
			dm.getField("statz").set(screenVars.statz);
			dm.getField("select").set(screenVars.select);
			dm.getField("datesubDisp").set(screenVars.datesubDisp);//ILJ-388
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6236screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6236ScreenVars screenVars = (S6236ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hcurrcd.clearFormatting();
		screenVars.hacccurr.clearFormatting();
		screenVars.trcode.clearFormatting();
		screenVars.bankcode.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.glcode.clearFormatting();
		screenVars.hmhii.clearFormatting();
		screenVars.hmhli.clearFormatting();
		screenVars.rdocnum.clearFormatting();
		screenVars.statz.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.datesubDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6236ScreenVars screenVars = (S6236ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hcurrcd.setClassString("");
		screenVars.hacccurr.setClassString("");
		screenVars.trcode.setClassString("");
		screenVars.bankcode.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.glcode.setClassString("");
		screenVars.hmhii.setClassString("");
		screenVars.hmhli.setClassString("");
		screenVars.rdocnum.setClassString("");
		screenVars.statz.setClassString("");
		screenVars.select.setClassString("");
		screenVars.datesubDisp.setClassString("");
	}

/**
 * Clear all the variables in S6236screensfl
 */
	public static void clear(VarModel pv) {
		S6236ScreenVars screenVars = (S6236ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hcurrcd.clear();
		screenVars.hacccurr.clear();
		screenVars.trcode.clear();
		screenVars.bankcode.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.tranno.clear();
		screenVars.glcode.clear();
		screenVars.hmhii.clear();
		screenVars.hmhli.clear();
		screenVars.rdocnum.clear();
		screenVars.statz.clear();
		screenVars.select.clear();
		screenVars.datesubDisp.clear();
	}
}
