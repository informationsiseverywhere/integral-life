package com.csc.life.enquiries.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UfnxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:42
 * Class transformed from UFNXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UfnxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 15;
	public FixedLengthStringData ufnxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ufnxpfRecord = ufnxrec;
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(ufnxrec);
	public FixedLengthStringData virtualFund = DD.vrtfund.copy().isAPartOf(ufnxrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(ufnxrec);
	public PackedDecimalData nofunts = DD.nofunts.copy().isAPartOf(ufnxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UfnxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for UfnxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UfnxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UfnxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UfnxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UfnxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UfnxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UFNXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"COMPANY, " +
							"VRTFUND, " +
							"UNITYP, " +
							"NOFUNTS, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     company,
                                     virtualFund,
                                     unitType,
                                     nofunts,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		company.clear();
  		virtualFund.clear();
  		unitType.clear();
  		nofunts.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUfnxrec() {
  		return ufnxrec;
	}

	public FixedLengthStringData getUfnxpfRecord() {
  		return ufnxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUfnxrec(what);
	}

	public void setUfnxrec(Object what) {
  		this.ufnxrec.set(what);
	}

	public void setUfnxpfRecord(Object what) {
  		this.ufnxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ufnxrec.getLength());
		result.set(ufnxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}