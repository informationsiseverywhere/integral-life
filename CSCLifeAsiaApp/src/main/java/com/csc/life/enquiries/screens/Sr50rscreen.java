package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50rscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50rScreenVars sv = (Sr50rScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50rscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50rScreenVars screenVars = (Sr50rScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifedesc.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.sex.setClassString("");
		screenVars.dobDisp.setClassString("");
		screenVars.anbage.setClassString("");
		screenVars.selection.setClassString("");
		screenVars.smoking.setClassString("");
		screenVars.occup.setClassString("");
		screenVars.pursuit01.setClassString("");
		screenVars.pursuit02.setClassString("");
		screenVars.height.setClassString("");
		screenVars.weight.setClassString("");
		screenVars.bmi.setClassString("");
		screenVars.relation.setClassString("");
	}

/**
 * Clear all the variables in Sr50rscreen
 */
	public static void clear(VarModel pv) {
		Sr50rScreenVars screenVars = (Sr50rScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.lifenum.clear();
		screenVars.lifedesc.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.sex.clear();
		screenVars.dobDisp.clear();
		screenVars.dob.clear();
		screenVars.anbage.clear();
		screenVars.selection.clear();
		screenVars.smoking.clear();
		screenVars.occup.clear();
		screenVars.pursuit01.clear();
		screenVars.pursuit02.clear();
		screenVars.height.clear();
		screenVars.weight.clear();
		screenVars.bmi.clear();
		screenVars.relation.clear();
	}
}
