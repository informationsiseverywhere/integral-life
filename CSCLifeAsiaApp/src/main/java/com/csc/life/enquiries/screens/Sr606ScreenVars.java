package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR606
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr606ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(159);
	public FixedLengthStringData dataFields = new FixedLengthStringData(79).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData contdesc = DD.contdesc.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData cownname = DD.cownname.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 79);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData contdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 99);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] contdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(146);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(64).isAPartOf(subfileArea, 0);
	public ZonedDecimalData acctamt = DD.acctamt.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData chqnum = DD.chqnum.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData rdocnum = DD.rdocnum.copy().isAPartOf(subfileFields,27);
	public ZonedDecimalData trandate = DD.trandate.copyToZonedDecimal().isAPartOf(subfileFields,36);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 64);
	public FixedLengthStringData acctamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData chqnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData rdocnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData trandateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData zdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 84);
	public FixedLengthStringData[] acctamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] chqnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] rdocnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] trandateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 144);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData trandateDisp = new FixedLengthStringData(10);

	public LongData Sr606screensflWritten = new LongData(0);
	public LongData Sr606screenctlWritten = new LongData(0);
	public LongData Sr606screenWritten = new LongData(0);
	public LongData Sr606protectWritten = new LongData(0);
	public GeneralTable sr606screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr606screensfl;
	}

	public Sr606ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {rdocnum, trandate, chqnum, zdesc, acctamt};
		screenSflOutFields = new BaseData[][] {rdocnumOut, trandateOut, chqnumOut, zdescOut, acctamtOut};
		screenSflErrFields = new BaseData[] {rdocnumErr, trandateErr, chqnumErr, zdescErr, acctamtErr};
		screenSflDateFields = new BaseData[] {trandate};
		screenSflDateErrFields = new BaseData[] {trandateErr};
		screenSflDateDispFields = new BaseData[] {trandateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, contdesc, cownnum, cownname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, contdescOut, cownnumOut, cownnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, contdescErr, cownnumErr, cownnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr606screen.class;
		screenSflRecord = Sr606screensfl.class;
		screenCtlRecord = Sr606screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr606protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr606screenctl.lrec.pageSubfile);
	}
}
