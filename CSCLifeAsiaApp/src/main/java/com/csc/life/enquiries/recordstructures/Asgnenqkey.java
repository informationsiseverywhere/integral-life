package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:18
 * Description:
 * Copybook name: ASGNENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Asgnenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData asgnenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData asgnenqKey = new FixedLengthStringData(256).isAPartOf(asgnenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData asgnenqChdrcoy = new FixedLengthStringData(1).isAPartOf(asgnenqKey, 0);
  	public FixedLengthStringData asgnenqChdrnum = new FixedLengthStringData(8).isAPartOf(asgnenqKey, 1);
  	public PackedDecimalData asgnenqSeqno = new PackedDecimalData(2, 0).isAPartOf(asgnenqKey, 9);
  	public FixedLengthStringData asgnenqAsgnnum = new FixedLengthStringData(8).isAPartOf(asgnenqKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(237).isAPartOf(asgnenqKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(asgnenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		asgnenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}