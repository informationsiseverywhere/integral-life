/*
 * File: Pr60u.java
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.SchmpfDAO;
import com.csc.fsu.general.dataaccess.model.Schmpf;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.enquiries.screens.Sr60uScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Itmmsmtkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.DateUtils;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr60u extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pr60u");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(13);
	private FixedLengthStringData wsaaKeyTabl = new FixedLengthStringData(5).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaKeyItem = new FixedLengthStringData(8).isAPartOf(wsaaKey, 5);
	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Itmmsmtkey wskyItmmkey = new Itmmsmtkey();

	private static final String t5688 = "T5688";
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	
	ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	SchmpfDAO schmpfDAO = getApplicationContext().getBean("schmpfDAO", SchmpfDAO.class);
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	DescpfDAO descpfDAO =  DAOFactory.getDescpfDAO();
	ClntpfDAO clntpfDAO =  getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	Schmpf schmpf = new Schmpf();
	Zctxpf zctxpf = new Zctxpf();
	Chdrpf chdrpf = new Chdrpf();
	Descpf descpf = new Descpf();
	Clntpf clntpf = new Clntpf();
	List<Zctxpf> zctxpflist = new ArrayList<Zctxpf>();
	private Wsspsmart wsspsmart = new Wsspsmart();
	String startDate;//ILIFE-7916
	private Sr60uScreenVars sv = ScreenProgram.getScreenVars( Sr60uScreenVars.class);
	
	public Pr60u() {
		super();
		screenVars = sv;
		new ScreenModel("Sr60u", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
	
		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.datefrm.set(SPACES);
		sv.dateto.set(SPACES);
		sv.totamnt.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("Sr60u", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrpf= chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString());
		if(chdrpf != null)
		{
			sv.chdrnum.set(chdrpf.getChdrnum());
			sv.clntnum.set(chdrpf.getCownnum());
		}
		descpf= descpfDAO.getItemTblItemByLang("IT", t5688, wsspcomn.chdrCnttype.value(),wsspcomn.language.toString(),wsspcomn.company.toString());//ILIFE-6334
		if(descpf != null)
		{
			sv.ctypedes.set(descpf.getLongdesc());
		}
		else
		{
			sv.ctypedes.fill("?");
		}
		clntpf = clntpfDAO.findClientByClntnum(sv.clntnum.toString());
		if(clntpf == null || isNE(clntpf.getValidflag(),1))
		{
			sv.clntname.set(SPACES);	
		}
		else
		{
			plainname();
			sv.clntname.set(wsspcomn.longconfname);
		}
		if(chdrpf != null /*IJTI-320*/
				&& chdrpf.getSchmno()!=null && isNE(chdrpf.getSchmno(),SPACES)){
			schmpf = schmpfDAO.getSchmpfRecord(chdrpf.getSchmno());
			if(schmpf != null){
				sv.schmno.set(schmpf.getSchmNo());
				sv.schmnme.set(schmpf.getSchmNme());
			}
			else
			{
				sv.schmno.set(SPACES);
				sv.schmnme.set(SPACES);
			}
		}
		else
		{
			sv.schmno.set(SPACES);
			sv.schmnme.set(SPACES);
		}
		readSubfileRecords1100();
		
		
}
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void readSubfileRecords1100()
{
	sub1.set(0);
	zctxpflist = zctxpfDAO.getZctxpfRecord(chdrpf.getChdrnum());/* IJTI-1523 */
	while (isLT(sub1,zctxpflist.size())&& isLT(sub1, 15)){
		obtainZctxpxfDetails1200();
	}
}

protected void obtainZctxpxfDetails1200()
{
	if(zctxpflist != null){			
		sv.datefrm.set(zctxpflist.get(sub1.toInt()).getDatefrm());
		sv.dateto.set(zctxpflist.get(sub1.toInt()).getDateto());
		sv.totamnt.set(zctxpflist.get(sub1.toInt()).getAmount());
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("Sr60u", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	sub1.add(1);
}

protected void preScreenEdit()
{
		preStart();
}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		scrnparams.subfileRrn.set(1);
		if (isNE(wsspcomn.programPtr,ZERO)){
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}
protected void whereNext4000()
{
	nextProgram4100();
	srnch4110();
	
}
protected void nextProgram4100()
{
	wsspcomn.nextprog.set(wsaaProg);
	if (isEQ(wsaaFunctionKey, varcom.calc)) {
		return;
	}
}

protected void srnch4110()
{
	scrnparams.function.set(varcom.srnch);
	processScreen("Sr60u", sv);
	wsspcomn.confirmationKey.set(SPACES);
	wsaaKeyItem.set(SPACES);
	wskyItmmkey.itmmsmtItemitem.set(SPACES);
	wsspsmart.itemkey.set(SPACES);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
/*	if (isNE(wsspcomn.flag, "I")) {
		//sv.change.set("*");
	}
*/	if (isNE(scrnparams.statuz, varcom.endp)
	&& isEQ(sv.select, SPACES)) {
		srnch4110();
		return ;
	}
	if (isNE(scrnparams.statuz, varcom.endp)) {
		
		wsaaKeyItem.set(sv.datefrm);
		wsspcomn.confirmationKey.set(wsaaKey);
		wskyItmmkey.itmmsmtItemitem.set(sv.dateto);
		wsspsmart.itemkey.set(wskyItmmkey);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("Sr60u", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(wsaaProg);
		}
		
		else {
			wsspcomn.programPtr.add(1);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		}
	}
	/*ILIFE-7916 Start*/
	if(!StringUtils.isEmpty(sv.datefrmDisp.getFormData().trim()));{/* IJTI-1523 */
	startDate =  DateUtils.convertDateByFormat( new FixedLengthStringData(sv.datefrmDisp.getFormData()), "dd/mm/yyyy", "yyyymmdd").trim();/* IJTI-1523 */
	wsspcomn.currto.set(startDate);
	}
	/*ILIFE-7916 END*/
return;
}
protected void exit4120()
{
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
	/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/
}
protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("Sr60u", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
