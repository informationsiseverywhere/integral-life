package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6248
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6248ScreenVars extends SmartVarModel { 


	/*public FixedLengthStringData dataArea = new FixedLengthStringData(482);
	public FixedLengthStringData dataFields = new FixedLengthStringData(194).isAPartOf(dataArea, 0);*/
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,99);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,107);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData occups = new FixedLengthStringData(8).isAPartOf(dataFields, 156);
	public FixedLengthStringData[] occup = FLSArrayPartOfStructure(2, 4, occups, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(occups, 0, FILLER_REDEFINE);
	public FixedLengthStringData occup01 = DD.occup.copy().isAPartOf(filler,0);
	public FixedLengthStringData occup02 = DD.occup.copy().isAPartOf(filler,4);
	public FixedLengthStringData pursuits = new FixedLengthStringData(24).isAPartOf(dataFields, 164);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(4, 6, pursuits, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler1,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler1,6);
	public FixedLengthStringData pursuit03 = DD.pursuit.copy().isAPartOf(filler1,12);
	public FixedLengthStringData pursuit04 = DD.pursuit.copy().isAPartOf(filler1,18);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData smokings = new FixedLengthStringData(4).isAPartOf(dataFields, 190);
	public FixedLengthStringData[] smoking = FLSArrayPartOfStructure(2, 2, smokings, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(smokings, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01 = DD.smoking.copy().isAPartOf(filler2,0);
	public FixedLengthStringData smoking02 = DD.smoking.copy().isAPartOf(filler2,2);
	
	/*public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 194);*/
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
		public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData occupsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] occupErr = FLSArrayPartOfStructure(2, 4, occupsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(occupsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData occup01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData occup02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(4, 4, pursuitsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData pursuit03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData pursuit04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData smokingsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] smokingErr = FLSArrayPartOfStructure(2, 4, smokingsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(smokingsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData smoking02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	
/*	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 266);*/
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData occupsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] occupOut = FLSArrayPartOfStructure(2, 12, occupsOut, 0);
	public FixedLengthStringData[][] occupO = FLSDArrayPartOfArrayStructure(12, 1, occupOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(occupsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] occup01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] occup02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(4, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] pursuit03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] pursuit04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData smokingsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(2, 12, smokingsOut, 0);
	public FixedLengthStringData[][] smokingO = FLSDArrayPartOfArrayStructure(12, 1, smokingOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(smokingsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] smoking01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] smoking02Out = FLSArrayPartOfStructure(12, 1, filler8, 12); 

/*	public FixedLengthStringData subfileArea = new FixedLengthStringData(251);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(57).isAPartOf(subfileArea, 0);*/
	public FixedLengthStringData subfileArea = new FixedLengthStringData(getSubfileAreaSize());
	public FixedLengthStringData subfileFields = new FixedLengthStringData(getSubfileFieldsSize()).isAPartOf(subfileArea, 0);
	public ZonedDecimalData agerate = DD.agerate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData extCessTerm = DD.ecestrm.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public ZonedDecimalData insprm = initialize_insprm();
	public FixedLengthStringData opcda = DD.opcda.copy().isAPartOf(subfileFields,12);
	public ZonedDecimalData oppc = DD.oppc.copyToZonedDecimal().isAPartOf(subfileFields,14);
	public FixedLengthStringData reasind = DD.reasind.copy().isAPartOf(subfileFields,19);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData seqnbr = DD.seqnbr.copyToZonedDecimal().isAPartOf(subfileFields,21);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(subfileFields,24);
	public ZonedDecimalData zmortpct = DD.zmortpct.copyToZonedDecimal().isAPartOf(subfileFields,34);
	public ZonedDecimalData znadjperc = DD.znadjperc.copyToZonedDecimal().isAPartOf(subfileFields,37);
	/*BRD-306 START */
	public ZonedDecimalData premadj = DD. zpremiumAdjusted.copyToZonedDecimal().isAPartOf(subfileFields, 42);
	/*BRD-306 END */
/*	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 57);*/
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(getErrorSubfileSize()).isAPartOf(subfileArea, getSubfileFieldsSize());
	public FixedLengthStringData agerateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData ecestrmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData insprmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData opcdaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData oppcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData reasindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData seqnbrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData zmortpctErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData znadjpercErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	/*BRD-306 START */
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	/*BRD-306 END */
	//public FixedLengthStringData outputSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 105);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(getOutputSubfileSize()).isAPartOf(subfileArea,getErrorSubfileSize()+getSubfileFieldsSize());
	public FixedLengthStringData[] agerateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] ecestrmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] insprmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] opcdaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] oppcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] reasindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] seqnbrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] zmortpctOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] znadjpercOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	/*BRD-306 START */
	public FixedLengthStringData[] premadjOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	/*BRD-306 END */
	/*public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 249);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();*/
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,getSubfileFieldsSize()+getErrorSubfileSize()+getOutputSubfileSize());
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6248screensflWritten = new LongData(0);
	public LongData S6248screenctlWritten = new LongData(0);
	public LongData S6248screenWritten = new LongData(0);
	public LongData S6248protectWritten = new LongData(0);
	public GeneralTable s6248screensfl = new GeneralTable(AppVars.getInstance());


	public static int[] screenSflPfInds = new int[]{4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21};
	
	public static int[] affectedInds = new int[] {};
	
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6248screensfl;
	}

	public S6248ScreenVars() {
		super();
		initialiseScreenVars();
	}
	{
		screenIndicArea = DD.indicarea.copy();
	}

	protected ZonedDecimalData initialize_insprm(){
		return DD.insprm.copyToZonedDecimal().isAPartOf(subfileFields,6);
	}
	
	protected void initialiseScreenVars() {
	/*	screenSflFields = new BaseData[] {opcda, shortdesc, agerate, oppc, insprm, extCessTerm, select, seqnbr, reasind, znadjperc, zmortpct,premadj};
		screenSflOutFields = new BaseData[][] {opcdaOut, shortdescOut, agerateOut, oppcOut, insprmOut, ecestrmOut, selectOut, seqnbrOut, reasindOut, znadjpercOut, zmortpctOut,premadjOut};
		screenSflErrFields = new BaseData[] {opcdaErr, shortdescErr, agerateErr, oppcErr, insprmErr, ecestrmErr, selectErr, seqnbrErr, reasindErr, znadjpercErr, zmortpctErr,premadjErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, life, coverage, rider, lifcnum, linsname, smoking01, occup01, pursuit01, pursuit02, jlifcnum, jlinsname, smoking02, occup02, pursuit03, pursuit04, crtable, crtabdesc};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, smoking01Out, occup01Out, pursuit01Out, pursuit02Out, jlifcnumOut, jlinsnameOut, smoking02Out, occup02Out, pursuit03Out, pursuit04Out, crtableOut, crtabdescOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, smoking01Err, occup01Err, pursuit01Err, pursuit02Err, jlifcnumErr, jlinsnameErr, smoking02Err, occup02Err, pursuit03Err, pursuit04Err, crtableErr, crtabdescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};*/
		

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		screenSflFields = getscreenLSflFields();
		screenSflOutFields = getscreenSflOutFields();
		screenSflErrFields = getscreenSflErrFields();
		
		
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6248screen.class;
		screenSflRecord = S6248screensfl.class;
		screenCtlRecord = S6248screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6248protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6248screenctl.lrec.pageSubfile);
	}
	
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	public static int[] getScreenSflAffectedInds()
	{
		return affectedInds;
	}
	
	
	public int getDataAreaSize() {
		return 482;
	}
	

	public int getDataFieldsSize(){
		return 194;  
	}
	public int getErrorIndicatorSize(){
		return 72; 
	}
	
	public int getOutputFieldSize(){
		return 216; 
	}
	public int getSubfileAreaSize()
	{
		return 251;
	}
	
	public int getSubfileFieldsSize()
	{
		return 57;
	}

	public int getErrorSubfileSize()
	{
		return 48;
	}

	public int getOutputSubfileSize()
	{
		return 144;
	}
	

	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, life, coverage, rider, lifcnum, linsname, smoking01, occup01, pursuit01, pursuit02, jlifcnum, jlinsname, smoking02, occup02, pursuit03, pursuit04, crtable, crtabdesc};   //IFSU-2036
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, smoking01Out, occup01Out, pursuit01Out, pursuit02Out, jlifcnumOut, jlinsnameOut, smoking02Out, occup02Out, pursuit03Out, pursuit04Out, crtableOut, crtabdescOut}; //IFSU-2036
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, smoking01Err, occup01Err, pursuit01Err, pursuit02Err, jlifcnumErr, jlinsnameErr, smoking02Err, occup02Err, pursuit03Err, pursuit04Err, crtableErr, crtabdescErr};
	}
	
	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {opcda, shortdesc, agerate, oppc, insprm, extCessTerm, select, seqnbr, reasind, znadjperc, zmortpct,premadj};
	}
	
	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {opcdaOut, shortdescOut, agerateOut, oppcOut, insprmOut, ecestrmOut, selectOut, seqnbrOut, reasindOut, znadjpercOut, zmortpctOut,premadjOut};
	}
	
	
	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {opcdaErr, shortdescErr, agerateErr, oppcErr, insprmErr, ecestrmErr, selectErr, seqnbrErr, reasindErr, znadjpercErr, zmortpctErr,premadjErr};
	}
	
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {};
	}
	
	
	
}
