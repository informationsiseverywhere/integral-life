package com.csc.life.enquiries.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UndrprpTableDAM.java
 * Date: Sun, 30 Aug 2009 03:51:13
 * Class transformed from UNDRPRP.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UndrprpTableDAM extends UndrpfTableDAM {

	public UndrprpTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UNDRPRP");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CLNTNUM"
		             + ", COY"
		             + ", CURRCODE"
		             + ", LRKCLS02"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CLNTNUM, " +
		            "COY, " +
		            "LRKCLS01, " +
		            "LRKCLS02, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "CNTTYP, " +
		            "CRTABLE, " +
		            "CURRCODE, " +
		            "SUMINS, " +
		            "EFFDATE, " +
		            "ADSC, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CLNTNUM ASC, " +
		            "COY ASC, " +
		            "CURRCODE ASC, " +
		            "LRKCLS02 ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CLNTNUM DESC, " +
		            "COY DESC, " +
		            "CURRCODE DESC, " +
		            "LRKCLS02 DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               clntnum,
                               coy,
                               lrkcls01,
                               lrkcls02,
                               chdrnum,
                               life,
                               cnttyp,
                               crtable,
                               currcode,
                               sumins,
                               effdate,
                               adsc,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(40);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getClntnum().toInternal()
					+ getCoy().toInternal()
					+ getCurrcode().toInternal()
					+ getLrkcls02().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, coy);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, lrkcls02);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(clntnum.toInternal());
	nonKeyFiller20.setInternal(coy.toInternal());
	nonKeyFiller40.setInternal(lrkcls02.toInternal());
	nonKeyFiller50.setInternal(chdrnum.toInternal());
	nonKeyFiller90.setInternal(currcode.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(147);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getLrkcls01().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getLife().toInternal()
					+ getCnttyp().toInternal()
					+ getCrtable().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getSumins().toInternal()
					+ getEffdate().toInternal()
					+ getAdsc().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, lrkcls01);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, cnttyp);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, adsc);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}
	public FixedLengthStringData getCoy() {
		return coy;
	}
	public void setCoy(Object what) {
		coy.set(what);
	}
	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}
	public FixedLengthStringData getLrkcls02() {
		return lrkcls02;
	}
	public void setLrkcls02(Object what) {
		lrkcls02.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLrkcls01() {
		return lrkcls01;
	}
	public void setLrkcls01(Object what) {
		lrkcls01.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCnttyp() {
		return cnttyp;
	}
	public void setCnttyp(Object what) {
		cnttyp.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getAdsc() {
		return adsc;
	}
	public void setAdsc(Object what) {
		adsc.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getLrkclss() {
		return new FixedLengthStringData(lrkcls01.toInternal()
										+ lrkcls02.toInternal());
	}
	public void setLrkclss(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getLrkclss().getLength()).init(obj);
	
		what = ExternalData.chop(what, lrkcls01);
		what = ExternalData.chop(what, lrkcls02);
	}
	public FixedLengthStringData getLrkcls(BaseData indx) {
		return getLrkcls(indx.toInt());
	}
	public FixedLengthStringData getLrkcls(int indx) {

		switch (indx) {
			case 1 : return lrkcls01;
			case 2 : return lrkcls02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setLrkcls(BaseData indx, Object what) {
		setLrkcls(indx.toInt(), what);
	}
	public void setLrkcls(int indx, Object what) {

		switch (indx) {
			case 1 : setLrkcls01(what);
					 break;
			case 2 : setLrkcls02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		clntnum.clear();
		coy.clear();
		currcode.clear();
		lrkcls02.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		lrkcls01.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		life.clear();
		cnttyp.clear();
		crtable.clear();
		nonKeyFiller90.clear();
		sumins.clear();
		effdate.clear();
		adsc.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}