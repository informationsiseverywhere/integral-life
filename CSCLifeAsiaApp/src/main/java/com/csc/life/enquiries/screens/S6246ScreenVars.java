package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6246
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6246ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1720);
	public FixedLengthStringData dataFields = new FixedLengthStringData(696).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData currcys = new FixedLengthStringData(30).isAPartOf(dataFields, 47);
	public FixedLengthStringData[] currcy = FLSArrayPartOfStructure(10, 3, currcys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(currcys, 0, FILLER_REDEFINE);
	public FixedLengthStringData currcy01 = DD.currcy.copy().isAPartOf(filler,0);
	public FixedLengthStringData currcy02 = DD.currcy.copy().isAPartOf(filler,3);
	public FixedLengthStringData currcy03 = DD.currcy.copy().isAPartOf(filler,6);
	public FixedLengthStringData currcy04 = DD.currcy.copy().isAPartOf(filler,9);
	public FixedLengthStringData currcy05 = DD.currcy.copy().isAPartOf(filler,12);
	public FixedLengthStringData currcy06 = DD.currcy.copy().isAPartOf(filler,15);
	public FixedLengthStringData currcy07 = DD.currcy.copy().isAPartOf(filler,18);
	public FixedLengthStringData currcy08 = DD.currcy.copy().isAPartOf(filler,21);
	public FixedLengthStringData currcy09 = DD.currcy.copy().isAPartOf(filler,24);
	public FixedLengthStringData currcy10 = DD.currcy.copy().isAPartOf(filler,27);
	public FixedLengthStringData virtFundSplitMethod = DD.fndspl.copy().isAPartOf(dataFields,77);
	public ZonedDecimalData instprem = DD.instprem.copyToZonedDecimal().isAPartOf(dataFields,81);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,163);
	public ZonedDecimalData numapp = DD.numapp.copyToZonedDecimal().isAPartOf(dataFields,210);
	public FixedLengthStringData percOrAmntInd = DD.prcamtind.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData statfund = DD.statfund.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,218);
	public FixedLengthStringData stsubsect = DD.stsubsect.copy().isAPartOf(dataFields,220);
	
	
	public FixedLengthStringData zafropt1 = DD.zafropt1.copy().isAPartOf(dataFields,224);//4
	
	public FixedLengthStringData zafritem = DD.zafritem.copy().isAPartOf(dataFields,228);//2
	public FixedLengthStringData unitAllocPercAmts = new FixedLengthStringData(170).isAPartOf(dataFields, 230);
	public ZonedDecimalData[] unitAllocPercAmt = ZDArrayPartOfStructure(10, 17, 2, unitAllocPercAmts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(170).isAPartOf(unitAllocPercAmts, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitAllocPercAmt01 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData unitAllocPercAmt02 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData unitAllocPercAmt03 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData unitAllocPercAmt04 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData unitAllocPercAmt05 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData unitAllocPercAmt06 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData unitAllocPercAmt07 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData unitAllocPercAmt08 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData unitAllocPercAmt09 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,136);
	public ZonedDecimalData unitAllocPercAmt10 = DD.ualprc.copyToZonedDecimal().isAPartOf(filler1,153);
	public FixedLengthStringData unitBidPrices = new FixedLengthStringData(90).isAPartOf(dataFields, 394);
	public ZonedDecimalData[] unitBidPrice = ZDArrayPartOfStructure(10, 9, 5, unitBidPrices, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(90).isAPartOf(unitBidPrices, 0, FILLER_REDEFINE);
	public ZonedDecimalData unitBidPrice01 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData unitBidPrice02 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,9);
	public ZonedDecimalData unitBidPrice03 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,18);
	public ZonedDecimalData unitBidPrice04 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,27);
	public ZonedDecimalData unitBidPrice05 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,36);
	public ZonedDecimalData unitBidPrice06 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,45);
	public ZonedDecimalData unitBidPrice07 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,54);
	public ZonedDecimalData unitBidPrice08 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,63);
	public ZonedDecimalData unitBidPrice09 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,72);
	public ZonedDecimalData unitBidPrice10 = DD.ubidpr.copyToZonedDecimal().isAPartOf(filler2,81);
	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(40).isAPartOf(dataFields, 484);
	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(10, 4, unitVirtualFunds, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
	public FixedLengthStringData unitVirtualFund01 = DD.vrtfnd.copy().isAPartOf(filler3,0);
	public FixedLengthStringData unitVirtualFund02 = DD.vrtfnd.copy().isAPartOf(filler3,4);
	public FixedLengthStringData unitVirtualFund03 = DD.vrtfnd.copy().isAPartOf(filler3,8);
	public FixedLengthStringData unitVirtualFund04 = DD.vrtfnd.copy().isAPartOf(filler3,12);
	public FixedLengthStringData unitVirtualFund05 = DD.vrtfnd.copy().isAPartOf(filler3,16);
	public FixedLengthStringData unitVirtualFund06 = DD.vrtfnd.copy().isAPartOf(filler3,20);
	public FixedLengthStringData unitVirtualFund07 = DD.vrtfnd.copy().isAPartOf(filler3,24);
	public FixedLengthStringData unitVirtualFund08 = DD.vrtfnd.copy().isAPartOf(filler3,28);
	public FixedLengthStringData unitVirtualFund09 = DD.vrtfnd.copy().isAPartOf(filler3,32);
	public FixedLengthStringData unitVirtualFund10 = DD.vrtfnd.copy().isAPartOf(filler3,36);
	public FixedLengthStringData winfndopt = DD.winfndopt.copy().isAPartOf(dataFields,524);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,644);
	/* ILIFE-4036 started*/
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 663);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 666);
	/* ILIFE-4036 ended*/
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(256).isAPartOf(dataArea, 696);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcysErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] currcyErr = FLSArrayPartOfStructure(10, 4, currcysErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(currcysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData currcy01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData currcy02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData currcy03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData currcy04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData currcy05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData currcy06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData currcy07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData currcy08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData currcy09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData currcy10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData fndsplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData instpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData numappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData prcamtindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData statfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stsubsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData ualprcsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData[] ualprcErr = FLSArrayPartOfStructure(10, 4, ualprcsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(ualprcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ualprc01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData ualprc02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData ualprc03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData ualprc04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData ualprc05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData ualprc06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData ualprc07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData ualprc08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData ualprc09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData ualprc10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData ubidprsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData[] ubidprErr = FLSArrayPartOfStructure(10, 4, ubidprsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(ubidprsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ubidpr01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData ubidpr02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData ubidpr03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData ubidpr04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData ubidpr05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData ubidpr06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData ubidpr07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData ubidpr08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData ubidpr09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData ubidpr10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData vrtfndsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData[] vrtfndErr = FLSArrayPartOfStructure(10, 4, vrtfndsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(vrtfndsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData vrtfnd01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData vrtfnd02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData vrtfnd03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData vrtfnd04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData vrtfnd05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData vrtfnd06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData vrtfnd07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData vrtfnd08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData vrtfnd09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData vrtfnd10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData winfndoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData zafropt1Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData zafritemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	/* ILIFE-4036 started*/
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);
	/* ILIFE-4036 ended*/
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(768).isAPartOf(dataArea, 952);
	
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData currcysOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] currcyOut = FLSArrayPartOfStructure(10, 12, currcysOut, 0);
	public FixedLengthStringData[][] currcyO = FLSDArrayPartOfArrayStructure(12, 1, currcyOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(currcysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] currcy01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] currcy02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] currcy03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] currcy04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] currcy05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] currcy06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] currcy07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] currcy08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] currcy09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] currcy10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] fndsplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] numappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] prcamtindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] statfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stsubsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData ualprcsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 336);
	public FixedLengthStringData[] ualprcOut = FLSArrayPartOfStructure(10, 12, ualprcsOut, 0);
	public FixedLengthStringData[][] ualprcO = FLSDArrayPartOfArrayStructure(12, 1, ualprcOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(120).isAPartOf(ualprcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ualprc01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] ualprc02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] ualprc03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] ualprc04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] ualprc05Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData[] ualprc06Out = FLSArrayPartOfStructure(12, 1, filler9, 60);
	public FixedLengthStringData[] ualprc07Out = FLSArrayPartOfStructure(12, 1, filler9, 72);
	public FixedLengthStringData[] ualprc08Out = FLSArrayPartOfStructure(12, 1, filler9, 84);
	public FixedLengthStringData[] ualprc09Out = FLSArrayPartOfStructure(12, 1, filler9, 96);
	public FixedLengthStringData[] ualprc10Out = FLSArrayPartOfStructure(12, 1, filler9, 108);
	public FixedLengthStringData ubidprsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 456);
	public FixedLengthStringData[] ubidprOut = FLSArrayPartOfStructure(10, 12, ubidprsOut, 0);
	public FixedLengthStringData[][] ubidprO = FLSDArrayPartOfArrayStructure(12, 1, ubidprOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(ubidprsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ubidpr01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] ubidpr02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] ubidpr03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] ubidpr04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] ubidpr05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] ubidpr06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] ubidpr07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] ubidpr08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] ubidpr09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] ubidpr10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData vrtfndsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 576);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(10, 12, vrtfndsOut, 0);
	public FixedLengthStringData[][] vrtfndO = FLSDArrayPartOfArrayStructure(12, 1, vrtfndOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(vrtfndsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] vrtfnd01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] vrtfnd02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] vrtfnd03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] vrtfnd04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] vrtfnd05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] vrtfnd06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] vrtfnd07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] vrtfnd08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] vrtfnd09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] vrtfnd10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public FixedLengthStringData[] winfndoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	
	public FixedLengthStringData[] zafropt1Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);//12
	public FixedLengthStringData[] zafritemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);//12
	/* ILIFE-4036 started*/
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);
	/* ILIFE-4036 started*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6246screenWritten = new LongData(0);
	public LongData S6246protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6246ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {"44",null, "44",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {winfndopt, chdrnum, life, coverage, rider, lifenum, linsname, crtable, crtabdesc, zagelit, anbAtCcd, statfund, statSect, stsubsect, jlifcnum, jlinsname, instprem, virtFundSplitMethod, percOrAmntInd, unitVirtualFund01, currcy01, unitAllocPercAmt01, unitBidPrice01, unitVirtualFund02, currcy02, unitAllocPercAmt02, unitBidPrice02, unitVirtualFund03, currcy03, unitAllocPercAmt03, unitBidPrice03, unitVirtualFund04, currcy04, unitAllocPercAmt04, unitBidPrice04, unitVirtualFund05, currcy05, unitAllocPercAmt05, unitBidPrice05, unitVirtualFund06, currcy06, unitAllocPercAmt06, unitBidPrice06, unitVirtualFund07, currcy07, unitAllocPercAmt07, unitBidPrice07, unitVirtualFund08, currcy08, unitAllocPercAmt08, unitBidPrice08, unitVirtualFund09, currcy09, unitAllocPercAmt09, unitBidPrice09, unitVirtualFund10, currcy10, unitAllocPercAmt10, unitBidPrice10, numapp,zafropt1,zafritem, cnttype, ctypedes};
		screenOutFields = new BaseData[][] {winfndoptOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, linsnameOut, crtableOut, crtabdescOut, zagelitOut, anbccdOut, statfundOut, stsectOut, stsubsectOut, jlifcnumOut, jlinsnameOut, instpremOut, fndsplOut, prcamtindOut, vrtfnd01Out, currcy01Out, ualprc01Out, ubidpr01Out, vrtfnd02Out, currcy02Out, ualprc02Out, ubidpr02Out, vrtfnd03Out, currcy03Out, ualprc03Out, ubidpr03Out, vrtfnd04Out, currcy04Out, ualprc04Out, ubidpr04Out, vrtfnd05Out, currcy05Out, ualprc05Out, ubidpr05Out, vrtfnd06Out, currcy06Out, ualprc06Out, ubidpr06Out, vrtfnd07Out, currcy07Out, ualprc07Out, ubidpr07Out, vrtfnd08Out, currcy08Out, ualprc08Out, ubidpr08Out, vrtfnd09Out, currcy09Out, ualprc09Out, ubidpr09Out, vrtfnd10Out, currcy10Out, ualprc10Out, ubidpr10Out, numappOut,zafritemOut,zafropt1Out, cnttypeOut, ctypedesOut};
		screenErrFields = new BaseData[] {winfndoptErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, linsnameErr, crtableErr, crtabdescErr, zagelitErr, anbccdErr, statfundErr, stsectErr, stsubsectErr, jlifcnumErr, jlinsnameErr, instpremErr, fndsplErr, prcamtindErr, vrtfnd01Err, currcy01Err, ualprc01Err, ubidpr01Err, vrtfnd02Err, currcy02Err, ualprc02Err, ubidpr02Err, vrtfnd03Err, currcy03Err, ualprc03Err, ubidpr03Err, vrtfnd04Err, currcy04Err, ualprc04Err, ubidpr04Err, vrtfnd05Err, currcy05Err, ualprc05Err, ubidpr05Err, vrtfnd06Err, currcy06Err, ualprc06Err, ubidpr06Err, vrtfnd07Err, currcy07Err, ualprc07Err, ubidpr07Err, vrtfnd08Err, currcy08Err, ualprc08Err, ubidpr08Err, vrtfnd09Err, currcy09Err, ualprc09Err, ubidpr09Err, vrtfnd10Err, currcy10Err, ualprc10Err, ubidpr10Err, numappErr,zafropt1Err,zafritemErr, cnttypeErr, ctypedesErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6246screen.class;
		protectRecord = S6246protect.class;
	}

}
