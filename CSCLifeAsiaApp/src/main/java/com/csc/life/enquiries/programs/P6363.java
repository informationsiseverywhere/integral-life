/*
 * File: P6363.java
 * Date: 30 August 2009 0:45:26
 * Author: Quipoz Limited
 * 
 * Class transformed from P6363.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Clntqy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsu.general.recordstructures.Srvunitcpy;
import com.csc.life.enquiries.screens.S6363ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ClntqyDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* This is the Sub-menu for the Contract Enquiry suite.
*
* Initialise
* ----------
*
*     Initialise the screen for display.
*
*     If returning  from  a  client  scroll enquiry and OPNQRYF ha 
*     already been used  then  call  QCMDEXC  to  issue  a close o 
*     CLRRPF.
*
*
* Validation
* ----------
*
*                    s selected:
*
*          If  Key1  must  be  entered  check  for a valid Contrac 
*          Number by performing a READS on CHDRENQ.
*
*          If a contract  is  found  check that the Validflag has  
*          value of '1'. If not report an error.
*
*          If a valid  contract header record is found then perfor 
*          a KEEPS on the  record  in  order to store it for use b 
*          the transaction enquiry programs.
*
*     If option 'B' is selected:
*
*          If a client  number  has  been  entered and KEY2 must b 
*          present then validate  it  against CLTS. If no record i 
*          found report an error.
*
*          If a valid  client  record has been found then perform  
*          KEEPS on it for use by the enquiry programs.
*
*          If  no client  number  was  entered  then  validate  th 
*          filters. Only the  Surname  field must be entered. Chec 
*          that it is non blank.
*
* Updating.
* ---------
*
*     If the action field  was 'A' or the client number was entere 
*     do not carry out the following processing.
*
*     QCMDEXC is  used  to  perform  an  OPNQRYF against the Clien 
*     Roles file (CLRRPF), and the Client Details file (CLNTPF) an 
*     to construct  a  temporary access path that allows the syste 
*     to read only those client records that match the input filte 
*     values.
*
*     The OPENQRYF command must be constructed piece by piece so a 
*     to include  only  those  filters  that are present. Allowanc 
*     must be  made  when  handling  the  Surname,  Given  Name an 
*     Postcode fields for the inclusion of an asterisk somewhere i 
*     the character  string.  If  any  are  found  then the generi 
*     wildcard search construct must be used.
*
*     The search should be over a joined file - CLRRPF and CLNTPF  
*     and use a  new  format  of  CLNTQY  which includes only thos 
*     fields necessary for this enquiry.
*
*     The keyfields will be Surname, Given Name, Postcode and Role 
*     The Join fields will be CLNTCOY and CLNTNUM.
*
*
* Next Program.
* -------------
*
*     Set WSSP-FLAG to 'I'.
*
*     Load the  first  four  occurrences  of WSSP-SEC-PROG with th 
*     program names from the submenu switching.
*
*     For  option 'A', if a valid contract number was entered add  
*     to  the  program  pointer  and  exit,  otherwise add 1 to th 
*     program pointer and exit.
*
*     For option 'B'  if a valid client number was entered add 2 t 
*     the program pointer  and exit, otherwise add 1 to the progra 
*     pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P6363 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6363");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaOvrdbfClntqy = new FixedLengthStringData(46);
	private FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(wsaaOvrdbfClntqy, 0, FILLER).init("OVRDBF FILE(CLNTQY) TOFILE(CLRRPF) SHARE(*YES)");

	private FixedLengthStringData wsaaClofClrrpf = new FixedLengthStringData(18);
	private FixedLengthStringData filler3 = new FixedLengthStringData(18).isAPartOf(wsaaClofClrrpf, 0, FILLER).init("CLOF OPNID(CLRRPF)");
	private FixedLengthStringData wsaaQcmdexcCommand = new FixedLengthStringData(1000);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).setUnsigned();

	private FixedLengthStringData wsaaOpnqryfUsed = new FixedLengthStringData(1);
	private Validator opnqryfUsed = new Validator(wsaaOpnqryfUsed, "Y");
	private Validator opnqryfNotUsed = new Validator(wsaaOpnqryfUsed, "N");
	private ZonedDecimalData wsaaStringPointer = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAsteriskCount1 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAsteriskCount2 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAsteriskCount3 = new ZonedDecimalData(3, 0).setUnsigned();
		/* ERRORS */
	private static final String e058 = "E058";
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e091 = "E091";
	protected static final String e704 = "E704";
	protected static final String f311 = "F311";
	protected static final String f917 = "F917";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String cltsrec = "CLTSREC";

	private FixedLengthStringData wsaaClntdet = new FixedLengthStringData(41);
	private FixedLengthStringData wsaaGivname = new FixedLengthStringData(20).isAPartOf(wsaaClntdet, 0);
	private FixedLengthStringData wsaaRole = new FixedLengthStringData(2).isAPartOf(wsaaClntdet, 20);
	private ZonedDecimalData wsaaCltdob = new ZonedDecimalData(8, 0).isAPartOf(wsaaClntdet, 22).setUnsigned();
	private FixedLengthStringData wsaaCltsex = new FixedLengthStringData(1).isAPartOf(wsaaClntdet, 30);
	private FixedLengthStringData wsaaCltpcode = new FixedLengthStringData(10).isAPartOf(wsaaClntdet, 31);
	private Clntkey wsaaClntkey = new Clntkey();
	private Batckey wsaaBatchkey = new Batckey();
	protected Srvunitcpy srvunitcpy = new Srvunitcpy();
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	protected Sdasancrec sdasancrec = new Sdasancrec();
	protected Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	/* MLI changes */
	/*
	 * private S6363ScreenVars sv = ScreenProgram.getScreenVars(
	 * S6363ScreenVars.class);
	 */

	private S6363ScreenVars sv = getLScreenVars();

	protected S6363ScreenVars getLScreenVars() {

		return ScreenProgram.getScreenVars(S6363ScreenVars.class);

	}
	
   private ClntqyDAO clntqyDAO = DAOFactory.getClntqyDAO();
   protected Chdrpf chdrpf = new Chdrpf();
   protected ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
   private Clntpf clntpf= new Clntpf();
   private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
   //private List<Clntpf> clntpdList = null;
   protected Clntpf clntpfModel=null;
	
/**
 * Contains all possible labels used by goTo action.
 */
   /**
    *Access modifier changes For MLI code extension By npatel206
    */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		validateActionB2220, 
		validateFilters2230, 
		exit2290, 
		exit12090, 
		batching3080, 
		exit3090
	}

	public P6363() {
		super();
		screenVars = sv;
		new ScreenModel("S6363", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}
@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
@Override
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
@Override
public void processBo(Object... parmArray) {
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0); 

	try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
	}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
@Override
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.cltdobDisp.set(SPACES);//ILIFE-7775
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		sv.cltdob.set(varcom.vrcmMaxDate);
		
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

@Override
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					checkSanctions2120();
				case checkForErrors2080: 
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6363IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6363-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(Varcom.oK);
		/*VALIDATE*/
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,Varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2200();
					validateActionA2210();
				case validateActionB2220: 
					validateActionB2220();
				case validateFilters2230: 
					validateFilters2230();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2200()
	{
		if (isEQ(sv.action,"B")) {
			goTo(GotoLabel.validateActionB2220);
		}
	}

protected void validateActionA2210()
	{
		wsspcomn.chdrkey.set(SPACES);
		// Ticket ILIFE-4481 the contract number is space
		if ("".equals(sv.chdrsel.toString().trim())) {
			sv.chdrselErr.set(f917);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
		// Ticket ILIFE-4481 end

		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
		if(chdrpf==null){
			sv.chdrselErr.set(f917);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
		if (isNE(chdrpf.getValidflag(),"1")) {
            sv.chdrselErr.set(e704);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
		srvunitcpy.servunit.set(chdrpf.getServunit());
		if (!srvunitcpy.life.isTrue()) {
			sv.chdrselErr.set(f311);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
		/* When enquiring on a contract, ensure that a user with        */
		/* restricted data access is authorised to view that contract.  */
		initialize(sdasancrec.sancRec);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		checkRuser2400();
		if (isNE(sdasancrec.statuz,varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
		/*   Store the contract header for use by the transaction programs*/
		chdrpfDAO.setCacheObject(chdrpf);
		goTo(GotoLabel.exit2290);
	}

protected void validateActionB2220()
	{
		/*    IF   S6363-CLNTNUM           = SPACES                        */
		if (isEQ(sv.clttwo,SPACES)) {
			goTo(GotoLabel.validateFilters2230);
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.clttwo.toString());
		clntpf.setValidflag("1");
		List<Clntpf> clntpdList = clntpfDAO.readClientpfData(clntpf);
		if(clntpdList==null || clntpdList.isEmpty()){
			// MOVE E058              TO S6363-CLNTNUM-ERR             
				sv.clttwoErr.set(e058);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2290);
		} else {//IJTI-320 START
			clntpfModel= clntpdList.get(0);
			/* If enquiring on a client, ensure that a user with restricted */
			/* data access is authorised to view this client.               */
			sdasancrec.entypfx.set(fsupfxcpy.clnt);
			sdasancrec.entycoy.set(wsspcomn.fsuco);
			sdasancrec.entynum.set(sv.clttwo);
			checkRuser2400();
			if (isNE(sdasancrec.statuz,varcom.oK)) {
				sv.clttwoErr.set(sdasancrec.statuz);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2290);
			}
			/*    If control has reached here a valid client record has been*/
			/*    found for enquiry. Store the record for the enquiry program.*/
			clntpfDAO.setCacheObject(clntpfModel);
			goTo(GotoLabel.exit2290);	
		}
		//IJTI-320 END
	}

protected void validateFilters2230()
	{
		if (isEQ(sv.surname,SPACES)) {
			sv.surnameErr.set(e091);
			wsspcomn.edterror.set("Y");
			return ;
		}
	}

protected void checkRuser2400()
	{
		/*BEGIN*/
		sdasancrec.function.set("VENTY");
		sdasancrec.statuz.set(varcom.oK);
		sdasancrec.userid.set(wsspcomn.userid);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isEQ(sdasancrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sdasancrec.statuz);
			syserrrec.params.set(sdasancrec.sancRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2990-EXIT.                                         */
		goTo(GotoLabel.exit12090);
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
@Override
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3000();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3000()
	{
		/*  Update WSSP Key details*/
		wsspcomn.flag.set("I");
		wsspcomn.sbmaction.set(scrnparams.action);
		wsspcomn.lastActn.set(sv.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isNE(sv.chdrsel,SPACES)) {
			wsspcomn.chdrChdrcoy.set(chdrpf.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
		}
		else {
			wsspcomn.chdrChdrnum.set(SPACES);
		}
		wssplife.life.set("01");
		wssplife.jlife.set("00");
		wssplife.coverage.set("00");
		wssplife.rider.set("00");
		/* If the client is entered pass to WSSP for client enquiry        */
		/*  as FAS programs do not do retrieves, yet!                      */
		if (isNE(sv.clttwo,SPACES)) {
			wsaaClntkey.clntClntpfx.set("CN");
			wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
			wsaaClntkey.clntClntnum.set(sv.clttwo);
			wsspcomn.clntkey.set(wsaaClntkey);
		}
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		goToBatching3080();
        
        /* New hibernate code
         * 1. Delete all data in table CLNTQY
         * 2. Build query to retrieve data and insert to table CLNTQY
         */
        
        // Delete old data in CLNTQY table.
        clntqyDAO.deleteAll();
        
        // Build query
        String clntcoy = delimitedExp(wsspcomn.fsuco, SPACES);
        String surname = delimitedExp(sv.surname, SPACES).replace('*', '%');
        String givname = delimitedExp(sv.givname, SPACES).replace('*', '%');
        String role = delimitedExp(sv.role, SPACES);
        //ILIFE-8115 start by dpuhawan
        //Date cltdob = ConversionUtil.toDate(sv.cltdob);
        
        int cltdob = 0;
        if (!"".equals(sv.cltdobDisp.toString().trim())) {
	        String dateInString = sv.cltdobDisp.toString();
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConfig.getDateFormat());
	        LocalDate dateTime = LocalDate.parse(dateInString, formatter);
			dateInString = dateTime.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
			cltdob = Integer.valueOf(dateInString);
        }else 
        	cltdob = Integer.valueOf(varcom.vrcmMaxDate.toString());
		
        //Date vrcmMaxDate = ConversionUtil.toDate(varcom.vrcmMaxDate);
		int vrcmMaxDate = Integer.valueOf(varcom.vrcmMaxDate.toString());
		//ILIFE-8115 end
        
        String cltsex = delimitedExp(sv.cltsex, SPACES);
        String cltpcode = delimitedExp(sv.cltpcode, SPACES).replace('*', '%');
        
      //ILIFE-8115 start by dpuhawan
        int intRowsAffected = clntqyDAO.findByCriteriaAndInsert(clntcoy, surname, givname, role, cltdob, vrcmMaxDate, cltsex,  cltpcode);
        if (intRowsAffected == 1) {
        	Clntqy clntqy = clntqyDAO.loadClnt();
        	sv.clttwo.set(clntqy.getClntnum());
        	
    		clntpf.setClntpfx("CN");
    		clntpf.setClntcoy(wsspcomn.fsuco.toString());
    		clntpf.setClntnum(sv.clttwo.toString());
    		clntpf.setValidflag("1");
    		List<Clntpf> clntpdList = clntpfDAO.readClientpfData(clntpf);
    		if(clntpdList != null && !clntpdList.isEmpty()) {
    			clntpfModel= clntpdList.get(0);
    			
    			sdasancrec.entypfx.set(fsupfxcpy.clnt);
    			sdasancrec.entycoy.set(wsspcomn.fsuco);
    			sdasancrec.entynum.set(sv.clttwo);
    			checkRuser2400();
    			if (isNE(sdasancrec.statuz,varcom.oK)) {
    				sv.clttwoErr.set(sdasancrec.statuz);
    				wsspcomn.edterror.set("Y");
    				goTo(GotoLabel.exit2290);
    			}
    			/*    If control has reached here a valid client record has been*/
    			/*    found for enquiry. Store the record for the enquiry program.*/
    			clntpfDAO.setCacheObject(clntpfModel);
    			wsaaClntkey.clntClntpfx.set("CN");
    			wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
    			wsaaClntkey.clntClntnum.set(sv.clttwo);
    			wsspcomn.clntkey.set(wsaaClntkey);
    		}
        }
      //ILIFE-8115 end
	}


	protected void goToBatching3080() {
		/*    IF   S6363-ACTION            = 'A' OR                        */
		if (isEQ(sv.action,"A")
		|| isEQ(sv.action,"C")
		|| isNE(sv.clttwo,SPACES)) {
			goTo(GotoLabel.batching3080);
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
@Override
protected void whereNext4000()
	{
			para4000();
		}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			return ;
		}
		if (isEQ(sv.action,"A")) {
			if (isEQ(sv.chdrsel,SPACES)) {
				wsspcomn.secProg[1].set(subprogrec.nxt1prog);
				wsspcomn.secProg[2].set(subprogrec.nxt2prog);
				wsspcomn.secProg[3].set(subprogrec.nxt3prog);
				wsspcomn.secProg[4].set(subprogrec.nxt4prog);
			}
			else {
				wsspcomn.secProg[1].set(subprogrec.nxt3prog);
				wsspcomn.secProg[2].set(subprogrec.nxt4prog);
			}
		}
		if (isEQ(sv.action,"C")) {
			wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		}
		if (isEQ(sv.action,"B")) {
			if (isEQ(sv.clttwo,SPACES)) {
				wsspcomn.secProg[1].set(subprogrec.nxt1prog);
				wsspcomn.secProg[2].set(subprogrec.nxt2prog);
				wsspcomn.secProg[3].set(subprogrec.nxt3prog);
				wsspcomn.secProg[4].set(subprogrec.nxt4prog);
			}
			else {
				if (isEQ(clntpfModel.getClttype(),"P")) {
					wsspcomn.secProg[1].set(subprogrec.nxt2prog);
					wsspcomn.secProg[2].set(subprogrec.nxt4prog);
				}
				else {
					wsspcomn.secProg[1].set(subprogrec.nxt3prog);
					wsspcomn.secProg[2].set(subprogrec.nxt4prog);
				}
			}
		}
		wsspcomn.programPtr.set(1);
	}
}
