package com.csc.life.enquiries.screens;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;
import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.tablemodel.GeneralTable;


/**
 * Screen variables for SR59AScreenvars
 * @version 1.0 generated on 22/08/16 06:53
 * @author csc
 */
public class Sr59aScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(449);
	public FixedLengthStringData dataFields = new FixedLengthStringData(177).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,8);
	public PackedDecimalData zlstupdte = DD.zlstupdte.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData zusrprf = DD.usrprf.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,76);	
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData zafropt1disp = DD.zafropt1.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData zafritemdisp = DD.zafritem.copy().isAPartOf(dataFields,135);
	public PackedDecimalData zlastdte = DD.zlastdte.copy().isAPartOf(dataFields,137);
	public PackedDecimalData znextdte = DD.znextdte.copy().isAPartOf(dataFields,145);	
	public FixedLengthStringData zafropt1 = DD.zafropt1.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData zafrfreq = DD.zafrfreq.copy().isAPartOf(dataFields,157);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,159);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,167);
	public FixedLengthStringData zafritem = DD.zafritem.copy().isAPartOf(dataFields,175);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 177);	
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData zlstupdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData zusrprfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData zafropt1dispErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData zafritemdispErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData zlastdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData znextdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData zafropt1Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData zafrfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData zafritemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 245);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] zlstupdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] zusrprfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] zafropt1dispOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] zafritemdispOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] zlastdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] znextdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] zafropt1Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] zafrfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] zafritemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);

	/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);	
	public FixedLengthStringData zlastdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData znextdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData zlstupdteDisp = new FixedLengthStringData(10);

	public LongData Sr59ascreenWritten = new LongData(0);
	public LongData Sr59aprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr59aScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		screenFields = new BaseData[] {chdrnum, ctypedes, zlstupdte, zusrprf, chdrstatus, premstatus, cownnum, ownername, zafropt1disp, zafritemdisp, zlastdte, znextdte, zafropt1, zafrfreq, zafritem};
		screenOutFields = new BaseData[][] {chdrnumOut, ctypedesOut, zlstupdteOut, zusrprfOut, chdrstatusOut, premstatusOut, cownnumOut, ownernameOut, zafropt1dispOut, zafritemdispOut, zlastdteOut, znextdteOut, zafropt1Out, zafrfreqOut, zafritemOut};
		screenErrFields = new BaseData[] {chdrnumErr, ctypedesErr, zlstupdteErr, zusrprfErr, chdrstatusErr, premstatusErr, cownnumErr, ownernameErr, zafropt1dispErr, zafritemdispErr, zlastdteErr, znextdteErr, zafropt1Err, zafrfreqErr, zafritemErr};
		screenDateFields = new BaseData[] {currfrom, ptdate, zlastdte, znextdte, zlstupdte,};
		screenDateErrFields = new BaseData[] {currfromErr, ptdateErr, zlastdteErr, znextdteErr, zlstupdteErr};
		screenDateDispFields = new BaseData[] {currfromDisp, ptdateDisp, zlastdteDisp, znextdteDisp, zlstupdteDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr59ascreen.class;
		protectRecord = Sr59aprotect.class;
	}

}
