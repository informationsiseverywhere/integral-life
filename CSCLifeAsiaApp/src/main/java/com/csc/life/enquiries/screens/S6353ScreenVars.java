
package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6353
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6353ScreenVars extends SmartVarModel { 

	
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());//fwang3 ICIL-4 1953
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);//ILIFE-7062
	public FixedLengthStringData agntsdets = DD.agntsdets.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData aiind = DD.aiind.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData brchname = DD.brchlname.copy().isAPartOf(dataFields,5);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,35);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,43);
	//ILIFE-875 START - Screen S6353 is not showing the field description
	public FixedLengthStringData chdrstatus = DD.chdrstatus_longdesc.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData claimsind = DD.claimsind.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData comphist = DD.comphist.copy().isAPartOf(dataFields,88);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,97);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,127);
	public FixedLengthStringData extradets = DD.extradets.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,136);
	public ZonedDecimalData hissdte = DD.hissdte.copyToZonedDecimal().isAPartOf(dataFields,137);
	public ZonedDecimalData hoissdte = DD.hoissdte.copyToZonedDecimal().isAPartOf(dataFields,145);
	public ZonedDecimalData hpropdte = DD.hpropdte.copyToZonedDecimal().isAPartOf(dataFields,153);
	public ZonedDecimalData hprrcvdt = DD.hprrcvdt.copyToZonedDecimal().isAPartOf(dataFields,161);
	public ZonedDecimalData huwdcdte = DD.huwdcdte.copyToZonedDecimal().isAPartOf(dataFields,169);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(dataFields,177);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData jlifename = DD.cltname2.copy().isAPartOf(dataFields,179);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,301);
	public FixedLengthStringData jowner = DD.jowner.copy().isAPartOf(dataFields,309);
	public FixedLengthStringData jownername = DD.cltname2.copy().isAPartOf(dataFields,317);
	public ZonedDecimalData instpramt = DD.lastinsamt.copyToZonedDecimal().isAPartOf(dataFields,439);
	public ZonedDecimalData lastinsdte = DD.lastinsdte.copyToZonedDecimal().isAPartOf(dataFields,456);
	public FixedLengthStringData lifename = DD.cltname2.copy().isAPartOf(dataFields,464);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,586);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,594);
	public ZonedDecimalData nextinsamt = DD.nextinsamt.copyToZonedDecimal().isAPartOf(dataFields,595);
	public ZonedDecimalData nextinsdte = DD.nextinsdte.copyToZonedDecimal().isAPartOf(dataFields,612);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,620);
	public FixedLengthStringData outind = DD.outind.copy().isAPartOf(dataFields,624);
	public FixedLengthStringData ownername = DD.cltname2.copy().isAPartOf(dataFields,625);
	public FixedLengthStringData payer = DD.payer.copy().isAPartOf(dataFields,747);
	public FixedLengthStringData payername = DD.cltname2.copy().isAPartOf(dataFields,755);
	public FixedLengthStringData payfreq = DD.payfreq.copy().isAPartOf(dataFields,877);
	public FixedLengthStringData plancomp = DD.plancomp.copy().isAPartOf(dataFields,879);
	public FixedLengthStringData polcomp = DD.polcomp.copy().isAPartOf(dataFields,880);
	public FixedLengthStringData premstatus = DD.premstatus_longdesc.copy().isAPartOf(dataFields,881);
	//ILIFE-875 END - Screen S6353 is not showing the field description
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,911);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,919);
	public FixedLengthStringData cltrole = DD.rolesind.copy().isAPartOf(dataFields,922);
	public FixedLengthStringData servagnam = DD.cltname2.copy().isAPartOf(dataFields,923);
	public FixedLengthStringData servagnt = DD.servagnt.copy().isAPartOf(dataFields,1045);
	public FixedLengthStringData servbr = DD.servbr.copy().isAPartOf(dataFields,1053);
	public FixedLengthStringData subacbal = DD.subacbal.copy().isAPartOf(dataFields,1055);
	public FixedLengthStringData transhist = DD.transhist.copy().isAPartOf(dataFields,1056);
	public FixedLengthStringData znfopt = DD.znfopt.copy().isAPartOf(dataFields,1057);
	public FixedLengthStringData zsredtrm = DD.zsredtrm.copy().isAPartOf(dataFields,1060);
	public FixedLengthStringData reqntype = DD.reqntype.copy().isAPartOf(dataFields,1061); //ILIFE-2472
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,1062);
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,1079);
	public FixedLengthStringData fatcastatus=DD.fatcastatus.copy().isAPartOf(dataFields,1096);
	public FixedLengthStringData fatcastatusdesc=DD.fatcastatusdesc.copy().isAPartOf(dataFields,1102);
	public FixedLengthStringData zctaxind = DD.zctaxind.copy().isAPartOf(dataFields,1142);//ALS-73
	public FixedLengthStringData schmno = DD.schemekey.copy().isAPartOf(dataFields,1143);//ALS-73
	public FixedLengthStringData schmnme = DD.schemeName.copy().isAPartOf(dataFields,1151);//ALS-73
	public FixedLengthStringData billday = DD.billday.copy().isAPartOf(dataFields,1211);//ILIFE-3735
	public FixedLengthStringData zroloverind = DD.Zroloverind.copy().isAPartOf(dataFields,1213);//ILIFE-3735
	public FixedLengthStringData nlgflg = DD.keya.copy().isAPartOf(dataFields,1214);//ILIFE-3997
	public FixedLengthStringData znlghist = DD.znlghist.copy().isAPartOf(dataFields,1215);
	public FixedLengthStringData asgnnum = DD.asgnnum.copy().isAPartOf(dataFields,1216);//ILIFE-4337
	public FixedLengthStringData asgnname = DD.cltname2.copy().isAPartOf(dataFields,1224);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,1346);//ILIFE-4108
	public ZonedDecimalData cbillamt = DD.cbillamt.copyToZonedDecimal().isAPartOf(dataFields,1347);
	
	public FixedLengthStringData refundOverpay = DD.zrefundOverpay.copy().isAPartOf(dataFields,1364);//fwang3 China localization
	public FixedLengthStringData cntaccriskamt = DD.cntaccriskamt.copy().isAPartOf(dataFields,1365);//icil-82
	//ICIL-12 start	
	public FixedLengthStringData bnkout = DD.bnkout.copy().isAPartOf(dataFields,1366); 
	public FixedLengthStringData bnkoutname = DD.bnkoutname.copy().isAPartOf(dataFields,1386); 
	public FixedLengthStringData bnksm = DD.bnksm.copy().isAPartOf(dataFields,1433); 
	public FixedLengthStringData bnksmname = DD.bnksmname.copy().isAPartOf(dataFields,1443); 
	public FixedLengthStringData bnktel = DD.bnktel.copy().isAPartOf(dataFields,1490); 
	public FixedLengthStringData bnktelname = DD.bnktelname.copy().isAPartOf(dataFields,1500); 	//ICIL-12 end
	
	public ZonedDecimalData cntfee = DD.cntfee.copyToZonedDecimal().isAPartOf(dataFields,1547);//ILIFE-7062 
	
	public FixedLengthStringData zmultOwner = DD.zmultOwner.copy().isAPartOf(dataFields,1564);//ILIFE-7277
	public FixedLengthStringData debcrdhistry = DD.extradets.copy().isAPartOf(dataFields,1565);
	public FixedLengthStringData action = DD.extradets.copy().isAPartOf(dataFields,1566);
	public ZonedDecimalData riskcommdte = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,1567);
	public ZonedDecimalData decldte = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,1575);
	public ZonedDecimalData fstprmrcptdte = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,1583);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());	//ILIFE-7062
	public FixedLengthStringData agntsdetsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData aiindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData brchlnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData claimsindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData comphistErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData extradetsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData hissdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData hoissdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData hpropdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData hprrcvdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData huwdcdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData jownerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData jownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData lastinsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData lastinsdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData nextinsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData nextinsdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData outindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData payerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData payernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData payfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData plancompErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData polcompErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData rolesindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData servagnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData servagntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData servbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData subacbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData transhistErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData znfoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData zsredtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData reqntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212); //ILIFE-2472
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData fatcastatusErr=new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData zctaxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);//ALS-73
	public FixedLengthStringData schmnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);//ALS-73
	public FixedLengthStringData schmnmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);//ALS-73
	public FixedLengthStringData billdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData zroloverindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData nlgflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);//ILIFE-3997
	public FixedLengthStringData znlghistErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);
	public FixedLengthStringData asgnnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 256); //ILIFE-4337
	public FixedLengthStringData asgnnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 260);
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 264);//ILIFE-4108
	public FixedLengthStringData cbillamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 268);
	public FixedLengthStringData cntaccriskamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 272);
	//ICIL-12 start	
	public FixedLengthStringData bnkoutErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 276);
	public FixedLengthStringData bnkoutnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 280);
	public FixedLengthStringData bnksmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 284);
	public FixedLengthStringData bnksmnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 288);
	public FixedLengthStringData bnktelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 292);
	public FixedLengthStringData bnktelnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 296);
	//ICIL-12 end
	public FixedLengthStringData cntfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 300);//ILIFE-7062
	
	public FixedLengthStringData zmultOwnerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 304);//ILIFE-7277
	public FixedLengthStringData debcrdhistryErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 308);
	public FixedLengthStringData riskcommdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 312);
	public FixedLengthStringData decldteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 316);
	public FixedLengthStringData fstprmrcptdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 320);
		
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(828).isAPartOf(dataArea, 1132);//ILIFE-7062
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());//ILIFE-7062
	public FixedLengthStringData[] agntsdetsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] aiindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] brchlnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] claimsindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] comphistOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] extradetsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] hissdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] hoissdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] hpropdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] hprrcvdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] huwdcdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] jownerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] jownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] lastinsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] lastinsdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] nextinsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] nextinsdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] outindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] payerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] payernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] payfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] plancompOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] polcompOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] rolesindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] servagnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] servagntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] servbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] subacbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] transhistOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] znfoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] zsredtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] reqntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636); //ILIFE-2472
	public FixedLengthStringData[]instPremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[]fatcastatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[] zctaxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);//ALS-73
    public FixedLengthStringData[] schmnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);//ALS-73
	public FixedLengthStringData[] schmnmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);//ALS-73
	public FixedLengthStringData[] billdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	public FixedLengthStringData[] zroloverindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] nlgflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);//ILIFE-3997
	public FixedLengthStringData[] znlghistOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);
	public FixedLengthStringData[] asgnnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 768);//ILIFE-4337
	public FixedLengthStringData[] asgnnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 780);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 792);//ILIFE-4108
	public FixedLengthStringData[] cbillamtOut = FLSArrayPartOfStructure(12,1, outputIndicators, 804);
	public FixedLengthStringData[] refundOverpayOut = FLSArrayPartOfStructure(12,1, outputIndicators, 816);//fwang3 ICIL-4
	public FixedLengthStringData[] cntaccriskamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 828);	
	//ICIL-12 start	
	public FixedLengthStringData[] bnkoutOut = FLSArrayPartOfStructure(12,1, outputIndicators, 840);
	public FixedLengthStringData[] bnkoutnameOut = FLSArrayPartOfStructure(12,1, outputIndicators, 852);
	public FixedLengthStringData[] bnksmOut = FLSArrayPartOfStructure(12,1, outputIndicators, 864);
	public FixedLengthStringData[] bnksmnameOut = FLSArrayPartOfStructure(12,1, outputIndicators, 876);
	public FixedLengthStringData[] bnktelOut = FLSArrayPartOfStructure(12,1, outputIndicators, 888);
	public FixedLengthStringData[] bnktelnameOut = FLSArrayPartOfStructure(12,1, outputIndicators, 900);
		//ICIL-12 end
	
	public FixedLengthStringData[] cntfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 912);//ILIFE-7062
	public FixedLengthStringData[] zmultOwnerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 924);//ILIFE-7277
	public FixedLengthStringData[] debcrdhistryOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 936);
	public FixedLengthStringData[] riskcommdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 948);
	public FixedLengthStringData[] decldteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 960);
	public FixedLengthStringData[] fstprmrcptdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 972);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hissdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hoissdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hpropdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hprrcvdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData huwdcdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastinsdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextinsdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	//ILJ-45 Starts
	public FixedLengthStringData riskcommdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData decldteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fstprmrcptdteDisp = new FixedLengthStringData(10);	
	//ILJ-45 End

	public LongData S6353screenWritten = new LongData(0);
	public LongData S6353protectWritten = new LongData(0);
	
	public static int[] screenSflPfInds =new int[] {9, 17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21};

	public boolean hasSubfile() {
		return false;
	}


	public S6353ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(numpolsOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimsindOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plancompOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(polcompOut,new String[] {"23","10","-23","10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rolesindOut,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subacbalOut,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transhistOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntsdetsOut,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(extradetsOut,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aiindOut,new String[] {"29","30","-29","30",null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupflgOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(indOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zsredtrmOut,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(outindOut,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comphistOut,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "35", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fatcastatusOut,new String[] {null,"80", null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zctaxindOut,new String[] {"63","64", "-63","65", null, null, null, null, null, null, null, null});//ALS-73
		fieldIndMap.put(schmnoOut,new String[] {"66","67", "-66","68", null, null, null, null, null, null, null, null});//ALS-73
		fieldIndMap.put(schmnmeOut,new String[] {"69","70", "-69","71", null, null, null, null, null, null, null, null});//ALS-73
		fieldIndMap.put(cownnumOut,new String[] {"72","73", "-72","74", null, null, null, null, null, null, null, null});//ALS-73
		fieldIndMap.put(billdayOut,new String[] {null,null, null,"75", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zroloverindOut,new String[] {"78","76", "-78","77", null, null, null, null, null, null, null, null});//ALS-313
		fieldIndMap.put(nlgflgOut,new String[] {"102","101", "-102",null, null, null, null, null, null, null, null, null});//ILIFE-3997
		fieldIndMap.put(indxflgOut,new String[] {null, "103", null, "130", null, null, null, null, null, null, null, null});///ILIFE-4108
		fieldIndMap.put(bnkoutOut,new String[] {null,null, null,"106", null, null, null, null, null, null, null, null});///ILIFE-4108
		fieldIndMap.put(bnksmOut,new String[] {null,null, null,"107", null, null, null, null, null, null, null, null});///ILIFE-4108
		fieldIndMap.put(bnktelOut,new String[] {null,null, null,"108", null, null, null, null, null, null, null, null});///ILIFE-4108
		fieldIndMap.put(cntaccriskamtOut,new String[] {null,null, null,"105", null, null, null, null, null, null, null, null});///ILIFE-4108
		fieldIndMap.put(cntfeeOut,new String[] {null, null, null, "121", null, null, null, null, null, null, null, null}); //ILIFE-7062
		fieldIndMap.put(zmultOwnerOut,new String[] {null, null, null, "123", null, null, null, null, null, null, null, null}); //ILIFE-7277
		fieldIndMap.put(riskcommdteOut,new String[] {null, null, null, "124", null, null, null, null, null, null, null, null}); //ILJ-45
		fieldIndMap.put(decldteOut,new String[] {null, null, null, "125", null, null, null, null, null, null, null, null}); //ILJ-45
		fieldIndMap.put(fstprmrcptdteOut,new String[] {null, null, null, "126", null, null, null, null, null, null, null, null}); //ILJ-45				
		//ILJ-387
		fieldIndMap.put(jownerOut,new String[] {null, null, null, "127", null, null, null, null, null, null, null, null}); 				
		fieldIndMap.put(jlifenumOut,new String[] {null, null, null, "128", null, null, null, null, null, null, null, null});
		fieldIndMap.put(reqntypeOut,new String[] {null, null, null, "129", null, null, null, null, null, null, null, null});
		fieldIndMap.put(lastinsdteOut,new String[] {null, null, null, "131", null, null, null, null, null, null, null, null});
		fieldIndMap.put(lastinsamtOut,new String[] {null, null, null, "132", null, null, null, null, null, null, null, null});
		//ILJ-387 end
		/*screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, cownnum, ownername, jowner, jownername, payer, payername,indic, servagnt, servagnam, servbr, brchname, numpols, billcurr, mop, payfreq, lastinsdte, instpramt, currfrom, nextinsdte, nextinsamt, ptdate, hpropdte, btdate, hprrcvdt, hoissdte, huwdcdte, znfopt, hissdte, claimsind, plancomp, polcomp, cltrole, subacbal, transhist, agntsdets, extradets, aiind, fupflg, ind, zsredtrm, outind, comphist, reqntype,instPrem,zstpduty01,fatcastatus,zctaxind,schmno,schmnme,billday,zroloverind,nlgflg,znlghist, asgnnum,asgnname,indxflg,cbillamt,cntfee};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, cownnumOut, ownernameOut, jownerOut, jownernameOut, payerOut, payernameOut,indicOut, servagntOut, servagnamOut, servbrOut, brchlnameOut, numpolsOut, billcurrOut, mopOut, payfreqOut, lastinsdteOut, lastinsamtOut, currfromOut, nextinsdteOut, nextinsamtOut, ptdateOut, hpropdteOut, btdateOut, hprrcvdtOut, hoissdteOut, huwdcdteOut, znfoptOut, hissdteOut, claimsindOut, plancompOut, polcompOut, rolesindOut, subacbalOut, transhistOut, agntsdetsOut, extradetsOut, aiindOut, fupflgOut, indOut, zsredtrmOut, outindOut, comphistOut, reqntypeOut,instPremOut,zstpduty01Out,fatcastatusOut,zctaxindOut,schmnoOut,schmnmeOut,billdayOut,zroloverindOut,nlgflgOut,znlghistOut,asgnnumOut,asgnnameOut,indxflgOut,cbillamtOut,cntfeeOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, cownnumErr, ownernameErr, jownerErr, jownernameErr, payerErr, payernameErr,indicErr, servagntErr, servagnamErr, servbrErr, brchlnameErr, numpolsErr, billcurrErr, mopErr, payfreqErr, lastinsdteErr, lastinsamtErr, currfromErr, nextinsdteErr, nextinsamtErr, ptdateErr, hpropdteErr, btdateErr, hprrcvdtErr, hoissdteErr, huwdcdteErr, znfoptErr, hissdteErr, claimsindErr, plancompErr, polcompErr, rolesindErr, subacbalErr, transhistErr, agntsdetsErr, extradetsErr, aiindErr, fupflgErr, indErr, zsredtrmErr, outindErr, comphistErr, reqntypeErr,instPremErr,zstpduty01Err,fatcastatusErr,zctaxindErr,schmnoErr,schmnmeErr,billdayErr,zroloverindErr,nlgflgErr,znlghistErr,asgnnumErr,asgnnameErr,indxflgErr,cbillamtErr,cntfeeErr};
		
		screenDateFields = new BaseData[] {lastinsdte, currfrom, nextinsdte, ptdate, hpropdte, btdate, hprrcvdt, hoissdte, huwdcdte, hissdte};
		screenDateErrFields = new BaseData[] {lastinsdteErr, currfromErr, nextinsdteErr, ptdateErr, hpropdteErr, btdateErr, hprrcvdtErr, hoissdteErr, huwdcdteErr, hissdteErr};
		screenDateDispFields = new BaseData[] {lastinsdteDisp, currfromDisp, nextinsdteDisp, ptdateDisp, hpropdteDisp, btdateDisp, hprrcvdtDisp, hoissdteDisp, huwdcdteDisp, hissdteDisp};*/
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields(); 
		screenErrFields = getscreenErrFields();
		
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6353screen.class;
		protectRecord = S6353protect.class;
	}
	
	
	public int getDataAreaSize()
	{
		return 2899;
	}
	
	public int getDataFieldsSize()
	{
		return 1591;
	}
	public int getErrorIndicatorSize()
	{
		return 324;
	}
	public int getOutputFieldSize()
	{
		return 984;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenFields()
	{
		
		return new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, cownnum, ownername, jowner, jownername, payer, payername,indic, servagnt, servagnam, servbr, brchname, numpols, billcurr, mop, payfreq, lastinsdte, instpramt, currfrom, nextinsdte, nextinsamt, ptdate, hpropdte, btdate, hprrcvdt, hoissdte, huwdcdte, znfopt, hissdte, claimsind, plancomp, polcomp, cltrole, subacbal, transhist, agntsdets, extradets, aiind, fupflg, ind, zsredtrm, outind, comphist, reqntype,instPrem,zstpduty01,fatcastatus,zctaxind,schmno,schmnme,billday,zroloverind,nlgflg,znlghist, asgnnum,asgnname,indxflg,cbillamt,refundOverpay,cntaccriskamt,bnkout,bnkoutname,bnksm,bnksmname,bnktel,bnktelname, cntfee,zmultOwner,riskcommdte,decldte,fstprmrcptdte};
		
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[][] getscreenOutFields()
	{
		
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, cownnumOut, ownernameOut, jownerOut, jownernameOut, payerOut, payernameOut,indicOut, servagntOut, servagnamOut, servbrOut, brchlnameOut, numpolsOut, billcurrOut, mopOut, payfreqOut, lastinsdteOut, lastinsamtOut, currfromOut, nextinsdteOut, nextinsamtOut, ptdateOut, hpropdteOut, btdateOut, hprrcvdtOut, hoissdteOut, huwdcdteOut, znfoptOut, hissdteOut, claimsindOut, plancompOut, polcompOut, rolesindOut, subacbalOut, transhistOut, agntsdetsOut, extradetsOut, aiindOut, fupflgOut, indOut, zsredtrmOut, outindOut, comphistOut, reqntypeOut,instPremOut,zstpduty01Out,fatcastatusOut,zctaxindOut,schmnoOut,schmnmeOut,billdayOut,zroloverindOut,nlgflgOut,znlghistOut,asgnnumOut,asgnnameOut,indxflgOut,cbillamtOut,refundOverpayOut,cntaccriskamtOut,bnkoutOut,bnkoutnameOut,bnksmOut,bnksmnameOut,bnktelOut,bnktelnameOut, cntfeeOut,zmultOwnerOut,riskcommdteOut,decldteOut,fstprmrcptdteOut};
		
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenErrFields()
	{
		
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, cownnumErr, ownernameErr, jownerErr, jownernameErr, payerErr, payernameErr,indicErr, servagntErr, servagnamErr, servbrErr, brchlnameErr, numpolsErr, billcurrErr, mopErr, payfreqErr, lastinsdteErr, lastinsamtErr, currfromErr, nextinsdteErr, nextinsamtErr, ptdateErr, hpropdteErr, btdateErr, hprrcvdtErr, hoissdteErr, huwdcdteErr, znfoptErr, hissdteErr, claimsindErr, plancompErr, polcompErr, rolesindErr, subacbalErr, transhistErr, agntsdetsErr, extradetsErr, aiindErr, fupflgErr, indErr, zsredtrmErr, outindErr, comphistErr, reqntypeErr,instPremErr,zstpduty01Err,fatcastatusErr,zctaxindErr,schmnoErr,schmnmeErr,billdayErr,zroloverindErr,nlgflgErr,znlghistErr,asgnnumErr,asgnnameErr,indxflgErr,cbillamtErr,cntaccriskamtErr,bnkoutErr,bnkoutnameErr,bnksmErr,bnksmnameErr,bnktelErr,bnktelnameErr, cntfeeErr, zmultOwnerErr,riskcommdteErr,decldteErr,fstprmrcptdteErr};
		
	}	
	
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {lastinsdte, currfrom, nextinsdte, ptdate, hpropdte, btdate, hprrcvdt, hoissdte, huwdcdte, hissdte, riskcommdte, decldte, fstprmrcptdte};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {lastinsdteDisp, currfromDisp, nextinsdteDisp, ptdateDisp, hpropdteDisp, btdateDisp, hprrcvdtDisp, hoissdteDisp, huwdcdteDisp, hissdteDisp, riskcommdteDisp, decldteDisp, fstprmrcptdteDisp};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {lastinsdteErr, currfromErr, nextinsdteErr, ptdateErr, hpropdteErr, btdateErr, hprrcvdtErr, hoissdteErr, huwdcdteErr, hissdteErr, riskcommdteErr, decldteErr, fstprmrcptdteErr};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public static int[] getScreenSflPfInds()
	{
		return screenSflPfInds;
	}
	


}