package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6239screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 13;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2, 80, 2, 3, 4}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6239ScreenVars sv = (S6239ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6239screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6239screensfl, 
			sv.S6239screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6239ScreenVars sv = (S6239ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6239screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6239ScreenVars sv = (S6239ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6239screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6239screensflWritten.gt(0))
		{
			sv.s6239screensfl.setCurrentIndex(0);
			sv.S6239screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6239ScreenVars sv = (S6239ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6239screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6239ScreenVars screenVars = (S6239ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hlifcnum.setFieldName("hlifcnum");
				screenVars.hlifeno.setFieldName("hlifeno");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.linetype.setFieldName("linetype");
				screenVars.select.setFieldName("select");
				screenVars.cmpntnum.setFieldName("cmpntnum");
				screenVars.component.setFieldName("component");
				screenVars.compdesc.setFieldName("compdesc");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.sumins.setFieldName("sumins");//ILJ-388
				screenVars.instPrem.setFieldName("instPrem");//ILJ-388
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hlifcnum.set(dm.getField("hlifcnum"));
			screenVars.hlifeno.set(dm.getField("hlifeno"));
			screenVars.hsuffix.set(dm.getField("hsuffix"));
			screenVars.hcoverage.set(dm.getField("hcoverage"));
			screenVars.hrider.set(dm.getField("hrider"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.linetype.set(dm.getField("linetype"));
			screenVars.select.set(dm.getField("select"));
			screenVars.cmpntnum.set(dm.getField("cmpntnum"));
			screenVars.component.set(dm.getField("component"));
			screenVars.compdesc.set(dm.getField("compdesc"));
			screenVars.statcode.set(dm.getField("statcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.sumins.set(dm.getField("sumins"));//ILJ-388
			screenVars.instPrem.set(dm.getField("instPrem"));//ILJ-388
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6239ScreenVars screenVars = (S6239ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hlifcnum.setFieldName("hlifcnum");
				screenVars.hlifeno.setFieldName("hlifeno");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.linetype.setFieldName("linetype");
				screenVars.select.setFieldName("select");
				screenVars.cmpntnum.setFieldName("cmpntnum");
				screenVars.component.setFieldName("component");
				screenVars.compdesc.setFieldName("compdesc");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.sumins.setFieldName("sumins");//ILJ-388
				screenVars.instPrem.setFieldName("instPrem");//ILJ-388
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hlifcnum").set(screenVars.hlifcnum);
			dm.getField("hlifeno").set(screenVars.hlifeno);
			dm.getField("hsuffix").set(screenVars.hsuffix);
			dm.getField("hcoverage").set(screenVars.hcoverage);
			dm.getField("hrider").set(screenVars.hrider);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("linetype").set(screenVars.linetype);
			dm.getField("select").set(screenVars.select);
			dm.getField("cmpntnum").set(screenVars.cmpntnum);
			dm.getField("component").set(screenVars.component);
			dm.getField("compdesc").set(screenVars.compdesc);
			dm.getField("statcode").set(screenVars.statcode);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("sumins").set(screenVars.sumins);//ILJ-388
			dm.getField("instPrem").set(screenVars.instPrem);//ILJ-388
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6239screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6239ScreenVars screenVars = (S6239ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hlifcnum.clearFormatting();
		screenVars.hlifeno.clearFormatting();
		screenVars.hsuffix.clearFormatting();
		screenVars.hcoverage.clearFormatting();
		screenVars.hrider.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.linetype.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.cmpntnum.clearFormatting();
		screenVars.component.clearFormatting();
		screenVars.compdesc.clearFormatting();
		screenVars.statcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.sumins.clearFormatting();//ILJ-388
		screenVars.instPrem.clearFormatting();//ILJ-388
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6239ScreenVars screenVars = (S6239ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hlifcnum.setClassString("");
		screenVars.hlifeno.setClassString("");
		screenVars.hsuffix.setClassString("");
		screenVars.hcoverage.setClassString("");
		screenVars.hrider.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.linetype.setClassString("");
		screenVars.select.setClassString("");
		screenVars.cmpntnum.setClassString("");
		screenVars.component.setClassString("");
		screenVars.compdesc.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.instPrem.setClassString("");
	}

/**
 * Clear all the variables in S6239screensfl
 */
	public static void clear(VarModel pv) {
		S6239ScreenVars screenVars = (S6239ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hlifcnum.clear();
		screenVars.hlifeno.clear();
		screenVars.hsuffix.clear();
		screenVars.hcoverage.clear();
		screenVars.hrider.clear();
		screenVars.hcrtable.clear();
		screenVars.linetype.clear();
		screenVars.select.clear();
		screenVars.cmpntnum.clear();
		screenVars.component.clear();
		screenVars.compdesc.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.sumins.clear();
		screenVars.instPrem.clear();
	}
}
