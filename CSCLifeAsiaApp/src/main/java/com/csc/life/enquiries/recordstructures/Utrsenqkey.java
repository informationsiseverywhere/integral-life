package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:53
 * Description:
 * Copybook name: UTRSENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData utrsenqKey = new FixedLengthStringData(256).isAPartOf(utrsenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsenqChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsenqKey, 0);
  	public FixedLengthStringData utrsenqChdrnum = new FixedLengthStringData(8).isAPartOf(utrsenqKey, 1);
  	public FixedLengthStringData utrsenqLife = new FixedLengthStringData(2).isAPartOf(utrsenqKey, 9);
  	public FixedLengthStringData utrsenqCoverage = new FixedLengthStringData(2).isAPartOf(utrsenqKey, 11);
  	public FixedLengthStringData utrsenqRider = new FixedLengthStringData(2).isAPartOf(utrsenqKey, 13);
  	public FixedLengthStringData utrsenqUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrsenqKey, 15);
  	public FixedLengthStringData utrsenqUnitType = new FixedLengthStringData(1).isAPartOf(utrsenqKey, 19);
  	public PackedDecimalData utrsenqPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsenqKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(utrsenqKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}