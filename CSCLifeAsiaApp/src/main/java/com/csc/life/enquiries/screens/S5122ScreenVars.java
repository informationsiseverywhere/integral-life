package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datadictionarydatatype.FLSDDObj;
import com.quipoz.framework.datadictionarydatatype.PackedDDObj;
import com.quipoz.framework.datadictionarydatatype.PackedDateDDObj;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5122
 * @version 1.0 generated on 30/08/09 06:35
 * @author Quipoz
 */
public class S5122ScreenVars extends SmartVarModel { 

	//ILIFE-1402 STARTS
	public FixedLengthStringData dataArea = new FixedLengthStringData(656);
	public FixedLengthStringData dataFields = new FixedLengthStringData(256).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData fundShortDesc = DD.fndshrtdsc.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData fundTypeShortDesc = DD.fndtypldsc.copy().isAPartOf(dataFields,69);
	public FixedLengthStringData fundtype = DD.fundtype.copy().isAPartOf(dataFields,99);
	public FixedLengthStringData inds = new FixedLengthStringData(2).isAPartOf(dataFields, 100);
	public FixedLengthStringData[] ind = FLSArrayPartOfStructure(2, 1, inds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(inds, 0, FILLER_REDEFINE);
	public FixedLengthStringData ind01 = DD.ind.copy().isAPartOf(filler,0);
	public FixedLengthStringData ind02 = DD.ind.copy().isAPartOf(filler,1);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,149);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,157);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,204);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,212);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,216);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,230);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,233);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData zvariable = DD.zvariable.copy().isAPartOf(dataFields,239);
	public FixedLengthStringData zvars = new FixedLengthStringData(6).isAPartOf(dataFields, 250);
	public FixedLengthStringData[] zvar = FLSArrayPartOfStructure(2, 3, zvars, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(zvars, 0, FILLER_REDEFINE);
	public FixedLengthStringData zvar01 = DD.zvar.copy().isAPartOf(filler1,0);
	public FixedLengthStringData zvar02 = DD.zvar.copy().isAPartOf(filler1,3);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(100).isAPartOf(dataArea, 256);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData fndcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData fndshrtdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData fndtypsdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData fundtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData indsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] indErr = FLSArrayPartOfStructure(2, 4, indsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(indsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ind01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData ind02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData zvariableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData zvarsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] zvarErr = FLSArrayPartOfStructure(2, 4, zvarsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(zvarsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zvar01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData zvar02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(300).isAPartOf(dataArea, 356);
	//ILIFE-1402 ENDS
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] fndcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] fndshrtdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] fndtypsdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] fundtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData indsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(2, 12, indsOut, 0);
	public FixedLengthStringData[][] indO = FLSDArrayPartOfArrayStructure(12, 1, indOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(indsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ind01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] ind02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] zvariableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData zvarsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] zvarOut = FLSArrayPartOfStructure(2, 12, zvarsOut, 0);
	public FixedLengthStringData[][] zvarO = FLSDArrayPartOfArrayStructure(12, 1, zvarOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(zvarsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zvar01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] zvar02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(267);  //202
	public FixedLengthStringData subfileFields = new FixedLengthStringData(89).isAPartOf(subfileArea, 0);  //72
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(subfileFields,4);
	public ZonedDecimalData fundAmount = DD.fundamnt.copyToZonedDecimal().isAPartOf(subfileFields,5);
	public ZonedDecimalData moniesDate = DD.moniesdt.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(subfileFields,30);
	public ZonedDecimalData nofDunits = DD.nofdunt.copyToZonedDecimal().isAPartOf(subfileFields,31);
	public ZonedDecimalData nofUnits = DD.nofunt.copyToZonedDecimal().isAPartOf(subfileFields,47);
	public ZonedDecimalData priceUsed = DD.priceused.copyToZonedDecimal().isAPartOf(subfileFields,63);
	public FixedLengthStringData  proctrancd = DD.proctrancd.copy().isAPartOf(subfileFields, 72);
	public ZonedDecimalData  tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields, 76);
	public PackedDecimalData  pricdte = DD.pricdte.copy().isAPartOf(subfileFields, 81);
 
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 89); //72
	public FixedLengthStringData batctrcdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData fdbkindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData fundamntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData moniesdtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData ndfindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData nofduntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData nofuntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData priceusedErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	
	public FixedLengthStringData proctrancdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData pricdteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 133);  //104
	public FixedLengthStringData[] batctrcdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] fdbkindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] fundamntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] moniesdtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] ndfindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] nofduntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] nofuntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] priceusedOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	
	public FixedLengthStringData[] proctrancdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] pricdteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	
	
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 200);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData moniesDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData priceDateDisp = new FixedLengthStringData(10);

	public LongData S5122screensflWritten = new LongData(0);
	public LongData S5122screenctlWritten = new LongData(0);
	public LongData S5122screenWritten = new LongData(0);
	public LongData S5122protectWritten = new LongData(0);
	public GeneralTable s5122screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5122screensfl;
	}

	public S5122ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "80",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zvar01Out,new String[] {null, null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zvar02Out,new String[] {null, null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zvariableOut,new String[] {null, null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind01Out,new String[] {null, null, null, "01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind02Out,new String[] {null, null, null, "02",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {priceUsed, batctrcde, nofUnits, nofDunits, moniesDate, fundAmount, feedbackInd, nowDeferInd,proctrancd,tranno,pricdte};
		screenSflOutFields = new BaseData[][] {priceusedOut, batctrcdeOut, nofuntOut, nofduntOut, moniesdtOut, fundamntOut, fdbkindOut, ndfindOut,proctrancdOut,trannoOut,pricdteOut};
		screenSflErrFields = new BaseData[] {priceusedErr, batctrcdeErr, nofuntErr, nofduntErr, moniesdtErr, fundamntErr, fdbkindErr, ndfindErr,proctrancdErr,trannoErr,pricdteErr};
		screenSflDateFields = new BaseData[] {moniesDate,pricdte};
		screenSflDateErrFields = new BaseData[] {moniesdtErr,pricdteErr};
		screenSflDateDispFields = new BaseData[] {moniesDateDisp,priceDateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, lifenum, lifename, jlife, jlifename, planSuffix, coverage, rider, unitVirtualFund, fundShortDesc, fundtype, fundTypeShortDesc, fundCurrency, zvar01, zvar02, zvariable, ind01, ind02};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, plnsfxOut, coverageOut, riderOut, vrtfndOut, fndshrtdscOut, fundtypeOut, fndtypsdscOut, fndcurrOut, zvar01Out, zvar02Out, zvariableOut, ind01Out, ind02Out};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, plnsfxErr, coverageErr, riderErr, vrtfndErr, fndshrtdscErr, fundtypeErr, fndtypsdscErr, fndcurrErr, zvar01Err, zvar02Err, zvariableErr, ind01Err, ind02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5122screen.class;
		screenSflRecord = S5122screensfl.class;
		screenCtlRecord = S5122screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5122protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5122screenctl.lrec.pageSubfile);
	}
}
