package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50uscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 16, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50uScreenVars sv = (Sr50uScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr50uscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr50uscreensfl, 
			sv.Sr50uscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr50uScreenVars sv = (Sr50uScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr50uscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr50uScreenVars sv = (Sr50uScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr50uscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr50uscreensflWritten.gt(0))
		{
			sv.sr50uscreensfl.setCurrentIndex(0);
			sv.Sr50uscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr50uScreenVars sv = (Sr50uScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr50uscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50uScreenVars screenVars = (Sr50uScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.nofUnits.setFieldName("nofUnits");
				screenVars.nofDunits.setFieldName("nofDunits");
				screenVars.moniesDateDisp.setFieldName("moniesDateDisp");
				screenVars.fundAmount.setFieldName("fundAmount");
				screenVars.feedbackInd.setFieldName("feedbackInd");
				screenVars.nowDeferInd.setFieldName("nowDeferInd");
				screenVars.vfund.setFieldName("vfund");
				screenVars.fundtype.setFieldName("fundtype");
				screenVars.asacscode.setFieldName("asacscode");
				screenVars.asacstyp.setFieldName("asacstyp");
				/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
				screenVars.priceUsed.setFieldName("priceUsed");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.nofUnits.set(dm.getField("nofUnits"));
			screenVars.nofDunits.set(dm.getField("nofDunits"));
			screenVars.moniesDateDisp.set(dm.getField("moniesDateDisp"));
			screenVars.fundAmount.set(dm.getField("fundAmount"));
			screenVars.feedbackInd.set(dm.getField("feedbackInd"));
			screenVars.nowDeferInd.set(dm.getField("nowDeferInd"));
			screenVars.vfund.set(dm.getField("vfund"));
			screenVars.fundtype.set(dm.getField("fundtype"));
			screenVars.asacscode.set(dm.getField("asacscode"));
			screenVars.asacstyp.set(dm.getField("asacstyp"));
			/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
			screenVars.priceUsed.set(dm.getField("priceUsed"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50uScreenVars screenVars = (Sr50uScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.nofUnits.setFieldName("nofUnits");
				screenVars.nofDunits.setFieldName("nofDunits");
				screenVars.moniesDateDisp.setFieldName("moniesDateDisp");
				screenVars.fundAmount.setFieldName("fundAmount");
				screenVars.feedbackInd.setFieldName("feedbackInd");
				screenVars.nowDeferInd.setFieldName("nowDeferInd");
				screenVars.vfund.setFieldName("vfund");
				screenVars.fundtype.setFieldName("fundtype");
				screenVars.asacscode.setFieldName("asacscode");
				screenVars.asacstyp.setFieldName("asacstyp");
				/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
				screenVars.priceUsed.setFieldName("priceUsed");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("nofUnits").set(screenVars.nofUnits);
			dm.getField("nofDunits").set(screenVars.nofDunits);
			dm.getField("moniesDateDisp").set(screenVars.moniesDateDisp);
			dm.getField("fundAmount").set(screenVars.fundAmount);
			dm.getField("feedbackInd").set(screenVars.feedbackInd);
			dm.getField("nowDeferInd").set(screenVars.nowDeferInd);
			dm.getField("vfund").set(screenVars.vfund);
			dm.getField("fundtype").set(screenVars.fundtype);
			dm.getField("asacscode").set(screenVars.asacscode);
			dm.getField("asacstyp").set(screenVars.asacstyp);
			/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
			dm.getField("priceUsed").set(screenVars.priceUsed);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr50uscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr50uScreenVars screenVars = (Sr50uScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.nofUnits.clearFormatting();
		screenVars.nofDunits.clearFormatting();
		screenVars.moniesDateDisp.clearFormatting();
		screenVars.fundAmount.clearFormatting();
		screenVars.feedbackInd.clearFormatting();
		screenVars.nowDeferInd.clearFormatting();
		screenVars.vfund.clearFormatting();
		screenVars.fundtype.clearFormatting();
		screenVars.asacscode.clearFormatting();
		screenVars.asacstyp.clearFormatting();
		/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
		screenVars.priceUsed.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr50uScreenVars screenVars = (Sr50uScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.nofUnits.setClassString("");
		screenVars.nofDunits.setClassString("");
		screenVars.moniesDateDisp.setClassString("");
		screenVars.fundAmount.setClassString("");
		screenVars.feedbackInd.setClassString("");
		screenVars.nowDeferInd.setClassString("");
		screenVars.vfund.setClassString("");
		screenVars.fundtype.setClassString("");
		screenVars.asacscode.setClassString("");
		screenVars.asacstyp.setClassString("");
		/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
		screenVars.priceUsed.setClassString("");
	}

/**
 * Clear all the variables in Sr50uscreensfl
 */
	public static void clear(VarModel pv) {
		Sr50uScreenVars screenVars = (Sr50uScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.nofUnits.clear();
		screenVars.nofDunits.clear();
		screenVars.moniesDateDisp.clear();
		screenVars.moniesDate.clear();
		screenVars.fundAmount.clear();
		screenVars.feedbackInd.clear();
		screenVars.nowDeferInd.clear();
		screenVars.vfund.clear();
		screenVars.fundtype.clear();
		screenVars.asacscode.clear();
		screenVars.asacstyp.clear();
		/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
		screenVars.priceUsed.clear();
	}
}
