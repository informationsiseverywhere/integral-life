package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6246screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6246ScreenVars sv = (S6246ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6246screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6246ScreenVars screenVars = (S6246ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.winfndopt.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statfund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.stsubsect.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.instprem.setClassString("");
		screenVars.virtFundSplitMethod.setClassString("");
		screenVars.percOrAmntInd.setClassString("");
		screenVars.unitVirtualFund01.setClassString("");
		screenVars.currcy01.setClassString("");
		screenVars.unitAllocPercAmt01.setClassString("");
		screenVars.unitBidPrice01.setClassString("");
		screenVars.unitVirtualFund02.setClassString("");
		screenVars.currcy02.setClassString("");
		screenVars.unitAllocPercAmt02.setClassString("");
		screenVars.unitBidPrice02.setClassString("");
		screenVars.unitVirtualFund03.setClassString("");
		screenVars.currcy03.setClassString("");
		screenVars.unitAllocPercAmt03.setClassString("");
		screenVars.unitBidPrice03.setClassString("");
		screenVars.unitVirtualFund04.setClassString("");
		screenVars.currcy04.setClassString("");
		screenVars.unitAllocPercAmt04.setClassString("");
		screenVars.unitBidPrice04.setClassString("");
		screenVars.unitVirtualFund05.setClassString("");
		screenVars.currcy05.setClassString("");
		screenVars.unitAllocPercAmt05.setClassString("");
		screenVars.unitBidPrice05.setClassString("");
		screenVars.unitVirtualFund06.setClassString("");
		screenVars.currcy06.setClassString("");
		screenVars.unitAllocPercAmt06.setClassString("");
		screenVars.unitBidPrice06.setClassString("");
		screenVars.unitVirtualFund07.setClassString("");
		screenVars.currcy07.setClassString("");
		screenVars.unitAllocPercAmt07.setClassString("");
		screenVars.unitBidPrice07.setClassString("");
		screenVars.unitVirtualFund08.setClassString("");
		screenVars.currcy08.setClassString("");
		screenVars.unitAllocPercAmt08.setClassString("");
		screenVars.unitBidPrice08.setClassString("");
		screenVars.unitVirtualFund09.setClassString("");
		screenVars.currcy09.setClassString("");
		screenVars.unitAllocPercAmt09.setClassString("");
		screenVars.unitBidPrice09.setClassString("");
		screenVars.unitVirtualFund10.setClassString("");
		screenVars.currcy10.setClassString("");
		screenVars.unitAllocPercAmt10.setClassString("");
		screenVars.unitBidPrice10.setClassString("");
		screenVars.numapp.setClassString("");
		screenVars.zafropt1.setClassString("");
		screenVars.zafritem.setClassString("");
		screenVars.cnttype.setClassString("");/*ILIFE-4036*/
		screenVars.ctypedes.setClassString("");
	}

/**
 * Clear all the variables in S6246screen
 */
	public static void clear(VarModel pv) {
		S6246ScreenVars screenVars = (S6246ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.winfndopt.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifenum.clear();
		screenVars.linsname.clear();
		screenVars.crtable.clear();
		screenVars.crtabdesc.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statfund.clear();
		screenVars.statSect.clear();
		screenVars.stsubsect.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.instprem.clear();
		screenVars.virtFundSplitMethod.clear();
		screenVars.percOrAmntInd.clear();
		screenVars.unitVirtualFund01.clear();
		screenVars.currcy01.clear();
		screenVars.unitAllocPercAmt01.clear();
		screenVars.unitBidPrice01.clear();
		screenVars.unitVirtualFund02.clear();
		screenVars.currcy02.clear();
		screenVars.unitAllocPercAmt02.clear();
		screenVars.unitBidPrice02.clear();
		screenVars.unitVirtualFund03.clear();
		screenVars.currcy03.clear();
		screenVars.unitAllocPercAmt03.clear();
		screenVars.unitBidPrice03.clear();
		screenVars.unitVirtualFund04.clear();
		screenVars.currcy04.clear();
		screenVars.unitAllocPercAmt04.clear();
		screenVars.unitBidPrice04.clear();
		screenVars.unitVirtualFund05.clear();
		screenVars.currcy05.clear();
		screenVars.unitAllocPercAmt05.clear();
		screenVars.unitBidPrice05.clear();
		screenVars.unitVirtualFund06.clear();
		screenVars.currcy06.clear();
		screenVars.unitAllocPercAmt06.clear();
		screenVars.unitBidPrice06.clear();
		screenVars.unitVirtualFund07.clear();
		screenVars.currcy07.clear();
		screenVars.unitAllocPercAmt07.clear();
		screenVars.unitBidPrice07.clear();
		screenVars.unitVirtualFund08.clear();
		screenVars.currcy08.clear();
		screenVars.unitAllocPercAmt08.clear();
		screenVars.unitBidPrice08.clear();
		screenVars.unitVirtualFund09.clear();
		screenVars.currcy09.clear();
		screenVars.unitAllocPercAmt09.clear();
		screenVars.unitBidPrice09.clear();
		screenVars.unitVirtualFund10.clear();
		screenVars.currcy10.clear();
		screenVars.unitAllocPercAmt10.clear();
		screenVars.unitBidPrice10.clear();
		screenVars.numapp.clear();
		screenVars.zafropt1.clear();
		screenVars.zafritem.clear();
		screenVars.cnttype.clear();/*ILIFE-4036*/
		screenVars.ctypedes.clear();
	}
}
