/*
 * File: Pd5e3.java
 * Date: 30 August 2009 0:37:43
 * Author: Quipoz Limited
 * 
 * Class transformed from PD5E3.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.tablestructures.T6632rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.screens.Sd5e3ScreenVars;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.recordstructures.SurtaxRec;
import com.csc.life.terminationclaims.tablestructures.Tr691rec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selectio 
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will b 
*     stored  in  the  CHDRENQ I/O module. Retrieve the details an 
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description fro 
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description fro 
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist 
*
*
*     The  details  that  will  be  displayed  will  come  from tw 
*     data-sets:  SACSENQ  and  ACBLENQ.  The same information wil 
*     be  extracted  from both data-sets and they will both be rea 
*     in  the  same sequence. However different fields are used fo 
*     the keys on each data-set.
*
*     This  program should process all the relevant ACBLENQ record 
*     and then all the relevant SACSENQ records.
*
*     Load the subfile as follows:
*
*          T5645 contains a list of all the SACS Code and SACS Typ 
*          combinations that are used to drive  the program throug 
*          the  ACBLENQ and SACSENQ data-sets. Perform  a  BEGN  o 
*          T5645 using WSAA-PROG as the key. Note that there may b 
*          several  pages  on  T5645 each with up to  15  lines  o 
*          detail.  From each line take the SACS Code and SACS Typ 
*          and use them to read through ACBLENQ. The  full  key is 
*          RLDGCOY   (from  CHDRCOY),  RLDGACCT   (from   CHDRNUM) 
*          SACSCODE (from T5645) and SACSTYP (from T5645).
*
*          Display the SACS CODE with its  short  description  fro 
*          T3616,  the SACS TYPE with its  short  description  fro 
*          T3695,  the  Original Currency, (ORIGCURR)  and  Curren 
*          Balance (SACSCURBAL).
*
*          Also store, on a hidden field in the  subfile  record,  
*          flag  indicating that this record's  details  come  fro 
*          ACBLENQ.
*
*          Process SACSENQ for the same combination  of  codes. Th 
*          SACSENQ key is CHDRCOY, CHDRNUM,  SACSCODE  and SACSTYP 
*          The  same  fields are displayed.  Original  Currency  i 
*          CNTCURR.
*
*          Also store, on a hidden field in the  subfile  record,  
*          flag  indicating that this record's  details  come  fro 
*          SACSENQ.
*
*     Load  all  pages  required in the subfile and set the subfil 
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Release  both  the ACBLENQ and SACSENQ I/O  modules  in  cas 
*     anything is held from a previous selection.
*
*     At  this  point  the program will be either searching for th 
*     FIRST  selected  record  in  order  to  pass  control  to th 
*     Transactions Postings program for the selected transaction o 
*     it  will  be returning from the Transactions Postings progra 
*     after  displaying  some  details  and  searching for the NEX 
*     selected record.
*
*     It  will be able to determine which of these two states it i 
*     in by examining the Stack Action Flag.
*
*     If not returning from a Transactions Postings display, (Stac 
*     Action  Flag  is  blank),  perform  a start on the subfile t 
*     position the file pointer at the beginning.
*
*     Each  time  it  returns  to  this  program after processing  
*     previous seelction its position in the subfile will have bee 
*     retained  and  it will be able to continue from where it lef 
*     off.
*
*     Processing  from here is the same for either state. After th 
*     Start  or  after  returning to the program after processing  
*     previous selection read the next record from the subfile.  I 
*     this  is not selected (Select is blank), continue reading th 
*     next  subfile  record  until  one  is  found with a non-blan 
*     Select  field or end of file is reached. Do not use the 'Rea 
*     Next Changed Subfile Record' function.
*
*     If nothing was selected or there are no  more  selections  t 
*     process,  continue  by  just  moving spaces  to  the  curren 
*     stackaction field and exit.
*
*     If a selection has been found the Sub-Account Balances are t 
*     be displayed.
*
*     If the selected subfile record contains data  from  a SACSEN 
*     record then the next function will read the RTRN data-set fo 
*     the Sub-Account Postings. Perform a READS  on the appropriat 
*     SACSENQ record, add 1 to the program pointer and exit.
*
*     If the selected subfile record contains data  from  a ACBLEN 
*     record then the next function will read the ACMV data-set fo 
*     the Sub-Account Postings. Perform a READS  on the appropriat 
*     ACBLENQ record, add 1 to the program pointer and exit.
*
*
* Notes.
* ------
*
*     Create  a  new  view of ACBLPF called ACBLENQ which uses onl 
*     those  fields  required for this program. It will be keyed o 
*     RLDGCOY, RLDGACCT (first 8 characters), SACSCODE and SACSTYP 
*
*     Create  a  new  view of SACSPF called SACSENQ which uses onl 
*     those fields required for this program. It will  be  keyed o 
*     CHDRCOY, CHDRNUM, SACSCODE and SACSTYP.
*
*     Tables Used:
*
* T1688 - Transaction Codes                 Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3616 - Sub-Account Codes                 Key: SACSCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T3695 - Sub-Account Types                 Key: SACSTYP
* T5645 - Financial Trans Accounting Rules  Key: WSAA-PROG
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class Pd5e3 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pd5e3.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5E3");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final String wsaaAcblenqFlag = "A";
	private ZonedDecimalData wsaaItemseq = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaComponent = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 8);
	private static final String t3588 = "T3588";
	private static final String t3616 = "T3616";
	private static final String t3623 = "T3623";
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String itemrec = "ITEMREC";
	
	private static final String t5687 = "T5687";
	private static final String t5611 = "T5611";
	private static final String t6598 = "T6598";
	private static final String tr691 = "TR691";
	private static final String t6632 = "T6632";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private static final String t5679 = "T5679"; 
	
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5611rec t5611rec = new T5611rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6632rec t6632rec = new T6632rec();
	private Tr691rec tr691rec = new Tr691rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Totloanrec totloanrec = new Totloanrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Txcalcrec txcalcrec = new Txcalcrec();	
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Boolean suspenseFlag=true;
	
	private Batckey wsaaBatckey = new Batckey();	
	private Wssplife wssplife = new Wssplife();
	private Sd5e3ScreenVars sv = ScreenProgram.getScreenVars( Sd5e3ScreenVars.class);
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private List<Acblpf> acblList;
	private Acblpf acblpf = new Acblpf(); 
	//private List<String> sacsCodeTypeList;	
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrList;
	
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
  	private Hpadpf hpadpf;
  	
  	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
  	private Utrspf utrspf = new Utrspf();
  	private List<Utrspf> utrssurList = null;

  	private UtrnpfDAO utrnpfDAO	 = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);  	
  	private Utrnpf utrnpf = new Utrnpf();
  	private List<Utrnpf> utrnList = null;
  	
  	private VprcpfDAO vprcpfDAO	 = getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class); 
  	private Vprcpf vprcpf = new Vprcpf();
  	private List<Vprcpf> vprcList = null;
  	
  	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private List<Itempf> itempfList = null;
	
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	private Map<String, Lifepf> lifepfMap= null;
	
	private List<Itempf> t5687List = null;
	private List<Itempf> t5611List = null;
	private List<Itempf> t6632List = null;
	
	
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private List<Clntpf> clntpfList = null;


  	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();   		 
  	private PackedDecimalData cur_balance = new PackedDecimalData(17, 2).init(0);
  	private PackedDecimalData cur_unit = new PackedDecimalData(17, 2).init(0);
  	private PackedDecimalData wsaaFundAmnt= new PackedDecimalData(17, 2).init(0);
	private Srcalcpy srcalcpy = new Srcalcpy();
	
	private FixedLengthStringData wsaaSurrenderRecStore = new FixedLengthStringData(150);
	private FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	
	
	private ErrorsInner errorsInner = new ErrorsInner();

	
	private Payrpf payrpf = new Payrpf();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	private List<Payrpf> payrpfList= null;
	private ExternalisedRules er = new ExternalisedRules();
	private String wsaaNoPrice = "";
	private FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	private FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 6, FILLER).init(SPACES);
	private ZonedDecimalData wsaaSwitch2 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime = new Validator(wsaaSwitch2, "1");
	private ZonedDecimalData wsaaSwitch1 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime1 = new Validator(wsaaSwitch1, "1");
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPenaltyTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans1 = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaLoanValue = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTaxAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaapolicyloan = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTdbtamt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaZrcshamt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2).init(0);	
	private PackedDecimalData wsaaComponentTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaCompLoanVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaLoanAvailable = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldLoanAvail = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCashAmount = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaCurrLoanReqd = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
		
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData tpoldbtrec = new FixedLengthStringData(10).init("TPOLDBTREC");
	
	private String wsaaValidStatus = "";

	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();

	private PackedDecimalData wsaaTotamnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHpltot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotbon = new PackedDecimalData(17, 2).init(0);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private static final String covrsurrec = "COVRSURREC";
	private static final String t6640 = "T6640";
	private T6640rec t6640rec = new T6640rec();
	private String wsaaBsurrOk = "";
    //private Subprogrec subprogrec = new Subprogrec();
    private Sdasancrec sdasancrec = new Sdasancrec();
    private LoanTableDAM loanIO = new LoanTableDAM();
    private static final String loanrec = "LOANREC";
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();

    private PackedDecimalData wsaaAcblCurrentBalance = new PackedDecimalData(17, 2);
    private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
    private Validator singleComponent = new Validator(wsaaProcessingType, "2");
    private T6598rec t6598rec1 = new T6598rec();

	boolean CTENQ003Permission  = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		loopCovrsur2975,
		exit2979,
		loopCovrsur2985,
		exit2989
	}

	public Pd5e3() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5e3", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
	CTENQ003Permission  = FeaConfg.isFeatureExist("2", "CTENQ003", appVars, "IT");
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	wsaaToday.set(datcon1rec.intDate); 
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//sv.sacscurbal.set(ZERO);
		wsaaRldgacct.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("Sd5e3", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		if(!(chdrpf.getUniqueNumber()>0)) {
			fatalError600();
		}
		payrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		payrpf.setChdrnum(chdrpf.getChdrnum());
		payrpf.setValidflag("1");
		payrpf.setPayrseqno(1);
		payrpfList = payrpfDAO.readPayrData(payrpf);
		if(payrpfList==null || (payrpfList!=null && payrpfList.size()==0))
		{
			fatalError600();
		}
		payrpf = payrpfList.get(0);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.cownnum.set(chdrpf.getCownnum());
		
		clntpf = new Clntpf();
		ownerName();
		readLife();
		readTr52d5120();		
		readCOVRPF();			
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}		
		else {
			sv.chdrstatus.set(descIO.getLongdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getLongdesc());
		}
        chdrmjaIO.setDataKey(SPACES);
        chdrmjaIO.setChdrcoy(chdrpf.getChdrcoy());
        chdrmjaIO.setChdrnum(chdrpf.getChdrnum());
        /* MOVE READH                  TO CHDRMJA-FUNCTION.             */
        chdrmjaIO.setFunction(varcom.readr);
        SmartFileCode.execute(appVars, chdrmjaIO);
	}

protected void ownerName() {
	clntpf.setClntcoy(wsspcomn.fsuco.toString());
	clntpf.setClntnum(sv.cownnum.toString());
	clntpf.setClntpfx("CN");
	clntpfList = clntpfDAO.readClientpfData(clntpf);
	if (clntpfList == null || (clntpfList != null && clntpfList.size() == 0)) {
		return;
	}
	clntpf = clntpfList.get(0);
	plainname(clntpf);
	sv.ownername.set(wsspcomn.longconfname);
	sv.lifename.set(wsspcomn.longconfname);
}

protected void readLife() {
	lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());
	lifepf.setChdrnum(chdrpf.getChdrnum());
	lifepf.setLife("01");
	lifepf.setJlife("00");
	lifepf.setValidflag("1");
	Map<String, Lifepf> lifepfMap =lifepfDAO.getLifeEnqData(lifepf);
	Lifepf lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim()+""+lifepf.getValidflag().trim());
	if(lifepfModel==null){
		return;
	}
	sv.lifenum.set(lifepfModel.getLifcnum());
	lifepf.setJlife("01");
	lifepfMap =lifepfDAO.getLifeEnqData(lifepf);
	lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim()+""+lifepf.getValidflag().trim());
	if(lifepfModel==null){            
		sv.jlife.set(SPACES);
		sv.jlifename.set(SPACES);
	}
	else {
		sv.jlife.set(lifepfModel.getLifcnum());
		clntpf.setClntnum(lifepfModel.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpfList= clntpfDAO.readClientpfData(clntpf);
			if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
			{
				return;
			}
			else{
				clntpf = clntpfList.get(0);
				plainname(clntpf);
				sv.jlifename.set(wsspcomn.longconfname);
		  }
	}
}


protected void plainname(Clntpf clntpf) {

	wsspcomn.longconfname.set(SPACES);
	if (clntpf.getClttype().equals("C")) {
		corpname(clntpf);
		return;
	}
	if (isNE(clntpf.getGivname(), SPACES)) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getSurname(), "  ");
		stringVariable1.addExpression(", ");
		stringVariable1.addExpression(clntpf.getGivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	} else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

}

protected void corpname(Clntpf clntpf) {

	wsspcomn.longconfname.set(SPACES);

	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(clntpf.getLsurname(), "  ");
	stringVariable1.addExpression(" ");
	stringVariable1.addExpression(clntpf.getLgivname(), "  ");
	stringVariable1.setStringInto(wsspcomn.longconfname);

}

protected void validateStatusesWp1650()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t5679); /* IJTI-1479 */
	itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
	itempf = itemDAO.getItempfRecord(itempf);
	if (null == itempf) {
		sv.chdrnumErr.set(errorsInner.f321);
		return;
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));				
	wsaaValidStatus = "N";
	
	for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
	|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
		riskStatusCheckWp1660();
	}
	if (isEQ(wsaaValidStatus, "Y")) {
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			premStatusCheckWp1670();
		}
	}
}

protected void riskStatusCheckWp1660()
{
	/*START*/
	if (isEQ(covrpf.getRider(), ZERO)) {
		if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
			if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
				wsaaValidStatus = "Y";
				return ;
			}
		}
	}
	if (isGT(covrpf.getRider(), ZERO)) {
		if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
			if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
				wsaaValidStatus = "Y";
				return ;
			}
		}
	}
	/*EXIT*/
}

protected void premStatusCheckWp1670()
{
	/*START*/
	if (isEQ(covrpf.getRider(), ZERO)) {
		if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
			if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
				wsaaValidStatus = "Y";
				return ;
			}
		}
	}
	if (isGT(covrpf.getRider(), ZERO)) {
		if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
			if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
				wsaaValidStatus = "Y";
				return ;
			}
		}
	}
	/*EXIT*/
}
	
	protected void readCOVRPF()
		{		
		covrList = covrDao.getCovrsurByComAndNum(wsspcomn.company.toString(), chdrpf.getChdrnum());
		wsaaNoPrice = "N";
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaPenaltyTot.set(ZERO);
		wsaaSwitch2.set(1);
		wsaaSwitch1.set(1);
		wsaaZrcshamt.set(ZERO);
		wsaapolicyloan.set(ZERO);
		wsaaTaxAmt.set(0);
		wsaaComponentTot.set(ZERO);
		wsaaCompLoanVal.set(ZERO);
		wsaaLoanAvailable.set(ZERO);
		wsaaHeldCurrLoans.set(ZERO);
		wsaaHeldCurrLoans1.set(ZERO);
		wsaaHeldLoanAvail.set(ZERO);
		wsaaTotamnt.set(ZERO);
		wsaaHpltot.set(ZERO);
		wsaaTotbon.set(ZERO);
		for(Covrpf covr:covrList) {
				covrpf = covr;				
				sv.subfileArea.set(SPACES);
				sv.component.set(covrpf.getCrtable());
				if (isEQ(covrpf.getInstprem(), 0)) {
					/*         this means we have single premium component            */
					sv.instPrem.set(covrpf.getSingp());
				}
				else {
					sv.instPrem.set(covrpf.getInstprem());
				}				
				suminwithTax();
				sv.sumin.set(covrpf.getSumins());
				sv.hissdte.set(covrpf.getCrrcd());
				sv.fundamnt.set(ZERO);
				sv.surrval.set(ZERO);
				sv.loanVal.set(ZERO);
				sv.bounsVal.set(ZERO);
				/*Obtain the Premuim Status description from T3588.*/
				descIO.setDataKey(SPACES);
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl(t3588);
				descIO.setDescitem(covrpf.getPstatcode());
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(),varcom.oK)
				&& isNE(descIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
					sv.pstatcode.fill("?");
				}
				else {
					sv.pstatcode.set(descIO.getShortdesc());
				}
				
				/*    Obtain the Contract Status description from T3623.*/
				descIO.setDataKey(SPACES);
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl(t3623);
				descIO.setDescitem(covrpf.getStatcode());
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(),varcom.oK)
				&& isNE(descIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
					sv.statcode.fill("?");
				}
				else {
					sv.statcode.set(descIO.getShortdesc());
				}
				readFUND();
				readSurr();
				//readHPAD();
				if (CTENQ003Permission) {
					readbouns();
				}
				readACBL();
				addsflData();
			}
			sv.totalSurVal.set(wsaaTotamnt);
			sv.totalLoanVal.set(wsaaHpltot);
			sv.totBonusVal.set(wsaaTotbon);
				
		}
	
	private void readbouns() {
		/*  Obtain the Surrender Bonus Calc Method from T6640.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getCurrfrom());//20200601
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);//3
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t6640)
		|| isNE(itdmIO.getItemitem(), covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t6640rec.t6640Rec.set(SPACES);
			return;
		}
		else {
			t6640rec.t6640Rec.set(itdmIO.getGenarea());//                RB01BS01P        N       
		}
		if (!isEQ(t6640rec.surrenderBonusMethod, SPACES)) {
		/*  Obtain the Surrender Bonus Routine from T6598.*/
		itempf = new Itempf();
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(),t6598,t6640rec.surrenderBonusMethod.toString());
		if (itempf == null) {
			t6598rec1.t6598Rec.set(SPACES);
		}
		else {
			t6598rec1.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		policyLoad5000();
		}
	}
	protected void policyLoad5000()
	{
		
		/*  Get Reserve value and Total Bonus Value.*/
		getReserve5100();
		sv.bounsVal.set(wsaaAcblCurrentBalance);
        wsaaTotbon.add(sv.bounsVal);
		/*EXIT*/
	}
	
	protected void getReserve5100()
	{
		start5100();
	}
	protected void start5100()
	{
		srcalcpy.currcode.set(chdrpf.getCntcurr());
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
		srcalcpy.lifeLife.set(covrpf.getLife());
		srcalcpy.lifeJlife.set(covrpf.getJlife());
		srcalcpy.covrCoverage.set(covrpf.getCoverage());
		srcalcpy.covrRider.set(covrpf.getRider());
		srcalcpy.crtable.set(covrpf.getCrtable());
		srcalcpy.crrcd.set(covrpf.getCrrcd());
		srcalcpy.status.set("BONS");
		srcalcpy.pstatcode.set(covrpf.getPstatcode());
		srcalcpy.polsum.set(chdrpf.getPolsum());
		srcalcpy.ptdate.set(chdrpf.getPtdate());
		srcalcpy.effdate.set(chdrpf.getCurrfrom());
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.type.set("F");
		/*  Set Plan Suffix as appropriate. If surrendering a*/
		/*  notional policy, the calculation routine must get its*/
		/*  information from the summary ACBL record.*/
		srcalcpy.planSuffix.set(covrpf.getPlanSuffix());
		
		
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			/*IVE-796 RUL Product - Partial Surrender Calculation started*/
			//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
			{
				callProgram(t6598rec1.calcprog, srcalcpy.surrenderRec);
			}
			else
			{
		 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
				vpxsurcrec.function.set("INIT");
				callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
				vpmfmtrec.initialize();
				vpmfmtrec.amount02.set(wsaaEstimateTot);			

				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrpf);//VPMS call
				
				
				if(isEQ(srcalcpy.type,"L"))
				{
					vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
					callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
					srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"C"))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else
				{
					srcalcpy.status.set(varcom.oK);
				}
			}

			/*IVE-796 RUL Product - Partial Surrender Calculation end*/
			if (isNE(srcalcpy.status, varcom.oK)
			&& isNE(srcalcpy.status, varcom.endp)) {
				syserrrec.statuz.set(srcalcpy.status);
				fatalError600();
			}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		callRounding6000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
		callRounding6000();
		srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		if (isEQ(srcalcpy.status,"****")) {//IBPLIFE-1333
		wsaaAcblCurrentBalance.set(srcalcpy.estimatedVal);
		}
		if (singleComponent.isTrue()
		&& isLTE(covrpf.getPlanSuffix(),chdrmjaIO.getPolsum())) {
			compute(wsaaAcblCurrentBalance, 2).set(div(wsaaAcblCurrentBalance,chdrmjaIO.getPolsum()));
		}
	}
	zrdecplrec.amountIn.set(wsaaAcblCurrentBalance);
	callRounding6000();
	wsaaAcblCurrentBalance.set(zrdecplrec.amountOut);
}


	protected void readFUND(){
		wsaaFundAmnt.set(ZERO);
		cur_balance.set(ZERO);
		cur_unit.set(ZERO);
		utrssurList = new ArrayList<Utrspf>();
		utrssurList = utrspfDAO.getUtrsRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(), covrpf.getRider()); /* IJTI-1479 */
		if(!utrssurList.isEmpty()){			
			for(Utrspf utrs:utrssurList) {
				cur_balance.set(ZERO);
				cur_unit.set(ZERO);
				cur_balance =  PackedDecimalData.parseObject(utrs.getCurrentUnitBal());
				cur_unit = PackedDecimalData.parseObject(vprcpfDAO.getvprcRecord(covrpf.getChdrcoy(), utrs.getUnitVirtualFund(),wsaaToday.toInt())); /* IJTI-1479 */
				compute(wsaaFundAmnt,2).set(add(wsaaFundAmnt,(mult(cur_balance,cur_unit))));			
				}			
		}else{
			wsaaFundAmnt.set(ZERO);
		}
		
		sv.fundamnt.set (wsaaFundAmnt);
	}

protected void readSurr() {
	/* Begin on the coverage/rider record*/	
		if (firstTime1.isTrue()) {
			wsaaSwitch1.set(0);					
				validateStatusesWp1650();
				if(isEQ(wsaaValidStatus, "Y")){
					processComponents1350();			
					
					wsaaHeldLoanAvail.set(wsaaLoanAvailable);
					if (isNE(t6598rec.calcprog, SPACES)) {
					calcSurr();
					}
					wsaaHeldLoanAvail.add(wsaaZrcshamt);
					if (isGT(wsaaHeldLoanAvail,wsaaHeldCurrLoans1)) {
						compute(wsaaHeldLoanAvail, 2).set(sub(wsaaHeldLoanAvail,wsaaHeldCurrLoans1));
					}
					else {
						wsaaHeldLoanAvail.set(ZERO);
					}
					sv.loanVal.set(wsaaHeldLoanAvail);
					wsaaHpltot.add(sv.loanVal);
			}
			
		}
	}

protected void processComponents1350()
	{
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrpf.getCrtable());
		obtainSurrenderCalc1400();
		srcalcpy.currcode.set(covrpf.getPremCurrency());	
		srcalcpy.planSuffix.set(covrpf.getPlanSuffix());
		/* check the reinsurance file at a later date and move*/
		/* 'y' to this field if they are present*/
		/* This Check never actually took place, but does now.             */
		/*  MOVE SPACES                    TO S5026-RIIND.               */
		/*   PERFORM 1980-READ-RACD.                              <A06843>*/
		/*                                                        <A06843>*/
		/*   IF RACDMJA-STATUZ            = O-K                   <A06843>*/
		/*       MOVE 'Y'                TO S5026-RIIND           <A06843>*/
		/*   END-IF.                                              <A06843>*/
		srcalcpy.endf.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
		srcalcpy.lifeLife.set(covrpf.getLife());
		srcalcpy.lifeJlife.set(covrpf.getJlife());
		srcalcpy.covrCoverage.set(covrpf.getCoverage());
		srcalcpy.covrRider.set(covrpf.getRider());
		srcalcpy.crtable.set(covrpf.getCrtable());
		srcalcpy.crrcd.set(covrpf.getCrrcd());
		srcalcpy.convUnits.set(covrpf.getConvertInitialUnits());
		srcalcpy.pstatcode.set(covrpf.getPstatcode());
		srcalcpy.status.set(SPACES);
		srcalcpy.polsum.set(chdrpf.getPolsum());
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.ptdate.set(chdrpf.getPtdate());
		srcalcpy.chdrCurr.set(chdrpf.getCntcurr());
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrpf.getBillfreq());
		}
		wsaaFeeTax.set(ZERO);
		while (isNE(srcalcpy.status, varcom.mrnf) && !(isEQ(srcalcpy.status, varcom.endp))) {
			callSurMethodWhole1600();
		}				
		if (isNE(t5687rec.loanmeth,SPACES)) {
			compute(wsaaCompLoanVal, 2).set(mult(wsaaComponentTot,t6632rec.maxpcnt));
			compute(wsaaCompLoanVal, 2).set(div(wsaaCompLoanVal,100));
			zrdecplrec.amountIn.set(wsaaCompLoanVal);
			callRounding6000();
			wsaaCompLoanVal.set(zrdecplrec.amountOut);
			wsaaLoanAvailable.add(wsaaCompLoanVal);
		}
	}

protected void obtainSurrenderCalc1400()
	{
	    itempf = new Itempf();	    
	    itempf.setItempfx("IT");
	    itempf.setItemcoy(wsspcomn.company.toString());
	    itempf.setItemitem(wsaaCrtable.toString());
	    itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
	    itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));

	    itempf.setItemtabl(t5687);
	    t5687List = itemDAO.findByItemDates(itempf);
		
		if(t5687List.size() == 0) {
	    	scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itempf);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687List.get(0).getGenarea()));
		}		
		
		itempf = new Itempf();    
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(),t6598,t5687rec.svMethod.toString());		
		
		if (itempf == null) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		
		
		
		if (isNE(t5687rec.loanmeth,SPACES)) {			
			itempf = new Itempf();	    
		    itempf.setItempfx("IT");
		    itempf.setItemcoy(wsspcomn.company.toString());
		    itempf.setItemitem(t5687rec.loanmeth.toString());
		    itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
		    itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));

		    itempf.setItemtabl(t6632);
		    t6632List = itemDAO.findByItemDates(itempf);
			
			if(t6632List.size() == 0) {
				t6632rec.t6632Rec.set(SPACES);
			}
			else {
				t6632rec.t6632Rec.set(StringUtil.rawToString(t6632List.get(0).getGenarea()));
			}							
		}		
	}

protected void callSurMethodWhole1600()
{	
	srcalcpy.effdate.set(wsaaToday);
	srcalcpy.type.set("F");
	/* IF COVRCLM-INSTPREM         > ZERO                           */
	srcalcpy.singp.set(covrpf.getInstprem());
	/* ELSE                                                         */
	/*    MOVE COVRCLM-SINGP       TO SURC-SINGP.                   */
	/* if no surrender method found print zeros as surrender value*/
	if (isEQ(t6598rec.calcprog, SPACES)) {		
		srcalcpy.status.set(varcom.endp);
		return;
		/*     GO TO 1630-ADD-TO-SUBFILE.                               */		
	}
	
	/*IVE-797 RUL Product - Full Surrender Calculation started*/
	//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))  
	{
		callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
	}
	else
	{
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
		Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

		vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
		vpxsurcrec.function.set("INIT");
		callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
		srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
		vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
		vpmfmtrec.initialize();
		vpmfmtrec.amount02.set(wsaaEstimateTot);			

		callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrpf);//VPMS call
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		
		if(isEQ(srcalcpy.type,"L"))
		{
			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
			callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			srcalcpy.status.set(varcom.endp);
		}
		else if(isEQ(srcalcpy.type,"C"))
		{
			srcalcpy.status.set(varcom.endp);
		}
		else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
		{
			srcalcpy.endf.set("Y");
		}
		else
		{
			srcalcpy.status.set(varcom.oK);
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(vpxsurcrec.statuz.equals(varcom.endp))
			srcalcpy.status.set(varcom.endp);
		/* ILIFE-3142 End*/
	}
	
	/*IVE-797 RUL Product - Full Surrender Calculation end*/
	if (isEQ(srcalcpy.status, varcom.bomb)) {
		syserrrec.statuz.set(srcalcpy.status);
		fatalError600();
	}
	if (isNE(srcalcpy.status, varcom.oK)
	&& isNE(srcalcpy.status, varcom.endp)
	&& isNE(srcalcpy.status, "NOPR") && isNE(srcalcpy.status, varcom.mrnf)) {
		syserrrec.statuz.set(srcalcpy.status);
		fatalError600();
	}
	if (isEQ(srcalcpy.status, "NOPR")) {
		wsaaNoPrice = "Y";
		return;
	}
	if (isEQ(srcalcpy.status, varcom.oK)
	|| isEQ(srcalcpy.status, varcom.endp)) {
		if (isEQ(srcalcpy.currcode, SPACES)) {
			srcalcpy.currcode.set(chdrpf.getCntcurr());
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		zrdecplrec.currency.set(srcalcpy.currcode);
		callRounding6000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
		zrdecplrec.currency.set(srcalcpy.currcode);
		callRounding6000();
		srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
	}
	if (isEQ(srcalcpy.type, "C")) {
		compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
	}
	if (isEQ(srcalcpy.type, "C")
			|| isEQ(srcalcpy.type, "J")) {
		if (isNE(tr52drec.txcode, SPACES)) {
				callTaxsubr();
		}
	}
	/* Check the amount returned for being negative. In the case of    */
	/* SUM products this is possible and so set these values to zero.  */
	/* Note SUM products do not have to have the same PT & BT dates.   */
	checkT56112700();
	if (isNE(wsaaSumFlag, SPACES)) {		
		if (isLT(srcalcpy.actualVal, ZERO)) {
			srcalcpy.actualVal.set(ZERO);
		}
	}
	/*    IF SURC-ENDF  = 'Y'                                          */
	/*       GO TO 1640-EXIT.                                          */
	if (isEQ(srcalcpy.estimatedVal, ZERO)
	&& isEQ(srcalcpy.actualVal, ZERO)) {
		return;
	}	
	
	/*  IF S5026-SHORTDS         NOT = COVRCLM-CRTABLE       <A06843>*/
	/*      MOVE SPACES             TO S5026-RIIND           <A06843>*/
	/*  END-IF.                                              <A06843>*/
	/*  If the description is 'PENALTY', as set up in subroutine       */
	/*  UNLSURC then subract this amount to give a true actual value.  */
	/* IF FIRST-TIME                                                */
	/*    MOVE 0                   TO WSAA-SWITCH2                  */
	/*    MOVE S5026-CNSTCUR       TO WSAA-STORED-CURRENCY          */
	/*    ADD SURC-ESTIMATED-VAL   TO  WSAA-ESTIMATE-TOT            */
	/*    ADD SURC-ACTUAL-VAL      TO  WSAA-ACTUAL-TOT              */
	/* ELSE                                                         */
	/*    IF SURC-CURRCODE            = WSAA-STORED-CURRENCY           */
	/* IF S5026-CNSTCUR            = WSAA-STORED-CURRENCY      <010>*/
	/*    ADD SURC-ESTIMATED-VAL   TO  WSAA-ESTIMATE-TOT            */
	/*    ADD SURC-ACTUAL-VAL      TO  WSAA-ACTUAL-TOT              */
	/* ELSE                                                         */
	/*    MOVE ZEROES              TO WSAA-ESTIMATE-TOT             */
	/*                                WSAA-ACTUAL-TOT               */
	/*      MOVE 1                 TO WSAA-CURRENCY-SWITCH.         */
	if (firstTime.isTrue()) {
		wsaaSwitch2.set(0);	
	}
	compute(wsaaEstimateTot, 2).set(add(wsaaEstimateTot, srcalcpy.estimatedVal));
	/*    IF SURC-DESCRIPTION      = 'PENALTY'                 <CAS1*/
	if (isEQ(srcalcpy.type, "C")) {
		compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
		compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
	}
	else {
		compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
	}
	
	/* Add values returned to our running Surrender value total*/
	
	wsaaComponentTot.add(srcalcpy.actualVal);	
	wsaaComponentTot.add(srcalcpy.estimatedVal);
	/* Check the effective date of the surrender against the paid to   */
	/* date of the contract. If there are to be premiums paid or       */
	/* refunded then call the appropriate subroutine held on T5611.    */
	/* Note that if the item does not exist then the coverage does not */
	/* use the SUM calculation package & is hence not applicable....   */
	if (isNE(srcalcpy.effdate, srcalcpy.ptdate)
	&& isNE(wsaaSumFlag, SPACES)
	&& isNE(t5611rec.calcprog, SPACES)) {
		checkPremadj2600();
	}
}
protected void callTaxsubr()
{	
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrpf.getCnttype());
	wsaaTr52eCrtable.set(covrpf.getCrtable());
	readTr52e5300();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e5300();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		readTr52e5300();
	}
	/* IF TR52E-TAXIND-05          NOT = 'Y'                        */
	if (isNE(tr52erec.taxind03, "Y")) {
		return ;
		/****     GO TO 5190-EXIT                                   <S19FIX>*/
	}
	/* Call tax subroutine                                             */
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(varcom.oK);
	txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
	txcalcrec.chdrnum.set(chdrpf.getChdrnum());
	txcalcrec.life.set(covrpf.getLife());
	txcalcrec.coverage.set(covrpf.getCoverage());
	txcalcrec.rider.set(covrpf.getRider());
	txcalcrec.crtable.set(covrpf.getCrtable());
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.cnttype.set(chdrpf.getCnttype());
	txcalcrec.txcode.set(tr52drec.txcode);
	txcalcrec.register.set(chdrpf.getReg());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrpf.getCntcurr());
	wsaaCntCurr.set(chdrpf.getCntcurr());
	
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.cntTaxInd.set(SPACES);
	/*   MOVE WSAA-TOTAL-FEE         TO TXCL-AMOUNT-IN.               */
	txcalcrec.amountIn.set(srcalcpy.actualVal);
	txcalcrec.transType.set("SURF");
	txcalcrec.effdate.set(wsaaToday);
	txcalcrec.tranno.set(ZERO);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaFeeTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaFeeTax.add(txcalcrec.taxAmt[2]);
		}
	}	
}

protected void readTr52e5300()
{	
	itdmIO.setDataArea(SPACES);
	tr52erec.tr52eRec.set(SPACES);
	itdmIO.setItemcoy(chdrpf.getChdrcoy());
	itdmIO.setItemtabl(tr52e);
	itdmIO.setItemitem(wsaaTr52eKey);
	itdmIO.setItmfrm(wsaaToday);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if ((isNE(itdmIO.getStatuz(), varcom.oK))
	&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
		syserrrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(itdmIO.getParams());
		fatalError600();
	}
	if (((isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy()))
	|| (isNE(itdmIO.getItemtabl(), tr52e))
	|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
	|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
	&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
		syserrrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(wsaaTr52eKey);
		fatalError600();
	}
	if (((isEQ(itdmIO.getItemcoy(), chdrpf.getChdrcoy()))
	&& (isEQ(itdmIO.getItemtabl(), tr52e))
	&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
	&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
		tr52erec.tr52eRec.set(itdmIO.getGenarea());
	}
}

protected void checkPremadj2600()
{
	
	/* Call the surrender routine for premium adjustments. Note that   */
	/* for the moment this uses the same copy-book as the surrender    */
	/* routine, which is stored and then re-instated .........         */
	wsaaSurrenderRecStore.set(srcalcpy.surrenderRec);
	/* MOVE CHDRSUR-BILLFREQ         TO WSAA-BILLFREQ.      <A05691>*/
	/* If this is a Single Premium Component then use a Billing        */
	/* frequency of '00'.                                              */
	if (isEQ(t5687rec.singlePremInd, "Y")) {
		srcalcpy.billfreq.set("00");
	}
	else {
		srcalcpy.billfreq.set(payrpf.getBillfreq());
	}
	srcalcpy.estimatedVal.set(covrpf.getInstprem());
	srcalcpy.actualVal.set(ZERO);
	callProgram(t5611rec.calcprog, srcalcpy.surrenderRec);
	if (isEQ(srcalcpy.status, varcom.bomb)) {
		syserrrec.params.set(srcalcpy.surrenderRec);
		syserrrec.statuz.set(srcalcpy.status);
		fatalError600();
	}
	else {
		if (isNE(srcalcpy.status, varcom.mrnf) && isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
	}
	zrdecplrec.amountIn.set(srcalcpy.actualVal);
	zrdecplrec.currency.set(covrpf.getPremCurrency());
	callRounding6000();
	srcalcpy.actualVal.set(zrdecplrec.amountOut);
	/* Note adjustments are subtracted from the total.                 */
	/* The actual value here is the premium adjustment returned...     */
	wsaaOtheradjst.add(srcalcpy.actualVal);
	srcalcpy.surrenderRec.set(wsaaSurrenderRecStore);
}

protected void checkT56112700()
{	
	/* Read the Coverage Surrender/Paid-Up/Bonus parameter table.      */
	itempf = new Itempf();	    
    itempf.setItempfx("IT");
    itempf.setItemcoy(wsspcomn.company.toString());
    itempf.setItemitem(wsaaT5611Item.toString());
    wsaaT5611Crtable.set(srcalcpy.crtable);
	wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
    itempf.setItmfrm(new BigDecimal(srcalcpy.effdate.toInt()));
    itempf.setItmto(new BigDecimal(srcalcpy.effdate.toInt()));

    itempf.setItemtabl(t5611);
    t5611List = itemDAO.findByItemDates(itempf);
	
	if(t5611List.size() == 0) {
		wsaaSumFlag.set(SPACES);
	}
	else {
		t5611rec.t5611Rec.set(StringUtil.rawToString(t5611List.get(0).getGenarea()));
		wsaaSumFlag.set("Y");
	}	
}

protected void calcSurr() {	
	/*  Computation of tax amount to be imposed.                      */
	
	
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("SURTAX") && er.isExternalized(sv.cnttype.toString(), covrpf.getCrtable())))//ILIFE-4833 /* IJTI-1479 */  
	{
		getTaxAmount1880();		
	}
	else
	{
		SurtaxRec surtaxRec = new SurtaxRec();	
		surtaxRec.cnttype.set(chdrpf.getCnttype());
		surtaxRec.cntcurr.set(chdrpf.getCntcurr());
		surtaxRec.effectiveDate.set(wsaaToday);
		surtaxRec.occDate.set(chdrpf.getOccdate());
		surtaxRec.actualAmount.set(wsaaActualTot);						
		callProgram("SURTAX", surtaxRec.surtaxRec);			
		wsaaTaxAmt.set(surtaxRec.taxAmount);
	}
	wsaaTaxAmt.add(wsaaFeeTax);
	/*ILIFE-2397 End */
		/*IVE-705 Surrender Tax Calc end*/
		
	
	
	/*  Get the value of any Loans held against this component.        */
	getLoanDetails1900();
	getPolicyDebt1990();	
	
	/*         COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT +               */
	/*            S5026-POLICYLOAN                                     */
	/*      COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT -               */
	/*         S5026-POLICYLOAN                                     */
	compute(sv.surrval, 2).set(sub(sub(add(sub(wsaaActualTot, wsaapolicyloan), wsaaZrcshamt), wsaaTdbtamt), wsaaTaxAmt));	
	if(isLT(sv.surrval,ZERO)){
		sv.surrval.set(mult(sv.surrval, (-1)));
	}
			
	/* In the case of SUM products with premium adjustments add the    */
	/* premium adjustments to the total value displayed on screen...   */
	if (isNE(wsaaSumFlag, SPACES)
	&& isNE(wsaaOtheradjst, ZERO)) {
		sv.surrval.add(wsaaOtheradjst);		
	}
	/*ILIFE-6277 SA*/
	if(isGT(wsaaEstimateTot,sv.surrval)) // ILIFE-6831
	{
	sv.surrval.set(sub(wsaaEstimateTot,sv.surrval));
	/*         MOVE CHDRSUR-CNTCURR   TO S5026-CURRCD.                 */
	}	
	wsaaTotamnt.add(sv.surrval);
	
}

protected void readTr52d5120()
{	
	itempf = itemDAO.findItemByItem(wsspcomn.company.toString(),tr52d,chdrpf.getReg());	
	if (itempf == null) {		
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(),tr52d,"***");
		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(itempf);
			fatalError600();
		}
	}
	tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
	
}

protected void getTaxAmount1880()
{	
	
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(chdrpf.getCnttype());
	stringVariable1.addExpression(chdrpf.getCntcurr());
	itempf = itemDAO.findItemByItem(wsspcomn.company.toString(),tr691,stringVariable1.toString());		
	
	if (itempf == null) {
		StringUtil stringVariable3 = new StringUtil();
		stringVariable3.addExpression("***");
		stringVariable3.addExpression(chdrpf.getCntcurr());
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(),tr691,stringVariable3.toString());	
		if (itempf == null) {
			return ;
		}
	}
	tr691rec.tr691Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	datcon3rec.intDate2.set(wsaaToday);
	datcon3rec.intDate1.set(chdrpf.getOccdate());
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isGTE(datcon3rec.freqFactor, tr691rec.tyearno)) {
		return ;
	}
	if (isNE(tr691rec.pcnt, 0)) {
		//MIBT-111
//		compute(wsaaTaxAmt, 3).setRounded(mult(wsaaActualTot, (div(tr691rec.pcnt, 100))));
		compute(wsaaTaxAmt, 3).setRounded(div(mult(wsaaActualTot,tr691rec.pcnt), 100));
	}
	if (isNE(tr691rec.flatrate, 0)) {
		wsaaTaxAmt.set(tr691rec.flatrate);
	}
	zrdecplrec.amountIn.set(wsaaTaxAmt);
	zrdecplrec.currency.set(chdrpf.getCntcurr());	
	callRounding6000();
	wsaaTaxAmt.set(zrdecplrec.amountOut);
}

	protected void addsflData(){
		scrnparams.function.set(varcom.sadd);
		processScreen("Sd5e1", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
		}
	}

	protected void getLoanDetails1900()
	{
		
	/**
	* <pre>
	*  Get the details of all loans currently held against this       
	*  Contract. If this is not the first component within the        
	*  current Surrender transaction then need to read the SURD       
	*  to get details of all previous surrender records for this      
	*  transaction.                                                   
	*  If there are no records (there will be none for Unit Linked),  
	*  then call TOTLOAN to get the current loan value. If there are  
	*  records then sum up their values before calling TOTLOAN and    
	*  subtracting the total value from the LOAN VALUE returned from  
	*  TOTLOAN.                                                       
	*  Previous surrenders may have been done in a different          
	*  currency to the present one and hence, a check should be       
	*  made for this and a conversion done where necessary before     
	*  the accumulation is done. Note that for the initial screen     
	*  display, the currency will always be CHDR currency.            
	*  Note also that TOTLOAN always returns details in the CHDR      
	*  currency.                                                      
	* </pre>
	*/

		surdclmIO.setDataArea(SPACES);
		surdclmIO.setChdrcoy(chdrpf.getChdrcoy());
		surdclmIO.setChdrnum(chdrpf.getChdrnum());
		surdclmIO.setLife(ZERO);
		surdclmIO.setCoverage(ZERO);
		surdclmIO.setRider(ZERO);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setTranno(chdrpf.getTranno());
		surdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		surdclmIO.setFormat(surdclmrec);
		surdclmIO.setStatuz(varcom.oK);
		wsaaLoanValue.set(0);
		/*  Read all SURD records currently unprocessed.                   */
		while ( !(isEQ(surdclmIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, surdclmIO);
			if (isNE(surdclmIO.getStatuz(), varcom.oK)
			&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(surdclmIO.getParams());
				fatalError600();
			}
			if (isNE(chdrpf.getChdrnum(), surdclmIO.getChdrnum())
			|| isNE(chdrpf.getChdrcoy(), surdclmIO.getChdrcoy())
			|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
				surdclmIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(surdclmIO.getCurrcd(), chdrpf.getCntcurr())) {
					readjustSurd1920();
				}
				else {
					wsaaActvalue.set(surdclmIO.getActvalue());
				}
				wsaaLoanValue.add(wsaaActvalue);
			}
			surdclmIO.setFunction(varcom.nextr);
		}		
		
		/*  Read TOTLOAN to get value of loans for this contract.          */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(wsaaToday);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		/* CALL 'TOTLOAN' USING TOTL-TOTLOAN-REC.                       */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		compute(wsaaHeldCurrLoans1, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isLT(wsaaLoanValue, wsaaHeldCurrLoans)) {
			wsaaHeldCurrLoans.subtract(wsaaLoanValue);
		}
		else {
			wsaaHeldCurrLoans.set(0);
		}
		
		
		wsaapolicyloan.set(totloanrec.principal);
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(wsaaToday);
		totloanrec.function.set("CASH");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaZrcshamt, 2).set(add(totloanrec.principal, totloanrec.interest));
		/* Change the sign to reflect the Client's context.             */
		if (isLT(wsaaZrcshamt, 0)) {
			compute(wsaaZrcshamt, 2).set(mult(wsaaZrcshamt, (-1)));
		}
	}

protected void readjustSurd1920()
	{		
		/*  Convert the SURD into the CHDR currency.                       */
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(surdclmIO.getCurrcd());
		/* MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.        <LA4958>*/
		conlinkrec.cashdate.set(wsaaToday);
		conlinkrec.currOut.set(chdrpf.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(surdclmIO.getActvalue());
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		wsaaActvalue.set(conlinkrec.amountOut);
	}

protected void getPolicyDebt1990()
{
	wsaaTdbtamt.set(ZERO);
	tpoldbtIO.setRecKeyData(SPACES);
	tpoldbtIO.setChdrcoy(chdrpf.getChdrcoy());
	tpoldbtIO.setChdrnum(chdrpf.getChdrnum());
	tpoldbtIO.setTranno(ZERO);
	tpoldbtIO.setFormat(tpoldbtrec);
	tpoldbtIO.setFunction(varcom.begn);
	//performance improvement --  Niharika Modi 
	tpoldbtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	tpoldbtIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	read1991();
}


protected void read1991()
{
	SmartFileCode.execute(appVars, tpoldbtIO);
	if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
	&& isNE(tpoldbtIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(tpoldbtIO.getParams());
		fatalError600();
	}
	if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
	|| isNE(tpoldbtIO.getChdrcoy(), chdrpf.getChdrcoy())
	|| isNE(tpoldbtIO.getChdrnum(), chdrpf.getChdrnum())) {
		return ;
	}
	wsaaTdbtamt.add(tpoldbtIO.getTdbtamt());
	tpoldbtIO.setFunction(varcom.nextr);
	read1991();
	return ;
}
	protected void readHPAD(){
		
		hpadpf=hpadpfDAO.getHpadData(wsspcomn.company.toString(), chdrpf.getChdrnum());		
		sv.hissdte.set(hpadpf.getHissdte());
	}
	
	protected void readACBL(){		
		acblList=acblDao.getAcblenqRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(),"LP","S");	
		for(Acblpf acbl:acblList) {		
			if(suspenseFlag){
			acblpf = acbl;				
			sv.sacscurbal.set(acblpf.getSacscurbal());
			if(isLT(sv.sacscurbal,ZERO))
			sv.sacscurbal.set(mult(sv.sacscurbal, (-1)));
			}
			suspenseFlag= false;
		}				
	}
	

	
		

	/**
	* <pre>	                                                     
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		if (CTENQ003Permission) {
			sv.survalindOut[varcom.nd.toInt()].set("N");
			sv.loanvalindOut[varcom.nd.toInt()].set("N");
			sv.bonusvalindOut[varcom.nd.toInt()].set("N");
		}
		else {
			sv.survalindOut[varcom.nd.toInt()].set("Y");
			sv.loanvalindOut[varcom.nd.toInt()].set("Y");
			sv.bonusvalindOut[varcom.nd.toInt()].set("Y");
		}
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO1*/
		/*    CALL 'SD5E3IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6235-DATA-AREA                         */
		/*                         S6235-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		para4000();				
	}

protected void para4000()
	{
		if (CTENQ003Permission) {
			/* get in full surrender by certain links*/
			validateKeys2200();
			chkValidAction2960();
			if (isEQ(sv.survalind, "X")||isEQ(sv.loanvalind,"X")||isEQ(sv.bonusvalind,"X")) {
				totalValLink4a00();
				return;
			}
			else {
				if ((isEQ(sv.survalind,"?")||isEQ(sv.loanvalind,"?")||isEQ(sv.bonusvalind,"?"))
						&&isNE(wsspcomn.secProg[2],SPACES) && isNE(wsspcomn.secProg[2],"COMIT")) {
					totalValLinkRec4a50();
					return ;
				}
			}
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			bypassStart();
			return;
		}
		/*    Re-start the subfile.*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("SD5E3", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		bypassStart();
	}

protected void bypassStart()
	{
		
		/*  Nothing pressed at all, end working*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		/* All requests services,*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			return;
		}
		
		nextProgram();
	}

	
protected void nextProgram()
	{		
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("SD5E3", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callRounding6000()
{
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(wsspcomn.company);
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(chdrpf.getCntcurr());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		fatalError600();
	}
	/*EXIT*/
}

	protected void suminwithTax() {
		/* Read table TR52E */
		itempf = new Itempf();  //ILIFE-7892
		wsaaTaxamt.set(ZERO);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		itempf.setItemitem((wsaaTr52eKey).toString());
		itempfList = itemDAO.getAllitemsbyCurrency("IT", (wsspcomn.company).toString(), (tr52e), /* IJTI-1479 */
				(wsaaTr52eKey).toString());
		if (itempfList == null || (itempfList != null && itempfList.size() == 0)) {
			itempf.setItempfx("IT");
			itempf.setItemcoy((wsspcomn.company).toString());
			itempf.setItemtabl((tr52e)); /* IJTI-1479 */
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			itempf.setItemitem((wsaaTr52eKey).toString());
		}
		itempfList = itemDAO.getAllitemsbyCurrency("IT", (wsspcomn.company).toString(), (tr52e), /* IJTI-1479 */
				wsaaTr52eKey.toString());
		if (itempfList == null || (itempfList != null && itempfList.size() == 0)) {
			itempf.setItempfx("IT");
			itempf.setItemcoy((wsspcomn.company).toString());
			itempf.setItemtabl((tr52e)); /* IJTI-1479 */
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itempf.setItemitem((wsaaTr52eKey).toString());
			itempfList = itemDAO.getAllitemsbyCurrency("IT", (wsspcomn.company).toString(), (tr52e), /* IJTI-1479 */
					wsaaTr52eKey.toString());
			if (itempfList == null || (itempfList != null && itempfList.size() == 0)) {
				return;
			}
		}

		tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));

		/* Call TR52D tax subroutine */
		if (isEQ(tr52erec.taxind01, "Y")) {
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covrpf.getLife());
			txcalcrec.coverage.set(covrpf.getCoverage());
			txcalcrec.rider.set(covrpf.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covrpf.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			/* We should calculate the tax on the regular premium only if */
			/* we have a regular premium component. If we have a single prem */
			/* component then we calculate the tax on the single premium amt */
			if (isEQ(covrpf.getInstprem(), 0)) {
				/* this means we have single premium component */
				txcalcrec.amountIn.set(covrpf.getSingp());
			} else {
				/* this means we have a regular premium component */
				if (isEQ(tr52erec.zbastyp, "Y")) {
					compute(txcalcrec.amountIn, 3).setRounded(sub(covrpf.getInstprem(), covrpf.getZlinstprem()));
				} else {
					txcalcrec.amountIn.set(sv.instPrem);

				}
			}
			txcalcrec.transType.set("PREM");
			if (isEQ(chdrpf.getBillfreq(), "00")) {
				txcalcrec.effdate.set(covrpf.getCrrcd());
			} else {
				txcalcrec.effdate.set(chdrpf.getPtdate());
			}
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			txcalcrec.txcode.set(tr52drec.txcode);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO) || isGT(txcalcrec.taxAmt[2], ZERO)) {				
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.instPrem.add(wsaaTaxamt);
			}
		}
	}

	protected void totalValLink4a00()
	{
		start4a00();
	}

	protected void keepsCovrmjaIO(){
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(lifepf.getLife());
		covrmjaIO.setCoverage("00");
		covrmjaIO.setRider("00");
		covrmjaIO.setPlanSuffix(0);
		covrmjaIO.setFunction(varcom.begn);
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRNUM","CHDRCOY","LIFE");
		SmartFileCode.execute(appVars, covrmjaIO);
		covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
	}

	protected void start4a00()
	{
        wsspcomn.currfrom.set(wsaaToday);

        covrsurIO.setFunction(varcom.keeps);
        SmartFileCode.execute(appVars, covrsurIO);
        chdrsurIO.setFunction(varcom.keeps);
        SmartFileCode.execute(appVars, chdrsurIO);
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		keepsCovrmjaIO();
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/* Save the next 8 programs from the program stack.*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}

		if (isEQ(sv.survalind, "X")) {
			sv.survalind.set("?");
			gensswrec.function.set("A");
		}
		if (isEQ(sv.loanvalind, "X")) {
			sv.loanvalind.set("?");
			gensswrec.function.set("B");
		}
		if (isEQ(sv.bonusvalind, "X")) {
			sv.bonusvalind.set("?");
			gensswrec.function.set("C");
		}

		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}

		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	protected void totalValLinkRec4a50(){

		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}

		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/* Blank out the stack  action".                                   */
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
            wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
            wsspcomn.nextprog.set(scrnparams.scrname);
        }else {
        	wsspcomn.programPtr.add(1);
        }
		/* Set WSSP-NEXTPROG to the current screen name (thus returning    */
		/*   returning to re-display the screen).                          */
		
	}

	protected void gensww4210()
	{
		para4211();
	}

	protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
				&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.params.set(gensswrec.gensswRec);
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			//scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

	protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

	protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

	protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

    protected void validateKeys2200()
    {
        chdrsurIO.setChdrcoy(wsspcomn.company);
        chdrsurIO.setChdrnum(chdrpf.getChdrnum());
        chdrsurIO.setFunction(varcom.readr);
        if (isNE(chdrpf.getChdrnum(), SPACES)) {
            SmartFileCode.execute(appVars, chdrsurIO);
        }
        else {
            chdrsurIO.setStatuz(varcom.mrnf);
        }
        if (isNE(chdrsurIO.getStatuz(), varcom.oK)
                && isNE(chdrsurIO.getStatuz(), varcom.mrnf)) {
            syserrrec.params.set(chdrsurIO.getParams());
            fatalError600();
        }
    }

    protected void chkValidAction2960()
    {
        /*INIT*/
        if (isEQ(sv.survalind, "X")) {
            chkValidSurr2970();
        }
        if (isEQ(sv.bonusvalind, "X")) {
            wsaaBsurrOk = "N";
            chkValidBsurr2980();
        }
        /*EXIT*/
    }

    protected void chkValidSurr2970()
    {
		readCovrsur2981();
		loopCovrsur2985();
		readT56872976();
    }

	protected void chkValidBsurr2980()
	{
		readCovrsur2981();
		loopCovrsur2985();
		readT66402986();
	}

	protected void readCovrsur2981()
	{
		covrsurIO.setParams(SPACES);
		covrsurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setPlanSuffix(ZERO);
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setFunction(varcom.begn);
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

	protected void loopCovrsur2985()
	{
		SmartFileCode.execute(appVars, covrsurIO);
        covrsurIO.setFunction(varcom.keeps);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
				&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrsurIO.getChdrcoy())
				|| isNE(covrsurIO.getChdrnum(), chdrsurIO.getChdrnum())
				|| isEQ(covrsurIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2989);
		}
	}

	protected void readT56872976()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
				&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
				|| isNE(itdmIO.getItemtabl(), t5687)
				|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
				|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrsurIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.f294);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isNE(t5687rec.svMethod, SPACES)) {
			return ;
		}
		covrsurIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loopCovrsur2975);
	}

	protected void readT66402986()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
				&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
				|| isNE(itdmIO.getItemtabl(), t6640)
				|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
				|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/* No need to give an error if no item found on this table,        */
			/* Traditional Components only exist on this table.                */
			return ;
		}
		t6640rec.t6640Rec.set(itdmIO.getGenarea());
		if (isNE(t6640rec.surrenderBonusMethod, SPACES)) {
			wsaaBsurrOk = "Y";
			return ;
		}
		covrsurIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loopCovrsur2985);
	}

private static final class ErrorsInner { 
	/* ERRORS */
private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
private FixedLengthStringData g094 = new FixedLengthStringData(4).init("G094");
private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
private FixedLengthStringData g588 = new FixedLengthStringData(4).init("G588");
}


}
