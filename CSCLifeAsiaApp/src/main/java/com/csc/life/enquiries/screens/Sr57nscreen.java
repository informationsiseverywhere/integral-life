package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;


public class Sr57nscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {18, 22, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57nScreenVars sv = (Sr57nScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr57nscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr57nScreenVars screenVars = (Sr57nScreenVars)pv;
		screenVars.effdateDisp.setClassString("");
		screenVars.clamant.setClassString("");
		screenVars.estimateTotalValue.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.totalfee.setClassString("");
		screenVars.totalamt.setClassString("");
		screenVars.prcnt.setClassString("");
		screenVars.taxamt.setClassString("");
	}

/**
 * Clear all the variables in Sr57nscreen
 */
	public static void clear(VarModel pv) {
		Sr57nScreenVars screenVars = (Sr57nScreenVars) pv;
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.clamant.clear();
		screenVars.estimateTotalValue.clear();
		screenVars.currcd.clear();
		screenVars.totalfee.clear();
		screenVars.totalamt.clear();
		screenVars.prcnt.clear();
		screenVars.taxamt.clear();
	}
}
