package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6363
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6363ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData cltdob = DD.cltdob.copyToZonedDecimal().isAPartOf(dataFields,11);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData cltsex = DD.cltsex.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData clttwo = DD.clttwo.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData givname = DD.givname.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData role = DD.role.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData submnuprog = DD.submnuprog.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData surname = DD.surname.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cltdobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cltpcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cltsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData clttwoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData givnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData roleErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData submnuprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData surnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cltdobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cltpcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cltsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] clttwoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] givnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] roleOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] submnuprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] surnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData cltdobDisp = new FixedLengthStringData(10);

	public LongData S6363screenWritten = new LongData(0);
	public LongData S6363protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6363ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrselOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clttwoOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(surnameOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(givnameOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(roleOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltdobOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltsexOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltpcodeOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		//screenFields = new BaseData[] {submnuprog, action, chdrsel, clttwo, surname, givname, role, cltdob, cltsex, cltpcode};
		screenFields=getscreenFields();
		//screenOutFields = new BaseData[][] {submnuprogOut, actionOut, chdrselOut, clttwoOut, surnameOut, givnameOut, roleOut, cltdobOut, cltsexOut, cltpcodeOut};
		screenOutFields=getscreenOutFields();
		//screenErrFields = new BaseData[] {submnuprogErr, actionErr, chdrselErr, clttwoErr, surnameErr, givnameErr, roleErr, cltdobErr, cltsexErr, cltpcodeErr};
		screenErrFields=getscreenErrFields();
		/*screenDateFields = new BaseData[] {cltdob};
		screenDateErrFields = new BaseData[] {cltdobErr};
		screenDateDispFields = new BaseData[] {cltdobDisp};*/
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields(); 
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S6363screen.class;
		protectRecord = S6363protect.class;
	}
	
	
	public BaseData[] getscreenFields() {
		return new BaseData[] {submnuprog, action, chdrsel, clttwo, surname, givname, role, cltdob, cltsex, cltpcode};
	}

   public BaseData[][] getscreenOutFields() {
		return new BaseData[][] {submnuprogOut, actionOut, chdrselOut, clttwoOut, surnameOut, givnameOut, roleOut, cltdobOut, cltsexOut, cltpcodeOut};
	}

	public BaseData[] getscreenErrFields() {
		return new BaseData[] {submnuprogErr, actionErr, chdrselErr, clttwoErr, surnameErr, givnameErr, roleErr, cltdobErr, cltsexErr, cltpcodeErr}; 
	}

	public BaseData[] getscreenDateFields() {
		return new BaseData[] {cltdob};
	}

	public BaseData[] getscreenDateDispFields() {
		return new BaseData[] {cltdobErr};
	}

	public BaseData[] getscreenDateErrFields() {
		return new BaseData[] {cltdobDisp};
	}

	public int getDataAreaSize(){
		return  255;
	}
	
	public int getDataFieldsSize(){
		return 95;
	}
	
	public int getErrorIndicatorSize(){
		return  40;
	}
	
	public int getOutputFieldSize(){
		return 120;  
	}
	






	
	
}
