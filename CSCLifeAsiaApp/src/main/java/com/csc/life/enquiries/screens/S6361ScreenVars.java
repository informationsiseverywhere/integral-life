package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6361
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6361ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(3);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(dataFields, 0, FILLER);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 1);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(errorIndicators, 0, FILLER);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 2);
	public FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(outputIndicators, 0, FILLER);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(244);/*pmujavadiya ILIFE-3212*/
	public FixedLengthStringData subfileFields = new FixedLengthStringData(193).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clntdets = DD.clntdets.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData clntdetsEx = DD.clntdets.copy().isAPartOf(subfileFields,96);/*pmujavadiya ILIFE-3212*/
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,192);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(12).isAPartOf(subfileArea, 193);/*pmujavadiya ILIFE-3212*/
	public FixedLengthStringData clntdetsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clntdetsExErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 205);/*pmujavadiya ILIFE-3212*/
	public FixedLengthStringData[] clntdetsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clntdetsExOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 201);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6361screensflWritten = new LongData(0);
	public LongData S6361screenctlWritten = new LongData(0);
	public LongData S6361screenWritten = new LongData(0);
	public LongData S6361protectWritten = new LongData(0);
	public GeneralTable s6361screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6361screensfl;
	}

	public S6361ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","03","-01","03",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, clntdets, clntdetsEx};
		screenSflOutFields = new BaseData[][] {selectOut, clntdetsOut, clntdetsExOut};
		screenSflErrFields = new BaseData[] {selectErr, clntdetsErr, clntdetsExErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {};
		screenOutFields = new BaseData[][] {};
		screenErrFields = new BaseData[] {};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6361screen.class;
		screenSflRecord = S6361screensfl.class;
		screenCtlRecord = S6361screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6361protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6361screenctl.lrec.pageSubfile);
	}
}
