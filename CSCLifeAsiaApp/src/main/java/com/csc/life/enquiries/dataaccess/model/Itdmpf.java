package com.csc.life.enquiries.dataaccess.model;

import java.util.Arrays;

public class Itdmpf {
	
	private long	unique_number;
	private String	itempfx;
	private String	itemcoy;
	private String	itemtabl;
	private String	itemitem;
	private String	itemseq;
	private String	tranid;
	private String	tableprog;
	private String	validflag;
	private int	itmfrm;
	private int	itmto;
	private byte[] genarea;

	private String	usrprf;
	private String	jobnm;
	private String	datime;
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getItempfx() {
		return itempfx;
	}
	public void setItempfx(String itempfx) {
		this.itempfx = itempfx;
	}
	public String getItemcoy() {
		return itemcoy;
	}
	public void setItemcoy(String itemcoy) {
		this.itemcoy = itemcoy;
	}
	public String getItemtabl() {
		return itemtabl;
	}
	public void setItemtabl(String itemtabl) {
		this.itemtabl = itemtabl;
	}
	public String getItemitem() {
		return itemitem;
	}
	public void setItemitem(String itemitem) {
		this.itemitem = itemitem;
	}
	public String getItemseq() {
		return itemseq;
	}
	public void setItemseq(String itemseq) {
		this.itemseq = itemseq;
	}
	public String getTranid() {
		return tranid;
	}
	public void setTranid(String tranid) {
		this.tranid = tranid;
	}
	public String getTableprog() {
		return tableprog;
	}
	public void setTableprog(String tableprog) {
		this.tableprog = tableprog;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getItmfrm() {
		return itmfrm;
	}
	public void setItmfrm(int itmfrm) {
		this.itmfrm = itmfrm;
	}
	public int getItmto() {
		return itmto;
	}
	public void setItmto(int itmto) {
		this.itmto = itmto;
	}
	public byte[] getGenarea() {
		return Arrays.copyOf(this.genarea, this.genarea.length);//IJTI-316
	}
	public void setGenarea(byte[] genarea) {
		this.genarea = genarea.clone();//IJTI-314
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}


}
