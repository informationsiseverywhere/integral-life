package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50Q
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50qScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(748);
	public FixedLengthStringData dataFields = new FixedLengthStringData(348).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData currency = DD.currency.copy().isAPartOf(dataFields,61);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,64);
	public FixedLengthStringData jlifedesc = DD.jlifedesc.copy().isAPartOf(dataFields,72);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,160);
	public FixedLengthStringData optdscs = new FixedLengthStringData(120).isAPartOf(dataFields, 168);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(8, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optdsc05 = DD.optdsc.copy().isAPartOf(filler,60);
	public FixedLengthStringData optdsc06 = DD.optdsc.copy().isAPartOf(filler,75);
	public FixedLengthStringData optdsc07 = DD.optdsc.copy().isAPartOf(filler,90);
	public FixedLengthStringData optdsc08 = DD.optdsc.copy().isAPartOf(filler,105);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,288);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,298);
	public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(dataFields,301);
	public ZonedDecimalData trandate = DD.trandate.copyToZonedDecimal().isAPartOf(dataFields,305);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(dataFields,313);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(dataFields,343);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(100).isAPartOf(dataArea, 348);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currencyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(8, 4, optdscsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData optdsc05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData optdsc06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData optdsc07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData optdsc08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData trancdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData trandateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(300).isAPartOf(dataArea, 448);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currencyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(8, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(96).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] optdsc05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] optdsc06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] optdsc07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] optdsc08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] trancdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] trandateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(280);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(86).isAPartOf(subfileArea, 0);
	public FixedLengthStringData compdesc = DD.cmpntdesc.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData cmpntnum = DD.cmpntnum.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData component = DD.component.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData hcoverage = DD.hcoverage.copy().isAPartOf(subfileFields,52);
	public FixedLengthStringData hjlife = DD.hjlife.copy().isAPartOf(subfileFields,54);
	public FixedLengthStringData hlife = DD.hlife.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData hrider = DD.hrider.copy().isAPartOf(subfileFields,58);
	public ZonedDecimalData hsuffix = DD.hsuffix.copyToZonedDecimal().isAPartOf(subfileFields,60);
	public FixedLengthStringData linetype = DD.linetype.copy().isAPartOf(subfileFields,64);
	public FixedLengthStringData pstatcode = DD.premdesc.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData statcode = DD.riskdesc.copy().isAPartOf(subfileFields,75);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,85);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 86);
	public FixedLengthStringData cmpntdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cmpntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData componentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hcoverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hjlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hriderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hsuffixErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData linetypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData premdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData riskdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 134);
	public FixedLengthStringData[] cmpntdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cmpntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] componentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hcoverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hjlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hriderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hsuffixOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] linetypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] premdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] riskdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 278);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData trandateDisp = new FixedLengthStringData(10);

	public LongData Sr50qscreensflWritten = new LongData(0);
	public LongData Sr50qscreenctlWritten = new LongData(0);
	public LongData Sr50qscreenWritten = new LongData(0);
	public LongData Sr50qprotectWritten = new LongData(0);
	public GeneralTable sr50qscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50qscreensfl;
	}

	public Sr50qScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"10","12","-10","11",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {linetype, hlife, hjlife, hsuffix, hcoverage, hrider, select, component, compdesc, statcode, pstatcode, cmpntnum};
		screenSflOutFields = new BaseData[][] {linetypeOut, hlifeOut, hjlifeOut, hsuffixOut, hcoverageOut, hriderOut, selectOut, componentOut, cmpntdescOut, riskdescOut, premdescOut, cmpntnumOut};
		screenSflErrFields = new BaseData[] {linetypeErr, hlifeErr, hjlifeErr, hsuffixErr, hcoverageErr, hriderErr, selectErr, componentErr, cmpntdescErr, riskdescErr, premdescErr, cmpntnumErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, chdrstatus, lifenum, jlife, tranno, trandate, cnttype, ctypdesc, premstatus, currency, register, lifedesc, jlifedesc, trancd, trandesc, effdate, crtuser, optdsc01, optdsc05, optdsc02, optdsc06, optdsc03, optdsc04, optdsc07, optdsc08};
		screenOutFields = new BaseData[][] {chdrnumOut, chdrstatusOut, lifenumOut, jlifenumOut, trannoOut, trandateOut, cnttypeOut, ctypdescOut, premstatusOut, currencyOut, registerOut, lifedescOut, jlifedescOut, trancdOut, trandescOut, effdateOut, crtuserOut, optdsc01Out, optdsc05Out, optdsc02Out, optdsc06Out, optdsc03Out, optdsc04Out, optdsc07Out, optdsc08Out};
		screenErrFields = new BaseData[] {chdrnumErr, chdrstatusErr, lifenumErr, jlifenumErr, trannoErr, trandateErr, cnttypeErr, ctypdescErr, premstatusErr, currencyErr, registerErr, lifedescErr, jlifedescErr, trancdErr, trandescErr, effdateErr, crtuserErr, optdsc01Err, optdsc05Err, optdsc02Err, optdsc06Err, optdsc03Err, optdsc04Err, optdsc07Err, optdsc08Err};
		screenDateFields = new BaseData[] {trandate, effdate};
		screenDateErrFields = new BaseData[] {trandateErr, effdateErr};
		screenDateDispFields = new BaseData[] {trandateDisp, effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50qscreen.class;
		screenSflRecord = Sr50qscreensfl.class;
		screenCtlRecord = Sr50qscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50qprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50qscreenctl.lrec.pageSubfile);
	}
}
