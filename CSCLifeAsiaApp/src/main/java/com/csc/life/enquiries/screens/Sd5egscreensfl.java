package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sd5egscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {13, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 16, 3, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5egScreenVars sv = (Sd5egScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sd5egscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sd5egscreensfl, 
			sv.Sd5egscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sd5egScreenVars sv = (Sd5egScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sd5egscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sd5egScreenVars sv = (Sd5egScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sd5egscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sd5egscreensflWritten.gt(0))
		{
			sv.sd5egscreensfl.setCurrentIndex(0);
			sv.Sd5egscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sd5egScreenVars sv = (Sd5egScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sd5egscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5egScreenVars screenVars = (Sd5egScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.tranno.setFieldName("tranno");
				screenVars.revind.setFieldName("revind");
				screenVars.trandesc.setFieldName("trandesc");
				screenVars.tranamt.setFieldName("tranamt");
				screenVars.total.setFieldName("total");
				screenVars.lbalperc.setFieldName("lbalperc");				
				screenVars.allocamt.setFieldName("allocamt");
				screenVars.fillh.setFieldName("fillh");
				screenVars.filll.setFieldName("filll");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.crtuser.set(dm.getField("crtuser"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.tranno.set(dm.getField("tranno"));	
			screenVars.revind.set(dm.getField("revind"));
			screenVars.trandesc.set(dm.getField("trandesc"));
			screenVars.tranamt.set(dm.getField("tranamt"));
			screenVars.total.set(dm.getField("total"));
			screenVars.lbalperc.set(dm.getField("lbalperc"));					
			screenVars.allocamt.set(dm.getField("allocamt"));
			screenVars.fillh.set(dm.getField("fillh"));
			screenVars.filll.set(dm.getField("filll"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5egScreenVars screenVars = (Sd5egScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.tranno.setFieldName("tranno");
				screenVars.revind.setFieldName("revind");
				screenVars.trandesc.setFieldName("trandesc");
				screenVars.tranamt.setFieldName("tranamt");
				screenVars.total.setFieldName("total");
				screenVars.lbalperc.setFieldName("lbalperc");
				screenVars.allocamt.setFieldName("allocamt");				
				screenVars.fillh.setFieldName("fillh");
				screenVars.filll.setFieldName("filll");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("crtuser").set(screenVars.crtuser);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("revind").set(screenVars.revind);
			dm.getField("trandesc").set(screenVars.trandesc);
			dm.getField("tranamt").set(screenVars.tranamt);
			dm.getField("total").set(screenVars.total);
			dm.getField("lbalperc").set(screenVars.lbalperc);
			dm.getField("allocamt").set(screenVars.allocamt);
			dm.getField("fillh").set(screenVars.fillh);
			dm.getField("filll").set(screenVars.filll);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sd5egscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sd5egScreenVars screenVars = (Sd5egScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.crtuser.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.revind.clearFormatting();		
		screenVars.trandesc.clearFormatting();
		screenVars.tranamt.clearFormatting();
		screenVars.total.clearFormatting();
		screenVars.lbalperc.clearFormatting();
		screenVars.allocamt.clearFormatting();
		screenVars.fillh.clearFormatting();
		screenVars.filll.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sd5egScreenVars screenVars = (Sd5egScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.revind.setClassString("");
		screenVars.trandesc.setClassString("");
		screenVars.tranamt.setClassString("");
		screenVars.total.setClassString("");
		screenVars.lbalperc.setClassString("");
		screenVars.allocamt.setClassString("");
		screenVars.fillh.setClassString("");
		screenVars.filll.setClassString("");
	}

/**
 * Clear all the variables in Sd5egscreensfl
 */
	public static void clear(VarModel pv) {
		Sd5egScreenVars screenVars = (Sd5egScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.crtuser.clear();
		screenVars.effdateDisp.clear();
		screenVars.tranno.clear();
		screenVars.revind.clear();
		screenVars.trandesc.clear();
		screenVars.tranamt.clear();
		screenVars.total.clear();
		screenVars.lbalperc.clear();
		screenVars.allocamt.clear();
		screenVars.fillh.clear();
		screenVars.filll.clear();
	}
}
