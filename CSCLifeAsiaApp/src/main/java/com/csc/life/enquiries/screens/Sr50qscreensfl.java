package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50qscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 10;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {10, 11, 12}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {13, 21, 3, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50qScreenVars sv = (Sr50qScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr50qscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr50qscreensfl, 
			sv.Sr50qscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr50qScreenVars sv = (Sr50qScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr50qscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr50qScreenVars sv = (Sr50qScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr50qscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr50qscreensflWritten.gt(0))
		{
			sv.sr50qscreensfl.setCurrentIndex(0);
			sv.Sr50qscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr50qScreenVars sv = (Sr50qScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr50qscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50qScreenVars screenVars = (Sr50qScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.linetype.setFieldName("linetype");
				screenVars.hlife.setFieldName("hlife");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.select.setFieldName("select");
				screenVars.component.setFieldName("component");
				screenVars.compdesc.setFieldName("compdesc");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.cmpntnum.setFieldName("cmpntnum");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.linetype.set(dm.getField("linetype"));
			screenVars.hlife.set(dm.getField("hlife"));
			screenVars.hjlife.set(dm.getField("hjlife"));
			screenVars.hsuffix.set(dm.getField("hsuffix"));
			screenVars.hcoverage.set(dm.getField("hcoverage"));
			screenVars.hrider.set(dm.getField("hrider"));
			screenVars.select.set(dm.getField("select"));
			screenVars.component.set(dm.getField("component"));
			screenVars.compdesc.set(dm.getField("compdesc"));
			screenVars.statcode.set(dm.getField("statcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.cmpntnum.set(dm.getField("cmpntnum"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50qScreenVars screenVars = (Sr50qScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.linetype.setFieldName("linetype");
				screenVars.hlife.setFieldName("hlife");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.select.setFieldName("select");
				screenVars.component.setFieldName("component");
				screenVars.compdesc.setFieldName("compdesc");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.cmpntnum.setFieldName("cmpntnum");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("linetype").set(screenVars.linetype);
			dm.getField("hlife").set(screenVars.hlife);
			dm.getField("hjlife").set(screenVars.hjlife);
			dm.getField("hsuffix").set(screenVars.hsuffix);
			dm.getField("hcoverage").set(screenVars.hcoverage);
			dm.getField("hrider").set(screenVars.hrider);
			dm.getField("select").set(screenVars.select);
			dm.getField("component").set(screenVars.component);
			dm.getField("compdesc").set(screenVars.compdesc);
			dm.getField("statcode").set(screenVars.statcode);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("cmpntnum").set(screenVars.cmpntnum);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr50qscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr50qScreenVars screenVars = (Sr50qScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.linetype.clearFormatting();
		screenVars.hlife.clearFormatting();
		screenVars.hjlife.clearFormatting();
		screenVars.hsuffix.clearFormatting();
		screenVars.hcoverage.clearFormatting();
		screenVars.hrider.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.component.clearFormatting();
		screenVars.compdesc.clearFormatting();
		screenVars.statcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.cmpntnum.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr50qScreenVars screenVars = (Sr50qScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.linetype.setClassString("");
		screenVars.hlife.setClassString("");
		screenVars.hjlife.setClassString("");
		screenVars.hsuffix.setClassString("");
		screenVars.hcoverage.setClassString("");
		screenVars.hrider.setClassString("");
		screenVars.select.setClassString("");
		screenVars.component.setClassString("");
		screenVars.compdesc.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.cmpntnum.setClassString("");
	}

/**
 * Clear all the variables in Sr50qscreensfl
 */
	public static void clear(VarModel pv) {
		Sr50qScreenVars screenVars = (Sr50qScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.linetype.clear();
		screenVars.hlife.clear();
		screenVars.hjlife.clear();
		screenVars.hsuffix.clear();
		screenVars.hcoverage.clear();
		screenVars.hrider.clear();
		screenVars.select.clear();
		screenVars.component.clear();
		screenVars.compdesc.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.cmpntnum.clear();
	}
}
