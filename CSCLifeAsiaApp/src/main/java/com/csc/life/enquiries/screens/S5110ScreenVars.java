package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5110
 * @version 1.0 generated on 30/08/09 06:34
 * @author Quipoz
 */
public class S5110ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(137);
	public FixedLengthStringData dataFields = new FixedLengthStringData(73).isAPartOf(dataArea, 0);
	public FixedLengthStringData grupname = DD.grupname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData grupnum = DD.grupnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData membsel = DD.membsel.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData mplnum = DD.mplnum.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 73);
	public FixedLengthStringData grupnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData grupnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData membselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData mplnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 89);
	public FixedLengthStringData[] grupnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] grupnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] membselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] mplnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5110screenWritten = new LongData(0);
	public LongData S5110windowWritten = new LongData(0);
	public LongData S5110protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5110ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {grupnum, grupname, membsel, mplnum};
		screenOutFields = new BaseData[][] {grupnumOut, grupnameOut, membselOut, mplnumOut};
		screenErrFields = new BaseData[] {grupnumErr, grupnameErr, membselErr, mplnumErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5110screen.class;
		protectRecord = S5110protect.class;
	}

}
