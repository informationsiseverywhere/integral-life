/*
 * File: P6685.java
 * Date: 30 August 2009 0:52:51
 * Author: Quipoz Limited
 * 
 * Class transformed from P6685.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.enquiries.screens.S6685ScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO; //IBPLIFE-9762
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Regppf; //IBPLIFE-9762
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*
*  P6685 - Work With Regular Payments Enquiry.
*  -------------------------------------------
*
*  Overview.
*  ---------
*
*  This program enquires on Regular Payments records.  It  uses
*  the  'Work  With'  approach  as used in the Regular Payments
*  Submenu, but only allows selection for enquiry.
*
*  The contract details are retrieved from the previous  screen
*  and  the  subfile part of the screen will display all of the
*  components of the contract, including any  existing  payment
*  details.  These  will be interspersed between the components
*  so that they are visually associated with the components  to
*  which  they  relate.  The  usual  indentation method will be
*  employed by the subfile portion of the screen  to  represent
*  the  structure  of  the  contract and the interdependance of
*  the coverages and riders within the lives assured.
*
*  The program  switching  that  follows  will  be  unlike  the
*  standard  SMART  sub-menu  switching  in  that  it  will  be
*  generically  driven.  Depending  on   the   component   type
*  selected  the  transaction  will  switch  the user to either
*  Regular Withdrawals or Regular Benefit Claims.
*
*
*  Initialise.
*  -----------
*
*  Initialise all  Working  Storage  variables  and  clear  the
*  subfile ready for loading.
*
*  Page  up  and  Page  down  should  never  be received as the
*  subfile load will load all of the  components  and  existing
*  payment  details  regardless  of  how  many there are. There
*  should never be so many that it impairs system response.
*
*
*       Initial Screen Load.
*       --------------------
*
*       Display the Contract Type  and  long  description  from
*       table  T5688  and  read all of the components and write
*       one  line  to  the  subfile  for   each.   The   normal
*       indentation  should  be  employed in the display of the
*       Life, Coverage and Rider  sequence  numbers.  For  each
*       life  read  the LIFE file and obtain the Life Assured's
*       Client Number. Place this on the screen and  read  CLTS
*       to  obtain  the client's name. Format this for display.
*       The selection field should be protected  for  the  line
*       displaying the life details.
*
*       On  the  coverage  and rider lines display the coverage
*       code, (CRTABLE) and  read  T5687  to  obtain  the  long
*       description  for display. The selection fields on these
*       lines should also be  protected,  as  enquiry  is  only
*       allowed on Regular Payment records.
*
*       The  component  lines  should  be interspersed with the
*       details of any existing  payment  detail.  So  for  any
*       component  read the Regular Payment Details file, REGP,
*       and if corresponding  records  are  found  display  the
*       payment details as follows:
*
*            .  In  the  same  place  as  the client number and
*            coverage  code  are  displayed  show   the   short
*            description from T6691, keyed on the Payment Type.
*
*            . Display the payment status.
*
*            .  Display  the  First  Payment Date and the Final
*            Payment Date if it is not Max Date.
*
*            .  If  the  amount  is   non-zero   display   this
*            otherwise display the percentage.
*
*            . Display the currency code.
*
*       These  details  are all displayed on one long field and
*       so will require formatting within the program.
*
*       Also store the Regular Payment  Sequence  Number  in  a
*       hidden  field  on  the  subfile for later processing if
*       the line is selected for further action.
*
*       For all component and  Regular  Payment  subfile  lines
*       set  the  Life,  Coverage  and  Rider  numbers  in  the
*       appropriate fields but non-display where  necessary  to
*       provide  the  indentation  and  also  the values of the
*       fields for later processing. Also store the CRTABLE  in
*       a  hidden  field  on  each  subfile  record  for  later
*       processing.
*
*
*  Validation. (2000 Section).
*  ---------------------------------------
*
*
*  Converse with the screen using the screen I/O module.
*
*  Processing after a screen I/O will be one of the following:
*
*  . Selections to Process
*       Two passes will  be  made  over  the  subfile,  one  to
*       validate   the   selections   and   a  second,  if  the
*       selections are all valid, to process them.  The  second
*       pass will be carried out in the 3000 section.
*
*       For  the  first  pass perform a loop using Subfile Read
*       Next Changed to pick up all of the selected lines.  For
*       each  one  check  that it is valid by reading T6693 and
*       ensuring that the selected action is a valid  entry  on
*       the Extra Data Screen.
*
*       For  each  selected  line  read T6693 with a key of the
*       Regular   Payment   Status   concatenated   with    the
*       associated  Component  Code.  If no entry is found then
*       read  again  using  four  asterisks  in  place  of  the
*       Component  Code.  If there is still no entry then there
*       is no possible action for the status. If  an  entry  is
*       found  then check the transaction against the Allowable
*       Transactions on the Extra Data Screen.
*
*       If no selections were made re-set the subfile RRN to  1
*       and re-display the screen.
*
*
*  Updating.
*  ---------
*
*  The  first  time  through after selections have been made it
*  will be necessary to perform  a  start  on  the  subfile  to
*  re-position at the beginning after the validation pass.
*
*  Therefore  if WSAA-SELECTION is space perform a start on the
*  subfile. Thereafter whether WSAA-SELECTION is blank  or  not
*  continue  to  read  until  the  first non-blank selection is
*  located.
*
*  If there are no more selections and end of file  is  reached
*  set  WSAA-SELECTION  to space, set WSSP-EDTERROR to 'Y', set
*  the screen RRN to 1 and go to  exit.  This  will  cause  the
*  screen to be re-displayed.
*
*  Each  selection  will  be  processed  one  at a time and the
*  appropriate transaction programs loaded and switched to  for
*  each action.
*
*  Locate  the  appropriate  transaction  to  use  when reading
*  T5671 in the following way:
*
*       Access T1690 by calling the subroutine SUBPROG  passing
*       the  signon  company,  program  id. and the action from
*       the subfile. If the returned Statuz  is  not  O-K  then
*       use it as an error code on the selection field.
*
*       If  the  returned  code is O-K then check that the user
*       is authorised to invoke the transaction by calling  the
*       subroutine SANCTN. Pass the following parameters:
*
*            . Function     -    'SUBM'
*            . Password     \
*            . Company       >   from WSSP
*            . Branch       /
*            . Transcode    -    SUBP-TRANSCD, (from T1690).
*
*       If  the  returned  Status  is 'BOMB' then perform fatal
*       error processing. If it is not O-K then use  it  as  an
*       error code on the selection field.
*
*  Set WSSP-FLAG accordingly:
*
*       . Enquire -  WSSP-FLAG =  'I'.
*
*  If  the  selection  has  been made against a regular payment
*  line then store the REGP record with a READS.
*
*  Set the current selection in  the  field  WSAA-SELECTION  in
*  Working  Storage  to  indicate  that  a  selection  is to be
*  processed.  This  will  be  used  upon   return   from   the
*  transaction  so  that  this  program  can track its progress
*  through the selections.
*
*  Blank out  the  selection  field  and  rewrite  the  subfile
*  record.
*
*
*  Where Next.
*  -----------
*
*  This  section  will only be reached if a valid selection has
*  been made. If there was no selection then  the  screen  will
*  have  been  re-displayed. As this is a sub-menu the user may
*  only leave by using a function key.
*
*  The current line has  already  been  selected  in  the  3000
*  section  and this section will load the appropriate programs
*  and set the transaction in motion.
*
*  As the processing from here will be  generically  based  the
*  appropriate  programs  will  be  found  on T5671. Read T5671
*  using the Transaction Code concatenated with  the  Component
*  Code,  CRTABLE,  and  load  the programs into the first four
*  entries on the program stack.
*
*
*  Notes.
*  ------
*
*
*  Tables Used.
*  ------------
*
*  . T5671 - Generic program Switching
*            Key: Transaction Code || CRTABLE
*
*  . T5687 - Coverage / Rider Details
*            Key: CRTABLE
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T1690 - Submenu Switching
*            Key: WSAA-PROG and Submenu Action
*
*  . T6691 - Regular Payment Type
*            Key: Regular Payment Type
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*****************************************************************
* </pre>
*/
public class P6685 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6685");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String e005 = "E005";
		/* TABLES */
	private String t5671 = "T5671";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
	private String t6691 = "T6691";
	private String t6693 = "T6693";
	private String cltsrec = "CLTSREC";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String regprec = "REGPREC";

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Transcd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);
	//ILIFE-1530 STARTS
	private FixedLengthStringData wsaaPaymentDetails = new FixedLengthStringData(53);
	//ILIFE-1530 ENDS
	private FixedLengthStringData wsaaPaystat = new FixedLengthStringData(2).isAPartOf(wsaaPaymentDetails, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 2, FILLER).init(SPACES);
	private FixedLengthStringData wsaaFirstPaydate = new FixedLengthStringData(10).isAPartOf(wsaaPaymentDetails, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 13, FILLER).init(SPACES);
	private FixedLengthStringData wsaaFinalPaydate = new FixedLengthStringData(10).isAPartOf(wsaaPaymentDetails, 14).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 24, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaPaymentDetails, 25);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 28, FILLER).init(SPACES);
	//ILIFE-1530 STARTS
	private ZonedDecimalData wsaaAmtPrcntVal = new ZonedDecimalData(17, 2).isAPartOf(wsaaPaymentDetails, 29).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaPercentSign = new FixedLengthStringData(2).isAPartOf(wsaaPaymentDetails, 51);
	//ILIFE-1530 ENDS
		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaFirstLifeRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstCovrRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstPaymRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSelection = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaCompare = new FixedLengthStringData(2);
	private FixedLengthStringData filler4 = new FixedLengthStringData(1).isAPartOf(wsaaCompare, 0, FILLER).init("0");
	private String wsaaFirstRecord = "";
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4);
	private String wsaaFoundFlag = "";

	private FixedLengthStringData wsaaSecProgs = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(8, 5, wsaaSecProgs, 0);
		/*Contract Enquiry - Contract Header.*/
	// ILIFE-3396 PERFORMANCE BY OPAL
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
			/*Regular Payments View.*/
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
		/*Client logical file with new fields*/
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Regular Payments File*/
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Subprogrec subprogrec = new Subprogrec();
	private T5671rec t5671rec = new T5671rec();
	private T6693rec t6693rec = new T6693rec();
	private Batckey wsaaBatchkey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6685ScreenVars sv = ScreenProgram.getScreenVars( S6685ScreenVars.class);
	//IBPLIFE-4592 start
	/*Life record*/
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private Lifepf lifepf = new Lifepf();
	/*Component (Coverage/Rider) Record*/
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf;
	//IBPLIFE-4592 end
	
	private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class); //IBPLIFE-9762

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit2090, 
		exit2100, 
		getRegPayment2310, 
		exit2300, 
		exit12090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		updateErrorIndicators2670, 
		exit3090
	}

	public P6685() {
		super();
		screenVars = sv;
		new ScreenModel("S6685", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if ((isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*"))
		|| (isEQ(scrnparams.statuz,varcom.endp))) {
			wsaaBatchkey.batcBatctrcde.set(wsaaBatctrcde);
			wsspcomn.batchkey.set(wsaaBatchkey);
			goTo(GotoLabel.exit1090);
		}
		scrnparams.subfileRrn.set(1);
		wsaaT5671Key.set(SPACES);
		wsaaT6693Key.set(SPACES);
		wsaaPaymentDetails.set(SPACES);
		wsaaFirstLifeRead.set(SPACES);
		wsaaFirstCovrRead.set(SPACES);
		wsaaFirstPaymRead.set(SPACES);
		wsaaSelection.set(SPACES);
		wsaaSub.set(ZERO);
		wsaaAmtPrcntVal.set(ZERO);
		wsaaEffdate.set(ZERO);
		wsaaFoundFlag = "N";
		// ILIFE-3396 PERFORMANCE BY OPAL
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf==null)
		{
			return;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6685", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.chdrsel.set(chdrpf.getChdrnum());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		sv.cnttype.set(chdrpf.getCnttype());
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaEffdate.set(datcon1rec.intDate);
		wsaaFirstLifeRead.set("Y");
		//IBPLIFE-4592 start, instead of DAM read which was taking time DAO approach used to fetch the record.
		List<Lifepf> data = lifepfDAO.getLfRecords(wsspcomn.company.toString(),chdrpf.getChdrnum(),"00");
		Iterator<Lifepf> it = data.iterator();
		while(it.hasNext())
		{
			lifepf = it.next();
			getPolicyDetails2100();	
		}
		//IBPLIFE-4592 end
		scrnparams.subfileRrn.set(1);
		if ((isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES))
		&& (isEQ(wsaaFoundFlag,"N"))) {
			wsaaBatchkey.set(wsspcomn.batchkey);
			wsaaBatctrcde.set(wsaaBatchkey.batcBatctrcde);
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if ((isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*"))
		|| (isEQ(scrnparams.statuz,varcom.endp))) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*DISPLAY-SCREEN*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz,varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		scrnparams.subfileRrn.set(1);
	}

protected void validateScreen2060()
	{
		if (isEQ(wsaaSelection,SPACES)) {
			scrnparams.function.set(varcom.srnch);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		processScreen("S6685", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		scrnparams.subfileRrn.set(1);
		wsaaFirstRecord = "Y";
	}

protected void getPolicyDetails2100()
	{
		try {
			getClientNumber2110();
			getClientName2120();
			getCompPayDetails2130();
		}
		catch (GOTOException e){
		}
	}

protected void getClientNumber2110()
	{
		if (isEQ(wsaaFirstLifeRead,"Y")) {
			wsaaFirstLifeRead.set("N");
		}
		//IBPLIFE-4592 start
		if((isNE(lifepf.getValidflag(),"1"))) {
			 	goTo(GotoLabel.exit2100); 
	 }
		//IBPLIFE-4592 end
	}

protected void getClientName2120()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifepf.getLifcnum());//IBPLIFE-4592
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.subfileArea.set(SPACES);
		sv.life.set(lifepf.getLife());//IBPLIFE-4592
		sv.shortdesc.set(lifepf.getLifcnum());//IBPLIFE-4592
		sv.elemdesc.set(wsspcomn.longconfname);
		sv.actionOut[varcom.pr.toInt()].set("Y");
		sv.actionOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		addToSubfile2500();
	}

protected void getCompPayDetails2130()
	{
		//IBPLIFE-4592 start, instead of DAM read which was taking time DAO approach used to fetch the record.
		 Covrpf covrdata=new Covrpf();
		 covrdata.setChdrcoy(lifepf.getChdrcoy());
		 covrdata.setChdrnum(lifepf.getChdrnum());
		 covrdata.setLife(lifepf.getLife());
		 List<Covrpf> covrList=covrpfDAO.selectCoverage(covrdata);
		 for ( Covrpf covr:covrList) {
				covrpf = covr;
				getComponentDetails2300();
			}
		//IBPLIFE-4592 end
	}

protected void getComponentDetails2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					getCovrDetails2310();
				}
				case getRegPayment2310: {
					getRegPayment2310();
				}
				case exit2300: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getCovrDetails2310()
	{
		if (isEQ(wsaaFirstCovrRead,"Y")) {
			wsaaFirstCovrRead.set("N");
		}
		if ((isEQ(covrpf.getLife(),sv.life))
		&& (isEQ(covrpf.getCoverage(),sv.coverage))
		&& (isEQ(covrpf.getRider(),sv.rider))) {
			goTo(GotoLabel.exit2300);
		}
		else {
			sv.subfileArea.set(SPACES);
			sv.life.set(covrpf.getLife());
			sv.rider.set(covrpf.getRider());
			sv.coverage.set(covrpf.getCoverage());
			sv.lifeOut[varcom.nd.toInt()].set("Y");
			if ((isEQ(covrpf.getRider(),ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
			}
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(t5687);
			descIO.setDescitem(covrpf.getCrtable());
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if ((isNE(descIO.getStatuz(),varcom.oK))
			&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
				sv.elemdesc.set(SPACES);
			}
			else {
				sv.elemdesc.set(descIO.getLongdesc());
			}
			sv.shortdesc.set(covrpf.getCrtable());
			sv.hcrtable.set(covrpf.getCrtable());
			sv.actionOut[varcom.pr.toInt()].set("Y");
			addToSubfile2500();
			goTo(GotoLabel.getRegPayment2310);
		}
	}

protected void getRegPayment2310()
	{
	//IBPLIFE-9762 start, DAM to DAO call change
	List<Regppf> regpList=regppfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider());
		for(Regppf regppf:regpList) {
			getPaymentDetails2400(covrpf,regppf);
		}
	}
	

protected void getPaymentDetails2400(Covrpf covrpf,Regppf regppf)
{

	descIO.setDataArea(SPACES);
	descIO.setDesccoy(wsspcomn.company);
	descIO.setDescpfx("IT");
	descIO.setDesctabl(t6691);
	descIO.setDescitem(regppf.getRgpytype());
	descIO.setLanguage(wsspcomn.language);
	descIO.setFormat(descrec);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if ((isNE(descIO.getStatuz(),varcom.oK))
	&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	datcon1rec.intDate.set(regppf.getFirstPaydate());
	datcon1rec.function.set("CONV");
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	if (isNE(datcon1rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon1rec.datcon1Rec);
		fatalError600();
	}
	wsaaFirstPaydate.set(datcon1rec.extDate);
	if (isNE(regppf.getFinalPaydate(),varcom.vrcmMaxDate)) {
		datcon1rec.intDate.set(regppf.getFinalPaydate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaFinalPaydate.set(datcon1rec.extDate);
	}
	else {
		wsaaFinalPaydate.set(SPACES);
	}
	wsaaPaystat.set(regppf.getRgpystat());
	wsaaCurrcode.set(regppf.getCurrcd());
	if (isNE(regppf.getPymt(),ZERO)) {
		wsaaAmtPrcntVal.set(regppf.getPymt());
		wsaaPercentSign.set(SPACES);
	}
	else {
		wsaaAmtPrcntVal.set(regppf.getPrcnt());
		wsaaPercentSign.set(" %");
	}
	sv.subfileArea.set(SPACES);
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		sv.shortdesc.set(SPACES);
	}
	else {
		sv.shortdesc.set(descIO.getShortdesc());
	}
	sv.life.set(regppf.getLife());
	sv.coverage.set(regppf.getCoverage());
	sv.rider.set(regppf.getRider());
	sv.lifeOut[varcom.nd.toInt()].set("Y");
	sv.coverageOut[varcom.nd.toInt()].set("Y");
	sv.riderOut[varcom.nd.toInt()].set("Y");
	sv.hcrtable.set(covrpf.getCrtable());
	sv.hrgpynum.set(regppf.getRgpynum());
	sv.elemdesc.set(wsaaPaymentDetails);
	addToSubfile2500();
	}
	//IBPLIFE-9762 end

protected void addToSubfile2500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6685", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		String name = getName();
		wsspcomn.longconfname.set(name);
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = ",";

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);
			
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = "";
			String salute = cltsIO.getSalutl().toString();

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = getStringUtil().includeSalute(salute, fullName);
			wsspcomn.longconfname.set(fullNameWithSalute);
	
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = cltsIO.getLgivname().toString();
		String lastName = cltsIO.getLsurname().toString();
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isEQ(sv.action,SPACES)) {
			sv.actionErr.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if ((isNE(sv.hrgpynum,SPACES))) {
			itdmIO.setDataArea(SPACES);
			wsaaPaymentDetails.set(sv.elemdesc);
			wsaaT6693Paystat.set(wsaaPaystat);
			wsaaT6693Crtable.set(sv.hcrtable);
			readT6693Table2700();
			if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
				itdmIO.setDataArea(SPACES);
				wsaaT6693Crtable.set("****");
				readT6693Table2700();
			}
		}
		else {
			itdmIO.setDataArea(SPACES);
			wsaaT6693Crtable.set(sv.hcrtable);
			wsaaT6693Paystat.set("**");
			readT6693Table2700();
		}
		if (isNE(itdmIO.getStatuz(),varcom.endp)) {
			t6693rec.t6693Rec.set(itdmIO.getGenarea());
		}
		else {
			sv.actionErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		subprogrec.action.set(sv.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			sv.actionOut[varcom.pr.toInt()].set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		wsaaSub.set(1);
		while ( !((isGT(wsaaSub,12))
		|| (isEQ(subprogrec.transcd,t6693rec.trcode[wsaaSub.toInt()])))) {
			if (isNE(subprogrec.transcd,t6693rec.trcode[wsaaSub.toInt()])) {
				wsaaSub.add(1);
			}
		}
		
		if (isGT(wsaaSub,12)) {
			sv.actionErr.set(e005);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.hrgpynum,SPACES)) {
			if ((isEQ(sv.rider,ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(SPACES);
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(SPACES);
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6685", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		if (isEQ(wsaaSelection,SPACES)) {
			scrnparams.function.set(varcom.srnch);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		processScreen("S6685", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void readT6693Table2700()
	{
		readTable2710();
	}

protected void readTable2710()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(wsaaEffdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(),wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(),t6693))
		|| (isNE(itdmIO.getItemitem(),wsaaT6693Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemitem(wsaaT6693Key);
		}
	}

protected void update3000()
	{
		try {
			processSelections3010();
			checkActionsSanctions3030();
			setActionFlag3040();
		}
		catch (GOTOException e){
		}
	}

protected void processSelections3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if ((isEQ(wsaaSelection,SPACES))
		&& (isEQ(wsaaFirstRecord,"Y"))) {
			scrnparams.function.set(varcom.sstrt);
			converseWithScreen3200();
		}
		scrnparams.function.set(varcom.srdn);
		while ( !((isNE(sv.action,SPACES))
		|| (isEQ(scrnparams.statuz,varcom.endp)))) {
			converseWithScreen3200();
		}
		
		/*FIND-RECORD*/
		if ((isEQ(scrnparams.statuz,varcom.endp))) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit3090);
		}
	}

protected void checkActionsSanctions3030()
	{
		wsaaFirstRecord = "N";
		subprogrec.action.set(sv.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			scrnparams.function.set(varcom.supd);
			converseWithScreen3200();
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit3090);
		}
	}

protected void setActionFlag3040()
	{
		if (isEQ(sv.action,"1")) {
			wsspcomn.flag.set("I");
		}
	}

protected void converseWithScreen3200()
	{
		getSelectedRecord3210();
	}

protected void getSelectedRecord3210()
	{
		if (isEQ(sv.hrgpynum,SPACES)) {
			if ((isEQ(sv.rider,ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(SPACES);
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(SPACES);
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		processScreen("S6685", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		if ((isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			sv.action.set(SPACES);
			sub2.set(1);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
				sub1.add(1);
				sub2.add(1);
			}
		}
		else {
			wsaaFoundFlag = "N";
			scrnparams.function.set(varcom.sstrt);
			processScreen("S6685", sv);
			if ((isNE(scrnparams.statuz,varcom.oK))) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		scrnparams.function.set(varcom.srdn);
		while ( !((isNE(sv.action,SPACES))
		|| (isEQ(scrnparams.statuz,varcom.endp)))) {
			processScreen("S6685", sv);
			if ((isNE(scrnparams.statuz,varcom.oK))
			&& (isNE(scrnparams.statuz,varcom.endp))) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isNE(scrnparams.statuz,varcom.endp)) {
			if (isNE(sv.action,SPACES)) {
				wsaaFoundFlag = "Y";
				readT56715100();
				saveOldStack5200();
				loadNewStack5300();
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			}
			else {
				if (isEQ(wsaaFoundFlag,"N")) {
					wsaaBatchkey.batcBatctrcde.set(wsaaBatctrcde);
					wsspcomn.batchkey.set(wsaaBatchkey);
				}
			}
			chdrrgpIO.setParams(SPACES);
			chdrrgpIO.setChdrnum(chdrpf.getChdrnum());
			chdrrgpIO.setChdrcoy(chdrpf.getChdrcoy());
			chdrrgpIO.setFunction(varcom.reads);
			SmartFileCode.execute(appVars, chdrrgpIO);
			if (isNE(chdrrgpIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrrgpIO.getParams());
				fatalError600();
			}
			regpIO.setChdrcoy(chdrpf.getChdrcoy());
			regpIO.setChdrnum(chdrpf.getChdrnum());
			regpIO.setLife(sv.life);
			regpIO.setCoverage(sv.coverage);
			regpIO.setRider(sv.rider);
			regpIO.setRgpynum(ZERO);
			regpIO.setCrtable(sv.hcrtable);
			regpIO.setFormat(regprec);
			regpIO.setRgpynum(sv.hrgpynum.toInt());
			regpIO.setFunction(varcom.reads);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			wsaaBatchkey.set(wsspcomn.batchkey);
			wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
			wsspcomn.batchkey.set(wsaaBatchkey);
			wsaaSelection.set(sv.action);
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			if (isEQ(wsaaSelection,SPACES)) {
				wsspcomn.programPtr.add(1);
			}
		}
	}

protected void readT56715100()
	{
		readTable5110();
	}

protected void readTable5110()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Transcd.set(subprogrec.transcd);
		wsaaT5671Crtable.set(sv.hcrtable);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void saveOldStack5200()
	{
		/*SAVE-STACK*/
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
			sub1.add(1);
			sub2.add(1);
		}
		/*EXIT*/
	}

protected void loadNewStack5300()
	{
		/*LOAD-STACK*/
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			wsspcomn.secProg[sub1.toInt()].set(t5671rec.pgm[sub2.toInt()]);
			sub1.add(1);
			sub2.add(1);
		}
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			wsspcomn.secProg[sub1.toInt()].set(SPACES);
			sub1.add(1);
			sub2.add(1);
		}
		/*EXIT*/
	}

	public String getName() {
		return cltsIO.getSurname().toString();
	}
	
	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
	
	
}