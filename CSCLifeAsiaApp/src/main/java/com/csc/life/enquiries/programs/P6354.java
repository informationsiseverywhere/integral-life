/*
 * File: P6354.java
 * Date: 30 August 2009 0:45:12
 * Author: Quipoz Limited
 *
 * Class transformed from P6354.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.procedures.Usrcypind;
import com.csc.fsu.general.recordstructures.Cipherrec;
import com.csc.fsu.general.recordstructures.Usrcypirec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.Tr29urec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.enquiries.dataaccess.AgntenqTableDAM;
import com.csc.life.enquiries.dataaccess.AsgnenqTableDAM;
import com.csc.life.enquiries.dataaccess.BnfyenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S6354ScreenVars;
import com.csc.life.newbusiness.dataaccess.CtrsTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.CreditCardUtility;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.MaskingUtil;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of  the  contract  being  enquired  upon  will be
*     stored in the  CHDRENQ  I/O  module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*     The PAYR file is read for the contract,followed by T3620.
*     If the contract requires bank details(i.e Direct Debit with
****  T3620-DDIND NOT = SPACES),then the Mandate file is read
****  to obtain the Mandate detalis.
*     T3620-DDIND NOT = SPACES or Credit Card with T3620-CRCIND
*     NOT = SPACES),then the Mandate file is read to obtain the
*     Mandate detalis.
*
*     Set up the  Factoring  House, (CHDR-FACTHOUS), on the screen.
*     Valid Factoring House  codes  are held on T3684. (See GETDESC
*     for an example of how to obtain the long description from the
*     DESC data-set. Do  not  call  GETDESC,  the  appropriate code
*     should be moved into the mainline.
*
*     Set up the  Bank  Code,  (CHDR-BANKKEY),  on  the  screen and
*     obtain the  corresponding  description  from the Bank Details
*     data-set BABR.  Read  BABR  with  a  key  of CHDR-BANKKEY and
*     obtain BABR-BANKDESC.
*
*     Set up the  Account  Number, (CHDR-BANKACCKEY), on the screen
*     and  obtain  the  correeponding  description  from  the  CLBL
*     data-set.  Read  CLBL   with   a   key  of  CHDR-BANKKEY  and
*     CHDR-BANKACCKEY and obtain CLBL-BANKADDDSC and CLBL-CURRCODE.
*
*     If the Assignee client, (CHDR-ASGNNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the usual way.
*
*     If the Despatch client, (CHDR-DESPNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the ususal way.
*
*
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation  required  is  of  the  indicator field -
*     CONBEN. This may be space or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  pressed  move  spaces  to  the current program
*     position in the program stack and exit.
*
*     If returning from  processing  a  selection  further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore the next  8  programs  from  the  WSSP and remove the
*     asterisk.
*
*     If nothing was  selected move '*' to the current stack action
*     field, add 1 to the program pointer and exit.
*
*     If a selection has  been found use GENSWCH to locate the next
*     program(s)  to process  the  selection,  (up  to  8).  Use  a
*     function of 'A'. Save  the  next  8 programs in the stack and
*     replace them with  the  ones  returned from GENSWCH. Place an
*     asterisk in the  current  stack  action  field,  add 1 to the
*     program pointer and exit.
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
* T3684 - Factoring House                   Key: FACTHOUS
* T3620 - Billing Channel                   Key: BILLCHNL
*
*
*****************************************************************
* </pre>
*/
public class P6354 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6354");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private static final String f973 = "F973";
	private static final String g620 = "G620";
	private static final String h093 = "H093";
	private static final String rfk5 = "RFK5";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t3684 = "T3684";
	private static final String t5688 = "T5688";
	private static final String t3678 = "T3678";
	private static final String t3620 = "T3620";
	private static final String t5729 = "T5729";
	private static final String tr29u = "TR29U";
	private AgntenqTableDAM agntenqIO = new AgntenqTableDAM();
	private AsgnenqTableDAM asgnenqIO = new AsgnenqTableDAM();
	private BabrTableDAM babrIO = new BabrTableDAM();
	private BnfyenqTableDAM bnfyenqIO = new BnfyenqTableDAM();
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ClblTableDAM clblIO = new ClblTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CtrsTableDAM ctrsIO = new CtrsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private MandTableDAM mandIO = new MandTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private T3620rec t3620rec = new T3620rec();
	private Tr29urec tr29urec = new Tr29urec();
	private Cipherrec cipherrec = new Cipherrec();
	private Usrcypirec usrcypirec = new Usrcypirec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	private S6354ScreenVars sv = getPScreenVars() ;
	private FormatsInner formatsInner = new FormatsInner();
	private static final String  DESADD_FEATURE_ID="CTENQ005"; //ILIFE-7407
	private boolean desaddFlag=false; //ILIFE-7407
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		bankCode1020,
		assigneeName1030,
		contractType1050,
		exit1090,
		exit2090,
		gensww4010,
		nextProgram4020,
		exit4090
	}

	public P6354() {
		super();
		screenVars = sv;
		new ScreenModel("S6354", AppVars.getInstance(), sv);
	}

	protected S6354ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S6354ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case bankCode1020:
					bankCode1020();
				case assigneeName1030:
					assigneeName1030();
					beneficiary1035();
					trustee1036();
					loanDetails1037();
					despatchName1040();
				case contractType1050:
					contractType1050();
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		/* Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/*  MOVE CHDRENQ-ASGNNUM        TO S6354-ASGNNUM.                */
		sv.despnum.set(chdrpf.getDespnum());
		sv.mandref.set(chdrpf.getMandref());
		sv.facthous.set(chdrpf.getFacthous());
		/*sv.bankkey.set(chdrpf.getBankkey());
		sv.bankacckey.set(chdrpf.getBankacckey());*/

		/*String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(sv.bankacckey.toString().trim());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			sv.bankacckey.set(creditCardInformation);
		}*/

		/*                                                            <004>*/
		/*                                                            <004>*/
		/* the MOP is by Direct Debit obtain the Bank details from the<004>*/
		/* Mandate File..                                             <004>*/
		/*                                                            <004>*/
		/*    IF CHDRENQ-BILLCHNL          = 'B'                      <004>*/
		/*       MOVE SPACES              TO MAND-PARAMS              <004>*/
		/*       MOVE WSSP-FSUCO          TO MAND-PAYRCOY             <004>*/
		/*       IF CHDRENQ-PAYRNUM        = SPACES                   <004>*/
		/*          MOVE CHDRENQ-COWNNUM  TO MAND-PAYRNUM             <004>*/
		/*       ELSE                                                 <004>*/
		/*          MOVE CHDRENQ-PAYRNUM  TO MAND-PAYRNUM             <004>*/
		/*       END-IF                                               <004>*/
		/*       MOVE CHDRENQ-MANDREF     TO MAND-MANDREF             <004>*/
		/*       MOVE MANDREC             TO MAND-FORMAT              <004>*/
		/*       MOVE READR               TO MAND-FUNCTION            <004>*/
		/*                                                            <004>*/
		/*       CALL 'MANDIO'            USING MAND-PARAMS           <004>*/
		/*                                                            <004>*/
		/*       IF MAND-STATUZ        NOT = O-K                      <004>*/
		/*          MOVE MAND-PARAMS      TO SYSR-PARAMS              <004>*/
		/*          PERFORM 600-FATAL-ERROR                           <004>*/
		/*       ELSE                                                 <004>*/
		/*          MOVE MAND-BANKKEY     TO S6354-BANKKEY            <004>*/
		/*          MOVE MAND-BANKACCKEY  TO S6354-BANKACCKEY         <004>*/
		/*       END-IF                                               <004>*/
		/*    END-IF.                                                 <004>*/
		/*  Next Read the PAYR file and then using the PAYR-BILLCHNL,      */
		/*  Read table T3620.If on this table the BILLING METHOD           */
		/*  requires bank details ie T3620-DDIND NOT SPACES(MOP is         */
		/*  Direct Debit), Read the Mandate file to obtain Mandate <V76F13>*/
		/*  Details.                                               <V76F13>*/
		/*  Direct Debit) or T3620-CRCIND NOT SPACES(MOP is Credit         */
		/*  Card), Read the Mandate file to obtain Mandate Details.        */
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrpf.getChdrcoy());
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Read TABLE T3620,using the PAYR-BILLCHNL to find out whether    */
		/* the payment method requires BANK DETAILS.If it does ie          */
		/* T3630-DDIND NOT = SPACES, then read the Mandate file to         */
		/* obtain Mandate details.                                         */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t3620);
		itemIO.setItemitem(payrIO.getBillchnl());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		sv.payrname.set(SPACES); //ILIFE-2472
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		if (isNE(t3620rec.ddind, SPACES)
		|| isNE(t3620rec.crcind, SPACES)) {
			readClntRole1100();
			mandIO.setParams(SPACES);
			/*     MOVE WSSP-FSUCO         TO MAND-PAYRCOY          <CAS1.0>*/
			/*     IF  CHDRENQ-PAYRNUM      = SPACES                <CAS1.0>*/
			/*         MOVE CHDRENQ-COWNNUM TO MAND-PAYRNUM         <CAS1.0>*/
			/*     ELSE                                             <CAS1.0>*/
			/*         MOVE CHDRENQ-PAYRNUM TO MAND-PAYRNUM         <CAS1.0>*/
			/*     END-IF                                           <CAS1.0>*/
			/*     MOVE CHDRENQ-MANDREF    TO MAND-MANDREF          <CAS1.0>*/
			clrfIO.setParams(SPACES);
			clrfIO.setForepfx("CH");
			clrfIO.setForecoy(payrIO.getChdrcoy());
			wsaaForenum.set(SPACES);
			wsaaChdrnum.set(payrIO.getChdrnum());
			wsaaPayrseqno.set(payrIO.getPayrseqno());
			clrfIO.setForenum(wsaaForenum);
			clrfIO.setClrrrole("PY");
			clrfIO.setFormat(formatsInner.clrfrec);
			clrfIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, clrfIO);
			if (isNE(clrfIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(clrfIO.getParams());
				fatalError600();
			}
			mandIO.setDataKey(SPACES);
			mandIO.setPayrcoy(clrfIO.getClntcoy());
			mandIO.setPayrnum(clrfIO.getClntnum());
			mandIO.setMandref(payrIO.getMandref());
			mandIO.setFormat(formatsInner.mandrec);
			mandIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, mandIO);
			if (isNE(mandIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(mandIO.getParams());
				syserrrec.statuz.set(mandIO.getStatuz());
				fatalError600();
			}
			else {
				//ILIFE-2472 Starts
				if(isEQ(chdrpf.getZmandref(),SPACES) && isEQ(chdrpf.getPayrnum(),SPACES))
				{
				sv.payor.set(chdrpf.getCownnum());
				}
				else
				{
				sv.payor.set(mandIO.getPayrnum());	
				}
				cltsIO.setClntnum(sv.payor);	//ILIFE-6500
				cltsIO.setClntcoy(wsspcomn.fsuco);
				cltsIO.setClntpfx("CN");
				cltsIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, cltsIO);
				if (isNE(cltsIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(cltsIO.getParams());
					fatalError600();
				}
				plainname();
				sv.payrname.set(wsspcomn.longconfname);
				//ILIFE-2472 Ends
				sv.bankkey.set(mandIO.getBankkey());
				sv.bankacckey.set(mandIO.getBankacckey());
				String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(sv.bankacckey.toString().trim());
				if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
					sv.bankacckey.set(creditCardInformation);
				}
			}
		}
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO S6354-JLIFE                   */
		/*                                 S6354-JLIFENAME               */
		/*  ELSE                                                         */
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		//ILIFE-2472-BEGIN		
		/* TSD 321 Phase 2 Starts */
		sv.payorname.set(SPACES);
		/* TSD 321 Phase 2 Ends */
		
		if( chdrpf.getReqntype()!=null) {
			if(((isEQ( chdrpf.getReqntype(),"C") ||isEQ( chdrpf.getReqntype(),"4")) && isNE(chdrpf.getZmandref(),SPACES)) || isNE(chdrpf.getZmandref(),SPACES))	{
				loadPayoutMandate4500();
			}else if( isEQ( chdrpf.getReqntype(),"C") ||isEQ( chdrpf.getReqntype(),"4") && isEQ(chdrpf.getZmandref(),SPACES))	{
				loadPayoutChdr();
			}			
		}	
		
		
		//ILIFE-2472-END		
		if (isEQ(sv.facthous, SPACES)) {
			goTo(GotoLabel.bankCode1020);
		}
		/*    Obtain the Factoring House description from T3684.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		/*MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 */
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3684);
		descIO.setDescitem(chdrpf.getFacthous());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.fcthsedsc.fill(" ");
		}
		else {
			sv.fcthsedsc.set(descIO.getLongdesc());
		}
		//ILIFE-2472
		//loadPayoutMandate4500();
	}
	

	
	protected void loadPayoutChdr(){
		
		sv.payrnum.set(chdrpf.getPayclt());
		if (isEQ(sv.payrnum,SPACE)) {
		    sv.payrnum.set(chdrpf.getCownnum());
		}
		
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.payorname.set(wsspcomn.longconfname);
		
		
		sv.bankkey02.set(chdrpf.getBankkey());
		sv.bankacckey02.set(chdrpf.getBankacckey());
		
		// Payout Factoring House, Bank Account description and Bank Description
				babrIO.setBankkey(sv.bankkey02);
				babrIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, babrIO);
				if (isNE(babrIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(babrIO.getParams());
					fatalError600();
				}
				sv.bankdesc02.set(babrIO.getBankdesc());
				clblIO.setBankkey(sv.bankkey02);
				String referenceNumber=CreditCardUtility.getInstance().getReference(sv.bankacckey02.toString().trim());
				if(referenceNumber==null || referenceNumber.trim().isEmpty()){
					clblIO.setBankacckey(sv.bankacckey02);
				} else {
					clblIO.setBankacckey(referenceNumber);
				}
				clblIO.setClntcoy(chdrpf.getCowncoy());
				clblIO.setClntnum(chdrpf.getCownnum());
				clblIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, clblIO);
				if (isNE(clblIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(clblIO.getParams());
					fatalError600();
				}
				sv.bankaccdsc02.set(clblIO.getBankaccdsc());
				sv.bnkcurr02.set(clblIO.getCurrcode());
			
				descIO.setDataKey(SPACES);
				sv.facthous.set(clblIO.getFacthous());
				descIO.setDescitem(clblIO.getFacthous());
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.fsuco);
				descIO.setDesctabl(t3684);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if ((isNE(descIO.getStatuz(), varcom.oK))
						&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
					sv.fcthsedsc02.fill(" ");
				} else {
					sv.fcthsedsc02.set(descIO.getLongdesc());
				}
			
				if (isEQ(chdrpf.getReqntype(), "C")) {
					decryption4600();
				}
		
	}
	//ILIFE-2472-BEGIN
	protected void loadPayoutMandate4500()	{
	
		/* TSD 321 Phase 2 Starts */
		sv.payrnum.set(chdrpf.getPayclt());
		if (isEQ(sv.payrnum,SPACE)) {
		    sv.payrnum.set(chdrpf.getCownnum());
		}
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.payorname.set(wsspcomn.longconfname);
		/* TSD 321 Phase 2 Ends */
		
		//Payout Mandate Reference
		sv.zmandref.set(chdrpf.getZmandref());
	
		// Payout Bank Code and Payout Bank Account Number
		mandIO.setPayrcoy(chdrpf.getCowncoy());
		mandIO.setPayrnum(sv.payrnum);
		mandIO.setMandref(chdrpf.getZmandref().trim());
		mandIO.setFormat(formatsInner.mandrec);
		mandIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			syserrrec.statuz.set(mandIO.getStatuz());
			fatalError600();
		}
		else {
			sv.bankkey02.set(mandIO.getBankkey());
			sv.bankacckey02.set(mandIO.getBankacckey());
			String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(sv.bankacckey02.toString().trim());
			if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
				sv.bankacckey02.set(creditCardInformation);
			}
		}
		// Payout Mandate Status
		descIO.setDataKey(SPACES);
		descIO.setDescitem(mandIO.getMandstat());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3678);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
				&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.manstatdsc02.fill(" ");
		} else {
			sv.manstatdsc02.set(descIO.getLongdesc());
		}
		sv.mandstat02.set(mandIO.getMandstat());
	
		// Payout Factoring House, Bank Account description and Bank Description
		babrIO.setBankkey(sv.bankkey02);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		sv.bankdesc02.set(babrIO.getBankdesc());
		clblIO.setBankkey(sv.bankkey02);
		String referenceNumber=CreditCardUtility.getInstance().getReference(sv.bankacckey02.toString().trim());
		if(referenceNumber==null || referenceNumber.trim().isEmpty()){
			clblIO.setBankacckey(sv.bankacckey02);
		} else {
			clblIO.setBankacckey(referenceNumber);
		}
		clblIO.setClntcoy(mandIO.getPayrcoy());
		clblIO.setClntnum(mandIO.getPayrnum());
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		sv.bankaccdsc02.set(clblIO.getBankaccdsc());
		sv.bnkcurr02.set(clblIO.getCurrcode());
	
		descIO.setDataKey(SPACES);
		sv.facthous.set(clblIO.getFacthous());
		descIO.setDescitem(clblIO.getFacthous());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3684);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
				&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.fcthsedsc02.fill(" ");
		} else {
			sv.fcthsedsc02.set(descIO.getLongdesc());
		}
	
		if (isEQ(chdrpf.getReqntype(), "C")) {
			decryption4600();
		}
	
	}
	
	
	
	protected void decryption4600()
		{
			start4700();
		}
	
	protected void start4700()
		{
			/* Check if the Encryption/Decryption routine is setup in          */
			/* the TR29U Table.                                                */
			/*itemIO.setDataKey(SPACES);
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tr29u);
			itemIO.setItemitem(wsspcomn.fsuco);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			tr29urec.tr29uRec.set(itemIO.getGenarea());*/
			/*   Calculate the length of credit card number                    */
			ix.set(20);
			while ( !(isNE(subString(sv.bankacckey02, ix, 1), SPACES))) {
				compute(ix, 0).set(sub(ix, 1));
			}
			
			/*   Propmt error when trying yo display the encrypted credit      */
			/*   card number while TR29U-SUBRNAME is set to spaces.            
			if (isEQ(tr29urec.subrname, SPACES)
			&& isNE(subString(sv.bankacckey02, 1, ix), NUMERIC)) {
				syserrrec.statuz.set(rfk5);
				syserrrec.params.set(tr29u);
				fatalError600();
			}*/
			/* Do credit card number decryption before display                 */
			/* No need to do decryption for not encrypted credit card          */
			/* number data although TR29U-SUBRNAME is not set to SPACES        
			if (isNE(tr29urec.subrname, SPACES)
			&& isNE(subString(sv.bankacckey02, 1, ix), NUMERIC)) {
				cipherrec.lfunc.set("DECR");
				cipherrec.lsrc.set(sv.bankacckey02);
				callTr29uSubrname1600();
				sv.bankacckey02.set(cipherrec.lrcv);
			}*/
			/*   Determine if user can view the full value of credit card      */
			/*   number when in enquiry mode                                   */
			usrcypirec.rec.set(SPACES);
			usrcypirec.func.set("CHCK");
			usrcypirec.userid.set(wsspcomn.userid);
			callProgram(Usrcypind.class, usrcypirec.rec);
			if (!(isEQ(usrcypirec.indic, "Y")
			&& isEQ(usrcypirec.statuz, "****"))) {
				/*cipherrec.lfunc.set("MASK");
				cipherrec.lsrc.set(sv.bankacckey02);
				callTr29uSubrname1600();
				sv.bankacckey02.set(cipherrec.lrcv);*/
				if(!StringUtil.isEmptyOrSpace(sv.bankacckey02.toString()))
                {
				String masked = MaskingUtil.getInstance().maskString(sv.bankacckey02.trim(),
						"************xxxx", 'x', '*', false);/* IJTI-1523 */
                      sv.bankacckey02.set(masked);  
                }
			}
		}
//ILIFE-2472-END
protected void bankCode1020()
	{
		if (isEQ(sv.bankkey, SPACES)) {
			goTo(GotoLabel.assigneeName1030);
		}
		/*    Obtain the Bank Code Description from BABR.*/
		/*MOVE CHDRENQ-BANKKEY        TO BABR-BANKKEY.                 */
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		sv.bankdesc.set(babrIO.getBankdesc());
		/*    Obtain the Bank Account description from CLBL.*/
		/*MOVE CHDRENQ-BANKKEY        TO CLBL-BANKKEY.                 */
		/*MOVE CHDRENQ-BANKACCKEY     TO CLBL-BANKACCKEY.              */
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);

		if(isEQ(chdrpf.getMandref(),SPACES))
		{
			clblIO.setClntcoy(chdrpf.getCowncoy());
			clblIO.setClntnum(chdrpf.getCownnum());
		}
		else
		{
			mandIO.setDataKey(SPACES);
			mandIO.setPayrcoy(chdrpf.getCowncoy());
			if(isEQ(chdrpf.getPayrnum(),SPACES)) {
				mandIO.setPayrnum(chdrpf.getCownnum()); /* IJTI-1738 */
			}
			else {
				mandIO.setPayrnum(chdrpf.getPayrnum());
			}
			mandIO.setMandref(chdrpf.getMandref());
			mandIO.setFormat(formatsInner.mandrec);
			mandIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, mandIO);
			if (isNE(mandIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(mandIO.getParams());
				syserrrec.statuz.set(mandIO.getStatuz());
				fatalError600();
			}	
			
			clblIO.setClntcoy(mandIO.getPayrcoy());
			clblIO.setClntnum(mandIO.getPayrnum());
		}
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		/*    MOVE CLBL-BANKACCDSC        TO S6354-BNKCURR.                */
		sv.bnkcurr.set(clblIO.getCurrcode());
		/* If the contract is paid by Direct Debit obtain the   <V76F13>*/
		/* Factoring House details from T3684 using the Factorin<V76F13>*/
		/* House from CLBL.                                     <V76F13>*/
		/*    If the contract is paid by Direct Debit or Credit Card,      */
		/*    obtain the Factoring House details from T3684 using the      */
		/*    Factoring House from CLBL.                                   */
		/*    IF CHDRENQ-BILLCHNL          = 'B'                      <004>*/
		if (isNE(t3620rec.ddind, SPACES)
		|| isNE(t3620rec.crcind, SPACES)) {
			descIO.setDataKey(SPACES);
			sv.facthous.set(clblIO.getFacthous());
			descIO.setDescitem(clblIO.getFacthous());
			descIO.setDescpfx("IT");
			/*   MOVE WSSP-COMPANY        TO DESC-DESCCOY             <004>*/
			descIO.setDesccoy(wsspcomn.fsuco);
			descIO.setDesctabl(t3684);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if ((isNE(descIO.getStatuz(), varcom.oK))
			&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
				sv.fcthsedsc.fill(" ");
			}
			else {
				sv.fcthsedsc.set(descIO.getLongdesc());
			}
		}
		/* If the contract is paid by Direct Debit obtain the   <V76F13>*/
		/* Mandate Statuz  details from T3678.                  <V76F13>*/
		/*    If the contract is paid by Direct Debit or Credit Card       */
		/*    obtain the Mandate Statuz  details from T3678.               */
		/*    IF CHDRENQ-BILLCHNL          = 'B'                      <004>*/
		if (isNE(t3620rec.ddind, SPACES)
		|| isNE(t3620rec.crcind, SPACES)) {
			descIO.setDataKey(SPACES);
			descIO.setDescitem(mandIO.getMandstat());
			descIO.setDescpfx("IT");
			/*       MOVE WSSP-COMPANY        TO DESC-DESCCOY                  */
			descIO.setDesccoy(wsspcomn.fsuco);
			descIO.setDesctabl(t3678);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if ((isNE(descIO.getStatuz(), varcom.oK))
			&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
				sv.manstatdsc.fill(" ");
			}
			else {
				sv.manstatdsc.set(descIO.getLongdesc());
			}
			sv.mandstat.set(mandIO.getMandstat());
		}
		/* Decryption or masking for credit card number.                   */
		if (isNE(t3620rec.crcind, SPACES)) {
			decryption1500();
		}
		/*    If the GRUPKEY is not equal to SPACES on the contract        */
		/*    header, place a '+' in the group details indicator.          */
		if (isEQ(chdrpf.getGrupkey(), SPACES)) {
			sv.grpind.set(SPACES);
		}
		else {
			sv.grpind.set("+");
		}
	}

	/**
	* <pre>
	*    Obtain the names of the other lives on the contract header.
	* </pre>
	*/
protected void assigneeName1030()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		asgnenqIO.setDataArea(SPACES);
		asgnenqIO.setChdrcoy(chdrpf.getChdrcoy());
		asgnenqIO.setChdrnum(chdrpf.getChdrnum());
		asgnenqIO.setSeqno(ZERO);
		asgnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		asgnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		asgnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, asgnenqIO);
		if (isNE(asgnenqIO.getStatuz(), varcom.oK)
		&& isNE(asgnenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(asgnenqIO.getParams());
			fatalError600();
		}
		if (isEQ(asgnenqIO.getStatuz(), varcom.oK)
		&& isEQ(asgnenqIO.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(asgnenqIO.getChdrnum(), chdrpf.getChdrnum())) {
			sv.asgnind.set("+");
		}
		else {
			sv.asgnind.set(SPACES);
		}
	}

	/**
	* <pre>
	*****IF   S6354-ASGNNUM           = SPACES
	*****     GO TO 1040-DESPATCH-NAME.
	*****MOVE WSSP-FSUCO             TO CLTS-CLNTCOY.
	*****MOVE S6354-ASGNNUM          TO CLTS-CLNTNUM.
	*****MOVE 'CN'                   TO CLTS-CLNTPFX.
	*****MOVE READR                  TO CLTS-FUNCTION.
	*****CALL 'CLTSIO'            USING CLTS-PARAMS.
	*****IF   CLTS-STATUZ         NOT = O-K
	*****     MOVE CLTS-PARAMS       TO SYSR-PARAMS
	*****     PERFORM 600-FATAL-ERROR.
	*****PERFORM PLAINNAME.
	*****MOVE WSSP-LONGCONFNAME      TO S6354-ASGNNAME.
	*****MOVE CLTS-CLTADDR01         TO S6354-ASGNADDR-01.
	*****MOVE CLTS-CLTADDR02         TO S6354-ASGNADDR-02.
	*****MOVE CLTS-CLTADDR03         TO S6354-ASGNADDR-03.
	*****MOVE CLTS-CLTADDR04         TO S6354-ASGNADDR-04.
	*****MOVE CLTS-CLTADDR05         TO S6354-ASGNADDR-05.
	*****MOVE CLTS-CLTPCODE          TO S6354-ASGNPCODE.
	*    Check for contract beneficiaries
	* </pre>
	*/
protected void beneficiary1035()
	{
		bnfyenqIO.setDataArea(SPACES);
		bnfyenqIO.setChdrcoy(chdrpf.getChdrcoy());
		bnfyenqIO.setChdrnum(chdrpf.getChdrnum());
		bnfyenqIO.setBnyclt(SPACES);
		bnfyenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bnfyenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bnfyenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, bnfyenqIO);
		if ((isNE(bnfyenqIO.getStatuz(), varcom.oK))
		&& (isNE(bnfyenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(bnfyenqIO.getParams());
			fatalError600();
		}
		if ((isEQ(bnfyenqIO.getStatuz(), varcom.oK))
		&& (isEQ(bnfyenqIO.getChdrcoy(), chdrpf.getChdrcoy()))
		&& (isEQ(bnfyenqIO.getChdrnum(), chdrpf.getChdrnum()))) {
			sv.conben.set("+");
		}
		else {
			sv.conben.set(SPACES);
		}
	}

protected void trustee1036()
	{
		/* Check for trustee                                               */
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		ctrsIO.setDataKey(SPACES);
		ctrsIO.setChdrcoy(chdrpf.getChdrcoy());
		ctrsIO.setChdrnum(chdrpf.getChdrnum());
		ctrsIO.setSeqno(ZERO);
		ctrsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ctrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ctrsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, ctrsIO);
		if ((isNE(ctrsIO.getStatuz(), varcom.oK))
		&& (isNE(ctrsIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(ctrsIO.getParams());
			fatalError600();
		}
		if ((isNE(ctrsIO.getChdrcoy(), chdrpf.getChdrcoy()))
		|| (isNE(ctrsIO.getChdrnum(), chdrpf.getChdrnum()))
		|| (isEQ(ctrsIO.getStatuz(), varcom.endp))) {
			sv.ctrsind.set(SPACES);
		}
		else {
			sv.ctrsind.set("+");
		}
	}

	/**
	* <pre>
	*    Obtain the loan details.
	* </pre>
	*/
protected void loanDetails1037()
	{
		loanIO.setDataArea(SPACES);
		loanIO.setChdrcoy(chdrpf.getChdrcoy());
		loanIO.setChdrnum(chdrpf.getChdrnum());
		loanIO.setLoanNumber(ZERO);
		loanIO.setFormat(formatsInner.loanrec);
		loanIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)
		&& isNE(loanIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanIO.getParams());
			fatalError600();
		}
		if (isEQ(loanIO.getStatuz(), varcom.oK)
		&& isEQ(loanIO.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(loanIO.getChdrnum(), chdrpf.getChdrnum())) {
			sv.hlndetails.set("+");
		}
		else {
			sv.hlndetailsOut[varcom.pr.toInt()].set("Y");
			sv.hlndetails.set(SPACES);
		}
		/* Check for policy despatch details.                              */
		checkHpad1200();
	}

protected void despatchName1040()
	{
		desaddFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),DESADD_FEATURE_ID, appVars, "IT");
		if (!desaddFlag) {
			sv.ctrycodeOut[varcom.nd.toInt()].set("Y");
			sv.cntryStateOut[varcom.nd.toInt()].set("Y");
		}
		
		if (isEQ(sv.despnum, SPACES)) {
			goTo(GotoLabel.contractType1050);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.despnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.despname.set(wsspcomn.longconfname);
		sv.despaddr01.set(cltsIO.getCltaddr01());
		sv.despaddr02.set(cltsIO.getCltaddr02());
		sv.despaddr03.set(cltsIO.getCltaddr03());
		sv.despaddr04.set(cltsIO.getCltaddr04());
		sv.despaddr05.set(cltsIO.getCltaddr05());
		sv.desppcode.set(cltsIO.getCltpcode());
		
		//ILIFE-7407
		if(desaddFlag) {
			sv.ctrycode.set(cltsIO.getCtrycode()); //ILIFE-7407
			sv.cntryState.set(cltsIO.getClntStateCd()); //ILIFE-7407
			sv.ctrycodeOut[varcom.nd.toInt()].set("N");
			sv.cntryStateOut[varcom.nd.toInt()].set("N");
		}
		//ILIFE-7407
	}

	/**
	* <pre>
	*    Obtain the Contract Type description from T5688.
	* </pre>
	*/
protected void contractType1050()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill(" ");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill(" ");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill(" ");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Here we need to read table T5729 to discover if*/
		/* we are enquiring on a flexible premium contract.*/
		/* If so we enable field PRMDETAILS.*/
		/*  Read T5729.*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5729)
		|| isNE(itdmIO.getItemitem(), chdrpf.getCnttype())) {
			sv.prmdetailsOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkHpad1200()
	{
		begin1210();
	}

protected void begin1210()
	{
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(chdrpf.getChdrcoy());
		hpadIO.setChdrnum(chdrpf.getChdrnum());
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		if (isNE(hpadIO.getDlvrmode(), SPACES)
		|| (isNE(hpadIO.getDespdate(), varcom.vrcmMaxDate)
		&& isNE(hpadIO.getDespdate(), ZERO))) {
			sv.ind.set("+");
			sv.indOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.ind.set(SPACES);
			sv.indOut[varcom.pr.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void readClntRole1100()
	{
		readClntRolePara1100();
	}

protected void readClntRolePara1100()
	{
		/* Read the client role file to get the payer number.              */
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		}else{ // validation empty strings
			String firstName =  cltsIO.getGivname().toString();
			String lastName =  cltsIO.getSurname().toString();
			prepareName(firstName, lastName);
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else if (isEQ(cltsIO.getEthorig(), "1")) {
			String firstName =  cltsIO.getGivname().toString();
			String lastName =  cltsIO.getSurname().toString();
			String salute =  cltsIO.getSalutl().toString();
			String delimiter = "";
			
			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = getStringUtil().includeSalute(salute, fullName);
			wsspcomn.longconfname.set(fullNameWithSalute);

		} else { 
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(cltsIO.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(cltsIO.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
		/* PAYEE-EXIT */
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		String delimiter = ""; // delimiter already has extra space
		String firstName =  cltsIO.getLgivname().toString();
		String lastName =  cltsIO.getLsurname().toString();
		
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);

		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void decryption1500()
	{
		start1510();
	}

protected void start1510()
	{
		/* Check if the Encryption/Decryption routine is setup in          */
		/* the TR29U Table.                                                */
		/*itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tr29u);
		itemIO.setItemitem(wsspcomn.fsuco);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr29urec.tr29uRec.set(itemIO.getGenarea());*/
		/*   Calculate the length of credit card number                    */
		ix.set(20);
		while ( !(isNE(subString(sv.bankacckey, ix, 1), SPACES))) {
			compute(ix, 0).set(sub(ix, 1));
		}

		/*   Propmt error when trying yo display the encrypted credit      */
		/*   card number while TR29U-SUBRNAME is set to spaces.            
		if (isEQ(tr29urec.subrname, SPACES)
		&& isNE(subString(sv.bankacckey, 1, ix), NUMERIC)) {
			syserrrec.statuz.set(rfk5);
			syserrrec.params.set(tr29u);
			fatalError600();
		}*/
		/* Do credit card number decryption before display                 */
		/* No need to do decryption for not encrypted credit card          */
		/* number data although TR29U-SUBRNAME is not set to SPACES        
		if (isNE(tr29urec.subrname, SPACES)
		&& isNE(subString(sv.bankacckey, 1, ix), NUMERIC)) {
			cipherrec.lfunc.set("DECR");
			cipherrec.lsrc.set(sv.bankacckey);
			callTr29uSubrname1600();
			sv.bankacckey.set(cipherrec.lrcv);
		}*/
		/*   Determine if user can view the full value of credit card      */
		/*   number when in enquiry mode                                   */
		usrcypirec.rec.set(SPACES);
		usrcypirec.func.set("CHCK");
		usrcypirec.userid.set(wsspcomn.userid);
		callProgram(Usrcypind.class, usrcypirec.rec);
		if (!(isEQ(usrcypirec.indic, "Y")
		&& isEQ(usrcypirec.statuz, "****"))) {
			/*cipherrec.lfunc.set("MASK");
			cipherrec.lsrc.set(sv.bankacckey);
			callTr29uSubrname1600();
			sv.bankacckey.set(cipherrec.lrcv);*/
			if(!StringUtil.isEmptyOrSpace(sv.bankacckey.toString()))
            {
				String masked = MaskingUtil.getInstance().maskString(sv.bankacckey.trim(),
						"************xxxx", 'x', '*', false);/* IJTI-1523 */
                  sv.bankacckey.set(masked);  
            } 
		}
	}

protected void callTr29uSubrname1600()
	{
		/*START*/
		/*cipherrec.lrcv.set(SPACES);
		cipherrec.lsts.set(SPACES);
		callProgram(tr29urec.subrname, cipherrec.lfunc, cipherrec.lsrc, cipherrec.lrcv, cipherrec.lsts);
		if (isNE(cipherrec.lsts, varcom.oK)) {
			syserrrec.statuz.set(cipherrec.lsts);
			syserrrec.params.set(cipherrec.lfunc);
			fatalError600();
		}*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S6354IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6354-DATA-AREA.                       */
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/*    Validate fields*/
		if (isNE(sv.conben, " ")
		&& isNE(sv.conben, "+")
		&& isNE(sv.conben, "X")) {
			sv.conbenErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ctrsind, " ")
		&& isNE(sv.ctrsind, "+")
		&& isNE(sv.ctrsind, "X")) {
			sv.ctrsindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.asgnind, " ")
		&& isNE(sv.asgnind, "+")
		&& isNE(sv.asgnind, "X")) {
			sv.asgnindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.prmdetails, " ")
		&& isNE(sv.prmdetails, "+")
		&& isNE(sv.prmdetails, "X")) {
			sv.prmdetailsErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.hlndetails, " ")
		&& isNE(sv.hlndetails, "+")
		&& isNE(sv.hlndetails, "X")) {
			sv.hlndetailsErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		/*    If Group Details indicator has a wrong value, display        */
		/*    error message.                                               */
		if (isNE(sv.grpind, "X")
		&& isNE(sv.grpind, "+")
		&& isNE(sv.grpind, " ")) {
			sv.grpindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		/*    If Group Details has been selected but there are no          */
		/*    details on the Contract Header, display error message.       */
		/*                                                         <V71L04>*/
		if (isEQ(sv.grpind, "X")
		&& isEQ(chdrpf.getGrupkey(), SPACES)) {
			sv.grpindErr.set(f973);
			wsspcomn.edterror.set("Y");
			sv.grpind.set(SPACES);
		}
		/* Validate policy dispatch                                        */
		if (isNE(sv.ind, " ")
		&& isNE(sv.ind, "+")
		&& isNE(sv.ind, "X")) {
			sv.indErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*PARA*/
		/*    There is no updating required in this program.*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para4000();
				case gensww4010:
					gensww4010();
				case nextProgram4020:
					nextProgram4020();
				case exit4090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		gensswrec.function.set(SPACES);
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.nextProgram4020);
		}
		if (isEQ(scrnparams.statuz, varcom.oK)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		&& isNE(sv.conben, "X")
		&& isNE(sv.ind, "X")
		&& isNE(sv.asgnind, "X")
		&& isNE(sv.grpind, "X")
		&& isNE(sv.prmdetails, "X")
		&& isNE(sv.hlndetails, "X")
		&& isNE(sv.ctrsind, "X")
		&& isNE(sv.loyaltyind, "X")) {
			goTo(GotoLabel.nextProgram4020);
		}
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4200();
			}
		}
		/*    If the indicators has been selected, (value - 'X'), then*/
		/*    set an asterisk in the program stack action field to ensure*/
		/*    that control returns here, set the parameters for*/
		/*    generalised secondary switching and save the original*/
		/*    programs from the program stack.*/
		if (isEQ(sv.conben, "X")
		|| isEQ(sv.ind, "X")
		|| isEQ(sv.asgnind, "X")
		|| isEQ(sv.grpind, "X")
		|| isEQ(sv.prmdetails, "X")
		|| isEQ(sv.hlndetails, "X")
		|| isEQ(sv.ctrsind, "X")
		|| isEQ(sv.loyaltyind, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4300();
			}
		}
		/*    If Contract Beneficiaries has been selected set 'A' in the*/
		/*    function. If it was selected previously set it to '+'.*/
		/*    If Contract ASSIGNEES has been selected set 'B' in the       */
		/*    function. If it was selected previously set it to '+'.       */
		/* IF   S6354-CONBEN            = '?'                           */
		/*      MOVE '+'               TO S6354-CONBEN.                 */
		if (isEQ(sv.conben, "X")) {
			sv.conben.set("?");
			/*      MOVE '*'               TO WSSP-SEC-ACTN                 */
			/*                               (WSSP-PROGRAM-PTR)             */
			gensswrec.function.set("A");
			/*      PERFORM 4100-SETUP-GENSWCH-REC.                    <002>*/
			goTo(GotoLabel.gensww4010);
		}
		/*      IF GENS-STATUZ = MRNF      GO TO 4090-EXIT.             */
		if (isEQ(sv.conben, "?")) {
			sv.conben.set("+");
		}
		/* IF   S6354-ASGNIND           = '?'                      <002>*/
		/*      MOVE '+'               TO S6354-ASGNIND.           <002>*/
		if (isEQ(sv.asgnind, "X")) {
			sv.asgnind.set("?");
			/*      MOVE '*'               TO WSSP-SEC-ACTN            <002>*/
			/*                               (WSSP-PROGRAM-PTR)        <002>*/
			gensswrec.function.set("B");
			/*      PERFORM 4100-SETUP-GENSWCH-REC.                    <002>*/
			goTo(GotoLabel.gensww4010);
		}
		/*      IF GENS-STATUZ = MRNF      GO TO 4090-EXIT.             */
		if (isEQ(sv.asgnind, "?")) {
			sv.asgnind.set("+");
		}
		/*    If Group Details has been selected set 'C' in the            */
		/*    function. If it was selected previously set it to '+'.       */
		/* IF   S6354-GRPIND           = '?'                       <010>*/
		/*      MOVE '+'               TO S6354-GRPIND             <010>*/
		/* END-IF.                                                 <010>*/
		if (isEQ(sv.grpind, "X")) {
			sv.grpind.set("?");
			/*      MOVE '*'               TO WSSP-SEC-ACTN            <010>*/
			/*                               (WSSP-PROGRAM-PTR)        <010>*/
			gensswrec.function.set("C");
			/*      PERFORM 4100-SETUP-GENSWCH-REC                     <010>*/
			goTo(GotoLabel.gensww4010);
			/*****      IF GENS-STATUZ         = MRNF                      <010>*/
			/*****         GO TO               4090-EXIT                   <010>*/
			/*****      END-IF                                             <010>*/
		}
		if (isEQ(sv.grpind, "?")) {
			sv.grpind.set("+");
		}
		/*    If Premium Details has been selected set 'C' in the*/
		/*    function. If it was selected previously set it to '+'*/
		if (isEQ(sv.prmdetails, "X")) {
			sv.prmdetails.set("?");
			gensswrec.function.set("D");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.prmdetails, "?")) {
			sv.prmdetails.set("+");
		}
		/* Check if trustee selected previously.                           */
		/* If trustee selected, set 'F' in the function.                   */
		if (isEQ(sv.ctrsind, "X")) {
			sv.ctrsind.set("?");
			gensswrec.function.set("F");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.ctrsind, "?")) {
			sv.ctrsind.set("+");
		}
		/*    If Loan Details has been selected set 'E' in the             */
		/*    function. If it was selected previously set it to '+'        */
		if (isEQ(sv.hlndetails, "X")) {
			sv.hlndetails.set("?");
			gensswrec.function.set("E");
			/* Pass today as Effective Date                                    */
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsspcomn.currfrom.set(datcon1rec.intDate);
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.hlndetails, "?")) {
			sv.hlndetails.set("+");
		}
		/*                                                         <LA2109>*/
		if (isEQ(sv.ind, "X")) {
			sv.ind.set("?");
			gensswrec.function.set("G");
			wsspcomn.flag.set("I");
			wsspcomn.chdrChdrcoy.set(chdrpf.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.ind, "?")) {
			sv.ind.set("+");
		}
		if (isEQ(sv.loyaltyind, "X")) {
			sv.loyaltyind.set("?");
			gensswrec.function.set("H");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.loyaltyind, "?")) {
			sv.loyaltyind.set("+");
		}
	}

protected void gensww4010()
	{
		if (isEQ(gensswrec.function, SPACES)) {
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator              */
		/* with its initial load value                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		/*ELSE                                                         */
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4400();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void setupGenswchRec4100()
	{
		para4100();
	}

protected void para4100()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			saveProgramStack4300();
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*ELSE                                                         */
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			loadProgramStack4400();
		}
	}

protected void restoreProgram4200()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4300()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4400()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}



public Chdrpf getChdrpf() {
		return chdrpf;
	}

	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData mandrec = new FixedLengthStringData(10).init("MANDREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
	private FixedLengthStringData loanrec = new FixedLengthStringData(10).init("LOANREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
}

public CltsTableDAM getCltsIO() {
	return cltsIO;
}

public void setCltsIO(CltsTableDAM cltsIO) {
	this.cltsIO = cltsIO;
}


	protected void prepareName(String firstName, String lastName) {
		String fullName = "";
		if (isNE(firstName, SPACES)) {
			fullName = getStringUtil().plainName(firstName, lastName, ",");
			 
		} else {
			fullName = lastName;
		}
		
		wsspcomn.longconfname.set(fullName);

	}

	/**
	 * can be overriden to change the its behaviour
	 * @return
	 */
	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}

	
}
