package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6685
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class S6685ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(91);
	public FixedLengthStringData dataFields = new FixedLengthStringData(43).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 43);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 55);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	//ILIFE-1530 STARTS
	public FixedLengthStringData subfileArea = new FixedLengthStringData(209);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(79).isAPartOf(subfileArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData elemdesc = DD.compdesc.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData hrgpynum = DD.hrgpynum.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData shortdesc = DD.shrtdesc.copy().isAPartOf(subfileFields,69);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 79);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData elemdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hrgpynumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData shrtdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 111);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] elemdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hrgpynumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] shrtdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 207);
	//ILIFE-1530 ENDS
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6685screensflWritten = new LongData(0);
	public LongData S6685screenctlWritten = new LongData(0);
	public LongData S6685screenWritten = new LongData(0);
	public LongData S6685protectWritten = new LongData(0);
	public GeneralTable s6685screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6685screensfl;
	}

	public S6685ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"02","04","-02","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverageOut,new String[] {null, null, null, "06",null, null, null, null, null, null, null, null});
		fieldIndMap.put(riderOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hrgpynum, hcrtable, action, life, coverage, rider, shortdesc, elemdesc};
		screenSflOutFields = new BaseData[][] {hrgpynumOut, hcrtableOut, actionOut, lifeOut, coverageOut, riderOut, shrtdescOut, elemdescOut};
		screenSflErrFields = new BaseData[] {hrgpynumErr, hcrtableErr, actionErr, lifeErr, coverageErr, riderErr, shrtdescErr, elemdescErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrsel, cnttype, ctypedes};
		screenOutFields = new BaseData[][] {chdrselOut, cnttypeOut, ctypedesOut};
		screenErrFields = new BaseData[] {chdrselErr, cnttypeErr, ctypedesErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenActionVar = action;
		screenRecord = S6685screen.class;
		screenSflRecord = S6685screensfl.class;
		screenCtlRecord = S6685screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6685protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6685screenctl.lrec.pageSubfile);
	}
}
