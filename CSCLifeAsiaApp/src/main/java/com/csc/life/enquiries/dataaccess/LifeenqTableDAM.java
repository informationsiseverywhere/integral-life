package com.csc.life.enquiries.dataaccess;

import com.csc.life.newbusiness.dataaccess.LifepfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;


/**
 * 	
 * File: LifeenqTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:34
 * Class transformed from LIFEENQ.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LifeenqTableDAM extends LifepfTableDAM {

	public LifeenqTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LIFEENQ");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", JLIFE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "LIFCNUM, " +
		            "SMOKING, " +
		            "OCCUP, " +
		            "PURSUIT01, " +
		            "PURSUIT02, " +
		            "VALIDFLAG, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "JLIFE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "JLIFE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               lifcnum,
                               smoking,
                               occup,
                               pursuit01,
                               pursuit02,
                               validflag,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(243);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(jlife.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(40);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ getLifcnum().toInternal()
					+ getSmoking().toInternal()
					+ getOccup().toInternal()
					+ getPursuit01().toInternal()
					+ getPursuit02().toInternal()
					+ getValidflag().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, lifcnum);
			what = ExternalData.chop(what, smoking);
			what = ExternalData.chop(what, occup);
			what = ExternalData.chop(what, pursuit01);
			what = ExternalData.chop(what, pursuit02);
			what = ExternalData.chop(what, validflag);	
			what = ExternalData.chop(what, datime);	
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(Object what) {
		lifcnum.set(what);
	}	
	public FixedLengthStringData getSmoking() {
		return smoking;
	}
	public void setSmoking(Object what) {
		smoking.set(what);
	}	
	public FixedLengthStringData getOccup() {
		return occup;
	}
	public void setOccup(Object what) {
		occup.set(what);
	}	
	public FixedLengthStringData getPursuit01() {
		return pursuit01;
	}
	public void setPursuit01(Object what) {
		pursuit01.set(what);
	}	
	public FixedLengthStringData getPursuit02() {
		return pursuit02;
	}
	public void setPursuit02(Object what) {
		pursuit02.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getPursuits() {
		return new FixedLengthStringData(pursuit01.toInternal()
										+ pursuit02.toInternal());
	}
	public void setPursuits(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPursuits().getLength()).init(obj);
	
		what = ExternalData.chop(what, pursuit01);
		what = ExternalData.chop(what, pursuit02);
	}
	public FixedLengthStringData getPursuit(BaseData indx) {
		return getPursuit(indx.toInt());
	}
	public FixedLengthStringData getPursuit(int indx) {

		switch (indx) {
			case 1 : return pursuit01;
			case 2 : return pursuit02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPursuit(BaseData indx, Object what) {
		setPursuit(indx.toInt(), what);
	}
	public void setPursuit(int indx, Object what) {

		switch (indx) {
			case 1 : setPursuit01(what);
					 break;
			case 2 : setPursuit02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		jlife.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		lifcnum.clear();
		smoking.clear();
		occup.clear();
		pursuit01.clear();
		pursuit02.clear();
		validflag.clear();		
		datime.clear();
	}


}