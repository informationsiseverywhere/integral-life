package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR596
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr596ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(128);
	public FixedLengthStringData dataFields = new FixedLengthStringData(48).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public ZonedDecimalData zrsacprd = DD.zrsacprd.copyToZonedDecimal().isAPartOf(dataFields,44);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 48);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData zrsacprdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 68);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] zrsacprdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr596screenWritten = new LongData(0);
	public LongData Sr596protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr596ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, zrsacprd};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, zrsacprdOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, zrsacprdErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr596screen.class;
		protectRecord = Sr596protect.class;
	}

}
