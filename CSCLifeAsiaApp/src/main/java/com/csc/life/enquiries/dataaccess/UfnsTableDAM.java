package com.csc.life.enquiries.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UfnsTableDAM.java
 * Date: Sun, 30 Aug 2009 03:50:56
 * Class transformed from UFNS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UfnsTableDAM extends UfnspfTableDAM {

	public UfnsTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UFNS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "COMPANY"
		             + ", VRTFUND"
		             + ", UNITYP";
		
		QUALIFIEDCOLUMNS = 
		            "COMPANY, " +
		            "VRTFUND, " +
		            "UNITYP, " +
		            "NOFUNTS, " +
		            "JOBNO, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "COMPANY ASC, " +
		            "VRTFUND ASC, " +
		            "UNITYP ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "COMPANY DESC, " +
		            "VRTFUND DESC, " +
		            "UNITYP DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               company,
                               virtualFund,
                               unitType,
                               nofunts,
                               jobno,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(250);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getCompany().toInternal()
					+ getVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, virtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(company.toInternal());
	nonKeyFiller20.setInternal(virtualFund.toInternal());
	nonKeyFiller30.setInternal(unitType.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(66);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getNofunts().toInternal()
					+ getJobno().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nofunts);
			what = ExternalData.chop(what, jobno);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public FixedLengthStringData getVirtualFund() {
		return virtualFund;
	}
	public void setVirtualFund(Object what) {
		virtualFund.set(what);
	}
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getNofunts() {
		return nofunts;
	}
	public void setNofunts(Object what) {
		setNofunts(what, false);
	}
	public void setNofunts(Object what, boolean rounded) {
		if (rounded)
			nofunts.setRounded(what);
		else
			nofunts.set(what);
	}	
	public PackedDecimalData getJobno() {
		return jobno;
	}
	public void setJobno(Object what) {
		setJobno(what, false);
	}
	public void setJobno(Object what, boolean rounded) {
		if (rounded)
			jobno.setRounded(what);
		else
			jobno.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		company.clear();
		virtualFund.clear();
		unitType.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nofunts.clear();
		jobno.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}