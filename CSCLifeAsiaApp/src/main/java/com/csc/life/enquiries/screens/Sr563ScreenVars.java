package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR563
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr563ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnPremStat = DD.cnpstat.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnRiskStat = DD.cnrstat.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,15);
	public ZonedDecimalData coverc = DD.coverc.copyToZonedDecimal().isAPartOf(dataFields,18);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData mbnkref = DD.mbnkref.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData mlinsopt = DD.mlinsopt.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData mlresind = DD.mlresind.copy().isAPartOf(dataFields,77);
	public ZonedDecimalData prat = DD.prat.copyToZonedDecimal().isAPartOf(dataFields,80);
	public FixedLengthStringData prmdetails = DD.prmdetails.copy().isAPartOf(dataFields,86);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,100);
	public ZonedDecimalData loandur = DD.loandur.copyToZonedDecimal().isAPartOf(dataFields,110);//BRD-139
	public FixedLengthStringData intcaltype = DD.intCalType.copy().isAPartOf(dataFields,113);//BRD-139
	public FixedLengthStringData mlresindvpms = DD.mlresindvpms.copy().isAPartOf(dataFields,133);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnpstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnrstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData covercErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData mbnkrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData mlinsoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData mlresindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData pratErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData prmdetailsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData loandurErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);//BRD-139
	public FixedLengthStringData intcaltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData mlresindvpmsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,68);//BRD-139
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getErrorIndicatorSize()+getDataFieldsSize());
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnpstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnrstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] covercOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] mbnkrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] mlinsoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] mlresindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] pratOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] prmdetailsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] loandurOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);//BRD-139
	public FixedLengthStringData[] intcaltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);//BRD-139
	public FixedLengthStringData[] mlresindvpmsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr563screenWritten = new LongData(0);
	public LongData Sr563protectWritten = new LongData(0);
	public FixedLengthStringData mrtaFlag = new FixedLengthStringData(1);//BRD-139
	public boolean hasSubfile() {
		return false;
	}


	public Sr563ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(pratOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmdetailsOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlinsoptOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlresindOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(covercOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(loandurOut,new String[] {null,null, null,"06", null, null, null, null, null, null, null, null});
		fieldIndMap.put(intcaltypeOut,new String[] {null,null, null,"07", null, null, null, null, null, null, null, null});		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr563screen.class;
		protectRecord = Sr563protect.class;
	}
	
	
	public int getDataAreaSize() {
		return 424;
	}
	

	public int getDataFieldsSize(){
		return 136;  
	}
	public int getErrorIndicatorSize(){
		return 72; 
	}
	
	public int getOutputFieldSize(){
		return 216; 
	}


	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, cnttype, descrip, cntcurr, cnRiskStat, rstate, register, cnPremStat, pstate, prat, prmdetails, mlinsopt, mlresind, coverc, mbnkref, loandur, intcaltype,mlresindvpms};
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, cnttypeOut, descripOut, cntcurrOut, cnrstatOut, rstateOut, registerOut, cnpstatOut, pstateOut, pratOut, prmdetailsOut, mlinsoptOut, mlresindOut, covercOut, mbnkrefOut, loandurOut, intcaltypeOut,mlresindvpmsOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, cnttypeErr, descripErr, cntcurrErr, cnrstatErr, rstateErr, registerErr, cnpstatErr, pstateErr, pratErr, prmdetailsErr, mlinsoptErr, mlresindErr, covercErr, mbnkrefErr, loandurErr, intcaltypeErr,mlresindvpmsErr};
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {};
	}

}
