/*
 * File: Pr586.java
 * Date: 30 August 2009 1:47:58
 * Author: Quipoz Limited
 * 
 * Class transformed from PR586.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.life.enquiries.dataaccess.UndrprpTableDAM;
import com.csc.life.enquiries.screens.Sr586ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart.recordstructures.Varcom;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
/* start TMLII-429 UW-01-008 */
import com.csc.smart.recordstructures.Batckey;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This program accepts the client number from P6221 submenu
* and read its related records from UNDRPRP logical file for
* accumulation by Coy/Curr/Rsk Cls and then display them
*
* Further detailed enquiry could be done if a '1' is entered
* at the 'Select' column of the relevant subfile line and then
* the control will pass to PR587 for more detailed information
*
*****************************************************************
* </pre>
*/
public class Pr586 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private BinaryData wsaaSflct = new BinaryData(5, 0);
	private BinaryData wsaaTotsi = new BinaryData(17, 2);

	private FixedLengthStringData wsaaPresSeq = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaPresClntnum = new FixedLengthStringData(8).isAPartOf(wsaaPresSeq, 0);
	private FixedLengthStringData wsaaPresCoy = new FixedLengthStringData(1).isAPartOf(wsaaPresSeq, 8);
	private FixedLengthStringData wsaaPresCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaPresSeq, 9);
	private FixedLengthStringData wsaaPresLrkcls = new FixedLengthStringData(4).isAPartOf(wsaaPresSeq, 12);

	private FixedLengthStringData wsaaPrevSeq = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaPrevClntnum = new FixedLengthStringData(8).isAPartOf(wsaaPrevSeq, 0);
	private FixedLengthStringData wsaaPrevCoy = new FixedLengthStringData(1).isAPartOf(wsaaPrevSeq, 8);
	private FixedLengthStringData wsaaPrevCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaPrevSeq, 9);
	private FixedLengthStringData wsaaPrevLrkcls = new FixedLengthStringData(4).isAPartOf(wsaaPrevSeq, 12);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR586");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	/* start TMLII-429 UW-01-008 */
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData wsaaValid = new FixedLengthStringData(1);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private String chdrlifrec = "CHDRLIFREC";
	private String t5679 = "T5679";
	private T5679rec t5679rec = new T5679rec();
	private String f321 = "F321";
	/* end TMLII-429 UW-01-008 */
	private String g615 = "G615";
		/* FORMATS */
	private String clntrec = "CLNTREC";
	private String undrprprec = "UNDRPRPREC";

	private Wsspsmart wsspsmart = new Wsspsmart();
	//fixed bug #721
    private FixedLengthStringData filler = new FixedLengthStringData(767).isAPartOf(wsspsmart.userArea, 0, FILLER_REDEFINE);// Updated by Tidy
	private FixedLengthStringData wsspCurrcode = new FixedLengthStringData(3).isAPartOf(filler, 0);
	private FixedLengthStringData wsspLrkcls = new FixedLengthStringData(4).isAPartOf(filler, 3);
	private PackedDecimalData wsspTotsi = new PackedDecimalData(17, 2).isAPartOf(filler, 7);
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*U/W Logical File for Proposal*/
	private UndrprpTableDAM undrprpIO = new UndrprpTableDAM();
	//private Wsspsmart wsspsmart = new Wsspsmart();// Updated by Tidy
	private Sr586ScreenVars sv = ScreenProgram.getScreenVars( Sr586ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		addTotsi1200, 
		exit1200, 
		exit2090, 
		nextSflLine4020, 
		switch4070, 
		exit9000
	}

	public Pr586() {
		super();
		screenVars = sv;
		new ScreenModel("Sr586", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("4000");
			goTo(GotoLabel.exit1090);
		}
		wsaaPresSeq.set(LOVALUES);

		/* start TMLII-429 UW-01-008 */
		wsaaBatckey.set(wsspcomn.batchkey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			scrnparams.errorCode.set(f321);
			return;
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* end TMLII-429 UW-01-008 */
		
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR586", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		initialize(sv.dataFields);
		sv.clntnum.set(wsspsmart.flddkey);
		moveClntnam9100();
		wsaaSflct.set(ZERO);
		undrprpIO.setRecKeyData(SPACES);
		undrprpIO.setClntnum(wsspsmart.flddkey);
		undrprpIO.setCoy(wsspcomn.company);
		undrprpIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undrprpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undrprpIO.setFitKeysSearch("CLNTNUM");
		undrprpIO.setStatuz(varcom.oK);
		while ( !(isEQ(undrprpIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSflct,sv.subfilePage))) {
			loadSubfile1200();
		}
		
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set(optswchrec.optsFunction);
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					ctrl1200();
				}
				case addTotsi1200: {
					addTotsi1200();
				}
				case exit1200: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void ctrl1200()
	{
		callUndrprpio9000();
		/* start TMLII-429 UW-01-008 */
		if(isEQ(undrprpIO.getStatuz(),Varcom.oK) && isNE(wsaaValid,"Y")) {
			return;
		}
		/* end TMLII-429 UW-01-008 */
		if (isEQ(wsaaPresSeq,wsaaPrevSeq)) {
			wsaaTotsi.add(undrprpIO.getSumins());
			goTo(GotoLabel.exit1200);
		}
		if (isEQ(wsaaPrevSeq,LOVALUES)) {
			goTo(GotoLabel.addTotsi1200);
		}
		initialize(sv.subfileFields);
		sv.coy.set(wsaaPrevCoy);
		sv.currcode.set(wsaaPrevCurrcode);
		sv.lrkcls.set(wsaaPrevLrkcls);
		sv.sumins.set(wsaaTotsi);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR586", sv);
		wsaaSflct.add(1);
	}

protected void addTotsi1200()
	{
		if (isEQ(wsaaPresSeq,HIVALUES)) {
			goTo(GotoLabel.exit1200);
		}
		//ILIFE-4237
		if(isEQ(wsaaValid,"Y")){
			wsaaTotsi.set(undrprpIO.getSumins());
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
		/*VALIDATE-SCREEN*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR586", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isNE(wsaaScrnStatuz,varcom.rolu)) {
			goTo(GotoLabel.exit2090);
		}
		wsaaSflct.set(ZERO);
		undrprpIO.setStatuz(varcom.oK);
		while ( !(isEQ(undrprpIO.getStatuz(),varcom.endp)
		|| isGT(wsaaSflct,sv.subfilePage))) {
			loadSubfile1200();
		}
		
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		if (!(isEQ(sv.sel,SPACES)
		|| isEQ(sv.sel,"1"))) {
			sv.selErr.set(g615);
		}
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR586", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR586", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case nextSflLine4020: {
					nextSflLine4020();
				}
				case switch4070: {
					switch4070();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			scrnparams.function.set(varcom.sstrt);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
	}

protected void nextSflLine4020()
	{
		processScreen("SR586", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelOptno.set(ZERO);
			optswchrec.optsSelType.set(SPACES);
			goTo(GotoLabel.switch4070);
		}
		if (isEQ(sv.sel,SPACES)) {
			scrnparams.function.set(varcom.srdn);
			goTo(GotoLabel.nextSflLine4020);
		}
		wsspCurrcode.set(sv.currcode);
		wsspLrkcls.set(sv.lrkcls);
		wsspTotsi.set(sv.sumins);
		optswchrec.optsSelOptno.set(sv.sel);
		optswchrec.optsSelType.set("L");
		sv.sel.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("SR586", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void switch4070()
	{
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set(optswchrec.optsFunction);
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		/*EXIT*/
	}

protected void callUndrprpio9000()
	{
		try {
			ctrl9000();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl9000()
	{
		wsaaPrevSeq.set(wsaaPresSeq);
		undrprpIO.setFormat(undrprprec);
		SmartFileCode.execute(appVars, undrprpIO);
		if (!(isEQ(undrprpIO.getStatuz(),varcom.oK)
		|| isEQ(undrprpIO.getStatuz(),varcom.mrnf)
		|| isEQ(undrprpIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(undrprpIO.getStatuz());
			syserrrec.params.set(undrprpIO.getParams());
			fatalError600();
		}
		scrnparams.subfileMore.set("Y");
		if (!(isEQ(undrprpIO.getStatuz(),varcom.oK)
		&& isEQ(wsspsmart.flddkey,undrprpIO.getClntnum()))) {
			undrprpIO.setStatuz(varcom.endp);
			scrnparams.subfileMore.set("N");
			wsaaPresSeq.set(HIVALUES);
			goTo(GotoLabel.exit9000);
		}
		undrprpIO.setFunction(varcom.nextr);
		/* start TMLII-429 UW-01-008 */
		wsaaValid.set("N");
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(wsspcomn.company);
		chdrlifIO.setChdrnum(undrprpIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		&& isNE(chdrlifIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlifIO.getStatuz(),varcom.mrnf)) {
			return;
		}
		for(int i =1; i<=12 || isNE(wsaaValid,"Y"); i++) {
			//ILIFE-2075-STARTS
			if (i>=13) {
				return;
			}
			//ILIFE-2075-ENDS
			if(isEQ(t5679rec.cnRiskStat[i],chdrlifIO.getStatcode())) {
				for(int j =1; j<=12 || isNE(wsaaValid,"Y"); j++) { 
					//ILIFE-2075-STARTS
					if (j>=13) {
						return;
					}
					//ILIFE-2075-ENDS
					if(isEQ(t5679rec.cnPremStat[j],chdrlifIO.getPstatcode())) {
						wsaaValid.set("Y");
					}
				}
			}
		}
		if(isNE(wsaaValid,"Y")) {
			return;
		}
		/* end TMLII-429 UW-01-008 */

		wsaaPresClntnum.set(undrprpIO.getClntnum());
		wsaaPresCoy.set(undrprpIO.getCoy());
		wsaaPresCurrcode.set(undrprpIO.getCurrcode());
		wsaaPresLrkcls.set(undrprpIO.getLrkcls02());
	}

protected void moveClntnam9100()
	{
		ctrl9100();
	}

protected void ctrl9100()
	{
		clntIO.setRecKeyData(SPACES);
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setClntnum(wsspsmart.flddkey);
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntIO.getSurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntIO.getGivname(), "  "));
		sv.clntnam.setLeft(stringVariable1.toString());
	}
}
