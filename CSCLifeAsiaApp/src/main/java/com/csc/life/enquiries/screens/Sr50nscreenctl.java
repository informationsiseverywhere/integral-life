package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50nscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr50nscreensfl";
		lrec.subfileClass = Sr50nscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 6;
		lrec.pageSubfile = 5;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 11, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50nScreenVars sv = (Sr50nScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50nscreenctlWritten, sv.Sr50nscreensflWritten, av, sv.sr50nscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50nScreenVars screenVars = (Sr50nScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.optdsc01.setClassString("");
		screenVars.optdsc02.setClassString("");
		screenVars.optdsc03.setClassString("");
		screenVars.optdsc04.setClassString("");
		screenVars.optdsc05.setClassString("");
		screenVars.optdsc06.setClassString("");
		screenVars.optdsc07.setClassString("");
		screenVars.optdsc08.setClassString("");
		// ILB-326:Starts
		screenVars.trannosearch.setClassString("");
		screenVars.datesubsearch.setClassString("");
		screenVars.effdatesearch.setClassString("");
		screenVars.trcodesearch.setClassString("");
		screenVars.trandescsearch.setClassString("");
		screenVars.crtusersearch.setClassString("");
		screenVars.rdocnumsearch.setClassString("");
		screenVars.statzsearch.setClassString("");
		// ILB-326:Ends
	}

/**
 * Clear all the variables in Sr50nscreenctl
 */
	public static void clear(VarModel pv) {
		Sr50nScreenVars screenVars = (Sr50nScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.optdsc01.clear();
		screenVars.optdsc02.clear();
		screenVars.optdsc03.clear();
		screenVars.optdsc04.clear();
		screenVars.optdsc05.clear();
		screenVars.optdsc06.clear();
		screenVars.optdsc07.clear();
		screenVars.optdsc08.clear();
		// ILB-326:Starts
		screenVars.trannosearch.clear();
		screenVars.datesubsearch.clear();
		screenVars.effdatesearch.clear();
		screenVars.trcodesearch.clear();
		screenVars.trandescsearch.clear();
		screenVars.crtusersearch.clear();
		screenVars.rdocnumsearch.clear();
		screenVars.statzsearch.clear();
		// ILB-326:Ends
	}
}
