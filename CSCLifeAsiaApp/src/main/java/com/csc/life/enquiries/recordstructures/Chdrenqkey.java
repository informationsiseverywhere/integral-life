package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:20
 * Description:
 * Copybook name: CHDRENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrenqKey = new FixedLengthStringData(64).isAPartOf(chdrenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrenqChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrenqKey, 0);
  	public FixedLengthStringData chdrenqChdrnum = new FixedLengthStringData(8).isAPartOf(chdrenqKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrenqKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}