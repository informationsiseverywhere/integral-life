package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sd5eg
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class Sd5egScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(531);
	public FixedLengthStringData dataFields = new FixedLengthStringData(243).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,177);
	
	// ILB-326:Starts
	public ZonedDecimalData trannosearch = DD.tranno.copyToZonedDecimal().isAPartOf(dataFields,178);
	public ZonedDecimalData datesubsearch = DD.datesub.copyToZonedDecimal().isAPartOf(dataFields,183);
	public ZonedDecimalData effdatesearch = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,191);
	public FixedLengthStringData trcodesearch = DD.trcode.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData trandescsearch = DD.trandesc.copy().isAPartOf(dataFields,203);
	public FixedLengthStringData crtusersearch = DD.crtuser.copy().isAPartOf(dataFields,233);
	// ILB-326:Ends
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 243);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	
	public FixedLengthStringData trannosearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData datesubsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData effdatesearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData trcodesearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData trandescsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData crtusersearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 315);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	
	public FixedLengthStringData[] trannosearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] datesubsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] effdatesearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] trcodesearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] trandescsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] crtusersearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(306);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(128).isAPartOf(subfileArea, 0);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public FixedLengthStringData revind = DD.crtind.copy().isAPartOf(subfileFields,23);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(subfileFields,24);
	public ZonedDecimalData tranamt = DD.tranamt.copyToZonedDecimal().isAPartOf(subfileFields,54);
	public ZonedDecimalData total = DD.tranamt.copyToZonedDecimal().isAPartOf(subfileFields,71);
	public ZonedDecimalData lbalperc = DD.totprcnt.copyToZonedDecimal().isAPartOf(subfileFields,88);
	public ZonedDecimalData allocamt = DD.tranamt.copyToZonedDecimal().isAPartOf(subfileFields,105);
	public FixedLengthStringData fillh = DD.fillh.copy().isAPartOf(subfileFields,122);
	public FixedLengthStringData filll = DD.filll.copy().isAPartOf(subfileFields,125);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 128);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData revindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData tranamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData totalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData lbalpercErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData allocamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData fillhErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData filllErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 172);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] revindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] tranamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] totalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] lbalpercOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] allocamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] fillhOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] filllOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 304);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datesubsearchDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdatesearchDisp = new FixedLengthStringData(10);
	
	public LongData Sd5egscreensflWritten = new LongData(0);
	public LongData Sd5egscreenctlWritten = new LongData(0);
	public LongData Sd5egscreenWritten = new LongData(0);
	public LongData Sd5egprotectWritten = new LongData(0);
	public GeneralTable sd5egscreensfl = new GeneralTable(AppVars.getInstance());
	
	


	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sd5egscreensfl;
	}

	public Sd5egScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(indicOut,new String[] {null,"03",null,null,null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] { crtuser, effdate, tranno, revind, trandesc, tranamt, total, allocamt, fillh, filll};
		screenSflOutFields = new BaseData[][] {crtuserOut, effdateOut, trannoOut, revindOut, trandescOut, tranamtOut, totalOut, allocamtOut, fillhOut, filllOut};
		screenSflErrFields = new BaseData[] {crtuserErr, effdateErr, trannoErr, revindErr, trandescErr, tranamtErr, totalErr, allocamtErr, fillhErr, filllErr};
		screenSflDateFields = new BaseData[] {effdate};
		screenSflDateErrFields = new BaseData[] { effdateErr};
		screenSflDateDispFields = new BaseData[] { effdateDisp};
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename,indic, trannosearch, datesubsearch, effdatesearch, trcodesearch, trandescsearch, crtusersearch};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut,indicOut, trannosearchOut, datesubsearchOut, effdatesearchOut, trcodesearchOut, trandescsearchOut, crtusersearchOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr,indicErr, trannosearchErr, datesubsearchErr, effdatesearchErr, trcodesearchErr, trandescsearchErr, crtusersearchErr};
		screenDateFields = new BaseData[] {datesubsearch, effdatesearch};
		screenDateErrFields = new BaseData[] {datesubsearchErr, effdatesearchErr};
		screenDateDispFields = new BaseData[] {datesubsearchDisp, effdatesearchDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5egscreen.class;
		screenSflRecord = Sd5egscreensfl.class;
		screenCtlRecord = Sd5egscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5egprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5egscreenctl.lrec.pageSubfile);
	}
}
