package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:45
 * Description:
 * Copybook name: ACMVTRNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvtrnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvtrnFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData acmvtrnKey = new FixedLengthStringData(256).isAPartOf(acmvtrnFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvtrnRldgcoy = new FixedLengthStringData(1).isAPartOf(acmvtrnKey, 0);
  	public FixedLengthStringData acmvtrnRdocnum = new FixedLengthStringData(9).isAPartOf(acmvtrnKey, 1);
  	public PackedDecimalData acmvtrnTranno = new PackedDecimalData(5, 0).isAPartOf(acmvtrnKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(acmvtrnKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvtrnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvtrnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}