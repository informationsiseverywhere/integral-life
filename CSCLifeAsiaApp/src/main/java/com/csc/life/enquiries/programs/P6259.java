/*
 * File: P6259.java
 * Date: 30 August 2009 0:40:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P6259.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.RegtvstTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S6259ScreenVars;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.dataaccess.dao.CovppfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Covppf;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* REGULAR BENEFIT BASED COMPONENT ENQUIRY
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen as follows:
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*
*
*     Perform a RETRV  on  COVRENQ.  This  will  provide  the  COVR
*     record that is being enquired upon. At this point the program
*     must work out whether it is to provide a Plan Level or Policy
*     Level  enquiry.  This  can  be  done  by  checking  the field
*     WSSP-PLAN-POLICY-FLAG.  If it is 'L', then Plan Level Enquiry
*     is  required,  if  it  is  'O',  then Policy Level Enquiry is
*     required.  For Plan Level Enquiry all the COVRENQ records for
*     the  given  Life, Coverage and Rider must be read and the Sum
*     Insured  and Premium values added together so a total for the
*     whole  Plan  may  be  displayed.  For  Policy  Level  Enquiry
*     processing  will  be different depending on whether a summary
*     record or non-summary record is being displayed. Normally the
*     program  could  determine whether or not a summary record was
*     being  displayed  by  checking  the  Plan Suffix. If this was
*     '0000',  then  it  was  a summary record. However, for Policy
*     Level Enquiry when a summary record is held in the I/O module
*     for  processing  the previous function will replace the value
*     of  '0000'  in  the  Plan  Suffix  with the calculated suffix
*     number of the actual policy that the user selected. Therefore
*     it  will  be  necessary  to check the Plan Suffix against the
*     number  of policies summarised, (CHDRENQ-POLSUM). If the Plan
*     Suffix is not greater the CHDRENQ-POLSUM then it is a summary
*     record.
*
*     For  a  non-summary  record,  (Plan Suffix > CHDRENQ-POLSUM),
*     then display the Sum Insured and Premium as found.
*
*     For  a  summary  record,  (Plan Suffix not > CHDRENQ-POLSUM),
*     then  the  values  held on the COVRENQ record will be for all
*     the  policies summarised, and therefore a calculation must be
*     performed  to  arrive  at the value for one of the summarised
*     policies.  For  all  summarised policies execpt the first one
*     simply divide the total by the number of policies summarised,
*     (CHDRENQ-POLSUM)  and  display the result. This may give rise
*     to  a  rounding  discrepancy. This discrepancy is absorbed by
*     the  first policy summarised, the notional policy number one.
*     If  this  policy  has  been  selected for display perform the
*     following calculation:
*
*     Value To Display = TOTAL - (TOTAL * (POLSUM - 1))
*                                          ~~~~~~~~~~~           *                                            POLSUM
*
*     Where  TOTAL  is  either  the Sum Insured or the Premium, and
*     POLSUM is the number of policies summarised, CHDRENQ-POLSUM.
*
*     For example if the premium is $100 and the number of policies
*     summarised  is 3, the Premium for notional policies #2 and #3
*     will  be  calculated  and  displayed as $33. For policy #1 it
*     will be:
*
*                        100 - (100 * (3 - 1)
*                                      ~~~~~                     *                                        3
*
*                     =  100 - (100 * 2/3)
*
*                     =  $34
*
*
*     The screen title  is  the  component  description from T5687.
*     (See P5123 for how this is obtained and centred).
*
*     Read table T5671,  key  is Transaction Code concatenated with
*     Coverage/Rider Code,  (CRTABLE).  Take  the  validation  item
*     that matches the current program and use it concatenated with
*     the Coverage/Rider Currency to read T5606.
*
*     If Options and Extras  are  not allowed, as defined on T5606,
*     then protect and  non-display  the  prompt and Options/Extras
*     Indicator, (OPTIND02).
*
*     Call  OPTSWCH  (Option  Switching) to load the  two check box
*     fields  at  the  bottom  of the  screen - Annuity Details and
*     Options & Extras.  T1661 / T1660  drive  what  appears in the
*     check box literals.
*
*     Subroutines  exist - P6259ANY  and  P6259LEX  that  check  to
*     see  if  any  ANNY or  LEXT records exist for this  component.
*     OPTSWCH drives  whether  a '+' or spaces  appears in the check
*     box indicator fields.
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
* Updating
* --------
*
*     There is no updating in this program.
*
* Next Program
* ------------
*
*     If "CF11" was requested  move  spaces  to the current program
*     field in the program stack field and exit.
*
*     Call  OPTSWCH  (Option  Switching)  to  switch  to  the   two
*     check boxes at the bottom of the screen if applicable.
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5606 - Reg Benefit Edit Rules            Key: Val. Item||Currency Code
* T5671 - Coverage/Rider Switching          Key: Tr. Code||CRTABLE
* T5687 - General Coverage/Rider Details    Key: CRTABLE
* T5688 - Contract Structure                Key: CNTTYPE
*
*****************************************************************
* </pre>
*/
public class P6259 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6259");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");


	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	
	//Ramalakshmi ILIFE-967 - start
	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(30, 1, wsaaHeading, 0);
	//Ramalakshmi ILIFE-967 - End
	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaCovrPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaPremStatus, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private static final String itemrec = "ITEMREC";
	private static final String povrrec = "POVRREC";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
	private Batckey wsaaBatckey = new Batckey();
	private T2240rec t2240rec = new T2240rec();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wssplife wssplife = new Wssplife();
	private S6259ScreenVars sv = getLScreenVars();//ScreenProgram.getScreenVars( S6259ScreenVars.class);
	private TablesInner tablesInner = new TablesInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private boolean stampDutyflag = false;
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private boolean loadingFlag = false;/* BRD-306 */
	private boolean occFlag = false;//BRD-009
	private T5687rec t5687rec = new T5687rec();
	private boolean dialdownFlag = false;
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrpf = new Covrpf();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private boolean exclFlag = false;
	private RegtvstTableDAM regtvstIO = new RegtvstTableDAM();
	private boolean lnkgFlag = false;
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	protected static final String T609 = "T609";
	//ILJ-45 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-45 End
	
	// ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	// ILJ-387 end
	private CovppfDAO covppfDAO= getApplicationContext().getBean("covppfDAO",CovppfDAO.class);//IBPLIFE-2141
	
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endPolicy1020, 
		nextProg10640, 
		exit1090, 
		search1320, 
		exit1790
	}

	public P6259() {
		super();
		screenVars = sv;
		new ScreenModel("S6259", AppVars.getInstance(), sv);
	}
	protected S6259ScreenVars getLScreenVars() {

		return ScreenProgram.getScreenVars(S6259ScreenVars.class);

	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case endPolicy1020: 
					endPolicy1020();
					gotRecord10620();
				case nextProg10640: 
					nextProg10640();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		wsaaMiscellaneousInner.wsaaSingp.set(ZERO);
		wsaaMiscellaneousInner.wsaaTaxamt.set(ZERO);
		sv.singlePremium.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		/*ILIFE-6968 start*/
		lnkgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),
				"NBPRP055", appVars, "IT");
		if (lnkgFlag == true) {
			sv.lnkgnoOut[varcom.nd.toInt()].set("Y");
			sv.lnkgsubrefnoOut[varcom.nd.toInt()].set("Y");
		}
		/*ILIFE-6968 end*/
		//ILJ-45 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{
			sv.crrcdOut[varcom.nd.toInt()].set("Y");
			//sv.rcessageOut[varcom.nd.toInt()].set("Y");
			sv.contDtCalcScreenflag.set("N");
			sv.rcesdteOut[varcom.nd.toInt()].set("Y");
		}
		//ILJ-45 End
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}

		// ILJ-387 Start
		boolean cntEnqFlag = false;
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if (cntEnqFlag) {
			sv.cntEnqScreenflag.set("Y");
		}else {
			sv.cntEnqScreenflag.set("N");
		}
		// ILJ-387 End
		/*BRD-306 START */
		sv.loadper.set(ZERO);
		sv.rateadj.set(ZERO);
		sv.fltmort.set(ZERO);
		sv.premadj.set(ZERO);
		sv.adjustageamt.set(ZERO);
		/*BRD-306 END */
		sv.zstpduty01.set(ZERO);
		sv.sumin.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.numpols.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		sv.statcode.set(SPACES);//BRD-009
		/*    Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRENQ-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			}
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO S6259-JLIFE                   */
		/*                                 S6259-JLIFENAME               */
		/*  ELSE                                                         */
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Retrieve valid status'.                                         */
		readT5679Table1400();
		/*    Read the first LIFE details on the contract.*/
		covrpf = covrDao.getCacheObject(covrpf);
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			/*     MOVE COVRENQ-SINGP      TO WSAA-SINGP                    */
			wsaaMiscellaneousInner.wsaaSingp.set(covrpf.getSingp());
			/*     ADD  COVRENQ-SINGP, COVRENQ-INSTPREM                <006>*/
			/*                             GIVING WSAA-SINGP           <006>*/
			wsaaMiscellaneousInner.wsaaPremium.set(covrpf.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrpf.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrpf.getSumins());
			setPrecision(covrpf.getPlanSuffix(), 0);
			covrpf.setPlanSuffix(chdrpf.getPolsum()+1);
			while ( !(isGT(covrpf.getPlanSuffix(),chdrpf.getPolinc()))) {
				planComponent1100();
			}
			
			goTo(GotoLabel.endPolicy1020);
		}
		if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
			/*     MOVE COVRENQ-SINGP      TO WSAA-SINGP                    */
			wsaaMiscellaneousInner.wsaaSingp.set(covrpf.getSingp());
			/*     ADD  COVRENQ-SINGP, COVRENQ-INSTPREM                <006>*/
			/*                             GIVING WSAA-SINGP           <006>*/
			wsaaMiscellaneousInner.wsaaPremium.set(covrpf.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrpf.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrpf.getSumins());
			goTo(GotoLabel.endPolicy1020);
		}
		/*  ADD  COVRENQ-SINGP, COVRENQ-INSTPREM                   <006>*/
		/*                             GIVING WSAA-PREMIUM.        <006>*/
		wsaaMiscellaneousInner.wsaaPremium.set(covrpf.getInstprem());
		if (isEQ(covrpf.getPlanSuffix(),1)) {
			/*     COMPUTE WSAA-SINGP       = COVRENQ-SINGP                 */
			/*  - (COVRENQ-SINGP  * (CHDRENQ-POLSUM - 1) / CHDRENQ-POLSUM)  */
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(sub(covrpf.getSingp(), (div(mult(covrpf.getSingp(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			/*     COMPUTE WSAA-SINGP       = WSAA-PREMIUM             <006>*/
			/*  - (WSAA-PREMIUM  * (CHDRENQ-POLSUM - 1) / CHDRENQ-POLSUM)06>*/
			
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(sub(wsaaMiscellaneousInner.wsaaPremium, (div(mult(wsaaMiscellaneousInner.wsaaPremium, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(sub(wsaaMiscellaneousInner.wsaaZlinstprem, (div(mult(wsaaMiscellaneousInner.wsaaZlinstprem, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(sub(covrpf.getSumins(), (div(mult(covrpf.getSumins(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
		}
		else {
			/*     COMPUTE WSAA-SINGP       = COVRENQ-SINGP                 */
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(div(covrpf.getSingp(), chdrpf.getPolsum()));
			/*     COMPUTE WSAA-SINGP       = WSAA-PREMIUM             <006>*/
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(div(wsaaMiscellaneousInner.wsaaPremium, chdrpf.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(div(wsaaMiscellaneousInner.wsaaZlinstprem, chdrpf.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(div(covrpf.getSumins(), chdrpf.getPolsum()));
		}
		
	}

protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
		/* IJTI-1479 START */
		rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
		rcvdPFObject.setChdrnum(covrpf.getChdrnum());
		rcvdPFObject.setLife(covrpf.getLife());
		rcvdPFObject.setCoverage(covrpf.getCoverage());
		rcvdPFObject.setRider(covrpf.getRider());
		rcvdPFObject.setCrtable(covrpf.getCrtable());
		rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
		/* IJTI-1479 END */
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}
protected void endPolicy1020()
	{
		/*
		 * fwang3 ICIL-4
		 */
		regtvstIO.setChdrcoy(covrpf.getChdrcoy());
		regtvstIO.setChdrnum(covrpf.getChdrnum());
		regtvstIO.setLife(covrpf.getLife());
		regtvstIO.setRider(covrpf.getRider());
		regtvstIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtvstIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "RIDER");
		regtvstIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, regtvstIO);
		if ((isNE(regtvstIO.getStatuz(), varcom.oK)) 
				&& (isNE(regtvstIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		if (!(isNE(regtvstIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(regtvstIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(regtvstIO.getLife(),covrpf.getLife())
		|| isNE(regtvstIO.getRider(),covrpf.getRider()))
		&& isEQ(regtvstIO.getStatuz(), varcom.oK)) {
			sv.payoutoption.set(regtvstIO.getPayoutopt());
		}
		sv.benBillDate.set(covrpf.getBenBillDate());
		if (isEQ(covrpf.getBenBillDate(),varcom.vrcmMaxDate)
		|| isEQ(covrpf.getBenBillDate(),0)) {
			sv.bbldatOut[varcom.nd.toInt()].set("Y");
		}
		sv.mortcls.set(covrpf.getMortcls());
		sv.numpols.set(chdrpf.getPolinc());
		sv.anbAtCcd.set(covrpf.getAnbAtCcd());
		sv.crrcd.set(covrpf.getCrrcd());
		sv.annivProcDate.set(covrpf.getAnnivProcDate());
		/*  MOVE SPACES                 TO S6259-OPTEXTIND.              */
		sv.premcess.set(covrpf.getPremCessDate());
		sv.liencd.set(covrpf.getLiencd());
		sv.riskCessDate.set(covrpf.getRiskCessDate());
		
		sv.riskCessAge.set(covrpf.getRiskCessAge());
		sv.premCessAge.set(covrpf.getPremCessAge());
		sv.riskCessTerm.set(covrpf.getRiskCessTerm());
		sv.premCessTerm.set(covrpf.getPremCessTerm());
		sv.benCessAge.set(covrpf.getBenCessAge());
		sv.benCessTerm.set(covrpf.getBenCessTerm());
		sv.benCessDate.set(covrpf.getBenCessDate());
		/*ILIFE-6968 start*/
		sv.lnkgno.set(covrpf.getLnkgno());
		sv.lnkgsubrefno.set(covrpf.getLnkgsubrefno());
		/*ILIFE-6968 end*/
		sv.register.set(chdrpf.getReg());
		sv.life.set(covrpf.getLife());
		if (isNE(covrpf.getLife(),lifeenqIO.getLife())) {
			getLifeName4900();
		}
		sv.rider.set(covrpf.getRider());
		sv.coverage.set(covrpf.getCoverage());
		sv.rerateDate.set(covrpf.getRerateDate());
		sv.rerateFromDate.set(covrpf.getRerateFromDate());
		/*BRD-306 START */
		sv.zbinstprem.set(covrpf.getZbinstprem());
		sv.loadper.set(covrpf.getLoadper());
		sv.rateadj.set(covrpf.getRateadj());
		sv.fltmort.set(covrpf.getFltmort());
		sv.premadj.set(covrpf.getPremadj());
		sv.adjustageamt.set(covrpf.getPremadj());
		loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		/*BRD-306 END */
		exclFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.optind05Out[varcom.nd.toInt()].set("Y");
		}
		
		//ILIFE-3518-starts
		//ItempfDAO itempfDAO = DAOFactory.getItempfDAO();//ILIFE-3955
		Itempf itempf = new Itempf();
		itempf.setItemtabl("T5687");
		itempf.setItemitem(covrpf.getCrtable()); /* IJTI-1479 */
		itempf.setItemcoy(covrpf.getChdrcoy().trim()); /* IJTI-1479 */
		itempf.setItempfx("IT");
		itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate()));
		itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
		List<Itempf> itempfList = itemDAO.findByItemDates(itempf);//ILIFE-3955
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {
				t5687rec.t5687Rec.set(new String(it.getGenarea()));
			}
		}
		//BRD-009-STARTS
		occFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP05", appVars, "IT");
		if((occFlag) &&
		isEQ(t5687rec.premmeth,"PM30")){
			rcvdPFObject= new Rcvdpf();
			callReadRCVDPF();
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getStatcode()!=null && !rcvdPFObject.getStatcode().trim().equals("") ){
					sv.statcode.set(rcvdPFObject.getStatcode());
				}
			}
		}
		else{
			sv.statcodeOut[varcom.nd.toInt()].set("Y");
		}
		//BRD-009-ENDS
		//ILIFE-3518-ENDS
		stampDutyflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP01", appVars, "IT"); /* IJTI-1479 */
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			if(isNE(covrpf.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
				sv.zstpduty01.set(covrpf.getZstpduty01());
			}
			
		}
		dialdownFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag||premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!incomeProtectionflag){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
			sv.poltypOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.waitperiodOut[varcom.nd.toInt()].set("N");
			sv.bentrmOut[varcom.nd.toInt()].set("N");
			sv.poltypOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
					sv.waitperiod.set(rcvdPFObject.getWaitperiod());
				}
				if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
					sv.poltyp.set(rcvdPFObject.getPoltyp());
				}
				if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
					sv.bentrm.set(rcvdPFObject.getBentrm());
				}
			}
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends
		sv.singlePremium.set(wsaaMiscellaneousInner.wsaaSingp);
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.zlinstprem.set(wsaaMiscellaneousInner.wsaaZlinstprem);
		/*--- Read TR386 table to get screen literals                      */
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		/*  MOVE TR386-PROGDESC-1       TO S6259-ZDESC.                  */
		sv.zdesc.set(tr386rec.progdesc01);
		/*    IF  WSAA-PREMIUM            = 0                              */
		/*        MOVE WSAA-SINGLE-DESC   TO S6259-ZDESC                   */
		/*    ELSE                                                         */
		/*    IF  WSAA-PREMIUM        not = 0                      <SPLPRM>*/
		/*        MOVE WSAA-REG-DESC      TO S6259-ZDESC           <SPLPRM>*/
		/*    END-IF.                                              <SPLPRM>*/
		sv.zdescOut[varcom.hi.toInt()].set("N");
		sv.statFund.set(covrpf.getStatFund());
		sv.statSect.set(covrpf.getStatSect());
		sv.statSubsect.set(covrpf.getStatSubsect());
		sv.sumin.set(wsaaMiscellaneousInner.wsaaSumins);
		if (isNE(covrpf.getPlanSuffix(),ZERO)) {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		else {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			sv.planSuffix.set(ZERO);
		}
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.planSuffix.set(ZERO);
		setupBonus1500();
		sv.bappmeth.set(covrpf.getBappmeth());
		/* GET THE COMPONENT DESCRIPTION*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		/* READ T5671*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaMiscellaneousInner.wsaaTrCode.set(wsaaBatckey.batcBatctrcde);
		wsaaMiscellaneousInner.wsaaCrtable.set(covrpf.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5671);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5671Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5671)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5671Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itdmIO.getGenarea());
		}
		//IBPLIFE-2141 Start
		boolean covrprpseFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP126", appVars, "IT");
		if (covrprpseFlag) {
			sv.nbprp126lag.set("Y");
			sv.covrprpseOut[varcom.nd.toInt()].set("Y");
			checkCoverPurpose1040();
		} else {
			sv.nbprp126lag.set("N");
		}
		//IBPLIFE-2141 End	
	}

	//IBPLIFE-2141
	private void checkCoverPurpose1040() {

		if (covrpf != null) {
			Covppf covppf = covppfDAO.getCovppfCrtable(lifeenqIO.getChdrcoy().toString(),
					lifeenqIO.getChdrnum().toString(), covrpf.getCrtable(),
					covrpf.getCoverage(),covrpf.getRider());
			if (covppf != null) {
				sv.effdate.set(covppf.getEffdate());
				sv.covrprpse.set(covppf.getCovrprpse());
				sv.trancd.set(covppf.getTranCode());
				descIO.setDataKey(SPACES);
				descIO.setDescpfx("IT");
				descIO.setDesccoy(lifeenqIO.getChdrcoy());
				descIO.setDesctabl("T1688");
				descIO.setDescitem(sv.trancd);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
					sv.trandesc.fill("?");
				} else {
					sv.trandesc.set(descIO.getLongdesc());
				}
			}
		}

	}

protected void gotRecord10620()
	{
		sub1.set(0);
	}

protected void nextProg10640()
	{
		sub1.add(1);
		if (isNE(t5671rec.pgm[sub1.toInt()],wsaaProg)) {
			goTo(GotoLabel.nextProg10640);
		}
		else {
			wsaaMiscellaneousInner.wsaaValidItem.set(t5671rec.edtitm[sub1.toInt()]);
		}
		/* READ T5606*/
		wsaaMiscellaneousInner.wsaaCrcyCode.set(chdrpf.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5606Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5606)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5606Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5606rec.specind,"N")) {
			/*     MOVE 'Y'                 TO S6259-OPTEXTIND-OUT (ND)      */
			sv.optdsc02Out[varcom.nd.toInt()].set("Y");
			sv.optdsc02Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc02Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc02Out[varcom.pr.toInt()].set(SPACES);
		}
		if(isEQ(t5606rec.waitperiod, SPACES)){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.bentrm, SPACES)){
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.poltyp, SPACES)){
			sv.poltypOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		/*     PERFORM 1900-CHECK-LEXT.                                  */
		/*    Obtain the frequency short description from T3590.           */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3590);
		descIO.setDescitem(t5606rec.benfreq);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		else {
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.frqdesc.set(descIO.getShortdesc());
			}
			else {
				sv.frqdesc.fill("?");
			}
		}
		checkPovr1800();
		optswchInit1200();
	}

protected void planComponent1100()
	{
		para1100();
	}

protected void para1100()
	{
		
	if (isEQ(covrpf.getPlanSuffix(),1)
			&& isNE(chdrpf.getPolinc(),1)) {
				wsaaMiscellaneousInner.wsaaSumins.set(ZERO);
			}
			Covrpf covr = covrDao.getCovrRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix(), covrpf.getValidflag());
			if(covr.getUniqueNumber()>0) {
				covrpf = covr;
			}
			/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
			if (covr.getUniqueNumber()>0) {
				wsaaCovrPstatcode.set(covrpf.getPstatcode());
				checkPremStatus1300();
			}
			/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
			if (covr.getUniqueNumber()>0
			&& validStatus.isTrue()) {
				compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(add(wsaaMiscellaneousInner.wsaaSingp, covrpf.getSingp()));
				/*                              + COVRENQ-SINGP.           <006>*/
				/*                              + COVRENQ-SINGP                 */
				compute(wsaaMiscellaneousInner.wsaaPremium, 2).set(add(wsaaMiscellaneousInner.wsaaPremium, covrpf.getInstprem()));
				/*                              + COVRENQ-INSTPREM.        <006>*/
				compute(wsaaMiscellaneousInner.wsaaZlinstprem, 2).set(add(wsaaMiscellaneousInner.wsaaZlinstprem, covrpf.getZlinstprem()));
				/*                              + COVRENQ-INSTPREM              */
				compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(add(wsaaMiscellaneousInner.wsaaSumins, covrpf.getSumins()));
			}
			setPrecision(covrpf.getPlanSuffix(), 0);
			covrpf.setPlanSuffix(covrpf.getPlanSuffix()+1);
	}

protected void optswchInit1200()
	{
		call1210();
	}

protected void call1210()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6625)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.optdsc01Out[varcom.nd.toInt()].set("Y");
			sv.optdsc01Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc01Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc01Out[varcom.pr.toInt()].set(SPACES);
		}
		/*  Read TR52D for Taxcode.                                        */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.optdsc04Out[varcom.nd.toInt()].set("Y");
		sv.optind04Out[varcom.nd.toInt()].set("Y");
		sv.optdsc04Out[varcom.pr.toInt()].set("Y");
		sv.optind04Out[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc04Out[varcom.nd.toInt()].set("N");
			sv.optind04Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc04Out[varcom.pr.toInt()].set("N");
			sv.optind04Out[varcom.pr.toInt()].set("N");
		}
		/* Call OPTSWCH to retrieve check box text description             */
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set("0");
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optdsc01.set(optswchrec.optsDsc[1]);
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optdsc02.set(optswchrec.optsDsc[2]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optdsc03.set(optswchrec.optsDsc[3]);
		sv.optind03.set(optswchrec.optsInd[3]);
		sv.optdsc04.set(optswchrec.optsDsc[4]);
		sv.optind04.set(optswchrec.optsInd[4]);
		sv.optdsc05.set(optswchrec.optsDsc[5]);
		sv.optind05.set(optswchrec.optsInd[5]);
		
	}

protected void checkPremStatus1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1310();
				case search1320: 
					search1320();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1310()
	{
		wsaaPremStatus.set(SPACES);
		wsaaSub.set(ZERO);
	}

protected void search1320()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			return ;
		}
		else {
			if (isNE(wsaaCovrPstatcode,t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search1320);
			}
			else {
				wsaaPremStatus.set("Y");
				return ;
			}
		}
	}

protected void readT5679Table1400()
	{
		read1410();
	}

protected void read1410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void setupBonus1500()
	{
		/*PARA*/
		/* Check if Coverage/Rider is a SUM product. If not SUM product    */
		/* do not display field.                                           */
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covrpf.getChdrcoy());
		itemIO.setItemitem(covrpf.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(29); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		//Ramalakshmi ILIFE-967 - start
		compute(wsaaY, 0).set(sub(30,wsaaX));
		//Ramalakshmi ILIFE-967 - End
//		compute(wsaaY, 0).set(sub(26,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
//		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
//		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(9, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
//		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkPovr1800()
	{
		readPovr1810();
	}

protected void readPovr1810()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covrpf.getChdrcoy());
		povrIO.setChdrnum(covrpf.getChdrnum());
		povrIO.setLife(covrpf.getLife());
		povrIO.setCoverage(covrpf.getCoverage());
		povrIO.setRider(covrpf.getRider());
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)
		&& isNE(povrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(povrIO.getStatuz(),varcom.endp)
			|| isNE(povrIO.getChdrcoy(),covrpf.getChdrcoy())
			|| isNE(povrIO.getChdrnum(),covrpf.getChdrnum())
			|| isNE(povrIO.getLife(),covrpf.getLife())
			|| isNE(povrIO.getCoverage(),covrpf.getCoverage())
			|| isNE(povrIO.getRider(),covrpf.getRider())) {
			sv.optdsc03Out[varcom.nd.toInt()].set("Y");
			sv.optdsc03Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc03Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc03Out[varcom.pr.toInt()].set(SPACES);
			pbKeeps5100();
		}
	}

	/**
	* <pre>
	*1900-CHECK-LEXT SECTION.                                         
	*1910-READ-LEXT.                                                  
	***  MOVE COVRENQ-CHDRCOY        TO LEXT-CHDRCOY.                 
	***  MOVE COVRENQ-CHDRNUM        TO LEXT-CHDRNUM.                 
	***  MOVE COVRENQ-LIFE           TO LEXT-LIFE.                    
	***  MOVE COVRENQ-COVERAGE       TO LEXT-COVERAGE.                
	***  MOVE COVRENQ-RIDER          TO LEXT-RIDER.                   
	***  MOVE 0                      TO LEXT-SEQNBR.                  
	***  MOVE LEXTREC                TO LEXT-FORMAT.                  
	***  MOVE BEGN                   TO LEXT-FUNCTION.                
	***  CALL 'LEXTIO' USING LEXT-PARAMS.                             
	***  IF LEXT-STATUZ              NOT = O-K AND                    
	***                              NOT = ENDP                       
	***     MOVE LEXT-PARAMS         TO SYSR-PARAMS                   
	***     PERFORM 600-FATAL-ERROR.                                  
	***  IF COVRENQ-CHDRCOY   NOT = LEXT-CHDRCOY                      
	***   OR COVRENQ-CHDRNUM  NOT = LEXT-CHDRNUM                      
	***   OR COVRENQ-LIFE     NOT = LEXT-LIFE                         
	***   OR COVRENQ-COVERAGE NOT = LEXT-COVERAGE                     
	***   OR COVRENQ-RIDER    NOT = LEXT-RIDER                        
	***     MOVE ENDP                TO LEXT-STATUZ.                  
	***  IF LEXT-STATUZ              = ENDP                           
	***     MOVE ' '                 TO S6259-OPTEXTIND               
	***  ELSE                                                         
	***     MOVE '+'                 TO S6259-OPTEXTIND.              
	*1990-EXIT.                                                       
	***  EXIT.                                                        
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5000();
	}
		if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.optdsc04Out[varcom.nd.toInt()].set("Y");
			sv.optind04Out[varcom.nd.toInt()].set("Y");
			sv.optdsc04Out[varcom.pr.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.optind04Out[varcom.pr.toInt()].set("Y");
		}
		/*2010-SCREEN-IO.                                                  */
		sv.statcodeOut[varcom.pr.toInt()].set("Y");//BRD-009
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
			screenIo2010();
		}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S6259IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   S6259-DATA-AREA .             */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*VALIDATE-SCREEN*/
		/*  IF  S6259-OPTEXTIND NOT = ' '  AND                           */
		/*                            '+'  AND                           */
		/*                            'X'                                */
		/*      MOVE G620               TO S6259-OPTEXTIND-ERR.          */
		optswchChck2100();
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void optswchChck2100()
	{
		call2110();
	}

protected void call2110()
	{
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		optswchrec.optsInd[1].set(sv.optind01);
		optswchrec.optsInd[2].set(sv.optind02);
		optswchrec.optsInd[3].set(sv.optind03);
		optswchrec.optsInd[4].set(sv.optind04);
		optswchrec.optsInd[5].set(sv.optind05);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.optind01Err.set(optswchrec.optsStatuz);
			sv.optind02Err.set(optswchrec.optsStatuz);
			sv.optind03Err.set(optswchrec.optsStatuz);
			sv.optind04Err.set(optswchrec.optsStatuz);
			sv.optind05Err.set(optswchrec.optsStatuz);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*PARA*/
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			pbRlse5200();
			return ;
		}
		wsaaBatchkey.set(wsspcomn.batchkey);
		if(isEQ(wsaaBatchkey.batcBatctrcde, T609)) {
			
			chdrDao.setCacheObject(chdrpf);
			covtlnbIO.setChdrcoy(covrpf.getChdrcoy());
			covtlnbIO.setChdrnum(covrpf.getChdrnum());
			covtlnbIO.setLife(covrpf.getLife());
			covtlnbIO.setCoverage(covrpf.getCoverage());
			covtlnbIO.setRider(covrpf.getRider());
			covtlnbIO.setSeqnbr(ZERO);
			covtlnbIO.setCrtable(covrpf.getCrtable());
			covtlnbIO.setFormat(covrpf);
			covtlnbIO.setFunction("KEEPS");
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}
		optswchCall4800();
	}

protected void optswchCall4800()
	{
		call4800();
	}

protected void call4800()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
		wsspcomn.crtable.set(covrpf.getCrtable());
		
		wsspcomn.cmode.set("IFE");
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK))
		&& (isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optind03.set(optswchrec.optsInd[3]);
		sv.optind04.set(optswchrec.optsInd[4]);
		sv.optind05.set(optswchrec.optsInd[5]);
		if (isEQ(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void getLifeName4900()
	{
		para4900();
	}

protected void para4900()
	{
		lifeenqIO.setChdrcoy(covrpf.getChdrcoy());
		lifeenqIO.setChdrnum(covrpf.getChdrnum());
		lifeenqIO.setLife(covrpf.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void checkCalcTax5000()
	{
		start5010();
	}

protected void start5010()
	{
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(covrpf.getLife());
		txcalcrec.coverage.set(covrpf.getCoverage());
		txcalcrec.rider.set(covrpf.getRider());
		txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.amountIn.set(ZERO);
		txcalcrec.transType.set("PREM");
		txcalcrec.txcode.set(tr52drec.txcode);
		if (isEQ(chdrpf.getBillfreq(), "00")) {
			txcalcrec.effdate.set(chdrpf.getOccdate());
		}
		else {
			txcalcrec.effdate.set(chdrpf.getPtdate());
		}
		txcalcrec.tranno.set(chdrpf.getTranno());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			wsaaMiscellaneousInner.wsaaTaxamt.set(sv.instPrem);
			if(isNE(sv.singlePremium, ZERO)) {
				wsaaMiscellaneousInner.wsaaTaxamt.set(sv.singlePremium);
			}
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[2]);
			}
			sv.taxamt.set(wsaaMiscellaneousInner.wsaaTaxamt);
			sv.optind04.set("+");
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc04Out[varcom.nd.toInt()].set("N");
			sv.optind04Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc04Out[varcom.pr.toInt()].set("N");
			sv.optind04Out[varcom.pr.toInt()].set("N");
		}
	}

protected void pbKeeps5100()
	{
		/*START*/
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void pbRlse5200()
	{
		/*START*/
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private PackedDecimalData wsaaSuffixCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaCrcyCode = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
}
}
