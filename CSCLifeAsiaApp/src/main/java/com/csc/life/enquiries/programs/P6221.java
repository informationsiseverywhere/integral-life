/*
 * File: P6221.java
 * Date: 30 August 2009 0:36:39
 * Author: Quipoz Limited
 * 
 * Class transformed from P6221.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.screens.S6221ScreenVars;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* The is the submenu program for the underwriting client
* inquiries.
*
*****************************************************************
* </pre>
*/
public class P6221 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6221");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaLfFoundFlag = "N";
		/* ERRORS */
	private String e070 = "E070";
	private String e073 = "E073";
	private String e132 = "E132";
	private String e005 = "E005";
	private String e186 = "E186";
	private String f393 = "F393";
	private String g611 = "G611";
		/* FORMATS */
	private String clntrec = "CLNTREC";
	private String clrrrec = "CLRRREC";
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
		/*Client role and relationships logical fi*/
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Batckey wsaaBatchkey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private boolean riskAmountFlag = false;
	private S6221ScreenVars sv = getLScreenVars();  //ScreenProgram.getScreenVars( S6221ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		validate2200, 
		verifyBatchControl2300, 
		exit2090, 
		exit2952, 
		exit2962, 
		continue3400, 
		exit3900
	}

	public P6221() {
		super();
		screenVars = sv;
		new ScreenModel("S6221", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		/*ICIL-79 starts*/
		riskAmountFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "UNWRT001", appVars, "IT");
		if(!riskAmountFlag) {
			sv.actionOut[varcom.nd.toInt()].set("Y");
		}
		else
		{
			sv.actionOut[varcom.nd.toInt()].set("N");
		}
		/*ICIL-79 ends*/
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validateAction2100();
					checkIndics2200();
				}
				case validate2200: {
					validate2200();
				}
				case verifyBatchControl2300: {
					verifyBatchControl2300();
					batchProgs2310();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateAction2100()
	{
		if (!(isEQ(sv.action,"A")
		|| isEQ(sv.action,"B")|| isEQ(sv.action,"C"))) /* ICIL-79*/
		{
			sv.actionErr.set(e005);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.clntnum,SPACES)) {
			sv.clntnumErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		clntIO.setFunction(varcom.readr);
		clntIO.setClntnum(sv.clntnum);
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setFormat(clntrec);
		clntIO.setClntpfx("CN");
		SmartFileCode.execute(appVars, clntIO);
		if (isEQ(clntIO.getStatuz(),varcom.mrnf)) {
			sv.clntnumErr.set(f393);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		checkContracts2950();
		if (isEQ(clrrIO.getStatuz(),varcom.endp)) {
			sv.clntnumErr.set(g611);
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkIndics2200()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.validate2200);
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			wsspcomn.edterror.set("Y");
			sv.actionErr.set(sanctnrec.statuz);
			goTo(GotoLabel.validate2200);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.verifyBatchControl2300);
		}
	}

protected void validate2200()
	{
		wsspsmart.flddkey.set(sv.clntnum);
		wsspcomn.flag.set(sv.action);
		goTo(GotoLabel.exit2090);
	}

protected void verifyBatchControl2300()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		bcbprogrec.transcd.set(subprogrec.transcd);
	}

protected void batchProgs2310()
	{
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
		wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
		wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
		wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
	}

protected void checkContracts2950()
	{
		try {
			go2951();
		}
		catch (GOTOException e){
		}
	}

protected void go2951()
	{
		clrrIO.setStatuz(SPACES);
		clrrIO.setParams(SPACES);
		clrrIO.setGenDate(ZERO);
		clrrIO.setGenTime(ZERO);
		wsaaLfFoundFlag = "N";
		clrrIO.setClntpfx("CN");
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClrrrole("LF");
		clrrIO.setClntnum(sv.clntnum);
		clrrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		clrrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clrrIO.setFitKeysSearch("CLNTNUM");
		clrrIO.setFormat(clrrrec);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)
		&& isNE(clrrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		if (isEQ(clrrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2952);
		}
		if (isGT(clrrIO.getClntnum(),sv.clntnum)) {
			clrrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2952);
		}
		if ((isNE(clrrIO.getClntpfx(),"CN"))
		|| (isNE(clrrIO.getClntcoy(),wsspcomn.fsuco))
		|| (isNE(clrrIO.getClntnum(),sv.clntnum))
		|| (isNE(clrrIO.getClrrrole(),"LF"))) {
			while ( !((isEQ(clrrIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaLfFoundFlag,"Y")))) {
				loopThroughRoles2960();
			}
			
		}
		else {
			wsaaLfFoundFlag = "Y";
		}
	}

protected void loopThroughRoles2960()
	{
		try {
			begin2961();
		}
		catch (GOTOException e){
		}
	}

protected void begin2961()
	{
		clrrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)
		&& isNE(clrrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		if (isGT(clrrIO.getClntnum(),sv.clntnum)) {
			clrrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2962);
		}
		if (isEQ(clrrIO.getClntpfx(),"CN")
		&& isEQ(clrrIO.getClntcoy(),wsspcomn.fsuco)
		&& isEQ(clrrIO.getClrrrole(),"LF")
		&& isEQ(clrrIO.getClntnum(),sv.clntnum)) {
			wsaaLfFoundFlag = "Y";
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateWssp3100();
					updateBatchControl3200();
					openNewBatch3300();
				}
				case continue3400: {
					continue3400();
				}
				case exit3900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3100()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(subprogrec.bchrqd,"Y")) {
			goTo(GotoLabel.exit3900);
		}
	}

protected void updateBatchControl3200()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(wsspcomn.batchkey);
		batcchkrec.tranid.set(wsspcomn.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz,varcom.oK)) {
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.exit3900);
		}
		if (isEQ(batcchkrec.statuz,varcom.dupr)) {
			sv.actionErr.set(e132);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3900);
		}
		if (isEQ(batcchkrec.statuz,"INAC")) {
			batcdorrec.function.set("ACTIV");
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.continue3400);
		}
		if (isNE(batcchkrec.statuz,varcom.mrnf)) {
			sv.actionErr.set(batcchkrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3900);
		}
	}

protected void openNewBatch3300()
	{
		batcdorrec.function.set("AUTO");
	}

protected void continue3400()
	{
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			goTo(GotoLabel.exit3900);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}

protected S6221ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(S6221ScreenVars.class);
}

}
