package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sa575
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class Sa575ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(103);
	public FixedLengthStringData dataFields = new FixedLengthStringData(55).isAPartOf(dataArea, 0);
	public FixedLengthStringData payer = DD.payer.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData payername = DD.payername.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 55);
	public FixedLengthStringData payerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData payernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 67);
	public FixedLengthStringData[] payerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] payernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(400);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(127).isAPartOf(subfileArea, 0);
	
	public PackedDecimalData dbtcdtdate = DD.dbtcdtdate.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData dbtcdtdesc = DD.dbtcdtdesc.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData amnt = DD.amnt.copyToZonedDecimal().isAPartOf(subfileFields,14);
	public FixedLengthStringData bankcode = DD.bankkey.copy().isAPartOf(subfileFields,31);
	public FixedLengthStringData aacct = DD.bankacckey.copy().isAPartOf(subfileFields,41);
	public FixedLengthStringData statdesc = DD.statdesc.copy().isAPartOf(subfileFields,61);
	public FixedLengthStringData transcode = DD.transcode.copy().isAPartOf(subfileFields,85);
	public FixedLengthStringData trcdedesc = DD.trandesc.copy().isAPartOf(subfileFields,89);
	public FixedLengthStringData trdate = DD.trandtex.copy().isAPartOf(subfileFields,119);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 127);
	public FixedLengthStringData dbtcdtdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData dbtcdtdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData amntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData bankcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData aacctErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData statdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData transcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData trcdedescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData trdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 163);
	public FixedLengthStringData[] dbtcdtdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] dbtcdtdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] amntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] bankcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] aacctOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] statdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] transcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] trcdedescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] trdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 271);
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

//	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sa575screensflWritten = new LongData(0);
	public LongData Sa575screenctlWritten = new LongData(0);
	public LongData Sa575screenWritten = new LongData(0);
	public LongData Sa575protectWritten = new LongData(0);
	public GeneralTable sa575screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData dbtcdtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData trdateDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sa575screensfl;
	}

	public Sa575ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dbtcdtdescOut,new String[] {"01","20","-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {dbtcdtdate, dbtcdtdesc, amnt, bankcode, aacct, statdesc, transcode,trcdedesc,trdate};
		screenSflOutFields = new BaseData[][] {dbtcdtdateOut, dbtcdtdescOut,amntOut, bankcodeOut, aacctOut, statdescOut,transcodeOut,trcdedescOut,trdateOut};
		screenSflErrFields = new BaseData[] {dbtcdtdateErr, dbtcdtdescErr,amntErr, bankcodeErr, aacctErr, statdescErr, transcodeErr,trcdedescErr,trdateErr};
		screenSflDateFields = new BaseData[] {dbtcdtdate,trdate};
		screenSflDateErrFields = new BaseData[] {dbtcdtdateErr,trdateErr};
		screenSflDateDispFields = new BaseData[] {dbtcdtdateDisp,trdateDisp};

		screenFields = new BaseData[] {payer, payername};
		screenOutFields = new BaseData[][] {payerOut, payernameOut};
		screenErrFields = new BaseData[] {payerErr, payernameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sa575screen.class;
		screenSflRecord = Sa575screensfl.class;
		screenCtlRecord = Sa575screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sa575protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sa575screenctl.lrec.pageSubfile);
	}
}
