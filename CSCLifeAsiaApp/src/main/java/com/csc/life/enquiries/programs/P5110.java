/*
 * File: P5110.java
 * Date: 30 August 2009 0:08:42
 * Author: Quipoz Limited
 * 
 * Class transformed from P5110.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.GrpsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.screens.S5110ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                  Contract Enquiry Group Details
*                  ==============================
*
* Overview
* --------
*
* A pop-up window will display the group number and member number
* of a contract and will be accessed from the Contract Extra
* Details screen.
*
* Initialise
* ----------
*
* Retrieves the contract header details from the contract header
* file (CHDRENQ).    Using RETRV
*
* Set up the screen fields for the Group Number and Member Number.
*
* The group details are retrieved from the Group Master (GRPS)
* file.
*
* Set up the screen field for the Group Name.
*
* Validation
* ----------
*
* No validation is required.  This is an enquiry screen and
* therefore all the fields will be protected.
*
* Updating
* --------
*
* This is an enquiry no update is need.
*
* Where Next
* ----------
*
* Add 1 to WSSP-PROGRAM-PTR.
*
*****************************************************************
* </pre>
*/
public class P5110 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5110");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	//private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private GrpsTableDAM grpsIO = new GrpsTableDAM();
	private S5110ScreenVars sv = ScreenProgram.getScreenVars( S5110ScreenVars.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	
	public P5110() {
		super();
		screenVars = sv;
		new ScreenModel("S5110", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		sv.dataArea.set(SPACES);
		/*    Retrieve contract header information.*/
		
		 chdrpf = chdrpfDAO.getCacheObject(chdrpf);
			if(chdrpf==null)
				{
					return;
				}
			
		/*chdrpf.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrpf);
		if (isNE(chdrpf.getStatuz(), varcom.oK)
		&& isNE(chdrpf.getStatuz(), varcom.mrnf)) {
			chdrpf.setParams(chdrpf.getParams());
			fatalError600();
		}*/
			
		/*    Read logical GRPS to obtain the group details.*/
		grpsIO.setGrupcoy(chdrpf.getChdrcoy());
		grpsIO.setGrupnum(chdrpf.getGrupkey());
		grpsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, grpsIO);
		if (isNE(grpsIO.getStatuz(), varcom.oK)
		&& isNE(grpsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(grpsIO.getParams());
			fatalError600();
		}
		/*    Set screen fields.*/
		sv.grupname.set(grpsIO.getGrupname());
		sv.grupnum.set(chdrpf.getGrupkey());
		sv.membsel.set(chdrpf.getMembsel());
		sv.mplnum.set(chdrpf.getMplnum());
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		scrnparams.function.set(varcom.prot);
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		start2000();
	}

protected void start2000()
	{
		/*    CALL 'S5110IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                S5110-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    If F11 pressed bypass validation.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/*    If F5 pressed then set a screen error so the screen*/
		/*    is re-displayed.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*START*/
		/**  No update required as this is just an enquiry program.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*START*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}
}
