package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6239
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6239ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(463);
	public FixedLengthStringData dataFields = new FixedLengthStringData(223).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.contrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,167);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,175);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,179);
	public FixedLengthStringData premstatus = DD.primstatus.copy().isAPartOf(dataFields,183);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,203);
	public ZonedDecimalData totalPrem = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,206);//ILJ-388
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 223);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData totalPremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 283);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] totalPremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);//ILJ-388
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(412);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(170).isAPartOf(subfileArea, 0);
	public FixedLengthStringData compdesc = DD.cmpntdesc.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData cmpntnum = DD.cmpntnum.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData component = DD.component.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData hcoverage = DD.hcoverage.copy().isAPartOf(subfileFields,52);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,54);
	public FixedLengthStringData hlifcnum = DD.hlifcnum.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData hlifeno = DD.hlifeno.copy().isAPartOf(subfileFields,66);
	public FixedLengthStringData hrider = DD.hrider.copy().isAPartOf(subfileFields,68);
	public ZonedDecimalData hsuffix = DD.hsuffix.copyToZonedDecimal().isAPartOf(subfileFields,70);
	public FixedLengthStringData linetype = DD.linetype.copy().isAPartOf(subfileFields,74);
	public FixedLengthStringData pstatcode = DD.premstatus_longdesc.copy().isAPartOf(subfileFields,75);
	public FixedLengthStringData statcode = DD.riskstatus_longdesc.copy().isAPartOf(subfileFields,105);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,135);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,136);//ILJ-388
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(subfileFields,153);//ILJ-388
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 170);
	public FixedLengthStringData cmpntdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cmpntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData componentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hcoverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hlifenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hriderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hsuffixErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData linetypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData premdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData riskdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(180).isAPartOf(subfileArea, 230);
	public FixedLengthStringData[] cmpntdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cmpntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] componentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hcoverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hlifcnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hlifenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hriderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hsuffixOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] linetypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] premdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] riskdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 410);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6239screensflWritten = new LongData(0);
	public LongData S6239screenctlWritten = new LongData(0);
	public LongData S6239screenWritten = new LongData(0);
	public LongData S6239protectWritten = new LongData(0);
	public GeneralTable s6239screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6239screensfl;
	}

	public S6239ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "80",null, null, null, null, null, null, null, null});
		fieldIndMap.put(indOut,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsOut,new String[] {null, null, null, "04",null, null, null, null, null, null, null, null});//ILJ-388
		fieldIndMap.put(totalPremOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});//ILJ-388
		
		screenSflFields = new BaseData[] {hlifcnum, hlifeno, hsuffix, hcoverage, hrider, hcrtable, linetype, select, cmpntnum, component, compdesc, statcode, pstatcode, sumins, instPrem};
		screenSflOutFields = new BaseData[][] {hlifcnumOut, hlifenoOut, hsuffixOut, hcoverageOut, hriderOut, hcrtableOut, linetypeOut, selectOut, cmpntnumOut, componentOut, cmpntdescOut, riskdescOut, premdescOut, suminsOut, instPremOut};
		screenSflErrFields = new BaseData[] {hlifcnumErr, hlifenoErr, hsuffixErr, hcoverageErr, hriderErr, hcrtableErr, linetypeErr, selectErr, cmpntnumErr, componentErr, cmpntdescErr, riskdescErr, premdescErr, suminsErr, instPremErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, lifenum, lifename, jlife, jlifename, planSuffix, ind, totalPrem};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, plnsfxOut, indOut, totalPremOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, plnsfxErr, indErr, totalPremErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6239screen.class;
		screenSflRecord = S6239screensfl.class;
		screenCtlRecord = S6239screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6239protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6239screenctl.lrec.pageSubfile);
	}
}
