package com.csc.life.enquiries.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.model.Clntpf;
import  com.csc.life.enquiries.dataaccess.dao.AgntenqpfDAO;
import com.csc.life.enquiries.dataaccess.model.Agntenqpf;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgntenqpfDAOImpl extends   BaseDAOImpl<Agntenqpf> implements AgntenqpfDAO{
	
	  
	  private static final Logger LOGGER = LoggerFactory.getLogger(AgntenqpfDAOImpl.class);
		 //private Map<String,Agntenqpf> retentionMap = new HashMap<String,Agntenqpf>();
		 
		 
		 public List<Agntenqpf> getAgntenqpfData(Agntenqpf agntenqpfModel) {
			// ---------------------------------
			// Initialize variables
			// ---------------------------------
			Agntenqpf agntenqpf = null;
			List<Agntenqpf> agntenqpfReadResult = new LinkedList<Agntenqpf>();
			PreparedStatement stmn = null;
			ResultSet rs = null;
			StringBuilder sqlStringBuilder = new StringBuilder();
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sqlStringBuilder.append("SELECT  CLNTNUM ");
			sqlStringBuilder.append(" FROM AGNTENQ  ");
			sqlStringBuilder.append(" WHERE AGNTCOY=? AND AGNTNUM=? AND VALIDFLAG=? ");
			stmn = getPrepareStatement(sqlStringBuilder.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmn.setString(1, agntenqpfModel.getAgntcoy());
				stmn.setString(2, agntenqpfModel.getAgntnum());
				stmn.setString(3, agntenqpfModel.getValidflag());
				
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				rs = executeQuery(stmn);


				while (rs.next()) {
					agntenqpf = new Agntenqpf();
					agntenqpf.setClntnum(rs.getString(1));
					agntenqpfReadResult.add(agntenqpf);
				}

			} catch (SQLException e) {
				LOGGER.error("getAgntenqpfData()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(stmn, rs);
			}
			return agntenqpfReadResult;
		}


		@Override
		public List<Agntenqpf> getAgntenqpfDataWithClntInform(
				Agntenqpf agntenqpfModel, Wsspcomn wsspcomn) {
			// ---------------------------------
						// Initialize variables
						// ---------------------------------
						Agntenqpf agntenqpf = null;
						List<Agntenqpf> agntenqpfReadResult = new LinkedList<Agntenqpf>();
						PreparedStatement stmn = null;
						ResultSet rs = null;
						StringBuilder sqlStringBuilder = new StringBuilder();
						
						// ---------------------------------
						// Construct Query
						// ---------------------------------
						sqlStringBuilder.append("SELECT  t1.CLNTNUM, t2.CLTTYPE, t2.SURNAME, t2.GIVNAME, t2.CLTTYPE, t2.ETHORIG, t2.SALUTL, t2.LSURNAME, t2.LGIVNAME, t2.VALIDFLAG ");
						sqlStringBuilder.append(" FROM AGNTENQ t1, CLTS t2 ");
						sqlStringBuilder.append(" WHERE t2.CLNTNUM = t1.CLNTNUM and t1.AGNTCOY=? AND t1.AGNTNUM=? AND t1.VALIDFLAG=? and t2.CLNTPFX =? AND t2.CLNTCOY =? AND t2.VALIDFLAG =1");
						stmn = getPrepareStatement(sqlStringBuilder.toString());
						
						try {
							// ---------------------------------
							// Set Parameters dynamically
							// ---------------------------------
							stmn.setString(1, agntenqpfModel.getAgntcoy());
							stmn.setString(2, agntenqpfModel.getAgntnum());
							stmn.setString(3, agntenqpfModel.getValidflag());
							stmn.setString(4, "CN");
							stmn.setString(5, wsspcomn.fsuco.toString());
							
							// ---------------------------------
							// Execute Query
							// ---------------------------------
							rs = executeQuery(stmn);


							while (rs.next()) {
								agntenqpf = new Agntenqpf();
								agntenqpf.setClntnum(rs.getString(1));
								Clntpf clntpf= new Clntpf();
				              	
				              	clntpf.setClttype(rs.getString(2));
				              	clntpf.setSurname(rs.getString(3));
				              	clntpf.setGivname(rs.getString(4));
				              	clntpf.setClttype(rs.getString(5));
				              	clntpf.setEthorig(rs.getString(6));
				              	clntpf.setSalutl(rs.getString(7));
				              	clntpf.setLsurname(rs.getString(8));
				              	clntpf.setLgivname(rs.getString(9));
				              	clntpf.setValidflag(rs.getString(10));
				              	agntenqpf.setClntpf(clntpf);
								agntenqpfReadResult.add(agntenqpf);
							}

						} catch (SQLException e) {
							LOGGER.error("getAgntenqpfDataWithClntInform()", e);//IJTI-1561
							throw new SQLRuntimeException(e);
						} finally {
							close(stmn, rs);
						}
						return agntenqpfReadResult;
					}


}
