package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH5LL
 * @version 1.0 generated on 1/10/14 9:44 PM
 * @author CSC
 */
public class Sh5llScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(127);
	public FixedLengthStringData dataFields = new FixedLengthStringData(47).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);	
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public ZonedDecimalData day = DD.zchrgday.copyToZonedDecimal().isAPartOf(dataFields,44);
 
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 47);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData dayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 67);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] dayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh5llscreenWritten = new LongData(0);
	public LongData Sh5llprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh5llScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dayOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		
		
		screenFields = new BaseData[] {company,tabl,item, longdesc,day };
		screenOutFields = new BaseData[][] {companyOut,tablOut,itemOut, longdescOut, dayOut };
		screenErrFields = new BaseData[] {companyErr,tablErr,itemErr, longdescErr,dayErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh5llscreen.class;
		protectRecord = Sh5llprotect.class;
	}

}
