package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:45
 * Description:
 * Copybook name: ACMVSACKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvsackey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvsacFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData acmvsacKey = new FixedLengthStringData(256).isAPartOf(acmvsacFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvsacRldgcoy = new FixedLengthStringData(1).isAPartOf(acmvsacKey, 0);
  	public FixedLengthStringData acmvsacSacscode = new FixedLengthStringData(2).isAPartOf(acmvsacKey, 1);
  	public FixedLengthStringData acmvsacRldgacct = new FixedLengthStringData(16).isAPartOf(acmvsacKey, 3);
  	public FixedLengthStringData acmvsacSacstyp = new FixedLengthStringData(2).isAPartOf(acmvsacKey, 19);
  	public FixedLengthStringData acmvsacOrigcurr = new FixedLengthStringData(3).isAPartOf(acmvsacKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(232).isAPartOf(acmvsacKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvsacFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvsacFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}