/*
 * File: Pr50o.java
 * Date: 30 August 2009 1:32:49
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50O.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrtrnTableDAM;
import com.csc.life.enquiries.screens.Sr50oScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrrevTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import java.util.List;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import java.math.BigDecimal;
/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* PR50O - Component Details.
* The program allows to enquire on the component history of a
* contract, user can view changes made on the selected transaction.
* At the bottom of the screen, user can put 'X' against the Spec
* Terms, sub program will allow user to view the Special Terms
* Detail screen SR50S.
*
* 1000-section
* - Initialize screen fields
* - Call option switch using 'INIT' function
* - Retrieve data from CHDRTRX and COVRTRN files by using RETRV
*   function
* - Set the screen header fields
*
* 2000-section
* - Call option switch by using 'CHCK' function
*
* 3000-section
*
* 4000-section
* - Call option switch by using 'STCK' function
*
***********************************************************************
* </pre>
*/
public class Pr50o extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50O");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
		/* WSAA-CLNT */
	private FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8);
	
	/*BRD-306 STARTS*/
	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	/*BRD-306 ENDS*/
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String tr52d = "TR52D";
	private static final String t2240 = "T2240";//BRD-306

	private static final String covrenqrec = "COVRENQREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String payrrevrec = "PAYRREVREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrtrnTableDAM covrtrnIO = new CovrtrnTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrrevTableDAM payrrevIO = new PayrrevTableDAM();
	private Desckey wsaaDesckey = new Desckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Optswchrec optswchrec = new Optswchrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr52drec tr52drec = new Tr52drec();
	private T2240rec t2240rec = new T2240rec();//BRD-306
	private Txcalcrec txcalcrec = new Txcalcrec();
	private boolean loadingFlag = false;//ILIFE-3399
	/*BRD-306 STARTS */
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private String occuptationCode="";
	/*BRD-306 END */
	private boolean occFlag = false;//BRD-009
	private boolean stampDutyflag = false;//ILIFE-3509
	private T5687rec t5687rec = new T5687rec();
	
	// private Sr50oScreenVars sv = ScreenProgram.getScreenVars( Sr50oScreenVars.class);
	/*MLI changes START*/
	private Sr50oScreenVars sv = getLScreenVars();

	protected Sr50oScreenVars getLScreenVars() {

		return ScreenProgram.getScreenVars(Sr50oScreenVars.class);

	}
	
	//MLI changes END
	private boolean dialdownFlag = false;
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrenqpf = new Covrpf();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private boolean exclFlag = false;
	public Pr50o() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50o", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.annivProcDate.set(varcom.vrcmMaxDate);
		sv.crrcd.set(varcom.vrcmMaxDate);
		sv.instPrem.set(ZERO);
		sv.premcess.set(varcom.vrcmMaxDate);
		sv.rcdate.set(varcom.vrcmMaxDate);
		sv.rerateDate.set(varcom.vrcmMaxDate);
		sv.rerateFromDate.set(varcom.vrcmMaxDate);
		sv.singlePremium.set(ZERO);
		sv.sumin.set(ZERO);
		sv.zlinstprem.set(ZERO);
		/*BRD-306 START */
		sv.loadper.set(ZERO);
		sv.rateadj.set(ZERO);
		sv.fltmort.set(ZERO);
		sv.premadj.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.adjustageamt.set(ZERO);
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		/*BRD-306 END */
		sv.zstpduty01.set(ZERO);
		wsaaTaxamt.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.statcode.set(SPACES);//BRD-009
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		initOptswch1100();
		chdrtrxIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		/*BRD-306 START */
		sv.cnttype.set(chdrtrxIO.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrtrxIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypdesc.fill("?");
		}
		else {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		/*BRD-306 END */
		covrtrnIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrtrnIO.getStatuz());
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrtrxIO.getChdrnum());
		sv.cnttype.set(chdrtrxIO.getCnttype());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(lifelnbIO.getLifcnum());
		a1000CallNamadrs();
		sv.lifedesc.set(namadrsrec.name);
		/* Check for the existence of Joint Life details.*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isEQ(lifelnbIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifelnbIO.getLifcnum());
			wsaaClntPrefix.set(fsupfxcpy.clnt);
			wsaaClntCompany.set(wsspcomn.fsuco);
			wsaaClntNumber.set(lifelnbIO.getLifcnum());
			a1000CallNamadrs();
			sv.jlifedesc.set(namadrsrec.name);
		}
		/* Obtain the Contract Type description from T5688.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5688);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getCnttype());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypdesc.fill("?");
		}
		else {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3623);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getStatcode());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3588);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getPstatcode());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		sv.life.set(covrtrnIO.getLife());
		sv.coverage.set(covrtrnIO.getCoverage());
		sv.rider.set(covrtrnIO.getRider());
		sv.crtable.set(covrtrnIO.getCrtable());
		/* Obtain the COVERAGE description from T5687.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5687);
		wsaaDesckey.descDescitem.set(covrtrnIO.getCrtable());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.crtabled.fill("?");
		}
		else {
			/*    MOVE DESC-SHORTDESC      TO SR50O-CRTABLED                */
			sv.crtabled.set(descIO.getLongdesc());
		}
		payrrevIO.setDataArea(SPACES);
		payrrevIO.setStatuz(varcom.oK);
		payrrevIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		payrrevIO.setChdrnum(chdrtrxIO.getChdrnum());
		payrrevIO.setPayrseqno(1);
		payrrevIO.setTranno(covrtrnIO.getTranno());
		payrrevIO.setFormat(payrrevrec);
		payrrevIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(),varcom.oK)
		&& isNE(payrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(payrrevIO.getStatuz());
			syserrrec.params.set(payrrevIO.getParams());
			fatalError600();
		}
		if (isNE(payrrevIO.getStatuz(),varcom.oK)
		|| isNE(payrrevIO.getChdrcoy(),chdrtrxIO.getChdrcoy())
		|| isNE(payrrevIO.getChdrnum(),chdrtrxIO.getChdrnum())
		|| isNE(payrrevIO.getPayrseqno(),1)) {
			payrrevIO.setDataArea(SPACES);
			payrrevIO.setStatuz(varcom.oK);
			payrrevIO.setChdrcoy(chdrtrxIO.getChdrcoy());
			payrrevIO.setChdrnum(chdrtrxIO.getChdrnum());
			payrrevIO.setPayrseqno(1);
			payrrevIO.setTranno(covrtrnIO.getTranno());
			payrrevIO.setFormat(payrrevrec);
			payrrevIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			payrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			payrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","PAYRSEQNO");
			SmartFileCode.execute(appVars, payrrevIO);
			if (isNE(payrrevIO.getStatuz(),varcom.oK)
			&& isNE(payrrevIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(payrrevIO.getStatuz());
				syserrrec.params.set(payrrevIO.getParams());
				fatalError600();
			}
			if (isNE(payrrevIO.getStatuz(),varcom.oK)
			|| isNE(payrrevIO.getChdrcoy(),chdrtrxIO.getChdrcoy())
			|| isNE(payrrevIO.getChdrnum(),chdrtrxIO.getChdrnum())
			|| isNE(payrrevIO.getPayrseqno(),1)) {
				syserrrec.statuz.set(payrrevIO.getStatuz());
				syserrrec.params.set(payrrevIO.getParams());
				fatalError600();
			}
		}
		sv.billfreq.set(payrrevIO.getBillfreq());
		sv.mop.set(payrrevIO.getBillchnl());
		sv.sumin.set(covrtrnIO.getSumins());
		sv.singlePremium.set(covrtrnIO.getSingp());
		sv.instPrem.set(covrtrnIO.getInstprem());
		sv.zlinstprem.set(covrtrnIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covrtrnIO.getLoadper());
		sv.rateadj.set(covrtrnIO.getRateadj());
		sv.fltmort.set(covrtrnIO.fltmort);
		sv.premadj.set(covrtrnIO.getPremadj());
		sv.zbinstprem.set(covrtrnIO.getZbinstprem());
		sv.adjustageamt.set(covrtrnIO.getPremadj());
		/*BRD-306 END */
		
		/*ILIFE-3509 starts*/
		stampDutyflag = FeaConfg.isFeatureExist(covrtrnIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			if(isNE(covrtrnIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
				sv.zstpduty01.set(covrtrnIO.getZstpduty01());
			}
			
		}
      /*ILIFE-3509 ends*/
		/*BRD-306 STARTS */
		dialdownFlag = FeaConfg.isFeatureExist(chdrtrxIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrtrxIO.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrtrxIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag || premiumflag || dialdownFlag ){
			callReadRCVDPF();
		}
		if(!incomeProtectionflag){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
			sv.poltypOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.waitperiodOut[varcom.nd.toInt()].set("N");
			sv.bentrmOut[varcom.nd.toInt()].set("N");
			sv.poltypOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
					sv.waitperiod.set(rcvdPFObject.getWaitperiod());
				}
				if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
					sv.poltyp.set(rcvdPFObject.getPoltyp());
				}
				if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
					sv.bentrm.set(rcvdPFObject.getBentrm());
				}
			}
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		/*BRD-306 END */
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
				sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends
		exclFlag = FeaConfg.isFeatureExist(chdrtrxIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.optind03Out[varcom.nd.toInt()].set("Y");
		}
		sv.rcdate.set(varcom.vrcmMaxDate);
		sv.crrcd.set(covrtrnIO.getCrrcd());
		sv.premcess.set(covrtrnIO.getPremCessDate());
		sv.riskCessDate.set(covrtrnIO.getRiskCessDate());
		sv.riskCessAge.set(covrtrnIO.getRiskCessAge());
		sv.premCessAge.set(covrtrnIO.getPremCessAge());
		sv.riskCessTerm.set(covrtrnIO.getRiskCessTerm());
		sv.premCessTerm.set(covrtrnIO.getPremCessTerm());
		sv.benCessDate.set(covrtrnIO.getBenCessDate());
		sv.benCessAge.set(covrtrnIO.getBenCessAge());
		sv.benCessTerm.set(covrtrnIO.getBenCessDate());
		
		sv.rerateDate.set(covrtrnIO.getRerateDate());
		sv.rerateFromDate.set(covrtrnIO.getRerateFromDate());
		sv.annivProcDate.set(covrtrnIO.getAnnivProcDate());
		sv.mortcls.set(covrtrnIO.getMortcls());
		
		
		/*BRD-306 STARTS*/
		sv.anbAtCcd.set(covrtrnIO.getAnbAtCcd());
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t2240);
		/* MOVE CHDRENQ-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrtrxIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			/* MTL002 */	
			}
		}
		/*BRD-306 ENDS*/
		//ILIFE-3399-STARTS
		loadingFlag = FeaConfg.isFeatureExist(covrtrnIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-3399-ends
		//ILIFE-3518-STARTS
		//ItempfDAO itempfDAO = DAOFactory.getItempfDAO();//ILIFE-3955
		Itempf itempf = new Itempf();
		itempf.setItemtabl("T5687");
		itempf.setItemitem(covrtrnIO.getCrtable().toString());
		itempf.setItemcoy(covrtrnIO.getChdrcoy().toString().trim());
		itempf.setItempfx("IT");
		itempf.setItmfrm(new BigDecimal(chdrtrxIO.getOccdate().toString()));
		itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
		List<Itempf> itempfList = itemDAO.findByItemDates(itempf);//ILIFE-3955
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {
				t5687rec.t5687Rec.set(new String(it.getGenarea()));
			}
		}
		//BRD-009-STARTS
		occFlag = FeaConfg.isFeatureExist(covrtrnIO.getChdrcoy().toString().trim(), "NBPROP05", appVars, "IT");
		if((occFlag) &&
		isEQ(t5687rec.premmeth,"PM30")){
			rcvdPFObject= new Rcvdpf();
			callReadRCVDPF();
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getStatcode()!=null && !rcvdPFObject.getStatcode().trim().equals("") ){
					sv.statcode.set(rcvdPFObject.getStatcode());
				}
			}
		}
		else{
			sv.statcodeOut[varcom.nd.toInt()].set("Y");
		}
		//BRD-009-ENDS
	}
/*BRD-306 STARTS */
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covrtrnIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covrtrnIO.getChdrnum().toString());
	rcvdPFObject.setLife(covrtrnIO.getLife().toString());
	rcvdPFObject.setCoverage(covrtrnIO.getCoverage().toString());
	rcvdPFObject.setRider(covrtrnIO.getRider().toString());
	rcvdPFObject.setCrtable(covrtrnIO.getCrtable().toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}
/*BRD-306 ENDS */
protected void initOptswch1100()
	{
		begin1100();
	}

protected void begin1100()
	{
		/*  Read TR52D for Taxcode.                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrtrxIO.getRegister());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		/* Retrieve Line Select and Function Key switching options.*/
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		for (ix.set(1); !(isGT(ix,4)); ix.add(1)){
			sv.optindO[ix.toInt()][varcom.pr.toInt()].set("Y");
			sv.optindO[ix.toInt()][varcom.nd.toInt()].set("Y");
		}
		for (ix.set(1); !(isGT(ix,20)
		|| isEQ(optswchrec.optsType[ix.toInt()],SPACES)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"X")) {
				sv.optindO[ix.toInt()][varcom.pr.toInt()].set("N");
				sv.optindO[ix.toInt()][varcom.nd.toInt()].set("N");
				sv.optdsc[ix.toInt()].set(optswchrec.optsDsc[ix.toInt()]);
				sv.optind[ix.toInt()].set(optswchrec.optsInd[ix.toInt()]);
			}
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc02Out[varcom.nd.toInt()].set("N");
			sv.optind02Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc02Out[varcom.pr.toInt()].set("N");
			sv.optind02Out[varcom.pr.toInt()].set("N");
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5000();
		}
		if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.optdsc02Out[varcom.nd.toInt()].set("Y");
			sv.optind02Out[varcom.nd.toInt()].set("Y");
			sv.optdsc02Out[varcom.pr.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.optind02Out[varcom.pr.toInt()].set("Y");
		}
		sv.statcodeOut[varcom.pr.toInt()].set("Y");//BRD-009
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/* Validate "check box"*/
		optswchChck2100();
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void optswchChck2100()
	{
		start2100();
	}

protected void start2100()
	{
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		optswchrec.optsInd[1].set(sv.optind01);
		optswchrec.optsInd[2].set(sv.optind02);
		optswchrec.optsInd[3].set(sv.optind03);
		optswchrec.optsInd[4].set(sv.optind04);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.optind01Err.set(optswchrec.optsStatuz);
			sv.optind02Err.set(optswchrec.optsStatuz);
			sv.optind03Err.set(optswchrec.optsStatuz);
			sv.optind04Err.set(optswchrec.optsStatuz);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
		optswch4080();
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/* Check for check box switching*/
		cboxSwitching4300();
	}

protected void optswch4080()
	{
	      wsspcomn.chdrChdrnum.set(covrtrnIO.getChdrnum());
	      wsspcomn.crtable.set(covrtrnIO.getCrtable());
	      
	      wsspcomn.cmode.set("IFE");
	     optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optind03.set(optswchrec.optsInd[3]);
		sv.optind04.set(optswchrec.optsInd[4]);
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void cboxSwitching4300()
	{
		begn4300();
	}

protected void begn4300()
	{
		if (isEQ(sv.optind01,"X")) {
			optswchrec.optsInd[1].set(sv.optind01);
			sv.optind01.set(SPACES);
		}
		if (isEQ(sv.optind02,"X")) {
			optswchrec.optsInd[2].set(sv.optind02);
			sv.optind02.set(SPACES);
		}
		if (isEQ(sv.optind03,"X")) {
			optswchrec.optsInd[3].set(sv.optind03);
			sv.optind03.set(SPACES);
		}
		if (isEQ(sv.optind04,"X")) {
			optswchrec.optsInd[4].set(sv.optind04);
			sv.optind04.set(SPACES);
		}
		if (isNE(optswchrec.optsInd[2], SPACES)) {
			//ILIFE-3879 starts
			covrenqpf = new Covrpf();
		//	covrenqIO.setDataArea(SPACES);
			covrenqpf.setChdrcoy(wsspcomn.company.toString());
			covrenqpf.setChdrnum(covrtrnIO.getChdrnum().toString());
			covrenqpf.setLife(covrtrnIO.getLife().toString());
			covrenqpf.setPlanSuffix(covrtrnIO.getPlanSuffix().toInt());
			covrenqpf.setCoverage(covrtrnIO.getCoverage().toString());
			covrenqpf.setRider(covrtrnIO.getRider().toString());
			
			covrpfDAO.setCacheObject(covrenqpf);
			
		/*	covrenqIO.setFormat(covrenqrec);
			covrenqIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, covrenqIO);
			if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrenqIO.getParams());
				fatalError600();
			}*/
			
			//ENDS
		}
	}

protected void a1000CallNamadrs()
	{
		a1000Begin();
	}

protected void a1000Begin()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void checkCalcTax5000()
	{
		start5010();
	}

protected void start5010()
	{
		/* CALL TR52D TAX SUBROUTINE                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrtrxIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrtrxIO.getChdrnum());
		txcalcrec.life.set(covrtrnIO.getLife());
		txcalcrec.coverage.set(covrtrnIO.getCoverage());
		txcalcrec.rider.set(covrtrnIO.getRider());
		txcalcrec.planSuffix.set(covrtrnIO.getPlanSuffix());
		txcalcrec.crtable.set(covrtrnIO.getCrtable());
		txcalcrec.cnttype.set(chdrtrxIO.getCnttype());
		txcalcrec.register.set(chdrtrxIO.getRegister());
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.ccy.set(chdrtrxIO.getCntcurr());
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.amountIn.set(ZERO);
		txcalcrec.transType.set("PREM");
		txcalcrec.txcode.set(tr52drec.txcode);
		if (isEQ(chdrtrxIO.getBillfreq(), "00")) {
			txcalcrec.effdate.set(chdrtrxIO.getOccdate());
		}
		else {
			txcalcrec.effdate.set(chdrtrxIO.getPtdate());
		}
		txcalcrec.tranno.set(chdrtrxIO.getTranno());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			wsaaTaxamt.set(sv.instPrem);
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTaxamt.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTaxamt.add(txcalcrec.taxAmt[2]);
			}
			sv.taxamt.set(wsaaTaxamt);
			sv.optind02.set("+");
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc02Out[varcom.nd.toInt()].set("N");
			sv.optind02Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc02Out[varcom.pr.toInt()].set("N");
			sv.optind02Out[varcom.pr.toInt()].set("N");
		}
	}

protected void a2000GetDesc()
	{
		a2000Begin();
	}

protected void a2000Begin()
	{
		descIO.setDataArea(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsaaDesckey.descDesccoy);
		descIO.setDesctabl(wsaaDesckey.descDesctabl);
		descIO.setDescitem(wsaaDesckey.descDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
//setter getter
public ChdrtrxTableDAM getChdrtrxIO() {
	return chdrtrxIO;
}

public void setChdrtrxIO(ChdrtrxTableDAM chdrtrxIO) {
	this.chdrtrxIO = chdrtrxIO;
}
}
