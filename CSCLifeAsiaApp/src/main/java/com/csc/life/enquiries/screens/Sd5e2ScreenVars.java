package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sd5e2
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class Sd5e2ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(316);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(172).isAPartOf(dataArea, 0);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,8);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,55);
	public ZonedDecimalData accident = DD.fundamnt.copyToZonedDecimal().isAPartOf(dataFields,70);
	public ZonedDecimalData hospital = DD.fundamnt.copyToZonedDecimal().isAPartOf(dataFields,87);
	public ZonedDecimalData fundamnt = DD.fundamnt.copyToZonedDecimal().isAPartOf(dataFields,104); 
	public ZonedDecimalData loanVal = DD.loanallow.copyToZonedDecimal().isAPartOf(dataFields,121);
	public ZonedDecimalData surrval = DD.surrval.copyToZonedDecimal().isAPartOf(dataFields,138);
	public ZonedDecimalData bonusVal = DD.bonusval.copyToZonedDecimal().isAPartOf(dataFields,155);
		
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 172);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData accidentErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData hospitalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData fundamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData loanValErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData surrvalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData bonusValErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 208);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] accidentOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] hospitalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] fundamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] loanValOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] surrvalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] bonusValOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
			
	public FixedLengthStringData subfileArea = new FixedLengthStringData(159);
	
	public FixedLengthStringData subfileFields = new FixedLengthStringData(60).isAPartOf(subfileArea, 0);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData chdrnum = DD.cownnum.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData cnttype = DD.cnttyp.copy().isAPartOf(subfileFields,9);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData premstatus = DD.premstatus_longdesc.copy().isAPartOf(subfileFields,22);
	public ZonedDecimalData occdate = DD.hprrcvdt.copyToZonedDecimal().isAPartOf(subfileFields,52);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 60);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	
	
	
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 84);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 156);
	
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);


	public LongData Sd5e2screensflWritten = new LongData(0);
	public LongData Sd5e2screenctlWritten = new LongData(0);
	public LongData Sd5e2screenWritten = new LongData(0);
	public LongData Sd5e2protectWritten = new LongData(0);
	public GeneralTable sd5e2screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sd5e2screensfl;
	}

	public Sd5e2ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(bonusValOut,new String[] {"01","02", "-01","02", null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, chdrnum, cnttype, chdrstatus, premstatus, occdate};
		screenSflOutFields = new BaseData[][] {selectOut, chdrnumOut, cnttypeOut, chdrstatusOut, premstatusOut, occdateOut};
		screenSflErrFields = new BaseData[] {selectErr, chdrnumErr, cnttypeErr, chdrstatusErr, premstatusErr, occdateErr};
		screenSflDateFields = new BaseData[] {occdate};
		screenSflDateErrFields = new BaseData[] {occdateErr};
		screenSflDateDispFields = new BaseData[] {occdateDisp};
		/*fieldIndMap.put(bonusValOut,new String[] {"90","91", "-90","91", null, null, null, null, null, null, null, null});*/
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {cownnum, ownername, sumin, accident, hospital, fundamnt,loanVal, bonusVal};
		screenOutFields = new BaseData[][] {cownnumOut, ownernameOut, suminOut, accidentOut, hospitalOut, fundamntOut, loanValOut, bonusValOut};
		screenErrFields = new BaseData[] {cownnumErr, ownernameErr, suminErr, accidentErr, hospitalErr, fundamntErr, loanValErr, bonusValErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5e2screen.class;
		screenSflRecord = Sd5e2screensfl.class;
		screenCtlRecord = Sd5e2screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5e2protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5e2screenctl.lrec.pageSubfile);
	}
}
