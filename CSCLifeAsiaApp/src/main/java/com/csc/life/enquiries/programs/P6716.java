/*
 * File: P6716.java
 * Date: 30 August 2009 0:54:54
 * Author: Quipoz Limited
 * 
 * Class transformed from P6716.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Cltskey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
//import com.csc.fsu.general.dataaccess.model.Chdrpf;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.screens.S6716ScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Lives Assured Detail Enquiry.
* ----------------------------
*
*INITIALISE
*  - If returning from iterative processing skip the section.
*  - Initialise the screen data, numeric fields and the screen
*status held in working storage.
*  - Retrieve the batchkey into working storage for extraction
*of the transaction code.
*  - Retrieve the contract header details (CHDRLNB).
*  - Retrieve Life details (LIFELNB).
*  - Release Life record for use within the system.
*  - Read contract definition table, T5688 (using contract type
*and original contract commencement date), for the relevance of
*joint lives for the contract.
*  - Set up header fields on the screen.
*  - Retrieve client details and move fields to screen fields
*for display.
*
*
*SCREEN EDIT AND VALIDATION.
*  -  Set WSSP-EDTERROR to O-K.
*  -  If returning from iterative processing skip this section.
*  -  Display screen and check for screen errors.
*  -  Set WSSP-EDTERROR to O-K.
*  -  Check for use of CF11 'KILL', and abort the section.
*  -  Validate the client details selection indicator, only 'X'
*or space are valid.
*  -  Perform scrolling validation, which restricts a ROLLDOWN
*if your viewing the second life and ROLLUP if your viewing the
*first life.
*  -  If client details are selected store the screen statuz in
*working storage.
*  -  Check for field errors.
*
*UPDATES
*  there are none
*
*WHERE NEXT
*  -  Move this program to WSSP-NEXTPROG.
*  -  If returning from iterative processing,
*         restore the original program stack,
*         reset the secondary action,
*         check for 'KILL' request or
*         continue and scroll to second life (if requested
*         before iterative processing was actioned) or
*         move the screen name to nextprog and exit.
*
*  -  If its the first time through,
*         If 'KILL' is requested move spaces to the secondary
*             stack and abort section
*         If client details are requested
*             save the program stack
*             perform generalised switching section (T1675)
*             move the client details to WSSP
*         If client details are not required continue on to
*             next program in the current stack
*         If scrolling is requested
*             read the life details to display the relevant life
*             (redisplaying this screen).
*
*****************************************************************
* </pre>
*/
public class P6716 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6716");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private String wsaaRollIndc = "N";

	private FixedLengthStringData wsaaFoundCheckBox = new FixedLengthStringData(1);
	private Validator foundUndwCheck = new Validator(wsaaFoundCheckBox, "Y");

	private FixedLengthStringData wsaaFoundCCheckBox = new FixedLengthStringData(1);
	protected Validator foundClntCheck = new Validator(wsaaFoundCCheckBox, "Y");

	private FixedLengthStringData wsaaUnderwritingReqd = new FixedLengthStringData(1);
	protected Validator underwritingReqd = new Validator(wsaaUnderwritingReqd, "Y");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private static final String e268 = "E268";
	private static final String h143 = "H143";
	private static final String f250 = "F250";
	private static final String f782 = "F782";
	private static final String h118 = "H118";
	private static final String e040 = "E040";
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t2240 = "T2240";
	private static final String tr675 = "TR675";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String lifelnbrec = "LIFELNBREC";
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private UndlTableDAM undlIO = new UndlTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private Cltskey wsaaCltsKey = new Cltskey();
	private T5688rec t5688rec = new T5688rec();
	private T2240rec t2240rec = new T2240rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	protected Optswchrec optswchrec = new Optswchrec();
	private Wssplife wssplife = new Wssplife();
	/*private S6716ScreenVars sv = ScreenProgram.getScreenVars( S6716ScreenVars.class);*/
	private S6716ScreenVars sv =   getLScreenVars();
	private Itempf itempf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private T3644rec t3644rec = new T3644rec();
	private boolean benesequence = false;
	//ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	//ILJ-387 end

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090, 
		exit2290, 
		clearScreen4450, 
		redisplay4460, 
		exit4490
	}

	public P6716() {
		super();
		screenVars = sv;
		new ScreenModel("S6716", AppVars.getInstance(), sv);
	}
	protected S6716ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6716ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.dob.set(varcom.vrcmMaxDate);
		sv.anbage.set(ZERO);
		sv.height.set(ZERO);
		sv.weight.set(ZERO);
		sv.bmi.set(ZERO);
		wsaaBatcKey.set(wsspcomn.batchkey);
		/*  Retrieve contract header details*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		/*  Retrieve life details*/
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		wsaaUnderwritingReqd.set("N");
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tr675);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),tr675)
		|| isNE(itdmIO.getItemitem(),chdrpf.getCnttype())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			underwritingReqd.setTrue();
		}
		callOptswch1100();
		lifelnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t2240);
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t2240);
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			/* MTL002 */	
			}
		}
		sv.zagelitOut[varcom.hi.toInt()].set("N");
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrpf.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(e268);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5688rec.jlifemax,ZERO)) {
			sv.rollitOut[varcom.nd.toInt()].set("Y");
			sv.jlifeOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(lifelnbIO.getJlife(),ZERO)) {
			sv.relationOut[varcom.nd.toInt()].set("Y");
		}
		
		// ILJ-387 Start
		boolean cntEnqFlag = false;
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if (!cntEnqFlag) {
			sv.cntEnqScreenflag.set("N");
		}else {
			sv.cntEnqScreenflag.set("Y");
		}
		// ILJ-387 End
		/*
		 * fwang3 ICIL-4
		 */
		benesequence = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP087", appVars, "IT"); // fwang3 ICIL-4	
		if (benesequence) {
			sv.relationwithowner.set(lifelnbIO.getRelation());
		} else {
			sv.relationwithownerOut[varcom.nd.toInt()].set("Y");
		}
		sv.chdrnum.set(lifelnbIO.getChdrnum());
		sv.life.set(lifelnbIO.getLife());
		sv.jlife.set(lifelnbIO.getJlife());
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsio1900();
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		sv.lifenum.set(lifelnbIO.getLifcnum());
		sv.sex.set(cltsIO.getCltsex());
		sv.dob.set(cltsIO.getCltdob());
		sv.sex.set(lifelnbIO.getCltsex()); //PINNACLE-2643
		sv.dob.set(lifelnbIO.getCltdob()); //PINNACLE-2643
		sv.anbage.set(lifelnbIO.getAnbAtCcd());
		sv.selection.set(lifelnbIO.getSelection());
		sv.relation.set(lifelnbIO.getLiferel());
		if (isEQ(lifelnbIO.getAgeadm(),SPACES)) {
			sv.dummyOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.dummyOut[varcom.nd.toInt()].set(SPACES);
		}
		sv.smoking.set(lifelnbIO.getSmoking());
		sv.occup.set(lifelnbIO.getOccup());//ICIL-236
		boolean chinaOccupPermission = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "NBPRP056", appVars, "IT"); // fwang3 ICIL-4
		if(chinaOccupPermission) {
			getOccupationClass1900();
			sv.occupcls.set(t3644rec.occupationClass);//ICIL-236
		} else {
			sv.occupclsOut[varcom.nd.toInt()].set("Y");
		}
		sv.pursuit01.set(lifelnbIO.getPursuit01());
		sv.pursuit02.set(lifelnbIO.getPursuit02());
		if (isEQ(cltsIO.getClttype(),"C")) {
			sv.lifenum.set(SPACES);
			sv.lifename.set(SPACES);
		}
		if (underwritingReqd.isTrue()) {
			undlIO.setDataKey(SPACES);
			undlIO.setChdrcoy(chdrpf.getChdrcoy());
			undlIO.setChdrnum(chdrpf.getChdrnum());
			undlIO.setLife(lifelnbIO.getLife());
			undlIO.setJlife(lifelnbIO.getJlife());
			undlIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, undlIO);
			if (isNE(undlIO.getStatuz(),varcom.oK)
			&& isNE(undlIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(undlIO.getParams());
				syserrrec.statuz.set(undlIO.getStatuz());
				fatalError600();
			}
			if (isNE(undlIO.getStatuz(),varcom.mrnf)) {
				sv.height.set(undlIO.getHeight());
				sv.weight.set(undlIO.getWeight());
				sv.bmi.set(undlIO.getBmi());
			}
		}
	}

protected void getOccupationClass1900() {
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl("T3644");
	   itempf.setItemitem(sv.occup.toString());
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
}

protected void callOptswch1100()
	{
		init1110();
	}

protected void init1110()
	{
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set("0");
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optdsc01.set(optswchrec.optsDsc[1]);
		sv.optind01.set(optswchrec.optsInd[1]);
		if (underwritingReqd.isTrue()) {
			sv.optdsc02.set(optswchrec.optsDsc[2]);
			sv.optind02.set(optswchrec.optsInd[2]);
		}
		if (isEQ(sv.optdsc02,SPACES)) {
			sv.optind02Out[varcom.nd.toInt()].set("Y");
		}
	}

protected void cltsio1900()
	{
		/*CLTSIO*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.lifenumErr.set(h143);
		}
		else {
			if (isLTE(cltsIO.getCltdod(),chdrpf.getOccdate())&&isNE(wsspcomn.flag,"I")) {//IJS-302
				sv.lifenumErr.set(f782);
			}
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (underwritingReqd.isTrue()) {
			if (isEQ(optswchrec.optsType[2],"X")) {
				sv.optdsc02.set(optswchrec.optsDsc[2]);
				sv.optind02.set(optswchrec.optsInd[2]);
			}
		}
		sv.optdsc01.set(optswchrec.optsDsc[1]);
		sv.optind01.set(optswchrec.optsInd[1]);
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		checkOptions2100();
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isNE(sv.optind01,"X")
		&& isNE(sv.optind01,SPACES)) {
			sv.optind01Err.set(h118);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		checkScrolling2200();
		if (isEQ(sv.optind01,"X")) {
			if (isEQ(sv.lifenum,SPACES)) {
				sv.optind01Err.set(e040);
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkOptions2100()
	{
		check2110();
	}

protected void check2110()
	{
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		optswchrec.optsInd[1].set(sv.optind01);
		optswchrec.optsInd[2].set(sv.optind02);
		check2111CustomerSpecific();
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.optind01Err.set(optswchrec.optsStatuz);
			sv.optind02Err.set(optswchrec.optsStatuz);
		}
		check2112CustomerSpecific();
			
	}

protected void checkScrolling2200()
	{
		try {
			checkScrolling2210();
		}
		catch (GOTOException e){
		}
	}

protected void checkScrolling2210()
	{
		wsaaRollIndc = " ";
		if (isEQ(scrnparams.statuz,varcom.rold)) {
			rold2230();
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			rolu2240();
		}
		goTo(GotoLabel.exit2290);
	}

protected void rold2230()
	{
		if (isEQ(sv.jlife,"00")) {
			scrnparams.errorCode.set(f250);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaRollIndc = "D";
		}
	}

protected void rolu2240()
	{
		if (isEQ(sv.jlife,"01")) {
			scrnparams.errorCode.set(f250);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaRollIndc = "U";
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsaaFoundCheckBox.set("N");
		wsaaFoundCCheckBox.set("N");
		checkBoxs4600();
		lifelnbIO.setLife(sv.life);
		lifelnbIO.setJlife(sv.jlife);
		lifelnbIO.setFunction(varcom.keeps);
		lifelnbIO.setFormat(lifelnbrec);
		callLifelnbio5000();
		if (foundClntCheck.isTrue()) {
			wsaaCltsKey.cltsClntpfx.set(fsupfxcpy.clnt);
			wsaaCltsKey.cltsClntcoy.set(wsspcomn.fsuco);
			wsaaCltsKey.cltsClntnum.set(sv.lifenum);
			wsspcomn.clntkey.set(wsaaCltsKey.cltsKey);
		}
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		if (foundUndwCheck.isTrue()
		|| foundClntCheck.isTrue()) {
			nextProgram4500();
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.oK)) {
			nextProgram4500();
			return ;
		}
		else {
			scrolling4400();
		}
	}

protected void scrolling4400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					scroll4410();
				case clearScreen4450: 
					clearScreen4450();
				case redisplay4460: 
					redisplay4460();
					goBack4470();
				case exit4490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void scroll4410()
	{
		wsspcomn.nextprog.set(wsaaProg);
		scrnparams.positionCursor.set("CLIENTIND");
		if (isEQ(wsaaRollIndc,"U")) {
			if (isEQ(t5688rec.jlifemin,0)
			&& isEQ(t5688rec.jlifemax,0)) {
				nextProgram4500();
				goTo(GotoLabel.exit4490);
			}
			else {
				sv.outputIndicators.set(SPACES);
				goTo(GotoLabel.clearScreen4450);
			}
		}
		if (isEQ(wsaaRollIndc,"D")) {
			lifelnbIO.setJlife("00");
			lifelnbIO.setFunction("READR");
			callLifelnbio5000();
			sv.relationOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.redisplay4460);
		}
	}

protected void clearScreen4450()
	{
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction("READR");
		callLifelnbio5000();
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			scrnparams.errorCode.set(f250);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit4490);
		}
		else {
			lifelnbIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
				syserrrec.dbparams.set(lifelnbIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(wsaaRollIndc,"U")) {
			goTo(GotoLabel.redisplay4460);
		}
		else {
			nextProgram4500();
			goTo(GotoLabel.exit4490);
		}
	}

protected void redisplay4460()
	{
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsio1900();
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		sv.sex.set(cltsIO.getCltsex());
		sv.dob.set(cltsIO.getCltdob());
		sv.jlife.set(lifelnbIO.getJlife());
		sv.lifenum.set(lifelnbIO.getLifcnum());
		sv.anbage.set(lifelnbIO.getAnbAtCcd());
		sv.selection.set(lifelnbIO.getSelection());
		sv.relation.set(lifelnbIO.getLiferel());
		if (isEQ(lifelnbIO.getAgeadm(),SPACES)) {
			sv.dummyOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.dummyOut[varcom.nd.toInt()].set(SPACES);
		}
		sv.smoking.set(lifelnbIO.getSmoking());
		sv.occup.set(cltsIO.getOccpcode());
		sv.pursuit01.set(lifelnbIO.getPursuit01());
		sv.pursuit02.set(lifelnbIO.getPursuit02());
	}

protected void goBack4470()
	{
		if (isNE(wsaaRollIndc,"N")) {
			wsaaRollIndc = "N";
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		return ;
	}

protected void nextProgram4500()
	{
		next4510();
	}

protected void next4510()
	{
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(sv.chdrnum);
		lifelnbIO.setLife(sv.life);
		lifelnbIO.setJlife("00");
		callLifelnbio5000();
		lifelnbIO.setFunction(varcom.keeps);
		lifelnbIO.setFormat(lifelnbrec);
		callLifelnbio5000();
		wsspcomn.programPtr.add(1);
	}

protected void checkBoxs4600()
	{
		/*CHECK-BOXS*/
		wsaaFoundCheckBox.set("N");
		if (underwritingReqd.isTrue()) {
			if (isEQ(sv.optind02,"X")) {
				optswchrec.optsInd[2].set("X");
				sv.optind[2].set(SPACES);
				foundUndwCheck.setTrue();
			}
		}
		if (isEQ(sv.optind01,"X")) {
			optswchrec.optsInd[1].set("X");
			sv.optind[1].set(SPACES);
			foundClntCheck.setTrue();
		}
		/*EXIT*/
	}

protected void callLifelnbio5000()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.dbparams.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

public LifelnbTableDAM getLifelnbIO() {
	return lifelnbIO;
}


public void setLifelnbIO(LifelnbTableDAM lifelnbIO) {
	this.lifelnbIO = lifelnbIO;
}

protected void check2112CustomerSpecific(){}

protected void check2111CustomerSpecific(){}
}