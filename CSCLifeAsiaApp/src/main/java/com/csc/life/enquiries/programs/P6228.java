/*
 * File: P6228.java
 * Date: 30 August 2009 0:37:09
 * Author: Quipoz Limited
 * 
 * Class transformed from P6228.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.enquiries.dataaccess.AsgnenqTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.screens.S6228ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* ASSIGNEE ENQUIRIES.
*
* Retrieves the contract header details from the contract header
* file (CHDRENQ).    Using RETRV
*
* The assignee details (ie Address) are retrieved from the client
* details file (CLTS). If the assignee number (asgnnum from the
* header) is blank then the name and address fields are left
* blank.
*
* The assignee number may be changed, and will be checked
* against the client file.
*
* If F11 (scrn-statuz of KILL) is pressed then all validation
* is ignored.
*
* NOTE: The retrieval of assignee details and movement of these
* details for both the 1000 and 2000 section is in the
* 1600-RETRIEVAL section.
*
*****************************************************************
* </pre>
*/
public class P6228 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6228");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaFirstPage = "";

		/* WSAA-ROLL-FLGS */
	private FixedLengthStringData wsaaRolu = new FixedLengthStringData(1);
	private Validator rollup = new Validator(wsaaRolu, "Y");

	private FixedLengthStringData wsaaRold = new FixedLengthStringData(1);
	private Validator rolldown = new Validator(wsaaRold, "Y");

	private FixedLengthStringData wsaaNoMoreAssignees = new FixedLengthStringData(1);
	private Validator asgnenqEof = new Validator(wsaaNoMoreAssignees, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
		/* ERRORS */
	private String e040 = "E040";
	private String f782 = "F782";
	private String f498 = "F498";
	private String f499 = "F499";
		/* TABLES */
	private String t5500 = "T5500";
		/*Assignee Enquiry Logical*/
	private AsgnenqTableDAM asgnenqIO = new AsgnenqTableDAM();
		/*Contract Enquiry - Contract Header.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6228ScreenVars sv = ScreenProgram.getScreenVars( S6228ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		exit3090, 
		exit5090, 
		x190Exit
	}

	public P6228() {
		super();
		screenVars = sv;
		new ScreenModel("S6228", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		loadAssignee1020();
	}

protected void initialise1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		sv.dataArea.set(SPACES);
		sv.commfrom.set(varcom.vrcmMaxDate);
		sv.commto.set(varcom.vrcmMaxDate);
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf==null)
		{
			return;
		}
	}

protected void loadAssignee1020()
	{
		wsaaNoMoreAssignees.set("N");
		asgnenqIO.setDataArea(SPACES);
		asgnenqIO.setChdrcoy(chdrpf.getChdrcoy());
		asgnenqIO.setChdrnum(chdrpf.getChdrnum());
		asgnenqIO.setSeqno(ZERO);
		asgnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		asgnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		asgnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loadAssignee5000();
		wsaaFirstPage = "Y";
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.function.set(varcom.prot);
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.rold)
		&& isEQ(wsaaFirstPage,"Y")) {
			sv.asgnnumErr.set(f498);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)
		&& asgnenqEof.isTrue()) {
			sv.asgnnumErr.set(f499);
		}
		if (isEQ(scrnparams.statuz,varcom.rold)) {
			wsaaRold.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaRolu.set("Y");
		}
	}

protected void validate2020()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		goTo(GotoLabel.exit3090);
	}

protected void whereNext4000()
	{
		nextProgram4100();
	}

protected void nextProgram4100()
	{
		if (rollup.isTrue()) {
			wsaaFirstPage = "N";
			asgnenqIO.setFunction(varcom.nextr);
			loadAssignee5000();
		}
		if (rollup.isTrue()
		&& asgnenqEof.isTrue()) {
			sv.asgnnumErr.set(f499);
			asgnenqIO.setFunction(varcom.nextp);
			loadAssignee5000();
		}
		if (rolldown.isTrue()) {
			asgnenqIO.setFunction(varcom.nextp);
			loadAssignee5000();
		}
		if (rolldown.isTrue()
		&& asgnenqEof.isTrue()) {
			wsaaFirstPage = "Y";
			sv.asgnnumErr.set(f498);
			asgnenqIO.setFunction(varcom.nextr);
			loadAssignee5000();
		}
		if (rollup.isTrue()
		|| rolldown.isTrue()) {
			wsaaRolu.set("N");
			wsaaRold.set("N");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
		}
	}

protected void loadAssignee5000()
	{
		try {
			assigneeRead5010();
			asgnToScreen5030();
			reasonDesc5030();
		}
		catch (GOTOException e){
		}
	}

protected void assigneeRead5010()
	{
		SmartFileCode.execute(appVars, asgnenqIO);
		if (isNE(asgnenqIO.getStatuz(),varcom.oK)
		&& isNE(asgnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(asgnenqIO.getParams());
			fatalError600();
		}
		/*END-OF-FILE*/
		if (isNE(asgnenqIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(asgnenqIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(asgnenqIO.getStatuz(),varcom.endp)) {
			wsaaNoMoreAssignees.set("Y");
			goTo(GotoLabel.exit5090);
		}
	}

protected void asgnToScreen5030()
	{
		if (isNE(asgnenqIO.getAsgnnum(),SPACES)) {
			cltsIO.setDataArea(SPACES);
			cltsIO.setClntnum(asgnenqIO.getAsgnnum());
			x100AssigneeName();
		}
		sv.asgnnum.set(asgnenqIO.getAsgnnum());
		sv.reasoncd.set(asgnenqIO.getReasoncd());
		sv.commfrom.set(asgnenqIO.getCommfrom());
		sv.commto.set(asgnenqIO.getCommto());
		wsaaNoMoreAssignees.set("N");
		if (isLTE(asgnenqIO.getCommfrom(),wsaaToday)
		&& isGTE(asgnenqIO.getCommto(),wsaaToday)) {
			sv.zasgnflagOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.zasgnflagOut[varcom.nd.toInt()].set(SPACES);
		}
	}

protected void reasonDesc5030()
	{
		if (isEQ(asgnenqIO.getReasoncd(),SPACES)) {
			sv.asgnCodeDesc.fill("?");
			goTo(GotoLabel.exit5090);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.asgnCodeDesc.set(descIO.getLongdesc());
		}
		else {
			sv.asgnCodeDesc.fill("?");
		}
	}

protected void x100AssigneeName()
	{
		try {
			x110LoadScreen();
		}
		catch (GOTOException e){
		}
	}

protected void x110LoadScreen()
	{
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.asgnnumErr.set(e040);
			goTo(GotoLabel.x190Exit);
		}
		if (isLTE(cltsIO.getCltdod(),chdrpf.getOccdate())) {
			sv.asgnnumErr.set(f782);
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.name.fill("?");
			goTo(GotoLabel.x190Exit);
		}
		else {
			plainname();
			sv.name.set(wsspcomn.longconfname);
		}
		sv.cltaddr01.set(cltsIO.getCltaddr01());
		sv.cltaddr02.set(cltsIO.getCltaddr02());
		sv.cltaddr03.set(cltsIO.getCltaddr03());
		sv.cltaddr04.set(cltsIO.getCltaddr04());
		sv.cltaddr05.set(cltsIO.getCltaddr05());
		sv.cltpcode.set(cltsIO.getCltpcode());
	}
}
