package com.csc.life.enquiries.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:33
 * Description:
 * Copybook name: CRTUNDWREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Crtundwrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRec = new FixedLengthStringData(81);
  	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(parmRec, 0);
  	public FixedLengthStringData coy = new FixedLengthStringData(2).isAPartOf(parmRec, 8);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(parmRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(parmRec, 18);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(parmRec, 20);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(parmRec, 24);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(parmRec, 28);
  	public FixedLengthStringData extra = new FixedLengthStringData(30).isAPartOf(parmRec, 37);
  	public FixedLengthStringData cnttyp = new FixedLengthStringData(3).isAPartOf(parmRec, 67);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(parmRec, 70);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(parmRec, 73);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(parmRec, 77);


	public void initialize() {
		COBOLFunctions.initialize(parmRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}