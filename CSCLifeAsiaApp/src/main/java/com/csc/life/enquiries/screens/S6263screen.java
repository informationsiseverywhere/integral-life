package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6263screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {17, 4, 22, 18, 5, 23, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 22, 10, 72}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6263ScreenVars sv = (S6263ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6263screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6263ScreenVars screenVars = (S6263ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.rapayfrq.setClassString("");
		screenVars.payfrqd.setClassString("");
		screenVars.ratype.setClassString("");
		screenVars.ratypeDesc.setClassString("");
		screenVars.totsuma.setClassString("");
		screenVars.raAmount.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.currds.setClassString("");
		screenVars.btdateDisp.setClassString("");
	}

/**
 * Clear all the variables in S6263screen
 */
	public static void clear(VarModel pv) {
		S6263ScreenVars screenVars = (S6263ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.rasnum.clear();
		screenVars.cltname.clear();
		screenVars.rapayfrq.clear();
		screenVars.payfrqd.clear();
		screenVars.ratype.clear();
		screenVars.ratypeDesc.clear();
		screenVars.totsuma.clear();
		screenVars.raAmount.clear();
		screenVars.instPrem.clear();
		screenVars.currcode.clear();
		screenVars.currds.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
	}
}
