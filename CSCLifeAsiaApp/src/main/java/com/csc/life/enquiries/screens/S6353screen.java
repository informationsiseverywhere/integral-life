package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6353screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	//public static final int[] pfInds = new int[] {9, 17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6353ScreenVars sv = (S6353ScreenVars) pv;
		//clearInds(av, pfInds);
		clearInds(av,sv.getScreenSflPfInds());
		write(lrec, sv.S6353screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6353ScreenVars screenVars = (S6353ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.jowner.setClassString("");
		screenVars.jownername.setClassString("");
		screenVars.payer.setClassString("");
		screenVars.payername.setClassString("");
		screenVars.asgnnum.setClassString("");
		screenVars.asgnname.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.servagnt.setClassString("");
		screenVars.servagnam.setClassString("");
		screenVars.servbr.setClassString("");
		screenVars.brchname.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.payfreq.setClassString("");
		screenVars.lastinsdteDisp.setClassString("");
		screenVars.instpramt.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.nextinsdteDisp.setClassString("");
		screenVars.nextinsamt.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.hpropdteDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.hprrcvdtDisp.setClassString("");
		screenVars.hoissdteDisp.setClassString("");
		screenVars.huwdcdteDisp.setClassString("");
		screenVars.znfopt.setClassString("");
		screenVars.hissdteDisp.setClassString("");
		screenVars.claimsind.setClassString("");
		screenVars.plancomp.setClassString("");
		screenVars.polcomp.setClassString("");
		screenVars.cltrole.setClassString("");
		screenVars.subacbal.setClassString("");
		screenVars.transhist.setClassString("");
		screenVars.agntsdets.setClassString("");
		screenVars.extradets.setClassString("");
		screenVars.aiind.setClassString("");
		screenVars.fupflg.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.zsredtrm.setClassString("");
		screenVars.outind.setClassString("");
		screenVars.comphist.setClassString("");
		screenVars.reqntype.setClassString(""); //ILIFE-2552
		screenVars.instPrem.setClassString("");
		screenVars.zstpduty01.setClassString("");
		screenVars.fatcastatusdesc.setClassString("");
		screenVars.fatcastatus.setClassString("");
		screenVars.zctaxind.setClassString("");//ALS-73
		screenVars.schmno.setClassString("");//ALS-73
		screenVars.schmnme.setClassString("");//ALS-73
		screenVars.billday.setClassString("");//ILIFE-3735
		screenVars.zroloverind.setClassString("");//ALS-700
		screenVars.nlgflg.setClassString("");//ILIFE-3997
		screenVars.znlghist.setClassString("");
		screenVars.indxflg.setClassString("");//ILIFE-4108
		screenVars.cbillamt.setClassString("");
		screenVars.cntfee.setClassString("");//ILIFE-7062*/
		ScreenRecord.setClassStringFormatting(pv);
	}

/**
 * Clear all the variables in S6353screen
 */
	public static void clear(VarModel pv) {
		S6353ScreenVars screenVars = (S6353ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.jowner.clear();
		screenVars.jownername.clear();
		screenVars.payer.clear();
		screenVars.payername.clear();
		screenVars.asgnnum.clear();
		screenVars.asgnname.clear();
		screenVars.indic.clear();
		screenVars.servagnt.clear();
		screenVars.servagnam.clear();
		screenVars.servbr.clear();
		screenVars.brchname.clear();
		screenVars.numpols.clear();
		screenVars.billcurr.clear();
		screenVars.mop.clear();
		screenVars.payfreq.clear();
		screenVars.lastinsdteDisp.clear();
		screenVars.lastinsdte.clear();
		screenVars.instpramt.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.nextinsdteDisp.clear();
		screenVars.nextinsdte.clear();
		screenVars.nextinsamt.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.hpropdteDisp.clear();
		screenVars.hpropdte.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.hprrcvdtDisp.clear();
		screenVars.hprrcvdt.clear();
		screenVars.hoissdteDisp.clear();
		screenVars.hoissdte.clear();
		screenVars.huwdcdteDisp.clear();
		screenVars.huwdcdte.clear();
		screenVars.znfopt.clear();
		screenVars.hissdteDisp.clear();
		screenVars.hissdte.clear();
		screenVars.claimsind.clear();
		screenVars.plancomp.clear();
		screenVars.polcomp.clear();
		screenVars.cltrole.clear();
		screenVars.subacbal.clear();
		screenVars.transhist.clear();
		screenVars.agntsdets.clear();
		screenVars.extradets.clear();
		screenVars.aiind.clear();
		screenVars.fupflg.clear();
		screenVars.ind.clear();
		screenVars.zsredtrm.clear();
		screenVars.outind.clear();
		screenVars.comphist.clear();
		screenVars.reqntype.clear(); //ILIFE-2552
		screenVars.instPrem.clear();
		screenVars.zstpduty01.clear();
		screenVars.fatcastatusdesc.clear();
		screenVars.fatcastatus.clear();
		screenVars.zctaxind.clear();//ALS-73
		screenVars.schmno.clear();//ALS-73
		screenVars.schmnme.clear();//ALS-73
		screenVars.billday.clear();//ILIFE-3735
		screenVars.zroloverind.clear();//ALS-700
		screenVars.nlgflg.clear();//ILIFE-3997
		screenVars.znlghist.clear();
		screenVars.indxflg.clear();//ILIFE-4108
		screenVars.cbillamt.clear();
		screenVars.cntfee.clear();//ILIFE-7062*/
		ScreenRecord.clear(pv);
	}
}