package com.csc.life.enquiries.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UfnspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:41
 * Class transformed from UFNSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UfnspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 66;
	public FixedLengthStringData ufnsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ufnspfRecord = ufnsrec;
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(ufnsrec);
	public FixedLengthStringData virtualFund = DD.vrtfund.copy().isAPartOf(ufnsrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(ufnsrec);
	public PackedDecimalData nofunts = DD.nofunts.copy().isAPartOf(ufnsrec);
	public PackedDecimalData jobno = DD.jobno.copy().isAPartOf(ufnsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ufnsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ufnsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ufnsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UfnspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UfnspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UfnspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UfnspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UfnspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UfnspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UfnspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UFNSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"COMPANY, " +
							"VRTFUND, " +
							"UNITYP, " +
							"NOFUNTS, " +
							"JOBNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     company,
                                     virtualFund,
                                     unitType,
                                     nofunts,
                                     jobno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		company.clear();
  		virtualFund.clear();
  		unitType.clear();
  		nofunts.clear();
  		jobno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUfnsrec() {
  		return ufnsrec;
	}

	public FixedLengthStringData getUfnspfRecord() {
  		return ufnspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUfnsrec(what);
	}

	public void setUfnsrec(Object what) {
  		this.ufnsrec.set(what);
	}

	public void setUfnspfRecord(Object what) {
  		this.ufnspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ufnsrec.getLength());
		result.set(ufnsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}