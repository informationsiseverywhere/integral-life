
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifematTableDAM;
import com.csc.life.terminationclaims.dataaccess.MatdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;

import com.csc.life.enquiries.screens.Sr57mScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr57m extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5019");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");


	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	/* WSAA-TRANSACTION-REC */
	/* ERRORS */
	private static final String e304 = "E304";
	/* TABLES */


	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();

	private DescTableDAM descIO = new DescTableDAM();
	private LifematTableDAM lifematIO = new LifematTableDAM();
	private MatdclmTableDAM matdclmIO = new MatdclmTableDAM();
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();

	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr57mScreenVars sv = ScreenProgram.getScreenVars( Sr57mScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private Wssplife wssplife = new Wssplife();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	


	public Pr57m() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57m", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}



	/**
	 * <pre>
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */


	protected void initialise1000()
	{
		
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("Sr57m", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.clamant.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.tdbtamt.set(ZERO);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.planSuffix.set(ZERO);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmatIO);
		if(isNE(chdrmatIO.getStatuz(),varcom.oK)){
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}

		sv.chdrnum.set(chdrmatIO.getChdrnum());
		sv.cnttype.set(chdrmatIO.getCnttype());
		descIO.setDescitem(chdrmatIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.ctypedes.set(descIO.getLongdesc());
		}else{
			sv.ctypedes.set(SPACE);
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmatIO.getStatcode());
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.rstate.set(descIO.getShortdesc());
		}else{
			sv.rstate.set(SPACE);
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmatIO.getPstatcode());
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.pstate.set(descIO.getShortdesc());
		}else{
			sv.pstate.set(SPACE);
		}

		sv.occdate.set(chdrmatIO.getOccdate());
		sv.cownnum.set(chdrmatIO.getCownnum());
		cltsIO.setClntnum(chdrmatIO.getCownnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else{
			plainname();
			sv.ownername.set(wsspcomn.longconfname); 
		}
		sv.btdate.set(chdrmatIO.getBtdate());
		sv.ptdate.set(chdrmatIO.getPtdate());
		mathclmIO.setChdrcoy(chdrmatIO.getChdrcoy());
		mathclmIO.setChdrnum(chdrmatIO.getChdrnum());
		mathclmIO.setTranno(0);
		mathclmIO.setPlanSuffix(0);
		mathclmIO.setFunction(varcom.begn);
		mathclmIO.setFormat(formatsInner.mathclmrec);
		SmartFileCode.execute(appVars, mathclmIO);
		if(isNE(mathclmIO.getStatuz(),varcom.oK)&&isNE(mathclmIO.getStatuz(),varcom.endp)){
			syserrrec.statuz.set(mathclmIO.getStatuz());
			syserrrec.params.set(mathclmIO.getParams());
			fatalError600();
		}
		if(isEQ(mathclmIO.getStatuz(),varcom.endp)||isNE(mathclmIO.getChdrcoy(),chdrmatIO.getChdrcoy())||isNE(mathclmIO.getChdrnum(),chdrmatIO.getChdrnum())){
			syserrrec.statuz.set(mathclmIO.getStatuz());
			syserrrec.params.set(mathclmIO.getParams());
			fatalError600();
		}

		sv.effdate.set(mathclmIO.getEffdate());
		sv.currcd.set(mathclmIO.getCurrcd());
		sv.policyloan.set(mathclmIO.getPolicyloan());
		sv.tdbtamt.set(mathclmIO.getTdbtamt());
		sv.otheradjst.set(mathclmIO.getOtheradjst());
		sv.reasoncd.set(mathclmIO.getReasoncd());
		sv.resndesc.set(mathclmIO.getResndesc());
		lifematIO.setDataArea(SPACES);
		lifematIO.setChdrcoy(mathclmIO.getChdrcoy());
		lifematIO.setChdrnum(mathclmIO.getChdrnum());
		lifematIO.setLife(mathclmIO.getLife());
		lifematIO.setJlife(SPACES);
		lifematIO.setFunction(varcom.begn);

		SmartFileCode.execute(appVars, lifematIO);
		if(isNE(lifematIO.getStatuz(),varcom.endp)&&isNE(lifematIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(lifematIO.getStatuz());
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}
		if(isNE(mathclmIO.getChdrcoy(),lifematIO.getChdrcoy())||isNE(mathclmIO.getChdrnum(),lifematIO.getChdrnum())||isEQ(lifematIO.getStatuz(),varcom.endp)){
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}
		while(isNE(lifematIO.getStatuz(),varcom.endp)){
			SmartFileCode.execute(appVars, lifematIO);


			if(isNE(lifematIO.getStatuz(),varcom.endp)&&isNE(lifematIO.getStatuz(),varcom.oK)){
				syserrrec.statuz.set(lifematIO.getStatuz());
				syserrrec.params.set(lifematIO.getParams());			
				fatalError600();
			}
			if(isNE(mathclmIO.getChdrcoy(),lifematIO.getChdrcoy())||isNE(mathclmIO.getChdrnum(),lifematIO.getChdrnum())||isEQ(lifematIO.getStatuz(),varcom.endp)){
				lifematIO.setStatuz(varcom.endp);
			}		

			SmartFileCode.execute(appVars, mathclmIO);
			if(isNE(mathclmIO.statuz,varcom.oK)){
				syserrrec.statuz.set(mathclmIO.statuz);
				fatalError600();
			}
			lifematIO.setFunction(varcom.nextr);
		}


		sv.lifcnum.set(lifematIO.getLifcnum());
		cltsIO.setClntnum(lifematIO.getLifcnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else{
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		lifematIO.setJlife("01");
		lifematIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifematIO);
		if(isNE(lifematIO.getStatuz(),varcom.oK)&&isNE(lifematIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}

		if(isNE(lifematIO.getStatuz(),varcom.mrnf)){
			lifematIO.setJlife(SPACES);
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			continue1030();		

		}
		//else{		
			matdclmIO.setChdrcoy(chdrmatIO.getChdrcoy());
			matdclmIO.setChdrnum(chdrmatIO.getChdrnum());
			matdclmIO.setTranno(mathclmIO.getTranno());
			matdclmIO.setPlanSuffix(mathclmIO.getPlanSuffix());
			matdclmIO.setLife(mathclmIO.getLife());
			matdclmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, matdclmIO);
			if (isNE(matdclmIO.getStatuz(), varcom.oK)
					&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(matdclmIO.getParams());
				fatalError600();
			}
			if (isNE(chdrmatIO.getChdrnum(), matdclmIO.getChdrnum())
					|| isNE(chdrmatIO.getChdrcoy(), matdclmIO.getChdrcoy())
					|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
				matdclmIO.setStatuz(varcom.endp);
			}
			while(isNE(matdclmIO.getStatuz(),varcom.endp)){
				processMatdclm1500();
			}
			sv.estimateTotalValue.set(wsaaEstimateTot);
			if(isEQ(sv.estimateTotalValue,0)){
				//	COMPUTE SZ002-CLAMANT = ( WSAA-ACTUAL-TOT   - ( SZ002-POLICYLOAN + SZ002-TDBTAMT) + SZ002-OTHERADJST);
				compute(sv.clamant, 2).set(sub(add(add(sv.policyloan,sv.tdbtamt),sv.otheradjst),wsaaActualTot));
			}
		//}


	}

	protected void continue1030()
	{
		sv.jlifcnum.set(lifematIO.getLifcnum());
		cltsIO.setClntnum(lifematIO.getLifcnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else{
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}

	}

	protected void corpname()
	{

		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);

	}
	private void findDesc1300(){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if(isNE(descIO.getStatuz(),varcom.oK)&&isNE(descIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}


	private void getClientDetails1400(){
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if(isNE(cltsIO.getStatuz(),varcom.oK)&&isNE(cltsIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}



	
	/**
	 * The mainline method is the default entry point to the class
	 */




	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}



	

	private void processMatdclm1500(){	
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
				&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmatIO.getChdrnum(), matdclmIO.getChdrnum())
				|| isNE(chdrmatIO.getChdrcoy(), matdclmIO.getChdrcoy())
				|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
		}

		if (isEQ(chdrmatIO.getChdrnum(), matdclmIO.getChdrnum())){


			sv.estMatValue.set(ZERO);
			sv.hemv.set(ZERO);
			sv.hactval.set(ZERO);
			sv.actvalue.set(ZERO);
	
			sv.lifcnum.set(matdclmIO.getLife());
			sv.coverage.set(matdclmIO.getCoverage());
		
			if(isEQ(matdclmIO.getRider(),00)){
				sv.rider.set(SPACES);
			}else{
				sv.rider.set(matdclmIO.getRider());
			}
			if(isNE(matdclmIO.getRider(),00)){
				sv.coverage.set(SPACES);
			}
			sv.fund.set(matdclmIO.getCrtable());
			sv.fieldType.set(matdclmIO.getFieldType());
			sv.shortds.set(matdclmIO.getShortds());
			sv.cnstcur.set(matdclmIO.getCurrcd());
			sv.hcnstcur.set(matdclmIO.getCurrcd());
			sv.estMatValue.set(matdclmIO.getEstMatValue());
			sv.hemv.set(matdclmIO.getEstMatValue());
			sv.actvalue.set(matdclmIO.getActvalue());
			
			if(isNE(sv.fieldType,"C")){
				wsaaActualTot.add(matdclmIO.estMatValue);
				wsaaActualTot.add(matdclmIO.actvalue);
			}
			
			//		subtract matdclmEstMatValue  from wsaaEstimateTot subtract matdclmActvalue       from wsaaActualTot)){
			
			if (isEQ(sv.fieldType, "C")) {
				compute(wsaaEstimateTot, 2).set(sub(wsaaEstimateTot,  matdclmIO.estMatValue));
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot,  matdclmIO.actvalue));
				
			}			
			
			scrnparams.function.set(varcom.sadd);
			processScreen("Sr57m", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		matdclmIO.setFunction(varcom.nextr);
	}
	protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6234IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6234-DATA-AREA                         */
		/*                         S6234-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  No updates are required.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		} 
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.set(wsaaProg);;
		scrnparams.function.set("HIDEW"); 
		if(isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.edterror.set("Y");
		}

		/*EXIT*/
	}
	
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 

		/* FORMATS */
		private FixedLengthStringData mathclmrec = new FixedLengthStringData(10).init("MATHCLMREC");	

	}

}
