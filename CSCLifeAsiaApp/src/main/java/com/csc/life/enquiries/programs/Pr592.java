/*
 * File: Pr592.java
 * Date: 30 August 2009 1:48:43
 * Author: Quipoz Limited
 * 
 * Class transformed from PR592.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.enquiries.screens.Sr592ScreenVars;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
*             Authority Limit
*
* This program is input
*     The Underwriter Level indicator is only to specify the
*     underwriter group. Validation with TR594 and update UWLV
*     file.
***********************************************************************
* </pre>
*/
public class Pr592 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR592");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String uwlvrec = "UWLVREC";
	private String usrdrec = "USRDREC";

	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private FixedLengthStringData wsspUserco = new FixedLengthStringData(1).isAPartOf(wsspUserArea, 0);
		/*User Details Logical - Validflag '1's on*/
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
		/*USER UNDERWRITING LEVEL*/
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private Sr592ScreenVars sv = ScreenProgram.getScreenVars( Sr592ScreenVars.class);
	boolean susur002Permission = false;
	boolean compSurrPosPermission = false;
	boolean automaticUWdecision = false;
	private static final String pos041Confg = "SUSUR008";
	private static final String NBPRP123 = "NBPRP123";
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Pr592() {
		super();
		screenVars = sv;
		new ScreenModel("Sr592", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		retrvUserid1020();
		retrvUwlvInfo1030();
		if (isEQ(wsspcomn.flag,"I")){
			sv.uwlevelOut[varcom.pr.toInt()].set("Y");
		}
		if (susur002Permission || compSurrPosPermission) {
		if (isEQ(wsspcomn.flag,"I")){
			sv.poslevelOut[varcom.pr.toInt()].set("Y");
		}
		}
	}

protected void initialise1010()
	{
	    susur002Permission = FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT");        //ICIL-279 
	    compSurrPosPermission = FeaConfg.isFeatureExist("2", pos041Confg, appVars, "IT");
	    automaticUWdecision = FeaConfg.isFeatureExist("2", NBPRP123, appVars, "IT");
	    usrdIO.setDataArea(SPACES); 
		uwlvIO.setDataArea(SPACES);
		sv.dataArea.set(SPACES);
	
		if (!susur002Permission) {
			sv.susur002flag.set("N");
		}
		else {
			sv.susur002flag.set("Y");
		}
		if(!automaticUWdecision){
			sv.automatciUWFlag.set("N");
		}else{
			sv.automatciUWFlag.set("Y");
		}
	}

protected void retrvUserid1020()
	{
		usrdIO.setDataKey(SPACES);
		usrdIO.setFunction(varcom.retrv);
		usrdIO.setFormat(usrdrec);
		SmartFileCode.execute(appVars, usrdIO);
		if (isNE(usrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(usrdIO.getParams());
			fatalError600();
		}
		sv.userid.set(usrdIO.getUserid());
		sv.company.set(wsspUserco);
	}

protected void retrvUwlvInfo1030()
	{
		uwlvIO.setDataKey(SPACES);
		uwlvIO.setUserid(usrdIO.getUserid());
		uwlvIO.setCompany(wsspUserco);
		uwlvIO.setFunction(varcom.readr);
		uwlvIO.setFormat(uwlvrec);
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(),varcom.oK)
		&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(uwlvIO.getParams());
			fatalError600();
		}
		if (isEQ(uwlvIO.getStatuz(),varcom.oK)) {
			sv.uwlevel.set(uwlvIO.getUwlevel());
			if (automaticUWdecision) {
				sv.uwdecision.set(uwlvIO.getUwdecision());
				sv.skipautouw.set(uwlvIO.getSkipautouw());
			}
			if (susur002Permission || compSurrPosPermission) {
				sv.poslevel.set(uwlvIO.getPoslevel());
			}
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		uwlvIO.setParams(SPACES);
		uwlvIO.setCompany(sv.company);
		uwlvIO.setUserid(sv.userid);
		uwlvIO.setFormat(uwlvrec);
		uwlvIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(),varcom.oK)
		&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(uwlvIO.getStatuz());
			syserrrec.params.set(uwlvIO.getParams());
			fatalError600();
		}
		uwlvIO.setUwlevel(sv.uwlevel);
		if (susur002Permission || compSurrPosPermission) {
		uwlvIO.setPoslevel(sv.poslevel);
		}
		if (automaticUWdecision) {
			uwlvIO.setUwdecision(sv.uwdecision);
			uwlvIO.setSkipautouw(sv.skipautouw);
		}
		if (isEQ(uwlvIO.getStatuz(),varcom.oK)) {
			uwlvIO.setFunction(varcom.rewrt);
			if (isEQ(sv.uwlevel,SPACES)) {
				uwlvIO.setFunction(varcom.delet);
			}
		}
		else {
			uwlvIO.setFunction(varcom.writr);
			if (isEQ(sv.uwlevel,SPACES)) {
				goTo(GotoLabel.exit3090);
			}
		}
		if (susur002Permission || compSurrPosPermission ) {
		if (isEQ(uwlvIO.getStatuz(),varcom.oK)) {
			uwlvIO.setFunction(varcom.rewrt);
			if (isEQ(sv.poslevel,SPACES)) {
				uwlvIO.setFunction(varcom.delet);
			}
		}
		else {
			uwlvIO.setFunction(varcom.writr);
			if (isEQ(sv.poslevel,SPACES)) {
				goTo(GotoLabel.exit3090);
			}
		}
		}
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(uwlvIO.getStatuz());
			syserrrec.params.set(uwlvIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}
}
