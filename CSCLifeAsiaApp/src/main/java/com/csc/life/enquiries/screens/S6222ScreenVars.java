package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6222
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6222ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData names = new FixedLengthStringData(60).isAPartOf(dataFields, 8);
	public FixedLengthStringData[] name = FLSArrayPartOfStructure(2, 30, names, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(names, 0, FILLER_REDEFINE);
	public FixedLengthStringData name1 = new FixedLengthStringData(30).isAPartOf(filler, 0);
	public FixedLengthStringData name2 = new FixedLengthStringData(30).isAPartOf(filler, 30);
	public ZonedDecimalData tsumins = DD.tsumins.copyToZonedDecimal().isAPartOf(dataFields,68);
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 86);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData namesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] nameErr = FLSArrayPartOfStructure(2, 4, namesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(namesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData name1Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData name2Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData tsuminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 102);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData namesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] nameOut = FLSArrayPartOfStructure(2, 12, namesOut, 0);
	public FixedLengthStringData[][] nameO = FLSDArrayPartOfArrayStructure(12, 1, nameOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(namesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] name1Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] name2Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] tsuminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);

	/*public FixedLengthStringData subfileArea = new FixedLengthStringData(132);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(34).isAPartOf(subfileArea, 0);*/
	public FixedLengthStringData subfileArea = new FixedLengthStringData(getSubfileAreaSize());
	public FixedLengthStringData subfileFields = new FixedLengthStringData(getSubfileFieldsSize()).isAPartOf(subfileArea, 0);
	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData sicurr = DD.sicurr.copy().isAPartOf(subfileFields,16);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(subfileFields,19);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(getErrorSubfileSize()).isAPartOf(subfileArea, getSubfileFieldsSize());
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData sicurrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	//public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 58);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(getOutputSubfileSize()).isAPartOf(subfileArea,getErrorSubfileSize()+getSubfileFieldsSize());
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] sicurrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
//	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 130);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,getSubfileFieldsSize()+getErrorSubfileSize()+getOutputSubfileSize());	
	//public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	
	public LongData S6222screensflWritten = new LongData(0);
	public LongData S6222screenctlWritten = new LongData(0);
	public LongData S6222screenWritten = new LongData(0);
	public LongData S6222protectWritten = new LongData(0);
	public GeneralTable s6222screensfl = new GeneralTable(AppVars.getInstance());

	
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21};
	public static int[] affectedInds = new int[] {};
	
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6222screensfl;
	}

	public S6222ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	{
		screenIndicArea = DD.indicarea.copy();
	}

	protected void initialiseScreenVars() {
	/*	screenSflFields = new BaseData[] {company, chdrnum, cnttype, crtable, sicurr, sumin};
		screenSflOutFields = new BaseData[][] {companyOut, chdrnumOut, cnttypeOut, crtableOut, sicurrOut, suminOut};
		screenSflErrFields = new BaseData[] {companyErr, chdrnumErr, cnttypeErr, crtableErr, sicurrErr, suminErr};*/
		
		screenSflFields = getscreenLSflFields();
		screenSflOutFields = getscreenSflOutFields();
		screenSflErrFields = getscreenSflErrFields();
		
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		/*screenFields = new BaseData[] {clntnum, tsumins};
		screenOutFields = new BaseData[][] {clntnumOut, tsuminsOut};
		screenErrFields = new BaseData[] {clntnumErr, tsuminsErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};*/

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6222screen.class;
		screenSflRecord = S6222screensfl.class;
		screenCtlRecord = S6222screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6222protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6222screenctl.lrec.pageSubfile);
	}

	public int getDataAreaSize()
	{
		return 150;
	}
	
	public int getDataFieldsSize()
	{
		return 86;
	}
	
	public int getErrorIndicatorSize()
	{
		return 16;
	}

	public int getOutputFieldSize()
	{
		return 48;
	}

	public int getSubfileAreaSize()
	{
		return 132;
	}
	
	public int getSubfileFieldsSize()
	{
		return 34;
	}

	public int getErrorSubfileSize()
	{
		return 24;
	}

	public int getOutputSubfileSize()
	{
		return 72;
	}
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {clntnum, tsumins};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {clntnumOut, tsuminsOut};
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {clntnumErr, tsuminsErr};
	}
	
	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {company, chdrnum, cnttype, crtable, sicurr, sumin};
	}
	
	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {companyOut, chdrnumOut, cnttypeOut, crtableOut, sicurrOut, suminOut};
	}
	
	
	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {companyErr, chdrnumErr, cnttypeErr, crtableErr, sicurrErr, suminErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};
	}
	
	
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {};
	}
	

	public static int[] getScreenSflPfInds()
	{
		return pfInds;
	}
	
	
	public static int[] getScreenSflAffectedInds()
	{
		return affectedInds;
	}
	
	
	
}
