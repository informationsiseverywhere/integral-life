/*
 * File: Pd5eg.java
 * Date: 28 February 2018 0:37:27
 * Author: Quipoz Limited
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.fsuframework.cls.FeaConfg;
import com.csc.life.enquiries.dataaccess.AcmvtrnTableDAM;
import com.csc.life.enquiries.dataaccess.RtrntrnTableDAM;
import com.csc.life.enquiries.dataaccess.dao.LbonpfDAO;
import com.csc.life.enquiries.dataaccess.model.Lbonpf;
import com.csc.life.enquiries.screens.Sd5egScreenVars;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Pd5eg extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5EG");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private BinaryData wsaaSubfchg = new BinaryData(3, 0);

	private FixedLengthStringData wsaaRecordFound = new FixedLengthStringData(1);
	private Validator recordFound = new Validator(wsaaRecordFound, "Y");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaDatime = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaYear = new FixedLengthStringData(4).isAPartOf(wsaaDatime, 0);
	private FixedLengthStringData wsaaMonth = new FixedLengthStringData(2).isAPartOf(wsaaDatime, 5);
	private FixedLengthStringData wsaaDay = new FixedLengthStringData(2).isAPartOf(wsaaDatime, 8);

	private FixedLengthStringData wsaaDateFormat = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDfYy = new FixedLengthStringData(4).isAPartOf(wsaaDateFormat, 0);
	private FixedLengthStringData wsaaDfMm = new FixedLengthStringData(2).isAPartOf(wsaaDateFormat, 4);
	private FixedLengthStringData wsaaDfDd = new FixedLengthStringData(2).isAPartOf(wsaaDateFormat, 6);
	private ZonedDecimalData wsaaTxDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaDateFormat, 0, REDEFINE).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String e494 = "E494";
	private static final String h093 = "H093";
	private static final String bclm02 = "BCLM02";
	/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private AcmvtrnTableDAM acmvtrnIO = new AcmvtrnTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private ResnenqTableDAM resnenqIO = new ResnenqTableDAM();
	private RtrntrnTableDAM rtrntrnIO = new RtrntrnTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
//	ILIFE-471
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sd5egScreenVars sv = ScreenProgram.getScreenVars( Sd5egScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();

	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private FixedLengthStringData wsaaT502 = new FixedLengthStringData(4).init("T502");
	private FixedLengthStringData wsaaT510 = new FixedLengthStringData(4).init("T510");
	private FixedLengthStringData wsaaT512 = new FixedLengthStringData(4).init("T512");
	private FixedLengthStringData wsaaT539 = new FixedLengthStringData(4).init("T539");
	private FixedLengthStringData wsaaT542 = new FixedLengthStringData(4).init("T542");
	private FixedLengthStringData wsaaT668 = new FixedLengthStringData(4).init("T668");
	private FixedLengthStringData wsaaT669 = new FixedLengthStringData(4).init("T669");
	private FixedLengthStringData wsaaT671 = new FixedLengthStringData(4).init("T671");
	private static final String g889 = "G889";
	private static final String covrsurrec = "COVRSURREC";
	private boolean ispermission = false ;
	private FeaConfg feaConfg = new FeaConfg();
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private Lbonpf lbonqpf = new Lbonpf(); 
	private LbonpfDAO lbonpfDAO = getApplicationContext().getBean("lbonpfDAO", LbonpfDAO.class);
	private List<Lbonpf> lbonpfList = null;

	private FixedLengthStringData wsaaPrevtrannosearch =  new FixedLengthStringData(5);
	private FixedLengthStringData wsaaPrevdatesubsearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPreveffdatesearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPrevtrcodesearch =  new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevtrandescsearch =  new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPrevcrtusersearch =  new FixedLengthStringData(10);
	private DescDAO descdao =new DescDAOImpl();

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		updateErrorIndicators2670, 
		checkHits4850
	}

	public Pd5eg() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5eg", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
	
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		// ILB-326:Starts
		wsaaPrevtrannosearch.set("00000");
		wsaaPrevdatesubsearch.set("99999999");
		wsaaPreveffdatesearch.set("99999999");
		wsaaPrevtrcodesearch.set(SPACES);
		wsaaPrevtrandescsearch.set(SPACES);
		wsaaPrevcrtusersearch.set(SPACES);
		sv.datesubsearchDisp.clear();
		sv.effdatesearchDisp.clear();
		// ILB-326:Ends
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SD5EG", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);

					if(null==chdrpf){
						chdrenqIO.setFunction(Varcom.retrv);
						chdrenqIO.setFormat(formatsInner.chdrenqrec);
						SmartFileCode.execute(appVars, chdrenqIO);
						if (isNE(chdrenqIO.getStatuz(), Varcom.oK)) {
							syserrrec.params.set(chdrenqIO.getParams());
							fatalError600();
						}
						else {
							chdrpf = chdrDao.getChdrpf(chdrenqIO.getChdrcoy().toString(), chdrenqIO.getChdrnum().toString());
							if(null==chdrpf) {
								fatalError600();
							}
							else {
								chdrDao.setCacheObject(chdrpf);
							}
						}
					} 
	
		
		
		ispermission = feaConfg.isFeatureExist(wsspcomn.company.toString(),bclm02,appVars, "IT");		
		if (!ispermission)
			sv.indicOut[varcom.pr.toInt()].set("Y");		
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		/*    MOVE CHDRENQ-CHDRCOY        TO LIFEENQ-CHDRCOY.              */
		/*    MOVE CHDRENQ-CHDRNUM        TO LIFEENQ-CHDRNUM.              */
		/*    MOVE '01'                   TO LIFEENQ-LIFE.                 */
		/*    MOVE '00'                   TO LIFEENQ-JLIFE.                */
		/*    MOVE BEGN                   TO LIFEENQ-FUNCTION.             */
		/*    CALL 'LIFEENQIO'            USING LIFEENQ-PARAMS.            */
		/*    IF LIFEENQ-STATUZ           NOT = O-K                        */
		/*         MOVE LIFEENQ-PARAMS    TO SYSR-PARAMS                   */
		/*         PERFORM 600-FATAL-ERROR.                                */
		/*    MOVE LIFEENQ-LIFCNUM        TO SD5EG-LIFENUM                 */
		lifelnbIO.setChdrcoy(chdrpf.getChdrcoy());
		lifelnbIO.setChdrnum(chdrpf.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		/*    MOVE '01'                   TO LIFEENQ-JLIFE.                */
		/*    MOVE READR                  TO LIFEENQ-FUNCTION.             */
		/*    CALL 'LIFEENQIO'            USING LIFEENQ-PARAMS.            */
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO SD5EG-JLIFE                   */
		/*                                 SD5EG-JLIFENAME               */
		/*  ELSE                                                         */
		/*    IF   LIFEENQ-STATUZ             = O-K                        */
		/*         MOVE LIFEENQ-LIFCNUM   TO SD5EG-JLIFE                   */
		/*                                   CLTS-CLNTNUM                  */
		/*         MOVE WSSP-FSUCO        TO CLTS-CLNTCOY                  */
		/*         MOVE 'CN'              TO CLTS-CLNTPFX                  */
		/*         MOVE READR             TO CLTS-FUNCTION                 */
		/*         CALL 'CLTSIO'          USING CLTS-PARAMS                */
		/*         IF CLTS-STATUZ         NOT = O-K                        */
		/*              MOVE CLTS-PARAMS  TO SYSR-PARAMS                   */
		/*              PERFORM 600-FATAL-ERROR                            */
		/*         ELSE                                                    */
		/*              PERFORM PLAINNAME                                  */
		/*              MOVE WSSP-LONGCONFNAME                             */
		/*                                TO SD5EG-JLIFENAME.              */
		if (isEQ(lifelnbIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifelnbIO.getLifcnum());
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		
		lbonpfList = lbonpfDAO.getLbonpfData(chdrpf.getChdrnum(), wsspcomn.company.toString());
		for(Lbonpf lbon:lbonpfList) {
			lbonqpf=lbon;
			processPtrn1100();
		}
		
		/* Do not move 1 to the sunfile RRN if this is a call from a       */
		/* remote device so that the RRN will contain the total number of  */
		/* transaction records.                                            */
		/* MOVE 1                      TO SCRN-SUBFILE-RRN.             */
		if (isNE(scrnparams.deviceInd, "*RMT")) {
			scrnparams.subfileRrn.set(1);
		}

	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*      (including subfile load section)
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processPtrn1100()
	{
		para1100();
		writeSubfile1110();
	}

protected void para1100()
	{
		
		sv.subfileFields.set(SPACES);
		sv.effdate.set(lbonqpf.getEffdate());
		sv.tranno.set(lbonqpf.getTranno());
		sv.tranamt.set(lbonqpf.getTrnamt());
		if (lbonqpf.getUsrprf()!=null && isNE(lbonqpf.getUsrprf(), SPACES)) {
			sv.crtuser.set(lbonqpf.getUsrprf());
		}
		else {
			sv.crtuser.set(lbonqpf.getUsrprf());
		}
		sv.fillh.set(SPACES);
		sv.filll.set(SPACES);
		sv.allocamt.set(lbonqpf.getUalprc());
		sv.lbalperc.set(lbonqpf.getUextpc());
		sv.total.set(lbonqpf.getNetpre());
		sv.revind.set(lbonqpf.getRevflag());
		/*    Obtain the Transaction Code description from T1688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(lbonqpf.getTrcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.trandesc.fill("?");
		}
		else {
			sv.trandesc.set(descIO.getLongdesc());
		}
		/*  Obtain the actual transaction date from the date time stamp    */
		wsaaDatime.set(lbonqpf.getDatime());
		wsaaDfYy.set(wsaaYear);
		wsaaDfMm.set(wsaaMonth);
		wsaaDfDd.set(wsaaDay);
		/*  Check to see if a reason for the transaction exists            */
		reason1300();
	}

protected void writeSubfile1110()
	{
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SD5EG", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
	}
	
protected void reason1300()
	{
		start1300();
	}

protected void start1300()
	{
		
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo12010();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo12010()
	{
		/*    CALL 'SD5EGIO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SD5EG-DATA-AREA                         */
		/*                         SD5EG-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		
		
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		filterSearch();
		/*VALIDATE-SCREEN*/
		scrnparams.subfileRrn.set(1);
	}

private void filterSearch(){
	
	if (isNE(sv.trannosearch,wsaaPrevtrannosearch) 
			|| isNE(sv.datesubsearch,wsaaPrevdatesubsearch) 
			|| isNE(sv.effdatesearch,wsaaPreveffdatesearch)
			|| isNE(sv.trcodesearch,wsaaPrevtrcodesearch)
			|| isNE(sv.trandescsearch,wsaaPrevtrandescsearch)
			|| isNE(sv.crtusersearch,wsaaPrevcrtusersearch)) {
		
		wsaaPrevtrannosearch.set(sv.trannosearch);
		wsaaPrevdatesubsearch.set(sv.datesubsearch);
		wsaaPreveffdatesearch.set(sv.effdatesearch);
		wsaaPrevtrcodesearch.set(sv.trcodesearch);
		wsaaPrevtrandescsearch.set(sv.trandescsearch);
		wsaaPrevcrtusersearch.set(sv.crtusersearch);
		
		scrnparams.function.set(varcom.sclr);
		processScreen("SD5EG", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		ArrayList<Lbonpf> filteredList = new ArrayList<>();
		String tranNoStr = sv.trannosearch.toString().trim();
		String datesubStr = sv.datesubsearch.toString().trim();
		String effdateStr = sv.effdatesearch.toString().trim();
		String trcodeStr = sv.trcodesearch.toString().trim();
		String trandescStr = sv.trandescsearch.toString().trim();
		String crtuserStr = sv.crtusersearch.toString().trim();
		
		boolean isTranNoEmpty = "".equalsIgnoreCase(tranNoStr) ? true : "00000".equalsIgnoreCase(tranNoStr);
		boolean isDateSubEmpty = "".equalsIgnoreCase(datesubStr) ? true : "99999999".equalsIgnoreCase(datesubStr);
		boolean isEffDateEmpty = "".equalsIgnoreCase(effdateStr) ? true : "99999999".equalsIgnoreCase(effdateStr);
		boolean isTrCodeEmpty = "".equalsIgnoreCase(trcodeStr);
		boolean isTranDescEmpty = "".equalsIgnoreCase(trandescStr);
		boolean isCrtUserEmpty = "".equalsIgnoreCase(crtuserStr);
		if(isTranNoEmpty && isDateSubEmpty && isEffDateEmpty && isTrCodeEmpty && isTranDescEmpty && isCrtUserEmpty){
			for(Lbonpf lbon:lbonpfList) {
				lbonqpf=lbon;
				processPtrn1100();
			}
		}else{
			Descpf descpf = null;
			if(!"".equalsIgnoreCase(trandescStr)){
				descpf= descdao.getitemByDesc("IT", t1688, wsspcomn.company.toString(), trandescStr);
			}
			if("".equalsIgnoreCase(trandescStr) 
					|| ((descpf != null) &&(isTrCodeEmpty || (descpf.getDescitem() != null && trcodeStr.equalsIgnoreCase(descpf.getDescitem().trim()))))){
				for(Lbonpf lbon:lbonpfList){
					if(isTranNoEmpty || lbon.getTranno().equals(Integer.valueOf(tranNoStr))){
						if(isEffDateEmpty || (lbon.getEffdate().equals(Integer.valueOf(effdateStr)))){
							if(isTranDescEmpty || (lbon.getTrcde() != null && descpf.getDescitem()!= null && lbon.getTrcde().trim().
									equalsIgnoreCase(descpf.getDescitem().trim()))){
								if(isCrtUserEmpty || (lbon.getUsrprf() != null 
										&& ((lbon.getUsrprf().trim().equalsIgnoreCase("")) ? (lbon.getUsrprf().trim().equalsIgnoreCase(crtuserStr)) : (lbon.getUsrprf().trim().equalsIgnoreCase(crtuserStr))))){
									filteredList.add(lbon);
								}
							}
						}
					}
				}
				for(Lbonpf lbon:filteredList) {
					lbonqpf=lbon;
					processPtrn1100();
				}
				if(filteredList.isEmpty()){
					scrnparams.errorCode.set("E040");
				}
			}else{
				scrnparams.errorCode.set("E040");
			}
		}
		wsspcomn.edterror.set("Y");
	}
}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SD5EG", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{}
protected void readUtrn(){
	utrnIO.setDataArea(SPACES);
	utrnIO.setFunction(varcom.begn);
	//Start Life Performance atiwari23
	utrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	utrnIO.setFitKeysSearch("CHDRNUM","CHDRCOY","LIFE","COVERAGE","RIDER","TRANNO");
	
	utrnIO.setChdrcoy(wsspcomn.company);
	utrnIO.setChdrnum(chdrpf.getChdrnum());
	utrnIO.setLife("01");
	utrnIO.setCoverage("01");
	utrnIO.setRider("00");
	utrnIO.setPlanSuffix(ZERO);
	utrnIO.setTranno(sv.tranno);
	SmartFileCode.execute(appVars, utrnIO);
			
}
protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SD5EG", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SD5EG", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		wsspcomn.programPtr.add(1);
	}
	
	
protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("SD5EG", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void unitEnquiry4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case checkHits4850: 
					checkHits4850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		/*    First perform a BEGN  on the selected UTRS record. This will */
		/*    return the first UTRS record for the contract, life, policy  */
		/*    and component.                                               */
		utrsIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		utrsIO.setDataArea(SPACES);
		utrsIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRNUM","CHDRCOY","LIFE","COVERAGE","RIDER","PLNSFX");
	//end;
	

		utrsIO.setChdrcoy(wsspcomn.company);
		utrsIO.setChdrnum(chdrpf.getChdrnum());
		utrsIO.setLife("01");
		utrsIO.setCoverage("01");
		utrsIO.setRider("00");
		utrsIO.setPlanSuffix(ZERO);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)
		|| isNE(utrsIO.getChdrcoy(), wsspcomn.company)
		|| isNE(utrsIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(utrsIO.getLife(), "01")
		|| isNE(utrsIO.getCoverage(), "01")
		|| isNE(utrsIO.getRider(), "00")
		|| isNE(utrsIO.getPlanSuffix(), ZERO)) {
			wsaaRecordFound.set("N");
			wssplife.unitType.set("D");
			goTo(GotoLabel.checkHits4850);
		}
		/*    If in Policy Level enquiry the Plan Suffix from the selected */
		/*    Policy is placed in the UTRS record before it is saved.      */
		wsaaRecordFound.set("Y");
		/*    IF   POLICY-LEVEL-ENQUIRY                                    */
		/*         MOVE SD5EG-PLAN-SUFFIX TO UTRS-PLAN-SUFFIX.             */
		utrsIO.setFormat(formatsInner.utrsrec);
		utrsIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
	}

protected void checkHits4850()
	{
		hitsIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		hitsIO.setParams(SPACES);
		hitsIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRNUM","CHDRCOY","LIFE","COVERAGE","RIDER");
	//end;
		
		hitsIO.setChdrcoy(wsspcomn.company);
		hitsIO.setChdrnum(chdrpf.getChdrnum());
		hitsIO.setLife("01");
		hitsIO.setCoverage("01");
		hitsIO.setRider("00");
		hitsIO.setPlanSuffix(ZERO);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		|| isNE(hitsIO.getChdrcoy(), wsspcomn.company)
		|| isNE(hitsIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(hitsIO.getLife(), "01")
		|| isNE(hitsIO.getCoverage(), "01")
		|| isNE(hitsIO.getRider(), "00")) {
			/*    OR HITS-PLAN-SUFFIX         NOT = SD5EG-HSUFFIX              */
			if (recordFound.isTrue()) {
				return ;
			}
			else {
				syserrrec.params.set(hitsIO.getParams());
				fatalError600();
			}
		}
		/*    IF POLICY-LEVEL-ENQUIRY                                      */
		/*        MOVE SD5EG-PLAN-SUFFIX  TO HITS-PLAN-SUFFIX              */
		/*    END-IF.                                                      */
		hitsIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
	}
	

	protected void keepsChdrclm5000(){
		chdrclmIO.setChdrcoy(wsspcomn.company);
		chdrclmIO.setChdrnum(sv.chdrnum);
		chdrclmIO.setFunction(varcom.readr);
		chdrclmIO.setFormat(formatsInner.chdrclmrec);
		SmartFileCode.execute(appVars, chdrclmIO);
		chdrclmIO.setFunction("KEEPS");
		chdrclmIO.setFormat(formatsInner.chdrclmrec);
		SmartFileCode.execute(appVars, chdrclmIO);
		if(isNE(chdrclmIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
	}

	protected void keepsChdrsur5100(){
		chdrsurIO.setChdrcoy(wsspcomn.company);
		chdrsurIO.setChdrnum(sv.chdrnum);
		chdrsurIO.setFunction(varcom.readr);
		chdrsurIO.setFormat(formatsInner.chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		chdrsurIO.setFunction("KEEPS");
		chdrsurIO.setFormat(formatsInner.chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);


		if(isNE(chdrsurIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrsurIO.getStatuz());
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
	}

	protected void keepsChdrmat5200(){
		chdrmatIO.setChdrcoy(wsspcomn.company);
		chdrmatIO.setChdrnum(sv.chdrnum);
		chdrmatIO.setFunction(varcom.readr);
		chdrmatIO.setFormat(formatsInner.chdrmatrec);
		SmartFileCode.execute(appVars, chdrmatIO);
		chdrmatIO.setFunction("KEEPS");
		chdrmatIO.setFormat(formatsInner.chdrmatrec);
		SmartFileCode.execute(appVars, chdrmatIO);



		if(isNE(chdrmatIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrmatIO.getStatuz());
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
	}

	protected void keepsChdrpts5300(){
		chdrptsIO.setChdrcoy(wsspcomn.company);
		chdrptsIO.setChdrnum(sv.chdrnum);
		chdrptsIO.setFunction(varcom.readr);
		chdrptsIO.setFormat(formatsInner.chdrptsrec);
		SmartFileCode.execute(appVars, chdrptsIO);
		chdrptsIO.setFunction("KEEPS");
		chdrptsIO.setFormat(formatsInner.chdrptsrec);
		SmartFileCode.execute(appVars, chdrptsIO);
		if(isNE(chdrptsIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrptsIO.getStatuz());
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
	}

	
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 
		private FixedLengthStringData resnenqrec = new FixedLengthStringData(10).init("RESNENQREC");
		private FixedLengthStringData utrsrec = new FixedLengthStringData(10).init("UTRSREC");
		private FixedLengthStringData chdrclmrec = new FixedLengthStringData(10).init("CHDRCLMREC");
		private FixedLengthStringData chdrsurrec = new FixedLengthStringData(10).init("CHDRSURREC");
		private FixedLengthStringData chdrmatrec = new FixedLengthStringData(10).init("CHDRMATREC");
		private FixedLengthStringData chdrptsrec = new FixedLengthStringData(10).init("CHDRPTSREC");
		private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");



	}

}
