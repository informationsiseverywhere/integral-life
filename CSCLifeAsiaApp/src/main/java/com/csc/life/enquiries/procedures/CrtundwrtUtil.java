package com.csc.life.enquiries.procedures;

import com.csc.life.enquiries.recordstructures.Crtundwrec;

public interface CrtundwrtUtil {

	public Crtundwrec process(Crtundwrec crtundwrec);
}
