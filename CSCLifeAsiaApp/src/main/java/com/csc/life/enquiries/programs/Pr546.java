/*
 * File: Pr546.java
 * Date: 30 August 2009 1:39:46
 * Author: Quipoz Limited
 * 
 * Class transformed from PR546.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.Sr546ScreenVars;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Waiver of Premium Component Enquiry
*
*****************************************************************
* </pre>
*/
public class Pr546 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR546");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaMessages = new FixedLengthStringData(100);
	private FixedLengthStringData[] wsaaMessage = FLSArrayPartOfStructure(2, 50, wsaaMessages, 0);

		/*    VALUE 'Loaded Single Prem :'.                        <SPLPRM>
		    VALUE 'Loaded Inst. Prem  :'.                        <SPLPRM>*/
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(31);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(31, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaCovrPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaPremStatus, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private static final String itemrec = "ITEMREC";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
	private Batckey wsaaBatckey = new Batckey();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private T5679rec t5679rec = new T5679rec();
	private T2240rec t2240rec = new T2240rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wssplife wssplife = new Wssplife();
	private Sr546ScreenVars sv = ScreenProgram.getScreenVars( Sr546ScreenVars.class);
	private TablesInner tablesInner = new TablesInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private boolean stampDutyflag = false;
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private boolean loadingFlag = false;//ILIFE-3399
	private boolean dialdownFlag = false;
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrenqIO = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrenqIO = new Covrpf();
	private boolean exclFlag = false;
	//ILJ-45 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-45 End
	// ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	// ILJ-387 end

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endPolicy1020, 
		nextProg10640, 
		exit1090, 
		search1320, 
		exit1790
	}

	public Pr546() {
		super();
		screenVars = sv;
		new ScreenModel("Sr546", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case endPolicy1020: 
					endPolicy1020();
					gotRecord10620();
				case nextProg10640: 
					nextProg10640();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		wsaaMiscellaneousInner.wsaaSingp.set(ZERO);
		wsaaMiscellaneousInner.wsaaTaxamt.set(ZERO);
		sv.singlePremium.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		/*BRD-306 START */
		sv.loadper.set(ZERO);
		sv.rateadj.set(ZERO);
		sv.fltmort.set(ZERO);
		sv.premadj.set(ZERO);
		sv.adjustageamt.set(ZERO);
		/*BRD-306 END */
		sv.zstpduty01.set(ZERO);
		sv.zrsumin.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.numpols.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		//ILJ-45 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{			
			sv.crrcdOut[varcom.nd.toInt()].set("Y");
			//sv.rcessageOut[varcom.nd.toInt()].set("Y");
			sv.contDtCalcScreenflag.set("N");
			sv.rcesdteOut[varcom.nd.toInt()].set("Y");
		}
		//ILJ-45 End
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}
		
		// ILJ-387 Start
		boolean cntEnqFlag = false;
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if (cntEnqFlag) {
			sv.cntEnqScreenflag.set("Y");
		}else {
			sv.cntEnqScreenflag.set("N");
		}
		// ILJ-387 End
		/*    Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrenqIO = chdrDao.getCacheObject(chdrenqIO);
		/* Read T2240 for age definition.                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRENQ-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrenqIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*     MOVE '***'              TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}//MTL002
			}
		}
		sv.zagelitOut[varcom.hi.toInt()].set("N");
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getReg());
		sv.numpols.set(chdrenqIO.getPolinc());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		//ILIFE-3399-STARTS
		loadingFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
			}
		//ILIFE-3399-ENDS
		exclFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.optind04Out[varcom.nd.toInt()].set("Y");
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrenqIO.getPstcde());		//modified for ILIFE-4345
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Retrieve valid status'.*/
		readT5679Table1400();
		/*    Read the first LIFE details on the contract.*/
		covrenqIO = covrDao.getCacheObject(covrenqIO);
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaMiscellaneousInner.wsaaSingp.set(covrenqIO.getSingp());
			wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrenqIO.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrenqIO.getSumins());
			setPrecision(covrenqIO.getPlanSuffix(), 0);
			covrenqIO.setPlanSuffix(chdrenqIO.getPolsum()+1);
			
			List<Covrpf> covrenqList = covrDao.searchValidCovrpfRecord(covrenqIO.getChdrcoy(),
			        covrenqIO.getChdrnum(),covrenqIO.getLife(),covrenqIO.getCoverage(),covrenqIO.getRider());
			while ( !(isGT(covrenqIO.getPlanSuffix(), chdrenqIO.getPolinc()))) {
				planComponent1100(covrenqList);
			}
			goTo(GotoLabel.endPolicy1020);
		}
		if (isGT(covrenqIO.getPlanSuffix(), chdrenqIO.getPolsum())) {
			wsaaMiscellaneousInner.wsaaSingp.set(covrenqIO.getSingp());
			wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrenqIO.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrenqIO.getSumins());
			goTo(GotoLabel.endPolicy1020);
		}
		wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
		if (isEQ(covrenqIO.getPlanSuffix(), 1)) {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(sub(covrenqIO.getSingp(), (div(mult(covrenqIO.getSingp(), (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(sub(wsaaMiscellaneousInner.wsaaPremium, (div(mult(wsaaMiscellaneousInner.wsaaPremium, (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(sub(covrenqIO.getSumins(), (div(mult(covrenqIO.getSumins(), (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(sub(wsaaMiscellaneousInner.wsaaZlinstprem, (div(mult(wsaaMiscellaneousInner.wsaaZlinstprem, (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(div(covrenqIO.getSingp(), chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(div(wsaaMiscellaneousInner.wsaaPremium, chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(div(wsaaMiscellaneousInner.wsaaZlinstprem, chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(div(covrenqIO.getSumins(), chdrenqIO.getPolsum()));
		}
	}
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();	
	//IJTI-1485 Start
	rcvdPFObject.setChdrcoy(covrenqIO.getChdrcoy());
	rcvdPFObject.setChdrnum(covrenqIO.getChdrnum());
	rcvdPFObject.setLife(covrenqIO.getLife());
	rcvdPFObject.setCoverage(covrenqIO.getCoverage());
	rcvdPFObject.setRider(covrenqIO.getRider());
	rcvdPFObject.setCrtable(covrenqIO.getCrtable());
	//IJTI-1485 End
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}

protected void endPolicy1020()
	{
		sv.benBillDate.set(covrenqIO.getBenBillDate());
		if (isEQ(covrenqIO.getBenBillDate(), varcom.vrcmMaxDate)
		|| isEQ(covrenqIO.getBenBillDate(), 0)) {
			sv.bbldatOut[varcom.nd.toInt()].set("Y");
		}
		sv.mortcls.set(covrenqIO.getMortcls());
		sv.numpols.set(chdrenqIO.getPolinc());
		sv.anbAtCcd.set(covrenqIO.getAnbAtCcd());
		sv.crrcd.set(covrenqIO.getCrrcd());
		sv.annivProcDate.set(covrenqIO.getAnnivProcDate());
		sv.premcess.set(covrenqIO.getPremCessDate());
		sv.liencd.set(covrenqIO.getLiencd());
		sv.riskCessDate.set(covrenqIO.getRiskCessDate());
		sv.riskCessAge.set(covrenqIO.getRiskCessAge());
		sv.premCessAge.set(covrenqIO.getPremCessAge());
		sv.riskCessTerm.set(covrenqIO.getRiskCessTerm());
		sv.premCessTerm.set(covrenqIO.getPremCessTerm());
		sv.register.set(chdrenqIO.getReg());
		sv.life.set(covrenqIO.getLife());
		if (isNE(covrenqIO.getLife(), lifeenqIO.getLife())) {
			getLifeName4900();
		}
		sv.rider.set(covrenqIO.getRider());
		sv.coverage.set(covrenqIO.getCoverage());
		sv.rerateDate.set(covrenqIO.getRerateDate());
		sv.rerateFromDate.set(covrenqIO.getRerateFromDate());
		/*BRD-306 START */
		sv.zbinstprem.set(covrenqIO.getZbinstprem());
		sv.loadper.set(covrenqIO.getLoadper());
		sv.rateadj.set(covrenqIO.getRateadj());
		sv.fltmort.set(covrenqIO.getFltmort());
		sv.premadj.set(covrenqIO.getPremadj());
		sv.adjustageamt.set(covrenqIO.getPremadj());
		/*BRD-306 END */
		stampDutyflag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			if(isNE(covrenqIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
			sv.zstpduty01.set(covrenqIO.getZstpduty01());
			}
		}
		dialdownFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag||premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!incomeProtectionflag){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
			sv.poltypOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.waitperiodOut[varcom.nd.toInt()].set("N");
			sv.bentrmOut[varcom.nd.toInt()].set("N");
			sv.poltypOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
					sv.waitperiod.set(rcvdPFObject.getWaitperiod());
				}
				if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
					sv.poltyp.set(rcvdPFObject.getPoltyp());
				}
				if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
					sv.bentrm.set(rcvdPFObject.getBentrm());
				}
			}
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
					}
				}
		}
		//BRD-NBP-011 ends
		sv.singlePremium.set(wsaaMiscellaneousInner.wsaaSingp);
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.zlinstprem.set(wsaaMiscellaneousInner.wsaaZlinstprem);
		/*--- Read TR386 table to get screen literals                      */
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		/* IF WSAA-PREMIUM             = 0                      <FA1226>*/
		/*    MOVE 'S'                 TO WSAA-TR386-ID         <FA1226>*/
		/* ELSE                                                 <FA1226>*/
		/*    MOVE 'R'                 TO WSAA-TR386-ID         <FA1226>*/
		/* END-IF.                                              <FA1226>*/
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setItemseq(SPACES);
		/* MOVE READR                  TO ITEM-FUNCTION.        <FA1226>*/
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setStatuz(varcom.oK);
		iy.set(1);
		initialize(wsaaMessages);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadTr3861600();
		}
		
		if (isEQ(wsaaMiscellaneousInner.wsaaPremium, 0)) {
			sv.zdesc.set(wsaaMessage[2]);
		}
		else {
			sv.zdesc.set(wsaaMessage[1]);
		}
		/* CALL  'ITEMIO'           USING ITEM-PARAMS.          <FA1226>*/
		/* IF  ITEM-STATUZ          NOT = O-K                   <FA1226>*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <FA1226>*/
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <FA1226>*/
		/*     PERFORM 600-FATAL-ERROR.                         <FA1226>*/
		/* MOVE ITEM-GENAREA           TO TR386-TR386-REC.      <FA1226>*/
		/* MOVE TR386-PROGDESC-1       TO SR546-ZDESC.          <FA1226>*/
		/* IF  WSAA-PREMIUM            = 0                      <SPLPRM>*/
		/*     MOVE WSAA-SINGLE-DESC   TO SR546-ZDESC           <SPLPRM>*/
		/* ELSE                                                 <SPLPRM>*/
		/* IF  WSAA-PREMIUM         NOT = 0                     <SPLPRM>*/
		/*     MOVE WSAA-REG-DESC      TO SR546-ZDESC           <SPLPRM>*/
		/* END-IF.                                              <SPLPRM>*/
		sv.zdescOut[varcom.hi.toInt()].set("N");
		sv.statFund.set(covrenqIO.getStatFund());
		sv.statSect.set(covrenqIO.getStatSect());
		sv.statSubsect.set(covrenqIO.getStatSubsect());
		sv.zrsumin.set(wsaaMiscellaneousInner.wsaaSumins);
		if (isNE(covrenqIO.getPlanSuffix(), ZERO)) {
			sv.planSuffix.set(covrenqIO.getPlanSuffix());
		}
		else {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			sv.planSuffix.set(ZERO);
		}
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.planSuffix.set(ZERO);
		setupBonus1500();
		sv.bappmeth.set(covrenqIO.getBappmeth());
		/* GET THE COMPONENT DESCRIPTION*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrenqIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		/* READ T5671*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaMiscellaneousInner.wsaaTrCode.set(wsaaBatckey.batcBatctrcde);
		wsaaMiscellaneousInner.wsaaCrtable.set(covrenqIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5671);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5671Key);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5671)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5671Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itdmIO.getGenarea());
		}
	}

protected void gotRecord10620()
	{
		sub1.set(0);
	}

protected void nextProg10640()
	{
		sub1.add(1);
		if (isNE(t5671rec.pgm[sub1.toInt()], wsaaProg)) {
			goTo(GotoLabel.nextProg10640);
		}
		else {
			wsaaMiscellaneousInner.wsaaValidItem.set(t5671rec.edtitm[sub1.toInt()]);
		}
		/* READ T5606*/
		wsaaMiscellaneousInner.wsaaCrcyCode.set(chdrenqIO.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5606Key);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5606)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5606Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5606rec.specind, "N")) {
			sv.optdsc02Out[varcom.nd.toInt()].set("Y");
			sv.optdsc02Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc02Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc02Out[varcom.pr.toInt()].set(SPACES);
		}
		if(isEQ(t5606rec.waitperiod, SPACES)){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.bentrm, SPACES)){
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.poltyp, SPACES)){
			sv.poltypOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		/*    Read PAYR for billing frequency*/
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		payrIO.setChdrnum(chdrenqIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/*    Obtain the frequency short description from T3590.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3590);
		/* MOVE T5606-BENFREQ          TO DESC-DESCITEM.*/
		descIO.setDescitem(payrIO.getBillfreq());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		else {
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.frqdesc.set(descIO.getShortdesc());
			}
			else {
				sv.frqdesc.fill("?");
			}
		}
		optswchInit1200();
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void planComponent1100(List<Covrpf> covrIt)
	{
        /*    For Plan Level Enquiry the first coverage record read will*/
        /*    have a Plan Suffix of 1. All the components for the given*/
        /*    Life, Coverage and Rider must be read and the amounts added*/
        /*    up to give the total for the component being enquired upon.*/
        if (isEQ(covrenqIO.getPlanSuffix(), 1)
        && isNE(chdrenqIO.getPolinc(), 1)) {
            wsaaMiscellaneousInner.wsaaSumins.set(ZERO);
        }
        int curPln = covrenqIO.getPlanSuffix();
        Covrpf covr = null;
        if(covrIt != null && !covrIt.isEmpty()){
            for(Covrpf c:covrIt){
                if(c.getPlanSuffix() == covrenqIO.getPlanSuffix()){
                    covr = c;
                    break;
                }
            }
            if(covr != null){
                covrenqIO = covr;
                wsaaCovrPstatcode.set(covrenqIO.getPstatcode());
                checkPremStatus1300();
            }
        }

        
        if (validStatus.isTrue()) {
            compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(add(wsaaMiscellaneousInner.wsaaSingp, covrenqIO.getSingp()));
            compute(wsaaMiscellaneousInner.wsaaPremium, 2).set(add(wsaaMiscellaneousInner.wsaaPremium, covrenqIO.getInstprem()));
            compute(wsaaMiscellaneousInner.wsaaZlinstprem, 2).set(add(wsaaMiscellaneousInner.wsaaZlinstprem, covrenqIO.getZlinstprem()));
            compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(add(wsaaMiscellaneousInner.wsaaSumins, covrenqIO.getSumins()));
        }
        setPrecision(covrenqIO.getPlanSuffix(), 0);
        covrenqIO.setPlanSuffix(curPln + 1);

	}

protected void optswchInit1200()
	{
		call1210();
	}

protected void call1210()
	{
		/* Read TR52D for Taxcode                                       */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.optdsc03Out[varcom.nd.toInt()].set("Y");
		sv.optind03Out[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.optdsc03Out[varcom.pr.toInt()].set("Y");
		sv.optind03Out[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrenqIO.getReg());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc03Out[varcom.nd.toInt()].set("N");
			sv.optind03Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc03Out[varcom.pr.toInt()].set("N");
			sv.optind03Out[varcom.pr.toInt()].set("N");
		}
		/* The following lines checks whether a window to Annuity*/
		/* Details is made available. This is done by referring to*/
		/* Table T6625*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(covrenqIO.getCrtable());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		/* Check that the record is either found or at EOF*/
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6625)
		|| isNE(itdmIO.getItemitem(), covrenqIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.optdsc01Out[varcom.nd.toInt()].set("Y");
			sv.optdsc01Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc01Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc01Out[varcom.pr.toInt()].set(SPACES);
		}
		/* Call OPTSWCH to retrieve check box text description*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		if (isEQ(wsaaBatckey.batcBatctrcde, "T908")) {
			optswchrec.optsVrsnPrfx.set("001");
		}
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			optswchrec.optsItemCompany.set("0");
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optdsc01.set(optswchrec.optsDsc[1]);
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optdsc02.set(optswchrec.optsDsc[2]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optdsc03.set(optswchrec.optsDsc[3]);
		sv.optind03.set(optswchrec.optsInd[3]);
		sv.optdsc04.set(optswchrec.optsDsc[4]);
		sv.optind04.set(optswchrec.optsInd[4]);
	}

protected void checkPremStatus1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1310();
				case search1320: 
					search1320();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Check the twelve premium status' retrieved from table T5679
	* to see if any match the coverage premium status. If so, set
	* the VALID-STATUS flag to yes.
	* </pre>
	*/
protected void start1310()
	{
		wsaaPremStatus.set(SPACES);
		wsaaSub.set(ZERO);
	}

protected void search1320()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			return ;
		}
		else {
			if (isNE(wsaaCovrPstatcode, t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search1320);
			}
			else {
				wsaaPremStatus.set("Y");
				return ;
			}
		}
		/*EXIT*/
	}

protected void readT5679Table1400()
	{
		read1410();
	}

protected void read1410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void setupBonus1500()
	{
		/*PARA*/
		/* Check if Coverage/Rider is a SUM product. If not SUM product    */
		/* do not display field.                                           */
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covrenqIO.getChdrcoy());
		itemIO.setItemitem(covrenqIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void loadTr3861600()
	{
		start1610();
	}

protected void start1610()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), wsspcomn.company)
		|| isNE(itemIO.getItemtabl(), tablesInner.tr386)
		|| isNE(itemIO.getItemitem(), wsaaTr386Key)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isEQ(itemIO.getFunction(), varcom.begn)) {
				itemIO.setItemitem(wsaaTr386Key);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		ix.set(1);
		while ( !(isGT(ix, 10))) {
			if (isNE(tr386rec.progdesc[ix.toInt()], SPACES)
			&& isLT(iy, 3)) {
				wsaaMessage[iy.toInt()].set(tr386rec.progdesc[ix.toInt()]);
				iy.add(1);
			}
			ix.add(1);
		}
		
		itemIO.setFunction(varcom.nextr);
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5000();
		}
		if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.optdsc03Out[varcom.nd.toInt()].set("Y");
			sv.optind03Out[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.optdsc03Out[varcom.pr.toInt()].set("Y");
			sv.optind03Out[varcom.pr.toInt()].set("Y");
		}
		/*SCREEN-IO*/
		stampDutyflag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
		}else{
			if(isNE(covrenqIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
			sv.zstpduty01.set(covrenqIO.getZstpduty01());
		}
		}

		return ;
	}

protected void screenEdit2000()
	{
		para2000();
	}

protected void para2000()
	{
		/*    CALL 'SR546IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   SR546-DATA-AREA .             */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*VALIDATE-SCREEN*/
		optswchChck2100();
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void optswchChck2100()
	{
		call2110();
	}

protected void call2110()
	{
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		optswchrec.optsInd[1].set(sv.optind01);
		optswchrec.optsInd[2].set(sv.optind02);
		optswchrec.optsInd[3].set(sv.optind03);
		optswchrec.optsInd[4].set(sv.optind04);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		/* If the check has detected an error in OPTSWCH a non o-k*/
		/* status is returned. Unfortunately, it doesn't tell you*/
		/* which indicator was wrong! Rather than hard code a check*/
		/* for 'X' the status is moved to both indicators.*/
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			sv.optind01Err.set(optswchrec.optsStatuz);
			sv.optind02Err.set(optswchrec.optsStatuz);
			sv.optind03Err.set(optswchrec.optsStatuz);
			sv.optind04Err.set(optswchrec.optsStatuz);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*PARA*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		optswchCall4800();
		/*EXIT*/
	}

protected void optswchCall4800()
	{
		call4800();
	}

protected void call4800()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/* Call 'OPTSWCH' with a function of 'STCK' to determine*/
		/* which screen to display next.*/
		wsspcomn.chdrChdrnum.set(covrenqIO.getChdrnum());
		wsspcomn.crtable.set(covrenqIO.getCrtable());
		
		wsspcomn.cmode.set("IFE");
	    optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz, varcom.oK))
		&& (isNE(optswchrec.optsStatuz, varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optind03.set(optswchrec.optsInd[3]);
		sv.optind04.set(optswchrec.optsInd[4]);
		if (isEQ(optswchrec.optsStatuz, varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void getLifeName4900()
	{
		para4900();
	}

protected void para4900()
	{
		lifeenqIO.setChdrcoy(covrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrenqIO.getChdrnum());
		lifeenqIO.setLife(covrenqIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void checkCalcTax5000()
	{
		start5010();
	}

protected void start5010()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(covrenqIO.getLife());
		txcalcrec.coverage.set(covrenqIO.getCoverage());
		txcalcrec.rider.set(covrenqIO.getRider());
		txcalcrec.planSuffix.set(covrenqIO.getPlanSuffix());
		txcalcrec.crtable.set(covrenqIO.getCrtable());
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.register.set(chdrenqIO.getReg());
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.ccy.set(chdrenqIO.getCntcurr());
		txcalcrec.amountIn.set(ZERO);
		txcalcrec.transType.set("PREM");
		txcalcrec.txcode.set(tr52drec.txcode);
		if (isEQ(chdrenqIO.getBillfreq(), "00")) {
			txcalcrec.effdate.set(covrenqIO.getCrrcd());
		}
		else {
			txcalcrec.effdate.set(chdrenqIO.getPtdate());
		}
		txcalcrec.tranno.set(chdrenqIO.getTranno());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			wsaaMiscellaneousInner.wsaaTaxamt.set(sv.instPrem);
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[2]);
			}
			sv.taxamt.set(wsaaMiscellaneousInner.wsaaTaxamt);
			sv.optind03.set("+");
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc03Out[varcom.nd.toInt()].set("N");
			sv.optind03Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc03Out[varcom.pr.toInt()].set("N");
			sv.optind03Out[varcom.pr.toInt()].set("N");
		}
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private PackedDecimalData wsaaSuffixCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaCrcyCode = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
}
}
