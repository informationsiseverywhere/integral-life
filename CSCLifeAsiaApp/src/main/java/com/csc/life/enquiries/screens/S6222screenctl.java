package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6222screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
//	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S6222screensfl";
		lrec.subfileClass = S6222screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 16;
		lrec.pageSubfile = 15;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 6, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6222ScreenVars sv = (S6222ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S6222screenctlWritten, sv.S6222screensflWritten, av, sv.s6222screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6222ScreenVars screenVars = (S6222ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		ScreenRecord.setClassStringFormatting(pv);
	/*	screenVars.clntnum.setClassString("");
		screenVars.name1.setClassString("");
		screenVars.name2.setClassString("");
		screenVars.tsumins.setClassString("");*/
	}

/**
 * Clear all the variables in S6222screenctl
 */
	public static void clear(VarModel pv) {
		S6222ScreenVars screenVars = (S6222ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		ScreenRecord.clear(pv);
	/*	screenVars.clntnum.clear();
		screenVars.name1.clear();
		screenVars.name2.clear();
		screenVars.tsumins.clear();*/
	}
}
