/*
 * File: Tr597pt.java
 * Date: 30 August 2009 2:43:41
 * Author: Quipoz Limited
 * 
 * Class transformed from TR597PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.enquiries.tablestructures.Tr597rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR597.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr597pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Magnum Field Details                     SR597");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(37);
	private FixedLengthStringData filler7 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Status Item      Current Status Code");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(9);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Proposal");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler9 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Contract Risk");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 18);
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 21);
	private FixedLengthStringData filler11 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 24);
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 27);
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 30);
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 33);
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 36);
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 39);
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 42);
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 45);
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 48);
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 51);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 54);
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 57);
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 60);
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 63);
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 66);
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 69);
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 72);
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 75);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(77);
	private FixedLengthStringData filler29 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Contract Premium");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 18);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 21);
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 24);
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 27);
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 30);
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 36);
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 39);
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 42);
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 45);
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 48);
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 51);
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 54);
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 57);
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 60);
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 63);
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 66);
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 69);
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 72);
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 75);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(7);
	private FixedLengthStringData filler49 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Others");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler50 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" Contract Risk");
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 18);
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 21);
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 24);
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 27);
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 30);
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 36);
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 39);
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 42);
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 45);
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 48);
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 51);
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 54);
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 57);
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 60);
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 63);
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 66);
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69);
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 72);
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 75);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler70 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" Contract Premium");
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 18);
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 21);
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 24);
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 27);
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 30);
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 36);
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 39);
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 42);
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 45);
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 48);
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 51);
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 54);
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57);
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 60);
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 63);
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 66);
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 69);
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 72);
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 75);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler90 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" Coverage Risk");
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 18);
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 21);
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 24);
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 27);
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 30);
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 36);
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 39);
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo093 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 42);
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 45);
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 48);
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 54);
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 57);
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 60);
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 63);
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo101 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 66);
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 69);
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 72);
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 75);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler110 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" Coverage Premium");
	private FixedLengthStringData fieldNo105 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 18);
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 21);
	private FixedLengthStringData filler112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 24);
	private FixedLengthStringData filler113 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 27);
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo109 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 30);
	private FixedLengthStringData filler115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo110 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler116 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo111 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 36);
	private FixedLengthStringData filler117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 39);
	private FixedLengthStringData filler118 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo113 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 42);
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 45);
	private FixedLengthStringData filler120 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 48);
	private FixedLengthStringData filler121 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 51);
	private FixedLengthStringData filler122 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo117 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 54);
	private FixedLengthStringData filler123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 57);
	private FixedLengthStringData filler124 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo119 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 60);
	private FixedLengthStringData filler125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 63);
	private FixedLengthStringData filler126 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo121 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 66);
	private FixedLengthStringData filler127 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo122 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 69);
	private FixedLengthStringData filler128 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo123 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 72);
	private FixedLengthStringData filler129 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo124 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 75);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(21);
	private FixedLengthStringData filler130 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" TSAR Currency");
	private FixedLengthStringData fieldNo125 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 18);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr597rec tr597rec = new Tr597rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr597pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr597rec.tr597Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tr597rec.setCnRiskStat01);
		fieldNo006.set(tr597rec.setCnRiskStat02);
		fieldNo007.set(tr597rec.setCnRiskStat03);
		fieldNo008.set(tr597rec.setCnRiskStat04);
		fieldNo009.set(tr597rec.setCnRiskStat05);
		fieldNo010.set(tr597rec.setCnRiskStat06);
		fieldNo011.set(tr597rec.setCnRiskStat07);
		fieldNo012.set(tr597rec.setCnRiskStat08);
		fieldNo013.set(tr597rec.setCnRiskStat09);
		fieldNo014.set(tr597rec.setCnRiskStat10);
		fieldNo015.set(tr597rec.setCnRiskStat11);
		fieldNo016.set(tr597rec.setCnRiskStat12);
		fieldNo017.set(tr597rec.setCnRiskStat13);
		fieldNo018.set(tr597rec.setCnRiskStat14);
		fieldNo019.set(tr597rec.setCnRiskStat15);
		fieldNo020.set(tr597rec.setCnRiskStat16);
		fieldNo021.set(tr597rec.setCnRiskStat17);
		fieldNo022.set(tr597rec.setCnRiskStat18);
		fieldNo023.set(tr597rec.setCnRiskStat19);
		fieldNo024.set(tr597rec.setCnRiskStat20);
		fieldNo025.set(tr597rec.setCnPremStat01);
		fieldNo026.set(tr597rec.setCnPremStat02);
		fieldNo027.set(tr597rec.setCnPremStat03);
		fieldNo028.set(tr597rec.setCnPremStat04);
		fieldNo029.set(tr597rec.setCnPremStat05);
		fieldNo030.set(tr597rec.setCnPremStat06);
		fieldNo031.set(tr597rec.setCnPremStat07);
		fieldNo032.set(tr597rec.setCnPremStat08);
		fieldNo033.set(tr597rec.setCnPremStat09);
		fieldNo034.set(tr597rec.setCnPremStat10);
		fieldNo035.set(tr597rec.setCnPremStat11);
		fieldNo036.set(tr597rec.setCnPremStat12);
		fieldNo037.set(tr597rec.setCnPremStat13);
		fieldNo038.set(tr597rec.setCnPremStat14);
		fieldNo039.set(tr597rec.setCnPremStat15);
		fieldNo040.set(tr597rec.setCnPremStat16);
		fieldNo041.set(tr597rec.setCnPremStat17);
		fieldNo042.set(tr597rec.setCnPremStat18);
		fieldNo043.set(tr597rec.setCnPremStat19);
		fieldNo044.set(tr597rec.setCnPremStat20);
		fieldNo045.set(tr597rec.cnRiskStat01);
		fieldNo046.set(tr597rec.cnRiskStat02);
		fieldNo047.set(tr597rec.cnRiskStat03);
		fieldNo048.set(tr597rec.cnRiskStat04);
		fieldNo049.set(tr597rec.cnRiskStat05);
		fieldNo050.set(tr597rec.cnRiskStat06);
		fieldNo051.set(tr597rec.cnRiskStat07);
		fieldNo052.set(tr597rec.cnRiskStat08);
		fieldNo053.set(tr597rec.cnRiskStat09);
		fieldNo054.set(tr597rec.cnRiskStat10);
		fieldNo055.set(tr597rec.cnRiskStat11);
		fieldNo056.set(tr597rec.cnRiskStat12);
		fieldNo057.set(tr597rec.cnRiskStat13);
		fieldNo058.set(tr597rec.cnRiskStat14);
		fieldNo059.set(tr597rec.cnRiskStat15);
		fieldNo060.set(tr597rec.cnRiskStat16);
		fieldNo061.set(tr597rec.cnRiskStat17);
		fieldNo062.set(tr597rec.cnRiskStat18);
		fieldNo063.set(tr597rec.cnRiskStat19);
		fieldNo064.set(tr597rec.cnRiskStat20);
		fieldNo065.set(tr597rec.cnPremStat01);
		fieldNo066.set(tr597rec.cnPremStat02);
		fieldNo067.set(tr597rec.cnPremStat03);
		fieldNo068.set(tr597rec.cnPremStat04);
		fieldNo069.set(tr597rec.cnPremStat05);
		fieldNo070.set(tr597rec.cnPremStat06);
		fieldNo071.set(tr597rec.cnPremStat07);
		fieldNo072.set(tr597rec.cnPremStat08);
		fieldNo073.set(tr597rec.cnPremStat09);
		fieldNo074.set(tr597rec.cnPremStat10);
		fieldNo075.set(tr597rec.cnPremStat11);
		fieldNo076.set(tr597rec.cnPremStat12);
		fieldNo077.set(tr597rec.cnPremStat13);
		fieldNo078.set(tr597rec.cnPremStat14);
		fieldNo079.set(tr597rec.cnPremStat15);
		fieldNo080.set(tr597rec.cnPremStat16);
		fieldNo081.set(tr597rec.cnPremStat17);
		fieldNo082.set(tr597rec.cnPremStat18);
		fieldNo083.set(tr597rec.cnPremStat19);
		fieldNo084.set(tr597rec.cnPremStat20);
		fieldNo085.set(tr597rec.covRiskStat01);
		fieldNo086.set(tr597rec.covRiskStat02);
		fieldNo087.set(tr597rec.covRiskStat03);
		fieldNo088.set(tr597rec.covRiskStat04);
		fieldNo089.set(tr597rec.covRiskStat05);
		fieldNo090.set(tr597rec.covRiskStat06);
		fieldNo091.set(tr597rec.covRiskStat07);
		fieldNo092.set(tr597rec.covRiskStat08);
		fieldNo093.set(tr597rec.covRiskStat09);
		fieldNo094.set(tr597rec.covRiskStat10);
		fieldNo095.set(tr597rec.covRiskStat11);
		fieldNo096.set(tr597rec.covRiskStat12);
		fieldNo097.set(tr597rec.covRiskStat13);
		fieldNo098.set(tr597rec.covRiskStat14);
		fieldNo099.set(tr597rec.covRiskStat15);
		fieldNo100.set(tr597rec.covRiskStat16);
		fieldNo101.set(tr597rec.covRiskStat17);
		fieldNo102.set(tr597rec.covRiskStat18);
		fieldNo103.set(tr597rec.covRiskStat19);
		fieldNo104.set(tr597rec.covRiskStat20);
		fieldNo105.set(tr597rec.covPremStat01);
		fieldNo106.set(tr597rec.covPremStat02);
		fieldNo107.set(tr597rec.covPremStat03);
		fieldNo108.set(tr597rec.covPremStat04);
		fieldNo109.set(tr597rec.covPremStat05);
		fieldNo110.set(tr597rec.covPremStat06);
		fieldNo111.set(tr597rec.covPremStat07);
		fieldNo112.set(tr597rec.covPremStat08);
		fieldNo113.set(tr597rec.covPremStat09);
		fieldNo114.set(tr597rec.covPremStat10);
		fieldNo115.set(tr597rec.covPremStat11);
		fieldNo116.set(tr597rec.covPremStat12);
		fieldNo117.set(tr597rec.covPremStat13);
		fieldNo118.set(tr597rec.covPremStat14);
		fieldNo119.set(tr597rec.covPremStat15);
		fieldNo120.set(tr597rec.covPremStat16);
		fieldNo121.set(tr597rec.covPremStat17);
		fieldNo122.set(tr597rec.covPremStat18);
		fieldNo123.set(tr597rec.covPremStat19);
		fieldNo124.set(tr597rec.covPremStat20);
		fieldNo125.set(tr597rec.cntcurr);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
