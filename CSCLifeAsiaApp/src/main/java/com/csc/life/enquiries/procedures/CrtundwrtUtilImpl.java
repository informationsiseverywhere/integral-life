package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.csc.fsu.general.dataaccess.dao.UndrpfDAO;
import com.csc.fsu.general.dataaccess.model.Undrpf;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CrtundwrtUtilImpl implements CrtundwrtUtil {

	private final static String invf = "INVF";
	private BinaryData x = new BinaryData(5, 0);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Th618rec th618rec = new Th618rec();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Map<String, List<Itempf>> t5446ListMap;
	private Map<String, List<Itempf>> t5448ListMap;
	private Map<String, List<Itempf>> th618ListMap;
	
    private UndrpfDAO undrpfDao = IntegralApplicationContext.getApplicationContext().getBean("UndrpfDAO", UndrpfDAO.class);
    private ItemDAO itempfDAO = IntegralApplicationContext.getApplicationContext().getBean("itemDao", ItemDAO.class);
    
	@Override
	public Crtundwrec process(Crtundwrec crtundwrec) {
		crtundwrec.status.set(Varcom.oK);
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isEQ(crtundwrec.function,"DEL")
			&& crtundwrec.crtable.containsOnly("*")) {
			delSect2200(crtundwrec);
			return crtundwrec;
		}
		initSmartTable(crtundwrec);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(crtundwrec.cnttyp.toString());
		stringVariable1.append(crtundwrec.crtable.toString());
		
		//ILIFE-6587 wli31
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5448ListMap.containsKey(stringVariable1.toString().trim())){
			itempfList = t5448ListMap.get(stringVariable1.toString().trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(itempf.getItmfrm().compareTo(new BigDecimal(datcon1rec.intDate.toString())) < 1){
					t5448rec.t5448Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
				}else{
					crtundwrec.status.set(Varcom.endp);
					return crtundwrec;
				}			
			}		
		} else {
			crtundwrec.status.set(Varcom.endp);
			return crtundwrec;
		}
		
		String keyItemitem = t5448rec.rrsktyp.toString().trim();
		if (th618ListMap.containsKey(keyItemitem)) {
			itempfList = th618ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				th618rec.th618Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
		if (isEQ(th618rec.lrkclss,SPACES)) {
			crtundwrec.status.set(Varcom.mrnf);
			return crtundwrec;
		}
		for (x.set(1); !(isGT(x,5)); x.add(1)){
			boolean isExit = eachTh618Lrkcls1200(crtundwrec);
			if (isExit) {
				break;
			}
		}
		return crtundwrec;
	}

	private boolean delSect2200(Crtundwrec crtundwrec) {
		if (isEQ(crtundwrec.clntnum,SPACES)) {
			undrpfDao.deleteByChdrNum(crtundwrec.chdrnum.toString().trim(), crtundwrec.coy.toString().trim());
			return true;
		}
		Undrpf undrpf = new Undrpf();
		undrpf.setCLNTNUM(StringUtils.trimToEmpty(crtundwrec.clntnum.toString()));
		undrpf.setCOY(StringUtils.trimToEmpty(crtundwrec.coy.toString()));
		undrpf.setCURRCODE(StringUtils.trimToEmpty(crtundwrec.currcode.toString()));
		undrpf.setCHDRNUM(StringUtils.trimToEmpty(crtundwrec.chdrnum.toString()));
		if (!crtundwrec.crtable.containsOnly("*")) {
			undrpf.setCRTABLE(StringUtils.trimToEmpty(crtundwrec.crtable.toString()));
			undrpf.setLRKCLS01(StringUtils.trimToEmpty(th618rec.lrkcls[x.toInt()].toString()));
		}
		undrpfDao.deleteByUndrpfKeys(undrpf);
		return false;
	}
	
	private void initSmartTable(Crtundwrec crtundwrec){
		String coy = crtundwrec.coy.toString().trim();
		String pfx = smtpfxcpy.item.toString().trim();
		if (t5446ListMap == null) {
			t5446ListMap = itempfDAO.loadSmartTable(pfx, coy, "T5446");	
		}
		if (t5448ListMap == null) {
			t5448ListMap = itempfDAO.loadSmartTable(pfx, coy, "T5448");
		}
		if (th618ListMap == null) {
			th618ListMap = itempfDAO.loadSmartTable(pfx, coy, "TH618");	
		}
	}
	
	private boolean eachTh618Lrkcls1200(Crtundwrec crtundwrec) {
		if (isEQ(th618rec.lrkcls[x.toInt()],SPACES)) {
			return true;
		}
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5446ListMap.containsKey(th618rec.lrkcls[x.toInt()].toString().trim())){	
			itempfList = t5446ListMap.get(th618rec.lrkcls[x.toInt()].toString().trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(itempf.getItmfrm().compareTo(new BigDecimal(datcon1rec.intDate.toString())) < 1){
					t5446rec.t5446Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
				}else{
					return true;
				}			
			}		
		}else{
			return true;
		}
		
		if (isEQ(t5446rec.lrkcls,SPACES)) {
			t5446rec.lrkcls.set(th618rec.lrkcls[x.toInt()]);
		}
		if (isEQ(crtundwrec.function,"ADD")){
			return addSect2000(crtundwrec);
		} else if (isEQ(crtundwrec.function,"DEL")){
			return delSect2200(crtundwrec);
		} else if (isEQ(crtundwrec.function,"MOD")){
			Undrpf undrpf = new Undrpf();
			undrpf.setCLNTNUM(StringUtils.trimToEmpty(crtundwrec.clntnum.toString()));
			undrpf.setCOY(StringUtils.trimToEmpty(crtundwrec.coy.toString()));
			undrpf.setCURRCODE(StringUtils.trimToEmpty(crtundwrec.currcode.toString()));
			undrpf.setCHDRNUM(StringUtils.trimToEmpty(crtundwrec.chdrnum.toString()));
			if (!crtundwrec.crtable.containsOnly("*")) {
				undrpf.setCRTABLE(StringUtils.trimToEmpty(crtundwrec.crtable.toString()));
				undrpf.setLRKCLS01(StringUtils.trimToEmpty(th618rec.lrkcls[x.toInt()].toString()));
			}
			undrpf.setSUMINS(crtundwrec.sumins.getbigdata());
			undrpfDao.updateByUndrpfKeys(undrpf);
			crtundwrec.status.set(Varcom.oK);
		} else {
			crtundwrec.status.set(invf);
			x.set(99);
		}
		return false;
	}
	
	private boolean addSect2000(Crtundwrec crtundwrec) {
		Undrpf undrpf = new Undrpf();
		undrpf.setCLNTNUM(crtundwrec.clntnum.toString());
		undrpf.setCOY(crtundwrec.coy.toString());
		undrpf.setCURRCODE(crtundwrec.currcode.toString());
		undrpf.setCHDRNUM(crtundwrec.chdrnum.toString());
		undrpf.setCRTABLE(crtundwrec.crtable.toString());
		undrpf.setLRKCLS01(th618rec.lrkcls[x.toInt()].toString());
		undrpf.setLRKCLS02(t5446rec.lrkcls.toString());
		undrpf.setCNTTYP(crtundwrec.cnttyp.toString());
		undrpf.setLIFE(crtundwrec.life.toString());
		undrpf.setSUMINS(crtundwrec.sumins.getbigdata());
		undrpf.setEFFDATE(datcon1rec.intDate.toInt());
		undrpf.setADSC(SPACES.toString());
		try {
			undrpfDao.insertUndrpf(undrpf);
		} catch (SQLRuntimeException e) {
			return true;
		}
		return false;
	}

}
