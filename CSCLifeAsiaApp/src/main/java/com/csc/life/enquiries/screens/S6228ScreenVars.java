package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6228
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6228ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(307+DD.cltaddr.length*5);//pmujavadiya
	public FixedLengthStringData dataFields = new FixedLengthStringData(99+DD.cltaddr.length*5).isAPartOf(dataArea, 0);//pmujavadiya//ILIFE-3222
	public FixedLengthStringData asgnCodeDesc = DD.asgncddesc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData asgnnum = DD.asgnnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData cltaddrs = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(dataFields, 38);//pmujavadiya
	public FixedLengthStringData[] cltaddr = FLSArrayPartOfStructure(5, DD.cltaddr.length, cltaddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(cltaddrs.length()).isAPartOf(cltaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(filler,0);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*1);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*2);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*3);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*4);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(dataFields,38+DD.cltaddr.length*5);//pmujavadiya
	public ZonedDecimalData commfrom = DD.commfrom.copyToZonedDecimal().isAPartOf(dataFields,48+DD.cltaddr.length*5);
	public ZonedDecimalData commto = DD.commto.copyToZonedDecimal().isAPartOf(dataFields,56+DD.cltaddr.length*5);
	public FixedLengthStringData name = DD.name.copy().isAPartOf(dataFields,64+DD.cltaddr.length*5);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,94+DD.cltaddr.length*5);
	public FixedLengthStringData asgnflag = DD.zasgnflag.copy().isAPartOf(dataFields,98+DD.cltaddr.length*5);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 99+DD.cltaddr.length*5);//ILIFE-3222
	public FixedLengthStringData asgncddescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData asgnnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cltaddrsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] cltaddrErr = FLSArrayPartOfStructure(5, 4, cltaddrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(cltaddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData cltaddr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData cltaddr03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData cltaddr04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData cltaddr05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData cltpcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData commfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData commtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData nameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData zasgnflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 151+DD.cltaddr.length*5);//ILIFE-3222
	public FixedLengthStringData[] asgncddescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] asgnnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData cltaddrsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] cltaddrOut = FLSArrayPartOfStructure(5, 12, cltaddrsOut, 0);
	public FixedLengthStringData[][] cltaddrO = FLSDArrayPartOfArrayStructure(12, 1, cltaddrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(cltaddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cltaddr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] cltaddr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] cltaddr03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] cltaddr04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] cltaddr05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] cltpcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] commfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] commtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] nameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] zasgnflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData commfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData commtoDisp = new FixedLengthStringData(10);

	public LongData S6228screenWritten = new LongData(0);
	public LongData S6228windowWritten = new LongData(0);
	public LongData S6228protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6228ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zasgnflagOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {name, cltaddr01, cltaddr02, cltaddr03, cltaddr04, cltaddr05, asgnCodeDesc, reasoncd, commfrom, commto, asgnnum, asgnflag, cltpcode};
		screenOutFields = new BaseData[][] {nameOut, cltaddr01Out, cltaddr02Out, cltaddr03Out, cltaddr04Out, cltaddr05Out, asgncddescOut, reasoncdOut, commfromOut, commtoOut, asgnnumOut, zasgnflagOut, cltpcodeOut};
		screenErrFields = new BaseData[] {nameErr, cltaddr01Err, cltaddr02Err, cltaddr03Err, cltaddr04Err, cltaddr05Err, asgncddescErr, reasoncdErr, commfromErr, commtoErr, asgnnumErr, zasgnflagErr, cltpcodeErr};
		screenDateFields = new BaseData[] {commfrom, commto};
		screenDateErrFields = new BaseData[] {commfromErr, commtoErr};
		screenDateDispFields = new BaseData[] {commfromDisp, commtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6228screen.class;
		protectRecord = S6228protect.class;
	}

}
