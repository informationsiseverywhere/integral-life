/*
 * File: P6235.java
 * Date: 30 August 2009 0:37:43
 * Author: Quipoz Limited
 * 
 * Class transformed from P6235.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.enquiries.screens.S6235ScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.recordstructures.Batckey;
//ILB-498 starts
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selectio 
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will b 
*     stored  in  the  CHDRENQ I/O module. Retrieve the details an 
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description fro 
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description fro 
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist 
*
*
*     The  details  that  will  be  displayed  will  come  from tw 
*     data-sets:  SACSENQ  and  ACBLENQ.  The same information wil 
*     be  extracted  from both data-sets and they will both be rea 
*     in  the  same sequence. However different fields are used fo 
*     the keys on each data-set.
*
*     This  program should process all the relevant ACBLENQ record 
*     and then all the relevant SACSENQ records.
*
*     Load the subfile as follows:
*
*          T5645 contains a list of all the SACS Code and SACS Typ 
*          combinations that are used to drive  the program throug 
*          the  ACBLENQ and SACSENQ data-sets. Perform  a  BEGN  o 
*          T5645 using WSAA-PROG as the key. Note that there may b 
*          several  pages  on  T5645 each with up to  15  lines  o 
*          detail.  From each line take the SACS Code and SACS Typ 
*          and use them to read through ACBLENQ. The  full  key is 
*          RLDGCOY   (from  CHDRCOY),  RLDGACCT   (from   CHDRNUM) 
*          SACSCODE (from T5645) and SACSTYP (from T5645).
*
*          Display the SACS CODE with its  short  description  fro 
*          T3616,  the SACS TYPE with its  short  description  fro 
*          T3695,  the  Original Currency, (ORIGCURR)  and  Curren 
*          Balance (SACSCURBAL).
*
*          Also store, on a hidden field in the  subfile  record,  
*          flag  indicating that this record's  details  come  fro 
*          ACBLENQ.
*
*          Process SACSENQ for the same combination  of  codes. Th 
*          SACSENQ key is CHDRCOY, CHDRNUM,  SACSCODE  and SACSTYP 
*          The  same  fields are displayed.  Original  Currency  i 
*          CNTCURR.
*
*          Also store, on a hidden field in the  subfile  record,  
*          flag  indicating that this record's  details  come  fro 
*          SACSENQ.
*
*     Load  all  pages  required in the subfile and set the subfil 
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Release  both  the ACBLENQ and SACSENQ I/O  modules  in  cas 
*     anything is held from a previous selection.
*
*     At  this  point  the program will be either searching for th 
*     FIRST  selected  record  in  order  to  pass  control  to th 
*     Transactions Postings program for the selected transaction o 
*     it  will  be returning from the Transactions Postings progra 
*     after  displaying  some  details  and  searching for the NEX 
*     selected record.
*
*     It  will be able to determine which of these two states it i 
*     in by examining the Stack Action Flag.
*
*     If not returning from a Transactions Postings display, (Stac 
*     Action  Flag  is  blank),  perform  a start on the subfile t 
*     position the file pointer at the beginning.
*
*     Each  time  it  returns  to  this  program after processing  
*     previous seelction its position in the subfile will have bee 
*     retained  and  it will be able to continue from where it lef 
*     off.
*
*     Processing  from here is the same for either state. After th 
*     Start  or  after  returning to the program after processing  
*     previous selection read the next record from the subfile.  I 
*     this  is not selected (Select is blank), continue reading th 
*     next  subfile  record  until  one  is  found with a non-blan 
*     Select  field or end of file is reached. Do not use the 'Rea 
*     Next Changed Subfile Record' function.
*
*     If nothing was selected or there are no  more  selections  t 
*     process,  continue  by  just  moving spaces  to  the  curren 
*     stackaction field and exit.
*
*     If a selection has been found the Sub-Account Balances are t 
*     be displayed.
*
*     If the selected subfile record contains data  from  a SACSEN 
*     record then the next function will read the RTRN data-set fo 
*     the Sub-Account Postings. Perform a READS  on the appropriat 
*     SACSENQ record, add 1 to the program pointer and exit.
*
*     If the selected subfile record contains data  from  a ACBLEN 
*     record then the next function will read the ACMV data-set fo 
*     the Sub-Account Postings. Perform a READS  on the appropriat 
*     ACBLENQ record, add 1 to the program pointer and exit.
*
*
* Notes.
* ------
*
*     Create  a  new  view of ACBLPF called ACBLENQ which uses onl 
*     those  fields  required for this program. It will be keyed o 
*     RLDGCOY, RLDGACCT (first 8 characters), SACSCODE and SACSTYP 
*
*     Create  a  new  view of SACSPF called SACSENQ which uses onl 
*     those fields required for this program. It will  be  keyed o 
*     CHDRCOY, CHDRNUM, SACSCODE and SACSTYP.
*
*     Tables Used:
*
* T1688 - Transaction Codes                 Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3616 - Sub-Account Codes                 Key: SACSCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T3695 - Sub-Account Types                 Key: SACSTYP
* T5645 - Financial Trans Accounting Rules  Key: WSAA-PROG
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6235 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	private static final Logger LOGGER = LoggerFactory.getLogger(P6235.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6235");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final String wsaaAcblenqFlag = "A";
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);

	protected FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaComponent = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 8);
	private static final String t3588 = "T3588";
	private static final String t3616 = "T3616";
	private static final String t3623 = "T3623";
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";	
	private Batckey wsaaBatckey = new Batckey();
	private T5645rec t5645rec = new T5645rec();
	private Wssplife wssplife = new Wssplife();
	private S6235ScreenVars sv = ScreenProgram.getScreenVars( S6235ScreenVars.class);
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	protected List<Acblpf> acblList;
	protected Acblpf acblpf = new Acblpf(); 
	protected List<String> sacsCodeTypeList;
	
	//ILB-498 starts
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private Clntpf clntpf = new Clntpf();

	private DescDAO descDAO = getApplicationContext().getBean("descDAO",DescDAO.class);
//	private Descpf descpf = new Descpf();

	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private Lifepf lifepf = new Lifepf();
	//ILB-498 ends
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		bypassStart4010, 
		nextProgram4080, 
		exit4090
	}

	public P6235() {
		super();
		screenVars = sv;
		new ScreenModel("S6235", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.sacscurbal.set(ZERO);
		wsaaRldgacct.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6235", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		if(!(chdrpf.getUniqueNumber()>0)) {
			fatalError600();
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		
		//ILB-498 starts
		
		lifepf = lifepfDAO.getLifeRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
		if (lifepf == null) {
			fatalError600();
		}
		/*lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}*/
		sv.lifenum.set(lifepf.getLifcnum());
		clntpf.setClntnum(lifepf.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpf = clntpfDAO.selectClient(clntpf);
		if (clntpf == null) {
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifepf.setJlife("01");
		List<Lifepf> lifepfList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "01");//IJTI-1485
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            sTO S6235-JLIFE                   */
		/*                                 S6235-JLIFENAME               */
		/*  ELSE                                                         */
		if (!lifepfList.isEmpty()) {
			for (Lifepf life : lifepfList) {
				sv.jlife.set(life.getLifcnum());
				clntpf.setClntnum(life.getLifcnum());
				clntpf.setClntcoy(wsspcomn.fsuco.toString());
				clntpf.setClntpfx("CN");
				clntpf = clntpfDAO.selectClient(clntpf);
				if (clntpf == null) {
					fatalError600();
				}
				else {
					plainname();
					sv.jlifename.set(wsspcomn.longconfname);
				}
			}
		}
		/*    Obtain the Contract Type description from T5688.*/

//		descpf.setDescpfx("IT");
//		descpf.setDesccoy(wsspcomn.company.toString());
//		descpf.setDesctabl(t5688);
//		descpf.setDescitem(chdrpf.getCnttype());
//		descpf.setLanguage(wsspcomn.language.toString());
//		descpf=descDAO.getdescData("IT", t5688, chdrpf.getCnttype(), wsspcomn.company.toString(), wsspcomn.language.toString());
//
//		if (descpf == null) {
//			sv.ctypedes.fill("?");
//		}
//		else {
//			sv.ctypedes.set(descpf.getLongdesc());
//		}
//
//		descpf=descDAO.getdescData("IT", t3623, chdrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());
//		if (descpf == null) {
//			sv.chdrstatus.fill("?");
//		}
//		else {
//			sv.chdrstatus.set(descpf.getLongdesc());
//		}
//		
//		/*    Obtain the Premuim Status description from T3588.*/
//		descpf=descDAO.getdescData("IT", t3588, chdrpf.getPstcde(), wsspcomn.company.toString(), wsspcomn.language.toString());
//		if (descpf == null) {
//			sv.premstatus.fill("?");
//		}
//		else {
//			sv.premstatus.set(descpf.getLongdesc());
//		}
		/* ILB-1100 START */
		Map<String, String> itemMap = new HashMap<String, String>();
		itemMap.put(t5688, chdrpf.getCnttype());
		itemMap.put(t3623, chdrpf.getStatcode());
		itemMap.put(t3588, chdrpf.getPstcde());
		Map<String, Descpf> descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(),wsspcomn.language.toString(), itemMap);
		if(descMap.isEmpty()) {
			sv.ctypedes.fill("?");
			sv.chdrstatus.fill("?");
			sv.premstatus.fill("?");
		} else {
			if(descMap.containsKey(t5688)) {
				sv.ctypedes.set(descMap.get(t5688).getLongdesc());	
			} else {
				sv.ctypedes.fill("?");
			}
			if(descMap.containsKey(t3623)) {
				sv.chdrstatus.set(descMap.get(t3623).getLongdesc());	
			} else {
				sv.chdrstatus.fill("?");
			}
			if(descMap.containsKey(t3588)) {
				sv.premstatus.set(descMap.get(t3588).getLongdesc());	
			} else {
				sv.premstatus.fill("?");
			}
		}		
		/* ILB-1100 END */
		/*    This is the processing for the ACBLENQ data-set. It will   **/
		/*    be repeated for the SACS data-set.                         **/
		/*    Read the first record from table T5645 for this program.*/
		sacsCodeTypeList = new ArrayList<String>();
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		List<Itempf> list = this.itempfDAO.findByItemSeq(itempf);
		for(Itempf item : list) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(item.getGenarea()));
			sub1.set(1);
			while(!isGT(sub1, 15)) {
				sacsCodeTypeList.add(t5645rec.sacscode[sub1.toInt()].toString().concat(t5645rec.sacstype[sub1.toInt()].toString()));
				sub1.add(1);
			}
		}

		
		processT5645Page1100();
		/*    There may be several pages on T5645 for the current program.*/
		/*    These are handled one at a time. Each page may have up to 15*/
		/*    lines containing a pair of codes: SACS CODE and SACS TYPE.*/
		/*    Each pair is then used in a key to read ACBLENQ. There may*/
		/*    be several records on ACBLENQ sharing this pair of codes.*/
		/*    Each one will be displayed as a line on the subfile.*/
		
		/*   UNTIL ITEM-ITEMPFX        NOT = 'IT'         OR            */
		/*         ITEM-ITEMCOY        NOT = WSSP-COMPANY OR            */
		/*         ITEM-ITEMTABL       NOT = T5645        OR            */
		/*         ITEM-ITEMITEM       NOT = WSAA-PROG    OR            */
		/*         ITEM-STATUZ             = ENDP.                      */
		scrnparams.subfileRrn.set(1);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

//--edited in MLIL 271-//
protected void plainname()
{
	/*PLAIN-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname();

	} else if (isNE(clntpf.getGivname(), SPACES)) {
		String firstName = clntpf.getGivname();//IJTI-1485
		String lastName = clntpf.getSurname();//IJTI-1485
		String delimiter = ",";
		
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		
	} else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
	/*PLAIN-EXIT*/
}
//-----//

//--edited in MLIL-271---//
protected void payeename()
{
	/*PAYEE-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname();
	} else if (isEQ(clntpf.getEthorig(), "1")) {
		String firstName = clntpf.getGivname();//IJTI-1485
		String lastName = clntpf.getSurname();//IJTI-1485
		String delimiter = "";
		String salute = clntpf.getSalutl();//IJTI-1485
		
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		String fullNameWithSalute = stringUtil.includeSalute(salute, fullName);
	
		wsspcomn.longconfname.set(fullNameWithSalute);
	} else {
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}
	/*PAYEE-EXIT*/
}
//-- edited in MLIL 271-//
protected void corpname()
{
	/* PAYEE-1001 */
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME DELIMITED SIZE */
	/* CLTS-GIVNAME DELIMITED ' ' */
	String firstName = clntpf.getLgivname();//IJTI-1485
	String lastName = clntpf.getLsurname();//IJTI-1485
	String delimiter = "";
	
	// this way we can override StringUtil behaviour in formatName()
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	/*CORP-EXIT*/
}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processT5645Page1100()
	{
		
		/* PERFORM 1200-PROCESS-T5645-LINE 15 TIMES.                    */
		wsaaChdrnum.set(chdrpf.getChdrnum());
		wsaaComponent.set(SPACES);
		acblList = acblDao.loadDataBulk(chdrpf.getChdrcoy().toString(), wsaaChdrnum.toString(), "", "", "", "");
		processT5645Line1200();
	}

	protected void processT5645Line1200() {
		// MOVE CHDRENQ-CHDRNUM TO ACBLENQ-RLDGACCT.
		for (Acblpf acbl : acblList) {
			acblpf = acbl;
			if (isNE(wsaaChdrnum, chdrpf.getChdrnum())) {
				break;
			}
			if (sacsCodeTypeList.contains(acblpf.getSacscode().trim() + acblpf.getSacstyp().trim())
					|| sacsCodeTypeList.contains(acblpf.getSacscode() + acblpf.getSacstyp())) {
				wsaaRldgacct.set(acblpf.getRldgacct());
				acblenqSacsGroup1300();
			}
		}
	}



	/**
	* <pre>
	*****Take the SACS CODE and the SACS TYPE from the T5645 line and 
	*****read all records on SACSENQ that have matching codes. The    
	*****key is Company, Contract No. and Trans. No. (descending).    
	*****MOVE CHDRENQ-CHDRCOY        TO SACSENQ-CHDRCOY.              
	*****MOVE CHDRENQ-CHDRNUM        TO SACSENQ-CHDRNUM.              
	*****MOVE T5645-SACSCODE (SUB1)  TO SACSENQ-SACSCODE.             
	*****MOVE T5645-SACSTYPE (SUB1)  TO SACSENQ-SACSTYP.              
	*****MOVE BEGN                   TO SACSENQ-FUNCTION.             
	*****MOVE SPACES                 TO SACSENQ-CNTCURR.              
	*****CALL 'SACSENQIO'         USING SACSENQ-PARAMS.               
	*****IF   SACSENQ-STATUZ      NOT = O-K                           
	***** AND SACSENQ-STATUZ      NOT = ENDP                          
	*****     MOVE SACSENQ-PARAMS    TO SYSR-PARAMS                   
	*****     PERFORM 600-FATAL-ERROR.                                
	*****PERFORM 1400-SACSENQ-SACS-GROUP                              
	*****  UNTIL SACSENQ-CHDRCOY  NOT = CHDRENQ-CHDRCOY       OR      
	*****        SACSENQ-CHDRNUM  NOT = CHDRENQ-CHDRNUM       OR      
	*****        SACSENQ-SACSCODE NOT = T5645-SACSCODE (SUB1) OR      
	*****        SACSENQ-SACSTYP  NOT = T5645-SACSTYPE (SUB1) OR      
	*****        SACSENQ-STATUZ       = ENDP.                         
	* </pre>
	*/

protected void acblenqSacsGroup1300()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		/* MOVE ACBLENQ-RLDGACCT       TO WSAA-RLDGACCT.           <004>*/
		/* IF WSAA-CHDRNUM             NOT = CHDRENQ-CHDRNUM  OR   <004>*/
		/*    T5645-SACSCODE(SUB1)     NOT = ACBLENQ-SACSCODE OR   <004>*/
		/*    T5645-SACSTYPE(SUB1)     NOT = ACBLENQ-SACSTYP       <004>*/
		/*    GO                       TO 1350-READ.               <004>*/
		sv.subfileFields.set(SPACES);
		sv.component.set(wsaaComponent);
		sv.rectype.set(wsaaAcblenqFlag);
		sv.sacscode.set(acblpf.getSacscode());
		sv.sacstyp.set(acblpf.getSacstyp());
		sv.curr.set(acblpf.getOrigcurr());
		sv.sacscurbal.set(acblpf.getSacscurbal());
		/*    Obtain the SACS CODE description from T3616.*/
		
//		//ILB-498 starts
//		descpf=descDAO.getdescData("IT", t3616, acblpf.getSacscode(), wsspcomn.company.toString(), wsspcomn.language.toString());
//		if (descpf == null) {
//			sv.sacscoded.fill("?");
//		}
//		else {
//			sv.sacscoded.set(descpf.getLongdesc());
//		}
//		
//		/*    Obtain the SACS TYPE description from T3695.*/
//		
//		descpf=descDAO.getdescData("IT", t3695, acblpf.getSacstyp(), wsspcomn.company.toString(), wsspcomn.language.toString());
//		if (descpf == null) {
//			sv.sacstypd.fill("?");
//		}
//		else {
//			sv.sacstypd.set(descpf.getLongdesc());
//		}
//		//ILB-498 ends
		
		/* ILB-1100 START */
		Map<String, String> itemMap = new HashMap<String, String>();
		itemMap.put(t3616, acblpf.getSacscode());
		itemMap.put(t3695, acblpf.getSacstyp());
		Map<String, Descpf> descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(),wsspcomn.language.toString(), itemMap);
		if(descMap.isEmpty()) {
			sv.sacscoded.fill("?");
			sv.sacstypd.fill("?");
		} else {
			if(descMap.containsKey(t3616)) {
				sv.sacscoded.set(descMap.get(t3616).getLongdesc());	
			} else {
				sv.sacscoded.fill("?");
			}
			if(descMap.containsKey(t3695)) {
				sv.sacstypd.set(descMap.get(t3695).getLongdesc());	
			} else {
				sv.sacstypd.fill("?");
			}
		}		
		/* ILB-1100 END */
		
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6235", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*1400-SACSENQ-SACS-GROUP SECTION.                                 
	*1400-PARA.                                                       
	*****MOVE SPACES                 TO S6235-SUBFILE-FIELDS.         
	*****MOVE WSAA-SACSENQ-FLAG      TO S6235-RECTYPE.                
	*****MOVE SACSENQ-SACSCODE       TO S6235-SACSCODE.               
	*****MOVE SACSENQ-SACSTYP        TO S6235-SACSTYP.                
	*****MOVE SACSENQ-CNTCURR        TO S6235-CURR.                   
	*****MOVE SACSENQ-SACSCURBAL     TO S6235-SACSCURBAL.             
	*****Obtain the SACS CODE description from T3616.                 
	*****MOVE SPACES                 TO DESC-DATA-KEY.                
	*****MOVE 'IT'                   TO DESC-DESCPFX.                 
	*****MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 
	*****MOVE T3616                  TO DESC-DESCTABL.                
	*****MOVE SACSENQ-SACSCODE       TO DESC-DESCITEM.                
	*****MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.                
	*****MOVE READR                  TO DESC-FUNCTION.                
	*****CALL 'DESCIO' USING DESC-PARAMS.                             
	*****IF   DESC-STATUZ            NOT = O-K                        
	*****                        AND NOT = MRNF                       
	*****     MOVE DESC-PARAMS       TO SYSR-PARAMS                   
	*****     PERFORM 600-FATAL-ERROR.                                
	*****IF   DESC-STATUZ            = MRNF                           
	*****     MOVE ALL '?'           TO S6235-SACSCODED               
	*****ELSE                                                         
	*****     MOVE DESC-SHORTDESC    TO S6235-SACSCODED.              
	*****Obtain the SACS TYPE description from T3695.                 
	*****MOVE SPACES                 TO DESC-DATA-KEY.                
	*****MOVE 'IT'                   TO DESC-DESCPFX.                 
	*****MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 
	*****MOVE T3695                  TO DESC-DESCTABL.                
	*****MOVE SACSENQ-SACSTYP        TO DESC-DESCITEM.                
	*****MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.                
	*****MOVE READR                  TO DESC-FUNCTION.                
	*****CALL 'DESCIO' USING DESC-PARAMS.                             
	*****IF   DESC-STATUZ            NOT = O-K                        
	*****                        AND NOT = MRNF                       
	*****     MOVE DESC-PARAMS       TO SYSR-PARAMS                   
	*****     PERFORM 600-FATAL-ERROR.                                
	*****IF   DESC-STATUZ            = MRNF                           
	*****     MOVE ALL '?'           TO S6235-SACSTYPD                
	*****ELSE                                                         
	*****     MOVE DESC-SHORTDESC    TO S6235-SACSTYPD.               
	*****Write the subfile record.                                    
	*****MOVE SADD                   TO SCRN-FUNCTION                 
	*****CALL 'S6235IO'           USING SCRN-SCREEN-PARAMS            
	*****                               S6235-DATA-AREA               
	*****                               S6235-SUBFILE-AREA            
	*****IF SCRN-STATUZ           NOT = O-K                           
	*****     MOVE SCRN-STATUZ       TO SYSR-STATUZ                   
	*****     PERFORM 600-FATAL-ERROR.                                
	*****Read the next SACSENQ record.                                
	*****MOVE NEXTR                  TO SACSENQ-FUNCTION.             
	*****CALL 'SACSENQIO'         USING SACSENQ-PARAMS.               
	*****IF   SACSENQ-STATUZ      NOT = O-K                           
	***** AND SACSENQ-STATUZ      NOT = ENDP                          
	*****     MOVE SACSENQ-PARAMS    TO SYSR-PARAMS                   
	*****     PERFORM 600-FATAL-ERROR.                                
	*1490-EXIT.                                                       
	***** EXIT.                                                       
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO1*/
		/*    CALL 'S6235IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6235-DATA-AREA                         */
		/*                         S6235-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
				case bypassStart4010: 
					bypassStart4010();
				case nextProgram4080: 
					nextProgram4080();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*    Release any ACBLENQ record that may have been stored.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		acblDao.deleteCacheObject(acblpf);
		/*Release any SACSENQ record that may have been stored.        */
		/*MOVE RLSE                   TO SACSENQ-FUNCTION.             */
		/*CALL 'SACSENQIO'         USING SACSENQ-PARAMS.               */
		/*IF   SACSENQ-STATUZ      NOT = O-K                           */
		/*     MOVE SACSENQ-PARAMS    TO SYSR-PARAMS                   */
		/*     PERFORM 600-FATAL-ERROR.                                */
		/*    If returning from a program further down the stack then*/
		/*    bypass the start on the subfile.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.bypassStart4010);
		}
		/*    Re-start the subfile.*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6235", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypassStart4010()
	{
		if (isEQ(sv.select, SPACES)) {
			while ( !(isNE(sv.select, SPACES)
			|| isEQ(scrnparams.statuz, varcom.endp))) {
				readSubfile4100();
			}
			
		}
		/*  Nothing pressed at all, end working*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/* All requests services,*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			goTo(GotoLabel.exit4090);
		}
		/* Blank out action to ensure it is not processed again*/
		sv.select.set(SPACES);
		/*IF   S6235-RECTYPE           = WSAA-SACSENQ-FLAG             */
		/*     GO TO 4020-STORE-SACS-RECORD.                           */
		/*    Read and store the selected ACBLENQ record.*/
		/*    Read and store the selected ACBLENQ record.*/
		/*MOVE CHDRENQ-CHDRNUM        TO ACBLENQ-RLDGACCT.             */
		wsaaChdrnum.set(chdrpf.getChdrnum());
		wsaaComponent.set(sv.component);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		acblpf = acblDao.loadDataByBatch(wsspcomn.company.toString(), sv.sacscode.toString(), wsaaRldgacct.toString(), sv.curr.toString(), sv.sacstyp.toString());
		LOGGER.info("Company: {} SACSCODE: {}", wsspcomn.company.toString(), sv.sacscode.toString());//IJTI-1561
		LOGGER.info("wsaaRldgacct: {} Currency: {}", wsaaRldgacct, sv.curr.toString());//IJTI-1561
		LOGGER.info("SACSTYP: {}", sv.sacstyp.toString());//IJTI-1561
		if(acblpf != null && !(acblpf.getUniqueNumber()>0)) {
			fatalError600();
		}
		acblDao.setCacheObject(acblpf);
		goTo(GotoLabel.nextProgram4080);
	}

	/**
	* <pre>
	*****Read and store the selected SACSENQ record.                  
	*4020-STORE-SACS-RECORD.                                          
	*****MOVE SPACES                 TO SACSENQ-DATA-AREA.            
	*****MOVE WSSP-COMPANY           TO SACSENQ-CHDRCOY.              
	*****MOVE CHDRENQ-CHDRNUM        TO SACSENQ-CHDRNUM               
	*****MOVE S6235-SACSCODE         TO SACSENQ-SACSCODE.             
	*****MOVE S6235-SACSTYP          TO SACSENQ-SACSTYP.              
	*****MOVE S6235-CURR             TO SACSENQ-CNTCURR.              
	*****MOVE READS                  TO SACSENQ-FUNCTION.             
	*****CALL 'SACSENQIO'         USING SACSENQ-PARAMS.               
	*****IF   SACSENQ-STATUZ      NOT = O-K                           
	*****     MOVE SACSENQ-PARAMS    TO SYSR-PARAMS                   
	*****     PERFORM 600-FATAL-ERROR.                                
	* </pre>
	*/
protected void nextProgram4080()
	{
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S6235", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}


	public StringUtil getStringUtil() {
		return stringUtil;
	}

}
