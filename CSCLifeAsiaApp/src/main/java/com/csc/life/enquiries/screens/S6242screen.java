package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6242screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6242ScreenVars sv = (S6242ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6242screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6242ScreenVars screenVars = (S6242ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.singlePremium.setClassString("");
		screenVars.premcessDisp.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.annivProcDateDisp.setClassString("");
		screenVars.rerateDateDisp.setClassString("");
		screenVars.rerateFromDateDisp.setClassString("");
		screenVars.benBillDateDisp.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.optextind.setClassString("");
		screenVars.unitStatementDateDisp.setClassString("");
		screenVars.bonusInd.setClassString("");
		screenVars.pbind.setClassString("");
		screenVars.payflag.setClassString("");
		screenVars.optsmode.setClassString("");
		screenVars.bappmeth.setClassString("");
		screenVars.zdesc.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.taxind.setClassString("");
		/*BRD-306 START*/
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		screenVars.select.setClassString("");
		screenVars.riskCessAge.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		/*BRD-306 END*/
		//ILIFE-3421-STARTS
		screenVars.prmbasis.setClassString("");
		screenVars.benCessAge.setClassString("");
		screenVars.benCessTerm.setClassString("");
		screenVars.benCessDateDisp.setClassString("");
		screenVars.benCessDate.clear();
		//ILIFE-3421-ENDS
		screenVars.zsredtrm.setClassString("");/*ILIFE-3685*/
		screenVars.dialdownoption.setClassString("");//BRD-NBP-011
		screenVars.exclind.setClassString("");
		/*ILIFE-6968 start*/
		screenVars.lnkgno.setClassString("");
		screenVars.lnkgsubrefno.setClassString("");
		/*ILIFE-6968 end*/
		screenVars.tpdtype.setClassString("");//ILIFE-7118
		screenVars.lnkgind.setClassString("");

		screenVars.aepaydet.setClassString("");

		screenVars.zstpduty01.setClassString("");	//ILIFE-7746
		// IBPLIFE-2139
				screenVars.trcode.setClassString("");
				screenVars.trcdedesc.setClassString("");
				screenVars.dateeff.setClassString("");
				screenVars.covrprpse.setClassString("");

	}

/**
 * Clear all the variables in S6242screen
 */
	public static void clear(VarModel pv) {
		S6242ScreenVars screenVars = (S6242ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.numpols.clear();
		screenVars.planSuffix.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.liencd.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.sumin.clear();
		screenVars.singlePremium.clear();
		screenVars.premcessDisp.clear();
		screenVars.premcess.clear();
		screenVars.instPrem.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.annivProcDateDisp.clear();
		screenVars.annivProcDate.clear();
		screenVars.rerateDateDisp.clear();
		screenVars.rerateDate.clear();
		screenVars.rerateFromDateDisp.clear();
		screenVars.rerateFromDate.clear();
		screenVars.benBillDateDisp.clear();
		screenVars.benBillDate.clear();
		screenVars.mortcls.clear();
		screenVars.optextind.clear();
		screenVars.unitStatementDateDisp.clear();
		screenVars.unitStatementDate.clear();
		screenVars.bonusInd.clear();
		screenVars.pbind.clear();
		screenVars.payflag.clear();
		screenVars.optsmode.clear();
		screenVars.bappmeth.clear();
		screenVars.zdesc.clear();
		screenVars.zlinstprem.clear();
		screenVars.taxamt.clear();
		screenVars.taxind.clear();
		/*BRD-306 START*/
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		screenVars.select.clear();
		screenVars.riskCessAge.clear();
		screenVars.riskCessTerm.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		/*BRD-306 END*/
		screenVars.prmbasis.clear();/*ILIFE-3421*/
		screenVars.benCessAge.clear();
		screenVars.benCessTerm.clear();
		screenVars.benCessDateDisp.clear();
		screenVars.benCessDate.clear();
		//ILIFE-3421-ENDS
		screenVars.zsredtrm.clear();/*ILIFE-3685*/
		screenVars.dialdownoption.clear(); //BRD-NBP-011
		screenVars.exclind.clear();
		/*ILIFE-6968 start*/
		screenVars.lnkgno.clear();
		screenVars.lnkgsubrefno.clear();
		/*ILIFE-6968 start*/
		screenVars.tpdtype.clear();//ILIFE-7118
		screenVars.lnkgind.clear();

		screenVars.aepaydet.clear();

		screenVars.zstpduty01.clear();	//ILIFE-7746
		screenVars.trcode.clear();
		screenVars.trcdedesc.clear();
		screenVars.dateeff.clear();
		screenVars.covrprpse.clear();

	}
}
