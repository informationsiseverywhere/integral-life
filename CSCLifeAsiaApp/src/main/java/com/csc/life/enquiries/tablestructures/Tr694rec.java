package com.csc.life.enquiries.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:23
 * Description:
 * Copybook name: TR694REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr694rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr694Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData sels = new FixedLengthStringData(2).isAPartOf(tr694Rec, 0);
  	public FixedLengthStringData[] sel = FLSArrayPartOfStructure(2, 1, sels, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(sels, 0, FILLER_REDEFINE);
  	public FixedLengthStringData sel01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData sel02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(498).isAPartOf(tr694Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr694Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr694Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}