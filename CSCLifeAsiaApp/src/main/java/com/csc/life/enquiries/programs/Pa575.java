/*
 * File: Pr5b3.java
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.general.dataaccess.dao.DcdbpfDAO;
import com.csc.fsu.general.dataaccess.model.Dcdbpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.enquiries.screens.Sa575ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Itmmsmtkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pa575 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA575");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(13);
	private FixedLengthStringData wsaaKeyTabl = new FixedLengthStringData(5).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaKeyItem = new FixedLengthStringData(8).isAPartOf(wsaaKey, 5);
	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Itmmsmtkey wskyItmmkey = new Itmmsmtkey();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private ClntpfDAO clntpfDAO =  getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf desc = new Descpf();
	private Clntpf clntpf = new Clntpf();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sa575ScreenVars sv = getLScreenVars();
	private Clrfpf clrfpf =  new Clrfpf();
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private List<Clrfpf> clrfpfList = null;
	private List<Clntpf> clntpfList = null;
	List<Ptrnpf> ptrnList = null;
	List<Dcdbpf> dcdbList = null;
	private DcdbpfDAO dcdbpfDAO = getApplicationContext().getBean("dcdbpfDAO", DcdbpfDAO.class);
	private Map<String, Descpf> t1688DescMap = null;
	
	protected Sa575ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars( Sa575ScreenVars.class);
	}
	
	public Pa575() {
		super();
		screenVars = sv;
		new ScreenModel("Sa575", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010(){

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		
		scrnparams.function.set(varcom.sclr);
		processScreen("Sa575", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		if(t1688DescMap == null){
			t1688DescMap = descDAO.getItems("IT", wsspcomn.company.toString(), "T1688", wsspcomn.language.toString());
		}
		setPayerName();
		readSubfileRecords1100();
}
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void readSubfileRecords1100(){
	sub1.set(0);
	
	dcdbList = dcdbpfDAO.readDcdbDcdbPf(sv.payer.toString(),wsspcomn.chdrChdrnum.toString(),wsspcomn.company.toString());
	while (isLT(sub1,dcdbList.size())){
		subfileread();
	}
}

private void subfileread() {
		getDcbdDetails();
		/*    Write the subfile record.*/
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	
}

protected void preScreenEdit()
{
		preStart();
}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		scrnparams.subfileRrn.set(1);
		if (isNE(wsspcomn.programPtr,ZERO)){
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}
protected void whereNext4000()
{
	nextProgram4100();
	srnch4110();
	
}
protected void nextProgram4100()
{
	wsspcomn.nextprog.set(wsaaProg);
//	wsspcomn.programPtr.add(1);
	if (isEQ(wsaaFunctionKey, varcom.calc)) {
		return;
	}
}

protected void srnch4110()
{
	scrnparams.function.set(varcom.srnch);
	processScreen("Sa575", sv);
	wsspcomn.confirmationKey.set(SPACES);
	wsaaKeyItem.set(SPACES);
	wskyItmmkey.itmmsmtItemitem.set(SPACES);
	wsspsmart.itemkey.set(SPACES);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}

	if (isNE(scrnparams.statuz, varcom.endp)) {
		
		wsaaKeyItem.set(sv.transcode);
		wsspcomn.confirmationKey.set(wsaaKey);
		wskyItmmkey.itmmsmtItemitem.set(sv.transcode);
		wsspsmart.itemkey.set(wskyItmmkey);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		//sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("Sa575", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(wsaaProg);
		}
		
		else {
			wsspcomn.programPtr.add(1);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		}
	}

return;
}
protected void exit4120()
{
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
	/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/
}
protected void readSubfile4100(){
	/*READ*/
	scrnparams.function.set(varcom.srdn);
	processScreen("Sa575", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*EXIT*/
}
protected void setPayerName(){
	/* Read the Client Role file CLRF logical to find details of    */
	/* the Payer.                                                   */
	clrfpf.setForepfx("CH");
	clrfpf.setForecoy(wsspcomn.company.toString());
	wsaaChdrnum.set(wsspcomn.chdrChdrnum.toString());
	wsaaPayrseqno.set("1");
	clrfpf.setForenum(wsaaForenum.toString());
	clrfpf.setClrrrole("PY");
	clrfpfList = clrfpfDAO.readClrfpfData(clrfpf);
	if(clrfpfList==null || (clrfpfList !=null && clrfpfList.size()==0))
	{
		sv.payer.set(SPACES);
		return;
	}
	else {
		clrfpf = clrfpfList.get(0);
		sv.payer.set(clrfpf.getClntnum());

		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.payer.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || (clntpfList !=null && clntpfList.size()==0))
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname();
		sv.payername.set(wsspcomn.longconfname);
	}

}
protected void addSubfile(){
	scrnparams.function.set(varcom.sadd);
	processScreen("Sa575", sv);
}
protected void getTransactionDesc(String batctrde){
	
	if(t1688DescMap==null || !t1688DescMap.containsKey(batctrde)){
		sv.trcdedesc.fill("?");
	}else{
		 desc = t1688DescMap.get(batctrde);
		sv.trcdedesc.set(desc.getLongdesc());
	}
}
protected void getDcbdDetails(){

	for(Dcdbpf dcdbpf : dcdbList){
		sv.dbtcdtdesc.set(dcdbpf.getType());
		sv.dbtcdtdate.set(dcdbpf.getDbdcdate());
		sv.amnt.set(dcdbpf.getAmount());
		sv.bankcode.set(dcdbpf.getBankkey());
		sv.aacct.set(dcdbpf.getBankacckey());
		sv.transcode.set(dcdbpf.getBatctrcde());
		sv.trdate.set(dcdbpf.getPtrneff());
		sv.statdesc.set(dcdbpf.getStatus().trim());
		getTransactionDesc(dcdbpf.getBatctrcde());/* IJTI-1523 */
		addSubfile();
		sub1.add(1);
	}
}
}
