package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6233screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {23, 23, 4, 4}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6233ScreenVars sv = (S6233ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6233screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6233ScreenVars screenVars = (S6233ScreenVars)pv;
		// ILB-326:Starts
		screenVars.trannosearch.setClassString("");
		screenVars.datesubsearch.setClassString("");
		screenVars.effdatesearch.setClassString("");
		screenVars.trcodesearch.setClassString("");
		screenVars.trandescsearch.setClassString("");
		screenVars.crtusersearch.setClassString("");
		screenVars.datesubsearchDisp.setClassString("");
		screenVars.effdatesearchDisp.setClassString("");
		// ILB-326:Ends
	}

/**
 * Clear all the variables in S6233screen
 */
	public static void clear(VarModel pv) {
		S6233ScreenVars screenVars = (S6233ScreenVars) pv;
		// ILB-326:Starts
		screenVars.trannosearch.clear();
		screenVars.datesubsearch.clear();
		screenVars.effdatesearch.clear();
		screenVars.trcodesearch.clear();
		screenVars.trandescsearch.clear();
		screenVars.crtusersearch.clear();
		screenVars.datesubsearchDisp.clear();
		screenVars.effdatesearchDisp.clear();
		// ILB-326:Ends
	}
}
