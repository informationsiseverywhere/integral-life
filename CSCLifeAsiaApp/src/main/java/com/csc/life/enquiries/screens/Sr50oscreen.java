package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50oscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50oScreenVars sv = (Sr50oScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50oscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50oScreenVars screenVars = (Sr50oScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.singlePremium.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.lifedesc.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtabled.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.rcdateDisp.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.premcessDisp.setClassString("");
		screenVars.rerateDateDisp.setClassString("");
		screenVars.rerateFromDateDisp.setClassString("");
		screenVars.annivProcDateDisp.setClassString("");
		screenVars.optdsc01.setClassString("");
		screenVars.optind01.setClassString("");
		screenVars.optdsc02.setClassString("");
		screenVars.optind02.setClassString("");
		screenVars.optdsc03.setClassString("");
		screenVars.optind03.setClassString("");
		screenVars.optdsc04.setClassString("");
		screenVars.optind04.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifedesc.setClassString("");
		screenVars.taxamt.setClassString("");
		/*BRD-306 START*/
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		screenVars.riskCessAge.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.liencd.clear();
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
	
		screenVars.waitperiod.setClassString("");
		screenVars.bentrm.setClassString("");
		screenVars.poltyp.setClassString("");
		screenVars.prmbasis.setClassString("");
		screenVars.benCessAge.setClassString("");
		screenVars.benCessTerm.setClassString("");
		screenVars.benCessDateDisp.setClassString("");
		screenVars.benCessDate.setClassString("");
		/*BRD-306 END*/
		screenVars.statcode.setClassString("");//BRD-009
		screenVars.zstpduty01.setClassString("");//ILIFE-3509
		screenVars.dialdownoption.setClassString("");//BRD-NBP-011
	}

/**
 * Clear all the variables in Sr50oscreen
 */
	public static void clear(VarModel pv) {
		Sr50oScreenVars screenVars = (Sr50oScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.chdrstatus.clear();
		screenVars.lifenum.clear();
		screenVars.life.clear();
		screenVars.crtable.clear();
		screenVars.billfreq.clear();
		screenVars.singlePremium.clear();
		screenVars.instPrem.clear();
		screenVars.zlinstprem.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.premstatus.clear();
		screenVars.lifedesc.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.crtabled.clear();
		screenVars.mop.clear();
		screenVars.mortcls.clear();
		screenVars.rcdateDisp.clear();
		screenVars.rcdate.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.premcessDisp.clear();
		screenVars.premcess.clear();
		screenVars.rerateDateDisp.clear();
		screenVars.rerateDate.clear();
		screenVars.rerateFromDateDisp.clear();
		screenVars.rerateFromDate.clear();
		screenVars.annivProcDateDisp.clear();
		screenVars.annivProcDate.clear();
		screenVars.optdsc01.clear();
		screenVars.optind01.clear();
		screenVars.optdsc02.clear();
		screenVars.optind02.clear();
		screenVars.optdsc03.clear();
		screenVars.optind03.clear();
		screenVars.optdsc04.clear();
		screenVars.optind04.clear();
		screenVars.sumin.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.jlife.clear();
		screenVars.jlifedesc.clear();
		screenVars.taxamt.clear();
		/*BRD-306 START*/
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		screenVars.riskCessAge.clear();
		screenVars.riskCessTerm.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		screenVars.liencd.clear();;
		screenVars.zagelit.clear();;
		screenVars.anbAtCcd.clear();;
		
		screenVars.waitperiod.clear();
		screenVars.bentrm.clear();
		screenVars.poltyp.clear();
		screenVars.prmbasis.clear();
		screenVars.benCessAge.clear();
		screenVars.benCessTerm.clear();
		screenVars.benCessDateDisp.clear();
		screenVars.benCessDate.clear();
		/*BRD-306 END*/
		screenVars.statcode.clear();//BRD-009
		screenVars.zstpduty01.clear();//ILIFE-3509
		screenVars.dialdownoption.clear(); //BRD-NBP-011
	}
}
