package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr592screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 11, 3, 55}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr592ScreenVars sv = (Sr592ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr592screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr592ScreenVars screenVars = (Sr592ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.windowRow.setClassString("");
		screenVars.windowColumn.setClassString("");
		screenVars.userid.setClassString("");
		screenVars.company.setClassString("");
		screenVars.uwlevel.setClassString("");
		screenVars.uwdecision.setClassString("");
		screenVars.skipautouw.setClassString("");
	}

/**
 * Clear all the variables in Sr592screen
 */
	public static void clear(VarModel pv) {
		Sr592ScreenVars screenVars = (Sr592ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.windowRow.clear();
		screenVars.windowColumn.clear();
		screenVars.userid.clear();
		screenVars.company.clear();
		screenVars.uwlevel.clear();
		screenVars.uwdecision.clear();
		screenVars.skipautouw.clear();
	}
}
