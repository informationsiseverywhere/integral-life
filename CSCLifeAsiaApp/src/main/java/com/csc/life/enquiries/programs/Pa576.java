/*
 * File: Pa576.java
 * Date: 30 August 2009 0:22:12
 * Author: Quipoz Limited
 * 
 * Class transformed from Pa576.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.screens.Sa576ScreenVars;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pa576 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA576");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Batckey wsaaBatchkey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Subprogrec subprogrec = new Subprogrec();
	private Sa576ScreenVars sv = ScreenProgram.getScreenVars( Sa576ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	boolean cscom004Permission = false;
	//ICIL-14 Start
	boolean isLoanConfig = false;
	//ICIL-14 End
	boolean csulk005Permission = false;
	private static final String f399 = "F399";
	private static final String f907 = "F907";
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
/**
 * Contains all possible labels used by goTo action.
 */

	public Pa576() {
		super();
		screenVars = sv;
		new ScreenModel("Sa576", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
	
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
	
	
		
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
	
	wsspcomn.edterror.set(varcom.oK);
	sv.errorIndicators.set(SPACES);
	/*VALIDATE*/
		if(isEQ(sv.facthous,SPACES))  
		{
			//throw error
			sv.facthousErr.set(f399);
		}
		else if(isEQ(sv.effdate,varcom.vrcmMaxDate))
		{
			sv.effdateErr.set(f399);
		}
		else
		{
		validateFhousecode();
		}
	
		
		checkAgainstTable2110();
		
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void updateWssp3010()
{
	wsspcomn.sbmaction.set(scrnparams.action);
	wsaaBatchkey.set(wsspcomn.batchkey);
	wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
	wsspcomn.batchkey.set(wsaaBatchkey);
	wsspcomn.submenu.set(wsaaProg);
	if (isEQ(scrnparams.statuz, "BACH")) {
		exit();
	}
	wsspcomn.secProg[1].set(subprogrec.nxt1prog);
	wsspcomn.secProg[2].set(subprogrec.nxt2prog);
	wsspcomn.secProg[3].set(subprogrec.nxt3prog);
	wsspcomn.secProg[4].set(subprogrec.nxt4prog);
	/*  Update WSSP Key details*/
}

protected void exit()
{

}

protected void validateFhousecode()
	{
		Itempf itempf = itemDAO.findItemByItem("9", "T3684", sv.facthous.toString());
		
		if(null == itempf)
		{
			//throw error
			sv.facthousErr.set(f907);
		} 
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		keeps3070();
		updateWssp3010();
	}

protected void checkAgainstTable2110()
{
	subprogrec.action.set(scrnparams.action);
	subprogrec.company.set(wsspcomn.company);
	subprogrec.progCode.set(wsaaProg);
	callProgram(Subprog.class, subprogrec.subprogRec);
	if (isNE(subprogrec.statuz, varcom.oK)) {
		sv.actionErr.set(subprogrec.statuz);
		exit();
	}
}

protected void keeps3070()
{
	wsspcomn.bankcode.set(sv.facthous);
	wsspcomn.currfrom.set(sv.effdate);
}

protected void exit1020()
{
	
}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
}
}
