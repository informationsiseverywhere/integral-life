/*

 * File: pr59a.java
 * Date: 25 August 2016 0:45:00
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr59a.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;


import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //ILIFE-8304
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
//import com.csc.life.enquiries.screens.S6353ScreenVars;
import com.csc.life.enquiries.screens.Sr59aScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.Tr58Zrec;
import com.csc.life.productdefinition.tablestructures.Tr58yrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf; //ILIFE-8304
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision; //ILIFE-8304



/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Version #2 - Incorporating Plan Processing.
* This program is the main screen for the contract enquiry.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*     The details of the contract being enqired upon will be stored
*     in  the  CHDR I/O module.  Retrieve the details, then release
*     the  CHDR I/O module with a RLSE function and perform a READS
*     on  CHDRENQ so that the appropriate details are held for this
*     sub-system.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist.
*
*
*          The agent (AGNT) to get the client number, and hence the
*          client (agent's) name.
*
*
* Validation
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*          If  the  'KILL'  function  key  was pressed skip all the
*          validation.
*
*     Validation  is  only  required  against the indicators at the
*     foot  of  the  screen. The only valid entries in those are an
*     'X'.
*
*     If  the  Plan Level Component indicator has been selected and
*     the number of policies in plan, (from the Contract Header) is
*     1 then highlight the indicator as being in error.
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Check  each  'selection'  indicator in turn.  If an option is
*     selected,  call  GENSSWCH  with  the  appropriate  action  to
*     retrieve the optional program switching:
*
*               A - Outstanding Future transactions
*               B - Plan Components
*               C - Policy Components
*               D - Roles
*               E - Sub-Account Balances
*               F - Transactions History
*               G - Agents Details
*               H - Extra Header Details
*
*     For the first one selected, save the next set of programs (8)
*     from  the  program  stack  and flag the current position with
*     '*'.
*
*     If  a  selection  had  been made previously, its select field
*     would contain a '?'. Replace this '?' with a '+'.
*
*     If  a selection is made (selection field X), load the program
*     stack with the programs returned by GENSSWCH, replace the 'X'
*     in  the  selection field with a '?', add 1 to the pointer and
*     exit.
*
*     If   Plan   Components   has   been   selected  move  'L'  to
*     WSSP-PLAN-POLICY, if Policy Components has been selected move
*     'O'   to  WSSP-PLAN-POLICY.  For  all  other  selections  set
*     WSSP-PLAN-POLICY to space.
*
*     Once all the selections have been serviced, re-load the saved
*     programs  onto  the  stack,  and blank out the '*'. Leave the
*     program  pointer  at  the current position (to re-display the
*     screen).
*
*     If  nothing was selected blank out the stack action field and
*     leave the program pointer at the current position.
*
*
*
* Notes.
* ------
*
*
*     Create  a  new  view of CHDRPF called CHDRENQ which uses only
*     those  fields  required  for this program P6353 and the extra
*     contract details program - P6354.
*
*     Table Used:
*
* T1675 - Generalised Secondary Switching   Key: Transaction Code
*                                              + Program Number
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
 *
 *****************************************************************
 * </pre>
 */
public class Pr59a extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pr59a.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR59A");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaWsspChdrky = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaWsspChdrpfx = new FixedLengthStringData(2).isAPartOf(wsaaWsspChdrky, 0);
	private FixedLengthStringData wsaaWsspChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaWsspChdrky, 2);
	private FixedLengthStringData wsaaWsspChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaWsspChdrky, 3);

	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

	/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private static final String zswrrec = "ZSWRREC";
	private static final String unltunlrec = "UNLTUNLREC";
	/* WSAA-LAST-CRRCDS */
	private static final String h055 = "H055";
	private static final String et50 = "ET50";
	private static final String et71 = "ET71";
	private static final String e186 = "E186";

	/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	// private S6353ScreenVars sv = ScreenProgram.getScreenVars(
	// S6353ScreenVars.class);
	private Sr59aScreenVars sv = ScreenProgram.getScreenVars(Sr59aScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private FixedLengthStringData wsaaModifyFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(6, 0).init(ZERO);// MTL-114
	private T5540rec t5540rec = new T5540rec();
	private static final String t5540 = "T5540";
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	Boolean afrAvble = false;
	private FixedLengthStringData afr1 = new FixedLengthStringData(4).init("E186");
	private UlnkTableDAM UlnkIO = new UlnkTableDAM();
	ArrayList<String> wsaaArrfund = new ArrayList<String>();
	ArrayList<Integer> wsaaArrPerg = new ArrayList<Integer>();
	private ZswrpfDAO zswrDAO = getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);
	private FixedLengthStringData wsaazafritem = new FixedLengthStringData(15);
	private FixedLengthStringData wsaazafropt1 = new FixedLengthStringData(15);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> tr58ZListMap;
	private Map<String, List<Itempf>> t5540ListMap;
	private Tr58Zrec Tr58zrec = new Tr58Zrec();
	private Tr58yrec  tr58yrec = new Tr58yrec();	 
	private Map<String, List<Itempf>> tr58YListMap;
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private FixedLengthStringData wsaaUnltunlFound = new FixedLengthStringData(1).init(SPACES);
	private String changedInd = "";
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData one = new PackedDecimalData(3, 0).init(1).setUnsigned();
	private static final int two = 2;
	private static final int ten = 10;
	private PackedDecimalData zI = new PackedDecimalData(3, 0).init(0).setUnsigned();
	 
	private Isuallrec isuallrec = new Isuallrec();
	private ZonedDecimalData wsbbSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wssSub = new FixedLengthStringData(20);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, jointOwnerName1030, payerName1040, agentName1050, exit1090, addTax1100, exit1100, exit1110, exit2090, gensww4010, exit4090, exit6190, exit6220
	}

	public Pr59a() {
		super();
		screenVars = sv;
		// new ScreenModel("S6353", AppVars.getInstance(), sv);
		new ScreenModel("Sr59a", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
		 
	}

	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
					ownerName1020();
				case agentName1050:
					branchName1070();
				case exit1090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.chdrnum.set(ZERO);
		sv.ctypedes.set(ZERO);
		sv.zlstupdte.set(ZERO);
		sv.zusrprf.set(ZERO);
		sv.chdrstatus.set(ZERO);
		sv.premstatus.set(ZERO);
		sv.cownnum.set(ZERO);
		sv.ownername.set(ZERO);
		sv.zlastdte.set(ZERO);
		sv.znextdte.set(ZERO);
		sv.currfrom.set(ZERO);
		sv.ptdate.set(ZERO);

		/* Set screen fields */
		/* Read CHDRENQ (RETRV) in order to obtain the contract header */
		/* information. If the number of policies in the plan is zero */
		/* or one then Plan-processing does not apply. If there is any */
		/* other numeric value, this value indicates the number of */
		/* policies in the Plan. */
	
//ILIFE-8304 Start
		
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
	//ILIFE-8304 END	
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setChdrcoy(chdrpf.getChdrcoy()); //ILIFE-8304
		chdrenqIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8304
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			/*syserrrec.statuz.set(chdrmjaIO.getStatuz()); 
			syserrrec.params.set(chdrmjaIO.getParams());*/ //ILIFE-8304
			fatalError600();
		}

		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cownnum.set(chdrenqIO.getCownnum());
		sv.currfrom.set(chdrenqIO.getOccdate());

		/* Read the PAYR file for billing information. */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		payrIO.setChdrnum(chdrenqIO.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.ptdate.set(payrIO.getPtdate());

		if (isEQ(sv.currfrom, ZERO)) {
			sv.currfrom.set(varcom.vrcmMaxDate);
		}

		if (isEQ(sv.ptdate, ZERO)) {
			sv.ptdate.set(varcom.vrcmMaxDate);
		}
		// AFR
		// MPTD-156/411 Start AFR
		List<Zswrpf> zswrpfiList = new ArrayList<>();
		Zswrpf zw = new Zswrpf();
		zw.setChdrpfx("CH");
		zw.setChdrcoy(chdrenqIO.getChdrcoy().toString());
		zw.setChdrnum(chdrenqIO.getChdrnum().toString());
		zw.setValidflag("1");
		zswrpfiList = zswrDAO.getZswrpfData(zw.getChdrnum(), zw.getChdrcoy(),
				zw.getChdrpfx(), zw.getValidflag());//IJTI-1485

		if (zswrpfiList.size() > 0) {
			for (Zswrpf p : zswrpfiList) {

				if (isEQ(p.getChdrcoy(), chdrenqIO.getChdrcoy()) && isEQ(p.getChdrnum(), chdrenqIO.getChdrnum())) {
					sv.zafropt1.set(p.getZafropt());
					sv.zafritem.set(p.getZafritem());
					sv.zafrfreq.set(p.getZafrfreq());
					sv.zafropt1disp.set(p.getZafropt());
					sv.zafritemdisp.set(p.getZafritem());
					sv.zusrprf.set(p.getZlstupdusr());
					sv.zlstupdte.set(p.getZlstupdate());
					sv.zlastdte.set(p.getZlastdte());
					sv.znextdte.set(p.getZnextdte());
					wsaazafritem.set(sv.zafritem);
					wsaazafropt1.set(sv.zafropt1);
				} else {
					sv.zafropt1disp.set("NO");
					sv.zafritemdisp.set("NO");
					sv.zusrprf.set("");
					sv.zafropt1.set("NO");
					sv.zafritem.set("NO");

				}
			}
		}

		
		
		// Getting Coverage for itemitem for T5540
		covrunlIO.setParams(SPACES);
		covrunlIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrunlIO.setChdrnum(chdrenqIO.getChdrnum());
		covrunlIO.setLife("01");
		covrunlIO.setCoverage("01");
		covrunlIO.setRider("00");
		covrunlIO.setPlanSuffix("0");
		covrunlIO.setFunction(varcom.readr);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			fatalError600();
		}

		// AFR
		t5540ListMap = itemDAO.loadSmartTable("IT", chdrenqIO.getChdrcoy().toString(), "T5540");
		String keyItemitem = covrunlIO.getCrtable().toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t5540ListMap.containsKey(keyItemitem.trim())) {
			itempfList = t5540ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {

				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
					if ((Integer.parseInt(chdrenqIO.getOccdate().toString()) >= Integer
							.parseInt(itempf.getItmfrm().toString()))
							&& Integer.parseInt(chdrenqIO.getOccdate().toString()) <= Integer
									.parseInt(itempf.getItmto().toString())) {
						t5540rec.t5540Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound = true;
					}

				} else {
					t5540rec.t5540Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}
		}

		// Get the Todays date in datcon1rec.datcon1Rec
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);

		/* Obtain the Life Assured and Joint Life Assured, if they exist. */
		/* The BEGN function is used to retrieve the first Life for the */
		/* contract in case life '01' has been deleted. */
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		/* IF LIFEENQ-STATUZ NOT = O-K */
		if (isNE(lifeenqIO.getStatuz(), varcom.oK) && isNE(lifeenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			if (isNE(subString(wsspcomn.lastprog, 2, 4), "6222")) {
				syserrrec.params.set(lifeenqIO.getParams());
				fatalError600();
			} else {
				lifelnbIO.setChdrcoy(chdrenqIO.getChdrcoy());
				lifelnbIO.setChdrnum(chdrenqIO.getChdrnum());
				lifelnbIO.setLife("01");
				lifelnbIO.setJlife("00");
				lifelnbIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, lifelnbIO);
				if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lifelnbIO.getParams());
					fatalError600();
				}
				lifeenqIO.setLifcnum(lifelnbIO.getLifcnum());
			}
		}

		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();

		/* Check for the existence of Joint Life details. */
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
		} else {

			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			} else {
				plainname();
			}
		}
	}

	/**
	 * <pre>
	*    Obtain the names of the other lives on the contract header.
	 * </pre>
	 */
	protected void ownerName1020() {
		if (isEQ(sv.cownnum, SPACES)) {
			goTo(GotoLabel.agentName1050);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
	}

	protected void branchName1070() {
		/* Obtain the Branch name from T1692. */

		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrenqIO.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/* MOVE ALL '?' TO S6353-BRCHNAME */
			// sv.brchname.set(SPACES);
		} else {
			// sv.brchname.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Type description from T5688. */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/* MOVE ALL '?' TO S6353-CTYPEDES */
			sv.ctypedes.set(SPACES);
		} else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623. */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/* MOVE ALL '?' TO S6353-CHDRSTATUS */
			sv.chdrstatus.set(SPACES);
		} else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588. */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/* MOVE ALL '?' TO S6353-PREMSTATUS */
			sv.premstatus.set(SPACES);
		} else {
			sv.premstatus.set(descIO.getShortdesc());
		}

	}

	/**
	 * <pre>
	*    Sections performed from the 1000 section above.
	 * </pre>
	 */
	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/* LGNM-EXIT */
	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/* PAYEE-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/* CORP-EXIT */
	}

	/**
	 * <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit() {
		/* PRE-START */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}
		/* 2010-SCREEN-IO. */
		return;
		/* PRE-EXIT */
	}

	protected void screenEdit2000() {
		try {
			screenIo2010();
			validate2020();
			checkForErrors2030();// MPTD-1118
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	 * <pre>
	*2000-PARA.
	 * </pre>
	 */
	protected void screenIo2010() {
		/* CALL 'S6353IO' USING SCRN-SCREEN-PARAMS */
		/* S6353-DATA-AREA. */
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* Screen errors are now handled in the calling program. */
		/* PERFORM 200-SCREEN-ERRORS. */
		wsspcomn.edterror.set(varcom.oK);
	}

	protected void validate2020() {
		/* Validate fields */
		checkAFRTR58Z();
		if (isEQ(wsspcomn.edterror, "Y")) {
			return;
		}
		wsaaModifyFlag.set("N");
		// AFR
		if (isNE(sv.zafritem, wsaazafritem) || isNE(sv.zafropt1, wsaazafropt1)) {
			wsaaModifyFlag.set("Y");
		} else {
			wsaaModifyFlag.set("N");
		}

		if ((isEQ(sv.zafritem, SPACE) || isEQ(sv.zafropt1, SPACE))) {
			sv.zafropt1Err.set(e186);
			wsspcomn.edterror.set("Y");
			return;
		}

		if (isEQ(t5540rec.zafropt1, "AFRN")) {

			sv.zafropt1Err.set(et50);
			wsspcomn.edterror.set("Y");
			return;
		}

	}

	protected void checkAFRTR58Z() {
		// AFR
		
		covrunlIO.setParams(SPACES);
		covrunlIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrunlIO.setChdrnum(chdrenqIO.getChdrnum());
		covrunlIO.setLife("01");
		covrunlIO.setCoverage("01");
		covrunlIO.setRider("00");
		covrunlIO.setPlanSuffix("0");
		covrunlIO.setFunction(varcom.readr);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			fatalError600();
		}
		
		tr58ZListMap = itemDAO.loadSmartTable("IT", chdrenqIO.getChdrcoy().toString(), "TR58Z");
		String keyItemitem = covrunlIO.getCrtable() + sv.zafropt1.toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (tr58ZListMap.containsKey(keyItemitem.trim())) {
			itempfList = tr58ZListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {

				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {

					Tr58zrec.tr58zRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;

				} else {
					Tr58zrec.tr58zRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}
		}

		if (isEQ(sv.zafropt1, "NO") && isNE(sv.zafritem, "NO")) {
			sv.zafritem.set("NO");
		}
		afrAvble = false;

		for (int i = 1; i <= 30; i++) {
			if (Tr58zrec.billfreq[i].toString().equals(sv.zafritem.toString()) && isNE(Tr58zrec.billfreq[i], SPACE)) {
				afrAvble = true;
			}
		}
		if (!afrAvble) {
			sv.zafropt1Err.set(afr1);
			wsspcomn.edterror.set("Y");
			return;
		}
	}

	/*
	 * To catch error and raise. Also to halt the screen after refresh and not
	 * allow to commit/continue the transaction
	 */

	protected void checkForErrors2030() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*
		 * If the refresh key is pressed, Screen validate and show the same
		 * screen
		 */
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	 * <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000() {
		/* PARA */
		/* There is no updating required in this program. */
		wsaaWsspChdrnum.set(sv.chdrnum);
		wsaaWsspChdrcoy.set(chdrenqIO.getChdrcoy());
		wsaaWsspChdrpfx.set(SPACES);
		wssplife.chdrky.set(wsaaWsspChdrky);
		if (isEQ(wsaaModifyFlag, "Y")) {
			updateDatabase3010();
		} else {
			return;
		}
		/* EXIT */
	}

	protected void updateDatabase3010() {
		updtChdrmja7100();
		writePtrn7200();
		rewriteZswrpos7300();
	}

	protected void updtChdrmja7100()

	{

		List<Zswrpf> zswrpfiList = new ArrayList<>();
		Zswrpf zw = new Zswrpf();
		zw.setChdrpfx("CH");
		zw.setChdrcoy(chdrenqIO.getChdrcoy().toString());
		zw.setChdrnum(chdrenqIO.getChdrnum().toString());
		zw.setValidflag("1");
		zswrpfiList = zswrDAO.getZswrpfData(zw.getChdrnum(), zw.getChdrcoy(),
				zw.getChdrpfx(), zw.getValidflag());//IJTI-1485
		if (zswrpfiList.size() > 0) {
			for (Zswrpf p : zswrpfiList) {

				if (isEQ(p.getChdrcoy(), chdrenqIO.getChdrcoy()) && isEQ(p.getChdrnum(), chdrenqIO.getChdrnum())) {
		//ILIFE-8304 --Start
					/*chdrmjaIO.setFunction(varcom.rlse);
					SmartFileCode.execute(appVars, chdrmjaIO);
					if (isNE(chdrmjaIO.getStatuz(), varcom.oK) && isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
						syserrrec.params.set(chdrmjaIO.getParams());
						fatalError600();*/
					//}
					// Readh for hold and ReWrite
					chdrpf = chdrpfDAO.getChdrpf(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());//IJTI-1485
					if(chdrpf == null){
						fatalError600();
					}
					/*chdrpf.setDataKey(SPACES);
					chdrmjaIO.setFormat(formatsInner.chdrmjarec);
					chdrmjaIO.setChdrcoy(p.getChdrcoy());
					chdrmjaIO.setChdrnum(p.getChdrnum());
					chdrmjaIO.setFunction(varcom.readh);
					SmartFileCode.execute(appVars, chdrmjaIO);
					if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(chdrmjaIO.getParams());
						syserrrec.statuz.set(syserrrec.dbioStatuz);
						fatalError600();
					}*/
					rewrt3030();
					/*chdrmjaIO.setValidflag("2");
				chdrmjaIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					syserrrec.statuz.set(syserrrec.dbioStatuz);
					fatalError600();
				}

				chdrmjaIO.setValidflag("1");

				wsaaTranno.set(chdrmjaIO.getTranno());
				compute(wsaaTranno).set(add(wsaaTranno, 1));
				chdrmjaIO.setTranno(wsaaTranno);
				chdrmjaIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					syserrrec.statuz.set(syserrrec.dbioStatuz);
					fatalError600();
				}
					 */
					}

			}
		}

	}
	
	protected void rewrt3030()
	{
		/*    Update CHDRMJA header with TRANNO + 1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
 //ILIFE-8304 start
		chdrpf.setCurrto(wsaaToday.toInt());
		chdrpf.setValidflag('2');
		List<Chdrpf> chdrList = new ArrayList<>();
		chdrList.add(chdrpf);
		chdrpfDAO.updateInvalidChdrRecords(chdrList);
		
		/*chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		chdrpf.setValidflag('1');
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		chdrpf.setCurrfrom(wsaaToday.toInt());
		chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpfDAO.insertChdrRecords(chdrpf);
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction("WRITR");
		 MOVE 'REWRT'                 TO CHDRMJA-FUNCTION.            
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		
 //ILIFE-8304 --End
	}


	protected void writePtrn7200()

	{
		wsaaBatckey.set(wsspcomn.batchkey);
		ptrnIO.setParams(SPACES);
	//ILIFE-8304 --Start
		/*ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());*/
		ptrnIO.setChdrcoy(chdrpf.getChdrcoy());
		ptrnIO.setChdrpfx(chdrpf.getChdrpfx());
		ptrnIO.setChdrnum(chdrpf.getChdrnum());
		ptrnIO.setTranno(chdrpf.getTranno());
	//ILIFE-8304 --End
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setBatcpfx(wsaaBatckey.batcBatcpfx);
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setPrtflg(SPACES);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);

		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

	protected void writeZswr7000() {

		List<Zswrpf> zswrpfiList = new ArrayList<>();
		Zswrpf zw = new Zswrpf();
		zw.setChdrpfx("CH");
		zw.setChdrcoy(chdrenqIO.getChdrcoy().toString());
		zw.setChdrnum(chdrenqIO.getChdrnum().toString());
		zw.setValidflag("1");
		zw.setZwdwlrsn("");
		zw.setZlastdte(varcom.maxdate.toInt());
		zw.setZnextdte(varcom.vrcmMaxDate.toInt());
		zw.setZwdlddte(varcom.vrcmMaxDate.toInt());
		zw.setZenable("");
		zw.setCnntype(chdrenqIO.getCnttype().toString());
		zw.setCrtable(covrunlIO.getCrtable().toString());
		zw.setZchkopt("");
		zw.setZregdte(varcom.vrcmMaxDate.toInt());
		zw.setZenable("E");
		zw.setZafritem(sv.zafritem.toString());
		//zw.setTranno(chdrmjaIO.getTranno().toInt()); 
		zw.setTranno(chdrpf.getTranno());//ILIFE-8304

		if (isEQ(sv.zafropt1, "FR")) {
			zw.setZafrfreq(sv.zafritem.toString());

			zw.setVrtfund("");
		} else if (isEQ(sv.zafropt1, "NO")) {
			zw.setZafrfreq("NO");
			zw.setVrtfund("");
		} else {
			zw.setZafrfreq("01");
			zw.setVrtfund("");
		}
		zw.setZafropt(sv.zafropt1.toString());
		loadFunds(zw);
		if (isEQ(sv.zafropt1,"RK"))
		{
			clearUnitAllocFundAndPerc(zw);
			getFundAllocation(zw);
		}
		if (isNE(sv.zafropt1, "NO")) {
			getNextAfrDate();
			zw.setZnextdte(datcon4rec.intDate2.toInt());
			zw.setZlstupdate(wsaaToday.toInt());
		} else {
			zw.setZnextdte(0);
			zw.setZlstupdate(wsaaToday.toInt());

		}
		zw.setZlstupdusr(wsspcomn.userid.toString());
		zw.setZwdlddte(varcom.maxdate.toInt());
		zw.setZwdwlrsn(" ");
		zw.setUsrprf(varcom.vrcmUser.toString());
		zswrpfiList.add(zw);
		zswrDAO.insertZswrpfListRecord(zswrpfiList);
	}

	protected void rewriteZswrpos7300() {

		List<Zswrpf> zswrpfiList = new ArrayList<>();
		Zswrpf zw = new Zswrpf();
		zw.setChdrpfx("CH");
		zw.setChdrcoy(chdrenqIO.getChdrcoy().toString());
		zw.setChdrnum(chdrenqIO.getChdrnum().toString());
		zw.setValidflag("1");
		zswrpfiList = zswrDAO.getZswrpfData(zw.getChdrnum(), zw.getChdrcoy(),
				zw.getChdrpfx(), zw.getValidflag());//IJTI-1485

		if (zswrpfiList.size() == 0) {
			writeZswr7000();
		} else {
			zswrDAO.deleteZswrpf(zw.getChdrnum(), zw.getChdrcoy(), zw.getChdrpfx());//IJTI-1485
			writeZswr7000();
		}

	}

	protected void getNextAfrDate() {

		// Calculate the no of years b/w RCD and Todays date
		//datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon3rec.intDate1.set(chdrpf.getOccdate()); //ILIFE-8304
		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.frequency.set("01");
		// datcon3rec.function.set("CMDF");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		// Setting the no of years to wsaaFreqFactor
		wsaaFreqFactor.set(ZERO);
		wsaaFreqFactor.set(datcon3rec.freqFactor.toInt());
		// Use the no of Years and RCD to compute the last anniversary date
		initialize(datcon4rec.datcon4Rec);
		
		if (isNE(sv.zafropt1, "NO") || isNE(sv.zafritem,"NO")) 
		{
			 datcon4rec.freqFactor.set(1);
			if (isEQ(sv.zafritem,"01") || isEQ(sv.zafritem,"02") || isEQ(sv.zafritem,"04") || isEQ(sv.zafritem,"12"))
				datcon4rec.frequency.set(sv.zafritem);
			else
				//datcon4rec.frequency.set(chdrmjaIO.billfreq);
				datcon4rec.frequency.set(chdrpf.getBillfreq());//ILIFE-8304
		}
		else
		{
			datcon4rec.freqFactor.set(wsaaFreqFactor.toInt());
			datcon4rec.frequency.set("01");
		}
		
		//datcon4rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon4rec.intDate1.set(chdrpf.getOccdate());//ILIFE-8304
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon4rec.statuz);
			syserrrec.params.set(datcon4rec.datcon4Rec);
			fatalError600();
		}
		LOGGER.info("Output=>", datcon4rec.intDate2);//IJTI-1561
		// Calculate the newt AFR date that is greater than the last Anniversay
		// by the frequency that we choose on the screen.
		while (isGT(wsaaToday, datcon4rec.intDate2)) {

			datcon4rec.freqFactor.set(1);
			// datcon4rec.frequency.set(zswrposIO.getZafrfreq());
			datcon4rec.intDate1.set(datcon4rec.intDate2);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon4rec.statuz);
				syserrrec.params.set(datcon4rec.datcon4Rec);
				fatalError600();
			}
		}

		LOGGER.info("Next AFR Date ", datcon4rec.intDate2);//IJTI-1561

	}

	protected void loadFunds(Zswrpf zw) {
		clearUnitAllocFundAndPerc(zw);
		UlnkIO.setDataKey(SPACES);
		UlnkIO.setCoverage("01");
		UlnkIO.setRider("00");
	//ILIFE-8304 --Start
		/*UlnkIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		UlnkIO.setChdrnum(chdrmjaIO.getChdrnum());*/
		UlnkIO.setChdrcoy(chdrpf.getChdrcoy());
		UlnkIO.setChdrnum(chdrpf.getChdrnum());
		//ILIFE-8304 --End
		UlnkIO.setLife("01");
		UlnkIO.setTranno(ZERO);
		UlnkIO.setFunction(varcom.begn);
		loadfunds5240(zw);
	}

	protected void loadfunds5240(Zswrpf zw) {
		SmartFileCode.execute(appVars, UlnkIO);
		if (isNE(UlnkIO.getStatuz(), varcom.oK) && isNE(UlnkIO.getStatuz(), varcom.mrnf)
				&& isNE(UlnkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(UlnkIO.getParams());
			fatalError600();
		}
		if (isNE(UlnkIO.getChdrnum(), chdrpf.getChdrnum()) && isNE(UlnkIO.getCoverage(), "01")) { //ILIFE-8304
			UlnkIO.setStatuz(varcom.endp);
			return;
		}
		if (isEQ(UlnkIO.getStatuz(), varcom.endp) || isEQ(UlnkIO.getStatuz(), varcom.mrnf)) {
			return;
		}
		if (isNE(UlnkIO.getStatuz(), varcom.endp) || isNE(UlnkIO.getStatuz(), varcom.mrnf)) {
			FixedLengthStringData ualprc = new FixedLengthStringData(15);
			for (int i = 1; i <= 10; i++) {
				if (isNE(UlnkIO.getUalfnd(i), SPACE)) {

					switch (i) {
					case 1:
						zw.setUalfnd01(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc01(ualprc.toInt());
						break;
					case 2:
						zw.setUalfnd02(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc02(ualprc.toInt());
						break;
					case 3:
						zw.setUalfnd03(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc03(ualprc.toInt());
						break;
					case 4:
						zw.setUalfnd04(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc04(ualprc.toInt());
						break;
					case 5:
						zw.setUalfnd05(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc05(ualprc.toInt());
						break;
					case 6:
						zw.setUalfnd06(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc06(ualprc.toInt());
						break;
					case 7:
						zw.setUalfnd07(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc07(ualprc.toInt());
						break;
					case 8:
						zw.setUalfnd08(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc08(ualprc.toInt());
						break;
					case 9:
						zw.setUalfnd09(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc09(ualprc.toInt());
						break;
					case 10:
						zw.setUalfnd10(UlnkIO.getUalfnd(i).toString());
						ualprc.set(UlnkIO.getUalprc(i));
						zw.setUalprc10(ualprc.toInt());
					}
				}
			}

		}
		UlnkIO.setFunction(varcom.nextr);
	}

	/**
	 * <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4020();
				case exit4090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void nextProgram4020() {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

	protected void restoreProgram4100() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void saveProgramStack4200() {
		/* PARA */
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void loadProgramStack4300() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void keepsLetc4400() {

	}
	
public void clearUnitAllocFundAndPerc(Zswrpf zw) {
		
	zw.setUalfnd01("");	 
	zw.setUalprc01(0);
	zw.setUalfnd02("");	 
	zw.setUalprc02(0);
	zw.setUalfnd03("");	 
	zw.setUalprc03(0);
	zw.setUalfnd04("");	 
	zw.setUalprc04(0);
	zw.setUalfnd05("");	 
	zw.setUalprc05(0);
	zw.setUalfnd06("");	 
	zw.setUalprc06(0);
	zw.setUalfnd07("");	 
	zw.setUalprc07(0);
	zw.setUalfnd08("");	 
	zw.setUalprc08(0);
	zw.setUalfnd09("");	 
	zw.setUalprc09(0);
	zw.setUalfnd10("");	 
	zw.setUalprc10(0);		
	}

protected void getFundAllocation(Zswrpf zw)
{
if (isNE(sv.zafropt1,"NO")){
	setAFRRiskBase(zw);
	writeUnlt(zw);
	UnitAlloc();
	if(isEQ(wsspcomn.edterror,"Y")){
			return;}
}
}

protected void unltunl1300()
{
	/*    Read UNLTUNL to see if Details already exist*/

	unltunlIO.setParams(SPACES);
	unltunlIO.setDataKey(covrunlIO.getDataKey());
	unltunlIO.setFunction("READR");
	unltunlIO.setFormat(unltunlrec);
	SmartFileCode.execute(appVars, unltunlIO);
	/*    Record exists then use dets*/
	/*    or No record so use 'blank' screen.*/
	if (isEQ(unltunlIO.getStatuz(), "MRNF")
	|| isEQ(unltunlIO.getStatuz(), varcom.oK)) {
		/*NEXT_SENTENCE*/
	}
	else {
		 
	}
	if (isEQ(unltunlIO.getStatuz(), "MRNF")) 
	{
		wsaaUnltunlFound.set("N");		 
	}
	else
	{
		wsaaUnltunlFound.set("Y");
	}
	 
}
 
protected void writeUnlt(Zswrpf zw)
{
	unltunl1300();
	if ( isEQ(wsaaUnltunlFound,"N")){
			write6020(zw);
	}
	else	
		rewrite6041(zw);
}
protected void write6020(Zswrpf zw)
{
	/*WRITE*/
	newUnltunl6060();
	unltunlIO.setSeqnbr(1); 
	move6050(zw);
	unltunlIO.setPercOrAmntInd("P");
	unltunlIO.setFunction(varcom.writr);
	/* MOVE 'UNLTUNLREC'           TO UNLTUNL-FORMAT.               */
	accessUnltunl6080();
	/*EXIT*/
}
protected void newUnltunl6060()
{
	newUnltunl6061();
}

protected void newUnltunl6061()
{
	unltunlIO.setDataArea(SPACES);
	unltunlIO.setGenDate(varcom.vrcmDate);
	unltunlIO.setGenTime(varcom.vrcmTime);
	unltunlIO.setVn(wsspcomn.version);
	unltunlIO.setIo("UNLTUNLIO");
	/* MOVE 'UNLTUNLREC'           TO UNLTUNL-FORMAT.               */
	unltunlIO.setDataKey(covrunlIO.getDataKey());
	unltunlIO.setCurrto(varcom.vrcmMaxDate);
	unltunlIO.setCurrfrom(covrunlIO.getCurrfrom() );
	for (x.set(one); !(isGT(x, ten)); x.add(one)){
		zeroise6070();
	}
	/*  MOVE ZEROS                  TO UNLTUNL-TRANNO.               */
	unltunlIO.setTranno(chdrenqIO.getTranno());
	unltunlIO.setNumapp(1);
}
protected void rewrite6041(Zswrpf zw)
{
	unltunlIO.setFunction(varcom.readh);
	accessUnltunl6080();
	if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
		return ;
	}
	if (isEQ(wsaaModifyFlag, "Y")) {
		unltunlIO.setUalfnds(SPACES);
		for (x.set(one); !(isGT(x, ten)); x.add(one)){
			zeroise6070();
		}
	}
	unltunlIO.setPercOrAmntInd("P");
	for (zI.set(one); !(isGT(zI, ten)); zI.add(one)){
		zeroise6090();
	}
	 
	move6050(zw);
	unltunlIO.setSeqnbr(1);
	unltunlIO.setNumapp(1);
	unltunlIO.setCurrfrom(covrunlIO.getCurrfrom());
	unltunlIO.setFunction(varcom.rewrt);
	accessUnltunl6080();
}

protected void zeroise6070()
{
	/*ZEROISE*/
	unltunlIO.setUalprc(x, ZERO);
	unltunlIO.setUspcpr(x, ZERO);
	/*EXIT*/
}

protected void zeroise6090()
{
	/*ZEROISE-PAR*/
	unltunlIO.setUalprc(zI, ZERO);
	unltunlIO.setUspcpr(zI, ZERO);
	unltunlIO.setUalfnd(zI, SPACES);
	/*EXIT*/
}
protected void accessUnltunl6080()
{
	/*ACCESS-UNLTUNL*/
	unltunlIO.setStatuz(SPACES);
	unltunlIO.setFormat(unltunlrec);
	SmartFileCode.execute(appVars, unltunlIO);
	if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(unltunlIO.getParams());
		fatalError600();
	}
	/*EXIT*/
}

protected void move6050(Zswrpf zw)
{
	/*MOVE*/
	/* MOVE S5417-UNIT-VIRTUAL-FUND   (X)  TO UNLTUNL-UALFND (X).   */
	/* MOVE S5417-UNIT-ALLOC-PERC-AMT (X)  TO UNLTUNL-UALPRC (X)   .*/
	/* MOVE S5417-UNIT-BID-PRICE      (X)  TO UNLTUNL-USPCPR (X).   */
	unltunlIO.setUalfnd(1,  zw.getUalfnd01());
	unltunlIO.setUalprc(1, zw.getUalprc01());
	unltunlIO.setUspcpr(1, 0.00);
	
	unltunlIO.setUalfnd(2,  zw.getUalfnd02());
	unltunlIO.setUalprc(2, zw.getUalprc02());
	unltunlIO.setUspcpr(2, 0.00);
	
	unltunlIO.setUalfnd(3,  zw.getUalfnd03());
	unltunlIO.setUalprc(3, zw.getUalprc03());
	unltunlIO.setUspcpr(3, 0.00);
	
	unltunlIO.setUalfnd(4,  zw.getUalfnd04());
	unltunlIO.setUalprc(4, zw.getUalprc04());
	unltunlIO.setUspcpr(4, 0.00);
	
	unltunlIO.setUalfnd(5,  zw.getUalfnd05());
	unltunlIO.setUalprc(5, zw.getUalprc05());
	unltunlIO.setUspcpr(5, 0.00);
	
	unltunlIO.setUalfnd(6,  zw.getUalfnd06());
	unltunlIO.setUalprc(6, zw.getUalprc06());
	unltunlIO.setUspcpr(6, 0.00);
	
	unltunlIO.setUalfnd(7,  zw.getUalfnd07());
	unltunlIO.setUalprc(7, zw.getUalprc07());
	unltunlIO.setUspcpr(7, 0.00);
	
	unltunlIO.setUalfnd(8,  zw.getUalfnd08());
	unltunlIO.setUalprc(8, zw.getUalprc08());
	unltunlIO.setUspcpr(8, 0.00);
	
	unltunlIO.setUalfnd(9,  zw.getUalfnd09());
	unltunlIO.setUalprc(9, zw.getUalprc09());
	unltunlIO.setUspcpr(9, 0.00);
	
	unltunlIO.setUalfnd(10,  zw.getUalfnd10());
	unltunlIO.setUalprc(10, zw.getUalprc10());
	unltunlIO.setUspcpr(10, 0.00);
	
	
	/*EXIT*/
}
protected void setAFRRiskBase(Zswrpf zw){
	
	//AFR
	if(isEQ(sv.zafropt1,"RK")){
		readTr58y();
		
		if(isNE( tr58yrec.tr58yrec ,SPACES) ){
		 
			
				int age = zw.getAge();
				
				if (age==0)				 
				{
					if (isGT(covrunlIO.getAnbAtCcd(),ZERO))
						age=covrunlIO.getAnbAtCcd().toInt();
				}
				
				for(int i =0 ; i < 9; i++){
					if(tr58yrec.ageIssageFrm[i+1].toInt()<=age && tr58yrec.ageIssageTo[i+1].toInt()>=age){
						for (int j = 1; j<=5; j++){
								switch (j) {
								case 1:
									zw.setUalfnd01(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc01(tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 2:
									zw.setUalfnd02(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc02( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 3:
									zw.setUalfnd03(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc03( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 4:
									zw.setUalfnd04(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc04( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 5:
									zw.setUalfnd05(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc05( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 6:
									zw.setUalfnd06(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc06( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 7:
									zw.setUalfnd07(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc07( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 8:
									zw.setUalfnd08(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc08( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 9:
									zw.setUalfnd09(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc09( tr58yrec.unitPremPercent[(i*5)+j].toLong());
									break;
								case 10:
									zw.setUalfnd10(tr58yrec.unitVirtualFund[(i*5)+j].toString());									 
									zw.setUalprc10( tr58yrec.unitPremPercent[(i*5)+j].toLong());
								}
						}
					}
				}
		} 
	}
}

protected void UnitAlloc()
{
	wssSub.set("ZRULISS");
	isuallrec.company.set(chdrenqIO.getChdrcoy());
	isuallrec.chdrnum.set(chdrenqIO.getChdrnum());
	isuallrec.life.set(covrunlIO.getLife());
	//wsaaLifeNum.set(covrlnbIO.getLife());
	isuallrec.coverage.set(covrunlIO.getCoverage());
	//wsaaCoverageNum.set(covrunlIO.getCoverage());
	isuallrec.rider.set(covrunlIO.getRider());
	isuallrec.planSuffix.set(covrunlIO.getPlanSuffix());
	//wsbbSub.set(covrlnbIO.getPayrseqno());
	isuallrec.freqFactor.set(1);
	isuallrec.batchkey.set("T642");
	isuallrec.batctrcde.set("T642");
	isuallrec.transactionDate.set(wsaaToday);
	isuallrec.transactionTime.set(wsaaToday);
	isuallrec.user.set(wsspcomn.userid);
	//isuallrec.termid.set(wsspcomn.ter);
	isuallrec.convertUnlt.set("Y");
	isuallrec.covrInstprem.set(covrunlIO.getInstprem());
	isuallrec.covrSingp.set(covrunlIO.getSingp());
	isuallrec.effdate.set(covrunlIO.getCrrcd());
	/*    This new field, ISUA-NEW-TRANNO, is NOT used for issue.      */
	/*     just initialise to ZEROS. It is only used by the            */
	/*     subroutine GRVULCHG when trying to Reverse INCI records     */
	/*     affected by a Component Modify.                             */
	isuallrec.newTranno.set(ZERO);
	isuallrec.function.set(SPACES);
	isuallrec.oldcovr.set(SPACES);
	isuallrec.oldrider.set(SPACES);
	/*  Pass the Language field to the Issue Subroutine.            */
	isuallrec.language.set("E");
	isuallrec.runDate.set(covrunlIO.getCrrcd());
	
	chdrlnbIO.setDataArea(SPACES);
	chdrlnbIO.setChdrcoy(chdrenqIO.getChdrcoy());
	chdrlnbIO.setChdrnum(chdrenqIO.getChdrnum());
	chdrlnbIO.setFunction(varcom.reads);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		syserrrec.statuz.set(chdrlnbIO.getStatuz());
		fatalError600();
	}
	
	chdrlnbIO.setFunction(varcom.keeps);
	chdrlnbIO.setFormat(chdrlnbrec);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		syserrrec.statuz.set(chdrlnbIO.getStatuz());
		fatalError600();
	}
	isuallrec.statuz.set(varcom.oK);
	callProgram(wssSub, isuallrec.isuallRec);
	if (isNE(isuallrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(isuallrec.statuz);
		syserrrec.params.set(isuallrec.isuallRec);
		fatalError600();
	}
}

protected void readTr58y()
{
	tr58YListMap = itemDAO.loadSmartTable("IT", chdrpf.getChdrcoy().toString(), "TR58Y"); //ILIFE-8304
	String keyItemitem = sv.zafritem.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr58YListMap.containsKey(keyItemitem.trim())){	
		itempfList = tr58YListMap.get(keyItemitem.trim());
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				 
				tr58yrec.tr58yrec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				 
			}else{
				tr58yrec.tr58yrec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	 
	
}
/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
		private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
		private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
		private FixedLengthStringData letcutirec = new FixedLengthStringData(10).init("LETCUTIREC");
		private FixedLengthStringData letcrec = new FixedLengthStringData(10).init("LETCREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData taxdbilrec = new FixedLengthStringData(10).init("TAXDBILREC");
		private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
		private FixedLengthStringData dcxccntrec = new FixedLengthStringData(10).init("DCXCCNTREC");
		private FixedLengthStringData rldgrec = new FixedLengthStringData(10).init("RLDGREC");
		private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
		private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
		private FixedLengthStringData incrrefrec = new FixedLengthStringData(10).init("INCRREFREC");
		private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
		private FixedLengthStringData zswrrec = new FixedLengthStringData(10).init("ZSWRREC");
		private FixedLengthStringData zswrposrec = new FixedLengthStringData(10).init("ZSWRPOSREC");
		private FixedLengthStringData zafarec = new FixedLengthStringData(10).init("ZAFAREC");
		private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	}
}
