package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6238screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 8;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 15, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6238ScreenVars sv = (S6238ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6238screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6238screensfl, 
			sv.S6238screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6238ScreenVars sv = (S6238ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6238screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6238ScreenVars sv = (S6238ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6238screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6238screensflWritten.gt(0))
		{
			sv.s6238screensfl.setCurrentIndex(0);
			sv.S6238screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6238ScreenVars sv = (S6238ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6238screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6238ScreenVars screenVars = (S6238ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.chdrelem.setFieldName("chdrelem");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.annprem.setFieldName("annprem");
				screenVars.initcom.setFieldName("initcom");
				screenVars.comern.setFieldName("comern");
				screenVars.compay.setFieldName("compay");
				screenVars.dormflag.setFieldName("dormflag");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.chdrelem.set(dm.getField("chdrelem"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.annprem.set(dm.getField("annprem"));
			screenVars.initcom.set(dm.getField("initcom"));
			screenVars.comern.set(dm.getField("comern"));
			screenVars.compay.set(dm.getField("compay"));
			screenVars.dormflag.set(dm.getField("dormflag"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6238ScreenVars screenVars = (S6238ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.chdrelem.setFieldName("chdrelem");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.annprem.setFieldName("annprem");
				screenVars.initcom.setFieldName("initcom");
				screenVars.comern.setFieldName("comern");
				screenVars.compay.setFieldName("compay");
				screenVars.dormflag.setFieldName("dormflag");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("chdrelem").set(screenVars.chdrelem);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("annprem").set(screenVars.annprem);
			dm.getField("initcom").set(screenVars.initcom);
			dm.getField("comern").set(screenVars.comern);
			dm.getField("compay").set(screenVars.compay);
			dm.getField("dormflag").set(screenVars.dormflag);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6238screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6238ScreenVars screenVars = (S6238ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.chdrelem.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.annprem.clearFormatting();
		screenVars.initcom.clearFormatting();
		screenVars.comern.clearFormatting();
		screenVars.compay.clearFormatting();
		screenVars.dormflag.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6238ScreenVars screenVars = (S6238ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.chdrelem.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.annprem.setClassString("");
		screenVars.initcom.setClassString("");
		screenVars.comern.setClassString("");
		screenVars.compay.setClassString("");
		screenVars.dormflag.setClassString("");
	}

/**
 * Clear all the variables in S6238screensfl
 */
	public static void clear(VarModel pv) {
		S6238ScreenVars screenVars = (S6238ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.chdrelem.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.annprem.clear();
		screenVars.initcom.clear();
		screenVars.comern.clear();
		screenVars.compay.clear();
		screenVars.dormflag.clear();
	}
}
