/*
 * File: P6237.java
 * Date: 30 August 2009 0:38:04
 * Author: Quipoz Limited
 * 
 * Class transformed from P6237.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.agents.dataaccess.AgcmvsqTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.enquiries.dataaccess.AgntenqTableDAM;
import com.csc.life.enquiries.screens.S6237ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will be
*     stored  in  the  CHDRENQ I/O module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist.
*
*
*     Load the subfile as follows:
*
*          Ther  first  line  of  the  subfile  should  contain the
*          servicing  agent's commission details, if there are any.
*          Read  AGCMVSQ  with a key of Contract Company (CHDRCOY),
*          Contract Number (CHDRNUM) and the Servicing Agent Number
*          (AGNTNUM).  If  a match record is found then display its
*          details in record number one of the subfile.
*
*          Perform a BEGN on AGCMVSQ with a key of Contract Company
*          and Contract Number and Agent Number set to spaces.
*
*          Continue  reading  AGCMVSQ  until  end  of  file  or the
*          Contract  Company  or Contract Number portion of the key
*          changes.  If  the  Servicing  Agent's commission details
*          were  displayed  on  line  one then ignore these details
*          when  they  are reached while reading through the file -
*          they should not appear twice on the display.
*
*          Display  the  Agent  Number (AGNTNUM) and the Commission
*          (SPLIT-BCOMM). The Agent's name is decoded in the normal
*          way  by  first  reading the CLTS data set and then using
*          the format subroutine for the name.
*
*
*     Load  all  pages  required in the subfile and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*     The  only  validation  required is of the Select fields.  The
*     only  permitted values are '1' and '2'. The program will read
*     all the changed subfile records and check that the entries an
*     the  Select  fields  are  only  one of these values or space.
*     Provide an error message and highlight the offending field if
*     any are found in error.
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If  "CF11"  was  requested  add  1 to the program pointer and
*     exit.
*
*     At  this  point  the program will be either searching for the
*     FIRST  selected  record  in  order  to  pass  control  to the
*     Transactions Postings program for the selected transaction or
*     it  will  be returning from the Transactions Postings program
*     after  displaying  some  details  and  searching for the NEXT
*     selected record.
*
*     It will  be able to determine which of these two states it is
*     in by examining the Stack Action Flag.
*
*     If  returning  from  a  Transaction  Postings display, (Stack
*     Action  field = '*') restore the next 8 positions of the WSSP
*     program stack from Working-Storage.
*
*     If not returning from a Transactions Postings display, (Stack
*     Action  Flag  is  blank),  perform  a start on the subfile to
*     position the file pointer at the beginning.
*
*     Each  time  it  returns  to  this  program after processing a
*     previous seelction its position in the subfile will have been
*     retained  and  it will be able to continue from where it left
*     off.
*
*     Processing  from here is the same for either state. After the
*     Start  or  after  returning to the program after processing a
*     previous selection read the next record from the subfile.  If
*     this  is not selected (Select is blank), continue reading the
*     next  subfile  record  until  one  is  found with a non-blank
*     Select field  or end of file is reached. Do NOT use the 'Read
*     Next Changed Subfile Record' function.
*
*     If  nothing  was  selected or there are no more selections to
*     process,  continue  by  moving  blanks  to  the current Stack
*     Action  field  and  exit.  This  will cause the program to be
*     re-invoked and the subfile re-loaded.
*
*     If  a selection has been found the first record that the next
*     function  is  to  process  should  be  read and stored in the
*     appropriate  I/O module.  For a Select value of '1' perform a
*     BEGN  and  then  a  READS  on  AGCM with a key of Con   tract
*     Company (CHDRCOY), Contract Number (CHDRNUM) and Agent Number
*     (AGNTNUM).  At  least one matching record should be found, if
*     it  is  not  then  a  system error should be reported.  For a
*     Select value  of  '2'  perform  a READS on AGLF with a key of
*     Contract  Company  (CHDRCOY)  and Agent number (AGNTNUM) from
*     AGCM. If no record is found then report a system error.
*
*     Call  GENSSWCH with the value of the Select field to retrieve
*     the program switching details. This will give the following:
*
*               1 - Initial Commission Status
*               2 - Agent's Details
*
*     Save  the next set of programs (8) from the program stack and
*     flag  the  current  position  with  '*'. Add 1 to the program
*     pointer and exit.
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1675 - Generalised Secondary Switching   Key: Transaction Code
*                                              + Program Number
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6237 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6237");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaSecProgs = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(8, 5, wsaaSecProgs, 0);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub3 = new PackedDecimalData(5, 0);
		/* ERRORS */
	private String e005 = "E005";
	private String h093 = "H093";
	private String t1692 = "T1692";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String agcmvf1rec = "AGCMVF1REC";
		/*AGCM Contract Commission Details.*/
	private AgcmvsqTableDAM agcmvsqIO = new AgcmvsqTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent Header - Contract Enquiry.*/
	private AgntenqTableDAM agntenqIO = new AgntenqTableDAM();
		/*Contract Enquiry - Contract Header.*/
	Chdrpf chdrpf = new Chdrpf();
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6237ScreenVars sv = getPScreenVars();//ScreenProgram.getScreenVars( S6237ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		updateErrorIndicators2670, 
		readNextModifiedRecord2680, 
		bypassStart4010, 
		selectIs24030, 
		callGenssw4070, 
		nextProgram4080, 
		exit4090
	}

	public P6237() {
		super();
		screenVars = sv;
		new ScreenModel("S6237", AppVars.getInstance(), sv);
	}
	
	protected S6237ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S6237ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf==null)
		{
			return;
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.servagnt.set(chdrpf.getAgntnum());
		sv.servbr.set(chdrpf.getCntbranch());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(chdrpf.getAgntcoy());
		agntenqIO.setAgntnum(chdrpf.getAgntnum());
		agntenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(),varcom.oK)
		&& isNE(agntenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.servagnam.set(wsspcomn.longconfname);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrpf.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.servbrname.fill("?");
		}
		else {
			sv.servbrname.set(descIO.getLongdesc());
		}
		agcmvsqIO.setDataArea(SPACES);
		agcmvsqIO.setChdrcoy(wsspcomn.company);
		agcmvsqIO.setChdrnum(chdrpf.getChdrnum());
		agcmvsqIO.setPlanSuffix(ZERO);
		agcmvsqIO.setAgntnum(SPACES);
		agcmvsqIO.setOvrdcat(SPACES);
		agcmvsqIO.setSeqno(ZERO);
		agcmvsqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmvsqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmvsqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, agcmvsqIO);
		if (isNE(agcmvsqIO.getStatuz(),varcom.oK)
		&& isNE(agcmvsqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmvsqIO.getParams());
			fatalError600();
		}
		while ( !(isNE(agcmvsqIO.getChdrcoy(),wsspcomn.company)
		|| isNE(agcmvsqIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(agcmvsqIO.getStatuz(),varcom.endp))) {
			processAgcmvsq1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}
//--- edited in MLIL 271 //
protected void plain100()
	{
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();

		} else if (isNE(cltsIO.getGivname(), SPACES)) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = ",";

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);

		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* PLAIN-EXIT */
	}
// --------//

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}
//-- MLIL 271 -- //
protected void payee100()
	{
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else if (isEQ(cltsIO.getEthorig(), "1")) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = "";
			String salute = cltsIO.getSalutl().toString();

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = stringUtil.includeSalute(salute, fullName);

			wsspcomn.longconfname.set(fullNameWithSalute);
		} else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(cltsIO.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(cltsIO.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
	/*PAYEE-EXIT*/
	}
// --  MLIL 271 -- //
	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = cltsIO.getLgivname().toString();
		String lastName = cltsIO.getLsurname().toString();
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}
// -- -- //
protected void processAgcmvsq1100()
	{
		para1100();
		nextAgcmvsq1110();
	}

protected void para1100()
	{
		sv.subfileFields.set(SPACES);
		sv.comagnt.set(agcmvsqIO.getAgntnum());
		sv.coverage.set(agcmvsqIO.getCoverage());
		sv.rider.set(agcmvsqIO.getRider());
		sv.planSuffix.set(agcmvsqIO.getPlanSuffix());
		sv.life.set(agcmvsqIO.getLife());
		sv.seqno.set(agcmvsqIO.getSeqno());
		sv.initcom.set(agcmvsqIO.getInitcom());
		sv.ovrdcat.set(agcmvsqIO.getOvrdcat());
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(agcmvsqIO.getChdrcoy());
		agntenqIO.setAgntnum(agcmvsqIO.getAgntnum());
		agcmvsqIO.setPlanSuffix(ZERO);
		agntenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(),varcom.oK)
		&& isNE(agntenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.agntname.set(wsspcomn.longconfname);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextAgcmvsq1110()
	{
		agcmvsqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmvsqIO);
		agcmvsqIO.setAnnprem(agcmvsqIO.getAnnprem());
		if (isNE(agcmvsqIO.getStatuz(),varcom.oK)
		&& isNE(agcmvsqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmvsqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo12010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo12010()
	{
		if (isEQ(scrnparams.statuz,varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		scrnparams.subfileRrn.set(1);
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
				}
				case readNextModifiedRecord2680: {
					readNextModifiedRecord2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.select,"1")
		|| isEQ(sv.select,"2")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.selectErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.readNextModifiedRecord2680);
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4000();
				}
				case bypassStart4010: {
					bypassStart4010();
					selectIs14020();
				}
				case selectIs24030: {
					selectIs24030();
				}
				case callGenssw4070: {
					callGenssw4070();
				}
				case nextProgram4080: {
					nextProgram4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		agcmvsqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, agcmvsqIO);
		if (isNE(agcmvsqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmvsqIO.getParams());
			fatalError600();
		}
		aglfIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			sv.select.set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
			goTo(GotoLabel.bypassStart4010);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypassStart4010()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4400();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.nextProgram4080);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		if (isEQ(sv.select,"2")) {
			goTo(GotoLabel.selectIs24030);
		}
	}

protected void selectIs14020()
	{
		agcmvsqIO.setDataArea(SPACES);
		agcmvsqIO.setChdrcoy(wsspcomn.company);
		agcmvsqIO.setChdrnum(chdrpf.getChdrnum());
		agcmvsqIO.setPlanSuffix(ZERO);
		agcmvsqIO.setAgntnum(SPACES);
		agcmvsqIO.setOvrdcat(SPACES);
		agcmvsqIO.setSeqno(ZERO);
		agcmvsqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, agcmvsqIO);
		if (isNE(agcmvsqIO.getStatuz(),varcom.oK)
		&& isNE(agcmvsqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmvsqIO.getParams());
			fatalError600();
		}
		compute(wsaaSub3, 0).set(sub(scrnparams.subfileRrn,1));
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaSub3);
		for (int loopVar3 = 0; !(isEQ(loopVar3,loopEndVar1.toInt())); loopVar3 += 1){
			agcmvsqIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, agcmvsqIO);
			if (isNE(agcmvsqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(agcmvsqIO.getParams());
				fatalError600();
			}
		}
		agcmvsqIO.setFunction(varcom.keeps);
		agcmvsqIO.setFormat(agcmvf1rec);
		SmartFileCode.execute(appVars, agcmvsqIO);
		if (isNE(agcmvsqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmvsqIO.getParams());
			fatalError600();
		}
		goTo(GotoLabel.callGenssw4070);
	}

protected void selectIs24030()
	{
		aglfIO.setDataArea(SPACES);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(chdrpf.getChdrnum());
		aglfIO.setAgntnum(sv.comagnt);
		aglfIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		goTo(GotoLabel.callGenssw4070);
	}

protected void callGenssw4070()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set(sv.select);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4080()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void readSubfile4400()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S6237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	public StringUtil getStringUtil() {
		return stringUtil;
	}

}
