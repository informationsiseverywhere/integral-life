package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sa575screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sa575screensfl";
		lrec.subfileClass = Sa575screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 13;
		lrec.pageSubfile = 12;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa575ScreenVars sv = (Sa575ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa575screenctlWritten, sv.Sa575screensflWritten, av, sv.sa575screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sa575ScreenVars screenVars = (Sa575ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.dbtcdtdate.setClassString("");
		screenVars.dbtcdtdesc.setClassString("");
		screenVars.amnt.setClassString("");
		screenVars.bankcode.setClassString("");
		screenVars.aacct.setClassString("");
		screenVars.statdesc.setClassString("");
		screenVars.transcode.setClassString("");
		screenVars.trcdedesc.setClassString("");
		screenVars.trdate.setClassString("");
		screenVars.dbtcdtdateDisp.setClassString("");
		screenVars.trdateDisp.setClassString("");
		
		
	}

/**
 * Clear all the variables in Sa575screenctl
 */
	public static void clear(VarModel pv) {
		Sa575ScreenVars screenVars = (Sa575ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.dbtcdtdate.clear();
		screenVars.dbtcdtdesc.clear();
		screenVars.amnt.clear();
		screenVars.bankcode.clear();
		screenVars.aacct.clear();
		screenVars.statdesc.clear();
		screenVars.transcode.clear();
		screenVars.trcdedesc.clear();
		screenVars.trdate.clear();
		screenVars.dbtcdtdateDisp.clear();
		screenVars.trdateDisp.clear();
		
	
	}
}
