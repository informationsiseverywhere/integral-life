package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6259
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6259ScreenVars extends SmartVarModel { 


	//public FixedLengthStringData dataArea = new FixedLengthStringData(1934);
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	//public FixedLengthStringData dataFields = new FixedLengthStringData(766).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData benBillDate = DD.bbldat.copyToZonedDecimal().isAPartOf(dataFields,7);
	public ZonedDecimalData annivProcDate = DD.cbanpr.copyToZonedDecimal().isAPartOf(dataFields,15);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,47);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,49);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,117);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,127);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,144);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,191);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,203);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,250);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,258);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,259);
	public FixedLengthStringData optdscs = new FixedLengthStringData(75).isAPartOf(dataFields, 263);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(5, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(75).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optdsc05 = DD.optdsc.copy().isAPartOf(filler,60);
	public FixedLengthStringData optinds = new FixedLengthStringData(5).isAPartOf(dataFields, 338);
	public FixedLengthStringData[] optind = FLSArrayPartOfStructure(5, 1, optinds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(5).isAPartOf(optinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01 = DD.optind.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optind02 = DD.optind.copy().isAPartOf(filler1,1);
	public FixedLengthStringData optind03 = DD.optind.copy().isAPartOf(filler1,2);
	public FixedLengthStringData optind04 = DD.optind.copy().isAPartOf(filler1,3);
	public FixedLengthStringData optind05 = DD.optind.copy().isAPartOf(filler1,4);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,343);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,347);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,355);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,365);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,373);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,376);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(dataFields,378);
	public ZonedDecimalData rerateFromDate = DD.rrtfrm.copyToZonedDecimal().isAPartOf(dataFields,386);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,394);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,411);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,412);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,414);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,418);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,433);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,450);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(dataFields,463);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,483);
	/*BRD-306 STARTS*/
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 500);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 517);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 534);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 551);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 568);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,585);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,586);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,589);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,592);//ILIFE-4889
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,595);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,598);
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,615);
	/*BRD-306 END*/
	public ZonedDecimalData benCessDate = DD.bcesdte.copyToZonedDecimal().isAPartOf(dataFields,632);
	public ZonedDecimalData benCessAge = DD.bcessage.copyToZonedDecimal().isAPartOf(dataFields,640);
	public ZonedDecimalData benCessTerm = DD.bcesstrm.copyToZonedDecimal().isAPartOf(dataFields,643);
	public FixedLengthStringData waitperiod = DD.waitperiod.copy().isAPartOf(dataFields,646);
	public FixedLengthStringData bentrm = DD.bentrm.copy().isAPartOf(dataFields,649);
	public FixedLengthStringData poltyp = DD.zpoltyp.copy().isAPartOf(dataFields,651);
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,652);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(dataFields,653);//BRD-009
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,655);//BRD-NBP-011
	public FixedLengthStringData lnkgno = DD.lnkgno.copy().isAPartOf(dataFields,658); //ILIFE-6968
	public FixedLengthStringData lnkgsubrefno = DD.lnkgsubrefno.copy().isAPartOf(dataFields,708);//ILIFE-6968
	public FixedLengthStringData payoutoption = DD.payoutopt.copy().isAPartOf(dataFields,758);//fwang3 ICIL-4
	// IBPLIFE-2141 Start
	public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(dataFields, 766);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(dataFields, 770);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(dataFields, 800);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields, 801);
	public FixedLengthStringData covrprpse = DD.covrprpse.copy().isAPartOf(dataFields, 809);
	// IBPLIFE-2141 End
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(292).isAPartOf(dataArea, 766);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbldatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cbanprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(5, 4, optdscsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData optdsc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData optindsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] optindErr = FLSArrayPartOfStructure(5, 4, optindsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(optindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optind02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData optind03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData optind04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData optind05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData rrtfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData zdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	/*BRD-306 STARTS*/
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	/*ILIFE-3417 START*/
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	/*ILIFE-3417 ENDS*/
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	/*BRD-306 END*/
	public FixedLengthStringData bcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData bcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	public FixedLengthStringData bcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);
	public FixedLengthStringData waitperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 256);
	public FixedLengthStringData bentrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 260);
	public FixedLengthStringData poltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 264);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 268);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 272);//BRD-009
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 276);//BRD-NBP-011
	public FixedLengthStringData lnkgnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 280);//ILIFE-6968
	public FixedLengthStringData lnkgsubrefnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 284);//ILIFE-6968
	public FixedLengthStringData payoutoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 288);
	//IBPLIFE-2141 Start
	public FixedLengthStringData trancdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 292);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 296);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 300);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 304);
	public FixedLengthStringData covrprpseErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 308);
	//IBPLIFE-2141 End	
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(876).isAPartOf(dataArea, 1058);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbldatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cbanprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 264);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(5, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] optdsc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData optindsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(5, 12, optindsOut, 0);
	public FixedLengthStringData[][] optindO = FLSDArrayPartOfArrayStructure(12, 1, optindOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(60).isAPartOf(optindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optind01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] optind02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] optind03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] optind04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] optind05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] rrtfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	/*BRD-306 STARTS*/
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	/*ILIFE-3417 STARTS*/
	public FixedLengthStringData[] selectout = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	/*ILIFE-3417 ENDS*/										
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	/*BRD-306 END*/
	public FixedLengthStringData[] bcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] bcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	public FixedLengthStringData[] bcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);
	public FixedLengthStringData[]waitperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 768);
	public FixedLengthStringData[]bentrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 780);
	public FixedLengthStringData[]poltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 792);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators,804 );
	public FixedLengthStringData[]statcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 816);//BRD-009
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 828);//BRD-NBP-011
	public FixedLengthStringData[] lnkgnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 840);//ILIFE-6968
	public FixedLengthStringData[] lnkgsubrefnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 852);//ILIFE-6968
	public FixedLengthStringData[] payoutoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 864); //fwang3
	//IBPLIFE-2141 Start
	public FixedLengthStringData[] trancdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 876);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 888);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 900);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 912);
	public FixedLengthStringData[] covrprpseOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 924);
	//IBPLIFE-2141 End
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData benBillDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData annivProcDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateFromDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData benCessDateDisp = new FixedLengthStringData(10);
	public LongData S6259screenWritten = new LongData(0);
	public LongData S6259protectWritten = new LongData(0);	
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1); 
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);//IBPLIFE-2141
	public FixedLengthStringData nbprp126lag = new FixedLengthStringData(1);//IBPLIFE-2141
	
	public boolean hasSubfile() {
		return false;
	}


	public S6259ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bbldatOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc03Out,new String[] {null, null, null, "04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind03Out,new String[] {"06","04","-06","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc01Out,new String[] {null, null, null, "02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind01Out,new String[] {"05","02","-05","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc02Out,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {"08","03","-08","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdescOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc04Out,new String[] {null, null, null, "43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind04Out,new String[] {"46","43","-46","43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29","79", null, null, null, null, null, null, null, null});//ILJ-45
		fieldIndMap.put(pcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(waitperiodOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentrmOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(poltypOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {null, null, null, "56", null, null, null, null, null, null, null, null});
		/*BRD-306 START */
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "58", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "59", null, null, null, null, null, null, null, null});
		/*BRD-306 END */
		fieldIndMap.put(statcodeOut,new String[] {null, "66", null, "67", null, null, null, null, null, null, null, null});//BRD-009
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(optdsc05Out,new String[] {null, null, null, "73",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind05Out,new String[] {null,null,null,"74",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lnkgnoOut,new String[] {null, null, null, "121", null, null, null, null, null, null, null, null}); //ILIFE-6968
		fieldIndMap.put(lnkgsubrefnoOut,new String[] {null, null, null, "122", null, null, null, null, null, null, null, null});//ILIFE-6968
		fieldIndMap.put(payoutoptionOut,new String[] {"75","76","-75",null, null, null, null, null, null, null, null, null});//fwang3
		fieldIndMap.put(crrcdOut,new String[] {null, null, null,"77",null, null, null, null, null, null, null, null});//ILJ-45
		//IBPLIFE-2141 Start
		fieldIndMap.put(trancdOut,new String[] {null, null, null, "80",null, null, null, null, null, null, null, null});
		fieldIndMap.put(trandescOut,new String[] {null, null, null, "81",null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {null, null, null, "82",null, null, null, null, null, null, null, null});
		fieldIndMap.put(covrprpseOut,new String[] {null, null, null, "83",null, null, null, null, null, null, null, null});
		//IBPLIFE-2141 End		
		//fieldIndMap.put(rcessageOut,new String[] {null, null, null,"78",null, null, null, null, null, null, null, null});//ILJ-45
		/*screenFields = new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, frqdesc, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, mortcls, optdsc03, optind03, optdsc01, optind01, optdsc02, optind02, optdsc05, optind05, bappmeth, zdesc, zlinstprem, taxamt, optdsc04, optind04,loadper, rateadj, fltmort, premadj,adjustageamt,select,zbinstprem,riskCessAge,riskCessTerm,premCessAge,premCessTerm,zstpduty01, benCessAge, benCessTerm, benCessDate,waitperiod,bentrm,poltyp,prmbasis,statcode,dialdownoption,lnkgno,lnkgsubrefno,payoutoption};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, frqdescOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, rrtdatOut, rrtfrmOut, bbldatOut, mortclsOut, optdsc03Out, optind03Out, optdsc01Out, optind01Out, optdsc02Out, optind02Out, optdsc05Out, optind05Out, bappmethOut, zdescOut, zlinstpremOut, taxamtOut, optdsc04Out, optind04Out,loadperOut, rateadjOut, fltmortOut, premadjOut,adjustageamtOut,selectout,zbinstpremOut,rcesstrmOut,pcesstrmOut,rcessageOut,pcessageOut,zstpduty01Out, bcessageOut, bcesstrmOut, bcesdteOut,waitperiodOut,bentrmOut,poltypOut,prmbasisOut,statcodeOut,dialdownoptionOut,lnkgnoOut,lnkgsubrefnoOut,payoutoptionOut};//ILIFE-3417
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, frqdescErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, mortclsErr, optdsc03Err, optind03Err, optdsc01Err, optind01Err, optdsc02Err, optind02Err, optdsc05Err, optind05Err, bappmethErr, zdescErr, zlinstpremErr, taxamtErr, optdsc04Err, optind04Err,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,selectErr,zbinstpremErr,rcesstrmErr,pcesstrmErr,rcessageErr,pcessageErr,zstpduty01Err, bcessageErr, bcesstrmErr, bcesdteErr,waitperiodErr,bentrmErr,poltypErr,prmbasisErr,statcodeErr,dialdownoptionErr,lnkgnoErr,lnkgsubrefnoErr,payoutoptionErr};//ILIFE-3417
		screenDateFields = new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, benCessDate};
		screenDateErrFields = new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, bcesdteErr};
		screenDateDispFields = new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, rerateDateDisp, rerateFromDateDisp, benBillDateDisp, benCessDateDisp};
		 */
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6259screen.class;
		protectRecord = S6259protect.class;
	}
	public BaseData[] getscreenFields() {
		return new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, frqdesc, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, mortcls, optdsc03, optind03, optdsc01, optind01, optdsc02, optind02, optdsc05, optind05, bappmeth, zdesc, zlinstprem, taxamt, optdsc04, optind04,loadper, rateadj, fltmort, premadj,adjustageamt,select,zbinstprem,riskCessAge,riskCessTerm,premCessAge,premCessTerm,zstpduty01, benCessAge, benCessTerm, benCessDate,waitperiod,bentrm,poltyp,prmbasis,statcode,dialdownoption,lnkgno,lnkgsubrefno,payoutoption,
				trancd, trandesc, validflag, effdate, covrprpse};
	}
	public BaseData[][] getscreenOutFields() {
			return new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, frqdescOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, rrtdatOut, rrtfrmOut, bbldatOut, mortclsOut, optdsc03Out, optind03Out, optdsc01Out, optind01Out, optdsc02Out, optind02Out, optdsc05Out, optind05Out, bappmethOut, zdescOut, zlinstpremOut, taxamtOut, optdsc04Out, optind04Out,loadperOut, rateadjOut, fltmortOut, premadjOut,adjustageamtOut,selectout,zbinstpremOut,rcesstrmOut,pcesstrmOut,rcessageOut,pcessageOut,zstpduty01Out, bcessageOut, bcesstrmOut, bcesdteOut,waitperiodOut,bentrmOut,poltypOut,prmbasisOut,statcodeOut,dialdownoptionOut,lnkgnoOut,lnkgsubrefnoOut,payoutoptionOut,
				trancdOut, trandescOut, validflagOut, effdateOut, covrprpseOut};
	}
	public BaseData[] getscreenErrFields() {
			return new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, frqdescErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, mortclsErr, optdsc03Err, optind03Err, optdsc01Err, optind01Err, optdsc02Err, optind02Err, optdsc05Err, optind05Err, bappmethErr, zdescErr, zlinstpremErr, taxamtErr, optdsc04Err, optind04Err,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,selectErr,zbinstpremErr,rcesstrmErr,pcesstrmErr,rcessageErr,pcessageErr,zstpduty01Err, bcessageErr, bcesstrmErr, bcesdteErr,waitperiodErr,bentrmErr,poltypErr,prmbasisErr,statcodeErr,dialdownoptionErr,lnkgnoErr,lnkgsubrefnoErr,payoutoptionErr,
				trancdErr, trandescErr, validflagErr, effdateErr, covrprpseErr};
	}
	public BaseData[] getscreenDateFields() {
		return new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, benCessDate, effdate};
	}
	public BaseData[] getscreenDateDispFields() {
		return new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, rerateDateDisp, rerateFromDateDisp, benBillDateDisp, benCessDateDisp, effdateDisp};
	}
	public BaseData[] getscreenDateErrFields() {
		return new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, bcesdteErr, effdateErr};
	}

	public int getDataAreaSize(){
		return 1934 + 223; 
	}
	
	public int getDataFieldsSize(){
		return 766 + 143;
	}
	
	public int getErrorIndicatorSize(){
		return  292 + 20;
	}
	
	public int getOutputFieldSize(){
		return 876 + 60;  
	}

}
