package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5122screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S5122screensfl";
		lrec.subfileClass = S5122screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		//MIBT-82 starts
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 50;
		lrec.pageSubfile = 49;
		//MIBT-82 ends
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5122ScreenVars sv = (S5122ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5122screenctlWritten, sv.S5122screensflWritten, av, sv.s5122screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5122ScreenVars screenVars = (S5122ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.fundShortDesc.setClassString("");
		screenVars.fundtype.setClassString("");
		screenVars.fundTypeShortDesc.setClassString("");
		screenVars.fundCurrency.setClassString("");
		screenVars.zvar01.setClassString("");
		screenVars.zvar02.setClassString("");
		screenVars.zvariable.setClassString("");
		screenVars.ind01.setClassString("");
		screenVars.ind02.setClassString("");
	}

/**
 * Clear all the variables in S5122screenctl
 */
	public static void clear(VarModel pv) {
		S5122ScreenVars screenVars = (S5122ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.numpols.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.planSuffix.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.fundShortDesc.clear();
		screenVars.fundtype.clear();
		screenVars.fundTypeShortDesc.clear();
		screenVars.fundCurrency.clear();
		screenVars.zvar01.clear();
		screenVars.zvar02.clear();
		screenVars.zvariable.clear();
		screenVars.ind01.clear();
		screenVars.ind02.clear();
	}
}
