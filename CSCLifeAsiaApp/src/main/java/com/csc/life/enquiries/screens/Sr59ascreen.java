package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 22/08/16 05:43
 * @author Quipoz
 */
public class Sr59ascreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr59aScreenVars sv = (Sr59aScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr59ascreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr59aScreenVars screenVars = (Sr59aScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.zlstupdte.setClassString("");
		screenVars.zusrprf.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.zafropt1disp.setClassString("");
		screenVars.zafritemdisp.setClassString("");
		screenVars.zlastdte.setClassString("");
		screenVars.znextdte.setClassString("");
		screenVars.zafropt1.setClassString("");
		screenVars.zafrfreq.setClassString("");
		screenVars.currfrom.setClassString("");
		screenVars.ptdate.setClassString("");
		screenVars.zafritem.setClassString("");
	}

/**
 * Clear all the variables in St118screen
 */
	public static void clear(VarModel pv) {
		Sr59aScreenVars screenVars = (Sr59aScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.ctypedes.clear();
		screenVars.zlstupdte.clear();
		screenVars.zusrprf.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.zafropt1disp.clear();
		screenVars.zafritemdisp.clear();
		screenVars.zlastdte.clear();
		screenVars.znextdte.clear();
		screenVars.zafropt1.clear();
		screenVars.zafrfreq.clear();
		screenVars.currfrom.clear();
		screenVars.ptdate.clear();
		screenVars.zafritem.clear();
	}
}
