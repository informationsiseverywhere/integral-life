package com.csc.life.enquiries.dataaccess;

import com.csc.life.newbusiness.dataaccess.BnfypfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BnfyenqTableDAM.java
 * Date: Sun, 30 Aug 2009 03:30:00
 * Class transformed from BNFYENQ.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BnfyenqTableDAM extends BnfypfTableDAM {

	public BnfyenqTableDAM() {
		super();
		setKeyConstants();

	}
	@Override
	public void setTable() {
		super.setTable();
		TABLE = getTableName("BNFYENQ");
	}
	@Override
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", BNYCLT";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "BNYCLT, " +
		            "VALIDFLAG, " +
		            "CURRFR, " +
		            "CURRTO, " +
		            "BNYRLN, " +
		            "BNYCD, " +
		            "BNYPC, " +
		            "EFFDATE, " +
		            "BNYTYPE, " +
		            "CLTRELN, " +
		            "RELTO, " +
		            "SELFIND, " +
		            "REVCFLG, " +
		            "ENDDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "SEQUENCE, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "SEQUENCE ASC, " +
		            "BNYCLT ASC, " +		            
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "SEQUENCE DESC, " +
		            "BNYCLT DESC, " +		           
					"UNIQUE_NUMBER DESC";//#ILIFE-7217

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               bnyclt,
                               validflag,
                               currfrom,
                               currto,
                               bnyrln,
                               bnycd,
                               bnypc,
                               effdate,
                               bnytype,
                               cltreln,
                               relto,
                               selfind,
                               revcflg,
                               enddate,
                               userProfile,
                               jobName,
                               datime,
                               sequence,
                               unique_number
		                       };
		
	}
	@Override
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	@Override
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/
	@Override
	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(239);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	@Override
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getBnyclt().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	@Override
	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, bnyclt);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	@Override
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(bnyclt.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	@Override
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(102);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getBnyrln().toInternal()
					+ getBnycd().toInternal()
					+ getBnypc().toInternal()
					+ getEffdate().toInternal()
					+ getBnytype().toInternal()
					+ getCltreln().toInternal()
					+ getRelto().toInternal()
					+ getSelfind().toInternal()
					+ getRevcflg().toInternal()
					+ getEnddate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getSequence().toInternal());
		return nonKeyData;
	}

	@Override
	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, bnyrln);
			what = ExternalData.chop(what, bnycd);
			what = ExternalData.chop(what, bnypc);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, bnytype);
			what = ExternalData.chop(what, cltreln);
			what = ExternalData.chop(what, relto);
			what = ExternalData.chop(what, selfind);
			what = ExternalData.chop(what, revcflg);
			what = ExternalData.chop(what, enddate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);	
			what = ExternalData.chop(what, sequence);	
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getBnyclt() {
		return bnyclt;
	}
	public void setBnyclt(Object what) {
		bnyclt.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getBnyrln() {
		return bnyrln;
	}
	public void setBnyrln(Object what) {
		bnyrln.set(what);
	}	
	public FixedLengthStringData getBnycd() {
		return bnycd;
	}
	public void setBnycd(Object what) {
		bnycd.set(what);
	}	
	public PackedDecimalData getBnypc() {
		return bnypc;
	}
	public void setBnypc(Object what) {
		setBnypc(what, false);
	}
	public void setBnypc(Object what, boolean rounded) {
		if (rounded)
			bnypc.setRounded(what);
		else
			bnypc.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getEnddate() {
		return enddate;
	}
	public void setEnddate(Object what) {
		setEnddate(what, false);
	}
	public void setEnddate(Object what, boolean rounded) {
		if (rounded)
			enddate.setRounded(what);
		else
			enddate.set(what);
	}
	public FixedLengthStringData getBnytype() {
		return bnytype;
	}
	public void setBnytype(Object what) {
		bnytype.set(what);
	}	
	public FixedLengthStringData getCltreln() {
		return cltreln;
	}
	public void setCltreln(Object what) {
		cltreln.set(what);
	}	
	public FixedLengthStringData getRelto() {
		return relto;
	}
	public void setRelto(Object what) {
		relto.set(what);
	}	
	public FixedLengthStringData getSelfind() {
		return selfind;
	}
	public void setSelfind(Object what) {
		selfind.set(what);
	}	
	public FixedLengthStringData getRevcflg() {
		return revcflg;
	}
	public void setRevcflg(Object what) {
		revcflg.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public FixedLengthStringData getSequence() {
		return sequence;
	}
	public void setSequence(Object what) {
		sequence.set(what);
	}	
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	@Override
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		bnyclt.clear();
		keyFiller.clear();
	}

	@Override
	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		bnyrln.clear();
		bnycd.clear();
		bnypc.clear();
		effdate.clear();
		bnytype.clear();
		cltreln.clear();
		relto.clear();
		selfind.clear();
		revcflg.clear();
		enddate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();	
		sequence.clear();
	}


}