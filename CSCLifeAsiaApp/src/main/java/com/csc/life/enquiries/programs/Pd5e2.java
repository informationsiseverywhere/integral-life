/*
 * File: Pd5e2.java
 * Date: 30 August 2009 0:37:43
 * Author: Quipoz Limited
 * 
 * Class transformed from PD5E2.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.UndrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Undrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.tablestructures.T6632rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.screens.Sd5e2ScreenVars;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.recordstructures.SurtaxRec;
import com.csc.life.terminationclaims.tablestructures.Tr691rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

public class Pd5e2 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pd5e2.class);// IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5E2");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5679 = "T5679";
	private static final String t6632 = "T6632";
	private static final String t5687 = "T5687";
	private static final String tr52d = "TR52D";
	private static final String t6598 = "T6598";
	private static final String tr52e = "TR52E";
	private static final String tr691 = "TR691";
	private static final String t5611 = "T5611";
	private static final String f321 = "F321";
	private static final String f294 = "F294";

	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6632rec t6632rec = new T6632rec();
	private T6598rec t6598rec = new T6598rec();
    private T6598rec t6598recBonus = new T6598rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private T5611rec t5611rec = new T5611rec();
	private Tr691rec tr691rec = new Tr691rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Totloanrec totloanrec = new Totloanrec();

	private List<Itempf> t6632List = null;
	private List<Itempf> t5611List = null;
	private List<Itempf> t5687List = null;
    private List<Itempf> t6640List = null;

	private DescTableDAM descIO = new DescTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sd5e2ScreenVars sv = ScreenProgram.getScreenVars(Sd5e2ScreenVars.class);
	private ExternalisedRules er = new ExternalisedRules();

	private Boolean wsaaValidStatus1;
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData cur_balance = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData cur_unit = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSumAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalSumAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaAccAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalAccAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHospitalAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalHospitalAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaFundAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalFundAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaLoanAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotLoanAmnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldSurrVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSurrValTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldLoanAvail = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldTotLoanAvail = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaLoanAvailable = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaZrcshamt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans1 = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaCompLoanVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaComponentTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaSurrenderRecStore = new FixedLengthStringData(150);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaapolicyloan = new PackedDecimalData(17, 2).init(0);
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private PackedDecimalData wsaaTaxAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaLoanValue = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTdbtamt = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private PackedDecimalData wsaaPenaltyTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData tpoldbtrec = new FixedLengthStringData(10).init("TPOLDBTREC");
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	private FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();

	private String wsaaNoPrice = "";
	private String wsaaValidStatus = "";

	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();

	private Srcalcpy srcalcpy = new Srcalcpy();
	private Srcalcpy srcalcpyBonus = new Srcalcpy();
	private ZonedDecimalData wsaaSwitch1 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime1 = new Validator(wsaaSwitch1, "1");
	private ZonedDecimalData wsaaSwitch2 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime = new Validator(wsaaSwitch2, "1");

	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private Utrspf utrspf = new Utrspf();
	private List<Utrspf> utrssurList = null;

	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrList;

	private VprcpfDAO vprcpfDAO = getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class);
	private Vprcpf vprcpf = new Vprcpf();
	private List<Vprcpf> vprcList = null;

	private ChdrpfDAO chdrpfDao = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private List<Chdrpf> chdrpfList;

	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private List<Clntpf> clntpfList = null;

	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	private Itempf itempf = null;

	private UndrpfDAO undrpfDao = getApplicationContext().getBean("UndrpfDAO", UndrpfDAO.class);
	private Undrpf undrpf = new Undrpf();

	private Payrpf payrpf = new Payrpf();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	private List<Payrpf> payrpfList = null;
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(4, 5);

    private T6640rec t6640rec = new T6640rec();
    private static final String t6640 = "T6640";
	private PackedDecimalData wsaaTotalBonusAmnt = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private PackedDecimalData wsaaAcblCurrentBalance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaStoredReserve = new PackedDecimalData(17, 2);
	boolean CTENQ003Permission  = false;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, bypassStart4010, nextProgram4080, exit4090, exit4190, exit4290
	}

	public Pd5e2() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5e2", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void initialise1000() {
		initialise1010();
	}

	protected void initialise1010() {
		CTENQ003Permission  = FeaConfg.isFeatureExist("2", "CTENQ003", appVars, "IT");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("Sd5e2", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.cownnum.set(wsspcomn.chdrCownnum);
		clntpf = new Clntpf();
		ownerName();
		readCHDRPF();

		scrnparams.subfileRrn.set(1);
	}

	protected void ownerName() {
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.cownnum.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if (clntpfList == null || (clntpfList != null && clntpfList.size() == 0)) {
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		sv.ownername.set(wsspcomn.longconfname);
	}

	protected void plainname(Clntpf clntpf) {

		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}

	}

	protected void corpname(Clntpf clntpf) {

		wsspcomn.longconfname.set(SPACES);

		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);

	}

	protected void readCHDRPF() {
		chdrpfList = chdrpfDao.getListOwnLife("CH", wsspcomn.company.toString(), wsspcomn.chdrCownnum.toString(), wsspcomn.chdrClntpfx.toString(),wsspcomn.chdrClntcoy.toString());
		//chdrpfList = covrDao.getListOwnLife("CH", "2", wsspcomn.chdrCownnum.toString(), "CN",wsspcomn.chdrClntcoy.toString());
		if (chdrpfList == null || (chdrpfList != null && chdrpfList.size() == 0)) {
			return;
		}
		sv.sumin.set(ZERO);
		sv.accident.set(ZERO);
		sv.hospital.set(ZERO);
		sv.fundamnt.set(ZERO);
		sv.loanVal.set(ZERO);
		sv.surrval.set(ZERO);
		sv.bonusVal.set(ZERO);
		for (Chdrpf chdr : chdrpfList) {
			chdrpf = chdr;
			sv.subfileArea.set(SPACES);
			checkStatus();
			payrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
			payrpf.setChdrnum(chdrpf.getChdrnum());
			payrpf.setValidflag("1");
			payrpf.setPayrseqno(1);
			payrpfList = payrpfDAO.readPayrData(payrpf);
			if(payrpfList==null || (payrpfList!=null && payrpfList.size()==0))
			{
				fatalError600();
			}
			payrpf = payrpfList.get(0);
			if (wsaaValidStatus1) {
				sv.chdrnum.set(chdr.getChdrnum());
				sv.cnttype.set(chdr.getCnttype());
				sv.chdrstatus.set(chdr.getStatcode());
				sv.premstatus.set(chdr.getPstcde());
				sv.occdate.set(chdr.getOccdate());
				setContractStatus();
				setPremiumStatus();
				readTr52d5120();	
				readCOVRPF();
				addsflData();
			}
		}
		sv.sumin.set(wsaaTotalSumAmnt);
		sv.accident.set(wsaaTotalAccAmnt);
		sv.hospital.set(wsaaTotalHospitalAmnt);
		sv.fundamnt.set(wsaaTotalFundAmnt);
		sv.loanVal.set(wsaaHeldTotLoanAvail);
		sv.surrval.set(wsaaSurrValTot);
		sv.bonusVal.set(wsaaTotalBonusAmnt);
	}

	protected void readCOVRPF() {
		covrList = covrDao.getCovrsurByComAndNum(wsspcomn.company.toString(), chdrpf.getChdrnum());
		wsaaNoPrice = "N";
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaPenaltyTot.set(ZERO);
		wsaaSwitch1.set(1);
		wsaapolicyloan.set(ZERO);
		wsaaTaxAmt.set(0);
		
		wsaaComponentTot.set(ZERO);
		wsaaCompLoanVal.set(ZERO);
		wsaaLoanAvailable.set(ZERO);		
		wsaaHeldCurrLoans.set(ZERO);
		wsaaHeldCurrLoans1.set(ZERO);
		
		sv.fundamnt.set(ZERO);
		for (Covrpf covr : covrList) {
			covrpf = covr;		
			wsaaSwitch2.set(1);
			
			wsaaZrcshamt.set(ZERO);
			wsaaSumAmnt.set(ZERO);
			wsaaAccAmnt.set(ZERO);
			wsaaHospitalAmnt.set(ZERO);
			wsaaHeldSurrVal.set(ZERO);
			wsaaHeldLoanAvail.set(ZERO);
			wsaaFundAmnt.set(ZERO);
			readSumAssured();
			readSumAssuredByRisk();
			readAccidentBenefit();
			readHospitalBenefit();
			readAccidentBenefitByRisk();
			readHospitalBenefitByRisk();
			readHospitalBenefitByRisk100(); /* IBPLIFE-6765 BY LOHITH*/
			readFUND();
			readSurr();
		}
	}
	protected void readSumAssured() {		
		wsaaSumAmnt = PackedDecimalData.parseObject(
				undrpfDao.getTotalRecord(covrpf.getChdrnum(), covrpf.getCrtable(), "CDTH")); /* IJTI-1479 */
		wsaaTotalSumAmnt.add(wsaaSumAmnt);
	}
	
	protected void readSumAssuredByRisk(){
		wsaaSumAmnt = PackedDecimalData.parseObject(
				undrpfDao.getTotalRecord(covrpf.getChdrnum(), covrpf.getCrtable(), "TERM")); 
		wsaaTotalSumAmnt.add(wsaaSumAmnt);
	}

	protected void readAccidentBenefit() {		
		wsaaAccAmnt = PackedDecimalData.parseObject(
				undrpfDao.getTotalRecord(covrpf.getChdrnum(), covrpf.getCrtable(), "CACC")); /* IJTI-1479 */
		wsaaTotalAccAmnt.add(wsaaAccAmnt);
	}

	protected void readHospitalBenefit() {		
		wsaaHospitalAmnt = PackedDecimalData.parseObject(
				undrpfDao.getTotalRecord(covrpf.getChdrnum(), covrpf.getCrtable(), "CHSR")); /* IJTI-1479 */
		wsaaTotalHospitalAmnt.add(wsaaHospitalAmnt);
	}
	protected void readAccidentBenefitByRisk() {
		/*Added new Method with CDTH for the new record, whereas already existing 
		 record with risk class CACC will work with readAccidentBenefit() */
		wsaaAccAmnt = PackedDecimalData.parseObject(
				undrpfDao.getTotalRecord(covrpf.getChdrnum(), covrpf.getCrtable(), "CDTH")); 
		wsaaTotalAccAmnt.add(wsaaAccAmnt);
	}

	protected void readHospitalBenefitByRisk() {	
		/*Added new Method with CWOP for the new record, whereas already existing 
		 record with risk class CHSR will work with readAccidentBenefit() */
		wsaaHospitalAmnt = PackedDecimalData.parseObject(
				undrpfDao.getTotalRecord(covrpf.getChdrnum(), covrpf.getCrtable(), "CWOP")); 
		wsaaTotalHospitalAmnt.add(wsaaHospitalAmnt);
	}
		/* IBPLIFE-6765 STARTS- ADDED NEW RISKTYPE HOSC FOR NEW RECORD */
	protected void readHospitalBenefitByRisk100() {		
		wsaaHospitalAmnt = PackedDecimalData.parseObject(
				undrpfDao.getTotalRecord(covrpf.getChdrnum(), covrpf.getCrtable(), "HOSC")); 
		wsaaTotalHospitalAmnt.add(wsaaHospitalAmnt);
	}/* IBPLIFE-6765 ENDS*/

	protected void readFUND() {		
		cur_balance.set(ZERO);
		cur_unit.set(ZERO);
		utrssurList = new ArrayList<Utrspf>();
		utrssurList = utrspfDAO.getUtrsRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),
				covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider()); /* IJTI-1479 */
		if (!utrssurList.isEmpty()) {
			for (Utrspf utrs : utrssurList) {
				cur_balance.set(ZERO);
				cur_unit.set(ZERO);
				cur_balance = PackedDecimalData.parseObject(utrs.getCurrentUnitBal());
				cur_unit = PackedDecimalData.parseObject(vprcpfDAO.getvprcRecord(covrpf.getChdrcoy(),utrs.getUnitVirtualFund(), wsaaToday.toInt())); /* IJTI-1479 */
				compute(wsaaFundAmnt, 2).set(add(wsaaFundAmnt, (mult(cur_balance, cur_unit))));
			}
		} else {
			wsaaFundAmnt.set(ZERO);
		}
		wsaaTotalFundAmnt.add(wsaaFundAmnt);

	}

	protected void setContractStatus() {
		/* Obtain the Contract Status description from T3623. */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		} else {
			sv.chdrstatus.set(descIO.getLongdesc());
		}
	}

	protected void setPremiumStatus() {

		/* Obtain the Premuim Status description from T3588. */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getLongdesc());
		}
	}

	protected void checkStatus() {
		wsaaValidStatus1 = false;
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679); /* IJTI-1479 */
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaSub.set(ZERO);
		lookForStat();
	}

	protected void lookForStat() {
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			return;
		} else {
			if (isNE(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				lookForStat();
			} else {
				wsaaValidStatus1 = true;
			}
		}
	}

	protected void preScreenEdit() {
		preStart();
	}

	protected void preStart() {
        if (CTENQ003Permission) {
            sv.bonusValOut[varcom.nd.toInt()].set("N");
            sv.bonusValOut[varcom.pr.toInt()].set("N");
        }
        else {
            sv.bonusValOut[varcom.nd.toInt()].set("Y");
            sv.bonusValOut[varcom.pr.toInt()].set("Y");
        }
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}
		return;
	}

	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			return;
		}
	}

	protected void update3000() {
		/* UPDATE-DATABASE */
		/** No database updates are required. */
		/* EXIT */
	}	

	protected void whereNext4000()
	{
		try {
			nextProgram4010();
			continue4080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nextProgram4010()
	{
		/* If selection terminated clear program and action stacks.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		chdrpfDao.deleteCacheObject(chdrpf);
		/* If not returning from a selection ACTION = ' ' then we must*/
		/*   store the next four Programs in the stack and load in the*/
		/*   generic programs according to Table T5671.*/
		/* If returning from a selection ACTION = '*' then sequentially*/
		/*   read down the subfile to find the next selected record.*/
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			initSelection4100();
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		/* Read the Subfile until a Selection has been made.*/
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4200();
		}
		/*if(!wsaaSelection.isTrue()){
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}*/
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		/* NOTE  With the limitations in subsequent transactions we have*/
		/* a problem with creation of poayments. They call AT which then*/
		/* fail because we still hold records. As a temp measure lets*/
		/* exit immediatly rather than display info to the user if in a*/
		/* create mode.*/
		
//		ILIFE-734 start-sgadkari
//		if (isNE(wsspcomn.flag, "C")) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				if (wsaaImmExit.isTrue()) {
					wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
					wsspcomn.nextprog.set(scrnparams.scrname);
					goTo(GotoLabel.exit4090);
				}
			}
//		}
//		ILIFE-734 end
		/* If the end of the subfile, exit to next initial program.*/
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		if (wsaaImmExit.isTrue()) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
	}

protected void continue4080()
	{
		/* Set up for execution of the component specific programs to be*/
		/* executed. Note that this includes a set of follow up records*/
		/* for each payment created according to table settings.*/
		chdrpf = chdrpfDao.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrnum.toString().trim());
		if (!(chdrpf.getUniqueNumber() > 0)) {
			fatalError600();
		}
		chdrpfDao.setCacheObject(chdrpf);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(wsaaProg);		
		/*UPDATE-SCREEN*/
		screenUpdate4600();
	}

protected void initSelection4100()
	{
		try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void saveNextProgs4110()
	{
		/* Save the  next  four  programs  after  P5186  into  Working*/
		/* Storage for re-instatement at the end of the Subfile.*/
		compute(wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			loop14120();
		}
		/* First Read of the Subfile for a Selection.*/
		scrnparams.function.set(varcom.sstrt);
		/* Go to the first  record  in  the Subfile to  find the First*/
		/* Selection.*/
		goTo(GotoLabel.exit4190);
	}

protected void loop14120()
	{
		/* This loop will load the next four  programs from WSSP Stack*/
		/* into a Working Storage Save area.*/
		wsaaSecProg[wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaSub1.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}

protected void next4200()
	{
		try {
			nextRec4210();
			ifSubfileEndp4220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nextRec4210()
	{
		/* Read next subfile record sequentially.*/
		processScreen("S5186", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4220()
	{
		/* Check for the end of the Subfile.*/
		/* If end of Subfile re-load the Saved four programs to Return*/
		/*   to initial stack order.*/
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			reloadProgsWssp4230();
			goTo(GotoLabel.exit4290);
		}
		/* Exit if a selection has been found. NOTE that we space*/
		/* and protect the selection as the subsequent transactions*/
		/* cannot handle multiple entry.*/
		if (isNE(sv.select, SPACES)) {
			
			sv.select.set(SPACES);
			
			wsaaSelectFlag.set("Y");
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.exit4290);
	}

protected void reloadProgsWssp4230()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		wsaaImmexitFlag.set("Y");
		compute(wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop34240();
		}
		goTo(GotoLabel.exit4290);
	}

protected void loop34240()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaSub1.toInt()].set(wsaaSecProg[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}


protected void screenUpdate4600()
	{
		/*PARA*/
		scrnparams.function.set(varcom.supd);
		processScreen("S5186", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	protected void para4000() {
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		chdrpfDao.deleteCacheObject(chdrpf);

		/* If returning from a program further down the stack then */
		/* bypass the start on the subfile. */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			bypassStart4010();
		}
		/* Re-start the subfile. */
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("sd5e2", sv);
		if (isNE(scrnparams.statuz, varcom.oK) && isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		bypassStart4010();
	}

	protected void bypassStart4010() {
		if (isEQ(sv.select, SPACES)) {
			while (!(isNE(sv.select, SPACES) || isEQ(scrnparams.statuz, varcom.endp))) {
				readSubfile4100();
			}
		}

		if (isEQ(scrnparams.statuz, varcom.endp) && isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}

		if (isEQ(scrnparams.statuz, varcom.endp) && isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			return;
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			nextProgram4085();
			return;
		}
		
		
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		
		nextProgram4085();
	}

	protected void nextProgram4085() {	
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

	protected void readSubfile4100() {
		scrnparams.function.set(varcom.srdn);
		processScreen("sd5e2", sv);
		if (isNE(scrnparams.statuz, varcom.oK) && isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void addsflData() {
		scrnparams.function.set(varcom.sadd);
		processScreen("Sd5e1", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

		protected void readSurr() {
		/* Begin on the coverage/rider record */
		if (firstTime1.isTrue()) {
			wsaaSwitch1.set(0); 
			validateStatusesWp1650();
			if (isEQ(wsaaValidStatus, "Y")) {
				processComponents1350();				
				wsaaHeldLoanAvail.set(wsaaLoanAvailable);
				if (isNE(t6598rec.calcprog, SPACES)) {
					calcSurr();
					if (isLT(wsaaHeldSurrVal, ZERO)){
						wsaaHeldSurrVal.set(mult(wsaaHeldSurrVal, (-1)));
					}					
					wsaaSurrValTot.add(wsaaHeldSurrVal);
				}
				wsaaHeldLoanAvail.add(wsaaZrcshamt);
				if (isGT(wsaaHeldLoanAvail, wsaaHeldCurrLoans1)) {
					compute(wsaaHeldLoanAvail, 2).set(sub(wsaaHeldLoanAvail, wsaaHeldCurrLoans1));
				} else {
					wsaaHeldLoanAvail.set(ZERO);
				}
				wsaaHeldTotLoanAvail.add(wsaaHeldLoanAvail);
				if(CTENQ003Permission) {
					wsaaTotalBonusAmnt.add(wsaaAcblCurrentBalance);
				}
			}
		}
	}

	protected void validateStatusesWp1650() {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679); /* IJTI-1479 */
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null == itempf) {
			sv.chdrnumErr.set(f321);
			return;
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaValidStatus = "N";

		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
			riskStatusCheckWp1660();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
				premStatusCheckWp1670();
			}
		}
	}

	protected void riskStatusCheckWp1660() {
		if (isEQ(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					wsaaValidStatus = "Y";
					return;
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					wsaaValidStatus = "Y";
					return;
				}
			}
		}
	}

	protected void premStatusCheckWp1670() {
		if (isEQ(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return;
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return;
				}
			}
		}
	}

	protected void processComponents1350() {
		/* Read table T5687 to find the surrender calculation subroutine */
		wsaaCrtable.set(covrpf.getCrtable());
		obtainSurrenderCalc1400();
		srcalcpy.currcode.set(covrpf.getPremCurrency());
		srcalcpy.planSuffix.set(covrpf.getPlanSuffix());
		/* check the reinsurance file at a later date and move */
		/* 'y' to this field if they are present */
		/* This Check never actually took place, but does now. */
		/* MOVE SPACES TO S5026-RIIND. */
		/* PERFORM 1980-READ-RACD. <A06843> */
		/* <A06843> */
		/* IF RACDMJA-STATUZ = O-K <A06843> */
		/* MOVE 'Y' TO S5026-RIIND <A06843> */
		/* END-IF. <A06843> */
		srcalcpy.endf.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
		srcalcpy.lifeLife.set(covrpf.getLife());
		srcalcpy.lifeJlife.set(covrpf.getJlife());
		srcalcpy.covrCoverage.set(covrpf.getCoverage());
		srcalcpy.covrRider.set(covrpf.getRider());
		srcalcpy.crtable.set(covrpf.getCrtable());
		srcalcpy.crrcd.set(covrpf.getCrrcd());
		srcalcpy.convUnits.set(covrpf.getConvertInitialUnits());
		srcalcpy.pstatcode.set(covrpf.getPstatcode());
		srcalcpy.status.set(SPACES);
		//readchdrTable
		srcalcpy.polsum.set(chdrpf.getPolsum());
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.ptdate.set(chdrpf.getPtdate());
		srcalcpy.chdrCurr.set(chdrpf.getCntcurr());
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		} else {
			srcalcpy.billfreq.set(payrpf.getBillfreq());
		}
		wsaaFeeTax.set(ZERO);
		while (isNE(srcalcpy.status, varcom.mrnf) && !(isEQ(srcalcpy.status, varcom.endp))) {	   
			callSurMethodWhole1600();
		}
		if(CTENQ003Permission) {
		    	if(isNE(t6598recBonus.calcprog,SPACES)){
		    		callBonusMethod1610();
		    		
		    	}
         }
		if (isNE(t5687rec.loanmeth, SPACES)) {
			compute(wsaaCompLoanVal, 2).set(mult(wsaaComponentTot, t6632rec.maxpcnt));
			compute(wsaaCompLoanVal, 2).set(div(wsaaCompLoanVal, 100));
			zrdecplrec.amountIn.set(wsaaCompLoanVal);
			callRounding6000();
			wsaaCompLoanVal.set(zrdecplrec.amountOut);
			wsaaLoanAvailable.add(wsaaCompLoanVal);
		}	
	}

	protected void obtainSurrenderCalc1400() {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaCrtable.toString());
		itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
		itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));

		itempf.setItemtabl(t5687);
		t5687List = itempfDAO.findByItemDates(itempf);

		if (t5687List.size() == 0) {
			scrnparams.errorCode.set(f294);
			syserrrec.params.set(itempf);
			fatalError600();
		} else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687List.get(0).getGenarea()));
		}

        if(CTENQ003Permission) {
            itempf = new Itempf();
            itempf.setItempfx("IT");
            itempf.setItemcoy(wsspcomn.company.toString());
            itempf.setItemitem(wsaaCrtable.toString());
            itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
            itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));
            itempf.setItemtabl(t6640);
            t6640List = itempfDAO.findByItemDates(itempf);
            if (t6640List.size() != 0) {
                t6640rec.t6640Rec.set(StringUtil.rawToString(t6640List.get(0).getGenarea()));
            }

            itempf = new Itempf();
            itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), t6598, t6640rec.surrenderBonusMethod.toString());
            if (itempf == null) {
                t6598recBonus.t6598Rec.set(SPACES);
            } else {
                t6598recBonus.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            }
        }

		itempf = new Itempf();
		itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), t6598, t5687rec.svMethod.toString());

		if (itempf == null) {
			t6598rec.t6598Rec.set(SPACES);
		} else {
			t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}

		if (isNE(t5687rec.loanmeth, SPACES)) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemitem(t5687rec.loanmeth.toString());
			itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
			itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));

			itempf.setItemtabl(t6632);
			t6632List = itempfDAO.findByItemDates(itempf);

			if (t6632List.size() == 0) {
				t6632rec.t6632Rec.set(SPACES);
			} else {
				t6632rec.t6632Rec.set(StringUtil.rawToString(t6632List.get(0).getGenarea()));
			}
		}
	}

	protected void callBonusMethod1610(){
		srcalcpyBonus.currcode.set(covrpf.getPremCurrency());
		srcalcpyBonus.planSuffix.set(covrpf.getPlanSuffix());
		srcalcpyBonus.endf.set(SPACES);
		srcalcpyBonus.tsvtot.set(ZERO);
		srcalcpyBonus.tsv1tot.set(ZERO);
		srcalcpyBonus.estimatedVal.set(ZERO);
		srcalcpyBonus.actualVal.set(ZERO);
		srcalcpyBonus.chdrChdrcoy.set(covrpf.getChdrcoy());
		srcalcpyBonus.chdrChdrnum.set(covrpf.getChdrnum());
		srcalcpyBonus.lifeLife.set(covrpf.getLife());
		srcalcpyBonus.lifeJlife.set(covrpf.getJlife());
		srcalcpyBonus.covrCoverage.set(covrpf.getCoverage());
		srcalcpyBonus.covrRider.set(covrpf.getRider());
		srcalcpyBonus.crtable.set(covrpf.getCrtable());
		srcalcpyBonus.crrcd.set(covrpf.getCrrcd());
		srcalcpyBonus.convUnits.set(covrpf.getConvertInitialUnits());
		srcalcpyBonus.pstatcode.set(covrpf.getPstatcode());
		//srcalcpyBonus.status.set(SPACES);
		//readchdrTable
		srcalcpyBonus.polsum.set(chdrpf.getPolsum());
		srcalcpyBonus.language.set(wsspcomn.language);
		srcalcpyBonus.ptdate.set(chdrpf.getPtdate());
		srcalcpyBonus.chdrCurr.set(chdrpf.getCntcurr());
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpyBonus.billfreq.set("00");
		} else {
			srcalcpyBonus.billfreq.set(payrpf.getBillfreq());
		}

		srcalcpyBonus.currcode.set(chdrpf.getCntcurr());
		srcalcpyBonus.effdate.set(wsaaToday);
		srcalcpyBonus.type.set("F");
		srcalcpyBonus.status.set("BONS");
		srcalcpyBonus.singp.set(covrpf.getInstprem());
		wsaaAcblCurrentBalance.set(ZERO);
		while ( !(isEQ(srcalcpyBonus.status, varcom.endp))) {
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598recBonus.calcprog.toString())))
			{
				callProgram(t6598recBonus.calcprog, srcalcpyBonus.surrenderRec);
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(srcalcpyBonus.surrenderRec);
				vpxsurcrec.function.set("INIT");
				callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
				srcalcpyBonus.surrenderRec.set(vpmcalcrec.linkageArea);
				vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
				vpmfmtrec.initialize();
				vpmfmtrec.amount02.set(wsaaEstimateTot);

				callProgram(t6598recBonus.calcprog, srcalcpyBonus.surrenderRec, vpmfmtrec, vpxsurcrec,chdrpf);//VPMS call


				if(isEQ(srcalcpyBonus.type,"L"))
				{
					vpmcalcrec.linkageArea.set(srcalcpyBonus.surrenderRec);
					callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
					srcalcpyBonus.surrenderRec.set(vpmcalcrec.linkageArea);
					srcalcpyBonus.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpyBonus.type,"C"))
				{
					srcalcpyBonus.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpyBonus.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
				{
					srcalcpyBonus.status.set(varcom.endp);
				}
				else
				{
					srcalcpyBonus.status.set(varcom.oK);
				}
			}

			/*IVE-796 RUL Product - Partial Surrender Calculation end*/
			if (isNE(srcalcpyBonus.status, varcom.oK)
					&& isNE(srcalcpyBonus.status, varcom.endp)) {
				syserrrec.statuz.set(srcalcpyBonus.status);
				fatalError600();
			}

		wsaaStoredReserve.set(srcalcpyBonus.actualVal);
		zrdecplrec.amountIn.set(wsaaStoredReserve);
		callRounding6000();
		wsaaStoredReserve.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpyBonus.estimatedVal);
		callRounding6000();
		srcalcpyBonus.estimatedVal.set(zrdecplrec.amountOut);
		wsaaAcblCurrentBalance.add(srcalcpyBonus.estimatedVal);
		/*  If notional policy then divide the summary record figures*/
		/*  by number of policies summarised.*/
		if (singleComponent.isTrue()
				&& isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			compute(wsaaAcblCurrentBalance, 2).set(div(wsaaAcblCurrentBalance, chdrpf.getPolsum()));
			compute(wsaaStoredReserve, 2).set(div(wsaaStoredReserve, chdrpf.getPolsum()));
			}
		}
		zrdecplrec.amountIn.set(wsaaAcblCurrentBalance);
		callRounding6000();
		wsaaAcblCurrentBalance.set(zrdecplrec.amountOut);

    }

	protected void callSurMethodWhole1600() {
		srcalcpy.effdate.set(wsaaToday);
		srcalcpy.type.set("F");
		/* IF COVRCLM-INSTPREM > ZERO */
		srcalcpy.singp.set(covrpf.getInstprem());
		/* ELSE */
		/* MOVE COVRCLM-SINGP TO SURC-SINGP. */
		/* if no surrender method found print zeros as surrender value */
		if (isEQ(t6598rec.calcprog, SPACES)) {
			srcalcpy.status.set(varcom.endp);
			return;
			/* GO TO 1630-ADD-TO-SUBFILE. */
		}

		// callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		if (!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) {
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		} else {
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec, vpxsurcrec); // IO
																			// read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec, chdrpf);// VPMS
																									// call
			// callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);

			if (isEQ(srcalcpy.type, "L")) {
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			} else if (isEQ(srcalcpy.type, "C")) {
				srcalcpy.status.set(varcom.endp);
			} else if (isEQ(srcalcpy.type, "A") && vpxsurcrec.statuz.equals(varcom.endp)) {
				srcalcpy.endf.set("Y");
			} else {
				srcalcpy.status.set(varcom.oK);
			}
			if (vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
		}

		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.status, varcom.oK) && isNE(srcalcpy.status, varcom.endp) && isNE(srcalcpy.status, "NOPR")
				&& isNE(srcalcpy.status, varcom.mrnf)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, "NOPR")) {
			wsaaNoPrice = "Y";
			return;
		}
		if (isEQ(srcalcpy.status, varcom.oK) || isEQ(srcalcpy.status, varcom.endp)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				srcalcpy.currcode.set(chdrpf.getCntcurr());
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		if (isEQ(srcalcpy.type, "C")) {
			compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
		}
		if (isEQ(srcalcpy.type, "C") || isEQ(srcalcpy.type, "J")) {
			if (isNE(tr52drec.txcode, SPACES)) {
				callTaxsubr();
			}
		}
		/* Check the amount returned for being negative. In the case of */
		/* SUM products this is possible and so set these values to zero. */
		/* Note SUM products do not have to have the same PT & BT dates. */
		checkT56112700();
		if (isNE(wsaaSumFlag, SPACES)) {
			if (isLT(srcalcpy.actualVal, ZERO)) {
				srcalcpy.actualVal.set(ZERO);
			}
		}
		/* IF SURC-ENDF = 'Y' */
		/* GO TO 1640-EXIT. */
		if (isEQ(srcalcpy.estimatedVal, ZERO) && isEQ(srcalcpy.actualVal, ZERO)) {
			return;
		}

		/* IF S5026-SHORTDS NOT = COVRCLM-CRTABLE <A06843> */
		/* MOVE SPACES TO S5026-RIIND <A06843> */
		/* END-IF. <A06843> */
		/* If the description is 'PENALTY', as set up in subroutine */
		/* UNLSURC then subract this amount to give a true actual value. */
		/* IF FIRST-TIME */
		/* MOVE 0 TO WSAA-SWITCH2 */
		/* MOVE S5026-CNSTCUR TO WSAA-STORED-CURRENCY */
		/* ADD SURC-ESTIMATED-VAL TO WSAA-ESTIMATE-TOT */
		/* ADD SURC-ACTUAL-VAL TO WSAA-ACTUAL-TOT */
		/* ELSE */
		/* IF SURC-CURRCODE = WSAA-STORED-CURRENCY */
		/* IF S5026-CNSTCUR = WSAA-STORED-CURRENCY <010> */
		/* ADD SURC-ESTIMATED-VAL TO WSAA-ESTIMATE-TOT */
		/* ADD SURC-ACTUAL-VAL TO WSAA-ACTUAL-TOT */
		/* ELSE */
		/* MOVE ZEROES TO WSAA-ESTIMATE-TOT */
		/* WSAA-ACTUAL-TOT */
		/* MOVE 1 TO WSAA-CURRENCY-SWITCH. */
		if (firstTime.isTrue()) {
			wsaaSwitch2.set(0);
		}
		compute(wsaaEstimateTot, 2).set(add(wsaaEstimateTot, srcalcpy.estimatedVal));
		/* IF SURC-DESCRIPTION = 'PENALTY' <CAS1 */
		if (isEQ(srcalcpy.type, "C")) {
			compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
			compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
		} else {
			compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
		}

		/* Add values returned to our running Surrender value total */
		wsaaComponentTot.add(srcalcpy.actualVal);
		wsaaComponentTot.add(srcalcpy.estimatedVal);
		/* Check the effective date of the surrender against the paid to */
		/* date of the contract. If there are to be premiums paid or */
		/* refunded then call the appropriate subroutine held on T5611. */
		/* Note that if the item does not exist then the coverage does not */
		/* use the SUM calculation package & is hence not applicable.... */
		if (isNE(srcalcpy.effdate, srcalcpy.ptdate) && isNE(wsaaSumFlag, SPACES) && isNE(t5611rec.calcprog, SPACES)) {
			checkPremadj2600();
		}
	}

	protected void callTaxsubr() {
		/* Read table TR52E */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		readTr52e5300();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		/* IF TR52E-TAXIND-05 NOT = 'Y' */
		if (isNE(tr52erec.taxind03, "Y")) {
			return;
			/**** GO TO 5190-EXIT <S19FIX> */
		}
		/* Call tax subroutine */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(covrpf.getLife());
		txcalcrec.coverage.set(covrpf.getCoverage());
		txcalcrec.rider.set(covrpf.getRider());
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		wsaaCntCurr.set(chdrpf.getCntcurr());

		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.cntTaxInd.set(SPACES);
		/* MOVE WSAA-TOTAL-FEE TO TXCL-AMOUNT-IN. */
		txcalcrec.amountIn.set(srcalcpy.actualVal);
		txcalcrec.transType.set("SURF");
		txcalcrec.effdate.set(wsaaToday);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO) || isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

	protected void readTr52e5300() {
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())) || (isNE(itdmIO.getItemtabl(), tr52e))
				|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey)) || (isEQ(itdmIO.getStatuz(), varcom.endp)))
				&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(wsaaTr52eKey);
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), chdrpf.getChdrcoy())) && (isEQ(itdmIO.getItemtabl(), tr52e))
				&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey)) && (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

	protected void checkPremadj2600() {

		/* Call the surrender routine for premium adjustments. Note that */
		/* for the moment this uses the same copy-book as the surrender */
		/* routine, which is stored and then re-instated ......... */
		wsaaSurrenderRecStore.set(srcalcpy.surrenderRec);
		/* MOVE CHDRSUR-BILLFREQ TO WSAA-BILLFREQ. <A05691> */
		/* If this is a Single Premium Component then use a Billing */
		/* frequency of '00'. */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		} else {
			srcalcpy.billfreq.set(payrpf.getBillfreq());
		}
		srcalcpy.estimatedVal.set(covrpf.getInstprem());
		srcalcpy.actualVal.set(ZERO);
		callProgram(t5611rec.calcprog, srcalcpy.surrenderRec);
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		} else {
			if (isNE(srcalcpy.status, varcom.mrnf) && isNE(srcalcpy.status, varcom.oK)
					&& isNE(srcalcpy.status, varcom.endp)) {
				syserrrec.params.set(srcalcpy.surrenderRec);
				syserrrec.statuz.set(srcalcpy.status);
				fatalError600();
			}
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		zrdecplrec.currency.set(covrpf.getPremCurrency());
		callRounding6000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* Note adjustments are subtracted from the total. */
		/* The actual value here is the premium adjustment returned... */
		wsaaOtheradjst.add(srcalcpy.actualVal);
		srcalcpy.surrenderRec.set(wsaaSurrenderRecStore);
	}

	protected void checkT56112700() {
		/* Read the Coverage Surrender/Paid-Up/Bonus parameter table. */
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaT5611Item.toString());
		wsaaT5611Crtable.set(srcalcpy.crtable);
		wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
		itempf.setItmfrm(new BigDecimal(srcalcpy.effdate.toInt()));
		itempf.setItmto(new BigDecimal(srcalcpy.effdate.toInt()));

		itempf.setItemtabl(t5611);
		t5611List = itempfDAO.findByItemDates(itempf);

		if (t5611List.size() == 0) {
			wsaaSumFlag.set(SPACES);
		} else {
			t5611rec.t5611Rec.set(StringUtil.rawToString(t5611List.get(0).getGenarea()));
			wsaaSumFlag.set("Y");
		}
	}

	protected void calcSurr() {
		/* Computation of tax amount to be imposed. */

		if (!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("SURTAX")
				&& er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable()))) /* IJTI-1479 */
		{
			getTaxAmount1880();
		} else {
			SurtaxRec surtaxRec = new SurtaxRec();
			surtaxRec.cnttype.set(chdrpf.getCnttype());
			surtaxRec.cntcurr.set(chdrpf.getCntcurr());
			surtaxRec.effectiveDate.set(wsaaToday);
			surtaxRec.occDate.set(chdrpf.getOccdate());
			surtaxRec.actualAmount.set(wsaaActualTot);
			callProgram("SURTAX", surtaxRec.surtaxRec);
			wsaaTaxAmt.set(surtaxRec.taxAmount);
		}
		wsaaTaxAmt.add(wsaaFeeTax);

		/* Get the value of any Loans held against this component. */
		getLoanDetails1900();
		getPolicyDebt1990();

		/* COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT + */
		/* S5026-POLICYLOAN */
		/* COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT - */
		/* S5026-POLICYLOAN */
		
		compute(wsaaHeldSurrVal, 2)
				.set(sub(sub(add(sub(wsaaActualTot, wsaapolicyloan), wsaaZrcshamt), wsaaTdbtamt), wsaaTaxAmt));
		if (isLT(wsaaHeldSurrVal, ZERO)){
			wsaaHeldSurrVal.set(mult(wsaaHeldSurrVal, (-1)));
		}
		/* In the case of SUM products with premium adjustments add the */
		/* premium adjustments to the total value displayed on screen... */
		if (isNE(wsaaSumFlag, SPACES) && isNE(wsaaOtheradjst, ZERO)) {
			wsaaHeldSurrVal.add(wsaaOtheradjst);
		}
		/*ILIFE-6277 SA*/
		wsaaHeldSurrVal.set(sub(wsaaEstimateTot,wsaaHeldSurrVal));
		/* MOVE CHDRSUR-CNTCURR TO S5026-CURRCD. */

	}

	protected void readTr52d5120() {
		itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), tr52d, chdrpf.getReg());
		if (itempf == null) {
			itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), tr52d, "***");
			if (itempf == null) {
				syserrrec.statuz.set("MRNF");
				syserrrec.params.set(itempf);
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));

	}

	protected void getTaxAmount1880() {

		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrpf.getCnttype());
		stringVariable1.addExpression(chdrpf.getCntcurr());
		itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), tr691, stringVariable1.toString());

		if (itempf == null) {
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression("***");
			stringVariable3.addExpression(chdrpf.getCntcurr());
			itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), tr691, stringVariable3.toString());
			if (itempf == null) {
				return;
			}
		}
		tr691rec.tr691Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.intDate1.set(chdrpf.getOccdate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isGTE(datcon3rec.freqFactor, tr691rec.tyearno)) {
			return;
		}
		if (isNE(tr691rec.pcnt, 0)) {
			// MIBT-111
			// compute(wsaaTaxAmt, 3).setRounded(mult(wsaaActualTot,
			// (div(tr691rec.pcnt, 100))));
			compute(wsaaTaxAmt, 3).setRounded(div(mult(wsaaActualTot, tr691rec.pcnt), 100));
		}
		if (isNE(tr691rec.flatrate, 0)) {
			wsaaTaxAmt.set(tr691rec.flatrate);
		}
		zrdecplrec.amountIn.set(wsaaTaxAmt);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		callRounding6000();
		wsaaTaxAmt.set(zrdecplrec.amountOut);
	}


	protected void getLoanDetails1900() {

		/**
		 * <pre>
		*  Get the details of all loans currently held against this       
		*  Contract. If this is not the first component within the        
		*  current Surrender transaction then need to read the SURD       
		*  to get details of all previous surrender records for this      
		*  transaction.                                                   
		*  If there are no records (there will be none for Unit Linked),  
		*  then call TOTLOAN to get the current loan value. If there are  
		*  records then sum up their values before calling TOTLOAN and    
		*  subtracting the total value from the LOAN VALUE returned from  
		*  TOTLOAN.                                                       
		*  Previous surrenders may have been done in a different          
		*  currency to the present one and hence, a check should be       
		*  made for this and a conversion done where necessary before     
		*  the accumulation is done. Note that for the initial screen     
		*  display, the currency will always be CHDR currency.            
		*  Note also that TOTLOAN always returns details in the CHDR      
		*  currency.
		 * </pre>
		 */

		surdclmIO.setDataArea(SPACES);
		surdclmIO.setChdrcoy(chdrpf.getChdrcoy());
		surdclmIO.setChdrnum(chdrpf.getChdrnum());
		surdclmIO.setLife(ZERO);
		surdclmIO.setCoverage(ZERO);
		surdclmIO.setRider(ZERO);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setTranno(chdrpf.getTranno());
		surdclmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		surdclmIO.setFormat(surdclmrec);
		surdclmIO.setStatuz(varcom.oK);
		wsaaLoanValue.set(0);
		/* Read all SURD records currently unprocessed. */
		while (!(isEQ(surdclmIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, surdclmIO);
			if (isNE(surdclmIO.getStatuz(), varcom.oK) && isNE(surdclmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(surdclmIO.getParams());
				fatalError600();
			}
			if (isNE(chdrpf.getChdrnum(), surdclmIO.getChdrnum()) || isNE(chdrpf.getChdrcoy(), surdclmIO.getChdrcoy())
					|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
				surdclmIO.setStatuz(varcom.endp);
			} else {
				if (isNE(surdclmIO.getCurrcd(), chdrpf.getCntcurr())) {
					readjustSurd1920();
				} else {
					wsaaActvalue.set(surdclmIO.getActvalue());
				}
				wsaaLoanValue.add(wsaaActvalue);
			}
			surdclmIO.setFunction(varcom.nextr);
		}

		/* Read TOTLOAN to get value of loans for this contract. */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(wsaaToday);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		/* CALL 'TOTLOAN' USING TOTL-TOTLOAN-REC. */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		compute(wsaaHeldCurrLoans1, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isLT(wsaaLoanValue, wsaaHeldCurrLoans)) {
			wsaaHeldCurrLoans.subtract(wsaaLoanValue);
		} else {
			wsaaHeldCurrLoans.set(0);
		}

		wsaapolicyloan.set(totloanrec.principal);
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(wsaaToday);
		totloanrec.function.set("CASH");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaZrcshamt, 2).set(add(totloanrec.principal, totloanrec.interest));
		/* Change the sign to reflect the Client's context. */
		if (isLT(wsaaZrcshamt, 0)) {
			compute(wsaaZrcshamt, 2).set(mult(wsaaZrcshamt, (-1)));
		}
	}

	protected void readjustSurd1920() {
		/* Convert the SURD into the CHDR currency. */
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(surdclmIO.getCurrcd());
		/* MOVE VRCM-MAX-DATE TO CLNK-CASHDATE. <LA4958> */
		conlinkrec.cashdate.set(wsaaToday);
		conlinkrec.currOut.set(chdrpf.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(surdclmIO.getActvalue());
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		wsaaActvalue.set(conlinkrec.amountOut);
	}

	protected void getPolicyDebt1990() {
		wsaaTdbtamt.set(ZERO);
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrpf.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrpf.getChdrnum());
		tpoldbtIO.setTranno(ZERO);
		tpoldbtIO.setFormat(tpoldbtrec);
		tpoldbtIO.setFunction(varcom.begn);
		tpoldbtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tpoldbtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		read1991();
	}

	protected void read1991() {
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK) && isNE(tpoldbtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			fatalError600();
		}
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK) || isNE(tpoldbtIO.getChdrcoy(), chdrpf.getChdrcoy())
				|| isNE(tpoldbtIO.getChdrnum(), chdrpf.getChdrnum())) {
			return;
		}
		wsaaTdbtamt.add(tpoldbtIO.getTdbtamt());
		tpoldbtIO.setFunction(varcom.nextr);
		read1991();
		return;
	}

	protected void callRounding6000() {
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
	}
}