package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6716
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6716ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	/*public FixedLengthStringData dataArea = new FixedLengthStringData(550+DD.relationwithowner.length+12+2+12);
	public FixedLengthStringData dataFields = new FixedLengthStringData(166+DD.relationwithowner.length+2).isAPartOf(dataArea, 0);*/
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbage = DD.anbage.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData bmi = DD.bmi.copyToZonedDecimal().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,7);
	public ZonedDecimalData dob = DD.dob.copyToZonedDecimal().isAPartOf(dataFields,15);
	public FixedLengthStringData dummy = DD.dummy.copy().isAPartOf(dataFields,23);
	public ZonedDecimalData height = DD.height.copyToZonedDecimal().isAPartOf(dataFields,24);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData occup = DD.occup.copy().isAPartOf(dataFields,86);
	public FixedLengthStringData optdscs = new FixedLengthStringData(30).isAPartOf(dataFields, 90);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(2, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optinds = new FixedLengthStringData(2).isAPartOf(dataFields, 120);
	public FixedLengthStringData[] optind = FLSArrayPartOfStructure(2, 1, optinds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(optinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01 = DD.optind.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optind02 = DD.optind.copy().isAPartOf(filler1,1);
	public FixedLengthStringData pursuits = new FixedLengthStringData(12).isAPartOf(dataFields, 122);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(2, 6, pursuits, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler2,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler2,6);
	public FixedLengthStringData relation = DD.relation.copy().isAPartOf(dataFields,134);
	public FixedLengthStringData rollit = DD.rollit.copy().isAPartOf(dataFields,136);
	public FixedLengthStringData selection = DD.selection.copy().isAPartOf(dataFields,137);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(dataFields,141);
	public FixedLengthStringData smoking = DD.smoking.copy().isAPartOf(dataFields,142);
	public ZonedDecimalData weight = DD.weight.copyToZonedDecimal().isAPartOf(dataFields,144);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData relationwithowner = DD.relationwithowner.copy().isAPartOf(dataFields,153+zagelit.length()); //fwang3 ICIL-4
	public FixedLengthStringData occupcls = DD.statcode.copy().isAPartOf(dataFields,153+zagelit.length()+4); //fwang3
	
	
	/*public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 166+DD.relationwithowner.length+2);
	*/
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData anbageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bmiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData dummyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData heightErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData occupErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(2, 4, optdscsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData optindsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] optindErr = FLSArrayPartOfStructure(2, 4, optindsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(optindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData optind02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(2, 4, pursuitsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData rollitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData selectionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData sexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData smokingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData weightErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	
	/*public FixedLengthStringData outputIndicators = new FixedLengthStringData(288+24).isAPartOf(dataArea, 262+DD.relationwithowner.length);
	 */ 
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] anbageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bmiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] dummyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] heightOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] occupOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(2, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData optindsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(2, 12, optindsOut, 0);
	public FixedLengthStringData[][] optindO = FLSDArrayPartOfArrayStructure(12, 1, optindOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(24).isAPartOf(optindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optind01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] optind02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(2, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] rollitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] selectionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] weightOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] relationwithownerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276+12);
	public FixedLengthStringData[] occupclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276+24);
	
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dobDisp = new FixedLengthStringData(10);

	public LongData S6716screenWritten = new LongData(0);
	public LongData S6716protectWritten = new LongData(0);
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387

	public static int[] screenSflPfInds = new int[]{1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24};
	
	public boolean hasSubfile() {
		return false;
	}


	public S6716ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(heightOut,new String[] {null, null, null, "02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(weightOut,new String[] {null, null, null, "02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind01Out,new String[] {"04","01","-04","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {"05","02","-05","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rollitOut,new String[] {null, null, null, "53",null, null, null, null, null, null, null, null});
		fieldIndMap.put(dummyOut,new String[] {null, null, null, "54",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bmiOut,new String[] {null, null, null, "02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(relationwithownerOut,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null}); //fwang3
		fieldIndMap.put(occupclsOut,new String[] {null, null, null, "04",null, null, null, null, null, null, null, null}); //fwang3
		fieldIndMap.put(relationOut,new String[] {null, null, null, "06",null, null, null, null, null, null, null, null});//ILJ-387
		/*screenFields = new BaseData[] {chdrnum, life, jlife, lifenum, lifename, sex, dob, zagelit, anbage, selection, smoking, occup, pursuit01, pursuit02, height, weight, optdsc01, optind01, optdsc02, optind02, relation, rollit, dummy, bmi, relationwithowner,occupcls};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, jlifeOut, lifenumOut, lifenameOut, sexOut, dobOut, zagelitOut, anbageOut, selectionOut, smokingOut, occupOut, pursuit01Out, pursuit02Out, heightOut, weightOut, optdsc01Out, optind01Out, optdsc02Out, optind02Out, relationOut, rollitOut, dummyOut, bmiOut,relationwithownerOut,occupclsOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, jlifeErr, lifenumErr, lifenameErr, sexErr, dobErr, zagelitErr, anbageErr, selectionErr, smokingErr, occupErr, pursuit01Err, pursuit02Err, heightErr, weightErr, optdsc01Err, optind01Err, optdsc02Err, optind02Err, relationErr, rollitErr, dummyErr, bmiErr};
		screenDateFields = new BaseData[] {dob};
		screenDateErrFields = new BaseData[] {dobErr};
		screenDateDispFields = new BaseData[] {dobDisp};*/

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6716screen.class;
		protectRecord = S6716protect.class;
	}
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	
	public int getDataAreaSize() {
		return 580;
	}
	

	public int getDataFieldsSize(){
		return 172;  
	}
	public int getErrorIndicatorSize(){
		return 96; 
	}
	
	public int getOutputFieldSize(){
		return 312; 
	}


	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, life, jlife, lifenum, lifename, sex, dob, zagelit, anbage, selection, smoking, occup, pursuit01, pursuit02, height, weight, optdsc01, optind01, optdsc02, optind02, relation, rollit, dummy, bmi, relationwithowner,occupcls};   //IFSU-2036
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, lifeOut, jlifeOut, lifenumOut, lifenameOut, sexOut, dobOut, zagelitOut, anbageOut, selectionOut, smokingOut, occupOut, pursuit01Out, pursuit02Out, heightOut, weightOut, optdsc01Out, optind01Out, optdsc02Out, optind02Out, relationOut, rollitOut, dummyOut, bmiOut,relationwithownerOut,occupclsOut}; //IFSU-2036
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, lifeErr, jlifeErr, lifenumErr, lifenameErr, sexErr, dobErr, zagelitErr, anbageErr, selectionErr, smokingErr, occupErr, pursuit01Err, pursuit02Err, heightErr, weightErr, optdsc01Err, optind01Err, optdsc02Err, optind02Err, relationErr, rollitErr, dummyErr, bmiErr};
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {dob};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {dobDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {dobErr};
	}
}
