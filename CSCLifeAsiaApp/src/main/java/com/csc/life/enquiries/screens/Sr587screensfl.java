package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr587screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 17;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 20, 4, 59}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr587ScreenVars sv = (Sr587ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr587screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr587screensfl, 
			sv.Sr587screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr587ScreenVars sv = (Sr587ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr587screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr587ScreenVars sv = (Sr587ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr587screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr587screensflWritten.gt(0))
		{
			sv.sr587screensfl.setCurrentIndex(0);
			sv.Sr587screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr587ScreenVars sv = (Sr587ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr587screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr587ScreenVars screenVars = (Sr587ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coy.setFieldName("coy");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.cnttyp.setFieldName("cnttyp");
				screenVars.crtable.setFieldName("crtable");
				screenVars.currcode.setFieldName("currcode");
				screenVars.sumins.setFieldName("sumins");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.coy.set(dm.getField("coy"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.cnttyp.set(dm.getField("cnttyp"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.currcode.set(dm.getField("currcode"));
			screenVars.sumins.set(dm.getField("sumins"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr587ScreenVars screenVars = (Sr587ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coy.setFieldName("coy");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.cnttyp.setFieldName("cnttyp");
				screenVars.crtable.setFieldName("crtable");
				screenVars.currcode.setFieldName("currcode");
				screenVars.sumins.setFieldName("sumins");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("coy").set(screenVars.coy);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("cnttyp").set(screenVars.cnttyp);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("currcode").set(screenVars.currcode);
			dm.getField("sumins").set(screenVars.sumins);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr587screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr587ScreenVars screenVars = (Sr587ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.coy.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.cnttyp.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.currcode.clearFormatting();
		screenVars.sumins.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr587ScreenVars screenVars = (Sr587ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.coy.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttyp.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.sumins.setClassString("");
	}

/**
 * Clear all the variables in Sr587screensfl
 */
	public static void clear(VarModel pv) {
		Sr587ScreenVars screenVars = (Sr587ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.coy.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttyp.clear();
		screenVars.crtable.clear();
		screenVars.currcode.clear();
		screenVars.sumins.clear();
	}
}
