package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6363screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6363ScreenVars sv = (S6363ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6363screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6363ScreenVars screenVars = (S6363ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.submnuprog.setClassString("");
		screenVars.action.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.clttwo.setClassString("");
		screenVars.surname.setClassString("");
		screenVars.givname.setClassString("");
		screenVars.role.setClassString("");
		screenVars.cltdobDisp.setClassString("");
		screenVars.cltsex.setClassString("");
		screenVars.cltpcode.setClassString("");
	}

/**
 * Clear all the variables in S6363screen
 */
	public static void clear(VarModel pv) {
		S6363ScreenVars screenVars = (S6363ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.submnuprog.clear();
		screenVars.action.clear();
		screenVars.chdrsel.clear();
		screenVars.clttwo.clear();
		screenVars.surname.clear();
		screenVars.givname.clear();
		screenVars.role.clear();
		screenVars.cltdobDisp.clear();
		screenVars.cltdob.clear();
		screenVars.cltsex.clear();
		screenVars.cltpcode.clear();
	}
}
