package com.csc.life.enquiries.dataaccess;

import com.csc.fsu.financials.dataaccess.RtrnpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RtrntrnTableDAM.java
 * Date: Sun, 30 Aug 2009 03:46:35
 * Class transformed from RTRNTRN.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RtrntrnTableDAM extends RtrnpfTableDAM {

	public RtrntrnTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("RTRNTRN");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "RLDGCOY"
		             + ", RDOCNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "RLDGCOY, " +
		            "BATCTRCDE, " +
		            "RDOCNUM, " +
		            "TRANNO, " +
		            "GLCODE, " +
		            "SACSCODE, " +
		            "SACSTYP, " +
		            "ORIGAMT, " +
		            "ORIGCCY, " +
		            "ACCTAMT, " +
		            "RLDGACCT, " +
		            "GENLCUR, " +
		            "GLSIGN, " +
		            "EFFDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "RLDGCOY ASC, " +
		            "RDOCNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "RLDGCOY DESC, " +
		            "RDOCNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               rldgcoy,
                               batctrcde,
                               rdocnum,
                               tranno,
                               glcode,
                               sacscode,
                               sacstyp,
                               origamt,
                               origccy,
                               acctamt,
                               rldgacct,
                               genlcur,
                               glsign,
                               effdate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(243);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRtrnrecKeyData() {
		return getRecKeyData();
	}
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getRldgcoy().toInternal()
					+ getRdocnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRtrnrecKeyData(Object obj) {
		return setRecKeyData(obj);
	}
	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, rldgcoy);
			what = ExternalData.chop(what, rdocnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(9);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(rldgcoy.toInternal());
	nonKeyFiller30.setInternal(rdocnum.toInternal());
	nonKeyFiller40.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRtrnrecNonKeyData() {
		return getRecNonKeyData();
	}
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(127);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ getBatctrcde().toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getGlcode().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getOrigamt().toInternal()
					+ getOrigccy().toInternal()
					+ getAcctamt().toInternal()
					+ getRldgacct().toInternal()
					+ getGenlcur().toInternal()
					+ getGlsign().toInternal()
					+ getEffdate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRtrnrecNonKeyData(Object obj) {
		return setRecNonKeyData(obj);
	}
	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, glcode);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, origamt);
			what = ExternalData.chop(what, origccy);
			what = ExternalData.chop(what, acctamt);
			what = ExternalData.chop(what, rldgacct);
			what = ExternalData.chop(what, genlcur);
			what = ExternalData.chop(what, glsign);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(Object what) {
		rldgcoy.set(what);
	}
	public FixedLengthStringData getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(Object what) {
		rdocnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getGlcode() {
		return glcode;
	}
	public void setGlcode(Object what) {
		glcode.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}	
	public PackedDecimalData getOrigamt() {
		return origamt;
	}
	public void setOrigamt(Object what) {
		setOrigamt(what, false);
	}
	public void setOrigamt(Object what, boolean rounded) {
		if (rounded)
			origamt.setRounded(what);
		else
			origamt.set(what);
	}	
	public FixedLengthStringData getOrigccy() {
		return origccy;
	}
	public void setOrigccy(Object what) {
		origccy.set(what);
	}	
	public PackedDecimalData getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(Object what) {
		setAcctamt(what, false);
	}
	public void setAcctamt(Object what, boolean rounded) {
		if (rounded)
			acctamt.setRounded(what);
		else
			acctamt.set(what);
	}	
	public FixedLengthStringData getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(Object what) {
		rldgacct.set(what);
	}	
	public FixedLengthStringData getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(Object what) {
		genlcur.set(what);
	}	
	public FixedLengthStringData getGlsign() {
		return glsign;
	}
	public void setGlsign(Object what) {
		glsign.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		rldgcoy.clear();
		rdocnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		batctrcde.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		glcode.clear();
		sacscode.clear();
		sacstyp.clear();
		origamt.clear();
		origccy.clear();
		acctamt.clear();
		rldgacct.clear();
		genlcur.clear();
		glsign.clear();
		effdate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}