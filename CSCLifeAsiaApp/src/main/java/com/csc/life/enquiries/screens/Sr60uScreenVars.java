package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;


public class Sr60uScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(260);
	public FixedLengthStringData dataFields = new FixedLengthStringData(164).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData schmno = DD.schemekey.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData schmnme = DD.schemeName.copy().isAPartOf(dataFields,104);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 164);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData schmnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData schmnmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 188);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] schmnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] schmnmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(100);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(34).isAPartOf(subfileArea, 0);
	public ZonedDecimalData dateto = DD.dateto.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData datefrm = DD.datefrm.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,16);
	public ZonedDecimalData totamnt = DD.acntamt.copyToZonedDecimal().isAPartOf(subfileFields,17);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 34);
	public FixedLengthStringData datetoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData datefrmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 50);
	public FixedLengthStringData[] datetoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] datefrmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 98);
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData datetoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datefrmDisp = new FixedLengthStringData(10);
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr60uscreensflWritten = new LongData(0);
	public LongData Sr60uscreenctlWritten = new LongData(0);
	public LongData Sr60uscreenWritten = new LongData(0);
	public LongData Sr60uprotectWritten = new LongData(0);
	public GeneralTable sr60uscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr60uscreensfl;
	}

	public Sr60uScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {dateto, datefrm, select,totamnt};
		screenSflOutFields = new BaseData[][] {datetoOut, datefrmOut, selectOut, totamntOut};
		screenSflErrFields = new BaseData[] {datetoErr, datefrmErr, selectErr ,totamntErr};
		screenSflDateFields = new BaseData[] {dateto, datefrm};
		screenSflDateErrFields = new BaseData[] {datetoErr, datefrmErr};
		screenSflDateDispFields = new BaseData[] {datetoDisp, datefrmDisp};

		screenFields = new BaseData[] {clntnum, clntname, chdrnum, ctypedes, schmno, schmnme};
		screenOutFields = new BaseData[][] {clntnumOut, clntnameOut, chdrnumOut, ctypedesOut, schmnoOut, schmnmeOut};
		screenErrFields = new BaseData[] {clntnumErr, clntnameErr, chdrnumErr, ctypedesErr, schmnoErr, schmnmeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr60uscreen.class;
		screenSflRecord = Sr60uscreensfl.class;
		screenCtlRecord = Sr60uscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr60uprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr60uscreenctl.lrec.pageSubfile);
	}
}
