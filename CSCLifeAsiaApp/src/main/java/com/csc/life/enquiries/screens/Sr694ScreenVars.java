package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR694
 * @version 1.0 generated on 30/08/09 07:24
 * @author Quipoz
 */
public class Sr694ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(142);
	public FixedLengthStringData dataFields = new FixedLengthStringData(46).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData sels = new FixedLengthStringData(2).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] sel = FLSArrayPartOfStructure(2, 1, sels, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(sels, 0, FILLER_REDEFINE);
	public FixedLengthStringData sel01 = DD.sel.copy().isAPartOf(filler,0);
	public FixedLengthStringData sel02 = DD.sel.copy().isAPartOf(filler,1);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 46);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData selsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] selErr = FLSArrayPartOfStructure(2, 4, selsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(selsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sel01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData sel02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 70);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData selsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] selOut = FLSArrayPartOfStructure(2, 12, selsOut, 0);
	public FixedLengthStringData[][] selO = FLSDArrayPartOfArrayStructure(12, 1, selOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(selsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sel01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] sel02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr694screenWritten = new LongData(0);
	public LongData Sr694protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr694ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(sel01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sel02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, sel01, sel02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, sel01Out, sel02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, sel01Err, sel02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr694screen.class;
		protectRecord = Sr694protect.class;
	}

}
