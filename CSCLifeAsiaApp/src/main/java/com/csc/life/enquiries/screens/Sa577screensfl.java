package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.TableModel.Subfile.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sa577screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 18, 3, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa577ScreenVars sv = (Sa577ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa577screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa577screensfl, 
			sv.Sa577screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sa577ScreenVars sv = (Sa577ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa577screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sa577ScreenVars sv = (Sa577ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa577screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sa577screensflWritten.gt(0))
		{
			sv.sa577screensfl.setCurrentIndex(0);
			sv.Sa577screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sa577ScreenVars sv = (Sa577ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa577screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa577ScreenVars screenVars = (Sa577ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seq.setFieldName("seq");
				screenVars.dateDisp.setFieldName("dateDisp");
				screenVars.date.setFieldName("date");
				screenVars.fTotalAmt.setFieldName("fTotalAmt");
				screenVars.fTotalRecords.setFieldName("fTotalRecords");
				screenVars.ftype.setFieldName("ftype");
				screenVars.userID.setFieldName("userID");
				screenVars.extractFile.setFieldName("extractFile");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.seq.set(dm.getField("seq"));
			screenVars.dateDisp.set(dm.getField("dateDisp"));
			screenVars.date.set(dm.getField("date"));
			screenVars.fTotalAmt.set(dm.getField("fTotalAmt"));
			screenVars.fTotalRecords.set(dm.getField("fTotalRecords"));
			screenVars.ftype.set(dm.getField("ftype"));
			screenVars.userID.set(dm.getField("userID"));
			screenVars.extractFile.set(dm.getField("extractFile"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa577ScreenVars screenVars = (Sa577ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seq.setFieldName("seq");
				screenVars.date.setFieldName("date");
				screenVars.dateDisp.setFieldName("dateDisp");
				screenVars.fTotalAmt.setFieldName("fTotalAmt");
				screenVars.fTotalRecords.setFieldName("fTotalRecords");
				screenVars.ftype.setFieldName("ftype");
				screenVars.userID.setFieldName("userID");
				screenVars.extractFile.setFieldName("extractFile");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("seq").set(screenVars.seq);
			dm.getField("dateDisp").set(screenVars.dateDisp);
			dm.getField("date").set(screenVars.date);
			dm.getField("fTotalAmt").set(screenVars.fTotalAmt);
			dm.getField("fTotalRecords").set(screenVars.fTotalRecords);
			dm.getField("ftype").set(screenVars.ftype);
			dm.getField("userID").set(screenVars.userID);
			dm.getField("extractFile").set(screenVars.extractFile);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sa577screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sa577ScreenVars screenVars = (Sa577ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.seq.clearFormatting();
		screenVars.date.clearFormatting();
		screenVars.dateDisp.clearFormatting();
		screenVars.fTotalAmt.clearFormatting();
		screenVars.fTotalRecords.clearFormatting();
		screenVars.ftype.clearFormatting();
		screenVars.userID.clearFormatting();
		screenVars.extractFile.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sa577ScreenVars screenVars = (Sa577ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.seq.setClassString("");
		screenVars.dateDisp.setClassString("");
		screenVars.date.setClassString("");
		screenVars.fTotalAmt.setClassString("");
		screenVars.fTotalRecords.setClassString("");
		screenVars.ftype.setClassString("");
		screenVars.userID.setClassString("");
		screenVars.extractFile.setClassString("");
	}

/**
 * Clear all the variables in sa577screensfl
 */
	public static void clear(VarModel pv) {
		Sa577ScreenVars screenVars = (Sa577ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.seq.clear();
		screenVars.dateDisp.clear();
		screenVars.date.clear();
		screenVars.fTotalAmt.clear();
		screenVars.fTotalRecords.clear();
		screenVars.ftype.clear();
		screenVars.userID.clear();
		screenVars.extractFile.clear();
	}
}	
