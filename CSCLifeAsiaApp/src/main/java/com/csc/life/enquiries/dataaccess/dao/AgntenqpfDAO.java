package com.csc.life.enquiries.dataaccess.dao;

import java.util.List;

import com.csc.life.enquiries.dataaccess.model.Agntenqpf;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AgntenqpfDAO extends BaseDAO<Agntenqpf>{
	
	public List<Agntenqpf> getAgntenqpfData(Agntenqpf agntenqpfModel);
	public List<Agntenqpf> getAgntenqpfDataWithClntInform(Agntenqpf agntenqpfModel, Wsspcomn wsspcomn);

}
