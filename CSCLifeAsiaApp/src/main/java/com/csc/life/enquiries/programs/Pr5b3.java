/*
 * File: Pr5b3.java
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.enquiries.screens.Sr5b3ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Itmmsmtkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr5b3 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5B3");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(13);
	private FixedLengthStringData wsaaKeyTabl = new FixedLengthStringData(5).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaKeyItem = new FixedLengthStringData(8).isAPartOf(wsaaKey, 5);
	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Itmmsmtkey wskyItmmkey = new Itmmsmtkey();

	private static final String t5688 = "T5688";
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);


	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	List<Descpf> descpflist = new ArrayList<Descpf>();
	ClntpfDAO clntpfDAO =  getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	
	Chdrpf chdrpf = new Chdrpf();
	Descpf desc = new Descpf();
	Clntpf clntpf = new Clntpf();
	Nlgtpf nlgtpf = new Nlgtpf();
	List<Nlgtpf> nlgtpflist = new ArrayList<Nlgtpf>();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr5b3ScreenVars sv = getLScreenVars();
	
	protected Sr5b3ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars( Sr5b3ScreenVars.class);
	}
	
	public Pr5b3() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5b3", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
	
		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		
		scrnparams.function.set(varcom.sclr);
		processScreen("Sr5b3", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrpf= chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString());
		if(chdrpf != null)
		{
			sv.chdrnum.set(chdrpf.getChdrnum());
			sv.cntcurr.set(chdrpf.getCntcurr());
			sv.register.set(chdrpf.getReg());
			
		}
		
		descpflist =descDAO.getItemByDescItem("IT", wsspcomn.company.toString(),t5688, wsspcomn.chdrCnttype.toString(), wsspcomn.language.toString());
		sv.cnttype.set(wsspcomn.chdrCnttype.value());
		boolean itemFound = false;
		for (Descpf descItem : descpflist) {
			if (descItem.getDescitem().trim().equals( wsspcomn.chdrCnttype.toString().trim())){
				sv.ctypedes.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}		
		
		if (isEQ(itemFound,false)) {
		
			sv.ctypedes.fill("?");
		}
		
	
		descpflist =descDAO.getItemByDescItem("IT", wsspcomn.company.toString(),"T3588",chdrpf.getPstcde(), wsspcomn.language.toString());
		sv.premstatus.set(chdrpf.getPstcde());
		boolean itemFound1 = false;
		for (Descpf descItem : descpflist) {
			if (descItem.getDescitem().trim().equals(chdrpf.getPstcde().trim())){/* IJTI-1523 */
				sv.premstatus.set(descItem.getLongdesc());
				itemFound1=true;
				break;
			}
		}		
		
		if (isEQ(itemFound1,false)) {
		
			sv.premstatus.fill("?");
		}
		
		descpflist =descDAO.getItemByDescItem("IT", wsspcomn.company.toString(),"T3623",chdrpf.getStatcode(), wsspcomn.language.toString());
		sv.chdrstatus.set(chdrpf.getStatcode());
		boolean itemFound2 = false;
		for (Descpf descItem : descpflist) {
			if (descItem.getDescitem().trim().equals(chdrpf.getStatcode().trim())){/* IJTI-1523 */
				sv.chdrstatus.set(descItem.getLongdesc());
				itemFound2=true;
				break;
			}
		}		
		
		if (isEQ(itemFound2,false)) {
		
			sv.chdrstatus.fill("?");
		}
		
		readSubfileRecords1100();
		
		
}
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void readSubfileRecords1100()
{
	sub1.set(0);
	nlgtpflist = nlgtpfDAO.readNlgtpf(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	while (isLT(sub1,nlgtpflist.size())&& isLT(sub1, 15)){
		obtainNlgtpfDetails1200();
	
}
}




private void obtainNlgtpfDetails1200() {
	if(nlgtpflist != null){			
		sv.tranno.set(nlgtpflist.get(sub1.toInt()).getTranno());
		sv.batctrcde.set(nlgtpflist.get(sub1.toInt()).getBatctrcde());
		sv.trandesc.set(nlgtpflist.get(sub1.toInt()).getTrandesc());
		sv.effdate.set(nlgtpflist.get(sub1.toInt()).getEffdate());
		sv.nlgflag.set(nlgtpflist.get(sub1.toInt()).getNlgflag());
		sv.tranamt.set(nlgtpflist.get(sub1.toInt()).getTranamt());
		sv.nlgbal.set(nlgtpflist.get(sub1.toInt()).getNlgbal());
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("Sr5b3", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	sub1.add(1);
	
}
	// TODO Auto-generated method stub
	



protected void preScreenEdit()
{
		preStart();
}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		scrnparams.subfileRrn.set(1);
		if (isNE(wsspcomn.programPtr,ZERO)){
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}
protected void whereNext4000()
{
	nextProgram4100();
	srnch4110();
	
}
protected void nextProgram4100()
{
	wsspcomn.nextprog.set(wsaaProg);
//	wsspcomn.programPtr.add(1);
	if (isEQ(wsaaFunctionKey, varcom.calc)) {
		return;
	}
}

protected void srnch4110()
{
	scrnparams.function.set(varcom.srnch);
	processScreen("Sr5b3", sv);
	wsspcomn.confirmationKey.set(SPACES);
	wsaaKeyItem.set(SPACES);
	wskyItmmkey.itmmsmtItemitem.set(SPACES);
	wsspsmart.itemkey.set(SPACES);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}

	if (isNE(scrnparams.statuz, varcom.endp)) {
		
		wsaaKeyItem.set(sv.tranno);
		wsspcomn.confirmationKey.set(wsaaKey);
		wskyItmmkey.itmmsmtItemitem.set(sv.tranno);
		wsspsmart.itemkey.set(wskyItmmkey);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("Sr5b3", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(wsaaProg);
		}
		
		else {
			wsspcomn.programPtr.add(1);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		}
	}

return;
}
protected void exit4120()
{
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
	/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/
}
protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("Sr5b3", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
