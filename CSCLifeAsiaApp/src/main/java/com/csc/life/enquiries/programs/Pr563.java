/*
 * File: Pr563.java
 * Date: 30 August 2009 1:41:27
 * Author: Quipoz Limited
 * 
 * Class transformed from PR563.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsJniSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.runtime.base.VpmsTcpSessionFactory;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.screens.Sr563ScreenVars;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  This table will determine which option we will use in triggering
*  Follow Up Codes. If this Table is not setup or item not found
*  it will use the base method which is Contract Type Base.
*
*****************************************************************
* </pre>
*/
public class Pr563 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pr563.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR563");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String mrtarec = "MRTAREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Enquiry - Contract Header.*/
	// ILIFE-3396 PERFORMANCE BY OPAL
	protected Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Reducing Term Assurance details*/
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private Sr563ScreenVars sv = getLScreenVars(); 

	private boolean mrtaPermission = false;//BRD-139

	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
	private Mrtapf mrtapf=null;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Pr563() {
		super();
		screenVars = sv;
		new ScreenModel("Sr563", AppVars.getInstance(), sv);
	}
	
	protected Sr563ScreenVars getLScreenVars()
	{
		return ScreenProgram.getScreenVars( Sr563ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.coverc.set(ZERO);
		sv.prat.set(ZERO);
		sv.loandur.set(SPACES);
		// ILIFE-3396 PERFORMANCE BY OPAL
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf==null)
		{
			return;
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cnRiskStat.set(chdrpf.getStatcode());
		sv.cnPremStat.set(chdrpf.getPstcde());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrpf.getChdrcoy());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.descrip.fill("?");
		}
		else {
			sv.descrip.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		mrtaIO.setChdrcoy(chdrpf.getChdrcoy());
		mrtaIO.setChdrnum(chdrpf.getChdrnum());
		mrtaPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars, "IT");
		/*mrtaIO.setFormat(mrtarec);
		mrtaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mrtaIO);
		if (isEQ(mrtaIO.getStatuz(),varcom.oK)) {*/
			mrtapf = new Mrtapf();
			mrtapf=mrtapfDAO.getMrtaRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
			if ((mrtapf !=null)) {
			sv.prat.set(mrtapf.getPrat());
			sv.coverc.set(mrtapf.getCoverc());
			sv.prmdetails.set(mrtapf.getPrmdetails());
			sv.mlinsopt.set(mrtapf.getMlinsopt());
		
			sv.mbnkref.set(mrtapf.getMbnkref());
			//BRD-139:Start
			if(mrtaPermission){
			sv.loandur.set(mrtapf.getLoandur());
			sv.intcaltype.set(mrtapf.getIntcalType());
			sv.mlresindvpms.set(mrtapf.getMlresind());
			}else{
				sv.mlresind.set(mrtapf.getMlresind());
			}
			/*ILIFE-3754 ends*/
			//BRD-139:Start
		}
		
		if(!mrtaPermission){
			sv.mrtaFlag.set("N");
		}
		else {
			sv.mrtaFlag.set("Y");
		}
		if(!mrtaPermission){
			sv.loandurOut[varcom.nd.toInt()].set("Y");
			sv.intcaltypeOut[varcom.nd.toInt()].set("Y");
			//sv.mlresindOut[varcom.chg.toInt()].set("Y");
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.function.set(varcom.prot);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}


public static Map<String,Map<String,String>> f4(COBOLAppVars appVars, String field)throws VpmsLoadFailedException{
	
	Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();
	Map<String, String> itemDesc=new HashMap<String,String>();
	IVpmsBaseSessionFactory sessionFactory = new VpmsJniSessionFactory();
	IVpmsBaseSession session;
	//ILIFE-5701 start
	IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();
	
	if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
		factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
		session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
	}
	else
	{
		session= factory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
	}
	//ILIFE-5701 end
	session.setAttribute("Input Contract Type", "mrt");
	session.setAttribute("Input Region", " "); 
	session.setAttribute("Input Locale", " "); 
	session.setAttribute("Input TransEffDate", "20161010");
	session.setAttribute("Input MTL PLan Cd", "MAS05S"); 
	session.setAttribute("Input Prod Code", "GS1"); 
	session.setAttribute("Input Coverage Code", "mrt1");

	VpmsComputeResult result = session.computeUsingDefaults("Output Fetch MRTA Frequency");
	//sv.mlresind.set(result.getResult());
	String temp1=result.getResult();
    temp1=temp1.replace("||","/n");
  
    String ReducingFreq[] = temp1.split("/n");
    ArrayList<String> ReducingFreqList = new ArrayList<String>();
    ArrayList<String> ReducingFreqList1 = new ArrayList<String>();
    LOGGER.info("parts1={}",Arrays.toString(ReducingFreq));//IJTI-1561
	 
    for (int i = 0; i < ReducingFreq.length; i =i+2) {
    	ReducingFreqList.add(ReducingFreq[i]);
    	ReducingFreqList1.add(ReducingFreq[i+1]);

    }
    for (int i = 0; i < ReducingFreqList.size(); i++) {
    	itemDesc.put( ReducingFreqList.get(i).trim(),  ReducingFreqList1.get(i).trim());
		
        }	 
	f4map.put(field, itemDesc);
	
    return f4map;
    
} 
}
