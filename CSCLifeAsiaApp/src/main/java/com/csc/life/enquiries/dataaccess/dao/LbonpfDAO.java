package com.csc.life.enquiries.dataaccess.dao;

import java.util.List;

import com.csc.life.enquiries.dataaccess.model.Lbonpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LbonpfDAO extends BaseDAO<Lbonpf>{
	
	public List<Lbonpf> getLbonpfData(String chdrnum, String chdrcoy);
	
	public Lbonpf getLbonpfRecord(String chdrnum, int tranno, String chdrcoy);
	
	public void updateLbonpfRecord(Lbonpf lbonpf);
	 public void insertLBonPFValidRecord(List<Lbonpf> lbonpfBulkOpList) ;

}
