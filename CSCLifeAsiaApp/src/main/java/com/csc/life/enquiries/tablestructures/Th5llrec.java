package com.csc.life.enquiries.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Fri, 10 Jan 2014 21:15:47
 * Description:
 * Copybook name: TR363REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th5llrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th5llRec = new FixedLengthStringData(3);
  	public FixedLengthStringData day = new FixedLengthStringData(3).isAPartOf(th5llRec, 0);
  	
	public void initialize() {
		COBOLFunctions.initialize(th5llRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th5llRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}