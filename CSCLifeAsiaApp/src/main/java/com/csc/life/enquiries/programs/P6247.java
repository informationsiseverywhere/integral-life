/*
 * File: P6247.java
 * Date: 30 August 2009 0:39:11
 * Author: Quipoz Limited
 * 
 * Class transformed from P6247.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Cltskey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S6247ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The details  of  the  contract  being  enquired  upon will be
*     stored in  the  CHDRENQ I/O module.  Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's client (CLTS) details if they exist.
*
*
*     Load the subfile as follows:
*
*          Perform a BEGN on BNFYENQ with a key of Contract Company
*          and Contract Number.
*
*          Continue  Reading the BNFYENQ data-set until end of file
*          or either Contract Company or Contract number changes.
*
*          Display  the  Beneficiary  details and decode the client
*          relationship against T5663.
*
*     Load all  pages  required  in the subfile and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation in this program.
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11"  was  requested  add  1  to the program pointer and
*     exit.
*
*     Release any CLTS record that may have been stored.
*
*     At this  point  the  program will be either searching for the
*     FIRST selected  record in order to pass control to the Client
*     Enquiry program  for the selected client number or it will be
*     returning  from  the  Client Enquiry program after displaying
*     some details and searching for the NEXT selected record.
*
*     It will  be able to determine which of these two states it is
*     in by examining the Stack Action Flag.
*
*     If returning  from  a  Client  display, (Stack Action Flag is
*     blank), move spaces to the current select field.
*
*     Each time  it  returns  to  this  program  after processing a
*     previous seelction its position in the subfile will have been
*     retained and  it  will be able to continue from where it left
*     off.
*
*     Processing from  here is the same for either state. After the
*     Start or  after  returning  to the program after processing a
*     previous  selection  read  the  next  changed record from the
*     subfile.  If this is not selected (Select is blank), continue
*     reading the  next  changed  subfile record until one is found
*     with a non-blank Select field or end of file is reached.
*
*     If nothing  was  selected  or there are no more selections to
*     process,  continue  by  moving  blanks  to  the current Stack
*     Action field, and Program position and exit.
*
*     If a  selection  has been found the corresponding CLTS record
*     should be  read  and  stored  in the I/O module ready for the
*     next program.  Use  the  selected  Client number to perform a
*     READS on CLTS. If no record is found report a system error.
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5663 - Relationships                     Key: BNFYRLN
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6247 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6247");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private static final String h093 = "H093";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Cltskey wsaaCltskey = new Cltskey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	private S6247ScreenVars sv = ScreenProgram.getScreenVars( S6247ScreenVars.class);
	private WsbbStackArrayInner wsbbStackArrayInner = new WsbbStackArrayInner();	
	private int wsaaNofRead;
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(5, 0).setUnsigned();
	private static final int wsaaSubfileSize = 30;
	private List<Bnfypf> bnfypfList = null;
	private Bnfypf bnfypf = null;
	private Clntpf clntpf = null;
	private BnfypfDAO bnfypfDAO = getApplicationContext().getBean("bnfypfDAO", BnfypfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private boolean benesequence = false;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextProgram4020, 
		exit4090
	}

	public P6247() {
		super();
		screenVars = sv;
		new ScreenModel("S6247", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6247", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		benesequence = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP087", appVars, "IT");
		if(benesequence){
			sv.actionflag.set("Y");
		}
		else{
			sv.actionflag.set("N");
		}
		 chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf==null)
			{
				return;
			}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/* Owner number and name                                           */
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO S6247-JLIFE                   */
		/*                                 S6247-JLIFENAME               */
		/*  ELSE                                                         */
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		//ILIFE-7346
		wsbbStackArrayInner.wsbbStackArray.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaSubfileSize)); wsaaIndex.add(1)){
			initialZero1300();
		}
		
		bnfypfList = bnfypfDAO.getBnfymnaByCoyAndNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
		
		if(bnfypfList !=null && bnfypfList.size()>0){
		 for (wsaaNofRead=1; wsaaNofRead <= bnfypfList.size() ; wsaaNofRead++){
			bnfypf = bnfypfList.get(wsaaNofRead-1);
			loadBeneficiaries1200();
		  }
		}
		
		if(bnfypfList !=null && bnfypfList.size()>0){
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, bnfypfList.size())); wsaaIndex.add(1)){
			moveToScreen1500();
			}
		}
		
		scrnparams.subfileRrn.set(1);
	}



	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*      (including subfile load section)
	* </pre>
	*/

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();

		} else if (isNE(cltsIO.getGivname(), SPACES)) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = ",";
			
			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);
			
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void plainname01()
{
	wsspcomn.longconfname.set(SPACES);
	if (clntpf.getClttype().trim().equals("C")) {
		corpname01();		/* IBPLIFE-10722 */
	}
	else if (clntpf.getGivname()!=null && !clntpf.getGivname().trim().equals("")) {
		String firstName = clntpf.getGivname();/* IJTI-1523 */
		String lastName = clntpf.getSurname();/* IJTI-1523 */
		String delimiter = ",";
		
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
	}
	else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
}
protected void corpname01(){
	
	wsspcomn.longconfname.set(SPACES);
	String firstName = clntpf.getGivname();
	String lastName = clntpf.getSurname();
	String delimiter = ",";
	
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	

	
}


protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else if (isEQ(cltsIO.getEthorig(), "1")) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = "";
			String salute = cltsIO.getSalutl().toString();
			
			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = stringUtil.includeSalute(salute, fullName);
		
			wsspcomn.longconfname.set(fullNameWithSalute);
		} else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(cltsIO.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(cltsIO.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = cltsIO.getLgivname().toString();
		String lastName = cltsIO.getLsurname().toString();
		String delimiter = "";
		
		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/

//ILIFE-7346
protected void initialZero1300()
{
	
	wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()].set(SPACES);
	wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
	wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
	wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()].set(SPACES);
	if(benesequence){
	wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()].set(SPACES);
	}
	wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()].set(SPACES);
	wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()].set(SPACES);
	wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()].set(SPACES);
	wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()].set(ZERO);
	

}

//ILIFE-7346
protected void loadBeneficiaries1200()
{
	sv.subfileFields.set(SPACES);
	clntpf = new Clntpf();
	clntpf.setClntpfx("CN");
	clntpf.setClntcoy(wsspcomn.fsuco.toString());
	clntpf.setClntnum(bnfypf.getBnyclt());/* IJTI-1523 */
	clntpf = clntpfDAO.selectClient(clntpf);

	plainname01();

	wsbbStackArrayInner.wsbbclntnum[wsaaNofRead].set(bnfypf.getBnyclt());/* IJTI-1523 */
	wsbbStackArrayInner.wsbbClntnm[wsaaNofRead].set(wsspcomn.longconfname);
	wsbbStackArrayInner.wsbbEffdate[wsaaNofRead].set(bnfypf.getEffdate());
	wsbbStackArrayInner.wsbbEnddate[wsaaNofRead].set(bnfypf.getEnddate());
	wsbbStackArrayInner.wsbbBnytype[wsaaNofRead].set(bnfypf.getBnytype());
	if(benesequence){
	wsbbStackArrayInner.wsbbSequence[wsaaNofRead].set(bnfypf.getSequence());
	}
	wsbbStackArrayInner.wsbbBnyrln[wsaaNofRead].set(bnfypf.getCltreln());
	wsbbStackArrayInner.wsbbRelto[wsaaNofRead].set(bnfypf.getRelto());
	wsbbStackArrayInner.wsbbRevcflg[wsaaNofRead].set(bnfypf.getRevcflg());
	wsbbStackArrayInner.wsbbBnypc[wsaaNofRead].set(bnfypf.getBnypc());

	
}

//ILIFE-7346
protected void moveToScreen1500()
{
	sv.clntnum.set(wsbbStackArrayInner.wsbbclntnum[wsaaIndex.toInt()]);
	sv.bnyrlndesc.set(wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()]);
	sv.bnypc.set(wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()]);
	sv.bnytype.set(wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()]);
	sv.relto.set(wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()]);
	sv.revcflg.set(wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()]);
	sv.clntsname.set(wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()]);
	sv.effdate.set(wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()]);
	sv.enddate.set(wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()]);
	if(benesequence){
	sv.sequence.set(wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()]);
	}
	scrnparams.subfileRrn.set(wsaaIndex);
	
	scrnparams.function.set(varcom.sadd);
	processScreen("S6247", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6247IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6247-DATA-AREA                         */
		/*                         S6247-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
					gensww4010();
				case nextProgram4020: 
					nextProgram4020();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		gensswrec.function.set(SPACES);
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*    Release any CLTS record that may have been stored.*/
		cltsIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    If returning from a program further down the stack then*/
		/*    bypass the start on the subfile.*/
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      MOVE SPACES            TO WSSP-SEC-ACTN                 */
		/*                               (WSSP-PROGRAM-PTR)             */
		/*                                S6247-SELECT.                 */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			sv.select.set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4200();
			}
		}
		/*    Read the next changed subfile record.*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.srnch);
		while ( !(isNE(sv.select, SPACES)
		|| isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile4100();
		}
		
		/* IF   SCRN-STATUZ             = ENDP                          */
		/*      MOVE SPACES            TO WSSP-SEC-PROG                 */
		/*                               (WSSP-PROGRAM-PTR)             */
		/*                                WSSP-SEC-ACTN                 */
		/*                               (WSSP-PROGRAM-PTR)             */
		/*      GO TO 4090-EXIT.                                        */
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.nextProgram4020);
		}
		if (isNE(sv.select, SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4300();
			}
		}
		/*    Read and store the selected CLTS record.*/
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.clntnum);
		cltsIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		/* ADD 1                       TO WSSP-PROGRAM-PTR.             */
		if (isEQ(cltsIO.getClttype(), "P")) {
			gensswrec.function.set("A");
		}
		else {
			gensswrec.function.set("B");
		}
		/*    Pass the client to wssp as FAS programs do not do            */
		/*    retrieves yet!                                               */
		wsaaCltskey.set(SPACES);
		wsaaCltskey.cltsClntpfx.set("CN");
		wsaaCltskey.cltsClntcoy.set(wsspcomn.fsuco);
		wsaaCltskey.cltsClntnum.set(sv.clntnum);
		wsspcomn.clntkey.set(wsaaCltskey);
	}

protected void gensww4010()
	{
		if (isEQ(gensswrec.function, SPACES)) {
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scr */
		/* with an error and the options and extras indicator              */
		/* with its initial load value                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4400();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4100()
	{
		/*READ*/
		processScreen("S6247", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4300()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4400()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

private static final class WsbbStackArrayInner {

	private FixedLengthStringData wsbbStackArray = new FixedLengthStringData(4290);
	private FixedLengthStringData wsbbBnytypeArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 0);
	private FixedLengthStringData[] wsbbBnytype = FLSArrayPartOfStructure(30, 2, wsbbBnytypeArray, 0);
	private FixedLengthStringData wsbbBnyrlnArray = new FixedLengthStringData(120).isAPartOf(wsbbStackArray, 60);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnyrln = FLSArrayPartOfStructure(30, 4, wsbbBnyrlnArray, 0);
	private FixedLengthStringData wsbbReltoArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 180);
	private FixedLengthStringData[] wsbbRelto = FLSArrayPartOfStructure(30, 1, wsbbReltoArray, 0);
	private FixedLengthStringData wsbbRevcflgArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 210);
	private FixedLengthStringData[] wsbbRevcflg = FLSArrayPartOfStructure(30, 1, wsbbRevcflgArray, 0);
	private FixedLengthStringData wsbbBnycdArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 240);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnycd = FLSArrayPartOfStructure(30, 1, wsbbBnycdArray, 0);
	private FixedLengthStringData wsbbBnypcArray = new FixedLengthStringData(150).isAPartOf(wsbbStackArray, 270);
		/*                             OCCURS 10.                       */
	private ZonedDecimalData[] wsbbBnypc = ZDArrayPartOfStructure(30, 5, 2, wsbbBnypcArray, 0, UNSIGNED_TRUE);
	private FixedLengthStringData wsbbBnyselArray = new FixedLengthStringData(300).isAPartOf(wsbbStackArray, 420);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnysel = FLSArrayPartOfStructure(30, 10, wsbbBnyselArray, 0);
	private FixedLengthStringData wsbbClntnmArray = new FixedLengthStringData(900).isAPartOf(wsbbStackArray, 720);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbClntnm = FLSArrayPartOfStructure(30, 30, wsbbClntnmArray, 0);
	private FixedLengthStringData wsbbEffdateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 1620);
		/*                             OCCURS 10.                       */
	private PackedDecimalData[] wsbbEffdate = PDArrayPartOfStructure(30, 8, 0, wsbbEffdateArray, 0);
	private FixedLengthStringData wsbbEnddateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 1860);
	/*                             OCCURS 10.               <V76L01>*/
private PackedDecimalData[] wsbbEnddate = PDArrayPartOfStructure(30, 8, 0, wsbbEnddateArray, 0);

	private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2100);
		/*                             OCCURS 30.                       */
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(30, 1, wsbbChangeArray, 0);
	private FixedLengthStringData wsbbSelfindArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2130);
	private FixedLengthStringData[] wsbbSelfind = FLSArrayPartOfStructure(30, 1, wsbbSelfindArray, 0);
	/*ICIL-11*/
	private FixedLengthStringData wsbbSequenceArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 2160);
	private FixedLengthStringData[] wsbbSequence = FLSArrayPartOfStructure(30, 2, wsbbSequenceArray, 0);
	private FixedLengthStringData wsbbpaymthbfArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2220); 
	private FixedLengthStringData[] wsbbpaymthbf = FLSArrayPartOfStructure(30, 1, wsbbpaymthbfArray, 0);
	private FixedLengthStringData wsbbBankkeyArray = new FixedLengthStringData(300).isAPartOf(wsbbStackArray, 2250);
	private FixedLengthStringData[] wsbbBankkey = FLSArrayPartOfStructure(30, 10, wsbbBankkeyArray, 0);
	private FixedLengthStringData wsbbBankacckeyArray = new FixedLengthStringData(600).isAPartOf(wsbbStackArray, 2550);
	private FixedLengthStringData[] wsbbBankacckey = FLSArrayPartOfStructure(30, 20, wsbbBankacckeyArray, 0);
	private FixedLengthStringData wsbbBnyrlndescArray = new FixedLengthStringData(900).isAPartOf(wsbbStackArray, 3150);
	private FixedLengthStringData[] wsbbBnyrlndesc = FLSArrayPartOfStructure(30, 30, wsbbBnyrlndescArray, 0);
	private FixedLengthStringData wsbbclntnumArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 4050);
	private FixedLengthStringData[] wsbbclntnum = FLSArrayPartOfStructure(30, 8, wsbbclntnumArray, 0);
}

public StringUtil getStringUtil() {
	return stringUtil;
}


public void setStringUtil(StringUtil stringUtil) {
	this.stringUtil = stringUtil;
}


}