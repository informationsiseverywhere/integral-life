package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH514
 * @version 1.0 generated on 30/08/09 07:01
 * @author Quipoz
 */
public class Sh514ScreenVars extends SmartVarModel { 


	//public FixedLengthStringData dataArea = new FixedLengthStringData(1743);
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	//public FixedLengthStringData dataFields = new FixedLengthStringData(687).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData bonusInd = DD.bnusin.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,68);
	public ZonedDecimalData annivProcDate = DD.cbanpr.copyToZonedDecimal().isAPartOf(dataFields,98);
	public ZonedDecimalData unitStatementDate = DD.cbunst.copyToZonedDecimal().isAPartOf(dataFields,106);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,138);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,140);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,148);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData divdInd = DD.hdvdind.copy().isAPartOf(dataFields,208);
	public FixedLengthStringData puaInd = DD.hpuaind.copy().isAPartOf(dataFields,209);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,210);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,227);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,274);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,282);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,284);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,286);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,333);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,341);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,342);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,346);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(dataFields,347);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(dataFields,355);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(dataFields,358);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,360);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,407);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,408);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,412);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,420);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,430);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,438);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,441);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(dataFields,443);
	public ZonedDecimalData rerateFromDate = DD.rrtfrm.copyToZonedDecimal().isAPartOf(dataFields,451);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,459);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,476);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,477);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,479);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,483);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,498);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,515);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,532);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,533);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(dataFields,546);
	/*BRD-306 STARTS*/
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 550);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 567);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 584);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 601);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 618);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,635);	
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,652);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,669);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,672);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,675);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,678);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,681);
	/*BRD-306 END*/
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,682);
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,683);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,686);
	public FixedLengthStringData aepaydet = DD.optextind.copy().isAPartOf(dataFields,687);
	
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 687);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bnusinErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cbanprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cbunstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData hdvdindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData hpuaindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData paycltErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData paycurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData paymthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData rrtfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData zdivoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	
	/*BRD-306 STARTS*/
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	/*BRD-306 END*/
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 256);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 260);
	public FixedLengthStringData aepaydetErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 264);
	
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(792).isAPartOf(dataArea, 951);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bnusinOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cbanprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cbunstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] hdvdindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] hpuaindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] paycltOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] paycurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] paymthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] rrtfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] zdivoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	/*BRD-306 STARTS*/
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	/*BRD-306 END*/
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 768);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 780);
	public FixedLengthStringData[] aepaydetOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 784);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData annivProcDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData unitStatementDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateFromDateDisp = new FixedLengthStringData(10);

	public LongData Sh514screenWritten = new LongData(0);
	public LongData Sh514protectWritten = new LongData(0);	
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1);
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387
	

	public boolean hasSubfile() {
		return false;
	}


	public Sh514ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsOut,new String[] {null, null, null, "37",null, null, null, null, null, null, null, null});
		fieldIndMap.put(cbunstOut,new String[] {null, null, null, "34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnusinOut,new String[] {null, null, null, "34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"35","36","-35","36",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hpuaindOut,new String[] {"32","37","-32","37",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hdvdindOut,new String[] {"30","31","-30","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {"74", "75", "-74", "80",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"38","39","-38","38",null, null, null, null, null, null, null, null});
		/*BRD-306 STARTS*/
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		/*BRD-306 END*/
		//ILIFE-3399-STARTS
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		fieldIndMap.put(prmbasisOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "73", null, null, null, null, null, null, null, null});
		fieldIndMap.put(aepaydetOut,new String[] {null, null, null, "69", null, null, null, null, null, null, null, null});
		fieldIndMap.put(crrcdOut,new String[] {null, null, null,"77",null, null, null, null, null, null, null, null});//ILJ-45
	//	fieldIndMap.put(rcessageOut,new String[] {null, null, null,"78",null, null, null, null, null, null, null, null});//ILJ-45
		fieldIndMap.put(rcesdteOut,new String[] {null, null, null,"79",null, null, null, null, null, null, null, null});//ILJ-45
		/*screenFields = new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, sumins, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, unitStatementDate, bonusInd, rerateDate, rerateFromDate, mortcls, optextind, zdivopt, paymth, paycurr, bappmeth, payclt, payorname, bankkey, bankdesc, bankacckey, branchdesc, pbind, puaInd, divdInd, taxamt, taxind,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, zlinstprem, riskCessAge, riskCessTerm, premCessAge,premCessTerm,prmbasis,dialdownoption,exclind};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, suminsOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, cbunstOut, bnusinOut, rrtdatOut, rrtfrmOut, mortclsOut, optextindOut, zdivoptOut, paymthOut, paycurrOut, bappmethOut, paycltOut, payornameOut, bankkeyOut, bankdescOut, bankacckeyOut, branchdescOut, pbindOut, hpuaindOut, hdvdindOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut, zlinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut,prmbasisOut,dialdownoptionOut,exclindOut};
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, suminsErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, cbunstErr, bnusinErr, rrtdatErr, rrtfrmErr, mortclsErr, optextindErr, zdivoptErr, paymthErr, paycurrErr, bappmethErr, paycltErr, payornameErr, bankkeyErr, bankdescErr, bankacckeyErr, branchdescErr, pbindErr, hpuaindErr, hdvdindErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr, zlinstpremErr,rcessageErr, rcesstrmErr, pcessageErr,pcesstrmErr,prmbasisErr,dialdownoptionErr,exclindErr};
		screenDateFields = new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, unitStatementDate, rerateDate, rerateFromDate};
		screenDateErrFields = new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, cbunstErr, rrtdatErr, rrtfrmErr};
		screenDateDispFields = new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, unitStatementDateDisp, rerateDateDisp, rerateFromDateDisp};
		 */
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh514screen.class;
		protectRecord = Sh514protect.class;
	}
	
	
	public BaseData[] getscreenFields() {
		return new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, sumins, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, unitStatementDate, bonusInd, rerateDate, rerateFromDate, mortcls, optextind, zdivopt, paymth, paycurr, bappmeth, payclt, payorname, bankkey, bankdesc, bankacckey, branchdesc, pbind, puaInd, divdInd, taxamt, taxind,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, zlinstprem, riskCessAge, riskCessTerm, premCessAge,premCessTerm,prmbasis,dialdownoption,exclind,aepaydet};
	}
	public BaseData[][] getscreenOutFields() {
			return new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, suminsOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, cbunstOut, bnusinOut, rrtdatOut, rrtfrmOut, mortclsOut, optextindOut, zdivoptOut, paymthOut, paycurrOut, bappmethOut, paycltOut, payornameOut, bankkeyOut, bankdescOut, bankacckeyOut, branchdescOut, pbindOut, hpuaindOut, hdvdindOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut, zlinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut,prmbasisOut,dialdownoptionOut,exclindOut,aepaydetOut};
		}
	public BaseData[] getscreenErrFields() {
			return new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, suminsErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, cbunstErr, bnusinErr, rrtdatErr, rrtfrmErr, mortclsErr, optextindErr, zdivoptErr, paymthErr, paycurrErr, bappmethErr, paycltErr, payornameErr, bankkeyErr, bankdescErr, bankacckeyErr, branchdescErr, pbindErr, hpuaindErr, hdvdindErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr, zlinstpremErr,rcessageErr, rcesstrmErr, pcessageErr,pcesstrmErr,prmbasisErr,dialdownoptionErr,exclindErr,aepaydetErr}; 
		}
	public BaseData[] getscreenDateFields() {
		return new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, unitStatementDate, rerateDate, rerateFromDate};
	}
	public BaseData[] getscreenDateDispFields() {
		return new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, unitStatementDateDisp, rerateDateDisp, rerateFromDateDisp};
	}
	public BaseData[] getscreenDateErrFields() {
		return new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, cbunstErr, rrtdatErr, rrtfrmErr};
	}

	public int getDataAreaSize(){
		return  1760;
	}
	
	public int getDataFieldsSize(){
		return 688;
	}
	
	public int getErrorIndicatorSize(){
		return  268;
	}
	
	public int getOutputFieldSize(){
		return 804;  
	}

}
