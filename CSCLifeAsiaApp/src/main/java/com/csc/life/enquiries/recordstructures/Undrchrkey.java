package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:35
 * Description:
 * Copybook name: UNDRCHRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undrchrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undrchrFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData undrchrKey = new FixedLengthStringData(256).isAPartOf(undrchrFileKey, 0, REDEFINE);
  	public FixedLengthStringData undrchrCoy = new FixedLengthStringData(1).isAPartOf(undrchrKey, 0);
  	public FixedLengthStringData undrchrChdrnum = new FixedLengthStringData(8).isAPartOf(undrchrKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(undrchrKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undrchrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undrchrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}