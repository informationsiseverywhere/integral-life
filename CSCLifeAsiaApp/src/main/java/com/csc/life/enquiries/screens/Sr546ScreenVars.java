package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR546
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr546ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1631);
	public FixedLengthStringData dataFields = new FixedLengthStringData(623).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData benBillDate = DD.bbldat.copyToZonedDecimal().isAPartOf(dataFields,7);
	public ZonedDecimalData annivProcDate = DD.cbanpr.copyToZonedDecimal().isAPartOf(dataFields,15);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,47);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,49);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,117);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,127);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,144);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,191);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,203);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,250);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,258);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,259);
	public FixedLengthStringData optdscs = new FixedLengthStringData(60).isAPartOf(dataFields, 263);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(4, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optinds = new FixedLengthStringData(4).isAPartOf(dataFields, 323);
	public FixedLengthStringData[] optind = FLSArrayPartOfStructure(4, 1, optinds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(optinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01 = DD.optind.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optind02 = DD.optind.copy().isAPartOf(filler1,1);
	public FixedLengthStringData optind03 = DD.optind.copy().isAPartOf(filler1,2);
	public FixedLengthStringData optind04 = DD.optind.copy().isAPartOf(filler1,3);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,327);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,331);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,339);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,349);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,357);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,360);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(dataFields,362);
	public ZonedDecimalData rerateFromDate = DD.rrtfrm.copyToZonedDecimal().isAPartOf(dataFields,370);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,378);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,395);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,396);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,398);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,402);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,419);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(dataFields,432);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,452);
	public ZonedDecimalData zrsumin = DD.zrsumin.copyToZonedDecimal().isAPartOf(dataFields,469);
	/* BRD-306 */
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 482);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 499);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 516);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 533);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 550);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,567);	
	
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,584);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,587);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,590);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,593);
	/* BRD-306 */
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,596);
	public FixedLengthStringData waitperiod = DD.waitperiod.copy().isAPartOf(dataFields,613);
	public FixedLengthStringData bentrm = DD.bentrm.copy().isAPartOf(dataFields,616);
	public FixedLengthStringData poltyp = DD.zpoltyp.copy().isAPartOf(dataFields,618);
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,619);
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,620);//BRD-NBP-011

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 623);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbldatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cbanprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(4, 4, optdscsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData optindsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData[] optindErr = FLSArrayPartOfStructure(4, 4, optindsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(optindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optind02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData optind03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData optind04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData rrtfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData zdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData zrsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);

	/*306*/
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);

	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	/*306*/
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData waitperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData bentrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData poltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);//BRD-NBP-011
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(756).isAPartOf(dataArea, 875);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbldatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cbanprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 264);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(4, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(48).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData optindsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 312);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(4, 12, optindsOut, 0);
	public FixedLengthStringData[][] optindO = FLSDArrayPartOfArrayStructure(12, 1, optindOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(48).isAPartOf(optindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optind01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] optind02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] optind03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] optind04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] rrtfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] zrsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	/*306*/
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[]waitperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[]bentrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public FixedLengthStringData[]poltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);//BRD-NBP-011
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData benBillDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData annivProcDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateFromDateDisp = new FixedLengthStringData(10);

	public LongData Sr546screenWritten = new LongData(0);
	public LongData Sr546protectWritten = new LongData(0);	
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1); 
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387

	public boolean hasSubfile() {
		return false;
	}


	public Sr546ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bbldatOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc01Out,new String[] {null, null, null, "02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind01Out,new String[] {"05","02","-05","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc02Out,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {"04","03","-04","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdescOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc03Out,new String[] {null, null, null, "43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind03Out,new String[] {"44","42","-42","42",null, null, null, null, null, null, null, null});
		/*BRD-306 STARTS*/
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		/*BRD-306 END*/
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(waitperiodOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentrmOut,new String[] {null, null, null, "56", null, null, null, null, null, null, null, null});
		fieldIndMap.put(poltypOut,new String[] {null, null, null, "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {null, null, null, "58", null, null, null, null, null, null, null, null});
		//ILIFE-3399-STARTS
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "59", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "60", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(optdsc02Out,new String[] {null, null, null, "73",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {null,null,null,"74",null, null, null, null, null, null, null, null});
		fieldIndMap.put(crrcdOut,new String[] {null, null, null,"77",null, null, null, null, null, null, null, null});//ILJ-45
		//fieldIndMap.put(rcessageOut,new String[] {null, null, null,"78",null, null, null, null, null, null, null, null});//ILJ-45
		fieldIndMap.put(rcesdteOut,new String[] {null, null, null,"79",null, null, null, null, null, null, null, null});//ILJ-45
		
		screenFields = new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, zrsumin, frqdesc, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, mortcls, optdsc01, optind01, optdsc02, optind02, bappmeth, zdesc, zlinstprem, taxamt, optdsc03, optind03,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, riskCessAge, riskCessTerm, premCessAge, premCessTerm, zstpduty01,waitperiod,bentrm,poltyp,prmbasis,dialdownoption, optdsc04, optind04};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, zrsuminOut, frqdescOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, rrtdatOut, rrtfrmOut, bbldatOut, mortclsOut, optdsc01Out, optind01Out, optdsc02Out, optind02Out, bappmethOut, zdescOut, zlinstpremOut, taxamtOut, optdsc03Out, optind03Out,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut, zstpduty01Out,waitperiodOut,bentrmOut,poltypOut,prmbasisOut,dialdownoptionOut , optdsc04Out, optind04Out};
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, zrsuminErr, frqdescErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, mortclsErr, optdsc01Err, optind01Err, optdsc02Err, optind02Err, bappmethErr, zdescErr, zlinstpremErr, taxamtErr, optdsc03Err, optind03Err,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr,rcessageErr, rcesstrmErr, pcessageErr, pcesstrmErr, zstpduty01Err,waitperiodErr,bentrmErr,poltypErr,prmbasisErr,dialdownoptionErr, optdsc04Err, optind04Err};
		screenDateFields = new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate};
		screenDateErrFields = new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr};
		screenDateDispFields = new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, rerateDateDisp, rerateFromDateDisp, benBillDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr546screen.class;
		protectRecord = Sr546protect.class;
	}

}
