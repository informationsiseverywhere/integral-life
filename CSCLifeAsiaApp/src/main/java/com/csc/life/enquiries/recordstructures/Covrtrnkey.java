package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:27
 * Description:
 * Copybook name: COVRTRNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrtrnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrtrnFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covrtrnKey = new FixedLengthStringData(256).isAPartOf(covrtrnFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrtrnChdrcoy = new FixedLengthStringData(1).isAPartOf(covrtrnKey, 0);
  	public FixedLengthStringData covrtrnChdrnum = new FixedLengthStringData(8).isAPartOf(covrtrnKey, 1);
  	public FixedLengthStringData covrtrnLife = new FixedLengthStringData(2).isAPartOf(covrtrnKey, 9);
  	public FixedLengthStringData covrtrnCoverage = new FixedLengthStringData(2).isAPartOf(covrtrnKey, 11);
  	public FixedLengthStringData covrtrnRider = new FixedLengthStringData(2).isAPartOf(covrtrnKey, 13);
  	public PackedDecimalData covrtrnPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrtrnKey, 15);
  	public PackedDecimalData covrtrnTranno = new PackedDecimalData(5, 0).isAPartOf(covrtrnKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(235).isAPartOf(covrtrnKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrtrnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrtrnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}