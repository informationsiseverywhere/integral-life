package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.CactpfDAO;
import com.csc.fsu.general.dataaccess.dao.DactpfDAO;
import com.csc.fsu.general.dataaccess.model.Cactpf;
import com.csc.fsu.general.dataaccess.model.Dactpf;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.enquiries.screens.Sa577ScreenVars;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pa577 extends ScreenProgCS {

private static final Logger LOGGER = LoggerFactory.getLogger(Pa577.class);
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit1090
	}
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA577");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private String wsaaCalcFlag = "";
	
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private FixedLengthStringData wsaaParams = new FixedLengthStringData(21);
	private PackedDecimalData wsaaCount = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaJboNum = new PackedDecimalData(3, 0);
	
	private Sa577ScreenVars sv = ScreenProgram.getScreenVars(Sa577ScreenVars.class);
			
	private DactpfDAO dactpfDAO = getApplicationContext().getBean("dactpfDAO", DactpfDAO.class);
	private CactpfDAO cactpfDAO = getApplicationContext().getBean("cactpfDAO", CactpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<Integer, List<Dactpf>> dactMap = new LinkedHashMap<Integer, List<Dactpf>>();
	private Map<Integer, List<Cactpf>> cactMap = new LinkedHashMap<Integer, List<Cactpf>>();
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private static final String t3684 = "T3684";
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private Tr386rec tr386rec = new Tr386rec();
	public ZonedDecimalData wsaadate = new ZonedDecimalData(8, 0);
	
	private static final String OUTGOING = "Outgoing";
	private static final String RETURN = "Return";
	
	
	public Pa577() {
		super();
		screenVars = sv;
		new ScreenModel("Sa577", AppVars.getInstance(), sv);
	}
	@Override
	protected void preScreenEdit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	protected void initialise1000()
	{
		initialise1010();
		a7050Begin();
		continue1020();
		if (isEQ(wsspcomn.sbmaction, "A")) {
			getDebitFiles1030();
		} else if (isEQ(wsspcomn.sbmaction, "B")) {
			getCreditFiles1030();
		}
	}
	
	protected void initialise1010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);

		/* Clear the screen. */
		scrnparams.function.set(varcom.sclr);
		processScreen("Sa577", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn = new ZonedDecimalData(1);
		if (isEQ(wsaaCalcFlag, "Y")) {
			wsaaCalcFlag = "N";
		}
		wsaaParams.set(SPACES);
		
		
	}
	
	protected void continue1020() {
		/***
		 * Set screen fields
		 */
		sv.factHouseNum.set(wsspcomn.bankcode.toString());
		descpf = descDAO.getdescData("IT", t3684, sv.factHouseNum.toString(), wsspcomn.fsuco.toString(),
				wsspcomn.language.toString());/* IJTI-1523 */
		if(descpf != null) {
			sv.factHouseName.set(descpf.getLongdesc());
		}else {
			sv.factHouseName.set("");
		}
		
	}
	
	protected void getDebitFiles1030() {
		int sequence = 1;
		String date = wsspcomn.currfrom.toString().trim();
		dactMap = dactpfDAO.getDactByFactHousMap(sv.factHouseNum.toString(), date);
		if(!dactMap.isEmpty()) {
			for (Map.Entry<Integer, List<Dactpf>> entry : dactMap.entrySet()) {
			    List<Dactpf> dactList = (List<Dactpf>) entry.getValue();
			    if(dactList != null && !dactList.isEmpty()) {
			    	
			    	Dactpf dactpf = dactList.get(0);
			    	if(dactpf != null) {
			    		if(dactpf.getDatime() != null){
			    			wsaadate.set(dactpf.getDatime().toString().replace("-","").split(" "));
			    		}
			    		else{
			    			wsaadate.set(SPACES);
			    		}
			    		sv.seq.set(sequence);
			    		sv.ftype.set(OUTGOING);
						sv.userID.set(dactpf.getUsrPrf());/* IJTI-1523 */
						sv.date.set(wsaadate);
				    	if(dactpf.getExtractFile() != null){
				    	    sv.extractFile.set(dactpf.getExtractFile().trim());
				    	}
				    	else{
				    		sv.extractFile.set(SPACES);
				    	}
			    		sv.fTotalRecords.set(dactList.size());
			    		BigDecimal totalAmt = BigDecimal.ZERO;
			    		for(Dactpf dactObj: dactList) {
			    			totalAmt = totalAmt.add(dactObj.getDebitAmt());
			    			
			    		}
			    		sv.fTotalAmt.set(totalAmt);
			    		
			    		wsaaJboNum.add(1);
						
						scrnparams.function.set(varcom.sadd);
						processScreen("Sa577", sv);
						if (isNE(scrnparams.statuz, varcom.oK)) {
							syserrrec.statuz.set(scrnparams.statuz);
							fatalError600();
						}
						wsaaCount.add(1);
						sequence++;
			    	
						if(dactpf.getReturnFile() != null && !dactpf.getReturnFile().trim().isEmpty()) {
							sv.ftype.set(RETURN);
							sv.seq.set(sequence);
							sv.extractFile.set(dactpf.getReturnFile());
							scrnparams.function.set(varcom.sadd);
							processScreen("Sa577", sv);
							if (isNE(scrnparams.statuz, varcom.oK)) {
								syserrrec.statuz.set(scrnparams.statuz);
								fatalError600();
							}
							wsaaCount.add(1);
							sequence++;
						}
			    	}
			    }
			}
		}
	}
	
	private void getCreditFiles1030() {
		int sequence = 1;
		String date = wsspcomn.currfrom.toString().trim();
		cactMap = cactpfDAO.getCactByFactHousMap(sv.factHouseNum.toString(), date);
		if(!cactMap.isEmpty()) {
			for (Map.Entry<Integer, List<Cactpf>> entry : cactMap.entrySet()) {
			    List<Cactpf> cactList = (List<Cactpf>) entry.getValue();
			    if(cactList != null && !cactList.isEmpty()) {
			    	
			    	Cactpf cactpf = cactList.get(0);
			    	if(cactpf != null) {
			    		if(cactpf.getDatime() != null){
			    			wsaadate.set(cactpf.getDatime().toString().replace("-","").split(" "));
			    		}
			    		else{
			    			wsaadate.set(SPACES);
			    		}
			    		sv.seq.set(sequence);
			    		sv.ftype.set(OUTGOING);
						sv.userID.set(cactpf.getUserProfile());/* IJTI-1523 */
						sv.date.set(wsaadate);
				    	if(cactpf.getExtractFile() != null){
				    	    sv.extractFile.set(cactpf.getExtractFile().trim());
				    	}
				    	else{
				    		sv.extractFile.set(SPACES);
				    	}
			    		sv.fTotalRecords.set(cactList.size());
			    		BigDecimal totalAmt = new BigDecimal(0);
			    		for(Cactpf cactObj: cactList) {
			    			totalAmt = totalAmt.add(cactObj.getDebitAmt());
			    		}
			    		sv.fTotalAmt.set(totalAmt);
			    	}
			    }
			   
				wsaaJboNum.add(1);
				
				scrnparams.function.set(varcom.sadd);
				processScreen("Sr50e", sv);
				if (isNE(scrnparams.statuz, varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
				wsaaCount.add(1);
				sequence++;
			}
		}
	}

	protected void screenEdit2000()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsaaCalcFlag = "Y";
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsspcomn.edterror.set("Y");
		}
	}
	

	protected void update3000()
	{
		if (isEQ(wsaaCalcFlag, "Y")) {
			return;
		}
	}
	
	protected void whereNext4000() {
		if (isEQ(wsaaCalcFlag, "Y")) {
			wsspcomn.nextprog.set(wsaaProg);
			return;
		}
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}
	
	protected void a7050Begin()
	{
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
		
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		if (isEQ(wsspcomn.sbmaction, "A")) {
			sv.scrndesc.set(tr386rec.progdesc[1].toString());
		} else if (isEQ(wsspcomn.sbmaction, "B")) {
			sv.scrndesc.set(tr386rec.progdesc[2].toString());
		}
	}
}
