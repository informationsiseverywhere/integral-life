/*
 * File: Pr57n.java
 * Date: 30 August 2009 0:42:35
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr57n.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.screens.Sr57nScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtsdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtshclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
public class Pr57n extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5019");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");	
	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, "0");	private FixedLengthStringData wsaaNoSurrmeth = new FixedLengthStringData(1).init("N");
	private Validator noSurrenderMethod = new Validator(wsaaNoSurrmeth, "Y");
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaa1stTime = new FixedLengthStringData(1);	
	private ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();
	/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	/*Coverage and Rider Details - Full Surren*/
	private DescTableDAM descIO = new DescTableDAM();		
	/*Life and Joint Life - Part Surr*/
	private LifeptsTableDAM lifeptsIO = new LifeptsTableDAM();
	/*Part Surrender details record*/
	private PtsdclmTableDAM ptsdclmIO = new PtsdclmTableDAM();
	/*Part Surr Header Record - Claim*/
	private PtshclmTableDAM ptshclmIO = new PtshclmTableDAM();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr57nScreenVars sv = ScreenProgram.getScreenVars( Sr57nScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private Wssplife wssplife = new Wssplife();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 





	public Pr57n() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57n", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */

	protected void initialise1000()
	{

		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("Sr57n", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.clamant.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		sv.estimateTotalValue.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.totalamt.set(ZERO);
		sv.totalfee.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrptsIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrptsIO);
		if(isNE(chdrptsIO.getStatuz(),varcom.oK)){
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrptsIO.getChdrnum());
		sv.cnttype.set(chdrptsIO.getCnttype());
		descIO.setDescitem(chdrptsIO.getCnttype());
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.ctypedes.set(descIO.getLongdesc());
		}else{
			sv.ctypedes.set(SPACE);
		}


		descIO.setDataKey(SPACES);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrptsIO.getStatcode());
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.rstate.set(descIO.getShortdesc());
		}else{
			sv.rstate.set(SPACE);
		}
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrptsIO.getPstatcode());
		findDesc1300();
		if(isEQ(descIO.getStatuz(),varcom.oK)){
			sv.pstate.set(descIO.getShortdesc());
		}else{
			sv.pstate.set(SPACE);
		}
		sv.occdate.set(chdrptsIO.getOccdate());
		sv.cownnum.set(chdrptsIO.getCownnum());
		cltsIO.setClntnum(chdrptsIO.getCownnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else{
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}

		sv.btdate.set(chdrptsIO.getBtdate());
		sv.ptdate.set(chdrptsIO.getPtdate());
		//initialize(ptshclmIO.getParams());
		ptshclmIO.setChdrcoy(chdrptsIO.getChdrcoy());
		ptshclmIO.setChdrnum(chdrptsIO.getChdrnum());
		ptshclmIO.setTranno(wsspcomn.tranno);
		ptshclmIO.setPlanSuffix(0);
		ptshclmIO.setFunction(varcom.begn);
		ptshclmIO.setFormat(formatsInner.ptshclmrec);
		SmartFileCode.execute(appVars, ptshclmIO);
		if(isNE(ptshclmIO.getStatuz(),varcom.oK)&&isNE(ptshclmIO.getStatuz(),varcom.endp)){
			syserrrec.statuz.set(ptshclmIO.getStatuz());
			syserrrec.params.set(ptshclmIO.getParams());
			fatalError600();
		}
		if(isEQ(ptshclmIO.getStatuz(),varcom.endp)
				||isNE(ptshclmIO.getChdrcoy(),chdrptsIO.getChdrcoy())
				||isNE(ptshclmIO.getChdrnum(),chdrptsIO.getChdrnum())){
			
			syserrrec.statuz.set(ptshclmIO.getStatuz());
			syserrrec.params.set(ptshclmIO.getParams());
			fatalError600();
		}

		sv.effdate.set(ptshclmIO.getEffdate());
		sv.currcd.set(ptshclmIO.getCurrcd());
		sv.totalamt.set(ptshclmIO.getTotalamt());
		sv.prcnt.set(ptshclmIO.getPrcnt());
		lifeptsIO.setDataArea(SPACES);
		lifeptsIO.setChdrcoy(ptshclmIO.getChdrcoy());
		lifeptsIO.setChdrnum(ptshclmIO.getChdrnum());
		lifeptsIO.setLife(ptshclmIO.getLife());
		lifeptsIO.setJlife(SPACES);
		lifeptsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeptsIO);
		if(isNE(lifeptsIO.getStatuz(),varcom.oK)&&isNE(lifeptsIO.getStatuz(),varcom.endp)){
			syserrrec.statuz.set(lifeptsIO.getStatuz());
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}
		if(isNE(ptshclmIO.getChdrcoy(),lifeptsIO.getChdrcoy())
				||isNE(ptshclmIO.getChdrnum(),lifeptsIO.getChdrnum())
				||isNE(ptshclmIO.getLife(),lifeptsIO.getLife())
				||isNE(lifeptsIO.getJlife(),"00")
				||isEQ(lifeptsIO.getStatuz(),varcom.endp)){
			syserrrec.statuz.set(lifeptsIO.getStatuz());
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}


		sv.lifcnum.set(lifeptsIO.getLifcnum());
		cltsIO.setClntnum(lifeptsIO.getLifcnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.linsnameErr.set(errorsInner.e304);
			sv.linsname.set(SPACES);
		}
		else{
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}

		lifeptsIO.setJlife("01");
		lifeptsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeptsIO);
		if(isNE(lifeptsIO.getStatuz(),varcom.oK)&&isNE(lifeptsIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}

		if(isNE(lifeptsIO.getStatuz(),varcom.mrnf)){
			lifeptsIO.setJlife(SPACES);
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			continue1030();
		}
		//else{

			//initialize(ptsdclmIO.getParams());
			ptsdclmIO.setChdrcoy(chdrptsIO.getChdrcoy());
			ptsdclmIO.setChdrnum(chdrptsIO.getChdrnum());
			ptsdclmIO.setTranno(ptshclmIO.getTranno());
			ptsdclmIO.setPlanSuffix(ptshclmIO.getPlanSuffix());
			ptsdclmIO.setLife(ptshclmIO.getLife());
			ptsdclmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, ptsdclmIO);
			if(isNE(ptsdclmIO.getStatuz(),varcom.endp)&&isNE(ptsdclmIO.getStatuz(),varcom.oK)){
				syserrrec.params.set(ptsdclmIO.getParams());
				fatalError600();
			}
			if(isNE(chdrptsIO.getChdrcoy(),ptsdclmIO.getChdrcoy())||isNE(chdrptsIO.getChdrnum(),ptsdclmIO.getChdrnum())||isEQ(ptsdclmIO.getStatuz(),varcom.endp)){
				syserrrec.params.set(ptsdclmIO.getParams());
				fatalError600();
			}
			while(isNE(ptsdclmIO.getStatuz(),varcom.endp)){
				processPtsdclm1500();
			}
			sv.estimateTotalValue.set(wsaaEstimateTot);
			sv.clamant.set(wsaaActualTot);
		//}
	}




	protected void  continue1030(){
		sv.jlifcnum.set(lifeptsIO.getLifcnum());
		cltsIO.setClntnum(lifeptsIO.getLifcnum());
		getClientDetails1400();
		if(isEQ(cltsIO.getStatuz(),varcom.mrnf)||isNE(cltsIO.getValidflag(),1)){
			sv.jlinsnameErr.set(errorsInner.e304);
			sv.jlinsname.set(SPACES);
		}
		else{
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}	
	}


	protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	private void findDesc1300(){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if(isNE(descIO.getStatuz(),varcom.oK)&&isNE(descIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
	private void getClientDetails1400(){
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if(isNE(cltsIO.getStatuz(),varcom.oK)&&isNE(cltsIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}





	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	



	private void processPtsdclm1500(){

		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(), varcom.oK)
				&& isNE(ptsdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptsdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrptsIO.getChdrnum(), ptsdclmIO.getChdrnum())
				|| isNE(chdrptsIO.getChdrcoy(), ptsdclmIO.getChdrcoy())
				|| isEQ(ptsdclmIO.getStatuz(), varcom.endp)) {
			ptsdclmIO.setStatuz(varcom.endp);
			return;//ILIFE-7572
		}

		if (isEQ(chdrptsIO.getChdrnum(), ptsdclmIO.getChdrnum())){
			sv.estMatValue.set(ZERO);
			sv.crrcd.set(ZERO);
			sv.percreqd.set(ZERO);
			sv.hemv.set(ZERO);
			sv.hactval.set(ZERO);
			sv.actvalue.set(ZERO);
			sv.coverage.set(ptsdclmIO.getCoverage());
			sv.hcover.set(ptsdclmIO.getCoverage());
			if(isEQ(ptsdclmIO.getRider(),"00")){
				sv.rider.set(SPACES);
			}else{
				sv.rider.set(ptsdclmIO.getRider());
			}
			if(isNE(ptsdclmIO.getRider(),"00")){
				sv.coverage.set(SPACES);
			}
			sv.fund.set(ptsdclmIO.getCrtable());
			sv.fieldType.set(ptsdclmIO.getFieldType());
			sv.shortds.set(ptsdclmIO.getShortds());
			sv.cnstcur.set(ptsdclmIO.getCurrcd());
			sv.hcnstcur.set(ptsdclmIO.getCurrcd());
			sv.estMatValue.set(ptsdclmIO.getEstMatValue());
			sv.hemv.set(ptsdclmIO.getEstMatValue());
			sv.percreqd.set(ptsdclmIO.getPercreqd());
			sv.actvalue.set(ptsdclmIO.getActvalue());
		
			if(isNE(sv.fieldType,"c")&&isNE(sv.fieldType,"j")){
				wsaaEstimateTot.add(ptsdclmIO.estMatValue);
				wsaaActualTot.add(ptsdclmIO.actvalue);
			}else{
			
				//subtract ptsdclmEstMatValue  from wsaaEstimateTot subtract ptsdclmActvalue       from wsaaActualTot)){
				compute(wsaaEstimateTot, 2).set(sub(wsaaEstimateTot, ptsdclmIO.estMatValue));									
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, ptsdclmIO.actvalue));				
			}
						scrnparams.function.set(varcom.sadd);
			processScreen("Sr57n", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		ptsdclmIO.setFunction(varcom.nextr);

	}



	
	

	protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6234IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6234-DATA-AREA                         */
		/*                         S6234-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  No updates are required.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		} 
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.set(wsaaProg);;
		scrnparams.function.set("HIDEW"); 
		if(isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.edterror.set("Y");
		}

		/*EXIT*/
	}

	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 

		/* ERRORS */
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData h355 = new FixedLengthStringData(4).init("H355");		
		private FixedLengthStringData h388 = new FixedLengthStringData(4).init("H388");		
	}

	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {		
		private FixedLengthStringData ptshclmrec = new FixedLengthStringData(10).init("PTSHCLMREC");
	}

	/*
	 * Class transformed  from Data Structure TABLES--INNER
	 */
	private static final class TablesInner { 
		private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
		private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");		
		/* TABLES */
		private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");	

	}
}
