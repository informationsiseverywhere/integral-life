package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50S
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50sScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(468);
	public FixedLengthStringData dataFields = new FixedLengthStringData(180).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData jlifedesc = DD.jlifedesc.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,94);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,134);
	public FixedLengthStringData occups = new FixedLengthStringData(8).isAPartOf(dataFields, 142);
	public FixedLengthStringData[] occup = FLSArrayPartOfStructure(2, 4, occups, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(occups, 0, FILLER_REDEFINE);
	public FixedLengthStringData occup01 = DD.occup.copy().isAPartOf(filler,0);
	public FixedLengthStringData occup02 = DD.occup.copy().isAPartOf(filler,4);
	public FixedLengthStringData pursuits = new FixedLengthStringData(24).isAPartOf(dataFields, 150);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(4, 6, pursuits, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler1,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler1,6);
	public FixedLengthStringData pursuit03 = DD.pursuit.copy().isAPartOf(filler1,12);
	public FixedLengthStringData pursuit04 = DD.pursuit.copy().isAPartOf(filler1,18);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData smokings = new FixedLengthStringData(4).isAPartOf(dataFields, 176);
	public FixedLengthStringData[] smoking = FLSArrayPartOfStructure(2, 2, smokings, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(smokings, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01 = DD.smoking.copy().isAPartOf(filler2,0);
	public FixedLengthStringData smoking02 = DD.smoking.copy().isAPartOf(filler2,2);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 180);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData occupsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] occupErr = FLSArrayPartOfStructure(2, 4, occupsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(occupsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData occup01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData occup02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(4, 4, pursuitsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData pursuit03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData pursuit04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData smokingsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] smokingErr = FLSArrayPartOfStructure(2, 4, smokingsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(smokingsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData smoking02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 252);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData occupsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] occupOut = FLSArrayPartOfStructure(2, 12, occupsOut, 0);
	public FixedLengthStringData[][] occupO = FLSDArrayPartOfArrayStructure(12, 1, occupOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(occupsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] occup01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] occup02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(4, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] pursuit03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] pursuit04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData smokingsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(2, 12, smokingsOut, 0);
	public FixedLengthStringData[][] smokingO = FLSDArrayPartOfArrayStructure(12, 1, smokingOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(smokingsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] smoking01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] smoking02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(244);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(50).isAPartOf(subfileArea, 0);
	public ZonedDecimalData agerate = DD.agerate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData extCessDate = DD.ecesdte.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public ZonedDecimalData extCessTerm = DD.ecestrm.copyToZonedDecimal().isAPartOf(subfileFields,11);
	public ZonedDecimalData insprm = DD.insprm.copyToZonedDecimal().isAPartOf(subfileFields,14);
	public FixedLengthStringData opcda = DD.opcda.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData oppc = DD.oppc.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public FixedLengthStringData reasind = DD.reasind.copy().isAPartOf(subfileFields,27);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,28);
	public ZonedDecimalData seqnbr = DD.seqnbr.copyToZonedDecimal().isAPartOf(subfileFields,29);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(subfileFields,32);
	public ZonedDecimalData zmortpct = DD.zmortpct.copyToZonedDecimal().isAPartOf(subfileFields,42);
	public ZonedDecimalData znadjperc = DD.znadjperc.copyToZonedDecimal().isAPartOf(subfileFields,45);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 50);
	public FixedLengthStringData agerateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData ecesdteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData ecestrmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData insprmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData opcdaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData oppcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData reasindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData seqnbrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData zmortpctErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData znadjpercErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 98);
	public FixedLengthStringData[] agerateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] ecesdteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] ecestrmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] insprmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] opcdaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] oppcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] reasindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] seqnbrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] zmortpctOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] znadjpercOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 242);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData extCessDateDisp = new FixedLengthStringData(10);

	public LongData Sr50sscreensflWritten = new LongData(0);
	public LongData Sr50sscreenctlWritten = new LongData(0);
	public LongData Sr50sscreenWritten = new LongData(0);
	public LongData Sr50sprotectWritten = new LongData(0);
	public GeneralTable sr50sscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50sscreensfl;
	}

	public Sr50sScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {seqnbr, opcda, shortdesc, reasind, znadjperc, agerate, oppc, insprm, extCessTerm, select, zmortpct, extCessDate};
		screenSflOutFields = new BaseData[][] {seqnbrOut, opcdaOut, shortdescOut, reasindOut, znadjpercOut, agerateOut, oppcOut, insprmOut, ecestrmOut, selectOut, zmortpctOut, ecesdteOut};
		screenSflErrFields = new BaseData[] {seqnbrErr, opcdaErr, shortdescErr, reasindErr, znadjpercErr, agerateErr, oppcErr, insprmErr, ecestrmErr, selectErr, zmortpctErr, ecesdteErr};
		screenSflDateFields = new BaseData[] {extCessDate};
		screenSflDateErrFields = new BaseData[] {ecesdteErr};
		screenSflDateDispFields = new BaseData[] {extCessDateDisp};

		screenFields = new BaseData[] {chdrnum, life, coverage, rider, lifenum, lifedesc, smoking01, occup01, pursuit01, pursuit02, jlife, jlifedesc, smoking02, occup02, pursuit03, pursuit04, crtable, crtabdesc};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, lifedescOut, smoking01Out, occup01Out, pursuit01Out, pursuit02Out, jlifenumOut, jlifedescOut, smoking02Out, occup02Out, pursuit03Out, pursuit04Out, crtableOut, crtabdescOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, lifedescErr, smoking01Err, occup01Err, pursuit01Err, pursuit02Err, jlifenumErr, jlifedescErr, smoking02Err, occup02Err, pursuit03Err, pursuit04Err, crtableErr, crtabdescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50sscreen.class;
		screenSflRecord = Sr50sscreensfl.class;
		screenCtlRecord = Sr50sscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50sprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50sscreenctl.lrec.pageSubfile);
	}
}
