/*
 * File: P6263.java
 * Date: 30 August 2009 0:41:43
 * Author: Quipoz Limited
 * 
 * Class transformed from P6263.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.screens.S6263ScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RactTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*     P6263 - Reassurance Enquiry.
*     ****************************
*
*     Overview.
*
*     This document specifies the program required to handle
*     enquiry on Reassurance from within Contract Enquiry.
*
*     Reassurance Coverage details will be defined using coverage
*     codes similar to the ordinary Coverage/Rider codes used to
*     define LIFE contracts. Each one of these will be assigned to
*     Coverages or Riders that already exist on the contract and
*     will relate to the selected component on a plan-wide basis,
*     ie. if a Reassurance Code is assigned to Coverage #1 on a
*     plan of 10 policies it will apply to each Coverage #1 on each
*     of the 10 policies. Therefore as Reassurance operates on a
*     Sum Assured basis the amount to be reassured will be a
*     fraction of the total of all the Sums Assured on all 10 of
*     the occurrences of Coverage #1.
*
*     When reassurance is specified for any component on a
*     contract the system will create one reassurance header
*     record on RACT for the whole contract and then one detail
*     record on RACT for each component for which reassurance
*     has been specified.
*
*     Initialise.
*     ***********
*
*     The details of the contract being processed will have been
*     stored in the CHDRENQ I/O module. Use the RETRV
*     function to obtain the contract header details.
*
*     The Coverage/Rider record with which the Reassurance
*     details are associated will be held in the COVRENQ I/O
*     module.
*
*     Perform a RETRV on COVRENQ. This will provide the
*     details of the component to which the Reassurance details
*     are attached.
*
*     Obtain the main Life details by using the Company,
*     Contract and Life returned from COVRENQ plus a Joint Life
*     of '00' to perform a READR on LIFELNB.
*
*     Use the Client Number from LIFELNB,
*     (LIFELNB-LIFCNUM), to do a READR on CLTS to obtain
*     the main life details. Format the name for display using the
*     routine CONFNAME.
*
*     Use the same LIFELNB key except with a Joint Life set to
*     '01' to READR on LIFELNB again in order to retrieve the
*     Joint Life details if they exist.
*
*     If found then use the Client number on that record to
*     obtain the Joint Life details for display as for the main lif
*     above.
*
*     The total sum assured for the selected component must be
*     calculated and displayed. This is carried out as follows:
*
*          Read all the COVRENQ records for the selected
*          component, that is all records that share the same key
*          values for Company, Contract Number, Life, Coverage
*          and Rider and for all possible values of Plan Suffix.
*          Use the information from the Contract Header -
*          Number of Policies In Plan, (POLINS) and Number of
*          Policies Summarised, (POLSUM), in order to set the
*          correct values for Plan Suffix. Accumulate the value of
*          the Sum Assured from each record and  output as
*          'Total S/A for Component'.
*           
*     Use the Contract Company and Contract Number to perform
*     a READR on the Reassurance Contract Header, RACT. If
*     no record is found then there is no Reassurance for the
*     contract so output a message to that effect and go to the
*     end of this section.
*
*     If a matching record is found then proceed as follows:
*
*          Display the Reassurance Account Number, RASNUM,
*          and use this to read RASA, the Reassurance Account
*          master file. Use the client number from there to read
*          CLTS and then use CONFNAME to format the client's
*          name for display.
*           
*          Next attempt to locate a matching record on RACT,
*          the Reassurance Coverage Type file, that matches on
*          the Company, Contract Number and Reassurance
*          Number from RACT and the Life, Coverage and Rider
*          Numbers from COVRENQ.
*           
*          Use the Contract Company, Contract Number,
*          Reassurance Account Number, (RASNUM), from RACT
*          along with the Life, Coverage and Rider Number from
*          COVRENQ to perform a BEGN on RACT, the
*          Reassurance Coverage Type file. If end of file or the
*          main part of the key on the returned record does not
*          match with the Company, Contract Number or
*          Reassurance Number from RACT then report a
*          database error and cease processing.
*           
*          If the record found matches on the portion of the key
*          up to and including Rider Number then the details of
*          the record will be displayed. If the Company, Contract
*          Number and Reassurance Number, (the main part of
*          the key), match but any of the rest of the key does
*          not then output a message informing the user that
*          there is no Reassurance for the selected component.
*
*     Validation.
*     ***********
*
*     Process the screen using 'S6263IO'.
*     There is no validation in this program.
*
*     If 'F9', ('CALC'), was requested re-display the screen.
*
*     Updating.
*     *********
*
*     There is no updating in this program.
*
*     Next Program.
*     *************
*
*     Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P6263 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6263");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsbbRaAmount = new PackedDecimalData(17, 2);
	private String f003 = "F003";
		/* TABLES */
	private String t5687 = "T5687";
	private String t3629 = "T3629";
	private String t5633 = "T5633";
		/* FORMATS */
	private String descrec = "DESCREC";
	private String cltsrec = "CLTSREC";
	private String chdrenqrec = "CHDRENQREC";
	private String covrenqrec = "COVRENQREC";
	private String lifelnbrec = "LIFELNBREC";
	private String ractrec = "RACTREC";
	private String rasarec = "RASAREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Reassurance Contract Type.*/
	private RactTableDAM ractIO = new RactTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S6263ScreenVars sv = ScreenProgram.getScreenVars( S6263ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit
	}

	public P6263() {
		super();
		screenVars = sv;
		new ScreenModel("S6263", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1100();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.instPrem.set(ZERO);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.raAmount.set(ZERO);
		sv.totsuma.set(ZERO);
		chdrenqIO.setFunction(varcom.retrv);
		callChdrenqioModule1100();
		covrenqIO.setFunction(varcom.retrv);
		callCovrenqioModule1200();
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFunction(varcom.readr);
		covrenqIO.setFormat(covrenqrec);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(covrenqIO.getStatuz(),varcom.mrnf)) {
			wsbbRaAmount.set(ZERO);
		}
		else {
			wsbbRaAmount.set(covrenqIO.getSumins());
		}
		setPrecision(covrenqIO.getPlanSuffix(), 0);
		covrenqIO.setPlanSuffix(add(chdrenqIO.getPolsum(),1));
		if (isGT(chdrenqIO.getPolinc(),1)) {
			while ( !(isGT(covrenqIO.getPlanSuffix(),chdrenqIO.getPolinc()))) {
				accumulateRaAmount1900();
			}
			
		}
		sv.totsuma.set(wsbbRaAmount);
		ractIO.setChdrcoy(chdrenqIO.getChdrcoy());
		ractIO.setChdrnum(chdrenqIO.getChdrnum());
		ractIO.setLife(covrenqIO.getLife());
		ractIO.setCoverage(covrenqIO.getCoverage());
		ractIO.setRider(covrenqIO.getRider());
		ractIO.setRasnum(SPACES);
		ractIO.setRatype(SPACES);
		ractIO.setValidflag(SPACES);
		ractIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ractIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ractIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		callRactioModule1500();
		if (isEQ(ractIO.getStatuz(),varcom.endp)
		|| isNE(ractIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(ractIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(ractIO.getLife(),covrenqIO.getLife())
		|| isNE(ractIO.getCoverage(),covrenqIO.getCoverage())
		|| isNE(ractIO.getRider(),covrenqIO.getRider())
		|| isNE(ractIO.getValidflag(),"1")) {
			scrnparams.errorCode.set(f003);
			goTo(GotoLabel.exit1090);
		}
		sv.rasnum.set(ractIO.getRasnum());
		rasaIO.setRasnum(ractIO.getRasnum());
		rasaIO.setRascoy(ractIO.getChdrcoy());
		sv.btdate.set(ractIO.getBtdate());
		rasaIO.setFunction(varcom.readr);
		callRasaioModule1700();
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setClntcoy(rasaIO.getClntcoy());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		callCltsioModule1400();
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
		sv.rapayfrq.set(ractIO.getRapayfrq());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ractIO.getChdrcoy());
		descIO.setDesctabl(t5633);
		descIO.setDescitem(ractIO.getRapayfrq());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		callDescioModule1800();
		sv.payfrqd.set(descIO.getShortdesc());
		sv.raAmount.set(ractIO.getRaAmount());
		if (isEQ(ractIO.getSingp(),ZERO)) {
			sv.instPrem.set(ractIO.getInstprem());
		}
		else {
			sv.instPrem.set(ractIO.getSingp());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ractIO.getChdrcoy());
		descIO.setDesctabl(t5687);
		sv.ratype.set(ractIO.getRatype());
		descIO.setDescitem(ractIO.getRatype());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		callDescioModule1800();
		sv.ratypeDesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ractIO.getChdrcoy());
		descIO.setDesctabl(t3629);
		sv.currcode.set(ractIO.getCurrcode());
		descIO.setDescitem(ractIO.getCurrcode());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		callDescioModule1800();
		sv.currds.set(descIO.getLongdesc());
	}

protected void callChdrenqioModule1100()
	{
		/*PARA*/
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callCovrenqioModule1200()
	{
		/*PARA*/
		covrenqIO.setFormat(covrenqrec);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callLifelnbioModule1300()
	{
		/*PARA*/
		lifelnbIO.setChdrcoy(covrenqIO.getChdrcoy());
		lifelnbIO.setChdrnum(covrenqIO.getChdrnum());
		lifelnbIO.setLife(covrenqIO.getLife());
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callCltsioModule1400()
	{
		/*PARA*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callRactioModule1500()
	{
		/*PARA*/
		ractIO.setFormat(ractrec);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)
		&& isNE(ractIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callRasaioModule1700()
	{
		/*PARA*/
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)
		&& isNE(rasaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callDescioModule1800()
	{
		/*PARA*/
		descIO.setFormat(descrec);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void accumulateRaAmount1900()
	{
		/*PARA*/
		covrenqIO.setFunction(varcom.readr);
		callCovrenqioModule1200();
		compute(wsbbRaAmount, 2).set(add(wsbbRaAmount,covrenqIO.getSumins()));
		setPrecision(covrenqIO.getPlanSuffix(), 0);
		covrenqIO.setPlanSuffix(add(covrenqIO.getPlanSuffix(),1));
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.function.set(varcom.prot);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
