package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr500screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr500ScreenVars sv = (Sr500ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr500screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr500ScreenVars screenVars = (Sr500ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.jowner.setClassString("");
		screenVars.jownername.setClassString("");
		screenVars.payer.setClassString("");
		screenVars.payername.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.servagnt.setClassString("");
		screenVars.servagnam.setClassString("");
		screenVars.servbr.setClassString("");
		screenVars.brchname.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.payfreq.setClassString("");
		screenVars.lastinsdteDisp.setClassString("");
		screenVars.instpramt.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.nextinsdteDisp.setClassString("");
		screenVars.nextinsamt.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.hpropdteDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.hprrcvdtDisp.setClassString("");
		screenVars.hoissdteDisp.setClassString("");
		screenVars.huwdcdteDisp.setClassString("");
		screenVars.znfopt.setClassString("");
		screenVars.hissdteDisp.setClassString("");
		screenVars.ind01.setClassString("");
		screenVars.ind02.setClassString("");
		screenVars.ind03.setClassString("");
		screenVars.ind04.setClassString("");
	}

/**
 * Clear all the variables in Sr500screen
 */
	public static void clear(VarModel pv) {
		Sr500ScreenVars screenVars = (Sr500ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.jowner.clear();
		screenVars.jownername.clear();
		screenVars.payer.clear();
		screenVars.payername.clear();
		screenVars.indic.clear();
		screenVars.servagnt.clear();
		screenVars.servagnam.clear();
		screenVars.servbr.clear();
		screenVars.brchname.clear();
		screenVars.numpols.clear();
		screenVars.billcurr.clear();
		screenVars.mop.clear();
		screenVars.payfreq.clear();
		screenVars.lastinsdteDisp.clear();
		screenVars.lastinsdte.clear();
		screenVars.instpramt.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.nextinsdteDisp.clear();
		screenVars.nextinsdte.clear();
		screenVars.nextinsamt.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.hpropdteDisp.clear();
		screenVars.hpropdte.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.hprrcvdtDisp.clear();
		screenVars.hprrcvdt.clear();
		screenVars.hoissdteDisp.clear();
		screenVars.hoissdte.clear();
		screenVars.huwdcdteDisp.clear();
		screenVars.huwdcdte.clear();
		screenVars.znfopt.clear();
		screenVars.hissdteDisp.clear();
		screenVars.hissdte.clear();
		screenVars.ind01.clear();
		screenVars.ind02.clear();
		screenVars.ind03.clear();
		screenVars.ind04.clear();
	}
}
