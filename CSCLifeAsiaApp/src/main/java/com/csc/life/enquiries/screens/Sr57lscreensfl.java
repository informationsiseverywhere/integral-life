package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class Sr57lscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 8;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {10, 1, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 17, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57lScreenVars sv = (Sr57lScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.Sr57lscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.Sr57lscreensfl, 
			sv.Sr57lscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr57lScreenVars sv = (Sr57lScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.Sr57lscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr57lScreenVars sv = (Sr57lScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.Sr57lscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr57lscreensflWritten.gt(0))
		{
			sv.Sr57lscreensfl.setCurrentIndex(0);
			sv.Sr57lscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr57lScreenVars sv = (Sr57lScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.Sr57lscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr57lScreenVars screenVars = (Sr57lScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hactval.setFieldName("hactval");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hemv.setFieldName("hemv");
				screenVars.htype.setFieldName("htype");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fund.setFieldName("fund");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.life.setFieldName("life");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hactval.set(dm.getField("hactval"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.hemv.set(dm.getField("hemv"));
			screenVars.htype.set(dm.getField("htype"));
			screenVars.hcnstcur.set(dm.getField("hcnstcur"));
			screenVars.hcover.set(dm.getField("hcover"));
			screenVars.hjlife.set(dm.getField("hjlife"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.fund.set(dm.getField("fund"));
			screenVars.fieldType.set(dm.getField("fieldType"));
			screenVars.shortds.set(dm.getField("shortds"));
			screenVars.cnstcur.set(dm.getField("cnstcur"));
			screenVars.estMatValue.set(dm.getField("estMatValue"));
			screenVars.actvalue.set(dm.getField("actvalue"));
			screenVars.life.set(dm.getField("life"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr57lScreenVars screenVars = (Sr57lScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hactval.setFieldName("hactval");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hemv.setFieldName("hemv");
				screenVars.htype.setFieldName("htype");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fund.setFieldName("fund");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.life.setFieldName("life");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hactval").set(screenVars.hactval);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("hemv").set(screenVars.hemv);
			dm.getField("htype").set(screenVars.htype);
			dm.getField("hcnstcur").set(screenVars.hcnstcur);
			dm.getField("hcover").set(screenVars.hcover);
			dm.getField("hjlife").set(screenVars.hjlife);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("fund").set(screenVars.fund);
			dm.getField("fieldType").set(screenVars.fieldType);
			dm.getField("shortds").set(screenVars.shortds);
			dm.getField("cnstcur").set(screenVars.cnstcur);
			dm.getField("estMatValue").set(screenVars.estMatValue);
			dm.getField("actvalue").set(screenVars.actvalue);
			dm.getField("life").set(screenVars.life);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr57lscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr57lScreenVars screenVars = (Sr57lScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hactval.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.hemv.clearFormatting();
		screenVars.htype.clearFormatting();
		screenVars.hcnstcur.clearFormatting();
		screenVars.hcover.clearFormatting();
		screenVars.hjlife.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.fund.clearFormatting();
		screenVars.fieldType.clearFormatting();
		screenVars.shortds.clearFormatting();
		screenVars.cnstcur.clearFormatting();
		screenVars.estMatValue.clearFormatting();
		screenVars.actvalue.clearFormatting();
		screenVars.life.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr57lScreenVars screenVars = (Sr57lScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hactval.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.hemv.setClassString("");
		screenVars.htype.setClassString("");
		screenVars.hcnstcur.setClassString("");
		screenVars.hcover.setClassString("");
		screenVars.hjlife.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.fund.setClassString("");
		screenVars.fieldType.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.cnstcur.setClassString("");
		screenVars.estMatValue.setClassString("");
		screenVars.actvalue.setClassString("");
		screenVars.life.setClassString("");
	}

/**
 * Clear all the variables in Sr57lscreensfl
 */
	public static void clear(VarModel pv) {
		Sr57lScreenVars screenVars = (Sr57lScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hactval.clear();
		screenVars.hcrtable.clear();
		screenVars.hemv.clear();
		screenVars.htype.clear();
		screenVars.hcnstcur.clear();
		screenVars.hcover.clear();
		screenVars.hjlife.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.fund.clear();
		screenVars.fieldType.clear();
		screenVars.shortds.clear();
		screenVars.cnstcur.clear();
		screenVars.estMatValue.clear();
		screenVars.actvalue.clear();
		screenVars.life.clear();
	}
}
