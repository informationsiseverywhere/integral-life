package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr597screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr597ScreenVars sv = (Sr597ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr597screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr597ScreenVars screenVars = (Sr597ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.setCnRiskStat01.setClassString("");
		screenVars.setCnRiskStat02.setClassString("");
		screenVars.setCnRiskStat03.setClassString("");
		screenVars.setCnRiskStat04.setClassString("");
		screenVars.setCnRiskStat05.setClassString("");
		screenVars.setCnRiskStat06.setClassString("");
		screenVars.setCnRiskStat07.setClassString("");
		screenVars.setCnRiskStat08.setClassString("");
		screenVars.setCnRiskStat09.setClassString("");
		screenVars.setCnRiskStat10.setClassString("");
		screenVars.setCnRiskStat11.setClassString("");
		screenVars.setCnRiskStat12.setClassString("");
		screenVars.setCnRiskStat13.setClassString("");
		screenVars.setCnRiskStat14.setClassString("");
		screenVars.setCnRiskStat15.setClassString("");
		screenVars.setCnRiskStat16.setClassString("");
		screenVars.setCnRiskStat17.setClassString("");
		screenVars.setCnRiskStat18.setClassString("");
		screenVars.setCnRiskStat19.setClassString("");
		screenVars.setCnRiskStat20.setClassString("");
		screenVars.setCnPremStat01.setClassString("");
		screenVars.setCnPremStat02.setClassString("");
		screenVars.setCnPremStat03.setClassString("");
		screenVars.setCnPremStat04.setClassString("");
		screenVars.setCnPremStat05.setClassString("");
		screenVars.setCnPremStat06.setClassString("");
		screenVars.setCnPremStat07.setClassString("");
		screenVars.setCnPremStat08.setClassString("");
		screenVars.setCnPremStat09.setClassString("");
		screenVars.setCnPremStat10.setClassString("");
		screenVars.setCnPremStat11.setClassString("");
		screenVars.setCnPremStat12.setClassString("");
		screenVars.setCnPremStat13.setClassString("");
		screenVars.setCnPremStat14.setClassString("");
		screenVars.setCnPremStat15.setClassString("");
		screenVars.setCnPremStat16.setClassString("");
		screenVars.setCnPremStat17.setClassString("");
		screenVars.setCnPremStat18.setClassString("");
		screenVars.setCnPremStat19.setClassString("");
		screenVars.setCnPremStat20.setClassString("");
		screenVars.cnRiskStat01.setClassString("");
		screenVars.cnRiskStat02.setClassString("");
		screenVars.cnRiskStat03.setClassString("");
		screenVars.cnRiskStat04.setClassString("");
		screenVars.cnRiskStat05.setClassString("");
		screenVars.cnRiskStat06.setClassString("");
		screenVars.cnRiskStat07.setClassString("");
		screenVars.cnRiskStat08.setClassString("");
		screenVars.cnRiskStat09.setClassString("");
		screenVars.cnRiskStat10.setClassString("");
		screenVars.cnRiskStat11.setClassString("");
		screenVars.cnRiskStat12.setClassString("");
		screenVars.cnRiskStat13.setClassString("");
		screenVars.cnRiskStat14.setClassString("");
		screenVars.cnRiskStat15.setClassString("");
		screenVars.cnRiskStat16.setClassString("");
		screenVars.cnRiskStat17.setClassString("");
		screenVars.cnRiskStat18.setClassString("");
		screenVars.cnRiskStat19.setClassString("");
		screenVars.cnRiskStat20.setClassString("");
		screenVars.cnPremStat01.setClassString("");
		screenVars.cnPremStat02.setClassString("");
		screenVars.cnPremStat03.setClassString("");
		screenVars.cnPremStat04.setClassString("");
		screenVars.cnPremStat05.setClassString("");
		screenVars.cnPremStat06.setClassString("");
		screenVars.cnPremStat07.setClassString("");
		screenVars.cnPremStat08.setClassString("");
		screenVars.cnPremStat09.setClassString("");
		screenVars.cnPremStat10.setClassString("");
		screenVars.cnPremStat11.setClassString("");
		screenVars.cnPremStat12.setClassString("");
		screenVars.cnPremStat13.setClassString("");
		screenVars.cnPremStat14.setClassString("");
		screenVars.cnPremStat15.setClassString("");
		screenVars.cnPremStat16.setClassString("");
		screenVars.cnPremStat17.setClassString("");
		screenVars.cnPremStat18.setClassString("");
		screenVars.cnPremStat19.setClassString("");
		screenVars.cnPremStat20.setClassString("");
		screenVars.covRiskStat01.setClassString("");
		screenVars.covRiskStat02.setClassString("");
		screenVars.covRiskStat03.setClassString("");
		screenVars.covRiskStat04.setClassString("");
		screenVars.covRiskStat05.setClassString("");
		screenVars.covRiskStat06.setClassString("");
		screenVars.covRiskStat07.setClassString("");
		screenVars.covRiskStat08.setClassString("");
		screenVars.covRiskStat09.setClassString("");
		screenVars.covRiskStat10.setClassString("");
		screenVars.covRiskStat11.setClassString("");
		screenVars.covRiskStat12.setClassString("");
		screenVars.covRiskStat13.setClassString("");
		screenVars.covRiskStat14.setClassString("");
		screenVars.covRiskStat15.setClassString("");
		screenVars.covRiskStat16.setClassString("");
		screenVars.covRiskStat17.setClassString("");
		screenVars.covRiskStat18.setClassString("");
		screenVars.covRiskStat19.setClassString("");
		screenVars.covRiskStat20.setClassString("");
		screenVars.covPremStat01.setClassString("");
		screenVars.covPremStat02.setClassString("");
		screenVars.covPremStat03.setClassString("");
		screenVars.covPremStat04.setClassString("");
		screenVars.covPremStat05.setClassString("");
		screenVars.covPremStat06.setClassString("");
		screenVars.covPremStat07.setClassString("");
		screenVars.covPremStat08.setClassString("");
		screenVars.covPremStat09.setClassString("");
		screenVars.covPremStat10.setClassString("");
		screenVars.covPremStat11.setClassString("");
		screenVars.covPremStat12.setClassString("");
		screenVars.covPremStat13.setClassString("");
		screenVars.covPremStat14.setClassString("");
		screenVars.covPremStat15.setClassString("");
		screenVars.covPremStat16.setClassString("");
		screenVars.covPremStat17.setClassString("");
		screenVars.covPremStat18.setClassString("");
		screenVars.covPremStat19.setClassString("");
		screenVars.covPremStat20.setClassString("");
		screenVars.cntcurr.setClassString("");
	}

/**
 * Clear all the variables in Sr597screen
 */
	public static void clear(VarModel pv) {
		Sr597ScreenVars screenVars = (Sr597ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.setCnRiskStat01.clear();
		screenVars.setCnRiskStat02.clear();
		screenVars.setCnRiskStat03.clear();
		screenVars.setCnRiskStat04.clear();
		screenVars.setCnRiskStat05.clear();
		screenVars.setCnRiskStat06.clear();
		screenVars.setCnRiskStat07.clear();
		screenVars.setCnRiskStat08.clear();
		screenVars.setCnRiskStat09.clear();
		screenVars.setCnRiskStat10.clear();
		screenVars.setCnRiskStat11.clear();
		screenVars.setCnRiskStat12.clear();
		screenVars.setCnRiskStat13.clear();
		screenVars.setCnRiskStat14.clear();
		screenVars.setCnRiskStat15.clear();
		screenVars.setCnRiskStat16.clear();
		screenVars.setCnRiskStat17.clear();
		screenVars.setCnRiskStat18.clear();
		screenVars.setCnRiskStat19.clear();
		screenVars.setCnRiskStat20.clear();
		screenVars.setCnPremStat01.clear();
		screenVars.setCnPremStat02.clear();
		screenVars.setCnPremStat03.clear();
		screenVars.setCnPremStat04.clear();
		screenVars.setCnPremStat05.clear();
		screenVars.setCnPremStat06.clear();
		screenVars.setCnPremStat07.clear();
		screenVars.setCnPremStat08.clear();
		screenVars.setCnPremStat09.clear();
		screenVars.setCnPremStat10.clear();
		screenVars.setCnPremStat11.clear();
		screenVars.setCnPremStat12.clear();
		screenVars.setCnPremStat13.clear();
		screenVars.setCnPremStat14.clear();
		screenVars.setCnPremStat15.clear();
		screenVars.setCnPremStat16.clear();
		screenVars.setCnPremStat17.clear();
		screenVars.setCnPremStat18.clear();
		screenVars.setCnPremStat19.clear();
		screenVars.setCnPremStat20.clear();
		screenVars.cnRiskStat01.clear();
		screenVars.cnRiskStat02.clear();
		screenVars.cnRiskStat03.clear();
		screenVars.cnRiskStat04.clear();
		screenVars.cnRiskStat05.clear();
		screenVars.cnRiskStat06.clear();
		screenVars.cnRiskStat07.clear();
		screenVars.cnRiskStat08.clear();
		screenVars.cnRiskStat09.clear();
		screenVars.cnRiskStat10.clear();
		screenVars.cnRiskStat11.clear();
		screenVars.cnRiskStat12.clear();
		screenVars.cnRiskStat13.clear();
		screenVars.cnRiskStat14.clear();
		screenVars.cnRiskStat15.clear();
		screenVars.cnRiskStat16.clear();
		screenVars.cnRiskStat17.clear();
		screenVars.cnRiskStat18.clear();
		screenVars.cnRiskStat19.clear();
		screenVars.cnRiskStat20.clear();
		screenVars.cnPremStat01.clear();
		screenVars.cnPremStat02.clear();
		screenVars.cnPremStat03.clear();
		screenVars.cnPremStat04.clear();
		screenVars.cnPremStat05.clear();
		screenVars.cnPremStat06.clear();
		screenVars.cnPremStat07.clear();
		screenVars.cnPremStat08.clear();
		screenVars.cnPremStat09.clear();
		screenVars.cnPremStat10.clear();
		screenVars.cnPremStat11.clear();
		screenVars.cnPremStat12.clear();
		screenVars.cnPremStat13.clear();
		screenVars.cnPremStat14.clear();
		screenVars.cnPremStat15.clear();
		screenVars.cnPremStat16.clear();
		screenVars.cnPremStat17.clear();
		screenVars.cnPremStat18.clear();
		screenVars.cnPremStat19.clear();
		screenVars.cnPremStat20.clear();
		screenVars.covRiskStat01.clear();
		screenVars.covRiskStat02.clear();
		screenVars.covRiskStat03.clear();
		screenVars.covRiskStat04.clear();
		screenVars.covRiskStat05.clear();
		screenVars.covRiskStat06.clear();
		screenVars.covRiskStat07.clear();
		screenVars.covRiskStat08.clear();
		screenVars.covRiskStat09.clear();
		screenVars.covRiskStat10.clear();
		screenVars.covRiskStat11.clear();
		screenVars.covRiskStat12.clear();
		screenVars.covRiskStat13.clear();
		screenVars.covRiskStat14.clear();
		screenVars.covRiskStat15.clear();
		screenVars.covRiskStat16.clear();
		screenVars.covRiskStat17.clear();
		screenVars.covRiskStat18.clear();
		screenVars.covRiskStat19.clear();
		screenVars.covRiskStat20.clear();
		screenVars.covPremStat01.clear();
		screenVars.covPremStat02.clear();
		screenVars.covPremStat03.clear();
		screenVars.covPremStat04.clear();
		screenVars.covPremStat05.clear();
		screenVars.covPremStat06.clear();
		screenVars.covPremStat07.clear();
		screenVars.covPremStat08.clear();
		screenVars.covPremStat09.clear();
		screenVars.covPremStat10.clear();
		screenVars.covPremStat11.clear();
		screenVars.covPremStat12.clear();
		screenVars.covPremStat13.clear();
		screenVars.covPremStat14.clear();
		screenVars.covPremStat15.clear();
		screenVars.covPremStat16.clear();
		screenVars.covPremStat17.clear();
		screenVars.covPremStat18.clear();
		screenVars.covPremStat19.clear();
		screenVars.covPremStat20.clear();
		screenVars.cntcurr.clear();
	}
}
