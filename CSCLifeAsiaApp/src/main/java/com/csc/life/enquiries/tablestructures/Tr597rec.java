package com.csc.life.enquiries.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:15
 * Description:
 * Copybook name: TR597REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr597rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr597Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cnPremStats = new FixedLengthStringData(40).isAPartOf(tr597Rec, 0);
  	public FixedLengthStringData[] cnPremStat = FLSArrayPartOfStructure(20, 2, cnPremStats, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(cnPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cnPremStat01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData cnPremStat02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData cnPremStat03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData cnPremStat04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData cnPremStat05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData cnPremStat06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData cnPremStat07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData cnPremStat08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData cnPremStat09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData cnPremStat10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData cnPremStat11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData cnPremStat12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData cnPremStat13 = new FixedLengthStringData(2).isAPartOf(filler, 24);
  	public FixedLengthStringData cnPremStat14 = new FixedLengthStringData(2).isAPartOf(filler, 26);
  	public FixedLengthStringData cnPremStat15 = new FixedLengthStringData(2).isAPartOf(filler, 28);
  	public FixedLengthStringData cnPremStat16 = new FixedLengthStringData(2).isAPartOf(filler, 30);
  	public FixedLengthStringData cnPremStat17 = new FixedLengthStringData(2).isAPartOf(filler, 32);
  	public FixedLengthStringData cnPremStat18 = new FixedLengthStringData(2).isAPartOf(filler, 34);
  	public FixedLengthStringData cnPremStat19 = new FixedLengthStringData(2).isAPartOf(filler, 36);
  	public FixedLengthStringData cnPremStat20 = new FixedLengthStringData(2).isAPartOf(filler, 38);
  	public FixedLengthStringData cnRiskStats = new FixedLengthStringData(40).isAPartOf(tr597Rec, 40);
  	public FixedLengthStringData[] cnRiskStat = FLSArrayPartOfStructure(20, 2, cnRiskStats, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(cnRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cnRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData cnRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData cnRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData cnRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData cnRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData cnRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData cnRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData cnRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData cnRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData cnRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	public FixedLengthStringData cnRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler1, 20);
  	public FixedLengthStringData cnRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler1, 22);
  	public FixedLengthStringData cnRiskStat13 = new FixedLengthStringData(2).isAPartOf(filler1, 24);
  	public FixedLengthStringData cnRiskStat14 = new FixedLengthStringData(2).isAPartOf(filler1, 26);
  	public FixedLengthStringData cnRiskStat15 = new FixedLengthStringData(2).isAPartOf(filler1, 28);
  	public FixedLengthStringData cnRiskStat16 = new FixedLengthStringData(2).isAPartOf(filler1, 30);
  	public FixedLengthStringData cnRiskStat17 = new FixedLengthStringData(2).isAPartOf(filler1, 32);
  	public FixedLengthStringData cnRiskStat18 = new FixedLengthStringData(2).isAPartOf(filler1, 34);
  	public FixedLengthStringData cnRiskStat19 = new FixedLengthStringData(2).isAPartOf(filler1, 36);
  	public FixedLengthStringData cnRiskStat20 = new FixedLengthStringData(2).isAPartOf(filler1, 38);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(tr597Rec, 80);
  	public FixedLengthStringData covPremStats = new FixedLengthStringData(40).isAPartOf(tr597Rec, 83);
  	public FixedLengthStringData[] covPremStat = FLSArrayPartOfStructure(20, 2, covPremStats, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(covPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData covPremStat01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData covPremStat02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData covPremStat03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData covPremStat04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData covPremStat05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData covPremStat06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData covPremStat07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData covPremStat08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData covPremStat09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData covPremStat10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData covPremStat11 = new FixedLengthStringData(2).isAPartOf(filler2, 20);
  	public FixedLengthStringData covPremStat12 = new FixedLengthStringData(2).isAPartOf(filler2, 22);
  	public FixedLengthStringData covPremStat13 = new FixedLengthStringData(2).isAPartOf(filler2, 24);
  	public FixedLengthStringData covPremStat14 = new FixedLengthStringData(2).isAPartOf(filler2, 26);
  	public FixedLengthStringData covPremStat15 = new FixedLengthStringData(2).isAPartOf(filler2, 28);
  	public FixedLengthStringData covPremStat16 = new FixedLengthStringData(2).isAPartOf(filler2, 30);
  	public FixedLengthStringData covPremStat17 = new FixedLengthStringData(2).isAPartOf(filler2, 32);
  	public FixedLengthStringData covPremStat18 = new FixedLengthStringData(2).isAPartOf(filler2, 34);
  	public FixedLengthStringData covPremStat19 = new FixedLengthStringData(2).isAPartOf(filler2, 36);
  	public FixedLengthStringData covPremStat20 = new FixedLengthStringData(2).isAPartOf(filler2, 38);
  	public FixedLengthStringData covRiskStats = new FixedLengthStringData(40).isAPartOf(tr597Rec, 123);
  	public FixedLengthStringData[] covRiskStat = FLSArrayPartOfStructure(20, 2, covRiskStats, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(covRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData covRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData covRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData covRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData covRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData covRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData covRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData covRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData covRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData covRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData covRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	public FixedLengthStringData covRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler3, 20);
  	public FixedLengthStringData covRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler3, 22);
  	public FixedLengthStringData covRiskStat13 = new FixedLengthStringData(2).isAPartOf(filler3, 24);
  	public FixedLengthStringData covRiskStat14 = new FixedLengthStringData(2).isAPartOf(filler3, 26);
  	public FixedLengthStringData covRiskStat15 = new FixedLengthStringData(2).isAPartOf(filler3, 28);
  	public FixedLengthStringData covRiskStat16 = new FixedLengthStringData(2).isAPartOf(filler3, 30);
  	public FixedLengthStringData covRiskStat17 = new FixedLengthStringData(2).isAPartOf(filler3, 32);
  	public FixedLengthStringData covRiskStat18 = new FixedLengthStringData(2).isAPartOf(filler3, 34);
  	public FixedLengthStringData covRiskStat19 = new FixedLengthStringData(2).isAPartOf(filler3, 36);
  	public FixedLengthStringData covRiskStat20 = new FixedLengthStringData(2).isAPartOf(filler3, 38);
  	public FixedLengthStringData setCnPremStats = new FixedLengthStringData(40).isAPartOf(tr597Rec, 163);
  	public FixedLengthStringData[] setCnPremStat = FLSArrayPartOfStructure(20, 2, setCnPremStats, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(setCnPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData setCnPremStat01 = new FixedLengthStringData(2).isAPartOf(filler4, 0);
  	public FixedLengthStringData setCnPremStat02 = new FixedLengthStringData(2).isAPartOf(filler4, 2);
  	public FixedLengthStringData setCnPremStat03 = new FixedLengthStringData(2).isAPartOf(filler4, 4);
  	public FixedLengthStringData setCnPremStat04 = new FixedLengthStringData(2).isAPartOf(filler4, 6);
  	public FixedLengthStringData setCnPremStat05 = new FixedLengthStringData(2).isAPartOf(filler4, 8);
  	public FixedLengthStringData setCnPremStat06 = new FixedLengthStringData(2).isAPartOf(filler4, 10);
  	public FixedLengthStringData setCnPremStat07 = new FixedLengthStringData(2).isAPartOf(filler4, 12);
  	public FixedLengthStringData setCnPremStat08 = new FixedLengthStringData(2).isAPartOf(filler4, 14);
  	public FixedLengthStringData setCnPremStat09 = new FixedLengthStringData(2).isAPartOf(filler4, 16);
  	public FixedLengthStringData setCnPremStat10 = new FixedLengthStringData(2).isAPartOf(filler4, 18);
  	public FixedLengthStringData setCnPremStat11 = new FixedLengthStringData(2).isAPartOf(filler4, 20);
  	public FixedLengthStringData setCnPremStat12 = new FixedLengthStringData(2).isAPartOf(filler4, 22);
  	public FixedLengthStringData setCnPremStat13 = new FixedLengthStringData(2).isAPartOf(filler4, 24);
  	public FixedLengthStringData setCnPremStat14 = new FixedLengthStringData(2).isAPartOf(filler4, 26);
  	public FixedLengthStringData setCnPremStat15 = new FixedLengthStringData(2).isAPartOf(filler4, 28);
  	public FixedLengthStringData setCnPremStat16 = new FixedLengthStringData(2).isAPartOf(filler4, 30);
  	public FixedLengthStringData setCnPremStat17 = new FixedLengthStringData(2).isAPartOf(filler4, 32);
  	public FixedLengthStringData setCnPremStat18 = new FixedLengthStringData(2).isAPartOf(filler4, 34);
  	public FixedLengthStringData setCnPremStat19 = new FixedLengthStringData(2).isAPartOf(filler4, 36);
  	public FixedLengthStringData setCnPremStat20 = new FixedLengthStringData(2).isAPartOf(filler4, 38);
  	public FixedLengthStringData setCnRiskStats = new FixedLengthStringData(40).isAPartOf(tr597Rec, 203);
  	public FixedLengthStringData[] setCnRiskStat = FLSArrayPartOfStructure(20, 2, setCnRiskStats, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(setCnRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData setCnRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler5, 0);
  	public FixedLengthStringData setCnRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler5, 2);
  	public FixedLengthStringData setCnRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler5, 4);
  	public FixedLengthStringData setCnRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler5, 6);
  	public FixedLengthStringData setCnRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler5, 8);
  	public FixedLengthStringData setCnRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler5, 10);
  	public FixedLengthStringData setCnRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler5, 12);
  	public FixedLengthStringData setCnRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler5, 14);
  	public FixedLengthStringData setCnRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler5, 16);
  	public FixedLengthStringData setCnRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler5, 18);
  	public FixedLengthStringData setCnRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler5, 20);
  	public FixedLengthStringData setCnRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler5, 22);
  	public FixedLengthStringData setCnRiskStat13 = new FixedLengthStringData(2).isAPartOf(filler5, 24);
  	public FixedLengthStringData setCnRiskStat14 = new FixedLengthStringData(2).isAPartOf(filler5, 26);
  	public FixedLengthStringData setCnRiskStat15 = new FixedLengthStringData(2).isAPartOf(filler5, 28);
  	public FixedLengthStringData setCnRiskStat16 = new FixedLengthStringData(2).isAPartOf(filler5, 30);
  	public FixedLengthStringData setCnRiskStat17 = new FixedLengthStringData(2).isAPartOf(filler5, 32);
  	public FixedLengthStringData setCnRiskStat18 = new FixedLengthStringData(2).isAPartOf(filler5, 34);
  	public FixedLengthStringData setCnRiskStat19 = new FixedLengthStringData(2).isAPartOf(filler5, 36);
  	public FixedLengthStringData setCnRiskStat20 = new FixedLengthStringData(2).isAPartOf(filler5, 38);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(257).isAPartOf(tr597Rec, 243, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr597Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr597Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}