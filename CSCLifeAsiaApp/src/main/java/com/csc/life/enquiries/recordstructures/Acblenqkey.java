package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:33
 * Description:
 * Copybook name: ACBLENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acblenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acblenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acblenqKey = new FixedLengthStringData(64).isAPartOf(acblenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData acblenqRldgcoy = new FixedLengthStringData(1).isAPartOf(acblenqKey, 0);
  	public FixedLengthStringData acblenqSacscode = new FixedLengthStringData(2).isAPartOf(acblenqKey, 1);
  	public FixedLengthStringData acblenqSacstyp = new FixedLengthStringData(2).isAPartOf(acblenqKey, 3);
  	public FixedLengthStringData acblenqRldgacct = new FixedLengthStringData(16).isAPartOf(acblenqKey, 5);
  	public FixedLengthStringData acblenqOrigcurr = new FixedLengthStringData(3).isAPartOf(acblenqKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(acblenqKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acblenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acblenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}