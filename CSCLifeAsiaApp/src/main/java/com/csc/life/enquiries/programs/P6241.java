/*
 * File: P6241.java
 * Date: 30 August 2009 0:38:37
 * Author: Quipoz Limited
 * 
 * Class transformed from P6241.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.CovrincTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S6241ScreenVars;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* UNIT LINKED COMPONENT ENQUIRY
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selectio 
*     (current stack position action flag = '*').
*
*     The details of  the  contract  being  enquired  upon  will b 
*     stored in the  CHDRENQ  I/O  module. Retrieve the details an 
*     set up the header portion of the screen as follows:
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description fro 
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description fro 
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist 
*
*
*     Perform a RETRV  on  COVRENQ.  This  will  provide  the  COV 
*     record that is being enquired upon. At this point the progra 
*     must work out whether it is to provide a Plan Level or Polic 
*     Level  enquiry.  This  can  be  done  by  checking  the fiel 
*     WSSP-PLAN-POLICY-FLAG.  If it is 'L', then Plan Level Enquir 
*     is  required,  if  it  is  'O',  then Policy Level Enquiry i 
*     required.  For Plan Level Enquiry all the COVRENQ records fo 
*     the  given  Life, Coverage and Rider must be read and the Su 
*     Insured  and Premium values added together so a total for th 
*     whole  Plan  may  be  displayed.  For  Policy  Level  Enquir 
*     processing  will  be different depending on whether a summar 
*     record or non-summary record is being displayed. Normally th 
*     program  could  determine whether or not a summary record wa 
*     being  displayed  by  checking  the  Plan Suffix. If this wa 
*     '0000',  then  it  was  a summary record. However, for Polic 
*     Level Enquiry when a summary record is held in the I/O modul 
*     for  processing  the previous function will replace the valu 
*     of  '0000'  in  the  Plan  Suffix  with the calculated suffi 
*     number of the actual policy that the user selected. Therefor 
*     it  will  be  necessary  to check the Plan Suffix against th 
*     number  of policies summarised, (CHDRENQ-POLSUM). If the Pla 
*     Suffix is not greater the CHDRENQ-POLSUM then it is a summar 
*     record.
*
*     For  a  non-summary  record,  (Plan Suffix > CHDRENQ-POLSUM) 
*     then display the Sum Insured and Premium as found.
*
*     For  a  summary  record,  (Plan Suffix not > CHDRENQ-POLSUM) 
*     then  the  values  held on the COVRENQ record will be for al 
*     the  policies summarised, and therefore a calculation must b 
*     performed  to  arrive  at the value for one of the summarise 
*     policies.  For  all  summarised policies execpt the first on 
*     simply divide the total by the number of policies summarised 
*     (CHDRENQ-POLSUM)  and  display the result. This may give ris 
*     to  a  rounding  discrepancy. This discrepancy is absorbed b 
*     the  first policy summarised, the notional policy number one 
*     If  this  policy  has  been  selected for display perform th 
*     following calculation:
*
*     Value To Display = TOTAL - (TOTAL * (POLSUM - 1))
*                                         ~~~~~~~~~~~~                   
*                                            POLSUM
*
*     Where  TOTAL  is  either  the Sum Insured or the Premium, an 
*     POLSUM is the number of policies summarised, CHDRENQ-POLSUM.
*
*     For example if the premium is $100 and the number of policie 
*     summarised  is 3, the Premium for notional policies #2 and # 
*     will  be  calculated  and  displayed as $33. For policy #1 i 
*     will be:
*
*                        100 - (100 * (3 - 1)
*                                     ~~~~~~~                            
*                                        3
*
*                     =  100 - (100 * 2/3)
*
*                     =  $34
*
*
*     The screen title  is  the  component  description from T5687 
*     (See p6241 for how this is obtained and centred).
*
*     Read table T5671,  key  is Transaction Code concatenated wit 
*     Coverage/Rider Code,  (CRTABLE).  Take  the  validation  ite 
*     that matches the current program and use it concatenated wit 
*     the Coverage/Rider Currency to read T5551.
*
*     Obtain  the  Unit  Linked Edit rules from T5551 using ITEMIO 
*     This table will be accessed in the following way:
*
*          Use  a  key  of SMTP-ITEM, Company, 'T5671', Transactio 
*          Code  and  Coverage/Rider  Code  to read T5671 - Generi 
*          Component Control Table.
*
*          This  will  give  a  list  of  up  to  4  programs  wit 
*          associated  AT  module  names and Validation Item Codes 
*          Match  the  current  program  name against this list an 
*          select the associated Validation Item Code.
*
*          Use  this  to  construct  a  key  of SMTP-ITEM, Company 
*          'T5551',  Validation  Item Code and the Currency Code t 
*          read  T5551  directly  using  ITEMIO  and  a function o 
*          READS.
*
*     If Special Options are not allowed, as defined on T5551, the 
*     protect  and  non-display  the  prompt  and  Special  Option 
*     Indicator, (OPTEXTIND). Otherwise read the options and extra 
*     data-set,  (LEXT),  for  the  current  Coverage/Rider  with  
*     READS.  If  any  matching record is found then place a '+' i 
*     the field to indicate that these details exist.
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selectio 
*     (current stack position action flag = '*').
*
*     The only validation required  is  of  the  indicator  field  
*     OPTEXTIND.  This may be space, '+' or 'X'.
*
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  requested  move  spaces to the current progra 
*     field in the program stack field and exit.
*
*     If returning from  processing  the selection further down th 
*     program stack, (current  stack  action  field  will  be '*') 
*     restore  the next  8  programs  from  the  WSSP,  remove  th 
*     asterisk and if the select indicator is '?' then re-set it t 
*     '+'.
*
*     If nothing was selected,  continue  by  just  adding 1 to th 
*     program pointer and exit.
*
*     If a selection has been  made move '?' to the select field o 
*     the screen, use  GENSWCH  to  locate  the  next program(s) t 
*     process the selection, (up  to 8).  Use function of 'U'. Sav 
*     the next 8 programs  in  the  stack and replace them with th 
*     ones returned from GENSWCH.  Place an asterisk in the curren 
*     stack action field, add 1 to the program pointer and exit.
*
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5551 - Unit Linked Edit Rules            Key: Val. Item (T5671)
* T5671 - Generic Component Control         Key: TRCODE||CRTABLE
* T5687 - General Coverage/Rider Details    Key: CRTABLE
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*
*****************************************************************
* </pre>
*/
public class P6241 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6241");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaMessages = new FixedLengthStringData(100);
	private FixedLengthStringData[] wsaaMessage = FLSArrayPartOfStructure(2, 50, wsaaMessages, 0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

		/*    VALUE 'Loaded Single Prem :'.                        <SPLPRM>
		    VALUE 'Loaded Inst. Prem  :'.                        <SPLPRM>*/
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	
	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData[] wsaaSurrTable = PDInittedArray(1000, 4, 0);
	private PackedDecimalData wsaaSurrCnt = new PackedDecimalData(4, 0).init(0);
	private PackedDecimalData wsaaSurrSub = new PackedDecimalData(4, 0);
		/* 88 PAYING-STATUS            VALUES 'PP', 'SP'.               */
	private FixedLengthStringData wsaaBypassAccumulation = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaPremStatus, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private FixedLengthStringData wsaaTr52dItemtype = new FixedLengthStringData(8);   //ILIFE-6635
	private static final String g620 = "G620";
	private static final String h093 = "H093";
		/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String itemrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrincTableDAM covrincIO = new CovrincTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private T2240rec t2240rec = new T2240rec();
	private T5671rec t5671rec = new T5671rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private T5551rec t5551rec = new T5551rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S6241ScreenVars sv = ScreenProgram.getScreenVars( S6241ScreenVars.class);
	private TablesInner tablesInner = new TablesInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
	private static final String mbnsrec = "MBNSREC"; 
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	boolean ispermission=true;
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends */
	private boolean loadingFlag = false;//ILIFE-3399	
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private Itempf itempf = new Itempf();
	private List<Itempf> itempfList;
	
	private Covrpf covrenqpf = new Covrpf();
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrsurpf = new Covrpf();
	private List<Covrpf> covrpfList;
	//ILIFE-7805 - Starts
	private boolean singPremTypeFlag = false; 	
	private T5687rec t5687rec = new T5687rec();	
	//ILIFE-7805 - Ends
	private boolean isBenefitSchedule = false;//ILIFE-7994
	//ILJ-45 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-45 End

	// ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	// ILJ-387 end
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endPolicy1020, 
		exit1090, 
		incExcPremium1100, 
		preExit1190, 
		exit1190, 
		exit1790, 
		cont4000, 
		exit4090, 
		setNext8888, 
		exit8888
	}

	public P6241() {
		super();
		screenVars = sv;
		new ScreenModel("S6241", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case endPolicy1020: 
					endPolicy1020();
					gotRecord10620();
					nextProg10640();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
		//ispermission=FeaConfg.isFeatureExist(wsspcomn.company.toString(),"BCLM01",appVars, "IT");
		if(!ispermission)
			sv.optsmodeOut[varcom.nd.toInt()].set("Y");
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/
		//ILJ-45 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{			
			sv.crrcdOut[varcom.nd.toInt()].set("Y");
			//sv.rcessageOut[varcom.nd.toInt()].set("Y");
			sv.contDtCalcScreenflag.set("N");
			sv.rcesdteOut[varcom.nd.toInt()].set("Y");
		}
		//ILJ-45 End
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}
		// ILJ-387 Start
		boolean cntEnqFlag = false;
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if (cntEnqFlag) {
			sv.cntEnqScreenflag.set("Y");
		}else {
			sv.cntEnqScreenflag.set("N");
		}
		// ILJ-387 End
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		wsaaMiscellaneousInner.wsaaCoverageDebit.set(ZERO);
		sv.taxamt.set(ZERO);
		/*    Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
  		
		//ILIFE-7994 starts
        isBenefitSchedule=FeaConfg.isFeatureExist(wsspcomn.company.toString(),"CTENQ006",appVars, "IT");
		
		if (isBenefitSchedule) {
			t5687Load7010();
	         if (isEQ(t5687rec.zsbsmeth , SPACES)){
	        	 sv.optsmodeOut[varcom.nd.toInt()].set("Y");	
	         }
		}
		//ILIFE-7994 ends
		covrenqpf = covrpfDAO.getCacheObject(covrenqpf);//ILIFE-6263
		/*  Read T2240 for age definition                                  */
		itempf.setItempfx("IT");
		itempf.setItemcoy((wsspcomn.fsuco).toString());
		itempf.setItemtabl((tablesInner.t2240).toString());
		/* MOVE CHDRENQ-CNTTYPE        TO ITEM-ITEMITEM.           <C02>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.fsuco).toString() , (tablesInner.t2240).toString() , (wsaaT2240Key).toString() );
		if(itempfList == null || (itempfList!=null && itempfList.size()==0))
				{
					
					itempf.setItempfx("IT");
					itempf.setItemcoy((wsspcomn.fsuco).toString());
					itempf.setItemtabl((tablesInner.t2240).toString());
					wsaaT2240Key.set(SPACES);
					wsaaT2240Lang.set(wsspcomn.language);
					wsaaT2240Cnttype.set("***");
					itempf.setItemitem((wsaaT2240Key).toString());
					
					itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.fsuco).toString() , (tablesInner.t2240).toString() , (wsaaT2240Key).toString() );
					if(itempfList == null || (itempfList!=null && itempfList.size()==0))
						{
							return;
						}
				}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else {
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				} //MTL002
			}
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
//		itemIO.setDataKey(SPACES);
//		itemIO.setItempfx("IT");
//		itemIO.setItemcoy(wsspcomn.company);
//		itemIO.setItemtabl(tablesInner.tr52d);
//		itempf.setItemitem(chdrpf.getReg());  //ILIFE-6004
		itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.company).toString() , (tablesInner.tr52d).toString() , chdrpf.getReg() ); //ILIFE-6004
		if(itempfList == null || (itempfList!=null && itempfList.size()==0)){
						itempf.setItempfx("IT");
						itempf.setItemcoy((wsspcomn.company).toString());
						itempf.setItemtabl((tablesInner.tr52d).toString());
						wsaaTr52dItemtype.set("***");   //ILIFE-6635
						itempf.setItemitem((wsaaTr52dItemtype).toString());   //ILIFE-6635
						//itempf.setItemitem("***");
						itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.company).toString() , (tablesInner.tr52d).toString() , (wsaaTr52dItemtype).toString() );
						if(itempfList == null || (itempfList!=null && itempfList.size()==0)){
										return;
							}
				}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));//ILIFE-8131
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		//ILIFE-3399-STARTS
		loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
			}
		//ILIFE-3399-ENDS
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Retrieve valid status'.                                         */
		readT5679Table1200();
		/*    Read the first LIFE details on the contract.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		//covrenqpf = covrpfDAO.getCacheObject(covrenqpf);ILIFE-6263
		/*  Store Life, Coverage & Rider for use later on*/
		wsaaCovrLife.set(covrenqpf.getLife());
		if (isNE(covrenqpf.getLife(), lifeenqIO.getLife())) {
			getLifeName9999();
		}
		wsaaCovrCoverage.set(covrenqpf.getCoverage());
		wsaaCovrRider.set(covrenqpf.getRider());
		sv.premCessTerm.set(covrenqpf.getPremCessTerm());
		sv.riskCessTerm.set(covrenqpf.getRiskCessTerm());
		//ILIFE-7805
		singPremTypeFlag = isSinglePremTypeComp();		
		if (!singPremTypeFlag) {			
			sv.singpremtypeOut[varcom.nd.toInt()].set("Y");			
		}
		sv.singpremtypeOut[varcom.pr.toInt()].set("Y");
		sv.singpremtype.set(covrenqpf.getSingpremtype());//ILIFE-7805
		/*    Need to use either INSTPREM or SINGP on display so use a*/
		/*    different WSAA- field and slightly re-arrange the IF*/
		/*    Statements below.*/
		wsaaMiscellaneousInner.wsaaPremium.set(covrenqpf.getInstprem());
		wsaaMiscellaneousInner.wsaaZlinstprem.set(covrenqpf.getZlinstprem());
		wsaaMiscellaneousInner.wsaaSingp.set(covrenqpf.getSingp());
		wsaaMiscellaneousInner.wsaaSumins.set(covrenqpf.getSumins());
		wsaaMiscellaneousInner.wsaaCoverageDebit.set(covrenqpf.getCoverageDebt());
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			covrsurpf.setChdrcoy(chdrpf.getChdrcoy().toString());
			covrsurpf.setChdrnum(chdrpf.getChdrnum());
			covrsurpf.setPlanSuffix(0);
			covrpfList= covrpfDAO.readCovrsurData(covrsurpf);
			for(Covrpf covrpf : covrpfList )
			{
				determineSurrCovr8888(covrpf);
			}
			/*    Just checking here to see if any policies within plan
		     have already been Surrendered
			 */
			/* commented as part of ILIFE-4036 starts*/
			/*wsaaMiscellaneousInner.wsaaPremium.set(ZERO);
			wsaaMiscellaneousInner.wsaaSingp.set(ZERO);
			wsaaMiscellaneousInner.wsaaZlinstprem.set(ZERO);
			wsaaMiscellaneousInner.wsaaSumins.set(ZERO);
			wsaaMiscellaneousInner.wsaaCoverageDebit.set(ZERO);*/
			/* commented as part of ILIFE-4036 ends*/
			/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
			covrincIO.setDataKey(SPACES);
			covrenqpf.setPlanSuffix(0);
			covrincIO.setPlanSuffix(0);
			covrenqpf.setChdrcoy(chdrpf.getChdrcoy().toString());
			covrincIO.setChdrcoy(chdrpf.getChdrcoy());
			covrenqpf.setChdrnum(chdrpf.getChdrnum());
			covrincIO.setChdrnum(chdrpf.getChdrnum());
			covrenqpf.setLife((wsaaCovrLife).toString());
			covrincIO.setLife(wsaaCovrLife);
			covrenqpf.setCoverage((wsaaCovrCoverage).toString());
			covrincIO.setCoverage(wsaaCovrCoverage);
			covrenqpf.setRider((wsaaCovrRider).toString());
			covrincIO.setRider(wsaaCovrRider);
			covrincIO.setFunction(varcom.begn);
			covrenqpf = covrpfDAO.readCovrenqData(covrenqpf);
			/*     PERFORM 1100-PLAN-COMPONENT                              */
			/*                  UNTIL COVRENQ-PLAN-SUFFIX > CHDRENQ-POLINC  */
			/*                  OR                                          */
			/*                  COVRENQ-STATUZ  =  ENDP                     */
			while ( !(isGT(covrincIO.getPlanSuffix(), chdrpf.getPolinc())
					|| isEQ(covrincIO.getStatuz(), varcom.endp))) {
						planComponent1100();
					}
			
			goTo(GotoLabel.endPolicy1020);
		}
		
		if (wsaaMiscellaneousInner.policyLevelEnquiry.isTrue()) {
			/*     MOVE COVRENQ-PSTATCODE      TO WSAA-PREMIUM-STATII.      */
			wsaaCovrPstatcode.set(covrenqpf.getPstatcode());
			checkPremStatus1300();
		}
		/* IF NOT PAYING-STATUS                                         */
		/*    MOVE 0                       TO WSAA-SINGP                */
		/*                                    WSAA-PREMIUM              */
		/*                                    WSAA-COVERAGE-DEBIT  <013>*/
		/*                                    WSAA-SUMINS.              */
		if (isGT(covrenqpf.getPlanSuffix(), chdrpf.getPolsum())) {
			goTo(GotoLabel.endPolicy1020);
		}
		if (isEQ(covrenqpf.getPlanSuffix(), 1)) {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(sub(covrenqpf.getSingp(), (div(mult(covrenqpf.getSingp(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(sub(wsaaMiscellaneousInner.wsaaPremium, (div(mult(wsaaMiscellaneousInner.wsaaPremium, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(sub(covrenqpf.getSumins(), (div(mult(covrenqpf.getSumins(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaCoverageDebit, 2).set(sub(covrenqpf.getCoverageDebt(), (div(mult(covrenqpf.getCoverageDebt(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 2).set(sub(covrenqpf.getZlinstprem(), (div(mult(covrenqpf.getZlinstprem(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			goTo(GotoLabel.endPolicy1020);
		}
		if (isNE(wsaaMiscellaneousInner.wsaaSingp, 0)) {
			compute(wsaaMiscellaneousInner.wsaaSingp, 0).set(div(wsaaMiscellaneousInner.wsaaSingp, chdrpf.getPolsum()));
		}
		if (isNE(wsaaMiscellaneousInner.wsaaPremium, 0)) {
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(div(wsaaMiscellaneousInner.wsaaPremium, chdrpf.getPolsum()));
		}
		if (isNE(wsaaMiscellaneousInner.wsaaSumins, 0)) {
			compute(wsaaMiscellaneousInner.wsaaSumins, 0).set(div(wsaaMiscellaneousInner.wsaaSumins, chdrpf.getPolsum()));
		}
		if (isNE(wsaaMiscellaneousInner.wsaaCoverageDebit, 0)) {
			compute(wsaaMiscellaneousInner.wsaaCoverageDebit, 0).set(div(wsaaMiscellaneousInner.wsaaCoverageDebit, chdrpf.getPolsum()));
		}
		if (isNE(wsaaMiscellaneousInner.wsaaZlinstprem, 0)) {
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(div(wsaaMiscellaneousInner.wsaaZlinstprem, chdrpf.getPolsum()));
		}

	}

protected void endPolicy1020()
	{
		b000Rounding();
		/*sv.premCessTerm.set(covrenqpf.getPremCessTerm());*/
		sv.numpols.set(chdrpf.getPolinc());
		sv.anbAtCcd.set(covrenqpf.getAnbAtCcd());
		sv.annivProcDate.set(covrenqpf.getAnnivProcDate());
		if (isEQ(sv.annivProcDate, ZERO)) {
			sv.annivProcDate.set(varcom.vrcmMaxDate);
		}
		sv.crrcd.set(covrenqpf.getCrrcd());
		if (isEQ(sv.crrcd, ZERO)) {
			sv.crrcd.set(varcom.vrcmMaxDate);
		}
		sv.unitStatementDate.set(covrenqpf.getUnitStatementDate());
		if (isEQ(sv.unitStatementDate, ZERO)) {
			sv.unitStatementDate.set(varcom.vrcmMaxDate);
		}
		sv.optextind.set(SPACES);
		sv.benBillDate.set(covrenqpf.getBenBillDate());
		if (isEQ(sv.benBillDate, ZERO)) {
			sv.benBillDate.set(varcom.vrcmMaxDate);
		}
		/* MOVE COVRENQ-COVERAGE-DEBT  TO S6241-COVERAGE-DEBT.          */
		sv.coverageDebt.set(wsaaMiscellaneousInner.wsaaCoverageDebit);
		sv.premcess.set(covrenqpf.getPremCessDate());
		if (isEQ(sv.premcess, ZERO)) {
			sv.premcess.set(varcom.vrcmMaxDate);
		}
		sv.liencd.set(covrenqpf.getLiencd());
		sv.riskCessDate.set(covrenqpf.getRiskCessDate());
		if (isEQ(sv.riskCessDate, ZERO)) {
			sv.riskCessDate.set(varcom.vrcmMaxDate);
		}
		sv.mortcls.set(covrenqpf.getMortcls());/*ILIFE-4036*/
		sv.riskCessAge.set(covrenqpf.getRiskCessAge());
		sv.premCessAge.set(covrenqpf.getPremCessAge());
	/*	sv.riskCessTerm.set(covrenqpf.getRiskCessTerm());
		sv.premCessTerm.set(covrenqpf.getPremCessTerm());*/
		sv.register.set(chdrpf.getReg());
		sv.life.set(covrenqpf.getLife());
		sv.rider.set(covrenqpf.getRider());
		sv.coverage.set(covrenqpf.getCoverage());
		sv.rerateDate.set(covrenqpf.getRerateDate());
		if (isEQ(sv.rerateDate, ZERO)) {
			sv.rerateDate.set(varcom.vrcmMaxDate);
		}
		sv.rerateFromDate.set(covrenqpf.getRerateFromDate());
		if (isEQ(sv.rerateFromDate, ZERO)) {
			sv.rerateFromDate.set(varcom.vrcmMaxDate);
		}
		sv.singlePremium.set(wsaaMiscellaneousInner.wsaaSingp);
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.statFund.set(covrenqpf.getStatFund());
		sv.statSect.set(covrenqpf.getStatSect());
		sv.statSubsect.set(covrenqpf.getStatSubsect());
		/*BRD-306 START */
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		if(covrenqpf.getLoadper()==null){
			sv.loadper.set(SPACE);
		}
		else{
			sv.loadper.set(covrenqpf.getLoadper());
		}
		if(covrenqpf.getRateadj()==null)
		{
			sv.rateadj.set(SPACE);
		}
		else{
			sv.rateadj.set(covrenqpf.getRateadj());
		}
		if(covrenqpf.getFltmort()==null)
		{
			sv.fltmort.set(SPACE);
		}
		else{
			sv.fltmort.set(covrenqpf.getFltmort());
		}
		if(covrenqpf.getPremadj()==null){
			sv.premadj.set(SPACE);
			sv.adjustageamt.set(SPACE);
		}
		else{
			sv.premadj.set(covrenqpf.getPremadj());
			sv.adjustageamt.set(covrenqpf.getPremadj());
		}
		
		sv.zbinstprem.set(covrenqpf.getZbinstprem());
		/*BRD-306 END */
		sv.sumin.set(wsaaMiscellaneousInner.wsaaSumins);
		/*--- Read TR386 table to get screen literals                      */
		
		iy.set(1);
		initialize(wsaaMessages);
		itempf.setItempfx("IT");
		itempf.setItemcoy((wsspcomn.company).toString());
		itempf.setItemtabl((tablesInner.tr386).toString());
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		itempf.setItemitem((wsaaTr386Key).toString());
		itempf.setItemseq("");
		itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.company).toString() , (tablesInner.tr386).toString() , (wsaaTr386Key).toString());
		if(itempfList!= null ){
		for(Itempf item : itempfList)
			{
				if(itempf.getItemseq().equals(""))
					{
						loadTr3861600(item);
					}
			}
		}
		
		if (isEQ(wsaaMiscellaneousInner.wsaaPremium, 0)) {
			sv.zdesc.set(wsaaMessage[2]);
		}
		else {
			sv.zdesc.set(wsaaMessage[1]);
		}
		/* CALL  'ITEMIO'           USING ITEM-PARAMS.          <FA1226>*/
		/* IF  ITEM-STATUZ          NOT = O-K                   <FA1226>*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <FA1226>*/
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <FA1226>*/
		/*     PERFORM 600-FATAL-ERROR.                         <FA1226>*/
		/* MOVE ITEM-GENAREA           TO TR386-TR386-REC.      <FA1226>*/
		/* MOVE TR386-PROGDESC-1       TO S6241-ZDESC.          <FA1226>*/
		/* IF  WSAA-PREMIUM            = 0                      <SPLPRM>*/
		/*     MOVE WSAA-SINGLE-DESC   TO S6241-ZDESC           <SPLPRM>*/
		/* ELSE                                                 <SPLPRM>*/
		/*     MOVE WSAA-REG-DESC      TO S6241-ZDESC           <SPLPRM>*/
		/* END-IF.                                              <SPLPRM>*/
		sv.zlinstprem.set(wsaaMiscellaneousInner.wsaaZlinstprem);
		sv.zdescOut[varcom.hi.toInt()].set("N");
		if (isNE(covrenqpf.getPlanSuffix(), ZERO)) {
			sv.planSuffix.set(covrenqpf.getPlanSuffix());
		}
		else {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			sv.planSuffix.set(ZERO);
		}
		/* GET THE COMPONENT DESCRIPTION*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrenqpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		/* READ T5671*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaMiscellaneousInner.wsaaTrCode.set(wsaaBatckey.batcBatctrcde);
		wsaaMiscellaneousInner.wsaaCrtable.set(covrenqpf.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5671);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5671Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5671)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5671Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itdmIO.getGenarea());
		}
	}

protected void gotRecord10620()
	{
		sub1.set(0);
	}

protected void nextProg10640()
	{
		sub1.add(1);
		if (isNE(t5671rec.pgm[sub1.toInt()], wsaaProg)) {
			nextProg10640();
			return ;
		}
		else {
			wsaaMiscellaneousInner.wsaaValidItem.set(t5671rec.edtitm[sub1.toInt()]);
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts */
		if (isEQ(t5551rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext1900();
		}
		m100CheckMbns();
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/

		/* READ T5551*/
		wsaaMiscellaneousInner.wsaaCrcyCode.set(chdrpf.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5551);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5551Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5551)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5551Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5551rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext1900();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void planComponent1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1100();
				case incExcPremium1100: 
					incExcPremium1100();
				case preExit1190: 
					preExit1190();
				case exit1190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
protected void m100CheckMbns()
{
	m100Start();
}

protected void m100Start()
{
	mbnsIO.setChdrcoy(chdrpf.getChdrcoy());
	mbnsIO.setChdrnum(chdrpf.getChdrnum());
	mbnsIO.setLife(covrenqpf.getLife());
	mbnsIO.setCoverage(covrenqpf.getCoverage());
	mbnsIO.setRider(covrenqpf.getRider());
	mbnsIO.setYrsinf(ZERO);
	mbnsIO.setFormat(mbnsrec);
	mbnsIO.setFunction(varcom.begn);
	mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
	SmartFileCode.execute(appVars, mbnsIO);
	if (isNE(mbnsIO.getStatuz(), varcom.oK)
	&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
		syserrrec.statuz.set(mbnsIO.getStatuz());
		syserrrec.params.set(mbnsIO.getParams());
		fatalError600();
	}
	if (isEQ(mbnsIO.getStatuz(), varcom.endp)) {		
		sv.optsmode.set(" ");
		return ;
	}
	if (isNE(mbnsIO.getRider(), covrenqpf.getRider())
			|| isNE(mbnsIO.getChdrcoy(), chdrpf.getChdrcoy())
			|| isNE(mbnsIO.getChdrnum(), chdrpf.getChdrnum())
			|| isNE(mbnsIO.getLife(), covrenqpf.getLife())
			|| isNE(mbnsIO.getCoverage(), covrenqpf.getCoverage())) {
				mbnsIO.setStatuz(varcom.endp);
			}
	else {
		sv.optsmode.set("+");
	}
}
/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/
protected void para1100()
	{
		/*    For Plan Level Enquiry the first coverage record read will*/
		/*    have a Plan Suffix of 1. All the components for the given*/
		/*    Life, Coverage and Rider must be read and the amounts added*/
		/*    up to give the total for the component being enquired upon.*/
		/* CALL 'COVRENQIO'         USING COVRENQ-PARAMS.               */
		/* IF   COVRENQ-STATUZ      NOT = O-K AND ENDP                  */
		/*      MOVE COVRENQ-PARAMS TO SYSR-PARAMS                      */
		/*      PERFORM 600-FATAL-ERROR.                                */
		/* IF   COVRENQ-STATUZ      = ENDP                              */
		/*      GO TO 1190-EXIT.                                        */
		/* IF   COVRENQ-CHDRNUM     NOT = CHDRENQ-CHDRNUM               */
		/*      OR COVRENQ-CHDRCOY  NOT = CHDRENQ-CHDRCOY               */
		/*      OR COVRENQ-LIFE     NOT = WSAA-COVR-LIFE                */
		/*      OR COVRENQ-COVERAGE NOT = WSAA-COVR-COVERAGE            */
		/*      OR COVRENQ-RIDER    NOT = WSAA-COVR-RIDER               */
		/* if there are no more Coverage records for this contract,*/
		/* i.e. the Contract Number or Company has changed, then we must*/
		/* Read backwards one record to get the previous Coverage record*/
		/* which is associated with the Contract we are processing*/
		/*      MOVE NEXTP             TO COVRENQ-FUNCTION              */
		/*      CALL 'COVRENQIO'       USING COVRENQ-PARAMS             */
		/*      IF COVRENQ-STATUZ      NOT = O-K                        */
		/*          MOVE COVRENQ-PARAMS TO SYSR-PARAMS                  */
		/*          PERFORM 600-FATAL-ERROR                             */
		/*      END-IF                                                  */
		/*      MOVE ENDP              TO COVRENQ-STATUZ                */
		/*      GO TO 1190-EXIT.                                        */
		/* IF   COVRENQ-PLAN-SUFFIX > CHDRENQ-POLINC                    */
		/*      MOVE ENDP           TO COVRENQ-STATUZ                   */
		/*      GO TO 1190-EXIT.                                        */
		/* MOVE SPACES              TO WSAA-BYPASS-ACCUMULATION.        */
		/* extra check for summarised policy                               */
		/* IF COVRENQ-PLAN-SUFFIX   = ZEROS                             */
		/*     MOVE COVRENQ-PSTATCODE TO WSAA-PREMIUM-STATII            */
		/*     IF NOT PAYING-STATUS                                     */
		/*         MOVE 'Y'         TO WSAA-BYPASS-ACCUMULATION         */
		/*     END-IF                                                   */
		/*     GO TO 1100-INC-EXC-PREMIUM                               */
		/* END-IF.                                                      */
		/* PERFORM                  VARYING WSAA-SURR-SUB               */
		/*                                  FROM 1 BY 1                 */
		/*                                  UNTIL WSAA-SURR-SUB >       */
		/*                                        WSAA-SURR-CNT         */
		/*    IF COVRENQ-PLAN-SUFFIX    = WSAA-SURR-TABLE               */
		/*                                     (WSAA-SURR-SUB)          */
		/*       MOVE 'Y'               TO WSAA-BYPASS-ACCUMULATION     */
		/*       MOVE WSAA-SURR-CNT     TO WSAA-SURR-SUB                */
		/*    END-IF                                                    */
		/* END-PERFORM.                                                 */
		SmartFileCode.execute(appVars, covrincIO);
		/* IF   COVRENQ-STATUZ      NOT = O-K AND                       */
		/*      COVRENQ-STATUZ      NOT = MRNF                          */
		if (isNE(covrincIO.getStatuz(), varcom.oK)
		&& isNE(covrincIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrincIO.getParams());
			fatalError600();
		}
		if (isEQ(covrincIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit1190);
		}
		if (isNE(covrincIO.getChdrnum(), chdrpf.getChdrnum())
				|| isNE(covrincIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(covrincIO.getLife(), wsaaCovrLife)
		|| isNE(covrincIO.getCoverage(), wsaaCovrCoverage)
		|| isNE(covrincIO.getRider(), wsaaCovrRider)) {
			/* if there are no more Coverage records for this contract,        */
			/* i.e. the Contract Number or Company has changed, then we must   */
			/* Read backwards one record to get the previous Coverage record   */
			/* which is associated with the Contract we are processing         */
			covrincIO.setFunction(varcom.nextp);
			SmartFileCode.execute(appVars, covrincIO);
			if (isNE(covrincIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrincIO.getParams());
				fatalError600();
			}
			covrincIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		if (isGT(covrincIO.getPlanSuffix(), chdrpf.getPolinc())) {
			covrincIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		wsaaBypassAccumulation.set(SPACES);
		/* extra check for summarised policy                               */
		if (isEQ(covrincIO.getPlanSuffix(), ZERO)) {
			/*     MOVE COVRINC-PSTATCODE TO WSAA-PREMIUM-STATII            */
			wsaaCovrPstatcode.set(covrincIO.getPstatcode());
			checkPremStatus1300();
			/*     IF NOT PAYING-STATUS                                     */
			if (!validStatus.isTrue()) {
				wsaaBypassAccumulation.set("Y");
			}
			goTo(GotoLabel.incExcPremium1100);
		}
		for (wsaaSurrSub.set(1); !(isGT(wsaaSurrSub, wsaaSurrCnt)); wsaaSurrSub.add(1)){
			if (isEQ(covrincIO.getPlanSuffix(), wsaaSurrTable[wsaaSurrSub.toInt()])) {
				wsaaBypassAccumulation.set("Y");
				wsaaSurrSub.set(wsaaSurrCnt);
			}
		}
	}

protected void incExcPremium1100()
	{
		if (isEQ(wsaaBypassAccumulation, "Y")) {
			goTo(GotoLabel.preExit1190);
		}
		if(covrenqpf!= null){
			/*      COMPUTE WSAA-SINGP      = WSAA-SINGP                    */
			/*                              + COVRENQ-SINGP                 */
			/*      COMPUTE WSAA-SUMINS     = WSAA-SUMINS                   */
			/*                              + COVRENQ-SUMINS                */
			/*      COMPUTE WSAA-PREMIUM    = WSAA-PREMIUM                  */
			/*                              + COVRENQ-INSTPREM.             */
			compute(wsaaMiscellaneousInner.wsaaCoverageDebit, 2).set(add(wsaaMiscellaneousInner.wsaaCoverageDebit, covrincIO.getCoverageDebt()));
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(add(wsaaMiscellaneousInner.wsaaSingp, covrincIO.getSingp()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(add(wsaaMiscellaneousInner.wsaaSumins, covrincIO.getSumins()));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 2).set(add(wsaaMiscellaneousInner.wsaaZlinstprem, covrincIO.getZlinstprem()));
			compute(wsaaMiscellaneousInner.wsaaPremium, 2).set(add(wsaaMiscellaneousInner.wsaaPremium, covrincIO.getInstprem()));
		}
	}

	/**
	* <pre>
	****      IF COVRENQ-INSTPREM     NOT = ZERO                      
	****          COMPUTE WSAA-PREMIUM =                              
	****                              WSAA-PREMIUM + COVRENQ-INSTPREM 
	****      ELSE                                                    
	****          COMPUTE WSAA-PREMIUM =                              
	****                              WSAA-PREMIUM + COVRENQ-SINGP.   
	* </pre>
	*/
protected void preExit1190()
	{
		/* MOVE NEXTR                   TO COVRENQ-FUNCTION.            */
		covrincIO.setFunction(varcom.nextr);
	}

protected void readT5679Table1200()
	{
		read1210();
	}

protected void read1210()
	{	
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		itempf.setItempfx("IT");
		itempf.setItemcoy((wsspcomn.company).toString());
		itempf.setItemtabl((tablesInner.t5679).toString());
		itempf.setItemitem((wsaaBatckey.batcBatctrcde).toString());
		itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.company).toString() , (tablesInner.tr52d).toString() , chdrpf.getReg() ); //ILIFE-6004
		if(itempfList == null || (itempfList!=null && itempfList.size()==0))
				{
					return;
				}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}

protected void checkPremStatus1300()
	{
		start1310();
		search1320();
	}

	/**
	* <pre>
	* Check the twelve premium status' retrieved from table T5679     
	* to see if any match the coverage premium status. If so, set     
	* the VALID-STATUS flag to yes.                                   
	* </pre>
	*/
protected void start1310()
	{
		wsaaPremStatus.set(SPACES);
		wsaaSub.set(ZERO);
	}

protected void search1320()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			return ;
		}
		else {
			if (isNE(wsaaCovrPstatcode, t5679rec.covPremStat[wsaaSub.toInt()])) {
				search1320();
				return ;
			}
			else {
				wsaaPremStatus.set("Y");
				return ;
			}
		}
		/*EXIT*/
	}

protected void loadTr3861600(Itempf item)
{
		if(item ==null ) 
		{
			item = new Itempf();//IJTI-320
			item.setItemitem(wsaaTr386Key.toString());
			return;
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		ix.set(1);
		while ( !(isGT(ix, 10))) {
			if (isNE(tr386rec.progdesc[ix.toInt()], SPACES)
			&& isLT(iy, 3)) {
				wsaaMessage[iy.toInt()].set(tr386rec.progdesc[ix.toInt()]);
				iy.add(1);
			}
			ix.add(1);
		}
}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
		lextIO.setChdrcoy(covrenqpf.getChdrcoy());
		lextIO.setChdrnum(covrenqpf.getChdrnum());
		lextIO.setLife(covrenqpf.getLife());
		lextIO.setCoverage(covrenqpf.getCoverage());
		lextIO.setRider(covrenqpf.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqpf.getChdrcoy(), lextIO.getChdrcoy())
				|| isNE(covrenqpf.getChdrnum(), lextIO.getChdrnum())
				|| isNE(covrenqpf.getLife(), lextIO.getLife())
				|| isNE(covrenqpf.getCoverage(), lextIO.getCoverage())
				|| isNE(covrenqpf.getRider(), lextIO.getRider())) {
					lextIO.setStatuz(varcom.endp);
				}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5000();
		}
		return ;
	}

protected void screenEdit2000()
	{
		screenIo12010();
	}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo12010()
	{
		/*    CALL 'S6241IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   S6241-DATA-AREA .             */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*VALIDATE-SCREEN*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(g620);
		}
		/* Tax indicator must either be ' ', '+' or 'X'                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(g620);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
		if (ispermission  && isNE(sv.optsmode, " ") 
				&& isNE(sv.optsmode, "+")
				&& isNE(sv.optsmode, "X")) {
					sv.optsmodeErr.set(g620);
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe4400();
			sv.taxind.set("?");
			gensswrec.function.set("F");
			cont4000(); 
		}
		else { 
			if (isEQ(sv.optsmode, "X")) { 
				sv.optsmode.set("?");  
				gensswrec.function.set("R"); 
				cont4000(); 
        	     }
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/

	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
				case cont4000: 
					cont4000();
					nextProgram4080();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				restoreProgram4100();
			}
			if (isEQ(sv.taxind, "?")) {
				taxRet4500();
				goTo(GotoLabel.exit4090);
			}
			if (isEQ(sv.optextind, "?")) {
				sv.optextind.set("+");
			}
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe4400();
			sv.taxind.set("?");
			gensswrec.function.set("F");
			goTo(GotoLabel.cont4000);
		}
		else {
			if (isNE(sv.optextind, "X")) {
				wsspcomn.nextprog.set(wsaaProg);
				wsspcomn.programPtr.add(1);
				goTo(GotoLabel.exit4090);
			}
		}
		/*   SELECTION MADE*/
		/*   The generalised secondary switching module is called to get*/
		/*   the next 8 programs and load them into the program stack.*/
		sv.optextind.set("?");
		gensswrec.function.set("U");
	}

protected void cont4000()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			saveProgramStack4200();
		}
		/*    MOVE 'U'                    TO GENS-FUNCTION.                */
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				goTo(GotoLabel.exit4090);
			}
			else {
				sv.optextind.set("+");
				goTo(GotoLabel.exit4090);
			}
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4080()
	{
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void taxExe4400()
	{
		start4400();
	}

protected void start4400()
	{
		
		covrpfDAO.setCacheObject(covrenqpf);
		chdrDao.setCacheObject(chdrpf);
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.taxind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
	}

protected void taxRet4500()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.taxind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void checkCalcTax5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrenqpf.getCrtable());
		itempf.setItemitem((wsaaTr52eKey).toString());
		itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.company).toString() , (tablesInner.tr52e).toString() , (wsaaTr52eKey).toString() );
		if(itempfList == null || (itempfList!=null && itempfList.size()==0))
				{
					itempf.setItempfx("IT");
					itempf.setItemcoy((wsspcomn.company).toString());
					itempf.setItemtabl((tablesInner.tr52e).toString());
					wsaaTr52eKey.set(SPACES);
					wsaaTr52eTxcode.set(tr52drec.txcode);
					wsaaTr52eCnttype.set(chdrpf.getCnttype());
					wsaaTr52eCrtable.set("****");
					itempf.setItemitem((wsaaTr52eKey).toString());
				}
		itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.company).toString() , (tablesInner.tr52e).toString() , wsaaTr52eKey.toString() );
		if(itempfList == null || (itempfList!=null && itempfList.size()==0))
						{
					itempf.setItempfx("IT");
					itempf.setItemcoy((wsspcomn.company).toString());
					itempf.setItemtabl((tablesInner.tr52e).toString());
					wsaaTr52eKey.set(SPACES);
					wsaaTr52eTxcode.set(tr52drec.txcode);
					wsaaTr52eCnttype.set("***");
					wsaaTr52eCrtable.set("****");
					itempf.setItemitem((wsaaTr52eKey).toString());
					itempfList= itemDao.getAllitemsbyCurrency("IT" , (wsspcomn.company).toString() , (tablesInner.tr52e).toString() , wsaaTr52eKey.toString() );
		if(itempfList == null || (itempfList!=null && itempfList.size()==0))
					{
						return;
					}
			}
		tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));//ILIFE-8131
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covrenqpf.getLife());
			txcalcrec.coverage.set(covrenqpf.getCoverage());
			txcalcrec.rider.set(covrenqpf.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covrenqpf.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			/* We should calculate the tax on the regular premium only if     */
			/* we have a regular premium component. If we have a single prem  */
			/* component then we calculate the tax on the single premium amt  */
			if (isEQ(covrenqpf.getInstprem(), 0)) {
				/*         this means we have single premium component            */
				txcalcrec.amountIn.set(sv.singlePremium);
			}
			else {
				/*         this means we have a regular premium component         */
				if (isEQ(tr52erec.zbastyp, "Y")) {
					txcalcrec.amountIn.set(sv.instPrem);
				}
				else {
					compute(txcalcrec.amountIn, 2).set(add(add(sv.zlinstprem, sv.instPrem),sv.singlePremium));/*ILIFE-3361*/
				}
			}
			txcalcrec.transType.set("PREM");
			if (isEQ(chdrpf.getBillfreq(), "00")) {
				txcalcrec.effdate.set(covrenqpf.getCrrcd());
			}
			else {
				txcalcrec.effdate.set(chdrpf.getPtdate());
			}
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			txcalcrec.txcode.set(tr52drec.txcode);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(txcalcrec.amountIn);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("N");
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void determineSurrCovr8888(Covrpf covrpf)
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		if (isEQ(covrsurpf.getPlanSuffix(), ZERO))
			{
				return;
			}
		/* MOVE COVRSUR-PSTATCODE      TO WSAA-PREMIUM-STATII.          */
		wsaaCovrPstatcode.set(covrsurIO.getPstatcode());
		checkPremStatus1300();
		/* IF NOT PAYING-STATUS                                         */
		if (!validStatus.isTrue()) {
			wsaaSurrCnt.add(1);
			wsaaSurrTable[wsaaSurrCnt.toInt()].set(covrsurIO.getPlanSuffix());
		}
	}



protected void getLifeName9999()
	{
		para9999();
	}

protected void para9999()
	{
		lifeenqIO.setChdrcoy(covrenqpf.getChdrcoy());
		lifeenqIO.setChdrnum(covrenqpf.getChdrnum());
		lifeenqIO.setLife(covrenqpf.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.currency.set(sv.cntcurr);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}

protected void b000Rounding()
	{
		b100Start();
	}

protected void b100Start()
	{
		zrdecplrec.amountIn.set(wsaaMiscellaneousInner.wsaaSingp);
		a000CallRounding();
		wsaaMiscellaneousInner.wsaaSingp.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaMiscellaneousInner.wsaaPremium);
		a000CallRounding();
		wsaaMiscellaneousInner.wsaaPremium.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaMiscellaneousInner.wsaaSumins);
		a000CallRounding();
		wsaaMiscellaneousInner.wsaaSumins.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaMiscellaneousInner.wsaaCoverageDebit);
		a000CallRounding();
		wsaaMiscellaneousInner.wsaaCoverageDebit.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaMiscellaneousInner.wsaaZlinstprem);
		a000CallRounding();
		wsaaMiscellaneousInner.wsaaZlinstprem.set(zrdecplrec.amountOut);
	}

/*ILIFE-7805 - Start*/

/**
* This method determines if the component is Single Premium Type or not
* 
* @return boolean
*/
protected boolean isSinglePremTypeComp(){	
	
	boolean superAnnuFlag = false;
	boolean singPrmTypCompFlag = false;
	boolean cntTypeFlag = false;	
	boolean singlePremIndFlag = false;
	String itemItem = "";
	Itempf itempf = null;
	if(chdrpf.getChdrcoy() != null){
	    superAnnuFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP07", appVars, "IT");
	}	
	if(superAnnuFlag){
		//1.1. Check TD5J9 (Single Premium Type Components) for Contract Type (3 characters) + Component Code (4 characters)
		if(chdrpf.getCnttype() != null && covrenqpf.getCrtable() != null){
				itemItem = chdrpf.getCnttype().trim() + covrenqpf.getCrtable().trim();/* IJTI-1523 */
		}		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(itemItem);
		itempf.setItemtabl("TD5J9");

		
		itempf = itemDao.getItempfRecord(itempf);
		if(itempf != null && itempf.getGenarea() != null) {						
			singPrmTypCompFlag = true;			
		} 
		
		//1.2. Component is single premium or not by checking single premium indicator in T5687
		t5687Load7010();
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			singlePremIndFlag = true;
		}
		
		//1.3. Check contract type (3 characters) has valid item in TR59X (Existence of contract type e.g SIS in TR59X)
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrpf.getCnttype().trim());/* IJTI-1523 */
		itempf.setItemtabl("TR59X");		
		
		itempf = itemDao.getItempfRecord(itempf);
		if(itempf != null && itempf.getGenarea() != null) {						
			cntTypeFlag = true;			
		} 
		
		//1.4. Check if all conditions required for Single premium type component are met or not
		if(singPrmTypCompFlag && cntTypeFlag && singlePremIndFlag){
			singPremTypeFlag = true;
		}
	}	
	
	return singPremTypeFlag;
}

protected void t5687Load7010()
{
	/* General Coverage/Rider details.*/
	itdmIO.setDataKey(SPACES);
	itdmIO.setItemcoy(chdrpf.getChdrcoy());
	itdmIO.setItemtabl(tablesInner.t5687);
	itdmIO.setItemitem(covrenqpf.getCrtable());
	itdmIO.setItmfrm(chdrpf.getOccdate());
	itdmIO.setFunction("BEGN");
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		fatalError600();
	}
	t5687rec.t5687Rec.set(itdmIO.getGenarea());
}

//ILIFE-7805 - END


/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private PackedDecimalData wsaaSuffixCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(5).isAPartOf(wsaaT5551Key, 0);
	private FixedLengthStringData wsaaCrcyCode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCoverageDebit = new PackedDecimalData(17, 2);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5551 = new FixedLengthStringData(5).init("T5551");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
}
