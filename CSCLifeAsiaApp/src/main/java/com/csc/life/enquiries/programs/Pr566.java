/*
 * File: Pr566.java
 * Date: 30 August 2009 1:41:53
 * Author: Quipoz Limited
 * 
 * Class transformed from PR566.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.screens.Sr566ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovttrmTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.MinsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.tablestructures.Th616rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  This program to capture Installment Premium Details during
*  Proposal Entry is Provided.
*
*****************************************************************
* </pre>
*/
public class Pr566 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR566");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTotInstalment = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCoveragePrem = new PackedDecimalData(17, 2);
	private String e492 = "E492";
	private String rl19 = "RL19";
	private String rl20 = "RL20";
	private String rl21 = "RL21";
		/* TABLES */
	private String th616 = "TH616";
	private String itemrec = "ITEMREC";
	private String mrtarec = "MRTAREC";
	private String minsrec = "MINSREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
		/*Coverage transactions - term*/
	private CovttrmTableDAM covttrmIO = new CovttrmTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Installment Schedule Details*/
	private MinsTableDAM minsIO = new MinsTableDAM();
		/*Reducing Term Assurance details*/
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private Th616rec th616rec = new Th616rec();
	private Sr566ScreenVars sv = ScreenProgram.getScreenVars( Sr566ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Pr566() {
		super();
		screenVars = sv;
		new ScreenModel("Sr566", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaIndex.set(ZERO);
		wsaaCoveragePrem.set(ZERO);
		wsaaTotInstalment.set(ZERO);
		sv.linstamt01.set(ZERO);
		sv.linstamt02.set(ZERO);
		sv.linstamt03.set(ZERO);
		sv.linstamt04.set(ZERO);
		sv.linstamt05.set(ZERO);
		sv.linstamt06.set(ZERO);
		sv.linstamt07.set(ZERO);
		sv.mnth01.set(ZERO);
		sv.mnth02.set(ZERO);
		sv.mnth03.set(ZERO);
		sv.mnth04.set(ZERO);
		sv.mnth05.set(ZERO);
		sv.mnth06.set(ZERO);
		sv.mnth07.set(ZERO);
		sv.datedue01.set(varcom.vrcmMaxDate);
		sv.datedue02.set(varcom.vrcmMaxDate);
		sv.datedue03.set(varcom.vrcmMaxDate);
		sv.datedue04.set(varcom.vrcmMaxDate);
		sv.datedue05.set(varcom.vrcmMaxDate);
		sv.datedue06.set(varcom.vrcmMaxDate);
		sv.datedue07.set(varcom.vrcmMaxDate);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setSeqnbr(ZERO);
		covttrmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covttrmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covttrmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(),varcom.oK)
		&& isNE(covttrmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(),covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(),covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
		minsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		minsIO.setChdrnum(chdrlnbIO.getChdrnum());
		minsIO.setLife(covtlnbIO.getLife());
		minsIO.setCoverage(covtlnbIO.getCoverage());
		minsIO.setRider(covtlnbIO.getRider());
		minsIO.setFormat(minsrec);
		minsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(),varcom.oK)
		&& isNE(minsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		if (isEQ(minsIO.getStatuz(),varcom.oK)) {
			sv.linstamt01.set(minsIO.getLinstamt01());
			sv.linstamt02.set(minsIO.getLinstamt02());
			sv.linstamt03.set(minsIO.getLinstamt03());
			sv.linstamt04.set(minsIO.getLinstamt04());
			sv.linstamt05.set(minsIO.getLinstamt05());
			sv.linstamt06.set(minsIO.getLinstamt06());
			sv.linstamt07.set(minsIO.getLinstamt07());
			sv.datedue01.set(minsIO.getDatedue01());
			sv.datedue02.set(minsIO.getDatedue02());
			sv.datedue03.set(minsIO.getDatedue03());
			sv.datedue04.set(minsIO.getDatedue04());
			sv.datedue05.set(minsIO.getDatedue05());
			sv.datedue06.set(minsIO.getDatedue06());
			sv.datedue07.set(minsIO.getDatedue07());
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit1090);
		}
		readMrta1100();
		if (isNE(mrtaIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit1090);
		}
		if (isNE(mrtaIO.getMlinsopt(),SPACES)) {
			readTh616Table1200();
			sv.mnth01Out[varcom.pr.toInt()].set("Y");
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,th616rec.mnth)); wsaaIndex.add(1)){
				sv.mnth[wsaaIndex.toInt()].set(wsaaIndex);
			}
		}
		if (isEQ(th616rec.mnth,1)) {
			sv.linstamt01.set(covttrmIO.getInstprem());
		}
	}

protected void readMrta1100()
	{
		/*START*/
		mrtaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mrtaIO.setChdrnum(chdrlnbIO.getChdrnum());
		mrtaIO.setFormat(mrtarec);
		mrtaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mrtaIO);
		if (isNE(mrtaIO.getStatuz(),varcom.oK)
		&& isNE(mrtaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(mrtaIO.getStatuz());
			syserrrec.params.set(mrtaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readTh616Table1200()
	{
		start1200();
	}

protected void start1200()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(th616);
		itemIO.setItemitem(mrtaIO.getMlinsopt());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th616rec.th616Rec.set(itemIO.getGenarea());
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		compute(wsaaTotInstalment, 2).set(add(add(add(add(add(add(sv.linstamt01,sv.linstamt02),sv.linstamt03),sv.linstamt04),sv.linstamt05),sv.linstamt06),sv.linstamt07));
		if (isNE(covttrmIO.getInstprem(),ZERO)) {
			wsaaCoveragePrem.set(covttrmIO.getInstprem());
		}
		else {
			wsaaCoveragePrem.set(covttrmIO.getSingp());
		}
		if (isNE(wsaaTotInstalment,wsaaCoveragePrem)) {
			sv.linstamt01Err.set(rl19);
		}
		if (isNE(sv.linstamt01,ZERO)
		&& isEQ(sv.mnth01,ZERO)) {
			sv.linstamt01Err.set(e492);
		}
		if (isNE(sv.linstamt02,ZERO)
		&& isEQ(sv.mnth02,ZERO)) {
			sv.linstamt02Err.set(e492);
		}
		if (isNE(sv.linstamt03,ZERO)
		&& isEQ(sv.mnth03,ZERO)) {
			sv.linstamt03Err.set(e492);
		}
		if (isNE(sv.linstamt04,ZERO)
		&& isEQ(sv.mnth04,ZERO)) {
			sv.linstamt04Err.set(e492);
		}
		if (isNE(sv.linstamt05,ZERO)
		&& isEQ(sv.mnth05,ZERO)) {
			sv.linstamt05Err.set(e492);
		}
		if (isNE(sv.linstamt06,ZERO)
		&& isEQ(sv.mnth06,ZERO)) {
			sv.linstamt06Err.set(e492);
		}
		if (isNE(sv.linstamt07,ZERO)
		&& isEQ(sv.mnth07,ZERO)) {
			sv.linstamt07Err.set(e492);
		}
		if (isNE(sv.mnth01,ZERO)
		&& isEQ(sv.datedue01,varcom.vrcmMaxDate)) {
			sv.datedue01Err.set(rl20);
		}
		if (isNE(sv.mnth02,ZERO)
		&& isEQ(sv.datedue02,varcom.vrcmMaxDate)) {
			sv.datedue02Err.set(rl20);
		}
		if (isNE(sv.mnth03,ZERO)
		&& isEQ(sv.datedue03,varcom.vrcmMaxDate)) {
			sv.datedue03Err.set(rl20);
		}
		if (isNE(sv.mnth04,ZERO)
		&& isEQ(sv.datedue04,varcom.vrcmMaxDate)) {
			sv.datedue04Err.set(rl20);
		}
		if (isNE(sv.mnth05,ZERO)
		&& isEQ(sv.datedue05,varcom.vrcmMaxDate)) {
			sv.datedue05Err.set(rl20);
		}
		if (isNE(sv.mnth06,ZERO)
		&& isEQ(sv.datedue06,varcom.vrcmMaxDate)) {
			sv.datedue06Err.set(rl20);
		}
		if (isNE(sv.mnth07,ZERO)
		&& isEQ(sv.datedue07,varcom.vrcmMaxDate)) {
			sv.datedue07Err.set(rl20);
		}
		if (isNE(sv.datedue01,varcom.vrcmMaxDate)
		&& isEQ(sv.mnth01,ZERO)) {
			sv.datedue01Err.set(e492);
		}
		if (isNE(sv.datedue02,varcom.vrcmMaxDate)
		&& isEQ(sv.mnth02,ZERO)) {
			sv.datedue02Err.set(e492);
		}
		if (isNE(sv.datedue03,varcom.vrcmMaxDate)
		&& isEQ(sv.mnth03,ZERO)) {
			sv.datedue03Err.set(e492);
		}
		if (isNE(sv.datedue04,varcom.vrcmMaxDate)
		&& isEQ(sv.mnth04,ZERO)) {
			sv.datedue04Err.set(e492);
		}
		if (isNE(sv.datedue05,varcom.vrcmMaxDate)
		&& isEQ(sv.mnth05,ZERO)) {
			sv.datedue05Err.set(e492);
		}
		if (isNE(sv.datedue06,varcom.vrcmMaxDate)
		&& isEQ(sv.mnth06,ZERO)) {
			sv.datedue06Err.set(e492);
		}
		if (isNE(sv.datedue07,varcom.vrcmMaxDate)
		&& isEQ(sv.mnth07,ZERO)) {
			sv.datedue07Err.set(e492);
		}
		if (isNE(sv.datedue02,varcom.vrcmMaxDate)
		&& isNE(sv.mnth02,ZERO)) {
			if (isGTE(sv.datedue01,sv.datedue02)) {
				sv.datedue02Err.set(rl21);
			}
		}
		if (isNE(sv.datedue03,varcom.vrcmMaxDate)
		&& isNE(sv.mnth03,ZERO)) {
			if (isGTE(sv.datedue02,sv.datedue03)) {
				sv.datedue03Err.set(rl21);
			}
		}
		if (isNE(sv.datedue04,varcom.vrcmMaxDate)
		&& isNE(sv.mnth04,ZERO)) {
			if (isGTE(sv.datedue03,sv.datedue04)) {
				sv.datedue04Err.set(rl21);
			}
		}
		if (isNE(sv.datedue05,varcom.vrcmMaxDate)
		&& isNE(sv.mnth05,ZERO)) {
			if (isGTE(sv.datedue04,sv.datedue05)) {
				sv.datedue05Err.set(rl21);
			}
		}
		if (isNE(sv.datedue06,varcom.vrcmMaxDate)
		&& isNE(sv.mnth06,ZERO)) {
			if (isGTE(sv.datedue05,sv.datedue06)) {
				sv.datedue06Err.set(rl21);
			}
		}
		if (isNE(sv.datedue07,varcom.vrcmMaxDate)
		&& isNE(sv.mnth07,ZERO)) {
			if (isGTE(sv.datedue06,sv.datedue07)) {
				sv.datedue07Err.set(rl21);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		minsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		minsIO.setChdrnum(chdrlnbIO.getChdrnum());
		minsIO.setLife(covtlnbIO.getLife());
		minsIO.setCoverage(covtlnbIO.getCoverage());
		minsIO.setRider(covtlnbIO.getRider());
		minsIO.setFormat(minsrec);
		minsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(),varcom.oK)
		&& isNE(minsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		minsIO.setLinstamt01(sv.linstamt01);
		minsIO.setLinstamt02(sv.linstamt02);
		minsIO.setLinstamt03(sv.linstamt03);
		minsIO.setLinstamt04(sv.linstamt04);
		minsIO.setLinstamt05(sv.linstamt05);
		minsIO.setLinstamt06(sv.linstamt06);
		minsIO.setLinstamt07(sv.linstamt07);
		minsIO.setDatedue01(sv.datedue01);
		minsIO.setDatedue02(sv.datedue02);
		minsIO.setDatedue03(sv.datedue03);
		minsIO.setDatedue04(sv.datedue04);
		minsIO.setDatedue05(sv.datedue05);
		minsIO.setDatedue06(sv.datedue06);
		minsIO.setDatedue07(sv.datedue07);
		if (isEQ(minsIO.getStatuz(),varcom.oK)) {
			minsIO.setFunction(varcom.rewrt);
		}
		else {
			minsIO.setFunction(varcom.updat);
		}
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(),varcom.oK)
		&& isNE(minsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		scrnparams.function.set("HIDEW");
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
