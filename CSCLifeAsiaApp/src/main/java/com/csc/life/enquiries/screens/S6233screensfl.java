package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6233screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {13, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 16, 3, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6233ScreenVars sv = (S6233ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6233screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6233screensfl, 
			sv.S6233screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6233ScreenVars sv = (S6233ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6233screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6233ScreenVars sv = (S6233ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6233screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6233screensflWritten.gt(0))
		{
			sv.s6233screensfl.setCurrentIndex(0);
			sv.S6233screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6233ScreenVars sv = (S6233ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6233screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6233ScreenVars screenVars = (S6233ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.hselect.setFieldName("hselect");
				screenVars.hflag.setFieldName("hflag");
				screenVars.htxdateDisp.setFieldName("htxdateDisp");
				screenVars.hreason.setFieldName("hreason");
				screenVars.tranno.setFieldName("tranno");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.trcode.setFieldName("trcode");
				screenVars.trandesc.setFieldName("trandesc");
				screenVars.fillh.setFieldName("fillh");
				screenVars.filll.setFieldName("filll");
				screenVars.datesubDisp.setFieldName("datesubDisp");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.dataloc.setFieldName("dataloc");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.hselect.set(dm.getField("hselect"));
			screenVars.hflag.set(dm.getField("hflag"));
			screenVars.htxdateDisp.set(dm.getField("htxdateDisp"));
			screenVars.hreason.set(dm.getField("hreason"));
			screenVars.tranno.set(dm.getField("tranno"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.trcode.set(dm.getField("trcode"));
			screenVars.trandesc.set(dm.getField("trandesc"));
			screenVars.fillh.set(dm.getField("fillh"));
			screenVars.filll.set(dm.getField("filll"));
			screenVars.datesubDisp.set(dm.getField("datesubDisp"));
			screenVars.crtuser.set(dm.getField("crtuser"));
			screenVars.dataloc.set(dm.getField("dataloc"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6233ScreenVars screenVars = (S6233ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.hselect.setFieldName("hselect");
				screenVars.hflag.setFieldName("hflag");
				screenVars.htxdateDisp.setFieldName("htxdateDisp");
				screenVars.hreason.setFieldName("hreason");
				screenVars.tranno.setFieldName("tranno");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.trcode.setFieldName("trcode");
				screenVars.trandesc.setFieldName("trandesc");
				screenVars.fillh.setFieldName("fillh");
				screenVars.filll.setFieldName("filll");
				screenVars.datesubDisp.setFieldName("datesubDisp");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.dataloc.setFieldName("dataloc");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("hselect").set(screenVars.hselect);
			dm.getField("hflag").set(screenVars.hflag);
			dm.getField("htxdateDisp").set(screenVars.htxdateDisp);
			dm.getField("hreason").set(screenVars.hreason);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("trcode").set(screenVars.trcode);
			dm.getField("trandesc").set(screenVars.trandesc);
			dm.getField("fillh").set(screenVars.fillh);
			dm.getField("filll").set(screenVars.filll);
			dm.getField("datesubDisp").set(screenVars.datesubDisp);
			dm.getField("crtuser").set(screenVars.crtuser);
			dm.getField("dataloc").set(screenVars.dataloc);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6233screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6233ScreenVars screenVars = (S6233ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.hselect.clearFormatting();
		screenVars.hflag.clearFormatting();
		screenVars.htxdateDisp.clearFormatting();
		screenVars.hreason.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.trcode.clearFormatting();
		screenVars.trandesc.clearFormatting();
		screenVars.fillh.clearFormatting();
		screenVars.filll.clearFormatting();
		screenVars.datesubDisp.clearFormatting();
		screenVars.crtuser.clearFormatting();
		screenVars.dataloc.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6233ScreenVars screenVars = (S6233ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.hselect.setClassString("");
		screenVars.hflag.setClassString("");
		screenVars.htxdateDisp.setClassString("");
		screenVars.hreason.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.trcode.setClassString("");
		screenVars.trandesc.setClassString("");
		screenVars.fillh.setClassString("");
		screenVars.filll.setClassString("");
		screenVars.datesubDisp.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.dataloc.setClassString("");
	}

/**
 * Clear all the variables in S6233screensfl
 */
	public static void clear(VarModel pv) {
		S6233ScreenVars screenVars = (S6233ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.hselect.clear();
		screenVars.hflag.clear();
		screenVars.htxdateDisp.clear();
		screenVars.htxdate.clear();
		screenVars.hreason.clear();
		screenVars.tranno.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.trcode.clear();
		screenVars.trandesc.clear();
		screenVars.fillh.clear();
		screenVars.filll.clear();
		screenVars.datesubDisp.clear();
		screenVars.datesub.clear();
		screenVars.crtuser.clear();
		screenVars.dataloc.clear();
	}
}
