/*
 * File: Pr50r.java
 * Date: 30 August 2009 1:33:26
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50R.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.enquiries.dataaccess.LifetrnTableDAM;
import com.csc.life.enquiries.screens.Sr50rScreenVars;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* PR50R - Life Assured Details
* The program displays the details of the Life Assured on the
* transaction date.
*
* 1000-section
* - Initialize screen fields
* - Set the field RELATION non-display indicator to 'Y'
* - Retrieve data from LIFETRN files by using RETRV function
* - Set the screen fields
*
***********************************************************************
* </pre>
*/
public class Pr50r extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50R");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-CLNT */
	private FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8);
		/* FORMATS */
	private static final String lifetrnrec = "LIFETRNREC";
	private static final String undlrec = "UNDLREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private LifetrnTableDAM lifetrnIO = new LifetrnTableDAM();
	private UndlTableDAM undlIO = new UndlTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Sr50rScreenVars sv = ScreenProgram.getScreenVars( Sr50rScreenVars.class);

	public Pr50r() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50r", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.anbage.set(ZERO);
		sv.dob.set(varcom.maxdate);
		sv.bmi.set(ZERO);
		sv.height.set(ZERO);
		sv.weight.set(ZERO);
		sv.relationOut[varcom.nd.toInt()].set("Y");
		lifetrnIO.setFormat(lifetrnrec);
		lifetrnIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(lifetrnIO.getChdrnum());
		sv.life.set(lifetrnIO.getLife());
		sv.jlife.set(lifetrnIO.getJlife());
		sv.lifenum.set(lifetrnIO.getLifcnum());
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(lifetrnIO.getLifcnum());
		a1000CallNamadrs();
		sv.lifedesc.set(namadrsrec.name);
		sv.sex.set(lifetrnIO.getCltsex());
		sv.dob.set(lifetrnIO.getCltdob());
		sv.anbage.set(lifetrnIO.getAnbAtCcd());
		sv.selection.set(lifetrnIO.getSelection());
		sv.smoking.set(lifetrnIO.getSmoking());
		sv.occup.set(lifetrnIO.getOccup());
		sv.pursuit01.set(lifetrnIO.getPursuit01());
		sv.pursuit02.set(lifetrnIO.getPursuit02());
		if (isNE(lifetrnIO.getJlife(),"00")) {
			sv.relationOut[varcom.nd.toInt()].set("N");
			sv.relation.set(lifetrnIO.getLiferel());
		}
		undlIO.setDataArea(SPACES);
		undlIO.setChdrcoy(lifetrnIO.getChdrcoy());
		undlIO.setChdrnum(lifetrnIO.getChdrnum());
		undlIO.setLife(lifetrnIO.getLife());
		undlIO.setJlife(lifetrnIO.getJlife());
		undlIO.setFormat(undlrec);
		undlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(),varcom.oK)
		&& isNE(undlIO.getStatuz(),varcom.mrnf)) {
			syserrrec.dbparams.set(undlIO.getParams());
			fatalError600();
		}
		if (isEQ(undlIO.getStatuz(),varcom.oK)) {
			sv.bmi.set(undlIO.getBmi());
			sv.height.set(undlIO.getHeight());
			sv.weight.set(undlIO.getWeight());
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a1000CallNamadrs()
	{
		a1000Begin();
	}

protected void a1000Begin()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}
}
