package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:55
 * Description:
 * Copybook name: UWLVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Uwlvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData uwlvFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData uwlvKey = new FixedLengthStringData(256).isAPartOf(uwlvFileKey, 0, REDEFINE);
  	public FixedLengthStringData uwlvCompany = new FixedLengthStringData(1).isAPartOf(uwlvKey, 0);
  	public FixedLengthStringData uwlvUserid = new FixedLengthStringData(10).isAPartOf(uwlvKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(uwlvKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(uwlvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		uwlvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}