/*
 * File: Chkrlmt.java
 * Date: 29 August 2009 22:39:39
 * Author: Quipoz Limited
 * 
 * Class transformed from CHKRLMT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.enquiries.dataaccess.UndrprpTableDAM;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.enquiries.tablestructures.Tr694rec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is a subprogram with 3 called functions, namely
*
* 1)  C/R? - To let the subprogram determine whether it is
*            to use Risk Method or Base Method (ie Contract
*            Type Level)
*
* 2)  READ - To let the subprogram determine the different
*            Risk Classes base on the CntType & CRTable
*
* 3)  CHCK - To let the subprogram determine whether the total
*            Sum Assured has exceeded the maximum limit
*
*****************************************************************
* </pre>
*/
public class Chkrlmt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private BinaryData wsaaTotSumins = new BinaryData(17, 2);
	private FixedLengthStringData wsaaTxxxx = new FixedLengthStringData(6);
	private BinaryData x = new BinaryData(5, 0);
		/* ERRORS */
	private String e417 = "E417";
	private String invf = "INVF";
	private String r051 = "R051";
	private String r065 = "R065";
	private String rl30 = "RL30";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String undrprprec = "UNDRPRPREC";
		/* TABLES */
	private String t5446 = "T5446";
	private String t5448 = "T5448";
	private String th618 = "TH618";
	private String tr694 = "TR694";
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private Th618rec th618rec = new Th618rec();
	private Tr694rec tr694rec = new Tr694rec();
		/*U/W Logical File for Proposal*/
	private UndrprpTableDAM undrprpIO = new UndrprpTableDAM();
	private Varcom varcom = new Varcom();
	private Itdmkey wsaaItdmkey = new Itdmkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit4000, 
		exit7000
	}

	public Chkrlmt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		chkrlrec.parmRec = convertAndSetParam(chkrlrec.parmRec, parmArray, 0);
		try {
			mainSect1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainSect1000()
	{
		ctrl1000();
		exit1000();
	}

protected void ctrl1000()
	{
		chkrlrec.statuz.set(varcom.oK);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isEQ(chkrlrec.function,"C/R?")){
			readTr6942000();
		}
		else if (isEQ(chkrlrec.function,"READ")){
			readTabls3000();
		}
		else if (isEQ(chkrlrec.function,"CHCK")){
			readTr6942000();
			if (isEQ(tr694rec.sel02,"Y")) {
				readTabls3000();
				for (x.set(1); !(isGT(x,5)); x.add(1)){
					readT54464000();
					if (isNE(t5446rec.maxAmount,ZERO)) {
						readUndrpf5000();
						chckLimit6000();
					}
				}
			}
		}
		else{
			chkrlrec.statuz.set(invf);
		}
	}

protected void exit1000()
	{
		exitProgram();
	}

protected void readTr6942000()
	{
		/*CTRL*/
		wsaaTxxxx.set(tr694);
		wsaaItemitem.set(chkrlrec.cnttype);
		readrItemio9000();
		tr694rec.tr694Rec.set(itemIO.getGenarea());
		if (isEQ(chkrlrec.function,"C/R?")) {
			chkrlrec.statuz.set("C");
			if (isEQ(tr694rec.sel01,"R")) {
				chkrlrec.statuz.set("R");
			}
		}
		/*EXIT*/
	}

protected void readTabls3000()
	{
		ctrl3000();
	}

protected void ctrl3000()
	{
		wsaaTxxxx.set(t5448);
		wsaaItemitem.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(chkrlrec.cnttype.toString());
		stringVariable1.append(chkrlrec.crtable.toString());
		wsaaItemitem.setLeft(stringVariable1.toString());
		begnItdmio9100();
		t5448rec.t5448Rec.set(itdmIO.getGenarea());
		if (isEQ(t5448rec.rrsktyp,SPACES)) {
			chkrlrec.statuz.set(r065);
			itdmIO.setDataKey(wsaaItdmkey);
			chkrlrec.params.set(itdmIO.getParams());
			exit1000();
		}
		wsaaTxxxx.set(th618);
		wsaaItemitem.set(t5448rec.rrsktyp);
		readrItemio9000();
		th618rec.th618Rec.set(itemIO.getGenarea());
		if (isEQ(th618rec.lrkclss,SPACES)) {
			chkrlrec.statuz.set(rl30);
			chkrlrec.params.set(itemIO.getParams());
			exit1000();
		}
		chkrlrec.lrkclss.set(th618rec.lrkclss);
	}

protected void readT54464000()
	{
		try {
			ctrl4000();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl4000()
	{
		if (isEQ(th618rec.lrkcls[x.toInt()],SPACES)) {
			goTo(GotoLabel.exit4000);
		}
		wsaaTxxxx.set(t5446);
		wsaaItemitem.set(th618rec.lrkcls[x.toInt()]);
		begnItdmio9100();
		t5446rec.t5446Rec.set(itdmIO.getGenarea());
		if (isEQ(t5446rec.t5446Rec,SPACES)) {
			chkrlrec.statuz.set(r051);
			itdmIO.setDataKey(wsaaItdmkey);
			chkrlrec.params.set(itdmIO.getParams());
			exit1000();
		}
		if (isEQ(t5446rec.lrkcls,SPACES)) {
			t5446rec.lrkcls.set(th618rec.lrkcls[x.toInt()]);
		}
		if (isEQ(t5446rec.fieldseq,ZERO)) {
			t5446rec.fieldseq.set(1);
		}
	}

protected void readUndrpf5000()
	{
		/*CTRL*/
		wsaaTotSumins.set(chkrlrec.sumins);
		undrprpIO.setRecKeyData(SPACES);
		undrprpIO.setClntnum(chkrlrec.clntnum);
		undrprpIO.setCoy(chkrlrec.company);
		undrprpIO.setCurrcode(chkrlrec.currcode);
		undrprpIO.setLrkcls02(t5446rec.lrkcls);
		undrprpIO.setFormat(undrprprec);
		undrprpIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undrprpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undrprpIO.setFitKeysSearch("CLNTNUM", "COY", "CURRCODE", "LRKCLS02");
		undrprpIO.setStatuz(varcom.oK);
		while ( !(isNE(undrprpIO.getStatuz(),varcom.oK))) {
			nextrUndrprpio7000();
		}
		
		/*EXIT*/
	}

protected void chckLimit6000()
	{
		/*CTRL*/
		chkrlrec.statuz.set(varcom.oK);
		if ((setPrecision(wsaaTotSumins, 2)
		&& isGT(wsaaTotSumins,(mult(t5446rec.maxAmount,t5446rec.fieldseq))))) {
			chkrlrec.statuz.set(e417);
			exit1000();
		}
		/*EXIT*/
	}

protected void nextrUndrprpio7000()
	{
		try {
			ctrl7000();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl7000()
	{
		SmartFileCode.execute(appVars, undrprpIO);
		if (!(isEQ(undrprpIO.getStatuz(),varcom.oK)
		&& isEQ(chkrlrec.clntnum,undrprpIO.getClntnum())
		&& isEQ(chkrlrec.company,undrprpIO.getCoy())
		&& isEQ(chkrlrec.currcode,undrprpIO.getCurrcode())
		&& isEQ(t5446rec.lrkcls,undrprpIO.getLrkcls02()))) {
			undrprpIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit7000);
		}
		if (isEQ(undrprpIO.getChdrnum(),chkrlrec.chdrnum)
		&& isEQ(undrprpIO.getLife(),chkrlrec.life)
		&& isEQ(undrprpIO.getCrtable(),chkrlrec.crtable)) {
			undrprpIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit7000);
		}
		wsaaTotSumins.add(undrprpIO.getSumins());
		undrprpIO.setFunction(varcom.nextr);
	}

protected void readrItemio9000()
	{
		/*CTRL*/
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chkrlrec.company);
		itemIO.setItemtabl(wsaaTxxxx);
		itemIO.setItemitem(wsaaItemitem);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setGenarea(SPACES);
		}
		/*EXIT*/
	}

protected void begnItdmio9100()
	{
		ctrl9100();
	}

protected void ctrl9100()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(chkrlrec.company);
		itdmIO.setItemtabl(wsaaTxxxx);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		wsaaItdmkey.set(itdmIO.getDataKey());
		SmartFileCode.execute(appVars, itdmIO);
		if (!(isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(chkrlrec.company,itdmIO.getItemcoy())
		&& isEQ(wsaaTxxxx,itdmIO.getItemtabl())
		&& isEQ(wsaaItemitem,itdmIO.getItemitem()))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
		}
	}
}
