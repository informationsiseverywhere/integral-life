package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;


public class Sr57nscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] { 1, 5,  7,  60}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 17, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57nScreenVars sv = (Sr57nScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr57nscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr57nscreensfl, 
			sv.Sr57nscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr57nScreenVars sv = (Sr57nScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr57nscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr57nScreenVars sv = (Sr57nScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr57nscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr57nscreensflWritten.gt(0))
		{
			sv.sr57nscreensfl.setCurrentIndex(0);
			sv.Sr57nscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr57nScreenVars sv = (Sr57nScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr57nscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr57nScreenVars screenVars = (Sr57nScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hactval.setFieldName("hactval");
				screenVars.hemv.setFieldName("hemv");
				screenVars.htype.setFieldName("htype");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hlife.setFieldName("hlife");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.hupdflg.setFieldName("hupdflg");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fund.setFieldName("fund");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.percreqd.setFieldName("percreqd");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.estMatValue.setFieldName("estMatValue");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hactval.set(dm.getField("hactval"));
			screenVars.hemv.set(dm.getField("hemv"));
			screenVars.htype.set(dm.getField("htype"));
			screenVars.hcnstcur.set(dm.getField("hcnstcur"));
			screenVars.hcover.set(dm.getField("hcover"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.hlife.set(dm.getField("hlife"));
			screenVars.hjlife.set(dm.getField("hjlife"));
			screenVars.hupdflg.set(dm.getField("hupdflg"));
			screenVars.crrcdDisp.set(dm.getField("crrcdDisp"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.fund.set(dm.getField("fund"));
			screenVars.fieldType.set(dm.getField("fieldType"));
			screenVars.shortds.set(dm.getField("shortds"));
			screenVars.cnstcur.set(dm.getField("cnstcur"));
			screenVars.percreqd.set(dm.getField("percreqd"));
			screenVars.actvalue.set(dm.getField("actvalue"));
			screenVars.estMatValue.set(dm.getField("estMatValue"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr57nScreenVars screenVars = (Sr57nScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hactval.setFieldName("hactval");
				screenVars.hemv.setFieldName("hemv");
				screenVars.htype.setFieldName("htype");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hlife.setFieldName("hlife");
				screenVars.hjlife.setFieldName("hjlife");
				screenVars.hupdflg.setFieldName("hupdflg");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fund.setFieldName("fund");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.percreqd.setFieldName("percreqd");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.estMatValue.setFieldName("estMatValue");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hactval").set(screenVars.hactval);
			dm.getField("hemv").set(screenVars.hemv);
			dm.getField("htype").set(screenVars.htype);
			dm.getField("hcnstcur").set(screenVars.hcnstcur);
			dm.getField("hcover").set(screenVars.hcover);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("hlife").set(screenVars.hlife);
			dm.getField("hjlife").set(screenVars.hjlife);
			dm.getField("hupdflg").set(screenVars.hupdflg);
			dm.getField("crrcdDisp").set(screenVars.crrcdDisp);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("fund").set(screenVars.fund);
			dm.getField("fieldType").set(screenVars.fieldType);
			dm.getField("shortds").set(screenVars.shortds);
			dm.getField("cnstcur").set(screenVars.cnstcur);
			dm.getField("percreqd").set(screenVars.percreqd);
			dm.getField("actvalue").set(screenVars.actvalue);
			dm.getField("estMatValue").set(screenVars.estMatValue);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr57nscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr57nScreenVars screenVars = (Sr57nScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hactval.clearFormatting();
		screenVars.hemv.clearFormatting();
		screenVars.htype.clearFormatting();
		screenVars.hcnstcur.clearFormatting();
		screenVars.hcover.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.hlife.clearFormatting();
		screenVars.hjlife.clearFormatting();
		screenVars.hupdflg.clearFormatting();
		screenVars.crrcdDisp.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.fund.clearFormatting();
		screenVars.fieldType.clearFormatting();
		screenVars.shortds.clearFormatting();
		screenVars.cnstcur.clearFormatting();
		screenVars.percreqd.clearFormatting();
		screenVars.actvalue.clearFormatting();
		screenVars.estMatValue.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr57nScreenVars screenVars = (Sr57nScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hactval.setClassString("");
		screenVars.hemv.setClassString("");
		screenVars.htype.setClassString("");
		screenVars.hcnstcur.setClassString("");
		screenVars.hcover.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.hlife.setClassString("");
		screenVars.hjlife.setClassString("");
		screenVars.hupdflg.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.fund.setClassString("");
		screenVars.fieldType.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.cnstcur.setClassString("");
		screenVars.percreqd.setClassString("");
		screenVars.actvalue.setClassString("");
		screenVars.estMatValue.setClassString("");
	}

/**
 * Clear all the variables in Sr57nscreensfl
 */
	public static void clear(VarModel pv) {
		Sr57nScreenVars screenVars = (Sr57nScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hactval.clear();
		screenVars.hemv.clear();
		screenVars.htype.clear();
		screenVars.hcnstcur.clear();
		screenVars.hcover.clear();
		screenVars.hcrtable.clear();
		screenVars.hlife.clear();
		screenVars.hjlife.clear();
		screenVars.hupdflg.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.fund.clear();
		screenVars.fieldType.clear();
		screenVars.shortds.clear();
		screenVars.cnstcur.clear();
		screenVars.percreqd.clear();
		screenVars.actvalue.clear();
		screenVars.estMatValue.clear();
	}
}
