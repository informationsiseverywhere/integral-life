package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:22
 * Description:
 * Copybook name: COVRENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrenqKey = new FixedLengthStringData(64).isAPartOf(covrenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrenqChdrcoy = new FixedLengthStringData(1).isAPartOf(covrenqKey, 0);
  	public FixedLengthStringData covrenqChdrnum = new FixedLengthStringData(8).isAPartOf(covrenqKey, 1);
  	public FixedLengthStringData covrenqLife = new FixedLengthStringData(2).isAPartOf(covrenqKey, 9);
  	public PackedDecimalData covrenqPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrenqKey, 11);
  	public FixedLengthStringData covrenqCoverage = new FixedLengthStringData(2).isAPartOf(covrenqKey, 14);
  	public FixedLengthStringData covrenqRider = new FixedLengthStringData(2).isAPartOf(covrenqKey, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrenqKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}