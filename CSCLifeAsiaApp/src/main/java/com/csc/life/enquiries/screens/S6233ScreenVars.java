package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6233
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6233ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(588);
	public FixedLengthStringData dataFields = new FixedLengthStringData(284).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.conStatus.copy().isAPartOf(dataFields,8);//IBPLIFE-3582
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,121);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,129);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,176);
	public FixedLengthStringData premstatus = DD.premstatus_longdesc.copy().isAPartOf(dataFields,184);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,217);
	
	
	// ILB-326:Starts
	public ZonedDecimalData trannosearch = DD.tranno.copyToZonedDecimal().isAPartOf(dataFields,218);
	public ZonedDecimalData datesubsearch = DD.datesub.copyToZonedDecimal().isAPartOf(dataFields,223);
	public ZonedDecimalData effdatesearch = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,231);
	public FixedLengthStringData trcodesearch = DD.trcode.copy().isAPartOf(dataFields,239);
	public FixedLengthStringData trandescsearch = DD.trandesc.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData crtusersearch = DD.crtuser.copy().isAPartOf(dataFields,273);
	// ILB-326:Ends
	public FixedLengthStringData indAnn = DD.indic.copy().isAPartOf(dataFields,283);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 284);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	
	
	// ILB-326:Starts
	public FixedLengthStringData trannosearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData datesubsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData effdatesearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData trcodesearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData trandescsearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData crtusersearchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	// ILB-326:Ends

	public FixedLengthStringData indAnnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 360);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	
	// ILB-326:Starts
	public FixedLengthStringData[] trannosearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] datesubsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] effdatesearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] trcodesearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] trandescsearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] crtusersearchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	// ILB-326:Ends
	public FixedLengthStringData[] indAnnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(359);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(133).isAPartOf(subfileArea, 0);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData dataloc = DD.dataloc.copy().isAPartOf(subfileFields,10);
	public ZonedDecimalData datesub = DD.datesub.copyToZonedDecimal().isAPartOf(subfileFields,11);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,19);
	public FixedLengthStringData fillh = DD.fillh.copy().isAPartOf(subfileFields,27);
	public FixedLengthStringData filll = DD.filll.copy().isAPartOf(subfileFields,30);
	public FixedLengthStringData hflag = DD.hflag.copy().isAPartOf(subfileFields,33);
	public FixedLengthStringData hreason = DD.hreason.copy().isAPartOf(subfileFields,34);
	public FixedLengthStringData hselect = DD.hselect.copy().isAPartOf(subfileFields,84);
	public ZonedDecimalData htxdate = DD.htxdate.copyToZonedDecimal().isAPartOf(subfileFields,85);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,93);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(subfileFields,94);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,124);
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(subfileFields,129);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(56).isAPartOf(subfileArea, 133);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData datalocErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData datesubErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fillhErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData filllErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hreasonErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hselectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData htxdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(168).isAPartOf(subfileArea, 189);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] datalocOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] datesubOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fillhOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] filllOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hreasonOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hselectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] htxdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 357);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData datesubDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData htxdateDisp = new FixedLengthStringData(10);

	// ILB-326:Starts
	public FixedLengthStringData datesubsearchDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdatesearchDisp = new FixedLengthStringData(10);
	// ILB-326:Ends
	
	public LongData S6233screensflWritten = new LongData(0);
	public LongData S6233screenctlWritten = new LongData(0);
	public LongData S6233screenWritten = new LongData(0);
	public LongData S6233protectWritten = new LongData(0);
	public GeneralTable s6233screensfl = new GeneralTable(AppVars.getInstance());
	
	


	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6233screensfl;
	}

	public S6233ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","13",null, null, null, null, null, null, null, null});
		fieldIndMap.put(indicOut,new String[] {null,"03",null,null,null, null, null, null, null, null, null, null});
		fieldIndMap.put(indAnnOut,new String[] {null,"04",null,null,null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, hselect, hflag, htxdate, hreason, tranno, effdate, trcode, trandesc, fillh, filll, datesub, crtuser, dataloc};
		screenSflOutFields = new BaseData[][] {selectOut, hselectOut, hflagOut, htxdateOut, hreasonOut, trannoOut, effdateOut, trcodeOut, trandescOut, fillhOut, filllOut, datesubOut, crtuserOut, datalocOut};
		screenSflErrFields = new BaseData[] {selectErr, hselectErr, hflagErr, htxdateErr, hreasonErr, trannoErr, effdateErr, trcodeErr, trandescErr, fillhErr, filllErr, datesubErr, crtuserErr, datalocErr};
		screenSflDateFields = new BaseData[] {htxdate, effdate, datesub};
		screenSflDateErrFields = new BaseData[] {htxdateErr, effdateErr, datesubErr};
		screenSflDateDispFields = new BaseData[] {htxdateDisp, effdateDisp, datesubDisp};
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename,indic, trannosearch, datesubsearch, effdatesearch, trcodesearch, trandescsearch, crtusersearch,indAnn};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut,indicOut, trannosearchOut, datesubsearchOut, effdatesearchOut, trcodesearchOut, trandescsearchOut, crtusersearchOut,indAnnOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr,indicErr, trannosearchErr, datesubsearchErr, effdatesearchErr, trcodesearchErr, trandescsearchErr, crtusersearchErr,indAnnErr};
		screenDateFields = new BaseData[] {datesubsearch, effdatesearch};
		screenDateErrFields = new BaseData[] {datesubsearchErr, effdatesearchErr};
		screenDateDispFields = new BaseData[] {datesubsearchDisp, effdatesearchDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6233screen.class;
		screenSflRecord = S6233screensfl.class;
		screenCtlRecord = S6233screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6233protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6233screenctl.lrec.pageSubfile);
	}
}
