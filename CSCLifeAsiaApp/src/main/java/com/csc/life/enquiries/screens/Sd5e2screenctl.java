package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sd5e2screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sd5e2screensfl";
		lrec.subfileClass = Sd5e2screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 14;
		lrec.pageSubfile = 13;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 8, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5e2ScreenVars sv = (Sd5e2ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5e2screenctlWritten, sv.Sd5e2screensflWritten, av, sv.sd5e2screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5e2ScreenVars screenVars = (Sd5e2ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.accident.setClassString("");
		screenVars.hospital.setClassString("");
		screenVars.fundamnt.setClassString("");
		screenVars.loanVal.setClassString("");
		screenVars.surrval.setClassString("");
		screenVars.bonusVal.setClassString("");
	}

/**
 * Clear all the variables in Sd5e2screenctl
 */
	public static void clear(VarModel pv) {
		Sd5e2ScreenVars screenVars = (Sd5e2ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.sumin.clear();
		screenVars.accident.clear();
		screenVars.hospital.clear();
		screenVars.fundamnt.clear();
		screenVars.loanVal.clear();
		screenVars.surrval.clear();
		screenVars.bonusVal.clear();
	}
}
