package com.csc.life.enquiries.dataaccess.model;

import java.util.Date;

import com.csc.fsu.clients.dataaccess.model.Clntpf;

public class Agntenqpf {

	
		
		public long	unique_number;
		public String	agntcoy;
		public String	agntnum;
		public String	tranid;
		public String	clntcoy;
		public String	clntnum;
		public String	validflag;
		public String	usrprf;
		public String	jobnm;
		public Date	datime;
		
		private Clntpf clntpf;
	
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getTranid() {
		return tranid;
	}
	public void setTranid(String tranid) {
		this.tranid = tranid;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public Clntpf getClntpf() {
		return clntpf;
	}
	public void setClntpf(Clntpf clntpf) {
		this.clntpf = clntpf;
	}
		
	

}
