/*
 * File: Pr50sex.java
 * Date: 30 August 2009 1:33:41
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50SEX.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.dataaccess.CovrtrnTableDAM;
import com.csc.life.newbusiness.dataaccess.LextafiTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
***********************************************************************
* </pre>
*/
public class Pr50sex extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "PR50SEX";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String covrtrnrec = "COVRTRNREC";
	private String lextafirec = "LEXTAFIREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Coverage by Tranno*/
	private CovrtrnTableDAM covrtrnIO = new CovrtrnTableDAM();
		/*AFI view of LEXT Options/Extras file*/
	private LextafiTableDAM lextafiIO = new LextafiTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		errorProg610
	}

	public Pr50sex() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			performs0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void performs0010()
	{
		covrtrnIO.setFormat(covrtrnrec);
		covrtrnIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
		lextafiIO.setChdrcoy(covrtrnIO.getChdrcoy());
		lextafiIO.setChdrnum(covrtrnIO.getChdrnum());
		lextafiIO.setLife(covrtrnIO.getLife());
		lextafiIO.setCoverage(covrtrnIO.getCoverage());
		lextafiIO.setRider(covrtrnIO.getRider());
		lextafiIO.setSeqnbr(999);
		lextafiIO.setTranno(covrtrnIO.getTranno());
		lextafiIO.setFormat(lextafirec);
		lextafiIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, lextafiIO);
		if (isNE(lextafiIO.getStatuz(),varcom.oK)
		&& isNE(lextafiIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextafiIO.getParams());
			fatalError600();
		}
		if (isNE(covrtrnIO.getChdrcoy(),lextafiIO.getChdrcoy())
		|| isNE(covrtrnIO.getChdrnum(),lextafiIO.getChdrnum())
		|| isNE(covrtrnIO.getLife(),lextafiIO.getLife())
		|| isNE(covrtrnIO.getCoverage(),lextafiIO.getCoverage())
		|| isNE(covrtrnIO.getRider(),lextafiIO.getRider())) {
			lextafiIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextafiIO.getStatuz(),varcom.oK)) {
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit090);
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
