package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR50R
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50rScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(384);
	public FixedLengthStringData dataFields = new FixedLengthStringData(112).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbage = DD.anbage.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData bmi = DD.bmi.copyToZonedDecimal().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,7);
	public ZonedDecimalData dob = DD.dob.copyToZonedDecimal().isAPartOf(dataFields,15);
	public ZonedDecimalData height = DD.height.copyToZonedDecimal().isAPartOf(dataFields,23);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData occup = DD.occup.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData pursuits = new FixedLengthStringData(12).isAPartOf(dataFields, 82);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(2, 6, pursuits, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler,6);
	public FixedLengthStringData relation = DD.relation.copy().isAPartOf(dataFields,94);
	public FixedLengthStringData selection = DD.selection.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData smoking = DD.smoking.copy().isAPartOf(dataFields,101);
	public ZonedDecimalData weight = DD.weight.copyToZonedDecimal().isAPartOf(dataFields,103);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 112);
	public FixedLengthStringData anbageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bmiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData heightErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData occupErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(2, 4, pursuitsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData selectionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData sexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData smokingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData weightErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 180);
	public FixedLengthStringData[] anbageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bmiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] heightOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] occupOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(2, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] selectionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] weightOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dobDisp = new FixedLengthStringData(10);

	public LongData Sr50rscreenWritten = new LongData(0);
	public LongData Sr50rprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr50rScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(relationOut,new String[] {null, null, null, "01",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, lifenum, lifedesc, life, jlife, sex, dob, anbage, selection, smoking, occup, pursuit01, pursuit02, height, weight, bmi, relation};
		screenOutFields = new BaseData[][] {chdrnumOut, lifenumOut, lifedescOut, lifeOut, jlifeOut, sexOut, dobOut, anbageOut, selectionOut, smokingOut, occupOut, pursuit01Out, pursuit02Out, heightOut, weightOut, bmiOut, relationOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifenumErr, lifedescErr, lifeErr, jlifeErr, sexErr, dobErr, anbageErr, selectionErr, smokingErr, occupErr, pursuit01Err, pursuit02Err, heightErr, weightErr, bmiErr, relationErr};
		screenDateFields = new BaseData[] {dob};
		screenDateErrFields = new BaseData[] {dobErr};
		screenDateDispFields = new BaseData[] {dobDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr50rscreen.class;
		protectRecord = Sr50rprotect.class;
	}

}
