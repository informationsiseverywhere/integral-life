package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6247screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 30;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 21, 3, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6247ScreenVars sv = (S6247ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6247screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6247screensfl, 
			sv.S6247screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6247ScreenVars sv = (S6247ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6247screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6247ScreenVars sv = (S6247ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6247screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6247screensflWritten.gt(0))
		{
			sv.s6247screensfl.setCurrentIndex(0);
			sv.S6247screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6247ScreenVars sv = (S6247ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6247screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6247ScreenVars screenVars = (S6247ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.select.setFieldName("select");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.clntsname.setFieldName("clntsname");
				screenVars.bnyrlndesc.setFieldName("bnyrlndesc");
				screenVars.bnypc.setFieldName("bnypc");
				screenVars.cltreln.setFieldName("cltreln");
				screenVars.bnytype.setFieldName("bnytype");
				screenVars.relto.setFieldName("relto");
				screenVars.revcflg.setFieldName("revcflg");
				screenVars.enddateDisp.setFieldName("enddateDisp");/*ILIFE-3581*/
				screenVars.sequence.setFieldName("sequence");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.clntnum.set(dm.getField("clntnum"));
			screenVars.select.set(dm.getField("select"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.clntsname.set(dm.getField("clntsname"));
			screenVars.bnyrlndesc.set(dm.getField("bnyrlndesc"));
			screenVars.bnypc.set(dm.getField("bnypc"));
			screenVars.cltreln.set(dm.getField("cltreln"));
			screenVars.bnytype.set(dm.getField("bnytype"));
			screenVars.relto.set(dm.getField("relto"));
			screenVars.revcflg.set(dm.getField("revcflg"));
			screenVars.enddateDisp.set(dm.getField("enddateDisp"));/*ILIFE-3581*/
			screenVars.sequence.set(dm.getField("sequence"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6247ScreenVars screenVars = (S6247ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.select.setFieldName("select");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.clntsname.setFieldName("clntsname");
				screenVars.bnyrlndesc.setFieldName("bnyrlndesc");
				screenVars.bnypc.setFieldName("bnypc");
				screenVars.cltreln.setFieldName("cltreln");
				screenVars.bnytype.setFieldName("bnytype");
				screenVars.relto.setFieldName("relto");
				screenVars.revcflg.setFieldName("revcflg");
				screenVars.enddateDisp.setFieldName("enddateDisp");/*ILIFE-3581*/
				screenVars.sequence.setFieldName("sequence");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("clntnum").set(screenVars.clntnum);
			dm.getField("select").set(screenVars.select);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("clntsname").set(screenVars.clntsname);
			dm.getField("bnyrlndesc").set(screenVars.bnyrlndesc);
			dm.getField("bnypc").set(screenVars.bnypc);
			dm.getField("cltreln").set(screenVars.cltreln);
			dm.getField("bnytype").set(screenVars.bnytype);
			dm.getField("relto").set(screenVars.relto);
			dm.getField("revcflg").set(screenVars.revcflg);
			dm.getField("enddateDisp").set(screenVars.enddateDisp);/*ILIFE-3581*/
			dm.getField("sequence").set(screenVars.sequence);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6247screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6247ScreenVars screenVars = (S6247ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.clntnum.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.clntsname.clearFormatting();
		screenVars.bnyrlndesc.clearFormatting();
		screenVars.bnypc.clearFormatting();
		screenVars.cltreln.clearFormatting();
		screenVars.bnytype.clearFormatting();
		screenVars.relto.clearFormatting();
		screenVars.revcflg.clearFormatting();
		screenVars.enddateDisp.clearFormatting();/*ILIFE-3581*/
		screenVars.sequence.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6247ScreenVars screenVars = (S6247ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.select.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.clntsname.setClassString("");
		screenVars.bnyrlndesc.setClassString("");
		screenVars.bnypc.setClassString("");
		screenVars.cltreln.setClassString("");
		screenVars.bnytype.setClassString("");
		screenVars.relto.setClassString("");
		screenVars.revcflg.setClassString("");
		screenVars.enddateDisp.setClassString("");/*ILIFE-3581*/
		screenVars.sequence.setClassString("");
	}

/**
 * Clear all the variables in S6247screensfl
 */
	public static void clear(VarModel pv) {
		S6247ScreenVars screenVars = (S6247ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.clntnum.clear();
		screenVars.select.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.clntsname.clear();
		screenVars.bnyrlndesc.clear();
		screenVars.bnypc.clear();
		screenVars.cltreln.clear();
		screenVars.bnytype.clear();
		screenVars.relto.clear();
		screenVars.revcflg.clear();
		screenVars.enddateDisp.clear();/*ILIFE-3581*/
		screenVars.enddate.clear();
		screenVars.sequence.clear();
	}
}
