package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR50P
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50pScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(552+DD.cltaddr.length*5); //starts ILIFE-3212
	public FixedLengthStringData dataFields = new FixedLengthStringData(232+DD.cltaddr.length*5).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData despaddrs = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(dataFields, 51);
	public FixedLengthStringData[] despaddr = FLSArrayPartOfStructure(5, DD.cltaddr.length, despaddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(despaddrs.length()).isAPartOf(despaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData despaddr01 = DD.cltaddr.copy().isAPartOf(filler,0);
	public FixedLengthStringData despaddr02 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*1);
	public FixedLengthStringData despaddr03 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*2);
	public FixedLengthStringData despaddr04 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*3);
	public FixedLengthStringData despaddr05 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*4);
	public FixedLengthStringData despnum = DD.despnum.copy().isAPartOf(dataFields,51+DD.cltaddr.length*5);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,59+DD.cltaddr.length*5);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,67+DD.cltaddr.length*5);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,107+DD.cltaddr.length*5);
	public FixedLengthStringData longname = DD.longname.copy().isAPartOf(dataFields,115+DD.cltaddr.length*5);
	public FixedLengthStringData postcd = DD.postcd.copy().isAPartOf(dataFields,165+DD.cltaddr.length*5);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,175+DD.cltaddr.length*5);
	public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(dataFields,185+DD.cltaddr.length*5);
	public ZonedDecimalData trandate = DD.trandate.copyToZonedDecimal().isAPartOf(dataFields,189+DD.cltaddr.length*5);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(dataFields,197+DD.cltaddr.length*5);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(dataFields,227+DD.cltaddr.length*5);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 232+DD.cltaddr.length*5); //end ILIFE-3212
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData despaddrsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] despaddrErr = FLSArrayPartOfStructure(5, 4, despaddrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(despaddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData despaddr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData despaddr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData despaddr03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData despaddr04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData despaddr05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData despnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData longnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData postcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData trancdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData trandateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 312+DD.cltaddr.length*5); //ILIFE-3212
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData despaddrsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] despaddrOut = FLSArrayPartOfStructure(5, 12, despaddrsOut, 0);
	public FixedLengthStringData[][] despaddrO = FLSDArrayPartOfArrayStructure(12, 1, despaddrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(despaddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] despaddr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] despaddr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] despaddr03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] despaddr04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] despaddr05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] despnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] longnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] postcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] trancdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] trandateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData trandateDisp = new FixedLengthStringData(10);

	public LongData Sr50pscreenWritten = new LongData(0);
	public LongData Sr50pprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr50pScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {chdrnum, chdrstatus, ctypdesc, cnttype, lifenum, lifedesc, premstatus, tranno, trancd, trandesc, trandate, effdate, despnum, longname, despaddr01, despaddr02, despaddr03, despaddr04, postcd, despaddr05};
		screenOutFields = new BaseData[][] {chdrnumOut, chdrstatusOut, ctypdescOut, cnttypeOut, lifenumOut, lifedescOut, premstatusOut, trannoOut, trancdOut, trandescOut, trandateOut, effdateOut, despnumOut, longnameOut, despaddr01Out, despaddr02Out, despaddr03Out, despaddr04Out, postcdOut, despaddr05Out};
		screenErrFields = new BaseData[] {chdrnumErr, chdrstatusErr, ctypdescErr, cnttypeErr, lifenumErr, lifedescErr, premstatusErr, trannoErr, trancdErr, trandescErr, trandateErr, effdateErr, despnumErr, longnameErr, despaddr01Err, despaddr02Err, despaddr03Err, despaddr04Err, postcdErr, despaddr05Err};
		screenDateFields = new BaseData[] {trandate, effdate};
		screenDateErrFields = new BaseData[] {trandateErr, effdateErr};
		screenDateDispFields = new BaseData[] {trandateDisp, effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr50pscreen.class;
		protectRecord = Sr50pprotect.class;
	}

}
