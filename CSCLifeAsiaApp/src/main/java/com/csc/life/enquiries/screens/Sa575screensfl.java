package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sa575screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 13;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {20, 1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 22, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa575ScreenVars sv = (Sa575ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa575screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa575screensfl, 
			sv.Sa575screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sa575ScreenVars sv = (Sa575ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa575screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sa575ScreenVars sv = (Sa575ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa575screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sa575screensflWritten.gt(0))
		{
			sv.sa575screensfl.setCurrentIndex(0);
			sv.Sa575screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sa575ScreenVars sv = (Sa575ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa575screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa575ScreenVars screenVars = (Sa575ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.dbtcdtdate.setFieldName("dbtcdtdate");
				screenVars.dbtcdtdesc.setFieldName("dbtcdtdesc");
				screenVars.amnt.setFieldName("amnt");
				screenVars.bankcode.setFieldName("bankcode");
				screenVars.aacct.setFieldName("aacct");
				screenVars.statdesc.setFieldName("statdesc");
				screenVars.transcode.setFieldName("transcode");
				screenVars.trcdedesc.setFieldName("trcdedesc");
				screenVars.trdate.setFieldName("trdate");
				screenVars.dbtcdtdateDisp.setFieldName("dbtcdtdateDisp");
				screenVars.trdateDisp.setFieldName("trdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.dbtcdtdate.set(dm.getField("dbtcdtdate"));
			screenVars.dbtcdtdesc.set(dm.getField("dbtcdtdesc"));
			screenVars.amnt.set(dm.getField("amnt"));
			screenVars.bankcode.set(dm.getField("bankcode"));
			screenVars.aacct.set(dm.getField("aacct"));
			screenVars.statdesc.set(dm.getField("statdesc"));
			screenVars.transcode.set(dm.getField("transcode"));
			screenVars.trcdedesc.set(dm.getField("trcdedesc"));
			screenVars.trdate.set(dm.getField("trdate"));
			screenVars.dbtcdtdateDisp.set(dm.getField("dbtcdtdateDisp"));
			screenVars.trdateDisp.set(dm.getField("trdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa575ScreenVars screenVars = (Sa575ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.dbtcdtdate.setFieldName("dbtcdtdate");
				screenVars.dbtcdtdesc.setFieldName("dbtcdtdesc");
				screenVars.amnt.setFieldName("amnt");
				screenVars.bankcode.setFieldName("bankcode");
				screenVars.aacct.setFieldName("aacct");
				screenVars.statdesc.setFieldName("statdesc");
				screenVars.transcode.setFieldName("transcode");
				screenVars.trcdedesc.setFieldName("trcdedesc");
				screenVars.trdate.setFieldName("trdate");
				screenVars.dbtcdtdateDisp.setFieldName("dbtcdtdateDisp");
				screenVars.trdateDisp.setFieldName("trdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("dbtcdtdate").set(screenVars.dbtcdtdate);
			dm.getField("dbtcdtdesc").set(screenVars.dbtcdtdesc);
			dm.getField("amnt").set(screenVars.amnt);
			dm.getField("bankcode").set(screenVars.bankcode);
			dm.getField("aacct").set(screenVars.aacct);
			dm.getField("statdesc").set(screenVars.statdesc);
			dm.getField("transcode").set(screenVars.transcode);
			dm.getField("trcdedesc").set(screenVars.trcdedesc);
			dm.getField("trdate").set(screenVars.trdate);
			dm.getField("dbtcdtdateDisp").set(screenVars.dbtcdtdateDisp);
			dm.getField("trdateDisp").set(screenVars.trdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sa575screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sa575ScreenVars screenVars = (Sa575ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.dbtcdtdate.clearFormatting();
		screenVars.dbtcdtdesc.clearFormatting();
		screenVars.amnt.clearFormatting();
		screenVars.bankcode.clearFormatting();
		screenVars.aacct.clearFormatting();
		screenVars.statdesc.clearFormatting();
		screenVars.transcode.clearFormatting();
		screenVars.trcdedesc.clearFormatting();
		screenVars.trdate.clearFormatting();
		screenVars.dbtcdtdateDisp.clearFormatting();
		screenVars.trdateDisp.clearFormatting();
		
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sa575ScreenVars screenVars = (Sa575ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.dbtcdtdate.setClassString("");
		screenVars.dbtcdtdesc.setClassString("");
		screenVars.amnt.setClassString("");
		screenVars.bankcode.setClassString("");
		screenVars.aacct.setClassString("");
		screenVars.statdesc.setClassString("");
		screenVars.transcode.setClassString("");
		screenVars.trcdedesc.setClassString("");
		screenVars.trdate.setClassString("");
		screenVars.dbtcdtdateDisp.setClassString("");
		screenVars.trdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sa575screensfl
 */
	public static void clear(VarModel pv) {
		Sa575ScreenVars screenVars = (Sa575ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.dbtcdtdate.clear();
		screenVars.dbtcdtdesc.clear();
		screenVars.amnt.clear();
		screenVars.bankcode.clear();
		screenVars.aacct.clear();
		screenVars.statdesc.clear();
		screenVars.transcode.clear();
		screenVars.trcdedesc.clear();
		screenVars.trdate.clear();
		screenVars.dbtcdtdateDisp.clear();
		screenVars.trdateDisp.clear();
	}
}
