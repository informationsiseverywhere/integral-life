package com.csc.life.enquiries.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import  com.csc.life.enquiries.dataaccess.dao.LbonpfDAO;
import com.csc.life.enquiries.dataaccess.model.Lbonpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LbonpfDAOImpl extends   BaseDAOImpl<Lbonpf> implements LbonpfDAO{
	
	  
	  private static final Logger LOGGER = LoggerFactory.getLogger(LbonpfDAOImpl.class);
		 
		 
		 public List<Lbonpf> getLbonpfData(String chdrnum, String chdrcoy) {
			// ---------------------------------
			// Initialize variables
			// ---------------------------------
			Lbonpf lbonpf = null;
			List<Lbonpf> lbonpfReadResult = new LinkedList<Lbonpf>();
			PreparedStatement stmn = null;
			ResultSet rs = null;
			StringBuilder sqlStringBuilder = new StringBuilder();
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sqlStringBuilder.append("SELECT  EFFDATE, TRANNO, TRNAMT, NETPRE, UEXTPC, UALPRC, USRPRF, REVFLAG, TRCDE, CHDRCOY");
			sqlStringBuilder.append(" FROM Lbonpf  ");
			sqlStringBuilder.append(" WHERE CHDRNUM=? AND CHDRCOY=? ORDER BY TRANNO DESC ");
			stmn = getPrepareStatement(sqlStringBuilder.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmn.setString(1, chdrnum);
				stmn.setString(2, chdrcoy);
				
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				rs = executeQuery(stmn);


				while (rs.next()) {
					lbonpf = new Lbonpf();
					lbonpf.setEffdate(rs.getInt(1));
					lbonpf.setTranno(rs.getInt(2));					
					lbonpf.setTrnamt(rs.getBigDecimal(3));
					lbonpf.setNetpre(rs.getInt(4));
					lbonpf.setUextpc(rs.getBigDecimal(5));
					lbonpf.setUalprc(rs.getBigDecimal(6));
					lbonpf.setUsrprf(rs.getString(7));
					lbonpf.setRevflag(rs.getString(8));
					lbonpf.setTrcde(rs.getString(9));
					lbonpf.setChdrcoy(rs.getString(10));
					
					lbonpfReadResult.add(lbonpf);
				}

			} catch (SQLException e) {
				LOGGER.error("getLbonpfData()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(stmn, rs);
			}
			return lbonpfReadResult;
		}


		public Lbonpf getLbonpfRecord(String chdrnum, int tranno, String chdrcoy) {
			
			// ---------------------------------
			// Initialize variables
			// ---------------------------------
			Lbonpf lbonpf = null;
			List<Lbonpf> lbonpfReadResult = new LinkedList<Lbonpf>();
			PreparedStatement stmn = null;
			ResultSet rs = null;
			StringBuilder sqlStringBuilder = new StringBuilder();
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sqlStringBuilder.append("SELECT EFFDATE, TRANNO, TRNAMT, NETPRE, UEXTPC, UALPRC, USRPRF, REVFLAG, TRCDE, CHDRNUM, CHDRCOY");
			sqlStringBuilder.append(" FROM Lbonpf  ");
			sqlStringBuilder.append(" WHERE CHDRNUM=? AND TRANNO=? AND CHDRCOY=?");
			stmn = getPrepareStatement(sqlStringBuilder.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmn.setString(1, chdrnum);
				stmn.setInt(2, tranno);
				stmn.setString(3, chdrcoy);
				
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				rs = executeQuery(stmn);


				while (rs.next()) {
					lbonpf = new Lbonpf();
					lbonpf.setEffdate(rs.getInt(1));
					lbonpf.setTranno(rs.getInt(2));
					lbonpf.setTrnamt(rs.getBigDecimal(3));
					lbonpf.setNetpre(rs.getInt(4));
					lbonpf.setUextpc(rs.getBigDecimal(5));
					lbonpf.setUalprc(rs.getBigDecimal(6));
					lbonpf.setUsrprf(rs.getString(7));
					lbonpf.setRevflag(rs.getString(8));
					lbonpf.setTrcde(rs.getString(9));
					lbonpf.setChdrnum(rs.getString(10));
					lbonpf.setChdrcoy(rs.getString(11));
					break;
				}

			} catch (SQLException e) {
				LOGGER.error("getLbonpfRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(stmn, rs);
			}
			return lbonpf;
		}


		public void updateLbonpfRecord(Lbonpf lbonpf) {
			// ---------------------------------
			// Initialize variables
			// ---------------------------------
			PreparedStatement stmn = null;
			ResultSet rs = null;
			StringBuilder sqlStringBuilder = new StringBuilder();
			int i = 0;
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sqlStringBuilder.append("UPDATE Lbonpf set REVFLAG= ?,  TRNAMT= ?, UALPRC= ?");
			sqlStringBuilder.append(" WHERE CHDRNUM=? AND TRANNO=? AND CHDRCOY=?");
			stmn = getPrepareStatement(sqlStringBuilder.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmn.setString(1, lbonpf.getRevflag());
				stmn.setBigDecimal(2, lbonpf.getTrnamt());
				stmn.setBigDecimal(3, lbonpf.getUalprc());
				stmn.setString(4, lbonpf.getChdrnum());
				stmn.setInt(5, lbonpf.getTranno());
				stmn.setString(6, lbonpf.getChdrcoy());
				
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				i = executeUpdate(stmn);

			} catch (SQLException e) {
				LOGGER.error("updateLbonpfRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(stmn, rs);
			}
		}
public void insertLBonPFValidRecord(List<Lbonpf> lbonpfBulkOpList) {
		        if (lbonpfBulkOpList != null && !lbonpfBulkOpList.isEmpty()) {
		           
		            String SQL_INSERT = "INSERT INTO LBONPF( [CHDRCOY] , [CHDRNUM] ,  [LIFE] ,  [COVERAGE] ,  [RIDER] ,  [PLANSFX] ,  [EFFDATE],  [TRANNO] ,   [TRCDE] ,  [TRNAMT] ,  [NETPRE], [UEXTPC], [UALPRC],[REVFLAG],[USRPRF],[JOBNM],[DATIME] ) "
		            		+ " Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		       
		            PreparedStatement psChdrInsert = getPrepareStatement(SQL_INSERT);
		            try {
		                for (Lbonpf c : lbonpfBulkOpList) {
		                    psChdrInsert.setString(1, c.getChdrcoy());
		                    psChdrInsert.setString(2, c.getChdrnum());
		                    psChdrInsert.setString(3, c.getLife());
		                    psChdrInsert.setString(4, c.getCoverage());		                 
		                    psChdrInsert.setString(5, c.getRider());
		                    psChdrInsert.setString(6, c.getPlnsfx());
		                    psChdrInsert.setInt(7, c.getEffdate());
							psChdrInsert.setLong(8, c.getTranno());					
		                    psChdrInsert.setString(9, c.getTrcde());
		                    psChdrInsert.setBigDecimal(10, c.getTrnamt());
		                    psChdrInsert.setInt(11, c.getNetpre());
		                    psChdrInsert.setBigDecimal(12, c.getUextpc());
		                    psChdrInsert.setBigDecimal(13, c.getUalprc());
		                    psChdrInsert.setString(14, c.getRevflag());
		                    psChdrInsert.setString(15 ,getUsrprf());
		                    psChdrInsert.setString(16, getJobnm());
		                    psChdrInsert.setTimestamp(17, new Timestamp(System.currentTimeMillis()));
		                    psChdrInsert.addBatch();
		                }
		                psChdrInsert.executeBatch();
		            } catch (SQLException e) {
		                LOGGER.error("insertLBonPFValidRecord()", e);//IJTI-1561
		                throw new SQLRuntimeException(e);
		            } finally {
		                close(psChdrInsert, null);
		            }
		        }
		    }

}
