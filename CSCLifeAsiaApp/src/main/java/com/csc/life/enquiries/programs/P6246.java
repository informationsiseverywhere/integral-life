/*
 * File: P6246.java
 * Date: 30 August 2009 0:39:04
 * Author: Quipoz Limited
 * 
 * Class transformed from P6246.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.dataaccess.dao.ItdmpfDAO;
import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.life.enquiries.screens.S6246ScreenVars;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;  
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* Initialise
* ----------
*
*
*     The  details  of  the  contract  being  enquired upon will be
*     stored  in  the CHDRENQ I/O module.  Retrieve the details and
*     set up the header portion of the screen as follows:
*
*     Look up the following descriptions and names:
*
*          The owner's client (CLTS) details.
*
*          The joint owner's client (CLTS) details if they exist.
*
*     Perform a RETRV  on  COVRENQ.  This  will  provide  the  COVR
*     record that is being enquired upon. At this point the program
*     must work out whether it is to provide a Plan Level or Policy
*     Level  enquiry.  This  can  be  done  by  checking  the field
*     WSSP-PLAN-POLICY-FLAG.  If it is 'L', then Plan Level Enquiry
*     is  required,  if  it  is  'O',  then Policy Level Enquiry is
*     required.  For Plan Level Enquiry all the COVRENQ records for
*     the given Life,  Coverage  and Rider must be read and all the
*     Amount fields added  together  so  a total for the whole Plan
*     may be  displayed.  For  Policy Level Enquiry processing will
*     be  different  depending  on  whether  a  summary  record  or
*     non-summary  record  is being displayed. Normally the program
*     could determine  whether  or  not  a summary record was being
*     displayed by  checking  the  Plan Suffix. If this was '0000',
*     then it  was  a  summary  record.  However,  for Policy Level
*     Enquiry when a  summary  record is held in the I/O module for
*     processing the  previous  function  will replace the value of
*     '0000' in  the  Plan Suffix with the calculated suffix number
*     of the  actual  policy  that  the user selected. Therefore it
*     will be necessary to check the Plan Suffix against the number
*     of  policies summarised, (CHDRENQ-POLSUM). If the Plan Suffix
*     is not  greater  the  CHDRENQ-POLSUM  then  it  is  a summary
*     record.
*
*     For  a  non-summary  record,  (Plan Suffix > CHDRENQ-POLSUM),
*     then display amounts as found.
*
*     For  a  summary  record,  (Plan Suffix not > CHDRENQ-POLSUM),
*     then  the  values  held on the COVRENQ record will be for all
*     the  policies summarised, and therefore a calculation must be
*     performed  to  arrive  at the value for one of the summarised
*     policies.  For  all  summarised policies execpt the first one
*     simply divide the total by the number of policies summarised,
*     (CHDRENQ-POLSUM)  and  display the result. This may give rise
*     to  a  rounding  discrepancy. This discrepancy is absorbed by
*     the  first policy summarised, the notional policy number one.
*     If  this  policy  has  been  selected for display perform the
*     following calculation:
*
*     Value To Display = AMOUNT - (AMOUNT * (POLSUM - 1))
*                                           ~~~~~~~~~~~          *                                             POLSUM
*
*     For example if  the amount is $100 and the number of policies
*     summarised is 3,  the  amount for notional policies #2 and #3
*     will be calculated  and  displayed  as  $33. For policy #1 it
*     will be:
*
*                        100 - (100 * (3 - 1)
*                                      ~~~~~~                    *                                        3
*
*                     =  100 - (100 * 2/3)
*
*                     =  $34
*
*     Perform  a  READR  on  ULNKUNL and display the details on the
*     screen.
*
* Validation
* ----------
*
*     There is no validation in this program.
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Add 1 to the prorgam pointer and exit.
*
*
*
*****************************************************************
* </pre>
*/
public class P6246 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6246");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private ZonedDecimalData wsaaNofRead = new ZonedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaEofFlag = new FixedLengthStringData(1);
	private Validator wsaaEof = new Validator(wsaaEofFlag, "Y");

	private FixedLengthStringData wsaaNoMoreInsertFlag = new FixedLengthStringData(1);
	private Validator wsaaNoMoreInsert = new Validator(wsaaNoMoreInsertFlag, "Y");

	private FixedLengthStringData wsaaActionOk = new FixedLengthStringData(1);
	private Validator actionOk = new Validator(wsaaActionOk, "Y");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
		/* TABLES */
	private String t2240 = "T2240";
	private String t5515 = "T5515";
	private String itemrec = "ITEMREC";
	private String ulnkrec = "ULNKREC";
	private String unltunlrec = "UNLTUNLREC";
		/*Contract Enquiry - Contract Header.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	
		/*Client logical file with new fields*/
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private List<Clntpf> clntpfList = null;
		/*Contract Enquiry - Coverage Details.*/
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrenqpf = new Covrpf();
		/*Table items, date - maintenance view*/
	private Itdmpf itdmpf = new Itdmpf();
	private ItdmpfDAO itdmpfDAO = getApplicationContext().getBean("itdmpfDAO", ItdmpfDAO.class);
	private List<Itdmpf> itdmpfList = null;		
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details - Contract Enquiry.*/
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	private T2240rec t2240rec = new T2240rec();
	private T5515rec t5515rec = new T5515rec();
		/*Unit Linked Fund Direction.*/
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
		/*Unit Linked transaction details*/
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S6246ScreenVars sv = ScreenProgram.getScreenVars( S6246ScreenVars.class);
	private ZswrpfDAO  zswrDAO =  getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);
	private DescDAO descdao =new DescDAOImpl();/*ILIFE-4036*/
	Descpf descpf=null;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		next1240, 
		exit1290, 
		next1440, 
		exit1490, 
		preExit, 
		exit2090
	}

	public P6246() {
		super();
		screenVars = sv;
		new ScreenModel("S6246", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname(clntpf);
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

protected void plainname(Clntpf clntpf)
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname(clntpf);
			goTo(GotoLabel.plainExit);
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname(clntpf);
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(clntpf.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(clntpf.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(clntpf.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retreiveHeader1030();
		endPolicy1020();
		initialStack1050();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
	}

protected void retreiveHeader1030()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf==null)
		{
			return;
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		//ILIFE-4036 started
		sv.cnttype.set(chdrpf.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		 descpf= descdao.getdescData("IT", "T5688", sv.cnttype.toString(), chdrpf.getChdrcoy().toString(),wsspcomn.language.toString());
				
				if (descpf==null) {
					sv.ctypedes.fill("?");
				}
				else {
					sv.ctypedes.set(descpf.getLongdesc());
				}
				//ILIFE-4036 ended
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t2240);
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t2240);
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			/* MTL002 */
			}
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());
		lifepf.setChdrnum(chdrpf.getChdrnum());
		lifepf.setLife("01");
		lifepf.setJlife("00");
		lifepf.setValidflag("1");
		Map<String, Lifepf> lifepfMap =lifepfDAO.getLifeEnqData(lifepf);
		Lifepf lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim()+""+lifepf.getValidflag().trim());
		if(lifepfModel==null){
			return;
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		sv.lifenum.set(lifepfModel.getLifcnum());
		clntpf.setClntnum(lifepfModel.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpfList= clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
		{
			return;
		}
		for(Clntpf clntpf :clntpfList)
		{
			plainname(clntpf);
		}
		sv.linsname.set(wsspcomn.longconfname);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		lifepf.setJlife("01");
		lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim());
		if(lifepfModel!=null)
			{
			clntpf.setClntnum(lifepfModel.getLifcnum());
			clntpf.setClntcoy(wsspcomn.fsuco.toString());
			clntpf.setClntpfx("CN");
			clntpfList= clntpfDAO.readClientpfData(clntpf);
				if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
				{
					return;
				}
				else{
					clntpf = clntpfList.get(0);
					plainname(clntpf);
					sv.jlinsname.set(wsspcomn.longconfname);
			  }
			}
			
		
		wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrenqpf = covrpfDAO.getCacheObject(covrenqpf);
		sv.crtable.set(covrenqpf.getCrtable());
		if ((isGT(covrenqpf.getPlanSuffix(),chdrpf.getPolsum()))
		|| (isEQ(chdrpf.getPolinc(),1))) {
			wsaaSingp.set(covrenqpf.getInstprem());
		}
		else {
			compute(wsaaSingp, 2).set(div(covrenqpf.getInstprem(),chdrpf.getPolsum()));
		}
		
		List<Zswrpf> zswrpfiList=new ArrayList<>();
        Zswrpf zw=new Zswrpf();
        zw.setChdrpfx("CH");
        zw.setChdrcoy(chdrpf.getChdrcoy().toString());
        zw.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */ 
        zw.setValidflag("1" );
		zswrpfiList = zswrDAO.getZswrpfData(zw.getChdrnum(), zw.getChdrcoy(), zw.getChdrpfx(),
				zw.getValidflag());/* IJTI-1523 */       

        if (zswrpfiList.size()>0 )
        {
               for (Zswrpf p : zswrpfiList) 
                {                    
                sv.zafropt1.set(p.getZafropt());
                sv.zafritem.set(p.getZafritem());             
               }
        }
        else
         {
        	  sv.zafropt1.set("NO");
              sv.zafritem.set("NO");
          }
                     
}
protected void endPolicy1020()
	{
		sv.anbAtCcd.set(covrenqpf.getAnbAtCcd());
		sv.numapp.set(chdrpf.getPolinc());
		sv.rider.set(covrenqpf.getRider());
		sv.statSect.set(covrenqpf.getStatSect());
		sv.stsubsect.set(covrenqpf.getStatSubsect());
		sv.instprem.set(wsaaSingp);
		sv.life.set(covrenqpf.getLife());
		if (isNE(covrenqpf.getLife(),lifepf.getLife())) {
			getLifeName4100();
		}
		sv.coverage.set(covrenqpf.getCoverage());
	}

protected void initialStack1050()
	{
		loadStack1200();
		/*EXIT*/
	}

protected void loadStack1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1200();
					read1230();
				}
				case next1240: {
					next1240();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1200()
	{
		wsaaNofRead.set(0);
		ulnkIO.setDataArea(SPACES);
		ulnkIO.setChdrcoy(covrenqpf.getChdrcoy());
		ulnkIO.setChdrnum(covrenqpf.getChdrnum());
		ulnkIO.setLife(covrenqpf.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setRider(covrenqpf.getRider());
		ulnkIO.setCoverage(covrenqpf.getCoverage());
		if (isGT(covrenqpf.getPlanSuffix(),chdrpf.getPolsum())) {
			ulnkIO.setPlanSuffix(covrenqpf.getPlanSuffix());
		}
		else {
			if (isEQ(chdrpf.getPolsum(),1)) {
				ulnkIO.setPlanSuffix(covrenqpf.getPlanSuffix());
			}
			else {
				ulnkIO.setPlanSuffix(ZERO);
			}
		}
		ulnkIO.setFunction(varcom.readr);
	}

protected void read1230()
	{
		callUlnkio5000();
		if (isNE(ulnkIO.getStatuz(),varcom.oK)
		&& isNE(ulnkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
		if (isEQ(ulnkIO.getStatuz(),varcom.endp)) {
			readUnltuln1400();
			goTo(GotoLabel.exit1290);
		}
		
	}

protected void next1240()
	{
	    sv.virtFundSplitMethod.set(ulnkIO.getFndSpl());/*ILIFE-4036*/
		sv.percOrAmntInd.set(ulnkIO.getPercOrAmntInd());
		wsaaNofRead.add(1);
		if (isGT(wsaaNofRead,10)) {
			goTo(GotoLabel.exit1290);
		}
		if (isEQ(ulnkIO.getUalfnd(wsaaNofRead),SPACES)) {
			sv.unitVirtualFund[wsaaNofRead.toInt()].set(SPACES);
			sv.currcy[wsaaNofRead.toInt()].set(SPACES);
			sv.unitAllocPercAmt[wsaaNofRead.toInt()].set(ZERO);
			sv.unitBidPrice[wsaaNofRead.toInt()].set(ZERO);
		}
		else {
			sv.unitVirtualFund[wsaaNofRead.toInt()].set(ulnkIO.getUalfnd(wsaaNofRead));
			getT55151300();
			sv.currcy[wsaaNofRead.toInt()].set(t5515rec.currcode);
			sv.unitAllocPercAmt[wsaaNofRead.toInt()].set(ulnkIO.getUalprc(wsaaNofRead));
			sv.unitBidPrice[wsaaNofRead.toInt()].set(ulnkIO.getUspcpr(wsaaNofRead));
		}
		goTo(GotoLabel.next1240);
	}

protected void getT55151300()
	{
		para1300();
	}

protected void para1300()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		itdmpf.setItempfx("IT");
		itdmpf.setItemcoy(chdrpf.getChdrcoy().toString());
		itdmpf.setItemtabl(t5515);
		if(isEQ(ulnkIO.getStatuz(),varcom.oK)){
			itdmpf.setItemitem(ulnkIO.getUalfnd(wsaaNofRead).toString());
		}
		else {
			itdmpf.setItemitem(unltunlIO.getUalfnd(wsaaNofRead).toString());
		}
		itdmpf.setItmfrm(chdrpf.getOccdate());
		itdmpfList = itdmpfDAO.readItdmData(itdmpf);
		if(itdmpfList ==null || (itdmpfList!=null && itdmpfList.size()==0))
			{
				return;
			}
		else{
			itdmpf = itdmpfList.get(0);
			t5515rec.t5515Rec.set(itdmpf.getGenarea());
		}
	}

protected void readUnltuln1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1400();
					read1430();
				}
				case next1440: {
					next1440();
				}
				case exit1490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1400()
	{
		wsaaNofRead.set(0);
		unltunlIO.setChdrcoy(covrenqpf.getChdrcoy());
		unltunlIO.setChdrnum(covrenqpf.getChdrnum());
		unltunlIO.setLife(covrenqpf.getLife());
		unltunlIO.setRider(covrenqpf.getRider());
		unltunlIO.setCoverage(covrenqpf.getCoverage());
		unltunlIO.setSeqnbr(0);
		unltunlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		unltunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		unltunlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
}

protected void read1430()
	{
		unltunlIO.setFormat(unltunlrec);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(),varcom.oK)
		&& isNE(unltunlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		if (isEQ(unltunlIO.getStatuz(),varcom.endp) 
				|| isNE(covrenqpf.getChdrcoy(),unltunlIO.getChdrcoy())
				|| isNE(covrenqpf.getChdrnum(),unltunlIO.getChdrnum())
				|| isNE(covrenqpf.getRider(),unltunlIO.getRider())
				|| isNE(covrenqpf.getCoverage(),unltunlIO.getCoverage())
				|| isNE(covrenqpf.getLife(),unltunlIO.getLife())) {
					unltunlIO.setStatuz(varcom.endp);
					goTo(GotoLabel.exit1490);
				}
	}

protected void next1440()
	{
		sv.percOrAmntInd.set(unltunlIO.getPercOrAmntInd());
		sv.virtFundSplitMethod.set(unltunlIO.getFndSpl());/*ILIFE-4036*/
		wsaaNofRead.add(1);
		if (isGT(wsaaNofRead,10)) {
			goTo(GotoLabel.exit1490);
		}
		if (isEQ(unltunlIO.getUalfnd(wsaaNofRead),SPACES)) {
			sv.unitVirtualFund[wsaaNofRead.toInt()].set(SPACES);
			sv.currcy[wsaaNofRead.toInt()].set(SPACES);
			sv.unitAllocPercAmt[wsaaNofRead.toInt()].set(ZERO);
			sv.unitBidPrice[wsaaNofRead.toInt()].set(ZERO);
		}
		else {
			sv.unitVirtualFund[wsaaNofRead.toInt()].set(unltunlIO.getUalfnd(wsaaNofRead));
			getT55151300();
			sv.currcy[wsaaNofRead.toInt()].set(t5515rec.currcode);
			sv.unitAllocPercAmt[wsaaNofRead.toInt()].set(unltunlIO.getUalprc(wsaaNofRead));
			sv.unitBidPrice[wsaaNofRead.toInt()].set(unltunlIO.getUspcpr(wsaaNofRead));
		}
		goTo(GotoLabel.next1440);
	}

protected void callUlnkio5000()
	{
		/*PARA*/
		ulnkIO.setFormat(ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)
		&& isNE(ulnkIO.getStatuz(),varcom.mrnf)
		&& isNE(ulnkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
		if (isEQ(ulnkIO.getStatuz(),varcom.mrnf)) {
			ulnkIO.setStatuz(varcom.endp);
		}
		if ((isNE(ulnkIO.getChdrcoy(),chdrpf.getChdrcoy()))
				|| (isNE(ulnkIO.getChdrnum(),chdrpf.getChdrnum()))) {
					ulnkIO.setStatuz(varcom.endp);
				}
		/*EXIT*/
	}

protected void callScreenIo5100()
	{
		/*PARA*/
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit2090);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void getLifeName4100()
	{
		para4100();
	}

protected void para4100()
	{
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	lifepf.setChdrcoy(covrenqpf.getChdrcoy());/* IJTI-1523 */
	lifepf.setChdrnum(covrenqpf.getChdrnum());/* IJTI-1523 */
	lifepf.setLife(covrenqpf.getLife());/* IJTI-1523 */
	lifepf.setJlife("00");
	lifepf.setValidflag("1");
	Map<String, Lifepf> lifepfMap =lifepfDAO.getLifeEnqData(lifepf);
	Lifepf lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim()+""+lifepf.getValidflag().trim());
	if(lifepfModel==null){
		return;
	}	
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	sv.lifenum.set(lifepfModel.getLifcnum());
	clntpf.setClntnum(lifepfModel.getLifcnum());
	clntpf.setClntcoy(wsspcomn.fsuco.toString());
	clntpf.setClntpfx("CN");
	clntpfList= clntpfDAO.readClientpfData(clntpf);
	if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
	{
		return;
	}
	for(Clntpf clntpf :clntpfList)
	{
		plainname(clntpf);
	}
	sv.linsname.set(wsspcomn.longconfname);
}
}
