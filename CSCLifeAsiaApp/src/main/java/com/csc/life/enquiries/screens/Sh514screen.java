package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh514screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh514ScreenVars sv = (Sh514ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh514screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh514ScreenVars screenVars = (Sh514ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.singlePremium.setClassString("");
		screenVars.premcessDisp.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.annivProcDateDisp.setClassString("");
		screenVars.unitStatementDateDisp.setClassString("");
		screenVars.bonusInd.setClassString("");
		screenVars.rerateDateDisp.setClassString("");
		screenVars.rerateFromDateDisp.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.optextind.setClassString("");
		screenVars.zdivopt.setClassString("");
		screenVars.paymth.setClassString("");
		screenVars.paycurr.setClassString("");
		screenVars.bappmeth.setClassString("");
		screenVars.payclt.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankdesc.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.branchdesc.setClassString("");
		screenVars.pbind.setClassString("");
		screenVars.puaInd.setClassString("");
		screenVars.divdInd.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.taxind.setClassString("");
		/*BRD-306 STARTS*/
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		screenVars.zbinstprem.setClassString("");
		screenVars.riskCessAge.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.zlinstprem.setClassString("");
		/*BRD-306 END*/
		screenVars.prmbasis.setClassString("");
		screenVars.aepaydet.setClassString("");
		screenVars.dialdownoption.setClassString("");//BRD-NBP-011
	}

/**
 * Clear all the variables in Sh514screen
 */
	public static void clear(VarModel pv) {
		Sh514ScreenVars screenVars = (Sh514ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.numpols.clear();
		screenVars.planSuffix.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.liencd.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.sumin.clear();
		screenVars.sumins.clear();
		screenVars.singlePremium.clear();
		screenVars.premcessDisp.clear();
		screenVars.premcess.clear();
		screenVars.instPrem.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.annivProcDateDisp.clear();
		screenVars.annivProcDate.clear();
		screenVars.unitStatementDateDisp.clear();
		screenVars.unitStatementDate.clear();
		screenVars.bonusInd.clear();
		screenVars.rerateDateDisp.clear();
		screenVars.rerateDate.clear();
		screenVars.rerateFromDateDisp.clear();
		screenVars.rerateFromDate.clear();
		screenVars.mortcls.clear();
		screenVars.optextind.clear();
		screenVars.zdivopt.clear();
		screenVars.paymth.clear();
		screenVars.paycurr.clear();
		screenVars.bappmeth.clear();
		screenVars.payclt.clear();
		screenVars.payorname.clear();
		screenVars.bankkey.clear();
		screenVars.bankdesc.clear();
		screenVars.bankacckey.clear();
		screenVars.branchdesc.clear();
		screenVars.pbind.clear();
		screenVars.puaInd.clear();
		screenVars.divdInd.clear();
		screenVars.taxamt.clear();
		screenVars.taxind.clear();
		/*BRD-306 STARTS*/
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		screenVars.zbinstprem.clear();
		screenVars.riskCessAge.clear();
		screenVars.riskCessTerm.clear();
		screenVars.riskCessDate.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		screenVars.zlinstprem.clear();
		/*BRD-306 ENDS*/
		screenVars.prmbasis.clear();
		screenVars.dialdownoption.clear(); //BRD-NBP-011
		screenVars.aepaydet.clear();
	}
}
