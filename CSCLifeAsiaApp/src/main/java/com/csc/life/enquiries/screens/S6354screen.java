package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6354screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6354ScreenVars sv = (S6354ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6354screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6354ScreenVars screenVars = (S6354ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.mandref.setClassString("");
		screenVars.facthous.setClassString("");
		screenVars.fcthsedsc.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankdesc.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.bankaccdsc.setClassString("");
		screenVars.bnkcurr.setClassString("");
		screenVars.despnum.setClassString("");
		screenVars.despname.setClassString("");
		screenVars.despaddr01.setClassString("");
		screenVars.despaddr02.setClassString("");
		screenVars.despaddr03.setClassString("");
		screenVars.despaddr04.setClassString("");
		screenVars.despaddr05.setClassString("");
		screenVars.desppcode.setClassString("");
		screenVars.conben.setClassString("");
		screenVars.asgnind.setClassString("");
		screenVars.prmdetails.setClassString("");
		screenVars.hlndetails.setClassString("");
		screenVars.mandstat.setClassString("");
		screenVars.manstatdsc.setClassString("");
		screenVars.grpind.setClassString("");
		screenVars.ctrsind.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.loyaltyind.setClassString("");
//ILIFE-2472-BEGIN
		screenVars.zmandref.setClassString("");
		screenVars.bankaccdsc02.setClassString("");
		screenVars.bankacckey02.setClassString("");
		screenVars.bankdesc02.setClassString("");
		screenVars.bankkey02.setClassString("");
		screenVars.bnkcurr02.setClassString("");
		screenVars.mandstat02.setClassString("");
		screenVars.manstatdsc02.setClassString("");
		screenVars.fcthsedsc02.setClassString("");
		
		screenVars.payrnum.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.payor.setClassString("");
		screenVars.payrname.setClassString("");
//ILIFE-2472-END		
	}

/**
 * Clear all the variables in S6354screen
 */
	public static void clear(VarModel pv) {
		S6354ScreenVars screenVars = (S6354ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.mandref.clear();
		screenVars.facthous.clear();
		screenVars.fcthsedsc.clear();
		screenVars.bankkey.clear();
		screenVars.bankdesc.clear();
		screenVars.bankacckey.clear();
		screenVars.bankaccdsc.clear();
		screenVars.bnkcurr.clear();
		screenVars.despnum.clear();
		screenVars.despname.clear();
		screenVars.despaddr01.clear();
		screenVars.despaddr02.clear();
		screenVars.despaddr03.clear();
		screenVars.despaddr04.clear();
		screenVars.despaddr05.clear();
		screenVars.desppcode.clear();
		screenVars.conben.clear();
		screenVars.asgnind.clear();
		screenVars.prmdetails.clear();
		screenVars.hlndetails.clear();
		screenVars.mandstat.clear();
		screenVars.manstatdsc.clear();
		screenVars.grpind.clear();
		screenVars.ctrsind.clear();
		screenVars.ind.clear();
		screenVars.loyaltyind.clear();
//ILIFE-2472-BEGIN
		screenVars.zmandref.clear();
		screenVars.bankaccdsc02.clear();
		screenVars.bankacckey02.clear();
		screenVars.bankdesc02.clear();
		screenVars.bankkey02.clear();
		screenVars.bnkcurr02.clear();
		screenVars.mandstat02.clear();
		screenVars.manstatdsc02.clear();
		screenVars.fcthsedsc02.clear();
		
		screenVars.payrnum.clear();
		screenVars.payorname.clear();
		screenVars.payor.clear();
		screenVars.payrname.clear();
//ILIFE-2472-END		
	}
}
