/*
 * File: P6248.java
 * Date: 30 August 2009 0:39:16
 * Author: Quipoz Limited
 * 
 * Class transformed from P6248.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S6248ScreenVars;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* Initialise
* ----------
*  Skip this  section  if  returning  from an optional selection
*  (current stack position action flag = '*').
*  The details of  the  contract  being  enquired  upon  will be
*  stored in the  CHDRENQ  I/O module.  Retrieve the details and
*  set up the header portion of the screen as follows:
*  Look up the following descriptions and names:
*  Contract Type, (CNTTYPE) - long description from T5688,
*  Contract  Status,  (STATCODE)  -  short description from
*  T3623,
*  Premium  Status,  (PSTATCODE)  -  short description from
*  T3588,
*  Servicing branch, - long description from T1692,
*  The owner's client (CLTS) details.
*  The joint  owner's  client (CLTS) details if they exist.
*  Perform a RETRV  on  COVRENQ.  This  will  provide  the  COVR
*  record that is  being  enquired upon and also the key for the
*  LEXT data-set.
*  The subfile is  a fixed size of 8 records. Initialse it first
*  and then perform a  BEGN  on  the LEXT data-set with a key of
*  Company Number, Contract Number, Life Number, Coverage Number
*  and Rider number and the Sequence Number set to zero.
*  Read all the  LEXT  records  until  any  portion  of the key,
*  (apart from  the  Sequence Number), changes.  Display all the
*  details on the subfile.  Obtina the short description for the
*  reason code from T5651.
* Validation
* ----------
*  a  There is no validation in this program.
* Updating
* --------
*  a  There is no updating in this program.
* Next Program
* ------------
*  a  Add 1 to the prorgam pointer and exit.
*  a  Tables Used:
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5651 - Coverage/Rider Options/Extras     Key: Option/Extra Code
* T5687 - General Coverage/Rider Details    Key: CRTABLE
* T5688 - Contract Structure                Key: CNTTYPE
*****************************************************************
* </pre>
*/
public class P6248 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6248");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private int wsaaStackPointer = 0;
	protected ZonedDecimalData wsaaIndex = new ZonedDecimalData(5, 0).setUnsigned();
	protected ZonedDecimalData wsaaNofRead = new ZonedDecimalData(4, 0).setUnsigned();
	private int wsaaStackRecAllow = 0;
	private int wsaaLastSeqNo = 0;
	private int wsaaSubfileSize = 8;
	protected int wsaaDatabaseSize = 8;

	private FixedLengthStringData wsaaEofFlag = new FixedLengthStringData(1);
	private Validator wsaaEof = new Validator(wsaaEofFlag, "Y");

	private FixedLengthStringData wsaaNoMoreInsertFlag = new FixedLengthStringData(1);
	private Validator wsaaNoMoreInsert = new Validator(wsaaNoMoreInsertFlag, "Y");

	private FixedLengthStringData wsaaActionOk = new FixedLengthStringData(1);
	private Validator actionOk = new Validator(wsaaActionOk, "Y");

	protected FixedLengthStringData wsbbStackArray = new FixedLengthStringData(336);
	protected FixedLengthStringData[] wsbbOpcda = FLSArrayPartOfStructure(8, 2, wsbbStackArray, 0);
	protected FixedLengthStringData[] wsbbDesc = FLSArrayPartOfStructure(8, 10, wsbbStackArray, 16);
	protected PackedDecimalData[] wsbbAgerate = PDArrayPartOfStructure(8, 3, 0, wsbbStackArray, 96);
	protected PackedDecimalData[] wsbbOppc = PDArrayPartOfStructure(8, 5, 2, wsbbStackArray, 112);
	protected PackedDecimalData[] wsbbInsprm = PDArrayPartOfStructure(8, 6, 0, wsbbStackArray, 136);
	protected ZonedDecimalData[] wsbbEcestrm = ZDArrayPartOfStructure(8, 3, 0, wsbbStackArray, 168);
	protected FixedLengthStringData[] wsbbJlife = FLSArrayPartOfStructure(8, 2, wsbbStackArray, 192);
	protected ZonedDecimalData[] wsbbSeqnbr = ZDArrayPartOfStructure(8, 3, 0, wsbbStackArray, 208, UNSIGNED_TRUE);
	protected FixedLengthStringData[] wsbbReasind = FLSArrayPartOfStructure(8, 1, wsbbStackArray, 232);
	protected PackedDecimalData[] wsbbZnadjperc = PDArrayPartOfStructure(8, 5, 2, wsbbStackArray, 240);
	protected PackedDecimalData[] wsbbZmortpct = PDArrayPartOfStructure(8, 3, 0, wsbbStackArray, 264, UNSIGNED_TRUE);
	protected PackedDecimalData[] wsbbPremadj = PDArrayPartOfStructure(8, 6, 0, wsbbStackArray, 288);
	protected String t5651 = "T5651";
	protected String lextrec = "LEXTREC";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
		/*Contract Enquiry - Contract Header.*/
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrpf = new Covrpf();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Enquiry - Coverage Details.*/
		/*Logical File: Extra data screen*/
	protected DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Life Options/Extras logical file*/
	protected LextTableDAM lextIO = new LextTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	//private S6248ScreenVars sv = ScreenProgram.getScreenVars( S6248ScreenVars.class);
	private S6248ScreenVars sv =   getLScreenVars();


	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		next1220, 
		read1230, 
		exit1290, 
		exit5090, 
		preExit, 
		exit2090
	}

	public P6248() {
		super();
		screenVars = sv;
		new ScreenModel("S6248", AppVars.getInstance(), sv);
	}
	protected S6248ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6248ScreenVars.class);
	}
	
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retreiveHeader1030();
		initialStack1050();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.znadjperc.set(ZERO);
		sv.zmortpct.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6248", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retreiveHeader1030()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		sv.smoking01.set(lifeenqIO.getSmoking());
		sv.occup01.set(lifeenqIO.getOccup());
		sv.pursuit01.set(lifeenqIO.getPursuit01());
		sv.pursuit02.set(lifeenqIO.getPursuit02());
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.smoking02.set(lifeenqIO.getSmoking());
			sv.occup02.set(lifeenqIO.getOccup());
			sv.pursuit03.set(lifeenqIO.getPursuit01());
			sv.pursuit04.set(lifeenqIO.getPursuit02());
			sv.jlifcnum.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
			}
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrpf = covrDao.getCacheObject(covrpf);
		sv.crtable.set(covrpf.getCrtable());
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
	}

protected void initialStack1050()
	{
		loadStack1200();
		/*LOAD-SUBFILE-FIELDS*/
		moveStackToScreen1400();
		/*EXIT*/
	}

protected void loadStack1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1200();
				}
				case next1220: {
					next1220();
				}
				case read1230: {
					read1230();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1200()
	{
		wsbbStackArray.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaDatabaseSize)); wsaaIndex.add(1)){
			initialZero1300();
		}
		wsaaNofRead.set(0);
		lextIO.setChdrcoy(covrpf.getChdrcoy());
		lextIO.setChdrnum(covrpf.getChdrnum());
		lextIO.setLife(covrpf.getLife());
		lextIO.setRider(covrpf.getRider());
		lextIO.setCoverage(covrpf.getCoverage());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFunction(varcom.begn);
		callLextio5000();
		goTo(GotoLabel.read1230);
	}

protected void next1220()
	{
		setPrecision(lextIO.getSeqnbr(), 0);
		lextIO.setSeqnbr(add(lextIO.getSeqnbr(),1));
		lextIO.setFunction(varcom.nextr);
		callLextio5000();
	}

protected void read1230()
	{
	if (isEQ(lextIO.getStatuz(),varcom.endp)
			/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
			|| isNE(lextIO.getChdrcoy(),covrpf.getChdrcoy())
			|| isNE(lextIO.getChdrnum(),covrpf.getChdrnum())
			|| isNE(lextIO.getLife(),covrpf.getLife())
			|| isNE(lextIO.getCoverage(),covrpf.getCoverage())
			|| isNE(lextIO.getRider(),covrpf.getRider())) {		
				goTo(GotoLabel.exit1290);
			}
		wsaaNofRead.add(1);
		wsbbOpcda[wsaaNofRead.toInt()].set(lextIO.getOpcda());
		wsbbAgerate[wsaaNofRead.toInt()].set(lextIO.getAgerate());
		wsbbOppc[wsaaNofRead.toInt()].set(lextIO.getOppc());
		wsbbInsprm[wsaaNofRead.toInt()].set(lextIO.getInsprm());
		wsbbPremadj[wsaaNofRead.toInt()].set(lextIO.getPremadj());
		wsbbEcestrm[wsaaNofRead.toInt()].set(lextIO.getExtCessTerm());
		wsbbSeqnbr[wsaaNofRead.toInt()].set(lextIO.getSeqnbr());
		wsbbJlife[wsaaNofRead.toInt()].set(lextIO.getJlife());
		wsbbReasind[wsaaNofRead.toInt()].set(lextIO.getReasind());
		wsbbZnadjperc[wsaaNofRead.toInt()].set(lextIO.getZnadjperc());
		wsbbZmortpct[wsaaNofRead.toInt()].set(lextIO.getZmortpct());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5651);
		descIO.setDescitem(lextIO.getOpcda());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsbbDesc[wsaaNofRead.toInt()].fill("?");
		}
		else {
			wsbbDesc[wsaaNofRead.toInt()].set(descIO.getShortdesc());
			getLongdescCustomerSpecific();
			
		}
		
		
		goTo(GotoLabel.next1220);
	}

protected void initialZero1300()
	{
		/*PARA*/
		wsbbOpcda[wsaaIndex.toInt()].set(SPACES);
		wsbbDesc[wsaaIndex.toInt()].set(SPACES);
		wsbbAgerate[wsaaIndex.toInt()].set(0);
		wsbbOppc[wsaaIndex.toInt()].set(0);
		wsbbInsprm[wsaaIndex.toInt()].set(0);
		wsbbPremadj[wsaaIndex.toInt()].set(0);
		wsbbEcestrm[wsaaIndex.toInt()].set(ZERO);
		wsbbSeqnbr[wsaaIndex.toInt()].set(0);
		wsbbReasind[wsaaIndex.toInt()].set(SPACES);
		wsbbZnadjperc[wsaaIndex.toInt()].set(ZERO);
		wsbbZmortpct[wsaaIndex.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveStackToScreen1400()
	{
		/*PARA*/
		scrnparams.function.set(varcom.sclr);
		callScreenIo5100();
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaSubfileSize)); wsaaIndex.add(1)){
			moveToScreen1500();
		}
		/*EXIT*/
	}

protected void moveToScreen1500()
	{
		moveToScreen1520();
	}

protected void moveToScreen1520()
	{
		sv.opcda.set(wsbbOpcda[wsaaIndex.toInt()]);
		sv.shortdesc.set(wsbbDesc[wsaaIndex.toInt()]);
		sv.agerate.set(wsbbAgerate[wsaaIndex.toInt()]);
		sv.oppc.set(wsbbOppc[wsaaIndex.toInt()]);
		sv.insprm.set(wsbbInsprm[wsaaIndex.toInt()]);
		sv.premadj.set(wsbbPremadj[wsaaIndex.toInt()]);
		sv.extCessTerm.set(wsbbEcestrm[wsaaIndex.toInt()]);
		sv.seqnbr.set(wsbbSeqnbr[wsaaIndex.toInt()]);
		sv.reasind.set(wsbbReasind[wsaaIndex.toInt()]);
		sv.znadjperc.set(wsbbZnadjperc[wsaaIndex.toInt()]);
		sv.zmortpct.set(wsbbZmortpct[wsaaIndex.toInt()]);
		if (isEQ(wsbbJlife[wsaaIndex.toInt()],SPACES)) {
			sv.select.set(SPACES);
		}
		else {
			if (isEQ(wsbbJlife[wsaaIndex.toInt()],"00")) {
				sv.select.set("L");
			}
			else {
				sv.select.set("J");
			}
		}
		setLongdescCustomerSpecific();
		scrnparams.subfileRrn.set(wsaaIndex);
		scrnparams.function.set(varcom.sadd);
		callScreenIo5100();
	}

protected void callLextio5000()
	{
		try {
			para5000();
		}
		catch (GOTOException e){
		}
	}

protected void para5000()
	{
		lextIO.setFormat(lextrec);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.mrnf)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isEQ(lextIO.getStatuz(),varcom.mrnf)) {
			lextIO.setStatuz(varcom.endp);
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		if ((isNE(lextIO.getChdrcoy(),chdrpf.getChdrcoy()))
		|| (isNE(lextIO.getChdrnum(),chdrpf.getChdrnum()))) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit5090);
		}
	}

protected void callScreenIo5100()
	{
		/*PARA*/
		processScreen("S6248", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.function.set(varcom.prot);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(sv.reasind,SPACES)) {
			sv.reasind.set("1");
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit2090);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
protected void getLongdescCustomerSpecific(){ };

protected void setLongdescCustomerSpecific(){ };

}
