package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50U
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50uScreenVars extends SmartVarModel { 

	//ILIFE-1513 START by dpuhawan
	//public FixedLengthStringData dataArea = new FixedLengthStringData(462);
	//public FixedLengthStringData dataFields = new FixedLengthStringData(206).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataArea = new FixedLengthStringData(508);
	public FixedLengthStringData dataFields = new FixedLengthStringData(236).isAPartOf(dataArea, 0);
	//ILIFE-1513 END
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,54);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,182);
	public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData zvariable = DD.zvariable.copy().isAPartOf(dataFields,189);
	public FixedLengthStringData zvars = new FixedLengthStringData(6).isAPartOf(dataFields, 200);
	//ILIFE-1513 START by dpuhawan
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(dataFields,206);
	//ILIFE-1513 END
	public FixedLengthStringData[] zvar = FLSArrayPartOfStructure(2, 3, zvars, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(zvars, 0, FILLER_REDEFINE);
	public FixedLengthStringData zvar01 = DD.zvar.copy().isAPartOf(filler,0);
	public FixedLengthStringData zvar02 = DD.zvar.copy().isAPartOf(filler,3);
	//ILIFE-1513 START by dpuhawan
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 206);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 236);
	//ILIFE-1513 END
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData trancdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData zvariableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData zvarsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 56);
	//ILIFE-1513 START by dpuhawan
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	//ILIFE-1513 END
	public FixedLengthStringData[] zvarErr = FLSArrayPartOfStructure(2, 4, zvarsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(zvarsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zvar01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData zvar02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	//ILIFE-1513 START by dpuhawan
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 270);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 304);
	//ILIFE-1513 END
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] trancdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] zvariableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData zvarsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 168);
	//ILIFE-1513 START by dpuhawan
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	//ILIFE-1513 END
	public FixedLengthStringData[] zvarOut = FLSArrayPartOfStructure(2, 12, zvarsOut, 0);
	public FixedLengthStringData[][] zvarO = FLSDArrayPartOfArrayStructure(12, 1, zvarOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(zvarsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zvar01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] zvar02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(256);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(77).isAPartOf(subfileArea, 0);
	public FixedLengthStringData asacscode = DD.asacscode.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData asacstyp = DD.asacstyp.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(subfileFields,4);
	public ZonedDecimalData fundAmount = DD.fundamnt.copyToZonedDecimal().isAPartOf(subfileFields,5);
	public FixedLengthStringData fundtype = DD.fundtype.copy().isAPartOf(subfileFields,22);
	public ZonedDecimalData moniesDate = DD.moniesdt.copyToZonedDecimal().isAPartOf(subfileFields,23);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(subfileFields,31);
	public ZonedDecimalData nofDunits = DD.nofdunt.copyToZonedDecimal().isAPartOf(subfileFields,32);
	public ZonedDecimalData nofUnits = DD.nofunt.copyToZonedDecimal().isAPartOf(subfileFields,48);
	public FixedLengthStringData vfund = DD.vfund.copy().isAPartOf(subfileFields,64);
	/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
	public ZonedDecimalData priceUsed = DD.priceused.copyToZonedDecimal().isAPartOf(subfileFields,68);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 77);
	public FixedLengthStringData asacscodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData asacstypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData fdbkindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData fundamntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fundtypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData moniesdtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData ndfindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData nofduntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData nofuntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData vfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
	public FixedLengthStringData priceUsedErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 121);
	public FixedLengthStringData[] asacscodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] asacstypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] fdbkindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] fundamntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fundtypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] moniesdtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] ndfindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] nofduntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] nofuntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] vfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
	public FixedLengthStringData[] priceUsedOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 253);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData moniesDateDisp = new FixedLengthStringData(10);

	public LongData Sr50uscreensflWritten = new LongData(0);
	public LongData Sr50uscreenctlWritten = new LongData(0);
	public LongData Sr50uscreenWritten = new LongData(0);
	public LongData Sr50uprotectWritten = new LongData(0);
	public GeneralTable sr50uscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50uscreensfl;
	}

	public Sr50uScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zvar01Out,new String[] {null, null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zvar02Out,new String[] {null, null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zvariableOut,new String[] {null, null, "-02",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {nofUnits, nofDunits, moniesDate, fundAmount, feedbackInd, nowDeferInd, vfund, fundtype, asacscode, asacstyp,priceUsed};
		screenSflOutFields = new BaseData[][] {nofuntOut, nofduntOut, moniesdtOut, fundamntOut, fdbkindOut, ndfindOut, vfundOut, fundtypeOut, asacscodeOut, asacstypOut,priceUsedOut};
		screenSflErrFields = new BaseData[] {nofuntErr, nofduntErr, moniesdtErr, fundamntErr, fdbkindErr, ndfindErr, vfundErr, fundtypeErr, asacscodeErr, asacstypErr,priceUsedErr};
		screenSflDateFields = new BaseData[] {moniesDate};
		screenSflDateErrFields = new BaseData[] {moniesdtErr};
		screenSflDateDispFields = new BaseData[] {moniesDateDisp};

		//ILIFE-1513 START by dpuhawan
		/*
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, zvar01, zvar02, zvariable, effdate, trancd};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, zvar01Out, zvar02Out, zvariableOut, effdateOut, trancdOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, zvar01Err, zvar02Err, zvariableErr, effdateErr, trancdErr};
		*/
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, zvar01, zvar02, zvariable, effdate, trancd, trandesc};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, zvar01Out, zvar02Out, zvariableOut, effdateOut, trancdOut, trandescOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, zvar01Err, zvar02Err, zvariableErr, effdateErr, trancdErr, trandescErr};
	
		//ILIFE-1513 END
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50uscreen.class;
		screenSflRecord = Sr50uscreensfl.class;
		screenCtlRecord = Sr50uscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50uprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50uscreenctl.lrec.pageSubfile);
	}
}
