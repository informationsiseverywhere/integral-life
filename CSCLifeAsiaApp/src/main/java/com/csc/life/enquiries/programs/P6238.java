/*
 * File: P6238.java
 * Date: 30 August 2009 0:38:12
 * Author: Quipoz Limited
 * 
 * Class transformed from P6238.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgcmvsqTableDAM;
import com.csc.life.agents.dataaccess.dao.AgcmpfDAO;
import com.csc.life.agents.dataaccess.model.Agcmpf;
import com.csc.life.enquiries.dataaccess.AgntenqTableDAM;
import com.csc.life.enquiries.screens.S6238ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selectio 
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will b 
*     stored  in  the  CHDRENQ I/O module. Retrieve the details an 
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description fro 
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description fro 
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist 
*
*
*     The first AGCM record to be displayed will in the AGCMVSQ I/ 
*     module.  Perform  a  RETRV  on  this  module  to  obtain  th 
*     corresponding AGCM record.
*
*     Load the subfile as follows:
*
*          Use the  key  from  the  I/O module to perform a BEGN o 
*          AGCMVSQ  and then continue reading the data-set fom tha 
*          point  until  end  of  file  or  the  Contract  Company 
*          Contract  Number  ar  Agent  Number  portion  of the ke 
*          changes.
*
*          The 'Element'  field  on  the  subfile will be filled a 
*          follows:
*
*               On   change   of  Plan  Suffix  (AGCM-PLAN-SUFFIX) 
*               display the  4  digit  plan  suffix  in the first  
*               characters   of   the  'Element'  field.  Fill  th 
*               remainder  of  the  'Element' field and the rest o 
*               the subfile record with spaces.
*
*               On  the  subsequent  subfile  record, indented by  
*               spaces display the Life Number, Coverage Number an 
*               Rider Number concatenated together to form the las 
*               6 characters of the 'Element' field.
*
*          On  this  subfile  record  display  the  following  AGC 
*          details:
*
*               The  Effective  Date  (EFDATE),  Annualised Premiu 
*               (ANNPREM), Commission Payable (INITCOM), Commissio 
*               Paid (COMPAY) and Commission Earned (COMERN).
*
* Plan Breakout.
* --------------
*
*     Note the following for Plan processing:
*
*          For the first AGCM record found where Plan Processing i 
*          applicable  this  record  will  apply  to  more than on 
*          policy  within  the  plan. The Plan Numbers displayed i 
*          the element  field  will  have  to  be calculated by th 
*          program  from  '0001'  up  to  the  number  of  policie 
*          summarised  by  the  plan  (obtainable from the contrac 
*          header).   Each   line  of  detail  that  follows  thes 
*          calculated  plan  numbers  will  have to be assembled a 
*          follows:
*
*               The  Effective  Date  will  be  the  same,  but th 
*               amounts will  all  have to divided by the number o 
*               policies summarised, (from the Contract Header).
*
*               This  information  will have to be repeated as man 
*               times  as  there are policies summarised within th 
*               plan.
*
*     The following example illustrates how this works in practice 
*
* Number of policies in Plan    = 5
*
* Number of policies summarised = 3
*
* <------ AGCM Records ------>  | <-- Screen Display --- - - - - - 
*  Plan  Life Cov. Rider Ann.   | Element    Ann. Prem  etc.
* Suffix                 Prem   |
*  0000   01   01   00  300000  | 0001                 \
*  0000   01   01   01   15000  | 0002                 | The recor s with
*  0000   01   01   02    3000  | 0003                 | a Plan Su fix of
*                               |   010100    10000    | '0000' ha e had
*                               |   010101     5000    | their amo nts
*                               |   010102     1000    | divided b  the
*                               |                      | no. of po icies
*                               |                      | summarise . The
*                               |                      | suffix nu bers
*                               |                      | represent d by
*                               |                      | the summa y rec
*                               |                      / are shown first.
*                               |
*                               |
*  0004   01   01   00    1000  |   010100    20000    \ These rec rds
*  0004   01   01   01    1000  |   010101     1200    | displayed as
*  0005   01   01   00    1000  |   010100     1000    | read.
*  0005   01   01   01    1000  |   010101      500    /
*
*
*
*     Load all  pages  required  in the subfile and set the subfil 
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     For "CF11"  or  "Enter" continue by adding one to the progra 
*     pointer.
*
*
*
* Notes.
* ------
*
*     Create a  new  view  of RTRNPF called RTRNENQ which uses onl 
*     those fields required for this program.
*
*     Create a  new  view  of ACMVPF called ACMVENQ which uses onl 
*     those fields required for this program.
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6238 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6238");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-ELEMENTS */
	private FixedLengthStringData wsaaSuffixPart = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaSuffixPart, 0).setUnsigned();

	private FixedLengthStringData wsaaComponentPart = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaComponentPart, 2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaComponentPart, 4);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaComponentPart, 6);
	private ZonedDecimalData wsaaStoreSuffix = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsbbPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsbbRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsbbCoverage = new FixedLengthStringData(2);
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private AgcmvsqTableDAM agcmvsqIO = new AgcmvsqTableDAM();
	private AgntenqTableDAM agntenqIO = new AgntenqTableDAM();
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private List<Chdrpf> chdrpfList=null;
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S6238ScreenVars sv = ScreenProgram.getScreenVars( S6238ScreenVars.class);
	private boolean ctenq011Permission; //IBPLIFE-9252
	private AgcmpfDAO agcmpfDAO;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		summaryPart1020, 
		exit1090, 
		para1350
	}

	public P6238() {
		super();
		screenVars = sv;
		new ScreenModel("S6238", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case summaryPart1020: 
					summaryPart1020();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		ctenq011Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CTENQ011", appVars, "IT"); //IBPLIFE-9252
		sv.ctenq011Flag.set(ctenq011Permission?"Y":"N");		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		/*  MOVE ZEROES                 TO S6238-EFFDATE*/
		/*                                 S6238-ANNPREM*/
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.annprem.set(ZERO);
		sv.initcom.set(ZERO);
		sv.compay.set(ZERO);
		sv.comern.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6238", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf= chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf==null){
			return;
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/*    MOVE CHDRENQ-AGNTNUM        TO S6238-SERVAGNT.*/
		sv.servbr.set(chdrpf.getCntbranch());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*    Retrieve the AGCM file for the selected servicing agent.*/
		agcmvsqIO.setDataArea(SPACES);
		agcmvsqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, agcmvsqIO);
		if (isNE(agcmvsqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmvsqIO.getParams());
			fatalError600();
		}
		sv.servagnt.set(agcmvsqIO.getAgntnum());
		/*    Obtain the Agent's name.*/
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(chdrpf.getAgntcoy());
		agntenqIO.setAgntnum(agcmvsqIO.getAgntnum());
		agntenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(), varcom.oK)
		&& isNE(agntenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.servagnam.set(wsspcomn.longconfname);
		/*    Obtain the Branch name from T1692.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrpf.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.servbrname.fill("?");
		}
		else {
			sv.servbrname.set(descIO.getLongdesc());
		}
		wsaaPlanSuffix.set(ZERO);
		/* on retreving the agcm record from previous screen p6237*/
		/* you have either a agcm record with plan suffix of 0*/
		/* (have to break out) or plan suffix of a number > 1*/
		/* display data for that suffix.*/
		wsaaAgntnum.set(agcmvsqIO.getAgntnum());
		wsbbRider.set(agcmvsqIO.getRider());
		wsbbCoverage.set(agcmvsqIO.getCoverage());
		wsbbPlanSuffix.set(agcmvsqIO.getPlanSuffix());
		if (isEQ(agcmvsqIO.getPlanSuffix(), 0)
		&& isGT(chdrpf.getPolinc(), 1)) {
			goTo(GotoLabel.summaryPart1020);
		}
		else {
			processPlanSuffix1400();
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit1090);
		}
	}

protected void summaryPart1020()
	{
		while ( !(isEQ(wsaaPlanSuffix, chdrpf.getPolsum()))) {
			processSummary1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processSummary1100()
	{
		para1100();
	}

protected void para1100()
	{
		sv.subfileFields.set(SPACES);
		wsaaPlanSuffix.add(1);
		sv.chdrelem.set(wsaaSuffixPart);
		/*  MOVE ZERO                   TO S6238-EFFDATE*/
		/*                                 S6238-ANNPREM*/
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.annprem.set(ZERO);
		sv.initcom.set(ZERO);
		sv.compay.set(ZERO);
		sv.comern.set(ZERO);
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6238", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaStoreSuffix.set(agcmvsqIO.getPlanSuffix());
		processComponent1300();
	}

protected void processComponent1300()
	{
		para1300();
	}
//IBPLIFE-9252
protected void processComponent1300(Agcmpf pf)
{
	para1300(pf);
}
protected void para1300()
	{
		sv.subfileFields.set(SPACES);
		wsaaLife.set(agcmvsqIO.getLife());
		wsaaCoverage.set(agcmvsqIO.getCoverage());
		wsaaRider.set(agcmvsqIO.getRider());
		sv.chdrelem.set(wsaaComponentPart);
		sv.effdate.set(agcmvsqIO.getEfdate());
		if (isEQ(agcmvsqIO.getPlanSuffix(), ZERO)
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(sv.annprem, 2).set(div(agcmvsqIO.getAnnprem(), chdrpf.getPolsum()));
			compute(sv.initcom, 2).set(div(agcmvsqIO.getInitcom(), chdrpf.getPolsum()));
			compute(sv.compay, 2).set(div(agcmvsqIO.getCompay(), chdrpf.getPolsum()));
			compute(sv.comern, 2).set(div(agcmvsqIO.getComern(), chdrpf.getPolsum()));
		}
		else {
			sv.annprem.set(agcmvsqIO.getAnnprem());
			sv.initcom.set(agcmvsqIO.getInitcom());
			sv.compay.set(agcmvsqIO.getCompay());
			sv.comern.set(agcmvsqIO.getComern());
		}
		zrdecplrec.amountIn.set(sv.annprem);
		a000CallRounding();
		sv.annprem.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(sv.initcom);
		a000CallRounding();
		sv.initcom.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(sv.compay);
		a000CallRounding();
		sv.compay.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(sv.comern);
		a000CallRounding();
		sv.comern.set(zrdecplrec.amountOut);
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6238", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(wsbbPlanSuffix, 0)
		&& isGT(chdrpf.getPolinc(), 1)) {
			nextAgcmvsq1350();
		}
		agcmvsqIO.setStatuz(varcom.endp);
	}
//IBPLIFE-9252
protected void para1300(Agcmpf pf)
{
	sv.subfileFields.set(SPACES);
	wsaaLife.set(pf.getLife());
	wsaaCoverage.set(pf.getCoverage());
	wsaaRider.set(pf.getRider());
	sv.chdrelem.set(wsaaComponentPart);
	sv.effdate.set(pf.getEfdate());
	if (isEQ(pf.getPlnsfx(), ZERO)
	&& isGT(chdrpf.getPolsum(), ZERO)) {
		compute(sv.annprem, 2).set(div(pf.getAnnprem(), chdrpf.getPolsum()));
		compute(sv.initcom, 2).set(div(pf.getInitcom(), chdrpf.getPolsum()));
		compute(sv.compay, 2).set(div(pf.getCompay(), chdrpf.getPolsum()));
		compute(sv.comern, 2).set(div(pf.getComern(), chdrpf.getPolsum()));
	}
	else {
		sv.annprem.set(pf.getAnnprem());
		sv.initcom.set(pf.getInitcom());
		sv.compay.set(pf.getCompay());
		sv.comern.set(pf.getComern());
	}
	zrdecplrec.amountIn.set(sv.annprem);
	a000CallRounding();
	sv.annprem.set(zrdecplrec.amountOut);
	zrdecplrec.amountIn.set(sv.initcom);
	a000CallRounding();
	sv.initcom.set(zrdecplrec.amountOut);
	zrdecplrec.amountIn.set(sv.compay);
	a000CallRounding();
	sv.compay.set(zrdecplrec.amountOut);
	zrdecplrec.amountIn.set(sv.comern);
	a000CallRounding();
	sv.comern.set(zrdecplrec.amountOut);
	if(ctenq011Permission) {//IBPLIFE-9252
		sv.dormflag.set(pf.getDormflag().equals("Y")?"N":"Y");
		sv.totalcompay.add(sv.compay);
		sv.totalcomern.add(sv.comern);
	}
	/*    Write the subfile record.*/
	scrnparams.function.set(varcom.sadd);
	processScreen("S6238", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}
protected void nextAgcmvsq1350()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para1350: 
					para1350();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1350()
	{
		/*    Read the next AGCMVSQ record.*/
		agcmvsqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmvsqIO);
		if (isNE(agcmvsqIO.getStatuz(), varcom.oK)
		&& isNE(agcmvsqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmvsqIO.getParams());
			fatalError600();
		}
		if (isNE(agcmvsqIO.getStatuz(), varcom.endp)
		&& isEQ(agcmvsqIO.getStatuz(), varcom.oK)
		&& (isNE(agcmvsqIO.getRider(), wsbbRider)
		|| isNE(agcmvsqIO.getCoverage(), wsbbCoverage))) {
			goTo(GotoLabel.para1350);
		}
		/*EXIT*/
	}

protected void processPlanSuffix1400()
	{
		para1400();
	}

protected void para1400()
	{
		sv.subfileFields.set(SPACES);
		wsaaPlanSuffix.set(agcmvsqIO.getPlanSuffix());
		sv.chdrelem.set(wsaaSuffixPart);
		/*MOVE ZERO                   TO S6238-EFFDATE*/
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.annprem.set(ZERO);
		sv.initcom.set(ZERO);
		sv.compay.set(ZERO);
		sv.comern.set(ZERO);
		/*    Write the subfile record.*/
		//ILIFE-7397
		/*scrnparams.function.set(varcom.sadd);
		processScreen("S6238", sv);*/
		// Extra subfile addition commented
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaStoreSuffix.set(agcmvsqIO.getPlanSuffix());
		//IBPLIFE-9252
		if (ctenq011Permission) {
			Agcmpf agcmpf = new Agcmpf();
			agcmpf.setChdrcoy(agcmvsqIO.getChdrcoy().toString());
			agcmpf.setChdrnum(agcmvsqIO.getChdrnum().toString());
			agcmpf.setAgntnum(agcmvsqIO.getAgntnum().toString());
			agcmpf.setLife(agcmvsqIO.getLife().toString());
			agcmpf.setCoverage(agcmvsqIO.getCoverage().toString());
			agcmpf.setRider(agcmvsqIO.getRider().toString());
			agcmpf.setPlnsfx(agcmvsqIO.getPlanSuffix().toInt());
			agcmpfDAO = getApplicationContext().getBean("agcmpfDAO2", AgcmpfDAO.class);
			List<Agcmpf> agcmpfList = agcmpfDAO.searchAgcmpfRecord(agcmpf);
			for (Agcmpf pf : agcmpfList) {
				processComponent1300(pf);
			}
		} else {
			while (!(isNE(agcmvsqIO.getChdrcoy(), chdrpf.getChdrcoy())
					|| isNE(agcmvsqIO.getChdrnum(), chdrpf.getChdrnum()) || isNE(agcmvsqIO.getAgntnum(), wsaaAgntnum)
					|| isNE(agcmvsqIO.getPlanSuffix(), wsaaStoreSuffix) || isEQ(agcmvsqIO.getStatuz(), varcom.endp))) {
				processComponent1300();
			}
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6238IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6238-DATA-AREA                         */
		/*                         S6238-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  No updates are required.*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
}
