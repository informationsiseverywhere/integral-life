/*
 * File: P5069.java
 * Date: 30 August 2009 0:01:41
 * Author: Quipoz Limited
 * 
 * Class transformed from P5069.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S5069ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this  section  if  returning  from an optional selection
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will be
*     stored  in  the  CHDRENQ I/O module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint  owner's  client (CLTS) details if they exist.
*
*     This  program  will  display  all  of the Plan Suffixes for a
*     given  Plan. This will entail 'breaking out' from the summary
*     record  in  the  case  of Plan Processing and generating Plan
*     Suffixes for display.
*
*     The  Risk Status code and description and Premium Status Code
*     and  description  from  the  first Coverage on the first Life
*     will be displayed for each Plan Suffix.
*
*     Load the subfile as follows:
*
*          Read  the  first  COVRENQ  record on the contract, using
*          zero  in  Plan  Suffix,  '01'  in  the  Life field and a
*          function of BEGN.
*
*          If the Plan Suffix from the returned record is zero then
*          add 1 to it before display. Obtain the long descriptions
*          for  the  coverage risk status, STATCODE, from T5682 and
*          the  premium  status  code,  PSTATCODE,  from  T5681 and
*          display them.
*
*          Repeat  the  above  line  for as mamy times as there are
*          summarised policies, incrementing the Plan Suffix by one
*          for each subfile record written.
*
*          Store  the  life number and increment the Plan Suffix by
*          one.  Perform  a BEGN to obtain the first COVRENQ record
*          for  the  next  Plan  Suffix.  Write  out its details as
*          before  and repeat the process until either the Company,
*          Contract Header Number or Life Number changes.
*
*
*     Load all pages  required  in  the subfile and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     There  is no validation required as any character may be used
*     to select a policy.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was requested move spaces to  the  current  program
*     position and action field, add 1 to the  program  pointer and
*     exit.
*
*     At  this  point  the program will be either searching for the
*     FIRST  selected  record  in  order  to  pass  control  to the
*     appropriate   generic   enquiry   program  for  the  selected
*     component  or it will be returning from the Component Enquiry
*     Screen  after  displaying  some details and searching for the
*     NEXT selected record.
*
*     It will be able  to determine which of these two states it is
*     in by examining the Stack Action Flag.
*
*     If  returning  from  an  enquiry  move  spaces to the current
*     WSSP-SEC-ACTN field and the Select field.
*
*     Continue  reading  the  subfile  records  using the Read Next
*     Changed  Subfile  function,  (SRNCH) until end of file or the
*     Select field is non-blank.
*
*     If nothing was selected or there are no  more  selections  to
*     process,  continue  by  moving blanks to  the  current  Stack
*     Action field and program position and exit.
*
*     If  a  selection  has been found perform a RLSE on COVRENQ to
*     ensure that any previously held record is freed up.
*
*     It  is  essential that the plan suffix of the selected record
*     is  passed  across  to  the Component Enquiry program even if
*     this is a 'generated' Plan Suffix fom the summary record. Use
*     the key of the selected record to perform a READS on COVRENQ.
*     If   the   selected   Plan   Suffix   is   not  greater  than
*     CHDRENQ-POLSUM  then  use  a value of zero in the Plan Suffix
*     field for the READS.
*
*     Then  move  in to COVRENQ-PLAN-SUFFIX the actual value of the
*     Plan  Suffix  that  was  selected  by  the user and perform a
*     KEEPS.  This  will  then  store the chosen Plan Suffix in the
*     COVRENQIO module.
*
*     Add 1 to the program pointer and exit.
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE  (CHDRENQ)
* T3623 - Contract Risk Status              Key: STATCODE   (CHDRENQ)
* T5681 - Coverage Premium Status           Key: PSTATCODE  (COVRENQ)
* T5682 - Coverage Risk Status              Key: STATCODE   (COVRENQ)
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*
*****************************************************************
* </pre>
*/
public class P5069 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5069");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaRstatdesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPstatdesc = new FixedLengthStringData(30);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5688 = "T5688";
	private String covrenqrec = "COVRENQREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5069ScreenVars sv = ScreenProgram.getScreenVars( S5069ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		bypass4020, 
		exit4090
	}

	public P5069() {
		super();
		screenVars = sv;
		new ScreenModel("S5069", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5069", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getRegister());
		sv.numpols.set(chdrenqIO.getPolinc());
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set("NONE");
			sv.jlifename.set("NONE");
		}
		else {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		covrenqIO.setDataArea(SPACES);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrenqIO.getChdrnum());
		covrenqIO.setPlanSuffix(ZERO);
		wsaaPlanSuffix.set(ZERO);
		covrenqIO.setLife("01");
		covrenqIO.setCoverage("00");
		covrenqIO.setRider("00");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(covrenqIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,covrenqIO.getChdrcoy())
		|| isNE(chdrenqIO.getChdrnum(),covrenqIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
		}
		wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
		if (isEQ(covrenqIO.getPlanSuffix(),ZERO)) {
			wsaaPlanSuffix.add(1);
		}
		sv.planSuffix.set(wsaaPlanSuffix);
		sv.life.set(covrenqIO.getLife());
		sv.coverage.set(covrenqIO.getCoverage());
		sv.rider.set(covrenqIO.getRider());
		sv.statcode.set(covrenqIO.getStatcode());
		sv.pstatcode.set(covrenqIO.getPstatcode());
		covrStatusDescs1600();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5069", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.add(1);
		if (isEQ(covrenqIO.getPlanSuffix(),ZERO)) {
			while ( !(isGT(wsaaPlanSuffix,chdrenqIO.getPolsum()))) {
				processSuffixGroup1100();
			}
			
		}
		wsaaLife.set(covrenqIO.getLife());
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrenqIO.getChdrnum());
		covrenqIO.setPlanSuffix(wsaaPlanSuffix);
		covrenqIO.setCoverage("00");
		covrenqIO.setRider("00");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		while ( !(isNE(covrenqIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrenqIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(covrenqIO.getLife(),wsaaLife)
		|| isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processPolicy1200();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void processSuffixGroup1100()
	{
		para1100();
	}

protected void para1100()
	{
		sv.planSuffix.set(wsaaPlanSuffix);
		sv.statcode.set(covrenqIO.getStatcode());
		sv.pstatcode.set(covrenqIO.getPstatcode());
		sv.rstatdesc.set(wsaaRstatdesc);
		sv.pstatdesc.set(wsaaPstatdesc);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5069", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.add(1);
	}

protected void processPolicy1200()
	{
		para1200();
	}

protected void para1200()
	{
		sv.planSuffix.set(covrenqIO.getPlanSuffix());
		wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
		sv.statcode.set(covrenqIO.getStatcode());
		sv.pstatcode.set(covrenqIO.getPstatcode());
		covrStatusDescs1600();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5069", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.add(1);
		covrenqIO.setFunction(varcom.begn);
		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrenqIO.getChdrnum());
		covrenqIO.setPlanSuffix(wsaaPlanSuffix);
		covrenqIO.setCoverage("00");
		covrenqIO.setRider("00");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
	}

protected void covrStatusDescs1600()
	{
		para1600();
	}

protected void para1600()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5681);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrenqIO.getPstatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatdesc.fill("?");
			wsaaPstatdesc.fill("?");
		}
		else {
			sv.pstatdesc.set(descIO.getLongdesc());
			wsaaPstatdesc.set(descIO.getLongdesc());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5682);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrenqIO.getStatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.rstatdesc.fill("?");
			wsaaRstatdesc.fill("?");
		}
		else {
			sv.rstatdesc.set(descIO.getLongdesc());
			wsaaRstatdesc.set(descIO.getLongdesc());
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo12010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo12010()
	{
		if (isEQ(scrnparams.statuz,varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4000();
				}
				case bypass4020: {
					bypass4020();
					nextProgram4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			goTo(GotoLabel.bypass4020);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5069", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypass4020()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4100();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.flag.set("I");
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		sv.select.set(SPACES);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		covrenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		wsaaPlanSuffix.set(sv.planSuffix);
		if (isLTE(wsaaPlanSuffix,chdrenqIO.getPolsum())
		&& isNE(chdrenqIO.getPolsum(),1)) {
			wsaaPlanSuffix.set(ZERO);
		}
		covrenqIO.setDataArea(SPACES);
		covrenqIO.setFunction(varcom.readr);
		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrenqIO.getChdrnum());
		covrenqIO.setPlanSuffix(wsaaPlanSuffix);
		covrenqIO.setLife(sv.life);
		covrenqIO.setCoverage(sv.coverage);
		covrenqIO.setRider(sv.rider);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		covrenqIO.setPlanSuffix(sv.planSuffix);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
	}

protected void nextProgram4080()
	{
		wsspcomn.flag.set("Y");
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5069", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
