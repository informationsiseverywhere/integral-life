package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:23
 * Description:
 * Copybook name: UFNSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ufnskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ufnsFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData ufnsKey = new FixedLengthStringData(256).isAPartOf(ufnsFileKey, 0, REDEFINE);
  	public FixedLengthStringData ufnsCompany = new FixedLengthStringData(1).isAPartOf(ufnsKey, 0);
  	public FixedLengthStringData ufnsVirtualFund = new FixedLengthStringData(4).isAPartOf(ufnsKey, 1);
  	public FixedLengthStringData ufnsUnitType = new FixedLengthStringData(1).isAPartOf(ufnsKey, 5);
  	public FixedLengthStringData filler = new FixedLengthStringData(250).isAPartOf(ufnsKey, 6, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ufnsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ufnsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}