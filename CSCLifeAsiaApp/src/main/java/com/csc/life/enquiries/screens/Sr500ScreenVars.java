package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR500
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sr500ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1249);
	public FixedLengthStringData dataFields = new FixedLengthStringData(561).isAPartOf(dataArea, 0);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData brchname = DD.brchlname.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,33);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,73);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,103);
	public ZonedDecimalData hissdte = DD.hissdte.copyToZonedDecimal().isAPartOf(dataFields,111);
	public ZonedDecimalData hoissdte = DD.hoissdte.copyToZonedDecimal().isAPartOf(dataFields,119);
	public ZonedDecimalData hpropdte = DD.hpropdte.copyToZonedDecimal().isAPartOf(dataFields,127);
	public ZonedDecimalData hprrcvdt = DD.hprrcvdt.copyToZonedDecimal().isAPartOf(dataFields,135);
	public ZonedDecimalData huwdcdte = DD.huwdcdte.copyToZonedDecimal().isAPartOf(dataFields,143);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,151);
	public FixedLengthStringData inds = new FixedLengthStringData(4).isAPartOf(dataFields, 152);
	public FixedLengthStringData[] ind = FLSArrayPartOfStructure(4, 1, inds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(inds, 0, FILLER_REDEFINE);
	public FixedLengthStringData ind01 = DD.ind.copy().isAPartOf(filler,0);
	public FixedLengthStringData ind02 = DD.ind.copy().isAPartOf(filler,1);
	public FixedLengthStringData ind03 = DD.ind.copy().isAPartOf(filler,2);
	public FixedLengthStringData ind04 = DD.ind.copy().isAPartOf(filler,3);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,203);
	public FixedLengthStringData jowner = DD.jowner.copy().isAPartOf(dataFields,211);
	public FixedLengthStringData jownername = DD.jownername.copy().isAPartOf(dataFields,219);
	public ZonedDecimalData instpramt = DD.lastinsamt.copyToZonedDecimal().isAPartOf(dataFields,266);
	public ZonedDecimalData lastinsdte = DD.lastinsdte.copyToZonedDecimal().isAPartOf(dataFields,283);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,291);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,338);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,346);
	public ZonedDecimalData nextinsamt = DD.nextinsamt.copyToZonedDecimal().isAPartOf(dataFields,347);
	public ZonedDecimalData nextinsdte = DD.nextinsdte.copyToZonedDecimal().isAPartOf(dataFields,364);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,372);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,376);
	public FixedLengthStringData payer = DD.payer.copy().isAPartOf(dataFields,423);
	public FixedLengthStringData payername = DD.payername.copy().isAPartOf(dataFields,431);
	public FixedLengthStringData payfreq = DD.payfreq.copy().isAPartOf(dataFields,478);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,480);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,490);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,498);
	public FixedLengthStringData servagnam = DD.servagnam.copy().isAPartOf(dataFields,501);
	public FixedLengthStringData servagnt = DD.servagnt.copy().isAPartOf(dataFields,548);
	public FixedLengthStringData servbr = DD.servbr.copy().isAPartOf(dataFields,556);
	public FixedLengthStringData znfopt = DD.znfopt.copy().isAPartOf(dataFields,558);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(172).isAPartOf(dataArea, 561);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData brchlnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData hissdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData hoissdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData hpropdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData hprrcvdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData huwdcdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData indsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] indErr = FLSArrayPartOfStructure(4, 4, indsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(indsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ind01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData ind02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData ind03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData ind04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData jownerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData jownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData lastinsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData lastinsdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData nextinsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData nextinsdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData payerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData payernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData payfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData servagnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData servagntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData servbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData znfoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(516).isAPartOf(dataArea, 733);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] brchlnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] hissdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] hoissdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] hpropdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] hprrcvdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] huwdcdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData indsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(4, 12, indsOut, 0);
	public FixedLengthStringData[][] indO = FLSDArrayPartOfArrayStructure(12, 1, indOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(indsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ind01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] ind02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] ind03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] ind04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] jownerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] jownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] lastinsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] lastinsdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] nextinsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] nextinsdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] payerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] payernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] payfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] servagnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] servagntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] servbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] znfoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hissdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hoissdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hpropdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hprrcvdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData huwdcdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastinsdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextinsdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr500screenWritten = new LongData(0);
	public LongData Sr500protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr500ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(numpolsOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind01Out,new String[] {"21","23","-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind02Out,new String[] {"22","24","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind03Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ind04Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, cownnum, ownername, jowner, jownername, payer, payername, indic, servagnt, servagnam, servbr, brchname, numpols, billcurr, mop, payfreq, lastinsdte, instpramt, currfrom, nextinsdte, nextinsamt, ptdate, hpropdte, btdate, hprrcvdt, hoissdte, huwdcdte, znfopt, hissdte, ind01, ind02, ind03, ind04};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, cownnumOut, ownernameOut, jownerOut, jownernameOut, payerOut, payernameOut, indicOut, servagntOut, servagnamOut, servbrOut, brchlnameOut, numpolsOut, billcurrOut, mopOut, payfreqOut, lastinsdteOut, lastinsamtOut, currfromOut, nextinsdteOut, nextinsamtOut, ptdateOut, hpropdteOut, btdateOut, hprrcvdtOut, hoissdteOut, huwdcdteOut, znfoptOut, hissdteOut, ind01Out, ind02Out, ind03Out, ind04Out};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, cownnumErr, ownernameErr, jownerErr, jownernameErr, payerErr, payernameErr, indicErr, servagntErr, servagnamErr, servbrErr, brchlnameErr, numpolsErr, billcurrErr, mopErr, payfreqErr, lastinsdteErr, lastinsamtErr, currfromErr, nextinsdteErr, nextinsamtErr, ptdateErr, hpropdteErr, btdateErr, hprrcvdtErr, hoissdteErr, huwdcdteErr, znfoptErr, hissdteErr, ind01Err, ind02Err, ind03Err, ind04Err};
		screenDateFields = new BaseData[] {lastinsdte, currfrom, nextinsdte, ptdate, hpropdte, btdate, hprrcvdt, hoissdte, huwdcdte, hissdte};
		screenDateErrFields = new BaseData[] {lastinsdteErr, currfromErr, nextinsdteErr, ptdateErr, hpropdteErr, btdateErr, hprrcvdtErr, hoissdteErr, huwdcdteErr, hissdteErr};
		screenDateDispFields = new BaseData[] {lastinsdteDisp, currfromDisp, nextinsdteDisp, ptdateDisp, hpropdteDisp, btdateDisp, hprrcvdtDisp, hoissdteDisp, huwdcdteDisp, hissdteDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr500screen.class;
		protectRecord = Sr500protect.class;
	}

}
