/*
 * File: Uwlmtchk.java
 * Date: 30 August 2009 2:53:00
 * Author: Quipoz Limited
 * 
 * Class transformed from UWLMTCHK.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.List;

//import com.csc.fsu.clients.dataaccess.ClntTableDAM;
//import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO; //ILB-1062
import com.csc.fsu.general.dataaccess.model.Itempf; //ILB-1062
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.enquiries.recordstructures.Uwlmtrec;
import com.csc.life.enquiries.tablestructures.Tr596rec;
import com.csc.life.enquiries.tablestructures.Tr597rec;
import com.csc.life.productdefinition.dataaccess.dao.Br50hTempDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO; //ILIFE-8901
import com.csc.life.productdefinition.dataaccess.model.Br50hDTO;
import com.csc.life.productdefinition.tablestructures.Tr593rec;
import com.csc.life.productdefinition.tablestructures.Tr594rec;
//import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil; //ILB-1062
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   UWLMTCHK - CHECK AUTHORITY LIMIT
*   ********************************
*
*   This subroutine will computes TRSA (Total Relevant Sum
*   Sum Assured) and validate against user's authority limit.
*
*   This sub-routines does the followings:
*
*   1 Base on the pass in LIFCNUM, to check for the same
*     life assured (Client Role is 'LF'), read CHDRLNB file to
*     policy info,get the valid contract (Valid contract, status
*     stated on TR597;valid component,i.e. read table
*     TR596 to see if component need to aggregate the
*     initial sum assured TRSA; valid period,i.e. within
*     number of month stated on TR596),then loop through
*     COVT or COVR to accumulate sum assured into corresponding
*     RP, or SP or MT (base on the pass in info) working storage
*
*   2 when aggregating the sum assured, need to also
*     include the current contract on hand
*
*   3 based on user's ID (user performing the transaction
*     online), check UWLV file, if found ID, then get his/her U/W
*     Level code, if not found, check on USRD file, to get the
*     USRD-GROUP-NAME and check UWLV file to get U/W level code
*
*   4 Use the level code check the aggregated TRSA against table
*     item TR594;if limit satisfied, proceed with the transaction
*     otherwise display online error message 'exceed authorized
*     limit.
*
*   UWLMT-STATUZ:
*   - 'O-K': Within authorized limits
*   - 'S'  : Exceed Standard authorized limits
*   - 'SS' : Exceed Sub-Standard authorized limits
*
***********************************************************************
* </pre>
*/
public class Uwlmtchk extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "UWLMTCHK";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData iy = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private String wsaaChdrcoy;
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSubSumins = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaSarProposal = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaFoundStatcode = new FixedLengthStringData(1);
	private Validator foundStatcode = new Validator(wsaaFoundStatcode, "Y");

	private FixedLengthStringData wsaaFoundPstatcode = new FixedLengthStringData(1);
	private Validator foundPstatcode = new Validator(wsaaFoundPstatcode, "Y");

	private FixedLengthStringData wsaaFoundCnttype = new FixedLengthStringData(1);
	private Validator foundCnttype = new Validator(wsaaFoundCnttype, "Y");

	private FixedLengthStringData wsaaFoundChdrtype = new FixedLengthStringData(1);
	private Validator foundChdrtype = new Validator(wsaaFoundChdrtype, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaTr593CnttypeRec = new FixedLengthStringData(120);
	private FixedLengthStringData[] wsaaTr593Chdrtype = FLSArrayPartOfStructure(40, 3, wsaaTr593CnttypeRec, 0);
	private FixedLengthStringData wsaaTr593Uwptype = new FixedLengthStringData(10);
	private PackedDecimalData wsaaTotalSi = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSubsi = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaUwlevel = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaUwptype = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaTr594Table = new FixedLengthStringData(34);
	private ZonedDecimalData wsaaTr594Undlimit = new ZonedDecimalData(17, 0).isAPartOf(wsaaTr594Table, 0);
	private ZonedDecimalData wsaaTr594Usundlim = new ZonedDecimalData(17, 0).isAPartOf(wsaaTr594Table, 17);
	private String wsaaTr594Found = "";
	private String wsaaStandFlag = "N";
	private String p129 = "P129";
	private String invf = "INVF";
		/* TABLES */
	private String tr596 = "TR596";
	private String tr597 = "TR597";
	private String tr593 = "TR593";
	private String tr594 = "TR594";
		/* FORMATS */
	//private String chdrlnbrec = "CHDRLNBREC";
	private String uwlvrec = "UWLVREC";
	private String usrdrec = "USRDREC";
	/*private String covrincrec = "COVRINCREC";
	private String covtlnbrec = "COVTLNBREC";*/
	private String itemrec = "ITEMREC";
		/*Contract header - life new business*/
	//private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file*/
	//private ClntTableDAM clntIO = new ClntTableDAM();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
		/*Client role and relationships logical fi*/
	//private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
		/*Coverage/Rider details - Increases*/
	//private CovrincTableDAM covrincIO = new CovrincTableDAM();
		/*Coverage/rider transactions - new busine*/
	//private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	//private DescTableDAM descIO = new DescTableDAM();
	private Freqcpy freqcpy = new Freqcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Options/Extras logical file*/

	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Tr593rec tr593rec = new Tr593rec();
	private Tr594rec tr594rec = new Tr594rec();
	private Tr596rec tr596rec = new Tr596rec();
	private Tr597rec tr597rec = new Tr597rec();
		/*User Details Logical - Validflag '1's on*/
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
	private Uwlmtrec uwlmtrec = new Uwlmtrec();
		/*USER UNDERWRITING LEVEL*/
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private Varcom varcom = new Varcom();
	private Itemkey wsaaItemKey = new Itemkey();
	private Br50hDTO br50hDto = new Br50hDTO();
	private Br50hTempDAO br50hTempDao = getApplicationContext().getBean("br50hTempDAO",Br50hTempDAO.class);
	private List<Br50hDTO> br50hList;
	private Br50hDTO br50hDtoCovtCovr = new Br50hDTO();
 	//ILIFE-8901 start
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	//ILIFE-8901 end
	
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class); //ILB-1062
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit0900, 
		exit1200, 
		nextClrr1220, 
		exit1220, 
		exit1300, 
		processCovr1400, 
		exit1400, 
		trsa1500, 
		nextCovtlnb1500, 
		trsa1600, 
		nextCovrinc1600, 
		nextTr5931730, 
		exit1730, 
		exit2090, 
		callItemio2120, 
		exit2190
	}

	public Uwlmtchk() {
		super();
	}

public void mainline(Object... parmArray)
	{
		uwlmtrec.rec = convertAndSetParam(uwlmtrec.rec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			begin0100();
		}
		catch (GOTOException e){
		}
		finally{
			exit0900();
		}
	}

protected void begin0100()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		readTableTr5971100();
		checkLext1200();
		wsaaTotalSi.set(ZERO);
		wsaaTotalSubsi.set(ZERO);
		wsaaSubSumins.set(ZERO);
		wsaaSumins.set(ZERO);
		uwlmtrec.statuz.set(varcom.oK);
		uwlmtrec.uwlevel.set(SPACES);
		if (isNE(uwlmtrec.function,"CHCK")) {
			uwlmtrec.statuz.set(invf);
			goTo(GotoLabel.exit0900);
		}
		/*clrrIO.setParams(SPACES);
		clrrIO.setStatuz(varcom.oK);
		clrrIO.setClntpfx(uwlmtrec.cownpfx);
		clrrIO.setClntcoy(uwlmtrec.fsuco);
		clrrIO.setClntnum(uwlmtrec.lifcnum);
		clrrIO.setClrrrole(clntrlsrec.life);
		clrrIO.setForepfx(SPACES);
		clrrIO.setForecoy(SPACES);
		clrrIO.setForenum(SPACES);
		clrrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		clrrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clrrIO.setFitKeysSearch("CLNTPFX", "CLNTCOY", "CLNTNUM", "CLRRROLE");
		while ( !(isEQ(clrrIO.getStatuz(),varcom.endp))) {
			loadClrr1220();
		}*/
		br50hList = br50hTempDao.getUwlmtchlClrrChdrRecords(uwlmtrec.chdrnum.toString(), uwlmtrec.cownpfx.toString(), uwlmtrec.fsuco.toString(), clntrlsrec.life.toString(), uwlmtrec.lifcnum.toString());
		if(!br50hList.isEmpty()){
			for(Br50hDTO br50h:br50hList) {
				br50hDto = br50h;
				loadClrr1220();
			}
		}
		getUnderwritingLevel2000();
		loadTr5942100();
		checkLimit2200();
	}

protected void exit0900()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		uwlmtrec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void readTableTr5971100()
	{
		/*START*/
		wsaaItemKey.itemItemcoy.set(uwlmtrec.chdrcoy);
		wsaaItemKey.itemItemtabl.set(tr597);
		wsaaItemKey.itemItemitem.set(uwlmtrec.chdrcoy);
		a1000CallNonDatedTable();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr597rec.tr597Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void checkLext1200()
	{
 		//ILIFE-8901 start
		boolean recFound = lextpfDAO.checkLextRecord(uwlmtrec.chdrcoy.toString(), uwlmtrec.chdrnum.toString());
		if(recFound) {
			wsaaStandFlag = "Y";
	 		//ILIFE-8901 end
		}
	}

protected void loadClrr1220()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin1220();
				}
				case nextClrr1220: {
					nextClrr1220();
				}
				case exit1220: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1220()
	{
		/*SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)
		&& isNE(clrrIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		if (isNE(clrrIO.getStatuz(),varcom.oK)
		|| isNE(clrrIO.getClntpfx(),uwlmtrec.cownpfx)
		|| isNE(clrrIO.getClntcoy(),uwlmtrec.fsuco)
		|| isNE(clrrIO.getClntnum(),uwlmtrec.lifcnum)
		|| isNE(clrrIO.getClrrrole(),clntrlsrec.life)) {
			clrrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1220);
		}*/
		checkChdrStatus1300();
		if (isEQ(wsaaSarProposal,SPACES)) {
			goTo(GotoLabel.nextClrr1220);
		}
		processComponent1400();
	}

protected void nextClrr1220()
	{
		//clrrIO.setFunction(varcom.nextr);
	}

protected void checkChdrStatus1300()
	{
		try {
			begin1300();
		}
		catch (GOTOException e){
		}
	}

protected void begin1300()
	{
		wsaaSarProposal.set(SPACES);
		/*chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setStatuz(varcom.oK);
		chdrlnbIO.setChdrcoy(clrrIO.getForecoy());
		chdrlnbIO.setChdrnum(subString(clrrIO.getForenum(), 1, 8));
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			chdrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
			&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
				syserrrec.statuz.set(chdrlnbIO.getStatuz());
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
			|| isNE(chdrlnbIO.getChdrcoy(),clrrIO.getForecoy())
			|| isNE(chdrlnbIO.getChdrnum(),subString(clrrIO.getForenum(), 1, 8))) {
				goTo(GotoLabel.exit1300);
			}
		}*/
		wsaaFoundStatcode.set("N");
		wsaaFoundPstatcode.set("N");
		for (iy.set(1); !(isGT(iy,20)
		|| (foundStatcode.isTrue()
		&& foundPstatcode.isTrue())); iy.add(1)){
			//if (isEQ(chdrlnbIO.getStatcode(),tr597rec.setCnRiskStat[iy.toInt()])) {
			if (isEQ(br50hDto.getStatcode(),tr597rec.setCnRiskStat[iy.toInt()])) {
				wsaaFoundStatcode.set("Y");
			}
			//if (isEQ(chdrlnbIO.getPstatcode(),tr597rec.setCnPremStat[iy.toInt()])) {
			if (isEQ(br50hDto.getPstcde(),tr597rec.setCnPremStat[iy.toInt()])) {
				wsaaFoundPstatcode.set("Y");
			}
		}
		if (foundStatcode.isTrue()
		&& foundPstatcode.isTrue()) {
			wsaaSarProposal.set("Y");
			goTo(GotoLabel.exit1300);
		}
		wsaaFoundStatcode.set("N");
		wsaaFoundPstatcode.set("N");
		for (iy.set(1); !(isGT(iy,20)
		|| (foundStatcode.isTrue()
		&& foundPstatcode.isTrue())); iy.add(1)){
			//if (isEQ(chdrlnbIO.getStatcode(),tr597rec.cnRiskStat[iy.toInt()])) {
			if (isEQ(br50hDto.getStatcode(),tr597rec.cnRiskStat[iy.toInt()])) {
				wsaaFoundStatcode.set("Y");
			}
			//if (isEQ(chdrlnbIO.getPstatcode(),tr597rec.cnPremStat[iy.toInt()])) {
			if (isEQ(br50hDto.getPstcde(),tr597rec.cnPremStat[iy.toInt()])) {
				wsaaFoundPstatcode.set("Y");
			}
		}
		if (foundStatcode.isTrue()
		&& foundPstatcode.isTrue()) {
			wsaaSarProposal.set("N");
			goTo(GotoLabel.exit1300);
		}
	}

protected void processComponent1400()
	{
		/*GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin1400();
				}
				case processCovr1400: {
					processCovr1400();
				}
				case exit1400: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}*/
		List<Br50hDTO> br50hCovrCovtList;
		br50hCovrCovtList = br50hTempDao.getUwlmtchlCovtCovrRecords(br50hDto.getChdrcoy(), br50hDto.getChdrnum(), subString(br50hDto.getForenum(), 9, 2).toString());//ILB-1114
		for(Br50hDTO br50h:br50hCovrCovtList) {
			br50hDtoCovtCovr = br50h;
			if(br50hDtoCovtCovr.getRecordtype().equalsIgnoreCase("COVTPF")) {
				loadCovt1500();
			}
			else if(br50hDtoCovtCovr.getRecordtype().equalsIgnoreCase("COVRPF")) {
				loadCovr1600();
			}
		}
	}

protected void begin1400()
	{
		/*covtlnbIO.setDataArea(SPACES);
		covtlnbIO.setStatuz(varcom.oK);
		covtlnbIO.setChdrcoy(clrrIO.getForecoy());
		covtlnbIO.setChdrnum(subString(clrrIO.getForenum(), 1, 8));
		covtlnbIO.setLife(subString(clrrIO.getForenum(), 9, 2));
		covtlnbIO.setCoverage(SPACES);
		covtlnbIO.setRider(SPACES);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFormat(covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		|| isNE(covtlnbIO.getChdrcoy(),clrrIO.getForecoy())
		|| isNE(covtlnbIO.getChdrnum(),subString(clrrIO.getForenum(), 1, 8))
		|| isNE(covtlnbIO.getLife(),subString(clrrIO.getForenum(), 9, 2))) {
			covtlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.processCovr1400);
		}
		while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
			loadCovt1500();
		}*/
		
		goTo(GotoLabel.exit1400);
	}

protected void processCovr1400()
	{
		/*covrincIO.setDataArea(SPACES);
		covrincIO.setStatuz(varcom.oK);
		covrincIO.setChdrcoy(clrrIO.getForecoy());
		covrincIO.setChdrnum(subString(clrrIO.getForenum(), 1, 8));
		covrincIO.setLife(subString(clrrIO.getForenum(), 9, 2));
		covrincIO.setCoverage(SPACES);
		covrincIO.setRider(SPACES);
		covrincIO.setPlanSuffix(ZERO);
		covrincIO.setFormat(covrincrec);
		covrincIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrincIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrincIO.setFitKeysSearch("CHDRCOY");
		SmartFileCode.execute(appVars, covrincIO);
		if (isNE(covrincIO.getStatuz(),varcom.oK)
		&& isNE(covrincIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrincIO.getStatuz());
			syserrrec.params.set(covrincIO.getParams());
			fatalError600();
		}
		if (isNE(covrincIO.getStatuz(),varcom.oK)
		|| isNE(covrincIO.getChdrcoy(),clrrIO.getForecoy())
		|| isNE(covrincIO.getChdrnum(),subString(clrrIO.getForenum(), 1, 8))
		|| isNE(covrincIO.getLife(),subString(clrrIO.getForenum(), 9, 2))) {
			covrincIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1400);
		}
		while ( !(isEQ(covrincIO.getStatuz(),varcom.endp))) {
			loadCovr1600();
		}*/
		
	}

protected void loadCovt1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin1500();
					tsarRelevantPeriod1500();
				}
				case trsa1500: {
					trsa1500();
				}
				case nextCovtlnb1500: {
					nextCovtlnb1500();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1500()
	{
		//wsaaItemKey.itemItemcoy.set(covtlnbIO.getChdrcoy());
		wsaaItemKey.itemItemcoy.set(br50hDtoCovtCovr.getChdrcoy());	
		wsaaItemKey.itemItemtabl.set(tr596);
		//wsaaItemKey.itemItemitem.set(covtlnbIO.getCrtable());
		wsaaItemKey.itemItemitem.set(br50hDtoCovtCovr.getCrtable());
		a1000CallNonDatedTable();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.nextCovtlnb1500);
		}
		tr596rec.tr596Rec.set(itemIO.getGenarea());
	}

protected void tsarRelevantPeriod1500()
	{
		datcon3rec.function.set(SPACES);
		//datcon3rec.intDate1.set(covtlnbIO.getEffdate());
		datcon3rec.intDate1.set(br50hDtoCovtCovr.getEffdate());
		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.frequency.set(freqcpy.mthly);
		a3000CallDatcon3();
		if (isGT(datcon3rec.freqFactor,tr596rec.zrsacprd)) {
			goTo(GotoLabel.nextCovtlnb1500);
		}

		wsaaChdrcoy=br50hDtoCovtCovr.getChdrcoy();
		wsaaSumins.set(br50hDtoCovtCovr.getSumins());
		if (isEQ(wsaaStandFlag,"N")) {
			goTo(GotoLabel.trsa1500);
		}
		/*if (isNE(covtlnbIO.getZlinstprem(),ZERO)) {
			if (isLT(covtlnbIO.getZlinstprem(),ZERO)) {
				compute(wsaaSubSumins, 2).set(mult(covtlnbIO.getZlinstprem(),(-1)));
			}
			else {
				wsaaSubSumins.set(covtlnbIO.getZlinstprem());
			}
		}*/
		if (isNE(br50hDtoCovtCovr.getZlinstprem(),ZERO)) {
			if (isLT(br50hDtoCovtCovr.getZlinstprem(),ZERO)) {
				compute(wsaaSubSumins, 2).set(mult(br50hDtoCovtCovr.getZlinstprem(),(-1)));
			}
			else {
				wsaaSubSumins.set(br50hDtoCovtCovr.getZlinstprem());
			}
		}
	}

protected void trsa1500()
	{
		getAmount1700();
	}

protected void nextCovtlnb1500()
	{
		/*covtlnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		|| isNE(covtlnbIO.getChdrcoy(),clrrIO.getForecoy())
		|| isNE(covtlnbIO.getChdrnum(),subString(clrrIO.getForenum(), 1, 8))
		|| isNE(covtlnbIO.getLife(),subString(clrrIO.getForenum(), 9, 2))) {
			covtlnbIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
	}

protected void loadCovr1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin1600();
					tsarRelevantPeriod1600();
				}
				case trsa1600: {
					trsa1600();
				}
				case nextCovrinc1600: {
					nextCovrinc1600();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1600()
	{
		wsaaFoundStatcode.set("N");
		wsaaFoundPstatcode.set("N");
		for (iy.set(1); !(isGT(iy,20)
		|| (foundStatcode.isTrue()
		&& foundPstatcode.isTrue())); iy.add(1)){
			//if (isEQ(covrincIO.getStatcode(),tr597rec.covRiskStat[iy.toInt()])) {
			if (isEQ(br50hDtoCovtCovr.getStatcode(),tr597rec.covRiskStat[iy.toInt()])) {
				wsaaFoundStatcode.set("Y");
			}
			//if (isEQ(covrincIO.getPstatcode(),tr597rec.covPremStat[iy.toInt()])) {
			if (isEQ(br50hDtoCovtCovr.getPstatcode(),tr597rec.covPremStat[iy.toInt()])) {
				wsaaFoundPstatcode.set("Y");
			}
		}
		if (!foundStatcode.isTrue()
		|| !foundPstatcode.isTrue()) {
			goTo(GotoLabel.nextCovrinc1600);
		}
		//wsaaItemKey.itemItemcoy.set(covrincIO.getChdrcoy());
		wsaaItemKey.itemItemcoy.set(br50hDtoCovtCovr.getChdrcoy());
		wsaaItemKey.itemItemtabl.set(tr596);
		//wsaaItemKey.itemItemitem.set(covrincIO.getCrtable());
		wsaaItemKey.itemItemitem.set(br50hDtoCovtCovr.getCrtable());
		a1000CallNonDatedTable();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.nextCovrinc1600);
		}
		tr596rec.tr596Rec.set(itemIO.getGenarea());
	}

protected void tsarRelevantPeriod1600()
	{
		datcon3rec.function.set(SPACES);
		//datcon3rec.intDate1.set(covrincIO.getCrrcd());
		datcon3rec.intDate1.set(br50hDtoCovtCovr.getCrrcd());
		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.frequency.set(freqcpy.mthly);
		a3000CallDatcon3();
		if (isGT(datcon3rec.freqFactor,tr596rec.zrsacprd)) {
			goTo(GotoLabel.nextCovrinc1600);
		}
		/*wsaaChdrcoy.set(covrincIO.getChdrcoy());
		wsaaSumins.set(covrincIO.getSumins());*/
		wsaaChdrcoy=br50hDtoCovtCovr.getChdrcoy();
		wsaaSumins.set(br50hDtoCovtCovr.getSumins());
		if (isEQ(wsaaStandFlag,"N")) {
			goTo(GotoLabel.trsa1600);
		}
		/*if (isNE(covrincIO.getZlinstprem(),ZERO)) {
			if (isLT(covrincIO.getZlinstprem(),ZERO)) {
				compute(wsaaSubSumins, 2).set(mult(covrincIO.getZlinstprem(),(-1)));
			}
			else {
				wsaaSubSumins.set(covrincIO.getZlinstprem());
			}
		}*/
		if (isNE(br50hDtoCovtCovr.getZlinstprem(),ZERO)) {
			if (isLT(br50hDtoCovtCovr.getZlinstprem(),ZERO)) {
				compute(wsaaSubSumins, 2).set(mult(br50hDtoCovtCovr.getZlinstprem(),(-1)));
			}
			else {
				wsaaSubSumins.set(br50hDtoCovtCovr.getZlinstprem());
			}
		}
	}

protected void trsa1600()
	{
		getAmount1700();
	}

protected void nextCovrinc1600()
	{
		/*covrincIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrincIO);
		if (isNE(covrincIO.getStatuz(),varcom.oK)
		&& isNE(covrincIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrincIO.getStatuz());
			syserrrec.params.set(covrincIO.getParams());
			fatalError600();
		}
		if (isNE(covrincIO.getStatuz(),varcom.oK)
		|| isNE(covrincIO.getChdrcoy(),clrrIO.getForecoy())
		|| isNE(covrincIO.getChdrnum(),subString(clrrIO.getForenum(), 1, 8))
		|| isNE(covrincIO.getLife(),subString(clrrIO.getForenum(), 9, 2))) {
			covrincIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
	}

protected void getAmount1700()
	{
		/*BEGIN*/
		if (isNE(br50hDto.getCntcurr(),tr597rec.cntcurr)) {
			convertCurr1710();
			wsaaSumins.set(conlinkrec.amountOut);
		}
		readTr5931730();
		/*EXIT*/
	}

protected void convertCurr1710()
	{
		begin1710();
	}

protected void begin1710()
	{
		initialize(conlinkrec.clnk002Rec);
		conlinkrec.statuz.set(varcom.oK);
		conlinkrec.function.set("CVRT");
		conlinkrec.company.set(br50hDto.getChdrcoy());
		conlinkrec.currIn.set(br50hDto.getCntcurr());
		conlinkrec.currOut.set(tr597rec.cntcurr);
		conlinkrec.amountIn.set(wsaaSumins);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(wsaaToday);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
	}

protected void readTr5931730()
	{
       List<Itempf> list = itempfDAO.getItempf(smtpfxcpy.item.toString(), wsaaChdrcoy, tr593);
       if(list.isEmpty()) {
    	  return ;
       }
       for(Itempf itemIO:list) {
   		tr593rec.tr593Rec.set(itemIO.getGenarea());
   		wsaaTr593Uwptype.set(itemIO.getItemitem());
   		wsaaTr593CnttypeRec.set(tr593rec.chdrtypes);
   		wsaaFoundCnttype.set("N");
   		wsaaFoundChdrtype.set("N");
   		for (wsaaSub.set(1); !(isGT(wsaaSub,40)
   		|| (foundCnttype.isTrue()
   		&& foundChdrtype.isTrue())); wsaaSub.add(1)){
   			if (isEQ(uwlmtrec.cnttyp,wsaaTr593Chdrtype[wsaaSub.toInt()])) {
   				wsaaFoundCnttype.set("Y");
   			}
   			if (isEQ(br50hDto.getCnttype(),wsaaTr593Chdrtype[wsaaSub.toInt()])) {
   				wsaaFoundChdrtype.set("Y");
   			}
   		}
   		if (foundCnttype.isTrue()
   		&& foundChdrtype.isTrue()) {
   			wsaaTotalSi.add(wsaaSumins);
   			if (isEQ(wsaaStandFlag,"Y")) {
   				wsaaTotalSubsi.add(wsaaSubSumins);
   			}
   			wsaaUwptype.set(wsaaTr593Uwptype);
   			break;
   		}
   	   }
       
	}


protected void getUnderwritingLevel2000()
	{
		try {
			begin2010();
		}
		catch (GOTOException e){
		}
	}

protected void begin2010()
	{
		uwlvIO.setParams(SPACES);
		uwlvIO.setCompany(uwlmtrec.chdrcoy);
		uwlvIO.setUserid(uwlmtrec.userid);
		uwlvIO.setFormat(uwlvrec);
		uwlvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(),varcom.oK)
		&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(uwlvIO.getParams());
			syserrrec.statuz.set(uwlvIO.getStatuz());
			fatalError600();
		}
		if (isEQ(uwlvIO.getStatuz(),varcom.oK)) {
			wsaaUwlevel.set(uwlvIO.getUwlevel());
			goTo(GotoLabel.exit2090);
		}
		usrdIO.setParams(SPACES);
		usrdIO.setCompany(uwlmtrec.chdrcoy);
		usrdIO.setUserid(uwlmtrec.userid);
		usrdIO.setFormat(usrdrec);
		usrdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, usrdIO);
		if (isNE(usrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(usrdIO.getParams());
			syserrrec.statuz.set(usrdIO.getStatuz());
			fatalError600();
		}
		uwlvIO.setParams(SPACES);
		uwlvIO.setCompany(uwlmtrec.chdrcoy);
		uwlvIO.setUserid(usrdIO.getGroupName());
		uwlvIO.setFormat(uwlvrec);
		uwlvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(),varcom.oK)
		&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(uwlvIO.getParams());
			syserrrec.statuz.set(uwlvIO.getStatuz());
			fatalError600();
		}
		if (isEQ(uwlvIO.getStatuz(),varcom.oK)) {
			wsaaUwlevel.set(uwlvIO.getUwlevel());
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(uwlvIO.getStatuz(),varcom.mrnf)) {
			uwlmtrec.statuz.set(p129);
		}
	}

protected void loadTr5942100()
	{
	
		wsaaTr594Found = "N";
		initialize(wsaaTr594Table);
		//ILB-1062 start
		List<Itempf> itempfList = itempfDAO.findItem(smtpfxcpy.item.toString(), 
				uwlmtrec.chdrcoy.toString(), tr594, wsaaUwlevel.toString());
		
		if(!itempfList.isEmpty()){
			for (Itempf itempf : itempfList) {
				tr594rec.tr594Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				for (wsaaIy.set(1); !(isGT(wsaaIy,9)
						|| isEQ(wsaaTr594Found,"Y")); wsaaIy.add(1)){
							if (isEQ(tr594rec.uwplntyp[wsaaIy.toInt()],wsaaUwptype)) {
								wsaaTr594Undlimit.set(tr594rec.undlimit[wsaaIy.toInt()]);
								wsaaTr594Usundlim.set(tr594rec.usundlim[wsaaIy.toInt()]);
								wsaaTr594Found = "Y";
							}
						}
				if (isEQ(wsaaTr594Found,"N")) {
					continue;
				}	
			}
		}
		 //ILB-1062 end
	}

protected void checkLimit2200()
	{
		begin2210();
	}

protected void begin2210()
	{
		if (isGT(wsaaTotalSubsi,wsaaTr594Usundlim)) {
			uwlmtrec.statuz.set("SS");
			uwlmtrec.sumins.set(wsaaTotalSubsi);
			uwlmtrec.undwrlim.set(wsaaTr594Usundlim);
			uwlmtrec.uwlevel.set(wsaaUwlevel);
		}
		else {
			if (isGT(wsaaTotalSi,wsaaTr594Undlimit)) {
				uwlmtrec.statuz.set("S");
				uwlmtrec.sumins.set(wsaaTotalSi);
				uwlmtrec.undwrlim.set(wsaaTr594Undlimit);
				uwlmtrec.uwlevel.set(wsaaUwlevel);
			}
			else {
				uwlmtrec.statuz.set(varcom.oK);
			}
		}
	}

protected void a1000CallNonDatedTable()
	{
		a1000Begin();
	}

protected void a1000Begin()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaItemKey.itemItemcoy);
		itemIO.setItemtabl(wsaaItemKey.itemItemtabl);
		itemIO.setItemitem(wsaaItemKey.itemItemitem);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void a3000CallDatcon3()
	{
		/*A3000-BEGIN*/
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		/*A3000-EXIT*/
	}
}
