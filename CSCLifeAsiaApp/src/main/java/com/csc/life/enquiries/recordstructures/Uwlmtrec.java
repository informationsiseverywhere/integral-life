package com.csc.life.enquiries.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:55
 * Description:
 * Copybook name: UWLMTREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Uwlmtrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(63);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(rec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rec, 4);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rec, 8);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rec, 9);
  	public FixedLengthStringData cnttyp = new FixedLengthStringData(3).isAPartOf(rec, 17);
  	public FixedLengthStringData fsuco = new FixedLengthStringData(1).isAPartOf(rec, 20);
  	public FixedLengthStringData lifcnum = new FixedLengthStringData(8).isAPartOf(rec, 21);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rec, 29);
  	public FixedLengthStringData userid = new FixedLengthStringData(10).isAPartOf(rec, 30);
  	public FixedLengthStringData cownpfx = new FixedLengthStringData(2).isAPartOf(rec, 40);
  	public FixedLengthStringData uwlevel = new FixedLengthStringData(3).isAPartOf(rec, 42);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(rec, 45);
  	public PackedDecimalData undwrlim = new PackedDecimalData(17, 2).isAPartOf(rec, 54);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}