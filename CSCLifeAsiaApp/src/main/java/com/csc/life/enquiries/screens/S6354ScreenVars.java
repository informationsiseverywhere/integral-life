package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6354
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6354ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(1757); //ILIFE-2472 
	//public FixedLengthStringData dataArea = new FixedLengthStringData(1137);
	public FixedLengthStringData dataFields = new FixedLengthStringData(943).isAPartOf(dataArea, 0); //ILIFE-2472
	//public FixedLengthStringData dataFields = new FixedLengthStringData(561).isAPartOf(dataArea, 0);
	public FixedLengthStringData asgnind = DD.asgnind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData bnkcurr = DD.bnkcurr.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,94);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,115);
	public FixedLengthStringData conben = DD.conben.copy().isAPartOf(dataFields,118);
	public FixedLengthStringData ctrsind = DD.ctrsind.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,120);
	
	/*public FixedLengthStringData despaddrs = new FixedLengthStringData(150).isAPartOf(dataFields, 150);
	public FixedLengthStringData[] despaddr = FLSArrayPartOfStructure(5, 30, despaddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(150).isAPartOf(despaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData despaddr01 = DD.despaddr.copy().isAPartOf(filler,0);
	public FixedLengthStringData despaddr02 = DD.despaddr.copy().isAPartOf(filler,30);
	public FixedLengthStringData despaddr03 = DD.despaddr.copy().isAPartOf(filler,60);
	public FixedLengthStringData despaddr04 = DD.despaddr.copy().isAPartOf(filler,90);
	public FixedLengthStringData despaddr05 = DD.despaddr.copy().isAPartOf(filler,120);
	public FixedLengthStringData despname = DD.despname.copy().isAPartOf(dataFields,300);
	public FixedLengthStringData despnum = DD.despnum.copy().isAPartOf(dataFields,347);
	public FixedLengthStringData desppcode = DD.desppcode.copy().isAPartOf(dataFields,355);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,365);
	public FixedLengthStringData fcthsedsc = DD.fcthsedsc.copy().isAPartOf(dataFields,367);
	public FixedLengthStringData grpind = DD.grpind.copy().isAPartOf(dataFields,397);
	public FixedLengthStringData hlndetails = DD.hlndetails.copy().isAPartOf(dataFields,398);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(dataFields,399);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,400);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,447);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,455);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,502);
	public FixedLengthStringData mandref = DD.mandref.copy().isAPartOf(dataFields,510);
	public FixedLengthStringData mandstat = DD.mandstat.copy().isAPartOf(dataFields,515);
	public FixedLengthStringData manstatdsc = DD.manstatdsc.copy().isAPartOf(dataFields,517);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,547);
	public FixedLengthStringData prmdetails = DD.prmdetails.copy().isAPartOf(dataFields,557);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,558);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 561);*/
//ILIFE-2472-BEGIN
	public FixedLengthStringData despaddrs = new FixedLengthStringData(250).isAPartOf(dataFields, 150);
	public FixedLengthStringData[] despaddr = FLSArrayPartOfStructure(5, 50, despaddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(250).isAPartOf(despaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData despaddr01 = DD.despaddrm.copy().isAPartOf(filler,0);
	public FixedLengthStringData despaddr02 = DD.despaddrm.copy().isAPartOf(filler,50);
	public FixedLengthStringData despaddr03 = DD.despaddrm.copy().isAPartOf(filler,100);
	public FixedLengthStringData despaddr04 = DD.despaddrm.copy().isAPartOf(filler,150);
	public FixedLengthStringData despaddr05 = DD.despaddrm.copy().isAPartOf(filler,200);
	// 	MTL-GAP-01 - End
	public FixedLengthStringData despname = DD.despname.copy().isAPartOf(dataFields,400);
	public FixedLengthStringData despnum = DD.despnum.copy().isAPartOf(dataFields,447);
	public FixedLengthStringData desppcode = DD.desppcode.copy().isAPartOf(dataFields,455);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,465);
	public FixedLengthStringData fcthsedsc = DD.fcthsedsc.copy().isAPartOf(dataFields,467);
	public FixedLengthStringData grpind = DD.grpind.copy().isAPartOf(dataFields,497);
	public FixedLengthStringData hlndetails = DD.hlndetails.copy().isAPartOf(dataFields,498);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(dataFields,499);
	public FixedLengthStringData loyaltyind = DD.ind.copy().isAPartOf(dataFields,500);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,501);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,548);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,556);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,603);
	public FixedLengthStringData mandref = DD.mandref.copy().isAPartOf(dataFields,611);
	public FixedLengthStringData mandstat = DD.mandstat.copy().isAPartOf(dataFields,616);
	public FixedLengthStringData manstatdsc = DD.manstatdsc.copy().isAPartOf(dataFields,618);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,648);
	public FixedLengthStringData prmdetails = DD.prmdetails.copy().isAPartOf(dataFields,658);
 // public FixedLengthStringData register = DD.regchannel.copy().isAPartOf(dataFields,658);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,659); //ILIFE-3003
	public FixedLengthStringData zmandref = DD.zmandref.copy().isAPartOf(dataFields,662); //MTL TSD 321 - 5
	public FixedLengthStringData bankaccdsc02 = DD.bankaccdsc.copy().isAPartOf(dataFields,667);//MTL TSD 321 - 30
	public FixedLengthStringData bankacckey02 = DD.bankacckey.copy().isAPartOf(dataFields,697);//MTL TSD 321 - 20
	public FixedLengthStringData bankdesc02 = DD.bankdesc.copy().isAPartOf(dataFields,717);//MTL TSD 321 - 30
	public FixedLengthStringData bankkey02 = DD.bankkey.copy().isAPartOf(dataFields,747);//MTL TSD 321 - 10
	public FixedLengthStringData bnkcurr02 = DD.bnkcurr.copy().isAPartOf(dataFields,757);//MTL TSD 321 - 3
	public FixedLengthStringData mandstat02 = DD.mandstat.copy().isAPartOf(dataFields,760);//MTL TSD 321 - 2
	public FixedLengthStringData manstatdsc02 = DD.manstatdsc.copy().isAPartOf(dataFields,762);//MTL TSD 321 - 30
	public FixedLengthStringData fcthsedsc02 = DD.fcthsedsc.copy().isAPartOf(dataFields,792);//MTL TSD 321 - 30
	
	/* TSD 321 Phase 2 Starts */
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields, 822); 
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields, 830); //length = 47
	/* TSD 321 Phase 2 Ends */
	
	//ILIFE-2472 Starts
	public FixedLengthStringData payor = DD.payrnum.copy().isAPartOf(dataFields, 877); 
	public FixedLengthStringData payrname = DD.payorname.copy().isAPartOf(dataFields, 885); //length = 47
	//ILIFE-2472 Ends
	//ILIFE-7407 Starts
	public FixedLengthStringData ctrycode = DD.ctrycode.copy().isAPartOf(dataFields, 932); 
	public FixedLengthStringData cntryState = DD.cntryState.copy().isAPartOf(dataFields, 935); 
	//ILIFE-7407 Ends
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(202).isAPartOf(dataArea, 943);
//ILIFE-2472-END
	
	public FixedLengthStringData asgnindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bnkcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData conbenErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctrsindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData despaddrsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] despaddrErr = FLSArrayPartOfStructure(5, 4, despaddrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(despaddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData despaddr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData despaddr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData despaddr03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData despaddr04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData despaddr05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData despnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData despnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData desppcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData fcthsedscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData grpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData hlndetailsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData mandrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData mandstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData manstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData prmdetailsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(432).isAPartOf(dataArea, 705);
//ILIFE-2472-BEGIN
	public FixedLengthStringData zmandrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144); //MTL TSD 321
	public FixedLengthStringData bankaccdsc02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 146); //MTL TSD 321
	public FixedLengthStringData bankacckey02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 150); //MTL TSD 321
	public FixedLengthStringData bankdesc02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 154); //MTL TSD 321
	public FixedLengthStringData bankkey02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 158); //MTL TSD 321
	public FixedLengthStringData bnkcurr02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 162); //MTL TSD 321
	public FixedLengthStringData mandstat02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 166); //MTL TSD 321
	public FixedLengthStringData manstatdsc02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 170); //MTL TSD 321
	public FixedLengthStringData fcthsedsc02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 174); //MTL TSD 321
	
	/* TSD 321 Phase 2 Starts */
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 178);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 182);
	/* TSD 321 Phase 2 Ends */
	
	/* TSD 321 Phase 2 Starts */
	public FixedLengthStringData payrnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 186);
	public FixedLengthStringData payorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 190);
	/* TSD 321 Phase 2 Ends */
	//ILIFE-7407
	public FixedLengthStringData ctrycodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 194);
	public FixedLengthStringData cntryStateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 198);
	//ILIFE-7407
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(612).isAPartOf(dataArea, 1145);
//ILIFE-2472-END
	public FixedLengthStringData[] asgnindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bnkcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] conbenOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctrsindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData despaddrsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] despaddrOut = FLSArrayPartOfStructure(5, 12, despaddrsOut, 0);
	public FixedLengthStringData[][] despaddrO = FLSDArrayPartOfArrayStructure(12, 1, despaddrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(despaddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] despaddr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] despaddr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] despaddr03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] despaddr04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] despaddr05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] despnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] despnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] desppcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] fcthsedscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] grpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] hlndetailsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] mandrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] mandstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] manstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] prmdetailsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
//ILIFE-2472-BEGIN
	public FixedLengthStringData[] zmandrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432); //MTL TSD 321
	public FixedLengthStringData[] bankaccdsc02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 444); //MTL TSD 321
	public FixedLengthStringData[] bankacckey02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 456); //MTL TSD 321
	public FixedLengthStringData[] bankdesc02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 468); //MTL TSD 321
	public FixedLengthStringData[] bankkey02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 480); //MTL TSD 321
	public FixedLengthStringData[] bnkcurr02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 492); //MTL TSD 321
	public FixedLengthStringData[] mandstat02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 504); //MTL TSD 321
	public FixedLengthStringData[] manstatdsc02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 516); //MTL TSD 321
	public FixedLengthStringData[] fcthsedsc02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 528); //MTL TSD 321
	
	/* TSD 321 Phase 2 Starts */
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	/* TSD 321 Phase 2 Starts */
	public FixedLengthStringData[] payorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] payrnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
//ILIFE-2472-END
	//ILIFE-7407
	public FixedLengthStringData[] ctrycodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] cntryStateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	//ILIFE-7407
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6354screenWritten = new LongData(0);
	public LongData S6354protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6354ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
//ILIFE-2472-BEGIN
		/* TSD 321 Phase 2 Starts */
		fieldIndMap.put(payornameOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payrnumOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		/* TSD 321 Phase 2 Starts */
//ILIFE-2472-END
		fieldIndMap.put(payorOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payrnameOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		fieldIndMap.put(conbenOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(asgnindOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmdetailsOut,new String[] {"13","14","-13","14",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hlndetailsOut,new String[] {"15","16","-15","16",null, null, null, null, null, null, null, null});
		fieldIndMap.put(grpindOut,new String[] {"12",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctrsindOut,new String[] {"13",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(indOut,new String[] {"17","18","-17","18",null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(ctrycodeOut,new String[] {null,null,null,"20",null, null, null, null, null, null, null, null});//ILIFE-7407
		fieldIndMap.put(cntryStateOut,new String[] {null,null,null,"21",null, null, null, null, null, null, null, null});//ILIFE-7407
		//screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, mandref, facthous, fcthsedsc, bankkey, bankdesc, bankacckey, bankaccdsc, bnkcurr, despnum, despname, despaddr01, despaddr02, despaddr03, despaddr04, despaddr05, desppcode, conben, asgnind, prmdetails, hlndetails, mandstat, manstatdsc, grpind, ctrsind, ind};
		//screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, mandrefOut, facthousOut, fcthsedscOut, bankkeyOut, bankdescOut, bankacckeyOut, bankaccdscOut, bnkcurrOut, despnumOut, despnameOut, despaddr01Out, despaddr02Out, despaddr03Out, despaddr04Out, despaddr05Out, desppcodeOut, conbenOut, asgnindOut, prmdetailsOut, hlndetailsOut, mandstatOut, manstatdscOut, grpindOut, ctrsindOut, indOut};
		//screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, mandrefErr, facthousErr, fcthsedscErr, bankkeyErr, bankdescErr, bankacckeyErr, bankaccdscErr, bnkcurrErr, despnumErr, despnameErr, despaddr01Err, despaddr02Err, despaddr03Err, despaddr04Err, despaddr05Err, desppcodeErr, conbenErr, asgnindErr, prmdetailsErr, hlndetailsErr, mandstatErr, manstatdscErr, grpindErr, ctrsindErr, indErr};
//ILIFE-2472-BEGIN		
		screenFields = new BaseData[] {payrnum, payorname, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, mandref, facthous, fcthsedsc, bankkey, bankdesc, bankacckey, bankaccdsc, bnkcurr, despnum, despname, despaddr01, despaddr02, despaddr03, despaddr04, despaddr05, desppcode, conben, asgnind, prmdetails, hlndetails, mandstat, manstatdsc, grpind, ctrsind, ind,bankaccdsc02,bankaccdsc02,bankdesc02,bankkey02,bnkcurr02,mandstat02,manstatdsc02,fcthsedsc02, payor, payrname, ctrycode, cntryState};
		screenOutFields = new BaseData[][] {payrnumOut, payornameOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, mandrefOut, facthousOut, fcthsedscOut, bankkeyOut, bankdescOut, bankacckeyOut, bankaccdscOut, bnkcurrOut, despnumOut, despnameOut, despaddr01Out, despaddr02Out, despaddr03Out, despaddr04Out, despaddr05Out, desppcodeOut, conbenOut, asgnindOut, prmdetailsOut, hlndetailsOut, mandstatOut, manstatdscOut, grpindOut, ctrsindOut, indOut,bankaccdsc02Out,bankaccdsc02Out,bankdesc02Out,bankkey02Out,bnkcurr02Out,mandstat02Out,manstatdsc02Out,fcthsedsc02Out,payorOut,payrnameOut, ctrycodeOut, cntryStateOut};
		screenErrFields = new BaseData[] {payrnumErr, payornameErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, mandrefErr, facthousErr, fcthsedscErr, bankkeyErr, bankdescErr, bankacckeyErr, bankaccdscErr, bnkcurrErr, despnumErr, despnameErr, despaddr01Err, despaddr02Err, despaddr03Err, despaddr04Err, despaddr05Err, desppcodeErr, conbenErr, asgnindErr, prmdetailsErr, hlndetailsErr, mandstatErr, manstatdscErr, grpindErr, ctrsindErr, indErr,bankaccdsc02Err,bankaccdsc02Err,bankdesc02Err,bankkey02Err,bnkcurr02Err,mandstat02Err,manstatdsc02Err,fcthsedsc02Err,payorErr,payrnameErr, ctrycodeErr, cntryStateErr};
//ILIFE-2472-END		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6354screen.class;
		protectRecord = S6354protect.class;
	}

}
