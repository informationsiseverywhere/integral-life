/*
 * File: P5122.java
 * Date: 30 August 2009 0:09:19
 * Author: Quipoz Limited
 * 
 * Class transformed from P5122.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S5122ScreenVars;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Clear the subfile ready for loading.
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status, (STATCODE)  -  short  description  from
*          T3623,
*
*          Premium Status, (PSTATCODE)  -  short  description  from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's client (CLTS) details if they exist.
*
*          Retrive  the   stored  COVRENQ  details  to  obtain  the
*          Coverage number and Rider numbers for display.
*
*     Load the subfile as follows:
*
*     Retrieve the first UTRS  details that have been stored in the
*     UTRS I/O module.  Display the Plan Suffix, Fund and Fund Type
*     from the retrieved  details. Obtain the Fund description from
*     T5515 and the Unit  Type  description from T6649. If the Plan
*     Suffix is not  greater than the number of policies summarised
*     from the Contract Header replace the Plan Suffix on UTRS with
*     zero before processing.
*
*     Perform a BEGN on  UTRN to position at the first UTRN record.
*     Store the 'Real' Plan Suffix that is being processed. Display
*     the Fund Currency from  the  first  UTRN record. Read all the
*     UTRN records  for  the  Company,  Contract,  Life, Component,
*     stored Plan Suffix, Fund and Fund Type displaying the details
*     from each record.
*
*
*     If Plan level processing  is in operation or the policy being
*     processed is a  non-summarised policy then display the Number
*     of Units and the  Number  of Deemed Units as found, otherwise
*     perform 'break out' processing as follows:
*
*          If the notional  policy  being processed is not '1' then
*          divide the numbers by the number of policies summarised.
*          If it is '1' then absorb any rounding discrepancies with
*          the formula:
*
*              Number = (Number -(POLSUM - 1 )
*                                ~~~~~~~~~~~~~
*                                   POLSUM
*
*
*     Load all pages required in  the  subfile  and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Add  1  to  the program pointer and exit.
*
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1688 - Transaction Codes                 Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5515 - Virtual Funds                     Key: VRTFND
* T5688 - Contract Structure                Key: CNTTYPE
* T6649 - Unit Types                        Key: Unit Type.
*
*****************************************************************
* </pre>
*/
public class P5122 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5122");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNumber = new ZonedDecimalData(5, 0);
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(3, 0);
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaMessages = new FixedLengthStringData(300);
	private FixedLengthStringData[] wsaaMessage = FLSArrayPartOfStructure(6, 50, wsaaMessages, 0);


	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5515 = "T5515";
	private static final String t5688 = "T5688";
	private static final String t6649 = "T6649";
	private static final String tr386 = "TR386";
	private static final String itemrec = "ITEMREC";
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	//private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Tr386rec tr386rec = new Tr386rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S5122ScreenVars sv = ScreenProgram.getScreenVars( S5122ScreenVars.class);
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO",HitspfDAO.class);
	private Hitspf hitspf = new Hitspf();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		writeSubfile1150, 
		readNext1160
	}

	public P5122() {
		super();
		screenVars = sv;
		new ScreenModel("S5122", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5122", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Set screen fields*/
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf=chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());
		if (isEQ(scrnparams.deviceInd,"*RMT")) {
			if (isEQ(chdrpf.getPolinc(),ZERO)
			|| isEQ(chdrpf.getPolinc(),1)) {
				wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("L");
			}
			else {
				wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("O");
			}
		}
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrpf = covrDao.getCacheObject(covrpf);
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.ind02Out[varcom.nd.toInt()].set("Y");
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setStatuz(varcom.oK);
		iy.set(1);
		initialize(wsaaMessages);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			loadTr3861200();
		}
		
		if (isEQ(wssplife.unitType,"D")) {
			sv.zvar01.set(wsaaMessage[4]);
			sv.zvar02.set(wsaaMessage[5]);
			sv.zvariable.set(wsaaMessage[6]);
			sv.ind01Out[varcom.nd.toInt()].set("Y");
			a100InterestBearing();
			return ;
		}
		sv.ind01Out[varcom.nd.toInt()].set("N");
		sv.zvar01.set(wsaaMessage[1]);
		sv.zvar02.set(wsaaMessage[2]);
		sv.zvariable.set(wsaaMessage[3]);
		utrsIO.setDataArea(SPACES);
		utrsIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		sv.planSuffix.set(utrsIO.getPlanSuffix());
		sv.unitVirtualFund.set(utrsIO.getUnitVirtualFund());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(utrsIO.getUnitVirtualFund());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundShortDesc.fill("?");
		}
		else {
			sv.fundShortDesc.set(descIO.getShortdesc());
		}
		sv.fundtype.set(utrsIO.getUnitType());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6649);
		descIO.setDescitem(utrsIO.getUnitType());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundTypeShortDesc.fill("?");
		}
		else {
			//ILIFE-1402 STARTS
			sv.fundTypeShortDesc.set(descIO.getLongdesc());
			//ILIFE-1402 ENDS
		}
		utrnIO.setChdrcoy(utrsIO.getChdrcoy());
		utrnIO.setChdrnum(utrsIO.getChdrnum());
		utrnIO.setLife(utrsIO.getLife());
		utrnIO.setCoverage(utrsIO.getCoverage());
		utrnIO.setRider(utrsIO.getRider());
		if (isLTE(utrsIO.getPlanSuffix(),chdrpf.getPolsum())) {
			utrnIO.setPlanSuffix(ZERO);
		}
		else {
			utrnIO.setPlanSuffix(utrsIO.getPlanSuffix());
		}
		utrnIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		utrnIO.setUnitType(utrsIO.getUnitType());
		utrnIO.setTranno(ZERO);
		utrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnIO.setFitKeysSearch("LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError600();
		}
		sv.fundCurrency.set(utrnIO.getFundCurrency());
		/*    Store the suffix that is actually being used to read UTRN.*/
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(utrnIO.getPlanSuffix());
		wsaaRem.set(1);
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			while ( !(isNE(utrnIO.getChdrcoy(),wsspcomn.company)
			|| isNE(utrnIO.getChdrnum(),chdrpf.getChdrnum())
			|| isNE(utrnIO.getLife(),utrsIO.getLife())
			|| isNE(utrnIO.getCoverage(),utrsIO.getCoverage())
			|| isNE(utrnIO.getRider(),utrsIO.getRider())
			|| isEQ(utrnIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaRem,0))) {
				processUtrn1100();
			}
			
		}
		else {
			while ( !(isNE(utrnIO.getChdrcoy(),wsspcomn.company)
			|| isNE(utrnIO.getChdrnum(),chdrpf.getChdrnum())
			|| isNE(utrnIO.getLife(),utrsIO.getLife())
			|| isNE(utrnIO.getCoverage(),utrsIO.getCoverage())
			|| isNE(utrnIO.getRider(),utrsIO.getRider())
			|| isNE(utrnIO.getPlanSuffix(), wsaaMiscellaneousInner.wsaaPlanSuffix)
			|| isNE(utrnIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund())
			|| isNE(utrnIO.getUnitType(),utrsIO.getUnitType())
			|| isEQ(utrnIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaRem,0))) {
				processUtrn1100();
			}
			
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void processUtrn1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1100();
				case writeSubfile1150: 
					writeSubfile1150();
				case readNext1160: 
					readNext1160();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1100()
	{
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			if (isNE(utrnIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund())
			|| isNE(utrnIO.getUnitType(),utrsIO.getUnitType())) {
				goTo(GotoLabel.readNext1160);
			}
		}
		sv.subfileFields.set(SPACES);
		sv.moniesDate.set(utrnIO.getMoniesDate());
		sv.nowDeferInd.set(utrnIO.getNowDeferInd());
		sv.feedbackInd.set(utrnIO.getFeedbackInd());
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()
		|| isGT(sv.planSuffix,chdrpf.getPolsum())) {
			sv.nofUnits.set(utrnIO.getNofUnits());
			sv.nofDunits.set(utrnIO.getNofDunits());
			sv.fundAmount.set(utrnIO.getFundAmount());
			goTo(GotoLabel.writeSubfile1150);
		}
		if (isEQ(utrnIO.getPlanSuffix(),1)) {
			compute(wsaaMiscellaneousInner.wsaaNofUnits, 5).set(sub(utrnIO.getNofUnits(), (div(mult(utrnIO.getNofUnits(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaNofDunits, 5).set(sub(utrnIO.getNofDunits(), (div(mult(utrnIO.getNofDunits(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaFundAmnt, 2).set(sub(utrnIO.getFundAmount(), (div(mult(utrnIO.getFundAmount(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaNofUnits, 5).set(div(utrnIO.getNofUnits(), chdrpf.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaNofDunits, 5).set(div(utrnIO.getNofDunits(), chdrpf.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaFundAmnt, 2).set(div(utrnIO.getFundAmount(), chdrpf.getPolsum()));
		}
		zrdecplrec.amountIn.set(wsaaMiscellaneousInner.wsaaFundAmnt);
		callRounding5000();
		wsaaMiscellaneousInner.wsaaFundAmnt.set(zrdecplrec.amountOut);
		sv.nofUnits.set(wsaaMiscellaneousInner.wsaaNofUnits);
		sv.nofDunits.set(wsaaMiscellaneousInner.wsaaNofDunits);
		sv.fundAmount.set(wsaaMiscellaneousInner.wsaaFundAmnt);
	}

protected void writeSubfile1150()
	{
		sv.proctrancd.set(utrnIO.getBatctrcde());
		sv.tranno.set(utrnIO.getTranno());
		if (isNE(utrnIO.getPriceDateUsed(),ZERO))
			sv.pricdte.set(utrnIO.getPriceDateUsed());
		else
			sv.priceDateDisp.set(SPACES);
		
		sv.batctrcde.set(utrnIO.getBatctrcde());
		sv.priceUsed.set(utrnIO.getPriceUsed());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5122", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNext1160()
	{
		utrnIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)
		&& isNE(utrnIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError600();
		}
		if (isEQ(scrnparams.deviceInd,"*RMT")) {
			return ;
		}
		//MIBT-82
//		compute(wsaaNumber, 0).setDivide(scrnparams.subfileRrn, (12));
		compute(wsaaNumber, 0).setDivide(scrnparams.subfileRrn, (49));
		wsaaRem.setRemainder(wsaaNumber);
		scrnparams.subfileMore.set(SPACES);
		if (isEQ(wsaaRem,0)) {
			if (isNE(utrnIO.getStatuz(),varcom.endp)) {
				scrnparams.subfileMore.set("Y");
			}
		}
	}

protected void loadTr3861200()
	{
			start1210();
		}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(),wsspcomn.company)
		|| isNE(itemIO.getItemtabl(),tr386)
		|| isNE(itemIO.getItemitem(),wsaaTr386Key)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			if (isEQ(itemIO.getFunction(),varcom.begn)) {
				itemIO.setItemitem(wsaaTr386Key);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		ix.set(1);
		while ( !(isGT(ix,10))) {
			if (isNE(tr386rec.progdesc[ix.toInt()],SPACES)
			&& isLT(iy,7)) {
				wsaaMessage[iy.toInt()].set(tr386rec.progdesc[ix.toInt()]);
				iy.add(1);
			}
			ix.add(1);
		}
		
		itemIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
			screenIo2010();
		}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaRem.set(1);
			if (isEQ(wssplife.unitType,"D")) {
				while ( !(isNE(hitrIO.getChdrcoy(),wsspcomn.company)
				|| isNE(hitrIO.getChdrnum(),chdrpf.getChdrnum())
				|| isNE(hitrIO.getLife(),hitspf.getLife())
				|| isNE(hitrIO.getCoverage(),hitspf.getCoverage())
				|| isNE(hitrIO.getRider(),hitspf.getRider())
				|| isNE(hitrIO.getPlanSuffix(),hitspf.getPlanSuffix())
				|| isNE(hitrIO.getZintbfnd(),hitspf.getZintbfnd())
				|| isEQ(hitrIO.getStatuz(),varcom.endp)
				|| isEQ(wsaaRem,0))) {
					a200ProcessHitr();
				}
				
				wsspcomn.edterror.set(SPACES);
				return ;
			}
			else {
				if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
					while ( !(isNE(utrnIO.getChdrcoy(),wsspcomn.company)
					|| isNE(utrnIO.getChdrnum(),chdrpf.getChdrnum())
					|| isNE(utrnIO.getLife(),utrsIO.getLife())
					|| isNE(utrnIO.getCoverage(),utrsIO.getCoverage())
					|| isNE(utrnIO.getRider(),utrsIO.getRider())
					|| isEQ(utrnIO.getStatuz(),varcom.endp)
					|| isEQ(wsaaRem,0))) {
						processUtrn1100();
					}
					
					wsspcomn.edterror.set(SPACES);
					return ;
				}
				else {
					while ( !(isNE(utrnIO.getChdrcoy(),wsspcomn.company)
					|| isNE(utrnIO.getChdrnum(),chdrpf.getChdrnum())
					|| isNE(utrnIO.getLife(),utrsIO.getLife())
					|| isNE(utrnIO.getCoverage(),utrsIO.getCoverage())
					|| isNE(utrnIO.getRider(),utrsIO.getRider())
					|| isNE(utrnIO.getPlanSuffix(), wsaaMiscellaneousInner.wsaaPlanSuffix)
					|| isNE(utrnIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund())
					|| isNE(utrnIO.getUnitType(),utrsIO.getUnitType())
					|| isEQ(utrnIO.getStatuz(),varcom.endp)
					|| isEQ(wsaaRem,0))) {
						processUtrn1100();
					}
					
					wsspcomn.edterror.set(SPACES);
					return ;
				}
			}
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  No database updates are required.*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*PARA*/
		/*    Release any UTRS record that may have been stored.*/
		/*MOVE RLSE                   TO UTRS-FUNCTION.*/
		/*CALL 'UTRSIO'         USING UTRS-PARAMS.*/
		/*IF   UTRS-STATUZ      NOT = O-K*/
		/*     MOVE UTRS-PARAMS    TO SYSR-PARAMS*/
		/*     PERFORM 600-FATAL-ERROR.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.fundCurrency);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a100InterestBearing()
	{
		a110Intb();
	}

protected void a110Intb()
	{
		hitspf = hitspfDAO.getCacheObject(hitspf);
		sv.planSuffix.set(hitspf.getPlanSuffix());
		sv.unitVirtualFund.set(hitspf.getZintbfnd());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(hitspf.getZintbfnd());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundShortDesc.fill("?");
		}
		else {
			sv.fundShortDesc.set(descIO.getShortdesc());
		}
		sv.fundtype.set("D");
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6649);
		descIO.setDescitem(sv.fundtype);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundTypeShortDesc.fill("?");
		}
		else {
			//ILIFE-1402 STARTS
			sv.fundTypeShortDesc.set(descIO.getLongdesc());
			//ILIFE-1402 ENDS
		}
		hitrIO.setParams(SPACES);
		hitrIO.setChdrcoy(hitspf.getChdrcoy());
		hitrIO.setChdrnum(hitspf.getChdrnum());
		hitrIO.setLife(hitspf.getLife());
		hitrIO.setCoverage(hitspf.getCoverage());
		hitrIO.setRider(hitspf.getRider());
		hitrIO.setPlanSuffix(hitspf.getPlanSuffix());
		hitrIO.setZintbfnd(hitspf.getZintbfnd());
		hitrIO.setTranno(ZERO);
		hitrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrIO.setFitKeysSearch("LIFE", "COVERAGE", "RIDER", "PLNSFX", "ZINTBFND");
		
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			fatalError600();
		}
		sv.fundCurrency.set(hitrIO.getFundCurrency());
		wsaaRem.set(1);
		while ( !(isNE(hitrIO.getChdrcoy(),wsspcomn.company)
		|| isNE(hitrIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(hitrIO.getLife(),hitspf.getLife())
		|| isNE(hitrIO.getCoverage(),hitspf.getCoverage())
		|| isNE(hitrIO.getRider(),hitspf.getRider())
		|| isNE(hitrIO.getPlanSuffix(),hitspf.getPlanSuffix())
		|| isNE(hitrIO.getZintbfnd(),hitspf.getZintbfnd())
		|| isEQ(hitrIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaRem,0))) {
			a200ProcessHitr();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void a200ProcessHitr()
	{
		a210Para();
		a260ReadNext();
	}

protected void a210Para()
	{
		sv.subfileFields.set(SPACES);
		sv.moniesDate.set(hitrIO.getEffdate());
		sv.nowDeferInd.set(hitrIO.getZintappind());
		sv.feedbackInd.set(hitrIO.getFeedbackInd());
		sv.nofUnits.set(ZERO);
		sv.nofDunits.set(ZERO);
		sv.priceUsed.set(ZERO);
		sv.batctrcde.set(hitrIO.getBatctrcde());
		sv.fundAmount.set(hitrIO.getFundAmount());
		
		sv.proctrancd.set(hitrIO.getBatctrcde());
		sv.tranno.set(hitrIO.getTranno());
		sv.pricdte.set(ZERO);
		
		scrnparams.function.set(varcom.sadd);
		processScreen("S5122", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void a260ReadNext()
	{
		hitrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)
		&& isNE(hitrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError600();
		}
		compute(wsaaNumber, 0).setDivide(scrnparams.subfileRrn, (12));
		wsaaRem.setRemainder(wsaaNumber);
		scrnparams.subfileMore.set(SPACES);
		if (isEQ(wsaaRem,0)
		&& isNE(hitrIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		/*A290-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	private FixedLengthStringData wsaaUtrsComponents = new FixedLengthStringData(15);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaUtrsComponents, 6).setUnsigned();
	private PackedDecimalData wsaaNofUnits = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaNofDunits = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaFundAmnt = new PackedDecimalData(17, 5);
}
}
