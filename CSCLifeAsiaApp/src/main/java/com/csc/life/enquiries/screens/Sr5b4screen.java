package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr5b4screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {2, 7, 2, 53}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr5b4ScreenVars sv = (Sr5b4ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr5b4screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr5b4ScreenVars screenVars = (Sr5b4ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.tranamt01.setClassString("");
		screenVars.tranamt02.setClassString("");
		screenVars.tranamt03.setClassString("");
		screenVars.tranamt04.setClassString("");
		screenVars.nlgbal.setClassString("");
	}

/**
 * Clear all the variables in Sr5b4screen
 */
	public static void clear(VarModel pv) {
		Sr5b4ScreenVars screenVars = (Sr5b4ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.tranamt01.clear();
		screenVars.tranamt02.clear();
		screenVars.tranamt03.clear();
		screenVars.tranamt04.clear();
		screenVars.nlgbal.clear();
		
	}
}
