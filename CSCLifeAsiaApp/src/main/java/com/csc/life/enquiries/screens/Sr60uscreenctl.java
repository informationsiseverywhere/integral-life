package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr60uscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr60uscreensfl";
		lrec.subfileClass = Sr60uscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 14;
		lrec.pageSubfile = 13;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 8, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr60uScreenVars sv = (Sr60uScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr60uscreenctlWritten, sv.Sr60uscreensflWritten, av, sv.sr60uscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr60uScreenVars screenVars = (Sr60uScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.clntname.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.schmno.setClassString("");
		screenVars.schmnme.setClassString("");
	}

/**
 * Clear all the variables in Sr60uscreenctl
 */
	public static void clear(VarModel pv) {
		Sr60uScreenVars screenVars = (Sr60uScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.clntnum.clear();
		screenVars.clntname.clear();
		screenVars.chdrnum.clear();
		screenVars.ctypedes.clear();
		screenVars.schmno.clear();
		screenVars.schmnme.clear();
	}
}
