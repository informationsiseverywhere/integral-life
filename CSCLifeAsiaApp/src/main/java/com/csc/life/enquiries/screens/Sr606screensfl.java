package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr606screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 17;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 20, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr606ScreenVars sv = (Sr606ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr606screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr606screensfl, 
			sv.Sr606screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr606ScreenVars sv = (Sr606ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr606screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr606ScreenVars sv = (Sr606ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr606screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr606screensflWritten.gt(0))
		{
			sv.sr606screensfl.setCurrentIndex(0);
			sv.Sr606screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr606ScreenVars sv = (Sr606ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr606screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr606ScreenVars screenVars = (Sr606ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rdocnum.setFieldName("rdocnum");
				screenVars.trandateDisp.setFieldName("trandateDisp");
				screenVars.chqnum.setFieldName("chqnum");
				screenVars.zdesc.setFieldName("zdesc");
				screenVars.acctamt.setFieldName("acctamt");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.rdocnum.set(dm.getField("rdocnum"));
			screenVars.trandateDisp.set(dm.getField("trandateDisp"));
			screenVars.chqnum.set(dm.getField("chqnum"));
			screenVars.zdesc.set(dm.getField("zdesc"));
			screenVars.acctamt.set(dm.getField("acctamt"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr606ScreenVars screenVars = (Sr606ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rdocnum.setFieldName("rdocnum");
				screenVars.trandateDisp.setFieldName("trandateDisp");
				screenVars.chqnum.setFieldName("chqnum");
				screenVars.zdesc.setFieldName("zdesc");
				screenVars.acctamt.setFieldName("acctamt");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("rdocnum").set(screenVars.rdocnum);
			dm.getField("trandateDisp").set(screenVars.trandateDisp);
			dm.getField("chqnum").set(screenVars.chqnum);
			dm.getField("zdesc").set(screenVars.zdesc);
			dm.getField("acctamt").set(screenVars.acctamt);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr606screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr606ScreenVars screenVars = (Sr606ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.rdocnum.clearFormatting();
		screenVars.trandateDisp.clearFormatting();
		screenVars.chqnum.clearFormatting();
		screenVars.zdesc.clearFormatting();
		screenVars.acctamt.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr606ScreenVars screenVars = (Sr606ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.rdocnum.setClassString("");
		screenVars.trandateDisp.setClassString("");
		screenVars.chqnum.setClassString("");
		screenVars.zdesc.setClassString("");
		screenVars.acctamt.setClassString("");
	}

/**
 * Clear all the variables in Sr606screensfl
 */
	public static void clear(VarModel pv) {
		Sr606ScreenVars screenVars = (Sr606ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.rdocnum.clear();
		screenVars.trandateDisp.clear();
		screenVars.trandate.clear();
		screenVars.chqnum.clear();
		screenVars.zdesc.clear();
		screenVars.acctamt.clear();
	}
}
