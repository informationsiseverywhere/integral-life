/*
 * File: P6234.java
 * Date: 30 August 2009 0:37:36
 * Author: Quipoz Limited
 * 
 * Class transformed from P6234.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.List;

import com.csc.fsu.accounting.dataaccess.dao.RtrnpfDAO;
/*import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;*/
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.impl.ClntpfDAOImpl;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
//import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.enquiries.dataaccess.AcmvtrnTableDAM;
/*import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.RtrntrnTableDAM;*/
import com.csc.life.enquiries.procedures.Acmvtrncp;
import com.csc.life.enquiries.screens.S6234ScreenVars;
//import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.LifepfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
/*import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;*/
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Rtrnpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
//import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this  section  if  returning  from an optional selectio 
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will b 
*     stored  in  the  CHDRENQ I/O module. Retrieve the details an 
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description fro 
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description fro 
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint  owner's  client (CLTS) details if they exist 
*
*
*     The details  of  the  transaction  selected from the previou 
*     screen will be  in the PTRNENQ I/O module. Perform a RETRV o 
*     this module to obtain the corresponding PTRN record.  Displa 
*     the  Effective   Date  (PTRNEFF)  and  the  Transaction  Cod 
*     (BATCTRCDE) from  the  PTRN  record in the heading portion o 
*     the screen  and decode the transaction code against T1688 fo 
*     its long description.
*
*     The details  that  will  be  displayed  will  come  from  tw 
*     data-sets: RTRNTRN and ACMVTRN.  The same information will b 
*     extracted from both  data-sets  and they will both be read i 
*     the same sequence.
*
*     This program  should process all the relevant ACMVTRN record 
*     and then all the relevant RTRNTRN records.
*
*     Load the subfile as follows:
*
*          From  the   PTRNENQ  record  take  the  fields  CHDRCOY 
*          CHDRNUM, BATCTRCDE and TRANNO and read ACMVTRN with thi 
*          key until end  of  file  or  the  any portion of the ke 
*          changes. Note  that  the  Contract Number, (CHDRNUM), i 
*          called RDOCNUM on ACMVTRN.
*
*          Display the  G/L  code  (GLCODE), SACS Code and Type an 
*          their  short  descriptions,  Original  Amount (ORIGAMT) 
*          Original  Amount  Currency  (ORIGCCY), Accounting Amoun 
*          (ACCTAMT), and Accounting Amount Currency (GENLCUR).
*
*          Read RTRNTRN with th esame key until any portion change 
*          and display  the  same  fields. Note that the Accountin 
*          Currency is called ACCTCCY on the RTRNTRN record.
*
*
*     Load all pages  required  in  the subfile and set the subfil 
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Add 1 to the program pointer and exit.
*
*
*
* Notes.
* ------
*
*     Create a new  view  of  RTRNPF called RTRNTRN which uses onl 
*     those fields  required for this program and keyed on Contrac 
*     Company, Batch Code, Contract Number and Transaction Number.
*
*     Create a new  view  of  ACMVPF called ACMVTRN which uses onl 
*     those fields  required for this program and keyed on Contrac 
*     Company, Batch Code, Contract Number and Transaction number.
*
*     Tables Used:
*
* T1688 - Transaction Codes                 Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6234 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6234");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private AcmvtrnTableDAM acmvtrnIO = new AcmvtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6234ScreenVars sv = ScreenProgram.getScreenVars( S6234ScreenVars.class);
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private AcmvpfDAO acmvDao = getApplicationContext().getBean("acmvpfDAO",AcmvpfDAO.class);
	private List<Acmvpf> acmvList;
	private PtrnpfDAO ptrnDao = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
	private Ptrnpf ptrnpf = new Ptrnpf();
	private RtrnpfDAO rtrnDao = getApplicationContext().getBean("rtrnpfDAO",RtrnpfDAO.class);
	private List<Rtrnpf> rtrnList;
	

	private LifepfDAO lifeDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAOImpl.class);
	private String occupation;
	private String jLife;
	private String life;
	DescDAO descpfDAO =  getApplicationContext().getBean("descDAO",DescDAO.class);;
	Descpf descpf = new Descpf();
	List<Lifepf> lifeList;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAOImpl.class);
	private Clntpf clntpf = new Clntpf();
	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		rtrn1050, 
		exit1090
	}

	public P6234() {
		super();
		screenVars = sv;
		new ScreenModel("S6234", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}




	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
	protected void initialise1000() {
		initialise1010();
	}


protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.effdate.set(ZERO);
		sv.hmhii.set(ZERO);
		sv.hmhli.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6234", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		/* MOVE CHDRENQ-CHDRCOY        TO LIFEENQ-CHDRCOY.              */
		/* MOVE CHDRENQ-CHDRNUM        TO LIFEENQ-CHDRNUM.              */
		/* MOVE '01'                   TO LIFEENQ-LIFE.                 */
		/* MOVE '00'                   TO LIFEENQ-JLIFE.                */
		/* MOVE BEGN                   TO LIFEENQ-FUNCTION.             */
		/* CALL 'LIFEENQIO'            USING LIFEENQ-PARAMS.            */
		/* IF LIFEENQ-STATUZ           NOT = O-K                        */
		/*      MOVE LIFEENQ-PARAMS    TO SYSR-PARAMS                   */
		/*      PERFORM 600-FATAL-ERROR.                                */
		/* MOVE LIFEENQ-LIFCNUM        TO S6234-LIFENUM                 */
		
		/* ilife- 6584 starts */
		
				jLife = "00";
				life = "01";
				lifeList	= lifeDAO.getOccRecordJLife(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum(),life,jLife);
				if (null==lifeList) {
					syserrrec.params.set(chdrpf.getChdrcoy()+chdrpf.getChdrnum());
					fatalError600();
				}else{
					for(Lifepf l : lifeList)
					{	if(l.getJlife().trim().equals(jLife.trim()))
						sv.lifenum.set(l.getLifcnum());
					}
		}
				clntpf = clntpfDAO.getClntpf("CN",wsspcomn.fsuco.toString(), sv.lifenum.toString());
						if(null == clntpf){
							syserrrec.params.set(wsspcomn.fsuco.toString()+sv.lifenum.toString());
							fatalError600();	
				 		}
				
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		/* MOVE '01'                   TO LIFEENQ-JLIFE.                */
		/* MOVE READR                  TO LIFEENQ-FUNCTION.             */
		/* CALL 'LIFEENQIO'            USING LIFEENQ-PARAMS.            */
	
		// new method need to create mverma54
				jLife = "01";
		
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO S6234-JLIFE                   */
		/*                                 S6234-JLIFENAME               */
		/*  ELSE                                                         */
		/* IF   LIFEENQ-STATUZ             = O-K                <V71L01>*/
		/*      MOVE LIFEENQ-LIFCNUM   TO S6234-JLIFE                   */
		/*                                CLTS-CLNTNUM                  */
		/*      MOVE WSSP-FSUCO        TO CLTS-CLNTCOY                  */
		/*      MOVE 'CN'              TO CLTS-CLNTPFX                  */
		/*      MOVE READR             TO CLTS-FUNCTION                 */
		/*      CALL 'CLTSIO'          USING CLTS-PARAMS                */
		/*      IF CLTS-STATUZ         NOT = O-K                        */
		/*           MOVE CLTS-PARAMS  TO SYSR-PARAMS                   */
		/*           PERFORM 600-FATAL-ERROR                            */
		/*      ELSE                                                    */
		/*           PERFORM PLAINNAME                                  */
		/*           MOVE WSSP-LONGCONFNAME                             */
		/*                             TO S6234-JLIFENAME.              */
	
				for(Lifepf l : lifeList){
							if (l.getJlife().trim().equals(jLife.trim())) {
									sv.jlife.set(l.getLifcnum());
					
									clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), l.getLifcnum());
									if(null == clntpf){
										syserrrec.params.set(wsspcomn.fsuco.toString() + l.getLifcnum());
										fatalError600();
									}
									else {
										plainname();
										sv.jlifename.set(wsspcomn.longconfname);
									}
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
	
			descpf= descpfDAO.getdescData("IT", t5688, chdrpf.getCnttype(),wsspcomn.company.toString(),wsspcomn.language.toString());//ILIFE-6334
					if(descpf != null)
					{
						sv.ctypedes.set(descpf.getLongdesc());
		}
		else
				{
			sv.ctypedes.fill("?");
		}
		
		 //   Obtain the Contract Status description from T3623.
		
					descpf= descpfDAO.getdescData("IT", t3623, chdrpf.getStatcode(),wsspcomn.company.toString(),wsspcomn.language.toString());//ILIFE-6334
						if(descpf != null)
							{
								sv.chdrstatus.set(descpf.getLongdesc());
							}
					else{
			sv.chdrstatus.fill("?");
		}
	
		   // Obtain the Premuim Status description from T3588.

		descpf= descpfDAO.getdescData("IT", t3588, chdrpf.getPstcde(),wsspcomn.company.toString(),wsspcomn.language.toString());//ILIFE-6334
			if(descpf != null)
			{
				sv.premstatus.set(descpf.getLongdesc());
		}
			else {
			sv.premstatus.fill("?");
		}

			
		/*    Read the stored PTRNENQ record for the contract.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		ptrnpf = ptrnDao.getCacheObject(ptrnpf);
		/* MOVE PTRNENQ-PTRNEFF        TO S6234-EFFDATE.                */
		sv.trcode.set(ptrnpf.getBatctrcde());
		/*    Obtain the Transaction Code description from T1688.*/
		
		descpf= descpfDAO.getdescData("IT", t1688, ptrnpf.getBatctrcde(),wsspcomn.company.toString(),wsspcomn.language.toString());//ILIFE-6334
				if(descpf != null)
			{
				sv.trandesc.set(descpf.getLongdesc());
		}
				else {
			sv.trandesc.fill("?");
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		acmvList = acmvDao.getAcmvPtrnRecords(ptrnpf.getChdrcoy(), ptrnpf.getChdrnum(), ptrnpf.getTranno());
		for(Acmvpf acmv:acmvList) {
			if (isEQ(sv.effdate, ZERO)) {
				sv.effdate.set(acmv.getEffdate());
			}
			/*    The ACMVTRN data set is read with the key from PTRNENQ until*/
			/*    any portion of the key changes.*/
			/*    Set up the Transaction details from the ACMVTRN record.*/
			sv.subfileFields.set(SPACES);
			sv.glcode.set(acmv.getGlcode());
			sv.sacscode.set(acmv.getSacscode());
			sv.sacstyp.set(acmv.getSacstyp());
			if (isEQ(acmv.getGlsign(), "-")) {
				setPrecision(acmv.getOrigamt(), 2);
				acmv.setOrigamt((mult(acmv.getOrigamt(), -1)).getbigdata());
				setPrecision(acmv.getAcctamt(), 2);
				acmv.setAcctamt((mult(acmv.getAcctamt(), -1)).getbigdata());
				//ILIFE-7381 start
				sv.hmhii.set(acmv.getOrigamt());
				sv.hmhli.set(acmv.getAcctamt());
				//ILIFE-7381 end
			}
			/*    MOVE ACMVTRN-ORIGAMT        TO S6234-ORIGAMT.                */
			else{
				sv.hmhii.set(acmv.getOrigamt());
				sv.hmhli.set(acmv.getAcctamt());
			}
			sv.origccy.set(acmv.getOrigcurr());
			/*    MOVE ACMVTRN-ACCTAMT        TO S6234-ACCTAMT.                */
			
			sv.acctccy.set(acmv.getGenlcur());
			sv.glsign.set(acmv.getGlsign());
			sv.rldgacct.set(acmv.getRldgacct());
			/*    Write the subfile record.*/
			//			MIBT-246
			scrnparams.function.set(varcom.sadd);
			processScreen("S6234", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		/* Now search for any ACMV records from Optical, ACMVTRNCP is      */
		/* the substitute IO module to get data from COLDPOINT.            */
		/* Search will not be necessary if ACMV's already found for this   */
		/* PTRN or if user cancelled out of Optical Search Window. In      */
		/* both these cases the UPDATE-FLAG is set to spaces.              */
		if (isNE(wssplife.updateFlag, "Y")) {
			/*    GO TO 1045-ACMC.                                  <LA4407>*/
			rtrn1050();
			return;
		}
		/*    Read the first ACMVTRN record for the PTRNENQ key.           */
		acmvtrnIO.setDataArea(SPACES);
		acmvtrnIO.setRldgcoy(ptrnpf.getChdrcoy());
		acmvtrnIO.setRdocnum(ptrnpf.getChdrnum());
		acmvtrnIO.setTranno(ptrnpf.getTranno());
		acmvtrnIO.setFunction(varcom.begn);
		wsaaIoCall = "CP";
		FixedLengthStringData groupTEMP = acmvtrnIO.getParams();
		callProgram(Acmvtrncp.class, groupTEMP);
		acmvtrnIO.setParams(groupTEMP);
		if (isNE(acmvtrnIO.getStatuz(), varcom.oK)
		&& isNE(acmvtrnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvtrnIO.getParams());
			fatalError600();
		}
		while ( !(isNE(acmvtrnIO.getRldgcoy(), ptrnpf.getChdrcoy())
		|| isNE(acmvtrnIO.getRdocnum(), ptrnpf.getChdrnum())
		|| isNE(acmvtrnIO.getTranno(), ptrnpf.getTranno())
		|| isEQ(acmvtrnIO.getStatuz(), varcom.endp))) {
			processAcmvtrn1100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(acmvtrnIO.getRldgcoy(), ptrnpf.getChdrcoy())
			|| isNE(acmvtrnIO.getRdocnum(), ptrnpf.getChdrnum())
			|| isNE(acmvtrnIO.getTranno(), ptrnpf.getTranno())){
			acmvtrnIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvtrnIO.getParams();
			callProgram(Acmvtrncp.class, groupTEMP2);
			acmvtrnIO.setParams(groupTEMP2);
			if (isNE(acmvtrnIO.getStatuz(), varcom.oK)
			&& isNE(acmvtrnIO.getStatuz(), varcom.endp)){
				syserrrec.params.set(acmvtrnIO.getParams());
				fatalError600();
			}
		}
		rtrn1050();
		return;
		}
	

	/**
	* <pre>
	*1045-ACMC.                                               <LA4407>
	****                                                      <LA4407>
	**** IF WSSP-UPDATE-FLAG     NOT = 'I'                    <LA4407>
	****    GO TO 1050-RTRN.                                  <LA4407>
	****                                                      <LA4407>
	**** Read the first ACMCTRN record for the PTRNENQ key.   <LA4407>
	****                                                      <LA4407>
	**** MOVE SPACES                 TO ACMCTRN-DATA-AREA.    <LA4407>
	**** MOVE PTRNENQ-CHDRCOY        TO ACMCTRN-RLDGCOY.      <LA4407>
	**** MOVE PTRNENQ-CHDRNUM        TO ACMCTRN-RDOCNUM.      <LA4407>
	**** MOVE PTRNENQ-TRANNO         TO ACMCTRN-TRANNO.       <LA4407>
	**** MOVE BEGN                   TO ACMCTRN-FUNCTION.     <LA4407>
	**** CALL 'ACMCTRNIO'         USING ACMCTRN-PARAMS.       <LA4407>
	**** IF   ACMCTRN-STATUZ      NOT = O-K                   <LA4407>
	****  AND ACMCTRN-STATUZ      NOT = ENDP                  <LA4407>
	****      MOVE ACMCTRN-PARAMS    TO SYSR-PARAMS           <LA4407>
	****      PERFORM 600-FATAL-ERROR.                        <LA4407>
	****                                                      <LA4407>
	**** PERFORM 1300-PROCESS-ACMCTRN                         <LA4407>
	****   UNTIL ACMCTRN-RLDGCOY  NOT = PTRNENQ-CHDRCOY       <LA4407>
	****      OR ACMCTRN-RDOCNUM  NOT = PTRNENQ-CHDRNUM       <LA4407>
	****      OR ACMCTRN-TRANNO   NOT = PTRNENQ-TRANNO        <LA4407>
	****      OR ACMCTRN-STATUZ       = ENDP.                 <LA4407>
	****                                                      <LA4407>
	* </pre>
	*/
protected void rtrn1050()
	{
		/*    Read the first RTRNTRN record for the PTRNENQ key.*/
	rtrnList = rtrnDao.getRtrnPtrnRecords(ptrnpf.getChdrcoy(), ptrnpf.getChdrnum(), ptrnpf.getTranno());
			for(Rtrnpf rtrn:rtrnList) {
				if (isEQ(sv.effdate, ZERO)) {
					sv.effdate.set(rtrn.getEffdate());
				}
				/*    The RTRNTRN data set is read with the key from PTRNENQ until*/
				/*    any portion of the key changes.*/
				/*    Set up the Transaction details from the RTRNTRN record.*/
				sv.subfileFields.set(SPACES);
				sv.glcode.set(rtrn.getGlcode());
				sv.sacscode.set(rtrn.getSacscode());
				sv.sacstyp.set(rtrn.getSacstyp());
				if (isEQ(rtrn.getGlsign(), "-")) {
					setPrecision(rtrn.getOrigamt(), 2);
					rtrn.setOrigamt(rtrn.getOrigamt().multiply(BigDecimal.valueOf(-1)));
					setPrecision(rtrn.getAcctamt(), 2);
					rtrn.setAcctamt(rtrn.getAcctamt().multiply(BigDecimal.valueOf(-1)));
				}
				/*    MOVE RTRNTRN-ORIGAMT        TO S6234-ORIGAMT.                */
				sv.hmhii.set(rtrn.getOrigamt());
				sv.origccy.set(rtrn.getOrigccy());
				/*    MOVE RTRNTRN-ACCTAMT        TO S6234-ACCTAMT.                */
				sv.hmhli.set(rtrn.getAcctamt());
				/*    MOVE RTRNTRN-ACCTCCY        TO S6234-ACCTCCY.                */
				sv.acctccy.set(rtrn.getGenlcur());
				sv.glsign.set(rtrn.getGlsign());
				sv.rldgacct.set(rtrn.getRldgacct());
				/*    Write the subfile record.*/
				scrnparams.function.set(varcom.sadd);
				processScreen("S6234", sv);
				if (isNE(scrnparams.statuz, varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
		}
	}
		
		/* Do not move 1 to the sunfile RRN if this is a call from a       */
		/* remote device so that the RRN will contain the total number of  */
		/* transaction records.                                            */
		/* MOVE 1                      TO SCRN-SUBFILE-RRN.             */
		if (isNE(scrnparams.deviceInd, "*RMT")) {
			scrnparams.subfileRrn.set(1);
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processAcmvtrn1100()
	{
		para1100();
	}

protected void para1100()
	{
		if (isEQ(sv.effdate, ZERO)) {
			sv.effdate.set(acmvtrnIO.getEffdate());
		}
		/*    The ACMVTRN data set is read with the key from PTRNENQ until*/
		/*    any portion of the key changes.*/
		/*    Set up the Transaction details from the ACMVTRN record.*/
		sv.subfileFields.set(SPACES);
		sv.glcode.set(acmvtrnIO.getGlcode());
		sv.sacscode.set(acmvtrnIO.getSacscode());
		sv.sacstyp.set(acmvtrnIO.getSacstyp());
		if (isEQ(acmvtrnIO.getGlsign(), "-")) {
			setPrecision(acmvtrnIO.getOrigamt(), 2);
			acmvtrnIO.setOrigamt(mult(acmvtrnIO.getOrigamt(), -1));
			setPrecision(acmvtrnIO.getAcctamt(), 2);
			acmvtrnIO.setAcctamt(mult(acmvtrnIO.getAcctamt(), -1));
		}
		/*    MOVE ACMVTRN-ORIGAMT        TO S6234-ORIGAMT.                */
		sv.hmhii.set(acmvtrnIO.getOrigamt());
		sv.origccy.set(acmvtrnIO.getOrigcurr());
		/*    MOVE ACMVTRN-ACCTAMT        TO S6234-ACCTAMT.                */
		sv.hmhli.set(acmvtrnIO.getAcctamt());
		sv.acctccy.set(acmvtrnIO.getGenlcur());
		sv.glsign.set(acmvtrnIO.getGlsign());
		sv.rldgacct.set(acmvtrnIO.getRldgacct());
		/*    Write the subfile record.*/
//		MIBT-246
		if((isNE(sv.hmhii, 0)) && (isNE(sv.hmhli, 0)))
		{
			scrnparams.function.set(varcom.sadd);
			processScreen("S6234", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}}		/*    Read the next ACMVTRN record.*/
		acmvtrnIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVTRNIO'         USING ACMVTRN-PARAMS.               */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvtrnIO.getParams();
			callProgram(Acmvtrncp.class, groupTEMP);
			acmvtrnIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvtrnIO);
		}
		if (isNE(acmvtrnIO.getStatuz(), varcom.oK)
		&& isNE(acmvtrnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvtrnIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	****                                                      <LA4407>
	*1300-PROCESS-ACMCTRN SECTION.                            <LA4407>
	*1300-PARA.                                               <LA4407>
	****                                                      <LA4407>
	**** The ACMCTRN data set is read with the key from PTRNENQ until 
	**** any portion of the key changes.                      <LA4407>
	****                                                      <LA4407>
	**** Set up the Transaction details from the ACMCTRN record.      
	****                                                      <LA4407>
	**** MOVE SPACES                 TO S6234-SUBFILE-FIELDS. <LA4407>
	**** MOVE ACMCTRN-GLCODE         TO S6234-GLCODE.         <LA4407>
	**** MOVE ACMCTRN-SACSCODE       TO S6234-SACSCODE.       <LA4407>
	**** MOVE ACMCTRN-SACSTYP        TO S6234-SACSTYP.        <LA4407>
	**** IF   ACMCTRN-GLSIGN          = '-'                   <LA4407>
	****      COMPUTE ACMCTRN-ORIGAMT = ACMCTRN-ORIGAMT  * -1 <LA4407>
	****      COMPUTE ACMCTRN-ACCTAMT = ACMCTRN-ACCTAMT  * -1.<LA4407>
	**** MOVE ACMCTRN-ORIGAMT        TO S6234-ORIGAMT.   <LA4407><006>
	**** MOVE ACMCTRN-ORIGAMT        TO S6234-HMHII.          <LA4407>
	**** MOVE ACMCTRN-ORIGCURR       TO S6234-ORIGCCY.        <LA4407>
	**** MOVE ACMCTRN-ACCTAMT        TO S6234-ACCTAMT.   <LA4407><006>
	**** MOVE ACMCTRN-ACCTAMT        TO S6234-HMHLI.          <LA4407>
	**** MOVE ACMCTRN-GENLCUR        TO S6234-ACCTCCY.        <LA4407>
	**** MOVE ACMCTRN-GLSIGN         TO S6234-GLSIGN.         <LA4407>
	**** MOVE ACMCTRN-RLDGACCT       TO S6234-RLDGACCT.       <LA4407>
	****                                                      <LA4407>
	**** Write the subfile record.                            <LA4407>
	****                                                      <LA4407>
	**** MOVE SADD                   TO SCRN-FUNCTION         <LA4407>
	**** CALL 'S6234IO'           USING SCRN-SCREEN-PARAMS    <LA4407>
	****                                S6234-DATA-AREA       <LA4407>
	****                                S6234-SUBFILE-AREA    <LA4407>
	**** IF SCRN-STATUZ           NOT = O-K                   <LA4407>
	****      MOVE SCRN-STATUZ       TO SYSR-STATUZ           <LA4407>
	****      PERFORM 600-FATAL-ERROR.                        <LA4407>
	****                                                      <LA4407>
	**** Read the next ACMCTRN record.                        <LA4407>
	****                                                      <LA4407>
	**** MOVE NEXTR                  TO ACMCTRN-FUNCTION.     <LA4407>
	**** CALL 'ACMCTRNIO'         USING ACMCTRN-PARAMS.       <LA4407>
	****                                                      <LA4407>
	**** IF   ACMCTRN-STATUZ      NOT = O-K                   <LA4407>
	****  AND ACMCTRN-STATUZ      NOT = ENDP                  <LA4407>
	****      MOVE ACMCTRN-PARAMS    TO SYSR-PARAMS           <LA4407>
	****      PERFORM 600-FATAL-ERROR.                        <LA4407>
	****                                                      <LA4407>
	*1390-EXIT.                                               <LA4407>
	****  EXIT.                                               <LA4407>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6234IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6234-DATA-AREA                         */
		/*                         S6234-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  No updates are required.*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
