/*
 * File: P6242.java
 * Date: 30 August 2009 0:38:51
 * Author: Quipoz Limited
 * 
 * Class transformed from P6242.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.S6242ScreenVars;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MinsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;  //ILIFE-4172
import com.csc.life.terminationclaims.dataaccess.dao.CovppfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Covppf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;




/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* TERM BASED COMPONENT ENQUIRY
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen as follows:
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*
*
*     Perform a RETRV  on  COVRENQ.  This  will  provide  the  COVR
*     record that is being enquired upon. At this point the program
*     must work out whether it is to provide a Plan Level or Policy
*     Level  enquiry.  This  can  be  done  by  checking  the field
*     WSSP-PLAN-POLICY-FLAG.  If it is 'L', then Plan Level Enquiry
*     is  required,  if  it  is  'O',  then Policy Level Enquiry is
*     required.  For Plan Level Enquiry all the COVRENQ records for
*     the  given  Life, Coverage and Rider must be read and the Sum
*     Insured  and Premium values added together so a total for the
*     whole  Plan  may  be  displayed.  For  Policy  Level  Enquiry
*     processing  will  be different depending on whether a summary
*     record or non-summary record is being displayed. Normally the
*     program  could  determine whether or not a summary record was
*     being  displayed  by  checking  the  Plan Suffix. If this was
*     '0000',  then  it  was  a summary record. However, for Policy
*     Level Enquiry when a summary record is held in the I/O module
*     for  processing  the previous function will replace the value
*     of  '0000'  in  the  Plan  Suffix  with the calculated suffix
*     number of the actual policy that the user selected. Therefore
*     it  will  be  necessary  to check the Plan Suffix against the
*     number  of policies summarised, (CHDRENQ-POLSUM). If the Plan
*     Suffix is not greater the CHDRENQ-POLSUM then it is a summary
*     record.
*
*     For  a  non-summary  record,  (Plan Suffix > CHDRENQ-POLSUM),
*     then display the Sum Insured and Premium as found.
*
*     For  a  summary  record,  (Plan Suffix not > CHDRENQ-POLSUM),
*     then  the  values  held on the COVRENQ record will be for all
*     the  policies summarised, and therefore a calculation must be
*     performed  to  arrive  at the value for one of the summarised
*     policies.  For  all  summarised policies execpt the first one
*     simply divide the total by the number of policies summarised,
*     (CHDRENQ-POLSUM)  and  display the result. This may give rise
*     to  a  rounding  discrepancy. This discrepancy is absorbed by
*     the  first policy summarised, the notional policy number one.
*     If  this  policy  has  been  selected for display perform the
*     following calculation:
*
*     Value To Display = TOTAL - (TOTAL * (POLSUM - 1))
*                                          ~~~~~~~~~~~           
*
*     Where  TOTAL  is  either  the Sum Insured or the Premium, and
*     POLSUM is the number of policies summarised, CHDRENQ-POLSUM.
*
*     For example if the premium is $100 and the number of policies
*     summarised  is 3, the Premium for notional policies #2 and #3
*     will  be  calculated  and  displayed as $33. For policy #1 it
*     will be:
*
*                        100 - (100 * (3 - 1)
*                                      ~~~~~                     *                                        3
*
*                     =  100 - (100 * 2/3)
*
*                     =  $34
*
*
*     The screen title  is  the  component  description from T5687.
*     (See P5123 for how this is obtained and centred).
*
*     Read table T5671,  key  is Transaction Code concatenated with
*     Coverage/Rider Code,  (CRTABLE).  Take  the  validation  item
*     that matches the current program and use it concatenated with
*     the Coverage/Rider Currency to read T5608.
*
*     If Options and Extras  are  not allowed, as defined on T5608,
*     then protect and  non-display  the  prompt and Options/Extras
*     Indicator, (OPTEXTIND). Otherwise read the options and extras
*     data-set, (LEXT), for  the  current  Coverage/Rider.  If  any
*     matching record is found then  place  a  '+'  in the field to
*     indicate that these details exist.
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation required  is  of  the  indicator  field -
*     OPTEXTIND.  This may be space, '+' or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.

*
*
* Next Program
* ------------
*
*     If "CF11" was requested  move  spaces  to the current program
*     field in the program stack field and exit.
*
*     If returning from  processing  the selection further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore  the  next 8  programs  from  the  WSSP,  remove  the
*     asterisk and set the select indicator to '+'.
*
*     If nothing was selected,  continue  by  just moving spaces to
*     the current stack action field and exit.  This will cause the
*     program to be re-invoked.
*
*     If a selection has been  made move '?' to the select field on
*     the screen, use GENSWCH  to  locate  the  next  program(s) to
*     process the selection, (up  to 8).  Use function of 'A'. Save
*     the next 8 programs in  the  stack  and replace them with the
*     ones returned from GENSWCH.  Place an asterisk in the current
*     stack action field, add 1 to the program pointer and exit.
*
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5608 - Term based Edit Rules             Key: Val. Item||Currency Code
* T5671 - Coverage/Rider Switching          Key: Tr. Code||CRTABLE
* T5687 - General Coverage/Rider Details    Key: CRTABLE
* T5688 - Contract Structure                Key: CNTTYPE
*
*****************************************************************
* </pre>
*/
public class P6242 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6242");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaMessages = new FixedLengthStringData(100);
	private FixedLengthStringData[] wsaaMessage = FLSArrayPartOfStructure(2, 50, wsaaMessages, 0);

		/*    VALUE 'Loaded Single Prem :'.                        <SPLPRM>
		    VALUE 'Loaded Inst. Prem  :'.                        <SPLPRM>*/
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaCovrPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaPremStatus, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private static final String g620 = "G620";
	private static final String h093 = "H093";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	protected LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MinsTableDAM minsIO = new MinsTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private static final String chdrenqrec = "CHDRENQREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private T2240rec t2240rec = new T2240rec();
	private T5608rec t5608rec = new T5608rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wssplife wssplife = new Wssplife();
	private T5687rec t5687rec = new T5687rec(); //ILIFE-4172
	private S6242ScreenVars sv =  getLScreenVars();//ScreenProgram.getScreenVars( S6242ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	protected WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private boolean loadingFlag = false;//ILIFE-3399
	private boolean premiumflag = false;//ILIFE-3421 starts
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();//ILIFE-3421 ends
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrpf = new Covrpf();
	protected boolean mrtaPermission = false;//ILIFE-3685
	private boolean dialdownFlag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
	
	private FixedLengthStringData setComponent = new FixedLengthStringData(4); //ILIFE-4172
	private boolean lnkgFlag = false; //ILIFE-6968
	/*ILIFE-7118-starts*/
	private String tpd1="TPD1";
	private String tps1="TPS1";
	private boolean tpdtypeFlag=false;
	private static final String  TPD_FEATURE_ID="NBPRP060";
	/*ILIFE-7118-ends*/
	boolean isEndMat = false;
	private boolean stampDutyflag = false;	//ILIFE-7746
	private static final String TR530 = "TR530";
	private static final String itemcoy = "2";
	private static final String TR529 = "TR529";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfListtr529;
	private List<Itempf> itempfListtr530;
	private static final String zraerec = "ZRAEREC";
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	//ILJ-45 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-45 End
	
	// ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	// ILJ-387 end
	
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private Tr52erec tr52erec = new Tr52erec();
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	
	//IBPLIFE-2139
		private boolean contnewBFlag = false;
		private String cntnewBFeature = "NBPRP126";
		Covppf covppf=null;
		private CovppfDAO covppfDAO = getApplicationContext().getBean("covppfDAO", CovppfDAO.class);
		private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();	//ICIL-1494
		//end
	
	
/**
 * Contains all possible labels used by goTo action.
 */
	
	
	
	
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endPolicy1020, 
		nextProg10640, 
		exit1090, 
		search1320, 
		exit1790, 
		exit2090, 
		exit4090
	}

	public P6242() {
		super();
		screenVars = sv;
		new ScreenModel("S6242", AppVars.getInstance(), sv);
	}
	protected S6242ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6242ScreenVars.class);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case endPolicy1020: 
					endPolicy1020();
					gotRecord10620();
				case nextProg10640: 
					nextProg10640();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		boolean cntEnqFlag = false;//ILJ-387
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.taxamt.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		wsaaMiscellaneousInner.wsaaTaxamt.set(ZERO);
		/*    Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());
		/*BRD-306 START */		
		sv.loadper.set(ZERO);
		sv.rateadj.set(ZERO);
		sv.fltmort.set(ZERO);
		sv.premadj.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.adjustageamt.set(ZERO);
		
		/*BRD-306 END */
		sv.prmbasis.set(SPACES);
		
		// ILJ-387 Start
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		
		contnewBFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntnewBFeature, appVars, "IT");//IBPLIFE-2139
		
		
		if (cntEnqFlag) {
			sv.cntEnqScreenflag.set("Y");
		}else 
		{
			sv.cntEnqScreenflag.set("N");
		}
		// ILJ-387 End
		
		isEndMat = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "PRPRO001", appVars, "IT");
		//ILJ-45 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{			
			sv.crrcdOut[varcom.nd.toInt()].set("Y");
			//sv.rcessageOut[varcom.nd.toInt()].set("Y");
			sv.contDtCalcScreenflag.set("N");
			sv.rcesdteOut[varcom.nd.toInt()].set("Y");
		}
		//ILJ-45 End
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}
		
		
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
		
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRENQ-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			/* MTL002 */	
			}
		}
		/*  Read TR52D for Taxcode                                         */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO S6242-JLIFE                   */
		/*                                 S6242-JLIFENAME               */
		/*  ELSE                                                         */
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		
		if(!isEndMat){
			sv.aepaydetOut[varcom.nd.toInt()].set("Y");
		}
		mrtaPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars, "IT");
		if(!mrtaPermission){
			sv.mrtaFlag.set("N");
		}
		else {
			sv.mrtaFlag.set("Y");
		}
		//ILIFE-3399-STARTS
		loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-3399-ENDS
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Retrieve valid status'.                                         */
		readT5679Table1200();
		checkForDisplayingReduceTerm();
		/*    Read the first LIFE details on the contract.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrpf = covrDao.getCacheObject(covrpf);
		if(isEndMat)
		{
			itempfListtr530=itempfDAO.getAllItemitem("IT",itemcoy, TR530, covrpf.getCrtable()); /* IJTI-1479 */
			itempfListtr529=itempfDAO.getAllItemitem("IT",itemcoy, TR529,  covrpf.getCrtable()); /* IJTI-1479 */
			if ((itempfListtr530.size()>0) || (itempfListtr529.size()>0))  {
				CheckZraeRecords();
			}
		}


		// IBPLIFE-2139
		if (!contnewBFlag) {
			sv.contnewBScreenflag.set("N");
		}
		else
		{
			sv.contnewBScreenflag.set("Y");	
			sv.covrprpseOut[varcom.nd.toInt()].set("Y");
			checkCoverPurpose1040();
		}
		// end



		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			/*     MOVE COVRENQ-SINGP      TO WSAA-SINGP                    */
			wsaaMiscellaneousInner.wsaaSingp.set(covrpf.getSingp());
			/*     ADD  COVRENQ-SINGP, COVRENQ-INSTPREM                <005>*/
			/*                             GIVING WSAA-SINGP           <005>*/
			wsaaMiscellaneousInner.wsaaPremium.set(covrpf.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrpf.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrpf.getSumins());
			setPrecision(covrpf.getPlanSuffix(), 0);
			covrpf.setPlanSuffix(chdrpf.getPolsum()+1);
			if (isGT(chdrpf.getPolinc(), 1)) {
				while ( !(isGT(covrpf.getPlanSuffix(), chdrpf.getPolinc()))) {
					planComponent1100();
				}
				
				goTo(GotoLabel.endPolicy1020);
			}
			else {
				goTo(GotoLabel.endPolicy1020);
			}
		}
		/*     PERFORM 1100-PLAN-COMPONENT                              */
		/*       UNTIL COVRENQ-PLAN-SUFFIX > CHDRENQ-POLINC             */
		/*             GO TO 1020-END-POLICY.                           */
		if (isGT(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			/*     MOVE COVRENQ-SINGP      TO WSAA-SINGP                    */
			wsaaMiscellaneousInner.wsaaSingp.set(covrpf.getSingp());
			/*     ADD  COVRENQ-SINGP, COVRENQ-INSTPREM                <005>*/
			/*                             GIVING WSAA-SINGP           <005>*/
			wsaaMiscellaneousInner.wsaaPremium.set(covrpf.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrpf.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrpf.getSumins());
			goTo(GotoLabel.endPolicy1020);
		}
		/*  ADD  COVRENQ-SINGP, COVRENQ-INSTPREM                   <005>*/
		/*                             GIVING WSAA-PREMIUM.        <005>*/
		wsaaMiscellaneousInner.wsaaPremium.set(covrpf.getInstprem());
		wsaaMiscellaneousInner.wsaaZlinstprem.set(covrpf.getZlinstprem());
		if (isEQ(covrpf.getPlanSuffix(), 1)) {
			/*     COMPUTE WSAA-SINGP       = COVRENQ-SINGP                 */
			/*  - (COVRENQ-SINGP  * (CHDRENQ-POLSUM - 1) / CHDRENQ-POLSUM)  */
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(sub(covrpf.getSingp(), (div(mult(covrpf.getSingp(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			/*     COMPUTE WSAA-SINGP       = WSAA-PREMIUM             <005>*/
			/*  - (WSAA-PREMIUM  * (CHDRENQ-POLSUM - 1) / CHDRENQ-POLSUM)05>*/
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(sub(wsaaMiscellaneousInner.wsaaPremium, (div(mult(wsaaMiscellaneousInner.wsaaPremium, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(sub(wsaaMiscellaneousInner.wsaaZlinstprem, (div(mult(wsaaMiscellaneousInner.wsaaZlinstprem, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(sub(covrpf.getSumins(), (div(mult(covrpf.getSumins(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
		}
		else {
			/*     COMPUTE WSAA-SINGP       = COVRENQ-SINGP                 */
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(div(covrpf.getSingp(), chdrpf.getPolsum()));
			/*     COMPUTE WSAA-SINGP       = WSAA-PREMIUM             <005>*/
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(div(wsaaMiscellaneousInner.wsaaPremium, chdrpf.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(div(wsaaMiscellaneousInner.wsaaZlinstprem, chdrpf.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(div(covrpf.getSumins(), chdrpf.getPolsum()));
		}
		
		
		}
		// end
		
//IBPLIFE-2141
	private void checkCoverPurpose1040() {

		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (covrpf != null) {
			Covppf covppf = covppfDAO.getCovppfCrtable(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getCrtable(),
					covrpf.getCoverage(),covrpf.getRider());
			if (covppf != null) {
				sv.dateeff.set(covppf.getEffdate());
				sv.trcode.set(covppf.getTranCode());
				sv.covrprpse.set(covppf.getCovrprpse());
				descIO.setDataKey(SPACES);
				descIO.setDescpfx("IT");
				descIO.setDesccoy(lifeenqIO.getChdrcoy());
				descIO.setDesctabl("T1688");
				descIO.setDescitem(sv.trcode);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
					sv.trcdedesc.fill("?");
				} else {
					sv.trcdedesc.set(descIO.getLongdesc());
					sv.trcode.set(descIO.getDescitem());
				}
			}
		}
	}
		
	
protected void callReadRCVDPF(){     /*ILIFE-3421*/
	//ILIFE-3800-STARTS
		if(rcvdPFObject == null)
			rcvdPFObject= new Rcvdpf();
	/* IJTI-1479 START*/
	rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
	rcvdPFObject.setChdrnum(covrpf.getChdrnum());
	rcvdPFObject.setLife(covrpf.getLife());
	rcvdPFObject.setCoverage(covrpf.getCoverage());
	rcvdPFObject.setRider(covrpf.getRider());
	rcvdPFObject.setCrtable(covrpf.getCrtable());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	/* IJTI-1479 END*/
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}
protected void endPolicy1020()
	{
	sv.benBillDate.set(covrpf.getBenBillDate());
	if (isEQ(covrpf.getBenBillDate(), varcom.vrcmMaxDate)
	|| isEQ(covrpf.getBenBillDate(), 0)) {
		sv.bbldatOut[varcom.nd.toInt()].set("Y");
	}
		sv.mortcls.set(covrpf.getMortcls());
		sv.numpols.set(chdrpf.getPolinc());
		sv.anbAtCcd.set(covrpf.getAnbAtCcd());
		sv.crrcd.set(covrpf.getCrrcd());
		sv.annivProcDate.set(covrpf.getAnnivProcDate());
		sv.optextind.set(SPACES);
		sv.premcess.set(covrpf.getPremCessDate());
		sv.liencd.set(covrpf.getLiencd());
		sv.riskCessDate.set(covrpf.getRiskCessDate());
		sv.riskCessAge.set(covrpf.getRiskCessAge());
		sv.premCessAge.set(covrpf.getPremCessAge());
		sv.riskCessTerm.set(covrpf.getRiskCessTerm());
		sv.premCessTerm.set(covrpf.getPremCessTerm());
		//ILIFE-3421-STARTS
		sv.benCessAge.set(covrpf.getBenCessAge());
		sv.benCessTerm.set(covrpf.getBenCessTerm());
		sv.benCessDate.set(covrpf.getBenCessDate());
		//3421-3421-ENDS
		sv.register.set(chdrpf.getReg());
		sv.life.set(covrpf.getLife());
		if (isNE(covrpf.getLife(), lifeenqIO.getLife())) {
			getLifeName4800();
		}
		sv.rider.set(covrpf.getRider());
		sv.coverage.set(covrpf.getCoverage());
		sv.rerateDate.set(covrpf.getRerateDate());
		sv.rerateFromDate.set(covrpf.getRerateFromDate());
		/*BRD-306 START */
		sv.loadper.set(covrpf.getLoadper());
		sv.rateadj.set(covrpf.getRateadj());
		sv.fltmort.set(covrpf.getFltmort());
		sv.premadj.set(covrpf.getPremadj());
		sv.zbinstprem.set(covrpf.getZbinstprem());
		sv.adjustageamt.set(covrpf.getPremadj());
		sv.tpdtype.set(covrpf.getTpdtype());//ILIFE-7118
		/*BRD-306 END */
		dialdownFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		/*ILIFE-3421 starts*/
		premiumflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		/*ILIFE-3421 ends*/

		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends

		sv.singlePremium.set(wsaaMiscellaneousInner.wsaaSingp);
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.zlinstprem.set(wsaaMiscellaneousInner.wsaaZlinstprem);
		
		//ILIFE-6968 - Start		
		lnkgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),
				"NBPRP055", appVars, "IT");
		if (lnkgFlag == true) {
			sv.lnkgnoOut[varcom.nd.toInt()].set("Y");
			sv.lnkgsubrefnoOut[varcom.nd.toInt()].set("Y");
		}	
		sv.lnkgno.set(covrpf.getLnkgno());
		sv.lnkgsubrefno.set(covrpf.getLnkgsubrefno());
		sv.lnkgind.set(covrpf.getLnkgind());
		//ILIFE-6968 - End
		/*ILIFE-7118-starts*/
	    tpdtypeFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),TPD_FEATURE_ID, appVars, "IT");	
		if((tpdtypeFlag) && (tpd1.equals(covrpf.getCrtable().trim())||tps1.equals(covrpf.getCrtable().trim()))) {
			sv.tpdtypeOut[varcom.nd.toInt()].set("N");
		}else {
			sv.tpdtypeOut[varcom.nd.toInt()].set("Y");
		}
		/*ILIFE-7118-ends*/
		/*--- Read TR386 table to get screen literals                      */
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		/* IF WSAA-PREMIUM             = 0                      <FA1226>*/
		/*    MOVE 'S'                 TO WSAA-TR386-ID         <FA1226>*/
		/* ELSE                                                 <FA1226>*/
		/*    MOVE 'R'                 TO WSAA-TR386-ID         <FA1226>*/
		/* END-IF.                                              <FA1226>*/
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setItemseq(SPACES);
		/* MOVE READR                  TO ITEM-FUNCTION.        <FA1226>*/
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setStatuz(varcom.oK);
		iy.set(1);
		initialize(wsaaMessages);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadTr3861600();
		}
		
		if (isEQ(wsaaMiscellaneousInner.wsaaPremium, 0)) {
			sv.zdesc.set(wsaaMessage[2]);
		}
		else {
			sv.zdesc.set(wsaaMessage[1]);
		}
		/* CALL  'ITEMIO'           USING ITEM-PARAMS.          <FA1226>*/
		/* IF  ITEM-STATUZ          NOT = O-K                   <FA1226>*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <FA1226>*/
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <FA1226>*/
		/*     PERFORM 600-FATAL-ERROR.                         <FA1226>*/
		/* MOVE ITEM-GENAREA           TO TR386-TR386-REC.      <FA1226>*/
		/* MOVE TR386-PROGDESC-1       TO S6242-ZDESC.          <FA1226>*/
		/* IF WSAA-PREMIUM             = 0                      <SPLPRM>*/
		/*    MOVE WSAA-SINGLE-DESC    TO S6242-ZDESC           <SPLPRM>*/
		/* ELSE                                                 <SPLPRM>*/
		/*    MOVE WSAA-REG-DESC       TO S6242-ZDESC           <SPLPRM>*/
		/* END-IF.                                              <SPLPRM>*/
		sv.zdescOut[varcom.hi.toInt()].set("N");
		sv.statFund.set(covrpf.getStatFund());
		sv.statSect.set(covrpf.getStatSect());
		sv.statSubsect.set(covrpf.getStatSubsect());
		
		
		if(isEndMat)
		{
			if ((itempfListtr530.size()==0) && (itempfListtr529.size()==0) )  {
				
				sv.aepaydetOut[varcom.nd.toInt()].set("Y");
			}
		}
		
		sv.sumin.set(wsaaMiscellaneousInner.wsaaSumins);
		customerSpecificSADisplay();
		if (isNE(covrpf.getPlanSuffix(), ZERO)) {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		else {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			sv.planSuffix.set(ZERO);
		}
		if (covrpf.getBonusInd() != null ) {							  
			if (isNE(covrpf.getBonusInd(), SPACES)) {
				sv.unitStatementDate.set(covrpf.getUnitStatementDate());
				sv.bonusInd.set(covrpf.getBonusInd());
			}
		}
		else {
			sv.cbunstOut[varcom.nd.toInt()].set("Y");
			sv.unitStatementDate.set(ZERO);
			sv.bonusInd.set(SPACES);
		}
		setupBonus1400();
		sv.bappmeth.set(covrpf.getBappmeth());
		/* GET THE COMPONENT DESCRIPTION*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		setComponent.set(covrpf.getCrtable()); //ILIFE-4172
		loadHeading1700();
		/* READ T5671*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaMiscellaneousInner.wsaaTrCode.set(wsaaBatckey.batcBatctrcde);
		wsaaMiscellaneousInner.wsaaCrtable.set(covrpf.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5671);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5671Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5671)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5671Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itdmIO.getGenarea());
		}
		
		/* START OF ILIFE-4172 */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5687);
		itemIO.setItemitem(setComponent);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itemIO.getGenarea());
	
		if(isNE(t5687rec.zsbsmeth, SPACES)){
			sv.optsmodeOut[varcom.nd.toInt()].set(SPACES);
			sv.payflagOut[varcom.nd.toInt()].set(SPACES);
			sv.zsredtrmOut[varcom.nd.toInt()].set(SPACES);
		}
		else {
			sv.optsmodeOut[varcom.nd.toInt()].set("Y");
			sv.payflagOut[varcom.nd.toInt()].set("Y");
			sv.zsredtrmOut[varcom.nd.toInt()].set("Y");
		}
		/* END OF ILIFE-4172 */
		if(isNE(t5687rec.lnkgind,"Y"))
		{
			sv.lnkgindOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-7746
		stampDutyflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			if(isNE(covrpf.getZstpduty01(),ZERO)){
				sv.zstpduty01.set(covrpf.getZstpduty01());
			}
		}
	}

protected void gotRecord10620()
	{
		sub1.set(0);
	}

protected void nextProg10640()
	{
		sub1.add(1);
		if (isNE(t5671rec.pgm[sub1.toInt()], wsaaProg)) {
			goTo(GotoLabel.nextProg10640);
		}
		else {
			wsaaMiscellaneousInner.wsaaValidItem.set(t5671rec.edtitm[sub1.toInt()]);
		}
		/* READ T5608*/
		wsaaMiscellaneousInner.wsaaCrcyCode.set(chdrpf.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5608);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5608Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5608)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5608Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			fatalError600();
		}
		else {
			t5608rec.t5608Rec.set(itdmIO.getGenarea());
		}
		if(isEQ(t5608rec.prmbasis, SPACES)){        
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");/*ILIFE-3421*/
		}
		if (isEQ(t5608rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext1900();
		}
		checkPovr1800();
		m100CheckMbns();
		m500CheckMins();
		exclFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
	}
protected void checkExcl()
{
	exclpf = exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getCrtable(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider()); /* IJTI-1479 */
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void planComponent1100()
	{
		para1100();
	}

protected void para1100()
	{
		/*    For Plan Level Enquiry the first coverage record read will*/
		/*    have a Plan Suffix of 1. All the components for the given*/
		/*    Life, Coverage and Rider must be read and the amounts added*/
		/*    up to give the total for the component being enquired upon.*/
	if (isEQ(covrpf.getPlanSuffix(), 1)
			&& isNE(chdrpf.getPolinc(), 1)) {
				wsaaMiscellaneousInner.wsaaSumins.set(ZERO);
			}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		wsaaCovrPstatcode.set(covrpf.getPstatcode());
		checkPremStatus1300();
		if (validStatus.isTrue()) {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(add(wsaaMiscellaneousInner.wsaaSingp, covrpf.getSingp()));
			/*                              + COVRENQ-SINGP.           <005>*/
			/*                              + COVRENQ-SINGP                 */
			compute(wsaaMiscellaneousInner.wsaaPremium, 2).set(add(wsaaMiscellaneousInner.wsaaPremium, covrpf.getInstprem()));
			/*                              + COVRENQ-INSTPREM.        <005>*/
			/*                              + COVRENQ-INSTPREM         <005>*/
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 2).set(add(wsaaMiscellaneousInner.wsaaZlinstprem, covrpf.getZlinstprem()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(add(wsaaMiscellaneousInner.wsaaSumins, covrpf.getSumins()));
		}
		setPrecision(covrpf.getPlanSuffix(), 0);
		covrpf.setPlanSuffix(covrpf.getPlanSuffix()+ 1);
	}

protected void readT5679Table1200()
	{
		read1210();
	}

protected void read1210()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void checkPremStatus1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1310();
				case search1320: 
					search1320();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Check the twelve premium status' retrieved from table T5679     
	* to see if any match the coverage premium status. If so, set     
	* the VALID-STATUS flag to yes.                                   
	* </pre>
	*/
protected void start1310()
	{
		wsaaPremStatus.set(SPACES);
		wsaaSub.set(ZERO);
	}

protected void search1320()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			return ;
		}
		else {
			if (isNE(wsaaCovrPstatcode, t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search1320);
			}
			else {
				wsaaPremStatus.set("Y");
				return ;
			}
		}
		/*EXIT*/
	}

protected void setupBonus1400()
	{
		/*PARA*/
		/* Check if Coverage/Rider is a SUM product. If not SUM product    */
		/* do not display field.                                           */
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covrpf.getChdrcoy());
		itemIO.setItemitem(covrpf.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void loadTr3861600()
	{
		start1610();
	}

protected void start1610()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), wsspcomn.company)
		|| isNE(itemIO.getItemtabl(), tablesInner.tr386)
		|| isNE(itemIO.getItemitem(), wsaaTr386Key)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isEQ(itemIO.getFunction(), varcom.begn)) {
				itemIO.setItemitem(wsaaTr386Key);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		ix.set(1);
		while ( !(isGT(ix, 10))) {
			if (isNE(tr386rec.progdesc[ix.toInt()], SPACES)
			&& isLT(iy, 3)) {
				wsaaMessage[iy.toInt()].set(tr386rec.progdesc[ix.toInt()]);
				iy.add(1);
			}
			ix.add(1);
		}
		
		itemIO.setFunction(varcom.nextr);
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		/* VARYING WSAA-X           FROM 24 BY -1                    */
		/*SUBTRACT WSAA-X             FROM 24 GIVING WSAA-Y.           */
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkPovr1800()
	{
		readPovr1810();
	}

protected void readPovr1810()
	{
		/* Test the POVR file for the existance of a Premium Breakdown     */
		/* record. If one exists then allow the option to display the      */
		/* select window for enquiry....if not then protect.               */
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covrpf.getChdrcoy());
		povrIO.setChdrnum(lifeenqIO.getChdrnum());
		povrIO.setLife(lifeenqIO.getLife());
		povrIO.setCoverage(covrpf.getCoverage());
		povrIO.setRider(covrpf.getRider());
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)
		&& isNE(povrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		else {
			if (isEQ(povrIO.getStatuz(), varcom.endp)
			|| isNE(povrIO.getChdrcoy(), covrpf.getChdrcoy())
			|| isNE(povrIO.getChdrnum(), lifeenqIO.getChdrnum())
			|| isNE(povrIO.getLife(), lifeenqIO.getLife())
			|| isNE(povrIO.getCoverage(), covrpf.getCoverage())
			|| isNE(povrIO.getRider(), covrpf.getRider())) {
				sv.pbind.set(SPACES);
				sv.pbindOut[varcom.nd.toInt()].set("Y");
				sv.pbindOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.pbind.set("+");
				sv.pbindOut[varcom.nd.toInt()].set("N");
				sv.pbindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void m100CheckMbns()
	{
		m100Start();
	}

protected void m100Start()
	{
		mbnsIO.setChdrcoy(chdrpf.getChdrcoy());
		mbnsIO.setChdrnum(chdrpf.getChdrnum());
		mbnsIO.setLife(covrpf.getLife());
		mbnsIO.setCoverage(covrpf.getCoverage());
		mbnsIO.setRider(covrpf.getRider());
		mbnsIO.setYrsinf(ZERO);
		mbnsIO.setFormat(formatsInner.mbnsrec);
		mbnsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)
		&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		if (isEQ(mbnsIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(mbnsIO.getRider(), covrpf.getRider())
				|| isNE(mbnsIO.getChdrcoy(), chdrpf.getChdrcoy())
				|| isNE(mbnsIO.getChdrnum(), chdrpf.getChdrnum())
				|| isNE(mbnsIO.getLife(), covrpf.getLife())
				|| isNE(mbnsIO.getCoverage(), covrpf.getCoverage())) {
					mbnsIO.setStatuz(varcom.endp);
				}
		else {
			sv.optsmode.set("+");
		}
	}

protected void m500CheckMins()
	{
		m500Start();
	}

protected void m500Start()
	{
		minsIO.setChdrcoy(chdrpf.getChdrcoy());
		minsIO.setChdrnum(chdrpf.getChdrnum());
		minsIO.setLife(covrpf.getLife());
		minsIO.setCoverage(covrpf.getCoverage());
		minsIO.setRider(covrpf.getRider());
		minsIO.setFormat(formatsInner.minsrec);
		minsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(), varcom.oK)
		&& isNE(minsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		if (isEQ(minsIO.getStatuz(), varcom.oK)) {
			sv.payflag.set("+");
		}
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
		lextIO.setChdrcoy(covrpf.getChdrcoy());
		lextIO.setChdrnum(covrpf.getChdrnum());
		lextIO.setLife(covrpf.getLife());
		lextIO.setCoverage(covrpf.getCoverage());
		lextIO.setRider(covrpf.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covrpf.getChdrcoy(), lextIO.getChdrcoy())
				|| isNE(covrpf.getChdrnum(), lextIO.getChdrnum())
				|| isNE(covrpf.getLife(), lextIO.getLife())
				|| isNE(covrpf.getCoverage(), lextIO.getCoverage())
				|| isNE(covrpf.getRider(), lextIO.getRider())) {
					lextIO.setStatuz(varcom.endp);
				}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
	
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			readTableTr52e();
			checkCalcTax5500();
		}
		if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}
		/*2010-SCREEN-IO.                                                  */
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S6242IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   S6242-DATA-AREA .             */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(g620);
		}
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind, "+")
		&& isNE(sv.pbind, "X")) {
			sv.pbindErr.set(g620);
		}
		if (isNE(sv.optsmode, " ")
		&& isNE(sv.optsmode, "+")
		&& isNE(sv.optsmode, "X")) {
			sv.optsmodeErr.set(g620);
		}
		if (isNE(sv.payflag, " ")
		&& isNE(sv.payflag, "+")
		&& isNE(sv.payflag, "X")) {
			sv.payflagErr.set(g620);
		}
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(g620);
				}
				
		/*ILIFE-3685 Starts by slakkala*/
		if(mrtaPermission){
		if (isNE(sv.zsredtrm, " ")
				&& isNE(sv.zsredtrm, "+")
				&& isNE(sv.zsredtrm, "X")) {
					sv.zsredtrmErr.set(g620);
					wsspcomn.edterror.set("Y");
				}
		}
		/*ILIFE-3685 ends by slakkala*/
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		try {
			para4000();
			nextProgram4080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				restoreProgram4100();
			}
			/*sv.optextind.set("+");*/
		}
		/*  BREAKPOINT SELECTION MADE                                      */
		if (isEQ(sv.pbind, "X")) {
			pbindExe5100();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.pbind, "?")) {
			pbindRet5200();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.optsmode, "X")) {
			m100BnfSchdExe();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.optsmode, "?")) {
			m200BnfSchdRet();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.payflag, "X")) {
			m600InstalmentExe();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.payflag, "?")) {
			m700InstalmentRet();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe5600();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.taxind, "?")) {
			taxRet5700();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.exclind, "X")) {
			exclExe6000();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.exclind, "?")) {
			exclRet6100();
			goTo(GotoLabel.exit4090);
		}
		/*ILIFE-3685 Starts by slakkala*/
		if(mrtaPermission){
		 if (isEQ(sv.zsredtrm, "X")) {
			redTerm6600();
			goTo(GotoLabel.exit4090);
		 }
		 if (isEQ(sv.zsredtrm, "?")) {
			redTrm6700();
			goTo(GotoLabel.exit4090);
		 }
		}
		
		if (isEQ(sv.aepaydet, "X") && isEndMat) {
			antcpEndwPayDet();
			chdrenq();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.aepaydet, "?") && isEndMat) {
			antcpEndwPayDet1();
			chdrenq();
			goTo(GotoLabel.exit4090);
		}
		
		checkIFReduceTermSelected();
		/*ILIFE-3685 ends by slakkala*/
		/*  SELECTION NOT MADE*/
		if (isNE(sv.optextind, "X")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		/*   SELECTION MADE*/
		/*   The generalised secondary switching module is called to get*/
		/*   the next 8 programs and load them into the program stack.*/
		sv.optextind.set("?");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			saveProgramStack4200();
		}
		gensswrec.function.set("A");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				goTo(GotoLabel.exit4090);
			}
			else {
				sv.optextind.set("+");
				goTo(GotoLabel.exit4090);
			}
		}
		/* ELSE                                                         */
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			loadProgramStack4300();
		}
	}
protected void exclExe6000(){
	if (isNE(sv.exclind, "X")) {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		return ;
	}
	sv.exclind.set("?");
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
		saveProgramStack4200();
	}
	gensswrec.function.set("H");
	wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
	wsspcomn.crtable.set(covrpf.getCrtable());
	
	wsspcomn.cmode.set("IFE");
	
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		scrnparams.errorCode.set(h093);
		wsspcomn.nextprog.set(scrnparams.scrname);
		if (exclpf==null) {
			sv.exclind.set(" ");
			return ;
		}
		else {
			sv.exclind.set("+");
			return ;
		}
	}
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
		loadProgramStack4300();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.programPtr.add(1);

}
protected void exclRet6100(){
	
	sv.exclind.set("+");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		restoreProgram4100();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	}



protected void antcpEndwPayDet(){
	if (isNE(sv.aepaydet, "X")) {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		return ;
	}
	sv.aepaydet.set("?");
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
		saveProgramStack4200();
	}
	gensswrec.function.set("I");
	wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
	wsspcomn.crtable.set(covrpf.getCrtable());
	
	wsspcomn.cmode.set("IFE");
	
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		scrnparams.errorCode.set(h093);
		wsspcomn.nextprog.set(scrnparams.scrname);
		if (exclpf==null) {
			sv.aepaydet.set(" ");
			return ;
		}
		else {
			sv.aepaydet.set("+");
			return ;
		}
	}
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
		loadProgramStack4300();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.programPtr.add(1);

}
protected void antcpEndwPayDet1(){	
	sv.aepaydet.set("+");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		restoreProgram4100();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	
}


protected void nextProgram4080()
	{
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void spaceFillStack4700()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(SPACES);
		sub1.add(1);
		/*EXIT*/
	}

protected void getLifeName4800()
	{
		para4800();
	}

protected void para4800()
	{
		lifeenqIO.setChdrcoy(covrpf.getChdrcoy());
		lifeenqIO.setChdrnum(covrpf.getChdrnum());
		lifeenqIO.setLife(covrpf.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void m100BnfSchdExe()
	{
		m100Start1();
	}

protected void m100Start1()
	{
		if (isNE(sv.optsmode, "X")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			return ;
		}
		/*   SELECTION MADE                                                */
		/*   The generalised secondary switching module is called to get   */
		/*   the next 8 programs and load them into the program stack.     */
		sv.optsmode.set("?");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			saveProgramStack4200();
		}
		gensswrec.function.set("C");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.optsmode.set(" ");
				return ;
			}
			else {
				sv.optsmode.set("+");
				return ;
			}
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			loadProgramStack4300();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void m200BnfSchdRet()
	{
		/*M200-START*/
		sv.optsmode.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restoreProgram4100();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*M200-EXIT*/
	}

protected void m600InstalmentExe()
	{
		m600Start();
	}

protected void m600Start()
	{
		if (isNE(sv.payflag, "X")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			return ;
		}
		/*   SELECTION MADE                                                */
		/*   The generalised secondary switching module is called to get   */
		/*   the next 8 programs and load them into the program stack.     */
		sv.payflag.set("?");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			saveProgramStack4200();
		}
		gensswrec.function.set("D");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.payflag.set(" ");
				return ;
			}
			else {
				sv.payflag.set("+");
				return ;
			}
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			loadProgramStack4300();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void m700InstalmentRet()
	{
		/*M700-START*/
		sv.payflag.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restoreProgram4100();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*M700-EXIT*/
	}

protected void pbindExe5100()
	{
		start5110();
	}

protected void start5110()
	{
		/*  - Keep the POVR record for Premium Breakdown enquiry.          */
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.pbind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			saveProgramStack4200();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("B");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator              */
		/* with its initial load value                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(povrIO.getStatuz(), varcom.endp)) {
				sv.pbind.set(" ");
				return ;
			}
			else {
				sv.pbind.set("+");
				return ;
			}
		}
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			loadProgramStack4300();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet5200()
	{
		start5210();
	}

protected void start5210()
	{
		/* Release the POVR record as it's no longer required.             */
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.pbind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restoreProgram4100();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
	}



protected void CheckZraeRecords()
{
	zraeIO.setDataKey(SPACES); 
	zraeIO.setChdrcoy(chdrpf.getChdrcoy());
	zraeIO.setChdrnum(chdrpf.getChdrnum());
	zraeIO.setPlanSuffix(ZERO);
	zraeIO.setFunction(varcom.begnh);
	zraeIO.setFormat(zraerec);
	SmartFileCode.execute(appVars, zraeIO);
	if (isNE(zraeIO.getStatuz(), varcom.oK)
	|| isEQ(zraeIO.getStatuz(), varcom.endp)
	|| isEQ(zraeIO.getValidflag(), "2")
	|| isNE(zraeIO.getChdrnum(), chdrpf.getChdrnum())) {
		syserrrec.params.set(zraeIO.getParams());
		syserrrec.statuz.set(zraeIO.getStatuz());
		fatalError600();
	}else
	{
		sv.aepaydet.set("+");
	}
	
}

protected void checkCalcTax5500()
	{
		start5510();
	}

protected void start5510()
	{
		if (isEQ(sv.instPrem, ZERO)
	//ILIFE-1511 Start by slakkala			
		&& (isEQ(sv.singlePremium,ZERO))
	//ILIFE-1511 ends
		) 
		{
			return ;
		}
		/* Call subroutine from table TR52D                                */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(covrpf.getLife());
		txcalcrec.coverage.set(covrpf.getCoverage());
		txcalcrec.rider.set(covrpf.getRider());
		txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		//txcalcrec.rateItem.set(SPACES);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		wsaaCntCurr.set(chdrpf.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		//txcalcrec.amountIn.set(ZERO);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(sv.zbinstprem);
		}
		else {
			txcalcrec.amountIn.set(sv.instPrem);
		}
		txcalcrec.transType.set("PREM");
		txcalcrec.txcode.set(tr52drec.txcode);
		if (isEQ(chdrpf.getBillfreq(), "00")) {
			txcalcrec.effdate.set(chdrpf.getOccdate());
		}
		else {
			txcalcrec.effdate.set(chdrpf.getPtdate());
		}
		txcalcrec.tranno.set(chdrpf.getTranno());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			wsaaMiscellaneousInner.wsaaTaxamt.set(sv.instPrem);
		//ILIFE-1511 start by slakkala	
			wsaaMiscellaneousInner.wsaaTaxamt.add(sv.singlePremium);
		//ILIFE-1511 end	
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[2]);
			}
			sv.taxamt.set(wsaaMiscellaneousInner.wsaaTaxamt);
			sv.taxind.set("+");
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
	}

protected void taxExe5600()
	{
		start5610();
	}

protected void start5610()
	{
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.taxind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			saveProgramStack4200();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("F");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the     */
		/* screen with an error and the options and extras indicator       */
		/* with its initial load value                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			loadProgramStack4300();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet5700()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.taxind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restoreProgram4100();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}
/*ILIFE-3685 Starts by slakkala*/
protected void redTerm6600()
{
	start6610();
}
protected void start6610()
{
	/* Use standard GENSWITCH table switching using the next option    */
	/* on table T1675.                                                 */
	/*  - change the request indicator to '?',                         */

	sv.zsredtrm.set("?");
	/*  - save the next 8 programs from the program stack,             */
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
		saveProgramStack4200();
	}
	/*  - call GENSSWCH with the next table action to retreive the     */
	/*    next program.                                                */
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	gensswrec.function.set("G");
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by genswch redisplay the     */
	/* screen with an error and the options and extras indicator       */
	/* with its initial load value                                     */
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		scrnparams.errorCode.set(h093);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
		loadProgramStack4300();
	}
	/*  - set the current stack "action" to '*',                       */
	/*  - add one to the program pointer and exit.                     */
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}

protected void chdrenq()
{
chdrenqIO.setChdrcoy(wsspcomn.company);
chdrenqIO.setChdrnum(chdrpf.getChdrnum());
chdrenqIO.setFunction(varcom.readr);
chdrenqIO.setFormat(chdrenqrec);
SmartFileCode.execute(appVars, chdrenqIO);
if (isNE(chdrenqIO.getStatuz(),varcom.oK)
&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
	syserrrec.params.set(chdrenqIO.getParams());
	syserrrec.statuz.set(chdrenqIO.getStatuz());
	fatalError600();
}


chdrenqIO.setFunction(varcom.keeps);
SmartFileCode.execute(appVars, chdrenqIO);
if (isNE(chdrenqIO.getStatuz(),varcom.oK)
&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
	syserrrec.params.set(chdrenqIO.getParams());
	syserrrec.statuz.set(chdrenqIO.getStatuz());
	fatalError600();
}	





}


protected void redTrm6700()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.zsredtrm.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restoreProgram4100();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

/*ILIFE-3685 ends by slakkala*/
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
protected static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private PackedDecimalData wsaaSuffixCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5608Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(5).isAPartOf(wsaaT5608Key, 0);
	private FixedLengthStringData wsaaCrcyCode = new FixedLengthStringData(3).isAPartOf(wsaaT5608Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	public PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5608 = new FixedLengthStringData(5).init("T5608");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData mbnsrec = new FixedLengthStringData(10).init("MBNSREC");
	private FixedLengthStringData minsrec = new FixedLengthStringData(10).init("MINSREC");
}

protected void checkIFReduceTermSelected(){

}

protected void checkForDisplayingReduceTerm(){
	
}

protected void customerSpecificSADisplay(){
	
}
protected void readTableTr52e() {
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrpf.getCnttype());
	wsaaTr52eCrtable.set(covrpf.getCrtable());
	a300ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set("****");
		a300ReadTr52e();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a300ReadTr52e();
	}
}
protected void a300ReadTr52e() {
	//	List<Itempf> tr52eList = itemDAO.getAllitemsbyCurrency("IT", wsspcomn.company.toString(), "TR52E", wsaaTr52eKey.toString());
		List<Itempf> tr52eList = itempfDAO.getAllItemitemByDateFrm("IT", wsspcomn.company.toString(), "TR52E", wsaaTr52eKey.toString().trim(), chdrpf.getCcdate()); //ILFE-7560
		if (tr52eList != null && !tr52eList.isEmpty()) {
			Iterator<Itempf> iterator = tr52eList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (itempf.getItmfrm().compareTo(new BigDecimal(chdrpf.getOccdate().toString())) <= 0) {
					tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
					break;
				}
			}
		}else{
			if (isEQ(subString(wsaaTr52eKey, 2, 7), "*******")) {
				syserrrec.params.set(wsaaTr52eKey);
				fatalError600();
			}
		}
	}
}