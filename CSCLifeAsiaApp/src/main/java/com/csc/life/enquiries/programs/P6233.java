/*
 * File: P6233.java
 * Date: 30 August 2009 0:37:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P6233.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.RtrnpfDAO;//ILB-1088
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.screens.S6233ScreenVars;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.newbusiness.dataaccess.dao.ResnpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Resnpf;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Rtrnpf; //ILB-1088
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will be
*     stored  in  the  CHDRENQ I/O module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist.
*
*
*     Load the subfile as follows:
*
*          Perform a BEGN on PTRNENQ with a key of Contract Company
*          (CHDRCOY),  Contract Header (CHDRNUM) and Effective Date
*          (PTRNEFF)  set  to  all  '9's.  Then  read  sequentially
*          through  until end of file or Company or Contract Number
*          changes.
*
*          Display   the   Effective   Date,   (PTRNEFF)   and  the
*          Transaction Code, (BATCTRCODE).  The transaction code is
*          also decoded for its long description against T1688.
*
*
*     Load all  pages  required  in the subfile and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If  "CF11"  was  requested move spaces to the current program
*     position add 1 to the program pointer and exit.
*
*     Release  any  PTRNENQ  record  that  may have been previously
*     stored.
*
*     At this  point  the  program will be either searching for the
*     FIRST  selected  record  in  order  to  pass  control  to the
*     Transactions Postings program for the selected transaction or
*     it  will  be returning from the Transactions Postings program
*     after  displaying  some  details  and  searching for the NEXT
*     selected record.
*
*     It will  be able to determine which of these two states it is
*     in by examining the Stack Action Flag.
*
*     If not returning from a Transactions Postings display, (Stack
*     Action Flag  is  blank),  perform  a  start on the subfile to
*     position the file pointer at the beginning.
*
*     Each  time  it  returns  to  this  program after processing a
*     previous selection its position in the subfile will have been
*     retained and  it  will be able to continue from where it left
*     off.
*
*     Processing from  here is the same for either state. After the
*     Start  or  after  returning to the program after processing a
*     previous selection read the next record from the subfile.  If
*     this  is not selected (Select is blank), continue reading the
*     next  subfile  record  until  one  is  found with a non-blank
*     Select field  or end of file is reached. Do not use the 'Read
*     Next Changed Subfile Record' function.
*
*     If nothing  was  selected  or there are no more selections to
*     process,  continue by just moving spaces to the current stack
*     action field and program position and exit.
*
*     If a  selection  has  been  found  read and store (READS) the
*     associated   PTRNENQ   record  using  the  Contract  Company,
*     Contract  Number and Effective date from the selected subfile
*     record. Add 1 to the program pointer and exit.
*
*
* Notes.
* ------
*
*     Create a  new  view  of PTRNPF called PTRNENQ which uses only
*     those fields required for this program. It should be keyed on
*     CHDRCOY CHDRNUM and PTRNEFF.
*
*     Tables Used:
*
* T1688 - Transaction Codes                 Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6233 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6233");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaRecordFound = new FixedLengthStringData(1);
	private Validator recordFound = new Validator(wsaaRecordFound, "Y");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaDatime = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaYear = new FixedLengthStringData(4).isAPartOf(wsaaDatime, 0);
	private FixedLengthStringData wsaaMonth = new FixedLengthStringData(2).isAPartOf(wsaaDatime, 5);
	private FixedLengthStringData wsaaDay = new FixedLengthStringData(2).isAPartOf(wsaaDatime, 8);

	private FixedLengthStringData wsaaDateFormat = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDfYy = new FixedLengthStringData(4).isAPartOf(wsaaDateFormat, 0);
	private FixedLengthStringData wsaaDfMm = new FixedLengthStringData(2).isAPartOf(wsaaDateFormat, 4);
	private FixedLengthStringData wsaaDfDd = new FixedLengthStringData(2).isAPartOf(wsaaDateFormat, 6);
	private ZonedDecimalData wsaaTxDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaDateFormat, 0, REDEFINE).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String e494 = "E494";
	private static final String h093 = "H093";
	private static final String bclm02 = "BCLM02";
	/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6233ScreenVars sv = ScreenProgram.getScreenVars( S6233ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();

	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();
	private FixedLengthStringData wsaaT502 = new FixedLengthStringData(4).init("T502");
	private FixedLengthStringData wsaaT510 = new FixedLengthStringData(4).init("T510");
	private FixedLengthStringData wsaaT512 = new FixedLengthStringData(4).init("T512");
	private FixedLengthStringData wsaaT539 = new FixedLengthStringData(4).init("T539");
	private FixedLengthStringData wsaaT542 = new FixedLengthStringData(4).init("T542");
	private FixedLengthStringData wsaaT668 = new FixedLengthStringData(4).init("T668");
	private FixedLengthStringData wsaaT669 = new FixedLengthStringData(4).init("T669");
	private FixedLengthStringData wsaaT671 = new FixedLengthStringData(4).init("T671");
	private FixedLengthStringData wsaaTASO = new FixedLengthStringData(4).init("TASO");
	private FixedLengthStringData wsaaTASP = new FixedLengthStringData(4).init("TASP");
	private static final String g889 = "G889";
	private boolean ispermission = false ;
	private FeaConfg feaConfg = new FeaConfg();
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private PtrnpfDAO ptrnDao = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
	private Ptrnpf ptrnpf = new Ptrnpf(); 
	private List<Ptrnpf> ptrnList;

	// ILB-326:Starts
	private FixedLengthStringData wsaaPrevtrannosearch =  new FixedLengthStringData(5);
	private FixedLengthStringData wsaaPrevdatesubsearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPreveffdatesearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPrevtrcodesearch =  new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevtrandescsearch =  new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPrevcrtusersearch =  new FixedLengthStringData(10);
	
	// ILB-326:Ends
//	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvDAO",AcmvpfDAO.class);
	private Map<String, Descpf> t1688DescMap = null;
	private Map<Integer, Acmvpf> acmvpfMap = null;

	//ILB-498 starts
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private Clntpf clntpf = new Clntpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO",DescDAO.class);
	private Descpf descpf = new Descpf();
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO",HitspfDAO.class);
	private Hitspf hitspf = new Hitspf();
	private List<Hitspf> hitspfList;
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO",UtrspfDAO.class);
	private Utrspf utrspf = new Utrspf();
	private List<Utrspf> utrspfList;
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	//ILB-498 ends
	
	private FixedLengthStringData wsaaTasj = new FixedLengthStringData(4).init("TASJ");
	private FixedLengthStringData wsaaTasl = new FixedLengthStringData(4).init("TASL");
	private FixedLengthStringData wsaaTask = new FixedLengthStringData(4).init("TASK");
	private FixedLengthStringData wsaaTasr = new FixedLengthStringData(4).init("TASR");
	boolean suotr008Permission = false;
	boolean CMDTH010Permission = false;
	private RtrnpfDAO rtrnDao = getApplicationContext().getBean("rtrnpfDAO",RtrnpfDAO.class);//ILB-1088
	private Rtrnpf rtrnpf = new Rtrnpf();//ILB-1088
    private ResnpfDAO resnpfDAO = getApplicationContext().getBean("resnpfDAO",ResnpfDAO.class);
	private Map<String, Resnpf> resnpfMap;
	private Resnpf resnpf = new Resnpf();
    private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		updateErrorIndicators2670, 
		bypassStart4010, 
		nextProgram4080, 
		nextProgram4085, 
		exit4090, 
		checkHits4850
	}

	public P6233() {
		super();
		screenVars = sv;
		new ScreenModel("S6233", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
	
		suotr008Permission  = FeaConfg.isFeatureExist("2", "SUOTR008", appVars, "IT");
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", "CMDTH010", appVars, "IT");
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		// ILB-326:Starts
		wsaaPrevtrannosearch.set("00000");
		wsaaPrevdatesubsearch.set("99999999");
		wsaaPreveffdatesearch.set("99999999");
		wsaaPrevtrcodesearch.set(SPACES);
		wsaaPrevtrandescsearch.set(SPACES);
		wsaaPrevcrtusersearch.set(SPACES);
		sv.datesubsearchDisp.clear();
		sv.effdatesearchDisp.clear();
		// ILB-326:Ends
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);

					if(null==chdrpf){
						chdrenqIO.setFunction(Varcom.retrv);
						chdrenqIO.setFormat(formatsInner.chdrenqrec);
						SmartFileCode.execute(appVars, chdrenqIO);
						if (isNE(chdrenqIO.getStatuz(), Varcom.oK)) {
							syserrrec.params.set(chdrenqIO.getParams());
							fatalError600();
						}
						else {
							chdrpf = chdrDao.getChdrpf(chdrenqIO.getChdrcoy().toString(), chdrenqIO.getChdrnum().toString());
							if(null==chdrpf) {
								fatalError600();
							}
							else {
								chdrDao.setCacheObject(chdrpf);
							}
						}
					} 
					
		
		if (!suotr008Permission)
		{
			sv.indAnnOut[Varcom.pr.toInt()].set("Y");	
		}
		ispermission = feaConfg.isFeatureExist(wsspcomn.company.toString(),bclm02,appVars, "IT");		
		if (!ispermission)
			sv.indicOut[varcom.pr.toInt()].set("Y");		
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		resnpfMap = new HashMap<String, Resnpf>();
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		/*    MOVE CHDRENQ-CHDRCOY        TO LIFEENQ-CHDRCOY.              */
		/*    MOVE CHDRENQ-CHDRNUM        TO LIFEENQ-CHDRNUM.              */
		/*    MOVE '01'                   TO LIFEENQ-LIFE.                 */
		/*    MOVE '00'                   TO LIFEENQ-JLIFE.                */
		/*    MOVE BEGN                   TO LIFEENQ-FUNCTION.             */
		/*    CALL 'LIFEENQIO'            USING LIFEENQ-PARAMS.            */
		/*    IF LIFEENQ-STATUZ           NOT = O-K                        */
		/*         MOVE LIFEENQ-PARAMS    TO SYSR-PARAMS                   */
		/*         PERFORM 600-FATAL-ERROR.                                */
		/*    MOVE LIFEENQ-LIFCNUM        TO S6233-LIFENUM                 */
		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());
		lifepf.setChdrnum(chdrpf.getChdrnum());
		lifepf.setLife("01");
		lifepf.setJlife("00");
		lifepf = lifepfDAO.getLifeRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
		/*lifelnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}*/
		sv.lifenum.set(lifepf.getLifcnum());
		clntpf.setClntnum(lifepf.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpf = clntpfDAO.selectClient(clntpf);
		if (clntpf == null) {
			fatalError600();
		}

/*		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
*/		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		/*    MOVE '01'                   TO LIFEENQ-JLIFE.                */
		/*    MOVE READR                  TO LIFEENQ-FUNCTION.             */
		/*    CALL 'LIFEENQIO'            USING LIFEENQ-PARAMS.            */
		lifepf.setJlife("01");
		List<Lifepf> lifepfList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "01");
		/*lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);*/
		/*  IF   LIFEENQ-STATUZ         NOT = O-K                        */
		/*       MOVE 'NONE'            TO S6233-JLIFE                   */
		/*                                 S6233-JLIFENAME               */
		/*  ELSE                                                         */
		/*    IF   LIFEENQ-STATUZ             = O-K                        */
		/*         MOVE LIFEENQ-LIFCNUM   TO S6233-JLIFE                   */
		/*                                   CLTS-CLNTNUM                  */
		/*         MOVE WSSP-FSUCO        TO CLTS-CLNTCOY                  */
		/*         MOVE 'CN'              TO CLTS-CLNTPFX                  */
		/*         MOVE READR             TO CLTS-FUNCTION                 */
		/*         CALL 'CLTSIO'          USING CLTS-PARAMS                */
		/*         IF CLTS-STATUZ         NOT = O-K                        */
		/*              MOVE CLTS-PARAMS  TO SYSR-PARAMS                   */
		/*              PERFORM 600-FATAL-ERROR                            */
		/*         ELSE                                                    */
		/*              PERFORM PLAINNAME                                  */
		/*              MOVE WSSP-LONGCONFNAME                             */
		/*                                TO S6233-JLIFENAME.              */
		if (!lifepfList.isEmpty()) {
			for (Lifepf life : lifepfList) {
				sv.jlife.set(life.getLifcnum());
				clntpf.setClntnum(life.getLifcnum());
				clntpf.setClntcoy(wsspcomn.fsuco.toString());
				clntpf.setClntpfx("CN");
				clntpf = clntpfDAO.selectClient(clntpf);
				if (clntpf == null) {
					fatalError600();
				}
				else {
					plainname();
					sv.jlifename.set(wsspcomn.longconfname);
				}
			}
		}
		descpf=descDAO.getdescData("IT", t5688, chdrpf.getCnttype(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		
		descpf=descDAO.getdescData("IT", t3623, chdrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descpf.getLongdesc());
		}
		
		descpf=descDAO.getdescData("IT", t3588, chdrpf.getPstcde(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descpf.getLongdesc());
		}
		/*descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descpf.getShortdesc());
		}*/
		/*    Read the first PTRNENQ record for the contract.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		ptrnList = ptrnDao.getPtrnAcmvRtrnRecords(wsspcomn.company.toString(), chdrpf.getChdrnum());
		processPtrn(ptrnList);
		
		/* Do not move 1 to the sunfile RRN if this is a call from a       */
		/* remote device so that the RRN will contain the total number of  */
		/* transaction records.                                            */
		/* MOVE 1                      TO SCRN-SUBFILE-RRN.             */
		if (isNE(scrnparams.deviceInd, "*RMT")) {
			scrnparams.subfileRrn.set(1);
		}
		

		

	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*      (including subfile load section)
	* </pre>
	*/
	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/* LGNM-EXIT */
	}

protected void plainname() {
	/* PLAIN-100 */
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname();

	} else if (isNE(clntpf.getGivname(), SPACES)) {
		String firstName = clntpf.getGivname();
		String lastName = clntpf.getSurname();
		String delimiter = ",";

		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);

	} else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
	/* PLAIN-EXIT */
}

	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
		} else if (isEQ(clntpf.getEthorig(), "1")) {
			String firstName = clntpf.getGivname();
			String lastName = clntpf.getSurname();
			String delimiter = "";
			String salute = clntpf.getSalutl();

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = getStringUtil().includeSalute(salute, fullName);

			wsspcomn.longconfname.set(fullNameWithSalute);
		} else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(clntpf.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(clntpf.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(clntpf.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
		/* PAYEE-EXIT */
	}
	
	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = clntpf.getLgivname();
		String lastName = clntpf.getLsurname();
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}
private void processPtrn(List<Ptrnpf> ptrnTempList)
{
	if(resnpfMap == null ||resnpfMap.isEmpty()){
		resnpfMap = resnpfDAO.searchResnpfMap(wsspcomn.company.toString(), chdrpf.getChdrnum());
	}
	if(t1688DescMap == null){
		t1688DescMap = descDAO.getItems("IT", wsspcomn.company.toString(), t1688, wsspcomn.language.toString());
	}
	if(acmvpfMap == null || acmvpfMap.isEmpty()){
		Ptrnpf p = ptrnTempList.get(0);
		String chdrnum = p.getChdrnum();
		String chdrcoy = p.getChdrcoy();
		acmvpfMap = acmvpfDAO.searchAcmvpfMap(chdrcoy, chdrnum);

	}
	for(Ptrnpf ptrn:ptrnTempList) {
		ptrnpf=ptrn;
		processPtrn1100();
	}
}
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processPtrn1100()
	{
		para1100();
		writeSubfile1110();
	}

protected void para1100()
	{
		/*    Do not select the record if the Transaction Code = B216      */
		/*    IF PTRNENQ-BATCTRCDE        = "B216"                         */
		/*       GO 1120-READ-NEXT.                                        */
		/*                                                         <008>*/
		/*Do not show Valid Flag '2' PTRNs on the subfile            <008>*/
		/*                                                         <008>*/
		/* IF PTRNENQ-VALIDFLAG        = '2'                       <008>*/
		/*    GO TO 1120-READ-NEXT.                                <008>*/
		/*    Set up the Transaction details from the PTRNENQ record.*/
		sv.subfileFields.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.selectOut[varcom.nd.toInt()].set(SPACES);
		sv.hflag.set(SPACES);
		sv.hreason.set(SPACES);
		sv.htxdate.set(varcom.vrcmMaxDate);
		sv.effdate.set(ptrnpf.getPtrneff());
		sv.trcode.set(ptrnpf.getBatctrcde());
		sv.datesub.set(ptrnpf.getDatesub());
		if (isNE(ptrnpf.getCrtuser(), SPACES)) {
			sv.crtuser.set(ptrnpf.getCrtuser());
		}
		else {
			sv.crtuser.set(ptrnpf.getUsrprf());
		}
		sv.tranno.set(ptrnpf.getTranno());
		sv.fillh.set(SPACES);
		sv.filll.set(SPACES);
		/*    Obtain the Transaction Code description from T1688.*/
		if(t1688DescMap==null || !t1688DescMap.containsKey(ptrnpf.getBatctrcde())){
			sv.trandesc.fill("?");
		}else{
			Descpf desc = t1688DescMap.get(ptrnpf.getBatctrcde());
			sv.trandesc.set(desc.getLongdesc());
		}
		
		/*  Obtain the actual transaction date from the date time stamp    */
		wsaaDatime.set(ptrnpf.getDatime());
		wsaaDfYy.set(wsaaYear);
		wsaaDfMm.set(wsaaMonth);
		wsaaDfDd.set(wsaaDay);
		sv.htxdate.set(wsaaTxDate);
		/*  Check to see if a reason for the transaction exists            */
		reason1300();
		/* If PTRN was a Validflag '2', then don't allow selection,        */
		/*   set fields on either side of TRANNO, FILLH & FILLL to         */
		/*   be '***'.                                                     */
		if (isEQ(ptrnpf.getValidflag(), "2")) {
			/*     MOVE 'Y'                TO S6233-SELECT-OUT(PR)  <CAS1.0>*/
			/*     MOVE 'Y'                TO S6233-SELECT-OUT(ND)  <CAS1.0>*/
			sv.fillh.set("***");
			sv.filll.set("***");
			/*****     GO TO 1110-WRITE-SUBFILE                         <CAS1.0>*/
		}
		/*    Attempt to read the ACMVTRN data-set.                      * */
		/*    If no records are found, set the protect for select to "ON"* */
		if(acmvpfMap != null && acmvpfMap.containsKey(ptrnpf.getTranno() )){
			sv.hflag.set("Y");
			sv.dataloc.set("");//acmvtrnIO.getDataLocation()
			return ;
		}
		
		if(suotr008Permission && (isEQ(sv.trcode,wsaaTasj)||isEQ(sv.trcode,wsaaTask)||isEQ(sv.trcode,wsaaTasl) || isEQ(sv.trcode,wsaaTasr))){
			return ;
		}
		if(CMDTH010Permission && isEQ(sv.trcode,wsaaTASO)) {
			return;
		}
		
		/*    Attempt to read the RTRNTRN data-set.                      * */
		/*    If no records are found, set the protect for select to "ON"* */
		//ILB-1088	start
		List<Rtrnpf> rtrnList = rtrnDao.getRtrnPtrnRecords(ptrnpf.getChdrcoy(), ptrnpf.getChdrnum(), ptrnpf.getTranno());
		if(rtrnList.isEmpty()) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			rtrnpf = rtrnList.get(0);
			sv.hflag.set("Y");
		}
		sv.dataloc.set(rtrnpf.getDataloc());
		//ILB-1088 end
	}

protected void writeSubfile1110()
	{
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
	}

	/**
	* <pre>
	*1200-CHECK-ARCHIVES SECTION.                             <LA4407>
	*1200-PARA.                                               <LA4407>
	****                                                      <LA4407>
	**** MOVE SPACES                  TO S6233-HSELECT.       <LA4407>
	****                                                      <LA4407>
	**** MOVE SPACES                  TO ITEM-PARAMS.         <LA4407>
	**** MOVE 'IT'                    TO ITEM-ITEMPFX.        <LA4407>
	**** MOVE WSSP-COMPANY            TO ITEM-ITEMCOY.        <LA4407>
	**** MOVE T3715                   TO ITEM-ITEMTABL.       <LA4407>
	**** MOVE 'ACMV'                  TO ITEM-ITEMITEM.       <LA4407>
	**** MOVE READR                   TO ITEM-FUNCTION.       <LA4407>
	****                                                      <LA4407>
	**** CALL 'ITEMIO' USING ITEM-PARAMS.                     <LA4407>
	****                                                      <LA4407>
	**** IF ITEM-STATUZ          NOT = O-K                    <LA4407>
	****    MOVE ITEM-PARAMS         TO SYSR-PARAMS           <LA4407>
	****    PERFORM 600-FATAL-ERROR                           <LA4407>
	**** END-IF.                                              <LA4407>
	****                                                      <LA4407>
	**** MOVE ITEM-GENAREA           TO T3715-T3715-REC.      <LA4407>
	****                                                      <LA4407>
	**** MOVE SPACES                 TO ARCM-PARAMS.          <LA4407>
	**** MOVE 'ACMV'                 TO ARCM-FILE-NAME.       <LA4407>
	**** MOVE READR                  TO ARCM-FUNCTION.        <LA4407>
	**** MOVE ARCMREC                TO ARCM-FORMAT.          <LA4407>
	****                                                      <LA4407>
	**** CALL 'ARCMIO'               USING ARCM-PARAMS.       <LA4407>
	****                                                      <LA4407>
	**** IF ARCM-STATUZ              NOT = O-K                <LA4407>
	****    AND ARCM-STATUZ          NOT = MRNF               <LA4407>
	****     MOVE ARCM-PARAMS        TO SYSR-PARAMS           <LA4407>
	****     MOVE ARCM-STATUZ        TO SYSR-STATUZ           <LA4407>
	****     PERFORM 600-FATAL-ERROR                          <LA4407>
	**** END-IF.                                              <LA4407>
	**                                                        <LA4407>
	**If the PTRN being processed has an Accounting Period Greater    
	**than the last period Archived then there is no need to  <LA4407>
	**attempt to read the Optical Device.                     <LA4407>
	****                                                      <LA4407>
	**** IF ARCM-STATUZ              = MRNF                   <LA4407>
	****    MOVE ZEROES              TO WSBB-PERIOD           <LA4407>
	**** ELSE                                                 <LA4407>
	****    MOVE ARCM-ACCTYR         TO WSBB-ACTYR            <LA4407>
	****    MOVE ARCM-ACCTMNTH       TO WSBB-ACTMN            <LA4407>
	**** END-IF.                                              <LA4407>
	****                                                      <LA4407>
	**** MOVE PTRNENQ-BATCACTYR      TO WSAA-ACTYR.           <LA4407>
	**** MOVE PTRNENQ-BATCACTMN      TO WSAA-ACTMN.           <LA4407>
	**** IF   WSAA-PERIOD             > WSBB-PERIOD           <LA4407>
	****      GO TO 1290-EXIT                                 <LA4407>
	**** END-IF.                                              <LA4407>
	****                                                      <LA4407>
	**** IF   T3715-ENQFLAG          NOT = SPACES             <LA4407>
	****                                                      <LA4407>
	****      MOVE SPACES            TO ACMCTRN-PARAMS        <LA4407>
	****      MOVE PTRNENQ-CHDRCOY   TO ACMCTRN-RLDGCOY       <LA4407>
	****      MOVE PTRNENQ-CHDRNUM   TO ACMCTRN-RDOCNUM       <LA4407>
	****      MOVE PTRNENQ-TRANNO    TO ACMCTRN-TRANNO        <LA4407>
	****      MOVE BEGN              TO ACMCTRN-FUNCTION      <LA4407>
	****      CALL 'ACMCTRNIO'         USING ACMCTRN-PARAMS   <LA4407>
	****      IF  ACMCTRN-STATUZ     NOT = O-K                <LA4407>
	****      AND ACMCTRN-STATUZ     NOT = ENDP               <LA4407>
	****         MOVE ACMCTRN-PARAMS TO SYSR-PARAMS           <LA4407>
	****         PERFORM 600-FATAL-ERROR                      <LA4407>
	****      END-IF                                          <LA4407>
	****                                                      <LA4407>
	****      IF ACMCTRN-RLDGCOY     NOT = PTRNENQ-CHDRCOY    <LA4407>
	****      OR ACMCTRN-RDOCNUM     NOT = PTRNENQ-CHDRNUM    <LA4407>
	****      OR ACMCTRN-TRANNO      NOT = PTRNENQ-TRANNO     <LA4407>
	****      OR ACMCTRN-STATUZ      = ENDP                   <LA4407>
	****          MOVE SPACES            TO S6233-HSELECT     <LA4407>
	****      ELSE                                            <LA4407>
	****          MOVE 'I'               TO S6233-HSELECT     <LA4407>
	****          MOVE RTRNTRN-DATA-LOCATION                  <LA4407>
	****                                 TO S6233-DATALOC     <LA4407>
	****     END-IF                                           <LA4407>
	****  END-IF.                                             <LA4407>
	****                                                      <LA4407>
	**** IF   T3715-COLDFLAG         NOT = SPACES             <LA4407>
	****      MOVE 'Y'               TO S6233-HSELECT         <LA4407>
	**** END-IF.                                              <LA4407>
	****                                                      <LA4407>
	*1290-EXIT.                                               <LA4407>
	**** EXIT.                                                <LA4407>
	* </pre>
	*/
protected void reason1300()
	{
		start1300();
	}
protected void start1300()
	{
		/*  Read the reason file to see if details exist for the           */
		/*  transaction, if yes move the details to the Hidden screen      */
		/*  field reason                                                   */
		
	if(resnpfMap != null && resnpfMap.containsKey(ptrnpf.getBatctrcde())){

		resnpf = resnpfMap.get(ptrnpf.getBatctrcde());
			sv.hreason.set(resnpf.getResndesc());
		}
		else {
			sv.hreason.set(SPACES);
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo12010();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo12010()
	{
		/*    CALL 'S6233IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6233-DATA-AREA                         */
		/*                         S6233-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, varcom.masm)) {
			wssplife.lifekey.set(ZERO);
		}
		
		
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		filterSearch();
		/*VALIDATE-SCREEN*/
		scrnparams.subfileRrn.set(1);
	}

private void filterSearch(){
	
	if (isNE(sv.trannosearch,wsaaPrevtrannosearch) 
			|| isNE(sv.datesubsearch,wsaaPrevdatesubsearch) 
			|| isNE(sv.effdatesearch,wsaaPreveffdatesearch)
			|| isNE(sv.trcodesearch,wsaaPrevtrcodesearch)
			|| isNE(sv.trandescsearch,wsaaPrevtrandescsearch)
			|| isNE(sv.crtusersearch,wsaaPrevcrtusersearch)) {
		
		wsaaPrevtrannosearch.set(sv.trannosearch);
		wsaaPrevdatesubsearch.set(sv.datesubsearch);
		wsaaPreveffdatesearch.set(sv.effdatesearch);
		wsaaPrevtrcodesearch.set(sv.trcodesearch);
		wsaaPrevtrandescsearch.set(sv.trandescsearch);
		wsaaPrevcrtusersearch.set(sv.crtusersearch);
		
		scrnparams.function.set(varcom.sclr);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		ArrayList<Ptrnpf> filteredList = new ArrayList<>();
		String tranNoStr = sv.trannosearch.toString().trim();
		String datesubStr = sv.datesubsearch.toString().trim();
		String effdateStr = sv.effdatesearch.toString().trim();
		String trcodeStr = sv.trcodesearch.toString().trim();
		String trandescStr = sv.trandescsearch.toString().trim();
		String crtuserStr = sv.crtusersearch.toString().trim();
		
		boolean isTranNoEmpty = "".equalsIgnoreCase(tranNoStr) ? true : "00000".equalsIgnoreCase(tranNoStr);
		boolean isDateSubEmpty = "".equalsIgnoreCase(datesubStr) ? true : "99999999".equalsIgnoreCase(datesubStr);
		boolean isEffDateEmpty = "".equalsIgnoreCase(effdateStr) ? true : "99999999".equalsIgnoreCase(effdateStr);
		boolean isTrCodeEmpty = "".equalsIgnoreCase(trcodeStr);
		boolean isTranDescEmpty = "".equalsIgnoreCase(trandescStr);
		boolean isCrtUserEmpty = "".equalsIgnoreCase(crtuserStr);
		if(isTranNoEmpty && isDateSubEmpty && isEffDateEmpty && isTrCodeEmpty && isTranDescEmpty && isCrtUserEmpty){
			processPtrn(ptrnList);
		}else{
			Descpf descpf = null;
			if(!"".equalsIgnoreCase(trandescStr)){
				descpf= descDAO.getitemByDesc("IT", t1688, wsspcomn.company.toString(), trandescStr);
			}
			if("".equalsIgnoreCase(trandescStr) 
					|| ((descpf != null) &&(isTrCodeEmpty || (descpf.getDescitem() != null && trcodeStr.equalsIgnoreCase(descpf.getDescitem().trim()))))){
				for(Ptrnpf ptrnfpfRec : ptrnList){
					if(isTranNoEmpty || ptrnfpfRec.getTranno().equals(Integer.valueOf(tranNoStr))){
						if(isDateSubEmpty || (ptrnfpfRec.getDatesub().equals(Integer.valueOf(datesubStr)))){
							if(isEffDateEmpty || (ptrnfpfRec.getPtrneff().equals(Integer.valueOf(effdateStr)))){
								if(isTrCodeEmpty || (ptrnfpfRec.getBatctrcde() != null && ptrnfpfRec.getBatctrcde().trim().equalsIgnoreCase(trcodeStr))){
									if(isTranDescEmpty || (ptrnfpfRec.getBatctrcde() != null && descpf.getDescitem()!= null && ptrnfpfRec.getBatctrcde().trim().equalsIgnoreCase(descpf.getDescitem().trim()))){
										if(isCrtUserEmpty || (ptrnfpfRec.getCrtuser() != null 
												&& ((ptrnfpfRec.getCrtuser().trim().equalsIgnoreCase("")) ? (ptrnfpfRec.getUsrprf().trim().equalsIgnoreCase(crtuserStr)) : (ptrnfpfRec.getCrtuser().trim().equalsIgnoreCase(crtuserStr))))){
											filteredList.add(ptrnfpfRec);
										}
									}
								}
							}
						}
					}
				}
				processPtrn(filteredList);
				if(filteredList.size() == 0){
					scrnparams.errorCode.set("E040");
				}
			}else{
				scrnparams.errorCode.set("E040");
			}
		}
		wsspcomn.edterror.set("Y");
	}
}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isEQ(sv.select, SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/*    These options are hard-coded on the screen display. If new   */
		/*    options are introduced then both the screen and this check   */
		/*    should be changed.                                           */
		if (isEQ(sv.select, "1")
				|| isEQ(sv.select, "2")
				|| isEQ(sv.select, "3")
				|| isEQ(sv.select, "4")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.selectErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if(isEQ(sv.select,3) ){
			validateSelect4900(); 
		}
		
		if(suotr008Permission && isEQ(sv.select,4)){
			validateSelect4910(); 
		}

		if (isNE(sv.select, "2")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/*    If control reaches this point then option 2 has been chosen  */
		/*    and the program must check that a UTRS record exists.        */
		/*    First perform a BEGN  on the selected UTRS record. This will */
		/*    return the first UTRS record for the contract, life, policy  */
		/*    and component.                                               */
		//ILB-498 starts
		utrspf.setChdrcoy(wsspcomn.company.toString());
		utrspf.setChdrnum(chdrpf.getChdrnum());
		utrspf.setLife("01");
		utrspf.setCoverage("01");
		utrspf.setRider("00");
		utrspf.setPlanSuffix(0);
		utrspfList = utrspfDAO.searchUtrsRecord(utrspf);
		for (Utrspf utrspf : utrspfList) {
			if ((utrspf == null)
					|| isNE(utrspf.getChdrcoy(), wsspcomn.company)
					|| isNE(utrspf.getChdrnum(), chdrpf.getChdrnum())
					|| isNE(utrspf.getLife(), "01")
					|| isNE(utrspf.getCoverage(), "01")
					|| isNE(utrspf.getRider(), "00")) {
						/*NEXT_SENTENCE*/
					}
			else {
		       List<Utrnpf> list = utrnpfDAO.searchUtrnRecord(wsspcomn.company.toString(), chdrpf.getChdrnum(), "01", "01", "00", sv.tranno.toInt());
		       if(!list.isEmpty()) {
			     goTo(GotoLabel.updateErrorIndicators2670);				
				}
			}
		}
		
		hitspf.setChdrcoy(wsspcomn.company.toString());
		hitspf.setChdrnum(chdrpf.getChdrnum());
		hitspf.setLife("01");
		hitspf.setCoverage("01");
		hitspf.setRider("00");
		hitspf.setPlanSuffix(0);
		hitspfList = hitspfDAO.searchHitsRecord(hitspf);
		for (Hitspf hitspf : hitspfList) {
			if ((hitspf == null)
					|| isNE(hitspf.getChdrcoy(), wsspcomn.company)
					|| isNE(hitspf.getChdrnum(), chdrpf.getChdrnum())
					|| isNE(hitspf.getLife(), "01")
					|| isNE(hitspf.getCoverage(), "01")
					|| isNE(hitspf.getRider(), "00")) {
						sv.selectErr.set(e494);
						sv.select.set(SPACES);
					}
		}
		//ILB-498 ends
	}
//ILIFE-471 Start 

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
				case bypassStart4010: 
					bypassStart4010();
				case nextProgram4080: 
					nextProgram4080();
				case nextProgram4085: 
					nextProgram4085();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*    Release any PTRNENQ record that may have been stored.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		ptrnDao.deleteCacheObject(ptrnpf);
		/*    If returning from a program further down the stack then*/
		/*    bypass the start on the subfile.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.bypassStart4010);
		}
		/*    Re-start the subfile.*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypassStart4010()
	{
		if (isEQ(sv.select, SPACES)) {
			while ( !(isNE(sv.select, SPACES)
			|| isEQ(scrnparams.statuz, varcom.endp))) {
				readSubfile4100();
			}
			
		}
		/*  Nothing pressed at all, end working*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/* All requests services,*/
		if (isEQ(scrnparams.statuz, varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.nextProgram4085);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			saveProgramStack4200();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		/* Blank out action to ensure it is not processed again*/
		/* MOVE SPACE                  TO S6233-SELECT.                 */
		/*    Read and store the selected PTRNENQ record.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		ptrnpf = ptrnDao.getPtrnRecord(wsspcomn.company.toString(), chdrpf.getChdrnum(), sv.effdate.toInt(), sv.trcode.toString(), sv.tranno.toInt());
		if(!(ptrnpf.getUniqueNumber()>0)) {
			fatalError600();
		}
		ptrnDao.setCacheObject(ptrnpf);

		wssplife.unitType.set(SPACES);
		if (isEQ(sv.select, "2")) {
			unitEnquiry4800();
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			gensswrec.function.set("A");
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, varcom.oK)
			&& isNE(gensswrec.statuz, varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			/* If an entry on T1675 was not found by Genswch, redisplay the    */
			/*   the screen with an error.                                     */
			if (isEQ(gensswrec.statuz, varcom.mrnf)) {
				goTo(GotoLabel.exit4090);
			}
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				loadProgramStack4300();
			}
			goTo(GotoLabel.nextProgram4080);
		}
		if (isEQ(sv.select, "1")) {
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			gensswrec.function.set("B");
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, varcom.oK)
			&& isNE(gensswrec.statuz, varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			/* If an entry on T1675 was not found by Genswch, redisplay the    */
			/*   the screen with an error.                                     */
			if (isEQ(gensswrec.statuz, varcom.mrnf)) {
				goTo(GotoLabel.exit4090);
			}
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
				loadProgramStack4300();
			}
			goTo(GotoLabel.nextProgram4080);
		}
		if (isEQ(sv.select, "3")) {
			selectClaimTransaction4900();
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);		
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, varcom.oK)
					&& isNE(gensswrec.statuz, varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			/* If an entry on T1675 was not found by Genswch, redisplay the    */
			/*   the screen with an error.                                     */
			if (isEQ(gensswrec.statuz, varcom.mrnf)) {
				goTo(GotoLabel.exit4090);
			}
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			//for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
			//}
			goTo(GotoLabel.nextProgram4080);

		}
		
		if (isEQ(sv.select, "4")) {
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			gensswrec.function.set("N");
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, varcom.oK)
			&& isNE(gensswrec.statuz, varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			/* If an entry on T1675 was not found by Genswch, redisplay the    */
			/*   the screen with an error.                                     */
			if (isEQ(gensswrec.statuz, varcom.mrnf)) {
				goTo(GotoLabel.exit4090);
			}
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
				loadProgramStack4300();
			}
			goTo(GotoLabel.nextProgram4080);
		}
	}

	protected void selectClaimTransaction4900(){

		if(isEQ(sv.trcode,wsaaT668)
				||isEQ(sv.trcode,wsaaT669)
				||isEQ(sv.trcode,wsaaT502)
				||isEQ(sv.trcode,wsaaT671)){
			keepsChdrclm5000();
			gensswrec.function.set("C");
			return;

		}
		if(CMDTH010Permission && isEQ(sv.trcode,wsaaTASO)) {
			keepsChdrclm5000();
			gensswrec.function.set("O");
			return;
		}

		if(isEQ(sv.trcode,wsaaT512)){
			keepsChdrsur5100();
			gensswrec.function.set("S");
			return;

		}
		if(isEQ(sv.trcode,wsaaT542)||isEQ(sv.trcode,wsaaT539)){
			keepsChdrmat5200();
			gensswrec.function.set("M");
			return;

		}
		if(isEQ(sv.trcode,wsaaT510)){
			keepsChdrpts5300();			
			gensswrec.function.set("P");
			return;

		}



	}

	/**
	* <pre>
	**** MOVE '*'                    TO WSSP-SEC-ACTN                 
	****                               (WSSP-PROGRAM-PTR).            
	**** MOVE S6233-HSELECT          TO WSSP-UPDATE-FLAG.     <V72L08>
	**** ADD 1                       TO WSSP-PROGRAM-PTR.             
	* </pre>
	*/
protected void nextProgram4080()
	{
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}

protected void nextProgram4085()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S6233", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void unitEnquiry4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case checkHits4850: 
					checkHits4850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		/*    First perform a BEGN  on the selected UTRS record. This will */
		/*    return the first UTRS record for the contract, life, policy  */
		/*    and component.                                               */
		//ILB-498
		utrspfDAO.deleteCacheObject(utrspf);
		
		utrspf.setChdrcoy(wsspcomn.company.toString());
		utrspf.setChdrnum(chdrpf.getChdrnum());
		utrspf.setLife("01");
		utrspf.setCoverage("01");
		utrspf.setRider("00");
		utrspf.setPlanSuffix(0);
		utrspfList = utrspfDAO.searchUtrsRecord(utrspf);
		//for (Utrspf utrspf : utrspfList) { modified for ILIFE-8391
		//if ((utrspf == null)
		if ((null == utrspfList || utrspfList.isEmpty())
				|| isNE(utrspf.getChdrcoy(), wsspcomn.company)
				|| isNE(utrspf.getChdrnum(), chdrpf.getChdrnum())
				|| isNE(utrspf.getLife(), "01")
				|| isNE(utrspf.getCoverage(), "01")
				|| isNE(utrspf.getRider(), "00")
				|| isNE(utrspf.getPlanSuffix(), ZERO)) {
			wsaaRecordFound.set("N");
			wssplife.unitType.set("D");
			goTo(GotoLabel.checkHits4850);
		}
		/*    If in Policy Level enquiry the Plan Suffix from the selected */
		/*    Policy is placed in the UTRS record before it is saved.      */
		wsaaRecordFound.set("Y");
		/*    IF   POLICY-LEVEL-ENQUIRY                                    */
		/*         MOVE S6233-PLAN-SUFFIX TO UTRS-PLAN-SUFFIX.             */
		utrspfDAO.setCacheObject(utrspf);
		//ILB-498 ends
	}

protected void checkHits4850()
	{
		//ILB-498
		hitspfDAO.deleteCacheObject(hitspf);
		
		hitspf.setChdrcoy(wsspcomn.company.toString());
		hitspf.setChdrnum(chdrpf.getChdrnum());
		hitspf.setLife("01");
		hitspf.setCoverage("01");
		hitspf.setRider("00");
		hitspf.setPlanSuffix(0);
		hitspfList = hitspfDAO.searchHitsRecord(hitspf);
		for (Hitspf hitspf : hitspfList) {
			if ((hitspf == null)
					|| isNE(hitspf.getChdrcoy(), wsspcomn.company)
					|| isNE(hitspf.getChdrnum(), chdrpf.getChdrnum())
					|| isNE(hitspf.getLife(), "01")
					|| isNE(hitspf.getCoverage(), "01")
					|| isNE(hitspf.getRider(), "00")) {
						/*    OR HITS-PLAN-SUFFIX         NOT = S6233-HSUFFIX              */
						if (recordFound.isTrue()) {
							return ;
						}
						else {
							fatalError600();
						}
			}
		}
		/*    IF POLICY-LEVEL-ENQUIRY                                      */
		/*        MOVE S6233-PLAN-SUFFIX  TO HITS-PLAN-SUFFIX              */
		/*    END-IF.                                                      */
		hitspfDAO.setCacheObject(hitspf);
	}
	

	protected void keepsChdrclm5000(){
		chdrclmIO.setChdrcoy(wsspcomn.company);
		chdrclmIO.setChdrnum(sv.chdrnum);
		chdrclmIO.setFunction(varcom.readr);
		chdrclmIO.setFormat(formatsInner.chdrclmrec);
		SmartFileCode.execute(appVars, chdrclmIO);
		chdrclmIO.setFunction("KEEPS");
		chdrclmIO.setFormat(formatsInner.chdrclmrec);
		SmartFileCode.execute(appVars, chdrclmIO);
		if(isNE(chdrclmIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
	}

	protected void keepsChdrsur5100(){
		chdrsurIO.setChdrcoy(wsspcomn.company);
		chdrsurIO.setChdrnum(sv.chdrnum);
		chdrsurIO.setFunction(varcom.readr);
		chdrsurIO.setFormat(formatsInner.chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		chdrsurIO.setFunction("KEEPS");
		chdrsurIO.setFormat(formatsInner.chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);


		if(isNE(chdrsurIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrsurIO.getStatuz());
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
	}

	protected void keepsChdrmat5200(){
		chdrmatIO.setChdrcoy(wsspcomn.company);
		chdrmatIO.setChdrnum(sv.chdrnum);
		chdrmatIO.setFunction(varcom.readr);
		chdrmatIO.setFormat(formatsInner.chdrmatrec);
		SmartFileCode.execute(appVars, chdrmatIO);
		chdrmatIO.setFunction("KEEPS");
		chdrmatIO.setFormat(formatsInner.chdrmatrec);
		SmartFileCode.execute(appVars, chdrmatIO);

		if(isNE(chdrmatIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrmatIO.getStatuz());
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
	}

	protected void keepsChdrpts5300(){
		chdrptsIO.setChdrcoy(wsspcomn.company);
		chdrptsIO.setChdrnum(sv.chdrnum);
		chdrptsIO.setFunction(varcom.readr);
		chdrptsIO.setFormat(formatsInner.chdrptsrec);
		SmartFileCode.execute(appVars, chdrptsIO);
		chdrptsIO.setFunction("KEEPS");
		chdrptsIO.setFormat(formatsInner.chdrptsrec);
		SmartFileCode.execute(appVars, chdrptsIO);
		if(isNE(chdrptsIO.getStatuz(),varcom.oK)){
			syserrrec.statuz.set(chdrptsIO.getStatuz());
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
	}

	protected void validateSelect4900(){
		if(isEQ(sv.select,3)){
			if(isEQ(sv.trcode,wsaaT668)
					||isEQ(sv.trcode,wsaaT510)
					||isEQ(sv.trcode,wsaaT539)
					||isEQ(sv.trcode,wsaaT542)
					||isEQ(sv.trcode,wsaaT669)
					||isEQ(sv.trcode,wsaaT502)
					||isEQ(sv.trcode,wsaaT512)//ILIFE-7836
					||isEQ(sv.trcode,wsaaT671)){
				if(isEQ(sv.fillh,"***")){
					sv.selectErr.set(g889);	//ILJ-521
					sv.select.set(SPACES);
				}
			}
			else if(CMDTH010Permission && isEQ(sv.trcode,wsaaTASO)){
				for(Ptrnpf p : ptrnList){
					if(p.getBatctrcde().equals(wsaaTASP.toString())){
						sv.selectErr.set(g889);	
						sv.select.set(SPACES);
					}
				}
			}
			else{
				sv.selectErr.set(g889);	
				sv.select.set(SPACES);
			}
		} }

	protected void validateSelect4910(){
		if(isEQ(sv.select,4)){
			if(isEQ(sv.trcode,wsaaTasj)
					||isEQ(sv.trcode,wsaaTask)
					||isEQ(sv.trcode,wsaaTasl)
					||isEQ(sv.trcode,wsaaTasr)){
				wsspcomn.effdate.set(sv.datesub);
				String tran = sv.tranno.toString().replaceFirst("^0+(?!$)", "");
				wsspcomn.tranno.set(tran);
				wsspcomn.batctrcde.set(sv.trcode);
				
			}else{
				sv.selectErr.set(e005);	
				sv.select.set(SPACES);
			}
		} 
	}
	
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 
		private FixedLengthStringData chdrclmrec = new FixedLengthStringData(10).init("CHDRCLMREC");
		private FixedLengthStringData chdrsurrec = new FixedLengthStringData(10).init("CHDRSURREC");
		private FixedLengthStringData chdrmatrec = new FixedLengthStringData(10).init("CHDRMATREC");
		private FixedLengthStringData chdrptsrec = new FixedLengthStringData(10).init("CHDRPTSREC");
		private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	}
	public StringUtil getStringUtil() {
		return stringUtil;
	}
}
