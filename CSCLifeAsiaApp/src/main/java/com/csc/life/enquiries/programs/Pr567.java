/*
 * File: Pr567.java
 * Date: 30 August 2009 1:42:02
 * Author: Quipoz Limited
 * 
 * Class transformed from PR567.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.CovttrmTableDAM;
import com.csc.life.productdefinition.dataaccess.MinsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.screens.Sr567ScreenVars;
import com.csc.life.productdefinition.tablestructures.Th616rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  This program to display Installment Premium Details during
*  Contract Enquiry is Provided.
*
*****************************************************************
* </pre>
*/
public class Pr567 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR567");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTotInstalment = new PackedDecimalData(17, 2);
		/* TABLES */
	private String th616 = "TH616";
	private String itemrec = "ITEMREC";
	private String mrtarec = "MRTAREC";
	private String minsrec = "MINSREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Enquiry - Contract Header.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
		/*Coverage transactions - term*/
	private CovttrmTableDAM covttrmIO = new CovttrmTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
		/*Installment Schedule Details*/
	private MinsTableDAM minsIO = new MinsTableDAM();
		/*Reducing Term Assurance details*/
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private Th616rec th616rec = new Th616rec();
	private Sr567ScreenVars sv = ScreenProgram.getScreenVars( Sr567ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Pr567() {
		super();
		screenVars = sv;
		new ScreenModel("Sr567", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaIndex.set(ZERO);
		sv.linstamt01.set(ZERO);
		sv.linstamt02.set(ZERO);
		sv.linstamt03.set(ZERO);
		sv.linstamt04.set(ZERO);
		sv.linstamt05.set(ZERO);
		sv.linstamt06.set(ZERO);
		sv.linstamt07.set(ZERO);
		sv.mnth01.set(ZERO);
		sv.mnth02.set(ZERO);
		sv.mnth03.set(ZERO);
		sv.mnth04.set(ZERO);
		sv.mnth05.set(ZERO);
		sv.mnth06.set(ZERO);
		sv.mnth07.set(ZERO);
		sv.datedue01.set(varcom.vrcmMaxDate);
		sv.datedue02.set(varcom.vrcmMaxDate);
		sv.datedue03.set(varcom.vrcmMaxDate);
		sv.datedue04.set(varcom.vrcmMaxDate);
		sv.datedue05.set(varcom.vrcmMaxDate);
		sv.datedue06.set(varcom.vrcmMaxDate);
		sv.datedue07.set(varcom.vrcmMaxDate);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		covrpf = covrDao.getCacheObject(covrpf);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		
		minsIO.setChdrcoy(chdrpf.getChdrcoy());
		minsIO.setChdrnum(chdrpf.getChdrnum());
		minsIO.setLife(covrpf.getLife());
		minsIO.setCoverage(covrpf.getCoverage());
		minsIO.setRider(covrpf.getRider());
		minsIO.setFormat(minsrec);
		minsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(),varcom.oK)
		&& isNE(minsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		if (isEQ(minsIO.getStatuz(),varcom.oK)) {
			sv.linstamt01.set(minsIO.getLinstamt01());
			sv.linstamt02.set(minsIO.getLinstamt02());
			sv.linstamt03.set(minsIO.getLinstamt03());
			sv.linstamt04.set(minsIO.getLinstamt04());
			sv.linstamt05.set(minsIO.getLinstamt05());
			sv.linstamt06.set(minsIO.getLinstamt06());
			sv.linstamt07.set(minsIO.getLinstamt07());
			sv.datedue01.set(minsIO.getDatedue01());
			sv.datedue02.set(minsIO.getDatedue02());
			sv.datedue03.set(minsIO.getDatedue03());
			sv.datedue04.set(minsIO.getDatedue04());
			sv.datedue05.set(minsIO.getDatedue05());
			sv.datedue06.set(minsIO.getDatedue06());
			sv.datedue07.set(minsIO.getDatedue07());
		}
		readMrta1100();
		if (isNE(mrtaIO.getMlinsopt(),SPACES)) {
			readTh616Table1200();
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,th616rec.mnth)); wsaaIndex.add(1)){
				sv.mnth[wsaaIndex.toInt()].set(wsaaIndex);
			}
		}
	}

protected void readMrta1100()
	{
		/*START*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		mrtaIO.setChdrcoy(chdrpf.getChdrcoy());
		mrtaIO.setChdrnum(chdrpf.getChdrnum());
		mrtaIO.setFormat(mrtarec);
		mrtaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mrtaIO);
		/*EXIT*/
	}

protected void readTh616Table1200()
	{
		start1200();
	}

protected void start1200()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(th616);
		itemIO.setItemitem(mrtaIO.getMlinsopt());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th616rec.th616Rec.set(itemIO.getGenarea());
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.function.set(varcom.prot);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		scrnparams.function.set("HIDEW");
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
