package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sr5b3
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class Sr5b3ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(195);
	public FixedLengthStringData dataFields = new FixedLengthStringData(67).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 67);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 32);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	

	public FixedLengthStringData subfileArea = new FixedLengthStringData(346);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(151).isAPartOf(subfileArea, 0);
	public FixedLengthStringData nlgflag = DD.nlgflag.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public ZonedDecimalData nlgbal = DD.nlgbal.copyToZonedDecimal().isAPartOf(subfileFields,9);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(subfileFields,26);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,60);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,61);
	public ZonedDecimalData tranamt01 = DD.tranamt01.copyToZonedDecimal().isAPartOf(subfileFields,66);
	public ZonedDecimalData tranamt02 = DD.tranamt02.copyToZonedDecimal().isAPartOf(subfileFields,83);
	public ZonedDecimalData tranamt03 = DD.tranamt03.copyToZonedDecimal().isAPartOf(subfileFields,100);
	public ZonedDecimalData tranamt04 = DD.tranamt04.copyToZonedDecimal().isAPartOf(subfileFields,117);
	public ZonedDecimalData tranamt = DD.tranamt.copyToZonedDecimal().isAPartOf(subfileFields,134);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 151);
	public FixedLengthStringData nlgflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData nlgbalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData batctrcdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData tranamt01Err = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData tranamt02Err = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData tranamt03Err = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData tranamt04Err = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData tranamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 199);
	public FixedLengthStringData[] nlgflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] nlgbalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] batctrcdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] tranamt01Out = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] tranamt02Out = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] tranamt03Out = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] tranamt04Out = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] tranamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 343);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sr5b3screensflWritten = new LongData(0);
	public LongData Sr5b3screenctlWritten = new LongData(0);
	public LongData Sr5b3screenWritten = new LongData(0);
	public LongData Sr5b3protectWritten = new LongData(0);
	public GeneralTable sr5b3screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr5b3screensfl;
	}

	public Sr5b3ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","20","-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {nlgflag, effdate, batctrcde, tranno, nlgbal, trandesc, tranamt,tranamt01,tranamt02,tranamt03,tranamt04, select};
		screenSflOutFields = new BaseData[][] {nlgflagOut, effdateOut,batctrcdeOut, trannoOut, nlgbalOut, trandescOut,tranamtOut,tranamt01Out,tranamt02Out,tranamt03Out,tranamt04Out, selectOut};
		screenSflErrFields = new BaseData[] {nlgflagErr, effdateErr,batctrcdeErr, trannoErr, nlgbalErr, trandescErr, tranamtErr,tranamt01Err,tranamt02Err,tranamt03Err,tranamt04Err,selectErr};
		screenSflDateFields = new BaseData[] {effdate};
		screenSflDateErrFields = new BaseData[] {effdateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr5b3screen.class;
		screenSflRecord = Sr5b3screensfl.class;
		screenCtlRecord = Sr5b3screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr5b3protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr5b3screenctl.lrec.pageSubfile);
	}
}
