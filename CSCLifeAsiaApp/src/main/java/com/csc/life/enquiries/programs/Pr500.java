/*
 * File: Pr500.java
 * Date: 30 August 2009 1:30:40
 * Author: Quipoz Limited
 * 
 * Class transformed from PR500.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.recordstructures.Sdartnrec;
import com.csc.life.contractservicing.procedures.P5051bo;
import com.csc.life.enquiries.dataaccess.AgntenqTableDAM;
import com.csc.life.enquiries.dataaccess.AsgnenqTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.Sr500ScreenVars;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Ldrhdr;
import com.csc.smart.recordstructures.Msgdta;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*         LIFE WEB ENQUIRY - CONTRACT
*         ---------------------------
*  This program is cloned from P6353 for the purpose of Web
*  Enquiry for Agency.
*
*****************************************************************
*
* </pre>
*/
public class Pr500 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR500");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaDftCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaDftBatchkey = new FixedLengthStringData(22);

	private FixedLengthStringData wsaaPlanProc = new FixedLengthStringData(1);
	private Validator plan = new Validator(wsaaPlanProc, "Y");
	private Validator nonplan = new Validator(wsaaPlanProc, "N");

	private FixedLengthStringData wsaaInKey = new FixedLengthStringData(50);
	private FixedLengthStringData wsaaInChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaInKey, 0);
	private ZonedDecimalData wsaaInDteeff = new ZonedDecimalData(8, 0).isAPartOf(wsaaInKey, 8).setUnsigned();

	private FixedLengthStringData wsaaWsspChdrky = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaWsspChdrpfx = new FixedLengthStringData(2).isAPartOf(wsaaWsspChdrky, 0);
	private FixedLengthStringData wsaaWsspChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaWsspChdrky, 2);
	private FixedLengthStringData wsaaWsspChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaWsspChdrky, 3);

		/*Fields for use with the CLRF logical.*/
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
		/* WSAA-WSSPS */
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSubmenu = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaNextprog = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaSectionno = new FixedLengthStringData(4);
	private PackedDecimalData wsaaProgramPtr = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaSecSwitching = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaSecActns = new FixedLengthStringData(22).isAPartOf(wsaaSecSwitching, 0);
	private FixedLengthStringData[] wsaaSecActn = FLSArrayPartOfStructure(22, 1, wsaaSecActns, 0);
	private FixedLengthStringData wsaaSecProgs = new FixedLengthStringData(110).isAPartOf(wsaaSecSwitching, 22);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(22, 5, wsaaSecProgs, 0);
	private static final String g620 = "G620";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String payrrec = "PAYRREC";
	private static final String clrfrec = "CLRFREC";
	private static final String hpadrec = "HPADREC";
	private static final String usrdrec = "USRDREC";
	private AgntenqTableDAM agntenqIO = new AgntenqTableDAM();
	private AsgnenqTableDAM asgnenqIO = new AsgnenqTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Optswchrec optswchrec = new Optswchrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sdartnrec sdartnrec = new Sdartnrec();
	private Ldrhdr ldrhdr = new Ldrhdr();
	private Msgdta responseData = new Msgdta();
	private Wssplife wssplife = new Wssplife();
	private Sr500ScreenVars sv = ScreenProgram.getScreenVars( Sr500ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		jointOwnerName1030, 
		payerName1040, 
		agentName1050, 
		exit1090, 
		exit2090
	}

	public Pr500() {
		super();
		screenVars = sv;
		new ScreenModel("Sr500", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					ownerName1020();
				case jointOwnerName1030: 
					jointOwnerName1030();
				case payerName1040: 
					payerName1040();
				case agentName1050: 
					agentName1050();
					assignees1060();
					branchName1070();
					checkPolicyLoan1070();
					optionSwitch1080();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.company.set(wsaaDftCompany);
			wsspcomn.batchkey.set(wsaaDftBatchkey);
			goTo(GotoLabel.exit1090);
		}
		wsaaDftCompany.set(wsspcomn.company);
		wsaaDftBatchkey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.btdate.set(ZERO);
		sv.currfrom.set(ZERO);
		sv.instpramt.set(ZERO);
		sv.nextinsamt.set(ZERO);
		sv.numpols.set(ZERO);
		sv.hpropdte.set(ZERO);
		sv.hprrcvdt.set(ZERO);
		sv.huwdcdte.set(ZERO);
		sv.hissdte.set(ZERO);
		sv.hoissdte.set(ZERO);
		sv.ptdate.set(ZERO);
		/* Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*    If the 'Number of Policies in Plan' field on the Contract*/
		/*    Header is not zeros then Plan Processing is applicable.*/
		if (isEQ(chdrenqIO.getPolinc(), ZERO)
		|| isEQ(chdrenqIO.getPolinc(), 1)) {
			wsaaPlanProc.set("N");
		}
		else {
			wsaaPlanProc.set("Y");
		}
		/*    If Plan Processing is not applicable, protect and*/
		/*    non-display the Plan Processing fields (number available,*/
		/*    number applicable and total number of policies in plan).*/
		if (plan.isTrue()) {
			sv.numpols.set(chdrenqIO.getPolinc());
		}
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getRegister());
		sv.cownnum.set(chdrenqIO.getCownnum());
		if (isNE(chdrenqIO.getJownnum(), SPACES)) {
			sv.jowner.set(chdrenqIO.getJownnum());
		}
		sv.servagnt.set(chdrenqIO.getAgntnum());
		sv.servbr.set(chdrenqIO.getCntbranch());
		sv.currfrom.set(chdrenqIO.getOccdate());
		sv.lastinsdte.set(chdrenqIO.getInstfrom());
		sv.instpramt.set(chdrenqIO.getInststamt06());
		/* Read the PAYR file for billing information.*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		payrIO.setChdrnum(chdrenqIO.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.ptdate.set(payrIO.getPtdate());
		sv.btdate.set(payrIO.getBtdate());
		sv.mop.set(payrIO.getBillchnl());
		sv.payfreq.set(payrIO.getBillfreq());
		sv.nextinsdte.set(payrIO.getBillcd());
		sv.nextinsamt.set(payrIO.getSinstamt06());
		sv.billcurr.set(payrIO.getBillcurr());
		if (isEQ(sv.btdate, ZERO)) {
			sv.btdate.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.currfrom, ZERO)) {
			sv.currfrom.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.lastinsdte, ZERO)) {
			sv.lastinsdte.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.nextinsdte, ZERO)) {
			sv.nextinsdte.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.ptdate, ZERO)) {
			sv.ptdate.set(varcom.vrcmMaxDate);
		}
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
		}
		else {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

	/**
	* <pre>
	*    Obtain the names of the other lives on the contract header.
	* </pre>
	*/
protected void ownerName1020()
	{
		if (isEQ(sv.cownnum, SPACES)) {
			goTo(GotoLabel.jointOwnerName1030);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
	}

protected void jointOwnerName1030()
	{
		if (isEQ(sv.jowner, SPACES)) {
			goTo(GotoLabel.payerName1040);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.jowner);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.jownername.set(wsspcomn.longconfname);
	}

protected void payerName1040()
	{
		/* Read the Client Role file CLRF logical to find details of*/
		/* the Payer.*/
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(chdrenqIO.getChdrcoy());
		wsaaChdrnum.set(chdrenqIO.getChdrnum());
		wsaaPayrseqno.set("1");
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		clrfIO.setFormat(clrfrec);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)
		&& isNE(clrfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clrfIO.getStatuz(), varcom.mrnf)) {
			sv.payer.set(SPACES);
		}
		else {
			sv.payer.set(clrfIO.getClntnum());
		}
		if (isEQ(sv.payer, SPACES)) {
			goTo(GotoLabel.agentName1050);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.payer);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.payername.set(wsspcomn.longconfname);
	}

protected void agentName1050()
	{
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(chdrenqIO.getAgntcoy());
		agntenqIO.setAgntnum(chdrenqIO.getAgntnum());
		agntenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agntenqIO.getStatuz());
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.servagnam.set(wsspcomn.longconfname);
	}

protected void assignees1060()
	{
		asgnenqIO.setDataKey(SPACES);
		asgnenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		asgnenqIO.setChdrnum(chdrenqIO.getChdrnum());
		asgnenqIO.setSeqno(ZERO);
		asgnenqIO.setAsgnnum(SPACES);
		asgnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		asgnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		asgnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, asgnenqIO);
		if (isNE(asgnenqIO.getStatuz(), varcom.oK)
		&& isNE(asgnenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(asgnenqIO.getParams());
			fatalError600();
		}
		if (isNE(asgnenqIO.getChdrcoy(), chdrenqIO.getChdrcoy())
		|| isNE(asgnenqIO.getChdrnum(), chdrenqIO.getChdrnum())
		|| isEQ(asgnenqIO.getStatuz(), varcom.endp)) {
			sv.indic.set(SPACES);
		}
		else {
			sv.indic.set("X");
		}
	}

protected void branchName1070()
	{
		/*    Obtain the Branch name from T1692.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrenqIO.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*         MOVE ALL '?'          TO SR500-BRCHNAME*/
			sv.brchname.set(SPACES);
		}
		else {
			sv.brchname.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*         MOVE ALL '?'          TO SR500-CHDRSTATUS*/
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*         MOVE ALL '?'          TO SR500-PREMSTATUS*/
			sv.premstatus.set(SPACES);
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*   Get any other details from HPADPF.*/
		hpadIO.setChdrcoy(chdrenqIO.getChdrcoy());
		hpadIO.setChdrnum(chdrenqIO.getChdrnum());
		hpadIO.setFunction(varcom.readr);
		hpadIO.setFormat(hpadrec);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.endp)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(), varcom.endp)
		|| isEQ(hpadIO.getStatuz(), varcom.mrnf)) {
			sv.znfopt.set(SPACES);
			sv.hpropdte.set(varcom.vrcmMaxDate);
			sv.hprrcvdt.set(varcom.vrcmMaxDate);
			sv.huwdcdte.set(varcom.vrcmMaxDate);
			sv.hoissdte.set(varcom.vrcmMaxDate);
			sv.hissdte.set(varcom.vrcmMaxDate);
		}
		else {
			sv.znfopt.set(hpadIO.getZnfopt());
			sv.hpropdte.set(hpadIO.getHpropdte());
			sv.hprrcvdt.set(hpadIO.getHprrcvdt());
			sv.huwdcdte.set(hpadIO.getHuwdcdte());
			sv.hoissdte.set(hpadIO.getHoissdte());
			sv.hissdte.set(hpadIO.getHissdte());
		}
	}

protected void checkPolicyLoan1070()
	{
		saveWssp4800();
		sdartnrec.sdarStatuz.set(SPACES);
		sdartnrec.sdarCompany.set(wsspcomn.company);
		wsaaInKey.set(SPACES);
		wsaaInChdrnum.set(sv.chdrnum);
		wsaaInDteeff.set(sv.currfrom);
		sdartnrec.sdarInKey.set(wsaaInKey);
		callProgram(P5051bo.class, ldrhdr.header, sdartnrec.rec, responseData);
		/*                               SDARTN-REC                     */
		/*                               WSSP-USER-AREA                 */
		/*                               WSSP-COMMON-AREA               */
		if (isNE(sdartnrec.sdarStatuz, SPACES)) {
			sv.ind02Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.ind02Out[varcom.pr.toInt()].set(SPACES);
		}
		restoreWssp4900();
		wsspcomn.batchkey.set(wsaaDftBatchkey);
	}

protected void optionSwitch1080()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		usrdIO.setUserid(wsspcomn.userid);
		usrdIO.setFunction(varcom.readr);
		usrdIO.setFormat(usrdrec);
		SmartFileCode.execute(appVars, usrdIO);
		if (isNE(usrdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(usrdIO.getParams());
			syserrrec.statuz.set(usrdIO.getStatuz());
			fatalError600();
		}
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(datcon1rec.intDate);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(usrdIO.getUsernum());
		wsspcomn.flag.set("I");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* Screen errors are now handled in the calling program.*/
		/*    PERFORM 200-SCREEN-ERRORS.*/
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/*    Validate fields*/
		if (isNE(sv.ind01, " ")
		&& isNE(sv.ind01, "+")
		&& isNE(sv.ind01, "X")) {
			sv.ind01Err.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ind02, " ")
		&& isNE(sv.ind02, "+")
		&& isNE(sv.ind02, "X")) {
			sv.ind02Err.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ind03, " ")
		&& isNE(sv.ind03, "+")
		&& isNE(sv.ind03, "X")) {
			sv.ind03Err.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ind04, " ")
		&& isNE(sv.ind04, "+")
		&& isNE(sv.ind04, "X")) {
			sv.ind04Err.set(g620);
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*PARA*/
		/*    There is no updating required in this program.*/
		wsaaWsspChdrnum.set(sv.chdrnum);
		wsaaWsspChdrcoy.set(chdrenqIO.getChdrcoy());
		wsaaWsspChdrpfx.set(SPACES);
		wssplife.chdrky.set(wsaaWsspChdrky);
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		para4000();
	}

protected void para4000()
	{
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(sv.inds, SPACES)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.programPtr.add(1);
			return ;
		}
		if (isEQ(sv.ind01, "X")) {
			wssplife.planPolicy.set("L");
			optswchrec.optsInd[1].set(sv.ind01);
			sv.ind01.set(SPACES);
		}
		else {
			if (isEQ(sv.ind02, "X")) {
				p5051Bo4100();
				wsspcomn.currfrom.set(sv.currfrom);
				optswchrec.optsInd[2].set(sv.ind02);
				sv.ind02.set(SPACES);
			}
			else {
				if (isEQ(sv.ind03, "X")) {
					optswchrec.optsInd[3].set(sv.ind03);
					sv.ind03.set(SPACES);
				}
				else {
					if (isEQ(sv.ind04, "X")) {
						wsspcomn.chdrChdrcoy.set(chdrenqIO.getChdrcoy());
						wsspcomn.chdrChdrnum.set(sv.chdrnum);
						wsspcomn.chdrCnttype.set(sv.cnttype);
						wsspcomn.chdrTypedesc.set(sv.ctypedes);
						wsspcomn.chdrCownnum.set(sv.cownnum);
						wsspcomn.chdrOwnername.set(sv.ownername);
						optswchrec.optsInd[4].set(sv.ind04);
						sv.ind04.set(SPACES);
					}
				}
			}
		}
		optswchrec.optsFunction.set("STCK");
		optswchrec.optsSelType.set("X");
		optswchrec.optsSelCode.set(SPACES);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isNE(optswchrec.optsStatuz, varcom.endp)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
			wsspcomn.nextprog.set(wsaaProg);
			return ;
		}
	}

protected void p5051Bo4100()
	{
		p5051bo4100();
	}

protected void p5051bo4100()
	{
		saveWssp4800();
		sdartnrec.sdarStatuz.set(SPACES);
		sdartnrec.sdarCompany.set(wsspcomn.company);
		wsaaInKey.set(SPACES);
		wsaaInChdrnum.set(sv.chdrnum);
		wsaaInDteeff.set(sv.currfrom);
		sdartnrec.sdarInKey.set(wsaaInKey);
		callProgram(P5051bo.class, ldrhdr.header, sdartnrec.rec, responseData);
		/*                               SDARTN-REC                     */
		/*                               WSSP-USER-AREA                 */
		/*                               WSSP-COMMON-AREA               */
		if (isNE(sdartnrec.sdarStatuz, SPACES)) {
			restoreWssp4900();
			return ;
		}
		wsspcomn.batchkey.set(sdartnrec.sdarBatchkey);
		restoreWssp4900();
	}

protected void saveWssp4800()
	{
		/*SAVE*/
		wsaaFlag.set(wsspcomn.flag);
		wsaaSecProgs.set(wsspcomn.secProgs);
		wsaaSecActns.set(wsspcomn.secActns);
		wsaaProgramPtr.set(wsspcomn.programPtr);
		wsaaSubmenu.set(wsspcomn.submenu);
		wsaaNextprog.set(wsspcomn.nextprog);
		wsaaSectionno.set(wsspcomn.sectionno);
		/*EXIT*/
	}

protected void restoreWssp4900()
	{
		/*RESTORE*/
		wsspcomn.flag.set(wsaaFlag);
		wsspcomn.secProgs.set(wsaaSecProgs);
		wsspcomn.secActns.set(wsaaSecActns);
		wsspcomn.submenu.set(wsaaSubmenu);
		wsspcomn.programPtr.set(wsaaProgramPtr);
		wsspcomn.company.set(wsaaDftCompany);
		wsspcomn.sectionno.set(wsaaSectionno);
		wsspcomn.nextprog.set(wsaaNextprog);
		/*EXIT*/
	}
}
