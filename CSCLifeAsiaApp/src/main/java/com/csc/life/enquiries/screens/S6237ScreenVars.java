package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6237
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6237ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(330);
	public FixedLengthStringData dataFields = new FixedLengthStringData(154).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData servagnam = DD.servagnam.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData servagnt = DD.servagnt.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData servbr = DD.servbr.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData servbrname = DD.servbrname.copy().isAPartOf(dataFields,124);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 154);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData servagnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData servagntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData servbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData servbrnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 198);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] servagnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] servagntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] servbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] servbrnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(231);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(69).isAPartOf(subfileArea, 0);
	public FixedLengthStringData agntname = DD.agntname.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData comagnt = DD.comagnt.copy().isAPartOf(subfileFields,30);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,38);
	public ZonedDecimalData initcom = DD.initcom.copyToZonedDecimal().isAPartOf(subfileFields,40);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,57);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(subfileFields,59);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(subfileFields,60);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,64);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,66);
	public ZonedDecimalData seqno = DD.seqno.copyToZonedDecimal().isAPartOf(subfileFields,67);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 69);
	public FixedLengthStringData agntnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData comagntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData initcomErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData ovrdcatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData seqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 109);
	public FixedLengthStringData[] agntnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] comagntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] initcomOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] ovrdcatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] seqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 229);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6237screensflWritten = new LongData(0);
	public LongData S6237screenctlWritten = new LongData(0);
	public LongData S6237screenWritten = new LongData(0);
	public LongData S6237protectWritten = new LongData(0);
	public GeneralTable s6237screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6237screensfl;
	}

	public S6237ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, comagnt, coverage, rider, planSuffix, ovrdcat, life, agntname, seqno, initcom};
		screenSflOutFields = new BaseData[][] {selectOut, comagntOut, coverageOut, riderOut, plnsfxOut, ovrdcatOut, lifeOut, agntnameOut, seqnoOut, initcomOut};
		screenSflErrFields = new BaseData[] {selectErr, comagntErr, coverageErr, riderErr, plnsfxErr, ovrdcatErr, lifeErr, agntnameErr, seqnoErr, initcomErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, servagnt, servagnam, servbr, servbrname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, servagntOut, servagnamOut, servbrOut, servbrnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, servagntErr, servagnamErr, servbrErr, servbrnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6237screen.class;
		screenSflRecord = S6237screensfl.class;
		screenCtlRecord = S6237screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6237protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6237screenctl.lrec.pageSubfile);
	}
}
