package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6241
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6241ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1432);//BRD-306 
	public FixedLengthStringData dataFields = new FixedLengthStringData(568).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData benBillDate = DD.bbldat.copyToZonedDecimal().isAPartOf(dataFields,3);
	public ZonedDecimalData annivProcDate = DD.cbanpr.copyToZonedDecimal().isAPartOf(dataFields,11);
	public ZonedDecimalData unitStatementDate = DD.cbunst.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,51);
	public ZonedDecimalData coverageDebt = DD.crdebt.copyToZonedDecimal().isAPartOf(dataFields,53);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,70);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,108);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,138);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,202);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,212);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,261);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,269);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,273);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,274);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,278);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,286);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,296);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,304);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,307);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(dataFields,309);
	public ZonedDecimalData rerateFromDate = DD.rrtfrm.copyToZonedDecimal().isAPartOf(dataFields,317);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,325);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,342);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,343);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,345);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,349);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,364);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,381);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,382);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(dataFields,395);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,415);
	/*BRD-306 STARTS*/
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 432);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 449);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 466);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 483);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 500);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,517);	
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,534);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,537);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,540);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,543);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,546);
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development */
	public FixedLengthStringData optsmode = DD.optsmode.copy().isAPartOf(dataFields,547);
	public FixedLengthStringData singpremtype = DD.sngprmtyp.copy().isAPartOf(dataFields,548);//ILIFE-7805
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,551);//ALS-7685
	
	/*BRD-306 END*/
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 568);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bbldatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cbanprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cbunstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crdebtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData rrtfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData zdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	/*BRD-306 STARTS*/
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development */
	public FixedLengthStringData optsmodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	/*BRD-306 END*/
	public FixedLengthStringData singpremtypeErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,208);//ILIFE-7805
	public FixedLengthStringData zstpduty01Err =new FixedLengthStringData(4).isAPartOf(errorIndicators,212);//ALS-7685
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(648).isAPartOf(dataArea, 784);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bbldatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cbanprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cbunstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crdebtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] rrtfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	/*BRD-306 STARTS*/
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development */
	public FixedLengthStringData[] optsmodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	/*BRD-306 END*/
	public FixedLengthStringData[] singpremtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);//ILIFE-7805
	public FixedLengthStringData[] zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);//
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData benBillDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData annivProcDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData unitStatementDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateFromDateDisp = new FixedLengthStringData(10);

	public LongData S6241screenWritten = new LongData(0);
	public LongData S6241protectWritten = new LongData(0);
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1); 
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387
	

	public boolean hasSubfile() {
		return false;
	}


	public S6241ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdescOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"32","31","-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"42","41","-42","43",null, null, null, null, null, null, null, null});
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development */
		fieldIndMap.put(optsmodeOut,new String[] {"45","46","-45","46",null, null, null, null, null, null, null, null});
		/*BRD-306 STARTS*/
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		/*BRD-306 END*/
		//ILIFE-3399-STARTS
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		fieldIndMap.put(singpremtypeOut,new String[] {null, "66", null,"67",null, null, null, null, null, null, null, null});//ILIFE-7805 
		fieldIndMap.put(zstpduty01Out,new String[] {null, "68", null,"69",null, null, null, null, null, null, null, null});//ALS-7685
		fieldIndMap.put(crrcdOut,new String[] {null, null, null,"70",null, null, null, null, null, null, null, null});//ILJ-45
		//fieldIndMap.put(rcessageOut,new String[] {null, null, null,"71",null, null, null, null, null, null, null, null});//ILJ-45
		fieldIndMap.put(rcesdteOut,new String[] {null, null, null,"72",null, null, null, null, null, null, null, null});//ILJ-45
		
		screenFields = new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, instPrem, premcess, zdesc, zlinstprem, singlePremium, crrcd, riskCessDate, annivProcDate, unitStatementDate, rerateDate, rerateFromDate, benBillDate, coverageDebt, optextind, taxamt, taxind,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, riskCessAge, riskCessTerm, premCessAge,premCessTerm,mortcls,optsmode,singpremtype,zstpduty01};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, instprmOut, premcessOut, zdescOut, zlinstpremOut, singprmOut, crrcdOut, rcesdteOut, cbanprOut, cbunstOut, rrtdatOut, rrtfrmOut, bbldatOut, crdebtOut, optextindOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut,rcessageOut, rcesstrmOut, pcessageOut,pcesstrmOut,mortclsOut,optsmodeOut,singpremtypeOut,zstpduty01Out};
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, instprmErr, premcessErr, zdescErr, zlinstpremErr, singprmErr, crrcdErr, rcesdteErr, cbanprErr, cbunstErr, rrtdatErr, rrtfrmErr, bbldatErr, crdebtErr, optextindErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr,rcessageErr, rcesstrmErr, pcessageErr,pcesstrmErr,mortclsErr,optsmodeErr,singpremtypeErr,zstpduty01Err};
		screenDateFields = new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, unitStatementDate, rerateDate, rerateFromDate, benBillDate};
		screenDateErrFields = new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, cbunstErr, rrtdatErr, rrtfrmErr, bbldatErr};
		screenDateDispFields = new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, unitStatementDateDisp, rerateDateDisp, rerateFromDateDisp, benBillDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6241screen.class;
		protectRecord = S6241protect.class;
	}

}
