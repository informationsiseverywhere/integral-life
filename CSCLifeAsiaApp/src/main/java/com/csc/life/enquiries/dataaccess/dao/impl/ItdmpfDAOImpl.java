package com.csc.life.enquiries.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.enquiries.dataaccess.dao.ItdmpfDAO;
import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ItdmpfDAOImpl extends BaseDAOImpl<Itdmpf> implements ItdmpfDAO{

	
	 private static final Logger LOGGER = LoggerFactory.getLogger(ItdmpfDAOImpl.class);

	 public List<Itdmpf> readItdmData(Itdmpf itdmpfModel){
     	
                     StringBuilder sqlDescSelect1 = new StringBuilder("SELECT GENAREA, ITMFRM  ");
                     sqlDescSelect1.append(" FROM ITDM  ");
                     sqlDescSelect1.append(" WHERE ITEMCOY =? AND ITEMTABL=? AND ITEMITEM=? AND ITMFRM=? ");
                     PreparedStatement psItdmSelect = getPrepareStatement(sqlDescSelect1.toString());
                     
                     ResultSet sqlItdmpf1rs = null;
                     List<Itdmpf> itdmpfList = new ArrayList<Itdmpf>();
                     try {
                            psItdmSelect.setString(1,itdmpfModel.getItemcoy());
                            psItdmSelect.setString(2,itdmpfModel.getItemtabl());
                            psItdmSelect.setString(3,itdmpfModel.getItemitem());
                            psItdmSelect.setInt(4,itdmpfModel.getItmfrm());                            
                           
                           
                         sqlItdmpf1rs = psItdmSelect.executeQuery();
                         while (sqlItdmpf1rs.next()) {
                        	 Itdmpf itdmpf = new Itdmpf();
                        	
                        	 itdmpf.setGenarea(new byte[]{sqlItdmpf1rs.getByte(1)});
                        	 itdmpf.setItmfrm(sqlItdmpf1rs.getInt(2));
                        	 itdmpfList.add(itdmpf);
                            }

                     } catch (SQLException e) {
                         LOGGER.error("readItdmData()", e);//IJTI-1561
                         throw new SQLRuntimeException(e);
                     } finally {
                         close(psItdmSelect, sqlItdmpf1rs);
                     }
                     return itdmpfList;
             
}
	 
	 public List<Itdmpf> readItdm(String itempfx,String	itemcoy,String	itemtabl,String	itemitem,int itmfrm){
	     	
         StringBuilder sqlDescSelect1 = new StringBuilder("SELECT ITEMITEM");
         sqlDescSelect1.append(" FROM ITDM  ");
         sqlDescSelect1.append(" WHERE ITEMPFX = ? ANd ITEMCOY =? AND ITEMTABL=? AND ITEMITEM=? AND ITMFRM=? ");
         PreparedStatement psItdmSelect = getPrepareStatement(sqlDescSelect1.toString());
         
         ResultSet sqlItdmpf1rs = null;
         List<Itdmpf> itdmpfList = new ArrayList<Itdmpf>();
         try {
                psItdmSelect.setString(1,itempfx);
                psItdmSelect.setString(2,itemcoy);
                psItdmSelect.setString(3,itemtabl);
                psItdmSelect.setString(4,itemitem);
                psItdmSelect.setInt(5,itmfrm);   
               
               
             sqlItdmpf1rs = psItdmSelect.executeQuery();
             while (sqlItdmpf1rs.next()) {
            	 Itdmpf itdmpf = new Itdmpf();
            	
            	 itdmpf.setItmfrm(sqlItdmpf1rs.getInt(1));
            	 itdmpfList.add(itdmpf);
                }

         } catch (SQLException e) {
             LOGGER.error("readItdm()", e);//IJTI-1561
             throw new SQLRuntimeException(e);
         } finally {
             close(psItdmSelect, sqlItdmpf1rs);
         }
         return itdmpfList;
 
	 }
	 //IBPLIFE-8670
	 public List<Itdmpf> readItdmGenarea(String itemcoy,String itemtabl,String itemitem,int itmfrm){
	     	
         StringBuilder sqlDescSelect1 = new StringBuilder(" SELECT GENAREA, ITEMCOY, ITEMTABL, ITEMITEM ");
         sqlDescSelect1.append(" FROM ITDM ");
         sqlDescSelect1.append(" WHERE ITEMCOY=? AND ITEMTABL=? AND ITMFRM<=? ");
         sqlDescSelect1.append(" AND ITEMITEM=? ");
         PreparedStatement psItdmSelect = getPrepareStatement(sqlDescSelect1.toString());
         
         ResultSet sqlItdmpf1rs = null;
         List<Itdmpf> itdmpfList = new ArrayList<Itdmpf>();
         try {
             psItdmSelect.setString(1,itemcoy);
             psItdmSelect.setString(2,itemtabl);
             psItdmSelect.setInt(3,itmfrm);
             psItdmSelect.setString(4,itemitem);
                           
             sqlItdmpf1rs = psItdmSelect.executeQuery();
             while (sqlItdmpf1rs.next()) {
            	 Itdmpf itdmpf = new Itdmpf();
            	 itdmpf.setGenarea(sqlItdmpf1rs.getBytes(1));
            	 itdmpf.setItemcoy(sqlItdmpf1rs.getString("ITEMCOY"));
            	 itdmpf.setItemtabl(sqlItdmpf1rs.getString("ITEMTABL"));
            	 itdmpf.setItemitem(sqlItdmpf1rs.getString("ITEMITEM"));
            	 itdmpfList.add(itdmpf);
             }
         } catch (SQLException e) {
             LOGGER.error("readItdmGenarea()", e);
             throw new SQLRuntimeException(e);
         } finally {
             close(psItdmSelect, sqlItdmpf1rs);
         }
         return itdmpfList; 
	 }
	//IBPLIFE-8670
}
