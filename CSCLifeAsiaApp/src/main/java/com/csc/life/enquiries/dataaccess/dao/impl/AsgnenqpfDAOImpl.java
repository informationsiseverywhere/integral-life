package com.csc.life.enquiries.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.enquiries.dataaccess.dao.AsgnenqpfDAO;
import com.csc.life.enquiries.dataaccess.model.Asgnenqpf;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AsgnenqpfDAOImpl extends BaseDAOImpl<Asgnenqpf> implements AsgnenqpfDAO  {
		private static final Logger LOGGER = LoggerFactory.getLogger(AsgnenqpfDAOImpl.class);
	
		public List<Asgnenqpf>	readAsgnenqData(Asgnenqpf asgnenqpfModel)	
				{
				
		
					// ---------------------------------
					// Initialize variables
					// ---------------------------------
					Asgnenqpf asgnenqpf = null;
					List<Asgnenqpf> asgnenqpfReadResult = new LinkedList<Asgnenqpf>();
					PreparedStatement stmn = null;
					ResultSet rs = null;
					StringBuilder sqlStringBuilder = new StringBuilder();
					
					// ---------------------------------
					// Construct Query
					// ---------------------------------
					sqlStringBuilder.append("SELECT CHDRCOY, CHDRNUM,	ASGNPFX, ASGNNUM	,REASONCD	,COMMFROM,	COMMTO,	TRANNO	,SEQNO	,TRANCDE");
					sqlStringBuilder.append(" FROM ASGNENQ  ");
					sqlStringBuilder.append(" WHERE CHDRCOY=?	AND CHDRNUM =? AND SEQNO=? AND ASGNNUM=?");
					stmn = getPrepareStatement(sqlStringBuilder.toString());
					
					try {
						// ---------------------------------
						// Set Parameters dynamically
						// ---------------------------------
						stmn.setString(1, asgnenqpfModel.getChdrcoy());
						stmn.setString(2, asgnenqpfModel.getChdrnum());
						stmn.setInt(3, asgnenqpfModel.getSeqno());
						stmn.setString(4, asgnenqpfModel.getAsgnnum());
						
						
						// ---------------------------------
						// Execute Query
						// ---------------------------------
						rs = executeQuery(stmn);
			
			
						while (rs.next()) {
						asgnenqpf = new Asgnenqpf();
						asgnenqpf.setChdrcoy(rs.getString(1));
						asgnenqpf.setChdrnum(rs.getString(2));
						asgnenqpf.setAsgnpfx(rs.getString(3));
						asgnenqpf.setAsgnnum(rs.getString(4));
						asgnenqpf.setReasoncd(rs.getString(5));
						asgnenqpf.setCommfrom(rs.getInt(6));
						asgnenqpf.setCommto(rs.getInt(7));
						asgnenqpf.setTranno(rs.getInt(8));
						asgnenqpf.setSeqno(rs.getInt(9));
						asgnenqpf.setTrancde(rs.getString(10));
						
						asgnenqpfReadResult.add(asgnenqpf);
						}
			
					} catch (SQLException e) {
						LOGGER.error("readasgnenqpfData()", e);//IJTI-1561
						throw new SQLRuntimeException(e);
					} finally {
						close(stmn, rs);
					}
					return asgnenqpfReadResult;
				
						
				}

}
