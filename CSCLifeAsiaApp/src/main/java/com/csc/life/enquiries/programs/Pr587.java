/*
 * File: Pr587.java
 * Date: 30 August 2009 1:48:03
 * Author: Quipoz Limited
 * 
 * Class transformed from PR587.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.life.enquiries.dataaccess.UndrprpTableDAM;
//Ticket #TMLII-429 start
import com.csc.smart.recordstructures.Batckey;
//Ticket #TMLII-429 end
import com.csc.life.enquiries.screens.Sr587ScreenVars;
//Ticket #TMLII-429 start
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
//Ticket #TMLII-429 end
import com.csc.smart.dataaccess.DescTableDAM;
//Ticket #TMLII-429
import com.csc.smart.dataaccess.ItemTableDAM;
//Ticket #TMLII-429
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This program is called from PR586 by receiving its parameters,
* displays them and retrieve thier detailed information from
* UNDRPRP logical file.
*
*****************************************************************
* </pre>
*/
public class Pr587 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private BinaryData wsaaSflct = new BinaryData(5, 0);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR587");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private String t5446 = "T5446";
		/* FORMATS */
	private String clntrec = "CLNTREC";
	private String descrec = "DESCREC";
	private String undrprprec = "UNDRPRPREC";

	private Wsspsmart wsspsmart = new Wsspsmart();
	
	//fixed bug #721
    private FixedLengthStringData filler = new FixedLengthStringData(767).isAPartOf(wsspsmart.userArea, 0, FILLER_REDEFINE);// Updated by Tidy
	private FixedLengthStringData wsspCurrcode = new FixedLengthStringData(3).isAPartOf(filler, 0);
	private FixedLengthStringData wsspLrkcls = new FixedLengthStringData(4).isAPartOf(filler, 3);
	private PackedDecimalData wsspTotsi = new PackedDecimalData(17, 2).isAPartOf(filler, 7);
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*U/W Logical File for Proposal*/
	private UndrprpTableDAM undrprpIO = new UndrprpTableDAM();
	/* start TMLII-429 UW-01-008 */
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData wsaaValid = new FixedLengthStringData(1);
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private String chdrlifrec = "CHDRLIFREC";
	private String t5679 = "T5679";
	private T5679rec t5679rec = new T5679rec();
	private String f321 = "F321";
	/* end TMLII-429 UW-01-008 */

	//private Wsspsmart wsspsmart = new Wsspsmart();// Updated by Tidy
	private Sr587ScreenVars sv = ScreenProgram.getScreenVars( Sr587ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1200, 
		preExit, 
		exit2090, 
		exit9000
	}

	public Pr587() {
		super();
		screenVars = sv;
		new ScreenModel("Sr587", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
	/* start TMLII-429 UW-01-008 */
	wsaaBatckey.set(wsspcomn.batchkey);
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setItemtabl(t5679);
	itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)
	&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
		scrnparams.errorCode.set(f321);
		return;
	}
	t5679rec.t5679Rec.set(itemIO.getGenarea());
	/* end TMLII-429 UW-01-008 */
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR587", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.clntnum.set(wsspsmart.flddkey);
		moveClntnam9100();
		sv.lrkcls.set(wsspLrkcls);
		moveLdesc9200();
		sv.tranamt.set(wsspTotsi);
		wsaaSflct.set(ZERO);
		undrprpIO.setRecKeyData(SPACES);
		undrprpIO.setClntnum(wsspsmart.flddkey);
		undrprpIO.setCoy(wsspcomn.company);
		undrprpIO.setCurrcode(wsspCurrcode);
		undrprpIO.setLrkcls02(wsspLrkcls);
		undrprpIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undrprpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undrprpIO.setFitKeysSearch("CLNTNUM", "COY", "CURRCODE", "LRKCLS02");
		undrprpIO.setStatuz(varcom.oK);
		while ( !(isEQ(undrprpIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaSflct,sv.subfilePage))) {
			loadSubfile1200();
		}
		
	}

protected void loadSubfile1200()
	{
		try {
			ctrl1200();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl1200()
	{
		callUndrprpio9000();
		/* start TMLII-429 UW-01-008 */
		if (isNE(undrprpIO.getStatuz(),varcom.oK) || isNE(wsaaValid,"Y")) {
			/* end TMLII-429 UW-01-008 */
			goTo(GotoLabel.exit1200);
		}
		initialize(sv.subfileFields);
		sv.chdrnum.set(undrprpIO.getChdrnum());
		sv.cnttyp.set(undrprpIO.getCnttyp());
		sv.coy.set(undrprpIO.getCoy());
		sv.crtable.set(undrprpIO.getCrtable());
		sv.currcode.set(undrprpIO.getCurrcode());
		sv.sumins.set(undrprpIO.getSumins());
		scrnparams.function.set(varcom.sadd);
		processScreen("SR587", sv);
		wsaaSflct.add(1);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isNE(scrnparams.statuz,varcom.rolu)) {
			goTo(GotoLabel.exit2090);
		}
		wsaaSflct.set(ZERO);
		undrprpIO.setStatuz(varcom.oK);
		while ( !(isEQ(undrprpIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaSflct,sv.subfilePage))) {
			loadSubfile1200();
		}
		
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void validateScreen2010()
	{
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR587", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR587", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR587", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.subtract(1);
		/*EXIT*/
	}

protected void callUndrprpio9000()
	{
		try {
			ctrl9000();
		}
		catch (GOTOException e){
		}
	}

protected void ctrl9000()
	{
		undrprpIO.setFormat(undrprprec);
		SmartFileCode.execute(appVars, undrprpIO);
		if (!(isEQ(undrprpIO.getStatuz(),varcom.oK)
		|| isEQ(undrprpIO.getStatuz(),varcom.mrnf)
		|| isEQ(undrprpIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(undrprpIO.getStatuz());
			syserrrec.params.set(undrprpIO.getParams());
			fatalError600();
		}
		scrnparams.subfileMore.set("Y");
		if (!(isEQ(undrprpIO.getStatuz(),varcom.oK)
		&& isEQ(wsspsmart.flddkey,undrprpIO.getClntnum())
		&& isEQ(wsspcomn.company,undrprpIO.getCoy())
		&& isEQ(wsspCurrcode,undrprpIO.getCurrcode())
		&& isEQ(wsspLrkcls,undrprpIO.getLrkcls02()))) {
			undrprpIO.setStatuz(varcom.endp);
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit9000);
		}
		undrprpIO.setFunction(varcom.nextr);

		/* start TMLII-429 UW-01-008 */
		wsaaValid.set("N");
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(wsspcomn.company);
		chdrlifIO.setChdrnum(undrprpIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		&& isNE(chdrlifIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlifIO.getStatuz(),varcom.mrnf)) {
			return;
		}
		for(int i =1; i<=12 || isNE(wsaaValid,"Y"); i++) {
			//ILIFE-2075-STARTS
			if (i>=13) {
				return;
			}
			//ILIFE-2075-ENDS
			if(isEQ(t5679rec.cnRiskStat[i],chdrlifIO.getStatcode())) {
				for(int j =1; j<=12 || isNE(wsaaValid,"Y"); j++) { 
					//ILIFE-2075-STARTS
					if (j>=13) {
						return;
					}
					//ILIFE-2075-ENDS
					if(isEQ(t5679rec.cnPremStat[j],chdrlifIO.getPstatcode())) {
						wsaaValid.set("Y");
					}
				}
			}
		}
		/* end TMLII-429 UW-01-008 */
	}

protected void moveClntnam9100()
	{
		ctrl9100();
	}

protected void ctrl9100()
	{
		clntIO.setRecKeyData(SPACES);
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setClntnum(wsspsmart.flddkey);
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntIO.getSurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntIO.getGivname(), "  "));
		sv.clntnam.setLeft(stringVariable1.toString());
	}

protected void moveLdesc9200()
	{
		ctrl9200();
	}

protected void ctrl9200()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5446);
		descIO.setDescitem(wsspLrkcls);
		descIO.setLanguage("E");
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.ldesc.set(descIO.getLongdesc());
	}
}
