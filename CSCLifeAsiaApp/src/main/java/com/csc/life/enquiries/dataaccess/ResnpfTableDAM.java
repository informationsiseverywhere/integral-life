package com.csc.life.enquiries.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ResnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:18
 * Class transformed from RESNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ResnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 86;
	public FixedLengthStringData resnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData resnpfRecord = resnrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(resnrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(resnrec);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(resnrec);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(resnrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(resnrec);
	public FixedLengthStringData trancde = DD.trancde.copy().isAPartOf(resnrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(resnrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(resnrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(resnrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(resnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ResnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ResnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ResnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ResnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ResnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ResnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ResnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("RESNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"REASONCD, " +
							"RESNDESC, " +
							"TRANNO, " +
							"TRANCDE, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     reasoncd,
                                     resndesc,
                                     tranno,
                                     trancde,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		reasoncd.clear();
  		resndesc.clear();
  		tranno.clear();
  		trancde.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getResnrec() {
  		return resnrec;
	}

	public FixedLengthStringData getResnpfRecord() {
  		return resnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setResnrec(what);
	}

	public void setResnrec(Object what) {
  		this.resnrec.set(what);
	}

	public void setResnpfRecord(Object what) {
  		this.resnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(resnrec.getLength());
		result.set(resnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}