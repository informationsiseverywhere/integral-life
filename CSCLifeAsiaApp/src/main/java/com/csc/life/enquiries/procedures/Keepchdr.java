/*
 * File: Keepchdr.java
 * Date: 29 August 2009 22:57:29
 * Author: Quipoz Limited
 * 
 * Class transformed from KEEPCHDR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  KEEP CHDR.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Keepchdr extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("KEEPCHDR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String chdrenqrec = "CHDRENQREC";

	private FixedLengthStringData wsaaChckRec = new FixedLengthStringData(31);
	private FixedLengthStringData wsaaChckChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaChckRec, 2);
	private FixedLengthStringData wsaaChckChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaChckRec, 3);
	private FixedLengthStringData wsaaChckStatuz = new FixedLengthStringData(4).isAPartOf(wsaaChckRec, 11);
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit0090
	}

	public Keepchdr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChckRec = convertAndSetParam(wsaaChckRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			performs0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit0090();
		}
	}

protected void performs0010()
	{
		syserrrec.subrname.set(wsaaProg);
		chdrenqIO.setDataKey(SPACES);
		chdrenqIO.setChdrcoy(wsaaChckChdrcoy);
		chdrenqIO.setChdrnum(wsaaChckChdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.endp)) {
			wsaaChckStatuz.set("NFND");
			goTo(GotoLabel.exit0090);
		}
		wsaaChckStatuz.set("DFND");
	}

protected void exit0090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
	}
}
