package com.csc.life.enquiries.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ClntqyTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:30
 * Class transformed from CLNTQY
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ClntqyTableDAM extends PFAdapterDAM {

	public int pfRecLen = 227+DD.cltaddr.length*5;//pmujavadiya//ILIFE-3212
	public FixedLengthStringData clntrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData clntqyRecord = clntrec;
	
	public FixedLengthStringData clntcoy = DD.clntcoy.copy().isAPartOf(clntrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(clntrec);
	public FixedLengthStringData clrrrole = DD.clrrrole.copy().isAPartOf(clntrec);
	public FixedLengthStringData surname = DD.surname.copy().isAPartOf(clntrec);
	public FixedLengthStringData givname = DD.givname.copy().isAPartOf(clntrec);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(clntrec);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(clntrec);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(clntrec);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(clntrec);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(clntrec);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(clntrec);
	public FixedLengthStringData cltsex = DD.cltsex.copy().isAPartOf(clntrec);
	public PackedDecimalData cltdob = DD.cltdob.copy().isAPartOf(clntrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ClntqyTableDAM() {
  		super();
  		setColumns();
  	    //journalled = false;
  		// Use the same transactions with P6363
  		journalled = true;
	}

	/**
	* Constructor for ClntqyTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ClntqyTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ClntqyTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ClntqyTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ClntqyTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ClntqyTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CLNTQY");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CLNTCOY, " +
							"CLNTNUM, " +
							"CLRRROLE, " +
							"SURNAME, " +
							"GIVNAME, " +
							"CLTADDR01, " +
							"CLTADDR02, " +
							"CLTADDR03, " +
							"CLTADDR04, " +
							"CLTADDR05, " +
							"CLTPCODE, " +
							"CLTSEX, " +
							"CLTDOB, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     clntcoy,
                                     clntnum,
                                     clrrrole,
                                     surname,
                                     givname,
                                     cltaddr01,
                                     cltaddr02,
                                     cltaddr03,
                                     cltaddr04,
                                     cltaddr05,
                                     cltpcode,
                                     cltsex,
                                     cltdob,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		clntcoy.clear();
  		clntnum.clear();
  		clrrrole.clear();
  		surname.clear();
  		givname.clear();
  		cltaddr01.clear();
  		cltaddr02.clear();
  		cltaddr03.clear();
  		cltaddr04.clear();
  		cltaddr05.clear();
  		cltpcode.clear();
  		cltsex.clear();
  		cltdob.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getClntrec() {
  		return clntrec;
	}

	public FixedLengthStringData getClntqyRecord() {
  		return clntqyRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setClntrec(what);
	}

	public void setClntrec(Object what) {
  		this.clntrec.set(what);
	}

	public void setClntqyRecord(Object what) {
  		this.clntqyRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(clntrec.getLength());
		result.set(clntrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}