/*
 * File: Pr50n.java
 * Date: 30 August 2009 1:32:36
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50N.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.dataaccess.CheqTableDAM;
import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.integral.search.FieldSearchService;
import com.csc.integral.search.SearchField;
import com.csc.integral.search.SearchResult;
import com.csc.integral.search.SearchService;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.RtrntrnTableDAM;
import com.csc.life.enquiries.screens.Sr50nScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.dao.UsrdDAO;
import com.csc.smart.dataaccess.model.Usrdpf;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* PR50N - Transactions/Component History
* This function is a part of Contract Enquiry, the program will
* list all transacitons that have been processed against the contract.
* if entering '1' against the transation no, sub-file program will
* allow user to view the Component Selection screen (SR50Q), if entering
* '2' against the transaction no, sub-file program will allow user
* to view the Despatch Address History screen (SR50P).
*
* 1000-section
* - Initialze the screen sub-file.
* - Call option swtich using 'INIT' function.
* - Retrieve contract file CHDRENQ using RETRV function.
* - Set the screen header fields.
* - Load sub-file from PTRNENQ file.
*
* 2000-section
* - If SR50N-SELECT not spaces, call option switch using 'CHCK'
*   function.
*
* 4000-section
* - Where the line selection is selected
*   Release PTRNENQ using RLSE function
*   Keep CHDRTRX and PTRNENQ files
* - Call option switch using 'STCK' function
*
***********************************************************************
* </pre>
*/
public class Pr50n extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50N");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final int wsaaToday = 0;
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private final int wsaaMaxOpt = 8;
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaSubfileRrn = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaSubfileEnd = new ZonedDecimalData(5, 0).setUnsigned();
	private final String contractIssue = "T642";
		/* WSAA-CLNT */
	private FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1).init("N");
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String tr50t = "TR50T";
	private static final String chdrtrxrec = "CHDRTRXREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String resnrec = "RESNREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	// ILIFE-3396 PERFORMANCE BY OPAL
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
		/*Contract Header by Transaction Number*/
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
		/*Logical File: Cheque details file*/
	private CheqTableDAM cheqIO = new CheqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
		/*Reason Details  Logical File*/
	private ResnTableDAM resnIO = new ResnTableDAM();
		/*Postings Details - by Transaction Detail*/
	private RtrntrnTableDAM rtrntrnIO = new RtrntrnTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	protected Namadrsrec namadrsrec = new Namadrsrec();
	private Optswchrec optswchrec = new Optswchrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sr50nScreenVars sv = ScreenProgram.getScreenVars( Sr50nScreenVars.class);
	// ILB-326:Starts
	private FieldSearchService searchService = null;
	private SearchResult result = null;
	private SearchResult.Record record = null;
	private LinkedHashMap<String, String> filterColumns = null;
	private List<String> requiredColumns = null;
	private LinkedHashMap<String, Boolean> sortingColumns = null;
	private DescDAO descdao =new DescDAOImpl();
	private FixedLengthStringData wsaaPrevtrannosearch =  new FixedLengthStringData(5);
	private FixedLengthStringData wsaaPrevdatesubsearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPreveffdatesearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPrevtrcodesearch =  new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevtrandescsearch =  new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPrevcrtusersearch =  new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPrevrdocnumsearch =  new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPrevstatzsearch =  new FixedLengthStringData(2);
	private UsrdDAO usrdDAO = getApplicationContext().getBean("usrdDAO" , UsrdDAO.class);
	
	// ILB-326:Ends

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextPtrnenq1500, 
		next4200, 
		exit4200
	}

	public Pr50n() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50n", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		initOptswch1100();
		wsaaBatchkey.set(wsspcomn.batchkey);
		retrvFiles1200();
		setScreenHeader1300();
		// ILB-326:Starts
		wsaaPrevtrannosearch.set("00000");
		wsaaPrevdatesubsearch.set("99999999");
		wsaaPreveffdatesearch.set("99999999");
		wsaaPrevtrcodesearch.set(SPACES);
		wsaaPrevtrandescsearch.set(SPACES);
		wsaaPrevcrtusersearch.set(SPACES);
		wsaaPrevrdocnumsearch.set(SPACES);
		wsaaPrevstatzsearch.set(SPACES);
		sv.datesubsearchDisp.clear();
		sv.effdatesearchDisp.clear();
		/*ptrnenqIO.setDataArea(SPACES);
		ptrnenqIO.setStatuz(varcom.oK);
		ptrnenqIO.setChdrcoy(wsspcomn.company);
		ptrnenqIO.setChdrnum(chdrpf.getChdrnum());
		ptrnenqIO.setTranno(99999);
		ptrnenqIO.setBatctrcde(SPACES);
		ptrnenqIO.setFormat(ptrnrec);
		ptrnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)
		&& isNE(ptrnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)
		|| isNE(ptrnenqIO.getChdrcoy(),wsspcomn.company)
		|| isNE(ptrnenqIO.getChdrnum(),chdrpf.getChdrnum())) {
			ptrnenqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(ptrnenqIO.getStatuz(),varcom.endp))) {
			loadSubfile1500();
		}*/
		search();
		if(result != null){
			while(result.hasNext()){
				record = result.fetchNextRecord();
				loadSubfile1500();
			}
		}
		// ILB-326:Ends
		scrnparams.subfileRrn.set(1);
	}

private void filterSearch(){
	record = null;
	List<SearchField> searchFields = new ArrayList<>();
	String dateSubStr = sv.datesubsearch.toString().trim();
	String effDateStr = sv.effdatesearch.toString().trim();
	String trannoStr = sv.trannosearch.toString().trim();
	trannoStr = "00000".equalsIgnoreCase(trannoStr) ? "" : trannoStr;
	if(!trannoStr.matches("\\d*")){
		result = null;
		scrnparams.errorCode.set("E040");
		return;
	}
	SearchField trainnoSearchField = new SearchField("TRANNO", "".equalsIgnoreCase(trannoStr) ? "" : Integer.valueOf(trannoStr).toString(), true);
	SearchField datesubSearchField = new SearchField("DATESUB", "99999999".equalsIgnoreCase(dateSubStr) ? "" : dateSubStr, true);
	SearchField effdateSearchField = new SearchField("PTRNEFF", "99999999".equalsIgnoreCase(effDateStr) ? "" : effDateStr, true);
	SearchField trcodeSearchField = new SearchField("BATCTRCDE", sv.trcodesearch.toString(), true);
	SearchField crtuserSearchField = new SearchField("CRTUSER", sv.crtusersearch.toString(), true,"USRPRF");
	//SearchField rdocnumSearchField = new SearchField("FORENUM", sv.rdocnumsearch.toString(), true);
	//SearchField statzSearchField = new SearchField("FORENUM", sv.statzsearch.toString(), true);

	searchFields.add(trainnoSearchField);
	searchFields.add(datesubSearchField);
	searchFields.add(effdateSearchField);
	searchFields.add(trcodeSearchField);

	String transDescribtion =  sv.trandescsearch.toString();
	String transCode = null;
	if(transDescribtion != null && !"".equalsIgnoreCase(transDescribtion = transDescribtion.trim())){
		Descpf descpf= descdao.getitemByDesc(smtpfxcpy.item.toString(), wsaaDesckey.descDesctabl.toString(), wsaaDesckey.descDesccoy.toString(), transDescribtion);
		if(descpf != null){
			transCode = descpf.getDescitem().trim();
		}
		if(transCode == null || 
				((!"".equalsIgnoreCase(sv.trcodesearch.toString().trim())) && (!transCode.equalsIgnoreCase(sv.trcodesearch.toString())))){
			result = null;
			scrnparams.errorCode.set("E040");
			return;
		}else{
			SearchField trCodeDescrSearchField = new SearchField("BATCTRCDE", transCode, true);
			if(!searchFields.contains(trCodeDescrSearchField)){
				searchFields.add(trCodeDescrSearchField);
			}
		}
	}

	searchFields.add(crtuserSearchField);
	result = searchService.filterSearch(searchFields);
	if(result == null || !result.hasNext()){
		scrnparams.errorCode.set("E040");
	}
}

private void search(){
	record = null;
	if(filterColumns == null){
		filterColumns = new LinkedHashMap<>();
		filterColumns.put("CHDRCOY", wsspcomn.company.toString());
		filterColumns.put("CHDRNUM", chdrpf.getChdrnum());
	}
	if(requiredColumns == null){
		requiredColumns = new ArrayList<>();
		requiredColumns.add("TRANNO");
		requiredColumns.add("DATESUB");
		requiredColumns.add("PTRNEFF");
		requiredColumns.add("BATCTRCDE");
		requiredColumns.add("CRTUSER");
		requiredColumns.add("VALIDFLAG");
		requiredColumns.add("CHDRCOY");
		requiredColumns.add("CHDRNUM");
		requiredColumns.add("USRPRF");
		requiredColumns.add("TRDT");
	}

	if(sortingColumns == null){
		sortingColumns = new LinkedHashMap<>();
		sortingColumns.put("CHDRCOY", true);
		sortingColumns.put("CHDRNUM", true);
		sortingColumns.put("TRANNO", false);
		sortingColumns.put("BATCTRCDE", true);
		sortingColumns.put("UNIQUE_NUMBER", false);
	}

	searchService = new FieldSearchService(ptrnenqIO.getClass().getCanonicalName());
	result = searchService.search(filterColumns, requiredColumns, sortingColumns);
	if(result == null || !result.hasNext()){
		scrnparams.errorCode.set("E040");
	}
}


protected void initOptswch1100()
	{
		begin1100();
	}

protected void begin1100()
	{
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		for (ix.set(1); !(isGT(ix,wsaaMaxOpt)
		|| isEQ(optswchrec.optsType[ix.toInt()],SPACES)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")) {
				sv.optdsc[ix.toInt()].set(optswchrec.optsDsc[ix.toInt()]);
			}
		}
	}

protected void retrvFiles1200()
	{
		/*BEGIN*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
	// ILIFE-3396 PERFORMANCE BY OPAL
			chdrpf= chdrpfDAO.getCacheObject(chdrpf);
			if(chdrpf==null)
			{
				return;
			}
		/*EXIT*/
	}

protected void setScreenHeader1300()
	{
		begin1300();
	}

protected void begin1300()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrpf.getChdrcoy());
		lifelnbIO.setChdrnum(chdrpf.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(lifelnbIO.getLifcnum());
		a1000CallNamadrs();
		sv.lifename.set(getFullName());
		
		/* Check for the existence of Joint Life details.*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrpf.getChdrcoy());
		lifelnbIO.setChdrnum(chdrpf.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isEQ(lifelnbIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifelnbIO.getLifcnum());
			wsaaClntPrefix.set(fsupfxcpy.clnt);
			wsaaClntCompany.set(wsspcomn.fsuco);
			wsaaClntNumber.set(lifelnbIO.getLifcnum());
			a1000CallNamadrs();
			sv.jlifename.set(getFullName());
		}
		/* Obtain the Contract Type description from T5688.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5688);
		wsaaDesckey.descDescitem.set(chdrpf.getCnttype());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3623);
		wsaaDesckey.descDescitem.set(chdrpf.getStatcode());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3588);
		wsaaDesckey.descDescitem.set(chdrpf.getPstcde());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void loadSubfile1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin1500();
					writeSubfile1500();
				case nextPtrnenq1500: 
					nextPtrnenq1500();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1500()
	{
		// ILB-326:Starts
		/*if (isEQ(ptrnenqIO.getValidflag(),"2")) {
			goTo(GotoLabel.nextPtrnenq1500);
		}*/
		if(((record.getColumnValue("VALIDFLAG")) != null) && (record.getColumnValue("VALIDFLAG").equalsIgnoreCase("2"))){
			goTo(GotoLabel.nextPtrnenq1500);
		}
		// ILB-326:Ends
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		// ILB-326:Starts
		//wsaaItemkey.itemItemcoy.set(ptrnenqIO.getChdrcoy());
		wsaaItemkey.itemItemcoy.set(record.getColumnValue("CHDRCOY"));
		// ILB-326:Ends
		wsaaItemkey.itemItemtabl.set(tr50t);
		// ILB-326:Starts
		//wsaaItemkey.itemItemitem.set(ptrnenqIO.getBatctrcde());
		wsaaItemkey.itemItemitem.set(record.getColumnValue("BATCTRCDE"));
		// ILB-326:Ends
		a3000CallNonDatedTable();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.nextPtrnenq1500);
		}
		sv.subfileFields.set(SPACES);
		sv.select.set(SPACES);
		// ILB-326:Starts
		/*sv.tranno.set(ptrnenqIO.getTranno());
		if (isNE(ptrnenqIO.getCrtuser(),SPACES)) {
			sv.crtuser.set(ptrnenqIO.getCrtuser());
		}
		else {
			sv.crtuser.set(ptrnenqIO.getUserProfile());
		}
		sv.effdate.set(ptrnenqIO.getPtrneff());
		sv.trcode.set(ptrnenqIO.getBatctrcde());
		if (isEQ(ptrnenqIO.getDatesub(),NUMERIC)
		&& isNE(ptrnenqIO.getDatesub(),ZERO)) {
			sv.datesub.set(ptrnenqIO.getDatesub());
		}
		else {
			sv.datesub.set(ptrnenqIO.getTransactionDate());
			sv.datesub.add(20000000);
		}*/
		sv.tranno.set(record.getColumnValue("TRANNO"));
		String crtuserStr = record.getColumnValue("CRTUSER");
		if(crtuserStr != null && !"".equalsIgnoreCase(crtuserStr)){
			sv.crtuser.set(crtuserStr);
		}else{
			Usrdpf usrdpf ; 
			usrdpf = usrdDAO.getLongUserid(record.getColumnValue("USRPRF"));
			if(null == usrdpf || usrdpf.getLonguserid().isEmpty())
				sv.crtuser.set(record.getColumnValue("USRPRF"));
			else
				sv.crtuser.set(usrdpf.getLonguserid()); 
		}
		sv.effdate.set(record.getColumnValue("PTRNEFF"));
		sv.trcode.set(record.getColumnValue("BATCTRCDE"));
		String dateSubStr = record.getColumnValue("DATESUB");
		if(dateSubStr != null && dateSubStr.matches("-?\\d+(\\.\\d+)?")){
			sv.datesub.set(dateSubStr);
		}else{
			sv.datesub.set(record.getColumnValue("TRDT"));
			sv.datesub.add(20000000);
		}
		// ILB-326:Ends
		/* Obtain the Transaction Code description from T1688.*/
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t1688);
		// ILB-326:Starts
		//wsaaDesckey.descDescitem.set(ptrnenqIO.getBatctrcde());
		wsaaDesckey.descDescitem.set(record.getColumnValue("BATCTRCDE"));
		// ILB-326:Ends
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.trandesc.fill("?");
		}
		else {
			sv.trandesc.set(descIO.getLongdesc());
		}
		/*    PERFORM 1600-GET-FSU-INFO.*/
		getReason1700();
	}

protected void writeSubfile1500()
	{
		/* Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextPtrnenq1500()
	{
		/* If current record transaction code is contract issue, stop*/
		/* to load subfile.*/
		// ILB-326:Starts
		/*if (isEQ(ptrnenqIO.getBatctrcde(),contractIssue)) {
			ptrnenqIO.setStatuz(varcom.endp);
			return ;
		}*/
		if(record.getColumnValue("BATCTRCDE").equalsIgnoreCase(contractIssue)){
			result.clearRecords();
			return;
		}
		
		/* Read the next PTRNENQ record.*/
		/*ptrnenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)
		&& isNE(ptrnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)
		|| isNE(ptrnenqIO.getChdrcoy(),wsspcomn.company)
		|| isNE(ptrnenqIO.getChdrnum(),chdrpf.getChdrnum())) {
			ptrnenqIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
		// ILB-326:Ends
	}

protected void getFsuInfo1600()
	{
		/*BEGIN*/
		/**    INITIALIZE                     FSULIFE-DATA-AREA.*/
		/**    MOVE O-K                    TO FSULIFE-STATUZ.*/
		/**    MOVE WSSP-COMPANY           TO FSULIFE-COMPANY.*/
		/**    MOVE CHDRENQ-CHDRNUM        TO FSULIFE-CHDRNUM*/
		/**    MOVE PTRNENQ-TRANNO         TO FSULIFE-TRANNO.*/
		/**    MOVE FSULIFEREC             TO FSULIFE-FORMAT.*/
		/**    MOVE READR                  TO FSULIFE-FUNCTION.*/
		/**    CALL 'FSULIFEIO'         USING FSULIFE-PARAMS.*/
		/**    IF  FSULIFE-STATUZ       NOT = O-K AND MRNF*/
		/**       MOVE FSULIFE-STATUZ      TO SYSR-STATUZ*/
		/**       MOVE FSULIFE-PARAMS      TO SYSR-PARAMS*/
		/**       PERFORM 600-FATAL-ERROR*/
		/**    END-IF.*/
		/**    IF  FSULIFE-STATUZ           = MRNF*/
		/**       GO TO 1600-EXIT*/
		/**    END-IF.*/
		/**    MOVE FSULIFE-RDOCNUM        TO SR50N-RDOCNUM.*/
		/**    INITIALIZE                     RESN-DATA-AREA.*/
		/**    MOVE O-K                    TO RESN-STATUZ.*/
		/**    MOVE PTRNENQ-CHDRCOY        TO RESN-CHDRCOY.*/
		/**    MOVE FSULIFE-RDOCNUM        TO RESN-CHDRNUM.*/
		/**    MOVE PTRNENQ-TRANNO         TO RESN-TRANNO.*/
		/**    MOVE RESNREC                TO RESN-FORMAT.*/
		/**    MOVE READR                  TO RESN-FUNCTION.*/
		/**    CALL 'RESNIO'            USING RESN-PARAMS.*/
		/**    IF  RESN-STATUZ              = O-K*/
		/**       MOVE RESN-REASONCD       TO SR50N-REASONCD*/
		/**       MOVE RESN-RESNDESC       TO SR50N-DESCR*/
		/**    END-IF.*/
		/**1600-READ-CHEQ.*/
		/**    IF  FSULIFE-RDOCPFX      NOT = 'RQ'*/
		/**       GO TO 1600-EXIT*/
		/**    END-IF.*/
		/**    INITIALIZE                     CHEQ-DATA-AREA.*/
		/**    MOVE O-K                    TO CHEQ-STATUZ.*/
		/**    MOVE FSULIFE-COMPANY        TO CHEQ-REQNCOY.*/
		/**    MOVE FSULIFE-RDOCNUM        TO CHEQ-REQNNO.*/
		/**    MOVE CHEQREC                TO CHEQ-FORMAT.*/
		/**    MOVE READR                  TO CHEQ-FUNCTION.*/
		/**    CALL 'CHEQIO'            USING CHEQ-PARAMS.*/
		/**    IF  CHEQ-STATUZ          NOT = O-K AND MRNF*/
		/**       MOVE CHEQ-STATUZ         TO SYSR-STATUZ*/
		/**       MOVE CHEQ-PARAMS         TO SYSR-PARAMS*/
		/**       PERFORM 600-FATAL-ERROR*/
		/**    END-IF.*/
		/**    MOVE CHEQ-PROCIND           TO SR50N-STATZ.*/
		/*EXIT*/
	}

protected void getReason1700()
	{
		begin1700();
	}

protected void begin1700()
	{
		resnIO.setDataArea(SPACES);
		resnIO.setStatuz(varcom.oK);
		// ILB-326:Starts
		/*resnIO.setChdrcoy(ptrnenqIO.getChdrcoy());
		resnIO.setChdrnum(ptrnenqIO.getChdrnum());
		resnIO.setTranno(ptrnenqIO.getTranno());*/
		resnIO.setChdrcoy(record.getColumnValue("CHDRCOY"));
		resnIO.setChdrnum(record.getColumnValue("CHDRNUM"));
		resnIO.setTranno(record.getColumnValue("TRANNO"));
		// ILB-326:Ends
		resnIO.setFormat(resnrec);
		resnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, resnIO);
		if (isEQ(resnIO.getStatuz(),varcom.oK)) {
			sv.reasoncd.set(resnIO.getReasoncd());
			sv.descr.set(resnIO.getResndesc());
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
			screenIo2010();
		}

private void filter(){
	if (isNE(sv.trannosearch,wsaaPrevtrannosearch) 
			|| isNE(sv.datesubsearch,wsaaPrevdatesubsearch) 
			|| isNE(sv.effdatesearch,wsaaPreveffdatesearch)
			|| isNE(sv.trcodesearch,wsaaPrevtrcodesearch)
			|| isNE(sv.trandescsearch,wsaaPrevtrandescsearch)
			|| isNE(sv.crtusersearch,wsaaPrevcrtusersearch)
			|| isNE(sv.rdocnumsearch, wsaaPrevrdocnumsearch)
			|| isNE(sv.statzsearch, wsaaPrevstatzsearch)) {
		
		wsaaPrevtrannosearch.set(sv.trannosearch);
		wsaaPrevdatesubsearch.set(sv.datesubsearch);
		wsaaPreveffdatesearch.set(sv.effdatesearch);
		wsaaPrevtrcodesearch.set(sv.trcodesearch);
		wsaaPrevtrandescsearch.set(sv.trandescsearch);
		wsaaPrevcrtusersearch.set(sv.crtusersearch);
		wsaaPrevrdocnumsearch.set(sv.rdocnumsearch);
		wsaaPrevstatzsearch.set(sv.statzsearch);
		
		scrnparams.function.set(varcom.sclr);
		processScreen("Sr50n", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		filterSearch();
		if(result != null){
			while(result.hasNext()){
				record = result.fetchNextRecord();
				loadSubfile1500();
			}
		}
		wsspcomn.edterror.set("Y");
	}
}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		// ILB-326:Starts
		filter();
		// ILB-326:Ends
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
		if (isNE(sv.select,SPACES)) {
			chckOptswch2700();
		}
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void chckOptswch2700()
	{
		begin2700();
	}

protected void begin2700()
	{
		optswchrec.optsStatuz.set(varcom.oK);
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelCode.set(SPACES);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
		stckOpts4010();
	}

protected void nextProgram4010()
	{
		scrnparams.statuz.set(varcom.oK);
		wsaaFoundSelection.set("N");
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz,varcom.endp)
		|| foundSelection.isTrue())) {
			lineSels4200();
		}
		
	}

protected void stckOpts4010()
	{
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(wsaaScrnStatuz,varcom.oK)) {
				scrnparams.subfileRrn.set(wsaaSubfileRrn);
				scrnparams.subfileEnd.set(wsaaSubfileEnd);
			}
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void lineSels4200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin4200();
				case next4200: 
					next4200();
				case exit4200: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin4200()
	{
		screenIo9000();
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit4200);
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.next4200);
		}
		for (ix.set(1); !(isGT(ix,wsaaMaxOpt)
		|| foundSelection.isTrue()); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")
			&& isEQ(optswchrec.optsCode[ix.toInt()],sv.select)) {
				optswchrec.optsInd[ix.toInt()].set("X");
				wsaaFoundSelection.set("Y");
				rlseFiles4210();
				keepsFiles4220();
			}
		}
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelCode.set(SPACES);
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		wsaaSubfileRrn.set(scrnparams.subfileRrn);
		wsaaSubfileEnd.set(scrnparams.subfileEnd);
	}

protected void next4200()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void rlseFiles4210()
	{
		/*BEGIN*/
		/* Release any PTRNENQ record that may have been stored.*/
		ptrnenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void keepsFiles4220()
	{
		begin4220();
	}

protected void begin4220()
	{
		chdrtrxIO.setDataArea(SPACES);
		chdrtrxIO.setStatuz(varcom.oK);
		chdrtrxIO.setChdrcoy(wsspcomn.company);
		chdrtrxIO.setChdrnum(chdrpf.getChdrnum());
		chdrtrxIO.setTranno(sv.tranno);
		chdrtrxIO.setValidflag("1");//ILIFE-8077
		chdrtrxIO.setFormat(chdrtrxrec);
		chdrtrxIO.setFunction(varcom.endr);
		//performance improvement -- Anjali
		chdrtrxIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrtrxIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","VALIDFLAG");
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)
		&& isNE(chdrtrxIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)
		|| isNE(chdrtrxIO.getChdrcoy(),wsspcomn.company)
		|| isNE(chdrtrxIO.getChdrnum(),chdrpf.getChdrnum())) {
			chdrtrxIO.setDataArea(SPACES);
			chdrtrxIO.setStatuz(varcom.oK);
			chdrtrxIO.setChdrcoy(wsspcomn.company);
			chdrtrxIO.setChdrnum(chdrpf.getChdrnum());
			chdrtrxIO.setTranno(sv.tranno);
			chdrtrxIO.setFormat(chdrtrxrec);
			chdrtrxIO.setValidflag("1");//added for ILIFE-6854
			chdrtrxIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			chdrtrxIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			//chdrtrxIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");// commented for ILIFE-6854
			chdrtrxIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "VALIDFLAG");//added for ILIFE-6854
			SmartFileCode.execute(appVars, chdrtrxIO);
			if (isNE(chdrtrxIO.getStatuz(),varcom.oK)
			&& isNE(chdrtrxIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(chdrtrxIO.getStatuz());
				syserrrec.params.set(chdrtrxIO.getParams());
				fatalError600();
			}
			if (isNE(chdrtrxIO.getStatuz(),varcom.oK)
			|| isNE(chdrtrxIO.getChdrcoy(),wsspcomn.company)
			|| isNE(chdrtrxIO.getChdrnum(),chdrpf.getChdrnum())) {
				syserrrec.statuz.set(chdrtrxIO.getStatuz());
				syserrrec.params.set(chdrtrxIO.getParams());
				fatalError600();
			}
		}
		chdrtrxIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		ptrnenqIO.setDataArea(SPACES);
		ptrnenqIO.setStatuz(varcom.oK);
		ptrnenqIO.setChdrcoy(wsspcomn.company);
		ptrnenqIO.setChdrnum(chdrpf.getChdrnum());
		ptrnenqIO.setPtrneff(sv.effdate);
		ptrnenqIO.setBatctrcde(sv.trcode);
		ptrnenqIO.setTranno(sv.tranno);
		ptrnenqIO.setFormat(ptrnrec);
		ptrnenqIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
	}

protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen("SR50N", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a1000CallNamadrs()
	{
		a1000Begin();
	}

protected void a1000Begin()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void a2000GetDesc()
	{
		a2000Begin();
	}

protected void a2000Begin()
	{
		descIO.setDataArea(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsaaDesckey.descDesccoy);
		descIO.setDesctabl(wsaaDesckey.descDesctabl);
		descIO.setDescitem(wsaaDesckey.descDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void a3000CallNonDatedTable()
	{
		a3000Begin();
	}

protected void a3000Begin()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(wsaaItemkey.itemItempfx);
		itemIO.setItemcoy(wsaaItemkey.itemItemcoy);
		itemIO.setItemtabl(wsaaItemkey.itemItemtabl);
		itemIO.setItemitem(wsaaItemkey.itemItemitem);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

	protected String getFullName() {
		return namadrsrec.name.toString();
	}
}
