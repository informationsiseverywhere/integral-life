/*
 * File: P6222.java
 * Date: 30 August 2009 0:36:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P6222.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.enquiries.screens.S6222ScreenVars;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Underwriting Enquiry Facility
* *****************************
*
*****************************************************************
* </pre>
*/
public class P6222 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6222");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	protected PackedDecimalData wsaaLineCount = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaPrevItemitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaScan = new FixedLengthStringData(30).init(SPACES);
	protected FixedLengthStringData wsaaCo = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaContractNo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	protected FixedLengthStringData wsaaValidStatusFlag = new FixedLengthStringData(1);
	protected String wsaaCovrFound = "";

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaContract = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaForenum, 8);
	protected String commandKeyPressed = "";
	protected String e040 = "E040";
	protected String f321 = "F321";
	protected String t5679 = "T5679";
		/* FORMATS */
	protected String clrrrec = "CLRRREC";
	private String chdrrec = "CHDRREC";
	protected String covrrec = "COVRREC";
	private String clntrec = "CLNTREC";
	private String covtlnbrec = "COVTLNBREC";
		/*Contract header file*/
	protected ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
		/*Client role and relationships logical fi*/
	protected ClrrTableDAM clrrIO = new ClrrTableDAM();
		/*Component (Coverage/Rider) Record*/
	protected CovrTableDAM covrIO = new CovrTableDAM();
		/*Coverage/rider transactions - new busine*/
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected T5679rec t5679rec = new T5679rec();
	protected Batckey wsaaBatckey = new Batckey();
	protected Wsspsmart wsspsmart = new Wsspsmart();
	private S6222ScreenVars sv = getLScreenVars(); //ScreenProgram.getScreenVars( S6222ScreenVars.class);

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1900, 
		exit1910, 
		exit1920, 
		readNextExit1930, 
		exit1940, 
		readNextExit1950, 
		preExit, 
		validateSubfile2600, 
		exit2090, 
		exit5900
	}

	public P6222() {
		super();
		screenVars = sv;
		new ScreenModel("S6222", AppVars.getInstance(), sv);
	}

	protected S6222ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6222ScreenVars.class);
	}
	
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1100();
			loadSubfile1300();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1100()
	{
		if (isEQ(commandKeyPressed,"Y")) {
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6222", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void loadSubfile1300()
	{
		clrrIO.setStatuz(SPACES);
		clrrIO.setClntpfx("CN");
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClrrrole("LF");
		clrrIO.setClntnum(wsspsmart.flddkey);
		clrrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		clrrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clrrIO.setFitKeysSearch("CLNTNUM");
		clrrIO.setFormat(clrrrec);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)
		&& isNE(clrrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		if (isEQ(clrrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1900);
		}
		if (isGT(clrrIO.getClntnum(),wsspsmart.flddkey)) {
			clrrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1900);
		}
		if ((isNE(clrrIO.getClntpfx(),"CN"))
		|| (isNE(clrrIO.getClntcoy(),wsspcomn.fsuco))
		|| (isNE(clrrIO.getClntnum(),wsspsmart.flddkey))
		|| (isNE(clrrIO.getClrrrole(),"LF"))) {
			goTo(GotoLabel.exit1900);
		}
		sv.clntnum.set(clrrIO.getClntnum());
		getName1905();
		sv.company.set(clrrIO.getForecoy());
		sv.tsumins.set(0);
		readContract1910();
		while ( !(isEQ(clrrIO.getStatuz(),varcom.endp)
		|| isGT(wsaaLineCount,14))) {
			loadSubfile5000();
		}
		
		if (isEQ(wsaaLineCount,0)
		&& isNE(scrnparams.errorCode,f321)) {
			scrnparams.errorCode.set(e040);
		}
		if (isEQ(clrrIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		commandKeyPressed = "Y";
		scrnparams.subfileRrn.set(1);
	}

protected void getName1905()
	{
		read1905();
	}

protected void read1905()
	{
		clntIO.setDataKey(SPACES);
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setClntnum(wsspsmart.flddkey);
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		if (isEQ(clntIO.getEthorig(),"1")) {
			sv.name1.set(clntIO.getSurname());
			sv.name2.set(clntIO.getGivname());
		}
		else {
			sv.name1.set(clntIO.getGivname());
			sv.name2.set(clntIO.getSurname());
		}
	}

protected void readContract1910()
	{
		try {
			read1910();
			
			checkHeaderStatus19101();
		}
		catch (GOTOException e){
		}
	}

protected void read1910()
	{
		chdrIO.setDataKey(SPACES);
		chdrIO.setChdrpfx("CH");
		chdrIO.setChdrcoy(clrrIO.getForecoy());
		wsaaForenum.set(clrrIO.getForenum());
		chdrIO.setCurrfrom(ZERO);
		chdrIO.setChdrnum(wsaaContract);
		chdrIO.setFunction(varcom.begn);
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1910);
		}
		
		wsaaBatckey.set(wsspcomn.batchkey);
	}

protected void checkHeaderStatus19101()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			scrnparams.errorCode.set(f321);
			goTo(GotoLabel.exit1910);
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		checkChdrStatus8000();
		if (isEQ(wsaaValidStatusFlag,"Y")) {
			sv.chdrnum.set(chdrIO.getChdrnum());
			sv.cnttype.set(chdrIO.getCnttype());
			checkLextpfTable(chdrIO.getChdrcoy().toString().trim(), chdrIO.getChdrnum().toString().trim());//edited by BARAKAT
			wsaaCovrFound = "N";
		}
		beginCovrCustomerspecific();
		
	}
	protected void beginCovrCustomerspecific(){
	beginCovr1920();
	if (isEQ(wsaaCovrFound,"N")) {
		beginCovt1940();
	}
	}

protected void beginCovr1920()
	{
		try {
			begin1920();
		}
		catch (GOTOException e){
		}
	}

protected void begin1920()
	{
		covrIO.setDataKey(SPACES);
		covrIO.setChdrcoy(chdrIO.getChdrcoy());
		covrIO.setChdrnum(chdrIO.getChdrnum());
		covrIO.setLife(wsaaLife);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRNUM");
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1920);
		}
		if (isGT(covrIO.getChdrnum(),chdrIO.getChdrnum())) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1920);
		}
		if ((isNE(covrIO.getChdrcoy(),chdrIO.getChdrcoy()))
		|| (isNE(covrIO.getChdrnum(),chdrIO.getChdrnum()))
		|| (isNE(wsaaLife,covrIO.getLife()))) {
			goTo(GotoLabel.exit1920);
		}
		if (isNE(covrIO.getValidflag(),"1")) {
			goTo(GotoLabel.exit1920);
		}
		wsaaCovrFound = "Y";
		checkCovrStatus9000();

		if (isNE(wsaaValidStatusFlag,"Y")) {
			goTo(GotoLabel.exit1920);
		}
		if (isNE(covrIO.getSicurr(),SPACES)) {
			sv.sicurr.set(covrIO.getSicurr());
		}
		else {
			sv.sicurr.set(covrIO.getPremCurrency());
		}
		sv.crtable.set(covrIO.getCrtable());
		sv.sumin.set(covrIO.getSumins());
		totSuminsCustomerSpecific1100();
		readT3629rate(sv.sicurr);
		
		
		if (isNE(covrIO.getSumins(),ZERO)) {
			printIt6000();
		
		}
		while ( !((isEQ(covrIO.getStatuz(),varcom.endp)
		|| isGT(wsaaLineCount,14)))) {
			readNextCovr1930();
		}
		
	}

protected void readNextCovr1930()
	{
		try {
			readNext1930();
		}
		catch (GOTOException e){
		}
	}

protected void readNext1930()
	{
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			if (isNE(covrIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.readNextExit1930);
		}
		if (isGT(covrIO.getChdrnum(),chdrIO.getChdrnum())) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.readNextExit1930);
		}
		if ((isNE(covrIO.getChdrcoy(),chdrIO.getChdrcoy()))
		|| (isNE(wsaaLife,covrIO.getLife()))
		|| (isNE(covrIO.getChdrnum(),chdrIO.getChdrnum()))) {
			goTo(GotoLabel.readNextExit1930);
		}
		if (isNE(covrIO.getValidflag(),"1")) {
			goTo(GotoLabel.readNextExit1930);
		}
		checkCovrStatus9000();
		if (isNE(wsaaValidStatusFlag,"Y")) {
			goTo(GotoLabel.readNextExit1930);
		}
		sv.sicurr.set(covrIO.getSicurr());
		sv.crtable.set(covrIO.getCrtable());
		sv.sumin.set(covrIO.getSumins());
		totSuminsCustomerSpecific1100();
		readT3629rate(sv.sicurr);
		
		if (isNE(covrIO.getSumins(),ZERO)) {
			printIt6000();
		}
	}

protected void beginCovt1940()
	{
		try {
			begin1940();
		}
		catch (GOTOException e){
		}
	}

protected void begin1940()
	{
		covtlnbIO.setDataKey(SPACES);
		covtlnbIO.setChdrcoy(chdrIO.getChdrcoy());
		covtlnbIO.setChdrnum(chdrIO.getChdrnum());
		covtlnbIO.setLife(wsaaLife);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRNUM");
		covtlnbIO.setSeqnbr(0);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1940);
		}
		if (isGT(covtlnbIO.getChdrnum(),chdrIO.getChdrnum())) {
			covtlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1940);
		}
		if ((isNE(covtlnbIO.getChdrcoy(),chdrIO.getChdrcoy()))
		|| (isNE(covtlnbIO.getChdrnum(),chdrIO.getChdrnum()))
		|| (isNE(wsaaLife,covtlnbIO.getLife()))) {
			goTo(GotoLabel.exit1940);
		}
		sv.sicurr.set(covtlnbIO.getCntcurr());
		sv.crtable.set(covtlnbIO.getCrtable());
		sv.sumin.set(covtlnbIO.getSumins());
		totSuminsCustomerSpecific1101();
		readT3629rate(sv.sicurr);
		if (isNE(covtlnbIO.getSumins(),ZERO)) {
			printIt6500();
			while ( !((isEQ(covtlnbIO.getStatuz(),varcom.endp)
			|| isGT(wsaaLineCount,14)))) {
				readNextCovt1950();
			}
			
		}
	}

protected void readNextCovt1950()
	{
		try {
			readNext1950();
		}
		catch (GOTOException e){
		}
	}

protected void readNext1950()
	{
		covtlnbIO.setFunction(varcom.nextr);
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			if (isNE(covtlnbIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.readNextExit1950);
		}
		if (isGT(covtlnbIO.getChdrnum(),chdrIO.getChdrnum())) {
			covtlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.readNextExit1950);
		}
		if ((isNE(covtlnbIO.getChdrcoy(),chdrIO.getChdrcoy()))
		|| (isNE(wsaaLife,covtlnbIO.getLife()))
		|| (isNE(covtlnbIO.getChdrnum(),chdrIO.getChdrnum()))) {
			goTo(GotoLabel.readNextExit1950);
		}
		sv.sicurr.set(covtlnbIO.getCntcurr());
		sv.crtable.set(covtlnbIO.getCrtable());
		sv.sumin.set(covtlnbIO.getSumins());
		totSuminsCustomerSpecific1101();
		readT3629rate(sv.sicurr);
		if (isNE(covtlnbIO.getSumins(),ZERO)) {
			printIt6500();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsaaCo.set(SPACES);
		wsaaContractNo.set(SPACES);
		wsaaLineCount.set(ZERO);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					writeNextPage2200();
				}
				case validateSubfile2600: {
					validateSubfile2600();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		commandKeyPressed = "N";
		/*VALIDATE-SCREEN*/
		if (isNE(scrnparams.statuz,varcom.rolu)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void writeNextPage2200()
	{
		wsspcomn.edterror.set("Y");
		if (isNE(covrIO.getStatuz(),varcom.endp)
		&& isNE(clrrIO.getStatuz(),varcom.endp)) {
			while ( !((isEQ(covrIO.getStatuz(),varcom.endp)
			|| isGT(wsaaLineCount,14)))) {
				readNextCovr1930();
			}
			
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		&& isNE(clrrIO.getStatuz(),varcom.endp)
		&& isLTE(wsaaLineCount,14)) {
			covrIO.setStatuz(varcom.oK);
			while ( !((isEQ(clrrIO.getStatuz(),varcom.endp)
			|| isGT(wsaaLineCount,14)))) {
				loadSubfile5000();
			}
			
		}
		if (isEQ(wsaaLineCount,0)
		&& isNE(scrnparams.errorCode,f321)) {
			scrnparams.errorCode.set(e040);
		}
		if (isEQ(clrrIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		commandKeyPressed = "Y";
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2600()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6222", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2090);
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6222", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		goTo(GotoLabel.validateSubfile2600);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		clrrIO.setDataKey(wsspsmart.itemkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadSubfile5000()
	{
		try {
			begin5000();
		}
		catch (GOTOException e){
		}
	}

protected void begin5000()
	{
		clrrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)
		&& isNE(clrrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		if (isGT(clrrIO.getClntnum(),wsspsmart.flddkey)) {
			clrrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5900);
		}
		if ((isNE(clrrIO.getClntpfx(),"CN"))
		|| (isNE(clrrIO.getClntcoy(),wsspcomn.fsuco))
		|| (isNE(clrrIO.getClrrrole(),"LF"))
		|| (isNE(clrrIO.getClntnum(),wsspsmart.flddkey))) {
			goTo(GotoLabel.exit5900);
		}
		if (isEQ(clrrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit5900);
		}
		readContract1910();
	}

protected void printIt6000()
	{
		checkCoverageStatus6010();
		checkLextpfTable(chdrIO.getChdrcoy().toString().trim(), chdrIO.getChdrnum().toString().trim());//edited by BARAKAT
	}

protected void checkCoverageStatus6010()
	{
		if (isNE(covrIO.getChdrcoy(),wsaaCo)) {
			wsaaCo.set(covrIO.getChdrcoy());
			sv.company.set(covrIO.getChdrcoy());
		}
		else {
			sv.company.set(SPACES);
		}
		if (isNE(chdrIO.getChdrnum(),wsaaContractNo)) {
			wsaaContractNo.set(chdrIO.getChdrnum());
		}
		else {
			sv.chdrnum.set(SPACES);
		}
		
		sv.crtable.set(covrIO.getCrtable());
		wsaaLineCount.add(1);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6222", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void printIt6500()
	{
		begin6510();
	}

protected void begin6510()
	{
		if (isNE(covtlnbIO.getChdrcoy(),wsaaCo)) {
			wsaaCo.set(covtlnbIO.getChdrcoy());
			sv.company.set(covtlnbIO.getChdrcoy());
		}
		else {
			sv.company.set(SPACES);
		}
		if (isNE(chdrIO.getChdrnum(),wsaaContractNo)) {
			wsaaContractNo.set(chdrIO.getChdrnum());
		}
		else {
			sv.chdrnum.set(SPACES);
		}

		sv.crtable.set(covtlnbIO.getCrtable());
		wsaaLineCount.add(1);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6222", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void checkChdrStatus8000()
	{
		/*START*/
		wsaaValidStatusFlag.set(SPACES);
		wsaaSub.set(1);
		/*SEARCH-CHDR-RISK-STAT*/
		while ( !((isGT(wsaaSub,12)
		|| isEQ(chdrIO.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])))) {
			wsaaSub.add(1);
		}
		
		/*SEARCH-CHDR-PREM-STAT*/
		if (isLTE(wsaaSub,12)) {
			wsaaSub.set(1);
			while ( !((isGT(wsaaSub,12)
			|| isEQ(wsaaValidStatusFlag,"Y")))) {
				if (isEQ(chdrIO.getPstatcode(),t5679rec.cnPremStat[wsaaSub.toInt()])) {
					wsaaValidStatusFlag.set("Y");
				}
				else {
					wsaaSub.add(1);
				}
			}
			
		}
		/*EXIT*/
	}

protected void checkCovrStatus9000()
	{
		/*START*/
		wsaaValidStatusFlag.set(SPACES);
		wsaaSub.set(1);
		/*SEARCH-COV-RISK-STAT*/
		while ( !((isGT(wsaaSub,12)
		|| isEQ(covrIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])))) {
			wsaaSub.add(1);
		}
		
		/*SEARCH-COV-PREM-STAT*/
		if (isLTE(wsaaSub,12)) {
			wsaaSub.set(1);
			while ( !((isGT(wsaaSub,12)
			|| isEQ(wsaaValidStatusFlag,"Y")))) {
				if (isEQ(covrIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
					wsaaValidStatusFlag.set("Y");
				}
				else {
					wsaaSub.add(1);
				}
			}
			
		}
		/*EXIT*/
	}
	
protected void readT3629rate(FixedLengthStringData sicurr) {
	
}

protected void checkLextpfTable(String company, String contractNo) {
	
}
protected void 	totSuminsCustomerSpecific1100(){
	sv.tsumins.add(covrIO.getSumins());
}
protected void totSuminsCustomerSpecific1101(){
	sv.tsumins.add(covtlnbIO.getSumins());
}

}
