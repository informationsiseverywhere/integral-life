package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6235screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 3, 74}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6235ScreenVars sv = (S6235ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6235screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6235screensfl, 
			sv.S6235screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6235ScreenVars sv = (S6235ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6235screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6235ScreenVars sv = (S6235ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6235screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6235screensflWritten.gt(0))
		{
			sv.s6235screensfl.setCurrentIndex(0);
			sv.S6235screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6235ScreenVars sv = (S6235ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6235screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6235ScreenVars screenVars = (S6235ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rectype.setFieldName("rectype");
				screenVars.select.setFieldName("select");
				screenVars.sacscode.setFieldName("sacscode");
				screenVars.sacscoded.setFieldName("sacscoded");
				screenVars.sacstyp.setFieldName("sacstyp");
				screenVars.sacstypd.setFieldName("sacstypd");
				screenVars.sacscurbal.setFieldName("sacscurbal");
				screenVars.curr.setFieldName("curr");
				screenVars.component.setFieldName("component");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.rectype.set(dm.getField("rectype"));
			screenVars.select.set(dm.getField("select"));
			screenVars.sacscode.set(dm.getField("sacscode"));
			screenVars.sacscoded.set(dm.getField("sacscoded"));
			screenVars.sacstyp.set(dm.getField("sacstyp"));
			screenVars.sacstypd.set(dm.getField("sacstypd"));
			screenVars.sacscurbal.set(dm.getField("sacscurbal"));
			screenVars.curr.set(dm.getField("curr"));
			screenVars.component.set(dm.getField("component"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6235ScreenVars screenVars = (S6235ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rectype.setFieldName("rectype");
				screenVars.select.setFieldName("select");
				screenVars.sacscode.setFieldName("sacscode");
				screenVars.sacscoded.setFieldName("sacscoded");
				screenVars.sacstyp.setFieldName("sacstyp");
				screenVars.sacstypd.setFieldName("sacstypd");
				screenVars.sacscurbal.setFieldName("sacscurbal");
				screenVars.curr.setFieldName("curr");
				screenVars.component.setFieldName("component");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("rectype").set(screenVars.rectype);
			dm.getField("select").set(screenVars.select);
			dm.getField("sacscode").set(screenVars.sacscode);
			dm.getField("sacscoded").set(screenVars.sacscoded);
			dm.getField("sacstyp").set(screenVars.sacstyp);
			dm.getField("sacstypd").set(screenVars.sacstypd);
			dm.getField("sacscurbal").set(screenVars.sacscurbal);
			dm.getField("curr").set(screenVars.curr);
			dm.getField("component").set(screenVars.component);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6235screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6235ScreenVars screenVars = (S6235ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.rectype.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.sacscode.clearFormatting();
		screenVars.sacscoded.clearFormatting();
		screenVars.sacstyp.clearFormatting();
		screenVars.sacstypd.clearFormatting();
		screenVars.sacscurbal.clearFormatting();
		screenVars.curr.clearFormatting();
		screenVars.component.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6235ScreenVars screenVars = (S6235ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.rectype.setClassString("");
		screenVars.select.setClassString("");
		screenVars.sacscode.setClassString("");
		screenVars.sacscoded.setClassString("");
		screenVars.sacstyp.setClassString("");
		screenVars.sacstypd.setClassString("");
		screenVars.sacscurbal.setClassString("");
		screenVars.curr.setClassString("");
		screenVars.component.setClassString("");
	}

/**
 * Clear all the variables in S6235screensfl
 */
	public static void clear(VarModel pv) {
		S6235ScreenVars screenVars = (S6235ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.rectype.clear();
		screenVars.select.clear();
		screenVars.sacscode.clear();
		screenVars.sacscoded.clear();
		screenVars.sacstyp.clear();
		screenVars.sacstypd.clear();
		screenVars.sacscurbal.clear();
		screenVars.curr.clear();
		screenVars.component.clear();
	}
}
