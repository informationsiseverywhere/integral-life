package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:47
 * Description:
 * Copybook name: BNFYENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bnfyenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bnfyenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData bnfyenqKey = new FixedLengthStringData(256).isAPartOf(bnfyenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData bnfyenqChdrcoy = new FixedLengthStringData(1).isAPartOf(bnfyenqKey, 0);
  	public FixedLengthStringData bnfyenqChdrnum = new FixedLengthStringData(8).isAPartOf(bnfyenqKey, 1);
  	public FixedLengthStringData bnfyenqBnyclt = new FixedLengthStringData(8).isAPartOf(bnfyenqKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(239).isAPartOf(bnfyenqKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bnfyenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnfyenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}