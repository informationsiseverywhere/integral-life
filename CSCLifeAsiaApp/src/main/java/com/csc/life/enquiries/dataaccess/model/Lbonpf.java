package com.csc.life.enquiries.dataaccess.model;

import java.util.Date;
import java.math.BigDecimal;

public class Lbonpf {

	
		
		public long	unique_number;
		public String	chdrcoy;
		public String	chdrnum;
		public String	revflag;
		public String	life;
		public String	coverage;
		public String	rider;
		public String	plnsfx;
		public Integer	effdate;
		public Integer	tranno;
		public String	trcde;
		public BigDecimal	trnamt;
		public Integer	netpre;
		public BigDecimal	uextpc;
		public BigDecimal	ualprc;
		public String	usrprf;
		public String	jobnm;
		public Date	datime;
	
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(String plnsfx) {
		this.plnsfx = plnsfx;
	}
	public Integer getEffdate() {
		return effdate;
	}
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getTrcde() {
		return trcde;
	}
	public void setTrcde(String trcde) {
		this.trcde = trcde;
	}
	public BigDecimal getTrnamt() {
		return trnamt;
	}
	public void setTrnamt( BigDecimal  trnamt) {
		this.trnamt = trnamt;
	}
	public Integer getNetpre() {
		return netpre;
	}
	public void setNetpre(Integer netpre) {
		this.netpre = netpre;
	}
	public BigDecimal getUextpc() {
		return uextpc;
	}
	public void setUextpc(BigDecimal uextpc) {
		this.uextpc = uextpc;
	}
	public BigDecimal getUalprc() {
		return ualprc;
	}
	public void setUalprc(BigDecimal ualprc) {
		this.ualprc = ualprc;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	//IJTI-461 START
	public Date getDatime() {
		if (this.datime==null){
			return null;
		} else {
			return (Date) this.datime.clone();
		}
	} //IJTI-461 END
	public void setDatime(Date datime) {
		this.datime = (Date) datime.clone(); //IJTI-460
	}
	public String getRevflag() {
		return revflag;
	}
	public void setRevflag(String revflag) {
		this.revflag = revflag;
	}	
}
