package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR592
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr592ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(115);
	public FixedLengthStringData dataFields = new FixedLengthStringData(19).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData userid = DD.userid.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData uwlevel = DD.uwlevel.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData poslevel = DD.poslevel.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData uwdecision = DD.uwdecision.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData skipautouw = DD.skipautouw.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 19);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData uwlevelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData poslevelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData uwdecisionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData skipautouwErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 43);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] uwlevelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] poslevelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] uwdecisionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] skipautouwOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData windowRow = DD.windrow.copyToZonedDecimal();
	public ZonedDecimalData windowColumn = DD.windcolm.copyToZonedDecimal();


	public LongData Sr592screenWritten = new LongData(0);
	public LongData Sr592windowWritten = new LongData(0);
	public LongData Sr592hideWritten = new LongData(0);
	public LongData Sr592protectWritten = new LongData(0);
	public FixedLengthStringData susur002flag = new FixedLengthStringData(1); 
	public FixedLengthStringData automatciUWFlag = new FixedLengthStringData(1); 
	public boolean hasSubfile() {
		return false;
	}


	public Sr592ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(uwlevelOut,new String[] {"09","10","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(poslevelOut,new String[] {"09","10","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwdecisionOut,new String[] {"09","10","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(skipautouwOut,new String[] {"09","10","-09",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {userid, company, uwlevel, poslevel,uwdecision,skipautouw};
		screenOutFields = new BaseData[][] {useridOut, companyOut, uwlevelOut, poslevelOut,uwdecisionOut,skipautouwOut};
		screenErrFields = new BaseData[] {useridErr, companyErr, uwlevelErr, poslevelErr,uwdecisionErr,skipautouwErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr592screen.class;
		protectRecord = Sr592protect.class;
		hideRecord = Sr592hide.class;
	}

}
