package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr587screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr587screensfl";
		lrec.subfileClass = Sr587screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 17;
		lrec.pageSubfile = 12;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 7, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr587ScreenVars sv = (Sr587ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr587screenctlWritten, sv.Sr587screensflWritten, av, sv.sr587screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr587ScreenVars screenVars = (Sr587ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.clntnam.setClassString("");
		screenVars.lrkcls.setClassString("");
		screenVars.ldesc.setClassString("");
		screenVars.tranamt.setClassString("");
	}

/**
 * Clear all the variables in Sr587screenctl
 */
	public static void clear(VarModel pv) {
		Sr587ScreenVars screenVars = (Sr587ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.clntnum.clear();
		screenVars.clntnam.clear();
		screenVars.lrkcls.clear();
		screenVars.ldesc.clear();
		screenVars.tranamt.clear();
	}
}
