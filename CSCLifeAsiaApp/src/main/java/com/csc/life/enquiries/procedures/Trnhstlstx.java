/*
 * File: Trnhstlstx.java
 * Date: December 3, 2013 4:00:06 AM ICT
 * Author: CSC
 * 
 * Class transformed from TRNHSTLSTX.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.businessobjects.programs.Boh;
import com.csc.life.enquiries.tablestructures.Trnhstlsti;
import com.csc.life.enquiries.tablestructures.Trnhstlsto;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Boverrrec;
import com.csc.smart.recordstructures.Ldrhdr;
import com.csc.smart.recordstructures.Msgdta;
import com.csc.smart.recordstructures.Msghdr;
import com.csc.smart.recordstructures.Sessioni;
import com.csc.smart.recordstructures.Sessiono;
import com.csc.smart.recordstructures.Sferrrec;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart.tablestructures.T1680rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.printing.PrinterSpool;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2006.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*              Business Object Test Rig.
*
****************************************************************** ****
* </pre>
*/
public class Trnhstlstx extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private PrinterSpool printerFile = new PrinterSpool("QSYSPRT");
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(12).init("TRNHSTLSTX  ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final int wsaaMaxLines = 60;
	private ZonedDecimalData wsaaX = new ZonedDecimalData(4, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaPages = new ZonedDecimalData(4, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaLines = new ZonedDecimalData(4, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaPhase = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(10);
	private String wsaaSessionVrbid = "";
	private PackedDecimalData wsaaRspl = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaLdrhdrl = new PackedDecimalData(5, 0).init(100);
	private PackedDecimalData wsaaMsgstart = new PackedDecimalData(5, 0).init(101);
	private FixedLengthStringData wsaaQmhsndpmParm1 = new FixedLengthStringData(7).init(SPACES);
	private FixedLengthStringData wsaaQmhsndpmParm2 = new FixedLengthStringData(20).init(SPACES);
	private FixedLengthStringData wsaaQmhsndpmParm3 = new FixedLengthStringData(200).init(SPACES);
	private BinaryData wsaaQmhsndpmParm4 = new BinaryData(9, 0).init(200);
	private FixedLengthStringData wsaaQmhsndpmParm5 = new FixedLengthStringData(10).init("*INFO");
	private FixedLengthStringData wsaaQmhsndpmParm6 = new FixedLengthStringData(10).init(SPACES);
	private BinaryData wsaaQmhsndpmParm7 = new BinaryData(9, 0).init(1);
	private FixedLengthStringData wsaaQmhsndpmParm8 = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaQmhsndpmParm9 = new FixedLengthStringData(16);
	private BinaryData bytesProvided = new BinaryData(9, 0).isAPartOf(wsaaQmhsndpmParm9, 0).init(0);
	private FixedLengthStringData wsaaBobj = new FixedLengthStringData(10).init("TRN       ");
	private FixedLengthStringData wsaaBmth = new FixedLengthStringData(10).init("HSTLST    ");
	private FixedLengthStringData wsaaMsgi = new FixedLengthStringData(10).init("TRNHSTLSTI");
	private static final String wsaaMsgo = "TRNHSTLSTO";

	private FixedLengthStringData wsaaPrintH1 = new FixedLengthStringData(100);
	private FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(wsaaPrintH1, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(wsaaPrintH1, 40, FILLER).init("Business Object Test Harness");

	private FixedLengthStringData wsaaPrintH2 = new FixedLengthStringData(43);
	private FixedLengthStringData filler3 = new FixedLengthStringData(13).isAPartOf(wsaaPrintH2, 0, FILLER).init("Object . . :");
	private FixedLengthStringData wsaaBobjH2 = new FixedLengthStringData(30).isAPartOf(wsaaPrintH2, 13);

	private FixedLengthStringData wsaaPrintH3 = new FixedLengthStringData(43);
	private FixedLengthStringData filler4 = new FixedLengthStringData(13).isAPartOf(wsaaPrintH3, 0, FILLER).init("Method . . :");
	private FixedLengthStringData wsaaBmthH3 = new FixedLengthStringData(30).isAPartOf(wsaaPrintH3, 13);

	private FixedLengthStringData wsaaPrintH4 = new FixedLengthStringData(43);
	private FixedLengthStringData filler5 = new FixedLengthStringData(13).isAPartOf(wsaaPrintH4, 0, FILLER).init("Message ID :");
	private FixedLengthStringData wsaaMsgidH4 = new FixedLengthStringData(30).isAPartOf(wsaaPrintH4, 13);

	private FixedLengthStringData wsaaPrintH5 = new FixedLengthStringData(121);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrintH5, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler7 = new FixedLengthStringData(5).isAPartOf(wsaaPrintH5, 2, FILLER).init("  Occ");
	private FixedLengthStringData filler8 = new FixedLengthStringData(2).isAPartOf(wsaaPrintH5, 7, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(50).isAPartOf(wsaaPrintH5, 9, FILLER).init("Field Name");
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrintH5, 59, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(60).isAPartOf(wsaaPrintH5, 61, FILLER).init("Value");
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
		/* TABLES */
	private static final String t1680 = "T1680";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaLanguage = new FixedLengthStringData(1);
	private PackedDecimalData lsaaYear = new PackedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData lsaaMonth = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData lsaaCmtcontrol = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaRspmode = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaOpmode = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaMoreInd = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaIgnoreDriverHeld = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaSuppressRclrsc = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaParams = new FixedLengthStringData(9999);

	private FixedLengthStringData filler12 = new FixedLengthStringData(14).isAPartOf(lsaaParams, 0, FILLER_REDEFINE);
	private PackedDecimalData lPageno = new PackedDecimalData(2, 0).isAPartOf(filler12, 2).setUnsigned();
	private PackedDecimalData lWindrow = new PackedDecimalData(3, 0).isAPartOf(filler12, 4).setUnsigned();
	private FixedLengthStringData lChdrnum = new FixedLengthStringData(8).isAPartOf(filler12, 6);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Msgdta wsaaRqsdta = new Msgdta();
	private Msgdta wsaaRspdta = new Msgdta();
	private Sessioni sessioni = new Sessioni();
	private Sessiono sessiono = new Sessiono();
	private Trnhstlsti trnhstlsti = new Trnhstlsti();
	private Trnhstlsto trnhstlsto = new Trnhstlsto();
	private Ldrhdr wsaaLdrhdr = new Ldrhdr();
	private Msghdr wsaaMsghdr = new Msghdr();
	private Boverrrec validationError = new Boverrrec();
	private Sferrrec fatalError = new Sferrrec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private T1680rec t1680rec = new T1680rec();
	private WsaaPrintD1Inner wsaaPrintD1Inner = new WsaaPrintD1Inner();

	public Trnhstlstx() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaParams = convertAndSetParam(lsaaParams, parmArray, 11);
		lsaaSuppressRclrsc = convertAndSetParam(lsaaSuppressRclrsc, parmArray, 10);
		lsaaIgnoreDriverHeld = convertAndSetParam(lsaaIgnoreDriverHeld, parmArray, 9);
		lsaaMoreInd = convertAndSetParam(lsaaMoreInd, parmArray, 8);
		lsaaOpmode = convertAndSetParam(lsaaOpmode, parmArray, 7);
		lsaaRspmode = convertAndSetParam(lsaaRspmode, parmArray, 6);
		lsaaCmtcontrol = convertAndSetParam(lsaaCmtcontrol, parmArray, 5);
		lsaaMonth = convertAndSetParam(lsaaMonth, parmArray, 4);
		lsaaYear = convertAndSetParam(lsaaYear, parmArray, 3);
		lsaaLanguage = convertAndSetParam(lsaaLanguage, parmArray, 2);
		lsaaBranch = convertAndSetParam(lsaaBranch, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		start0001();
		cleanup0080();
	}

protected void start0001()
	{
		/*    - Call BOH with Session.Start to start a session*/
		/*    - Call BOH with Session.Rtrv to retrieve the WSSP values*/
		/*    - Print the request data*/
		/*    - Call BOH with the actual Business Object to test*/
		/*    - Print the response data*/
		/*    - Call BOH with Session.End to end the session*/
		/*    - Send message to user*/
		x600ReadT1680();
		printerFile.openOutput();
		wsaaSessionVrbid = "START";
		callBohSession1000();
		if (!wsaaLdrhdr.noError.isTrue()) {
			return ;
		}
		wsaaSessionVrbid = "RTRV";
		callBohSession1000();
		if (!wsaaLdrhdr.noError.isTrue()) {
			return ;
		}
		printRequestData2000();
		callBohWsaaBobj3000();
		if (!wsaaLdrhdr.noError.isTrue()) {
			return ;
		}
		printResponseData4000();
	}

protected void cleanup0080()
	{
		x500SendMessage();
		wsaaSessionVrbid = "END";
		callBohSession1000();
		/*EXIT*/
		printerFile.close();
		/*EXIT*/
		goBack();
	}

protected void callBohSession1000()
	{
		start1010();
	}

protected void start1010()
	{
		/*    Set up SESSIONI values*/
		sessioni.msgid.set("SESSIONI");
		sessioni.msglng.set(length(sessioni.rec));
		sessioni.msglng.subtract(length(wsaaMsghdr));
		sessioni.msgcnt.set(1);
		sessioni.company.set(lsaaCompany);
		sessioni.branch.set(lsaaBranch);
		sessioni.language.set(lsaaLanguage);
		sessioni.acctyr.set(lsaaYear);
		sessioni.acctmn.set(lsaaMonth);
		/*    Retrieve User*/
		noSource("JOBUSER", wsaaUser);
		/*    Set up Leader Header Values*/
		wsaaLdrhdr.set(SPACES);
		wsaaLdrhdr.usrprf.set(wsaaUser);
		wsaaLdrhdr.wksid.set(SPACES);
		wsaaLdrhdr.objid.set("SESSION");
		wsaaLdrhdr.vrbid.set(wsaaSessionVrbid);
		compute(wsaaLdrhdr.totmsglng, 0).set(length(wsaaLdrhdr) + length(sessioni.rec));
		wsaaLdrhdr.opmode.set("2");
		wsaaLdrhdr.cmtcontrol.set(lsaaCmtcontrol);
		wsaaLdrhdr.rspmode.set("I");
		wsaaLdrhdr.msgintent.set("R");
		wsaaLdrhdr.moreInd.set("N");
		wsaaLdrhdr.ignoreDriverHeld.set(lsaaIgnoreDriverHeld);
		wsaaLdrhdr.suppressRclrsc.set(lsaaSuppressRclrsc);
		/*    Call Business Object Handler*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaLdrhdr);
		stringVariable1.addExpression(sessioni.rec);
		stringVariable1.setStringInto(wsaaRqsdta);
		callProgram(Boh.class, wsaaRqsdta, wsaaRspdta);
		wsaaLdrhdr.set(subString(wsaaRspdta, 1, wsaaLdrhdrl));
		if (wsaaLdrhdr.respFatal.isTrue()) {
			wsaaRspl.set(length(fatalError));
			fatalError.set(subString(wsaaRspdta, wsaaMsgstart, wsaaRspl));
			x200PrintFerr();
		}
		wsaaRspl.set(length(sessiono.rec));
		sessiono.rec.set(subString(wsaaRspdta, wsaaMsgstart, wsaaRspl));
		wsspcomn.commonArea.set(sessiono.wssp);
	}

protected void printRequestData2000()
	{
		start2010();
	}

protected void start2010()
	{
		wsaaPhase.set("1");
		x300PrintHeader();
		wsaaPrintD1Inner.wsaaFieldD1.set("Company");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaCompany);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Branch");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaBranch);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Year   ");
		wsaaPrintD1Inner.wsaaValueD1.set(wsspcomn.acctyear);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Month  ");
		wsaaPrintD1Inner.wsaaValueD1.set(wsspcomn.acctmonth);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Commitment Control");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaCmtcontrol);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Response Mode");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaRspmode);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Operation mode");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaOpmode);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("More Indicator");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaMoreInd);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Ignore DRIVER HELD status");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaIgnoreDriverHeld);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Suppress RCLRSC execution");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaSuppressRclrsc);
		x400PrintDetail();
		trnhstlsti.bgenPageno.set(lPageno);
		wsaaPrintD1Inner.wsaaValueD1Dp0.set(lPageno);
		wsaaPrintD1Inner.wsaaFieldD1.set("PAGENO");
		x400PrintDetail();
		trnhstlsti.bgenWindrow.set(lWindrow);
		wsaaPrintD1Inner.wsaaValueD1Dp0.set(lWindrow);
		wsaaPrintD1Inner.wsaaFieldD1.set("WINDROW");
		x400PrintDetail();
		trnhstlsti.bgenChdrnum.set(lChdrnum);
		wsaaPrintD1Inner.wsaaValueD1.set(lChdrnum);
		wsaaPrintD1Inner.wsaaFieldD1.set("CHDRNUM");
		x400PrintDetail();
	}

protected void callBohWsaaBobj3000()
	{
		start3010();
	}

protected void start3010()
	{
		wsaaRqsdta.set(SPACES);
		wsaaRspdta.set(SPACES);
		trnhstlsti.msgid.set(wsaaMsgi);
		trnhstlsti.msglng.set(length(trnhstlsti.messageData));
		trnhstlsti.msgcnt.set(1);
		trnhstlsto.msglng.set(length(trnhstlsto.messageData));
		trnhstlsto.msgcnt.set(1);
		wsaaLdrhdr.set(SPACES);
		wsaaLdrhdr.usrprf.set(wsaaUser);
		wsaaLdrhdr.wksid.set(SPACES);
		wsaaLdrhdr.objid.set(wsaaBobj);
		wsaaLdrhdr.vrbid.set(wsaaBmth);
		compute(wsaaLdrhdr.totmsglng, 0).set(length(wsaaLdrhdr) + length(trnhstlsti.rec));
		wsaaLdrhdr.opmode.set(lsaaOpmode);
		wsaaLdrhdr.cmtcontrol.set(lsaaCmtcontrol);
		wsaaLdrhdr.rspmode.set(lsaaRspmode);
		wsaaLdrhdr.msgintent.set("R");
		wsaaLdrhdr.moreInd.set(lsaaMoreInd);
		wsaaLdrhdr.ignoreDriverHeld.set(lsaaIgnoreDriverHeld);
		wsaaLdrhdr.suppressRclrsc.set(lsaaSuppressRclrsc);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaLdrhdr);
		stringVariable1.addExpression(trnhstlsti.rec);
		stringVariable1.setStringInto(wsaaRqsdta);
		callProgram(Boh.class, wsaaRqsdta, wsaaRspdta);
		wsaaLdrhdr.set(subString(wsaaRspdta, 1, wsaaLdrhdrl));
		if (wsaaLdrhdr.respFatal.isTrue()) {
			wsaaRspl.set(length(fatalError));
			fatalError.set(subString(wsaaRspdta, wsaaMsgstart, wsaaRspl));
			x200PrintFerr();
		}
		if (wsaaLdrhdr.respWarning.isTrue()) {
			wsaaRspl.set(length(validationError));
			validationError.set(subString(wsaaRspdta, wsaaMsgstart, wsaaRspl));
			x100PrintVerr();
		}
		wsaaRspl.set(length(trnhstlsto.rec));
		trnhstlsto.rec.set(subString(wsaaRspdta, wsaaMsgstart, wsaaRspl));
	}

protected void printResponseData4000()
	{
		start4010();
	}

protected void start4010()
	{
		wsaaPhase.set("2");
		x300PrintHeader();
		wsaaPrintD1Inner.wsaaFieldD1.set("Company");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaCompany);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Branch");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaBranch);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Year   ");
		wsaaPrintD1Inner.wsaaValueD1.set(wsspcomn.acctyear);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Month  ");
		wsaaPrintD1Inner.wsaaValueD1.set(wsspcomn.acctmonth);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Commitment Control");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaCmtcontrol);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Response Mode");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaRspmode);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Operation mode");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaOpmode);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("More Indicator");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaMoreInd);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Ignore DRIVER HELD status");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaIgnoreDriverHeld);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Suppress RCLRSC execution");
		wsaaPrintD1Inner.wsaaValueD1.set(lsaaSuppressRclrsc);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Errlvl  ");
		wsaaPrintD1Inner.wsaaValueD1.set(wsaaLdrhdr.errlvl);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaValueD1Dp0.set(trnhstlsto.bgenS6233Effdate);
		wsaaPrintD1Inner.wsaaFieldD1.set("BGEN-S6233-EFFDATE OF TRNHSTLSTO-REC");
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaValueD1.set(trnhstlsto.bgenS6233Statuz);
		wsaaPrintD1Inner.wsaaFieldD1.set("BGEN-S6233-STATUZ OF TRNHSTLSTO-REC");
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaValueD1Dp0.set(trnhstlsto.bgenS6233Rrn);
		wsaaPrintD1Inner.wsaaFieldD1.set("BGEN-S6233-RRN OF TRNHSTLSTO-REC");
		x400PrintDetail();
	}

protected void x100PrintVerr()
	{
		/*X110-START*/
		wsaaPhase.set("2");
		x300PrintHeader();
		validationError.set(subString(wsaaRspdta, wsaaMsgstart, wsaaRspl));
		for (wsaaX.set(1); !(isGT(wsaaX, 25)
		|| isEQ(validationError.boverrField[wsaaX.toInt()], SPACES)); wsaaX.add(1)){
			if (isEQ(validationError.boverrBofoccur[wsaaX.toInt()], NUMERIC)) {
				wsaaPrintD1Inner.wsaaOccD1.set(validationError.boverrBofoccur[wsaaX.toInt()]);
			}
			else {
				wsaaPrintD1Inner.wsaaOccD1.set(ZERO);
			}
			wsaaPrintD1Inner.wsaaFieldD1.set(validationError.boverrField[wsaaX.toInt()]);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaX);
			stringVariable1.addExpression(" - ");
			stringVariable1.addExpression(wsaaX);
			stringVariable1.setStringInto(wsaaPrintD1Inner.wsaaValueD1);
			x400PrintDetail();
		}
		/*X190-EXIT*/
	}

protected void x200PrintFerr()
	{
		x210Start();
	}

protected void x210Start()
	{
		wsaaPhase.set("1");
		wsaaBobj.set(wsaaLdrhdr.objid);
		wsaaBmth.set(wsaaLdrhdr.vrbid);
		wsaaMsgi.set(wsaaMsghdr.msgid);
		x300PrintHeader();
		wsaaPrintD1Inner.wsaaFieldD1.set("Description");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.erordesc);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Statuz");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.erorstatuz);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Batch");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.batch);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Company");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.company);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Branch");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.branch);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Year   ");
		if (isNE(wsspcomn.acctyear, NUMERIC)
		|| isEQ(wsspcomn.acctyear, ZERO)) {
			wsaaPrintD1Inner.wsaaValueD1.set(lsaaYear);
		}
		else {
			wsaaPrintD1Inner.wsaaValueD1.set(wsspcomn.acctyear);
		}
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Month  ");
		if (isNE(wsspcomn.acctmonth, NUMERIC)
		|| isEQ(wsspcomn.acctmonth, ZERO)) {
			wsaaPrintD1Inner.wsaaValueD1.set(lsaaMonth);
		}
		else {
			wsaaPrintD1Inner.wsaaValueD1.set(wsspcomn.acctmonth);
		}
		x400PrintDetail();
		if (isNE(fatalError.errlits, SPACES)) {
			wsaaPrintD1Inner.wsaaFieldD1.set("Text   ");
			if (isNE(fatalError.errlit[1], SPACES)) {
				wsaaPrintD1Inner.wsaaValueD1.set(fatalError.errlit[1]);
				x400PrintDetail();
				wsaaPrintD1Inner.wsaaFieldD1.set(SPACES);
			}
			if (isNE(fatalError.errlit[2], SPACES)) {
				wsaaPrintD1Inner.wsaaValueD1.set(fatalError.errlit[2]);
				x400PrintDetail();
				wsaaPrintD1Inner.wsaaFieldD1.set(SPACES);
			}
			if (isNE(fatalError.errlit[3], SPACES)) {
				wsaaPrintD1Inner.wsaaValueD1.set(fatalError.errlit[3]);
				x400PrintDetail();
				wsaaPrintD1Inner.wsaaFieldD1.set(SPACES);
			}
			if (isNE(fatalError.errlit[4], SPACES)) {
				wsaaPrintD1Inner.wsaaValueD1.set(fatalError.errlit[4]);
				x400PrintDetail();
			}
		}
		wsaaPrintD1Inner.wsaaFieldD1.set("Function");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.func);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Last Prog");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.lastprog);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Master Mnu");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.mastmenu);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Sub Menu  ");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.submenu);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Subroutine");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.subrname);
		x400PrintDetail();
		wsaaPrintD1Inner.wsaaFieldD1.set("Error Msg ");
		wsaaPrintD1Inner.wsaaValueD1.set(fatalError.syserrkey);
		x400PrintDetail();
	}

protected void x300PrintHeader()
	{
		x310Start();
	}

protected void x310Start()
	{
		wsaaPages.add(1);
		if (isEQ(wsaaPages, 1)) {
			printerFile.write(wsaaPrintH1);
		}
		else {
			printerFile.write(wsaaPrintH1, "AFTER", "PAGE");
		}
		wsaaBobjH2.set(wsaaBobj);
		printerFile.write(wsaaPrintH2);
		wsaaBmthH3.set(wsaaBmth);
		printerFile.write(wsaaPrintH3);
		if (isEQ(wsaaPhase, "1")) {
			wsaaMsgidH4.set(wsaaMsgi);
		}
		else {
			wsaaMsgidH4.set(wsaaMsgo);
		}
		printerFile.write(wsaaPrintH4);
		printerFile.write(wsaaPrintH5, "AFTER", 1);
		wsaaLines.set(7);
	}

protected void x400PrintDetail()
	{
		/*X410-START*/
		wsaaLines.add(1);
		if (isGT(wsaaLines, wsaaMaxLines)) {
			x300PrintHeader();
		}
		printerFile.write(wsaaPrintD1Inner.wsaaPrintD1);
		wsaaPrintD1Inner.wsaaPrintD1.set(SPACES);
		/*X490-EXIT*/
	}

protected void x500SendMessage()
	{
		/*X500-START*/
		if (wsaaLdrhdr.noError.isTrue()){
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("Business Object ");
			stringVariable1.addExpression(wsaaBobj, SPACES);
			stringVariable1.addExpression(" ended succesfully - See ");
			stringVariable1.addExpression("Spooled Files for details.");
			stringVariable1.setStringInto(wsaaQmhsndpmParm3);
		}
		else if (wsaaLdrhdr.respWarning.isTrue()){
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("Business Object ");
			stringVariable2.addExpression(wsaaBobj, SPACES);
			stringVariable2.addExpression(" found validation error - See ");
			stringVariable2.addExpression("Spooled Files for details.");
			stringVariable2.setStringInto(wsaaQmhsndpmParm3);
		}
		else if (wsaaLdrhdr.respFatal.isTrue()){
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression("Business Object ");
			stringVariable3.addExpression(wsaaBobj, SPACES);
			stringVariable3.addExpression(" returned fatal error - See ");
			stringVariable3.addExpression("Spooled Files and/or ELOGPF ");
			stringVariable3.addExpression("for details.");
			stringVariable3.setStringInto(wsaaQmhsndpmParm3);
		}
		wsaaQmhsndpmParm6.set(wsaaProg);
		sendProgramMessage(wsaaQmhsndpmParm1, wsaaQmhsndpmParm2, wsaaQmhsndpmParm3, wsaaQmhsndpmParm4, wsaaQmhsndpmParm5, wsaaQmhsndpmParm6, wsaaQmhsndpmParm7, wsaaQmhsndpmParm8, wsaaQmhsndpmParm9);
		/*X590-EXIT*/
	}

protected void x600ReadT1680()
	{
		x600Start();
	}

protected void x600Start()
	{
		itemIO.setRecKeyData(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1680);
		itemIO.setItemitem(lsaaLanguage);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), "MRNF")) {
			return ;
		}
		t1680rec.t1680Rec.set(itemIO.getGenarea());
		if (isEQ(t1680rec.languageDbcs, "Y")) {
			wsaaQcmdexc.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("OVRPRTF FILE(QSYSPRT) IGCDTA(*YES)");
			stringVariable1.setStringInto(wsaaQcmdexc);
			com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		}
	}
/*
 * Class transformed  from Data Structure WSAA-PRINT-D1--INNER
 */
private static final class WsaaPrintD1Inner { 

	private FixedLengthStringData wsaaPrintD1 = new FixedLengthStringData(121);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaPrintD1, 0, FILLER).init(SPACES);
	private ZonedDecimalData wsaaOccD1 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrintD1, 2).setPattern("ZZZZZ");
	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaPrintD1, 7, FILLER).init(SPACES);
	private FixedLengthStringData wsaaFieldD1 = new FixedLengthStringData(50).isAPartOf(wsaaPrintD1, 9);
	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaPrintD1, 59, FILLER).init(SPACES);
	private FixedLengthStringData wsaaValueD1 = new FixedLengthStringData(60).isAPartOf(wsaaPrintD1, 61);
	private FixedLengthStringData filler3 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp0 = new ZonedDecimalData(18, 0).isAPartOf(filler3, 1).setPattern("ZZZZZZZZZZZZZZZZZ9-");
	private FixedLengthStringData filler6 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp1 = new ZonedDecimalData(18, 1).isAPartOf(filler6, 0).setPattern("ZZZZZZZZZZZZZZZZ9.9-");
	private FixedLengthStringData filler8 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp2 = new ZonedDecimalData(18, 2).isAPartOf(filler8, 0).setPattern("ZZZZZZZZZZZZZZZ9.99-");
	private FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp3 = new ZonedDecimalData(18, 3).isAPartOf(filler10, 0).setPattern("ZZZZZZZZZZZZZZ9.999-");
	private FixedLengthStringData filler12 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp4 = new ZonedDecimalData(18, 4).isAPartOf(filler12, 0).setPattern("ZZZZZZZZZZZZZ9.9999-");
	private FixedLengthStringData filler14 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp5 = new ZonedDecimalData(18, 5).isAPartOf(filler14, 0).setPattern("ZZZZZZZZZZZZ9.99999-");
	private FixedLengthStringData filler16 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp6 = new ZonedDecimalData(18, 6).isAPartOf(filler16, 0).setPattern("ZZZZZZZZZZZ9.999999-");
	private FixedLengthStringData filler18 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp7 = new ZonedDecimalData(18, 7).isAPartOf(filler18, 0).setPattern("ZZZZZZZZZZ9.9999999-");
	private FixedLengthStringData filler20 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp8 = new ZonedDecimalData(18, 8).isAPartOf(filler20, 0).setPattern("ZZZZZZZZZ9.99999999-");
	private FixedLengthStringData filler22 = new FixedLengthStringData(60).isAPartOf(wsaaValueD1, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaValueD1Dp9 = new ZonedDecimalData(18, 9).isAPartOf(filler22, 0).setPattern("ZZZZZZZZ9.999999999-");
}
}
