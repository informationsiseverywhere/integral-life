/*
 * File: P6236.java
 * Date: 30 August 2009 0:37:51
 * Author: Quipoz Limited
 * 
 * Class transformed from P6236.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.accounting.recordstructures.Accinqrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.recordstructures.Wsspledg;
import com.csc.fsuframework.core.FeaConfg;
//ILIFE-8846 starts
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.model.Rdocpf;
import com.csc.fsu.general.dataaccess.dao.RdocDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.accounting.dataaccess.dao.RtrnpfDAO;
import com.csc.smart400framework.dataaccess.model.Rtrnpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
//ILIFE-8846 ends
import com.csc.life.enquiries.screens.S6236ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bowscpy;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Ldrhdr;
import com.csc.smart.recordstructures.Msgdta;
import com.csc.smart.recordstructures.Msghdr;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sessioni;
import com.csc.smart.recordstructures.Sessiono;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*
*     The details of  the  transaction  selected  from the previous
*     screen will be in  either  the  ACBLENQ  I/O  module  or  the
*     SACSENQ I/O module. One  module  will  contain  a  record the
*     other will not. By performing  a  RETRV  on  both  it will be
*     possible to determine which data-set to read for the data for
*     this function. If an ACBL  record  is found then ACMV will be
*     used, if a SACS record is found then RTRN will be used.
*
*     Display the SACSCODE and its short description from T3616 and
*     SACSTYP and its short  description  from  T3695 in the header
*     portion of the screen.
*
*     The  details  that will  be  displayed  will  come  from  two
*     data-sets:  ACMVSAC and  RTRNSAC.  The  same information will
*     be extracted from both  data-sets  and they will both be read
*     in the same sequence.
*
*     This program should  process all the relevant ACMVSAC records
*     OR all the relevant RTRNSAC records.
*
*     If an ACBLENQ record  was  found set the Original Currency in
*     the screen header  from ACBLENQ-ORIGCURR and load the subfile
*     as follows:
*
*          Set the Accounting  Currency  in  the screen header from
*          ACMVSAC-GENLCUR.
*
*          From  the  ACBLENQ   record  take  the  fields  RLDGCOY,
*          SACSCODE,  RLDGACCT,   SACSTYP  and  ORIGCURR  and  read
*          ACMVSAC with this key  until  end of file or any portion
*          of the key, (apart from the ORIGCURR), changes.
*
*          Display the Effective Date (EFFDATE), Transaction number
*          (TRANNO), G/L  code  (GLCODE), Original Amount (ORIGAMT)
*          and Accounting Amount (ACCTAMT).
*
*     If a SACSENQ record  was  found  set the Original Currency in
*     the screen header  from  SACSENQ-CNTCURR and load the subfile
*     as follows:
*
*          Set the Accounting  Currency  in  the screen header from
*          RTRNSAC-ACCTCCY.
*
*          From  the  SACSENQ   record  take  the  fields  CHDRCOY,
*          SACSCODE, CHDRNUM, SACSTYP  and CNTCURR and read RTRNSAC
*          with this key until end  of  file  or any portion of the
*          key, (apart from the CNTCURR), changes.
*
*          Display the Effective Date (EFFDATE), Transaction number
*          (TRANNO), G/L  code  (GLCODE), Original Amount (ORIGAMT)
*          and Accounting Amount (ACCTAMT).
*
*
*     Load all pages required  in  the  subfile and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     For "CF11" or "Enter"  continue  by adding one to the program
*     pointer.
*
*
*
* Notes.
* ------
*
*     Create a new view  of  ACMVPF  which  uses  only those fields
*     required for this  program  and  keyed  on Company (RLDGCOY),
*     SACSCODE, Contract  Number  (RLDGACCT),  SACSTYP and Original
*     Currency (ORIGCURR).
*
*     Create a new view  of  RTRNPF  which  uses  only those fields
*     required for this  program  and  keyed  on Company (RLDGCOY),
*     SACSCODE, Contract  Number  (RLDGACCT),  SACSTYP and Original
*     Currency (CNTCURR).
*
*
*     Tables Used:
*
* T1688 - Branch Codes                      Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3616 - Sub-Account Code                  Key: SACSCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T3695 - Sub-Account Type                  Key: SACSTYP
* T5688 - Contract Structure                Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6236 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6236");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaComponent = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaComponent, 0);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaComponent, 2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaComponent, 4);
	private FixedLengthStringData wsaaPlansuff = new FixedLengthStringData(2).isAPartOf(wsaaComponent, 6);
		/* WSAA-WSSPS */
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSubmenu = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaNextprog = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaSectionno = new FixedLengthStringData(4);
	private PackedDecimalData wsaaProgramPtr = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaSecSwitching = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaSecActns = new FixedLengthStringData(22).isAPartOf(wsaaSecSwitching, 0);
	private FixedLengthStringData[] wsaaSecActn = FLSArrayPartOfStructure(22, 1, wsaaSecActns, 0);
	private FixedLengthStringData wsaaSecProgs = new FixedLengthStringData(110).isAPartOf(wsaaSecSwitching, 22);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(22, 5, wsaaSecProgs, 0);
	private FixedLengthStringData wsaaGenvkey = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaDoctkey = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaAlockey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcumkey = new FixedLengthStringData(41);
	private FixedLengthStringData wsaaRgformat = new FixedLengthStringData(17);
	private FixedLengthStringData wsaaRgdetail = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaRgreport = new FixedLengthStringData(15);
	private FixedLengthStringData wsaaBatchkey = new FixedLengthStringData(24);
	private String wsaaRecProg = "P2067BO";
	private String wsaaPayProg = "P2201BO";
	private String wsaaJrnProg = "P2629BO";
	private FixedLengthStringData wsaaBoprog = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSelect = new ZonedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1).init("N");
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");

	private FixedLengthStringData wsaaJrnKey = new FixedLengthStringData(17);
	private FixedLengthStringData wsaaJrnJournal = new FixedLengthStringData(8).isAPartOf(wsaaJrnKey, 0);
	private ZonedDecimalData wsaaJrnDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaJrnKey, 8);
	private FixedLengthStringData wsaaJrnAction = new FixedLengthStringData(1).isAPartOf(wsaaJrnKey, 16).init("B");

	private FixedLengthStringData wsaaRecKey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaRecBankcode = new FixedLengthStringData(2).isAPartOf(wsaaRecKey, 0);
	private FixedLengthStringData wsaaRecReceipt = new FixedLengthStringData(8).isAPartOf(wsaaRecKey, 2);
	private FixedLengthStringData wsaaRecCheque = new FixedLengthStringData(9).isAPartOf(wsaaRecKey, 10).init(SPACES);
	private FixedLengthStringData wsaaRecAction = new FixedLengthStringData(1).isAPartOf(wsaaRecKey, 19).init("C");

	private FixedLengthStringData wsaaPayKey = new FixedLengthStringData(33);
	private FixedLengthStringData wsaaPayMethod = new FixedLengthStringData(1).isAPartOf(wsaaPayKey, 0).init(SPACES);
	private FixedLengthStringData wsaaPayPayee = new FixedLengthStringData(10).isAPartOf(wsaaPayKey, 1).init(SPACES);
	private FixedLengthStringData wsaaPayPaynme = new FixedLengthStringData(10).isAPartOf(wsaaPayKey, 11).init(SPACES);
	private FixedLengthStringData wsaaPayReqnno = new FixedLengthStringData(9).isAPartOf(wsaaPayKey, 21);
	private FixedLengthStringData wsaaPayBankcode = new FixedLengthStringData(2).isAPartOf(wsaaPayKey, 30).init(SPACES);
	private FixedLengthStringData wsaaPayAction = new FixedLengthStringData(1).isAPartOf(wsaaPayKey, 32).init("E");

	private FixedLengthStringData wsaaGlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaSbmaction = new FixedLengthStringData(1);
	private String t3588 = "T3588";
	private String t3616 = "T3616";
	private String t3623 = "T3623";
	private String t3695 = "T3695";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
		/*Sub Account Balances - Contract Enquiry.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Acblpf acblpf = new Acblpf();
	private boolean acblCache = false;
	private Accinqrec accinqrec = new Accinqrec();
	private Bowscpy bowscpy = new Bowscpy();
		/*Contract Enquiry - Contract Header.*/
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife lWsspUserArea = new Wssplife();
	private Optswchrec optswchrec = new Optswchrec();
	private Msgdta responseData = new Msgdta();
	private Sessioni sessioni = new Sessioni();
	private Sessiono sessiono = new Sessiono();
	private Batckey wsaaBatckey = new Batckey();
	private Ldrhdr wsaaLdrhdr = new Ldrhdr();
	private Msgdta wsaaRequest = new Msgdta();
	private Msgdta wsaaResponse = new Msgdta();
	private Wsspledg wsspledg = new Wsspledg();
	private S6236ScreenVars sv = ScreenProgram.getScreenVars( S6236ScreenVars.class);

	//fix by Tom Chi
	private Msghdr wsaaMsghdr = new Msghdr();
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(10);
	//Start looping 
	
	private FixedLengthStringData wsaaChdrCoy = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrNum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaAcctmonth = new PackedDecimalData(2,0);
	private PackedDecimalData wsaaAcctyear = new PackedDecimalData(4,0);
	
	//end looping 

	//ILJ-388 start
	private String cntEnqFeature = "CTENQ010";
	//ILJ-388 end
	
	//ILIFE-8846 starts
	private Acmvpf acmvpf = new Acmvpf();
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO",AcmvpfDAO.class);
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private RdocDAO rdocDAO = getApplicationContext().getBean("rdocDAO",RdocDAO.class);
	private RtrnpfDAO rtrnpfDAO = getApplicationContext().getBean("rtrnpfDAO",RtrnpfDAO.class);
	private Descpf descpf = new Descpf();
	private DescDAO descpfDAO = getApplicationContext().getBean("descDAO",DescDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	//ILIFE-8846 end
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1050, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		next2420, 
		exit3090, 
		callStck4020, 
		exit4090, 
		exit7090
	}

	public P6236() {
		super();
		screenVars = sv;
		new ScreenModel("S6236", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspledg.userArea = convertAndSetParam(wsspledg.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case continue1050: {
					continue1050();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaSbmaction.set(wsspcomn.sbmaction);
		wsaaRldgacct.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		// ILJ-388 Start
		boolean cntEnqFlag = false;
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if (cntEnqFlag) {
				sv.cntEnqScreenflag.set("Y");
				sv.trcodeOut[Varcom.nd.toInt()].set("N");
				
		}else {
				sv.cntEnqScreenflag.set("N");
				sv.trcodeOut[Varcom.nd.toInt()].set("Y");
				
		}
		//end
		sv.hmhii.set(ZERO);
		sv.hmhli.set(ZERO);
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6236", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		screenInitOptswch1050();
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		List<Lifepf> lifepfList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
		if (lifepfList.isEmpty()) {
			fatalError600();
		}
		else {
			for(Lifepf life : lifepfList) {
				lifepf=life;
			}
		}
		sv.lifenum.set(lifepf.getLifcnum());
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifepf.getLifcnum());
		if (clntpf == null) {
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		List<Lifepf> lifeList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "01");
		if (!lifeList.isEmpty()) {
			for (Lifepf life : lifepfList) {
					sv.jlife.set(life.getLifcnum());
					clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), life.getLifcnum());

					if(clntpf == null) {
						fatalError600();
					}
					else {
						plainname();
						sv.jlifename.set(wsspcomn.longconfname);
					}
			}
		}
		descpf=descpfDAO.getdescData("IT", t5688 ,chdrpf.getCnttype(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		descpf=descpfDAO.getdescData("IT", t3623 ,chdrpf.getStatcode(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descpf.getShortdesc());
		}
		descpf=descpfDAO.getdescData("IT", t3588 ,chdrpf.getPstcde(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descpf.getShortdesc());
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		acblpf = acblDao.getCacheObject(acblpf);
		if(!(acblpf.getUniqueNumber()>0)) {
			goTo(GotoLabel.continue1050);
		}
		acblCache=true;
		wsaaRldgacct.set(acblpf.getRldgacct());
		if (isEQ(wsaaCoverage,SPACES)) {
			goTo(GotoLabel.continue1050);
		}
		
		covrpf.setChdrnum(chdrpf.getChdrnum());
		covrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		covrpf.setLife(wsaaLife.toString());
		covrpf.setCoverage(wsaaCoverage.toString());
		covrpf.setRider(wsaaRider.toString());
		covrpf.setPlanSuffix(wsaaPlansuff.toInt());
		List<Covrpf> covrpfList = covrpfDAO.selectCovrRecordBegin(covrpf);
		if(!covrpfList.isEmpty()) {
			for(Covrpf covr : covrpfList) {
				covrpf = covr;
			}
		}
		sv.crtable.set(covrpf.getCrtable());
		descpf=descpfDAO.getdescData("IT", t5687 ,covrpf.getCrtable(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.descrip.fill("?");
		}
		else {
			sv.descrip.set(descpf.getLongdesc());
		}
	}

protected void continue1050()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		if(acblCache) {
			processAcbl1100();
		}
		if (isNE(scrnparams.deviceInd,"*RMT")) {
			scrnparams.subfileRrn.set(1);
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(clntpf.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(clntpf.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(clntpf.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void screenInitOptswch1050()
	{
		optswch1050();
	}

protected void optswch1050()
	{
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		wsspcomn.flag.set("I");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

protected void processAcbl1100()
	{
		para1100();
		rtrnRead1150();
	}

protected void para1100()
	{
		sv.sacscode.set(acblpf.getSacscode());
		sv.sacstyp.set(acblpf.getSacstyp());
		sv.entity.set(acblpf.getRldgacct());
		//ILIFE-8846 starts
		descpf=descpfDAO.getdescData("IT", t3616 ,acblpf.getSacscode(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.sacscoded.fill("?");
		}
		else {
			sv.sacscoded.set(descpf.getShortdesc());
		}
		descpf=descpfDAO.getdescData("IT", t3695 ,acblpf.getSacstyp(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.sacstypd.fill("?");
		}
		else {
			sv.sacstypd.set(descpf.getShortdesc());
		}
		acmvpf.setRldgcoy(acblpf.getRldgcoy());
		acmvpf.setSacscode(acblpf.getSacscode());
		acmvpf.setRldgacct(acblpf.getRldgacct());
		acmvpf.setSacstyp(acblpf.getSacstyp());
		acmvpf.setOrigcurr(acblpf.getOrigcurr());
		sv.origccy.set(acblpf.getOrigcurr());
		List<Acmvpf> acmvpfList = acmvpfDAO.searchAcmvpfRecords(acmvpf);
		if (!acmvpfList.isEmpty()) {
			Iterator<Acmvpf> it = acmvpfList.iterator();
			while(it.hasNext()) {
				Acmvpf acmv = it.next();
				sv.acctccy.set(acmv.getGenlcur());
				processAcmvsac1200(acmv);
			}
		}
	}

protected void rtrnRead1150()
	{		
		sv.origccy.set(acblpf.getOrigcurr());
		List<Rtrnpf> rtrnpfList = rtrnpfDAO.readAllRecords(acblpf.getRldgcoy(),acblpf.getSacscode(),acblpf.getRldgacct(),acblpf.getSacstyp(),acblpf.getOrigcurr());
		if(!rtrnpfList.isEmpty()) {
				Iterator<Rtrnpf> it = rtrnpfList.iterator();
				while(it.hasNext()) {
					Rtrnpf rtrn = it.next();
					sv.acctccy.set(rtrn.getGenlcur());
					processRtrnsac1400(rtrn);
				}
		}
			
	}

protected void processAcmvsac1200(Acmvpf acmvpf)
	{

		sv.effdate.set(acmvpf.getEffdate());
		sv.tranno.set(acmvpf.getTranno());
		sv.glcode.set(acmvpf.getGlcode());
		if (isEQ(acmvpf.getGlsign(),"-")) {
			setPrecision(acmvpf.getOrigamt(), 2);
			acmvpf.setOrigamt(acmvpf.getOrigamt().multiply(new BigDecimal(-1)));
			setPrecision(acmvpf.getAcctamt(), 2);
			acmvpf.setAcctamt(acmvpf.getAcctamt().multiply(new BigDecimal(-1)));
		}
		sv.hmhii.set(acmvpf.getOrigamt());
		sv.hmhli.set(acmvpf.getAcctamt());
		sv.trcode.set(acmvpf.getBatctrcde());
		sv.datesub.set(acmvpf.getTrdt());//ILJ-388
		sv.hcurrcd.set(acmvpf.getOrigcurr());
		sv.hacccurr.set(acmvpf.getGenlcur());
		if (isNE(acmvpf.getRdocpfx(),SPACES)
		&& isNE(acmvpf.getRdocpfx(),fsupfxcpy.chdr)) {
			sv.selectOut[varcom.pr.toInt()].set("N");
			sv.rdocnum.set(acmvpf.getRdocnum());
			sv.statz.set(acmvpf.getRdocpfx());
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.rdocnum.set(SPACES);
			sv.statz.set(SPACES);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6236", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void processRtrnsac1400(Rtrnpf rtrnpf)
	{
		loadBankcode6000(rtrnpf);
		sv.effdate.set(rtrnpf.getEffdate());
		sv.tranno.set(rtrnpf.getTranno());
		sv.glcode.set(rtrnpf.getGlcode());
		if (isEQ(rtrnpf.getGlsign(),"-")) {
			setPrecision(rtrnpf.getOrigamt(), 2);
			rtrnpf.setOrigamt(rtrnpf.getOrigamt().multiply(new BigDecimal(-1)));
			setPrecision(rtrnpf.getAcctamt(), 2);
			rtrnpf.setAcctamt(rtrnpf.getAcctamt().multiply(new BigDecimal(-1)));
		}
		sv.hmhii.set(rtrnpf.getOrigamt());
		sv.hmhli.set(rtrnpf.getAcctamt());
		sv.trcode.set(rtrnpf.getBatctrcde());
		sv.hcurrcd.set(rtrnpf.getOrigccy());
		sv.hacccurr.set(rtrnpf.getGenlcur());
		if (isNE(rtrnpf.getRdocpfx(),SPACES)
		&& isNE(rtrnpf.getRdocpfx(),fsupfxcpy.chdr)) {
			sv.selectOut[varcom.pr.toInt()].set("N");
			sv.rdocnum.set(rtrnpf.getRdocnum());
			sv.statz.set(rtrnpf.getRdocpfx());
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.rdocnum.set(SPACES);
			sv.statz.set(SPACES);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6236", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	//ILIFE-8846 ends

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkOptswch2040();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz,varcom.masm)) {
			lWsspUserArea.lifekey.set(ZERO);
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkOptswch2040()
	{
		scrnparams.function.set(varcom.sstrt);
		subfileIo9000();
		wsaaFoundSelection.set("N");
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2400();
		}
		
		if (foundSelection.isTrue()
		&& isEQ(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set(varcom.oK);
		}
		scrnparams.subfileRrn.set(1);
	}

protected void validateSubfile2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2410();
				}
				case next2420: {
					next2420();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2410()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.next2420);
		}
		optswchrec.optsStatuz.set(varcom.oK);
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelCode.set(SPACES);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
		else {
			wsaaFoundSelection.set("Y");
			goTo(GotoLabel.next2420);
		}
		scrnparams.function.set(varcom.supd);
		subfileIo9000();
	}

protected void next2420()
	{
		scrnparams.function.set(varcom.srdn);
		subfileIo9000();
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case callStck4020: {
					callStck4020();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,varcom.subm)) {
			wsspcomn.nextprog.set(wsspcomn.submenu);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.programPtr.add(1);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			restoreWsspledg7200();
			goTo(GotoLabel.callStck4020);
		}
		scrnparams.statuz.set(varcom.oK);
		wsaaFoundSelection.set("N");
		scrnparams.function.set(varcom.sstrt);
		subfileIo9000();
		while ( !(isEQ(scrnparams.statuz,varcom.endp)
		|| foundSelection.isTrue())) {
			if (isEQ(sv.select,"1")) {
				optswchrec.optsSelOptno.set(sv.select);
				checkingDoctype7000();
				sv.select.set(SPACES);
				scrnparams.function.set(varcom.supd);
				subfileIo9000();
			}
			if (!foundSelection.isTrue()) {
				scrnparams.function.set(varcom.srdn);
				subfileIo9000();
			}
		}
		
		if (!foundSelection.isTrue()) {
			scrnparams.subfileRrn.set(1);
			wsspcomn.programPtr.add(1);
			wsspcomn.sbmaction.set(wsaaSbmaction);
			goTo(GotoLabel.exit4090);
		}
	}

protected void callStck4020()
	{
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

//ILIFE-8846 starts
protected void loadBankcode6000(Rtrnpf rtrnpf)
	{
		List<Rdocpf> rdocpfList = rdocDAO.getListRdocDetails(fsupfxcpy.cash.toString(), rtrnpf.getRldgcoy(), rtrnpf.getRdocnum());
		if(!rdocpfList.isEmpty()) {
			for (Rdocpf rdocpf : rdocpfList) {
				sv.bankcode.set(rdocpf.getBankcode());
				break;
			}
		}
	}
//ILIFE-8846 ends

protected void checkingDoctype7000()
	{
		try {
			start7010();
		}
		catch (GOTOException e){
		}
	}

protected void start7010()
	{
		optswchrec.optsSelCode.set(SPACES);
		if (isEQ(optswchrec.optsSelOptno,1)) {
			initialize(accinqrec.data);
			if (isEQ(sv.statz,fsupfxcpy.gjrn)) {
				optswchrec.optsSelCode.set(sv.statz);
				wsaaJrnJournal.set(sv.rdocnum);
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaJrnDate.set(datcon1rec.intDate);
				accinqrec.acciInKey.set(wsaaJrnKey);
				wsaaBoprog.set(wsaaJrnProg);
				callBoProg7100();
			}
			else {
				if (isEQ(sv.statz,fsupfxcpy.cash)) {
					optswchrec.optsSelCode.set(sv.statz);
					wsaaRecBankcode.set(sv.bankcode);
					wsaaRecReceipt.set(sv.rdocnum);
					accinqrec.acciInKey.set(wsaaRecKey);
					wsaaBoprog.set(wsaaRecProg);
					callBoProg7100();
				}
				if (isEQ(sv.statz,fsupfxcpy.reqn)) {
					optswchrec.optsSelCode.set(sv.statz);
					wsaaPayReqnno.set(sv.rdocnum);
					accinqrec.acciInKey.set(wsaaPayKey);
					wsaaBoprog.set(wsaaPayProg);
					callBoProg7100();
				}
			}
		}
		if (isEQ(optswchrec.optsSelCode,SPACES)) {
			goTo(GotoLabel.exit7090);
		}
		wsaaSelect.set(sv.select);
		for (wsaaCount.set(1); !(isGT(wsaaCount,20)); wsaaCount.add(1)){
			if (isEQ(optswchrec.optsType[wsaaCount.toInt()],"L")
			&& isEQ(optswchrec.optsNo[wsaaCount.toInt()],wsaaSelect)
			&& isEQ(optswchrec.optsCode[wsaaCount.toInt()],optswchrec.optsSelCode)) {
				optswchrec.optsInd[wsaaCount.toInt()].set("X");
				wsaaFoundSelection.set("Y");
			}
		}
	}

protected void callBoProg7100()
	{
		start7110();
	}

protected void start7110()
	{
		wsaaFlag.set(wsspcomn.flag);
		wsaaSecProgs.set(wsspcomn.secProgs);
		wsaaSecActns.set(wsspcomn.secActns);
		wsaaProgramPtr.set(wsspcomn.programPtr);
		wsaaSubmenu.set(wsspcomn.submenu);
		wsaaNextprog.set(wsspcomn.nextprog);
		wsaaSectionno.set(wsspcomn.sectionno);
		wsaaGenvkey.set(wsspledg.genvkey);
		wsaaDoctkey.set(wsspledg.doctkey);
		wsaaAlockey.set(wsspledg.alockey);
		wsaaAcumkey.set(wsspledg.acumkey);
		wsaaRgformat.set(wsspledg.rgformat);
		wsaaRgdetail.set(wsspledg.rgdetail);
		wsaaRgreport.set(wsspledg.rgreport);
		wsaaBatchkey.set(wsspcomn.batchkey);
		accinqrec.acciCompany.set(wsspcomn.company);
		//Start looping
		wsaaChdrCoy.set(wsspcomn.chdrChdrcoy);
		wsaaChdrNum.set(wsspcomn.chdrChdrnum);
		wsaaAcctmonth.set(wsspcomn.acctmonth);
		wsaaAcctyear.set(wsspcomn.acctyear);
		//End looping
		//fix by Tom Chi
		para1110();
		//end		
		callProgram(wsaaBoprog, wsaaLdrhdr.header, wsaaResponse, responseData, accinqrec.rec);
		if (wsaaLdrhdr.respWarning.isTrue()) {
			syserrrec.params.set(accinqrec.rec);
			fatalError600();
		}
		wsaaLdrhdr.set(wsaaLdrhdr.header);
		wsaaLdrhdr.objid.set("SESSION");
		wsaaLdrhdr.vrbid.set("RTRV ");
		//fixed by Tom Chi
		//callProgram("SESSION", wsaaLdrhdr.header, wsaaRequest.data, wsaaResponse.data);
		callProgram("SESSION", wsaaLdrhdr.header, wsaaRequest, wsaaResponse);
		//noSource("SESSION", wsaaLdrhdr, wsaaRequest, wsaaResponse);
		//end
		sessiono.rec.set(wsaaResponse);
		bowscpy.wssp.set(sessiono.wssp);
		wsspcomn.commonArea.set(bowscpy.commonArea);
		//START Looping
		wsspcomn.chdrChdrcoy.set(wsaaChdrCoy);
		wsspcomn.chdrChdrnum.set(wsaaChdrNum);
		wsspcomn.acctmonth.set(wsaaAcctmonth);
		wsspcomn.batchkey.set(accinqrec.acciBatchkey);	//ILIFE-9569
		wsspcomn.acctyear.set(wsaaAcctyear);
		//END Looping
		
		wsspledg.userArea.set(bowscpy.userArea);		
		wsspcomn.flag.set(wsaaFlag);
		wsspcomn.secProgs.set(wsaaSecProgs);
		wsspcomn.secActns.set(wsaaSecActns);
		wsspcomn.programPtr.set(wsaaProgramPtr);
		wsspcomn.submenu.set(wsaaSubmenu);
		wsspcomn.nextprog.set(wsaaNextprog);
		wsspcomn.sectionno.set(wsaaSectionno);
	}
//Add by Tom Chi
protected void para1110()
{
	sessioni.msgid.set("SESSIONI");
	sessioni.msglng.set(length(sessioni.rec));
	sessioni.msglng.subtract(length(wsaaMsghdr.header));
	sessioni.msgcnt.set(1);
	sessioni.company.set(wsspcomn.company);
	sessioni.branch.set(wsspcomn.branch);
	sessioni.language.set(wsspcomn.language);
	sessioni.acctyr.set(ZERO);
	sessioni.acctmn.set(ZERO);
	wsaaLdrhdr.header.set(SPACES);
	//modify by tom chi
	//noSource("JOBUSER", wsaaUser);
	//callProgram("JOBUSER", wsaaUser);
	wsaaLdrhdr.usrprf.set(wsspcomn.userid);
	wsaaLdrhdr.wksid.set(SPACES);
	wsaaLdrhdr.objid.set("SESSION");
	wsaaLdrhdr.vrbid.set("START");
	compute(wsaaLdrhdr.totmsglng, 0).set(length(wsaaLdrhdr.header) + length(sessioni.rec));
	wsaaLdrhdr.opmode.set("1");
	wsaaLdrhdr.cmtcontrol.set("Y");
	wsaaLdrhdr.rspmode.set("I");
	wsaaLdrhdr.msgintent.set("R");
	wsaaLdrhdr.moreInd.set("N");
	StringBuilder stringVariable1 = new StringBuilder();
	stringVariable1.append(wsaaLdrhdr.header.toString());
	stringVariable1.append(sessioni.rec.toString());
	wsaaRequest.setLeft(stringVariable1.toString());
	//modify by tom chi
	//noSource("BOH", requestData, responseData);
	callProgram("BOH", wsaaRequest, wsaaResponse);
	
	wsaaLdrhdr.objid.set("SESSION");
	wsaaLdrhdr.vrbid.set("RTRV ");
	compute(wsaaLdrhdr.totmsglng, 0).set(length(wsaaLdrhdr.header) + length(sessioni.rec));
	/*
	 * Modify by Tom chi
	 */
	//callProgram("Session", wsaaLdrhdr.header, wsaaRequest.data, wsaaResponse.data);
	callProgram("Session", wsaaLdrhdr.header, wsaaRequest, wsaaResponse);
	sessiono.rec.set(wsaaResponse);
	wsspcomn.commonArea.set(sessiono.wssp);
//	wsspcomn.acctyear.set(bsscIO.getAcctYear());
//	wsspcomn.acctmonth.set(bsscIO.getAcctMonth());
	wsaaLdrhdr.objid.set("SESSION");
	wsaaLdrhdr.vrbid.set("UPD  ");
	compute(wsaaLdrhdr.totmsglng, 0).set(length(wsaaLdrhdr.header) + length(sessioni.rec));
	sessiono.wssp.set(wsspcomn.commonArea);
	wsaaResponse.set(sessiono.rec);
	//callProgram("Session", wsaaLdrhdr.header, wsaaRequest.data, wsaaResponse.data);
	callProgram("Session", wsaaLdrhdr.header, wsaaRequest, wsaaResponse);
}
//end
protected void restoreWsspledg7200()
	{
		/*START*/
		wsspledg.genlkey.set(wsaaGlkey);
		wsspledg.genvkey.set(wsaaGenvkey);
		wsspledg.doctkey.set(wsaaDoctkey);
		wsspledg.alockey.set(wsaaAlockey);
		wsspledg.acumkey.set(wsaaAcumkey);
		wsspledg.rgformat.set(wsaaRgformat);
		wsspledg.rgdetail.set(wsaaRgdetail);
		wsspledg.rgreport.set(wsaaRgreport);
		wsspcomn.batchkey.set(wsaaBatchkey);
		/*EXIT*/
	}

protected void subfileIo9000()
	{
		/*SUBFILE*/
		processScreen("S6236", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
