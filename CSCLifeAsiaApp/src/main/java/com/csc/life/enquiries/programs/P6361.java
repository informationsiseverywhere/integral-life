/*
 * File: P6361.java
 * Date: 30 August 2009 0:45:22
 * Author: Quipoz Limited
 * 
 * Class transformed from P6361.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Cltskey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.enquiries.dataaccess.ClntqyTableDAM;
import com.csc.life.enquiries.screens.S6361ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*****************************************************************
* </pre>
*/
public class P6361 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ClntqyTableDAM clntqy = new ClntqyTableDAM();
	private ClntqyTableDAM clntqyRec = new ClntqyTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6361");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaOvrdbfClntqy = new FixedLengthStringData(46);
	private FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(wsaaOvrdbfClntqy, 0, FILLER).init("OVRDBF FILE(CLNTQY) TOFILE(CLRRPF) SHARE(*YES)");
	private FixedLengthStringData wsaaQcmdexcCommand = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).setUnsigned();
	private PackedDecimalData subfileLineCount = new PackedDecimalData(3, 0);

	private FixedLengthStringData clntqyEofFlag = new FixedLengthStringData(1);
	private Validator eofClntqy = new Validator(clntqyEofFlag, "Y");
	private Validator notEofClntqy = new Validator(clntqyEofFlag, "N");

	private FixedLengthStringData wsaaClntdets1 = new FixedLengthStringData(45+DD.cltaddr.length);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData wsaaSurname = new FixedLengthStringData(20).isAPartOf(wsaaClntdets1, 2);
	private FixedLengthStringData wsaaAddr01 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaClntdets1, 23);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData wsaaCltpcode = new FixedLengthStringData(10).isAPartOf(wsaaClntdets1, 24+DD.cltaddr.length);
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8).isAPartOf(wsaaClntdets1, 37+DD.cltaddr.length);

	private FixedLengthStringData wsaaClntdets2 = new FixedLengthStringData(42+DD.cltaddr.length);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData wsaaGivname = new FixedLengthStringData(20).isAPartOf(wsaaClntdets2, 2);
	private FixedLengthStringData wsaaAddr02 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaClntdets2, 23);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData wsaaDob = new FixedLengthStringData(10).isAPartOf(wsaaClntdets2, 24+DD.cltaddr.length);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1).isAPartOf(wsaaClntdets2, 35+DD.cltaddr.length);
	private FixedLengthStringData wsaaClrrrole = new FixedLengthStringData(2).isAPartOf(wsaaClntdets2, 40+DD.cltaddr.length);

	private FixedLengthStringData wsaaInDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaInYear = new FixedLengthStringData(4).isAPartOf(wsaaInDate, 0);
	private FixedLengthStringData wsaaInMonth = new FixedLengthStringData(2).isAPartOf(wsaaInDate, 4);
	private FixedLengthStringData wsaaInDay = new FixedLengthStringData(2).isAPartOf(wsaaInDate, 6);
	private ZonedDecimalData wsaaInDate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaInDate, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaOutDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaOutDay = new FixedLengthStringData(2).isAPartOf(wsaaOutDate, 0);
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaOutDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaOutMonth = new FixedLengthStringData(2).isAPartOf(wsaaOutDate, 3);
	private FixedLengthStringData filler11 = new FixedLengthStringData(1).isAPartOf(wsaaOutDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaOutYear = new FixedLengthStringData(4).isAPartOf(wsaaOutDate, 6);
	private FixedLengthStringData wsaaLastSurname = new FixedLengthStringData(30);
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Cltskey wsaaCltskey = new Cltskey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S6361ScreenVars sv = ScreenProgram.getScreenVars( S6361ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNextClntqy1110
	}

	public P6361() {
		super();
		screenVars = sv;
		new ScreenModel("S6361", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/*MOVE 46                     TO WSAA-QCAEXEC-LENGTH.          */
		/*CALL 'QCAEXEC'           USING WSAA-QCAEXEC-COMMAND          */
		/*                               WSAA-QCAEXEC-LENGTH.          */
		wsaaQcmdexcCommand.set(wsaaOvrdbfClntqy);
		wsaaQcmdexcLength.set(46);
		// No change the overide table from CLNTQY to CLRRPF
        // Because read data from table CLNTQY with metadata from CLNTQY
		// com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexcCommand, wsaaQcmdexcLength);
		clntqy.openInput();
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6361", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		subfileLineCount.set(1);
		clntqyEofFlag.set("N");
		clntqy.read(clntqyRec);
		if (clntqy.isAtEnd()) {
			clntqyEofFlag.set("Y");
		}
		while ( !(eofClntqy.isTrue()
		|| isGT(subfileLineCount, 18))) {
			writeClientDetails1100();
		}
		
		/* MOVE 1                      TO SCRN-SUBFILE-RRN.*/
		if (notEofClntqy.isTrue()) {
			/*      MOVE 'Y'               TO SCRN-SUBFILE-MORE.            */
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set("N");
		}
	}

protected void writeClientDetails1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1100();
				case readNextClntqy1110: 
					readNextClntqy1110();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1100()
	{
		wsaaLastSurname.set(clntqyRec.surname);
		wsaaGivname.set(clntqyRec.givname);
		wsaaCltpcode.set(clntqyRec.cltpcode);
		wsaaClrrrole.set(clntqyRec.clrrrole);
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.clnt);
		sdasancrec.entycoy.set(wsspcomn.fsuco);
		sdasancrec.entynum.set(clntqyRec.clntnum);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			goTo(GotoLabel.readNextClntqy1110);
		}
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		wsaaSurname.set(clntqyRec.surname);
		wsaaAddr01.set(clntqyRec.cltaddr01);
		wsaaCltpcode.set(clntqyRec.cltpcode);
		wsaaClntnum.set(clntqyRec.clntnum);
		sv.clntdets.set(wsaaClntdets1);
		wsaaLastSurname.set(clntqyRec.surname);
		/*    Write the first subfile record of the pair.*/
		/*scrnparams.function.set(varcom.sadd);
		processScreen("S6361", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		subfileLineCount.add(1);*/
		wsaaGivname.set(clntqyRec.givname);
		wsaaAddr02.set(clntqyRec.cltaddr02);
		if (isNE(clntqyRec.cltdob, varcom.vrcmMaxDate)
		&& isNE(clntqyRec.cltdob, ZERO)) {
			wsaaInDate9.set(clntqyRec.cltdob);
			wsaaOutDay.set(wsaaInDay);
			wsaaOutMonth.set(wsaaInMonth);
			wsaaOutYear.set(wsaaInYear);
			wsaaDob.set(wsaaOutDate);
		}
		wsaaSex.set(clntqyRec.cltsex);
		wsaaClrrrole.set(clntqyRec.clrrrole);
		sv.clntdetsEx.set(wsaaClntdets2);
		/*sv.selectOut[varcom.pr.toInt()].set("Y");*/
		/*    Write the second subfile record of the pair.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6361", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		subfileLineCount.add(1);
	}

protected void readNextClntqy1110()
	{
		clntqy.read(clntqyRec);
		if (clntqy.isAtEnd()) {
			clntqyEofFlag.set("Y");
			return ;
		}
		/* IF   SURNAME                 = WSAA-SURNAME  AND             */
		/*      GIVNAME                 = WSAA-GIVNAME  AND             */
		/*      CLTPCODE                = WSAA-CLTPCODE                 */
		/*      GO TO 1110-READ-NEXT-CLNTQY.                            */
		if (isEQ(clntqyRec.surname, wsaaLastSurname)
		&& isEQ(clntqyRec.givname, wsaaGivname)
		&& isEQ(clntqyRec.cltpcode, wsaaCltpcode)
		&& isEQ(clntqyRec.clrrrole, wsaaClrrrole)) {
			goTo(GotoLabel.readNextClntqy1110);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		screenIo12010();
	}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo12010()
	{
		/*    CALL 'S6361IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6361-DATA-AREA                         */
		/*                         S6361-SUBFILE-AREA.                     */
		if (isEQ(scrnparams.statuz, "SUBM")) {
			clntqy.close();
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		/*    If ROLLUP has been requested read more records.*/
		if (isNE(scrnparams.statuz, "ROLU")) {
			clntqy.close();
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		/*  MOVE SCRN-SUBFILE-RRN       TO WSAA-SUBFILE-RRN*/
		subfileLineCount.set(1);
		while ( !(eofClntqy.isTrue()
		|| isGT(subfileLineCount, 18))) {
			writeClientDetails1100();
		}
		
		/*  MOVE WSAA-SUBFILE-RRN       TO SCRN-SUBFILE-RRN.*/
		if (notEofClntqy.isTrue()) {
			/*      MOVE 'Y'               TO SCRN-SUBFILE-MORE.            */
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set("N");
		}
		wsspcomn.edterror.set("Y");
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		para4000();
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*    Release any CLTS record that may have been stored.*/
		cltsIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			sv.select.set(SPACES);
		}
		/*    Read the next changed subfile record.*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.srnch);
		while ( !(isNE(sv.select, SPACES)
		|| isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile4100();
		}
		
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*    Read and store the selected CLTS record.*/
		wsaaClntdets1.set(sv.clntdets);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(wsaaClntnum);
		cltsIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		/*    Pass the client to wssp as FAS programs do not do            */
		/*    retrieves yet!                                               */
		wsaaCltskey.set(SPACES);
		wsaaCltskey.cltsClntpfx.set("CN");
		wsaaCltskey.cltsClntcoy.set(wsspcomn.fsuco);
		wsaaCltskey.cltsClntnum.set(cltsIO.getClntnum());
		wsspcomn.clntkey.set(wsaaCltskey);
		if (isEQ(wsspcomn.sbmaction, "B")) {
			if (isEQ(cltsIO.getClttype(), "C")) {
				wsspcomn.programPtr.add(2);
				return ;
			}
			else {
				wsspcomn.programPtr.add(1);
				wsspcomn.secProg[add(wsspcomn.programPtr, 1).toInt()].set(SPACES);
			}
		}
		if (isEQ(wsspcomn.sbmaction, "A")) {
			wsspcomn.programPtr.add(1);
		}
	}

protected void readSubfile4100()
	{
		/*READ*/
		processScreen("S6361", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
