package com.csc.life.enquiries.dataaccess;

import com.csc.fsu.general.dataaccess.AcmvpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AcmvsacTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:29
 * Class transformed from ACMVSAC.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AcmvsacTableDAM extends AcmvpfTableDAM {

	public AcmvsacTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ACMVSAC");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "RLDGCOY"
		             + ", SACSCODE"
		             + ", RLDGACCT"
		             + ", SACSTYP"
		             + ", ORIGCURR";
		
		QUALIFIEDCOLUMNS = 
		            "RLDGCOY, " +
		            "SACSCODE, " +
		            "RLDGACCT, " +
		            "SACSTYP, " +
		            "ORIGCURR, " +
		            "EFFDATE, " +
		            "FRCDATE, " +
		            "TRANNO, " +
		            "GLCODE, " +
		            "ORIGAMT, " +
		            "ACCTAMT, " +
		            "GENLCUR, " +
		            "GLSIGN, " +
		            "RDOCPFX, " +
		            "RDOCCOY, " +
		            "RDOCNUM, " +
		            "JRNSEQ, " +
		            "RLDGPFX, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "TRANREF, " +
		            "TRANDESC, " +
		            "CRATE, " +
		            "GENLCOY, " +
		            "POSTYEAR, " +
		            "POSTMONTH, " +
		            "DATETO, " +
		            "RCAMT, " +
		            "INTEXTIND, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "TERMID, " +
		            "RECONSBR, " +
		            "SUPRFLG, " +
		            "TAXCODE, " +
		            "RECONREF, " +
		            "STMTSORT, " +
		            "CREDDTE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "RLDGCOY ASC, " +
		            "SACSCODE ASC, " +
		            "RLDGACCT ASC, " +
		            "SACSTYP ASC, " +
		            "ORIGCURR ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "RLDGCOY DESC, " +
		            "SACSCODE DESC, " +
		            "RLDGACCT DESC, " +
		            "SACSTYP DESC, " +
		            "ORIGCURR DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               rldgcoy,
                               sacscode,
                               rldgacct,
                               sacstyp,
                               origcurr,
                               effdate,
                               frcdate,
                               tranno,
                               glcode,
                               origamt,
                               acctamt,
                               genlcur,
                               glsign,
                               rdocpfx,
                               rdoccoy,
                               rdocnum,
                               jrnseq,
                               rldgpfx,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               tranref,
                               trandesc,
                               crate,
                               genlcoy,
                               postyear,
                               postmonth,
                               dateto,
                               rcamt,
                               intextind,
                               transactionDate,
                               transactionTime,
                               user,
                               termid,
                               reconsbr,
                               suprflg,
                               taxcode,
                               reconref,
                               stmtsort,
                               creddte,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(232);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getAcmvrecKeyData() {
		return getRecKeyData();
	}
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getRldgcoy().toInternal()
					+ getSacscode().toInternal()
					+ getRldgacct().toInternal()
					+ getSacstyp().toInternal()
					+ getOrigcurr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setAcmvrecKeyData(Object obj) {
		return setRecKeyData(obj);
	}
	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, rldgcoy);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, rldgacct);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, origcurr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(16);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(rldgcoy.toInternal());
	nonKeyFiller20.setInternal(sacscode.toInternal());
	nonKeyFiller30.setInternal(rldgacct.toInternal());
	nonKeyFiller40.setInternal(sacstyp.toInternal());
	nonKeyFiller50.setInternal(origcurr.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getAcmvrecNonKeyData() {
		return getRecNonKeyData();
	}
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(303);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getEffdate().toInternal()
					+ getFrcdate().toInternal()
					+ getTranno().toInternal()
					+ getGlcode().toInternal()
					+ getOrigamt().toInternal()
					+ getAcctamt().toInternal()
					+ getGenlcur().toInternal()
					+ getGlsign().toInternal()
					+ getRdocpfx().toInternal()
					+ getRdoccoy().toInternal()
					+ getRdocnum().toInternal()
					+ getJrnseq().toInternal()
					+ getRldgpfx().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getTranref().toInternal()
					+ getTrandesc().toInternal()
					+ getCrate().toInternal()
					+ getGenlcoy().toInternal()
					+ getPostyear().toInternal()
					+ getPostmonth().toInternal()
					+ getDateto().toInternal()
					+ getRcamt().toInternal()
					+ getIntextind().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getTermid().toInternal()
					+ getReconsbr().toInternal()
					+ getSuprflg().toInternal()
					+ getTaxcode().toInternal()
					+ getReconref().toInternal()
					+ getStmtsort().toInternal()
					+ getCreddte().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setAcmvrecNonKeyData(Object obj) {
		return setRecNonKeyData(obj);
	}
	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, frcdate);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, glcode);
			what = ExternalData.chop(what, origamt);
			what = ExternalData.chop(what, acctamt);
			what = ExternalData.chop(what, genlcur);
			what = ExternalData.chop(what, glsign);
			what = ExternalData.chop(what, rdocpfx);
			what = ExternalData.chop(what, rdoccoy);
			what = ExternalData.chop(what, rdocnum);
			what = ExternalData.chop(what, jrnseq);
			what = ExternalData.chop(what, rldgpfx);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, tranref);
			what = ExternalData.chop(what, trandesc);
			what = ExternalData.chop(what, crate);
			what = ExternalData.chop(what, genlcoy);
			what = ExternalData.chop(what, postyear);
			what = ExternalData.chop(what, postmonth);
			what = ExternalData.chop(what, dateto);
			what = ExternalData.chop(what, rcamt);
			what = ExternalData.chop(what, intextind);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, reconsbr);
			what = ExternalData.chop(what, suprflg);
			what = ExternalData.chop(what, taxcode);
			what = ExternalData.chop(what, reconref);
			what = ExternalData.chop(what, stmtsort);
			what = ExternalData.chop(what, creddte);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(Object what) {
		rldgcoy.set(what);
	}
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}
	public FixedLengthStringData getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(Object what) {
		rldgacct.set(what);
	}
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}
	public FixedLengthStringData getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(Object what) {
		origcurr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(Object what) {
		setFrcdate(what, false);
	}
	public void setFrcdate(Object what, boolean rounded) {
		if (rounded)
			frcdate.setRounded(what);
		else
			frcdate.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getGlcode() {
		return glcode;
	}
	public void setGlcode(Object what) {
		glcode.set(what);
	}	
	public PackedDecimalData getOrigamt() {
		return origamt;
	}
	public void setOrigamt(Object what) {
		setOrigamt(what, false);
	}
	public void setOrigamt(Object what, boolean rounded) {
		if (rounded)
			origamt.setRounded(what);
		else
			origamt.set(what);
	}	
	public PackedDecimalData getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(Object what) {
		setAcctamt(what, false);
	}
	public void setAcctamt(Object what, boolean rounded) {
		if (rounded)
			acctamt.setRounded(what);
		else
			acctamt.set(what);
	}	
	public FixedLengthStringData getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(Object what) {
		genlcur.set(what);
	}	
	public FixedLengthStringData getGlsign() {
		return glsign;
	}
	public void setGlsign(Object what) {
		glsign.set(what);
	}	
	public FixedLengthStringData getRdocpfx() {
		return rdocpfx;
	}
	public void setRdocpfx(Object what) {
		rdocpfx.set(what);
	}	
	public FixedLengthStringData getRdoccoy() {
		return rdoccoy;
	}
	public void setRdoccoy(Object what) {
		rdoccoy.set(what);
	}	
	public FixedLengthStringData getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(Object what) {
		rdocnum.set(what);
	}	
	public PackedDecimalData getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(Object what) {
		setJrnseq(what, false);
	}
	public void setJrnseq(Object what, boolean rounded) {
		if (rounded)
			jrnseq.setRounded(what);
		else
			jrnseq.set(what);
	}	
	public FixedLengthStringData getRldgpfx() {
		return rldgpfx;
	}
	public void setRldgpfx(Object what) {
		rldgpfx.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public FixedLengthStringData getTranref() {
		return tranref;
	}
	public void setTranref(Object what) {
		tranref.set(what);
	}	
	public FixedLengthStringData getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(Object what) {
		trandesc.set(what);
	}	
	public PackedDecimalData getCrate() {
		return crate;
	}
	public void setCrate(Object what) {
		setCrate(what, false);
	}
	public void setCrate(Object what, boolean rounded) {
		if (rounded)
			crate.setRounded(what);
		else
			crate.set(what);
	}	
	public FixedLengthStringData getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(Object what) {
		genlcoy.set(what);
	}	
	public FixedLengthStringData getPostyear() {
		return postyear;
	}
	public void setPostyear(Object what) {
		postyear.set(what);
	}	
	public FixedLengthStringData getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(Object what) {
		postmonth.set(what);
	}	
	public PackedDecimalData getDateto() {
		return dateto;
	}
	public void setDateto(Object what) {
		setDateto(what, false);
	}
	public void setDateto(Object what, boolean rounded) {
		if (rounded)
			dateto.setRounded(what);
		else
			dateto.set(what);
	}	
	public PackedDecimalData getRcamt() {
		return rcamt;
	}
	public void setRcamt(Object what) {
		setRcamt(what, false);
	}
	public void setRcamt(Object what, boolean rounded) {
		if (rounded)
			rcamt.setRounded(what);
		else
			rcamt.set(what);
	}	
	public FixedLengthStringData getIntextind() {
		return intextind;
	}
	public void setIntextind(Object what) {
		intextind.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public FixedLengthStringData getReconsbr() {
		return reconsbr;
	}
	public void setReconsbr(Object what) {
		reconsbr.set(what);
	}	
	public FixedLengthStringData getSuprflg() {
		return suprflg;
	}
	public void setSuprflg(Object what) {
		suprflg.set(what);
	}	
	public FixedLengthStringData getTaxcode() {
		return taxcode;
	}
	public void setTaxcode(Object what) {
		taxcode.set(what);
	}	
	public PackedDecimalData getReconref() {
		return reconref;
	}
	public void setReconref(Object what) {
		setReconref(what, false);
	}
	public void setReconref(Object what, boolean rounded) {
		if (rounded)
			reconref.setRounded(what);
		else
			reconref.set(what);
	}	
	public FixedLengthStringData getStmtsort() {
		return stmtsort;
	}
	public void setStmtsort(Object what) {
		stmtsort.set(what);
	}	
	public PackedDecimalData getCreddte() {
		return creddte;
	}
	public void setCreddte(Object what) {
		setCreddte(what, false);
	}
	public void setCreddte(Object what, boolean rounded) {
		if (rounded)
			creddte.setRounded(what);
		else
			creddte.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		rldgcoy.clear();
		sacscode.clear();
		rldgacct.clear();
		sacstyp.clear();
		origcurr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		effdate.clear();
		frcdate.clear();
		tranno.clear();
		glcode.clear();
		origamt.clear();
		acctamt.clear();
		genlcur.clear();
		glsign.clear();
		rdocpfx.clear();
		rdoccoy.clear();
		rdocnum.clear();
		jrnseq.clear();
		rldgpfx.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		tranref.clear();
		trandesc.clear();
		crate.clear();
		genlcoy.clear();
		postyear.clear();
		postmonth.clear();
		dateto.clear();
		rcamt.clear();
		intextind.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		termid.clear();
		reconsbr.clear();
		suprflg.clear();
		taxcode.clear();
		reconref.clear();
		stmtsort.clear();
		creddte.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}