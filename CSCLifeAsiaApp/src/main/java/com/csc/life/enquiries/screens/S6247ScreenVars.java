package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6247
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6247ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(441);
	public FixedLengthStringData dataFields = new FixedLengthStringData(233).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,229);
	public FixedLengthStringData actionflag =DD.action.copy().isAPartOf(dataFields,232);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 233);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 285);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(272+2);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(78+2).isAPartOf(subfileArea, 0);
	public ZonedDecimalData bnypc = DD.bnypc.copyToZonedDecimal().isAPartOf(subfileFields,0);
	//smalchi2 for ILIFE-597
	public FixedLengthStringData bnyrlndesc = DD.bnyrlndescnew.copy().isAPartOf(subfileFields,5);
	public FixedLengthStringData bnytype = DD.bnytype.copy().isAPartOf(subfileFields,15);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData clntsname = DD.clntsname.copy().isAPartOf(subfileFields,25);
	public FixedLengthStringData cltreln = DD.cltreln.copy().isAPartOf(subfileFields,55);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,59);
	public FixedLengthStringData relto = DD.relto.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData revcflg = DD.revcflg.copy().isAPartOf(subfileFields,68);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,69);
	public ZonedDecimalData enddate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,70);/*ILIFE-3581*/
	public FixedLengthStringData sequence = new FixedLengthStringData(2).isAPartOf(subfileFields,70+enddate.getLength());
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 78+sequence.length());
	public FixedLengthStringData bnypcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData bnyrlndescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData bnytypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData clntsnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData cltrelnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData reltoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData revcflgErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData enddateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);/*ILIFE-3581*/
	public FixedLengthStringData sequenceErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 126);
	public FixedLengthStringData[] bnypcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] bnyrlndescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] bnytypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] clntsnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] cltrelnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] reltoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] revcflgOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] enddateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);/*ILIFE-3581*/
	public FixedLengthStringData[] sequenceOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 270);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData enddateDisp = new FixedLengthStringData(10);
	public LongData S6247screensflWritten = new LongData(0);
	public LongData S6247screenctlWritten = new LongData(0);
	public LongData S6247screenWritten = new LongData(0);
	public LongData S6247protectWritten = new LongData(0);
	public GeneralTable s6247screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6247screensfl;
	}

	public S6247ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {clntnum, select, effdate, clntsname, bnyrlndesc, bnypc, cltreln, bnytype, relto, revcflg, enddate, sequence};
		screenSflOutFields = new BaseData[][] {clntnumOut, selectOut, effdateOut, clntsnameOut, bnyrlndescOut, bnypcOut, cltrelnOut, bnytypeOut, reltoOut, revcflgOut, enddateOut,sequenceOut};
		screenSflErrFields = new BaseData[] {clntnumErr, selectErr, effdateErr, clntsnameErr, bnyrlndescErr, bnypcErr, cltrelnErr, bnytypeErr, reltoErr, revcflgErr, enddateErr,sequenceErr};
		screenSflDateFields = new BaseData[] {effdate, enddate};
		screenSflDateErrFields = new BaseData[] {effdateErr,enddateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp,enddateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, cownnum, ownername};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, cownnumOut, ownernameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, cownnumErr, ownernameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6247screen.class;
		screenSflRecord = S6247screensfl.class;
		screenCtlRecord = S6247screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6247protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6247screenctl.lrec.pageSubfile);
	}
}
