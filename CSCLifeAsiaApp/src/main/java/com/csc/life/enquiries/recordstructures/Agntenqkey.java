package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:08
 * Description:
 * Copybook name: AGNTENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agntenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agntenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData agntenqKey = new FixedLengthStringData(256).isAPartOf(agntenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData agntenqAgntcoy = new FixedLengthStringData(1).isAPartOf(agntenqKey, 0);
  	public FixedLengthStringData agntenqAgntnum = new FixedLengthStringData(8).isAPartOf(agntenqKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(agntenqKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agntenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agntenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}