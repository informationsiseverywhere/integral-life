/*
 * File: P6353.java
 * Date: 30 August 2009 0:45:00
 * Author: Quipoz Limited
 * 
 * Class transformed from P6353.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved..
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//ILIFE-7062 - Start		
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClftpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.dao.impl.ClftpfDAOImpl;
import com.csc.fsu.clients.dataaccess.model.Clftpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.SchmpfDAO;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Schmpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.dao.LetcutipfDAO;
import com.csc.fsu.printing.dataaccess.model.Letcutipf;
import com.csc.fsu.printing.procedures.Xmlsweep;
import com.csc.fsu.printing.recordstructures.Xmlswprec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.dao.AsgnpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Asgnpf;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
//import com.csc.life.enquiries.dataaccess.AsgnenqTableDAM; //ILB-498
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.dao.AgntenqpfDAO;
import com.csc.life.enquiries.dataaccess.dao.ItdmpfDAO;
import com.csc.life.enquiries.dataaccess.model.Agntenqpf;
import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.life.enquiries.screens.S6353ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.BnkoutpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnkoutpf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
//import com.csc.smart.dataaccess.ItemTableDAM; //ILB-498
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
//import com.csc.fsu.general.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
//ILIFE-7062 - End		
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Version #2 - Incorporating Plan Processing.
* This program is the main screen for the contract enquiry.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*     The details of the contract being enqired upon will be stored
*     in  the  CHDR I/O module.  Retrieve the details, then release
*     the  CHDR I/O module with a RLSE function and perform a READS
*     on  CHDRENQ so that the appropriate details are held for this
*     sub-system.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist.
*
*
*          The agent (AGNT) to get the client number, and hence the
*          client (agent's) name.
*
*
* Validation
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*          If  the  'KILL'  function  key  was pressed skip all the
*          validation.
*
*     Validation  is  only  required  against the indicators at the
*     foot  of  the  screen. The only valid entries in those are an
*     'X'.
*
*     If  the  Plan Level Component indicator has been selected and
*     the number of policies in plan, (from the Contract Header) is
*     1 then highlight the indicator as being in error.
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Check  each  'selection'  indicator in turn.  If an option is
*     selected,  call  GENSSWCH  with  the  appropriate  action  to
*     retrieve the optional program switching:
*
*               A - Outstanding Future transactions
*               B - Plan Components
*               C - Policy Components
*               D - Roles
*               E - Sub-Account Balances
*               F - Transactions History
*               G - Agents Details
*               H - Extra Header Details
*
*     For the first one selected, save the next set of programs (8)
*     from  the  program  stack  and flag the current position with
*     '*'.
*
*     If  a  selection  had  been made previously, its select field
*     would contain a '?'. Replace this '?' with a '+'.
*
*     If  a selection is made (selection field X), load the program
*     stack with the programs returned by GENSSWCH, replace the 'X'
*     in  the  selection field with a '?', add 1 to the pointer and
*     exit.
*
*     If   Plan   Components   has   been   selected  move  'L'  to
*     WSSP-PLAN-POLICY, if Policy Components has been selected move
*     'O'   to  WSSP-PLAN-POLICY.  For  all  other  selections  set
*     WSSP-PLAN-POLICY to space.
*
*     Once all the selections have been serviced, re-load the saved
*     programs  onto  the  stack,  and blank out the '*'. Leave the
*     program  pointer  at  the current position (to re-display the
*     screen).
*
*     If  nothing was selected blank out the stack action field and
*     leave the program pointer at the current position.
*
*
*
* Notes.
* ------
*
*
*     Create  a  new  view of CHDRPF called CHDRENQ which uses only
*     those  fields  required  for this program P6353 and the extra
*     contract details program - P6354.
*
*     Table Used:
*
* T1675 - Generalised Secondary Switching   Key: Transaction Code
*                                              + Program Number
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
*
*****************************************************************
* </pre>
*/
public class P6353 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6353");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaPlanProc = new FixedLengthStringData(1);
	private Validator plan = new Validator(wsaaPlanProc, "Y");
	private Validator nonplan = new Validator(wsaaPlanProc, "N");

	private FixedLengthStringData wsaaWsspChdrky = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaWsspChdrpfx = new FixedLengthStringData(2).isAPartOf(wsaaWsspChdrky, 0);
	private FixedLengthStringData wsaaWsspChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaWsspChdrky, 2);
	private FixedLengthStringData wsaaWsspChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaWsspChdrky, 3);

		/*Fields for use with the CLRF logical.                            */
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private PackedDecimalData wsaaTotTax = new PackedDecimalData(17, 2); //IBPLIFE-4293
	private ZonedDecimalData wsaaTr517Ix = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaTr517Item = new FixedLengthStringData(8);
	private String wsaaWaiveTax = "";

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaEscKey = new FixedLengthStringData(1);
	private Validator escapeToPrevMenu = new Validator(wsaaEscKey, "Y");
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private static final String e026 = "E026";
	private static final String g620 = "G620";
	private static final String h093 = "H093";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String t5604 = "T5604";
	private static final String tr517 = "TR517";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private Agntenqpf agntenqpf = new Agntenqpf(); 
	private AgntenqpfDAO agntenqpfDAO = getApplicationContext().getBean("agntenqpfDAO", AgntenqpfDAO.class);
	private List<Agntenqpf> agntenqpfList = null;
	private Clrfpf clrfpf =  new Clrfpf();
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private List<Clrfpf> clrfpfList = null;
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private List<Clntpf> clntpfList = null;
	private Covrpf covrpf = new Covrpf();
	private Covrpf coveragepf = new Covrpf();
	private List<Covrpf> covrpfList = null;
//	private DescTableDAM descIO = new DescTableDAM();
	//protected ItemTableDAM itemIO = new ItemTableDAM();//ILB-498
	private LetcTableDAM letcIO = new LetcTableDAM();
	//private AsgnenqTableDAM asgnenqIO = new AsgnenqTableDAM(); //ILB-498
	private AsgnpfDAO asgnpfDAO = getApplicationContext().getBean("asgnpfDAOP5079", AsgnpfDAO.class);
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Xmlswprec xmlswprec = new Xmlswprec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Tr517rec tr517rec = new Tr517rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Wssplife wssplife = new Wssplife();
	private S6353ScreenVars sv = getPScreenVars();
	protected FormatsInner formatsInner = new FormatsInner();
	private boolean ctaxPermission=false; 
	private boolean multOwnerFlag=false;
	private SchmpfDAO schmpfDAO = getApplicationContext().getBean("schmpfDAO", SchmpfDAO.class);
	private Schmpf schmpf=null;
	
	protected Chdrpf chdrpf = new Chdrpf();
	protected Chdrpf chdrpfObj = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Letcutipf letcutipf = new Letcutipf();
	private LetcutipfDAO letcutipfDAO = getApplicationContext().getBean("letcutipfDAO", LetcutipfDAO.class);
	private List<Letcutipf> letcutipfList = null;
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	protected Payrpf payrpf = new Payrpf();
	private PayrpfDAO payrpfDAO = getBeanPayerpf();
	private List<Payrpf> payrpfList= null;
	private TaxdpfDAO taxdbilpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private List<Taxdpf> taxdbilpfList = null;
	private Bnkoutpf bnkoutpf = new Bnkoutpf();
	private BnkoutpfDAO bnkoutpfDAO = getApplicationContext().getBean("bnkoutpfDAO" , BnkoutpfDAO.class);
	/*
	 * ILIFE-3149 Stamp Duty- start
	 */
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<BigDecimal>  totList = new ArrayList<BigDecimal>();
	private boolean stampDutyflag = false;

	private Itdmpf itdmpf = new Itdmpf();
	private ItdmpfDAO itdmpfDAO = getApplicationContext().getBean("itdmpfDAO", ItdmpfDAO.class);
	private List<Itdmpf> itdmpfList = null;

	private ClftpfDAO clftpfdao=new ClftpfDAOImpl();
	private boolean isFatcaAllowed=false;
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	private Map<String,Descpf> Fatcastatus;
	private Th506rec th506rec = new Th506rec();
	private boolean superprod=true;
	private Itempf itempf = new Itempf();
	private List<Itempf> itemlist = new ArrayList<Itempf>();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	/*
	 * ILIFE-3149 Stamp Duty-end
	 */
	private boolean isbillday=false;
	private boolean isFeatureConfig=false;
	//ILIFE-5975
	protected LinspfDAO linspfdao= getApplicationContext().getBean("linspfDAO",LinspfDAO.class);

	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	private boolean isClntAccRiskAmt=false;
	private boolean isAgentMainteance=false;

	//ILIFE-7062 - Start
	protected AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); 
	protected Acblpf acblpf;
	protected static final String SACSCODE = "LP";
	private static final String SACSTYPE = "FE";
	//ILIFE-7062 - End
	//ILJ-45 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-45 End
	
	//ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	//ILJ-387 end
	private FixedLengthStringData longconfname1 = new FixedLengthStringData(122);
	
	//PINNACLE-2753
	private boolean isAiaAusDirectDebit;
	protected Bextpf bextpf = new Bextpf();
	private BextpfDAO bextpfDAO = getBeanBextpf();
	private List<Bextpf> bextpfList= null;
	
	private FixedLengthStringData wsaaInvalidStatus = new FixedLengthStringData(2);
	private Validator invalidStatus = new Validator(wsaaInvalidStatus, "LA");
	
	private FixedLengthStringData wsaaInvalidPremStatus = new FixedLengthStringData(2);
	private Validator invalidPremStatus = new Validator(wsaaInvalidPremStatus, "LA");
	
	private Datcon2rec datcon2rec = new Datcon2rec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		jointOwnerName1030, 
		payerName1040, 
		agentName1050, 
		exit1090, 
		addTax1100, 
		exit1100, 
		exit1110, 
		exit2090, 
		gensww4010, 
		exit4090
	}
	
	public P6353() {
		super();
		screenVars = sv;
		new ScreenModel("S6353", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}
	
protected S6353ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars(S6353ScreenVars.class);
	}

protected PayrpfDAO getBeanPayerpf() {
		return getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	}

//PINNACLE-2753
protected BextpfDAO getBeanBextpf() {
	return getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010(); 
	}

protected void initialise1010()
	{
	    isAiaAusDirectDebit = FeaConfg.isFeatureExist("2", "BTPRO029", appVars, "IT");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.btdate.set(ZERO);
		sv.currfrom.set(ZERO);
		sv.instpramt.set(ZERO);
		sv.nextinsamt.set(ZERO);
		sv.numpols.set(ZERO);
		sv.hpropdte.set(ZERO);
		sv.hprrcvdt.set(ZERO);
		sv.huwdcdte.set(ZERO);
		sv.hissdte.set(ZERO);
		sv.hoissdte.set(ZERO);
		sv.ptdate.set(ZERO);
		sv.zstpduty01.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.cntfee.set(ZERO); //ILIFE-7062
		sv.nlgflg.set(SPACES); //ILIFE-3997
		//ILJ-45 Starts
		sv.riskcommdte.set(ZERO);
		sv.decldte.set(ZERO);
		sv.fstprmrcptdte.set(ZERO);		
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{			
			sv.riskcommdteOut[varcom.nd.toInt()].set("Y");
			sv.decldteOut[varcom.nd.toInt()].set("Y");
			sv.fstprmrcptdteOut[varcom.nd.toInt()].set("Y");			
		}		
		//ILJ-45 End	
		
		//ILJ-387 Start
		boolean cntEnqFlag = false;
		cntEnqFlag  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if(cntEnqFlag)	{			
			sv.jownerOut[Varcom.nd.toInt()].set("Y");
			sv.jlifenumOut[Varcom.nd.toInt()].set("Y");
			sv.reqntypeOut[Varcom.nd.toInt()].set("Y");
			sv.indxflgOut[Varcom.nd.toInt()].set("Y");
			sv.lastinsdteOut[Varcom.nd.toInt()].set("Y");
			sv.lastinsamtOut[Varcom.nd.toInt()].set("Y");
		}
		//ILJ-387 End
		isFeatureConfig  = FeaConfg.isFeatureExist(wsspcomn.company.toString(),"CTENQ007", appVars, "IT");
		 
		if(isFeatureConfig)
		{
			sv.action.set("Y");
		}
		else
		{
			sv.action.set("N");
		}
		
		initCustomerSpecific1();
		/* Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		isbillday = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP011", appVars, "IT");
		if(!isbillday){
			sv.billdayOut[varcom.nd.toInt()].set("Y");
			}
		
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		//ICIL-12 start
		isAgentMainteance = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "AGMTN015", appVars, "IT");
		if(!isAgentMainteance){
			sv.bnkoutOut[varcom.nd.toInt()].set("Y");
			sv.bnksmOut[varcom.nd.toInt()].set("Y");
			sv.bnktelOut[varcom.nd.toInt()].set("Y");			
			
		}else{
			sv.bnkoutOut[varcom.nd.toInt()].set("N");
			sv.bnksmOut[varcom.nd.toInt()].set("N");
			sv.bnktelOut[varcom.nd.toInt()].set("N");			
		}
		//ICIL-12 end
		// ILIFE-3755 begin
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf){
			chdrenqIO.setFunction(Varcom.retrv);
			chdrenqIO.setFormat(formatsInner.chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), Varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrenqIO.getChdrcoy().toString(), chdrenqIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		// ILIFE-3755 end
		if ((chdrpf.getPolinc()!=null) && ((chdrpf.getPolinc()==0) || (chdrpf.getPolinc()==1))) {
					wsaaPlanProc.set("N");
				}
				else {
					wsaaPlanProc.set("Y");
				}
		if ((chdrpf.getPolinc()!=null) && plan.isTrue()) {
			sv.numpols.set(chdrpf.getPolinc());
		}
		else {
			sv.numpolsOut[varcom.pr.toInt()].set("Y");
			sv.numpolsOut[varcom.nd.toInt()].set("Y");
			sv.polcompOut[varcom.pr.toInt()].set("Y");
			sv.polcompOut[varcom.nd.toInt()].set("Y");
		}
		/*    Check if any letters exist for the contract.                 */
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		letcutipf.setRdocpfx("CH");
		letcutipf.setRdoccoy(chdrpf.getChdrcoy().toString());
		letcutipf.setRdocnum(chdrpf.getChdrnum());
		letcutipf.setTranno(99999);
		letcutipf.setLetseqno(0);
		letcutipfList= letcutipfDAO.readData(letcutipf);
		if(letcutipfList==null || (letcutipfList!=null && letcutipfList.size()==0))
			{
				sv.outind.set(SPACES);
				sv.outindOut[varcom.pr.toInt()].set("Y");
				sv.outindOut[varcom.nd.toInt()].set("Y");
			}
		/*  Read Anniversary Rules to check if product exists. If yes      */
		/*  Anniversary window is displayed.                               */
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		itdmpf.setItemcoy(chdrpf.getChdrcoy().toString());
		itdmpf.setItemtabl(t5604);
		itdmpf.setItemitem(chdrpf.getCnttype());
		itdmpf.setItmfrm(chdrpf.getOccdate());
		itdmpfList= itdmpfDAO.readItdmData(itdmpf);
		if(itdmpfList ==null || (itdmpfList!=null && itdmpfList.size()==0))
		{
			sv.aiind.set(SPACES);
			sv.aiindOut[varcom.pr.toInt()].set("Y");
			sv.aiindOut[varcom.nd.toInt()].set("Y");
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.cownnum.set(chdrpf.getCownnum());
		sv.reqntype.set(chdrpf.getReqntype()); //ILIFE-2472
		if (chdrpf.getJownnum() != null){     //ILIFE-5120
			if(isNE(chdrpf.getJownnum(), SPACES) ) {
			sv.jowner.set(chdrpf.getJownnum());
		}
			}
		/*IF   CHDRENQ-PAYRNUM        NOT = SPACES                     */
		/*     MOVE CHDRENQ-PAYRNUM   TO S6353-PAYER.                  */
		sv.servagnt.set(chdrpf.getAgntnum());
		sv.servbr.set(chdrpf.getCntbranch());
		sv.currfrom.set(chdrpf.getOccdate());
		/*MOVE CHDRENQ-PTDATE         TO S6353-PTDATE.                 */
		/*MOVE CHDRENQ-BTDATE         TO S6353-BTDATE.                 */
		/*MOVE CHDRENQ-BILLCHNL       TO S6353-MOP.                    */
		/*MOVE CHDRENQ-BILLFREQ       TO S6353-PAYFREQ.                */
		if ( chdrpf.getInstfrom()!=null)
		{
			sv.lastinsdte.set(chdrpf.getInstfrom());
		}
		
		if(chdrpf.getIsam06()!=null){
		sv.instpramt.set(chdrpf.getIsam06());
		}
		/*MOVE CHDRENQ-BILLCD         TO S6353-NEXTINSDTE.        <007>*/
		/*MOVE CHDRENQ-SINSTAMT06     TO S6353-NEXTINSAMT.             */
		/* Read the PAYR file for billing information.                     */
		payrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		payrpf.setChdrnum(chdrpf.getChdrnum());
		payrpf.setValidflag("1");
		payrpf.setPayrseqno(1);
		payrpfList = payrpfDAO.readPayrData(payrpf);
		if(payrpfList==null || (payrpfList!=null && payrpfList.size()==0))
			{
				return;
			}
		payrpf = payrpfList.get(0);
		sv.ptdate.set(payrpf.getPtdate());
		sv.btdate.set(payrpf.getBtdate());
		sv.mop.set(payrpf.getBillchnl());
		sv.payfreq.set(payrpf.getBillfreq());
		
		//PINNACLE-2753
		if (!isAiaAusDirectDebit) {
			sv.nextinsdte.set(payrpf.getBillcd());
	    }else {
	    		bextpf.setChdrcoy(payrpf.getChdrcoy().toString());
	    		bextpf.setChdrnum(payrpf.getChdrnum());
	    		bextpfList = bextpfDAO.getBextpf(bextpf.getChdrcoy(),bextpf.getChdrnum());
	    		if(!bextpfList.isEmpty())
	    		{
	    			sv.nextinsdte.set(bextpfList.get(0).getBilldate());
	    		}
	    		else 
	    		{
	    			sv.nextinsdte.set(payrpf.getBillcd());
	    		}
	    	} 
		initCustomerSpecific2();
		/*
		 * sv.nextinsamt.set(payrpf.getSinstamt06()); if
		 * ((payrpf.getSinstamt05().compareTo(BigDecimal.ZERO) != 0) &&
		 * (isNE(chdrpf.getPtdate(), chdrpf.getBtdate()))) {
		 * sv.nextinsamt.add(payrpf.getSinstamt05().doubleValue()); }
		 */
		sv.billcurr.set(payrpf.getBillcurr());
		if(isbillday)//ILIFE-8583
			sv.billday.set(payrpf.getBillday());
		else
			sv.billday.set(SPACES);
		if (isEQ(sv.btdate, ZERO)) {
			sv.btdate.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.currfrom, ZERO)) {
			sv.currfrom.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.lastinsdte, ZERO)) {
			sv.lastinsdte.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.nextinsdte, ZERO)) {
			sv.nextinsdte.set(varcom.vrcmMaxDate);
		}
		if (isEQ(sv.ptdate, ZERO)) {
			sv.ptdate.set(varcom.vrcmMaxDate);
		}
		//ILJ-45 Starts
		if (isEQ(sv.fstprmrcptdte, ZERO)) {
			sv.fstprmrcptdte.set(varcom.vrcmMaxDate);
		}
		//ILJ-45 End
		//ILIFE-7062 - Start		
		initCustomerSpecific4();
		//ILIFE-7062 - End
		//ILIFE-5975
		checkLinsrnl5500();
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/

		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());
		lifepf.setChdrnum(chdrpf.getChdrnum());
		lifepf.setLife("01");
		lifepf.setJlife("00");
		lifepf.setValidflag("1");
		Map<String, Lifepf> lifepfMap =lifepfDAO.getLifeEnqData(lifepf);
		Lifepf lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim()+""+lifepf.getValidflag().trim());
		if(lifepfModel==null){
			return;
		}
		sv.lifenum.set(lifepfModel.getLifcnum());
		clntpf.setClntnum(lifepfModel.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpfList= clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
		{
			return;
		}
		for(Clntpf clntpf :clntpfList)
		{
			plainname(clntpf);
		}
		sv.lifename.set(longconfname1);
		lifepf.setJlife("01");
		lifepfMap =lifepfDAO.getLifeEnqData(lifepf);
		//lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim());
		lifepfModel= lifepfMap.get(lifepf.getChdrcoy().trim()+""+lifepf.getChdrnum().trim()+""+lifepf.getLife().trim()+""+lifepf.getJlife().trim()+""+lifepf.getValidflag().trim());
		if(lifepfModel==null){
		/*	MOVE 'NONE'            TO S6353-JLIFE                   
            S6353-JLIFENAME  */             
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
		}
		else {
			sv.jlife.set(lifepfModel.getLifcnum());
			clntpf.setClntnum(lifepfModel.getLifcnum());
			clntpf.setClntcoy(wsspcomn.fsuco.toString());
			clntpf.setClntpfx("CN");
			clntpfList= clntpfDAO.readClientpfData(clntpf);
				if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
				{
					return;
				}
				else{
					clntpf = clntpfList.get(0);
					plainname(clntpf);
					sv.jlifename.set(longconfname1);
			  }
		}
		isClntAccRiskAmt = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "UNWRT001", appVars, "IT");
		if(!isClntAccRiskAmt)
			sv.cntaccriskamtOut[varcom.nd.toInt()].set("Y");
		isFatcaAllowed= FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "CLMTN008", appVars, "IT");
		ownerName1020(clntpf);
		if(!isFatcaAllowed){
			sv.fatcastatusOut[varcom.pr.toInt()].set("Y");
		}
		ctaxPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPROP07", appVars, "IT"); //ALS-73
			if(ctaxPermission){
				chdrpf = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), sv.chdrnum.toString());
				itemlist = itemDAO.getAllItemitem("IT",wsspcomn.company.toString() ,"Tr59x", chdrpf.getCnttype());
			}
			if(itemlist.size()==0 || !ctaxPermission)
			{
				sv.zctaxindOut[varcom.nd.toInt()].set("Y");
				sv.schmnoOut[varcom.nd.toInt()].set("Y");
				sv.schmnmeOut[varcom.nd.toInt()].set("Y");	
				sv.cownnumOut[varcom.nd.toInt()].set("Y");	
				
				superprod=false;
			}
		if(!ctaxPermission||isNE(sv.mop,'S')) {
			sv.zroloverindOut[varcom.nd.toInt()].set("Y");
		}
		/*Obtain the scheme details*/
		multOwnerFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP078", appVars, "IT");
		
			if(superprod)
			{

				if(chdrpf.getSchmno()!=null && isNE(chdrpf.getSchmno(),SPACES))
				{
					schmpf=schmpfDAO.getSchmpfRecord(chdrpf.getSchmno());
					if(schmpf!=null)
					{
						sv.schmno.set(chdrpf.getSchmno());
						sv.schmnme.set(schmpf.getSchmNme());
					}
					else 
					{
						sv.schmno.set(SPACES);
						sv.schmnme.set(SPACES);
					}
							
				}
				else{
					sv.schmno.set(SPACES);
					sv.schmnme.set(SPACES);
				}
			sv.zmultOwnerOut[varcom.nd.toInt()].set("Y");//ILIFE-7277
			}
			else
			{
				if(!multOwnerFlag)
				{		
					sv.zmultOwnerOut[varcom.nd.toInt()].set("Y");//ILIFE-7277
				}
			}
		
		//ILIFE-3997 Starts
		sv.nlgflg.set(chdrpf.getNlgflg());
		sv.nlgflgOut[varcom.pr.toInt()].set("Y");
		//ILIFE-3997 End
		//ILIFE-4108 starts
		sv.indxflgOut[varcom.pr.toInt()].set("Y");
		getAutoIncreaseDetails1080();
		//ILIFE-4108 ends
		
		initCustomerSpecific3();
		
		if(isAgentMainteance){
			getBankDetails();	//ICIL-12
		}
		
		getLastInstallment();
	}

protected void getLastInstallment() {
	
	List<Linspf> list = linspfdao.getPaidLinspfRecordList(chdrpf.getChdrnum());
	if(list !=null && !list.isEmpty()) {
		for(Linspf linspf: list) {
			if(linspf.getChdrcoy().equals(wsspcomn.company.toString())) {
				sv.instpramt.set(linspf.getInstamt06());
				break;
			}
		}
	}else {
		getDatconDate(-1, payrpf.getBillfreq(), payrpf.getPtdate());
		List<Taxdpf> lastTaxdpflist = taxdbilpfDAO.searchTaxdbilRecordByChdr(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum(),datcon2rec.intDate2.toInt());
		if(lastTaxdpflist==null || (lastTaxdpflist!=null) && lastTaxdpflist.size()==0){
			return;
		}
		BigDecimal lastTotTax = BigDecimal.ZERO;
		for(Taxdpf taxdbilpf :lastTaxdpflist){
			if (taxdbilpf.getTxabsind01()!=null && !taxdbilpf.getTxabsind01().equals("Y")) {
				lastTotTax=lastTotTax.add(taxdbilpf.getTaxamt01());
			}
			if (taxdbilpf.getTxabsind02()!=null && !taxdbilpf.getTxabsind02().equals("Y")) {
				lastTotTax=lastTotTax.add(taxdbilpf.getTaxamt02());
			}
			if (taxdbilpf.getTxabsind03()!=null && !taxdbilpf.getTxabsind03().equals("Y")) {
				lastTotTax=lastTotTax.add(taxdbilpf.getTaxamt03());
			}
		}
		if (isGT(payrpf.getSinstamt06(),0))
		sv.instpramt.set(add(payrpf.getSinstamt06(), lastTotTax));
		
	}
	
	
}

protected void getDatconDate(int freqFactor, String freq, int date1) {
	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.freqFactor.set(freqFactor);
	datcon2rec.frequency.set(freq);
	datcon2rec.intDate1.set(date1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
}
	
protected void initCustomerSpecific4() {
	
	//ILIFE-8395-starts
		chdrpfObj = chdrpfDAO.getChdrpfByChdrnum("CH", wsspcomn.company.toString(), chdrpf.getChdrnum(),
				"1");/* IJTI-1523 */
	if(chdrpfObj != null) {	
		sv.cntfee.set(chdrpfObj.getSinstamt02());	
	}
	
/*	acblpf = acblDao.getAcblpfRecord(chdrpf.getChdrcoy().toString(), SACSCODE, chdrpf.getChdrnum().toString(), 
			SACSTYPE, null);
			if(acblpf!=null){
				BigDecimal contractFee = acblpf.getSacscurbal();
				String payFreq = sv.payfreq.toString();
				if(isEQ(payFreq,"12") || isEQ(payFreq,"01") || isEQ(payFreq,"02") || isEQ(payFreq,"04")
						|| isEQ(payFreq,"26")){
					contractFee = contractFee.multiply(new BigDecimal(Integer.parseInt(payFreq)));			
				}				
				sv.cntfee.set(contractFee);		
			}*/
	
}
	
protected void initCustomerSpecific1() {

	}

protected void initCustomerSpecific3() {

	}
	
protected void initCustomerSpecific2() {
		sv.nextinsamt.set(payrpf.getSinstamt01());  //IBPLIFE-4293
		if ((payrpf.getSinstamt05().compareTo(BigDecimal.ZERO) != 0)
				&& (isNE(chdrpf.getPtdate(), chdrpf.getBtdate()))) {
			sv.nextinsamt.add(payrpf.getSinstamt05().doubleValue());
		}
	}
protected void checkLinsrnl5500()
{
	sv.cbillamt.set(ZERO);
	Linspf linspf = new Linspf();
	linspf.setChdrnum(sv.chdrnum.toString());
	linspf.setChdrcoy(wsspcomn.company.toString());
	linspf.setPayflag("O");
	linspf =  linspfdao.getTotBilAmt(linspf);
	if(linspf==null)
		sv.cbillamt.set(ZERO);
	else 
		sv.cbillamt.set(linspf.getCbillamt());
}

//ILIFE-4108 starts
protected void getAutoIncreaseDetails1080()
{
	covrpfList=covrpfDAO.getCovrsurByComAndNum(wsspcomn.company.toString(), sv.chdrnum.toString());
	if(covrpfList.size()>0)
	{
		covrpf = covrpfList.get(0);
		if(isNE(covrpf.getCpiDate(),varcom.vrcmMaxDate) && isNE(covrpf.getCpiDate(),ZERO) && isNE(covrpf.getCpiDate(),SPACES))		
			sv.indxflg.set("C");
		else
			sv.indxflg.set("U");
	}
	else
		sv.indxflg.set("U");
}
//ILIFE-4108 ends
/*ICIL-12 Start*/
protected void getBankDetails(){
	bnkoutpf = bnkoutpfDAO.getBankRecord(sv.chdrnum.toString());
	
	if(bnkoutpf == null){
		sv.bnkout.set(SPACES);
		sv.bnkoutname.set(SPACES);
		sv.bnksm.set(SPACES);
		sv.bnksmname.set(SPACES);
		sv.bnktel.set(SPACES);
		sv.bnktelname.set(SPACES);
		
	}else{
		
		sv.bnkout.set(bnkoutpf.getBnkout());
		sv.bnkoutname.set(bnkoutpf.getBnkoutname());
		sv.bnksm.set(bnkoutpf.getBnksm());
		sv.bnksmname.set(bnkoutpf.getBnksmname());
		sv.bnktel.set(bnkoutpf.getBnktel());
		sv.bnktelname.set(bnkoutpf.getBnktelname());		
	}
	
}
	/**
	* <pre>
	*    Obtain the names of the other lives on the contract header.
	* </pre>
	*/
protected void ownerName1020(Clntpf clntpf)
	{
		if (isEQ(sv.cownnum, SPACES)) {
			jointOwnerName1030();
		}
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.cownnum.toString());
		clntpf.setClntpfx("CN");
		clntpfList= clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null ||(clntpfList!=null && clntpfList.size()==0))
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		sv.ownername.set(longconfname1);

		jointOwnerName1030();

		if(isFatcaAllowed){
			//ILB-498
			//itemIO.setDataKey(SPACES);
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("TH506");
			itempf.setItemitem(chdrpf.getCnttype());
			itemlist = itemDAO.getAllItemitem("IT",wsspcomn.company.toString(),"TH506",chdrpf.getCnttype());
			for (Itempf item : itemlist) {
				if (itemlist.isEmpty()) {
					fatalError600();
				}
				if (item == null) {
					if (isNE(item.getItemitem(), "****")){
						item.setItemitem("***");
						item = itemDAO.getItempfRecord(item);
						if (item == null) {
							fatalError600();
						}
					}
				}
				if (!itemlist.isEmpty()) {
					th506rec.th506Rec.set(item.getGenareaString());	//IBPLIFE-4609
				}
				else {
					th506rec.th506Rec.set(SPACES);
				}
			}
			
			/*itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				if (isNE(itemIO.getItemitem(), "***")) {
					itemIO.setItemitem("***");
					itemIO.setFormat(formatsInner.itemrec);
					itemIO.setFunction(varcom.readr);
					SmartFileCode.execute(appVars, itemIO);
					if (isNE(itemIO.getStatuz(), varcom.oK)
					&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
						syserrrec.params.set(itemIO.getParams());
						fatalError600();
					}
				}
			}
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				th506rec.th506Rec.set(itemIO.getGenarea());
			}
			else {
				th506rec.th506Rec.set(SPACES);
			}*/
			Fatcastatus= descdao.getItems("IT", wsspcomn.fsuco.toString(), "TR2DH", wsspcomn.language.toString());
			if(Fatcastatus.size() > 0) {
		     if(isEQ(th506rec.fatcaFlag,"Y"))	{
				Clftpf clftValue=clftpfdao.getRecordByClntnum("CN", wsspcomn.fsuco.toString(), chdrpf.getCownnum());
				
				
				if(clftValue==null){
					
					 sv.fatcastatus.set(Fatcastatus.get("NUSWOI").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("NUSWOI").getLongdesc());
					
				}else{
				 sv.fatcastatus.set(clftValue.getFfsts().trim());
				 sv.fatcastatusdesc.set(Fatcastatus.get(clftValue.getFfsts().trim()).getLongdesc());
				 }
				}
				else{
					sv.fatcastatus.set(Fatcastatus.get("N/A").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("N/A").getLongdesc());
				}
			}
				}

	}

protected void jointOwnerName1030()
	{
		if (isEQ(sv.jowner, SPACES)) {
			payerName1040();
			return;
		}
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.jowner.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null ||(clntpfList!=null && clntpfList.size()==0))
		{
			return;
		}
		for(Clntpf clntpfData :clntpfList)
		{
			plainname(clntpfData);
		}
		if (isNE(sv.jowner, SPACES)) {
			sv.jownername.set(longconfname1);
		}
		payerName1040();
	}

protected void payerName1040()
	{
		/* Read the Client Role file CLRF logical to find details of    */
		/* the Payer.                                                   */
		clrfpf.setForepfx("CH");
		clrfpf.setForecoy(chdrpf.getChdrcoy().toString());
		wsaaChdrnum.set(chdrpf.getChdrnum());
		wsaaPayrseqno.set("1");
		clrfpf.setForenum(wsaaForenum.toString());
		clrfpf.setClrrrole("PY");
		clrfpfList = clrfpfDAO.readClrfpfData(clrfpf);
		if(clrfpfList==null || (clrfpfList !=null && clrfpfList.size()==0))
			{
				sv.payer.set(SPACES);
				return;
			}
		else {
			clrfpf = clrfpfList.get(0);
				sv.payer.set(clrfpf.getClntnum());
			 }
		if(sv.payer.equals(""))
			{
				agentName1050();
			}
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.payer.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || (clntpfList !=null && clntpfList.size()==0))
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		sv.payername.set(longconfname1);
		agentName1050();
	}

protected void agentName1050() {
	
	agntenqpf.setAgntcoy(chdrpf.getAgntcoy().toString());
	agntenqpf.setAgntnum(chdrpf.getAgntnum());
	agntenqpf.setValidflag("1");
	agntenqpfList = agntenqpfDAO.getAgntenqpfDataWithClntInform(agntenqpf, wsspcomn);
	if(agntenqpfList ==null || (agntenqpfList!=null && agntenqpfList.size()==0))
		{
			return;
		}
	agntenqpf=	agntenqpfList.get(0);
	plainname(agntenqpf.getClntpf());
	sv.servagnam.set(longconfname1);
	assignees1060();
		/*agntenqpf.setAgntcoy(chdrpf.getAgntcoy().toString());
		agntenqpf.setAgntnum(chdrpf.getAgntnum());
		agntenqpf.setValidflag("1");
		agntenqpfList = agntenqpfDAO.getAgntenqpfData(agntenqpf);
		if(agntenqpfList ==null || (agntenqpfList!=null && agntenqpfList.size()==0))
			{
				return;
			}
		agntenqpf=	agntenqpfList.get(0);
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(agntenqpf.getClntnum());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || (clntpfList !=null && clntpfList.size()==0))
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		sv.servagnam.set(wsspcomn.longconfname);
		assignees1060();
		*/
	}

protected void assignees1060()
	{
/*
		asgnenqpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		asgnenqpf.setChdrnum(chdrpf.getChdrnum());
		asgnenqpf.setSeqno(0);
		asgnenqpf.setAsgnnum("");
		asgnenqpfList = asgnenqpfDAO.readAsgnenqData(asgnenqpf);
		if(asgnenqpfList ==null || (asgnenqpfList !=null && asgnenqpfList.size()==0))
			{
				//MOVE 'N'                 TO S6353-INDIC              <003>
				sv.indic.set(SPACES);
				
			}
		else {
				//MOVE 'Y'                 TO S6353-INDIC.             <003>
				sv.indic.set("X");
		}*/
	branchName1070();
	List<Asgnpf> asgnpf = asgnpfDAO.getAsgnpfByChdrNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "");
	if (asgnpf == null || asgnpf.size() == 0) {
		sv.indic.set(SPACES);
		return;
	} else {
		//sv.asgnnum.set(asgnenqIO.getAsgnnum());
		sv.asgnnum.set(asgnpf.get(0).getAsgnnum()); //ILIFE-7267
	}
	/*asgnenqIO.setDataKey(SPACES);
	asgnenqIO.setChdrcoy(chdrpf.getChdrcoy());
	asgnenqIO.setChdrnum(chdrpf.getChdrnum());
	asgnenqIO.setSeqno(ZERO);
	asgnenqIO.setAsgnnum(SPACES);
	asgnenqIO.setFunction(varcom.begn);
	asgnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	asgnenqIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
	SmartFileCode.execute(appVars, asgnenqIO);
	if (isNE(asgnenqIO.getStatuz(), varcom.oK)
	&& isNE(asgnenqIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(asgnenqIO.getParams());
		fatalError600();
	}
	if (isNE(asgnenqIO.getChdrcoy(), chdrpf.getChdrcoy())
	|| isNE(asgnenqIO.getChdrnum(), chdrpf.getChdrnum())
	|| isEQ(asgnenqIO.getStatuz(), varcom.endp)) {
//		       MOVE 'N'                 TO S6353-INDIC              <003>
		sv.indic.set(SPACES);
		return;
	}
	else {
//		       MOVE 'Y'                 TO S6353-INDIC.             <003>
//		sv.indic.set("X");
		sv.asgnnum.set(asgnenqIO.getAsgnnum());
	}
	if(sv.asgnnum.equals(""))
	{
		agentName1050();
	}*/
	clntpf.setClntcoy(wsspcomn.fsuco.toString());
	clntpf.setClntnum(sv.asgnnum.toString());
	clntpf.setClntpfx("CN");
	clntpfList = clntpfDAO.readClientpfData(clntpf);
	if(clntpfList==null || (clntpfList !=null && clntpfList.size()==0))
	{
		return;
	}
	clntpf = clntpfList.get(0);
	plainname(clntpf);
	sv.asgnname.set(longconfname1);
}


protected void branchName1070()
	{
		Map<String, String> itemMap = new HashMap<>();
		itemMap.put(t1692, chdrpf.getCntbranch());
		itemMap.put(t5688, chdrpf.getCnttype());
		itemMap.put(t3623, chdrpf.getStatcode());
		itemMap.put(t3588, chdrpf.getPstcde());
		Map<String, Descpf> descMap = descdao.searchMultiDescpf(wsspcomn.company.toString(),wsspcomn.language.toString(), itemMap);

		if (!descMap.containsKey(t1692)) {
			sv.brchname.set(SPACES);
		} else {
			sv.brchname.set(descMap.get(t1692).getLongdesc());
		}
		if (!descMap.containsKey(t5688)) {
			/*         MOVE ALL '?'           TO S6353-CTYPEDES*/
			sv.ctypedes.set(SPACES);
		} else {
			sv.ctypedes.set(descMap.get(t5688).getLongdesc());
		}
		if (!descMap.containsKey(t3623)) {
			/*         MOVE ALL '?'           TO S6353-CHDRSTATUS              */
			sv.chdrstatus.set(SPACES);
		} else {
			//ILIFE-875 START - Screen S6353 is not showing the field description
			//sv.chdrstatus.set(descIO.getShortdesc());
			sv.chdrstatus.set(descMap.get(t3623).getLongdesc());
			//ILIFE-875 END - Screen S6353 is not showing the field description
		}
		if (!descMap.containsKey(t3588)) {
			/*         MOVE ALL '?'           TO S6353-PREMSTATUS              */
			sv.premstatus.set(SPACES);
		} else {
			//ILIFE-875 START - Screen S6353 is not showing the field description
			//sv.premstatus.set(descIO.getShortdesc());
			sv.premstatus.set(descMap.get(t3588).getLongdesc());
			//ILIFE-875 END - Screen S6353 is not showing the field description
		}
		/*
		 * fwang3 ICIL-4  Get any other details from HPADPF
		 */
		Hpadpf hpadIO = this.hpadpfDAO.getHpadData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (hpadIO == null) {
			sv.znfopt.set(SPACES);
			sv.hpropdte.set(varcom.vrcmMaxDate);
			sv.hprrcvdt.set(varcom.vrcmMaxDate);
			sv.huwdcdte.set(varcom.vrcmMaxDate);
			sv.hoissdte.set(varcom.vrcmMaxDate);
			sv.hissdte.set(varcom.vrcmMaxDate);
			sv.refundOverpay.set("N");// fwang3 ICIL-4
			sv.riskcommdte.set(varcom.vrcmMaxDate);		//ILJ-45
			sv.decldte.set(varcom.vrcmMaxDate);			//ILJ-45
			sv.fstprmrcptdte.set(varcom.vrcmMaxDate);	//ILJ-45
		} else {
			sv.znfopt.set(hpadIO.getZnfopt());
			sv.hpropdte.set(hpadIO.getHpropdte());
			sv.hprrcvdt.set(hpadIO.getHprrcvdt());
			sv.huwdcdte.set(hpadIO.getHuwdcdte());
			sv.hoissdte.set(hpadIO.getHoissdte());
			sv.hissdte.set(hpadIO.getHissdte());
			sv.refundOverpay.set(hpadIO.getRefundOverpay());// fwang3 ICIL-4
			sv.riskcommdte.set(hpadIO.getRskcommdate());		//ILJ-45
			sv.decldte.set(hpadIO.getDecldate());				//ILJ-45
			sv.fstprmrcptdte.set(hpadIO.getFirstprmrcpdate());	//ILJ-45
		}
		if(isGT(payrpf.getSinstamt06(),ZERO) || ((isLT(payrpf.getSinstamt06().intValue(),ZERO)) && (payrpf.getSinstamt05().compareTo(BigDecimal.ZERO) != 0)))
		getTax1100();
		/*
		 * ILIFE-3149 Stamp Duty- start
		 */
		stampDutyflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			List<BigDecimal> totPrem=covrpfDAO.getTotalStampDuty(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
			compute(sv.instPrem, 2).setRounded(add(totPrem.get(0), add(wsaaTotTax,sv.cntfee))); //IBPLIFE-4293
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			getStampDuty1200();		
		}
		
		/*
		 *ILIFE-3149 Stamp Duty- start
		 */
	}

protected void getTax1100() {
	wsaaTotTax.set(ZERO);
	//ILB-498
	//itemIO.setDataKey(SPACES);
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrpf.getChdrcoy().toString());
	itempf.setItemtabl(tr52d);
	itempf.setItemitem(chdrpf.getReg());
	itemlist = itemDAO.getAllItemitem(itempf.getItempfx(),itempf.getItemcoy(),itempf.getItemtabl(),itempf.getItemitem());
 
		//ILIFE-9165--Start
		if(itemlist!=null && itemlist.size() > 0)
		{
			itempf = itemlist.get(0);
			tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		}
	
		else {
			itempf.setItempfx("IT");
			itempf.setItemcoy(chdrpf.getChdrcoy().toString());
			itempf.setItemtabl(tr52d);
			itempf.setItemitem("***");
			itemlist = itemDAO.getAllItemitem(itempf.getItempfx(),itempf.getItemcoy(),itempf.getItemtabl(),itempf.getItemitem());
			if(itemlist!=null && itemlist.size() > 0)
			{
				itempf = itemlist.get(0);
				tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
			
			}
			if (itemlist.isEmpty()) {
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
	 //ILIFE-9165--End

	/*itemIO.setFormat(formatsInner.itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.statuz.set(itemIO.getStatuz());
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem("***");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}
	tr52drec.tr52dRec.set(itemIO.getGenarea());*/
	if (isEQ(tr52drec.txcode, SPACES)) {
		return;
	}
	if (chdrpf.getPtdate()!=null && chdrpf.getBtdate()!=null )
	{
		if (isEQ(chdrpf.getPtdate(), chdrpf.getBtdate())) {
			calcTax5000();
			sv.nextinsamt.add(wsaaTotTax);
			return;
//			goTo(GotoLabel.addTax1100);
		}
	}
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	/*taxdbilpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	taxdbilpf.setChdrnum(chdrpf.getChdrnum());
	taxdbilpf.setInstfrom(chdrpf.getPtdate());
	taxdbilpf.setLife("");
	taxdbilpf.setCoverage("");
	taxdbilpf.setRider("");
	taxdbilpf.setTrantype("");
	taxdbilpf.setPlansfx(0);*/
	taxdbilpfList = taxdbilpfDAO.searchTaxdbilRecordByChdr(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum(),chdrpf.getPtdate());
	if(taxdbilpfList==null || (taxdbilpfList!=null) && taxdbilpfList.size()==0){
//		goTo(GotoLabel.exit1110);
		return;
	}
	for(Taxdpf taxdbilpf :taxdbilpfList){
		if (taxdbilpf.getTxabsind01()!=null && !taxdbilpf.getTxabsind01().equals("Y")) {
			wsaaTotTax.add(taxdbilpf.getTaxamt01().doubleValue());
		}
		if (taxdbilpf.getTxabsind02()!=null && !taxdbilpf.getTxabsind02().equals("Y")) {
			wsaaTotTax.add(taxdbilpf.getTaxamt02().doubleValue());
		}
		if (taxdbilpf.getTxabsind03()!=null && !taxdbilpf.getTxabsind03().equals("Y")) {
			wsaaTotTax.add(taxdbilpf.getTaxamt03().doubleValue());
		}
	}
	sv.nextinsamt.add(wsaaTotTax);
		/*GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1100();
				case addTax1100: 
					addTax1100();
				case exit1100: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}*/
	}
/*
protected void start1100()
	{
		wsaaTotTax.set(ZERO);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrpf.getChdrcoy());
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit1100);
		}
		if (chdrpf.getPtdate()!=null && chdrpf.getBtdate()!=null )
		{
			if (isEQ(chdrpf.getPtdate(), chdrpf.getBtdate())) {
				calcTax5000();
				sv.nextinsamt.add(wsaaTotTax);
				return;
//				goTo(GotoLabel.addTax1100);
			}
		}
		// ilife-3396 Improve the performance of  Contract enquiry- TEN transaction
		// taxdbilpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		// taxdbilpf.setChdrnum(chdrpf.getChdrnum());
		// taxdbilpf.setInstfrom(chdrpf.getPtdate());
		// taxdbilpf.setLife("");
		// taxdbilpf.setCoverage("");
		// taxdbilpf.setRider("");
		// taxdbilpf.setTrantype("");
		// taxdbilpf.setPlansfx(0);
		taxdbilpfList = taxdbilpfDAO.searchTaxdbilRecordByChdr(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum(),chdrpf.getPtdate());
		if(taxdbilpfList==null || (taxdbilpfList!=null) && taxdbilpfList.size()==0){
//			goTo(GotoLabel.exit1110);
			return;
		}
		for(Taxdpf taxdbilpf :taxdbilpfList){
			if (taxdbilpf.getTxabsind01()!=null && !taxdbilpf.getTxabsind01().equals("Y")) {
				wsaaTotTax.add(taxdbilpf.getTaxamt01().doubleValue());
			}
			if (taxdbilpf.getTxabsind02()!=null && !taxdbilpf.getTxabsind02().equals("Y")) {
				wsaaTotTax.add(taxdbilpf.getTaxamt02().doubleValue());
			}
			if (taxdbilpf.getTxabsind03()!=null && !taxdbilpf.getTxabsind03().equals("Y")) {
				wsaaTotTax.add(taxdbilpf.getTaxamt03().doubleValue());
			}
		}
		sv.nextinsamt.add(wsaaTotTax);
	}
protected void addTax1100()
	{
		sv.nextinsamt.add(wsaaTotTax);
	}
*/
	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		longconfname1.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return ;
		}
		longconfname1.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname(Clntpf clntpf)
	{
		/*PLAIN-100*/
		longconfname1.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return ;
		}
		//if (clntpf.getGivname().equals("")) {
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getLsurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getLgivname(), "  ");
			stringVariable1.setStringInto(longconfname1);
		}
		else {
			longconfname1.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
protected void payeename(Clntpf clntpf)
	{
		/*PAYEE-100*/
		longconfname1.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname(clntpf);
			return ;
		}
		if (isEQ(clntpf.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(longconfname1);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(longconfname1);
		/*PAYEE-EXIT*/
	}

protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-1001*/
		longconfname1.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(longconfname1);
		/*CORP-EXIT*/
	}

/*ILIFE-3149 starts*/
protected void getStampDuty1200()
{
	totList = new ArrayList<BigDecimal>();
	totList=covrpfDAO.getTotalStampDuty(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	sv.instPrem.set(totList.get(0));
	if(isGT(chdrpf.getBillfreq(),"00"))
	{
	 sv.instPrem.multiply(Integer.parseInt(chdrpf.getBillfreq()));
	}
	sv.zstpduty01.set(totList.get(1));
	
}
/*ILIFE-3149 starts*/
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*2010-SCREEN-IO.                                                  */
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S6353IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6353-DATA-AREA.                       */
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.calc)) {                  // IBPLIFE-2641
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		/*    Validate fields*/
		if (isNE(sv.claimsind, " ")
		&& isNE(sv.claimsind, "+")
		&& isNE(sv.claimsind, "X")) {
			sv.claimsindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		
//ILIFE-7277
		if (isNE(sv.zmultOwner, " ") && isNE(sv.zmultOwner, "+") && isNE(sv.zmultOwner, "X")) {
			sv.zmultOwnerErr.set(g620);
			wsspcomn.edterror.set("Y");
		}

		if (isNE(sv.plancomp, " ") && isNE(sv.plancomp, "+") && isNE(sv.plancomp, "X")) {
			sv.plancompErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.polcomp, " ")
		&& isNE(sv.polcomp, "+")
		&& isNE(sv.polcomp, "X")) {
			sv.polcompErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.cltrole, " ")
		&& isNE(sv.cltrole, "+")
		&& isNE(sv.cltrole, "X")) {
			sv.rolesindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.subacbal, " ")
		&& isNE(sv.subacbal, "+")
		&& isNE(sv.subacbal, "X")) {
			sv.subacbalErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.transhist, " ")
		&& isNE(sv.transhist, "+")
		&& isNE(sv.transhist, "X")) {
			sv.transhistErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.agntsdets, " ")
		&& isNE(sv.agntsdets, "+")
		&& isNE(sv.agntsdets, "X")) {
			sv.agntsdetsErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.extradets, " ")
		&& isNE(sv.extradets, "+")
		&& isNE(sv.extradets, "X")) {
			sv.extradetsErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.aiind, " ")
		&& isNE(sv.aiind, "+")
		&& isNE(sv.aiind, "X")) {
			sv.aiindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.zsredtrm, " ")
		&& isNE(sv.zsredtrm, "+")
		&& isNE(sv.zsredtrm, "X")) {
			sv.zsredtrmErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.fupflg, " ")
		&& isNE(sv.fupflg, "+")
		&& isNE(sv.fupflg, "X")) {
			sv.fupflgErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ind, " ")
		&& isNE(sv.ind, "+")
		&& isNE(sv.ind, "X")) {
			sv.indErr.set(g620);
		}
		if (isNE(sv.outind, " ")
		&& isNE(sv.outind, "+")
		&& isNE(sv.outind, "X")) {
			sv.outindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.comphist, " ")
		&& isNE(sv.comphist, "+")
		&& isNE(sv.comphist, "X")) {
			sv.comphistErr.set(g620);
		}
		if (superprod && isNE(sv.zctaxind, " ")
		&& isNE(sv.zctaxind, "+")
		&& isNE(sv.zctaxind, "X")) {
			sv.zctaxindErr.set(g620);
			wsspcomn.edterror.set("Y");
		}
		
		if (isNE(sv.zroloverind, " ")
				&& isNE(sv.zroloverind, "+")
				&& isNE(sv.zroloverind, "X")) {
					sv.zroloverindErr.set(g620);
					wsspcomn.edterror.set("Y");
				}
					if (isNE(sv.znlghist, " ")
				&& isNE(sv.znlghist, "+")
				&& isNE(sv.znlghist, "X")) {
					sv.znlghistErr.set(g620);
				}
					if(isClntAccRiskAmt) {
						if (isNE(sv.cntaccriskamt, " ")
								&& isNE(sv.cntaccriskamt, "+")
								&& isNE(sv.cntaccriskamt, "X")) {
									sv.cntaccriskamtErr.set(g620);
									wsspcomn.edterror.set("Y");
								}
							}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*PARA*/
		/*    There is no updating required in this program.*/
		wsaaWsspChdrnum.set(sv.chdrnum);
		wsaaWsspChdrcoy.set(chdrpf.getChdrcoy());
		wsaaWsspChdrpfx.set(SPACES);
		wssplife.chdrky.set(wsaaWsspChdrky);
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
					claimsind4001();
					plancomp4002();
					polcomp4003();
					cltrole4004();
					subacbal4005();
					transhist4006();
					agntsdets4007();
					extradets4008();
					aiind4009();
					fupflg4010();
					ind4011();
					zsredtrm4012();
					outind4013();
					comphist4014();
					znlghist4016();
					multowners4017();//ILIFE-7277
					debcrdhistry();
					if (isClntAccRiskAmt)
						cntaccriskamt4018(); // ICIL-82
					if (superprod) {
						wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
						wsspcomn.chdrCnttype.set(chdrpf.getCnttype());
						zctaxind4015();
					}
					if(ctaxPermission) {
						wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
						wsspcomn.chdrChdrcoy.set(chdrpf.getChdrcoy());
						checkRollover();
					}
				case gensww4010: 
					gensww4010();
					nextProgram4020();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		gensswrec.function.set(SPACES);
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		/* If any of the indicators have been selected, (value - 'X'), */
		/* then set an asterisk in the program stack action field to */
		/* ensure that control returns here, set the parameters for */
		/* generalised secondary switching and save the original */
		/* programs from the program stack. */
		if (isEQ(sv.claimsind, "X") || isEQ(sv.zmultOwner, "X") || isEQ(sv.plancomp, "X") || isEQ(sv.polcomp, "X")
				|| isEQ(sv.cltrole, "X") || isEQ(sv.subacbal, "X") || isEQ(sv.transhist, "X") || isEQ(sv.fupflg, "X")
				|| isEQ(sv.agntsdets, "X") || isEQ(sv.extradets, "X") || isEQ(sv.outind, "X") || isEQ(sv.aiind, "X")
				|| isEQ(sv.comphist, "X") || isEQ(sv.zsredtrm, "X") || isEQ(sv.ind, "X") || isEQ(sv.zctaxind, "X")
				|| isEQ(sv.zroloverind, "X") || isEQ(sv.znlghist, "X") || isEQ(sv.cntaccriskamt, "X") || isEQ(sv.debcrdhistry, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
		}
	}

protected void claimsind4001()
	{
		/*   If Claims Enquiry has been selected set 'A' in the function.*/
		/*   If it was selected previously set it to '+'.*/
		if (isEQ(sv.claimsind, "X")) {
			sv.claimsind.set("?");
			gensswrec.function.set("A");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.claimsind, "?")) {
			sv.claimsind.set("+");
		}
	}

	protected void multowners4017() {//ILIFE-7277
		/* If Claims Enquiry has been selected set 'A' in the function. */
		/* If it was selected previously set it to '+'. */
		if (isEQ(sv.zmultOwner, "X")) {
			wsspcomn.chdrChdrnum.set(sv.chdrnum);
			sv.zmultOwner.set("?");
			gensswrec.function.set("R");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.zmultOwner, "?")) {
			sv.zmultOwner.set("+");
		}
	}

	protected void plancomp4002() {
		/* If Plan Components have been selected set 'B' in the function */
		/* If it was selected previously set it to '+'. */
		/* Set WSSP-PLAN-POLICY to 'L' to indicate to P6239 that Plan */
		/* level enquiry is required. */
		if (isEQ(sv.plancomp, "X")) {
			sv.plancomp.set("?");
			gensswrec.function.set("B");
			wssplife.planPolicy.set("L");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.plancomp, "?")) {
			sv.plancomp.set("+");
		}
	}

protected void polcomp4003()
	{
		/*   If Policy Components have been selected set 'C' in the*/
		/*   function. If it was selected previously set it to '+'.*/
		/*   Set WSSP-PLAN-POLICY to 'O' to indicate to P6239 that Policy*/
		/*   level enquiry is required.*/
		if (isEQ(sv.polcomp, "X")) {
			sv.polcomp.set("?");
			gensswrec.function.set("C");
			wssplife.planPolicy.set("O");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.polcomp, "?")) {
			sv.polcomp.set("+");
		}
	}

protected void cltrole4004()
	{
		/*   If Client Roles have been selected set 'D' in the function.*/
		/*   If it was selected previously set it to '+'.*/
		if (isEQ(sv.cltrole, "X")) {
			sv.cltrole.set("?");
			gensswrec.function.set("D");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.cltrole, "?")) {
			sv.cltrole.set("+");
		}
	}

protected void subacbal4005()
	{
		/*   If Policy Notes have been selected set 'K' in the function.   */
		/*   IF  S6353-IND                = 'X'                   <LA4500>*/
		/*       MOVE '?'                TO S6353-IND             <LA4500>*/
		/*       MOVE 'K'                TO GENS-FUNCTION         <LA4500>*/
		/*       GO TO 4010-GENSWW                                <LA4500>*/
		/*   END-IF.                                              <LA4500>*/
		/*   If Sub Account Balances have been selected set 'E' in the*/
		/*   function. If it was selected previously set it to '+'.*/
		if (isEQ(sv.subacbal, "X")) {
			sv.subacbal.set("?");
			gensswrec.function.set("E");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.subacbal, "?")) {
			sv.subacbal.set("+");
		}
	}

protected void transhist4006()
	{
		/*   If Transactions History has been selected set 'F' in the*/
		/*   function. If it was selected previously set it to '+'.*/
		if (isEQ(sv.transhist, "X")) {
			sv.transhist.set("?");
			gensswrec.function.set("F");
			goTo(GotoLabel.gensww4010);
		}
		/*   IF S6353-FUPFLG    = 'X'                             <LA4500>*/
		/*        MOVE '?'               TO S6353-FUPFLG          <LA4500>*/
		/*        MOVE 'J'               TO GENS-FUNCTION         <LA4500>*/
		/*        GO TO 4010-GENSWW.                              <LA4500>*/
		if (isEQ(sv.transhist, "?")) {
			sv.transhist.set("+");
		}
	}

	/**
	* <pre>
	**   IF S6353-FUPFLG    = '?'                             <LA4500>
	**        MOVE '+'               TO S6353-FUPFLG.         <LA4500>
	* </pre>
	*/
protected void agntsdets4007()
	{
		/*   If Agents' Details have been selected set 'G' in the*/
		/*   function. If it was selected previously set it to '+'.*/
		if (isEQ(sv.agntsdets, "X")) {
			sv.agntsdets.set("?");
			gensswrec.function.set("G");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.agntsdets, "?")) {
			sv.agntsdets.set("+");
		}
	}

protected void extradets4008()
{
	/*   If Extra Header Details have been selected set 'H' in the*/
	/*   function. If it was selected previously set it to '+'.*/
	if (isEQ(sv.extradets, "X")) {
		sv.extradets.set("?");
		gensswrec.function.set("H");
		goTo(GotoLabel.gensww4010);
	}
	if (isEQ(sv.extradets, "?")) {
		sv.extradets.set("+");
	}
}



protected void debcrdhistry()
{

	if (isEQ(sv.debcrdhistry, "X")) {
		sv.debcrdhistry.set("?");
		gensswrec.function.set("R");
		goTo(GotoLabel.gensww4010);
	}
	if (isEQ(sv.debcrdhistry, "?")) {
		sv.debcrdhistry.set("+");
	}
	
	
}

protected void aiind4009()
	{
		/*   If Anniversary Rules have been selected set 'I' in the        */
		/*   function. If it was selected previously set it to '+'.        */
		if (isEQ(sv.aiind, "X")) {
			wsspcomn.chdrChdrcoy.set(chdrpf.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
			wsspcomn.chdrCnttype.set(chdrpf.getCnttype());
			wsspcomn.chdrValidflag.set(chdrpf.getValidflag());
			wsspcomn.currfrom.set(chdrpf.getOccdate());
			gensswrec.function.set("I");
			sv.aiind.set("?");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.aiind, "?")) {
			sv.aiind.set("+");
		}
	}

protected void fupflg4010()
	{
		/*   If Output has been selected set 'L' in the                    */
		/*   function. If it was selected previously set it to '+'.        */
		/*                                                         <LA4500>*/
		/*   IF S6353-OUTIND              = 'X'                   <LA4500>*/
		/*        MOVE CHDRENQ-CHDRCOY   TO WSSP-CHDR-CHDRCOY     <LA4500>*/
		/*        MOVE CHDRENQ-CHDRNUM   TO WSSP-CHDR-CHDRNUM     <LA4500>*/
		/*        MOVE CHDRENQ-CNTTYPE   TO WSSP-CHDR-CNTTYPE     <LA4500>*/
		/*        MOVE CHDRENQ-VALIDFLAG TO WSSP-CHDR-VALIDFLAG   <LA4500>*/
		/*        MOVE CHDRENQ-OCCDATE   TO WSSP-CURRFROM         <LA4500>*/
		/*        MOVE 'L'               TO GENS-FUNCTION         <LA4500>*/
		/*        MOVE '?'               TO S6353-OUTIND          <LA4500>*/
		/*        PERFORM 4400-KEEPS-LETC                         <LA4500>*/
		/*        GO TO 4010-GENSWW.                              <LA4500>*/
		/*   IF S6353-OUTIND              = '?'                   <LA4500>*/
		/*        MOVE '+'               TO S6353-OUTIND.         <LA4500>*/
		if (isEQ(sv.fupflg, "X")) {
			sv.fupflg.set("?");
			gensswrec.function.set("J");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.fupflg, "?")) {
			sv.fupflg.set("+");
		}
	}

protected void ind4011()
	{
		/*   If Policy Notes have been selected set 'K' in the function.   */
		if (isEQ(sv.ind, "X")) {
			sv.ind.set("?");
			gensswrec.function.set("K");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.ind, "?")) {
			sv.ind.set("+");
		}
	}

protected void zsredtrm4012()
	{
		/*   If Reducing Term have been selected set 'I' in the            */
		/*   function. If it was selected previously set it to '+'.        */
		if (isEQ(sv.zsredtrm, "X")) {
			gensswrec.function.set("I");
			sv.zsredtrm.set("?");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.zsredtrm, "?")) {
			sv.zsredtrm.set("+");
		}
	}

protected void outind4013()
	{
		/*   If Output has been selected set 'L' in the                    */
		/*   function. If it was selected previously set it to '+'.        */
		if (isEQ(sv.outind, "X")) {
			wsspcomn.chdrChdrcoy.set(chdrpf.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
			wsspcomn.chdrCnttype.set(chdrpf.getCnttype());
			wsspcomn.chdrValidflag.set(chdrpf.getValidflag());
			wsspcomn.currfrom.set(chdrpf.getOccdate());
			gensswrec.function.set("L");
			sv.outind.set("?");
			keepsLetc4400();
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.outind, "?")) {
			sv.outind.set("+");
		}
	}

protected void comphist4014()
	{
		if (isEQ(sv.comphist, "X")) {
			gensswrec.function.set("M");
			sv.comphist.set("?");
			wsspcomn.flag.set("I");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.comphist, "?")) {
			sv.comphist.set("+");
		}
		wsaaEscKey.set(SPACES);
		if (isEQ(sv.claimsind, SPACES) && isEQ(sv.zmultOwner, SPACES) && isEQ(sv.plancomp, SPACES)
				&& isEQ(sv.polcomp, SPACES) && isEQ(sv.cltrole, SPACES) && isEQ(sv.subacbal, SPACES)
				&& isEQ(sv.transhist, SPACES) && isEQ(sv.fupflg, SPACES) && isEQ(sv.agntsdets, SPACES)
				&& isEQ(sv.extradets, SPACES) && isEQ(sv.aiind, SPACES) && isEQ(sv.zsredtrm, SPACES)
				&& isEQ(sv.outind, SPACES) && isEQ(sv.comphist, SPACES) && isEQ(sv.ind, SPACES)
				&& isEQ(sv.zctaxind, SPACES) && isEQ(sv.zroloverind, SPACES) && isEQ(sv.znlghist, SPACES)
				&& isEQ(sv.cntaccriskamt, SPACES) && isEQ(sv.debcrdhistry, SPACES)) {
			escapeToPrevMenu.setTrue();
		}
	}

protected void cntaccriskamt4018()
{
	/*   If Client Accumulated Risk Amount has been selected set 'Q' in the function.*/
	/*   If it was selected previously set it to '+'.*/
	if (isEQ(sv.cntaccriskamt, "X")) {
		sv.cntaccriskamt.set("?");
		gensswrec.function.set("Q");
		goTo(GotoLabel.gensww4010);
	}
	if (isEQ(sv.cntaccriskamt, "?")) {
		sv.cntaccriskamt.set("+");
	}
}
protected void zctaxind4015()
{
	
	if (isEQ(sv.zctaxind, "X")) {
		sv.zctaxind.set("?");
		gensswrec.function.set("N");
		goTo(GotoLabel.gensww4010);
	}
	if (isEQ(sv.zctaxind, "?")) {
		sv.zctaxind.set("+");
	}
}


protected void checkRollover()
{
	
	if (isEQ(sv.zroloverind, "X")) {
		sv.zroloverind.set("?");
		gensswrec.function.set("P");
		goTo(GotoLabel.gensww4010);
	}
	if (isEQ(sv.zroloverind, "?")) {
		sv.zroloverind.set("+");
	}
}
protected void znlghist4016()
{
	wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
	wsspcomn.chdrCnttype.set(chdrpf.getCnttype());
	if (isEQ(sv.znlghist, "X")) {
		sv.znlghist.set("?");
		gensswrec.function.set("O");
		goTo(GotoLabel.gensww4010);
	}
	if (isEQ(sv.znlghist, "?")) {
		sv.znlghist.set("+");
	}
}
	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call
	*   the generalised secondary switching module to obtain the
	*   next 8 programs and load them into the program stack.
	* </pre>
	*/
protected void gensww4010()
	{
		if (isEQ(gensswrec.function, SPACES)) {
			if (escapeToPrevMenu.isTrue()) {
				wsspcomn.programPtr.add(1);
			}
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		/*ELSE*/
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void keepsLetc4400()
	{
		para4400();
	}

protected void para4400()
	{
		xmlswprec.function.set("ALL  ");
		xmlswprec.mode.set("R");
		xmlswprec.formatFor.set("XMLFORMAT");
		xmlswprec.letcRrn.set(ZERO);
		xmlswprec.company.set(wsspcomn.company);
		xmlswprec.nosOfIncludes.set(1);
		xmlswprec.included[1].set("*ALL");
		xmlswprec.nosOfExcludes.set(1);
		xmlswprec.excluded[1].set("*NONE");
		xmlswprec.clntcoy.set(wsspcomn.fsuco);
		xmlswprec.clntnum.set(SPACES);
		/*    MOVE LETCUTI-RDOCPFX        TO XMLS-RDOCPFX.         <LA4480>*/
		xmlswprec.rdocpfx.set("CH");
		/*    MOVE LETCUTI-RDOCCOY        TO XMLS-RDOCCOY.         <LA4480>*/
		xmlswprec.rdoccoy.set(wsspcomn.company);
		/*    MOVE LETCUTI-RDOCNUM        TO XMLS-RDOCNUM.         <LA4480>*/
		xmlswprec.rdocnum.set(sv.chdrnum);
		xmlswprec.tranno.set(ZERO);
		xmlswprec.branch.set(wsspcomn.branch);
		xmlswprec.nosOfOrderBys.set(1);
		xmlswprec.orderBy[1].set("*ENTITY");
		xmlswprec.dateFrom.set(ZERO);
		xmlswprec.dateTo.set(varcom.vrcmMaxDate);
		callProgram(Xmlsweep.class, xmlswprec.xmlswpRec);
		if (isEQ(xmlswprec.statuz, varcom.endp)) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(xmlswprec.statuz, varcom.oK)) {
			syserrrec.params.set(xmlswprec.function);
			syserrrec.statuz.set(xmlswprec.statuz);
			fatalError600();
		}
		
		letcIO.setParams(SPACES);
		/*    MOVE LETCUTI-RRN            TO LETC-RRN.             <LA4480>*/
		letcIO.setRrn(ZERO);
		/*    MOVE LETCUTI-RDOCPFX        TO LETC-RDOCPFX.         <LA4480>*/
		letcIO.setRdocpfx("CH");
		/*    MOVE LETCUTI-RDOCCOY        TO LETC-RDOCCOY.         <LA4480>*/
		letcIO.setRdoccoy(wsspcomn.company);
		/*    MOVE LETCUTI-RDOCNUM        TO LETC-RDOCNUM.         <LA4480>*/
		letcIO.setRdocnum(sv.chdrnum);
		letcIO.setTranno(ZERO);
		/*    MOVE WSKY-BATC-BATCTRCDE    TO LETC-OTHER-KEYS.      <LA4480>*/
		letcIO.setRequestCompany(wsspcomn.company);
		letcIO.setClntnum(SPACES);
		letcIO.setLetterSeqno(ZERO);
		letcIO.setFormat(formatsInner.letcrec);
		letcIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, letcIO);
		if (isNE(letcIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(letcIO.getStatuz());
			syserrrec.params.set(letcIO.getParams());
			fatalError600();
		}
	}

protected void calcTax5000()
	{
		start5000();
	}

protected void start5000()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		wsaaTotTax.set(ZERO);
		covrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		covrpf.setChdrnum(chdrpf.getChdrnum());
		covrpf.setPlanSuffix(0);
		covrpfList = covrpfDAO.readCovrRecord(covrpf);
		if(isNE(payrpf.getSinstamt05(), ZERO))
			{
			wsaaTr517Item.set(SPACES);
			
			if(covrpfList==null || (covrpfList!=null && covrpfList.size()==0))
				{
					return;
				}
			else {
				for(Covrpf covrpf : covrpfList){
						checkWop5300(covrpf);
					}
				}
			}
		for(Covrpf covrpf : covrpfList)
			{
			 wsaaInvalidPremStatus.set(covrpf.getPstatcode());
		     wsaaInvalidStatus.set(covrpf.getStatcode());
		      if(!invalidStatus.isTrue() && !invalidPremStatus.isTrue()) {
				 processCovrTax5100(covrpf);
		      }
			}
		 coveragepf=covrpfList.get(0);
		if (isGT(payrpf.getSinstamt02(), ZERO)) {
			processCtfeeTax5200();
		}
	}

protected void processCovrTax5100(Covrpf covrpf)
{
		/* Calculate tax on premiums.                                      */
		if (isEQ(covrpf.getInstprem(), BigDecimal.ZERO)) { //IBPLIFE-4293
			//	covrenqIO.setFunction(varcom.nextr);
				return ;
			}
		/*  Read table TR52E.                                              */
		/*  Read table TR52E.                                              */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		readTr52e5400();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		/*  If TR52E tax indicator not = 'Y', do not calculate tax         */
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		/*  If PAYR-SINSTAMT05 not = zero, check to see if component       */
		/* exists in TR517                                                 */
		/*if (isNE(payrpf.getSinstamt05(), ZERO)
		&& isEQ(covrpf.getCrtable(), wsaaTr517Item)
		&& isEQ(tr517rec.zrwvflg01, "Y")) {
			         waiving itself                                        
			return ;
		}*/
		wsaaTr517Ix.set(ZERO);
		if (isNE(payrpf.getSinstamt05(), ZERO)) {
			wsaaWaiveTax = " ";
			for (wsaaTr517Ix.set(1); !(isGT(wsaaTr517Ix, 50)); wsaaTr517Ix.add(1)){
				if (isEQ(tr517rec.ctable[wsaaTr517Ix.toInt()], SPACES)) {
					wsaaTr517Ix.set(51);
				}
				else {
					if (isEQ(tr517rec.ctable[wsaaTr517Ix.toInt()], covrpf.getCrtable())) {
						wsaaWaiveTax = "Y";
						wsaaTr517Ix.set(51);
					}
				}
			}
			/*  If component found in TR517, do not calculate tax              */
			/*  as this means the premium of the component is being waived     */
			/*if (isEQ(wsaaWaiveTax, "Y")) {
				return ;
			}*/
		}
		/*  Set values to tax linkage and call tax subroutine              */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.transType.set("PREM");
		txcalcrec.chdrcoy.set(covrpf.getChdrcoy());
		txcalcrec.chdrnum.set(covrpf.getChdrnum());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		wsaaCntCurr.set(chdrpf.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.amountIn.set(ZERO);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			//ILIFE-6907 starts
			if(covrpf.getZlinstprem()==null){ 
				covrpf.setZlinstprem(BigDecimal.ZERO);
			}
			//ILIFE-6907 ends
			compute(txcalcrec.amountIn, 3).setRounded(sub(covrpf.getInstprem(), covrpf.getZlinstprem()));
		}
		else {
			txcalcrec.amountIn.setRounded(covrpf.getInstprem());
		}
		txcalcrec.effdate.set(chdrpf.getPtdate());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.crtable.set(covrpf.getCrtable());
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/*  Read next COVRENQ                                              */
	}

protected void processCtfeeTax5200()
	{	
		/* Calculate tax on contract fee.                                  */
		/*if (isNE(payrpf.getSinstamt05(), ZERO)
		&& isEQ(tr517rec.zrwvflg03, "Y")) {
			return ;
		}*/
		/*  Read table TR52E.                                              */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		readTr52e5400();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		/*  If TR52E tax indicator2 not 'Y', do not calculate tax          */
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		/*  Set values to tax linkage and call tax subroutine              */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.transType.set("CNTF");
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		wsaaCntCurr.set(chdrpf.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(chdrpf.getPtdate());
		txcalcrec.amountIn.set(payrpf.getSinstamt02());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.crtable.set(coveragepf.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void checkWop5300(Covrpf covrpf)
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		itempf.setItempfx("IT");
		itempf.setItemcoy(covrpf.getChdrcoy());
		itempf.setItemtabl(tr517);
		itempf.setItemitem(covrpf.getCrtable());
		itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
		itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));
		itemlist = itemDAO.findByItemDates(itempf);
		if(itemlist!=null && itemlist.size() > 0)
		{
			itempf = itemlist.get(0);
			tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			wsaaTr517Item.set(itempf.getItemitem());		
		}
	}

protected void readTr52e5400()
	{
		start5400();
	}

protected void start5400()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrpf.getChdrcoy().toString());
		itempf.setItemtabl(tr52e);
		itempf.setItemitem(wsaaTr52eKey.toString());
		itempf.setItmfrm(new BigDecimal(chdrpf.getPtdate().intValue()));
		itempf.setItmto(new BigDecimal(chdrpf.getPtdate().intValue()));
		itemlist = itemDAO.findByItemDates(itempf);
		if(itemlist!=null && itemlist.size() > 0)
		{
			itempf=itemlist.get(0);
			tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */

protected static final class FormatsInner {
		private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
		private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
		private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
		private FixedLengthStringData letcutirec = new FixedLengthStringData(10).init("LETCUTIREC");
		private FixedLengthStringData letcrec = new FixedLengthStringData(10).init("LETCREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");

		private FixedLengthStringData taxdbilrec = new FixedLengthStringData(10).init("TAXDBILREC");
		private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");

		public FixedLengthStringData getItemrec() {
			return itemrec;
		}

		public void setItemrec(FixedLengthStringData itemrec) {
			this.itemrec = itemrec;
		}
	}


}