package com.csc.life.enquiries.dataaccess.dao;

import java.util.List;

import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ItdmpfDAO extends BaseDAO<Itdmpf> {
	public List<Itdmpf> readItdmData(Itdmpf itdmpf);
	public List<Itdmpf> readItdm(String	itempfx,String	itemcoy,String	itemtabl,String	itemitem,int itmfrm);
	public List<Itdmpf> readItdmGenarea(String itemcoy,String itemtabl,String itemitem,int itmfrm); //IBPLIFE-8670
}
