package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sr5b4
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr5b4ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(270);
	public FixedLengthStringData dataFields = new FixedLengthStringData(126).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData tranamt01 = DD.tranamt01.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData tranamt02 = DD.tranamt02.copyToZonedDecimal().isAPartOf(dataFields,25);
	public ZonedDecimalData tranamt03 = DD.tranamt03.copyToZonedDecimal().isAPartOf(dataFields,42);
	public ZonedDecimalData tranamt04 = DD.tranamt04.copyToZonedDecimal().isAPartOf(dataFields,59);
	public ZonedDecimalData nlgbal = DD.nlgbal.copyToZonedDecimal().isAPartOf(dataFields,76);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 126);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData tranamt01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tranamt02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tranamt03Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tranamt04Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData nlgbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 162);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] tranamt01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tranamt02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tranamt03Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tranamt04Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] nlgbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr5b4screenWritten = new LongData(0);
	public LongData Sr5b4windowWritten = new LongData(0);
	public LongData Sr5b4protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr5b4ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {chdrnum, tranamt01, tranamt02, tranamt03, tranamt04, nlgbal,cnttype,ctypedes};
		screenOutFields = new BaseData[][] {chdrnumOut, tranamt01Out, tranamt02Out, tranamt03Out, tranamt04Out, nlgbalOut,cnttypeOut,ctypedesOut};
		screenErrFields = new BaseData[] {chdrnumErr, tranamt01Err, tranamt02Err, tranamt03Err, tranamt04Err, nlgbalErr,cnttypeErr,ctypedesErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr5b4screen.class;
		protectRecord = Sr5b4protect.class;
		
	}

}
