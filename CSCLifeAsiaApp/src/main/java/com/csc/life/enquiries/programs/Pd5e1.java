/*
 * File: Pd5e1.java
 * Date: 30 August 2009 0:45:26
 * Author: Quipoz Limited
 * 
 * Class transformed from PD5E1.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsu.general.recordstructures.Srvunitcpy;
import com.csc.life.enquiries.screens.Sd5e1ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ClntqyDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Pd5e1 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5E1");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaOvrdbfClntqy = new FixedLengthStringData(46);
	private FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(wsaaOvrdbfClntqy, 0, FILLER).init("OVRDBF FILE(CLNTQY) TOFILE(CLRRPF) SHARE(*YES)");

	private FixedLengthStringData wsaaClofClrrpf = new FixedLengthStringData(18);
	private FixedLengthStringData filler3 = new FixedLengthStringData(18).isAPartOf(wsaaClofClrrpf, 0, FILLER).init("CLOF OPNID(CLRRPF)");
	private FixedLengthStringData wsaaQcmdexcCommand = new FixedLengthStringData(1000);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).setUnsigned();

	private FixedLengthStringData wsaaOpnqryfUsed = new FixedLengthStringData(1);
	private Validator opnqryfUsed = new Validator(wsaaOpnqryfUsed, "Y");
	private Validator opnqryfNotUsed = new Validator(wsaaOpnqryfUsed, "N");
	private ZonedDecimalData wsaaStringPointer = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAsteriskCount1 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAsteriskCount2 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAsteriskCount3 = new ZonedDecimalData(3, 0).setUnsigned();
		/* ERRORS */
	private static final String e058 = "E058";
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e091 = "E091";
	private static final String e704 = "E704";
	private static final String f311 = "F311";
	private static final String f917 = "F917";
	private static final String f442 = "F442";
	private static final String g053 = "G053"; 
	private static final String w121 = "W121";
	private static final String f321 = "F321";   
	private static final String e767 = "E767";
	
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String cltsrec = "CLTSREC";
	private static final String t5679 = "T5679";
	private T5679rec t5679rec = new T5679rec();	
	
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();

	private FixedLengthStringData wsaaClntdet = new FixedLengthStringData(41);
	private FixedLengthStringData wsaaGivname = new FixedLengthStringData(20).isAPartOf(wsaaClntdet, 0);
	private FixedLengthStringData wsaaRole = new FixedLengthStringData(2).isAPartOf(wsaaClntdet, 20);
	private ZonedDecimalData wsaaCltdob = new ZonedDecimalData(8, 0).isAPartOf(wsaaClntdet, 22).setUnsigned();
	private FixedLengthStringData wsaaCltsex = new FixedLengthStringData(1).isAPartOf(wsaaClntdet, 30);
	private FixedLengthStringData wsaaCltpcode = new FixedLengthStringData(10).isAPartOf(wsaaClntdet, 31);
	private Clntkey wsaaClntkey = new Clntkey();
	private Batckey wsaaBatchkey = new Batckey();
	private Srvunitcpy srvunitcpy = new Srvunitcpy();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	private Sd5e1ScreenVars sv = ScreenProgram.getScreenVars( Sd5e1ScreenVars.class);
	
   private ClntqyDAO clntqyDAO = DAOFactory.getClntqyDAO();
   private Chdrpf chdrpf = new Chdrpf();
   private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
   private Clntpf clntpf= new Clntpf();
   private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
   //private List<Clntpf> clntpdList = null;
   private Clntpf clntpfModel=null;
   
   private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	
	public Pd5e1() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5e1", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}
@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
@Override
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
@Override
public void processBo(Object... parmArray) {
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0); 

	try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
	}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
@Override
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);

		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		//sv.cltdob.set(varcom.vrcmMaxDate);
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

@Override
protected void screenEdit2000()
	{
		screenIo2010();
		checkSanctions2120();				 
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'SD5E1IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SD5E1-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(Varcom.oK);
		/*VALIDATE*/
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		wsspcomn.chdrCownnum.set(sv.clttwo.toString());
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,Varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			checkForErrors2080();
			return;
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			checkForErrors2080();
			return;
		}
		
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys();
			}
			else {
				verifyBatchControl2900();
			}
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateKeys()
	{			
	
		if((isNE(sv.chdrsel.toString().trim(),"")) && (isNE(sv.clttwo.toString().trim(),""))){
			sv.chdrselErr.set(g053);
			wsspcomn.edterror.set("Y");
			return;
			
		}
		if (isEQ(sv.action,'A')) {
			if (isEQ(sv.chdrsel,SPACES)) {
				sv.chdrselErr.set(w121);
				wsspcomn.edterror.set("Y");
				return;	
			} 
			else {
				chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
				if(chdrpf==null){
					sv.chdrselErr.set(f917);
					wsspcomn.edterror.set("Y");
					return;
				}
				if (isEQ(chdrpf.getValidflag(),"3")) {
		            sv.chdrselErr.set(e767);
					wsspcomn.edterror.set("Y");
					return;
				}
				
				if (isNE(chdrpf.getValidflag(),"1")) {
		            sv.chdrselErr.set(e704);
					wsspcomn.edterror.set("Y");
					return;
				}
				checkStatus2400();
				if (null != itempf) {
				/* When enquiring on a contract, ensure that a user with        */
				/* restricted data access is authorised to view that contract.  */
				initialize(sdasancrec.sancRec);
				sdasancrec.entypfx.set(fsupfxcpy.chdr);
				sdasancrec.entycoy.set(wsspcomn.company);
				sdasancrec.entynum.set(sv.chdrsel);
				checkRuser2400();
				if (isNE(sdasancrec.statuz,varcom.oK)) {
					sv.chdrselErr.set(sdasancrec.statuz);
					wsspcomn.edterror.set("Y");
					return;
				}
				/*   Store the contract header for use by the transaction programs*/
				chdrpfDAO.setCacheObject(chdrpf);
				return;
			}
			}
		} else {			
				if(isEQ(sv.clttwo,SPACES)){
					sv.clttwoErr.set(w121);
					wsspcomn.edterror.set("Y");
					return;
				} else {
					clntpf.setClntpfx("CN");
					clntpf.setClntcoy(wsspcomn.fsuco.toString());
					clntpf.setClntnum(sv.clttwo.toString());
					clntpf.setValidflag("1");
					List<Clntpf> clntpdList = clntpfDAO.readClientpfData(clntpf);
					if(clntpdList==null || clntpdList.isEmpty()){
						// MOVE E058              TO SD5E1-CLNTNUM-ERR             
							sv.clttwoErr.set(e058);
							wsspcomn.edterror.set("Y");
							return;
					} else {//IJTI-320 START
						clntpfModel= clntpdList.get(0);
						wsspcomn.chdrClntpfx.set(clntpf.getClntpfx());
						wsspcomn.chdrClntcoy.set(clntpf.getClntcoy());
						/* If enquiring on a client, ensure that a user with restricted */
						/* data access is authorised to view this client.               */
						sdasancrec.entypfx.set(fsupfxcpy.clnt);
						sdasancrec.entycoy.set(wsspcomn.fsuco);
						sdasancrec.entynum.set(sv.clttwo);
						checkRuser2400();
						if (isNE(sdasancrec.statuz,varcom.oK)) {
							sv.clttwoErr.set(sdasancrec.statuz);
							wsspcomn.edterror.set("Y");
							return;
						}
						/*    If control has reached here a valid client record has been*/
						/*    found for enquiry. Store the record for the enquiry program.*/
						clntpfDAO.setCacheObject(clntpfModel);
						return;
					}
				}
			}
		
			
		
		
		
		
		//IJTI-320 END
	
	}

	protected void checkStatus2400() {

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);/* IJTI-1523 */
		itempf.setItemitem(subprogrec.transcd.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null == itempf) {
			/* MOVE F321 TO S5002-ACTION */
			sv.chdrselErr.set(f321);
			return;
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaSub.set(ZERO);
		lookForStat2500();
	}

	protected void lookForStat2500() {
		
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(e767);
			wsspcomn.edterror.set("Y");
		} else {
			if (isNE(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				lookForStat2500();
			}
		}
		/* EXIT */
	}
protected void validateActionB2220()
	{}



protected void checkRuser2400()
	{
		/*BEGIN*/
		sdasancrec.function.set("VENTY");
		sdasancrec.statuz.set(varcom.oK);
		sdasancrec.userid.set(wsspcomn.userid);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isEQ(sdasancrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sdasancrec.statuz);
			syserrrec.params.set(sdasancrec.sancRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2990-EXIT.                                         */
		return;
	}



	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
@Override
protected void update3000()
	{
		para3000();
	}

protected void para3000()
	{
		/*  Update WSSP Key details*/
		wsspcomn.flag.set("I");
		wsspcomn.sbmaction.set(scrnparams.action);
		wsspcomn.lastActn.set(sv.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		
		if (isEQ(scrnparams.statuz,"BACH")) {
			return;
		}
		/*    IF   SD5E1-ACTION            = 'A' OR                        */
		if (isEQ(sv.action,"A")		
		|| isNE(sv.clttwo,SPACES)) {
			batching3080();
		}

	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
@Override
protected void whereNext4000()
	{
			para4000();
		}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			return ;
		}			
		wsspcomn.programPtr.set(1);
		
	}


}
