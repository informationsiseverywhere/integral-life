package com.csc.life.enquiries.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UndrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:45
 * Class transformed from UNDRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UndrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 147;
	public FixedLengthStringData undrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData undrpfRecord = undrrec;
	
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(undrrec);
	public FixedLengthStringData coy = DD.coy.copy().isAPartOf(undrrec);
	public FixedLengthStringData lrkcls01 = DD.lrkcls.copy().isAPartOf(undrrec);
	public FixedLengthStringData lrkcls02 = DD.lrkcls.copy().isAPartOf(undrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(undrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(undrrec);
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(undrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(undrrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(undrrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(undrrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(undrrec);
	public FixedLengthStringData adsc = DD.adsc.copy().isAPartOf(undrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(undrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(undrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(undrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UndrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UndrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UndrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UndrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UndrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UNDRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CLNTNUM, " +
							"COY, " +
							"LRKCLS01, " +
							"LRKCLS02, " +
							"CHDRNUM, " +
							"LIFE, " +
							"CNTTYP, " +
							"CRTABLE, " +
							"CURRCODE, " +
							"SUMINS, " +
							"EFFDATE, " +
							"ADSC, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     clntnum,
                                     coy,
                                     lrkcls01,
                                     lrkcls02,
                                     chdrnum,
                                     life,
                                     cnttyp,
                                     crtable,
                                     currcode,
                                     sumins,
                                     effdate,
                                     adsc,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		clntnum.clear();
  		coy.clear();
  		lrkcls01.clear();
  		lrkcls02.clear();
  		chdrnum.clear();
  		life.clear();
  		cnttyp.clear();
  		crtable.clear();
  		currcode.clear();
  		sumins.clear();
  		effdate.clear();
  		adsc.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUndrrec() {
  		return undrrec;
	}

	public FixedLengthStringData getUndrpfRecord() {
  		return undrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUndrrec(what);
	}

	public void setUndrrec(Object what) {
  		this.undrrec.set(what);
	}

	public void setUndrpfRecord(Object what) {
  		this.undrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(undrrec.getLength());
		result.set(undrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}