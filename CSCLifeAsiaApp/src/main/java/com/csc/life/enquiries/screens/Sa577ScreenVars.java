package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sa577ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(393);
	public FixedLengthStringData dataFields = new FixedLengthStringData(297).isAPartOf(dataArea,0);
	public FixedLengthStringData factHouseNum = DD.factHouseNum.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData factHouseName = DD.factHouseName.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData scrndesc = DD.scrndesc.copy().isAPartOf(dataFields,62);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea,297);
	public FixedLengthStringData factHouseNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData factHouseNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea,305);
	public FixedLengthStringData[] factHouseNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] factHouseNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(464);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(101).isAPartOf(subfileArea, 0);
	public ZonedDecimalData seq = DD.seq.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData date = DD.date.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public ZonedDecimalData fTotalAmt = DD.fTotalAmt.copyToZonedDecimal().isAPartOf(subfileFields,11);
	public ZonedDecimalData fTotalRecords = DD.fTotalRecords.copyToZonedDecimal().isAPartOf(subfileFields,28);
	public FixedLengthStringData ftype = DD.ftype.copy().isAPartOf(subfileFields,31);
	public FixedLengthStringData userID = DD.userID.copy().isAPartOf(subfileFields,41);
	public FixedLengthStringData extractFile = DD.extractFile.copy().isAPartOf(subfileFields,51);
		
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 101);
	public FixedLengthStringData seqErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData dateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData fTotalAmtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData fTotalRecordsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData ftypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData userIDErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData extractFileErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 129);
	public FixedLengthStringData[] seqOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] dateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] fTotalAmtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] fTotalRecordsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] ftypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] userIDOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] extractFileOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 213);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	

	public LongData Sa577screensflWritten = new LongData(0);
	public LongData Sa577screenctlWritten = new LongData(0);
	public LongData Sa577screenWritten = new LongData(0);
	public LongData Sa577protectWritten = new LongData(0);
	public GeneralTable sa577screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData dateDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sa577screensfl;
	}

	public Sa577ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {seq, date, fTotalAmt, fTotalRecords, ftype, userID, extractFile};
		screenSflOutFields = new BaseData[][] {seqOut, dateOut, fTotalAmtOut, fTotalRecordsOut, ftypeOut, userIDOut, extractFileOut};
		screenSflErrFields = new BaseData[] {seqErr, dateErr, fTotalAmtErr, fTotalRecordsErr, ftypeErr, userIDErr, extractFileErr};
		screenSflDateFields = new BaseData[] {date};
		screenSflDateErrFields = new BaseData[] {dateErr};
		screenSflDateDispFields = new BaseData[] {dateDisp};

		screenFields = new BaseData[] {factHouseNum, factHouseName};
		screenOutFields = new BaseData[][] {factHouseNumOut, factHouseNameOut};
		screenErrFields = new BaseData[] {factHouseNumErr, factHouseNameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sa577screen.class;
		screenSflRecord = Sa577screensfl.class;
		screenCtlRecord = Sa577screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sa577protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sa577screenctl.lrec.pageSubfile);
	}
	
}