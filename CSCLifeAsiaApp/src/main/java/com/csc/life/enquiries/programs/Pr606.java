/*
 * File: Pr606.java
 * Date: 30 August 2009 1:49:11
 * Author: Quipoz Limited
 * 
 * Class transformed from PR606.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.financials.dataaccess.CheqTableDAM;
import com.csc.fsu.general.dataaccess.RbnkTableDAM;
import com.csc.fsu.general.dataaccess.RldgdocTableDAM;
import com.csc.life.enquiries.screens.Sr606ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*           LIFE - PAYMENT HISTORY
*           -----------------------
* This program displays premium related transactions from RTRNPF.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr606 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("PR606");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNumber = new ZonedDecimalData(5, 0);
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(3, 0);
	private String wsaaEmptySfl = "";
		/* FORMATS */
	private String rldgdocrec = "RLDGDOCREC";
	private String rbnkrec = "RBNKREC";
	private String cheqrec = "CHEQREC";

	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(11);
		/*Logical File: Cheque details file*/
	private CheqTableDAM cheqIO = new CheqTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Bank details for Cash recpts type 2 reco*/
	private RbnkTableDAM rbnkIO = new RbnkTableDAM();
		/*RTRNPF *LF by RLDGACCT/TRANNO/RDOCNUM*/
	private RldgdocTableDAM rldgdocIO = new RldgdocTableDAM();
	private Sr606ScreenVars sv = ScreenProgram.getScreenVars( Sr606ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1390, 
		preExit, 
		exit2090
	}

	public Pr606() {
		super();
		screenVars = sv;
		new ScreenModel("Sr606", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initSubfile1100();
	}

protected void initSubfile1100()
	{
		sv.dataArea.set(SPACES);
		sv.chdrnum.set(wsspcomn.chdrChdrnum);
		sv.cnttype.set(wsspcomn.chdrCnttype);
		sv.contdesc.set(wsspcomn.chdrTypedesc);
		sv.cownnum.set(wsspcomn.chdrCownnum);
		sv.cownname.set(wsspcomn.chdrOwnername);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR606", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		readRtrn1200();
		if (isEQ(wsaaEmptySfl,"Y")) {
			initialize(sv.subfileFields);
			scrnparams.function.set(varcom.sadd);
			processScreen("SR606", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(varcom.bomb);
				fatalError600();
			}
		}
	}

protected void readRtrn1200()
	{
		start1200();
	}

protected void start1200()
	{
		wsaaEmptySfl = "Y";
		rldgdocIO.setDataArea(SPACES);
		rldgdocIO.setRtrnRldgpfx(fsupfxcpy.chdr);
		rldgdocIO.setRtrnRldgcoy(wsspcomn.chdrChdrcoy);
		rldgdocIO.setRtrnRldgacct(wsspcomn.chdrChdrnum);
		rldgdocIO.setRtrnTranno(ZERO);
		rldgdocIO.setFormat(rldgdocrec);
		rldgdocIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rldgdocIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		rldgdocIO.setFitKeysSearch("RLDGPFX", "RLDGACCT");
		rldgdocIO.setStatuz(varcom.oK);
		wsaaRem.set(1);
		while ( !(isNE(rldgdocIO.getStatuz(),varcom.oK)
		|| isEQ(wsaaRem,0))) {
			loadSubfile1300();
		}
		
	}

protected void loadSubfile1300()
	{
		try {
			load1300();
		}
		catch (GOTOException e){
		}
	}

protected void load1300()
	{
		SmartFileCode.execute(appVars, rldgdocIO);
		if (isNE(rldgdocIO.getStatuz(),varcom.oK)
		&& isNE(rldgdocIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(rldgdocIO.getStatuz());
			syserrrec.params.set(rldgdocIO.getParams());
			fatalError600();
		}
		if (isEQ(rldgdocIO.getStatuz(),varcom.endp)
		|| isNE(rldgdocIO.getRtrnRldgpfx(),fsupfxcpy.chdr)
		|| isNE(rldgdocIO.getRtrnRldgcoy(),wsspcomn.company)
		|| isNE(rldgdocIO.getRtrnRldgacct(),wsspcomn.chdrChdrnum)) {
			rldgdocIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1390);
		}
		if (isEQ(rldgdocIO.getRtrnRdocpfx(),"CA")) {
			readRbnk1400();
			sv.chqnum.set(rbnkIO.getChqnum());
		}
		else {
			readCheq1500();
			sv.chqnum.set(cheqIO.getCheqno());
		}
		wsaaEmptySfl = "N";
		rldgdocIO.setFunction(varcom.nextr);
		sv.rdocnum.set(rldgdocIO.getRtrnRdocnum());
		sv.acctamt.set(rldgdocIO.getRtrnAcctamt());
		sv.trandate.set(rldgdocIO.getRtrnTrandate());
		sv.zdesc.set(rldgdocIO.getRtrnTrandesc());
		scrnparams.function.set(varcom.sadd);
		processScreen("SR606", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		compute(wsaaNumber, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		wsaaRem.setRemainder(wsaaNumber);
		if (isEQ(wsaaRem,ZERO)) {
			if (isNE(rldgdocIO.getStatuz(),varcom.endp)) {
				scrnparams.subfileMore.set("Y");
			}
			else {
				scrnparams.subfileMore.set(SPACES);
			}
		}
	}

protected void readRbnk1400()
	{
		/*RBNK*/
		rbnkIO.setDataArea(SPACES);
		rbnkIO.setRdocpfx(rldgdocIO.getRtrnRdocpfx());
		rbnkIO.setRdoccoy(rldgdocIO.getRtrnRdoccoy());
		rbnkIO.setRdocnum(rldgdocIO.getRtrnRdocnum());
		rbnkIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rbnkIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		rbnkIO.setFormat(rbnkrec);
		while ( !(isEQ(rbnkIO.getStatuz(),varcom.endp))) {
			loopRbnk1410();
		}
		
		/*EXIT*/
	}

protected void loopRbnk1410()
	{
		start1410();
	}

protected void start1410()
	{
		SmartFileCode.execute(appVars, rbnkIO);
		if (isNE(rbnkIO.getStatuz(),varcom.oK)
		&& isNE(rbnkIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(rbnkIO.getStatuz());
			syserrrec.params.set(rbnkIO.getParams());
			fatalError600();
		}
		if (isEQ(rbnkIO.getStatuz(),varcom.oK)
		&& isEQ(rbnkIO.getRdocpfx(),rldgdocIO.getRtrnRdocpfx())
		&& isEQ(rbnkIO.getRdoccoy(),rldgdocIO.getRtrnRdoccoy())
		&& isEQ(rbnkIO.getRdocnum(),rldgdocIO.getRtrnRdocnum())) {
			if (isEQ(rbnkIO.getChqnum(),SPACES)) {
				rbnkIO.setFunction(varcom.nextr);
			}
			else {
				rbnkIO.setFunction(varcom.endp);
			}
		}
		else {
			rbnkIO.setChqnum(SPACES);
			rbnkIO.setFunction(varcom.endp);
		}
	}

protected void readCheq1500()
	{
		cheq1500();
	}

protected void cheq1500()
	{
		cheqIO.setReqncoy(rldgdocIO.getRtrnRdoccoy());
		cheqIO.setReqnno(rldgdocIO.getRtrnRdocnum());
		cheqIO.setFormat(cheqrec);
		cheqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cheqIO);
		if (isNE(cheqIO.getStatuz(),varcom.oK)
		&& isNE(cheqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cheqIO.getStatuz());
			syserrrec.params.set(cheqIO.getParams());
			fatalError600();
		}
		if (isEQ(cheqIO.getStatuz(),varcom.mrnf)) {
			cheqIO.setCheqno(SPACES);
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (varcom.vrcmMultiRead.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2001();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2001()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*ROLL-UP*/
		if (isNE(scrnparams.statuz,varcom.rolu)) {
			goTo(GotoLabel.exit2090);
		}
		loadSubfile1300();
		wsspcomn.edterror.set(SPACES);
		goTo(GotoLabel.exit2090);
	}

protected void update3000()
	{
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
