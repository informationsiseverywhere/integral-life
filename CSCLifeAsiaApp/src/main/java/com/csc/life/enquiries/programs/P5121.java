/*
 * File: P5121.java
 * Date: 30 August 2009 0:09:11
 * Author: Quipoz Limited
 * 
 * Class transformed from P5121.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.dataaccess.UtrsenqTableDAM;
import com.csc.life.enquiries.screens.S5121ScreenVars;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section if  returning  from  an  optional selection
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status, (STATCODE)  -  short  description  from
*          T3623,
*
*          Premium Status, (PSTATCODE)  -  short  description  from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's client (CLTS) details if they exist.
*
*          Retrive  the   stored  COVRENQ  details  to  obtain  the
*          Coverage number and Rider numbers for display.
*
*     Load the subfile as follows:
*
*     Retrieve the first UTRS  details that have been stored in the
*     UTRS I/O module.  Display  the Plan Suffix from the retrieved
*     details. If the Plan Suffix is not greater than the number of
*     policies summarised from the Contract Header replace the Plan
*     Suffix on UTRS with zero before processing.
*
*     Perform a READR on UTRS to position at the first UTRS record.
*     Store the 'Real'  Plan  Suffix  that is being processed. Read
*     all  the  UTRS  records  for  the  Company,  Contract,  Life,
*     Component and stored  Plan Suffix displaying the details from
*     each record.
*
*     Obtain the Virtual  Fund  description from T5515 and the Unit
*     Type description from T6649.
*
*     If Plan level processing  is in operation or the policy being
*     processed is a non-summarised policy then display the Current
*     Unit Balance and  the Deemed Unit Balance as found, otherwise
*     perform 'break out' processing as follows:
*
*          If the notional  policy  being processed is not '1' then
*          divide  the   balances   by   the   number  of  policies
*          summarised.  If it  is  '1'  then  absorb  any  rounding
*          discrepancies with the formula:
*
*              Balance = (Balance -(POLSUM - 1 )
*                                  ~~~~~~~~~~~~~
*                                     POLSUM
*
*
*     Load all pages required in  the  subfile  and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  requested  move  spaces to the current program
*     position add 1  to  the  program pointer, release the COVRENQ
*     record and exit.
*
*     Release the UTRS rcord.
*
*     At this point the  program  will  be either searching for the
*     FIRST  selected record  in  order  to  pass  control  to  the
*     Transactions Details  program for the selected transaction or
*     it will be  returning  from  the Transactions Details program
*     after displaying some  details  and  searching  for  the NEXT
*     selected record.
*
*     It will be able to determine  which of these two states it is
*     in by examining the Stack Action Flag.
*
*     If not returning from a Transactions Postings display, (Stack
*     Action Flag is blank), perform  a  start  on  the  subfile to
*     position the file pointer at the beginning.
*
*     Each time it returns  to  this  program  after  processing  a
*     previous selection its position in the subfile will have been
*     retained and it will be able  to  continue from where it left
*     off.
*
*     Processing from here is the  same for either state. After the
*     Start or after returning  to  the  program after processing a
*     previous selection read the next record from the subfile.  If
*     this is not selected  (Select is blank), continue reading the
*     next subfile record until  one  is  found  with  a  non-blank
*     Select field or end of file is reached.
*
*     If nothing was selected or  there  are  no more selections to
*     process, continue by  just moving spaces to the current stack
*     action field  and  program  position,  releasing  the COVRENQ
*     record and exiting.
*
*     If a selection has  been  found  read  and  store (READS) the
*     associated UTRS record,  replace  the  Plan  Suffix  with the
*     notional Plan Suffix thart  is  being enquired upon, add 1 to
*     the program pointer and exit.
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1688 - Transaction Codes                 Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5515 - Virtual Funds                     Key: VRTFND
* T5688 - Contract Structure                Key: CNTTYPE
* T6649 - Unit Types                        Key: Unit Type.
*
*****************************************************************
* </pre>
*/
public class P5121 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(P5121.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5121");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-MISCELLANEOUS */
	private PackedDecimalData wsaaEstimateTot  = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaRunningTot  = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private Ufpricerec ufpricerec = new Ufpricerec();
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	private FixedLengthStringData wsaaUtrsComponents = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaUtrsComponents, 0);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaUtrsComponents, 2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaUtrsComponents, 4);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaUtrsComponents, 6).setUnsigned();
	private PackedDecimalData wsaaUnitBal = new PackedDecimalData(18, 5).isAPartOf(wsaaUtrsComponents, 10);
	private PackedDecimalData wsaaDunitBal = new PackedDecimalData(18, 5).isAPartOf(wsaaUtrsComponents, 20);
	private PackedDecimalData wsaaUnitBal2d = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaDunitBal2d = new PackedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaSubfchg = new ZonedDecimalData(3, 0).init(0);
	private int wsaaRrn = 0;

	private FixedLengthStringData wsaaWsspChdrky = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaWsspChdrpfx = new FixedLengthStringData(2).isAPartOf(wsaaWsspChdrky, 0);
	private FixedLengthStringData wsaaWsspChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaWsspChdrky, 2);
	private FixedLengthStringData wsaaWsspChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaWsspChdrky, 3);

	private FixedLengthStringData wsaaFundDets = new FixedLengthStringData(46);
	private FixedLengthStringData wsaaFund = new FixedLengthStringData(4).isAPartOf(wsaaFundDets, 0);
	private FixedLengthStringData wsaaFundTypeI = new FixedLengthStringData(1).isAPartOf(wsaaFundDets, 4);
	private FixedLengthStringData wsaaFundTypeA = new FixedLengthStringData(1).isAPartOf(wsaaFundDets, 5);
	private PackedDecimalData wsaaCurrentTotI = new PackedDecimalData(18, 5).isAPartOf(wsaaFundDets, 6);
	private PackedDecimalData wsaaCurrentTotA = new PackedDecimalData(18, 5).isAPartOf(wsaaFundDets, 16);
	private PackedDecimalData wsaaDcurrentTotI = new PackedDecimalData(18, 5).isAPartOf(wsaaFundDets, 26);
	private PackedDecimalData wsaaDcurrentTotA = new PackedDecimalData(18, 5).isAPartOf(wsaaFundDets, 36);
	private FixedLengthStringData wsaaUtrsDataArea = new FixedLengthStringData(101).init(SPACES);
	private String e040 = "E040";
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5515 = "T5515";
	private String t5688 = "T5688";
	private String t6649 = "T6649";
	private String itemrec = "ITEMREC";
		/*Contract Enquiry - Contract Header.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Interest Bearing Transaction Summary*/
	//private HitsTableDAM hitsIO = new HitsTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private T5515rec t5515rec = new T5515rec();
		/*Unit transaction summary*/
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
		/*Unit transaction summary*/
	private UtrsenqTableDAM utrsenqIO = new UtrsenqTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5121ScreenVars sv = ScreenProgram.getScreenVars( S5121ScreenVars.class);
	//ILIFE-8349
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO",UtrspfDAO.class);
	private Utrspf utrspf = new Utrspf();
	private List<Utrspf> utrspfList;//ILIFE-8391
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO",HitspfDAO.class);
	private Hitspf hitspf = new Hitspf();
	private List<Hitspf> hitspfList = new ArrayList<Hitspf>();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		planLevel1020, 
		rrn1080, 
		nearExit1085, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		writeSubfile1150, 
		nextUtrs1220, 
		process1230, 
		store1270, 
		exit1290, 
		preExit, 
		exit2090, 
		exit3090, 
		bypassStart4010, 
		tryAgain4020, 
		hits4030, 
		tryAgain4040, 
		exit4090
	}

	public P5121() {
		super();
		screenVars = sv;
		new ScreenModel("S5121", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case planLevel1020: {
					planLevel1020();
				}
				case rrn1080: {
					rrn1080();
				}
				case nearExit1085: {
					nearExit1085();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		if (isNE(wsaaSubfchg,0)) {
			wsaaSubfchg.set(0);
			wsaaWsspChdrky.set(wssplife.chdrky);
			if ((isEQ(wsaaWsspChdrnum,sv.chdrnum))
			&& (isEQ(wssplife.planPolicy,wsaaPlanPolicyFlag))) {
				scrnparams.subfileRrn.set(wsaaRrn);
				goTo(GotoLabel.exit1090);
			}
		}
		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaRunningTot.set(ZERO);
		sv.fundAmount.set(ZERO);
		sv.estval.set(ZERO);
		sv.uoffpr.set(ZERO);
		sv.effDate.set(ZERO);
		wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		if (planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());
		if (isEQ(scrnparams.deviceInd,"*RMT")) {
			if (isEQ(chdrpf.getPolinc(),ZERO)
			|| isEQ(chdrpf.getPolinc(),1)) {
				wsaaPlanPolicyFlag.set("L");
			}
			else {
				wsaaPlanPolicyFlag.set("O");
			}
		}
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrpf = covrDao.getCacheObject(covrpf);
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		/* modified for ILIFE-8349
		/*utrsIO.setDataArea(SPACES);
		utrsIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isEQ(utrsIO.getStatuz(),varcom.mrnf)) {
			utrsIO.setDataArea(wsaaUtrsDataArea);
			goTo(GotoLabel.rrn1080);
		}
		else {
			wsaaUtrsDataArea.set(utrsIO.getDataArea());
		}*/

		utrspf = utrspfDAO.getCacheObject(utrspf);

		if(null==utrspf) {

			utrsIO.setFunction(varcom.retrv);

			SmartFileCode.execute(appVars, utrsIO);

			if (isNE(utrsIO.getStatuz(),varcom.oK)

					&& isNE(utrsIO.getStatuz(),varcom.mrnf)) {

				syserrrec.params.set(utrsIO.getParams());

				fatalError600();

			}

			else {


				utrspf.setChdrcoy(wsspcomn.company.toString());
				utrspf.setChdrnum(utrsIO.getChdrnum().toString());
				utrspf.setLife(utrsIO.getLife().toString());
				utrspf.setCoverage(utrsIO.getCoverage().toString());
				utrspf.setRider(utrsIO.getRider().toString());
				utrspf.setPlanSuffix(utrsIO.getPlanSuffix().toInt());

				utrspfList = utrspfDAO.searchUtrsRecord(utrspf);
				for (Utrspf utrspf : utrspfList) {

					if(null==utrspf) {

						fatalError600();

					}

					else {

						utrspfDAO.setCacheObject(utrspf);

						utrsIO.setPlanSuffix(utrspf.getPlanSuffix());
						utrsIO.setLife(utrspf.getLife());
						utrsIO.setCoverage(utrspf.getCoverage());
						utrsIO.setRider(utrspf.getRider());
						utrsIO.setChdrcoy(utrspf.getChdrcoy());
						utrsIO.setChdrnum(utrspf.getChdrnum());
						utrsIO.setUnitVirtualFund(utrspf.getUnitVirtualFund());
						utrsIO.setUnitType(utrspf.getUnitType());
						utrsIO.setCurrentUnitBal(utrspf.getCurrentUnitBal());
						utrsIO.setCurrentDunitBal(utrspf.getCurrentDunitBal());	

					}
				}

			}

		}

		else {

			utrspfList = utrspfDAO.searchUtrsRecord(utrspf);

			if(utrspfList.isEmpty()) {
				goTo(GotoLabel.rrn1080);

			}
			else {

				for (Utrspf utrspf : utrspfList) {
					if (null != utrspf) {
						utrsIO.setPlanSuffix(utrspf.getPlanSuffix());
						utrsIO.setLife(utrspf.getLife());
						utrsIO.setCoverage(utrspf.getCoverage());
						utrsIO.setRider(utrspf.getRider());
						utrsIO.setChdrcoy(utrspf.getChdrcoy());
						utrsIO.setChdrnum(utrspf.getChdrnum());
						utrsIO.setUnitVirtualFund(utrspf.getUnitVirtualFund());
						utrsIO.setUnitType(utrspf.getUnitType());
						utrsIO.setCurrentUnitBal(utrspf.getCurrentUnitBal());
						utrsIO.setCurrentDunitBal(utrspf.getCurrentDunitBal());	
					}
				}

			}


		}

		sv.planSuffix.set(utrsIO.getPlanSuffix());
		if (isLTE(utrsIO.getPlanSuffix(),chdrpf.getPolsum())
		&& isNE(chdrpf.getPolsum(),1)) {
			utrsIO.setPlanSuffix(ZERO);
		}
		if (planLevelEnquiry.isTrue()) {
			wsaaLife.set(utrsIO.getLife());
			wsaaCoverage.set(utrsIO.getCoverage());
			wsaaRider.set(utrsIO.getRider());
			wsaaPlanSuffix.set(utrsIO.getPlanSuffix());
			goTo(GotoLabel.planLevel1020);
		}
		utrsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		wsaaLife.set(utrsIO.getLife());
		wsaaCoverage.set(utrsIO.getCoverage());
		wsaaRider.set(utrsIO.getRider());
		wsaaPlanSuffix.set(utrsIO.getPlanSuffix());
		scrnparams.subfileRrn.set(1);
		wsaaRunningTot.set(ZERO);
		while ( !(isNE(utrsIO.getChdrcoy(),wsspcomn.company)
		|| isNE(utrsIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(utrsIO.getLife(),wsaaLife)
		|| isNE(utrsIO.getCoverage(),wsaaCoverage)
		|| isNE(utrsIO.getRider(),wsaaRider)
		|| isNE(utrsIO.getPlanSuffix(),wsaaPlanSuffix)
		|| isEQ(utrsIO.getStatuz(),varcom.endp))) {
			processUtrs1100();
		}
		sv.fundAmount.set(wsaaRunningTot);
		
		goTo(GotoLabel.rrn1080);
	}

protected void planLevel1020()
	{
		wsaaRunningTot.set(ZERO);
		utrsenqIO.setDataKey(SPACES);
		utrsenqIO.setChdrcoy(utrsIO.getChdrcoy());
		utrsenqIO.setChdrnum(utrsIO.getChdrnum());
		utrsenqIO.setLife(utrsIO.getLife());
		utrsenqIO.setCoverage(utrsIO.getCoverage());
		utrsenqIO.setRider(utrsIO.getRider());
		utrsenqIO.setPlanSuffix(0);
		utrsenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrsenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, utrsenqIO);
		if (isNE(utrsenqIO.getStatuz(),varcom.oK)
		&& isNE(utrsenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsenqIO.getParams());
			fatalError600();
		}
		if (isNE(utrsenqIO.getChdrcoy(),utrsIO.getChdrcoy())
		|| isNE(utrsenqIO.getChdrnum(),utrsIO.getChdrnum())
		|| isNE(utrsenqIO.getLife(),utrsIO.getLife())
		|| isNE(utrsenqIO.getCoverage(),utrsIO.getCoverage())
		|| isNE(utrsenqIO.getRider(),utrsIO.getRider())) {
			utrsenqIO.setStatuz(varcom.endp);
		}
		if (isEQ(utrsenqIO.getStatuz(),varcom.endp)) {
			sv.chdrnumErr.set(e040);
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit1090);
		}
		wsaaLife.set(utrsenqIO.getLife());
		wsaaCoverage.set(utrsenqIO.getCoverage());
		wsaaRider.set(utrsenqIO.getRider());
		processUtrsPlan1200();
		sv.fundAmount.set(wsaaRunningTot);
	}

protected void rrn1080()
	{
		hitspf.setChdrcoy(covrpf.getChdrcoy());
		hitspf.setChdrnum(covrpf.getChdrnum());
		hitspf.setLife(covrpf.getLife());
		hitspf.setCoverage(covrpf.getCoverage());
		hitspf.setRider(covrpf.getRider());
		hitspfList = hitspfDAO.searchHitsRecord(hitspf);
		if (hitspfList.isEmpty()) {
			goTo(GotoLabel.nearExit1085);
		}
		wsaaLife.set(hitspf.getLife());
		wsaaCoverage.set(hitspf.getCoverage());
		wsaaRider.set(hitspf.getRider());
		wsaaPlanSuffix.set(hitspf.getPlanSuffix());
		scrnparams.subfileRrn.set(1);
		/*while ( !(isNE(hitsIO.getChdrcoy(),wsspcomn.company)
		|| isNE(hitsIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(hitsIO.getLife(),wsaaLife)
		|| isNE(hitsIO.getCoverage(),wsaaCoverage)
		|| isNE(hitsIO.getRider(),wsaaRider)
		|| isNE(hitsIO.getPlanSuffix(),wsaaPlanSuffix)
		|| isEQ(hitsIO.getStatuz(),varcom.endp))) {
			processHits1300();
		}*/
		for(Hitspf hits : hitspfList) {
			checkHits4850(hits);
		}
	}

protected void nearExit1085()
	{
		scrnparams.subfileRrn.set(1);
		wsaaRrn = 1;
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void processUtrs1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1100();
				}
				case writeSubfile1150: {
					writeSubfile1150();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1100()
	{
		sv.subfileFields.set(SPACES);
		sv.unitVirtualFund.set(utrsIO.getUnitVirtualFund());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(utrsIO.getUnitVirtualFund());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundShortDesc.fill("?");
		}
		else {
			sv.fundShortDesc.set(descIO.getShortdesc());
		}
		sv.fundtype.set(utrsIO.getUnitType());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6649);
		descIO.setDescitem(utrsIO.getUnitType());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundTypeShortDesc.fill("?");
		}
		else {
			//ILIFE-1402 STARTS
			sv.fundTypeShortDesc.set(descIO.getLongdesc());
			//ILIFE-1402 ENDS
		}
		if (isGT(sv.planSuffix,chdrpf.getPolsum())) {
			sv.currentUnitBal.set(utrsIO.getCurrentUnitBal());
			sv.currentDunitBal.set(utrsIO.getCurrentDunitBal());
			goTo(GotoLabel.writeSubfile1150);
		}
		if (isEQ(utrsIO.getPlanSuffix(),1)) {
			compute(wsaaUnitBal, 5).set(sub(utrsIO.getCurrentUnitBal(),(div(mult(utrsIO.getCurrentUnitBal(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum()))));
			compute(wsaaDunitBal, 5).set(sub(utrsIO.getCurrentDunitBal(),(div(mult(utrsIO.getCurrentDunitBal(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum()))));
		}
		else {
			compute(wsaaUnitBal, 5).set(div(utrsIO.getCurrentUnitBal(),chdrpf.getPolsum()));
			compute(wsaaDunitBal, 5).set(div(utrsIO.getCurrentDunitBal(),chdrpf.getPolsum()));
		}
		compute(wsaaUnitBal2d, 6).setRounded(mult(wsaaUnitBal,1));
		compute(wsaaDunitBal2d, 6).setRounded(mult(wsaaDunitBal,1));
		sv.currentUnitBal.set(wsaaUnitBal2d);
		sv.currentDunitBal.set(wsaaDunitBal2d);
	}

protected void writeSubfile1150()
	{
	ufpricerec.function.set("PRICE");
	ufpricerec.mode.set(SPACES);
	ufpricerec.company.set(wsspcomn.company);
	ufpricerec.unitVirtualFund.set(wsaaFund);
	ufpricerec.unitType.set(sv.fundtype);
	ufpricerec.effdate.set(wsaaToday);
	ufpricerec.nowDeferInd.set("N");
	callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		
		if(isEQ(sv.currentDunitBal, ZERO) || isEQ(sv.currentUnitBal, ZERO)) {
			sv.effDate.set(varcom.vrcmMaxDate);
			sv.uoffpr.set(ZERO);
			sv.estval.set(ZERO);
		}else {
			sv.effDate.set(ufpricerec.priceDate);
			sv.uoffpr.set(ufpricerec.bidPrice);
		}
		wsaaEstimateTot.set(ZERO);
		wsaaEstimateTot.setRounded(mult(sv.currentDunitBal, ufpricerec.bidPrice));
		sv.estval.set(wsaaEstimateTot);
		wsaaRunningTot.add(wsaaEstimateTot);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		utrsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void processUtrsPlan1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fund1210();
				}
				case nextUtrs1220: {
					nextUtrs1220();
				}
				case process1230: {
					process1230();
					writeSubfile1250();
				}
				case store1270: {
					store1270();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fund1210()
	{
		wsaaFundDets.set(SPACES);
		wsaaCurrentTotA.set(ZERO);
		wsaaCurrentTotI.set(ZERO);
		wsaaDcurrentTotA.set(ZERO);
		wsaaDcurrentTotI.set(ZERO);
		wsaaFund.set(utrsenqIO.getUnitVirtualFund());
		if (isEQ(utrsenqIO.getUnitType(),"I")) {
			wsaaFundTypeI.set("I");
			wsaaCurrentTotI.set(utrsenqIO.getCurrentUnitBal());
			wsaaDcurrentTotI.set(utrsenqIO.getCurrentDunitBal());
		}
		else {
			wsaaFundTypeA.set("A");
			wsaaCurrentTotA.set(utrsenqIO.getCurrentUnitBal());
			wsaaDcurrentTotA.set(utrsenqIO.getCurrentDunitBal());
		}
	}

protected void nextUtrs1220()
	{
		utrsenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsenqIO);
		if (isNE(utrsenqIO.getStatuz(),varcom.oK)
		&& isNE(utrsenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsenqIO.getParams());
			fatalError600();
		}
		if (isEQ(utrsenqIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.process1230);
		}
		if (isNE(utrsenqIO.getChdrcoy(),wsspcomn.company)
		|| isNE(utrsenqIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(utrsenqIO.getLife(),wsaaLife)
		|| isNE(utrsenqIO.getCoverage(),wsaaCoverage)
		|| isNE(utrsenqIO.getRider(),wsaaRider)) {
			utrsenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.process1230);
		}
		if (isNE(utrsenqIO.getUnitVirtualFund(),wsaaFund)) {
			goTo(GotoLabel.process1230);
		}
		if (isEQ(utrsenqIO.getUnitType(),"I")) {
			wsaaCurrentTotI.add(utrsenqIO.getCurrentUnitBal());
			wsaaDcurrentTotI.add(utrsenqIO.getCurrentDunitBal());
			wsaaFundTypeI.set("I");
		}
		else {
			wsaaCurrentTotA.add(utrsenqIO.getCurrentUnitBal());
			wsaaDcurrentTotA.add(utrsenqIO.getCurrentDunitBal());
			wsaaFundTypeA.set("A");
		}
		goTo(GotoLabel.nextUtrs1220);
	}

protected void process1230()
	{
		sv.subfileFields.set(SPACES);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(wsaaFund);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundShortDesc.fill("?");
		}
		else {
			sv.fundShortDesc.set(descIO.getShortdesc());
		}
		sv.unitVirtualFund.set(wsaaFund);
		descIO.setDataKey(SPACES);
		if (isNE(wsaaFundTypeI,SPACES)) {
			wsaaFundTypeI.set(SPACES);
			descIO.setDescitem("I");
			sv.fundtype.set("I");
		}
		else {
			descIO.setDescitem("A");
			sv.fundtype.set("A");
			wsaaFundTypeA.set(SPACES);
		}
		desc1280();
		if (isEQ(sv.fundtype,"I")) {
			sv.currentUnitBal.set(wsaaCurrentTotI);
			sv.currentDunitBal.set(wsaaDcurrentTotI);
		}
		else {
			sv.currentUnitBal.set(wsaaCurrentTotA);
			sv.currentDunitBal.set(wsaaDcurrentTotA);
		}
		                                                             
		ufpricerec.function.set("PRICE");
		ufpricerec.mode.set(SPACES);
		ufpricerec.company.set(wsspcomn.company);
		ufpricerec.unitVirtualFund.set(wsaaFund);
		ufpricerec.unitType.set(sv.fundtype);
		ufpricerec.effdate.set(wsaaToday);
		ufpricerec.nowDeferInd.set("N");
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
			
			if(isEQ(sv.currentDunitBal, ZERO) || isEQ(sv.currentUnitBal, ZERO)) {
				sv.effDate.set(varcom.vrcmMaxDate);
				sv.uoffpr.set(ZERO);
				sv.estval.set(ZERO);
			}else {
				sv.effDate.set(ufpricerec.priceDate);
				sv.uoffpr.set(ufpricerec.bidPrice);
			}
			wsaaEstimateTot.set(ZERO);
			wsaaEstimateTot.setRounded(mult(sv.currentDunitBal, ufpricerec.bidPrice));
			sv.estval.set(wsaaEstimateTot);
			wsaaRunningTot.add(wsaaEstimateTot);
	}

protected void writeSubfile1250()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(wsaaFundTypeA,SPACES)) {
			goTo(GotoLabel.store1270);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescitem("A");
		sv.fundtype.set("A");
		desc1280();
		sv.currentUnitBal.set(wsaaCurrentTotA);
		sv.currentDunitBal.set(wsaaDcurrentTotA);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void store1270()
	{
		if (isEQ(utrsenqIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1290);
		}
		wsaaFundDets.set(SPACES);
		wsaaCurrentTotI.set(ZERO);
		wsaaCurrentTotA.set(ZERO);
		wsaaDcurrentTotI.set(ZERO);
		wsaaDcurrentTotA.set(ZERO);
		wsaaFund.set(utrsenqIO.getUnitVirtualFund());
		if (isEQ(utrsenqIO.getUnitType(),"I")) {
			wsaaFundTypeI.set("I");
			wsaaCurrentTotI.set(utrsenqIO.getCurrentUnitBal());
			wsaaDcurrentTotI.set(utrsenqIO.getCurrentDunitBal());
		}
		else {
			wsaaFundTypeA.set("A");
			wsaaCurrentTotA.set(utrsenqIO.getCurrentUnitBal());
			wsaaDcurrentTotA.set(utrsenqIO.getCurrentDunitBal());
		}
		goTo(GotoLabel.nextUtrs1220);
	}

protected void desc1280()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6649);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundTypeShortDesc.fill("?");
		}
		else {
			//ILIFE-1402 STARTS
			sv.fundTypeShortDesc.set(descIO.getLongdesc());
			//ILIFE-1402 ENDS
		}
	}

protected void para1310(Hitspf hits)
	{
		sv.subfileFields.set(SPACES);
		sv.effDate.set(ZERO);
		sv.uoffpr.set(ZERO);
		sv.unitVirtualFund.set(hits.getZintbfnd());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(hits.getZintbfnd());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundShortDesc.fill("?");
		}
		else {
			sv.fundShortDesc.set(descIO.getShortdesc());
		}
		sv.fundtype.set("D");
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6649);
		descIO.setDescitem(sv.fundtype);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fundTypeShortDesc.fill("?");
		}
		else {
			//ILIFE-1402 STARTS
			sv.fundTypeShortDesc.set(descIO.getLongdesc());
			//ILIFE-1402 ENDS
		}
		sv.currentUnitBal.set(ZERO);
		sv.currentDunitBal.set(hits.getZcurprmbal());
		sv.estval.set(hits.getZcurprmbal());
		wsaaRunningTot.add(hits.getZcurprmbal().doubleValue());
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo12010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo12010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		goTo(GotoLabel.exit3090);
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4000();
				}
				case bypassStart4010: {
					bypassStart4010();
				}
				case tryAgain4020: {
					tryAgain4020();
				}
				case hits4030: {
					hits4030();
				}
				case tryAgain4040: {
					tryAgain4040();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
			covrDao.deleteCacheObject(covrpf);
				goTo(GotoLabel.exit4090);
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			goTo(GotoLabel.bypassStart4010);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypassStart4010()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4100();
			}
			
		}
		if (isNE(scrnparams.statuz,varcom.endp)) {
			wsaaSubfchg.add(1);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
			covrDao.deleteCacheObject(covrpf);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),sv.unitVirtualFund)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.zfundtyp,"D")) {
			goTo(GotoLabel.hits4030);
		}
		utrsIO.setChdrcoy(wsspcomn.company);
		utrsIO.setChdrnum(chdrpf.getChdrnum());
		utrsIO.setLife(wsaaLife);
		utrsIO.setCoverage(wsaaCoverage);
		utrsIO.setRider(wsaaRider);
		utrsIO.setPlanSuffix(wsaaPlanSuffix);
		utrsIO.setUnitVirtualFund(sv.unitVirtualFund);
		utrsIO.setUnitType(sv.fundtype);
		utrsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("VRTFND", "UNITYP");
	}

protected void tryAgain4020()
	{
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isEQ(utrsIO.getStatuz(),varcom.endp)
		|| (isNE(utrsIO.getUnitType(),sv.fundtype))
		|| (isNE(utrsIO.getUnitVirtualFund(),sv.unitVirtualFund))) {
			utrsIO.setFunction(varcom.nextr);
			goTo(GotoLabel.tryAgain4020);
		}
		utrsIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		utrsIO.setPlanSuffix(sv.planSuffix);
		utrsIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		wsspcomn.programPtr.add(1);
		wssplife.unitType.set(SPACES);
		goTo(GotoLabel.exit4090);
	}

protected void hits4030()
	{
		hitspf.setChdrcoy(wsspcomn.company.toString());
		hitspf.setChdrnum(chdrpf.getChdrnum());
		hitspf.setLife(wsaaLife.toString());
		hitspf.setCoverage(wsaaCoverage.toString());
		hitspf.setRider(wsaaRider.toString());
		hitspf.setPlanSuffix(wsaaPlanSuffix.toInt());
		hitspf.setZintbfnd(sv.unitVirtualFund.toString());
	}

protected void tryAgain4040()
	{
		hitspf = hitspfDAO.getHitsRecord(hitspf);
		if(hitspf == null) {
			return;
		}
		hitspfDAO.setCacheObject(hitspf);
		wssplife.unitType.set("D");
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

//MIBT-318
protected void checkHits4850(Hitspf hits)
	{
		para1310(hits);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5121", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isGT(wsaaRunningTot,ZERO))
		{
			sv.fundAmount.set(wsaaRunningTot);
		}
	}

}
