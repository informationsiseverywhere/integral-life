package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR566
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr566ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(483);
	public FixedLengthStringData dataFields = new FixedLengthStringData(147).isAPartOf(dataArea, 0);
	public FixedLengthStringData datedues = new FixedLengthStringData(56).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] datedue = ZDArrayPartOfStructure(7, 8, 0, datedues, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(56).isAPartOf(datedues, 0, FILLER_REDEFINE);
	public ZonedDecimalData datedue01 = DD.datedue.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData datedue02 = DD.datedue.copyToZonedDecimal().isAPartOf(filler,8);
	public ZonedDecimalData datedue03 = DD.datedue.copyToZonedDecimal().isAPartOf(filler,16);
	public ZonedDecimalData datedue04 = DD.datedue.copyToZonedDecimal().isAPartOf(filler,24);
	public ZonedDecimalData datedue05 = DD.datedue.copyToZonedDecimal().isAPartOf(filler,32);
	public ZonedDecimalData datedue06 = DD.datedue.copyToZonedDecimal().isAPartOf(filler,40);
	public ZonedDecimalData datedue07 = DD.datedue.copyToZonedDecimal().isAPartOf(filler,48);
	public FixedLengthStringData linstamts = new FixedLengthStringData(77).isAPartOf(dataFields, 56);
	public ZonedDecimalData[] linstamt = ZDArrayPartOfStructure(7, 11, 2, linstamts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(77).isAPartOf(linstamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData linstamt01 = DD.linstamt.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData linstamt02 = DD.linstamt.copyToZonedDecimal().isAPartOf(filler1,11);
	public ZonedDecimalData linstamt03 = DD.linstamt.copyToZonedDecimal().isAPartOf(filler1,22);
	public ZonedDecimalData linstamt04 = DD.linstamt.copyToZonedDecimal().isAPartOf(filler1,33);
	public ZonedDecimalData linstamt05 = DD.linstamt.copyToZonedDecimal().isAPartOf(filler1,44);
	public ZonedDecimalData linstamt06 = DD.linstamt.copyToZonedDecimal().isAPartOf(filler1,55);
	public ZonedDecimalData linstamt07 = DD.linstamt.copyToZonedDecimal().isAPartOf(filler1,66);
	public FixedLengthStringData mnths = new FixedLengthStringData(14).isAPartOf(dataFields, 133);
	public ZonedDecimalData[] mnth = ZDArrayPartOfStructure(7, 2, 0, mnths, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(14).isAPartOf(mnths, 0, FILLER_REDEFINE);
	public ZonedDecimalData mnth01 = DD.mnth.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData mnth02 = DD.mnth.copyToZonedDecimal().isAPartOf(filler2,2);
	public ZonedDecimalData mnth03 = DD.mnth.copyToZonedDecimal().isAPartOf(filler2,4);
	public ZonedDecimalData mnth04 = DD.mnth.copyToZonedDecimal().isAPartOf(filler2,6);
	public ZonedDecimalData mnth05 = DD.mnth.copyToZonedDecimal().isAPartOf(filler2,8);
	public ZonedDecimalData mnth06 = DD.mnth.copyToZonedDecimal().isAPartOf(filler2,10);
	public ZonedDecimalData mnth07 = DD.mnth.copyToZonedDecimal().isAPartOf(filler2,12);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 147);
	public FixedLengthStringData dateduesErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] datedueErr = FLSArrayPartOfStructure(7, 4, dateduesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(28).isAPartOf(dateduesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData datedue01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData datedue02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData datedue03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData datedue04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData datedue05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData datedue06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData datedue07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData linstamtsErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] linstamtErr = FLSArrayPartOfStructure(7, 4, linstamtsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(28).isAPartOf(linstamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData linstamt01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData linstamt02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData linstamt03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData linstamt04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData linstamt05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData linstamt06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData linstamt07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData mnthsErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] mnthErr = FLSArrayPartOfStructure(7, 4, mnthsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(28).isAPartOf(mnthsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mnth01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData mnth02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData mnth03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData mnth04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData mnth05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData mnth06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData mnth07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 231);
	public FixedLengthStringData dateduesOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] datedueOut = FLSArrayPartOfStructure(7, 12, dateduesOut, 0);
	public FixedLengthStringData[][] datedueO = FLSDArrayPartOfArrayStructure(12, 1, datedueOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(84).isAPartOf(dateduesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] datedue01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] datedue02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] datedue03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] datedue04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] datedue05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] datedue06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] datedue07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData linstamtsOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] linstamtOut = FLSArrayPartOfStructure(7, 12, linstamtsOut, 0);
	public FixedLengthStringData[][] linstamtO = FLSDArrayPartOfArrayStructure(12, 1, linstamtOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(84).isAPartOf(linstamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] linstamt01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] linstamt02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] linstamt03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] linstamt04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] linstamt05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] linstamt06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] linstamt07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData mnthsOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] mnthOut = FLSArrayPartOfStructure(7, 12, mnthsOut, 0);
	public FixedLengthStringData[][] mnthO = FLSDArrayPartOfArrayStructure(12, 1, mnthOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(84).isAPartOf(mnthsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mnth01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] mnth02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] mnth03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] mnth04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] mnth05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] mnth06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] mnth07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData datedue01Disp = new FixedLengthStringData(10);
	public FixedLengthStringData datedue02Disp = new FixedLengthStringData(10);
	public FixedLengthStringData datedue03Disp = new FixedLengthStringData(10);
	public FixedLengthStringData datedue04Disp = new FixedLengthStringData(10);
	public FixedLengthStringData datedue05Disp = new FixedLengthStringData(10);
	public FixedLengthStringData datedue06Disp = new FixedLengthStringData(10);
	public FixedLengthStringData datedue07Disp = new FixedLengthStringData(10);

	public LongData Sr566screenWritten = new LongData(0);
	public LongData Sr566windowWritten = new LongData(0);
	public LongData Sr566hideWritten = new LongData(0);
	public LongData Sr566protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr566ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(mnth01Out,new String[] {"01","20","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamt01Out,new String[] {"08","21","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnth02Out,new String[] {"02","20","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnth03Out,new String[] {"03","20","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnth04Out,new String[] {"04","20","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnth05Out,new String[] {"05","20","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnth06Out,new String[] {"06","20","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnth07Out,new String[] {"07","20","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamt02Out,new String[] {"09","21","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamt03Out,new String[] {"10","21","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamt04Out,new String[] {"11","21","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamt05Out,new String[] {"12","21","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamt06Out,new String[] {"13","21","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamt07Out,new String[] {"14","21","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datedue01Out,new String[] {"15","21","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datedue02Out,new String[] {"16","21","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datedue03Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datedue04Out,new String[] {"18","21","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datedue05Out,new String[] {"19","21","-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datedue06Out,new String[] {"23","21","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datedue07Out,new String[] {"22","21","-22",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {mnth01, linstamt01, mnth02, mnth03, mnth04, mnth05, mnth06, mnth07, linstamt02, linstamt03, linstamt04, linstamt05, linstamt06, linstamt07, datedue01, datedue02, datedue03, datedue04, datedue05, datedue06, datedue07};
		screenOutFields = new BaseData[][] {mnth01Out, linstamt01Out, mnth02Out, mnth03Out, mnth04Out, mnth05Out, mnth06Out, mnth07Out, linstamt02Out, linstamt03Out, linstamt04Out, linstamt05Out, linstamt06Out, linstamt07Out, datedue01Out, datedue02Out, datedue03Out, datedue04Out, datedue05Out, datedue06Out, datedue07Out};
		screenErrFields = new BaseData[] {mnth01Err, linstamt01Err, mnth02Err, mnth03Err, mnth04Err, mnth05Err, mnth06Err, mnth07Err, linstamt02Err, linstamt03Err, linstamt04Err, linstamt05Err, linstamt06Err, linstamt07Err, datedue01Err, datedue02Err, datedue03Err, datedue04Err, datedue05Err, datedue06Err, datedue07Err};
		screenDateFields = new BaseData[] {datedue01, datedue02, datedue03, datedue04, datedue05, datedue06, datedue07};
		screenDateErrFields = new BaseData[] {datedue01Err, datedue02Err, datedue03Err, datedue04Err, datedue05Err, datedue06Err, datedue07Err};
		screenDateDispFields = new BaseData[] {datedue01Disp, datedue02Disp, datedue03Disp, datedue04Disp, datedue05Disp, datedue06Disp, datedue07Disp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr566screen.class;
		protectRecord = Sr566protect.class;
		hideRecord = Sr566hide.class;
	}

}
