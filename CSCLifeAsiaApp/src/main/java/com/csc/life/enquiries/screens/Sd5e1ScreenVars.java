package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sd5e1
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class Sd5e1ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(88);
	public FixedLengthStringData dataFields = new FixedLengthStringData(24).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData clttwo = DD.clttwo.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData submnuprog = DD.submnuprog.copy().isAPartOf(dataFields,19);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 24);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData clttwoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData submnuprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 40);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);	
	public FixedLengthStringData[] clttwoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] submnuprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	//public FixedLengthStringData cltdobDisp = new FixedLengthStringData(10);

	public LongData Sd5e1screenWritten = new LongData(0);
	public LongData Sd5e1protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5e1ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrselOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clttwoOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {submnuprog, action, chdrsel, clttwo};
		screenOutFields = new BaseData[][] {submnuprogOut, actionOut, chdrselOut, clttwoOut};
		screenErrFields = new BaseData[] {submnuprogErr, actionErr, chdrselErr, clttwoErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sd5e1screen.class;
		protectRecord = Sd5e1protect.class;
	}

}
