package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sd5e3screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 3, 74}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5e3ScreenVars sv = (Sd5e3ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sd5e3screensfl.getRowCount())) { 
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sd5e3screensfl, 
			sv.Sd5e3screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sd5e3ScreenVars sv = (Sd5e3ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sd5e3screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sd5e3ScreenVars sv = (Sd5e3ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sd5e3screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sd5e3screensflWritten.gt(0))
		{
			sv.sd5e3screensfl.setCurrentIndex(0);
			sv.Sd5e3screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sd5e3ScreenVars sv = (Sd5e3ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sd5e3screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5e3ScreenVars screenVars = (Sd5e3ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.component.setFieldName("component");
				screenVars.instPrem.setFieldName("instPrem");
				screenVars.sumin.setFieldName("sumin");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.hissdteDisp.setFieldName("hissdteDisp");
				screenVars.fundamnt.setFieldName("fundamnt");
				screenVars.surrval.setFieldName("surrval");
				screenVars.loanVal.setFieldName("loanVal"); 
				screenVars.bounsVal.setFieldName("bounsVal");
				screenVars.sacscurbal.setFieldName("sacscurbal");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.component.set(dm.getField("component"));
			screenVars.instPrem.set(dm.getField("instPrem"));
			screenVars.sumin.set(dm.getField("sumin"));
			screenVars.statcode.set(dm.getField("statcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.hissdteDisp.set(dm.getField("hissdteDisp"));
			screenVars.fundamnt.set(dm.getField("fundamnt"));
			screenVars.surrval.set(dm.getField("surrval"));
			screenVars.loanVal.set(dm.getField("loanVal"));
			screenVars.bounsVal.set(dm.getField("bounsVal"));
			screenVars.sacscurbal.set(dm.getField("sacscurbal"));
					
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5e3ScreenVars screenVars = (Sd5e3ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.component.setFieldName("component");
				screenVars.instPrem.setFieldName("instPrem");
				screenVars.sumin.setFieldName("sumin");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.hissdteDisp.setFieldName("hissdteDisp");
				screenVars.fundamnt.setFieldName("fundamnt");
				screenVars.surrval.setFieldName("surrval");
				screenVars.loanVal.setFieldName("loanVal");
				screenVars.bounsVal.setFieldName("bounsVal");
				screenVars.sacscurbal.setFieldName("sacscurbal");		
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("component").set(screenVars.component);
			dm.getField("instPrem").set(screenVars.instPrem);
			dm.getField("sumin").set(screenVars.sumin);
			dm.getField("statcode").set(screenVars.statcode);
			dm.getField("pstatcode").set(screenVars.pstatcode);			
			dm.getField("hissdteDisp").set(screenVars.hissdteDisp);
			dm.getField("fundamnt").set(screenVars.fundamnt);
			dm.getField("surrval").set(screenVars.surrval);
			dm.getField("loanVal").set(screenVars.loanVal);
			dm.getField("bounsVal").set(screenVars.bounsVal);
			dm.getField("sacscurbal").set(screenVars.sacscurbal);		
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sd5e3screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sd5e3ScreenVars screenVars = (Sd5e3ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.component.clearFormatting();
		screenVars.instPrem.clearFormatting();
		screenVars.sumin.clearFormatting();
		screenVars.statcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();		
		screenVars.hissdteDisp.clearFormatting();
		screenVars.fundamnt.clearFormatting();
		screenVars.surrval.clearFormatting();
		screenVars.loanVal.clearFormatting();
		screenVars.bounsVal.clearFormatting();
		screenVars.sacscurbal.clearFormatting();	
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sd5e3ScreenVars screenVars = (Sd5e3ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.component.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.hissdteDisp.setClassString("");
		screenVars.fundamnt.setClassString("");
		screenVars.surrval.setClassString("");
		screenVars.loanVal.setClassString("");
		screenVars.bounsVal.setClassString("");
		screenVars.sacscurbal.setClassString("");	
		
	}

/**
 * Clear all the variables in Sd5e3screensfl
 */
	public static void clear(VarModel pv) {
		Sd5e3ScreenVars screenVars = (Sd5e3ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.component.clear();
		screenVars.instPrem.clear();
		screenVars.sumin.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.hissdteDisp.clear();
		screenVars.fundamnt.clear();
		screenVars.surrval.clear();
		screenVars.loanVal.clear();
		screenVars.bounsVal.clear();
		screenVars.sacscurbal.clear();
	}
}
