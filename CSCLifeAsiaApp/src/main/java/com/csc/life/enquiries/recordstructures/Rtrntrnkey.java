package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:12
 * Description:
 * Copybook name: RTRNTRNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rtrntrnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData rtrntrnFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData rtrntrnKey = new FixedLengthStringData(256).isAPartOf(rtrntrnFileKey, 0, REDEFINE);
  	public FixedLengthStringData rtrntrnRldgcoy = new FixedLengthStringData(1).isAPartOf(rtrntrnKey, 0);
  	public FixedLengthStringData rtrntrnRdocnum = new FixedLengthStringData(9).isAPartOf(rtrntrnKey, 1);
  	public PackedDecimalData rtrntrnTranno = new PackedDecimalData(5, 0).isAPartOf(rtrntrnKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(rtrntrnKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(rtrntrnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rtrntrnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}