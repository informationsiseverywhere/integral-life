/*
 * File: Ph578.java
 * Date: 30 August 2009 1:08:55
 * Author: Quipoz Limited
 * 
 * Class transformed from PH578.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.screens.Sh5llScreenVars;
import com.csc.life.enquiries.tablestructures.Th5llrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            PH578 MAINLINE.
*
*   Parameter prompt program
*
*****************************************************************
* </pre>
*/
public class Ph5ll extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH5LL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();	
	private Th5llrec th5llrec = new Th5llrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh5llScreenVars sv = ScreenProgram.getScreenVars( Sh5llScreenVars.class);
	private String f147 = "F147";
	private String e186 = "E186";
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045,
		preExit,
		exit2090, 
		other3080, 
		exit3090
	}

	public Ph5ll() {
		super();
		screenVars = sv;
		new ScreenModel("Sh5ll", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				initialise1010();
				readRecord1031();
				moveToScreen1040();
			}
			case generalArea1045: {
				generalArea1045();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void initialise1010()
{
	/*INITIALISE-SCREEN*/
	sv.dataArea.set(SPACES);
	/*READ-PRIMARY-RECORD*/
	/*READ-RECORD*/
	itemIO.setDataKey(wsspsmart.itemkey);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	/*READ-SECONDARY-RECORDS*/
}

protected void readRecord1031()
{
	descIO.setDescpfx(itemIO.getItempfx());
	descIO.setDesccoy(itemIO.getItemcoy());
	descIO.setDesctabl(itemIO.getItemtabl());
	descIO.setDescitem(itemIO.getItemitem());
	descIO.setItemseq(SPACES);
	descIO.setLanguage(wsspcomn.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
}

protected void moveToScreen1040()
{
	sv.company.set(itemIO.getItemcoy());
	sv.tabl.set(itemIO.getItemtabl());
	sv.item.set(itemIO.getItemitem());
	sv.longdesc.set(descIO.getLongdesc());
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		sv.longdesc.set(SPACES);
	}
	th5llrec.th5llRec.set(itemIO.getGenarea());
	if (isNE(itemIO.getGenarea(),SPACES)) {
		goTo(GotoLabel.generalArea1045);
	}
}

protected void generalArea1045()
{
	sv.day.set(th5llrec.day);
	
	/*CONFIRMATION-FIELDS*/
	/*OTHER*/
	/*EXIT*/
}

protected void preScreenEdit()
{
	try {
		preStart();
	}
	catch (GOTOException e){
	}
}

protected void preStart()
{
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
	goTo(GotoLabel.preExit);
}

protected void screenEdit2000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				screenIo2010();
				validateDays();
				
			}
			case exit2090: {
				exit2090();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void screenIo2010()
{
	wsspcomn.edterror.set(varcom.oK);
	/*VALIDATE*/
	if (isEQ(wsspcomn.flag,"I")) {
		goTo(GotoLabel.exit2090);
	}
	/*OTHER*/
}




protected void validateDays()
{
	if (isEQ(sv.day, SPACES)){
		sv.dayErr.set(e186);
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}
}



protected void exit2090()
{
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}

protected void update3000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				preparation3010();
				updatePrimaryRecord3050();
				updateRecord3055();
			}
			case other3080: {
			}
			case exit3090: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void preparation3010()
{
	if (isEQ(wsspcomn.flag,"I")) {
		goTo(GotoLabel.exit3090);
	}
}

protected void updatePrimaryRecord3050()
{
	itemIO.setFunction(varcom.readh);
	itemIO.setDataKey(wsspsmart.itemkey);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	varcom.vrcmTranid.set(wsspcomn.tranid);
	varcom.vrcmCompTermid.set(varcom.vrcmTermid);
	varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
	itemIO.setTranid(varcom.vrcmCompTranid);
}

protected void updateRecord3055()
{
	wsaaUpdateFlag = "N";
	if(isEQ(wsspcomn.flag,"C"))
	{
		wsaaUpdateFlag = "Y";
	}
	else
	{
		checkChanges3100();
	}
	if (isNE(wsaaUpdateFlag,"Y")) {
		goTo(GotoLabel.other3080);
	}
	itemIO.setGenarea(th5llrec.th5llRec);
	itemIO.setFunction(varcom.rewrt);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
}

protected void checkChanges3100()
{
	/*CHECK*/
	if (isNE(sv.day,th5llrec.day)) {
		th5llrec.day.set(sv.day);
		wsaaUpdateFlag = "Y";
	}

		/*EXIT*/
}

protected void whereNext4000()
{
	/*NEXT-PROGRAM*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}
}
