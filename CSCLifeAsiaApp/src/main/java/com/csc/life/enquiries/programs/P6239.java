/*
 * File: P6239.java
 * Date: 30 August 2009 0:38:20
 * Author: Quipoz Limited
 * 
 * Class transformed from P6239.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
//ILB-498 starts
//import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
//import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
//import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
//import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
//import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
//ILB-498 ends
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.screens.S6239ScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* This program provides a scrolling enquiry of the components of
* Life Contract at either Plan or Policy level.
*
* Initialise
* ----------
*
*     Skip this  section  if  returning  from an optional selectio 
*     (current stack position action flag = '*').
*
*     Clear the subfile ready for loading.
*
*     The  details  of  the  contract  being  enquired upon will b 
*     stored  in  the  CHDRENQ I/O module. Retrieve the details an 
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description fro 
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description fro 
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint  owner's  client (CLTS) details if they exist 
*
*
*     The program should check WSSP-PLAN-POLICY to see whether Pla 
*     level or  Policy  level  enquiry  has been requested. If Pla 
*     level enquiry  has  been  selected then WSSP-PLAN-POLICY wil 
*     have a value  of  'L',  otherwise if Policy level enquiry ha 
*     been selected WSSP-PLAN-POLICY will have a value of 'O'.
*
*     Load the subfile as follows:
*
*     If an enquiry  at  Plan  level  has  been  requested then th 
*     display will  contain  the lives and associated coverages an 
*     riders for  the whole plan. Each Coverage/Rider will be show 
*     only once no  matter  how  many variations it has. These wil 
*     have their  descriptions displayed next to them but will hav 
*     spaces in the two status fields to the right of the screen.
*     NOTE: The two status fields are now displayed    <004>
*
*     If an enquiry  at  policy  level  has been requested then th 
*     plan will  be  'broken out' into its constituent policies an 
*     the lives, coverages and riders for each policy will be show 
*     along with  the  short  descriptions  of  their status codes 
*     COVR-STATCODE will be decoded against T5682 - Risk Status an 
*     COVR-PSTATCODE  will  be  decoded  against  T5681  -  Premiu 
*     Status.
*
*     One subfile record  is written for each component on the pla 
*     or  policy.   If   performing   Plan   level  processing  th 
*     components will be  written  only once. If this is for Polic 
*     level  enquiry  then  all  the  Plan   Suffixes   which   ar 
*     represented  by  the summary record will be  printed  out  o 
*     their  own  first.  These will then be followed  by  all  th 
*     component details for the remainder of  the  policies  withi 
*     the plan. It will be possible to identify  the components fo 
*     the  summarised portion as their Plan Suffixes  will  all  b 
*     '0000'.  When the Plan Suffix of a component  is greater tha 
*     '0000' then the program will have completed  all  the summar 
*     records and will have started on the non-summarised remainde 
*     of the plan.  When the Plan Suffix changes the program shoul 
*     write  a  subfile  record with only the Plan  Suffix  in  th 
*     Component field and all other fields  empty.  When processin 
*     the components for the summarised portion of the plan it wil 
*     be necessary to calculate the value of the  Plan Suffix. Thi 
*     should  be '0001' for the first subfile  record  written  an 
*     incremented  for  as  many  times  as  there  are  summarise 
*     records. The example at the end of  this document illustrate 
*     this.
*
*          For Plan  level  processing  the  policy  number  is NO 
*          displayed on the first line of the display - its detail 
*          are already shown at the head of the screen. Instead th 
*          first record  of  the  subfile  will have the first Lif 
*          details.
*
*          For a contract,  there  will  be one or many lives (wit 
*          optionally a  joint  life). For each life, there will b 
*          one or  many coverages. For each coverage, there will b 
*          none, one or many riders.
*
*          Use a key  of  CHDRCOY  and CHDRNUM to perform a BEGN o 
*          LIFEENQ.
*
*          Display the  Life  number from the LIFEENQ record in th 
*          Component   Number   field.  Place  the  Client  Number 
*          (LIFE-LIFCNUM),   in   the   Component   field  and  th 
*          "confirmation name"  (from  CLTS)  for  the  life as th 
*          Component   Description.   Use   the   Contract  Compan 
*          (CHDRCOY),   Contract   Number  (CHDRNUM),  Life  Numbe 
*          (LIFE), Joint Life  of '00' with a Coverage of zeroes t 
*          perform a BEGN on COVRENQ. This will
*          return  the   first   coverage   for   the   Life.  Als 
*          sequentially read the next life record to see if a Join 
*          Life exists for the current Life.
*
*          If  the  LIFEENQ  record  just  read  is  for  the  sam 
*          "life-no", the  joint life number should not be zero (i 
*          it is, there  is a database error.) This is a joint lif 
*          associated with  the  current  life. In this case, writ 
*          another record  with  the  action  field  protected, th 
*          life-no  "   +",   and   the   Component  and  Componen 
*          Description  fields   repeated  from  the  Life  on  th 
*          previous  subfile   record.   If  it  is  a  Joint  lif 
*          sequentially  read   the  next  life  record  for  late 
*          processing.
*
*          Coverage details  -  write  the  coverage  record to th 
*          subfile with the coverage number in the Component Numbe 
*          field.  Obtain the Coverage Code from T5673 and place i 
*          in the Component field indented by 2 spaces. Look up it 
*          description from  T5687  for the element description an 
*          place   this   in   the   Component  Description  field 
*          Sequentially read the next coverage/rider record
*
*          NOTE:   read  T5673  the  Contract  Structure  table  b 
*          contract  type  (from  CHDRLNB)  and  effective  on  th 
*          original   inception   date   of   the   contract.  Whe 
*          processing this table item remember that it can continu 
*          onto  additional  (unlimited) table items.  These shoul 
*          be read in turn during the processing.
*
*          Rider details -  if  the  coverage number is the same a 
*          the one  put  on  the coverage subfile record above, th 
*          rider number will  not  be  zero  (if  it  is, this is  
*          database error).  This is a rider on the above coverage 
*          Write the  rider  record  to  the subfile with the ride 
*          number in the Component Number field and its descriptio 
*          from   T5687   in   the   Component  Description  field 
*          Sequentially  read  the  next coverage/rider record.  I 
*          this  is   another   rider   record   for   the  curren 
*          transaction, repeat  this  rider  processing.  If  it i 
*          another  coverage   for   the   same  life,  repeat  th 
*          processing from  the coverage section above.  If it is  
*          coverage  for   another   life,  repeat  all  the  abov 
*          processing for the  next  life.  If it is a coverage fo 
*          another proposal (or the end of the file) all component 
*          have been loaded.
*
*
*     Load all pages  required  in  the subfile and set the subfil 
*     more indicator to no.
*
* Validation
* ----------
*
*     Skip this  section  if  returning  from an optional selectio 
*     (current stack position action flag = '*').
*
*     The only  validation  required  is of the Select fields.  Th 
*     only permitted values  are  '1'  and  '2'.  A value of '2' i 
*     allowed only  against  a Coverage or Rider.  The program wil 
*     read all  the  changed  subfile  records  and  check that th 
*     entries an the  Select fields are only one of these values o 
*     space.  Provide  an error message and highlight the offendin 
*     field if any are found in error.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was requested move spaces to  the  current  progra 
*     position and action field, add 1 to the  program  pointer an 
*     exit.
*
*     At this point the program will be either  searching  for  th 
*     FIRST  selected  record  in  order  to pass  control  to  th 
*     appropriate  generic  enquiry  program   for   the   selecte 
*     component  or it will be returning from one  of  the  Enquir 
*     screens after displaying some details and  searching  for th 
*     NEXT selected record.
*
*     It will be able  to determine which of these two states it i 
*     in by examining the Stack Action Flag.
*
*     If  returning  from an enquiry, (Stack Action  field  =  '*' 
*     restore the next 8 positions of the WSSP  program  stack fro 
*     Working-Storage.
*
*     If  not  returning  from an Enquiry, (Stack  Action  Flag  i 
*     blank), perform a start on the subfile to  position  the fil 
*     pointer at the beginning.
*
*     Each time  it  returns  to  this  program  after processing  
*     previous selection its position in the subfile will have bee 
*     retained and it  will  be able to continue from where it lef 
*     off.
*
*     Processing from here  is the same for either state. After th 
*     Start or  after  returning  to the program after processing  
*     previous selection read the next record from the subfile.  I 
*     this is  not selected (Select is blank), continue reading th 
*     next subfile  record  until  one  is  found  with a non-blan 
*     Select field or end  of file is reached. Do NOT use the 'Rea 
*     Next Changed Subfile Record' function.
*
*     If nothing was selected or there are no  more  selections  t 
*     process,  continue  by  moving blanks to  the  current  Stac 
*     Action field and program position and exit.
*
*     If a selection  has  been found save the next set of program 
*     (8) from the program stack and flag the current position wit 
*     '*'.
*
*     If a Life record has been selected or a Coverage/Rider recor 
*     is  selected  with  a  value  of '2' then  call  GENSSWCH  t 
*     retrieve the program switching details. The value to use wil 
*     be determined as follows:
*
*          When a life has been selected use an action of 'A'.
*
*          When a Coverage/Rider has been selected with  a value o 
*          '2' use an action of 'B'. *** NOTE ***  The program doe 
*          not yet allow nor handle an action of '2'.  This will b 
*          built into the program later.
*
*     When a Coverage/Rider has been selected with an action of '1 
*     then the  Generic  Component Processing Table is used to fin 
*     the programs required.  Look up the table T5671 with a key o 
*     the transaction number concatenated  with coverage/rider cod 
*     from the subfile record).  Move these four  programs into th 
*     program stack and move spaces to the four positions followin 
*     that.
*
*     GENSSWCH will return the following:
*
*               'A' - Life Scrolling Enquiry
*               'B' - Unit Fund Details
*               'C' - Component Reinsurance
*
*     If a Life was selected perform a READS on the LIFELNB datase 
*     to store the record for the Life Enquiry program.
*
*     If  a  Coverage/Rider  was  selected  then  there  may  be   
*     corresponding COVR  record or, in the case of Plan processin 
*     where one of the 'broken out' policies was selected it may b 
*     the summary record  that  has to be read.  In either case th 
*     COVRENQ record should be read and stored, (READS).  If one o 
*     the summary records has been selected use '0000'  as the Pla 
*     Suffix.
*
*     Add 1 to the program pointer and exit.
*
*
* Notes.
* ------
*
*     A new logical view of COVR must be created called COVRENQ. I 
*     will be keyed  the  same  as COVR and will include only thos 
*     fields required  for  this  program  and  P6241 and P6242 th 
*     component enquiry programs.
*
*     Tables Used:
*
* T1675 - Generalised Secondary Switching   Key: Transaction Code
*                                              + Program Number
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T3673 - Contract Structure                Key: CNTTYPE
* T5688 - Contract Structure                Key: CNTTYPE
*
*
* Examples.
* ---------
*
*     In this  example  the Contract Header record, (CHDR), has th 
*     following values:
*
* CHDR-POLSUM = 3
* CHDR-POLINC = 5
* CHDR-NXTSFX = 0004
*
*     This indicates that  the number of policies in plan is 5, th 
*     number summarised  is  3  and  therefore the next plan suffi 
*     number is 0004.
*
* <-- File Details -->          <--------  Screen Details -------- 
*
*  Plan  Life Cov Rid           Component     Component Descriptio 
* Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
*  0000   01   01  00              0001
*                                  0002
*                                  0003
*                               01 12345678   Life Name #1
*                               01   COV1     Coverage #1
*  0000   01   01  01           01     RID1   Rider #1 of Coverage #1
*  0000   01   01  02           02     RID2   Rider #2 of Coverage #1
*  0000   01   02  00           01   COV2     Coverage #2
*  0000   01   02  01           01     RID1   Rider #1 of Coverage #2
*  0000   02   01  00           02 87654321   Life Name #2
*                               01   COV1     Coverage #1
*  0000   02   01  01           01     RID1   Rider #1 of Coverage #1
*  0000   02   01  02           02     RID2   Rider #2 of Coverage #1
*
*     The  above  records  show the lives and  components  for  th 
*     summary records. In this example the first  three  lines sho 
*     which  policies  with  the  plan  are   summarised  by  thes 
*     components.
*
*     At this point  the program will come upon the first componen 
*     for the next  policy  within  the  plan  -  one that does no 
*     summarise  anything   and   so   its  display  will  be  mor 
*     straightforward.
*
*  Plan  Life Cov Rid           Component     Component Descriptio 
* Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
*  0004   01   01  00              0004
*                               01 12345678   Life Name #1
*                               01   COV1     Coverage #1
*  0004   01   01  01           01     RID1   Rider #1 of Coverage #1
*  0004   01   01  02           02     RID2   Rider #2 of Coverage #1
*  0004   01   01  03           03     RID3   Rider #3 of Coverage #1
*  0004   01   02  00           01   COV2     Coverage #2
*  0004   01   02  01           01     RID1   Rider #1 of Coverage #2
*  0004   01   03  00           03   COV3     Coverage #3
*  0004   02   01  00           02 87654321   Life Name #2
*                               01   COV1     Coverage #1
*  0004   02   01  01           01     RID1   Rider #1 of Coverage #1
*  0004   02   01  02           02     RID2   Rider #2 of Coverage #1
*  0005   01   01  00              0005
*                               01 12345678   Life Name #1
*                               01   COV1     Coverage #1
*  0005   01   01  01           01     RID1   Rider #1 of Coverage #1
*  0005   01   01  02           02     RID2   Rider #2 of Coverage #1
*  0005   01   01  03           03     RID3   Rider #3 of Coverage #1
*  0005   01   02  00           01   COV2     Coverage #2
*  0005   01   02  01           01     RID1   Rider #1 of Coverage #2
*  0005   01   03  00           03   COV3     Coverage #3
*  0005   02   01  00           02 87654321   Life Name #2
*                               01   COV1     Coverage #1
*  0005   02   01  01           01     RID1   Rider #1 of Coverage #1
*  0005   02   01  02           02     RID2   Rider #2 of Coverage #1
*
*****************************************************************
* </pre>
*/
public class P6239 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6239");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaCovrenqDataArea = new FixedLengthStringData(264).init(SPACES);
	protected BinaryData wsaaSubfchg = new BinaryData(3, 0);
	private int wsaaRrn = 0;

	private FixedLengthStringData wsaaRecordFound = new FixedLengthStringData(1);
	private Validator recordFound = new Validator(wsaaRecordFound, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOn = new Validator(indicTable, "1");
	private Validator indOff = new Validator(indicTable, "0");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaRestrictUser = new FixedLengthStringData(1);
	private Validator restrictedUser = new Validator(wsaaRestrictUser, "Y");
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String e494 = "E494";
	private static final String h093 = "H093";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5671 = "T5671";
	private static final String t5681 = "T5681";
	private static final String t5682 = "T5682";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrpf = new Covrpf();
	protected List<Covrpf> covrList;
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	protected Lifepf lifepf = new Lifepf();
	protected boolean loopflag=false;
	protected List<Lifepf> lifeList;
	protected Iterator<Lifepf> lifeIterator;
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private T5671rec t5671rec = new T5671rec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	Iterator<Covrpf> iteratorCovr ;
	protected S6239ScreenVars sv = ScreenProgram.getScreenVars( S6239ScreenVars.class);
	protected WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	
	//ILB-498 starts
	protected ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	protected Clntpf clntpf = new Clntpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO",DescDAO.class);
	private Descpf descpf = new Descpf();
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO",HitspfDAO.class);
	private Hitspf hitspf = new Hitspf();
	private List<Hitspf> hitspfList;
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO",UtrspfDAO.class);
	private Utrspf utrspf = new Utrspf();
	private List<Utrspf> utrspfList;
	//ILB-498 ends
	
	//ILJ-388 start
	private boolean cntEnqFlag = false;
	private String cntEnqFeature = "CTENQ010";
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Payrpf payrpf = null;
	//ILJ-388 end

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readFirstCoverage1110, 
		covrenqio1111, 
		planSuffixPart1120, 
		exit2090, 
		updateErrorIndicators2670, 
		bypassStart4010, 
		nextProgram4080, 
		nextProgram4085, 
		exit4090, 
		checkHits4850, 
		readCovr4860
	}

	public P6239() {
		super();
		screenVars = sv;
		new ScreenModel("S6239", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		wsaaSubfchg.set(0);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"Y")) {
			wsaaSubfchg.set(0);
			wsspcomn.flag.set("N");
		}
		/* If all the selections have been processed, redisplay the        */
		/* subfile.                                                        */
		if (isNE(wsaaSubfchg,0)) {
			/*        MOVE 0                  TO WSAA-SUBFCHG                  */
			scrnparams.subfileRrn.set(wsaaRrn);
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		indicArea.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		scrnparams.function.set(varcom.sclr);
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");//ILJ-388
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		checkSda1700();
		if (restrictedUser.isTrue()) {
			sv.indOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.indOut[varcom.nd.toInt()].set(SPACES);
		}
		/*    Set screen fields*/
		/*    IF   PLAN-LEVEL-ENQUIRY                                      */
		/*         MOVE 'Plan Components' TO S6239-SCRTITLE                */
		/*    ELSE                                                         */
		/*         MOVE 'Policy Components'                                */
		/*                                TO S6239-SCRTITLE.               */
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			indOn.setTrue(2);
		}
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifepf = lifepfDAO.getLifeRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
		sv.lifenum.set(lifepf.getLifcnum());
		clntpf.setClntnum(lifepf.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpf = clntpfDAO.selectClient(clntpf);
		if (clntpf == null) {
			fatalError600();
		}
		/*cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		lifepf = lifepfDAO.getLifeRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "01");
		if ( null != lifepf ) {
			sv.jlife.set(lifepf.getLifcnum());
			clntpf.setClntnum(lifepf.getLifcnum());
			clntpf.setClntcoy(wsspcomn.fsuco.toString());
			clntpf.setClntpfx("CN");
			clntpf = clntpfDAO.selectClient(clntpf);
			if (clntpf == null) {
				fatalError600();
			}
			/*cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}*/
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		//ILB-498 starts
		descpf=descDAO.getdescData("IT", t5688, chdrpf.getCnttype(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		
		/*    Obtain the Contract Status description from T3623.*/
		descpf=descDAO.getdescData("IT", t3623, chdrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descpf.getLongdesc());
		}
		
		/*    Obtain the Premuim Status description from T3588.*/
		descpf=descDAO.getdescData("IT", t3588, chdrpf.getPstcde(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descpf.getLongdesc());
		}
		//ILB-498 ends
		/*    Read the first LIFE details on the contract.*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		lifeList = lifepfDAO.getLifeRecords(wsspcomn.company.toString(), chdrpf.getChdrnum(), "1");
		if(!lifeList.isEmpty()){
			lifeIterator = lifeList.iterator();
			if(lifeIterator.hasNext()) {
				lifepf = lifeIterator.next();
				loopflag = true;
			}
		}
		covrList = covrDao.getCovrLifeenqRecords(wsspcomn.company.toString(), chdrpf.getChdrnum(), "1", "1");
		if(! covrList.isEmpty()){
			iteratorCovr = covrList.iterator();
			if (iteratorCovr.hasNext()) 
			{
				covrpf = iteratorCovr.next();	
				// ILJ-388 Start
				
				if (cntEnqFlag) {
						sv.cntEnqScreenflag.set("Y");
						sv.suminsOut[Varcom.nd.toInt()].set("N");
						sv.totalPremOut[Varcom.nd.toInt()].set("N");
					/* Read the PAYR record to get the Billing Details.                */
					payrpf = payrpfDAO.getpayrRecord(wsspcomn.company.toString(),chdrpf.getChdrnum(),1);	
					if (isEQ(payrpf.getBillfreq(), "00")) {
						sv.instPrem.set(covrpf.getSingp().setScale(0, BigDecimal.ROUND_HALF_UP)); 
						}
					else{
						sv.instPrem.set(covrpf.getInstprem().setScale(0, BigDecimal.ROUND_HALF_UP));
					}
					sv.sumins.set(covrpf.getSumins().toPlainString());
					//ILJ-388
					if(sv.component.toString().matches("[0-9]+") && sv.component.toString().length() > 0) {
						sv.instPrem.set(BigDecimal.ZERO);
						sv.instPrem.setBlankWhenZero();
					}
					
				}else {
					sv.cntEnqScreenflag.set("N");
					sv.suminsOut[Varcom.nd.toInt()].set("Y");
					sv.totalPremOut[Varcom.nd.toInt()].set("Y");
				}
				// ILJ-388 End
			}
		}
		while (isEQ(loopflag, true)) {
			processLife1100();
		}
		scrnparams.subfileRrn.set(1);
		wsaaRrn = 1;
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*      (including subfile load section)
	* </pre>
	*/

	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/* LGNM-EXIT */
	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();

		} else if (isNE(clntpf.getGivname(), SPACES)) {
			String firstName = clntpf.getGivname();
			String lastName = clntpf.getSurname();
			String delimiter = ",";

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);

		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
		} else if (isEQ(clntpf.getEthorig(), "1")) {
			String firstName = clntpf.getGivname();
			String lastName = clntpf.getSurname();
			String delimiter = "";
			String salute = clntpf.getSalutl();

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = getStringUtil().includeSalute(salute, fullName);

			wsspcomn.longconfname.set(fullNameWithSalute);
		} else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(clntpf.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(clntpf.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(clntpf.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
		/* PAYEE-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = clntpf.getLgivname();
		String lastName = clntpf.getLsurname();
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processLife1100()
	{
				/*    Set up the LIFE details from the previously read LIFEENQ*/
		/*    record.*/
		sv.subfileFields.set(SPACES);
		sv.hlifeno.set(lifepf.getLife());
		sv.hlifcnum.set(lifepf.getLifcnum());
		
		if (isEQ(lifepf.getJlife(),ZERO)) { //ILIFE-7791
			
		sv.linetype.set(wsaaMiscellaneousInner.lifeType);
		sv.cmpntnum.set(lifepf.getLife());
		resetCmpntnumIfJLifeExists();
		wsaaMiscellaneousInner.wsaaStoreLifeno.set(lifepf.getLife());
		sv.component.set(lifepf.getLifcnum());
		//ILJ-388
		if(sv.component.toString().matches("[0-9]+") && sv.component.toString().length() > 0) {
			sv.instPrem.set(BigDecimal.ZERO);
			sv.instPrem.setBlankWhenZero();
		}
		clntpf.setClntnum(lifepf.getLifcnum());
		sv.hsuffix.set(ZERO);
		wsaaSubfchg.set(ZERO);
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpf = clntpfDAO.selectClient(clntpf);
		if (clntpf == null) {
			fatalError600();
		}
		/*cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		plainname();
		sv.compdesc.set(wsspcomn.longconfname);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		for(Lifepf life: lifeList) {
			if(life.getLife().equals(lifepf.getLife()) && (!(life.getJlife().equals("00")))) {
				if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
					sv.plnsfxOut[varcom.nd.toInt()].set("Y");
						sv.linetype.set(wsaaMiscellaneousInner.jlifeType);
						sv.cmpntnum.set(" +");
						sv.component.set(life.getLifcnum());
						clntpf.setClntnum(life.getLifcnum());
						sv.hsuffix.set(ZERO);
						sv.selectOut[varcom.pr.toInt()].set("Y");
						clntpf.setClntpfx("CN");
						clntpf.setClntcoy(wsspcomn.fsuco.toString());
						clntpf = clntpfDAO.selectClient(clntpf);
						if (clntpf == null) {
							fatalError600();
						}
						/*cltsIO.setFunction(varcom.readr);
						SmartFileCode.execute(appVars, cltsIO);
						if (isNE(cltsIO.getStatuz(),varcom.oK)) {
							syserrrec.params.set(cltsIO.getParams());
							fatalError600();
						}*/
						plainname();
						//sv.sumins.set(covrpf.getSumins().toPlainString());//ILJ-388
						//sv.instPrem.set(covrpf.getInstprem().toString());//ILJ-388
						sv.compdesc.set(wsspcomn.longconfname);
						scrnparams.function.set(varcom.sadd);
						processScreen("S6239", sv);
						if (isNE(scrnparams.statuz,varcom.oK)) {
							syserrrec.statuz.set(scrnparams.statuz);
							fatalError600();
						}
						sv.selectOut[varcom.pr.toInt()].set(SPACES);
				}
			}
		}
		for(Covrpf covr:covrList) {//ILIFE-7791 starts
			if(isNE(covr.getLife(),wsaaMiscellaneousInner.wsaaStoreLifeno)) {
				continue;
			}
				covrpf = covr;
				if (cntEnqFlag) {
					sv.sumins.set(covrpf.getSumins().toPlainString());//ILJ-388
					if (isEQ(payrpf.getBillfreq(), "00")) {
						sv.instPrem.set(covrpf.getSingp().setScale(0, BigDecimal.ROUND_HALF_UP)); 
						}
					else{
						sv.instPrem.set(covrpf.getInstprem().setScale(0, BigDecimal.ROUND_HALF_UP)); 
					}
				}
				sv.linetype.set(wsaaMiscellaneousInner.coverageType);
				wsaaMiscellaneousInner.wsaaStoreCoverage.set(covrpf.getCoverage());
				wsaaMiscellaneousInner.wsaaCoverage.set(covrpf.getCrtable());
				if(isEQ(covr.getRider(),"00")) {
					sv.cmpntnum.set(covrpf.getCoverage());
				}
				else {
					sv.cmpntnum.set(covrpf.getRider());
				}
				sv.hcrtable.set(covrpf.getCrtable());
				sv.component.set(wsaaMiscellaneousInner.wsaaCovrComponent);
				//ILJ-388
				if(sv.component.toString().matches("[0-9]+") && sv.component.toString().length() > 0) {
					sv.instPrem.set(BigDecimal.ZERO);
					sv.instPrem.setBlankWhenZero();
				}
				getDescription1500();
				/*    IF   POLICY-LEVEL-ENQUIRY                                    */
				covrStatusDescs1600();
				/*    Store the COVRENQ key for later use.*/
				sv.hlifeno.set(covrpf.getLife());
				sv.hsuffix.set(covrpf.getPlanSuffix());
				sv.hcoverage.set(covrpf.getCoverage());
				sv.hrider.set(covrpf.getRider());
				scrnparams.function.set(varcom.sadd);
				processScreen("S6239", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
	}
////ILIFE-7791 ends
		
		/*	ILIFE-6967	*/
		if(lifeIterator.hasNext()) {
			lifepf = lifeIterator.next();
		}
		else {
			loopflag=false;
		}
		/*	ILIFE-6967	*/
	}

protected void para1100()
{
	/*    If a COVRENQ record has been retrieved then the program must*/
	/*    have been invoked from the Policy Selection screen. The*/
	/*    record is then released and processing should continue as*/
	/*    for a Plan Level Enquiry. The following BEGN will position*/
	/*    this program at the first COVRENQ record for th selected*/
	/*    policy.*/
	/*    MOVE RLSE                   TO COVRENQ-FUNCTION.             */
	/*    CALL 'COVRENQIO'         USING COVRENQ-PARAMS.               */
	/*    IF   COVRENQ-STATUZ      NOT = O-K  AND                      */
	/*         COVRENQ-STATUZ      NOT = MRNF                          */
	/*         MOVE COVRENQ-PARAMS    TO SYSR-PARAMS                   */
	/*         PERFORM 600-FATAL-ERROR.                                */
	/*    MOVE COVRENQ-PLAN-SUFFIX    TO S6239-PLAN-SUFFIX.            */
	/*    IF   COVRENQ-PLAN-SUFFIX NOT > CHDRENQ-POLSUM                */
	/*         MOVE ZERO              TO COVRENQ-PLAN-SUFFIX.          */
	/* MOVE LIFEENQ-LIFE           TO COVRENQ-LIFE.            <002>*/
	/* GO TO 1111-COVRENQIO.                                        */
	//sv.planSuffix.set(covrenqIO.getPlanSuffix());

	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	sv.planSuffix.set(covrpf.getPlanSuffix());
	
	if (isLTE(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
		covrpf.setPlanSuffix(0);
	}
	goTo(GotoLabel.covrenqio1111);
}

protected void covrenqio1111()
	{
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	/*    If the LIFEENQ record just read was for Joint Life details*/
	/*    on the current LIFE then display the details and read the*/
	/*    next LIFEENQ record.*/
	sv.linetype.set(wsaaMiscellaneousInner.jlifeType);
	sv.cmpntnum.set(" +");
	sv.component.set(lifepf.getLifcnum());
	clntpf.setClntnum(lifepf.getLifcnum());
	sv.hsuffix.set(ZERO);
	/*    Protect the action field as a joint life has been added.*/
	sv.selectOut[varcom.pr.toInt()].set("Y");
	clntpf.setClntpfx("CN");
	clntpf.setClntcoy(wsspcomn.fsuco.toString());
	clntpf = clntpfDAO.selectClient(clntpf);
	if (clntpf == null) {
		fatalError600();
	}
	/*cltsIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, cltsIO);
	if (isNE(cltsIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(cltsIO.getParams());
		fatalError600();
	}*/
	plainname();
	sv.compdesc.set(wsspcomn.longconfname);
	scrnparams.function.set(varcom.sadd);
	processScreen("S6239", sv);
	if (isNE(scrnparams.statuz,varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*    Unprotect the action field as a joint life has been added.*/
	sv.selectOut[varcom.pr.toInt()].set(SPACES);
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	if(lifeIterator.hasNext()) {
		lifepf = lifeIterator.next();
	}
}


	/**
	* <pre>
	*     GET THE DESCRIPTION FOR THE COVERAGE/RIDER
	* </pre>
	*/
protected void getDescription1500()
	{
		para1500();
	}

protected void para1500()
	{
		/* Read the contract definition description from table*/
		/* T5687 for the contract held on the client header record.*/
		//ILB-498 starts
		descpf=descDAO.getdescData("IT", t5687, covrpf.getCrtable(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.compdesc.fill("?");
		}
		else {
			sv.compdesc.set(descpf.getLongdesc());
		}
			
		/*descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.compdesc.fill("?");
		}
		else {
			sv.compdesc.set(descIO.getLongdesc());
		}*/
		//ILB-498 ends
	}

protected void covrStatusDescs1600()
	{
		para1600();
	}

protected void para1600()
	{
		/*    Use the Risk Status and Premium Status codes to read T5681*/
		/*    and T5682 and obtain the short descriptions.*/
		//ILB-498 starts
		descpf=descDAO.getdescData("IT", t5681, covrpf.getPstatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.pstatcode.fill("?");
		}
		else {
			sv.pstatcode.set(descpf.getLongdesc());
		}
		
		descpf=descDAO.getdescData("IT", t5682, covrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.statcode.fill("?");
		}
		else {
			sv.statcode.set(descpf.getLongdesc());
		}
		//ILB-498 ends
	}

protected void checkSda1700()
	{
		sda1700();
	}

protected void sda1700()
	{
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VUSER");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.statuz.set(varcom.oK);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isEQ(sdasancrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sdasancrec.statuz);
			syserrrec.params.set(sdasancrec.sancRec);
			fatalError600();
		}
		if (isEQ(sdasancrec.rflag,"X")
		&& isEQ(sdasancrec.statuz,varcom.oK)) {
			wsaaRestrictUser.set("Y");
		}
		else {
			wsaaRestrictUser.set("N");
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo12010();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*2000-PARA.                                                       
	* </pre>
	*/
protected void screenIo12010()
	{
		/*    CALL 'S6239IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   S6239-DATA-AREA               */
		/*                                   S6239-SUBFILE-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SCREEN*/
		/*2050-CHECK-FOR-ERRORS.*/
		/*    IF S6239-ERROR-INDICATORS NOT = SPACES*/
		/*       MOVE 'Y'                 TO WSSP-EDTERROR.*/
		scrnparams.subfileRrn.set(1);
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/*    These options are hard-coded on the screen display. If new*/
		/*    options are introduced then both the screen and this check*/
		/*    should be changed.*/
		if (isEQ(sv.select,"1")
		|| isEQ(sv.select,"2")
		|| isEQ(sv.select,"3")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.selectErr.set(e005);
			//ILIFE-1228 vchawda
			sv.select.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (restrictedUser.isTrue()) {
			if (isEQ(sv.select,"3")) {
				sv.selectErr.set(e005);
				//ILIFE-1228 vchawda
				sv.select.set(SPACES);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		if (isEQ(sv.select,"1")
		&& isEQ(sv.linetype, wsaaMiscellaneousInner.lifeType)||isEQ(sv.linetype,wsaaMiscellaneousInner.jlifeType)) { //ILIFE-7791
			/*NEXT_SENTENCE*/
		}
		else {
			if ((isEQ(sv.select,"1")
			|| isEQ(sv.select,"2")
			|| isEQ(sv.select,"3"))
			&& (isEQ(sv.linetype, wsaaMiscellaneousInner.coverageType)
			|| isEQ(sv.linetype, wsaaMiscellaneousInner.riderType))) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.selectErr.set(e005);
				//ILIFE-1228 vchawda
				sv.select.set(SPACES);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		if (isNE(sv.select,"2")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/*    If control reaches this point then option 2 has been chosen*/
		/*    and the program must check that a UTRS record exists.*/
		/*    First perform a BEGN  on the selected UTRS record. This will*/
		/*    return the first UTRS record for the contract, life, policy*/
		/*    and component.*/
		//ILB-498 starts
		//utrsIO.setDataArea(SPACES);
		//utrsIO.setFunction(varcom.begn);
		utrspf.setChdrcoy(wsspcomn.company.toString());
		utrspf.setChdrnum(chdrpf.getChdrnum());
		utrspf.setLife(sv.hlifeno.toString());
		utrspf.setCoverage(sv.hcoverage.toString());
		utrspf.setRider(sv.hrider.toString());
		utrspf.setPlanSuffix(sv.hsuffix.toInt());
		utrspfList = utrspfDAO.searchUtrsRecord(utrspf);
		for (Utrspf utrspf : utrspfList) {
			if ((utrspf == null)
					|| isNE(utrspf.getChdrcoy(),wsspcomn.company)
					|| isNE(utrspf.getChdrnum(),chdrpf.getChdrnum())
					|| isNE(utrspf.getLife(),sv.hlifeno)
					|| isNE(utrspf.getCoverage(),sv.hcoverage)
					|| isNE(utrspf.getRider(),sv.hrider)
					|| isNE(utrspf.getPlanSuffix(),sv.hsuffix)) {
						/*      MOVE E494              TO S6239-SELECT-ERR.             */
						/*NEXT_SENTENCE*/
					}
					else {
						goTo(GotoLabel.updateErrorIndicators2670);
					}
			}
		
		/*SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		|| isNE(utrsIO.getChdrcoy(),wsspcomn.company)
		|| isNE(utrsIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(utrsIO.getLife(),sv.hlifeno)
		|| isNE(utrsIO.getCoverage(),sv.hcoverage)
		|| isNE(utrsIO.getRider(),sv.hrider)
		|| isNE(utrsIO.getPlanSuffix(),sv.hsuffix)) {
			      MOVE E494              TO S6239-SELECT-ERR.             
			NEXT_SENTENCE
		}
		else {
			goTo(GotoLabel.updateErrorIndicators2670);
		}*/
		
		hitspf.setChdrcoy(wsspcomn.company.toString());
		hitspf.setChdrnum(chdrpf.getChdrnum());
		hitspf.setLife(sv.hlifeno.toString());
		hitspf.setCoverage(sv.hcoverage.toString());
		hitspf.setRider(sv.hrider.toString());
		hitspf.setPlanSuffix(sv.hsuffix.toInt());
		hitspfList = hitspfDAO.searchHitsRecord(hitspf);
		if ((hitspfList.isEmpty()) //ILIFE-8350
						|| isNE(hitspf.getChdrcoy(),wsspcomn.company)
						|| isNE(hitspf.getChdrnum(),chdrpf.getChdrnum())
						|| isNE(hitspf.getLife(),sv.hlifeno)
						|| isNE(hitspf.getCoverage(),sv.hcoverage)
						|| isNE(hitspf.getRider(),sv.hrider)
						|| isNE(hitspf.getPlanSuffix(),sv.hsuffix)) {
							sv.selectErr.set(e494);
							sv.select.set(SPACES);
						}
	
		/*hitsIO.setParams(SPACES);
		hitsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		|| isNE(hitspf.getChdrcoy(),wsspcomn.company)
		|| isNE(hitspf.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(hitspf.getLife(),sv.hlifeno)
		|| isNE(hitspf.getCoverage(),sv.hcoverage)
		|| isNE(hitspf.getRider(),sv.hrider)
		|| isNE(hitspf.getPlanSuffix(),sv.hsuffix)) {
			sv.selectErr.set(e494);
			//ILIFE-1228 vchawda
			sv.select.set(SPACES);
		}*/
		//ILB-498 ends
	}

protected void updateErrorIndicators2670()
	{
		/*    IF   S6239-ERROR-SUBFILE NOT = SPACES                        */
		/*         MOVE 'Y'               TO WSSP-EDTERROR                 */
		/*    ELSE                                                         */
		/*         MOVE O-K               TO WSSP-EDTERROR                 */
		/*         GO TO 2680-READ-NEXT-MODIFIED-RECORD.                   */
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaSubfchg.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  Update database files as required*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
				case bypassStart4010: 
					bypassStart4010();
				case nextProgram4080: 
					nextProgram4080();
				case nextProgram4085: 
					nextProgram4085();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			sv.select.set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
			goTo(GotoLabel.bypassStart4010);
		}
		/*    Re-start the subfile.*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*    If in Policy Level enquiry the Plan Suffix from the selected
	*    Policy is placed in the COVRENQ record before it is saved.
	* </pre>
	*/
protected void bypassStart4010()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4400();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			wsaaSubfchg.set(0);
			goTo(GotoLabel.nextProgram4085);
		}
		/*    If SCRN-STATUZ is not ENDP then a selection must have been*/
		/*    found. The next 8 programs currently in the stack are saved.*/
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		if ((isEQ(sv.linetype, wsaaMiscellaneousInner.coverageType)
		|| isEQ(sv.linetype, wsaaMiscellaneousInner.riderType))
		&& isEQ(sv.select,"1")) {
			covRiderEnquiry4500();
			goTo(GotoLabel.nextProgram4080);
		}
		if ((isEQ(sv.linetype, wsaaMiscellaneousInner.coverageType)
		|| isEQ(sv.linetype, wsaaMiscellaneousInner.riderType))
		&& isEQ(sv.select,"2")) {
			unitEnquiry4800();
			goTo(GotoLabel.nextProgram4080);
		}
		if ((isEQ(sv.linetype, wsaaMiscellaneousInner.coverageType)
		|| isEQ(sv.linetype, wsaaMiscellaneousInner.riderType))
		&& isEQ(sv.select,"3")) {
			reassuranceEnquiry4900();
			goTo(GotoLabel.nextProgram4080);
		}
		/*   If control reaches here then a life must have been selected.*/
		/*   Firstly release any previously held LIFEENQ, CLTS and CHDRENQ*/
		/*   records.*/
		/*   Then use the key to perform a READS on LIFELNB so that the*/
		/*   next program will be able to simply retrieve the details.*/
		lifelnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}

		clntpfDAO.deleteCacheObject(clntpf);
		
		lifelnbIO.setChdrcoy(chdrpf.getChdrcoy());
		lifelnbIO.setChdrnum(chdrpf.getChdrnum());
		lifelnbIO.setLife(sv.hlifeno);
		if(isEQ(sv.linetype,wsaaMiscellaneousInner.lifeType)){//ILIFE-7791
		lifelnbIO.setJlife("00");
		}
		else if (isEQ(sv.linetype,wsaaMiscellaneousInner.jlifeType)){
			lifelnbIO.setJlife("01");
		}
		checkForJointLifeExistence();
		lifelnbIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}

		//ILB-498
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(lifelnbIO.getLifcnum().toString());
		clntpf = clntpfDAO.selectClient(clntpf);
		if (clntpf == null) {
			fatalError600();
		}
		/*cltsIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrDao.setCacheObject(chdrpf);
		/*   The generalised secondary switching module is called to get*/
		/*   the next 8 programs and load them into the program stack.*/
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("A");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/*  ELSE*/
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			goTo(GotoLabel.nextProgram4080);
		}
		wsspcomn.flag.set("I");
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
	}

protected void checkForJointLifeExistence() {}

protected void resetCmpntnumIfJLifeExists() {}


protected void nextProgram4080()
	{
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*       MOVE V045              TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}

protected void nextProgram4085()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void readSubfile4400()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S6239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void covRiderEnquiry4500()
	{
		para4500();
	}

protected void para4500()
	{
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	covrpf = covrDao.getCovrRecord(wsspcomn.company.toString(), chdrpf.getChdrnum(), sv.hlifeno.toString(), sv.hcoverage.toString(), sv.hrider.toString(), sv.hsuffix.toInt(), "1");
	covrDao.setCacheObject(covrpf);
	/*    Lookup the programs required to process the Coverage/Rider*/
	/*    from table T5671.*/
	itemIO.setDataArea(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setItemtabl(t5671);
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
	stringVariable1.addExpression(sv.hcrtable);
	stringVariable1.setStringInto(itemIO.getItemitem());
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	t5671rec.t5671Rec.set(itemIO.getGenarea());
	compute(sub1, 0).set(add(wsspcomn.programPtr,1));
	sub2.set(1);
	for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
		t5671ToProgramStack4600();
	}
	for (int loopVar5 = 0; !(loopVar5 == 4); loopVar5 += 1){
		spaceFillStack4700();
	}
	/*    If Plan Level Enquiry is in progress then access to P6246,*/
	/*    the Fund Direction Enquiry, is removed. Control will then*/
	/*    only pass to Fund Direction Enquiry if an individual policy*/
	/*    is being enquired upon.*/
	if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()
	&& isNE(sv.numpols,1)) {
		compute(sub1, 0).set(add(wsspcomn.programPtr,2));
		wsspcomn.secProg[sub1.toInt()].set(SPACES);
	}
}

protected void t5671ToProgramStack4600()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(t5671rec.pgm[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void spaceFillStack4700()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(SPACES);
		sub1.add(1);
		/*EXIT*/
	}

protected void unitEnquiry4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case checkHits4850: 
					checkHits4850();
				case readCovr4860: 
					readCovr4860();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		/*    First perform a BEGN  on the selected UTRS record. This will*/
		/*    return the first UTRS record for the contract, life, policy*/
		/*    and component.*/
		//ILB-498 starts
		utrspfDAO.deleteCacheObject(utrspf);
		utrspf.setChdrcoy(wsspcomn.company.toString());
		utrspf.setChdrnum(chdrpf.getChdrnum());
		utrspf.setLife(sv.hlifeno.toString());
		utrspf.setCoverage(sv.hcoverage.toString());
		utrspf.setRider(sv.hrider.toString());
		utrspf.setPlanSuffix(sv.hsuffix.toInt());
		utrspfList = utrspfDAO.searchUtrsRecord(utrspf);
		for (Utrspf utrspf : utrspfList) {
			if ((utrspf == null)
					|| isNE(utrspf.getChdrcoy(),wsspcomn.company)
					|| isNE(utrspf.getChdrnum(),chdrpf.getChdrnum())
					|| isNE(utrspf.getLife(),sv.hlifeno)
					|| isNE(utrspf.getCoverage(),sv.hcoverage)
					|| isNE(utrspf.getRider(),sv.hrider)
					|| isNE(utrspf.getPlanSuffix(),sv.hsuffix)) {
							wsaaRecordFound.set("N");
							goTo(GotoLabel.checkHits4850);
				}
		}
		/*      MOVE UTRS-PARAMS       TO SYSR-PARAMS                   */
		/*      PERFORM 600-FATAL-ERROR.                                */
		/*    If in Policy Level enquiry the Plan Suffix from the selected*/
		/*    Policy is placed in the UTRS record before it is saved.*/
		wsaaRecordFound.set("Y");
		if (wsaaMiscellaneousInner.policyLevelEnquiry.isTrue()) {
			utrspf.setPlanSuffix(sv.planSuffix.toInt());
		}
		utrspfDAO.setCacheObject(utrspf);
		//ILB-498 ends
	}

protected void checkHits4850()
	{
		//ILB-498
		hitspfDAO.deleteCacheObject(hitspf);
		hitspf.setChdrcoy(wsspcomn.company.toString());
		hitspf.setChdrnum(chdrpf.getChdrnum());
		hitspf.setLife(sv.hlifeno.toString());
		hitspf.setCoverage(sv.hcoverage.toString());
		hitspf.setRider(sv.hrider.toString());
		hitspf.setPlanSuffix(sv.hsuffix.toInt());
		hitspfList = hitspfDAO.searchHitsRecord(hitspf);
		for (Hitspf hitspf : hitspfList) {
			if ((hitspf == null)
				|| isNE(hitspf.getChdrcoy(),wsspcomn.company)
				|| isNE(hitspf.getChdrnum(),chdrpf.getChdrnum())
				|| isNE(hitspf.getLife(),sv.hlifeno)
				|| isNE(hitspf.getCoverage(),sv.hcoverage)
				|| isNE(hitspf.getRider(),sv.hrider)
				|| isNE(hitspf.getPlanSuffix(),sv.hsuffix)) {
					if (recordFound.isTrue()) {
						goTo(GotoLabel.readCovr4860);
					}
					else {
						fatalError600();
					}
			}
		}
		if (wsaaMiscellaneousInner.policyLevelEnquiry.isTrue()) {
			hitspf.setPlanSuffix(sv.planSuffix.toInt());
		}
		hitspfDAO.setCacheObject(hitspf);
		
		/*hitsIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		hitsIO.setParams(SPACES);
		hitsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		|| isNE(hitsIO.getChdrcoy(),wsspcomn.company)
		|| isNE(hitsIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(hitsIO.getLife(),sv.hlifeno)
		|| isNE(hitsIO.getCoverage(),sv.hcoverage)
		|| isNE(hitsIO.getRider(),sv.hrider)
		|| isNE(hitsIO.getPlanSuffix(),sv.hsuffix)) {
			if (recordFound.isTrue()) {
				goTo(GotoLabel.readCovr4860);
			}
			else {
				syserrrec.params.set(hitsIO.getParams());
				fatalError600();
			}
		}
		
		hitsIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}*/
	}

protected void readCovr4860()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrpf = covrDao.getCovrRecord(wsspcomn.company.toString(), chdrpf.getChdrnum(), sv.hlifeno.toString(), sv.hcoverage.toString(), sv.hrider.toString(), sv.hsuffix.toInt(), "1");
		covrDao.setCacheObject(covrpf);
		/*   The generalised secondary switching module is called to get*/
		/*   the next 8 programs and load them into the program stack.*/
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("B");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/*  ELSE*/
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			return ;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			loadProgramStack4300();
		}
}

protected void reassuranceEnquiry4900()
	{
			para4900();
		}

protected void para4900()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrpf = covrDao.getCovrRecord(wsspcomn.company.toString(), chdrpf.getChdrnum(), sv.hlifeno.toString(), sv.hcoverage.toString(), sv.hrider.toString(), sv.hsuffix.toInt(), "1");
		covrDao.setCacheObject(covrpf);
		/*   Call GENSSW with a function of 'C'.                          */
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("C");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/*  ELSE                                                         */
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			return ;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			loadProgramStack4300();
		}
}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
protected static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	public Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	public FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	public FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
	public FixedLengthStringData lifeType = new FixedLengthStringData(1).init("L");
	private FixedLengthStringData jlifeType = new FixedLengthStringData(1).init("J");
	public FixedLengthStringData coverageType = new FixedLengthStringData(1).init("C");
	private FixedLengthStringData riderType = new FixedLengthStringData(1).init("R");
	private FixedLengthStringData wsaaStoreLifcnum = new FixedLengthStringData(8);
	public FixedLengthStringData wsaaStoreLifeno = new FixedLengthStringData(2);
	private PackedDecimalData wsaaStoreSuffix = new PackedDecimalData(5, 0);
	public FixedLengthStringData wsaaStoreCoverage = new FixedLengthStringData(2);
}

	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
}
