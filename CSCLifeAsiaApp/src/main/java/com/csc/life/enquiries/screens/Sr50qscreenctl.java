package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50qscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr50qscreensfl";
		lrec.subfileClass = Sr50qscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 10;
		lrec.pageSubfile = 9;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50qScreenVars sv = (Sr50qScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50qscreenctlWritten, sv.Sr50qscreensflWritten, av, sv.sr50qscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50qScreenVars screenVars = (Sr50qScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.trandateDisp.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.currency.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifedesc.setClassString("");
		screenVars.jlifedesc.setClassString("");
		screenVars.trancd.setClassString("");
		screenVars.trandesc.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.optdsc01.setClassString("");
		screenVars.optdsc05.setClassString("");
		screenVars.optdsc02.setClassString("");
		screenVars.optdsc06.setClassString("");
		screenVars.optdsc03.setClassString("");
		screenVars.optdsc04.setClassString("");
		screenVars.optdsc07.setClassString("");
		screenVars.optdsc08.setClassString("");
	}

/**
 * Clear all the variables in Sr50qscreenctl
 */
	public static void clear(VarModel pv) {
		Sr50qScreenVars screenVars = (Sr50qScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.chdrstatus.clear();
		screenVars.lifenum.clear();
		screenVars.jlife.clear();
		screenVars.tranno.clear();
		screenVars.trandateDisp.clear();
		screenVars.trandate.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.premstatus.clear();
		screenVars.currency.clear();
		screenVars.register.clear();
		screenVars.lifedesc.clear();
		screenVars.jlifedesc.clear();
		screenVars.trancd.clear();
		screenVars.trandesc.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.crtuser.clear();
		screenVars.optdsc01.clear();
		screenVars.optdsc05.clear();
		screenVars.optdsc02.clear();
		screenVars.optdsc06.clear();
		screenVars.optdsc03.clear();
		screenVars.optdsc04.clear();
		screenVars.optdsc07.clear();
		screenVars.optdsc08.clear();
	}
}
