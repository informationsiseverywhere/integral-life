package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:35
 * Description:
 * Copybook name: UNDRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData undrKey = new FixedLengthStringData(64).isAPartOf(undrFileKey, 0, REDEFINE);
  	public FixedLengthStringData undrClntnum = new FixedLengthStringData(8).isAPartOf(undrKey, 0);
  	public FixedLengthStringData undrCoy = new FixedLengthStringData(1).isAPartOf(undrKey, 8);
  	public FixedLengthStringData undrCurrcode = new FixedLengthStringData(3).isAPartOf(undrKey, 9);
  	public FixedLengthStringData undrChdrnum = new FixedLengthStringData(8).isAPartOf(undrKey, 12);
  	public FixedLengthStringData undrCrtable = new FixedLengthStringData(4).isAPartOf(undrKey, 20);
  	public FixedLengthStringData undrLrkcls01 = new FixedLengthStringData(4).isAPartOf(undrKey, 24);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(undrKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}