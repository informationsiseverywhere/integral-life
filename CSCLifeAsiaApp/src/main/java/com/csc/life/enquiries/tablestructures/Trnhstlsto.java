package com.csc.life.enquiries.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:28
 * Description:
 * Copybook name: TRNHSTLSTO
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Trnhstlsto extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(5639);
  	public FixedLengthStringData messageHeader = new FixedLengthStringData(30).isAPartOf(rec, 0);
  	public FixedLengthStringData msgid = new FixedLengthStringData(10).isAPartOf(messageHeader, 0);
  	public ZonedDecimalData msglng = new ZonedDecimalData(5, 0).isAPartOf(messageHeader, 10).setUnsigned();
  	public ZonedDecimalData msgcnt = new ZonedDecimalData(5, 0).isAPartOf(messageHeader, 15).setUnsigned();
  	public FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(messageHeader, 20, FILLER);
  	public FixedLengthStringData messageData = new FixedLengthStringData(5609).isAPartOf(rec, 30);
  	public FixedLengthStringData bgenS6233 = new FixedLengthStringData(5609).isAPartOf(messageData, 0);
  	public FixedLengthStringData[] bgenS6233Sfl = FLSArrayPartOfStructure(50, 112, bgenS6233, 0);
  	public FixedLengthStringData[] bgenS6233Fillh = FLSDArrayPartOfArrayStructure(3, bgenS6233Sfl, 0);
  	public FixedLengthStringData[] bgenS6233Filll = FLSDArrayPartOfArrayStructure(3, bgenS6233Sfl, 3);
  	public FixedLengthStringData[] bgenS6233Hflag = FLSDArrayPartOfArrayStructure(1, bgenS6233Sfl, 6);
  	public FixedLengthStringData[] bgenS6233Hreason = FLSDArrayPartOfArrayStructure(50, bgenS6233Sfl, 7);
  	public ZonedDecimalData[] bgenS6233Htxdate = ZDArrayPartOfArrayStructure(8, 0, bgenS6233Sfl, 57, UNSIGNED_TRUE);
  	public FixedLengthStringData[] filler1 = FLSDArrayPartOfArrayStructure(8, bgenS6233Htxdate, 0, FILLER_REDEFINE);
  	public ZonedDecimalData[] bgenS6233HtxdateCcyy = ZDArrayPartOfArrayStructure(4, 0, filler1, 0, UNSIGNED_TRUE);
  	public ZonedDecimalData[] bgenS6233HtxdateMm = ZDArrayPartOfArrayStructure(2, 0, filler1, 4, UNSIGNED_TRUE);
  	public ZonedDecimalData[] bgenS6233HtxdateDd = ZDArrayPartOfArrayStructure(2, 0, filler1, 6, UNSIGNED_TRUE);
  	public FixedLengthStringData[] bgenS6233Trandesc = FLSDArrayPartOfArrayStructure(30, bgenS6233Sfl, 65);
  	public ZonedDecimalData[] bgenS6233Tranno = ZDArrayPartOfArrayStructure(5, 0, bgenS6233Sfl, 95, UNSIGNED_TRUE);
  	public FixedLengthStringData[] bgenS6233Trcode = FLSDArrayPartOfArrayStructure(4, bgenS6233Sfl, 100);
  	public ZonedDecimalData[] bgenS6233Effdate = ZDArrayPartOfArrayStructure(8, 0, bgenS6233Sfl, 104, UNSIGNED_TRUE);
  	public FixedLengthStringData[] filler2 = FLSDArrayPartOfArrayStructure(8, bgenS6233Effdate, 0, FILLER_REDEFINE);
  	public ZonedDecimalData[] bgenS6233EffdateCcyy = ZDArrayPartOfArrayStructure(4, 0, filler2, 0, UNSIGNED_TRUE);
  	public ZonedDecimalData[] bgenS6233EffdateMm = ZDArrayPartOfArrayStructure(2, 0, filler2, 4, UNSIGNED_TRUE);
  	public ZonedDecimalData[] bgenS6233EffdateDd = ZDArrayPartOfArrayStructure(2, 0, filler2, 6, UNSIGNED_TRUE);
  	public FixedLengthStringData bgenS6233Statuz = new FixedLengthStringData(4).isAPartOf(bgenS6233, 5600);
  	public ZonedDecimalData bgenS6233Rrn = new ZonedDecimalData(5, 0).isAPartOf(bgenS6233, 5604).setUnsigned();


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}