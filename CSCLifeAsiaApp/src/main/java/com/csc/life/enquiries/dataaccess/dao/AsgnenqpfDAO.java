package com.csc.life.enquiries.dataaccess.dao;

import java.util.List;

import com.csc.life.enquiries.dataaccess.model.Asgnenqpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AsgnenqpfDAO extends BaseDAO<Asgnenqpf>{
	
	public List<Asgnenqpf>	readAsgnenqData(Asgnenqpf asgnenqpf);
	
}
