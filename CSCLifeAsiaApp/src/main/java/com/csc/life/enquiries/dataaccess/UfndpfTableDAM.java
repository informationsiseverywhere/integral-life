package com.csc.life.enquiries.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UfndpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:41
 * Class transformed from UFNDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UfndpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 61;
	public FixedLengthStringData ufndrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ufndpfRecord = ufndrec;
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(ufndrec);
	public FixedLengthStringData virtualFund = DD.vrtfund.copy().isAPartOf(ufndrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(ufndrec);
	public PackedDecimalData nofunts = DD.nofunts.copy().isAPartOf(ufndrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ufndrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ufndrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ufndrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UfndpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UfndpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UfndpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UfndpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UfndpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UfndpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UfndpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UFNDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"COMPANY, " +
							"VRTFUND, " +
							"UNITYP, " +
							"NOFUNTS, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     company,
                                     virtualFund,
                                     unitType,
                                     nofunts,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		company.clear();
  		virtualFund.clear();
  		unitType.clear();
  		nofunts.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUfndrec() {
  		return ufndrec;
	}

	public FixedLengthStringData getUfndpfRecord() {
  		return ufndpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUfndrec(what);
	}

	public void setUfndrec(Object what) {
  		this.ufndrec.set(what);
	}

	public void setUfndpfRecord(Object what) {
  		this.ufndpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ufndrec.getLength());
		result.set(ufndrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}