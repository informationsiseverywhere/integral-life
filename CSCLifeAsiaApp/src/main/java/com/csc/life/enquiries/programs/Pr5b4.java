/*
 * File: Pr5b4.java
 * Date: December 3, 2013 3:26:23 AM ICT
 * Author: CSC
 * 
 * Class transformed from Pr5b4.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.List;

import com.csc.life.enquiries.screens.Sr5b4ScreenVars;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
****************************************************************** ****
* </pre>
*/
public class Pr5b4 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5B4");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private FixedLengthStringData wsaaTableToRead = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaItemToRead = new FixedLengthStringData(8);
	
		/* TABLES */
	private static final String tr52b = "TR52B";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private static final String itemrec = "ITEMREC";
	private static final String descrec = "DESCREC";
	private static final String t5688 = "T5688";
	NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	Nlgtpf nlgtpf = new Nlgtpf();
	DescpfDAO descpfDAO =  DAOFactory.getDescpfDAO();
	Descpf descpf = new Descpf();
	Chdrpf chdrpf = new Chdrpf();
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Sr5b4ScreenVars sv = ScreenProgram.getScreenVars( Sr5b4ScreenVars.class);
	List<Nlgtpf> nlgtpflist = new ArrayList<Nlgtpf>();
	List<Descpf> descpflist = new ArrayList<Descpf>();
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);

	public Pr5b4() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5b4", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		
		chdrpf= chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString());
		if(chdrpf != null)
		{
			sv.chdrnum.set(chdrpf.getChdrnum());
		}
		descpflist =descDAO.getItemByDescItem("IT", wsspcomn.company.toString(),t5688, wsspcomn.chdrCnttype.toString(), wsspcomn.language.toString());
		sv.cnttype.set(wsspcomn.chdrCnttype.value());
		boolean itemFound = false;
		for (Descpf descItem : descpflist) {
			if (descItem.getDescitem().trim().equals( wsspcomn.chdrCnttype.toString().trim())){
				sv.ctypedes.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}		
		
		if (isEQ(itemFound,false)) {
		
			sv.ctypedes.fill("?");
		}
		
		//nlgtpf= nlgtpfDAO.getNlgtRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		nlgtpf=nlgtpfDAO.getnlgtran(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(),wsspcomn.confirmationKey.toString());
		if(nlgtpf != null)
		{
			sv.tranamt01.set(nlgtpf.getAmnt01());
			sv.tranamt02.set(nlgtpf.getAmnt02());
			sv.tranamt03.set(nlgtpf.getAmnt03());
			sv.tranamt04.set(nlgtpf.getAmnt04());
			sv.nlgbal.set(nlgtpf.getNlgbal());
		}
			}


protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/**    Validate fields*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required / WSSP*/
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(wsaaProg);
		/*EXIT*/
	}
}
