package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:33
 * Description:
 * Copybook name: CHKRLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chkrlrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRec = new FixedLengthStringData(156);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(parmRec, 0);
  	public FixedLengthStringData params = new FixedLengthStringData(152).isAPartOf(parmRec, 4);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(params, 0);
  	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(params, 4);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(params, 12);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(params, 13);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(params, 16);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(params, 19);
  	public FixedLengthStringData lrkclss = new FixedLengthStringData(20).isAPartOf(params, 23);
  	public FixedLengthStringData[] lrkcls = FLSArrayPartOfStructure(5, 4, lrkclss, 0);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(params, 43);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(params, 52);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(params, 60);
  	public FixedLengthStringData filler = new FixedLengthStringData(90).isAPartOf(params, 62, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(parmRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}