/*
 * File: P6259lex.java
 * Date: 30 August 2009 0:41:04
 * Author: Quipoz Limited
 * 
 * Class transformed from P6259LEX.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  This  subroutine  is  called  from  OPTSWCH Table T1661. It
*  determines whether any LEXT records exist for the component
*  being enquired upon in P6259.
*
*****************************************************************
* </pre>
*/
public class P6259lex extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "P6259LEX";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String lextrec = "LEXTREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Contract Enquiry - Coverage Details.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		errorProg610
	}

	public P6259lex() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			performs0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void performs0010()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrpf =  covrDao.getCacheObject(covrpf);
		if(!(covrpf.getUniqueNumber()>0)){
			fatalError600();
		}
		lextIO.setChdrcoy(covrpf.getChdrcoy());
		lextIO.setChdrnum(covrpf.getChdrnum());
		lextIO.setLife(covrpf.getLife());
		lextIO.setCoverage(covrpf.getCoverage());
		lextIO.setRider(covrpf.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covrpf.getChdrcoy(),lextIO.getChdrcoy())
		|| isNE(covrpf.getChdrnum(),lextIO.getChdrnum())
		|| isNE(covrpf.getLife(),lextIO.getLife())
		|| isNE(covrpf.getCoverage(),lextIO.getCoverage())
		|| isNE(covrpf.getRider(),lextIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(),varcom.oK)) {
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit090);
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
