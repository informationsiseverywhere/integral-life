package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR50O
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz.
 */
public class Sr50oScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData annivProcDate = DD.cbanpr.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,31);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,33);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData crtabled = DD.crtabled.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,75);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,105);
	public FixedLengthStringData jlifedesc = DD.jlifedesc.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,162);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,212);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,221);
	public FixedLengthStringData optdscs = new FixedLengthStringData(60).isAPartOf(dataFields, 222);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(4, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optinds = new FixedLengthStringData(4).isAPartOf(dataFields, 282);
	public FixedLengthStringData[] optind = FLSArrayPartOfStructure(4, 1, optinds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(optinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01 = DD.optind.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optind02 = DD.optind.copy().isAPartOf(filler1,1);
	public FixedLengthStringData optind03 = DD.optind.copy().isAPartOf(filler1,2);
	public FixedLengthStringData optind04 = DD.optind.copy().isAPartOf(filler1,3);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,286);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,294);
	public ZonedDecimalData rcdate = DD.rcdate.copyToZonedDecimal().isAPartOf(dataFields,304);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,312);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,320);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(dataFields,322);
	public ZonedDecimalData rerateFromDate = DD.rrtfrm.copyToZonedDecimal().isAPartOf(dataFields,330);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,338);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,355);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,370);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,387);
	/*BRD-306 STARTS*/
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 404);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 421);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 438);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 455);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 472);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,489);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,492);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,495);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,498);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,501);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,518);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,520);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,533);
	/*BRD-306 END*/
	public FixedLengthStringData waitperiod = DD.waitperiod.copy().isAPartOf(dataFields,536);
	public FixedLengthStringData bentrm = DD.bentrm.copy().isAPartOf(dataFields,539);
	public FixedLengthStringData poltyp = DD.zpoltyp.copy().isAPartOf(dataFields,541);
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,542);
	public ZonedDecimalData benCessDate = DD.bcesdte.copyToZonedDecimal().isAPartOf(dataFields,543);
	public ZonedDecimalData benCessAge = DD.bcessage.copyToZonedDecimal().isAPartOf(dataFields,551);
	public ZonedDecimalData benCessTerm = DD.bcesstrm.copyToZonedDecimal().isAPartOf(dataFields,554);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(dataFields,557);//BRD-009
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,559);//ILIFE-3509
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,576);//BRD-NBP-011
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cbanprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crtabledErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jlifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(4, 4, optdscsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData optindsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData[] optindErr = FLSArrayPartOfStructure(4, 4, optindsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(optindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optind02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData optind03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData optind04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rcdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData rrtfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	/*BRD-306 STARTS*/
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData anbAtCcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);//ILIFE-3879
	/*BRD-306 END*/
	public FixedLengthStringData waitperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData bentrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData poltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData bcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData bcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData bcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);//BRD-009
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);//BRD-NBP-011
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cbanprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crtabledOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jlifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(4, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(48).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData optindsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 264);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(4, 12, optindsOut, 0);
	public FixedLengthStringData[][] optindO = FLSDArrayPartOfArrayStructure(12, 1, optindOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(48).isAPartOf(optindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optind01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] optind02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] optind03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] optind04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rcdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] rrtfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	/*BRD-306 STARTS*/
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	
	public FixedLengthStringData[] anbAtCcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);//ILIFE-3879
	
	public FixedLengthStringData[]waitperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[]bentrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[]poltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] bcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] bcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] bcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	/*BRD-306 END*/
	public FixedLengthStringData[]statcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);//BRD-009
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);//BRD-NBP-011
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData annivProcDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rcdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateFromDateDisp = new FixedLengthStringData(10);

	public LongData Sr50oscreenWritten = new LongData(0);
	public LongData Sr50oprotectWritten = new LongData(0);
	public FixedLengthStringData benCessDateDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return false;
	}


	public Sr50oScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(optind01Out,new String[] {"10","12","-10","11",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {"13","15","-13","14",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind03Out,new String[] {"16","18","-16","17",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind04Out,new String[] {"19","21","-19","20",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
	//ILIFE-3399-STARTS
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "43", null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "44", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "45", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		/*BRD-306 Starts*/
		fieldIndMap.put(waitperiodOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentrmOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(poltypOut,new String[] {null, null, null, "56", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {null, null, null, "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcessageOut,new String[] {"22","24","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesstrmOut,new String[] {"23","25","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		/*BRD-306 END*/
		fieldIndMap.put(mortclsOut,new String[] {"26","06","-26","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(statcodeOut,new String[] {null, "58", null, "59", null, null, null, null, null, null, null, null});//BRD-009
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "60", null, null, null, null, null, null, null, null});//ILIFE-3509
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		//screenFields = new BaseData[] {chdrnum, chdrstatus, lifenum, life, crtable, billfreq, singlePremium, instPrem, zlinstprem, cnttype, ctypdesc, premstatus, lifedesc, coverage, rider, crtabled, mop, mortcls, rcdate, crrcd, premcess, rerateDate, rerateFromDate, annivProcDate, optdsc01, optind01, optdsc02, optind02, optdsc03, optind03, optdsc04, optind04, sumin, riskCessDate, jlife, jlifedesc, taxamt,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, riskCessAge, riskCessTerm, premCessAge, premCessTerm,liencd,zagelit,anbAtCcd,waitperiod,bentrm,poltyp,prmbasis, benCessAge, benCessTerm, benCessDate,statcode,zstpduty01,dialdownoption};/*BRD-306 */
		screenFields = getscreenFields();
		//screenOutFields = new BaseData[][] {chdrnumOut, chdrstatusOut, lifenumOut, lifeOut, crtableOut, billfreqOut, singprmOut, instprmOut, zlinstpremOut, cnttypeOut, ctypdescOut, premstatusOut, lifedescOut, coverageOut, riderOut, crtabledOut, mopOut, mortclsOut, rcdateOut, crrcdOut, premcessOut, rrtdatOut, rrtfrmOut, cbanprOut, optdsc01Out, optind01Out, optdsc02Out, optind02Out, optdsc03Out, optind03Out, optdsc04Out, optind04Out, suminOut, rcesdteOut, jlifenumOut, jlifedescOut, taxamtOut,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut,liencdOut,zagelitOut,anbAtCcdOut,waitperiodOut,bentrmOut,poltypOut,prmbasisOut, bcessageOut, bcesstrmOut, bcesdteOut,statcodeOut,zstpduty01Out,dialdownoptionOut };/*BRD-306 *///ILIFE-3879
		screenOutFields = getscreenOutFields();
		//screenErrFields = new BaseData[] {chdrnumErr, chdrstatusErr, lifenumErr, lifeErr, crtableErr, billfreqErr, singprmErr, instprmErr, zlinstpremErr, cnttypeErr, ctypdescErr, premstatusErr, lifedescErr, coverageErr, riderErr, crtabledErr, mopErr, mortclsErr, rcdateErr, crrcdErr, premcessErr, rrtdatErr, rrtfrmErr, cbanprErr, optdsc01Err, optind01Err, optdsc02Err, optind02Err, optdsc03Err, optind03Err, optdsc04Err, optind04Err, suminErr, rcesdteErr, jlifenumErr, jlifedescErr, taxamtErr,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr,rcessageErr, rcesstrmErr, pcessageErr,pcesstrmErr,liencdErr,zagelitErr,anbAtCcdErr,waitperiodErr,bentrmErr,poltypErr,prmbasisErr, bcessageErr, bcesstrmErr, bcesdteErr,statcodeErr,zstpduty01Err,dialdownoptionErr};/*BRD-306 *///ILIFE-3879
		screenErrFields = getscreenErrFields();
		/*screenDateFields = new BaseData[] {rcdate, crrcd, premcess, rerateDate, rerateFromDate, annivProcDate, riskCessDate,benCessDate};
		screenDateErrFields = new BaseData[] {rcdateErr, crrcdErr, premcessErr, rrtdatErr, rrtfrmErr, cbanprErr, rcesdteErr};
		screenDateDispFields = new BaseData[] {rcdateDisp, crrcdDisp, premcessDisp, rerateDateDisp, rerateFromDateDisp, annivProcDateDisp, riskCessDateDisp,benCessDateDisp};
		*/
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields(); 
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr50oscreen.class;
		protectRecord = Sr50oprotect.class;
	}
	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, chdrstatus, lifenum, life, crtable, billfreq, singlePremium, instPrem, zlinstprem, cnttype, ctypdesc, premstatus, lifedesc, coverage, rider, crtabled, mop, mortcls, rcdate, crrcd, premcess, rerateDate, rerateFromDate, annivProcDate, optdsc01, optind01, optdsc02, optind02, optdsc03, optind03, optdsc04, optind04, sumin, riskCessDate, jlife, jlifedesc, taxamt,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, riskCessAge, riskCessTerm, premCessAge, premCessTerm,liencd,zagelit,anbAtCcd,waitperiod,bentrm,poltyp,prmbasis, benCessAge, benCessTerm, benCessDate,statcode,zstpduty01,dialdownoption};  /*BRD-306 */
	}
	
	public BaseData[][] getscreenOutFields(){
		return  new BaseData[][] {chdrnumOut, chdrstatusOut, lifenumOut, lifeOut, crtableOut, billfreqOut, singprmOut, instprmOut, zlinstpremOut, cnttypeOut, ctypdescOut, premstatusOut, lifedescOut, coverageOut, riderOut, crtabledOut, mopOut, mortclsOut, rcdateOut, crrcdOut, premcessOut, rrtdatOut, rrtfrmOut, cbanprOut, optdsc01Out, optind01Out, optdsc02Out, optind02Out, optdsc03Out, optind03Out, optdsc04Out, optind04Out, suminOut, rcesdteOut, jlifenumOut, jlifedescOut, taxamtOut,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut,liencdOut,zagelitOut,anbAtCcdOut,waitperiodOut,bentrmOut,poltypOut,prmbasisOut, bcessageOut, bcesstrmOut, bcesdteOut,statcodeOut,zstpduty01Out,dialdownoptionOut };/*BRD-306 *///ILIFE-3879
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, chdrstatusErr, lifenumErr, lifeErr, crtableErr, billfreqErr, singprmErr, instprmErr, zlinstpremErr, cnttypeErr, ctypdescErr, premstatusErr, lifedescErr, coverageErr, riderErr, crtabledErr, mopErr, mortclsErr, rcdateErr, crrcdErr, premcessErr, rrtdatErr, rrtfrmErr, cbanprErr, optdsc01Err, optind01Err, optdsc02Err, optind02Err, optdsc03Err, optind03Err, optdsc04Err, optind04Err, suminErr, rcesdteErr, jlifenumErr, jlifedescErr, taxamtErr,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr,rcessageErr, rcesstrmErr, pcessageErr,pcesstrmErr,liencdErr,zagelitErr,anbAtCcdErr,waitperiodErr,bentrmErr,poltypErr,prmbasisErr, bcessageErr, bcesstrmErr, bcesdteErr,statcodeErr,zstpduty01Err,dialdownoptionErr};/*BRD-306 *///ILIFE-3879
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {rcdate, crrcd, premcess, rerateDate, rerateFromDate, annivProcDate, riskCessDate,benCessDate};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {rcdateDisp, crrcdDisp, premcessDisp, rerateDateDisp, rerateFromDateDisp, annivProcDateDisp, riskCessDateDisp,benCessDateDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {rcdateErr, crrcdErr, premcessErr, rrtdatErr, rrtfrmErr, cbanprErr, rcesdteErr};
	}

	
	public int getDataAreaSize(){
		return  1539;
	}
	
	public int getDataFieldsSize(){
		return 579;
	}
	
	public int getErrorIndicatorSize(){
		return  240;
	}
	
	public int getOutputFieldSize(){
		return 720;  
	}
}