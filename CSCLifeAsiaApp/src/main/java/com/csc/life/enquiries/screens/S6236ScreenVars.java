package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6236
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6236ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(577);
	public FixedLengthStringData dataFields = new FixedLengthStringData(257).isAPartOf(dataArea, 0);
	public FixedLengthStringData acctccy = DD.acctccy.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData entity = DD.entity.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,107);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,154);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,162);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,209);
	public FixedLengthStringData origccy = DD.origccy.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,230);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(dataFields,233);
	public FixedLengthStringData sacscoded = DD.sacscoded.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData sacstypd = DD.sacstypd.copy().isAPartOf(dataFields,247);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 257);
	public FixedLengthStringData acctccyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData origccyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData sacscodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData sacscodedErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData sacstypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData sacstypdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 337);
	public FixedLengthStringData[] acctccyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] origccyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] sacscodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] sacscodedOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] sacstypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] sacstypdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(295);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(85).isAPartOf(subfileArea, 0);
	public FixedLengthStringData bankcode = DD.bankcode.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,2);
	public FixedLengthStringData glcode = DD.glcode.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData hacccurr = DD.hacccurr.copy().isAPartOf(subfileFields,24);
	public FixedLengthStringData hcurrcd = DD.hcurrcd.copy().isAPartOf(subfileFields,27);
	public ZonedDecimalData hmhii = DD.hmhii.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData hmhli = DD.hmhli.copyToZonedDecimal().isAPartOf(subfileFields,43);
	public FixedLengthStringData rdocnum = DD.rdocnum.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData statz = DD.statz.copy().isAPartOf(subfileFields,66);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,68);
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(subfileFields,73);
	public ZonedDecimalData datesub = DD.datesub.copyToZonedDecimal().isAPartOf(subfileFields,77);//ILJ-388
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(52).isAPartOf(subfileArea, 85);
	public FixedLengthStringData bankcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData glcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hacccurrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hcurrcdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hmhiiErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hmhliErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData rdocnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData statzErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData datesubErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);//ILJ-388
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(156).isAPartOf(subfileArea, 137);
	public FixedLengthStringData[] bankcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] glcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hacccurrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hcurrcdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hmhiiOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hmhliOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] rdocnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] statzOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] datesubOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);//ILJ-388
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 293);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datesubDisp = new FixedLengthStringData(10);//ILJ-388
	

	public LongData S6236screensflWritten = new LongData(0);
	public LongData S6236screenctlWritten = new LongData(0);
	public LongData S6236screenWritten = new LongData(0);
	public LongData S6236protectWritten = new LongData(0);
	public GeneralTable s6236screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-388

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6236screensfl;
	}

	public S6236ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","20","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcodeOut,new String[] {null, null, null,"02", null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hcurrcd, hacccurr, trcode, bankcode, effdate, tranno, glcode, hmhii, hmhli, rdocnum, statz, select, datesub};
		screenSflOutFields = new BaseData[][] {hcurrcdOut, hacccurrOut, trcodeOut, bankcodeOut, effdateOut, trannoOut, glcodeOut, hmhiiOut, hmhliOut, rdocnumOut, statzOut, selectOut, datesubOut};
		screenSflErrFields = new BaseData[] {hcurrcdErr, hacccurrErr, trcodeErr, bankcodeErr, effdateErr, trannoErr, glcodeErr, hmhiiErr, hmhliErr, rdocnumErr, statzErr, selectErr, datesubErr};
		screenSflDateFields = new BaseData[] {effdate, datesub};
		screenSflDateErrFields = new BaseData[] {effdateErr, datesubErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp, datesubDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, sacscode, sacscoded, sacstyp, sacstypd, origccy, acctccy, lifenum, lifename, jlife, jlifename, entity, crtable, descrip};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, sacscodeOut, sacscodedOut, sacstypOut, sacstypdOut, origccyOut, acctccyOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, entityOut, crtableOut, descripOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, sacscodeErr, sacscodedErr, sacstypErr, sacstypdErr, origccyErr, acctccyErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, entityErr, crtableErr, descripErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6236screen.class;
		screenSflRecord = S6236screensfl.class;
		screenCtlRecord = S6236screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6236protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6236screenctl.lrec.pageSubfile);
	}
}
