package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:07
 * Description:
 * Copybook name: LIFEENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifeenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifeenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifeenqKey = new FixedLengthStringData(256).isAPartOf(lifeenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifeenqChdrcoy = new FixedLengthStringData(1).isAPartOf(lifeenqKey, 0);
  	public FixedLengthStringData lifeenqChdrnum = new FixedLengthStringData(8).isAPartOf(lifeenqKey, 1);
  	public FixedLengthStringData lifeenqLife = new FixedLengthStringData(2).isAPartOf(lifeenqKey, 9);
  	public FixedLengthStringData lifeenqJlife = new FixedLengthStringData(2).isAPartOf(lifeenqKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifeenqKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifeenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifeenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}