package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR587
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr587ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(169);
	public FixedLengthStringData dataFields = new FixedLengthStringData(89).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnam = DD.clntnam.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData ldesc = DD.ldesc.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(dataFields,68);
	public ZonedDecimalData tranamt = DD.tranamt.copyToZonedDecimal().isAPartOf(dataFields,72);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 89);
	public FixedLengthStringData clntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lrkclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tranamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 109);
	public FixedLengthStringData[] clntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lrkclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tranamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(134);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(36).isAPartOf(subfileArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData coy = DD.coy.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(subfileFields,16);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,19);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 36);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnttypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnttypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 132);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr587screensflWritten = new LongData(0);
	public LongData Sr587screenctlWritten = new LongData(0);
	public LongData Sr587screenWritten = new LongData(0);
	public LongData Sr587protectWritten = new LongData(0);
	public GeneralTable sr587screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr587screensfl;
	}

	public Sr587ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {coy, chdrnum, cnttyp, crtable, currcode, sumins};
		screenSflOutFields = new BaseData[][] {coyOut, chdrnumOut, cnttypOut, crtableOut, currcodeOut, suminsOut};
		screenSflErrFields = new BaseData[] {coyErr, chdrnumErr, cnttypErr, crtableErr, currcodeErr, suminsErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntnum, clntnam, lrkcls, ldesc, tranamt};
		screenOutFields = new BaseData[][] {clntnumOut, clntnamOut, lrkclsOut, ldescOut, tranamtOut};
		screenErrFields = new BaseData[] {clntnumErr, clntnamErr, lrkclsErr, ldescErr, tranamtErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr587screen.class;
		screenSflRecord = Sr587screensfl.class;
		screenCtlRecord = Sr587screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr587protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr587screenctl.lrec.pageSubfile);
	}
}
