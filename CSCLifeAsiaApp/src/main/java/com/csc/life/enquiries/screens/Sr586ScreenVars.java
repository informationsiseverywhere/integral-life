package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR586
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr586ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(70);
	public FixedLengthStringData dataFields = new FixedLengthStringData(38).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnam = DD.clntnam.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 38);
	public FixedLengthStringData clntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 46);
	public FixedLengthStringData[] clntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(108);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(26).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coy = DD.coy.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData sel = DD.sel.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,9);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 26);
	public FixedLengthStringData coyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData lrkclsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData selErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 46);
	public FixedLengthStringData[] coyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] lrkclsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] selOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 106);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr586screensflWritten = new LongData(0);
	public LongData Sr586screenctlWritten = new LongData(0);
	public LongData Sr586screenWritten = new LongData(0);
	public LongData Sr586protectWritten = new LongData(0);
	public GeneralTable sr586screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr586screensfl;
	}

	public Sr586ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {sel, coy, currcode, lrkcls, sumins};
		screenSflOutFields = new BaseData[][] {selOut, coyOut, currcodeOut, lrkclsOut, suminsOut};
		screenSflErrFields = new BaseData[] {selErr, coyErr, currcodeErr, lrkclsErr, suminsErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntnum, clntnam};
		screenOutFields = new BaseData[][] {clntnumOut, clntnamOut};
		screenErrFields = new BaseData[] {clntnumErr, clntnamErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr586screen.class;
		screenSflRecord = Sr586screensfl.class;
		screenCtlRecord = Sr586screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr586protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr586screenctl.lrec.pageSubfile);
	}
}
