package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:12
 * Description:
 * Copybook name: RTRNSACKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rtrnsackey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData rtrnsacFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData rtrnsacKey = new FixedLengthStringData(256).isAPartOf(rtrnsacFileKey, 0, REDEFINE);
  	public FixedLengthStringData rtrnsacRldgcoy = new FixedLengthStringData(1).isAPartOf(rtrnsacKey, 0);
  	public FixedLengthStringData rtrnsacSacscode = new FixedLengthStringData(2).isAPartOf(rtrnsacKey, 1);
  	public FixedLengthStringData rtrnsacRldgacct = new FixedLengthStringData(16).isAPartOf(rtrnsacKey, 3);
  	public FixedLengthStringData rtrnsacSacstyp = new FixedLengthStringData(2).isAPartOf(rtrnsacKey, 19);
  	public FixedLengthStringData rtrnsacOrigccy = new FixedLengthStringData(3).isAPartOf(rtrnsacKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(232).isAPartOf(rtrnsacKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(rtrnsacFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rtrnsacFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}