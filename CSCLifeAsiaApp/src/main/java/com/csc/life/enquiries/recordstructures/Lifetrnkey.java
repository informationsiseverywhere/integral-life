package com.csc.life.enquiries.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:09
 * Description:
 * Copybook name: LIFETRNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifetrnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifetrnFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifetrnKey = new FixedLengthStringData(256).isAPartOf(lifetrnFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifetrnChdrcoy = new FixedLengthStringData(1).isAPartOf(lifetrnKey, 0);
  	public FixedLengthStringData lifetrnChdrnum = new FixedLengthStringData(8).isAPartOf(lifetrnKey, 1);
  	public FixedLengthStringData lifetrnLife = new FixedLengthStringData(2).isAPartOf(lifetrnKey, 9);
  	public FixedLengthStringData lifetrnJlife = new FixedLengthStringData(2).isAPartOf(lifetrnKey, 11);
  	public PackedDecimalData lifetrnTranno = new PackedDecimalData(5, 0).isAPartOf(lifetrnKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(lifetrnKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifetrnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifetrnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}