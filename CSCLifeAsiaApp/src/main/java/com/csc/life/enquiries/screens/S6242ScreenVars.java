package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6242
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6242ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());//BRD-306 START ILIFE-6968
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0); //ILIFE-6968
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData benBillDate = DD.bbldat.copyToZonedDecimal().isAPartOf(dataFields,7);
	public FixedLengthStringData bonusInd = DD.bnusin.copy().isAPartOf(dataFields,15);
	public ZonedDecimalData annivProcDate = DD.cbanpr.copyToZonedDecimal().isAPartOf(dataFields,16);
	public ZonedDecimalData unitStatementDate = DD.cbunst.copyToZonedDecimal().isAPartOf(dataFields,24);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,56);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,58);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,96);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,126);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,143);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,190);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,200);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,202);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,249);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,257);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,258);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,262);
	public FixedLengthStringData optsmode = DD.optsmode.copy().isAPartOf(dataFields,263);
	public FixedLengthStringData payflag = DD.payflag.copy().isAPartOf(dataFields,264);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,265);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,266);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,270);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,278);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,288);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,299);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(dataFields,301);
	public ZonedDecimalData rerateFromDate = DD.rrtfrm.copyToZonedDecimal().isAPartOf(dataFields,309);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,317);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,334);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,335);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,337);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,341);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,356);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,373);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,374);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(dataFields,387);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,407);
	/*BRD-306 STARTS*/
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 424);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 441);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 458);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 475);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 492);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,509);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,510);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,513);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,516);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,519);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,522);
	/*BRD-306 END*/
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,539);//ILIFE-3421
	public ZonedDecimalData benCessDate = DD.bcesdte.copyToZonedDecimal().isAPartOf(dataFields,540);
	public ZonedDecimalData benCessAge = DD.bcessage.copyToZonedDecimal().isAPartOf(dataFields,548);
	public ZonedDecimalData benCessTerm = DD.bcesstrm.copyToZonedDecimal().isAPartOf(dataFields,551);
	public FixedLengthStringData zsredtrm = DD.zsredtrm.copy().isAPartOf(dataFields,554);//ILIFE-3685
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,555);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,558);
	public FixedLengthStringData lnkgno = DD.lnkgno.copy().isAPartOf(dataFields,559); //ILIFE-6968
	public FixedLengthStringData lnkgsubrefno = DD.lnkgsubrefno.copy().isAPartOf(dataFields,609);//ILIFE-6968
	public FixedLengthStringData tpdtype = DD.tpdtype.copy().isAPartOf(dataFields,659); //ILIFE-7118
	public FixedLengthStringData lnkgind = DD.jpjlnkind.copy().isAPartOf(dataFields, 664);
	public FixedLengthStringData aepaydet = DD.aepaydet.copy().isAPartOf(dataFields,665);
	
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,666);	//ILIFE-7746
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(dataFields,683);
	public FixedLengthStringData trcdedesc = DD.trandesc.copy().isAPartOf(dataFields,687);
	public FixedLengthStringData dateeff = DD.dateeff.copy().isAPartOf(dataFields,717);
	public FixedLengthStringData covrprpse = DD.covrprpse.copy().isAPartOf(dataFields,725);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());//ILIFE-3685 ILIFE-6968
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbldatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bnusinErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cbanprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cbunstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData optsmodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData payflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData rrtfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData zdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	/*BRD-306 STARTS*/
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	/*BRD-306 END*/
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);//ILIFE-3421
	public FixedLengthStringData bcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData bcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData bcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData zsredtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);//ILIFE-3685
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData lnkgnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);//ILIFE-6968
	public FixedLengthStringData lnkgsubrefnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);//ILIFE-6968
	public FixedLengthStringData tpdtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 256); //ILIFE-7118
	public FixedLengthStringData lnkgindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 260);
	public FixedLengthStringData aepaydetErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 264);
	
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 268);	//ILIFE-7746
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 272);
	public FixedLengthStringData trcdedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 276);
	public FixedLengthStringData dateeffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 280);
	public FixedLengthStringData covrprpseErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 284);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize()); //ILIFE-6968
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbldatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bnusinOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cbanprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cbunstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] optsmodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] payflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] rrtfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	/*BRD-306 STARTS*/
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);//ILIFE-3421
	public FixedLengthStringData[] bcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[] bcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[] bcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	/*BRD-306 END*/
	public FixedLengthStringData[] zsredtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);//ILIFE-3685
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] lnkgnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);//ILIFE-6968
	public FixedLengthStringData[] lnkgsubrefnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);//ILIFE-6968
	public FixedLengthStringData[] tpdtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 768); //ILIFE-7118
	public FixedLengthStringData[]  lnkgindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 780);

	public FixedLengthStringData[]  aepaydetOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 792);

	public FixedLengthStringData[] zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 804);	//ILIFE-7746
	
	
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 816);
    public FixedLengthStringData[] trcdedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 828);
	public FixedLengthStringData[] dateeffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 840);
	public FixedLengthStringData[] covrprpseOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 852);

	
	public FixedLengthStringData mrtaFlag = new FixedLengthStringData(1);//ILIFE-3685
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData benBillDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData annivProcDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData unitStatementDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateFromDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData benCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dateeffDisp = new FixedLengthStringData(10);//IBPLIFE-2139

	public LongData S6242screenWritten = new LongData(0);
	public LongData S6242protectWritten = new LongData(0);
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1); 
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387
	public FixedLengthStringData contnewBScreenflag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return false;
	}


	public S6242ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bbldatOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cbunstOut,new String[] {null, null, null, "34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnusinOut,new String[] {null, null, null, "34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"35","36","-35","36",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payflagOut,new String[] {"42","40","-42","40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optsmodeOut,new String[] {"41","40","-41","40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {null, null, null, "38",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdescOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"45","46","-45","46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		//ILIFE-3399-STARTS
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		fieldIndMap.put(prmbasisOut,new String[] {null, null, null, "50", null, null, null, null, null, null, null, null});//ILIFE-3421
		fieldIndMap.put(zsredtrmOut,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});//ILIFE-3685
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "73", null, null, null, null, null, null, null, null});
		fieldIndMap.put(lnkgnoOut,new String[] {null, null, null, "121", null, null, null, null, null, null, null, null}); //ILIFE-6968
		fieldIndMap.put(lnkgsubrefnoOut,new String[] {null, null, null, "122", null, null, null, null, null, null, null, null});//ILIFE-6968
		fieldIndMap.put(tpdtypeOut,new String[] {null, null, null, "77", null, null, null, null, null, null, null, null}); //ILIFE-7118
		fieldIndMap.put(lnkgindOut,new String[] {null, null, null, "62", null, null, null, null, null, null, null, null});

		fieldIndMap.put(aepaydetOut,new String[] {"21", "22", "-21", "23", null, null, null, null, null, null, null, null});

		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "63", null, null, null, null, null, null, null, null});
		fieldIndMap.put(crrcdOut,new String[] {null, null, null,"80",null, null, null, null, null, null, null, null});//ILJ-45
	//	fieldIndMap.put(rcessageOut,new String[] {null, null, null,"81",null, null, null, null, null, null, null, null});//ILJ-45
		fieldIndMap.put(rcesdteOut,new String[] {null, null, null,"82",null, null, null, null, null, null, null, null});//ILJ-45
		
       //new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, mortcls, optextind, unitStatementDate, bonusInd, pbind, payflag, optsmode, bappmeth, zdesc, zlinstprem, taxamt, taxind,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, riskCessAge, riskCessTerm, premCessAge, premCessTerm, prmbasis,benCessAge, benCessTerm, benCessDate,zsredtrm,dialdownoption,exclind,lnkgno,lnkgsubrefno,tpdtype};
	    screenFields=getscreenFields();
		//screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, rrtdatOut, rrtfrmOut, bbldatOut, mortclsOut, optextindOut, cbunstOut, bnusinOut, pbindOut, payflagOut, optsmodeOut, bappmethOut, zdescOut, zlinstpremOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut,prmbasisOut,bcessageOut, bcesstrmOut, bcesdteOut,zsredtrmOut,dialdownoptionOut,exclindOut,lnkgnoOut,lnkgsubrefnoOut,tpdtypeOut};
	    screenOutFields=getscreenOutFields();
	    //screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, mortclsErr, optextindErr, cbunstErr, bnusinErr, pbindErr, payflagErr, optsmodeErr, bappmethErr, zdescErr, zlinstpremErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr,rcessageErr, rcesstrmErr, pcessageErr, pcesstrmErr,prmbasisErr,bcessageErr, bcesstrmErr, bcesdteErr,zsredtrmErr,dialdownoptionErr,exclindErr,lnkgnoErr,lnkgsubrefnoErr,tpdtypeErr};
	    screenErrFields=getscreenErrFields();
	    screenDateFields = getscreenDateFields();
	    //new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, unitStatementDate};
		screenDateErrFields = getscreenDateErrFields();
		//new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, cbunstErr};
		screenDateDispFields = getscreenDateDispFields(); 
		//new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, rerateDateDisp, rerateFromDateDisp, benBillDateDisp, unitStatementDateDisp};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6242screen.class;
		protectRecord = S6242protect.class;
	}


	public BaseData[] getscreenFields() {

		return new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, mortcls, optextind, unitStatementDate, bonusInd, pbind, payflag, optsmode, bappmeth, zdesc, zlinstprem, taxamt, taxind,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, riskCessAge, riskCessTerm, premCessAge, premCessTerm, prmbasis,benCessAge, benCessTerm, benCessDate,zsredtrm,dialdownoption,exclind,lnkgno,lnkgsubrefno,tpdtype,lnkgind,aepaydet,zstpduty01,trcode,trcdedesc,dateeff,covrprpse};
	}

   public BaseData[][] getscreenOutFields() {
		return new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, rrtdatOut, rrtfrmOut, bbldatOut, mortclsOut, optextindOut, cbunstOut, bnusinOut, pbindOut, payflagOut, optsmodeOut, bappmethOut, zdescOut, zlinstpremOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut,prmbasisOut,bcessageOut, bcesstrmOut, bcesdteOut,zsredtrmOut,dialdownoptionOut,exclindOut,lnkgnoOut,lnkgsubrefnoOut,tpdtypeOut,lnkgindOut,aepaydetOut,zstpduty01Out,trcodeOut,trcdedescOut,dateeffOut,covrprpseOut};
	}

	public BaseData[] getscreenErrFields() {
		return new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, mortclsErr, optextindErr, cbunstErr, bnusinErr, pbindErr, payflagErr, optsmodeErr, bappmethErr, zdescErr, zlinstpremErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr,rcessageErr, rcesstrmErr, pcessageErr, pcesstrmErr,prmbasisErr,bcessageErr, bcesstrmErr, bcesdteErr,zsredtrmErr,dialdownoptionErr,exclindErr,lnkgnoErr,lnkgsubrefnoErr,tpdtypeErr,lnkgindErr,aepaydetErr,zstpduty01Err,trcodeErr,trcdedescErr,dateeffErr,covrprpseErr}; 
		
	}
 

	public BaseData[] getscreenDateFields() {
		return new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, unitStatementDate, dateeff};
	}

	public BaseData[] getscreenDateDispFields() {
		return new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, rerateDateDisp, rerateFromDateDisp, benBillDateDisp, unitStatementDateDisp, dateeffDisp};
	}

	public BaseData[] getscreenDateErrFields() {
		return new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, cbunstErr, dateeffErr};
	}

	public int getDataAreaSize(){
		return  1977;
	}
		
	
	public int getDataFieldsSize(){
		return 825;
	}
	
	public int getErrorIndicatorSize(){
		return  288;
	}
	
	public int getOutputFieldSize(){
		return 864;  
	}
	


}
