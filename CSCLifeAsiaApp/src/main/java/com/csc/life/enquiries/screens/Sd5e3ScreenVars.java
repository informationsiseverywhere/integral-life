package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sd5e3
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class Sd5e3ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(640);
	public FixedLengthStringData dataFields = new FixedLengthStringData(326).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttyp.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData chdrstatus = DD.chdrstatus_longdesc.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData premstatus = DD.premstatus_longdesc.copy().isAPartOf(dataFields,74);	
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,107);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,115);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,162);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,225);
	public ZonedDecimalData totalSurVal = DD.surval.copyToZonedDecimal().isAPartOf(dataFields,272);
	public ZonedDecimalData totalLoanVal = DD.loanval.copyToZonedDecimal().isAPartOf(dataFields,289);
	public ZonedDecimalData totBonusVal = DD.bonusval.copyToZonedDecimal().isAPartOf(dataFields,306);
	public FixedLengthStringData survalind = DD.survalind.copy().isAPartOf(dataFields,323);
	public FixedLengthStringData loanvalind = DD.loanvalind.copy().isAPartOf(dataFields,324);
	public FixedLengthStringData bonusvalind = DD.bonusvalind.copy().isAPartOf(dataFields,325);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 326);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData totalSurValErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData totalLoanValErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData totBonusValErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData survalindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData loanvalindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData bonusvalindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 412);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] totalSurValOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] totalLoanValOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] totBonusValOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] survalindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] loanvalindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] bonusvalindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(332);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(153).isAPartOf(subfileArea, 0);
	public FixedLengthStringData component = DD.component.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(subfileFields,25);
	public FixedLengthStringData pstatcode = DD.premdesc.copy().isAPartOf(subfileFields,40); 
	public FixedLengthStringData statcode = DD.riskdesc.copy().isAPartOf(subfileFields,50);
	public ZonedDecimalData hissdte = DD.hprrcvdt.copyToZonedDecimal().isAPartOf(subfileFields,60);
	public ZonedDecimalData fundamnt = DD.fundamnt.copyToZonedDecimal().isAPartOf(subfileFields,68);
	public ZonedDecimalData surrval = DD.surrval.copyToZonedDecimal().isAPartOf(subfileFields,85);
	public ZonedDecimalData loanVal = DD.loanallow.copyToZonedDecimal().isAPartOf(subfileFields,102);
	public ZonedDecimalData bounsVal = DD.bounsVal.copyToZonedDecimal().isAPartOf(subfileFields,119);
	public ZonedDecimalData sacscurbal = DD.sacscurbal.copyToZonedDecimal().isAPartOf(subfileFields,136);



	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 153);
	public FixedLengthStringData componentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hissdteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData fundamntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData surrvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData loanValErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData bounsValErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData sacscurbalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);




	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 197);
	public FixedLengthStringData[] componentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] statcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hissdteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] fundamntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] surrvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] loanValOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] bounsValOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] sacscurbalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 329);
	
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData hissdteDisp = new FixedLengthStringData(10);


	public LongData Sd5e3screensflWritten = new LongData(0);
	public LongData Sd5e3screenctlWritten = new LongData(0);
	public LongData Sd5e3screenWritten = new LongData(0);
	public LongData Sd5e3protectWritten = new LongData(0);
	public GeneralTable sd5e3screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sd5e3screensfl;
	}

	public Sd5e3ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(survalindOut,new String[] {"01","02", "-01","02", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loanvalindOut,new String[] {"03","04", "-03","04", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bonusvalindOut,new String[] {"05","06", "-05","06", null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {component,instPrem,sumin,pstatcode,statcode,hissdte,fundamnt,surrval,loanVal,bounsVal,sacscurbal};
		screenSflOutFields = new BaseData[][] {componentOut,instPremOut,suminOut,pstatcodeOut,statcodeOut,hissdteOut,fundamntOut,surrvalOut,loanValOut,bounsValOut,sacscurbalOut};
		screenSflErrFields = new BaseData[] {componentErr,instPremErr,suminErr,pstatcodeErr,statcodeErr,hissdteErr,fundamntErr,surrvalErr,loanValErr,bounsValErr,sacscurbalErr};
		screenSflDateFields = new BaseData[] {hissdte};
		screenSflDateErrFields = new BaseData[] {hissdteErr};
		screenSflDateDispFields = new BaseData[] {hissdteDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, register, chdrstatus, premstatus, cntcurr, cownnum, ownername, totalSurVal, totalLoanVal, totBonusVal, survalind, loanvalind, bonusvalind};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, registerOut, chdrstatusOut, premstatusOut, cntcurrOut, cownnumOut, ownernameOut, totalSurValOut, totalLoanValOut, totBonusValOut, survalindOut, loanvalindOut, bonusvalindOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, registerErr, chdrstatusErr, premstatusErr, cntcurrErr, cownnumErr, ownernameErr, totalSurValErr, totalLoanValErr, totBonusValErr, survalindErr, loanvalindErr, bonusvalindErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5e3screen.class;
		screenSflRecord = Sd5e3screensfl.class;
		screenCtlRecord = Sd5e3screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5e3protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5e3screenctl.lrec.pageSubfile);
	}
}
