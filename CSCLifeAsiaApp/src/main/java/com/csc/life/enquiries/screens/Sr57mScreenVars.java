package com.csc.life.enquiries.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sr57m
 * @version 1.0 generated on 30/08/09 06:30
 * @author Quipoz
 */
public class Sr57mScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(853);
	public FixedLengthStringData dataFields = new FixedLengthStringData(437).isAPartOf(dataArea, 0);
	public ZonedDecimalData clamant = DD.clamant.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,20);
	public FixedLengthStringData hcurrcd = DD.hcurrcd.copy().isAPartOf(dataFields,37);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,40);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,78);
	public ZonedDecimalData tdbtamt = DD.tdbtamt.copyToZonedDecimal().isAPartOf(dataFields,128);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,145);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,161);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,202);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,232);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,240);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,248);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,295);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,303);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,350);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,358);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,405);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,409);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,419);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,427);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 437);
	public FixedLengthStringData clamantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData hcurrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData tdbtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 541);
	public FixedLengthStringData[] clamantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] hcurrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] tdbtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(327);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(101).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public ZonedDecimalData hactval = DD.hactval.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,59);
	public ZonedDecimalData hemv = DD.hemv.copyToZonedDecimal().isAPartOf(subfileFields,63);
	public FixedLengthStringData hjlife = DD.hjlife.copy().isAPartOf(subfileFields,80);
	public FixedLengthStringData hlife = DD.hlife.copy().isAPartOf(subfileFields,82);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,84);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,86);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(subfileFields,96);
	public FixedLengthStringData fund = DD.virtfund.copy().isAPartOf(subfileFields,97);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(56).isAPartOf(subfileArea, 101);
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hactvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hemvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hjlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData hlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData typeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData virtfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(168).isAPartOf(subfileArea, 157);
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hactvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hemvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hjlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] hlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] typeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] virtfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 325);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr57mscreensflWritten = new LongData(0);
	public LongData Sr57mscreenctlWritten = new LongData(0);
	public LongData Sr57mscreenWritten = new LongData(0);
	public LongData Sr57mprotectWritten = new LongData(0);
	public GeneralTable sr57mscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr57mscreensfl;
	}

	public Sr57mScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ptdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "-01","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(otheradjstOut,new String[] {"07","60","-07","60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcdOut,new String[] {"08","60","-08","60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(estimtotalOut,new String[] {null, null, null, "60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(clamantOut,new String[] {null, null, null, "60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"05","60","-05","60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(resndescOut,new String[] {"06","60","-06","60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(policyloanOut,new String[] {null, null, null, "60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdbtamtOut,new String[] {null, null, null, "60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenSflFields = new BaseData[] {hactval, hcrtable, hemv, hcnstcur, hlife, hjlife, coverage, rider, estMatValue, cnstcur, actvalue, shortds, fund, fieldType};
		screenSflOutFields = new BaseData[][] {hactvalOut, hcrtableOut, hemvOut, hcnstcurOut, hlifeOut, hjlifeOut, coverageOut, riderOut, emvOut, cnstcurOut, actvalueOut, shortdsOut, virtfundOut, typeOut};
		screenSflErrFields = new BaseData[] {hactvalErr, hcrtableErr, hemvErr, hcnstcurErr, hlifeErr, hjlifeErr, coverageErr, riderErr, emvErr, cnstcurErr, actvalueErr, shortdsErr, virtfundErr, typeErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {descrip, chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, effdate, planSuffix, otheradjst, hcurrcd, currcd, estimateTotalValue, clamant, reasoncd, resndesc, policyloan, tdbtamt};
		screenOutFields = new BaseData[][] {descripOut, chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, effdateOut, plnsfxOut, otheradjstOut, hcurrcdOut, currcdOut, estimtotalOut, clamantOut, reasoncdOut, resndescOut, policyloanOut, tdbtamtOut};
		screenErrFields = new BaseData[] {descripErr, chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, effdateErr, plnsfxErr, otheradjstErr, hcurrcdErr, currcdErr, estimtotalErr, clamantErr, reasoncdErr, resndescErr, policyloanErr, tdbtamtErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr57mscreen.class;
		screenSflRecord = Sr57mscreensfl.class;
		screenCtlRecord = Sr57mscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr57mprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr57mscreenctl.lrec.pageSubfile);
	}
}
