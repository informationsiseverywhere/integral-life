package com.csc.life.enquiries.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6248screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	//public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S6248screensfl";
		lrec.subfileClass = S6248screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.sizeSubfile = 8;
		lrec.pageSubfile = 8;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6248ScreenVars sv = (S6248ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S6248screenctlWritten, sv.S6248screensflWritten, av, sv.s6248screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6248ScreenVars screenVars = (S6248ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		ScreenRecord.setClassStringFormatting(pv);
		/*screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.smoking01.setClassString("");
		screenVars.occup01.setClassString("");
		screenVars.pursuit01.setClassString("");
		screenVars.pursuit02.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.smoking02.setClassString("");
		screenVars.occup02.setClassString("");
		screenVars.pursuit03.setClassString("");
		screenVars.pursuit04.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.crtabdesc.setClassString("");*/
	}

/**
 * Clear all the variables in S6248screenctl
 */
	public static void clear(VarModel pv) {
		S6248ScreenVars screenVars = (S6248ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		/*screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.smoking01.clear();
		screenVars.occup01.clear();
		screenVars.pursuit01.clear();
		screenVars.pursuit02.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.smoking02.clear();
		screenVars.occup02.clear();
		screenVars.pursuit03.clear();
		screenVars.pursuit04.clear();
		screenVars.crtable.clear();
		screenVars.crtabdesc.clear();*/
		ScreenRecord.clear(pv);
	}
}
