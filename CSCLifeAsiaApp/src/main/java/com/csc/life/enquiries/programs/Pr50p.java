/*
 * File: Pr50p.java
 * Date: 30 August 2009 1:33:00
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50P.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.life.enquiries.screens.Sr50pScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* PR50P - Despatch Address History
* This program allows to enquire on despatch address history, user
* can view the address changes made on the selected transation.
*
* 1000-section
* - Initialize screen fields
* - Retrieve data from CHDRTRX and PTRNENQ files by using RETRV
*   function
* - Set the screen fields
*
***********************************************************************
* </pre>
*/
public class Pr50p extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50P");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-CLNT */
	private FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8);
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String descrec = "DESCREC";
	private String lifelnbrec = "LIFELNBREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Header by Transaction Number*/
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
		/*Contract Enquiry - Transactions File.*/
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Desckey wsaaDesckey = new Desckey();
	private Sr50pScreenVars sv = ScreenProgram.getScreenVars( Sr50pScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit
	}

	public Pr50p() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50p", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.trandate.set(varcom.vrcmMaxDate);
		sv.tranno.set(ZERO);
		chdrtrxIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		ptrnenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrtrxIO.getChdrnum());
		sv.cnttype.set(chdrtrxIO.getCnttype());
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(lifelnbIO.getLifcnum()); //uncommented for ILIFE-7274
		//wsaaClntNumber.set(chdrtrxIO.getDespnum()); //commented for ILIFE-7274
		a1000CallNamadrs();
		sv.lifedesc.set(namadrsrec.name);
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5688);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getCnttype());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypdesc.fill("?");
		}
		else {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3623);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getStatcode());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t3588);
		wsaaDesckey.descDescitem.set(chdrtrxIO.getPstatcode());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		sv.tranno.set(ptrnenqIO.getTranno());
		sv.trancd.set(ptrnenqIO.getBatctrcde());
		sv.trandate.set(ptrnenqIO.getDatesub());
		sv.effdate.set(ptrnenqIO.getPtrneff());
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t1688);
		wsaaDesckey.descDescitem.set(ptrnenqIO.getBatctrcde());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.trandesc.fill("?");
		}
		else {
			sv.trandesc.set(descIO.getShortdesc());
		}
		if (isEQ(chdrtrxIO.getDespnum(),SPACES)) {
			sv.despnum.set(chdrtrxIO.getCownnum());
		}
		else {
			sv.despnum.set(chdrtrxIO.getDespnum());
		}
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(sv.despnum);
		a1000CallNamadrs();
		sv.longname.set(namadrsrec.name);
		sv.despaddr01.set(namadrsrec.addr1);
		sv.despaddr02.set(namadrsrec.addr2);
		sv.despaddr03.set(namadrsrec.addr3);
		sv.despaddr04.set(namadrsrec.addr4);
		sv.despaddr05.set(namadrsrec.addr5);
		sv.postcd.set(namadrsrec.pcode);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a1000CallNamadrs()
	{
		a1000Begin();
	}

protected void a1000Begin()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void a2000GetDesc()
	{
		a2000Begin();
	}

protected void a2000Begin()
	{
		descIO.setDataArea(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsaaDesckey.descDesccoy);
		descIO.setDesctabl(wsaaDesckey.descDesctabl);
		descIO.setDescitem(wsaaDesckey.descDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
}
