/*
 * File: Pr50u.java
 * Date: 30 August 2009 1:33:45
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50U.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.Sr50uScreenVars;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*     Clear the subfile ready for loading.
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status, (STATCODE)  -  short  description  from
*          T3623,
*
*          Premium Status, (PSTATCODE)  -  short  description  from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's client (CLTS) details if they exist.
*
*          Retrive  the   stored  COVRENQ  details  to  obtain  the
*          Coverage number and Rider numbers for display.
*
*     Load the subfile as follows:
*
*     Retrieve the PTRNENQ details that have been stored in the
*     PTRNENQ I/O module.
*
*     Depending on the fund type, need to read two files UTRN
*     or HITR.
*
*     Perform a BEGN on UTRN to position at the first UTRN record
*     Read all the UTRN records for the Company, Contract, Life
*     Component.
*
*     Perform a BEGN on HITR to position at the first HITR record
*     Read all the HITR records for the Company, Contract, Life
*     Component.
*
*     Load all pages required in  the  subfile  and set the subfile
*     more indicator to no.
*
* Validation
* ----------
*
*     There is no validation required.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     Add  1  to  the program pointer and exit.
*
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1688 - Transaction Codes                 Key: Transaction Code
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5515 - Virtual Funds                     Key: VRTFND
* T5688 - Contract Structure                Key: CNTTYPE
* T6649 - Unit Types                        Key: Unit Type.
*
*
***********************************************************************
* </pre>
*/
public class Pr50u extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50U");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNumber = new ZonedDecimalData(5, 0);
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(3, 0);
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaMessages = new FixedLengthStringData(300);
	private FixedLengthStringData[] wsaaMessage = FLSArrayPartOfStructure(6, 50, wsaaMessages, 0);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String tr386 = "TR386";
//	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	/*private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();*/
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Tr386rec tr386rec = new Tr386rec();
	private Wssplife wssplife = new Wssplife();
	private Sr50uScreenVars sv = ScreenProgram.getScreenVars( Sr50uScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	//ILIFE-1513 START by dpuhawan
	private static final String t1688 = "T1688";
	//ILIFE-1513 END
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrenqIO=new Chdrpf();
	private PtrnpfDAO ptrnDao = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
	private Ptrnpf ptrnenqIO = new Ptrnpf(); 
	
	//ILB-498
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO",UtrspfDAO.class);
	private Utrspf utrspf = new Utrspf();
	private List<Utrspf> utrspfList;
	//ILIFE-8391
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO",HitspfDAO.class);
	private Hitspf hitspf = new Hitspf();
	private List<Hitspf> hitspfList;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNext1160
	}

	public Pr50u() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50u", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		/* MOVE WSSP-PLAN-POLICY       TO WSAA-PLAN-POLICY-FLAG.        */
		scrnparams.function.set(varcom.sclr);
		processScreen("SR50U", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		
		//chdrenqIO.setFunction(varcom.retrv);
		/*SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}*/
		chdrenqIO=chdrDao.getCacheObject(chdrenqIO);
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getReg());
		if (isEQ(chdrenqIO.getPolinc(), ZERO)
		|| isEQ(chdrenqIO.getPolinc(), 1)) {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("L");
		}
		else {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("O");
		}
		/* If being accessed remotely e.g. from Business Objects,*/
		/* check if plan component or policy component by checking*/
		/* the policies in plan field on the contract header.*/
		/* IF SCRN-DEVICE-IND           = '*RMT'                        */
		/*    IF CHDRENQ-POLINC         = ZEROES OR 1                   */
		/*       MOVE 'L'              TO WSAA-PLAN-POLICY-FLAG         */
		/*    ELSE                                                      */
		/*       MOVE 'O'              TO WSAA-PLAN-POLICY-FLAG         */
		/*    END-IF                                                    */
		/* END-IF.                                                      */
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*--- Read TR386 table to get screen literals*/
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setStatuz(varcom.oK);
		iy.set(1);
		initialize(wsaaMessages);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadTr3861200();
		}
		
		/*ptrnenqIO.setDataArea(SPACES);*/
		/*ptrnenqIO.setFunction(varcom.retrv);*/
		/*SmartFileCode.execute(appVars, ptrnenqIO);*/
		/*if (isNE(ptrnenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}*/
		ptrnenqIO = ptrnDao.getCacheObject(ptrnenqIO);
		sv.effdate.set(ptrnenqIO.getPtrneff());
		sv.trancd.set(ptrnenqIO.getBatctrcde());
		//ILIFE-1513 START by dpuhawan
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(ptrnenqIO.getBatctrcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.trandesc.fill("?");
		}
		else {
			sv.trandesc.set(descIO.getLongdesc());
		}
		//ILIFE-1513 END
		if (isEQ(wssplife.unitType, "D")) {
			sv.zvar01.set(wsaaMessage[4]);
			sv.zvar02.set(wsaaMessage[5]);
			sv.zvariable.set(wsaaMessage[6]);
			a100InterestBearing();
			return ;
		}
		//ILB-498
		utrspf = utrspfDAO.getCacheObject(utrspf);
		if(null==utrspf){
			utrsIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, utrsIO);
			if (isNE(utrsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(utrsIO.getParams());
				syserrrec.statuz.set(utrsIO.getStatuz());
				fatalError600();
			}
			else {
				utrspfList = utrspfDAO.searchUtrsRecord(utrspf.getChdrcoy(), utrspf.getChdrnum());
				if(utrspfList.isEmpty()) {
					fatalError600();
				}
				else {
					utrspfDAO.setCacheObject(utrspf);
				}
			}
		} 
		/*utrsIO.setDataArea(SPACES);
		utrsIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrsIO.getParams());
			syserrrec.statuz.set(utrsIO.getStatuz());
			fatalError600();
		}*/
		sv.zvar01.set(wsaaMessage[1]);
		sv.zvar02.set(wsaaMessage[2]);
		sv.zvariable.set(wsaaMessage[3]);
		utrnrevIO.setParams(SPACES);
		utrnrevIO.setChdrcoy(chdrenqIO.getChdrcoy());
		utrnrevIO.setChdrnum(chdrenqIO.getChdrnum());
		utrnrevIO.setTranno(ptrnenqIO.getTranno());
		utrnrevIO.setFeedbackInd(SPACES);
		utrnrevIO.setFormat(formatsInner.utrnrevrec);
		utrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnrevIO.setFitKeysSearch("CHDRNUM", "TRANNO");
		SmartFileCode.execute(appVars, utrnrevIO);
		/*    IF UTRNREV-STATUZ           NOT = O-K                        */
		if (isNE(utrnrevIO.getStatuz(), varcom.oK)
		&& isNE(utrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			fatalError600();
		}
		if (isEQ(utrnrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*    Store the suffix that is actually being used to read UTRNREV.*/
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(utrnrevIO.getPlanSuffix());
		wsaaRem.set(1);
		while ( !(isNE(utrnrevIO.getChdrcoy(), wsspcomn.company)
		|| isNE(utrnrevIO.getChdrnum(), chdrenqIO.getChdrnum())
		|| isNE(utrnrevIO.getTranno(), ptrnenqIO.getTranno())
		|| isEQ(utrnrevIO.getStatuz(), varcom.endp)
		|| isEQ(wsaaRem, 0))) {
			processUtrnrev1100();
		}
		
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*      (including subfile load section)
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void processUtrnrev1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1100();
					writeSubfile1150();
				case readNext1160: 
					readNext1160();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1100()
	{
		/*    Set up the Transaction details from the UTRN record.*/
		if (isEQ(utrnrevIO.getUnitVirtualFund(), SPACES)) {
			goTo(GotoLabel.readNext1160);
		}
		sv.subfileFields.set(SPACES);
		sv.moniesDate.set(utrnrevIO.getMoniesDate());
		sv.nowDeferInd.set(utrnrevIO.getNowDeferInd());
		sv.feedbackInd.set(utrnrevIO.getFeedbackInd());
		sv.asacscode.set(utrnrevIO.getSacscode());
		sv.asacstyp.set(utrnrevIO.getSacstyp());
		sv.vfund.set(utrnrevIO.getUnitVirtualFund());
		sv.fundtype.set(utrnrevIO.getUnitType());
		sv.nofUnits.set(utrnrevIO.getNofUnits());
		sv.nofDunits.set(utrnrevIO.getNofDunits());
		sv.fundAmount.set(utrnrevIO.getFundAmount());
	}

protected void writeSubfile1150()
	{
		/*    Write the subfile record.*/
	/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
		sv.priceUsed.set(utrnrevIO.getPriceUsed());
		scrnparams.function.set(varcom.sadd);
		processScreen("SR50U", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Read the next UTRN record.
	* </pre>
	*/
protected void readNext1160()
	{
		utrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(), varcom.oK)
		&& isNE(utrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			fatalError600();
		}
		if (isEQ(scrnparams.deviceInd, "*RMT")) {
			return ;
		}
		//ILIFE-1513 START by dpuhawan
		/* Check for End of Page*/
		/* DIVIDE SCRN-SUBFILE-RRN BY 12 GIVING WSAA-NUMBER             */
		/*
		compute(wsaaNumber, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		wsaaRem.setRemainder(wsaaNumber);
		scrnparams.subfileMore.set(SPACES);
		if (isEQ(wsaaRem, 0)) {
			if (isNE(utrnrevIO.getStatuz(), varcom.endp)
			&& isEQ(utrnrevIO.getChdrcoy(), wsspcomn.company)
			&& isEQ(utrnrevIO.getChdrnum(), chdrenqIO.getChdrnum())
			&& isEQ(utrnrevIO.getTranno(), ptrnenqIO.getTranno())) {
				scrnparams.subfileMore.set("Y");
			}
		}
		*/
		//ILIFE-1513 END
	}

protected void loadTr3861200()
	{
		start1210();
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), wsspcomn.company)
		|| isNE(itemIO.getItemtabl(), tr386)
		|| isNE(itemIO.getItemitem(), wsaaTr386Key)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isEQ(itemIO.getFunction(), varcom.begn)) {
				itemIO.setItemitem(wsaaTr386Key);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		ix.set(1);
		while ( !(isGT(ix, 10))) {
			if (isNE(tr386rec.progdesc[ix.toInt()], SPACES)
			&& isLT(iy, 7)) {
				wsaaMessage[iy.toInt()].set(tr386rec.progdesc[ix.toInt()]);
				iy.add(1);
			}
			ix.add(1);
		}
		
		itemIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*    Scroll*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaRem.set(1);
			if (isEQ(wssplife.unitType, "D")) {
				while ( !(isNE(hitrrevIO.getChdrcoy(), wsspcomn.company)
				|| isNE(hitrrevIO.getChdrnum(), chdrenqIO.getChdrnum())
				|| isNE(hitrrevIO.getLife(), hitspf.getLife())
				|| isNE(hitrrevIO.getCoverage(), hitspf.getCoverage())
				|| isNE(hitrrevIO.getRider(), hitspf.getRider())
				|| isNE(hitrrevIO.getPlanSuffix(), hitspf.getPlanSuffix())
				|| isNE(hitrrevIO.getZintbfnd(), hitspf.getZintbfnd())
				|| isNE(hitrrevIO.getTranno(), ptrnenqIO.getTranno())
				|| isEQ(hitrrevIO.getStatuz(), varcom.endp)
				|| isEQ(wsaaRem, 0))) {
					a200ProcessHitrrev();
				}
				
				wsspcomn.edterror.set(SPACES);
				return ;
			}
			else {
				if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
					while ( !(isNE(utrnrevIO.getChdrcoy(), wsspcomn.company)
					|| isNE(utrnrevIO.getChdrnum(), chdrenqIO.getChdrnum())
					|| isNE(utrnrevIO.getLife(), utrspf.getLife())
					|| isNE(utrnrevIO.getCoverage(), utrspf.getCoverage())
					|| isNE(utrnrevIO.getRider(), utrspf.getRider())
					|| isNE(utrnrevIO.getTranno(), ptrnenqIO.getTranno())
					|| isEQ(utrnrevIO.getStatuz(), varcom.endp)
					|| isEQ(wsaaRem, 0))) {
						processUtrnrev1100();
					}
					
					wsspcomn.edterror.set(SPACES);
					return ;
				}
				else {
					while ( !(isNE(utrnrevIO.getChdrcoy(), wsspcomn.company)
					|| isNE(utrnrevIO.getChdrnum(), chdrenqIO.getChdrnum())
					|| isNE(utrnrevIO.getLife(), utrspf.getLife())
					|| isNE(utrnrevIO.getCoverage(), utrspf.getCoverage())
					|| isNE(utrnrevIO.getRider(), utrspf.getRider())
					|| isNE(utrnrevIO.getTranno(), ptrnenqIO.getTranno())
					|| isNE(utrnrevIO.getPlanSuffix(), wsaaMiscellaneousInner.wsaaPlanSuffix)
					|| isNE(utrnrevIO.getUnitVirtualFund(), utrspf.getUnitVirtualFund())
					|| isNE(utrnrevIO.getUnitType(), utrspf.getUnitType())
					|| isEQ(utrnrevIO.getStatuz(), varcom.endp)
					|| isEQ(wsaaRem, 0))) {
						processUtrnrev1100();
					}
					
					wsspcomn.edterror.set(SPACES);
					return ;
				}
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  No database updates are required.*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*PARA*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a100InterestBearing()
	{
		a110Intb();
	}

protected void a110Intb()
{
	/*hitsIO.setDataArea(SPACES);
		hitsIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitsIO.getParams());
			syserrrec.statuz.set(hitsIO.getStatuz());
			fatalError600();
		}ILIFE-8391*/

	hitspf = hitspfDAO.getCacheObject(hitspf);

	if(null==hitspf) {

		hitsIO.setFunction(varcom.retrv);

		SmartFileCode.execute(appVars, hitsIO);

		if (isNE(hitsIO.getStatuz(),varcom.oK)

				&& isNE(hitsIO.getStatuz(),varcom.mrnf)) {

			syserrrec.params.set(hitsIO.getParams());

			fatalError600();

		}

		else {
			hitspf.setChdrcoy(hitsIO.getChdrcoy().toString());
			hitspf.setChdrnum(hitsIO.getChdrnum().toString());
			hitspf.setLife(hitsIO.getLife().toString());
			hitspf.setCoverage(hitsIO.getCoverage().toString());
			hitspf.setRider(hitsIO.getRider().toString());
			hitspfList = hitspfDAO.searchHitsRecord(hitspf);
			for (Hitspf hitspf : hitspfList) {
				if(null==hitspf) {

					fatalError600();

				}

				else {

					hitspfDAO.setCacheObject(hitspf);

				}
			}

		}

	}

	hitrrevIO.setParams(SPACES);
	hitrrevIO.setChdrcoy(chdrenqIO.getChdrcoy());
	hitrrevIO.setChdrnum(chdrenqIO.getChdrnum());
	hitrrevIO.setTranno(ptrnenqIO.getTranno());
	hitrrevIO.setFeedbackInd(SPACES);
		hitrrevIO.setFormat(formatsInner.hitrrevrec);
		hitrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrrevIO.setFitKeysSearch("CHDRNUM", "TRANNO");
		SmartFileCode.execute(appVars, hitrrevIO);
		/*    IF HITRREV-STATUZ           NOT = O-K                        */
		if (isNE(hitrrevIO.getStatuz(), varcom.oK)
		&& isNE(hitrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			fatalError600();
		}
		if (isEQ(hitrrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		wsaaRem.set(1);
		while ( !(isNE(hitrrevIO.getChdrcoy(), wsspcomn.company)
		|| isNE(hitrrevIO.getChdrnum(), chdrenqIO.getChdrnum())
		|| isNE(hitrrevIO.getTranno(), ptrnenqIO.getTranno())
		|| isEQ(hitrrevIO.getStatuz(), varcom.endp)
		|| isEQ(wsaaRem, 0))) {
			a200ProcessHitrrev();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void a200ProcessHitrrev()
	{
	//	MIBT-250 STARTS
	if (isNE(hitrrevIO.getZintbfnd()," ") 
			&& isNE(hitrrevIO.getZrectyp(), "N")) { 
        a210Para(); 
		} 
	//MIBT-250 ENDS
		a260ReadNext();
	}

protected void a210Para()
	{
		/*    Set up the Transaction details from the HITR record.*/
		sv.subfileFields.set(SPACES);
		sv.moniesDate.set(hitrrevIO.getEffdate());
		sv.nowDeferInd.set(hitrrevIO.getZintappind());
		sv.feedbackInd.set(hitrrevIO.getFeedbackInd());
		sv.nofUnits.set(ZERO);
		sv.nofDunits.set(ZERO);
		sv.fundAmount.set(hitrrevIO.getFundAmount());
		/*Ticket #TMLII-591 CS-04-004: Display Unit Price on S5122 Fund Transactions*/
		sv.priceUsed.set(ZERO);
		sv.asacscode.set(hitrrevIO.getSacscode());
		sv.asacstyp.set(hitrrevIO.getSacstyp());
		sv.vfund.set(hitrrevIO.getZintbfnd());
		sv.fundtype.set(hitrrevIO.getZrectyp());
		/*    Write the subfile record.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR50U", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void a260ReadNext()
	{
		hitrrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(), varcom.oK)
		&& isNE(hitrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			fatalError600();
		}
		//ILIFE-1513 START by dpuhawan
		/* DIVIDE SCRN-SUBFILE-RRN BY 12 GIVING WSAA-NUMBER             */
		/*
		compute(wsaaNumber, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		wsaaRem.setRemainder(wsaaNumber);
		scrnparams.subfileMore.set(SPACES);
		if (isEQ(wsaaRem, 0)
		&& isNE(hitrrevIO.getStatuz(), varcom.endp)
		&& isEQ(hitrrevIO.getChdrcoy(), wsspcomn.company)
		&& isEQ(hitrrevIO.getChdrnum(), chdrenqIO.getChdrnum())
		&& isEQ(hitrrevIO.getTranno(), ptrnenqIO.getTranno())) {
			scrnparams.subfileMore.set("Y");
		}
		*/
		//ILIFE-1513 END
		/*A290-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	private FixedLengthStringData wsaaUtrsComponents = new FixedLengthStringData(15);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaUtrsComponents, 6).setUnsigned();
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData utrnrevrec = new FixedLengthStringData(10).init("UTRNREVREC");
	private FixedLengthStringData hitrrevrec = new FixedLengthStringData(10).init("HITRREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
}
