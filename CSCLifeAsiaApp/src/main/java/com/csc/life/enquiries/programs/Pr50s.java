/*
 * File: Pr50s.java
 * Date: 30 August 2009 1:33:31
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50S.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.enquiries.dataaccess.CovrtrnTableDAM;
import com.csc.life.enquiries.screens.Sr50sScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* PR50S - Special Terms
* The program will display the details of the Special Terms
*
* 1000-section
* - Initialize the screen sub-file
* - Retrieve CHDRTRX and COVRTRN files using RETRV function
* - Set the screen header fields
*
***********************************************************************
* </pre>
*/
public class Pr50s extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50S");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(5, 0).setUnsigned();
		/* WSAA-CLNT */
	private FixedLengthStringData wsaaClntPrefix = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaClntCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaClntNumber = new FixedLengthStringData(8);
		/* TABLES */
	private String t5651 = "T5651";
	private String t5687 = "T5687";
	private String descrec = "DESCREC";
	private String lextrec = "LEXTREC";
	private String lifelnbrec = "LIFELNBREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Header by Transaction Number*/
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
		/*Coverage by Tranno*/
	private CovrtrnTableDAM covrtrnIO = new CovrtrnTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Desckey wsaaDesckey = new Desckey();
	private Sr50sScreenVars sv = ScreenProgram.getScreenVars( Sr50sScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextLext1500, 
		preExit
	}

	public Pr50s() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50s", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		retrvFiles1200();
		setScreenHeader1300();
		lextIO.setDataArea(SPACES);
		lextIO.setStatuz(varcom.oK);
		lextIO.setChdrcoy(covrtrnIO.getChdrcoy());
		lextIO.setChdrnum(covrtrnIO.getChdrnum());
		lextIO.setLife(covrtrnIO.getLife());
		lextIO.setRider(covrtrnIO.getRider());
		lextIO.setCoverage(covrtrnIO.getCoverage());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lextIO.getStatuz());
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(lextIO.getStatuz(),varcom.oK)
		|| isNE(lextIO.getChdrcoy(),covrtrnIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),covrtrnIO.getChdrnum())
		|| isNE(lextIO.getLife(),covrtrnIO.getLife())
		|| isNE(lextIO.getRider(),covrtrnIO.getRider())
		|| isNE(lextIO.getCoverage(),covrtrnIO.getCoverage())) {
			lextIO.setStatuz(varcom.endp);
		}
		wsaaCnt.set(1);
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp))) {
			loadSubfile1500();
		}
		
		if (isEQ(wsaaCnt,1)) {
			blankSubfile1400();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvFiles1200()
	{
		begin1200();
	}

protected void begin1200()
	{
		chdrtrxIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		covrtrnIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrtrnIO);
		if (isNE(covrtrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrtrnIO.getStatuz());
			syserrrec.params.set(covrtrnIO.getParams());
			fatalError600();
		}
	}

protected void setScreenHeader1300()
	{
		begin1300();
	}

protected void begin1300()
	{
		sv.chdrnum.set(chdrtrxIO.getChdrnum());
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		sv.smoking01.set(lifelnbIO.getSmoking());
		sv.occup01.set(lifelnbIO.getOccup());
		sv.pursuit01.set(lifelnbIO.getPursuit01());
		sv.pursuit02.set(lifelnbIO.getPursuit02());
		wsaaClntPrefix.set(fsupfxcpy.clnt);
		wsaaClntCompany.set(wsspcomn.fsuco);
		wsaaClntNumber.set(lifelnbIO.getLifcnum());
		a1000CallNamadrs();
		sv.lifedesc.set(namadrsrec.name);
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrtrxIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isEQ(lifelnbIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifelnbIO.getLifcnum());
			sv.smoking02.set(lifelnbIO.getSmoking());
			sv.occup02.set(lifelnbIO.getOccup());
			sv.pursuit03.set(lifelnbIO.getPursuit01());
			sv.pursuit04.set(lifelnbIO.getPursuit02());
			wsaaClntPrefix.set(fsupfxcpy.clnt);
			wsaaClntCompany.set(wsspcomn.fsuco);
			wsaaClntNumber.set(lifelnbIO.getLifcnum());
			a1000CallNamadrs();
			sv.jlifedesc.set(namadrsrec.name);
		}
		sv.crtable.set(covrtrnIO.getCrtable());
		sv.life.set(covrtrnIO.getLife());
		sv.coverage.set(covrtrnIO.getCoverage());
		sv.rider.set(covrtrnIO.getRider());
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5687);
		wsaaDesckey.descDescitem.set(covrtrnIO.getCrtable());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.crtabdesc.fill("?");
		}
		else {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
	}

protected void blankSubfile1400()
	{
		/*BEGIN*/
		sv.initialiseSubfileArea();
		sv.opcda.set(SPACES);
		sv.shortdesc.set(SPACES);
		sv.reasind.set(SPACES);
		sv.select.set(SPACES);
		sv.extCessDate.set(varcom.vrcmMaxDate);
		sv.agerate.set(ZERO);
		sv.extCessTerm.set(ZERO);
		sv.insprm.set(ZERO);
		sv.oppc.set(ZERO);
		sv.seqnbr.set(ZERO);
		sv.zmortpct.set(ZERO);
		sv.znadjperc.set(ZERO);
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		/*EXIT*/
	}

protected void loadSubfile1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin1500();
				}
				case nextLext1500: {
					nextLext1500();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1500()
	{
		if (isGT(lextIO.getTranno(),covrtrnIO.getTranno())) {
			goTo(GotoLabel.nextLext1500);
		}
		sv.initialiseSubfileArea();
		sv.opcda.set(lextIO.getOpcda());
		wsaaDesckey.descDesccoy.set(wsspcomn.company);
		wsaaDesckey.descDesctabl.set(t5651);
		wsaaDesckey.descDescitem.set(lextIO.getOpcda());
		a2000GetDesc();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.shortdesc.fill("?");
		}
		else {
			sv.shortdesc.set(descIO.getShortdesc());
		}
		sv.agerate.set(lextIO.getAgerate());
		sv.oppc.set(lextIO.getOppc());
		sv.insprm.set(lextIO.getInsprm());
		sv.extCessTerm.set(lextIO.getExtCessTerm());
		sv.extCessDate.set(lextIO.getExtCessDate());
		if (isEQ(sv.extCessDate,ZERO)) {
			sv.extCessDate.set(varcom.vrcmMaxDate);
		}
		sv.seqnbr.set(lextIO.getSeqnbr());
		sv.reasind.set(lextIO.getReasind());
		sv.znadjperc.set(lextIO.getZnadjperc());
		sv.zmortpct.set(lextIO.getZmortpct());
		if (isEQ(lextIO.getJlife(),SPACES)
		|| isEQ(lextIO.getJlife(),"00")) {
			sv.select.set("L");
		}
		else {
			sv.select.set("J");
		}
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaCnt.add(1);
	}

protected void nextLext1500()
	{
		lextIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(lextIO.getStatuz());
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(lextIO.getStatuz(),varcom.oK)
		|| isNE(lextIO.getChdrcoy(),covrtrnIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),covrtrnIO.getChdrnum())
		|| isNE(lextIO.getLife(),covrtrnIO.getLife())
		|| isNE(lextIO.getRider(),covrtrnIO.getRider())
		|| isNE(lextIO.getCoverage(),covrtrnIO.getCoverage())) {
			lextIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void update3000()
	{
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen("SR50S", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a1000CallNamadrs()
	{
		a1000Begin();
	}

protected void a1000Begin()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(wsaaClntPrefix);
		namadrsrec.clntCompany.set(wsaaClntCompany);
		namadrsrec.clntNumber.set(wsaaClntNumber);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void a2000GetDesc()
	{
		a2000Begin();
	}

protected void a2000Begin()
	{
		descIO.setDataArea(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsaaDesckey.descDesccoy);
		descIO.setDesctabl(wsaaDesckey.descDesctabl);
		descIO.setDescitem(wsaaDesckey.descDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
}
