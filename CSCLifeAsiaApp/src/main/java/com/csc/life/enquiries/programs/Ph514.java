/*
 * File: Ph514.java
 * Date: 30 August 2009 1:04:29
 * Author: Quipoz Limited
 * 
 * Class transformed from PH514.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.enquiries.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.List;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.tablestructures.Th505rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.screens.Sh514ScreenVars;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.smart400framework.dataaccess.model.Itempf;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* TERM BASED COMPONENT ENQUIRY (CASH DIVIDEND)
*
*     This program is cloned from P6242 with additional of Cash
*     Dividend information displayed.  The additional information
*     is retrieved from HCSDPF whose records are created during
*     issuing of a Cash Dividend component and subsequently
*     updated in component change transactions.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen as follows:
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*
*
*     Perform a RETRV  on  COVRENQ.  This  will  provide  the  COVR
*     record that is being enquired upon. At this point the program
*     must work out whether it is to provide a Plan Level or Policy
*     Level  enquiry.  This  can  be  done  by  checking  the field
*     WSSP-PLAN-POLICY-FLAG.  If it is 'L', then Plan Level Enquiry
*     is  required,  if  it  is  'O',  then Policy Level Enquiry is
*     required.  For Plan Level Enquiry all the COVRENQ records for
*     the  given  Life, Coverage and Rider must be read and the Sum
*     Insured  and Premium values added together so a total for the
*     whole  Plan  may  be  displayed.  For  Policy  Level  Enquiry
*     processing  will  be different depending on whether a summary
*     record or non-summary record is being displayed. Normally the
*     program  could  determine whether or not a summary record was
*     being  displayed  by  checking  the  Plan Suffix. If this was
*     '0000',  then  it  was  a summary record. However, for Policy
*     Level Enquiry when a summary record is held in the I/O module
*     for  processing  the previous function will replace the value
*     of  '0000'  in  the  Plan  Suffix  with the calculated suffix
*     number of the actual policy that the user selected. Therefore
*     it  will  be  necessary  to check the Plan Suffix against the
*     number  of policies summarised, (CHDRENQ-POLSUM). If the Plan
*     Suffix is not greater the CHDRENQ-POLSUM then it is a summary
*     record.
*
*     For  a  non-summary  record,  (Plan Suffix > CHDRENQ-POLSUM),
*     then display the Sum Insured and Premium as found.
*
*     For  a  summary  record,  (Plan Suffix not > CHDRENQ-POLSUM),
*     then  the  values  held on the COVRENQ record will be for all
*     the  policies summarised, and therefore a calculation must be
*     performed  to  arrive  at the value for one of the summarised
*     policies.  For  all  summarised policies execpt the first one
*     simply divide the total by the number of policies summarised,
*     (CHDRENQ-POLSUM)  and  display the result. This may give rise
*     to  a  rounding  discrepancy. This discrepancy is absorbed by
*     the  first policy summarised, the notional policy number one.
*     If  this  policy  has  been  selected for display perform the
*     following calculation:
*
*     Value To Display = TOTAL - (TOTAL * (POLSUM - 1))
*                                          ~~~~~~~~~~~           *                                            POLSUM
*
*     Where  TOTAL  is  either  the Sum Insured or the Premium, and
*     POLSUM is the number of policies summarised, CHDRENQ-POLSUM.
*
*     For example if the premium is $100 and the number of policies
*     summarised  is 3, the Premium for notional policies #2 and #3
*     will  be  calculated  and  displayed as $33. For policy #1 it
*     will be:
*
*                        100 - (100 * (3 - 1)
*                                      ~~~~~                     *                                        3
*
*                     =  100 - (100 * 2/3)
*
*                     =  $34
*
*
*     The screen title  is  the  component  description from T5687.
*     (See P5123 for how this is obtained and centred).
*
*     Read table T5671,  key  is Transaction Code concatenated with
*     Coverage/Rider Code,  (CRTABLE).  Take  the  validation  item
*     that matches the current program and use it concatenated with
*     the Coverage/Rider Currency to read T5608.
*
*     Read HCSD with key of COVRENQ for these fields:            th
*
*          Dividend Option
*          MOP
*          Payment Currency
*          Bank/Branch
*          Bank Account
*     Then Bank/Branch Description from BABR.
*
*     If Options and Extras  are  not allowed, as defined on T5608,
*     then protect and  non-display  the  prompt and Options/Extras
*     Indicator, (OPTEXTIND). Otherwise read the options and extras
*     data-set, (LEXT), for  the  current  Coverage/Rider.  If  any
*     matching record is found then  place  a  '+'  in the field to
*     indicate that these details exist.
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation required  is  of  the  indicator  field -
*     OPTEXTIND.  This may be space, '+' or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was requested  move  spaces  to the current program
*     field in the program stack field and exit.
*
*     If returning from  processing  the selection further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore  the  next 8  programs  from  the  WSSP,  remove  the
*     asterisk and set the select indicator to '+'.
*
*     If nothing was selected,  continue  by  just moving spaces to
*     the current stack action field and exit.  This will cause the
*     program to be re-invoked.
*
*     If a selection has been  made move '?' to the select field on
*     the screen, use GENSWCH  to  locate  the  next  program(s) to
*     process the selection, (up  to 8).  Use function of 'A'. Save
*     the next 8 programs in  the  stack  and replace them with the
*     ones returned from GENSWCH.  Place an asterisk in the current
*     stack action field, add 1 to the program pointer and exit.
*
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5608 - Term based Edit Rules             Key: Val. Item||Currency Code
* T5671 - Coverage/Rider Switching          Key: Tr. Code||CRTABLE
* T5687 - General Coverage/Rider Details    Key: CRTABLE
* T5688 - Contract Structure                Key: CNTTYPE
*
***********************************************************************
* </pre>
*/
public class Ph514 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH514");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaCovrPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaPremStatus, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private static final String g620 = "G620";
	private static final String h093 = "H093";
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Paid Up Addition Coverage Details Logica*/
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
		/*Life Details - Contract Enquiry.*/
	protected LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
		/*Premium breakdown additions to the covR.*/
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private T2240rec t2240rec = new T2240rec();
	private Th505rec th505rec = new Th505rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wssplife wssplife = new Wssplife();
	private Sh514ScreenVars sv = getLScreenVars(); //ScreenProgram.getScreenVars( Sh514ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private boolean loadingFlag = false;//ILIFE-3399

	/*BRD-306 STARTS */
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	/*BRD-306 END */
	private boolean dialdownFlag = false;
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrenqIO = new Chdrpf();
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrenqIO = new Covrpf();
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
	private boolean isEndMat = false;
	private static final String itemcoy = "2";
	private static final String TR530 = "TR530";	
	private static final String TR529 = "TR529";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfListtr529;
	private List<Itempf> itempfListtr530;
	private static final String chdrenqrec = "CHDRENQREC";
	protected ChdrenqTableDAM chdrenq2IO = new ChdrenqTableDAM();
	private static final String zraerec = "ZRAEREC";
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	//ILJ-45 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-45 End
	// ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	// ILJ-387 end

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endPolicy1020, 
		nextProg10640, 
		exit1090, 
		search1320, 
		exit1790, 
		exit2090, 
		gensww4010, 
		exit4090
	}

	public Ph514() {
		super();
		screenVars = sv;
		new ScreenModel("Sh514", AppVars.getInstance(), sv);
	}
	protected Sh514ScreenVars getLScreenVars() {

		return ScreenProgram.getScreenVars(Sh514ScreenVars.class);

	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case endPolicy1020: 
					endPolicy1020();
					gotRecord10620();
				case nextProg10640: 
					nextProg10640();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.taxamt.set(ZERO);
		sv.prmbasis.set(SPACES);
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		/*    Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrenqIO = chdrDao.getCacheObject(chdrenqIO);
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getReg());
		sv.numpols.set(chdrenqIO.getPolinc());
		/*Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRENQ-CNTTYPE        TO ITEM-ITEMITEM.        <CSDV01>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrenqIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <CSDV01>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			else {
				if (isNE(t2240rec.agecode04,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit04);
				}
			} //MTL002
			}
		}
		//ILJ-45 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{			
			sv.crrcdOut[varcom.nd.toInt()].set("Y");
			//sv.rcessageOut[varcom.nd.toInt()].set("Y");
			sv.contDtCalcScreenflag.set("N");
			sv.rcesdteOut[varcom.nd.toInt()].set("Y");
		}
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}
		//ILJ-45 End
		
		// ILJ-387 Start
		boolean cntEnqFlag = false;
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if (cntEnqFlag) {
			sv.cntEnqScreenflag.set("Y");
		}else {
			sv.cntEnqScreenflag.set("N");
		}
		// ILJ-387 End
		isEndMat = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "PRPRO001", appVars, "IT");
		if(!isEndMat){
			sv.aepaydetOut[varcom.nd.toInt()].set("Y");
		}
		
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		//ILIFE-3399-starts
		loadingFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-3399-ENDS
		
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrenqIO.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Retrieve valid status'.*/
		readT5679Table1200();
		/*    Read the first LIFE details on the contract.*/
		covrenqIO = covrDao.getCacheObject(covrenqIO);
		if(isEndMat)
		{
			itempfListtr530=itempfDAO.getAllItemitem("IT",itemcoy, TR530, covrenqIO.getCrtable()); /* IJTI-1479 */
			itempfListtr529=itempfDAO.getAllItemitem("IT",itemcoy, TR529, covrenqIO.getCrtable()); /* IJTI-1479 */
		}
		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaMiscellaneousInner.wsaaSingp.set(covrenqIO.getSingp());
			wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrenqIO.getSumins());
			/*     COMPUTE COVRENQ-PLAN-SUFFIX  = CHDRENQ-POLSUM + 1        */
			if (isGT(chdrenqIO.getPolinc(),1)) {
				setPrecision(covrenqIO.getPlanSuffix(), 0);
				covrenqIO.setPlanSuffix(chdrenqIO.getPolsum()+1);
				while ( !(isGT(covrenqIO.getPlanSuffix(),chdrenqIO.getPolinc()))) {
					planComponent1100();
				}
				
				goTo(GotoLabel.endPolicy1020);
			}
			else {
				goTo(GotoLabel.endPolicy1020);
			}
		}
		if (isGT(covrenqIO.getPlanSuffix(),chdrenqIO.getPolsum())) {
			wsaaMiscellaneousInner.wsaaSingp.set(covrenqIO.getSingp());
			wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrenqIO.getSumins());
			goTo(GotoLabel.endPolicy1020);
		}
		wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
		if (isEQ(covrenqIO.getPlanSuffix(),1)) {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(sub(covrenqIO.getSingp(), (div(mult(covrenqIO.getSingp(), (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(sub(wsaaMiscellaneousInner.wsaaPremium, (div(mult(wsaaMiscellaneousInner.wsaaPremium, (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(sub(covrenqIO.getSumins(), (div(mult(covrenqIO.getSumins(), (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(div(covrenqIO.getSingp(), chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(div(wsaaMiscellaneousInner.wsaaPremium, chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(div(covrenqIO.getSumins(), chdrenqIO.getPolsum()));
		}
	}
protected void checkExcl()
{
	exclpf = exclpfDAO.readRecord(covrenqIO.getChdrcoy(), covrenqIO.getChdrnum(),covrenqIO.getCrtable(),covrenqIO.getLife(),covrenqIO.getCoverage(),covrenqIO.getRider()); /* IJTI-1479 */
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}

/*BRD-306 STARTS */
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();	
	/* IJTI-1479 START*/
	rcvdPFObject.setChdrcoy(covrenqIO.getChdrcoy());
	rcvdPFObject.setChdrnum(covrenqIO.getChdrnum());
	rcvdPFObject.setLife(covrenqIO.getLife());
	rcvdPFObject.setCoverage(covrenqIO.getCoverage());
	rcvdPFObject.setRider(covrenqIO.getRider());
	rcvdPFObject.setCrtable(covrenqIO.getCrtable());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	/* IJTI-1479 END*/
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}
/*BRD-306 ENDS */
protected void endPolicy1020()
	{
		sv.mortcls.set(covrenqIO.getMortcls());
		sv.numpols.set(chdrenqIO.getPolinc());
		sv.anbAtCcd.set(covrenqIO.getAnbAtCcd());
		sv.crrcd.set(covrenqIO.getCrrcd());
		sv.annivProcDate.set(covrenqIO.getAnnivProcDate());
		sv.optextind.set(SPACES);
		sv.premcess.set(covrenqIO.getPremCessDate());
		sv.liencd.set(covrenqIO.getLiencd());
		sv.riskCessDate.set(covrenqIO.getRiskCessDate());
		sv.riskCessAge.set(covrenqIO.getRiskCessAge());
		sv.premCessAge.set(covrenqIO.getPremCessAge());
		sv.riskCessTerm.set(covrenqIO.getRiskCessTerm());
		sv.premCessTerm.set(covrenqIO.getPremCessTerm());
		sv.register.set(chdrenqIO.getReg());
		sv.life.set(covrenqIO.getLife());
		if (isNE(covrenqIO.getLife(),lifeenqIO.getLife())) {
			getLifeName4800();
		}
		sv.rider.set(covrenqIO.getRider());
		sv.coverage.set(covrenqIO.getCoverage());
		sv.rerateDate.set(covrenqIO.getRerateDate());
		sv.rerateFromDate.set(covrenqIO.getRerateFromDate());
		/*BRD-306 START */
		sv.zbinstprem.set(covrenqIO.getZbinstprem());
		sv.loadper.set(covrenqIO.getLoadper());
		sv.zlinstprem.set(covrenqIO.getZlinstprem());
		sv.rateadj.set(covrenqIO.getRateadj());
		sv.fltmort.set(covrenqIO.getFltmort());
		sv.premadj.set(covrenqIO.getPremadj());
		sv.adjustageamt.set(covrenqIO.getPremadj());
		/*BRD-306 END */
		dialdownFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		premiumflag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends
		exclFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
		sv.singlePremium.set(wsaaMiscellaneousInner.wsaaSingp);
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.statFund.set(covrenqIO.getStatFund());
		sv.statSect.set(covrenqIO.getStatSect());
		sv.statSubsect.set(covrenqIO.getStatSubsect());
		sv.sumin.set(wsaaMiscellaneousInner.wsaaSumins);
		if(isEndMat)
		{
			if ((itempfListtr530.size()==0) && (itempfListtr529.size()==0) )  {
				
				sv.aepaydetOut[varcom.nd.toInt()].set("Y");
			}
		}
		if (isNE(covrenqIO.getPlanSuffix(),ZERO)) {
			sv.planSuffix.set(covrenqIO.getPlanSuffix());
		}
		else {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			sv.planSuffix.set(ZERO);
		}
		if (isNE(covrenqIO.getBonusInd(),SPACES)) {
			sv.unitStatementDate.set(covrenqIO.getUnitStatementDate());
			sv.bonusInd.set(covrenqIO.getBonusInd());
		}
		else {
			sv.cbunstOut[varcom.nd.toInt()].set("Y");
			sv.unitStatementDate.set(ZERO);
			sv.bonusInd.set(SPACES);
		}
		setupBonus1400();
		sv.bappmeth.set(covrenqIO.getBappmeth());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrenqIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaMiscellaneousInner.wsaaTrCode.set(wsaaBatckey.batcBatctrcde);
		wsaaMiscellaneousInner.wsaaCrtable.set(covrenqIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5671);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5671Key);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5671)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5671Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itdmIO.getGenarea());
		}
	}

protected void gotRecord10620()
	{
		sub1.set(0);
	}

protected void nextProg10640()
	{
		sub1.add(1);
		if (isNE(t5671rec.pgm[sub1.toInt()],wsaaProg)) {
			goTo(GotoLabel.nextProg10640);
		}
		else {
			wsaaMiscellaneousInner.wsaaValidItem.set(t5671rec.edtitm[sub1.toInt()]);
		}
		/* READ TH505                                                      */
		wsaaMiscellaneousInner.wsaaCrcyCode.set(chdrenqIO.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th505);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaTh505Key);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.th505)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaTh505Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			fatalError600();
		}
		else {
			th505rec.th505Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(th505rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(th505rec.prmbasis, SPACES)){        
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext1900();
		}
		checkPovr1800();
		checkHcsd1a00();
		covrIO.setDataKey(SPACES);
		covrIO.setChdrcoy(covrenqIO.getChdrcoy());
		covrIO.setChdrnum(covrenqIO.getChdrnum());
		covrIO.setLife(covrenqIO.getLife());
		covrIO.setCoverage(covrenqIO.getCoverage());
		covrIO.setRider(covrenqIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		sv.sumins.set(covrIO.getVarSumInsured());
		hpuaIO.setDataKey(SPACES);
		hpuaIO.setChdrcoy(covrenqIO.getChdrcoy());
		hpuaIO.setChdrnum(covrenqIO.getChdrnum());
		hpuaIO.setLife(covrenqIO.getLife());
		hpuaIO.setCoverage(covrenqIO.getCoverage());
		hpuaIO.setRider(covrenqIO.getRider());
		hpuaIO.setPlanSuffix(ZERO);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFormat(formatsInner.hpuarec);
		hpuaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),hpuaIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),hpuaIO.getChdrnum())
		|| isNE(covrenqIO.getLife(),hpuaIO.getLife())
		|| isNE(covrenqIO.getCoverage(),hpuaIO.getCoverage())
		|| isNE(covrenqIO.getRider(),hpuaIO.getRider())
		|| isEQ(hpuaIO.getStatuz(),varcom.endp)) {
			sv.hpuaindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.puaInd.set("+");
		}
		hdisIO.setDataKey(SPACES);
		hdisIO.setChdrcoy(covrenqIO.getChdrcoy());
		hdisIO.setChdrnum(covrenqIO.getChdrnum());
		hdisIO.setLife(covrenqIO.getLife());
		hdisIO.setCoverage(covrenqIO.getCoverage());
		hdisIO.setRider(covrenqIO.getRider());
		hdisIO.setPlanSuffix(ZERO);
		hdisIO.setFormat(formatsInner.hdisrec);
		hdisIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdisIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),hdisIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),hdisIO.getChdrnum())
		|| isNE(covrenqIO.getLife(),hdisIO.getLife())
		|| isNE(covrenqIO.getCoverage(),hdisIO.getCoverage())
		|| isNE(covrenqIO.getRider(),hdisIO.getRider())
		|| isEQ(hdisIO.getStatuz(),varcom.endp)) {
			sv.hdvdindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.divdInd.set("+");
		}
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrenqIO.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void planComponent1100()
	{
		para1100();
	}

protected void para1100()
	{
		if (isEQ(covrenqIO.getPlanSuffix(),1)
		&& isNE(chdrenqIO.getPolinc(),1)) {
			wsaaMiscellaneousInner.wsaaSumins.set(ZERO);
		}
			wsaaCovrPstatcode.set(covrenqIO.getPstatcode());
			checkPremStatus1300();
		
		if (validStatus.isTrue()) {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(add(wsaaMiscellaneousInner.wsaaSingp, covrenqIO.getSingp()));
			compute(wsaaMiscellaneousInner.wsaaPremium, 2).set(add(wsaaMiscellaneousInner.wsaaPremium, covrenqIO.getInstprem()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(add(wsaaMiscellaneousInner.wsaaSumins, covrenqIO.getSumins()));
		}
		setPrecision(covrenqIO.getPlanSuffix(), 0);
		covrenqIO.setPlanSuffix(covrenqIO.getPlanSuffix()+ 1);
	}

protected void readT5679Table1200()
	{
		read1210();
	}

protected void read1210()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void checkPremStatus1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1310();
				case search1320: 
					search1320();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Check the twelve premium status' retrieved from table T5679
	* to see if any match the coverage premium status. If so, set
	* the VALID-STATUS flag to yes.
	* </pre>
	*/
protected void start1310()
	{
		wsaaPremStatus.set(SPACES);
		wsaaSub.set(ZERO);
	}

protected void search1320()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			return ;
		}
		else {
			if (isNE(wsaaCovrPstatcode,t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search1320);
			}
			else {
				wsaaPremStatus.set("Y");
				return ;
			}
		}
		/*EXIT*/
	}

protected void setupBonus1400()
	{
		/*PARA*/
		/* Check if Coverage/Rider is a SUM product. If not SUM product*/
		/* do not display field.*/
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covrenqIO.getChdrcoy());
		itemIO.setItemitem(covrenqIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
//		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		wsaaX.add(1);
//		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkPovr1800()
	{
		readPovr1810();
	}

protected void readPovr1810()
	{
		/* Test the POVR file for the existance of a Premium Breakdown*/
		/* record. If one exists then allow the option to display the*/
		/* select window for enquiry....if not then protect.*/
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covrenqIO.getChdrcoy());
		povrIO.setChdrnum(lifeenqIO.getChdrnum());
		povrIO.setLife(lifeenqIO.getLife());
		povrIO.setCoverage(covrenqIO.getCoverage());
		povrIO.setRider(covrenqIO.getRider());
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)
		&& isNE(povrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		else {
			if (isEQ(povrIO.getStatuz(),varcom.endp)
			|| isNE(povrIO.getChdrcoy(),covrenqIO.getChdrcoy())
			|| isNE(povrIO.getChdrnum(),lifeenqIO.getChdrnum())
			|| isNE(povrIO.getLife(),lifeenqIO.getLife())
			|| isNE(povrIO.getCoverage(),covrenqIO.getCoverage())
			|| isNE(povrIO.getRider(),covrenqIO.getRider())) {
				sv.pbind.set(SPACES);
				sv.pbindOut[varcom.nd.toInt()].set("Y");
				sv.pbindOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.pbind.set("+");
				sv.pbindOut[varcom.nd.toInt()].set("N");
				sv.pbindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
		lextIO.setChdrcoy(covrenqIO.getChdrcoy());
		lextIO.setChdrnum(covrenqIO.getChdrnum());
		lextIO.setLife(covrenqIO.getLife());
		lextIO.setCoverage(covrenqIO.getCoverage());
		lextIO.setRider(covrenqIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),lextIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),lextIO.getChdrnum())
		|| isNE(covrenqIO.getLife(),lextIO.getLife())
		|| isNE(covrenqIO.getCoverage(),lextIO.getCoverage())
		|| isNE(covrenqIO.getRider(),lextIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

protected void checkHcsd1a00()
	{
			readHcsd1a10();
		}

protected void readHcsd1a10()
	{
		hcsdIO.setChdrcoy(covrenqIO.getChdrcoy());
		hcsdIO.setChdrnum(covrenqIO.getChdrnum());
		hcsdIO.setLife(covrenqIO.getLife());
		hcsdIO.setCoverage(covrenqIO.getCoverage());
		hcsdIO.setRider(covrenqIO.getRider());
		hcsdIO.setPlanSuffix(ZERO);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)
		&& isNE(hcsdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hcsdIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),hcsdIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),hcsdIO.getChdrnum())
		|| isNE(covrenqIO.getLife(),hcsdIO.getLife())
		|| isNE(covrenqIO.getCoverage(),hcsdIO.getCoverage())
		|| isNE(covrenqIO.getRider(),hcsdIO.getRider())) {
			return ;
		}
		sv.zdivopt.set(hcsdIO.getZdivopt());
		sv.paymth.set(hcsdIO.getPaymth());
		sv.paycurr.set(hcsdIO.getPaycurr());
		sv.payclt.set(hcsdIO.getPayclt());
		cltsIO.setClntnum(hcsdIO.getPayclt());
		if (isNE(cltsIO.getClntnum(),SPACES)) {
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.payorname.set(wsspcomn.longconfname);
			}
		}
		sv.bankkey.set(hcsdIO.getBankkey());
		babrIO.setBankkey(hcsdIO.getBankkey());
		sv.bankacckey.set(hcsdIO.getBankacckey());
		if (isNE(babrIO.getBankkey(),SPACES)) {
			babrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, babrIO);
			if (isNE(babrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(babrIO.getParams());
				fatalError600();
			}
			else {
				sv.bankdesc.set(subString(babrIO.getBankdesc(), 1, 30));
				sv.branchdesc.set(subString(babrIO.getBankdesc(), 31, 30));
			}
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		
		if(isEndMat)
		{

			if ((itempfListtr530.size()>0) || (itempfListtr529.size()>0))  {
				CheckZraeRecords();
			}
			
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			a200CheckCalcTax();
		}
		if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SH514IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   SH514-DATA-AREA .             */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind,"+")
		&& isNE(sv.optextind,"X")) {
			sv.optextindErr.set(g620);
		}
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind,"+")
		&& isNE(sv.pbind,"X")) {
			sv.pbindErr.set(g620);
		}
		if (isNE(sv.divdInd, " ")
		&& isNE(sv.divdInd,"+")
		&& isNE(sv.divdInd,"X")) {
			sv.hdvdindErr.set(g620);
		}
		if (isNE(sv.puaInd, " ")
		&& isNE(sv.puaInd,"+")
		&& isNE(sv.puaInd,"X")) {
			sv.hpuaindErr.set(g620);
		}
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(g620);
				}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4000();
				case gensww4010: 
					gensww4010();
					nextProgram4020();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		gensswrec.function.set(SPACES);
		/*    If returning from a program further down the stack then      */
		/*    first restore the original programs in the program stack.    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				restoreProgram4100();
			}
		}
		/*    If any of the indicators have been selected, (value - 'X'),  */
		/*    then set an asterisk in the program stack action field to    */
		/*    ensure that control returns here, set the parameters for     */
		/*    generalised secondary switching and save the original        */
		/*    programs from the program stack.                             */
		if (isEQ(sv.divdInd,"X")
		|| isEQ(sv.puaInd,"X")
		|| isEQ(sv.optextind,"X")
		|| isEQ(sv.taxind, "X")
		|| isEQ(sv.pbind,"X")
		|| isEQ(sv.exclind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
				saveProgramStack4200();
			}
		}
		/*   If Dividend enquiry has been selected set 'E' in the function.*/
		/*   If it was selected previously set it to '+'.                  */
		if (isEQ(sv.divdInd,"X")) {
			sv.divdInd.set("?");
			gensswrec.function.set("E");
			covrDao.setCacheObject(covrenqIO);
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.divdInd,"?")) {
			sv.divdInd.set("+");
		}
		/*   If Paid Up Addition has been selected set 'E' in the function.*/
		/*   If it was selected previously set it to '+'.                  */
		if (isEQ(sv.puaInd,"X")) {
			sv.puaInd.set("?");
			gensswrec.function.set("D");
			covrDao.setCacheObject(covrenqIO);
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.puaInd,"?")) {
			sv.puaInd.set("+");
		}
		/*   If Premium Breakdown have been selected set 'B' in the        */
		/*   function. If it was selected previously set it to '+'.        */
		if (isEQ(sv.pbind,"X")) {
			sv.pbind.set("?");
			gensswrec.function.set("B");
			povrIO.setFunction(varcom.keeps);
			povrIO.setFormat(formatsInner.povrrec);
			SmartFileCode.execute(appVars, povrIO);
			if (isNE(povrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(povrIO.getParams());
				syserrrec.statuz.set(povrIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.pbind,"?")) {
			povrIO.setFunction(varcom.rlse);
			povrIO.setFormat(formatsInner.povrrec);
			SmartFileCode.execute(appVars, povrIO);
			if (isNE(povrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(povrIO.getParams());
				syserrrec.statuz.set(povrIO.getStatuz());
				fatalError600();
			}
			sv.pbind.set("+");
		}
		/*   If Special Terms have been selected set 'B' in the            */
		/*   function. If it was selected previously set it to '+'.        */
		if (isEQ(sv.optextind,"X")) {
			sv.optextind.set("?");
			gensswrec.function.set("A");
			covrDao.setCacheObject(covrenqIO);
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.optextind,"?")) {
			sv.optextind.set("+");
		}
		/*   If Tax has been selected set 'F' in the function.             */
		/*   If it was selected previously set it to '+'.                  */
		if (isEQ(sv.taxind, "X")) {
			chdrDao.setCacheObject(chdrenqIO);
			covrDao.setCacheObject(covrenqIO);
			sv.taxind.set("?");
			gensswrec.function.set("F");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.taxind, "?")) {
			sv.taxind.set("+");
		}
		if (isEQ(sv.exclind, "X")) {
			Covrpf covrpf = new Covrpf();
			covrpf.setLife(covrenqIO.getLife());
			covrpf.setCoverage(covrenqIO.getCoverage());
			covrpf.setRider(covrenqIO.getRider());
			covrDao.setCacheObject(covrpf);
			sv.exclind.set("?");
			gensswrec.function.set("G");
			wsspcomn.chdrChdrnum.set(covrenqIO.getChdrnum());
			wsspcomn.crtable.set(covrenqIO.getCrtable());
			
			wsspcomn.cmode.set("IFE");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.exclind, "?")) {
			checkExcl();
		}
		if (isEQ(sv.aepaydet, "X") && isEndMat) {
			chdrDao.setCacheObject(chdrenqIO);
			antcpEndwPayDet();			
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.aepaydet, "?") && isEndMat) {
			chdrDao.setCacheObject(chdrenqIO);
			antcpEndwPayDet1();
			sv.aepaydet.set("+");
			goTo(GotoLabel.exit4090);
		}
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call     
	*   the generalised secondary switching module to obtain the      
	*   next 8 programs and load them into the program stack.         
	* </pre>
	*/

protected void chdrenq()
{
chdrenq2IO.setChdrcoy(wsspcomn.company);
chdrenq2IO.setChdrnum(covrenqIO.getChdrnum());
chdrenq2IO.setFunction(varcom.readr);
chdrenq2IO.setFormat(chdrenqrec);
SmartFileCode.execute(appVars, chdrenq2IO);
if (isNE(chdrenq2IO.getStatuz(),varcom.oK)
&& isNE(chdrenq2IO.getStatuz(),varcom.mrnf)) {
	syserrrec.params.set(chdrenq2IO.getParams());
	syserrrec.statuz.set(chdrenq2IO.getStatuz());
	fatalError600();
}


chdrenq2IO.setFunction(varcom.keeps);
SmartFileCode.execute(appVars, chdrenq2IO);
if (isNE(chdrenq2IO.getStatuz(),varcom.oK)
&& isNE(chdrenq2IO.getStatuz(),varcom.mrnf)) {
	syserrrec.params.set(chdrenq2IO.getParams());
	syserrrec.statuz.set(chdrenq2IO.getStatuz());
	fatalError600();
}	





}



protected void antcpEndwPayDet(){
	if (isNE(sv.aepaydet, "X")) {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		return ;
	}
	sv.aepaydet.set("?");
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
		saveProgramStack4200();
	}
	gensswrec.function.set("I");
	wsspcomn.chdrChdrnum.set(covrenqIO.getChdrnum());
	wsspcomn.crtable.set(covrenqIO.getCrtable());
	
	wsspcomn.cmode.set("IFE");
	
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		scrnparams.errorCode.set(h093);
		wsspcomn.nextprog.set(scrnparams.scrname);
		if (exclpf==null) {
			sv.aepaydet.set(" ");
			return ;
		}
		else {
			sv.aepaydet.set("+");
			return ;
		}
	}
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
		loadProgramStack4300();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.programPtr.add(1);

}
protected void antcpEndwPayDet1(){	
	sv.aepaydet.set("+");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		restoreProgram4100();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	
}

protected void gensww4010()
	{
		if (isEQ(gensswrec.function,SPACES)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
			else {
				/*          MOVE SPACES         TO WSSP-NEXTPROG        <LA2108>*/
				wsspcomn.nextprog.set(wsaaProg);
				wsspcomn.programPtr.add(1);
			}
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay <LA2108>*/
		/* with an error and the options and extras indicator              */
		/* with its initial load value                                     */
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void getLifeName4800()
	{
		para4800();
	}

protected void para4800()
	{
		lifeenqIO.setChdrcoy(covrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrenqIO.getChdrnum());
		lifeenqIO.setLife(covrenqIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}


protected void CheckZraeRecords()
{
	zraeIO.setDataKey(SPACES); 
	zraeIO.setChdrcoy(covrenqIO.getChdrcoy());
	zraeIO.setChdrnum(covrenqIO.getChdrnum());
	zraeIO.setPlanSuffix(ZERO);
	zraeIO.setFunction(varcom.begnh);
	zraeIO.setFormat(zraerec);
	SmartFileCode.execute(appVars, zraeIO);
	if (isNE(zraeIO.getStatuz(), varcom.oK)
	|| isEQ(zraeIO.getStatuz(), varcom.endp)
	|| isEQ(zraeIO.getValidflag(), "2")
	|| isNE(zraeIO.getChdrnum(), covrenqIO.getChdrnum())) {
		syserrrec.params.set(zraeIO.getParams());
		syserrrec.statuz.set(zraeIO.getStatuz());
		fatalError600();
	}else
	{
		sv.aepaydet.set("+");
	}
	
}


protected void a200CheckCalcTax()
	{
		a210Start();
	}

protected void a210Start()
	{
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(covrenqIO.getLife());
		txcalcrec.coverage.set(covrenqIO.getCoverage());
		txcalcrec.rider.set(covrenqIO.getRider());
		txcalcrec.planSuffix.set(covrenqIO.getPlanSuffix());
		txcalcrec.crtable.set(covrenqIO.getCrtable());
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.register.set(chdrenqIO.getReg());
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.ccy.set(chdrenqIO.getCntcurr());
		txcalcrec.amountIn.set(ZERO);
		txcalcrec.transType.set("PREM");
		txcalcrec.txcode.set(tr52drec.txcode);
		if (isNE(chdrenqIO.getBillfreq(), "00")) {
			txcalcrec.effdate.set(chdrenqIO.getPtdate());
		}
		else {
			txcalcrec.effdate.set(chdrenqIO.getOccdate());
		}
		txcalcrec.tranno.set(chdrenqIO.getTranno());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			wsaaMiscellaneousInner.wsaaTaxamt.set(sv.instPrem);
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[2]);
			}
			sv.taxamt.set(wsaaMiscellaneousInner.wsaaTaxamt);
			sv.taxind.set("+");
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private PackedDecimalData wsaaSuffixCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaTh505Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(5).isAPartOf(wsaaTh505Key, 0);
	private FixedLengthStringData wsaaCrcyCode = new FixedLengthStringData(3).isAPartOf(wsaaTh505Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData th505 = new FixedLengthStringData(5).init("TH505");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData hpuarec = new FixedLengthStringData(10).init("HPUAREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
}
}
