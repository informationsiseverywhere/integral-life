/*
 * File: P6761.java
 * Date: 30 August 2009 0:56:25
 * Author: Quipoz Limited
 * 
 * Class transformed from P6761.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CwfddelTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                  Windforward Cancel Completion
*                  -----------------------------
*
*    This is a non-screen program which is accessed after a
*    deletion of windforward records has occurred.
*
*    The program checks for any outstanding windforward records
*    for this contract.  If none are found, the Contract Header
*    record is returned to its position prior to Windforward and
*    the Register Windforward transaction record is deleted.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6761 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6761");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaChange = "N";
	private String chdrmjarec = "CHDRMJAREC";
	private String cwfddelrec = "CWFDDELREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Contract Windforward logical for Deletio*/
	private CwfddelTableDAM cwfddelIO = new CwfddelTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Wssplife wssplife = new Wssplife();

	public P6761() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaChange = "N";
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
	}

protected void screenEdit2000()
	{
		/*START*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		cwfddelIO.setDataArea(SPACES);
		cwfddelIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfddelIO.setChdrcoy(chdrenqIO.getChdrcoy());
		cwfddelIO.setPrcSeqNbr(ZERO);
		cwfddelIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		cwfddelIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		cwfddelIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		cwfddelIO.setFormat(cwfddelrec);
		SmartFileCode.execute(appVars, cwfddelIO);
		if (isNE(cwfddelIO.getStatuz(),varcom.oK)
		&& isNE(cwfddelIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cwfddelIO.getStatuz());
			syserrrec.params.set(cwfddelIO.getParams());
			fatalError600();
		}
		if (isNE(cwfddelIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(cwfddelIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isEQ(cwfddelIO.getStatuz(),varcom.endp)) {
			wsaaChange = "Y";
		}
		if (isEQ(wsaaChange,"Y")) {
			updatePtrn3100();
			updateChdr3200();
		}
	}

protected void updatePtrn3100()
	{
		para3110();
	}

protected void para3110()
	{
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrnum(chdrenqIO.getChdrnum());
		ptrnrevIO.setChdrcoy(chdrenqIO.getChdrcoy());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		ptrnrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
	}

protected void updateChdr3200()
	{
		start3310();
	}

protected void start3310()
	{
		chdrmjaIO.setChdrnum(chdrenqIO.getChdrnum());
		chdrmjaIO.setChdrcoy(chdrenqIO.getChdrcoy());
		chdrmjaIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		chdrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		chdrmjaIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
