package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:51
 * @author Quipoz
 */
public class St525screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St525ScreenVars sv = (St525ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.st525screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.st525screensfl, 
			sv.St525screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		St525ScreenVars sv = (St525ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.st525screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		St525ScreenVars sv = (St525ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.st525screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.St525screensflWritten.gt(0))
		{
			sv.st525screensfl.setCurrentIndex(0);
			sv.St525screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		St525ScreenVars sv = (St525ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.st525screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			St525ScreenVars screenVars = (St525ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.tranno.setFieldName("tranno");
				screenVars.zbamt.setFieldName("zbamt");
				screenVars.tdbtrate.setFieldName("tdbtrate");
				screenVars.fromdateDisp.setFieldName("fromdateDisp");
				screenVars.todateDisp.setFieldName("todateDisp");
				screenVars.payable.setFieldName("payable");
				screenVars.lngdes.setFieldName("lngdes");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.tranno.set(dm.getField("tranno"));
			screenVars.zbamt.set(dm.getField("zbamt"));
			screenVars.tdbtrate.set(dm.getField("tdbtrate"));
			screenVars.fromdateDisp.set(dm.getField("fromdateDisp"));
			screenVars.todateDisp.set(dm.getField("todateDisp"));
			screenVars.payable.set(dm.getField("payable"));
			screenVars.lngdes.set(dm.getField("lngdes"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			St525ScreenVars screenVars = (St525ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.tranno.setFieldName("tranno");
				screenVars.zbamt.setFieldName("zbamt");
				screenVars.tdbtrate.setFieldName("tdbtrate");
				screenVars.fromdateDisp.setFieldName("fromdateDisp");
				screenVars.todateDisp.setFieldName("todateDisp");
				screenVars.payable.setFieldName("payable");
				screenVars.lngdes.setFieldName("lngdes");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("zbamt").set(screenVars.zbamt);
			dm.getField("tdbtrate").set(screenVars.tdbtrate);
			dm.getField("fromdateDisp").set(screenVars.fromdateDisp);
			dm.getField("todateDisp").set(screenVars.todateDisp);
			dm.getField("payable").set(screenVars.payable);
			dm.getField("lngdes").set(screenVars.lngdes);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		St525screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		St525ScreenVars screenVars = (St525ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.zbamt.clearFormatting();
		screenVars.tdbtrate.clearFormatting();
		screenVars.fromdateDisp.clearFormatting();
		screenVars.todateDisp.clearFormatting();
		screenVars.payable.clearFormatting();
		screenVars.lngdes.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		St525ScreenVars screenVars = (St525ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.zbamt.setClassString("");
		screenVars.tdbtrate.setClassString("");
		screenVars.fromdateDisp.setClassString("");
		screenVars.todateDisp.setClassString("");
		screenVars.payable.setClassString("");
		screenVars.lngdes.setClassString("");
	}

/**
 * Clear all the variables in St525screensfl
 */
	public static void clear(VarModel pv) {
		St525ScreenVars screenVars = (St525ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.tranno.clear();
		screenVars.zbamt.clear();
		screenVars.tdbtrate.clear();
		screenVars.fromdateDisp.clear();
		screenVars.fromdate.clear();
		screenVars.todateDisp.clear();
		screenVars.todate.clear();
		screenVars.payable.clear();
		screenVars.lngdes.clear();
	}
}
