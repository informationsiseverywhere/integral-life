/*
 * File: P5076.java
 * Date: 30 August 2009 0:03:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P5076.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.life.contractservicing.screens.S5076ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* JOINT OWNER MAINTENANCE.
*
* Retrieves the contract header details from the contract header
* file (CHDRLNB).    Using RETRV.
*
* The joint owner details (i.e. Address) are retrieved from the
* client details file (CLTS). If the joint owner number (jownnum
* from the header) is blank then the name and address fields are
* left blank.
*
* The joint owner number may be changed, and will be checked
* against the client file.
*
* If 'CALC' is pressed then the screen is redisplayed.
*
* If 'KILL' is pressed then all validation is ignored and the
* joint owner address in the header is left blank - ignored.
*
* Update  the joint owner in the header record held in the
* buffer using KEEPS.
*
* NOTE: The retrieval of joint owner details and movement of
* these details for both the 1000 and 2000 section is in the
* 1600-RETRIEVAL section.
*
*****************************************************************
* </pre>
*/
public class P5076 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5076");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private static final String e040 = "E040";
	private static final String f782 = "F782";
	private static final String f464 = "F464";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client role and relationships logical fi*/
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5076ScreenVars sv = ScreenProgram.getScreenVars( S5076ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		exit3900
	}

	public P5076() {
		super();
		screenVars = sv;
		new ScreenModel("S5076", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
					initialise1100();
					checkInquiry1490();
				}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		if (isEQ(chdrlnbIO.getJownnum(),SPACES)) {
			sv.jownname.set(SPACES);
			sv.cltaddrs.set(SPACES);
			sv.cltpcode.set(SPACES);
			/*    GO TO 1500-EXIT                                           */
			return ;
		}
		else {
			cltsIO.setClntnum(chdrlnbIO.getJownnum());
		}
		retrieve1600();
		if (isEQ(chdrlnbIO.getJownnum(),SPACES)) {
			sv.jownsel.set(SPACES);
		}
		else {
			sv.jownsel.set(cltsIO.getClntnum());
		}
	}

protected void checkInquiry1490()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			sv.jownselOut[varcom.pr.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void retrieve1600()
	{
			retrieve1700();
		}

protected void retrieve1700()
	{
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.cltaddrs.set(SPACES);
		sv.jownname.set(SPACES);
		sv.cltpcode.set(SPACES);
		/* If the client details are not found then an error is*/
		/* given.*/
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.jownselErr.set(e040);
			return ;
		}
		if (isLTE(cltsIO.getCltdod(),chdrlnbIO.getOccdate())) {
			sv.jownselErr.set(f782);
		}
		plainname();
		sv.jownname.set(wsspcomn.longconfname);
		sv.cltaddr01.set(cltsIO.getCltaddr01());
		sv.cltaddr02.set(cltsIO.getCltaddr02());
		sv.cltaddr03.set(cltsIO.getCltaddr03());
		sv.cltaddr04.set(cltsIO.getCltaddr04());
		sv.cltaddr05.set(cltsIO.getCltaddr05());
		sv.cltpcode.set(cltsIO.getCltpcode());
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2100();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*    If F11 pressed bypass validation.*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2100()
	{
		if (isNE(sv.jownsel,SPACES)) {
			cltsIO.setDataArea(SPACES);
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntnum(sv.jownsel);
			retrieve1600();
		}
		if (isEQ(sv.jownsel,chdrlnbIO.getCownnum())) {
			sv.jownselErr.set(f464);
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3100();
			contractHeader3110();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3100()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3900);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3900);
		}
		cltrelnrec.cltrelnRec.set(SPACES);
		if (isEQ(sv.jownsel,SPACES)) {
			if (isEQ(chdrlnbIO.getJownnum(),SPACES)) {
				/*NEXT_SENTENCE*/
			}
			else {
				cltrelnrec.function.set("REM  ");
				cltrelnrec.clntnum.set(chdrlnbIO.getJownnum());
				callCltreln3500();
			}
		}
		else {
			if (isEQ(chdrlnbIO.getJownnum(),SPACES)) {
				cltrelnrec.function.set("ADD  ");
				cltrelnrec.clntnum.set(sv.jownsel);
				callCltreln3500();
			}
			else {
				cltrelnrec.function.set("REM  ");
				cltrelnrec.clntnum.set(chdrlnbIO.getJownnum());
				callCltreln3500();
				cltrelnrec.function.set("ADD  ");
				cltrelnrec.clntnum.set(sv.jownsel);
				callCltreln3500();
			}
		}
	}

protected void contractHeader3110()
	{
		chdrlnbIO.setJownnum(sv.jownsel);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void callCltreln3500()
	{
		/*CALL-CLTRELN*/
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(chdrlnbIO.getCowncoy());
		cltrelnrec.clrrrole.set("JO");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrlnbIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrlnbIO.getChdrnum());
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}
}
