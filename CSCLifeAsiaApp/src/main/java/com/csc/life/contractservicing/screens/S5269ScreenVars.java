package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5269
 * @version 1.0 generated on 30/08/09 06:38
 * @author Quipoz
 */
public class S5269ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData causeofdth = DD.causeofdth.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData clamamt = DD.clamamt.copyToZonedDecimal().isAPartOf(dataFields,4);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,21);
	public ZonedDecimalData dtofdeath = DD.dtofdeath.copyToZonedDecimal().isAPartOf(dataFields,24);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,32);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,40);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,57);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,58);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,75);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,96);
	public ZonedDecimalData tdbtamt = DD.tdbtamt.copyToZonedDecimal().isAPartOf(dataFields,146);
	public ZonedDecimalData zrcshamt = DD.zrcshamt.copyToZonedDecimal().isAPartOf(dataFields,163);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(dataFields,176);
	public FixedLengthStringData astrsk = DD.astrsk.copy().isAPartOf(dataFields,177);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,178);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,186);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,194);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,197);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,290);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,298);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,345);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,353);
	/*ILIFE-965 start sgadkari*/
	public FixedLengthStringData pstate = DD.pstate_longdesc.copy().isAPartOf(dataFields,400);	
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,430);	
	public FixedLengthStringData rstate = DD.rstate_longdesc.copy().isAPartOf(dataFields,438);
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,447);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData causeofdthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clamamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dtofdeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData tdbtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData zrcshamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData astrskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	/*ILIFE-965 Ends*/
	public FixedLengthStringData[] causeofdthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clamamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dtofdeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] tdbtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] zrcshamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] astrskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(187);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(57).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,22);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,26);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(subfileFields,43);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,45);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,47);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 57);
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 89);
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 185);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dtofdeathDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5269screensflWritten = new LongData(0);
	public LongData S5269screenctlWritten = new LongData(0);
	public LongData S5269screenWritten = new LongData(0);
	public LongData S5269protectWritten = new LongData(0);
	public GeneralTable s5269screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5269screensfl;
	}

	public S5269ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {coverage, rider, crtable, shortds, liencd, cnstcur, estMatValue, actvalue};
		screenSflOutFields = new BaseData[][] {coverageOut, riderOut, crtableOut, shortdsOut, liencdOut, cnstcurOut, emvOut, actvalueOut};
		screenSflErrFields = new BaseData[] {coverageErr, riderErr, crtableErr, shortdsErr, liencdErr, cnstcurErr, emvErr, actvalueErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		/*screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, asterisk, linsname, jlifcnum, astrsk, jlinsname, ptdate, btdate, policyloan, effdate, causeofdth, otheradjst, fupflg, estimateTotalValue, clamamt, reasoncd, resndesc, dtofdeath, currcd, zrcshamt, tdbtamt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, asteriskOut, linsnameOut, jlifcnumOut, astrskOut, jlinsnameOut, ptdateOut, btdateOut, policyloanOut, effdateOut, causeofdthOut, otheradjstOut, fupflgOut, estimtotalOut, clamamtOut, reasoncdOut, resndescOut, dtofdeathOut, currcdOut, zrcshamtOut, tdbtamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, asteriskErr, linsnameErr, jlifcnumErr, astrskErr, jlinsnameErr, ptdateErr, btdateErr, policyloanErr, effdateErr, causeofdthErr, otheradjstErr, fupflgErr, estimtotalErr, clamamtErr, reasoncdErr, resndescErr, dtofdeathErr, currcdErr, zrcshamtErr, tdbtamtErr};
	*/	screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate, dtofdeath};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr, dtofdeathErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp, dtofdeathDisp};
		
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		fieldIndMap.put(claimnumberOut,new String[] {null,null, null,"19", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5269screen.class;
		screenSflRecord = S5269screensfl.class;
		screenCtlRecord = S5269screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5269protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5269screenctl.lrec.pageSubfile);
	}
	public int getDataAreaSize() {
		return 957;
	}
	public int getDataFieldsSize(){
		return 477;
	}
	public int getErrorIndicatorSize(){
		return 120;
	}
	public int getOutputFieldSize(){
		return 360;
	}
	
	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, asterisk, linsname, jlifcnum, astrsk, jlinsname, ptdate, btdate, policyloan, effdate, causeofdth, otheradjst, fupflg, estimateTotalValue, clamamt, reasoncd, resndesc, dtofdeath, currcd, zrcshamt, tdbtamt, claimnumber};     
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, asteriskOut, linsnameOut, jlifcnumOut, astrskOut, jlinsnameOut, ptdateOut, btdateOut, policyloanOut, effdateOut, causeofdthOut, otheradjstOut, fupflgOut, estimtotalOut, clamamtOut, reasoncdOut, resndescOut, dtofdeathOut, currcdOut, zrcshamtOut, tdbtamtOut, claimnumberOut};    
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, asteriskErr, linsnameErr, jlifcnumErr, astrskErr, jlinsnameErr, ptdateErr, btdateErr, policyloanErr, effdateErr, causeofdthErr, otheradjstErr, fupflgErr, estimtotalErr, clamamtErr, reasoncdErr, resndescErr, dtofdeathErr, currcdErr, zrcshamtErr, tdbtamtErr, claimnumberErr};    
		
	}
}
