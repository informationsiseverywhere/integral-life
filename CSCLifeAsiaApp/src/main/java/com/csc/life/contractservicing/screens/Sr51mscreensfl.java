package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr51mscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 4;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1,  48,  51,  68,  70,  71}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {13, 15, 4, 66}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51mScreenVars sv = (Sr51mScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr51mscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr51mscreensfl, 
			sv.Sr51mscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr51mScreenVars sv = (Sr51mScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr51mscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr51mScreenVars sv = (Sr51mScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr51mscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr51mscreensflWritten.gt(0))
		{
			sv.sr51mscreensfl.setCurrentIndex(0);
			sv.Sr51mscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr51mScreenVars sv = (Sr51mScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr51mscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51mScreenVars screenVars = (Sr51mScreenVars) pv;
			if (screenVars.zrwvflg01.getFieldName() == null) {
				screenVars.zrwvflg01.setFieldName("zrwvflg01");
				screenVars.zrwvflg02.setFieldName("zrwvflg02");
				screenVars.zrwvflg03.setFieldName("zrwvflg03");
				screenVars.zrwvflg04.setFieldName("zrwvflg04");
				screenVars.waiverprem.setFieldName("waiverprem");
				screenVars.ind.setFieldName("ind");
				screenVars.wvfind.setFieldName("wvfind");
				screenVars.genarea.setFieldName("genarea");
				screenVars.datakey.setFieldName("datakey");
				screenVars.zbinstprem.setFieldName("zbinstprem");
				screenVars.zlinstprem.setFieldName("zlinstprem");
				screenVars.rerateDateDisp.setFieldName("rerateDateDisp");
				screenVars.singp.setFieldName("singp");
				screenVars.origSum.setFieldName("origSum");
				screenVars.workAreaData.setFieldName("workAreaData");
				screenVars.rtrnwfreq.setFieldName("rtrnwfreq");
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.benpln.setFieldName("benpln");
				screenVars.zrwvflg05.setFieldName("zrwvflg05");
				screenVars.livesno.setFieldName("livesno");
				screenVars.waivercode.setFieldName("waivercode");
				screenVars.chgflag.setFieldName("chgflag");
				screenVars.zunit.setFieldName("zunit");
				screenVars.bftpaym.setFieldName("bftpaym");
				screenVars.premind.setFieldName("premind");
				screenVars.select.setFieldName("select");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.crtabled.setFieldName("crtabled");
				screenVars.crtable.setFieldName("crtable");
				screenVars.statcde.setFieldName("statcde");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.sumins.setFieldName("sumins");
				screenVars.instprem.setFieldName("instprem");
			}
			screenVars.zrwvflg01.set(dm.getField("zrwvflg01"));
			screenVars.zrwvflg02.set(dm.getField("zrwvflg02"));
			screenVars.zrwvflg03.set(dm.getField("zrwvflg03"));
			screenVars.zrwvflg04.set(dm.getField("zrwvflg04"));
			screenVars.waiverprem.set(dm.getField("waiverprem"));
			screenVars.ind.set(dm.getField("ind"));
			screenVars.wvfind.set(dm.getField("wvfind"));
			screenVars.genarea.set(dm.getField("genarea"));
			screenVars.datakey.set(dm.getField("datakey"));
			screenVars.zbinstprem.set(dm.getField("zbinstprem"));
			screenVars.zlinstprem.set(dm.getField("zlinstprem"));
			screenVars.rerateDateDisp.set(dm.getField("rerateDateDisp"));
			screenVars.singp.set(dm.getField("singp"));
			screenVars.origSum.set(dm.getField("origSum"));
			screenVars.workAreaData.set(dm.getField("workAreaData"));
			screenVars.rtrnwfreq.set(dm.getField("rtrnwfreq"));
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.benpln.set(dm.getField("benpln"));
			screenVars.zrwvflg05.set(dm.getField("zrwvflg05"));
			screenVars.livesno.set(dm.getField("livesno"));
			screenVars.waivercode.set(dm.getField("waivercode"));
			screenVars.chgflag.set(dm.getField("chgflag"));
			screenVars.zunit.set(dm.getField("zunit"));
			screenVars.bftpaym.set(dm.getField("bftpaym"));
			screenVars.premind.set(dm.getField("premind"));
			screenVars.select.set(dm.getField("select"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.crtabled.set(dm.getField("crtabled"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.statcde.set(dm.getField("statcde"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.sumins.set(dm.getField("sumins"));
			screenVars.instprem.set(dm.getField("instprem"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51mScreenVars screenVars = (Sr51mScreenVars) pv;
			if (screenVars.zrwvflg01.getFieldName() == null) {
				screenVars.zrwvflg01.setFieldName("zrwvflg01");
				screenVars.zrwvflg02.setFieldName("zrwvflg02");
				screenVars.zrwvflg03.setFieldName("zrwvflg03");
				screenVars.zrwvflg04.setFieldName("zrwvflg04");
				screenVars.waiverprem.setFieldName("waiverprem");
				screenVars.ind.setFieldName("ind");
				screenVars.wvfind.setFieldName("wvfind");
				screenVars.genarea.setFieldName("genarea");
				screenVars.datakey.setFieldName("datakey");
				screenVars.zbinstprem.setFieldName("zbinstprem");
				screenVars.zlinstprem.setFieldName("zlinstprem");
				screenVars.rerateDateDisp.setFieldName("rerateDateDisp");
				screenVars.singp.setFieldName("singp");
				screenVars.origSum.setFieldName("origSum");
				screenVars.workAreaData.setFieldName("workAreaData");
				screenVars.rtrnwfreq.setFieldName("rtrnwfreq");
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.benpln.setFieldName("benpln");
				screenVars.zrwvflg05.setFieldName("zrwvflg05");
				screenVars.livesno.setFieldName("livesno");
				screenVars.waivercode.setFieldName("waivercode");
				screenVars.chgflag.setFieldName("chgflag");
				screenVars.zunit.setFieldName("zunit");
				screenVars.bftpaym.setFieldName("bftpaym");
				screenVars.premind.setFieldName("premind");
				screenVars.select.setFieldName("select");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.crtabled.setFieldName("crtabled");
				screenVars.crtable.setFieldName("crtable");
				screenVars.statcde.setFieldName("statcde");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.sumins.setFieldName("sumins");
				screenVars.instprem.setFieldName("instprem");
			}
			dm.getField("zrwvflg01").set(screenVars.zrwvflg01);
			dm.getField("zrwvflg02").set(screenVars.zrwvflg02);
			dm.getField("zrwvflg03").set(screenVars.zrwvflg03);
			dm.getField("zrwvflg04").set(screenVars.zrwvflg04);
			dm.getField("waiverprem").set(screenVars.waiverprem);
			dm.getField("ind").set(screenVars.ind);
			dm.getField("wvfind").set(screenVars.wvfind);
			dm.getField("genarea").set(screenVars.genarea);
			dm.getField("datakey").set(screenVars.datakey);
			dm.getField("zbinstprem").set(screenVars.zbinstprem);
			dm.getField("zlinstprem").set(screenVars.zlinstprem);
			dm.getField("rerateDateDisp").set(screenVars.rerateDateDisp);
			dm.getField("singp").set(screenVars.singp);
			dm.getField("origSum").set(screenVars.origSum);
			dm.getField("workAreaData").set(screenVars.workAreaData);
			dm.getField("rtrnwfreq").set(screenVars.rtrnwfreq);
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("benpln").set(screenVars.benpln);
			dm.getField("zrwvflg05").set(screenVars.zrwvflg05);
			dm.getField("livesno").set(screenVars.livesno);
			dm.getField("waivercode").set(screenVars.waivercode);
			dm.getField("chgflag").set(screenVars.chgflag);
			dm.getField("zunit").set(screenVars.zunit);
			dm.getField("bftpaym").set(screenVars.bftpaym);
			dm.getField("premind").set(screenVars.premind);
			dm.getField("select").set(screenVars.select);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("crtabled").set(screenVars.crtabled);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("statcde").set(screenVars.statcde);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("sumins").set(screenVars.sumins);
			dm.getField("instprem").set(screenVars.instprem);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr51mscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr51mScreenVars screenVars = (Sr51mScreenVars)pv;
		screenVars.zrwvflg01.clearFormatting();
		screenVars.zrwvflg02.clearFormatting();
		screenVars.zrwvflg03.clearFormatting();
		screenVars.zrwvflg04.clearFormatting();
		screenVars.waiverprem.clearFormatting();
		screenVars.ind.clearFormatting();
		screenVars.wvfind.clearFormatting();
		screenVars.genarea.clearFormatting();
		screenVars.datakey.clearFormatting();
		screenVars.zbinstprem.clearFormatting();
		screenVars.zlinstprem.clearFormatting();
		screenVars.rerateDateDisp.clearFormatting();
		screenVars.singp.clearFormatting();
		screenVars.origSum.clearFormatting();
		screenVars.workAreaData.clearFormatting();
		screenVars.rtrnwfreq.clearFormatting();
		screenVars.screenIndicArea.clearFormatting();
		screenVars.benpln.clearFormatting();
		screenVars.zrwvflg05.clearFormatting();
		screenVars.livesno.clearFormatting();
		screenVars.waivercode.clearFormatting();
		screenVars.chgflag.clearFormatting();
		screenVars.zunit.clearFormatting();
		screenVars.bftpaym.clearFormatting();
		screenVars.premind.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.crtabled.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.statcde.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.sumins.clearFormatting();
		screenVars.instprem.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr51mScreenVars screenVars = (Sr51mScreenVars)pv;
		screenVars.zrwvflg01.setClassString("");
		screenVars.zrwvflg02.setClassString("");
		screenVars.zrwvflg03.setClassString("");
		screenVars.zrwvflg04.setClassString("");
		screenVars.waiverprem.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.wvfind.setClassString("");
		screenVars.genarea.setClassString("");
		screenVars.datakey.setClassString("");
		screenVars.zbinstprem.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.rerateDateDisp.setClassString("");
		screenVars.singp.setClassString("");
		screenVars.origSum.setClassString("");
		screenVars.workAreaData.setClassString("");
		screenVars.rtrnwfreq.setClassString("");
		screenVars.screenIndicArea.setClassString("");
		screenVars.benpln.setClassString("");
		screenVars.zrwvflg05.setClassString("");
		screenVars.livesno.setClassString("");
		screenVars.waivercode.setClassString("");
		screenVars.chgflag.setClassString("");
		screenVars.zunit.setClassString("");
		screenVars.bftpaym.setClassString("");
		screenVars.premind.setClassString("");
		screenVars.select.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtabled.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.statcde.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.instprem.setClassString("");
	}

/**
 * Clear all the variables in Sr51mscreensfl
 */
	public static void clear(VarModel pv) {
		Sr51mScreenVars screenVars = (Sr51mScreenVars) pv;
		screenVars.zrwvflg01.clear();
		screenVars.zrwvflg02.clear();
		screenVars.zrwvflg03.clear();
		screenVars.zrwvflg04.clear();
		screenVars.waiverprem.clear();
		screenVars.ind.clear();
		screenVars.wvfind.clear();
		screenVars.genarea.clear();
		screenVars.datakey.clear();
		screenVars.zbinstprem.clear();
		screenVars.zlinstprem.clear();
		screenVars.rerateDateDisp.clear();
		screenVars.rerateDate.clear();
		screenVars.singp.clear();
		screenVars.origSum.clear();
		screenVars.workAreaData.clear();
		screenVars.rtrnwfreq.clear();
		screenVars.screenIndicArea.clear();
		screenVars.benpln.clear();
		screenVars.zrwvflg05.clear();
		screenVars.livesno.clear();
		screenVars.waivercode.clear();
		screenVars.chgflag.clear();
		screenVars.zunit.clear();
		screenVars.bftpaym.clear();
		screenVars.premind.clear();
		screenVars.select.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.crtabled.clear();
		screenVars.crtable.clear();
		screenVars.statcde.clear();
		screenVars.pstatcode.clear();
		screenVars.sumins.clear();
		screenVars.instprem.clear();
	}
}
