package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6258
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6258ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(734);
	public FixedLengthStringData dataFields = new FixedLengthStringData(350).isAPartOf(dataArea, 0);
	public ZonedDecimalData payToDateAdvance = DD.advptdat.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,66);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,68);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,84);
	public ZonedDecimalData cntinst = DD.cntinst.copyToZonedDecimal().isAPartOf(dataFields,87);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,107);
	public ZonedDecimalData cpiDate = DD.cpidte.copyToZonedDecimal().isAPartOf(dataFields,115);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,123);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,153);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,161);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,216);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,263);
	public ZonedDecimalData prmdepst = DD.prmdepst.copyToZonedDecimal().isAPartOf(dataFields,271);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,288);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,298);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,306);
	public ZonedDecimalData sacscurbal = DD.sacscurbal.copyToZonedDecimal().isAPartOf(dataFields,316);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,333);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 350);
	public FixedLengthStringData advptdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntinstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cpidteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData prmdepstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData sacscurbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 446);
	public FixedLengthStringData[] advptdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntinstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cpidteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] prmdepstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] sacscurbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData payToDateAdvanceDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cpiDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S6258screenWritten = new LongData(0);
	public LongData S6258protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6258ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ptdateOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(advptdatOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, payrnum, payorname, agntnum, agentname, occdate, ptdate, cntcurr, btdate, billfreq, cntinst, sacscurbal, payToDateAdvance, effdate, cpiDate, billcurr, prmdepst, taxamt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, payrnumOut, payornameOut, agntnumOut, agentnameOut, occdateOut, ptdateOut, cntcurrOut, btdateOut, billfreqOut, cntinstOut, sacscurbalOut, advptdatOut, effdateOut, cpidteOut, billcurrOut, prmdepstOut, taxamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, payrnumErr, payornameErr, agntnumErr, agentnameErr, occdateErr, ptdateErr, cntcurrErr, btdateErr, billfreqErr, cntinstErr, sacscurbalErr, advptdatErr, effdateErr, cpidteErr, billcurrErr, prmdepstErr, taxamtErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, payToDateAdvance, effdate, cpiDate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, advptdatErr, effdateErr, cpidteErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, payToDateAdvanceDisp, effdateDisp, cpiDateDisp};
		
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6258screen.class;
		protectRecord = S6258protect.class;
	}

}
