/*
 * File: P5051bo.java
 * Date: 30 August 2009 0:00:51
 * Author: Quipoz Limited
 * 
 * Class transformed from P5051BO.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;

import com.csc.fsu.general.recordstructures.Sdartnrec;
import com.csc.life.contractservicing.programs.P5051;
import com.csc.life.contractservicing.screens.S5051ScreenVars;
import com.csc.smart.dataaccess.FlddmntTableDAM;
import com.csc.smart.dataaccess.SwndfldTableDAM;
import com.csc.smart.procedures.Geterror;
import com.csc.smart.recordstructures.Boverrrec;
import com.csc.smart400framework.parent.Mainx;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrcpy2;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2002.
*    All rights reserved. CSC Confidential.
*
*              Business Object Skeleton
*
***********************************************************************
* </pre>
*/
public class P5051bo extends Mainx {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(12).init("P5051BO     ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaStat = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLang = new FixedLengthStringData(1);
	private PackedDecimalData wsaaCnt = new PackedDecimalData(3, 0);
		/* VAL-ERRORS */
	private PackedDecimalData valErrorCnt = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaInKey = new FixedLengthStringData(50);
	private FixedLengthStringData wsaaInChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaInKey, 0);
	private ZonedDecimalData wsaaInDteeff = new ZonedDecimalData(8, 0).isAPartOf(wsaaInKey, 8).setUnsigned();
	private FlddmntTableDAM flddmntIO = new FlddmntTableDAM();
	private SwndfldTableDAM swndfldIO = new SwndfldTableDAM();
	private Sdartnrec sdartnrec = new Sdartnrec();
	private Boverrrec validationError = new Boverrrec();
	private Dbcstrcpy2 dbcstrcpy2 = new Dbcstrcpy2();
	private S5051ScreenVars sv1 = ScreenProgram.getScreenVars( S5051ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		aExit
	}

	public P5051bo() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		responseData = convertAndSetParam(responseData, parmArray, 2);
		requestData = convertAndSetParam(requestData, parmArray, 1);
		ldrhdr.header = convertAndSetParam(ldrhdr.header, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		/*START*/
		scrnparams.statuz.set(varcom.oK);
		ldrhdr.errlvl.set("0");
		/*EXIT*/
	}

protected void callPrograms2000()
	{
		/*CALL*/
		wsspcomn.edterror.set(varcom.oK);
		aInquiry();
		return ;
		/* UNREACHABLE CODE
		if (isEQ(ldrhdr.vrbid, SPACES)){
		}
		else{
			invalidMethodPassed();
		}
		*/
	}

protected void invalidMethodPassed()
	{
		/*INVALID-METHOD-PASSED-START*/
		syserrrec.statuz.set("IVMT");
		syserrrec.subrname.set(wsaaProg);
		syserrrec.dbparams.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("Invalid method passed:       ");
		stringVariable1.addExpression(ldrhdr.vrbid);
		stringVariable1.setStringInto(syserrrec.dbparams);
		syserrrec.syserrType.set("2");
		fatalError600();
		/*INVALID-METHOD-PASSED-EXIT*/
	}

protected void updateWssp9000()
	{
		/*PARA*/
		bowscpy.commonArea.set(wsspcomn.commonArea);
		bowscpy.userArea.set(wsspUserArea);
		/*CONTINUE_STMT*/
		/*EXIT*/
	}

protected void aInquiry()
	{
		try {
			aP5051Start();
			aP5051End();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void aP5051Start()
	{
		sdartnrec.rec.set(requestData);
		wsspcomn.company.set(sdartnrec.sdarCompany);
		wsspcomn.sbmaction.set("B");
		scrnparams.action.set("B");
		sv1.action.set("B");
		wsspcomn.sectionno.set("1000");
		callProgram(P5051.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		if (isEQ(wsspcomn.edterror, SPACES)) {
			wsspcomn.edterror.set(varcom.oK);
		}
		if (isEQ(scrnparams.statuz, SPACES)) {
			scrnparams.statuz.set(varcom.oK);
		}
		wsspcomn.sectionno.set("PRE");
		callProgram(P5051.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		wsaaInKey.set(sdartnrec.sdarInKey);
		sv1.chdrsel.set(wsaaInChdrnum);
		wsspcomn.edterror.set(varcom.oK);
		wsspcomn.sectionno.set("2000");
		callProgram(P5051.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		sdartnrec.sdarStatuz.set(sv1.chdrselErr);
		/* Replace the call to SVERRCPY by BOVERRCPY.                      */
		setUpErrorsP5051();
		if (isEQ(ldrhdr.opmode, "0")
		|| (isEQ(ldrhdr.opmode, "1"))){
			if (isNE(wsspcomn.edterror, varcom.oK)) {
				validationError.msgid.set("BOVERR");
				validationError.msgcnt.set(1);
				compute(validationError.msglng, 0).set(length(validationError) - length(validationError.header));
				ldrhdr.totmsglng.set(length(validationError));
				validationError.boverrLanguage.set(wsspcomn.language);
				responseData.set(validationError);
				ldrhdr.errlvl.set("1");
				goTo(GotoLabel.aExit);
			}
		}
		else if (isEQ(ldrhdr.opmode, "2")){
			/*CONTINUE_STMT*/
		}
		wsspcomn.sectionno.set("3000");
		callProgram(P5051.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		setUpErrorsP5051();
		if (isEQ(ldrhdr.opmode, "0")
		|| (isEQ(ldrhdr.opmode, "1"))){
			if (isNE(wsspcomn.edterror, varcom.oK)) {
				validationError.msgid.set("BOVERR");
				validationError.msgcnt.set(1);
				compute(validationError.msglng, 0).set(length(validationError) - length(validationError.header));
				ldrhdr.totmsglng.set(length(validationError));
				validationError.boverrLanguage.set(wsspcomn.language);
				responseData.set(validationError);
				ldrhdr.errlvl.set("1");
				goTo(GotoLabel.aExit);
			}
		}
		else if (isEQ(ldrhdr.opmode, "2")){
			/*CONTINUE_STMT*/
		}
		sdartnrec.sdarBatchkey.set(wsspcomn.batchkey);
	}

protected void aP5051End()
	{
		sdartnrec.msglng.set(length(sdartnrec.data));
		sdartnrec.msgcnt.set(1);
		sdartnrec.msgid.set("SDARTNREC ");
		if (ldrhdr.noError.isTrue()) {
			responseData.set(sdartnrec.rec);
			ldrhdr.totmsglng.set(length(sdartnrec.rec));
		}
	}

protected void setUpErrorsP5051()
	{
		setUpErrorsP5051Start();
	}

protected void setUpErrorsP5051Start()
	{
		validationError.boverrProg.set("P5051");
		wsaaCnt.set(1);
		if (isNE(scrnparams.errorCode, SPACES)) {
			validationError.boverrBofoccur[wsaaCnt.toInt()].set(ZERO);
			validationError.boverrEror[wsaaCnt.toInt()].set(scrnparams.errorCode);
			validationError.boverrField[wsaaCnt.toInt()].set("S5051-SCRN-ERROR-CODE");
			getErrorDesc8100();
		}
		if (isNE(sv1.actionErr, SPACES)) {
			validationError.boverrBofoccur[wsaaCnt.toInt()].set(ZERO);
			validationError.boverrEror[wsaaCnt.toInt()].set(sv1.actionErr);
			validationError.boverrField[wsaaCnt.toInt()].set("S5051-ACTION");
			getErrorDesc8100();
		}
		if (isNE(sv1.chdrselErr, SPACES)) {
			validationError.boverrBofoccur[wsaaCnt.toInt()].set(ZERO);
			validationError.boverrEror[wsaaCnt.toInt()].set(sv1.chdrselErr);
			validationError.boverrField[wsaaCnt.toInt()].set("S5051-CHDRSEL");
			getErrorDesc8100();
		}
		if (isNE(sv1.effdateErr, SPACES)) {
			validationError.boverrBofoccur[wsaaCnt.toInt()].set(ZERO);
			validationError.boverrEror[wsaaCnt.toInt()].set(sv1.effdateErr);
			validationError.boverrField[wsaaCnt.toInt()].set("S5051-EFFDATE");
			getErrorDesc8100();
		}
	}

protected void getErrorDesc8100()
	{
		start8110();
	}

protected void start8110()
	{
		if (isGT(wsaaCnt, 25)) {
			return ;
		}
		wsaaDesc.set(SPACES);
		wsaaLang.set(wsspcomn.language);
		callProgram(Geterror.class, validationError.boverrEror[wsaaCnt.toInt()], wsaaLang, wsaaDesc, wsaaStat);
		if (isEQ(wsaaDesc, SPACES)) {
			wsaaLang.set(SPACES);
			callProgram(Geterror.class, validationError.boverrEror[wsaaCnt.toInt()], wsaaLang, wsaaDesc, wsaaStat);
		}
		if (isEQ(wsaaDesc, SPACES)) {
			wsaaLang.set(SPACES);
			callProgram(Geterror.class, validationError.boverrEror[wsaaCnt.toInt()], wsaaLang, wsaaDesc, wsaaStat);
		}
		if (isEQ(wsaaStat, varcom.oK)) {
			validationError.boverrDesc[wsaaCnt.toInt()].set(wsaaDesc);
		}
		wsaaCnt.add(1);
	}

protected void callDbcs6000()
	{
		/*CALL-DBCS-SUB*/
		dbcstrcpy2.dbcsStatuz.set(SPACES);
		dbcsTrnc2(dbcstrcpy2.rec);
		if (isNE(dbcstrcpy2.dbcsStatuz, varcom.oK)) {
			syserrrec.statuz.set(dbcstrcpy2.dbcsStatuz);
			syserrrec.params.set(dbcstrcpy2.rec);
			fatalError600();
		}
		/*EXIT*/
	}
}
