package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6350
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6350ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(509);
	public FixedLengthStringData dataFields = new FixedLengthStringData(221).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,32);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,180);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,181);
	public FixedLengthStringData payfreq = DD.payfreq.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,187);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,197);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData textfield = DD.textfield.copy().isAPartOf(dataFields,208);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 221);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData payfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData textfieldErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 293);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] payfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] textfieldOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(221);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(75).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData covPremStat = DD.crpstat.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData covRiskStat = DD.crrstat.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,6);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public FixedLengthStringData pstatdesc = DD.pstatdesc.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData rstatdesc = DD.rstatdesc.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,74);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 75);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crpstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData crrstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData pstatdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData rstatdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 111);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crpstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] crrstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] pstatdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] rstatdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 219);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S6350screensflWritten = new LongData(0);
	public LongData S6350screenctlWritten = new LongData(0);
	public LongData S6350screenWritten = new LongData(0);
	public LongData S6350protectWritten = new LongData(0);
	public GeneralTable s6350screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6350screensfl;
	}

	public S6350ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(textfieldOut,new String[] {null, null, null, "99-89",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {rider, coverage, life, select, planSuffix, covRiskStat, rstatdesc, covPremStat, pstatdesc};
		screenSflOutFields = new BaseData[][] {riderOut, coverageOut, lifeOut, selectOut, plnsfxOut, crrstatOut, rstatdescOut, crpstatOut, pstatdescOut};
		screenSflErrFields = new BaseData[] {riderErr, coverageErr, lifeErr, selectErr, plnsfxErr, crrstatErr, rstatdescErr, crpstatErr, pstatdescErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, lifenum, lifename, jlife, jlifename, currfrom, payfreq, mop, ptdate, btdate, textfield};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, currfromOut, payfreqOut, mopOut, ptdateOut, btdateOut, textfieldOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, currfromErr, payfreqErr, mopErr, ptdateErr, btdateErr, textfieldErr};
		screenDateFields = new BaseData[] {currfrom, ptdate, btdate};
		screenDateErrFields = new BaseData[] {currfromErr, ptdateErr, btdateErr};
		screenDateDispFields = new BaseData[] {currfromDisp, ptdateDisp, btdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6350screen.class;
		screenSflRecord = S6350screensfl.class;
		screenCtlRecord = S6350screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6350protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6350screenctl.lrec.pageSubfile);
	}
}
