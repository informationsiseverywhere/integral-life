/*
 * File: Pt528.java
 * Date: 30 August 2009 2:01:41
 * Author: Quipoz Limited
 * 
 * Class transformed from PT528.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
//ILIFE-1274
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.fsu.clients.dataaccess.TclhTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.clients.tablestructures.T3583rec;
import com.csc.fsu.clients.tablestructures.T3716rec;
import com.csc.fsu.clients.tablestructures.Tr393rec;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.St528ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.T1680rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.newbusiness.recordstructures.Antisoclkey;
import com.csc.life.newbusiness.procedures.Antisocl;
import com.csc.smart.procedures.Errmesg;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart400framework.programs.Infosdsp;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.ChdrpfDAOImpl;


/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*             PT528 - INSURED/BENEFICIARY CHANGE NAME
*             ---------------------------------------
* Overview
* ========
*
* This is a minor alteration function to change the name and
* saluation of an active Life Insured/Beneficiary attached on
* a contract header using details captured from this screen.
* Validation will be given on any new entries of surname,given
* name and saluation. An appropriate message will return for
* an error happened in the related highlight field.
* For valid new entries, both client header and client detail
* history record will be updated. And also a letter request
* will generate optionally to notify the client the successful
* insured/beneficiary name change.
*
* Processing
* ==========
*
* Initialise
* ----------
*
* Read CHDRMNA (RETRV)  in  order to obtain the contract header
* information.
*
* Read  the 'DESCIO' in  order to obtain the description of the
* risk and premium statuses.
*
* In order to retrieve the required  names of owner, payor  and
* the selected insured/beneficiary, the  corresponding client
* details record is read.
*
* And the  relevant copybooks for formatting the owner & payor
* names must be included in the program.
*
* Read  the  agent  details (AGNTLNB) and format the name using
* the copybooks.
*
* Validation
* ----------
*
* If KILL is requested, then skip the validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Validation on surname/given name of insured/beneficiary.
*
*      - if the entry is new,
*           if ALTERNATIVE LANGUAGE is used and is capable of
*           DBCS,
*           then exit from the validation and proceed to next.
*           Otherwise,
*           verify it against iregularities among characters
*           and identify them as errors.
*
*      - if CALC is pressed, then redisplay.
*
* Validation on saluation
*
*      - if the entry is new,
*           verify it against available client title, T3583
*           and then test if the client's gender applicable to
*           this title. If the result is an error, then
*           exit from the validation.
*
*      - if CALC is pressed, then redisplay.
*
*
*Updating
*--------
*
* If the 'KILL' function key was pressed or no change on client
* name and saluation at all, skip the updating.
*
*  CLIENT HEADER UPDATE
*
*      - new surname, given name and saluation from the screen
*
*  CLIENT DETAIL HISTORY UPDATE
*
*      - original surname, given name and saluation stored
*      - client detail history key format from client header
*      - add a new client detail history record with VF 1
*
* (OPTIONAL) LETTER REQUEST CONTROL UPDATE if any,
*
*      - Write a LETC record (if updating the CLTS):
*
*          - involve a subroutine 'LETRQST' with function 'ADD'
*
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class Pt528 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PT528");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaLsurname = new FixedLengthStringData(60).init(SPACES);
	private FixedLengthStringData wsaaLgivname = new FixedLengthStringData(60).init(SPACES);
	private FixedLengthStringData wsaaSalutl = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaCtrycode = new FixedLengthStringData(3).isAPartOf(wsaaItem, 1);

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 0);
	private FixedLengthStringData wsaaOkey2 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 1);
	private FixedLengthStringData wsaaOkey3 = new FixedLengthStringData(8).isAPartOf(wsaaOkeys, 2);
	private FixedLengthStringData wsaaOkey4 = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 10);

		/* WSAA-VALID-CHARS */
	private FixedLengthStringData wsaaArray = new FixedLengthStringData(120);
	private FixedLengthStringData wsaaUpper = new FixedLengthStringData(40).isAPartOf(wsaaArray, 0);
	private FixedLengthStringData wsaaLower = new FixedLengthStringData(40).isAPartOf(wsaaArray, 40);
	private FixedLengthStringData wsaaSpecial = new FixedLengthStringData(40).isAPartOf(wsaaArray, 80);

	private FixedLengthStringData wsaaArrayRedef = new FixedLengthStringData(120).isAPartOf(wsaaArray, 0, REDEFINE);
	private FixedLengthStringData[] wsaaValidChar = FLSArrayPartOfStructure(120, 1, wsaaArrayRedef, 0);
	private static final int wsaaNoOfChar = 120;
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIndexa = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIndexb = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIndexc = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaCharacters = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaChar = FLSArrayPartOfStructure(60, 1, wsaaCharacters, 0);

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
	private FixedLengthStringData valnStatuz = new FixedLengthStringData(5);
	private FixedLengthStringData valnFunction = new FixedLengthStringData(5);
	private static final String valnName = "NAME";
	private static final String valnAddress = "ADDR";
	private static final String d036 = "D036";
	private static final String d037 = "D037";
	private static final String e186 = "E186";
	private static final String g983 = "G983";
	private static final String g984 = "G984";
	private static final String g986 = "G986";
		/* TABLES */
	private static final String t1680 = "T1680";
	private static final String t5688 = "T5688";
	private static final String t3588 = "T3588";
	private static final String t3583 = "T3583";
	private static final String t3623 = "T3623";
	private static final String t3639 = "T3639";
	private static final String t3716 = "T3716";
	private static final String tr384 = "TR384";
	private static final String tr393 = "TR393";
	private static final String cltsrec = "CLTSREC";
	private static final String itemrec = "ITEMREC";
	private static final String tclhrec = "TCLHREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private TclhTableDAM tclhIO = new TclhTableDAM();
	private Ptrnpf ptrnpf = null;
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private T1680rec t1680rec = new T1680rec();
	private T3583rec t3583rec = new T3583rec();
	private T3716rec t3716rec = new T3716rec();
	private Tr393rec tr393rec = new Tr393rec();
	private Tr384rec tr384rec = new Tr384rec();
	//ILIFE-1274
	private Sftlockrec sftlockrec = new Sftlockrec();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End
	private St528ScreenVars sv = ScreenProgram.getScreenVars( St528ScreenVars.class);
	protected boolean ispermission = false;// ILIFE-8682
	
	private Antisoclkey antisoclkey = new Antisoclkey();
	 
	 private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(70);
		private FixedLengthStringData wsaaMessage = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 0).init("Anti-Social Person ");
		private FixedLengthStringData wsaaMsgnum = new FixedLengthStringData(8).isAPartOf(wsaaMsgarea, 25);
		
		private FixedLengthStringData wsaaAgentError = new FixedLengthStringData(59);
		private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaAgentError, 0);
		private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaAgentError, 8, FILLER).init(SPACES);
		private FixedLengthStringData wsaaErrmesg = new FixedLengthStringData(50).isAPartOf(wsaaAgentError, 9);
		
		private boolean firstFlag = true;
		
		protected Errmesgrec errmesgrec = new Errmesgrec();
		private Tr386rec tr386rec = new Tr386rec();
		private Clntpf clntpf= new Clntpf();
		private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
		private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(8);
		private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
		private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
		
		private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAOImpl.class);
		
		private static final String tr386 = "TR386";
		private boolean NBPRP117permission = false;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readT37162021, 
		validateGivname2025, 
		validateSalutation2027, 
		checkForErrors2080, 
		loop1410, 
		testSpaces420, 
		loop2430, 
		exit490, 
		loop510, 
		next530, 
		loop1610, 
		loop2620, 
		next630, 
		loop710, 
		exit790, 
		loop810, 
		ar201Exit, 
		exit3090
	}

	public Pt528() {
		super();
		screenVars = sv;
		new ScreenModel("St528", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		readT16801020();
	}

protected void initialise1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		/* Retrieve contract header and client role file*/
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		clrrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.cownnum.set(chdrmnaIO.getCownnum());
		sv.payrnum.set(chdrmnaIO.getPayrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		sv.billfreq.set(chdrmnaIO.getBillfreq());
		sv.mop.set(chdrmnaIO.getBillchnl());
		sv.agntnum.set(chdrmnaIO.getAgntnum());
		sv.clntnum.set(clrrIO.getClntnum());
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
		/* Get contract type description*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		/*  get premium status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/*  Get Risk status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/* Get client role description*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3639);
		descIO.setDescitem(clrrIO.getClrrrole());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.clrole.fill("?");
		}
		else {
			sv.clrole.set(descIO.getShortdesc());
		}
		/* Get owner name, payor name and agent name.*/
		if (isNE(sv.cownnum,SPACES)) {
			formatClientName1700();
		}
		if (isNE(sv.payrnum,SPACES)) {
			formatPayorName1800();
		}
		else {
			sv.payrnumOut[varcom.nd.toInt()].set("Y");
		}
		if (isNE(sv.agntnum,SPACES)) {
			formatAgentName1900();
		}
		/* Read TR393 to get client salutation indicator.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tr393);
		itemIO.setItemitem(wsspcomn.fsuco);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		tr393rec.tr393Rec.set(itemIO.getGenarea());
		/* Read CLTS file to load client surname, givename and*/
		/* salutation.*/
		cltsIO.setClntpfx(clrrIO.getClntpfx());
		cltsIO.setClntcoy(clrrIO.getClntcoy());
		cltsIO.setClntnum(clrrIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lsurname.set(cltsIO.getLsurname());
		wsaaLsurname.set(cltsIO.getLsurname());
		sv.lgivname.set(cltsIO.getLgivname());
		wsaaLgivname.set(cltsIO.getLgivname());
		sv.salutl.set(cltsIO.getSalutl());
		wsaaSalutl.set(cltsIO.getSalutl());
		/*   Get Salutation Description                                    */
		if (isNE(sv.salutl,SPACES)) {
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.fsuco);
			descIO.setDesctabl(t3583);
			descIO.setDescitem(sv.salutl);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)
			&& isNE(descIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			sv.tsalutsd.set(descIO.getShortdesc());
		}
		ispermission=FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "JPNCN001",appVars, "IT");
		if(ispermission)   // ILIFE-8682
		{
			sv.zkanasurname.set(cltsIO.getzkanasnm());
			sv.zkanagivname.set(cltsIO.getzkanagnm());	
		}
		NBPRP117permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP117", appVars, "IT"); 
	}

protected void readT16801020()
	{
		/*    Read T1680 - to identify if Alternative Language is used.    */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ZERO);
		itemIO.setItemtabl(t1680);
		itemIO.setItemitem(wsspcomn.language);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1680rec.t1680Rec.set(itemIO.getGenarea());
	}

protected void formatClientName1700()
	{
		ownername1710();
	}

protected void ownername1710()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(sv.cownnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.ownername.set(namadrsrec.name);
	}

protected void formatPayorName1800()
	{
		payorname1810();
	}

protected void payorname1810()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(sv.payrnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.payorname.set(namadrsrec.name);
	}

protected void formatAgentName1900()
	{
		readAgent1910();
		agentname1920();
	}

protected void readAgent1910()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntnum);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
	}

protected void agentname1920()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(aglflnbIO.getClntnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.oragntnam.set(namadrsrec.name);
	}

protected void preScreenEdit()
	{
	
	if((!ispermission)){
		   sv.zkanasurnameOut[varcom.nd.toInt()].set("Y");  // ILIFE-8682
		   sv.zkanagivnameOut[varcom.nd.toInt()].set("Y");
		   
	    }else
	    {
	    	 sv.zkanasurnameOut[varcom.nd.toInt()].set("N");
			 sv.zkanagivnameOut[varcom.nd.toInt()].set("N");	   
	    }
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		wsspcomn.edterror.set(varcom.oK);
		return ;
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case readT37162021: 
					readT37162021();
					validateSurname2023();
				case validateGivname2025: 
					validateGivname2025();
				case validateSalutation2027: 
					validateSalutation2027();
				case checkForErrors2080: 
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		
	//ILJ-ANSO
		
	if(NBPRP117permission)
	{	
		clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.clntnum.toString());
		clntpf = clntpfDAO.getCltsRecordByKey(clntpf); 
		
		initialize(antisoclkey.antisocialKey);
		antisoclkey.kjSName.set(sv.lsurname);
		antisoclkey.kjGName.set(sv.lgivname);
		antisoclkey.clntype.set(clntpf.getClttype());

		
		callProgram(Antisocl.class, antisoclkey.antisocialKey);	
		
		if (isNE(antisoclkey.statuz, varcom.oK)) {
			
			scrnparams.errorCode.set(antisoclkey.statuz);
		
		}
	}
		
		//ILJ
		
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		/* -- Modify name validation to check the character set  --*/
		/* -- on T3716 for the client language.                  --*/
		/* -- If language and country have not been set up,      --*/
		/* -- default to item '****'.                            --*/
		valnFunction.set(valnName);
		if (isEQ(cltsIO.getLanguage(),SPACES)) {
			wsaaLanguage.set("*");
		}
		else {
			wsaaLanguage.set(cltsIO.getLanguage());
		}
		if (isEQ(cltsIO.getCtrycode(),SPACES)) {
			wsaaCtrycode.set("***");
		}
		else {
			wsaaCtrycode.set(cltsIO.getCtrycode());
		}
	}

protected void readT37162021()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemtabl(t3716);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*    Default item has not been found so error message produced*/
		if (isEQ(wsaaItem,"****")
		&& isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.lsurnameErr.set(d037);
			goTo(GotoLabel.checkForErrors2080);
		}
		/*    The read failed with real language or country so replace*/
		/*    with *'s and try for default records.*/
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		&& isNE(wsaaCtrycode,"***")) {
			wsaaCtrycode.set("***");
			goTo(GotoLabel.readT37162021);
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		&& isNE(wsaaLanguage,"*")) {
			wsaaLanguage.set("*");
			goTo(GotoLabel.readT37162021);
		}
		t3716rec.t3716Rec.set(itemIO.getGenarea());
		wsaaUpper.set(t3716rec.charset01);
		wsaaLower.set(t3716rec.charset02);
		wsaaSpecial.set(t3716rec.charset03);
	}

protected void validateSurname2023()
	{
		/* No need to validate if values are the same.*/
		if (isEQ(sv.lsurname,wsaaLsurname)) {
			goTo(GotoLabel.validateGivname2025);
		}
		if (isEQ(sv.lsurname,SPACES)) {
			sv.lsurnameErr.set(e186);
		}
		else {
			wsaaCharacters.set(sv.lsurname);
			valname2600();
			if (isNE(valnStatuz,varcom.oK)) {
				sv.lsurnameErr.set(valnStatuz);
			}
		}
	}

protected void validateGivname2025()
	{
		if (isEQ(sv.lgivname,wsaaLgivname)) {
			goTo(GotoLabel.validateSalutation2027);
		}
		if (isEQ(sv.lgivname,SPACES)) {
			sv.lgivnameErr.set(e186);
		}
		else {
			wsaaCharacters.set(sv.lgivname);
			valname2600();
			if (isNE(valnStatuz,varcom.oK)) {
				sv.lgivnameErr.set(valnStatuz);
			}
		}
	}

protected void validateSalutation2027()
	{
		if (isEQ(sv.salutl,wsaaSalutl)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.salutl,SPACES)) {
			sv.salutlErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t3583);
		itemIO.setItemitem(sv.salutl);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.salutlErr.set(g984);
			goTo(GotoLabel.checkForErrors2080);
		}
		t3583rec.t3583Rec.set(itemIO.getGenarea());
		if (isNE(t3583rec.cltsex,SPACES)
		&& isNE(cltsIO.getCltsex(),SPACES)) {
			if (isNE(t3583rec.cltsex,cltsIO.getCltsex())) {
				sv.salutlErr.set(g983);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void valname2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validate2600();
				case loop1410: 
					loop1410();
				case testSpaces420: 
					testSpaces420();
				case loop2430: 
					loop2430();
				case exit490: 
					exit490();
				case loop510: 
					loop510();
				case next530: 
					next530();
				case loop1610: 
					loop1610();
				case loop2620: 
					loop2620();
				case next630: 
					next630();
				case loop710: 
					loop710();
				case exit790: 
					exit790();
				case loop810: 
					loop810();
				case ar201Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validate2600()
	{
		valnStatuz.set(varcom.oK);
		/*    skip validation of name & address                            */
		if (isNE(t1680rec.funckeypr,"1")
		&& isEQ(t1680rec.languageDbcs,"Y")) {
			goTo(GotoLabel.ar201Exit);
		}
		/*    Check that first character is not a space*/
		if (isEQ(wsaaChar[1],SPACES)) {
			valnStatuz.set(g986);
			goTo(GotoLabel.ar201Exit);
		}
		/*    Check that there are not two spaces together*/
		wsaaIndex.set(61);
	}

protected void loop1410()
	{
		wsaaIndex.subtract(1);
		if (isNE(wsaaChar[wsaaIndex.toInt()],SPACES)) {
			goTo(GotoLabel.testSpaces420);
		}
		if (isGT(wsaaIndex,2)) {
			goTo(GotoLabel.loop1410);
		}
		goTo(GotoLabel.exit490);
	}

protected void testSpaces420()
	{
		wsaaIndexa.set(wsaaIndex);
		wsaaIndexa.subtract(1);
	}

protected void loop2430()
	{
		wsaaIndex.subtract(1);
		wsaaIndexa.subtract(1);
		if (isEQ(wsaaChar[wsaaIndex.toInt()],SPACES)
		&& isEQ(wsaaChar[wsaaIndexa.toInt()],SPACES)) {
			valnStatuz.set(g986);
			goTo(GotoLabel.ar201Exit);
		}
		if (isGT(wsaaIndex,2)) {
			goTo(GotoLabel.loop2430);
		}
	}

protected void exit490()
	{
		/*    Check that there are not more than three identical           */
		/*    characters together.                                         */
		wsaaIndex.set(1);
		wsaaIndexa.set(0);
		wsaaIndexb.set(2);
		wsaaIndexc.set(3);
	}

protected void loop510()
	{
		wsaaIndex.add(1);
		wsaaIndexa.add(1);
		wsaaIndexb.add(1);
		wsaaIndexc.add(1);
		if (isEQ(wsaaChar[wsaaIndex.toInt()],SPACES)) {
			goTo(GotoLabel.next530);
		}
		if (isEQ(wsaaChar[wsaaIndex.toInt()],NUMERIC)) {
			goTo(GotoLabel.next530);
		}
		if (isEQ(wsaaChar[wsaaIndex.toInt()],wsaaChar[wsaaIndexa.toInt()])
		&& isEQ(wsaaChar[wsaaIndex.toInt()],wsaaChar[wsaaIndexb.toInt()])
		&& isEQ(wsaaChar[wsaaIndex.toInt()],wsaaChar[wsaaIndexc.toInt()])) {
			valnStatuz.set(g986);
			goTo(GotoLabel.ar201Exit);
		}
	}

protected void next530()
	{
		if (isLT(wsaaIndex,59)) {
			goTo(GotoLabel.loop510);
		}
		/*    Ignore the character check if WSAA-VALID-CHARS = SPACES      */
		if (isEQ(wsaaArray,SPACES)) {
			wsaaIndex.set(60);
			goTo(GotoLabel.next630);
		}
		/*    Check that all characters are valid                          */
		/*    Note - the first character must be an upper case letter      */
		/*    Not true in all languages.                                   */
		wsaaIndex.set(0);
	}

protected void loop1610()
	{
		wsaaIndex.add(1);
		wsaaIndexa.set(0);
		/*    Address lines may have numeric chracters, commas and full    */
		/*                                                           stops */
		/*    and now '#' !                                                */
		if (isEQ(valnFunction,valnAddress)
		&& (isEQ(wsaaChar[wsaaIndex.toInt()],NUMERIC)
		|| (isEQ(wsaaChar[wsaaIndex.toInt()],",")
		&& isNE(wsaaIndex,1))
		|| (isEQ(wsaaChar[wsaaIndex.toInt()],".")
		&& isNE(wsaaIndex,1))
		|| (isEQ(wsaaChar[wsaaIndex.toInt()],"/")
		&& isNE(wsaaIndex,1))
		|| (isEQ(wsaaChar[wsaaIndex.toInt()],"#")))) {
			goTo(GotoLabel.next630);
		}
		if (isEQ(valnFunction,valnName)
		&& (isEQ(wsaaChar[wsaaIndex.toInt()],"/")
		&& isNE(wsaaIndex,1))) {
			goTo(GotoLabel.next630);
		}
	}

protected void loop2620()
	{
		wsaaIndexa.add(1);
		/*    Character check here                                         */
		if (isEQ(wsaaChar[wsaaIndex.toInt()],wsaaValidChar[wsaaIndexa.toInt()])) {
			goTo(GotoLabel.next630);
		}
		if (isLT(wsaaIndexa,wsaaNoOfChar)) {
			goTo(GotoLabel.loop2620);
		}
		/*    Not found - invalid character                                */
		valnStatuz.set(d036);
		goTo(GotoLabel.ar201Exit);
	}

protected void next630()
	{
		if (isLT(wsaaIndex,60)) {
			goTo(GotoLabel.loop1610);
		}
		/*    Check that there is at least one string of 2 or more chars*/
		wsaaIndex.set(1);
		wsaaIndexa.set(0);
	}

protected void loop710()
	{
		wsaaIndex.add(1);
		wsaaIndexa.add(1);
		/*    Check for consecutive characters*/
		if (isNE(wsaaChar[wsaaIndex.toInt()],SPACES)
		&& isNE(wsaaChar[wsaaIndexa.toInt()],SPACES)) {
			goTo(GotoLabel.exit790);
		}
		/*NEXT*/
		/* IF WSAA-INDEX               < 30                             */
		if (isLT(wsaaIndex,60)) {
			goTo(GotoLabel.loop710);
		}
		/*    No string of two characters has been found*/
		valnStatuz.set(g986);
		goTo(GotoLabel.ar201Exit);
	}

protected void exit790()
	{
		/*    Check for incorrect character combinations*/
		wsaaIndex.set(1);
		wsaaIndexa.set(0);
	}

protected void loop810()
	{
		wsaaIndex.add(1);
		wsaaIndexa.add(1);
		/*    Check that hyphen is not next to blank*/
		if (isEQ(wsaaChar[wsaaIndex.toInt()],SPACES)
		&& isEQ(wsaaChar[wsaaIndexa.toInt()],"-")) {
			valnStatuz.set(g986);
			return ;
		}
		if (isEQ(wsaaChar[wsaaIndexa.toInt()],SPACES)
		&& isEQ(wsaaChar[wsaaIndex.toInt()],"-")) {
			valnStatuz.set(g986);
			return ;
		}
		/*    Check that apostrophe is not next to blank*/
		if (isEQ(wsaaChar[wsaaIndex.toInt()],SPACES)
		&& isEQ(wsaaChar[wsaaIndexa.toInt()],"'")) {
			valnStatuz.set(g986);
			return ;
		}
		if (isEQ(wsaaChar[wsaaIndexa.toInt()],SPACES)
		&& isEQ(wsaaChar[wsaaIndex.toInt()],"'")) {
			valnStatuz.set(g986);
			return ;
		}
		/*    Check that a comma immedatly follows a character*/
		/*      and is always followed by a space  (address lines only)*/
		if (isNE(wsaaChar[wsaaIndex.toInt()],SPACES)
		&& isEQ(wsaaChar[wsaaIndexa.toInt()],",")) {
			valnStatuz.set(g986);
			return ;
		}
		if (isEQ(wsaaChar[wsaaIndexa.toInt()],SPACES)
		&& isEQ(wsaaChar[wsaaIndex.toInt()],",")) {
			valnStatuz.set(g986);
			return ;
		}
		if (isLT(wsaaIndex,60)) {
			goTo(GotoLabel.loop810);
		}
	}
protected void rewrt3030()
{
 
	List<Chdrpf> chdrList =   new ArrayList<Chdrpf>();
	Chdrpf chdrpf = new Chdrpf();
	chdrpf.setChdrcoy(chdrmnaIO.getChdrcoy().charat(0));
	chdrpf.setChdrnum(chdrmnaIO.getChdrnum().toString());	
	chdrpf.setTranno(chdrmnaIO.getTranno().toInt());
	chdrList.add(chdrpf);
	boolean b =  chdrpfDAO.updateChdrTranno(chdrList);
}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/

protected void update3000()
	{
		try {
			updateDatabase3010();
			updateClts3030();
			updateTclh3050();
			writeLetter3070();
			writePtrn3080(); //ILIFE-7923
			rewrt3030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateDatabase3010()
	{
		if (isNE(sv.lsurname,wsaaLsurname)
		|| isNE(sv.lgivname,wsaaLgivname)
		|| isNE(sv.salutl,wsaaSalutl) ||(ispermission)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updateClts3030()
	{
		/* Read hold CLTS file for updating.*/
		cltsIO.setClntpfx(clrrIO.getClntpfx());
		cltsIO.setClntcoy(clrrIO.getClntcoy());
		cltsIO.setClntnum(clrrIO.getClntnum());
		cltsIO.setFunction(varcom.readh);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*                                                      <V76F10>*/
		/* MOVE '2'                    TO CLTS-VALIDFLAG.       <V76F10>*/
		/* MOVE REWRT                  TO CLTS-FUNCTION.        <V76F10>*/
		/* MOVE CLTSREC                TO CLTS-FORMAT.          <V76F10>*/
		/* CALL  'CLTSIO'           USING CLTS-PARAMS.          <V76F10>*/
		/*                                                      <V76F10>*/
		/* IF CLTS-STATUZ              NOT = O-K                <V76F10>*/
		/*    MOVE CLTS-PARAMS         TO SYSR-PARAMS           <V76F10>*/
		/*    PERFORM 600-FATAL-ERROR.                          <V76F10>*/
		/* Update client surname, given name and salutation.*/
		cltsIO.setLsurname(sv.lsurname);
		cltsIO.setLgivname(sv.lgivname);
		cltsIO.setSalutl(sv.salutl);
		/*   MOVE ST528-TSALUTSD         TO CLTS-TSALUTSD.*/
		cltsIO.setSurname(sv.lsurname);
		cltsIO.setGivname(sv.lgivname);
		/* MOVE REWRT                  TO CLTS-FUNCTION.                */
		/* MOVE '1'                    TO CLTS-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO CLTS-FUNCTION.        <V76F10>*/
		if (ispermission) {  // ILIFE-8682
			cltsIO.setzkanasnm(sv.zkanasurname.trim());
			cltsIO.setzkanagnm(sv.zkanagivname.trim());
}
		cltsIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void updateTclh3050()
	{
		/* Update TCLH file*/
		tclhIO.setClntpfx(cltsIO.getClntpfx());
		tclhIO.setClntcoy(cltsIO.getClntcoy());
		tclhIO.setClntnum(cltsIO.getClntnum());
		tclhIO.setFunction(varcom.readh);
		tclhIO.setFormat(tclhrec);
		SmartFileCode.execute(appVars, tclhIO);
		if (isNE(tclhIO.getStatuz(),varcom.oK)) {
			writeTclh3100();
		}
		else {
			rewritTlch3200();
			writeTclh3100();
		}
	}

protected void writeLetter3070()
	{
		/* Write Letter Record*/
		/* Read Table T6634 for get letter-type.*/
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634             TO ITEM-ITEMITEM.   <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			/*     PERFORM  3300-READ-T6634-AGAIN                   <PCPPRT>*/
			readTr384Again3300();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		/* IF   T6634-LETTER-TYPE          = SPACE                      */
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
		/*   Get Endorsement no.*/
		/*    MOVE 'NEXT '                TO ALNO-FUNCTION.        <PCPPRT>*/
		/*    MOVE 'EN'                   TO ALNO-PREFIX.          <PCPPRT>*/
		/*    MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.          <PCPPRT>*/
		/*    MOVE WSSP-COMPANY           TO ALNO-COMPANY.         <PCPPRT>*/
		/*                                                         <PCPPRT>*/
		/*    CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                 <PCPPRT>*/
		/*                                                         <PCPPRT>*/
		/*    IF ALNO-STATUZ              NOT = O-K                <PCPPRT>*/
		/*       MOVE ALNO-STATUZ         TO SYSR-STATUZ           <PCPPRT>*/
		/*       MOVE 'ALOCNO'            TO SYSR-PARAMS           <PCPPRT>*/
		/*       PERFORM 600-FATAL-ERROR.                          <PCPPRT>*/
		/*   Get set-up parameter for call 'HLETRQS'*/
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		/* MOVE 'EN'                   TO LETRQST-RDOCPFX.      <PCPPRT>*/
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.      <PCPPRT>*/
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		wsaaOkeys.set(SPACES);
		wsaaOkey1.set(wsspcomn.language);
		wsaaOkey2.set(cltsIO.getClntcoy());
		wsaaOkey3.set(cltsIO.getClntnum());
		wsaaOkey4.set(clrrIO.getClrrrole());
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
//ILIFE-7923 starts
protected void writePtrn3080() {
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrcoy(chdrmnaIO.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrmnaIO.getChdrnum().toString());
		ptrnpf.setTranno(chdrmnaIO.getTranno().toInt());
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		wsaaBatckey.set(wsspcomn.batchkey);
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		List<Ptrnpf> list = new ArrayList<Ptrnpf>();
		list.add(ptrnpf);
		if (!ptrnpfDAO.insertPtrnPF(list)) {
			syserrrec.params.set(chdrmnaIO.getChdrcoy().toString()
					.concat(chdrmnaIO.getChdrnum().toString()));
			fatalError600();
		}
	}
//ILIFE-7923 ends
protected void writeTclh3100()
	{
		start3110();
	}

protected void start3110()
	{
		tclhIO.setRecKeyData(SPACES);
		tclhIO.setClntpfx(cltsIO.getClntpfx());
		tclhIO.setClntcoy(cltsIO.getClntcoy());
		tclhIO.setClntnum(cltsIO.getClntnum());
		tclhIO.setValidflag(cltsIO.getValidflag());
		tclhIO.setLsurname(wsaaLsurname);
		tclhIO.setLgivname(wsaaLgivname);
		tclhIO.setSalutl(wsaaSalutl);
		tclhIO.setInitials(cltsIO.getInitials());
		tclhIO.setCltaddr01(cltsIO.getCltaddr01());
		tclhIO.setCltaddr02(cltsIO.getCltaddr02());
		tclhIO.setCltaddr03(cltsIO.getCltaddr03());
		tclhIO.setCltaddr04(cltsIO.getCltaddr04());
		tclhIO.setCltaddr05(cltsIO.getCltaddr05());
		tclhIO.setCltpcode(cltsIO.getCltpcode());
		tclhIO.setCtrycode(cltsIO.getCtrycode());
		tclhIO.setCltphone01(cltsIO.getCltphone01());
		tclhIO.setCltphone02(cltsIO.getCltphone02());
		tclhIO.setMarryd(cltsIO.getMarryd());
		tclhIO.setFormat(tclhrec);
		tclhIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, tclhIO);
		if (isNE(tclhIO.getStatuz(),varcom.oK)
		&& isNE(tclhIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(tclhIO.getParams());
			fatalError600();
		}
	}

protected void rewritTlch3200()
	{
		/*STRART*/
		tclhIO.setValidflag("2");
		tclhIO.setFormat(tclhrec);
		tclhIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, tclhIO);
		if (isNE(tclhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(tclhIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*3300-READ-T6634-AGAIN SECTION.                           <PCPPRT>
	* </pre>
	*/
protected void readTr384Again3300()
	{
		start3310();
	}

protected void start3310()
	{
		wsaaItemCnttype.set("***");
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634             TO ITEM-ITEMITEM.   <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
	//ILIFE-1274
	//releaseSflock4300();//ILIFE-1986
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
//ILIFE-1274
protected void releaseSflock4300()
{
	start4310();
}

protected void start4310()
{
	sftlockrec.sftlockRec.set(SPACES);
	sftlockrec.company.set(wsspcomn.company);
	sftlockrec.entity.set(chdrmnaIO.getChdrnum());
	sftlockrec.enttyp.set("CH");
	sftlockrec.user.set(varcom.vrcmUser);
	sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
	sftlockrec.statuz.set(SPACES);
	sftlockrec.function.set("UNLK");
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}
}
