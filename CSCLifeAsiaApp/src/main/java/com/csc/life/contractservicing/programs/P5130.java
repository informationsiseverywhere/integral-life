/*
 * File: P5130.java
 * Date: 30 August 2009 0:11:34
 * Author: Quipoz Limited
 * 
 * Class transformed from P5130.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.S5130ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                     WORK WITH PROPOSAL
*                     ==================
*
*
* Initialise
* ----------
*
*   Skip  this  section  if  returning from an optional selection
*   (current stack position action flag = '*').
*
*   Clear the subfile ready for loading.
*
*   Read  CHDRMJA  (RETRV) in order to obtain the contract header
*   information.
*
*  Read  the  contract definition description from T5688 for the
*   contract type held on CHDRMJA.
*
*   Read  the  contract  definition  details  from  T5688 for the
*   contract  type  held  on  CHDRMJA. Access the version of this
*   item for the original commencement date of the risk.
*
*   Look up  the  client details of the contract owner (CLTS) and
*   format the name as a "confirmation name".
*
*   This program  displays  the  current  component  parts  of an
*   existing proposal.  One  record is written to the subfile for
*   each component part found.
*
*   Add a  "contract  header" record with all fields blank except
*   the   Element-key  (client  code)  and  Element-description
*   (contract description from above).
*
*   For a  contract,  there  will  be  one  or  many  lives (with
*   optionally a joint life). For each life, there will be one or
*   many coverages. For each coverage, there will be none, one or
*   many riders.
*
*   Start by reading on record from the life file (LIFELND with a
*   BEGN) for  the  current  proposal.  Maintain  a  note  of the
*   next  available  life  number used when reading life records.
*   This  will be the next number sequentially or a "hole" in the
*   life number sequence. It will be needed if there is a request
*   to  add  another  life. Also count the actual number of lives
*   (not  joint lives) added to the subfile.  This will be needed
*   during validation.  Add records as follows:
*
*        - Life details  -  write  the life record to the subfile
*             (life-no  from  record,  coverage and rider numbers
*             blank) with  the  client-no  as the element key and
*             the "confirmation name" (from CLTS) for the life as
*             the    description.   Read   the   first   coverage
*             transaction  (COVRMJA with BEGN) for the life added
*             to  the  subfile.  Sequentially  read the next life
*             record (it may be useful).
*
*        - Joint life  details  - if the LIFEMJA record just read
*             is for the same "life-no", the joint life no should
*             not be  zero (if it is, there is a database error.)
*             This is  a  joint  life associated with the current
*             life. In  this  case, write another record with the
*             action  field protected, the life-no " +", coverage
*             and rider  numbers  blank  and  the element key and
*             description  as  above. (This record is written for
*             information  only.) Sequentially read the next life
*             record (it may be useful).
*
*        - Coverage details  -  write  the coverage record to the
*             subfile  with  the coverage-no from the record, the
*             life  and  rider numbers blank. (Write the life-no,
*             coverage-on and rider-no to "hidden" fields for use
*             if  the  line  is selected.) Put the coverage/rider
*             code in the element key and look up its description
*             (T5687)  for the element description. Remember this
*             coverage  code  for  adding  to  the  rider records
*             attached  (in  a  hidden field).  Sequentially read
*             the next coverage/rider transaction.
*
*        - Rider details  - if the coverage number is the same as
*             the one  put  on the coverage subfile record above,
*             the rider-no will  not be zero (if it is, this is a
*             database error).  This  is  a  rider  on  the above
*             coverage.  Write  the  rider  record to the subfile
*             with the  rider-no  from  the  record, the life and
*             coverage   numbers   blank.   (Write  the  life-no,
*             coverage-on and rider-no to "hidden" fields for use
*             if  the  line  is selected.) Put the coverage/rider
*             code in the element key and look up its description
*             (T5687)  for the element description.  Sequentially
*             read  the  next coverage/rider transaction. If this
*             is   another   rider   record   for   the   current
*             transaction, repeat this rider processing. If it is
*             another  coverage  for  the  same  life, repeat the
*             processing  from  the coverage section above. If it
*             is a  coverage  for  another  life,  repeat all the
*             above processing  for  the  next  life.  If it is a
*             coverage for  another  proposal  (or the end of the
*             file) all components have been loaded.
*
*   In all cases, load  all pages required in the subfile and set
*   the subfile more indicator to no.
*
* Validation
* ----------
*
*   Skip  this  section  if  returning from an optional selection
*   (current stack position action flag = '*').
*
*   Read   all   modified   subfile   records.  In  enquiry  mode
*   (WSSP-FLAG  =  'I'), only an action of '1' is allowed against
*   any line.  Otherwise, validate the selection according to the
*   record  type  read. (This can be worked out from which fields
*   are not blank - life-no = life record, coverage-no = coverage
*   record, rider-no = rider record, none = contract header.):
*
*        Contract  header - valid values are '1' and '2'. If '2',
*             check  that  the maximum number of lives allowed on
*             this  type  of contract (from T5688 read above) has
*             not  been  reached already (no of lives accumulated
*             when the subfile was loaded).
*             If '1' then clear the available issue field on the
*             client header record and keep the header record.
*
*        Life - valid  values  are '1', '2' and '9'. If '9', skip
*             all  the  validation  on  any component coverage or
*             rider for this life (blanking out actions?).
*             If '1' or '9' then clear the available issue field
*             on the client header record and keep the header
*             record.
*
*        Coverage  -  valid  values are '1', '2', '3' and '9'. If
*             '9',   read  the  contract  structure  table  entry
*             (T5673)   for   the  contract  type,  effective  at
*             original commencement date.  Check that this is not
*             a mandatory coverage.  (NB this table item could be
*             continued  onto  more than one record). If OK, skip
*             all  the  validation on any rider for this coverage
*             (blanking out actions?). If '3',  read  the general
*             coverage/rider  details  table   (T5687)   for  the
*             coverage code and check that R/I is allowed.
*
*        Rider - valid values are '1', '3' and '9'.  If '9', read
*             the  contract structure table entry (T5673) for the
*             contract type, effective  at  original commencement
*             date.  Find the rider details for the coverage code
*             "hidden" on this record. Check that this  is  not a
*             mandatory  rider.   (NB this table  item  could  be
*             continued onto more than one record  and riders for
*             a coverage can continue onto more  than  one line).
*             If  '3', read the  general  coverage/rider  details
*             table (T5687) for the rider code and check that R/I
*             is allowed.
*
* Updating
* --------
*
*   This program performs no updating.
*
* Next program
* ------------
*
*   If not returning  from  a  component (stack action is blank),
*   read the  first  record  from  the  subfile.  If  this is not
*   selected (action  is  blank),  read  the  next one and so on,
*   until a selected  record  is found, or the end of the subfile
*   is reached.
*
*   Once the end  of  the  subfile  has been reached, if no items
*   were  selected at all (stack action still blank) and CALC has
*   not been requested, retrieve  the next program switching from
*   secondary switching with an action  of  'E'.  Add  one to the
*   program stack and exit.  If some items were selected and CALC
*   has been deAlt with,move a blank to the stack action and exit
*   (this will cause the  program  to  re-enter initialisation to
*   re-load the subfile).
*
*   Processing  of  what  to  do next is dependant on the type of
*   record read as follows:
*
*        Contract  header  - use generalised secondary switching.
*             Set   up  the  function  according  to  the  action
*             entered.   1 = A, 2 = B.  Move  '*'  to  the  stack
*             action.
*
*        Life -  use  generalised  secondary  switching.  Set  up
*             the function  according  to the action entered. 1 =
*             F, 2 = G, 9 = H.  Move '*' to the stack action.
*
*        Coverage/rider -  for  action  '1', look up the programs
*             required  to process the generic component (T5671 -
*             accessed  by  transaction  number concatenated with
*             coverage/rider  code from the subfile record). Move
*             these four  programs into the program stack and set
*             the current stack action to '*'.
*
*        Coverage/rider  -  for  actions  '2',  '3'  and  '9',use
*             generalised  secondary  switching.   Set   up   the
*             function according to the action entered.  2 = I, 3
*             = J, 9 = K.  Move '*' to the stack action.
*
*   Release any LIFEMJA or COVRMJA records stored (RLSE).
*
*   For action '2' on the contract header, set up the key details
*   of  the  life  to  be  processed  (in LIFEMJA using the KEEPS
*   function) by the called programs as follows:
*
*             Company - WSSP company
*             Contract no - from CHDRMJA
*             Life number - next life number available(worked out
*                  in 1000 section)
*
*   For all life  actions,  set up the key details of the life to
*   be processed  (in  LIFEMJA  using  the KEEPS function) by the
*   called programs as follows:
*
*             Company - WSSP company
*             Contract no - from CHDRMJA
*             Life number - from the subfile record
*
*   For all coverage/rider actions, set up the key details of the
*   coverage/rider to  be  processed  (in COVRMJA using the KEEPS
*   function) by the called programs as follows:
*
*             Company - WSSP company
*             Contract no - from CHDRMJA
*             Life number - from "hidden" field
*             Coverage number - from "hidden" field
*             Rider number - from "hidden" field
*
*   For all action, add one to the program pointer and exit.
*
*   After finishing the subfile records, if CALC was pressed, use
*   generalised secondaty switching, with a function of 'C'. Move
*   '*' to the stack action.
*
*****************************************************************
* </pre>
*/
public class P5130 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5130");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaMaxLives = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaActualRead = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsbbTableSub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsbbCurrLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbCurrCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbCalcPressed = new FixedLengthStringData(1).init("N");
	private ZonedDecimalData wsccSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsccSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsccError = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaCovrKeyFlag = new FixedLengthStringData(1);
	private Validator covrKeyChange = new Validator(wsaaCovrKeyFlag, "Y");

	private FixedLengthStringData wsaaCovtKeyFlag = new FixedLengthStringData(1);
	private Validator covtKeyChange = new Validator(wsaaCovtKeyFlag, "Y");
		/* ERRORS */
	private String e005 = "E005";
	private String e304 = "E304";
	private String f290 = "F290";
	private String g435 = "G435";
	private String h093 = "H093";
		/* TABLES */
	private String t5671 = "T5671";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private Batckey wsddBatckey = new Batckey();
	private S5130ScreenVars sv = ScreenProgram.getScreenVars( S5130ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		exit1190, 
		exit1290, 
		exit1390, 
		exit1490, 
		preExit, 
		updateErrorIndicators2670, 
		exit2890, 
		exit12090, 
		bypass4020, 
		exit4090, 
		exit4110, 
		coverRiderKeep4720, 
		exit4790, 
		exit4799
	}

	public P5130() {
		super();
		screenVars = sv;
		new ScreenModel("S5130", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			readClts1020();
			beginLifeFile1030();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsddBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsbbCalcPressed.set(SPACES);
		wsaaStoredLife.set(SPACES);
		wsaaStoredCoverage.set(SPACES);
		wsaaStoredRider.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		sv.cnttype.set(chdrmjaIO.getCnttype());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readClts1020()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cownnum.set(chdrmjaIO.getCownnum());
		sv.elemkey.set(chdrmjaIO.getCnttype());
		sv.elemdesc.set(descIO.getLongdesc());
		sv.actionOut[varcom.nd.toInt()].set("Y");
		sv.actionOut[varcom.pr.toInt()].set("Y");
		scrnparams.function.set(varcom.sadd);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void beginLifeFile1030()
	{
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		/*LOAD-SUBFILE*/
		while ( !(isEQ(lifemjaIO.getStatuz(),varcom.endp))) {
			loadSubfile1100();
		}
		
	}

protected void loadSubfile1100()
	{
		try {
			lifeDetails1110();
			loadComponents1130();
		}
		catch (GOTOException e){
		}
	}

protected void lifeDetails1110()
	{
		SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK))
		&& (isNE(lifemjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isNE(lifemjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(lifemjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		wsaaStoredLife.set(lifemjaIO.getLife());
		sv.life.set(lifemjaIO.getLife());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		sv.elemkey.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.elemdesc.set(wsspcomn.longconfname);
		sv.actionOut[varcom.nd.toInt()].set(SPACES);
		sv.actionOut[varcom.pr.toInt()].set(SPACES);
		sv.hcoverage.set(SPACES);
		sv.hrider.set(SPACES);
		sv.hcrtable.set(SPACES);
		sv.hcovt.set("N");
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.hlife.set(lifemjaIO.getLife());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		searchJointLife1200();
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(wsaaStoredLife);
		covrmjaIO.setCoverage("01");
		covrmjaIO.setRider("00");
		covrmjaIO.setPlanSuffix(9999);
	}

protected void loadComponents1130()
	{
		wsaaStoredCoverage.set(ZERO);
		wsaaCovrKeyFlag.set("N");
		while ( !(covrKeyChange.isTrue())) {
			loadCovrmja1300();
		}
		
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		covtmjaIO.setChdrcoy(wsspcomn.company);
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife(wsaaStoredLife);
		covtmjaIO.setCoverage(wsaaStoredCoverage);
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(9999);
		wsaaStoredLife.set(lifemjaIO.getLife());
		wsaaCovtKeyFlag.set("N");
		while ( !(covtKeyChange.isTrue())) {
			loadCovtmja1400();
		}
		
		lifemjaIO.setFunction(varcom.nextr);
	}

protected void searchJointLife1200()
	{
		try {
			nextLiferec1210();
			jlifeToScreen1220();
		}
		catch (GOTOException e){
		}
	}

protected void nextLiferec1210()
	{
		lifemjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK))
		&& (isNE(lifemjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isNE(lifemjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(lifemjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		if (isEQ(lifemjaIO.getJlife(),ZERO)) {
			goTo(GotoLabel.exit1290);
		}
		sv.life.set(" +");
		sv.hlife.set(lifemjaIO.getLife());
		sv.actionOut[varcom.pr.toInt()].set("Y");
		sv.actionOut[varcom.nd.toInt()].set("Y");
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		sv.elemkey.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.elemdesc.set(wsspcomn.longconfname);
	}

protected void jlifeToScreen1220()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void loadCovrmja1300()
	{
		try {
			coverAndRiders1310();
			compToScreen1320();
		}
		catch (GOTOException e){
		}
	}

protected void coverAndRiders1310()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(),wsaaStoredLife)
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			wsaaCovrKeyFlag.set("Y");
			goTo(GotoLabel.exit1390);
		}
		if (isEQ(covrmjaIO.getCoverage(),wsaaStoredCoverage)
		&& isEQ(covrmjaIO.getRider(),wsaaStoredRider)) {
			covrmjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit1390);
		}
		if (isNE(covrmjaIO.getCoverage(),wsaaStoredCoverage)
		&& isNE(wsaaStoredCoverage,ZERO)) {
			wsaaCovtKeyFlag.set("N");
			covtmjaIO.setDataArea(SPACES);
			covtmjaIO.setChdrcoy(wsspcomn.company);
			covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
			covtmjaIO.setLife(wsaaStoredLife);
			covtmjaIO.setCoverage(wsaaStoredCoverage);
			covtmjaIO.setRider(wsaaStoredRider);
			covtmjaIO.setPlanSuffix(9999);
			covtmjaIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
			wsaaCovtKeyFlag.set("N");
			while ( !(covtKeyChange.isTrue())) {
				loadCovtmja1400();
			}
			
		}
		wsaaStoredCoverage.set(covrmjaIO.getCoverage());
		wsaaStoredRider.set(covrmjaIO.getRider());
		sv.hcovt.set("N");
	}

protected void compToScreen1320()
	{
		sv.actionOut[varcom.pr.toInt()].set(SPACES);
		sv.actionOut[varcom.nd.toInt()].set(SPACES);
		if (isEQ(covrmjaIO.getRider(),SPACES)
		|| isEQ(covrmjaIO.getRider(),"00")) {
			sv.coverage.set(covrmjaIO.getCoverage());
			sv.hcoverage.set(covrmjaIO.getCoverage());
			sv.hrider.set("00");
			sv.rider.set(SPACES);
			sv.life.set(SPACES);
			sv.hcrtable.set(covrmjaIO.getCrtable());
		}
		else {
			sv.life.set(SPACES);
			sv.rider.set(covrmjaIO.getRider());
			sv.hrider.set(covrmjaIO.getRider());
			sv.coverage.set(SPACES);
			sv.hcoverage.set(covrmjaIO.getCoverage());
			sv.hcrtable.set(covrmjaIO.getCrtable());
		}
		descIO.setDescitem(covrmjaIO.getCrtable());
		sv.elemkey.set(covrmjaIO.getCrtable());
		getDescription1600();
		sv.hlife.set(covrmjaIO.getLife());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void loadCovtmja1400()
	{
		try {
			coverAndRiders1410();
			compToScreen1420();
		}
		catch (GOTOException e){
		}
	}

protected void coverAndRiders1410()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covtmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covtmjaIO.getLife(),wsaaStoredLife)
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			wsaaCovtKeyFlag.set("Y");
			goTo(GotoLabel.exit1490);
		}
		if (!covrKeyChange.isTrue()
		&& isNE(covtmjaIO.getCoverage(),wsaaStoredCoverage)) {
			wsaaCovtKeyFlag.set("Y");
			goTo(GotoLabel.exit1490);
		}
		if (isEQ(covtmjaIO.getCoverage(),wsaaStoredCoverage)
		&& isEQ(covtmjaIO.getRider(),wsaaStoredRider)) {
			covtmjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit1490);
		}
		wsaaStoredCoverage.set(covtmjaIO.getCoverage());
		wsaaStoredRider.set(covtmjaIO.getRider());
		sv.hcovt.set("Y");
	}

protected void compToScreen1420()
	{
		sv.actionOut[varcom.pr.toInt()].set(SPACES);
		sv.actionOut[varcom.nd.toInt()].set(SPACES);
		if (!covrKeyChange.isTrue()
		&& isEQ(covtmjaIO.getRider(),SPACES)
		|| isEQ(covtmjaIO.getRider(),"00")) {
			sv.coverage.set(covtmjaIO.getCoverage());
			sv.hcoverage.set(covtmjaIO.getCoverage());
			sv.hrider.set("00");
			sv.rider.set(SPACES);
			sv.life.set(SPACES);
			sv.hcrtable.set(covtmjaIO.getCrtable());
		}
		else {
			sv.life.set(SPACES);
			sv.rider.set(covtmjaIO.getRider());
			sv.hrider.set(covtmjaIO.getRider());
			sv.coverage.set(SPACES);
			sv.hcoverage.set(covtmjaIO.getCoverage());
			sv.hcrtable.set(covtmjaIO.getCrtable());
		}
		descIO.setDescitem(covtmjaIO.getCrtable());
		sv.elemkey.set(covtmjaIO.getCrtable());
		getDescription1600();
		sv.hlife.set(covtmjaIO.getLife());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		covtmjaIO.setFunction(varcom.nextr);
	}

protected void getDescription1600()
	{
		getDescription1610();
	}

protected void getDescription1610()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.elemdesc.set(descIO.getLongdesc());
	}

protected void getClientDetails1700()
	{
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateSubfile2020();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsbbCalcPressed.set("Y");
		}
		else {
			wsbbCalcPressed.set(SPACES);
		}
	}

protected void validateSubfile2020()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isNE(scrnparams.errorCode,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
					readNextRecord2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isNE(sv.action,"1")
		&& isNE(sv.action,"2")
		&& isNE(sv.action,"3")
		&& isNE(sv.action,SPACES)) {
			sv.actionErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isNE(sv.life,SPACES)) {
			validateLife2700();
		}
		else {
			if (isNE(sv.coverage,SPACES)) {
				validateCoverage2800();
			}
			else {
				validateRider2900();
			}
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void validateLife2700()
	{
		/*VALIDATE-LIFE*/
		if (isNE(sv.action,"2")
		&& isNE(sv.action,SPACES)) {
			sv.actionErr.set(e005);
		}
		/*EXIT*/
	}

protected void validateCoverage2800()
	{
		try {
			validateCoverage2810();
		}
		catch (GOTOException e){
		}
	}

protected void validateCoverage2810()
	{
		if (isNE(sv.action,"1")
		&& isNE(sv.action,"2")
		&& isNE(sv.action,"3")
		&& isNE(sv.action,SPACES)) {
			sv.actionErr.set(e005);
			goTo(GotoLabel.exit2890);
		}
		if (isNE(sv.hcovt,"Y")
		&& isEQ(sv.action,"1")) {
			sv.actionErr.set(g435);
			goTo(GotoLabel.exit2890);
		}
		if (isEQ(sv.action,"3")) {
			checkRIAllowed2f00();
		}
	}

protected void validateRider2900()
	{
		try {
			validateRider2910();
		}
		catch (GOTOException e){
		}
	}

protected void validateRider2910()
	{
		if (isNE(sv.action,"1")
		&& isNE(sv.action,"3")
		&& isNE(sv.action,SPACES)) {
			sv.actionErr.set(e005);
			goTo(GotoLabel.exit12090);
		}
		if (isNE(sv.hcovt,"Y")
		&& isEQ(sv.action,"1")) {
			sv.actionErr.set(g435);
			goTo(GotoLabel.exit12090);
		}
		if (isEQ(sv.action,"3")) {
			checkRIAllowed2f00();
		}
	}

protected void checkRIAllowed2f00()
	{
		checkRIAllowed2f10();
	}

protected void checkRIAllowed2f10()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(sv.elemkey);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),sv.elemkey)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.actionErr.set(f290);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case bypass4020: {
					bypass4020();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			goTo(GotoLabel.bypass4020);
		}
		wsccError.set(SPACES);
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypass4020()
	{
		if (isEQ(sv.action,SPACES)) {
			while ( !(isNE(sv.action,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4800();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)
		&& isNE(wsbbCalcPressed,"Y")) {
			gensswrec.function.set("E");
			callGenssw4100();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)
		&& isNE(wsbbCalcPressed,"Y")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(wsccError);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isEQ(wsbbCalcPressed,"Y")) {
			wsbbCalcPressed.set("N");
			gensswrec.function.set("D");
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			callGenssw4100();
			goTo(GotoLabel.exit4090);
		}
		if (isNE(sv.life,SPACES)) {
			lifeType4600();
		}
		if (isNE(sv.coverage,SPACES)
		|| isNE(sv.rider,SPACES)) {
			coverRiderType4700();
		}
		sv.action.set(SPACES);
	}

protected void callGenssw4100()
	{
		try {
			callGenssw4110();
		}
		catch (GOTOException e){
		}
	}

protected void callGenssw4110()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsddBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4110);
		}
		compute(wsccSub1, 0).set(add(1,wsspcomn.programPtr));
		wsccSub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			gensToWsspProgs4300();
		}
		if (isEQ(gensswrec.function,"E")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void gensToWsspProgs4300()
	{
		/*GENS-TO-WSSP-PROGS*/
		wsspcomn.secProg[wsccSub1.toInt()].set(gensswrec.progOut[wsccSub2.toInt()]);
		wsccSub1.add(1);
		wsccSub2.add(1);
		/*EXIT*/
	}

protected void lifeType4600()
	{
		lifeType4610();
	}

protected void lifeType4610()
	{
		covrmjaIO.setFunction(varcom.rlse);
		covrmjaCall6000();
		lifemjaIO.setFunction(varcom.rlse);
		lifemjaCall5000();
		if (isEQ(sv.action,"2")) {
			gensswrec.function.set("A");
		}
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(sv.chdrnum);
		lifemjaIO.setLife(sv.life);
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.reads);
		lifemjaCall5000();
		callGenssw4100();
	}

protected void coverRiderType4700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					coverRiderType4710();
				}
				case coverRiderKeep4720: {
					coverRiderKeep4720();
				}
				case exit4790: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void coverRiderType4710()
	{
		lifemjaIO.setFunction(varcom.rlse);
		lifemjaCall5000();
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(sv.chdrnum);
		lifemjaIO.setLife(sv.hlife);
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.reads);
		lifemjaCall5000();
		covrmjaIO.setFunction(varcom.rlse);
		covrmjaCall6000();
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(sv.hlife);
		covrmjaIO.setCoverage(sv.hcoverage);
		if (isEQ(sv.hrider,SPACES)) {
			sv.hrider.set("00");
		}
		covrmjaIO.setRider(sv.hrider);
		covrmjaIO.setPlanSuffix(ZERO);
		if (isEQ(sv.hcovt,"Y")) {
			appVars.addDiagnostic("CRTABLE "+sv.hcrtable, 0);
			covrmjaIO.setCrtable(sv.hcrtable);
			goTo(GotoLabel.coverRiderKeep4720);
		}
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,covrmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(sv.hlife,covrmjaIO.getLife())
		|| isNE(sv.hcoverage,covrmjaIO.getCoverage())
		|| isNE(sv.hrider,covrmjaIO.getRider())) {
			covrmjaIO.setStatuz(varcom.endp);
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void coverRiderKeep4720()
	{
		covrmjaIO.setFunction(varcom.keeps);
		covrmjaIO.setFormat("COVRMJAREC");
		covrmjaCall6000();
		if (isEQ(sv.hrider,"00")) {
			sv.hrider.set(SPACES);
		}
		if (isEQ(sv.action,"1")) {
			lookupT56714791();
			goTo(GotoLabel.exit4790);
		}
		if (isEQ(sv.action,"2")) {
			gensswrec.function.set("B");
		}
		if (isEQ(sv.action,"3")) {
			gensswrec.function.set("C");
		}
		callGenssw4100();
	}

protected void lookupT56714791()
	{
		try {
			lookupT56714792();
			loadProgsToWssp4794();
		}
		catch (GOTOException e){
		}
	}

protected void lookupT56714792()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsddBatckey.batcBatctrcde.toString());
		stringVariable1.append(sv.elemkey.toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
	}

protected void loadProgsToWssp4794()
	{
		compute(wsccSub1, 0).set(add(1,wsspcomn.programPtr));
		wsccSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			t5671ToWsspProgs4796();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4799);
	}

protected void t5671ToWsspProgs4796()
	{
		wsspcomn.secProg[wsccSub1.toInt()].set(t5671rec.pgm[wsccSub2.toInt()]);
		wsccSub1.add(1);
		wsccSub2.add(1);
	}

protected void readSubfile4800()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5130", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void lifemjaCall5000()
	{
		/*LIFEMJA-CALL*/
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void covrmjaCall6000()
	{
		/*COVRMJA-CALL*/
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
}
