/*
 * File: P5134.java
 * Date: 30 August 2009 0:12:56
 * Author: Quipoz Limited
 * 
 * Class transformed from P5134.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.S5134ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*             P5134 - COMPONENT SELECTION MAJOR ALTS
*             ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Initialise
*----------
*
*  Skip  this  section  if  returning from an optional selection
*  (current stack position action flag = '*').
*
*  Clear the subfile ready for loading.
*
*  The  details  of  the  contract  being  enquired upon will be
*  stored  in  the  CHDRMJA I/O module. Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  Load the subfile as follows:
*
*  Retrive the COVRMJA file (RETRV), if  the selected plan has a
*  plan suffix  which  is  contained  within the summarised plan
*  record:  less than  or  equal  to  the  number  of summarised
*  policies (CHDR-POLSUM):  then the policy must be 'broken out'
*  with each life,  coverage  and rider for an associated policy
*  written to the  subfile  along  with  the  description of the
*  cover/rider and their short status code descriptions.
*
*  COVR-STATCODE will be decoded against T5682 - Risk Status and
*  COVR-PSTATCODE  will  be  decoded  against  T5681  -  Premium
*  Status.
*
*       The first record of the subfile will have the first life
*       details for a contract. There will be one or many lives,
*       for each  life, there will be one or many coverages. For
*       each coverage, there will be none, one or many riders.
*
*       Use a key of CHDRCOY  and CHDRNUM, life  number 01 and a
*       joint life  number 00 to perform a BEGN on the life file
*       LIFEALT.
*
*       Display the  Life  number from the LIFEALT record in the
*       Component   Number   field.  Place  the  Client  Number,
*       (LIFE-LIFCNUM),   in   the   Component   field  and  the
*       "confirmation name"  (from  CLTS)  for  the  life as the
*       Component   Description.   Use   the   Contract  Company
*       (CHDRCOY),   Contract   Number  (CHDRNUM),  Life  Number
*       (LIFE),Plan suffix from COVRMJA (If contained within the
*       summarised records,  store  the  plan  suffix number and
*       move '0000' to Plan suffix.) with Coverage and Rider set
*       to '00',then perform a BEGN on COVRMJA. This will return
*       the first coverage for the Life.
*
*       If  the stored  Plan  Suffix  equal  to  '0000'  then  a
*       selection is  required accross the whole plan, therefore
*       load all  records  with  broken out Policies placing the
*       Plan suffix in  the Hidden Field. i.e. ignore changes in
*       Plan suffix,  exit  when change of company, and contract
*       number.
*
*       Coverage details  -  write  the  coverage  record to the
*       subfile with the coverage number in the Component Number
*       field.  Obtain the Coverage Code from T5673 and place it
*       in the Component field indented by 2 spaces. Look up its
*       description from  T5687  for the element description and
*       place   this   in   the   Component  Description  field.
*       Sequentially read the next coverage/rider record.
*
*       NOTE:   read  T5673  the  Contract  Structure  table  by
*       contract  type  (from  CHDRMJA)  and  effective  on  the
*       original   inception   date   of   the   contract.  When
*       processing this table item remember that it can continue
*       onto  additional  (unlimited) table items.  These should
*       be read in turn during the processing.
*
*       Rider details -  if  the  coverage number is the same as
*       the one  put  on  the coverage subfile record above, the
*       rider number will  not  be  zero  (if  it  is, this is a
*       database error).  This is a rider on the above coverage.
*       Write the  rider  record  to  the subfile with the rider
*       number in the Component Number field and its description
*       from   T5687   in   the   Component  Description  field.
*       Sequentially  read  the  next coverage/rider record.  If
*       this  is   another   rider   record   for   the  current
*       transaction, repeat  this  rider  processing.  If  it is
*       another  coverage   for   the   same  life,  repeat  the
*       processing from  the coverage section above.  If it is a
*       coverage  for   another   life,  repeat  all  the  above
*       processing for the  next  life.  If it is a coverage for
*       another proposal (or the end of the file) all components
*       have been  loaded. On each subfile load, store in hidden
*       fields the Plan Suffix, life, cover and rider numbers.
*
*  Load all pages  required  in  the subfile and set the subfile
*  more indicator to no.
*
*Validation
*----------
*
*  There is no validation in this program.
*
*Updating
*--------
*
*  There is no updating in this program.
*
*
*Next Program
*------------
*
*  If "CF11" was requested move spaces to  the  current  program
*  position and action field, add 1 to the  program  pointer and
*  exit.
*
*  At this point the program will be either  searching  for  the
*  FIRST  selected  record  in  order  to pass  control  to  the
*  appropriate  generic  enquiry  program   for   the   selected
*  component  or it will be returning from one  of  the  Enquiry
*  screens after displaying some details and  searching  for the
*  NEXT selected record.
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*  If not returning  from  a  component (stack action is blank),
*  save the next four  programs currently on the stack. Read the
*  first record from the subfile. If this is not  selected, read
*  the next one and so on, until a selected  record is found, or
*  the end of the subfile is reached.
*
*  If a subfile  record  has been selected, look up the programs
*  required to  be  processed  from  the coverage/rider programs
*  table (T5671  -  accessed  by transaction number concatenated
*  with coverage/rider code from the subfile record). Move these
*  four programs into  the  program  stack  and  set the current
*  stack action to '*'  (so  that the system will return to this
*  program to process the next one).
*
*  RLSE the COVRMJA record, then read with the key below. If O-K
*  set up the  key details of the coverage/rider to be processed
*  (in COVRMJA using  the KEEPS func) by the called programs as
*  follows:
*
*            Company - WSSP company
*            Contract no - from CHDRMJA
*            Plan Suffix - from hidden Plan suffix field
*            Life number - from hidden life field
*            Coverage number -  from hidden coverage field
*            Rider number - from hidden rider field
*
*  Add one to  the  program  pointer  and  exit  to  process the
*  required generic component.
*
*  If nothing was selected or there are no  more  selections  to
*  process,  continue  by  moving blanks to  the  current  Stack
*  Action  field  reload  the  saved  four programs and exit.
*
*Notes.
*------
*
*  Tables Used:
*
*T5671 - Cover/Rider Secondary Switching Key: Transaction Code
*                                           + Program Number
*T3588 - Contract Premium Status         Key: PSTATCODE
*T3623 - Contract Risk Status            Key: STATCODE
*T3673 - Contract Structure              Key: CNTTYPE
*T5688 - Contract Structure              Key: CNTTYPE
*
*Examples.
*---------
*
*  In this  example  the Contract Header record, (CHDR), has the
*  following values:
*
*CHDR-POLSUM = 3
*CHDR-POLINC = 5
*CHDR-NXTSFX = 0004
*
*  This indicates that  the number of policies in plan is 5, the
*  number summarised  is  3  and  therefore the next plan suffix
*  number is 0004.
*
*<-- File Details -->          <--------  Screen Details -------->
*
* Plan  Life Cov Rid           Component     Component Description
*Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
* 0000   01   01  00              0001
*                                 0002
*                                 0003
*                              01 12345678   Life Name #1
*                              01   COV1     Coverage #1
* 0000   01   01  01           01     RID1   Rider #1 of Coverage #1
* 0000   01   01  02           02     RID2   Rider #2 of Coverage #1
* 0000   01   02  00           01   COV2     Coverage #2
* 0000   01   02  01           01     RID1   Rider #1 of Coverage #2
* 0000   02   01  00           02 87654321   Life Name #2
*                              01   COV1     Coverage #1
* 0000   02   01  01           01     RID1   Rider #1 of Coverage #1
* 0000   02   01  02           02     RID2   Rider #2 of Coverage #1
*
*  The  above  records  show the lives and  components  for  the
*  summary records. In this example the first  three  lines show
*  which  policies  with  the  plan  are   summarised  by  these
*  components.
*
*  At this point  the program will come upon the first component
*  for the next  policy  within  the  plan  -  one that does not
*  summarise  anything   and   so   its  display  will  be  more
*  straightforward.
*
* Plan  Life Cov Rid           Component     Component Description
*Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
* 0004   01   01  00              0004
*                             01 12345678   Life Name #1
*                             01   COV1     Coverage #1
* 0004   01   01  01          01     RID1   Rider #1 of Coverage #1
* 0004   01   01  02          02     RID2   Rider #2 of Coverage #1
* 0004   01   01  03          03     RID3   Rider #3 of Coverage #1
* 0004   01   02  00          01   COV2     Coverage #2
* 0004   01   02  01          01     RID1   Rider #1 of Coverage #2
* 0004   01   03  00          03   COV3     Coverage #3
* 0004   02   01  00          02 87654321   Life Name #2
*                             01   COV1     Coverage #1
* 0004   02   01  01          01     RID1   Rider #1 of Coverage #1
* 0004   02   01  02          02     RID2   Rider #2 of Coverage #1
* 0005   01   01  00             0005
*                             01 12345678   Life Name #1
*                             01   COV1     Coverage #1
* 0005   01   01  01          01     RID1   Rider #1 of Coverage #1
* 0005   01   01  02          02     RID2   Rider #2 of Coverage #1
* 0005   01   01  03          03     RID3   Rider #3 of Coverage #1
* 0005   01   02  00          01   COV2     Coverage #2
* 0005   01   02  01          01     RID1   Rider #1 of Coverage #2
* 0005   01   03  00          03   COV3     Coverage #3
* 0005   02   01  00          02 87654321   Life Name #2
*                             01   COV1     Coverage #1
* 0005   02   01  01          01     RID1   Rider #1 of Coverage #1
* 0005   02   01  02          02     RID2   Rider #2 of Coverage #1
*
*
*****************************************************************
* </pre>
*/
public class P5134 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5134");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-MISCELLANEOUS-FLAGS */
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");

	private FixedLengthStringData wsaaLifeExit = new FixedLengthStringData(1);
	private Validator wsaaLifeEof = new Validator(wsaaLifeExit, "Y");

	private FixedLengthStringData wsaaCovrKeyFlag = new FixedLengthStringData(1);
	private Validator covrKeyChange = new Validator(wsaaCovrKeyFlag, "Y");

	private FixedLengthStringData wsaaCovtKeyFlag = new FixedLengthStringData(1);
	private Validator covtKeyChange = new Validator(wsaaCovtKeyFlag, "Y");
	private FixedLengthStringData wsaaLifemjaKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
		/* WSAA-PROGRAM-SAVE */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(4, 5);
	private static final String g433 = "G433";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5671 = "T5671";
	private static final String t5681 = "T5681";
	private static final String t5682 = "T5682";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String tr386 = "TR386";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String covtmjarec = "COVTMJAREC";
	private static final String itemrec = "ITEMREC";
		/*    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5671rec t5671rec = new T5671rec();
	private Tr386rec tr386rec = new Tr386rec();
	private S5134ScreenVars sv = ScreenProgram.getScreenVars( S5134ScreenVars.class);
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1190, 
		exit1390, 
		exit1490, 
		exit2690, 
		exit4190, 
		exit4290, 
		readProgramTable4510, 
		exit4590, 
		exit4690
	}

	public P5134() {
		super();
		screenVars = sv;
		new ScreenModel("S5134", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			retrvContract1020();
			retrvCoverage1040();
			readLifeDetails1050();
			jointLifeDetails1060();
			contractTypeStatus1070();
			subfileLoad1080();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaImmexitFlag.set(SPACES);
		wsaaSelectFlag.set(SPACES);
		wsaaLifeExit.set(SPACES);
		wsaaCovrKeyFlag.set(SPACES);
		wsaaCovtKeyFlag.set(SPACES);
		wsaaMiscellaneousInner.wsaaStoredCoverage.set(SPACES);
		wsaaMiscellaneousInner.wsaaStoredRider.set(SPACES);
		/* Clear the Subfile.*/
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvContract1020()
	{
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*HEADER-DETAIL*/
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.register.set(chdrmjaIO.getRegister());
		sv.numpols.set(chdrmjaIO.getPolinc());
	}

protected void retrvCoverage1040()
	{
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		sv.planSuffix.set(covrmjaIO.getPlanSuffix());
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(covrmjaIO.getPlanSuffix());
		/*--- Read TR386 table to get screen literals                      */
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		if (isEQ(sv.planSuffix,ZERO)) {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("L");
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			/*     MOVE 'Whole Plan      '  TO S5134-ENTITY                  */
			/*     MOVE TR386-PROGDESC-1    TO S5134-ENTITY                  */
			sv.entity.set(tr386rec.progdesc01);
			sv.entityOut[varcom.hi.toInt()].set("Y");
		}
		else {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("O");
			sv.plnsfxOut[varcom.nd.toInt()].set(" ");
			/*     MOVE 'Policy Number  :'  TO S5134-ENTITY                  */
			/*     MOVE TR386-PROGDESC-2    TO S5134-ENTITY                  */
			sv.entity.set(tr386rec.progdesc02);
			sv.entityOut[varcom.hi.toInt()].set(" ");
		}
		/* If the policy selected is a summarised policy then to read the*/
		/* Coverage/Rider records we must zeroise the Plan Suffix.*/
		if (isLTE(covrmjaIO.getPlanSuffix(),chdrmjaIO.getPolsum())
		&& isNE(covrmjaIO.getPlanSuffix(),ZERO)) {
			wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		}
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void readLifeDetails1050()
	{
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1060()
	{
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			sv.jlife.set("NONE");
			sv.jlifename.set("NONE");
		}
		else {
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

protected void contractTypeStatus1070()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void subfileLoad1080()
	{
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaLifeExit.set("N");
		while ( !(wsaaLifeEof.isTrue())) {
			processLife1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void processLife1100()
	{
		try {
			mainLife1110();
			screenFields1120();
			subfileAdd1130();
			jlifeCheck1140();
			listCoverages1150();
			loadComponents1160();
		}
		catch (GOTOException e){
		}
	}

protected void mainLife1110()
	{
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		/* If change of Contract or Branch then exit.*/
		if (isNE(wsspcomn.company, lifemjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),lifemjaIO.getChdrnum())) {
			wsaaLifeExit.set("Y");
			goTo(GotoLabel.exit1190);
		}
	}

protected void screenFields1120()
	{
		sv.subfileFields.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.selectOut[varcom.nd.toInt()].set("Y");
		sv.cmpntnum.set(lifemjaIO.getLife());
		sv.hlifeno.set(lifemjaIO.getLife());
		sv.component.set(lifemjaIO.getLifcnum());
		sv.hlifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		sv.hsuffix.set(ZERO);
		wsaaLifemjaKey.set(lifemjaIO.getDataKey());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.deit.set(wsspcomn.longconfname);
	}

protected void subfileAdd1130()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void jlifeCheck1140()
	{
		lifemjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isNE(lifemjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(lifemjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			wsaaLifeExit.set("Y");
		}
		if (isNE(lifemjaIO.getJlife(),ZERO)) {
			jlifeToScreen1200();
		}
		else {
			lifemjaIO.setDataKey(wsaaLifemjaKey);
		}
	}

protected void listCoverages1150()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(lifemjaIO.getLife());
		covrmjaIO.setCoverage("00");
		covrmjaIO.setRider("00");
		covrmjaIO.setPlanSuffix(wsaaMiscellaneousInner.wsaaPlanSuffix);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
	}

protected void loadComponents1160()
	{
		wsaaMiscellaneousInner.wsaaStoredCoverage.set(ZERO);
		wsaaCovrKeyFlag.set("N");
		while ( !(covrKeyChange.isTrue())) {
			loadCovrmja1300();
		}
		
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setChdrcoy(wsspcomn.company);
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife(lifemjaIO.getLife());
		covtmjaIO.setCoverage(wsaaMiscellaneousInner.wsaaStoredCoverage);
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(wsaaMiscellaneousInner.wsaaPlanSuffix);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		wsaaCovtKeyFlag.set("N");
		while ( !(covtKeyChange.isTrue())) {
			loadCovtmja1400();
		}
		
	}

protected void jlifeToScreen1200()
	{
		jlifeToSubfile1210();
		subfileAdd1220();
	}

protected void jlifeToSubfile1210()
	{
		sv.cmpntnum.set(" +");
		sv.component.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		sv.hsuffix.set(ZERO);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.deit.set(wsspcomn.longconfname);
	}

protected void subfileAdd1220()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadCovrmja1300()
	{
		try {
			coverAndRiders1310();
			compToScreen1320();
		}
		catch (GOTOException e){
		}
	}

protected void coverAndRiders1310()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(),lifemjaIO.getLife())
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			wsaaCovrKeyFlag.set("Y");
			goTo(GotoLabel.exit1390);
		}
		if (isEQ(covrmjaIO.getCoverage(), wsaaMiscellaneousInner.wsaaStoredCoverage)
		&& isEQ(covrmjaIO.getRider(), wsaaMiscellaneousInner.wsaaStoredRider)) {
			covrmjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit1390);
		}
		/* If we have selected a Policy and the Plan suffix changes on the*/
		/* file read then we have read all associated details.*/
		if (isNE(covrmjaIO.getPlanSuffix(), wsaaMiscellaneousInner.wsaaPlanSuffix)) {
			covrmjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit1390);
		}
		/* store coverage no*/
		if (isNE(covrmjaIO.getCoverage(), wsaaMiscellaneousInner.wsaaStoredCoverage)
		&& isNE(wsaaMiscellaneousInner.wsaaStoredCoverage, ZERO)) {
			wsaaCovtKeyFlag.set("N");
			covtmjaIO.setDataArea(SPACES);
			covtmjaIO.setChdrcoy(wsspcomn.company);
			covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
			covtmjaIO.setLife(lifemjaIO.getLife());
			covtmjaIO.setCoverage(wsaaMiscellaneousInner.wsaaStoredCoverage);
			covtmjaIO.setRider(wsaaMiscellaneousInner.wsaaStoredRider);
			covtmjaIO.setPlanSuffix(wsaaMiscellaneousInner.wsaaPlanSuffix);
			covtmjaIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
			wsaaCovtKeyFlag.set("N");
			while ( !(covtKeyChange.isTrue())) {
				loadCovtmja1400();
			}
			
		}
		wsaaMiscellaneousInner.wsaaStoredCoverage.set(covrmjaIO.getCoverage());
		wsaaMiscellaneousInner.wsaaStoredRider.set(covrmjaIO.getRider());
		sv.hcovt.set("N");
		sv.selectOut[varcom.pr.toInt()].set(" ");
		sv.selectOut[varcom.nd.toInt()].set(" ");
	}

protected void compToScreen1320()
	{
		/* Clear the life and rider fields in the subfile record and*/
		/* set up the record including hidden fields.*/
		if (isEQ(covrmjaIO.getRider(), "  ")
		|| isEQ(covrmjaIO.getRider(),"00")) {
			sv.cmpntnum.set(covrmjaIO.getCoverage());
			wsaaMiscellaneousInner.wsaaCoverage.set(covrmjaIO.getCrtable());
			sv.hcrtable.set(covrmjaIO.getCrtable());
			sv.component.set(wsaaMiscellaneousInner.wsaaCovrComponent);
			/* Store the COVRMJA key for later use in screen Hidden Fields.*/
			sv.hlifeno.set(covrmjaIO.getLife());
			sv.hsuffix.set(covrmjaIO.getPlanSuffix());
			sv.hcoverage.set(covrmjaIO.getCoverage());
			sv.hrider.set(covrmjaIO.getRider());
		}
		else {
			sv.cmpntnum.set(covrmjaIO.getCoverage());
			wsaaMiscellaneousInner.wsaaRider.set(covrmjaIO.getCrtable());
			sv.hcrtable.set(covrmjaIO.getCrtable());
			sv.component.set(wsaaMiscellaneousInner.wsaaRidrComponent);
			/* Store the COVRMJA key for later use in screen Hidden Fields.*/
			sv.hlifeno.set(covrmjaIO.getLife());
			sv.hsuffix.set(covrmjaIO.getPlanSuffix());
			sv.hcoverage.set(covrmjaIO.getCoverage());
			sv.hrider.set(covrmjaIO.getRider());
		}
		descIO.setDescitem(covrmjaIO.getCrtable());
		getDescription1600();
		covrStatusDescs1700();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void loadCovtmja1400()
	{
		try {
			coverAndRiders1410();
			compToScreen1420();
		}
		catch (GOTOException e){
		}
	}

protected void coverAndRiders1410()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covtmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covtmjaIO.getLife(),lifemjaIO.getLife())
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			wsaaCovtKeyFlag.set("Y");
			goTo(GotoLabel.exit1490);
		}
		if (!covrKeyChange.isTrue()
		&& isNE(covtmjaIO.getCoverage(), wsaaMiscellaneousInner.wsaaStoredCoverage)) {
			wsaaCovtKeyFlag.set("Y");
			goTo(GotoLabel.exit1490);
		}
		if (isEQ(covtmjaIO.getCoverage(), wsaaMiscellaneousInner.wsaaStoredCoverage)
		&& isEQ(covtmjaIO.getRider(), wsaaMiscellaneousInner.wsaaStoredRider)) {
			covtmjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit1490);
		}
		/* If we have selected a Policy and the Plan suffix changes on the*/
		/* file read then we have read all associated details.*/
		if (isNE(covrmjaIO.getPlanSuffix(), wsaaMiscellaneousInner.wsaaPlanSuffix)) {
			covtmjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit1490);
		}
		/*     GO TO 1390-EXIT.                                          */
		wsaaMiscellaneousInner.wsaaStoredCoverage.set(covtmjaIO.getCoverage());
		wsaaMiscellaneousInner.wsaaStoredRider.set(covtmjaIO.getRider());
		sv.hcovt.set("Y");
		sv.selectOut[varcom.pr.toInt()].set(" ");
		sv.selectOut[varcom.nd.toInt()].set(" ");
	}

protected void compToScreen1420()
	{
		if (!covrKeyChange.isTrue()
		&& isEQ(covtmjaIO.getRider(), "  ")
		|| isEQ(covtmjaIO.getRider(),"00")) {
			sv.cmpntnum.set(covtmjaIO.getCoverage());
			wsaaMiscellaneousInner.wsaaCoverage.set(covtmjaIO.getCrtable());
			sv.hcrtable.set(covtmjaIO.getCrtable());
			sv.component.set(wsaaMiscellaneousInner.wsaaCovrComponent);
			/* Store the COVRMJA key for later use in screen Hidden Fields.*/
			sv.hlifeno.set(covtmjaIO.getLife());
			sv.hsuffix.set(covtmjaIO.getPlanSuffix());
			sv.hcoverage.set(covtmjaIO.getCoverage());
			sv.hrider.set(covtmjaIO.getRider());
		}
		else {
			/* Set up and write the current Rider details.*/
			sv.cmpntnum.set(covtmjaIO.getCoverage());
			wsaaMiscellaneousInner.wsaaRider.set(covtmjaIO.getCrtable());
			sv.hcrtable.set(covtmjaIO.getCrtable());
			sv.component.set(wsaaMiscellaneousInner.wsaaRidrComponent);
			/* Store the COVTMJA key for later use in screen Hidden Fields.*/
			sv.hlifeno.set(covtmjaIO.getLife());
			sv.hsuffix.set(covtmjaIO.getPlanSuffix());
			sv.hcoverage.set(covtmjaIO.getCoverage());
			sv.hrider.set(covtmjaIO.getRider());
		}
		descIO.setDescitem(covtmjaIO.getCrtable());
		getDescription1600();
		sv.pstatcode.set(SPACES);
		sv.statcode.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		covtmjaIO.setFunction(varcom.nextr);
	}

protected void getDescription1600()
	{
		para1600();
	}

protected void para1600()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.deit.fill("?");
		}
		else {
			sv.deit.set(descIO.getLongdesc());
		}
	}

protected void covrStatusDescs1700()
	{
		para1710();
	}

protected void para1710()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5681);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrmjaIO.getPstatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatcode.fill("?");
		}
		else {
			sv.pstatcode.set(descIO.getShortdesc());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5682);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrmjaIO.getStatcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.statcode.fill("?");
		}
		else {
			sv.statcode.set(descIO.getShortdesc());
		}
	}

protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		scrnparams.subfileRrn.set(1);
		return ;
	}

protected void screenEdit2000()
	{
			screenIo2010();
		}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			return ;
		}
		scrnparams.subfileRrn.set(1);
		/*VALIDATE-SUBFILE*/
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		try {
			readNextModifiedRecord2610();
			updateScreen2630();
		}
		catch (GOTOException e){
		}
	}

protected void readNextModifiedRecord2610()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*VALIDATION*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2690);
		}
	}

protected void updateScreen2630()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* Update database files as required*/
		return ;
		/*EXIT*/
	}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			initSelection4100();
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4200();
		}
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (wsaaImmExit.isTrue()) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
				wsspcomn.nextprog.set(scrnparams.scrname);
				return ;
			}
		}
		if (wsaaImmExit.isTrue()) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(1);
			wsspcomn.nextprog.set(wsaaProg);
			return ;
		}
		if (isEQ(sv.hcovt,"Y")) {
			covtmjaStore4400();
		}
		else {
			covrmjaStore4300();
		}
		programTables4500();
	}

protected void initSelection4100()
	{
		try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
		}
	}

protected void saveNextProgs4110()
	{
		/*    IF WSAA-IMM-EXIT*/
		/*       GO TO 4190-EXIT.*/
		/* Save the  next  four  programs  after  P5134  into  Working*/
		/* Storage for re-instatement at the end of the Subfile.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			loop14120();
		}
		scrnparams.function.set(varcom.sstrt);
		goTo(GotoLabel.exit4190);
	}

protected void loop14120()
	{
		/* This loop will load the next four  programs from WSSP Stack*/
		/* into a Working Storage Save area.*/
		wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void next4200()
	{
		try {
			nextRec4210();
			ifSubfileEndp4220();
		}
		catch (GOTOException e){
		}
	}

protected void nextRec4210()
	{
		processScreen("S5134", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4220()
	{
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			reloadProgsWssp4230();
			goTo(GotoLabel.exit4290);
		}
		if (isNE(sv.select,SPACES)) {
			sv.asterisk.set("*");
			sv.select.set(SPACES);
			wsaaSelectFlag.set("Y");
			scrnparams.function.set(varcom.supd);
			processScreen("S5134", sv);
			if (isNE(scrnparams.statuz,varcom.oK)
			&& isNE(scrnparams.statuz,varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.exit4290);
	}

protected void reloadProgsWssp4230()
	{
		wsaaImmexitFlag.set("Y");
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop34240();
		}
		goTo(GotoLabel.exit4290);
	}

protected void loop34240()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void covrmjaStore4300()
	{
		coverRiderRead4300();
	}

protected void coverRiderRead4300()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(sv.hlifeno);
		covrmjaIO.setPlanSuffix(sv.hsuffix);
		covrmjaIO.setCoverage(sv.hcoverage);
		covrmjaIO.setRider(sv.hrider);
		wsaaMiscellaneousInner.wsaaCrtable.set(sv.hcrtable);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		covrmjaIO.setPlanSuffix(sv.planSuffix);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void covtmjaStore4400()
	{
		coverRiderRead4400();
	}

protected void coverRiderRead4400()
	{
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setChdrcoy(wsspcomn.company);
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife(sv.hlifeno);
		covtmjaIO.setPlanSuffix(sv.hsuffix);
		covtmjaIO.setCoverage(sv.hcoverage);
		covtmjaIO.setRider(sv.hrider);
		wsaaMiscellaneousInner.wsaaCrtable.set(sv.hcrtable);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		covtmjaIO.setPlanSuffix(sv.planSuffix);
		covtmjaIO.setFormat(covtmjarec);
		covtmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
	}

protected void programTables4500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case readProgramTable4510: 
					readProgramTable4510();
					loadProgsToWssp4520();
				case exit4590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readProgramTable4510()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.addExpression(wsaaMiscellaneousInner.wsaaCrtable);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			if (isEQ(wsaaMiscellaneousInner.wsaaCrtable, "****")) {
				sv.selectErr.set(g433);
				wsspcomn.edterror.set("Y");
				wsspcomn.nextprog.set(scrnparams.scrname);
				planReload4600();
				goTo(GotoLabel.exit4590);
			}
			else {
				wsaaMiscellaneousInner.wsaaCrtable.set("****");
				goTo(GotoLabel.readProgramTable4510);
			}
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
	}

protected void loadProgsToWssp4520()
	{
		/* Move the component programs to the WSSP stack.*/
		wsaaMiscellaneousInner.wsaaSub1.set(1);
		compute(wsaaMiscellaneousInner.wsaaSub2, 0).set(add(1, wsspcomn.programPtr));
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			loop24530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(wsaaProg);
		goTo(GotoLabel.exit4590);
	}

protected void loop24530()
	{
		/* This loop will load four programs from table T5671 to the WSSP*/
		/* stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(t5671rec.pgm[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void planReload4600()
	{
		try {
			reloadProgsWssp4610();
		}
		catch (GOTOException e){
		}
	}

protected void reloadProgsWssp4610()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			loop34620();
		}
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		goTo(GotoLabel.exit4690);
	}

protected void loop34620()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	private FixedLengthStringData wsaaPlanComponent = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanComponent, 0).setUnsigned();

	private FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	}
}
