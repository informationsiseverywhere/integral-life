package com.csc.life.contractservicing.dataaccess;

import com.csc.fsu.general.dataaccess.MandpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MandmjaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:15
 * Class transformed from MANDMJA.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MandmjaTableDAM extends MandpfTableDAM {

	public MandmjaTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MANDMJA");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "PAYRCOY"
		             + ", PAYRNUM"
		             + ", MANDREF";
		
		QUALIFIEDCOLUMNS = 
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "MANDREF, " +
		            "VALIDFLAG, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "TIMESUSE, " +
		            "MANDSTAT, " +
		            "STATCHDATE, " +
		            "LUSEDATE, " +
		            "LUSEAMT, " +
		            "LUSESCODE, " +
		            "MANDAMT, " +
		            "EFFDATE, " +
		            "DETLSUMM, " +
		            "DATESUB01, " +
		            "DATESUB02, " +
		            "DATESUB03, " +
		            "DATERJ01, " +
		            "DATERJ02, " +
		            "DATERJ03, " +
		            "DDRSNCDE01, " +
		            "DDRSNCDE02, " +
		            "DDRSNCDE03, " +
		            "CRCIND, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "PAYIND, " + //ILIFE-2472
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "PAYRCOY ASC, " +
		            "PAYRNUM ASC, " +
		            "MANDREF DESC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "PAYRCOY DESC, " +
		            "PAYRNUM DESC, " +
		            "MANDREF ASC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               payrcoy,
                               payrnum,
                               mandref,
                               validflag,
                               bankkey,
                               bankacckey,
                               timesUse,
                               mandstat,
                               statChangeDate,
                               lastUseDate,
                               lastUseAmt,
                               luStatCode,
                               mandAmt,
                               effdate,
                               detlsumm,
                               datesub01,
                               datesub02,
                               datesub03,
                               daterj01,
                               daterj02,
                               daterj03,
                               ddrsncde01,
                               ddrsncde02,
                               ddrsncde03,
                               crcind,
                               userProfile,
                               jobName,
                               datime,
                               payind, //ILIFE-2472
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(242);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getMandref().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller50.setInternal(payrcoy.toInternal());
	nonKeyFiller60.setInternal(payrnum.toInternal());
	nonKeyFiller70.setInternal(mandref.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(191);
		
		nonKeyData.set(
					getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getValidflag().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getTimesUse().toInternal()
					+ getMandstat().toInternal()
					+ getStatChangeDate().toInternal()
					+ getLastUseDate().toInternal()
					+ getLastUseAmt().toInternal()
					+ getLuStatCode().toInternal()
					+ getMandAmt().toInternal()
					+ getEffdate().toInternal()
					+ getDetlsumm().toInternal()
					+ getDatesub01().toInternal()
					+ getDatesub02().toInternal()
					+ getDatesub03().toInternal()
					+ getDaterj01().toInternal()
					+ getDaterj02().toInternal()
					+ getDaterj03().toInternal()
					+ getDdrsncde01().toInternal()
					+ getDdrsncde02().toInternal()
					+ getDdrsncde03().toInternal()
					+ getCrcind().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getPayind().toInternal()); //ILIFE-2472
	
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, timesUse);
			what = ExternalData.chop(what, mandstat);
			what = ExternalData.chop(what, statChangeDate);
			what = ExternalData.chop(what, lastUseDate);
			what = ExternalData.chop(what, lastUseAmt);
			what = ExternalData.chop(what, luStatCode);
			what = ExternalData.chop(what, mandAmt);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, detlsumm);
			what = ExternalData.chop(what, datesub01);
			what = ExternalData.chop(what, datesub02);
			what = ExternalData.chop(what, datesub03);
			what = ExternalData.chop(what, daterj01);
			what = ExternalData.chop(what, daterj02);
			what = ExternalData.chop(what, daterj03);
			what = ExternalData.chop(what, ddrsncde01);
			what = ExternalData.chop(what, ddrsncde02);
			what = ExternalData.chop(what, ddrsncde03);
			what = ExternalData.chop(what, crcind);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, payind); //ILIFE-2472
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public PackedDecimalData getTimesUse() {
		return timesUse;
	}
	public void setTimesUse(Object what) {
		setTimesUse(what, false);
	}
	public void setTimesUse(Object what, boolean rounded) {
		if (rounded)
			timesUse.setRounded(what);
		else
			timesUse.set(what);
	}	
	public FixedLengthStringData getMandstat() {
		return mandstat;
	}
	public void setMandstat(Object what) {
		mandstat.set(what);
	}	
	public PackedDecimalData getStatChangeDate() {
		return statChangeDate;
	}
	public void setStatChangeDate(Object what) {
		setStatChangeDate(what, false);
	}
	public void setStatChangeDate(Object what, boolean rounded) {
		if (rounded)
			statChangeDate.setRounded(what);
		else
			statChangeDate.set(what);
	}	
	public PackedDecimalData getLastUseDate() {
		return lastUseDate;
	}
	public void setLastUseDate(Object what) {
		setLastUseDate(what, false);
	}
	public void setLastUseDate(Object what, boolean rounded) {
		if (rounded)
			lastUseDate.setRounded(what);
		else
			lastUseDate.set(what);
	}	
	public PackedDecimalData getLastUseAmt() {
		return lastUseAmt;
	}
	public void setLastUseAmt(Object what) {
		setLastUseAmt(what, false);
	}
	public void setLastUseAmt(Object what, boolean rounded) {
		if (rounded)
			lastUseAmt.setRounded(what);
		else
			lastUseAmt.set(what);
	}	
	public FixedLengthStringData getLuStatCode() {
		return luStatCode;
	}
	public void setLuStatCode(Object what) {
		luStatCode.set(what);
	}	
	public PackedDecimalData getMandAmt() {
		return mandAmt;
	}
	public void setMandAmt(Object what) {
		setMandAmt(what, false);
	}
	public void setMandAmt(Object what, boolean rounded) {
		if (rounded)
			mandAmt.setRounded(what);
		else
			mandAmt.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getDetlsumm() {
		return detlsumm;
	}
	public void setDetlsumm(Object what) {
		detlsumm.set(what);
	}	
	public PackedDecimalData getDatesub01() {
		return datesub01;
	}
	public void setDatesub01(Object what) {
		setDatesub01(what, false);
	}
	public void setDatesub01(Object what, boolean rounded) {
		if (rounded)
			datesub01.setRounded(what);
		else
			datesub01.set(what);
	}	
	public PackedDecimalData getDatesub02() {
		return datesub02;
	}
	public void setDatesub02(Object what) {
		setDatesub02(what, false);
	}
	public void setDatesub02(Object what, boolean rounded) {
		if (rounded)
			datesub02.setRounded(what);
		else
			datesub02.set(what);
	}	
	public PackedDecimalData getDatesub03() {
		return datesub03;
	}
	public void setDatesub03(Object what) {
		setDatesub03(what, false);
	}
	public void setDatesub03(Object what, boolean rounded) {
		if (rounded)
			datesub03.setRounded(what);
		else
			datesub03.set(what);
	}	
	public PackedDecimalData getDaterj01() {
		return daterj01;
	}
	public void setDaterj01(Object what) {
		setDaterj01(what, false);
	}
	public void setDaterj01(Object what, boolean rounded) {
		if (rounded)
			daterj01.setRounded(what);
		else
			daterj01.set(what);
	}	
	public PackedDecimalData getDaterj02() {
		return daterj02;
	}
	public void setDaterj02(Object what) {
		setDaterj02(what, false);
	}
	public void setDaterj02(Object what, boolean rounded) {
		if (rounded)
			daterj02.setRounded(what);
		else
			daterj02.set(what);
	}	
	public PackedDecimalData getDaterj03() {
		return daterj03;
	}
	public void setDaterj03(Object what) {
		setDaterj03(what, false);
	}
	public void setDaterj03(Object what, boolean rounded) {
		if (rounded)
			daterj03.setRounded(what);
		else
			daterj03.set(what);
	}	
	public FixedLengthStringData getDdrsncde01() {
		return ddrsncde01;
	}
	public void setDdrsncde01(Object what) {
		ddrsncde01.set(what);
	}	
	public FixedLengthStringData getDdrsncde02() {
		return ddrsncde02;
	}
	public void setDdrsncde02(Object what) {
		ddrsncde02.set(what);
	}	
	public FixedLengthStringData getDdrsncde03() {
		return ddrsncde03;
	}
	public void setDdrsncde03(Object what) {
		ddrsncde03.set(what);
	}	
	public FixedLengthStringData getCrcind() {
		return crcind;
	}
	public void setCrcind(Object what) {
		crcind.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	//ILIFE-2472-START
	public FixedLengthStringData getPayind() {
		return payind;
	}
	public void setPayind(Object what) {
		payind.set(what);
	}
	
	//ILIFE-2472-END
	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getDdrsncdes() {
		return new FixedLengthStringData(ddrsncde01.toInternal()
										+ ddrsncde02.toInternal()
										+ ddrsncde03.toInternal());
	}
	public void setDdrsncdes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDdrsncdes().getLength()).init(obj);
	
		what = ExternalData.chop(what, ddrsncde01);
		what = ExternalData.chop(what, ddrsncde02);
		what = ExternalData.chop(what, ddrsncde03);
	}
	public FixedLengthStringData getDdrsncde(BaseData indx) {
		return getDdrsncde(indx.toInt());
	}
	public FixedLengthStringData getDdrsncde(int indx) {

		switch (indx) {
			case 1 : return ddrsncde01;
			case 2 : return ddrsncde02;
			case 3 : return ddrsncde03;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDdrsncde(BaseData indx, Object what) {
		setDdrsncde(indx.toInt(), what);
	}
	public void setDdrsncde(int indx, Object what) {

		switch (indx) {
			case 1 : setDdrsncde01(what);
					 break;
			case 2 : setDdrsncde02(what);
					 break;
			case 3 : setDdrsncde03(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getDatesubs() {
		return new FixedLengthStringData(datesub01.toInternal()
										+ datesub02.toInternal()
										+ datesub03.toInternal());
	}
	public void setDatesubs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDatesubs().getLength()).init(obj);
	
		what = ExternalData.chop(what, datesub01);
		what = ExternalData.chop(what, datesub02);
		what = ExternalData.chop(what, datesub03);
	}
	public PackedDecimalData getDatesub(BaseData indx) {
		return getDatesub(indx.toInt());
	}
	public PackedDecimalData getDatesub(int indx) {

		switch (indx) {
			case 1 : return datesub01;
			case 2 : return datesub02;
			case 3 : return datesub03;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDatesub(BaseData indx, Object what) {
		setDatesub(indx, what, false);
	}
	public void setDatesub(BaseData indx, Object what, boolean rounded) {
		setDatesub(indx.toInt(), what, rounded);
	}
	public void setDatesub(int indx, Object what) {
		setDatesub(indx, what, false);
	}
	public void setDatesub(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setDatesub01(what, rounded);
					 break;
			case 2 : setDatesub02(what, rounded);
					 break;
			case 3 : setDatesub03(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getDaterjs() {
		return new FixedLengthStringData(daterj01.toInternal()
										+ daterj02.toInternal()
										+ daterj03.toInternal());
	}
	public void setDaterjs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDaterjs().getLength()).init(obj);
	
		what = ExternalData.chop(what, daterj01);
		what = ExternalData.chop(what, daterj02);
		what = ExternalData.chop(what, daterj03);
	}
	public PackedDecimalData getDaterj(BaseData indx) {
		return getDaterj(indx.toInt());
	}
	public PackedDecimalData getDaterj(int indx) {

		switch (indx) {
			case 1 : return daterj01;
			case 2 : return daterj02;
			case 3 : return daterj03;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDaterj(BaseData indx, Object what) {
		setDaterj(indx, what, false);
	}
	public void setDaterj(BaseData indx, Object what, boolean rounded) {
		setDaterj(indx.toInt(), what, rounded);
	}
	public void setDaterj(int indx, Object what) {
		setDaterj(indx, what, false);
	}
	public void setDaterj(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setDaterj01(what, rounded);
					 break;
			case 2 : setDaterj02(what, rounded);
					 break;
			case 3 : setDaterj03(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		payrcoy.clear();
		payrnum.clear();
		mandref.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		validflag.clear();
		bankkey.clear();
		bankacckey.clear();
		timesUse.clear();
		mandstat.clear();
		statChangeDate.clear();
		lastUseDate.clear();
		lastUseAmt.clear();
		luStatCode.clear();
		mandAmt.clear();
		effdate.clear();
		detlsumm.clear();
		datesub01.clear();
		datesub02.clear();
		datesub03.clear();
		daterj01.clear();
		daterj02.clear();
		daterj03.clear();
		ddrsncde01.clear();
		ddrsncde02.clear();
		ddrsncde03.clear();
		crcind.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		payind.clear(); //ILIFE-2472
	}


}