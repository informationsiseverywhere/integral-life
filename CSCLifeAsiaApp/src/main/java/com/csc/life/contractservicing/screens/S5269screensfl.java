package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5269screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 16, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5269ScreenVars sv = (S5269ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5269screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5269screensfl, 
			sv.S5269screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5269ScreenVars sv = (S5269ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5269screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5269ScreenVars sv = (S5269ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5269screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5269screensflWritten.gt(0))
		{
			sv.s5269screensfl.setCurrentIndex(0);
			sv.S5269screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5269ScreenVars sv = (S5269ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5269screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5269ScreenVars screenVars = (S5269ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.crtable.setFieldName("crtable");
				screenVars.shortds.setFieldName("shortds");
				screenVars.liencd.setFieldName("liencd");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.shortds.set(dm.getField("shortds"));
			screenVars.liencd.set(dm.getField("liencd"));
			screenVars.cnstcur.set(dm.getField("cnstcur"));
			screenVars.estMatValue.set(dm.getField("estMatValue"));
			screenVars.actvalue.set(dm.getField("actvalue"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5269ScreenVars screenVars = (S5269ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.crtable.setFieldName("crtable");
				screenVars.shortds.setFieldName("shortds");
				screenVars.liencd.setFieldName("liencd");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("shortds").set(screenVars.shortds);
			dm.getField("liencd").set(screenVars.liencd);
			dm.getField("cnstcur").set(screenVars.cnstcur);
			dm.getField("estMatValue").set(screenVars.estMatValue);
			dm.getField("actvalue").set(screenVars.actvalue);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5269screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5269ScreenVars screenVars = (S5269ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.shortds.clearFormatting();
		screenVars.liencd.clearFormatting();
		screenVars.cnstcur.clearFormatting();
		screenVars.estMatValue.clearFormatting();
		screenVars.actvalue.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5269ScreenVars screenVars = (S5269ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.cnstcur.setClassString("");
		screenVars.estMatValue.setClassString("");
		screenVars.actvalue.setClassString("");
	}

/**
 * Clear all the variables in S5269screensfl
 */
	public static void clear(VarModel pv) {
		S5269ScreenVars screenVars = (S5269ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.crtable.clear();
		screenVars.shortds.clear();
		screenVars.liencd.clear();
		screenVars.cnstcur.clear();
		screenVars.estMatValue.clear();
		screenVars.actvalue.clear();
	}
}
