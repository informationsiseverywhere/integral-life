package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.contractservicing.dataaccess.model.Cocontpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CocontpfDAO extends BaseDAO<Cocontpf> {
	public Cocontpf checkUnProcessed(String chdrcoy,String chdrnum);
	public boolean updateProcessedflag(String chdrcoy,String chdrnum, String procflag);
	public Map<String, Cocontpf> fetchAllCocont(String chdrcoy,String chdrnumFrm, String chdrnumTo);

}
