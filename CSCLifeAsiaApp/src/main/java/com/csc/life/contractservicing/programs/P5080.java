/*
 * File: P5080.java
 * Date: 30 August 2009 0:04:31
 * Author: Quipoz Limited
 *
 * Class transformed from P5080.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.S5080ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.ResnpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Resnpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.dao.impl.ItemDAOImpl;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*                    DESPATCH ADDRESS
*
* This program allows creation or modification of despatch Addr.
* of a Contract . The Contract Header record file is updated.
*
* A "PTRN" record will be written.
*
* Initialise
* ----------
*
*
* The details of the contract being worked on will be stored in
* the CHDRMNA I/O module.  Retrieve the details.
* In order to format  the  required  names  the  client details
* record is read and  the  relevant copybook for formatting the
* names must be included in the program.
*
* Read  the  agent  details (AGNTLNB) and format the name using
* the copybooks.
*
*
* Validation
* ----------
*
* If KILL is requested, then skip validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Despatch validation.
*
*      - if the contract header despatch is not equal to spaces
*           and the despatch number (ex screen) is spaces, then
*           exit from the validation.
*
*       - if the  contract  header  despatch is equal to spaces,
*           then a despatch number  may be entered, if not then
*           the owner  (contract  header)  assumes the despatch
*           number.
*
*      - if a  despatch number is entered, then read the client
*           details  and  format the name and return the output
*           to the screen.
*
*      - if CALC is pressed, then redisplay.
*
*Updating
*--------
*
* Updating occurs  with  the  updating  of  the contract header
* record, the deletion of a client role record, the addition of
* a new client role and the creation of a PTRN record.
*
* If the 'KILL' function key was pressed, skip the updating.
*
* CLIENT ROLE UPDATE (use 'DA' as the client role)
*
*       - if the   despatch  number  entered  is  equal  to  the
*           despatch  number  on  the  contract header, then no
*           change occurs and exit from the update procedure.
*
*      - if the despatch  number  is  equal to spaces,
*           then delete the  client role that exists and add an
*           new client role using the owner number.
*
* CONTRACT HEADER UPDATE
*
*      - if there were  no  changes,  then  do  not  update the
*           contract header record.
*
*      - if the screen entered  despatch  number is spaces then
*           move  the  owner  number  to  the  contract  header
*           despatch  prefix,  company  and  number  otherwise,
*           update the  despatch prefix with the client prefix,
*           the despatch  company  with  the WSSP-FSUCO and the
*           despatch  number   with  the  despatch  number  (ex
*           screen).
*
*      - rewrite (WRITS) the contract header record.
*
* GENERAL HOUSEKEEPING
*
*       - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           SPACES and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      - Release contract SFTLOCK.
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5080 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5080");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaOrigReasoncd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaOrigResndesc = new FixedLengthStringData(50);
	private ZonedDecimalData wsaaLastTranno  = new ZonedDecimalData(5, 0); //TICKET# ILIFE-1183
	private FixedLengthStringData wsaaDespnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private FixedLengthStringData wsaaOkeyBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaLetokeys, 1);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 5).setUnsigned();

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
		/* ERRORS */
	private static final String e186 = "E186";
	private static final String e304 = "E304";
	private static final String e655 = "E655";
	private static final String f782 = "F782";

	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	protected ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Wssplife wssplife = new Wssplife();
	protected S5080ScreenVars sv = ScreenProgram.getScreenVars( S5080ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAOImpl.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAOImpl.class);
	private ClntpfDAO cltsDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ResnpfDAO resnpfDAO = getApplicationContext().getBean("resnpfDAO", ResnpfDAO.class);
	private Map<String, List<Itempf>> tr384ListMap;
	private Map<String,Descpf> t3623Map = null;
	private Map<String,Descpf> t5688Map = null;
	private Map<String,Descpf> t3588Map = null;
	private Map<String,Descpf> t5500Map = null;


	public P5080() {
		super();
		screenVars = sv;
		new ScreenModel("S5080", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		loadSmartTable();	
		initialise1010();
		blankOutFields1020();
	}
private void loadSmartTable(){
	String itemcoy = wsspcomn.company.toString();
	tr384ListMap = itemDAO.loadSmartTable("IT", itemcoy, "TR384");

	t3623Map = descDAO.getItems("IT", wsspcomn.company.toString(), "T3623",wsspcomn.language.toString());
	t5688Map = descDAO.getItems("IT", wsspcomn.company.toString(), "T5688",wsspcomn.language.toString());
	t3588Map = descDAO.getItems("IT", wsspcomn.company.toString(), "T3588",wsspcomn.language.toString());
	t5500Map = descDAO.getItems("IT", wsspcomn.company.toString(), "T5500",wsspcomn.language.toString());

	
}
protected void initialise1010()
	{
		wsaaBatcKey.set(wsspcomn.batchkey);
		/*    IF  WSAA-TODAY NOT = ZEROS                                  <*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		/* Retrieve contract fields from I/O module*/
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		wsaaLastTranno.set(chdrmnaIO.getTranno()); //TICKET# ILIFE-1183
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());

		if(t5688Map != null){
			if(t5688Map.get(chdrmnaIO.getCnttype().toString().trim()) != null){
				sv.ctypdesc.set(t5688Map.get(chdrmnaIO.getCnttype().toString().trim()).getLongdesc());
			}else{
				sv.ctypdesc.set("?");
			}
		}else{
			sv.ctypdesc.set("?");
		}
		/*  Load all fields from the Contract Header to the Screen*/
		sv.cownnum.set(chdrmnaIO.getCownnum());
		/*  Look up premium status*/
		if(t3588Map != null){
			if(t3588Map.get(chdrmnaIO.getPstatcode().toString().trim()) != null){
				sv.pstate.set(t3588Map.get(chdrmnaIO.getPstatcode().toString().trim()).getShortdesc());
			}else{
				sv.pstate.set("?");
			}
		}else{
			sv.pstate.set("?");
		}
		if(t3623Map != null){
			if(t3623Map.get(chdrmnaIO.getStatcode().toString().trim()) != null){
				sv.rstate.set(t3623Map.get(chdrmnaIO.getStatcode().toString().trim()).getShortdesc());
			}else{
				sv.rstate.set("?");
			}
		}else{
			sv.rstate.set("?");
		}
		
		/*   Look up all other descriptions*/
		/*       - owner name*/
		/*       - payor name*/
		/*       - agent name*/
		if (isNE(sv.cownnum, SPACES)) {
			formatClientName1700();
		}
		/*    Load any existing despatch details*/
		/*    Retrieve client details using DESPNUM if blank then*/
		/*    the Address is left blank.*/
		
		if (isEQ(chdrmnaIO.getDespnum(), SPACES)) {
			sv.despname.set(SPACES);
			return ;
		}
		Clntpf clts = cltsDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),chdrmnaIO.getDespnum().toString());
		retrieve1550(clts);
		if (isEQ(chdrmnaIO.getDespnum(), SPACES)) {
			sv.despsel.set(SPACES);
		}
		else {
			sv.despsel.set(clts.getClntnum());
			wsaaDespnum.set(sv.despsel); //TICKET# ILIFE-1183
		}
		formatResnDetails5000();
	}

	/**
	* <pre>
	*      RETRIEVE CLIENT DETAILS AND SET SCREEN
	* </pre>
	*/
protected void retrieve1550(Clntpf clts)
	{
		sv.despname.set(SPACES);
		if (clts == null) {
			sv.despselErr.set(e655);
			return ;
		}
		plainname(clts);
		sv.despname.set(wsspcomn.longconfname);
		/*    Check Death Date < Orig Comm Date                            */
		if (isNE(clts.getCltdod(), varcom.vrcmMaxDate)
		&& isGT(chdrmnaIO.getOccdate(), clts.getCltdod())) {
			sv.despselErr.set(f782);
		}
		
	}

protected void formatClientName1700()
	{
		Clntpf clts = cltsDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),sv.cownnum.toString());
		if (clts == null
		|| isNE(clts.getValidflag(), 1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname(clts);
			sv.ownername.set(wsspcomn.longconfname);
		}
	}


protected void plainname(Clntpf clts)
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname(clts);
			return ;
		}
		if (isNE(clts.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}



protected void corpname(Clntpf clts)
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000() {
		screenIo2010();
		reascd2050();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5080IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5080-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(chdrmnaIO.getDespnum(), SPACES)) {
			if (isEQ(sv.despsel, SPACES)) {
				/*         GO TO 2090-EXIT.                                     */
				sv.despsel.set(chdrmnaIO.getCownnum());
			}
		}
		if (isEQ(sv.despsel, SPACES)) {
			sv.despselErr.set(e186);
			//goTo(GotoLabel.checkForErrors2080);
			checkForErrors2080();
			return;
		}
		//cltsIO.setDataArea(SPACES);
		if (isEQ(sv.despsel, SPACES)) {
			sv.despname.set(SPACES);
		//	goTo(GotoLabel.reascd2050);
			reascd2050();
			return;
		}
		Clntpf clts = cltsDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),sv.despsel.toString().trim());//ILIFE-6316
		retrieve1550(clts);
	}

protected void reascd2050()
	{
		/* If the reason code is equal to spaces, skip the read of      */
		/* T5500.                                                       */
		if (isEQ(sv.reasoncd, SPACES)) {
			//goTo(GotoLabel.checkForErrors2080);
			checkForErrors2080();
		}
		if(t5500Map != null){
			if(t5500Map.get(sv.reasoncd.toString().trim()) != null &&  isEQ(sv.resndesc, SPACES)){
				sv.resndesc.set(t5500Map.get(sv.reasoncd.toString().trim()).getLongdesc());
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
	protected void update3000() {
		//ILIFE-7360 Start
		/*   No updating if no changes made.*/
		if (isEQ(wsspcomn.fsuco, chdrmnaIO.getDespcoy())
		&& isEQ(sv.despsel, chdrmnaIO.getDespnum())
		&& isEQ(sv.reasoncd, wsaaOrigReasoncd)
		&& isEQ(sv.resndesc, wsaaOrigResndesc)) {
		//	goTo(GotoLabel.relsfl3070);
			relsfl3070();
			return;
		}
		//ILIFE-7360 End
		updateDatabase3010();
		addRole3020();
		keepAndWritsChdr3030();
		writeReasncd3050();
		callBldenrl3060();
		relsfl3070();
	}

protected void updateDatabase3010()
	{
		//ILIFE-7360 Start
		/*   No updating if no changes made.*/
/*		if (isEQ(wsspcomn.fsuco, chdrmnaIO.getDespcoy())
		&& isEQ(sv.despsel, chdrmnaIO.getDespnum())
		&& isEQ(sv.reasoncd, wsaaOrigReasoncd)
		&& isEQ(sv.resndesc, wsaaOrigResndesc)) {
		//	goTo(GotoLabel.relsfl3070);
			relsfl3070();
			return;
		}*/
		//ILIFE-7360 End
		clrrIO.setParams(SPACES);
		/*   Read Client Role record prior to delete*/
		if (isEQ(chdrmnaIO.getDespnum(), SPACES)) {
			clrrIO.setClntnum(chdrmnaIO.getCownnum());
			clrrIO.setClntpfx(chdrmnaIO.getCownpfx());
			clrrIO.setClntcoy(chdrmnaIO.getCowncoy());
		}
		else {
			clrrIO.setClntpfx(chdrmnaIO.getDesppfx());
			clrrIO.setClntcoy(chdrmnaIO.getDespcoy());
			clrrIO.setClntnum(chdrmnaIO.getDespnum());
		}
		clrrIO.setFunction(varcom.readh);
		clrrIO.setClrrrole("DA");
		clrrIO.setForepfx("CH");
		clrrIO.setForecoy(chdrmnaIO.getChdrcoy());
		clrrIO.setForenum(chdrmnaIO.getChdrnum());
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
		/*   DELETE PREVIOUS DESP. FROM THE CLIENT ROLE FILE*/
		clrrIO.setParams(SPACES);
		if (isEQ(chdrmnaIO.getDespnum(), SPACES)) {
			clrrIO.setClntnum(chdrmnaIO.getCownnum());
			clrrIO.setClntpfx(chdrmnaIO.getCownpfx());
			clrrIO.setClntcoy(chdrmnaIO.getCowncoy());
		}
		else {
			clrrIO.setClntpfx(chdrmnaIO.getDesppfx());
			clrrIO.setClntcoy(chdrmnaIO.getDespcoy());
			clrrIO.setClntnum(chdrmnaIO.getDespnum());
		}
		clrrIO.setClrrrole("DA");
		clrrIO.setForepfx("CH");
		clrrIO.setForecoy(chdrmnaIO.getChdrcoy());
		clrrIO.setForenum(chdrmnaIO.getChdrnum());
		clrrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
	}

protected void addRole3020()
	{
		clrrIO.setParams(SPACES);
		if (isEQ(sv.despsel, SPACES)) {
			/*     Use contract owner*/
			clrrIO.setClntnum(chdrmnaIO.getCownnum());
			clrrIO.setClntpfx(chdrmnaIO.getCownpfx());
			clrrIO.setClntcoy(chdrmnaIO.getCowncoy());
		}
		else {
			/*     Otherwise use entered desp. no.*/
			/*        MOVE 'AG'                   TO CLRR-CLNTPFX              */
			clrrIO.setClntpfx("CN");
			clrrIO.setClntnum(sv.despsel);
			clrrIO.setClntcoy(wsspcomn.fsuco);
		}
		clrrIO.setFormat(formatsInner.clrrrec);
		clrrIO.setFunction(varcom.writr);
		clrrIO.setClrrrole("DA");
		clrrIO.setForepfx("CH");
		clrrIO.setForecoy(chdrmnaIO.getChdrcoy());
		clrrIO.setForenum(chdrmnaIO.getChdrnum());
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)
		&& isNE(clrrIO.getStatuz(), varcom.dupr)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
	}

protected void keepAndWritsChdr3030()
	{
		/* IF S5080-DESPSEL            = SPACES                         */
		/*     MOVE CHDRMNA-COWNNUM    TO CHDRMNA-DESPNUM               */
		/* ELSE                                                         */
		/*        MOVE 'AG'               TO CHDRMNA-DESPPFX               */
		/*     MOVE 'CN'               TO CHDRMNA-DESPPFX       <P002>  */
		/*     MOVE WSSP-FSUCO         TO CHDRMNA-DESPCOY               */
		/*     MOVE S5080-DESPSEL      TO CHDRMNA-DESPNUM.              */
		/* MOVE WSSP-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO VRCM-COMP-TERMID.             */
		/* MOVE VRCM-TRANID-N          TO VRCM-COMP-TRANID-N.           */
		/* MOVE VRCM-COMP-TRANID       TO CHDRMNA-TRANID.               */
		/*  Update contract header fields as follows*/
		/* MOVE KEEPS                  TO CHDRMNA-FUNCTION.             */
		/* MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.               */
		/* CALL 'CHDRMNAIO' USING CHDRMNA-PARAMS.                       */
		/* IF CHDRMNA-STATUZ           NOT = O-K                        */
		/*    MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* MOVE WRITS                  TO CHDRMNA-FUNCTION.             */
		/* MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.               */
		/* CALL 'CHDRMNAIO' USING CHDRMNA-PARAMS.                       */
		/* IF CHDRMNA-STATUZ           NOT = O-K                        */
		/*    MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* Instead of overwrite CHDR record, update this record with       */
		/* validflag 2 and write new record with new information.          */
		/*    MOVE RLSE                   TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/*    MOVE READH                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/*    IF  CHDRMNA-DESPNUM         = SPACES                 <LA4261>*/
		/*        MOVE WSAA-DESPNUM       TO CHDRMNA-DESPNUM       <LA4261>*/
		/*        MOVE CHDRMNA-COWNPFX    TO CHDRMNA-DESPPFX       <LA4261>*/
		/*        MOVE CHDRMNA-COWNCOY    TO CHDRMNA-DESPCOY       <LA4261>*/
		/*    END-IF.                                              <LA4261>*/
		/*    MOVE CHDRMNA-TRANNO         TO WSAA-LAST-TRANNO.     <LA4261>*/
		/*    MOVE '2'                    TO CHDRMNA-VALIDFLAG.    <LA4261>*/
		/*    MOVE REWRT                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/* Write new record.                                               */
		/*    MOVE 'CN'                   TO CHDRMNA-DESPPFX.      <LA4261>*/
		/*    MOVE WSSP-FSUCO             TO CHDRMNA-DESPCOY.      <LA4261>*/
		/*    IF S5080-DESPSEL            = SPACES                 <LA4261>*/
		/*        MOVE CHDRMNA-COWNNUM    TO CHDRMNA-DESPNUM       <LA4261>*/
		/*    ELSE                                                 <LA4261>*/
		/*        MOVE S5080-DESPSEL      TO CHDRMNA-DESPNUM.      <LA4261>*/
		/*    MOVE '1'                    TO CHDRMNA-VALIDFLAG.    <LA4261>*/
		/*    ADD   1                     TO CHDRMNA-TRANNO.       <LA4261>*/
		/*    MOVE WSSP-TRANID            TO VRCM-TRANID.          <LA4261>*/
		/*    MOVE VRCM-TERMID            TO VRCM-COMP-TERMID.     <LA4261>*/
		/*    MOVE VRCM-TRANID-N          TO VRCM-COMP-TRANID-N.   <LA4261>*/
		/*    MOVE VRCM-COMP-TRANID       TO CHDRMNA-TRANID.       <LA4261>*/
		/*    MOVE WRITR                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/* Instead of creating new record, only update the existing        */
		/* CHDR record.                                                    */
		if (isEQ(sv.despsel, SPACES)) {
			chdrmnaIO.setDespnum(chdrmnaIO.getCownnum());
		}
		else {
			chdrmnaIO.setDesppfx("AG");
			chdrmnaIO.setDesppfx("CN");
			chdrmnaIO.setDespcoy(wsspcomn.fsuco);
			chdrmnaIO.setDespnum(sv.despsel);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		/*  Update contract header fields as follows                       */
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
			//		TICKET# ILIFE-1183 STARTS
			chdrmnaIO.setFunction(varcom.rlse);
			chdrmnaIO.setFormat(formatsInner.chdrmnarec);
			SmartFileCode.execute(appVars, chdrmnaIO);
			if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmnaIO.getParams());
				fatalError600();
			}
			wsaaLastTranno.set(chdrmnaIO.tranno);
			compute(chdrmnaIO.tranno, 0).set(sub(chdrmnaIO.tranno,1));
			chdrmnaIO.validflag.set("2");
			chdrmnaIO.despnum.set(wsaaDespnum);
			chdrmnaIO.setFunction(varcom.rewrt);
			chdrmnaIO.setFormat(formatsInner.chdrmnarec);
			SmartFileCode.execute(appVars, chdrmnaIO);
			if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmnaIO.getParams());
				fatalError600();
			}
			chdrmnaIO.desppfx.set("CN");
			chdrmnaIO.despcoy.set(wsspcomn.fsuco);
		    if(isEQ(sv.despsel,SPACES))
		    {
			chdrmnaIO.despnum.set(chdrmnaIO.cownnum);
			}
		    else
		    {
			chdrmnaIO.despnum.set(sv.despsel);
			chdrmnaIO.validflag.set("1");
			chdrmnaIO.tranno.add(1);//ILIFE-7777
			varcom.vrcmTranid.set(wsspcomn.tranid);
			varcom.vrcmCompTermid.set(varcom.vrcmTermid);
			varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
			chdrmnaIO.setTranid(varcom.vrcmCompTranid);
			chdrmnaIO.setFunction(varcom.writr);
			chdrmnaIO.setFormat(formatsInner.chdrmnarec);
			SmartFileCode.execute(appVars, chdrmnaIO);
			if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmnaIO.getParams());
				fatalError600();
			}
		}
			
			//ILIFE-1183 ends
		writeLetter6000();
		/* Write a PTRN record.*/
		wsaaBatcKey.set(wsspcomn.batchkey);
		Ptrnpf ptrnIO = new Ptrnpf(); 
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum().toString());
		ptrnIO.setTranno(wsaaLastTranno.toInt()); // ILIFE-6701
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		ptrnIO.setTermid(varcom.vrcmTermid.toString());
		ptrnIO.setUserT(varcom.vrcmUser.toInt());
		ptrnIO.setPtrneff(wsaaToday.toInt());
		ptrnIO.setDatesub(wsaaToday.toInt());
		ptrnIO.setBatcpfx(wsaaBatcKey.batcBatcpfx.toString());
		ptrnIO.setBatccoy(wsaaBatcKey.batcBatccoy.toString());
		ptrnIO.setBatcbrn(wsaaBatcKey.batcBatcbrn.toString());
		ptrnIO.setBatcactyr(wsaaBatcKey.batcBatcactyr.toInt());
		ptrnIO.setBatctrcde(wsaaBatcKey.batcBatctrcde.toString());
		ptrnIO.setBatcactmn(wsaaBatcKey.batcBatcactmn.toInt());
		ptrnIO.setBatcbatch(wsaaBatcKey.batcBatcbatch.toString());
		ptrnIO.setValidflag("");
		ptrnIO.setRecode("");
		ptrnIO.setPrtflg("");
		ptrnIO.setCrtuser("");
		List<Ptrnpf> ptrnList = new ArrayList<Ptrnpf>();
		ptrnList.add(ptrnIO);
		boolean result = ptrnpfDAO.insertPtrnPF(ptrnList);
		ptrnList.clear();
		if (!result) {
			syserrrec.params.set(ptrnIO.getChdrcoy().concat(ptrnIO.getChdrnum()));
			fatalError600();
		}
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(SPACES);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
	}

protected void writeReasncd3050()
	{
		/* If the change originally had a reason record, delete it      */
		/* prior to writing a new reason record (if a new one is        */
		/* necessary).  This will ensure that reason records are        */
		/* deleted when the reason fields are blanked out and updated   */
		/* when a reason field or the despatch address is changed.      */
		if (isNE(wsaaOrigReasoncd, SPACES)
		|| isNE(wsaaOrigResndesc, SPACES)) {
			deleteReason3100();
		}
		/* Create a RESN record if reason code or narrative was entered*/
		if (isNE(sv.resndesc, SPACES)
		|| isNE(sv.reasoncd, SPACES)) {
			Resnpf resnIO = new Resnpf();
			resnIO.setChdrcoy(chdrmnaIO.getChdrcoy().toString().trim());
			resnIO.setChdrnum(chdrmnaIO.getChdrnum().toString().trim());
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde.toString().trim());
			resnIO.setTranno(chdrmnaIO.getTranno().toInt());
			resnIO.setReasoncd(sv.reasoncd.toString().trim());
			resnIO.setResndesc(sv.resndesc.toString().trim());
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde.toString().trim());
			resnIO.setTrdt(varcom.vrcmDate.toInt());
			resnIO.setTrtm(varcom.vrcmTime.toInt());
			resnIO.setUserT(varcom.vrcmUser.toInt());
			resnIO.setTermid("");
			resnpfDAO.insertResnpf(resnIO);
		}
	}

protected void callBldenrl3060()
	{
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrmnaIO.getChdrpfx());
		bldenrlrec.company.set(chdrmnaIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrmnaIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
	}

protected void relsfl3070()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		/*    MOVE WSAA-BATC-BATCTRCDE    TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected boolean deleteReason3100() {
		Resnpf resnIO = new Resnpf();
		resnIO.setChdrcoy(chdrmnaIO.getChdrcoy().toString().trim());
		resnIO.setChdrnum(chdrmnaIO.getChdrnum().toString().trim());
		resnIO.setTrancde(wsaaBatcKey.batcBatctrcde.toString().trim());
		boolean delFlag = resnpfDAO.deleteResnpf(resnIO);
		if (!delFlag) {
			syserrrec.params.set(chdrmnaIO.getChdrcoy().concat(chdrmnaIO.getChdrnum()));
			fatalError600();
		}
		return delFlag;
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatResnDetails5000()
	{
		start5010();
	}

	/**
	* <pre>
	*    This section formats the latest reason detailes on screen *
	*    if reason record  present.                                 *
	* </pre>
	*/
protected void start5010()
	{
		Resnpf resnpf = resnpfDAO.readResnpf(chdrmnaIO.getChdrcoy().toString().trim(), chdrmnaIO.getChdrnum().toString().trim(), 
				wsaaBatcKey.batcBatctrcde.toString().trim());
		if (resnpf == null) {
			sv.reasoncd.set(SPACES);
			wsaaOrigReasoncd.set(SPACES);
			sv.resndesc.set(SPACES);
			wsaaOrigResndesc.set(SPACES);
		}
		else {
			sv.reasoncd.set(resnpf.getReasoncd());
			wsaaOrigReasoncd.set(resnpf.getReasoncd());
			wsaaOrigResndesc.set(resnpf.getResndesc());
			sv.resndesc.set(resnpf.getResndesc());
		}
	}

	protected void writeLetter6000() {
		/* Read Table T6634 for get letter-type.                           */
		wsaaItemBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
		readTr3846020();
		return;
	}

	/**
	* <pre>
	*6020-READ-T6634.                                         <PCPPRT>
	* </pre>
	*/

protected void readTr3846020()
	{
		boolean itemFound = false;
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (tr384ListMap.containsKey(wsaaItemTr384.toString().trim())) {
			itempfList = tr384ListMap.get(wsaaItemTr384.toString().trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
			}
		}
		if(!itemFound){
			if (isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				/*     GO TO 6020-READ-T6634                            <PCPPRT>*/
			//	goTo(GotoLabel.readTr3846020);
				readTr3846020();
				return;
			}else{
				syserrrec.params.set("TR384");
				fatalError600();
			}
		}
		/* IF T6634-LETTER-TYPE        = SPACES                 <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*   Get Endorsement no.                                       */
		/* MOVE 'NEXT '                TO ALNO-FUNCTION.                */
		/* MOVE 'EN'                   TO ALNO-PREFIX.                  */
		/* MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.                  */
		/* MOVE WSSP-COMPANY           TO ALNO-COMPANY.                 */
		/* CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                         */
		/* IF ALNO-STATUZ              NOT = O-K                        */
		/*    MOVE ALNO-STATUZ         TO SYSR-STATUZ                   */
		/*    MOVE 'ALOCNO'            TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/*   Get set-up parameter for call 'HLETRQS'                   */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		/*   MOVE 'EN'                   TO LETRQST-RDOCPFX.              */
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		/*   MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.              */
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaOkeyTranno.set(wsaaLastTranno);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.branch.set(wsspcomn.branch);
		letrqstrec.servunit.set(wsspcomn.servunit);
		letrqstrec.trcde.set(wsaaBatcKey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		/*  CALL 'HLETRQS' USING LETRQST-PARAMS.                         */
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData clrrrec = new FixedLengthStringData(10).init("CLRRREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData resnrec = new FixedLengthStringData(10).init("RESNREC");
	private FixedLengthStringData chdrmnarec = new FixedLengthStringData(10).init("CHDRMNAREC");
	private FixedLengthStringData resnenqrec = new FixedLengthStringData(10).init("RESNENQREC");
}
}
