/*
 * File: P5129.java
 * Date: 30 August 2009 0:11:23
 * Author: Quipoz Limited
 * 
 * Class transformed from P5129.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

//ILIFE-8096 start
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//ILIFE-8096 end
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //ILIFE-8096
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.screens.S5129ScreenVars;
import com.csc.life.contractservicing.tablestructures.Td5izrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
//ILIFE-8096 start
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
//ILIFE-8096 end
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Td5gfrec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.SftlockUtil; //ILIFE-8096
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.SftlockRecBean; //ILIFE-8096
import com.csc.smart.recordstructures.Subprogrec;
//ILIFE-8096 start
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf; //ILIFE-8096
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf; //ILIFE-8096
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil; //ILIFE-8096
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
 //ILIFE-8096 end
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*               P5129 - Component CHANGES Submenu.
*               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Initialise
*----------
*
*  Initialise the screen for display.
*
*
*Validation
*----------
*
*  Validate contract number entered by performing a READR on the
*  Logical file CHDRMJA.
*
*  Read T5729 to check whether contract is flexible.
*
*  Validate Key - 1
*
*       If S5129-CHDRSEL not  equal  to  spaces call the CHDRMJA
*       I/O module for  the  contract.  If  any  combination  of
*       CHDRSEL and SUBP-KEY1 other than O-K and 'Y' then error.
*
*       Release the COVRMJA I/O module in case it has previously
*       been held.
*
*       Check STATCODE to validate  status of the contract is In
*       Force (IF), against T5679.
*
*       If NOT valid then Error.
*
*  Validate Key - 2
*
*       If the screen  effective  date has changed then validate
*       this date against the header record (CHDRMJA).
*
*Updating
*--------
*
*
*  At this point  we  must  distinguish  Journals from any other
*  Submenu as Journals  will  share  the following common select
*  programs. The reason  being, Selection accross the whole plan
*  (select option =  '0000')  is  only valid for Journals if the
*  number summarised equals  the  number  of policies within the
*  Plan. Fund Switching  however,  can select accross broken out
*  policies within a Plan.
*
*       If SCRN-ACTION = 'I' move 'J' to WSSP-FLAG
*       Else move 'N' to WSSP-FLAG.
*
*
*  If  WSSP-FLAG  is NOT = 'J' then softlock the contract header
*  by  calling SFTLOCK. If the record is already locked - Statuz
*  =  'LOCK'  then  error  with a code of F910 and redisplay the
*  screen.
*
*  If  a valid contract header record found then perform a KEEPS
*  on  the  record  in  order  to store it for use in the select
*  programs.
*
*
*Next Program.
*-------------
*
*  Otherwise add 1 to the Program pointer and exit to the Policy
*  selection screen.
*
*
*****************************************************************
* </pre>
*/
public class P5129 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5129");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaLives = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaStatFlag = new FixedLengthStringData(1);
	private Validator wsaaExitStat = new Validator(wsaaStatFlag, "Y");

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(SPACES);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5729 = "T5729";
	private static final String t5688 = "T5688";
	private static final String td5iz = "TD5IZ";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String lifemjarec = "LIFEMJAREC";

		/*    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
//	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM(); //ILIFE-8096
		/*Contract Header File - Major Alts*/
//	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM(); //ILIFE-8096
		/*Coverage/Rider details - Major Alts*/
//	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM(); //ILIFE-8096
		/*Table items, date - maintenance view*/
//	private ItdmTableDAM itdmIO = new ItdmTableDAM(); //ILIFE-8096
		/*Logical File: SMART table reference data*/
//	private ItemTableDAM itemIO = new ItemTableDAM(); //ILIFE-8096
		/*Life Details File - Major Alts*/
//	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM(); //ILIFE-8096
	private Batckey wsaaBatchkey = new Batckey();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
//	private Sftlockrec sftlockrec = new Sftlockrec(); //ILIFE-8096
	private Subprogrec subprogrec = new Subprogrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Td5izrec td5izrec = new Td5izrec();
	private Td5gfrec td5gfrec = new Td5gfrec();     
	private Datcon3rec datcon3rec = new Datcon3rec(); 
	private Agecalcrec agecalcrec = new Agecalcrec(); 
	private T5687rec t5687rec = new T5687rec();
	private S5129ScreenVars sv = ScreenProgram.getScreenVars( S5129ScreenVars.class);
//	private ErrorsInner errorsInner = new ErrorsInner(); //ILIFE-8096
	
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class); 
	private Itempf itempf = null; 
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	private Hpadpf hpadpf; 
 //ILIFE-8096 start
	private Map<String, List<Itempf>> t5729ListMap = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> t5688ListMap = new HashMap<String, List<Itempf>>();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Lifepf lifepf = null; 


	boolean cscom004Permission = false;
	
	private FixedLengthStringData wsaaFreelookBasisChn = new FixedLengthStringData(1); 
	private Validator issueDateChnBasis = new Validator(wsaaFreelookBasisChn, "1"); 
	private Validator ackBasis = new Validator(wsaaFreelookBasisChn, "2"); 
	private Validator deemedBasis = new Validator(wsaaFreelookBasisChn, "3"); 
	private Validator minAckDeemedBasis = new Validator(wsaaFreelookBasisChn, "4"); 
	private ZonedDecimalData wsaaFreelookDate = new ZonedDecimalData(8, 0).setUnsigned(); 
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned(); 
	private ZonedDecimalData wsaaTdayno = new ZonedDecimalData(3, 0).setUnsigned(); 
	    /* TABLES */
	private static final String td5gf = "TD5GF";  
	
	/* ERRORS */
	private static final String e455 = "E455";
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e544 = "E544";
	private static final String e767 = "E767";
	private static final String f910 = "F910";
	private static final String g436 = "G436";
	private static final String e331 = "E331";
	private static final String e370 = "E370";
	private static final String e268 = "E268";
	private static final String rrj5 = "RRJ5";
	private static final String rrgi = "RRGI";
	private static final String rlcb = "RLCB"; 
	private static final String rlaj = "RLAJ";  
 //ILIFE-8096 end
	private boolean isCompModify= false;
	private static final String rrrl = "RRRL";
	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2590, 
		keeps3070, 
		exit3090
	}

	public P5129() {
		super();
		screenVars = sv;
		new ScreenModel("S5129", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
	    cscom004Permission  = FeaConfg.isFeatureExist("2", "CSCOM004", appVars, "IT"); 
		sv.dataArea.set(SPACES);
		wsaaStatFlag.set("N");
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070); //ILIFE-8096
		}
		wsaaLives.set(ZERO);
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
	 //ILIFE-8096 start
		String coy = wsspcomn.company.toString();
		t5729ListMap = itemDAO.loadSmartTable("IT", coy, "T5729");	
		t5688ListMap = itemDAO.loadSmartTable("IT", coy, "T5688");
	 //ILIFE-8096 end	
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
				if (isEQ(sv.errorIndicators,SPACES)) {
					if(cscom004Permission){
						freelookCheckCHN();
					} 
				} 
			}
			else {
				verifyBatchControl2500();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}
protected void freelookCheckCHN() 	
{
	/* Validate the cooling off period for freelook cancellation       */
	/* based on the rule defined in TD5GF.                             */
	getCoolOff();
	wsaaFreelookBasisChn.set(td5gfrec.ratebas);       
	if (!(issueDateChnBasis.isTrue()
	|| ackBasis.isTrue()
	|| deemedBasis.isTrue()		
	|| minAckDeemedBasis.isTrue())) {
		sv.chdrselErr.set(rlcb);
		return ;
	}
	hpadpf=hpadpfDAO.getHpadData(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());//ILIFE-7968
	/* issue date*/
	if (issueDateChnBasis.isTrue()) {
		wsaaFreelookDate.set(hpadpf.getHissdte());
	}
	
	/* ack */
	if (ackBasis.isTrue()) {
		if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getPackdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getPackdate());
				}
				else {
					sv.chdrselErr.set(rlaj);
					return ;
				}
	}
	
	/* deemed */
	if (deemedBasis.isTrue()) {
		if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getDeemdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getDeemdate());
				}
				else {
					sv.chdrselErr.set(rlaj);
					return ;
				}
	}
	
	/* min- ack or deemed */
	if (minAckDeemedBasis.isTrue()) {
		if (isGT(hpadpf.getDeemdate(), hpadpf.getPackdate())) {
			if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getPackdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getPackdate());
					}
		}
		else {
			if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getDeemdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getDeemdate());
					}
					else {
						sv.chdrselErr.set(rlaj);
						return ;
					}
		}

	}

	datcon3rec.datcon3Rec.set(SPACES);
	datcon3rec.intDate1.set(wsaaFreelookDate);
	datcon3rec.intDate2.set(wsaaToday);
	datcon3rec.frequency.set("DY");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}

	if (isLT(datcon3rec.freqFactor, wsaaTdayno)) {
		sv.chdrselErr.set(rrgi);
	}
}   

protected void getCoolOff() 
{
	lifepf = lifepfDAO.getLifeRecord(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim(), "01", "00");//ILIFE-7968
	if (lifepf== null ) {
		syserrrec.params.set(lifepf);
		fatalError600();
	}
	
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(wsspcomn.language);
	agecalcrec.intDate1.set(lifepf.getCltdob());
	agecalcrec.intDate2.set(wsaaToday);
	agecalcrec.company.set(wsspcomn.fsuco);
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		fatalError600();
	}
	wsaaAnb.set(agecalcrec.agerating);

	readTd5gf1100();

		if (isEQ(lifepf.getCltsex(),td5gfrec.gender01)  && isGTE(wsaaAnb,td5gfrec.frmage01) && isLTE(wsaaAnb,td5gfrec.toage01)) {
			td5gfrec.cooloff.set(td5gfrec.coolingoff1);
			wsaaTdayno.set(td5gfrec.coolingoff);
			
		}
		else {
			if (isEQ(lifepf.getCltsex(),td5gfrec.gender02) && isGTE(wsaaAnb,td5gfrec.frmage02) && isLTE(wsaaAnb,td5gfrec.toage02)) {
				td5gfrec.cooloff.set(td5gfrec.coolingoff2);
				wsaaTdayno.set(td5gfrec.coolingoff);
			}
			else {
				if (isEQ(lifepf.getCltsex(),td5gfrec.gender03) && isGTE(wsaaAnb,td5gfrec.frmage03) && isLTE(wsaaAnb,td5gfrec.toage03)) {
					td5gfrec.cooloff.set(td5gfrec.coolingoff3);
					wsaaTdayno.set(td5gfrec.coolingoff);
				}
				
				else {
					if (isEQ(lifepf.getCltsex(),td5gfrec.gender04) && isGTE(wsaaAnb,td5gfrec.frmage04) && isLTE(wsaaAnb,td5gfrec.toage04)) {
						td5gfrec.cooloff.set(td5gfrec.coolingoff4);
						wsaaTdayno.set(td5gfrec.coolingoff);
					}
				}
						
			}
		}
}


protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
			validateKey12210();
		}

protected void validateKey12210()
	{
	isCompModify = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM010", appVars, "IT");
	if(!sv.chdrsel.toString().trim().equalsIgnoreCase("")){   //ILIFE-8140 starts
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), sv.chdrsel.toString());
		/*ILIFE-4853 STARTS*/
		if (!(chdrpf == null)) {
			chdrpfDAO.setCacheObject(chdrpf);
		} else {
			sv.chdrselErr.set(e544);
			wsspcomn.edterror.set("Y");
			// goTo(GotoLabel.exit2290);
			return;
		}
		/*   ILIFE-4853 ENDS*/
	}                                            //ILIFE-8140 ends
		/*chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(chdrpf.getChdrnum());
		chdrmjaIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrmjaIO);
		}
		else {
			chdrmjaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(errorsInner.e544);
			wsspcomn.edterror.set("Y");
			return ;
		}*/
		//ILIFE-8194 starts
		if((isCompModify) && (isEQ(sv.action,"A") || isEQ(sv.action,"B"))){
		if (isEQ(chdrpf.getBillfreq(), "00")) {
			sv.chdrselErr.set("rrrl");
			wsspcomn.edterror.set("Y");
			return;
		}
		}
		//ILIFE-8194 ends
		wsspcomn.currfrom.set(chdrpf.getPtdate());
 //ILIFE-8096 end
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		//MIBT-101
//		if (isEQ(sv.action,"F")) {
		if (isEQ(sv.action,"D")) {
			checkLives5000();
		}
		readT57292220();
 //ILIFE-8096 start
		covrpfDAO.deleteCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8096 end
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)) {
			checkStatus2300();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)
		&& isNE(wsspcomn.branch,chdrpf.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
		if (isEQ(sv.chdrselErr,SPACES)) {
			if (isNE(chdrpf.getBtdate(),chdrpf.getPtdate())
			&& !flexiblePremium.isTrue()
			&& isNE(scrnparams.action,"C")) {
				sv.chdrselErr.set(g436);
			}
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readTd5gf1100() 
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(td5gf); /* IJTI-1479 */
	itempf.setItemitem(chdrpf.getCnttype() + chdrpf.getSrcebus()); /* IJTI-1479 */
	itempf = itempfDAO.getItempfRecord(itempf);
	
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf); /* IJTI-1479 */
		itempf.setItemitem(chdrpf.getCnttype() + "**"); /* IJTI-1479 */
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf); /* IJTI-1479 */
		itempf.setItemitem("*****");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	if (itempf != null) {
		td5gfrec.td5gfRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else {
		td5gfrec.td5gfRec.set(SPACES);
	}
	
}

protected void readT57292220()
	{
		start2221();
	}

protected void start2221()
	{
		wsaaFlexiblePremium.set("N");
 //ILIFE-8096 start
		String keyItemitem = chdrpf.getCnttype(); /* IJTI-1479 */
		if (t5729ListMap.containsKey(keyItemitem)) {
				wsaaFlexiblePremium.set("Y");
		}
		/*itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5729);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			wsaaFlexiblePremium.set("Y");
		}*/
 //ILIFE-8096 end
	}

protected void checkStatus2300()
	{
		readStatusTable2310();
	}

protected void readStatusTable2310()
	{
	 //ILIFE-8096 start
		List<Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), "T5679", subprogrec.transcd.toString());
		if (null == items || items.isEmpty()) {
			syserrrec.params.set("T5679");
			fatalError600();
		} else {
			t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		}
	/*	itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());*/
 //ILIFE-8096 end
		wsaaSub.set(ZERO);
		wsaaStatFlag.set("N");
		while ( !(wsaaExitStat.isTrue())) {
			lookForStat2400();
		}
		
	}

protected void lookForStat2400()
	{
		/*SEARCH*/
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.chdrselErr.set(e767); //ILIFE-8096
			wsaaStatFlag.set("Y");
		}
		else {
			if (isNE(chdrpf.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaStatFlag.set("N");
			}
			else {
				wsaaStatFlag.set("Y");
			}
		}
		/*EXIT*/
	}

protected void verifyBatchControl2500()
	{
		try {
			validateRequest2510();
			retrieveBatchProgs2520();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2510()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			/*      MOVE E073               TO S5129-ACTION-ERR.             */
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit2590);
		}
	}

protected void retrieveBatchProgs2520()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
					softLock3030();
				case keeps3070: 
					keeps3070();
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}


protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.action,"A")) {
			wsspcomn.flag.set("A");
			checkProcessDate();//ICIL-250
		}
		if (isEQ(scrnparams.action,"B")) {
			wsspcomn.flag.set("M");
			checkProcessDate();//ICIL-250
		}
		if (isEQ(scrnparams.action,"C")) {
			wsspcomn.flag.set("I");
		}
		if (isEQ(scrnparams.action,"D")) {
			wsspcomn.flag.set("A");
		}
		if (isEQ(scrnparams.action,"E")) {
			wsspcomn.flag.set("A");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			batching3080();
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(scrnparams.action,"E")) {
			goTo(GotoLabel.keeps3070);
		}
	}

	/**
	 * @author fwang3
	 * ICIL-250
	 */
	private void checkProcessDate() {
		boolean chinaLocalPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM007", appVars, "IT");
		if(chinaLocalPermission) {
 //ILIFE-8096 start
			List<Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), td5iz,
					chdrpf.getCnttype()); /* IJTI-1479 */
			if (!items.isEmpty()) {
				td5izrec.td5izRec.set(StringUtil.rawToString(items.get(0).getGenarea()));
				if ((isEQ(sv.action, "A") && td5izrec.mthscompadd.toInt() != 0)
						|| (isEQ(sv.action, "B") && td5izrec.mthscompmod.toInt() != 0)) {
					checkCurrentDate();
			/*itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(td5iz);
			itemIO.setItemitem(chdrmjaIO.getCnttype());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK) && isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				td5izrec.td5izRec.set(itemIO.getGenarea());
				if ((isEQ(scrnparams.action, "A") && td5izrec.mthscompadd.toInt() != 0)
						|| (isEQ(scrnparams.action, "B") && td5izrec.mthscompmod.toInt() != 0)) {
					checkCurrentDate();*/
 //ILIFE-8096 end
				}
			}
		}
	}

	/**
	 * @author fwang3
	 * ICIL-250
	 */
	private void checkCurrentDate() {
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		String fromDateStr = "";
		if (isEQ(scrnparams.action, "A")) {
			fromDateStr = DateUtils.dateMinusMonths(chdrpf.getPtdate().toString(), td5izrec.mthscompadd.toInt()); //ILIFE-8096
		} else if (isEQ(scrnparams.action, "B")) {
			fromDateStr = DateUtils.dateMinusMonths(chdrpf.getPtdate().toString(), td5izrec.mthscompmod.toInt()); //ILIFE-8096
		}
		if (!DateUtils.isInDateRange(datcon1rec.intDate.toString(), fromDateStr, chdrpf.getPtdate().toString())) {
			sv.actionErr.set(rrj5);
			wsspcomn.edterror.set("Y");
		}
	}

protected void softLock3030()
	{
 //ILIFE-8096 start
		sftlockRecBean.setFunction("LOCK");
		sftlockRecBean.setCompany(wsspcomn.company.toString());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(chdrpf.getChdrnum()); /* IJTI-1479 */
		sftlockRecBean.setTransaction(subprogrec.transcd.toString());
		sftlockRecBean.setUser(varcom.vrcmUser.toString());
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
			&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
		}
		if (isEQ(sftlockRecBean.getStatuz(),"LOCK")) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		/*sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}*/
 //ILIFE-8096 end
	}

protected void keeps3070()
	{
 //ILIFE-8096 start
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction("KEEPS");
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
		if(chdrpf == null ){
			fatalError600();
		}
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrlnbIO.setDataKey(chdrmjaIO.getDataKey());
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}*/
		/*chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8096 end 
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

protected void checkLives5000()
	{
		begn5010();
	}

protected void begn5010()
	{
 //ILIFE-8096 start		/*itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.e268);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		wsaaLives.set(0);
		lifemjaIO.setStatuz(varcom.oK);
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(lifemjaIO.getStatuz(),varcom.endp))) {
			readLifemja5200();
		}
		
		if (isGTE(wsaaLives,t5688rec.lifemax)) {
			sv.actionErr.set(errorsInner.e370);
			wsspcomn.edterror.set("Y");
		}*/
	
		boolean itemFound = false;
		String keyItemitem = chdrpf.getCnttype(); /* IJTI-1479 */
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5688ListMap.containsKey(keyItemitem)) {
			itempfList = t5688ListMap.get(keyItemitem);   /* ILIFE-3787*/
			if(itempfList.get(0) != null){
				//Itempf itempf = new Itempf();
				Itempf itempf = itempfList.get(0); /* ILIFE-3787*/
				if (isEQ(itempf.getItemseq(), SPACES)) {
					t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}
		}
		if (!itemFound) {
			scrnparams.errorCode.set(e268); //ILB-499
		}
		wsaaLives.set(0);
		List<Lifepf> lifeList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00"); /* IJTI-1479 */
		if(lifeList != null && lifeList.size() > 0){
			wsaaLives.add(lifeList.size());
		}else{
			syserrrec.statuz.set(e331); //ILB-499
			fatalError600();
		}
		if (isGTE(wsaaLives,t5688rec.lifemax)) {
			sv.actionErr.set(e370); //ILB-499
			wsspcomn.edterror.set("Y");
		}
 //ILIFE-8096 end
	}

protected void readLifemja5200()
	{
		/*BEGN*/
 //ILIFE-8096 start
		/*SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(errorsInner.e331);
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.endp)
		|| isNE(lifemjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(lifemjaIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
			return ;
		}
		wsaaLives.add(1);
		lifemjaIO.setFunction(varcom.nextr);*/
 //ILIFE-8096 end
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
 //ILIFE-8096 start
/*private static final class ErrorsInner { 
		 ERRORS 
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData g436 = new FixedLengthStringData(4).init("G436");
	private FixedLengthStringData e331 = new FixedLengthStringData(4).init("E331");
	private FixedLengthStringData e370 = new FixedLengthStringData(4).init("E370");
	private FixedLengthStringData e268 = new FixedLengthStringData(4).init("E268");
	private FixedLengthStringData rrj5 = new FixedLengthStringData(4).init("RRJ5");// fwang3 ICIL-559
	
	}*/
 //ILIFE-8096 end
}
