/*
 * File: Revfund.java
 * Date: 30 August 2009 2:08:05
 * Author: Quipoz Limited
 * 
 * Class transformed from REVFUND.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UdelTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UdivTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswhTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Unit Switching Reversal Module
*
*    This subroutine is called from AT module P5155AT to perform
*  the processing necessary to reverse a fund switch transaction
*  (T676), and it does it in the following way:
*
*    -  Reads the contract header, CHDR.
*    -  Deletes any USWD's & USWH's that may exist for the switch
*       transaction.
*    -  Deletes all unprocessed UTRN's for the switch.
*    -  Creates reverse UTRN'S for each processed switch UTRN, ch-
*       anging the following information;
*
*           reverse the sign on  UTRNREV-CONTRACT-AMOUNT
*                                UTRNREV-FUND-AMOUNT
*                                UTRNREV-NOF-UNITS
*                                UTRNREV-NOF-DUNITS.
*           zeroise              UTRNREV-SURRENDER-PERCENT.
*           move spaces to       UTRNREV-FEEDBACK-IND
*                                UTRNREV-SWITCH-INDICATOR
*                                UTRNREV-TRIGGER-MODULE
*                                UTRNREV-TRIGGER-KEY.
*           move reversal tranno & batch trans code to
*                                UTRNREV-TRANNO
*                                UTRNREV-BATCTRCDE.
*           get proc-seq-no from T6647 for the reversal transactio 
*           & move to            UTRN-PROC-SEQ-NO.
*
*    -  Reads through the ACMV's for the policy & for any match on
*       TRANNO & TRANS CODE, writes a reverse ACMV changing the fo l-
*       owing information;
*
*           reverse the sign on  ACMVREV-ORIGAMT
*                                ACMVREV-ACCT-AMT.
*           set the tranno & batch key to the reversal values
*
*    -  Deletes the CHDR record if one has been created by the sw-
*       itch transaction, ie. if it has been through UNITDEAL.
*    -  Reinstates the old CHDR record setting the valid flag to ' '.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*        Include processing for interest bearing funds.               *
*        Reverse any HITR records created by the forward              *
*        transaction.                                                 *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Revfund extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private UdelTableDAM udelIO = new UdelTableDAM();
	private UdivTableDAM udivIO = new UdivTableDAM();
	private UswdTableDAM uswdIO = new UswdTableDAM();
	private UswhTableDAM uswhIO = new UswhTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		uswd1030, 
		nextUswd1040, 
		exit1090, 
		feedback2040, 
		nextUtrn2050, 
		exit2090, 
		feedback2340, 
		nextHitr2350, 
		exit2390
	}

	public Revfund() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline010()
	{
		init010();
		para020();
		deleteTaxd020();
	}

protected void init010()
	{
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFunction("BEGN");
		axesChdr6000();
	}

protected void para020()
	{
		procUswhUswd1000();
		procUtrn2000();
		procHitr2300();
		procAcmv3000();
		processAcmvOptical3010();
		procChdr4000();
		procUdel7000();
		procUdiv8000();
	}

protected void deleteTaxd020()
	{
		/* Perform to delete the service tax record.                       */
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(0);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			deleteTax3500();
		}
		
		/*EXIT*/
		exitProgram();
	}

protected void procUswhUswd1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readhUswh1010();
					deleteUswh1020();
				case uswd1030: 
					uswd1030();
				case nextUswd1040: 
					nextUswd1040();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Attempt to locate USWH record & any associated USWD
	*    Delete any matching USWH & USWD
	* </pre>
	*/
protected void readhUswh1010()
	{
		uswhIO.setDataKey(SPACES);
		uswhIO.setChdrcoy(reverserec.company);
		uswhIO.setChdrnum(reverserec.chdrnum);
		uswhIO.setTranno(reverserec.tranno);
		/* MOVE BEGNH                  TO USWH-FUNCTION.                */
		uswhIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		uswhIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)
		&& isNE(uswhIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError9000();
		}
		if (isEQ(uswhIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.uswd1030);
		}
		if (isNE(reverserec.company, uswhIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, uswhIO.getChdrnum())
		|| isNE(reverserec.tranno, uswhIO.getTranno())) {
			goTo(GotoLabel.uswd1030);
		}
	}

protected void deleteUswh1020()
	{
		/* MOVE DELET                  TO USWH-FUNCTION.                */
		uswhIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError9000();
		}
	}

protected void uswd1030()
	{
		uswdIO.setDataKey(SPACES);
		uswdIO.setPlanSuffix(ZERO);
		uswdIO.setTranno(ZERO);
		uswdIO.setChdrcoy(reverserec.company);
		uswdIO.setChdrnum(reverserec.chdrnum);
		/* MOVE BEGNH                  TO USWD-FUNCTION.                */
		uswdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		uswdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		uswdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		if (isEQ(uswdIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(uswdIO.getChdrcoy(), reverserec.company)
		&& isEQ(uswdIO.getChdrnum(), reverserec.chdrnum)
		&& isEQ(uswdIO.getTranno(), reverserec.tranno)) {
			deleteUswd1100();
		}
		else {
			goTo(GotoLabel.exit1090);
		}
	}

protected void nextUswd1040()
	{
		uswdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		if (isEQ(uswdIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(uswdIO.getChdrcoy(), reverserec.company)
		&& isEQ(uswdIO.getChdrnum(), reverserec.chdrnum)
		&& isEQ(uswdIO.getTranno(), reverserec.tranno)) {
			deleteUswd1100();
			goTo(GotoLabel.nextUswd1040);
		}
	}

protected void deleteUswd1100()
	{
		/*DELETE*/
		/* MOVE DELET                  TO USWD-FUNCTION.                */
		uswdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void procUtrn2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begnUtrn2010();
					tranno2030();
				case feedback2040: 
					feedback2040();
				case nextUtrn2050: 
					nextUtrn2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnUtrn2010()
	{
		utrnrevIO.setDataKey(SPACES);
		utrnrevIO.setChdrcoy(reverserec.company);
		utrnrevIO.setChdrnum(reverserec.chdrnum);
		utrnrevIO.setPlanSuffix(ZERO);
		utrnrevIO.setTranno(reverserec.tranno);
		/* MOVE BEGNH                  TO UTRNREV-FUNCTION.             */
		utrnrevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(), varcom.oK)
		&& isNE(utrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			fatalError9000();
		}
		if (isEQ(utrnrevIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void tranno2030()
	{
		if (isNE(utrnrevIO.getTranno(), reverserec.tranno)
		|| isNE(utrnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(utrnrevIO.getChdrnum(), reverserec.chdrnum)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void feedback2040()
	{
		if (isEQ(utrnrevIO.getFeedbackInd(), SPACES)) {
			deleteUtrn2100();
			goTo(GotoLabel.nextUtrn2050);
		}
		if (isEQ(utrnrevIO.getFeedbackInd(), "Y")) {
			createUtrn2200();
		}
	}

protected void nextUtrn2050()
	{
		utrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(), varcom.oK)
		&& isNE(utrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			fatalError9000();
		}
		if (isEQ(utrnrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(utrnrevIO.getTranno(), reverserec.tranno)
		&& isEQ(utrnrevIO.getChdrcoy(), reverserec.company)
		&& isEQ(utrnrevIO.getChdrnum(), reverserec.chdrnum)) {
			goTo(GotoLabel.feedback2040);
		}
	}

protected void deleteUtrn2100()
	{
		/*DELETE*/
		utrnrevIO.setFormat(formatsInner.utrnrevrec);
		/* MOVE DELET                  TO UTRNREV-FUNCTION.             */
		utrnrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnrevIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void createUtrn2200()
	{
		format2210();
	}

	/**
	* <pre>
	*    Create opposite UTRN record
	* </pre>
	*/
protected void format2210()
	{
		utrnrevIO.setTranno(reverserec.newTranno);
		setPrecision(utrnrevIO.getContractAmount(), 2);
		utrnrevIO.setContractAmount(mult(utrnrevIO.getContractAmount(), -1));
		setPrecision(utrnrevIO.getFundAmount(), 2);
		utrnrevIO.setFundAmount(mult(utrnrevIO.getFundAmount(), -1));
		setPrecision(utrnrevIO.getNofUnits(), 5);
		utrnrevIO.setNofUnits(mult(utrnrevIO.getNofUnits(), -1));
		setPrecision(utrnrevIO.getNofDunits(), 5);
		utrnrevIO.setNofDunits(mult(utrnrevIO.getNofDunits(), -1));
		setPrecision(utrnrevIO.getProcSeqNo(), 0);
		utrnrevIO.setProcSeqNo(mult(utrnrevIO.getProcSeqNo(), -1));
		utrnrevIO.setFormat(formatsInner.utrnrevrec);
		utrnrevIO.setSurrenderPercent(ZERO);
		utrnrevIO.setFeedbackInd(SPACES);
		utrnrevIO.setSwitchIndicator(SPACES);
		utrnrevIO.setTriggerModule(SPACES);
		utrnrevIO.setTriggerKey(SPACES);
		utrnrevIO.setBatctrcde(reverserec.batctrcde);
		/* MOVE WSAA-PROC-SEQ-NO       TO UTRNREV-PROC-SEQ-NO.          */
		utrnrevIO.setFunction(varcom.writr);
		utrnrevIO.setUstmno(ZERO);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnrevIO.getParams());
			fatalError9000();
		}
	}

protected void procHitr2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begnHitr2310();
					tranno2330();
				case feedback2340: 
					feedback2340();
				case nextHitr2350: 
					nextHitr2350();
				case exit2390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnHitr2310()
	{
		/*    Process all HITR records which match on TRANNO               */
		/*    depending on whether the record has been through             */
		/*    the Unit Dealing Feedback loop                               */
		hitrrevIO.setParams(SPACES);
		hitrrevIO.setChdrcoy(reverserec.company);
		hitrrevIO.setChdrnum(reverserec.chdrnum);
		hitrrevIO.setPlanSuffix(ZERO);
		hitrrevIO.setTranno(reverserec.tranno);
		hitrrevIO.setFormat(formatsInner.hitrrevrec);
		hitrrevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(), varcom.oK)
		&& isNE(hitrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			fatalError9000();
		}
		if (isEQ(hitrrevIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2390);
		}
	}

protected void tranno2330()
	{
		if (isNE(hitrrevIO.getTranno(), reverserec.tranno)
		|| isNE(hitrrevIO.getChdrcoy(), reverserec.company)
		|| isNE(hitrrevIO.getChdrnum(), reverserec.chdrnum)) {
			/*       GO 2090-EXIT                                       <INTBR>*/
			goTo(GotoLabel.exit2390);
		}
	}

protected void feedback2340()
	{
		if (isEQ(hitrrevIO.getFeedbackInd(), SPACES)) {
			deleteHitr2400();
			goTo(GotoLabel.nextHitr2350);
		}
		if (isEQ(hitrrevIO.getFeedbackInd(), "Y")) {
			createHitr2500();
		}
	}

protected void nextHitr2350()
	{
		hitrrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(), varcom.oK)
		&& isNE(hitrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			fatalError9000();
		}
		if (isEQ(hitrrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(hitrrevIO.getTranno(), reverserec.tranno)
		&& isEQ(hitrrevIO.getChdrcoy(), reverserec.company)
		&& isEQ(hitrrevIO.getChdrnum(), reverserec.chdrnum)) {
			goTo(GotoLabel.feedback2340);
		}
	}

protected void deleteHitr2400()
	{
		/*DELETE*/
		hitrrevIO.setFormat(formatsInner.hitrrevrec);
		hitrrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrrevIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void createHitr2500()
	{
		format2510();
	}

protected void format2510()
	{
		/*    Create opposite HITR record                                  */
		hitrrevIO.setTranno(reverserec.newTranno);
		setPrecision(hitrrevIO.getContractAmount(), 2);
		hitrrevIO.setContractAmount(mult(hitrrevIO.getContractAmount(), -1));
		setPrecision(hitrrevIO.getFundAmount(), 2);
		hitrrevIO.setFundAmount(mult(hitrrevIO.getFundAmount(), -1));
		setPrecision(hitrrevIO.getProcSeqNo(), 0);
		hitrrevIO.setProcSeqNo(mult(hitrrevIO.getProcSeqNo(), -1));
		hitrrevIO.setFormat(formatsInner.hitrrevrec);
		hitrrevIO.setSurrenderPercent(ZERO);
		hitrrevIO.setFeedbackInd(SPACES);
		hitrrevIO.setSwitchIndicator(SPACES);
		hitrrevIO.setTriggerModule(SPACES);
		hitrrevIO.setTriggerKey(SPACES);
		hitrrevIO.setBatctrcde(reverserec.batctrcde);
		hitrrevIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrrevIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrrevIO.setZintrate(ZERO);
		hitrrevIO.setZintalloc("N");
		hitrrevIO.setZintappind(SPACES);
		hitrrevIO.setFunction(varcom.writr);
		hitrrevIO.setUstmno(ZERO);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrrevIO.getParams());
			fatalError9000();
		}
	}

protected void procAcmv3000()
	{
		acmvRead3010();
	}

	/**
	* <pre>
	*    Attempt to locate ACMV records
	*    Where ACMV is found, create an opposite one for
	*    the reversal transaction
	* </pre>
	*/
protected void acmvRead3010()
	{
		acmvrevIO.setDataKey(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		/* MOVE BEGNH                   TO ACMVREV-FUNCTION.            */
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalError9000();
		}
		if (isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			return ;
		}
		while ( !(isNE(acmvrevIO.getRldgcoy(), reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(), reverserec.tranno)
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs3020();
		}
		
	}

protected void processAcmvOptical3010()
	{
		start3010();
	}

protected void start3010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			fatalError9000();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError9000();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs3020();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				fatalError9000();
			}
		}
	}

protected void reverseAcmvRecs3020()
	{
		/*START*/
		if (isEQ(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			revAcmv5000();
		}
		acmvrevIO.setFunction(varcom.nextr);
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void deleteTax3500()
	{
		start3500();
	}

protected void start3500()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			fatalError9000();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(reverserec.company, taxdrevIO.getChdrcoy())
		&& isEQ(reverserec.chdrnum, taxdrevIO.getChdrnum())
		&& isEQ(taxdrevIO.getTranno(), reverserec.tranno)
		&& isEQ(taxdrevIO.getBillcd(), varcom.vrcmMaxDate)) {
			taxdrevIO.setStatuz(varcom.oK);
			taxdrevIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdrevIO.getParams());
				syserrrec.statuz.set(taxdrevIO.getStatuz());
				fatalError9000();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void procChdr4000()
	{
		chdrRead4010();
	}

	/**
	* <pre>
	*    Update CHDRMJA record  switches left & switches used fields
	* </pre>
	*/
protected void chdrRead4010()
	{
		/* MOVE BEGNH                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.begn);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		axesChdr6000();
		/* DISPLAY 'BEGNH POL - ' CHDRMJA-CHDRNUM.                      */
		/* DISPLAY 'CHDR TRANNO - ' CHDRMJA-TRANNO.                     */
		/* DISPLAY 'REVE TRANNO - ' REVE-TRANNO.                        */
		/* IF CHDRMJA-TRANNO       NOT =  REVE-TRANNO                   */
		/*    MOVE REWRT               TO CHDRMJA-FUNCTION              */
		/*    MOVE CHDRMJAREC          TO CHDRMJA-FORMAT                */
		/*    PERFORM 6000-AXES-CHDR                                    */
		/*    GO TO 4090-EXIT                                           */
		/* END-IF.                                                      */
		/* MOVE 'DELET'                TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.deltd);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		axesChdr6000();
		chdrmjaIO.setFunction("READH");
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		axesChdr6000();
		if (isNE(chdrmjaIO.getValidflag(), 2)) {
			syserrrec.params.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		axesChdr6000();
	}

protected void revAcmv5000()
	{
		acmv1Write5010();
	}

	/**
	* <pre>
	*    Write ACMV reversal record for GL code used by
	*    FUNDSWCH.
	* </pre>
	*/
protected void acmv1Write5010()
	{
		/* Only reverse those postings created specifically by the      */
		/* fund switch transaction, not those created when dealing      */
		/* the funds involved in the switch.                            */
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.rldgcoy.set(reverserec.company);
		lifacmvrec.rdocnum.set(reverserec.chdrnum);
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.batccoy.set(reverserec.company);
		lifacmvrec.batcbrn.set(reverserec.batcbrn);
		lifacmvrec.batcactyr.set(reverserec.batcactyr);
		lifacmvrec.batcactmn.set(reverserec.batcactmn);
		lifacmvrec.batctrcde.set(reverserec.batctrcde);
		lifacmvrec.batcbatch.set(reverserec.batcbatch);
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		setPrecision(acmvrevIO.getOrigamt(), 2);
		acmvrevIO.setOrigamt(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.origamt.set(acmvrevIO.getOrigamt());
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(acmvrevIO.getTrandesc());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		setPrecision(acmvrevIO.getAcctamt(), 2);
		acmvrevIO.setAcctamt(mult(acmvrevIO.getAcctamt(), -1));
		lifacmvrec.acctamt.set(acmvrevIO.getAcctamt());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.postyear.set(acmvrevIO.getPostyear());
		lifacmvrec.postmonth.set(acmvrevIO.getPostmonth());
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-RCAMT          TO LIFA-RCAMT.                   */
		lifacmvrec.rcamt.set(ZERO);
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(acmvrevIO.getTransactionDate());
		lifacmvrec.transactionTime.set(acmvrevIO.getTransactionTime());
		lifacmvrec.user.set(acmvrevIO.getUser());
		lifacmvrec.termid.set(acmvrevIO.getTermid());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void axesChdr6000()
	{
		/*START*/
		/* DISPLAY 'CHDR FUNCTION  - ' CHDRMJA-FUNCTION.                */
		SmartFileCode.execute(appVars, chdrmjaIO);
		/* DISPLAY 'CHDR STATUZ    - ' CHDRMJA-STATUZ.                  */
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void procUdel7000()
	{
		procUdelInit7010();
	}

protected void procUdelInit7010()
	{
		udelIO.setDataArea(SPACES);
		udelIO.setFormat(formatsInner.udelrec);
		udelIO.setChdrcoy(reverserec.company);
		udelIO.setChdrnum(reverserec.chdrnum);
		udelIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, udelIO);
		if (isNE(udelIO.getStatuz(), varcom.oK)
		&& isNE(udelIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(udelIO.getParams());
			fatalError9000();
		}
		if (isEQ(udelIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		udelIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, udelIO);
		if (isNE(udelIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(udelIO.getParams());
			fatalError9000();
		}
	}

protected void procUdiv8000()
	{
		procUdivInit8010();
	}

protected void procUdivInit8010()
	{
		udivIO.setDataArea(SPACES);
		udivIO.setFormat(formatsInner.udivrec);
		udivIO.setChdrcoy(reverserec.company);
		udivIO.setChdrnum(reverserec.chdrnum);
		udivIO.setTranno(reverserec.tranno);
		udivIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)
		&& isNE(udivIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(udivIO.getParams());
			fatalError9000();
		}
		if (isEQ(udivIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		udivIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(udivIO.getParams());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		/*ERROR*/
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData utrnrevrec = new FixedLengthStringData(10).init("UTRNREVREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData hitrrevrec = new FixedLengthStringData(10).init("HITRREVREC");
	private FixedLengthStringData udelrec = new FixedLengthStringData(10).init("UDELREC");
	private FixedLengthStringData udivrec = new FixedLengthStringData(10).init("UDIVREC");
}
}
