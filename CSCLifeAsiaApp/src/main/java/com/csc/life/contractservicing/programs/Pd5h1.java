

package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.contractservicing.dataaccess.dao.RpdetpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.RplnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rpdetpf;
import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
import com.csc.life.contractservicing.procedures.Intcalc;
import com.csc.life.contractservicing.procedures.LoanRepay;
import com.csc.life.contractservicing.procedures.Totloan;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.recordstructures.Lnrepayrec;
import com.csc.life.contractservicing.screens.Sd5h1ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5h1 extends ScreenProgCS {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5H1");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaSuspCurrency = new FixedLengthStringData(3);
	private PackedDecimalData wsaaPrincipal = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaInterest = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaTotal = new PackedDecimalData(9, 2);
	private FixedLengthStringData wsaaHeldCurrency = new FixedLengthStringData(3);
	private PackedDecimalData wsaaHeldPrincipal = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaHeldInterest = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaHeldTotal = new PackedDecimalData(9, 2);
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaMandateType = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaIndTotal = new ZonedDecimalData(17,2).setUnsigned();
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSubfileSize = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSign = new FixedLengthStringData(1);
	private int wsaaBaseIdx ;
	private PackedDecimalData wsaaT5645Idx = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Batckey wsaaBatckey = new Batckey();
	private Sd5h1ScreenVars sv = ScreenProgram.getScreenVars(Sd5h1ScreenVars.class);

	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String loanrec = "LOANREC";
	private static final String acblrec = "ACBLREC";

	private Gensswrec gensswrec = new Gensswrec();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);// ILIFE-5826
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	protected BabrpfDAO bapfDAO = getApplicationContext().getBean("bapfDAO", BabrpfDAO.class);
	private LoanpfDAO loanpfDAO = getApplicationContext().getBean("loanpfDAO", LoanpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private BabrTableDAM babrIO = new BabrTableDAM();
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private Chdrpf chdrpf = new Chdrpf();
	private Chdrpf insChdrpf = new Chdrpf();
	private Payrpf payrpf = new Payrpf();
	private Lifepf lifepf = new Lifepf();
	private Itempf itempf = new Itempf();
	private Descpf descpf = null;
	private Acblpf acblpf = null;
	private List<Loanpf> loanpfList = new ArrayList<>();
	private List<Rpdetpf> rpdetpfloanList = new ArrayList<>();
	private Loanpf loanpf = null;
	private Clntpf clntpf;
	private List<Babrpf> lstBabrpf = null;
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private Intcalcrec intcalcrec = new Intcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Tr386rec tr386rec = new Tr386rec();
	private T6633rec t6633rec = new T6633rec();
	private static final String t6633 = "T6633";
	private static final String tr386 = "TR386";
	private static final String itdmrec = "ITEMREC";
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T3629rec t3629rec = new T3629rec();
	private Sftlockrec sftlockrec = new Sftlockrec();	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");	
	private FixedLengthStringData wsaaT6633Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Item, 0);
	private FixedLengthStringData wsaaLoanType = new FixedLengthStringData(1).isAPartOf(wsaaT6633Item, 3);
	private ZonedDecimalData wsaabusdate = new ZonedDecimalData(8, 0).setUnsigned();	
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	
	private List<Clrfpf> clrfpfList = null;
	private RplnpfDAO rplnpfDAO = getApplicationContext().getBean("rplnpfDAO", RplnpfDAO.class);
	private Rplnpf rplnpf = new Rplnpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Ptrnpf ptrnpf = new Ptrnpf();
	private Ptrnpf ptrnUpdpf = new Ptrnpf();
	private List<Ptrnpf> ptrnpfList = null;
	private BextpfDAO bextpfDAO = getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private Bextpf bextpf = new Bextpf();
	private List<Bextpf> bextBulkInsList = null;
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Clbapf clbapf = null;
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private Mandpf mandpf = null;
	
	private RpdetpfDAO rpdetpfDAO = getApplicationContext().getBean("rpdetpfDAO", RpdetpfDAO.class);
	private Rpdetpf rpdetpf = null;	
	private Rpdetpf upRpdetpf = null;
	private List<Rpdetpf>  rpdetBulkInsList = null;
	private List<Rpdetpf>  rpdetBulkupList = null;

	// Error Code Start
	private static final String rrel = "RREL";
	private static final String f020 = "F020";
	private static final String e961 = "E961";
	private static final String g900 = "G900";
	private static final String rrem = "RREM";
	private static final String e723 = "E723";
	private static final String g620 = "G620";
	private static final String h093 = "H093";
	private static final String hl63 = "HL63";	
	// Error Code End
	private String approveStatus = "A";
	private String pendingStatus="P";
	private Double totalPlPrinciple;
	private Double totalPlint;
	private Double totalAplPrinciple;
	private Double totalAplint;
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaTranno = new FixedLengthStringData(5);
	private List<Chdrpf> chdrBulkUpdtList = null;
	private List<Chdrpf> chdrBulkInstList = null;
	private char wasscoy;
	private ZonedDecimalData wsaarepaymentamt = new ZonedDecimalData(17, 2).setUnsigned();
	List<Integer> totalPlList  = new ArrayList<Integer>();
	List<Integer> totalAplList = new ArrayList<Integer>();
	private Map<String, List<Itempf>> t3629ListMap;

	private boolean loantypeStatus = false;
	private boolean isFullyPaid = true;
	private boolean plNotPaid = false;
	private String mandref = new String();
	private Mandpf mandlnb = null;
	private Mandpf mandccd = null;
	private boolean isFirstTime;
	
	
	private String strSacscode;
	private String strSacsType;
	private String strglMap;
	private String strglSign;
	private int intIndex;
	private Conlinkrec conlinkrec = new Conlinkrec();
	private static final int wsaaMaxDate = 99999999;
	private Lnrepayrec lnrepayrec = new Lnrepayrec();
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(19);       
	private FixedLengthStringData wsaaTranpfx = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);       
	private FixedLengthStringData wsaaTrancoy = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);       
	private FixedLengthStringData wsaaEntity = new FixedLengthStringData(16).isAPartOf(wsaaTrankey, 3);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaEntity, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaEntity, 8).setUnsigned();
	int wsaaSequenceNo ;
	private Map<String, Descpf> t1688Map = null;
	private Descpf descpfT1688;
	private PackedDecimalData checkTotal = new PackedDecimalData(9, 2).init(0);    //ILB-1339
	
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	
	public Pd5h1() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5h1", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void initialise1000() {
		initialise1010();
	}

	protected void initialise1010() {

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		//rpdetBulkInsList = new ArrayList<Rpdetpf>();//rpdetpf
		t3629ListMap =  new HashMap<String, List<Itempf>>();
		chdrBulkUpdtList = new ArrayList<Chdrpf>();
		chdrBulkInstList = new ArrayList<Chdrpf>();
		wsaaToday.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);									
		totalPlPrinciple = 0.00;
		totalPlint = 0.00;
		totalAplPrinciple = 0.00;
		totalAplint = 0.00;
		t1688Map = descDAO.getItems("IT", wsspcomn.company.toString(), "T1688", wsspcomn.language.toString());
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaPrincipal.set(ZERO);
		wsaaInterest.set(ZERO);
		wsaaTotal.set(ZERO);
		sv.totalaplint.set(ZERO);
		sv.totalaplprncpl.set(ZERO);
		sv.totalplprncpl.set(ZERO);
		sv.totalplint.set(ZERO);		
		sv.remngplprncpl.set(ZERO);
		sv.remngplint.set(ZERO);
		sv.remngaplprncpl.set(ZERO);
		sv.remngaplint.set(ZERO);
		sv.repaymentamt.set(ZERO);
		sv.repayment.set(ZERO);
		sv.repaymentstatus.set("");
		wsaaSuspCurrency.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.refcode.set(SPACES);
		
		wsaaTranpfx.set(chdrpf.getChdrpfx());
		wsaaTrancoy.set(wsspcomn.company);
		
		/*Read Tr386 table*/
		readTr3861100();
		if (isEQ(wsspcomn.sbmaction, "C")) {
			sv.descn.set(tr386rec.progdesc01);			
		}else if(isEQ(wsspcomn.sbmaction, "D")){
			sv.descn.set(tr386rec.progdesc02);
		}else if(isEQ(wsspcomn.sbmaction, "E")){
			sv.descn.set(tr386rec.progdesc03);
		}
		
		/* Get chdrpf details */
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);

		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.currcd.set(chdrpf.getCntcurr());
		
		wsaaHeldCurrency.set(chdrpf.getCntcurr());
		sv.cownnum.set(chdrpf.getCownnum());
		sv.currfrom.set(chdrpf.getOccdate());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
		sv.effdate.set(wsspcomn.currfrom);
		sv.register.set(chdrpf.getReg());
		wsaaTranno.set(chdrpf.getTranno() + 1);
		sv.rpcurr.set(chdrpf.getCntcurr());
		wasscoy = chdrpf.getChdrcoy();
		t3629ListMap = itemDao.loadSmartTable("IT", wsspcomn.company.toString(), "T3629");
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), chdrpf.getCownnum());//IJTI-1410
		descpf = descDAO.getdescData("IT", t5688, chdrpf.getCnttype(), chdrpf.getChdrcoy().toString(),//IJTI-1410
				wsspcomn.language.toString());
		if (descpf == null) {
			sv.ctypedes.set(SPACES);
		} else {
			sv.ctypedes.set(descpf.getLongdesc());
		}

		descpf = descDAO.getdescData("IT", t3623, chdrpf.getStatcode(), wsspcomn.company.toString(),//IJTI-1410
				wsspcomn.language.toString());
		if (descpf == null) {
			sv.chdrstatus.fill("?");
		} else {
			sv.chdrstatus.set(descpf.getShortdesc());
		}
		
		/* Obtain the Premuim Status description from T3588. */
		descpf = descDAO.getdescData("IT", t3588, chdrpf.getPstcde(), wsspcomn.company.toString(),//IJTI-1410
				wsspcomn.language.toString());
		if (descpf == null) {
			sv.premstatus.fill("?");
		} else {
			sv.premstatus.set(descpf.getShortdesc());
		}

		/* Get Rplnpf details */
		rplnpf = rplnpfDAO.getRploanRecord(sv.chdrnum.toString() ,"P");
		if (rplnpf == null) {
			sv.repaymentamt.set(ZERO);
			sv.totalplprncpl.set(ZERO);
			sv.totalplint.set(ZERO);
			sv.totalaplprncpl.set(ZERO);
			sv.totalaplint.set(ZERO);
			sv.remngplprncpl.set(ZERO);
			sv.remngplint.set(ZERO);
			sv.remngaplprncpl.set(ZERO);
			sv.remngaplint.set(ZERO);
		} else {
			sv.repaymentamt.set(rplnpf.getRepaymentamt());
			sv.repymop.set(rplnpf.getRepymop());			
			sv.totalplprncpl.set(rplnpf.getTotalplprncpl());
			sv.totalplint.set(rplnpf.getTotalplint());
			sv.totalaplprncpl.set(rplnpf.getTotalaplprncpl());
			sv.totalaplint.set(rplnpf.getTotalaplint());
			sv.remngplprncpl.set(rplnpf.getRemngplprncpl());
			sv.remngplint.set(rplnpf.getRemngplint());
			sv.remngaplprncpl.set(rplnpf.getRemngaplprncpl());
			sv.remngaplint.set(rplnpf.getRemngaplint());
		}
		
		/*get payer details*/
		payrpf = payrpfDAO.getpayrRecord(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum().trim());//IJTI-1410

		

		readLifeDetails(); //read life details
		calculateSuspense1600(); //calculate suspence
		protrctField();

		/* Perform section to load first page of subfile */

		scrnparams.function.set(varcom.sclr);
		processScreen("SD5H1", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		if(isEQ(wsspcomn.sbmaction,"C")){
			loadSubfile();
		}else{
			loadSubfileRpdetpf();			
		}
		
		
		if(isEQ(wsspcomn.flag, "I")  && isEQ(sv.repymop, "C") ){
			sv.crcindOut[varcom.nd.toInt()].set("Y");
			sv.crcindOut[varcom.pr.toInt()].set("Y");
			sv.ddindOut[varcom.nd.toInt()].set("Y");
			sv.ddindOut[varcom.pr.toInt()].set("Y");
			
		}	

	}
	protected void readTr3861100()
	{
		itempf = new Itempf();

		itempf.setItempfx(smtpfxcpy.item.toString().trim());
		itempf.setItemcoy(wsspcomn.company.toString().trim());
		itempf.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		itempf.setItemitem(wsaaTr386Key.toString().trim());
		itempf.setItemseq("  ");
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("TR386").concat(wsaaProg.toString()));
			fatalError600();
		}
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
	}
	
protected void loadSubfile() 
{
	loanpfList = loanpfDAO.loadDataByLoanNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "0");//IJTI-1410
	Collections.reverse(loanpfList);
	if (loanpfList != null && loanpfList.size() > 0) {
		for (Loanpf loan : loanpfList) {
			if(loan.getLoantype().equals("P")){
				loantypeStatus = true;
				totalPlList.add(loan.getLoannumber());
			}else if(loan.getLoantype().equals("A")){
				totalAplList.add(loan.getLoannumber());
			}				
			if (loan.getLoantype().equals("A") || loan.getLoantype().equals("P")) {
				initialize(sv.subfileFields);
				compute(wsaaCnt, 0).setDivide(scrnparams.subfileRrn, (wsaaSubfileSize));
				wsaaRem.setRemainder(wsaaCnt);
				if (isEQ(wsaaRem, 0)) {
					scrnparams.subfileMore.set("Y");
					break;
				}				
				writeToSubfile5010(loan);
			}

		}
	} else {
		scrnparams.function.set(varcom.prot);
	}

	scrnparams.subfileRrn.set(1);
}

protected void loadSubfileRpdetpf(){
	rpdetpfloanList = rpdetpfDAO.getRpdetRecordList(sv.chdrnum.toString(),rplnpf.getTranno());
	
	if (rpdetpfloanList != null && rpdetpfloanList.size() > 0) {
		for (Rpdetpf rpdet : rpdetpfloanList) {						
				if(rpdet.getLoantype().equals("P")){
					loantypeStatus = true;
					totalPlList.add(rpdet.getLoannum());
				}else if(rpdet.getLoantype().equals("A")){
					totalAplList.add(rpdet.getLoannum());
				}	
				initialize(sv.subfileFields);
				compute(wsaaCnt, 0).setDivide(scrnparams.subfileRrn, (wsaaSubfileSize));
				wsaaRem.setRemainder(wsaaCnt);
				if (isEQ(wsaaRem, 0)) {
					scrnparams.subfileMore.set("Y");
					break;
				}				
				writeToSubfile5011(rpdet);
			}
		} else {
		scrnparams.function.set(varcom.prot);
	}

	scrnparams.subfileRrn.set(1);
	
}

	protected void readLifeDetails() {

		lifepf = lifepfDAO.getLifeRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
		sv.lifenum.set(lifepf.getLifcnum());

		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		sv.ownername.set(wsspcomn.longconfname);

	}

	protected void calculateSuspense1600() {
		calculateSuspense1610();
		getSuspenceBal();
	}

	protected void calculateSuspense1610() {

		itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("  ");
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

	protected void getSuspenceBal() {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T3695");
		itempf.setItemitem(t5645rec.sacstype13.toString().trim());
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.params.set(
					"IT".concat(wsspcomn.company.toString()).concat("T3695").concat(t5645rec.sacstype13.toString()));
			fatalError600();
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode13.toString(),
				chdrpf.getChdrnum(), chdrpf.getBillcurr(), t5645rec.sacstype13.toString());//IJTI-1410

		if (acblpf == null) {
			sv.susbalnce.set(ZERO);
		} else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(sv.susbalnce, 2).set(mult(acblpf.getSacscurbal(), -1));
			} else {
				sv.susbalnce.set(acblpf.getSacscurbal());
			}
			wsaaSuspCurrency.set(acblpf.getOrigcurr());
		}

	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype().trim(), "C")) {
			corpname();
			return;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/* CORP-EXIT */
	}

	protected void protrctField() {
		if (isEQ(wsspcomn.flag, "I")) {
			sv.repymopOut[varcom.pr.toInt()].set("Y");
			sv.repaymentamtOut[varcom.pr.toInt()].set("Y");
		}

	}
	
	

	protected void writeToSubfile5010(Loanpf loan) {

		wsaaIndTotal.set(0);
		
		
		/* Work out Pending Interest Since last Billing date. */
		if (isLT(loan.getLstintbdte(), wsaaToday)) {
			intcalcrec.intcalcRec.set(SPACES);
			intcalcrec.loanNumber.set(loan.getLoannumber());
			intcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			intcalcrec.chdrnum.set(loan.getChdrnum());
			intcalcrec.cnttype.set(chdrpf.getCnttype());
			intcalcrec.interestTo.set(wsaaToday);
			intcalcrec.interestFrom.set(loan.getLstintbdte());
			intcalcrec.loanorigam.set(loan.getLstcaplamt());
			intcalcrec.lastCaplsnDate.set(loan.getLstcapdate());
			intcalcrec.loanStartDate.set(loan.getLoansdate());
			intcalcrec.interestAmount.set(ZERO);
			intcalcrec.loanCurrency.set(loan.getLoancurr());
			intcalcrec.loanType.set(loan.getLoantype());
			callProgram(Intcalc.class, intcalcrec.intcalcRec);
			if (isNE(intcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(intcalcrec.intcalcRec);
				syserrrec.statuz.set(intcalcrec.statuz);
			}
		} else {
			intcalcrec.interestAmount.set(0);
		}
		zrdecplrec.amountIn.set(intcalcrec.interestAmount);
		zrdecplrec.currency.set(loan.getLoancurr());
		a000CallRounding();
		intcalcrec.interestAmount.set(zrdecplrec.amountOut);		
		
		sv.hpndint.set(intcalcrec.interestAmount);
		
		if (isEQ(loan.getLoantype(), "P")){
			wsaaBaseIdx = 0;
		}
		else if (isEQ(loan.getLoantype(), "A")){
			wsaaBaseIdx = 4;
		}else{
			return;
		}
		
		getInterestAcbl6000(loan);	//ILB-1239

		getLoanAcbl(loan);
		
		wsaaIndTotal.set(add(sv.hpndint ,sv.hacrint , sv.hcurbal));
		sv.hpltot.set(wsaaIndTotal);
		sv.hprincipal.set(loan.getLoanorigam());
		sv.loanNumber.set(loan.getLoannumber());
		sv.loanType.set(loan.getLoantype());
		sv.cntcurr.set(loan.getLoancurr());
		sv.loanstdate.set(loan.getLoansdate());
		
		
		/* Get Interest Rate. */
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString().trim());
		itempf.setItemcoy(chdrpf.getChdrcoy().toString().trim());
		itempf.setItemtabl(t6633);
		wsaaItemCnttype.set(chdrpf.getCnttype());
		wsaaLoanType.set(loan.getLoantype());	
		itempf.setItemitem(wsaaT6633Item.toString().trim());
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.params.set(smtpfxcpy.item.toString().concat(wsspcomn.company.toString()).concat("T6633")
					.concat(wsaaT6633Item.toString()));
			fatalError600();
		}

		t6633rec.t6633Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		
		sv.intrstpercentage.set(t6633rec.intRate);

		if (loan.getLoantype().equals("P")) {
			totalPlPrinciple = totalPlPrinciple + sv.hcurbal.toDouble();
			totalPlint = totalPlint + sv.hacrint.toDouble() + sv.hpndint.toDouble();
		}

		if (loan.getLoantype().equals("A")) {
			totalAplPrinciple = totalAplPrinciple + sv.hcurbal.toDouble();
			totalAplint = totalAplint +sv.hacrint.toDouble() + sv.hpndint.toDouble();
		}

		sv.totalaplprncpl.set(totalAplPrinciple);
		sv.totalaplint.set(totalAplint);
		sv.totalplprncpl.set(totalPlPrinciple);
		sv.totalplint.set(totalPlint);		
		sv.remngplprncpl.set(totalPlPrinciple);
		sv.remngplint.set(totalPlint);
		sv.remngaplprncpl.set(totalAplPrinciple);
		sv.remngaplint.set(totalAplint);
		//sv.repayment.set(0);
		//sv.repaymentstatus.set("");
		scrnparams.function.set(varcom.sadd);
		processScreen("SD5H1", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.add(1);
	}
	
	protected void a000CallRounding() {
		/* A100-CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/* A900-EXIT */
	}

	protected void getInterestAcbl6000(Loanpf loan) {

		/* Use entry 2 on the T5645 record read to get Loan interest */
		/* ACBL for this loan number. */
		/* COMPUTE WSAA-T5645-IDX = WSAA-BASE-IDX + 2. */
		/* COMPUTE WSAA-T5645-IDX = WSAA-BASE-IDX + 4. */
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 2));
		wsaaSacscode.set(t5645rec.sacscode[wsaaT5645Idx.toInt()]);
		wsaaSacstype.set(t5645rec.sacstype[wsaaT5645Idx.toInt()]);
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(loan.getChdrnum());
		wsaaLoanNumber.set(loan.getLoannumber());

		acblpf = acblpfDAO.loadDataByBatch(loan.getChdrcoy(), wsaaSacscode.toString(),wsaaRldgacct.toString(),//IJTI-1410
				 chdrpf.getBillcurr(), wsaaSacstype.toString());

		if (acblpf == null) {
			sv.hacrint.set(ZERO);
		} else {	 
				 sv.hacrint.set(acblpf.getSacscurbal());		
		}

	}
	
	
	protected void writeToSubfile5011(Rpdetpf rpdet){
		wsaaIndTotal.set(0);
		if (isEQ(rpdet.getLoantype(), "P")){
			wsaaBaseIdx = 0;
		}
		else if (isEQ(rpdet.getLoantype(), "A")){
			wsaaBaseIdx = 4;
		}
		sv.hpndint.set(rpdet.getPendint());
		sv.hcurbal.set(rpdet.getCurramnt());
		sv.hacrint.set(rpdet.getAccrdint());
		wsaaIndTotal.set(add(sv.hpndint ,sv.hacrint , sv.hcurbal));	
		sv.hpltot.set(wsaaIndTotal);
		sv.hprincipal.set(rpdet.getPrinamnt());
		sv.loanNumber.set(rpdet.getLoannum());
		sv.loanType.set(rpdet.getLoantype());
		sv.cntcurr.set(rpdet.getLoancurr());
		sv.loanstdate.set(rpdet.getEfftdate());
		sv.intrstpercentage.set(rpdet.getInterest());
		sv.repayment.set(rpdet.getRepayamt());
		sv.repaymentstatus.set(rpdet.getRepaystatus());
		
		scrnparams.function.set(varcom.sadd);
		processScreen("SD5H1", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.add(1);
	}

	protected void getLoanAcbl(Loanpf loan)
	{
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 1));
		wsaaSacscode.set(t5645rec.sacscode[wsaaT5645Idx.toInt()]);
		wsaaSacstype.set(t5645rec.sacstype[wsaaT5645Idx.toInt()]);
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(loan.getChdrnum());
		wsaaLoanNumber.set(loan.getLoannumber());

		acblpf = acblpfDAO.loadDataByBatch(loan.getChdrcoy(), wsaaSacscode.toString(),wsaaRldgacct.toString(),//IJTI-1410
				 chdrpf.getBillcurr(), wsaaSacstype.toString());

		if (acblpf == null) {
			sv.hcurbal.set(ZERO);
		} else {	 
				 sv.hcurbal.set(acblpf.getSacscurbal());		
		}
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		nextProgram4010();
		//wsspcomn.programPtr.add(1);
		/* EXIT */
	}

	/**
	 * <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000() {
		/* UPDATE-DATABASE */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		 wsaaSequenceNo = 0;
		if (isEQ(scrnparams.statuz, "KILL")) {
			return;
		}
		if(isEQ(wsspcomn.flag, "I") && isNE(wsspcomn.sbmaction, "D") && (isEQ(sv.ansrepy, "N") || isEQ(sv.ansrepy, "X") || isEQ(sv.ansrepy, SPACES))) {
			return;
		}
		lnrepayrec.docamt.set(wsaarepaymentamt);
		if(isEQ(wsspcomn.sbmaction, "E"))
		{
			rpdetBulkupList = new ArrayList<Rpdetpf>();
			subfileStart();
			
			//Repay Policy loan first
			while (!(isEQ(scrnparams.statuz, varcom.endp))) {			
				lnrepayrec.repaidamt.set(ZERO);
				sv.repayment.set(ZERO);
				sv.repaymentstatus.set("O");
				if(totalPlList.size() > 0 && totalPlList.contains(sv.loanNumber)){	
					if(isNE(wsaaSuspCurrency,sv.cntcurr)) {
						conlinkrec.clnk002Rec.set(SPACES);         
						conlinkrec.currOut.set(wsaaSuspCurrency);
						conlinkrec.currIn.set(sv.cntcurr);
						conlinkrec.amountIn.set(lnrepayrec.docamt);
						convertCurrency();
						wsaarepaymentamt.set(conlinkrec.amountOut);
					} else {
						wsaarepaymentamt.set(lnrepayrec.docamt);
					}
						if(isGTE(wsaarepaymentamt,add(sv.hacrint,sv.hpndint))) {
							wsaaRldgChdrnum.set(sv.chdrnum);
							wsaaRldgLoanno.set(sv.loanNumber);
							lnrepayrecDetails(2);
							if (isNE(lnrepayrec.statuz, varcom.oK)) {
								syserrrec.subrname.set("TRNCURBAL");
								syserrrec.statuz.set(lnrepayrec.statuz);
								fatalError600();
							}							
							wsaaSequenceNo = lnrepayrec.transeq.toInt();
							sv.repayment.set(lnrepayrec.repaidamt);
							if(isGT(lnrepayrec.docamt, ZERO))
							{
								lnrepayrecDetails(1);
								if (isNE(lnrepayrec.statuz, varcom.oK)) {
									syserrrec.subrname.set("TRNCURBAL");
									syserrrec.statuz.set(lnrepayrec.statuz);
									fatalError600();
								}							
								wsaaSequenceNo = lnrepayrec.transeq.toInt();
								sv.repayment.set(add(sv.repayment,lnrepayrec.repaidamt));
							} else {
								plNotPaid = true;
								//break;								
							}
							//Set Repayment status
							if(isEQ(sv.repayment,add(sv.hacrint,sv.hpndint,sv.hcurbal).getbigdata()))		
								sv.repaymentstatus.set("F");  
							if(isLT(sv.repayment,add(sv.hacrint,sv.hpndint,sv.hcurbal).getbigdata())) {
								sv.repaymentstatus.set("P");
								plNotPaid = true;													
							}
							//Update Repayment detail
							upRpdetpf = new Rpdetpf();
							upRpdetpf.setApprovaldate(wsaaToday.toInt());
							upRpdetpf.setStatus(approveStatus);
							upRpdetpf.setRepayamt(sv.repayment.toDouble());
							upRpdetpf.setChdrnum(sv.chdrnum.toString());
							upRpdetpf.setLoannum(sv.loanNumber.toInt());
							upRpdetpf.setRepaystatus(sv.repaymentstatus.toString());
							upRpdetpf.setTranno(wsaaTranno.toInt());
							rpdetBulkupList.add(upRpdetpf);
							} else {								
								plNotPaid = true;
								//break;
							}
							
				}
				if(plNotPaid) {
					break;		
				}
				scrnparams.function.set(varcom.srdn);
				subfileIo4400();
			}
			
			//Repay Automatic Policy loan 
			
			if(!plNotPaid) {
				subfileStart();
				while (!(isEQ(scrnparams.statuz, varcom.endp))) {
					lnrepayrec.repaidamt.set(ZERO);
					sv.repayment.set(ZERO);
					sv.repaymentstatus.set("O");
					if(totalAplList.size() > 0 && totalAplList.contains(sv.loanNumber)){
						if(isNE(wsaaSuspCurrency,sv.cntcurr)) {
							conlinkrec.clnk002Rec.set(SPACES);         
							conlinkrec.currOut.set(wsaaSuspCurrency);
							conlinkrec.currIn.set(sv.cntcurr);
							conlinkrec.amountIn.set(lnrepayrec.docamt);
							convertCurrency();
							wsaarepaymentamt.set(conlinkrec.amountOut);
						} else {
							wsaarepaymentamt.set(lnrepayrec.docamt);
						}
						if(isGTE(wsaarepaymentamt,add(sv.hacrint,sv.hpndint,sv.hcurbal))) {
							wsaaEntity.set(sv.chdrnum.concat(sv.loanNumber).trim());
							lnrepayrecDetails(2);
							if (isNE(lnrepayrec.statuz, varcom.oK)) {
								syserrrec.subrname.set("TRNCURBAL");
								syserrrec.statuz.set(lnrepayrec.statuz);
								fatalError600();
							}						
							wsaaSequenceNo = lnrepayrec.transeq.toInt();
							sv.repayment.set(lnrepayrec.repaidamt);
							lnrepayrecDetails(1);
							if (isNE(lnrepayrec.statuz, varcom.oK)) {
								syserrrec.subrname.set("TRNCURBAL");
								syserrrec.statuz.set(lnrepayrec.statuz);
								fatalError600();
							}
							
							wsaaSequenceNo = lnrepayrec.transeq.toInt();
							sv.repayment.set(add(sv.repayment,lnrepayrec.repaidamt));
							sv.repaymentstatus.set("F");
							
							//Update Repayment detail
							upRpdetpf = new Rpdetpf();
							upRpdetpf.setApprovaldate(wsaaToday.toInt());
							upRpdetpf.setTranno(wsaaTranno.toInt());
							upRpdetpf.setStatus(approveStatus);
							upRpdetpf.setRepayamt(sv.repayment.toDouble());
							upRpdetpf.setChdrnum(sv.chdrnum.toString());
							upRpdetpf.setLoannum(sv.loanNumber.toInt());
							upRpdetpf.setRepaystatus(sv.repaymentstatus.toString());
							upRpdetpf.setTranno(rplnpf.getTranno());
							rpdetBulkupList.add(upRpdetpf);
						} else {
							break;
						}
							
						
					}
					scrnparams.function.set(varcom.srdn);
					subfileIo4400();
				}
			}
		}
		/* EXIT */
		
		if(isEQ(wsspcomn.sbmaction, "D") || isEQ(wsspcomn.sbmaction, "C"))
			updateTrnoChdrpf();		
		else
			processChdr();
			
		if(isEQ(wsspcomn.sbmaction, "E") || isEQ(wsspcomn.sbmaction, "C")){
			
			insertupdateRplnpf();	
			if(isEQ(wsspcomn.flag, "P")){
				postPendingInterest();
				if(isNE(sv.repymop,"C") && !mandref.isEmpty()){				
					writeBextRecord3060();
				}	
			}
			if (isEQ(wsspcomn.sbmaction, "E")){
				suspencePost();
			}
			
		}else if(isEQ(wsspcomn.sbmaction, "D")){
			updatePtrnRcd();
		}
	

		
	}
	
	protected void reversalLoan() {
			
		loanpfList = loanpfDAO.loadDataByLoanNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "0");			
		if(loanpfList == null || loanpfList.size()==0) {
			return ;
		}
		
		Loanpf revloan;
		List<Loanpf> revloanlist = new ArrayList<Loanpf>();
		for(Loanpf loan : loanpfList) {
			if (loan.getLstintbdte()<=wsaaToday.toInt()) {
				revloan = new Loanpf(reversalLoanDetail(loan));
				revloanlist.add(revloan);
			}
		}
		
		if(revloanlist!=null && revloanlist.size()>0) {
			loanpfDAO.insertLoanRec(revloanlist);
		}
	}
	
	protected Loanpf reversalLoanDetail(Loanpf loan) {
		loan.setValidflag("2");
		loanpfDAO.updateLoanValidflag(loan);
		
		loan.setValidflag("1");
		loan.setNxtintbdte(loan.getLstintbdte());
		
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(loan.getLstintbdte());
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString().trim());
		itempf.setItemcoy(chdrpf.getChdrcoy().toString().trim());
		itempf.setItemtabl(t6633);
		wsaaItemCnttype.set(chdrpf.getCnttype());
		wsaaLoanType.set(loan.getLoantype());	
		itempf.setItemitem(wsaaT6633Item.toString().trim());
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.params.set(smtpfxcpy.item.toString().concat(wsspcomn.company.toString()).concat("T6633")
					.concat(wsaaT6633Item.toString()));
			fatalError600();
		}

		t6633rec.t6633Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		if (isEQ(t6633rec.interestFrequency,SPACES)) {
			datcon2rec.frequency.set("01");
		}else {
			datcon2rec.frequency.set(t6633rec.interestFrequency);
		}
		
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		
		loan.setLstintbdte(datcon2rec.intDate2.toInt());
		loan.setLtranno(loan.getFtranno());
		loan.setFtranno(wsaaTranno.toInt());	
		return loan;
	}
	
	protected void reversalPosting() {
		List<Acmvpf> list = acmvpfDAO.getAcmvPtrnRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), rplnpf.getTranno());
		if(list == null || list.size()==0) {
			return ;
		}
		if (t1688Map.containsKey(wsaaBatckey.batcBatctrcde.toString())) {
	         descpfT1688 = t1688Map.get(wsaaBatckey.batcBatctrcde.toString());
	     } else {
	         descpfT1688 = new Descpf();
	         descpfT1688.setLongdesc("");
	     }
		
		for(Acmvpf acmvpf : list) {
			reversalPostingDetail(acmvpf);
		}
	}
	
	protected void reversalPostingDetail(Acmvpf acmvpf) {
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");

	  	lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.tranno.set(wsaaTranno.toInt());
		/*    MOVE REVE-NEW-TRANNO            TO LIFA-TRANREF.*/
		lifacmvrec.tranref.set(acmvpf.getTranref());
		lifacmvrec.rdocnum.set(acmvpf.getRdocnum());
		lifacmvrec.rldgcoy.set(acmvpf.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvpf.getGenlcoy());
		lifacmvrec.glcode.set(acmvpf.getGlcode());
		lifacmvrec.glsign.set(acmvpf.getGlsign());
		/*MOVE DESC-LONGDESC              TO LIFA-TRANDESC.            */
		lifacmvrec.trandesc.set(descpfT1688.getLongdesc());
		/* MOVE WSAA-TODAY                 TO LIFA-EFFDATE.             */
		lifacmvrec.effdate.set(acmvpf.getEffdate());
		lifacmvrec.termid.set(acmvpf.getTermid());
		lifacmvrec.user.set(varcom.vrcmUser.toInt());
		lifacmvrec.transactionTime.set(varcom.vrcmTime.toInt());
		lifacmvrec.transactionDate.set(varcom.vrcmDate.toInt());
		lifacmvrec.rldgacct.set(acmvpf.getRldgacct());
		lifacmvrec.origcurr.set(acmvpf.getOrigcurr());
		lifacmvrec.jrnseq.set(acmvpf.getJrnseq());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvpf.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvpf.getOrigamt(), -1));
		/*    MOVE REVE-NEW-TRANNO            TO LIFA-TRANREF.*/
		/*    MOVE ACMVREV-TRANDESC           TO LIFA-TRANDESC.*/
		lifacmvrec.crate.set(acmvpf.getCrate());
		lifacmvrec.genlcoy.set(acmvpf.getGenlcoy());
		lifacmvrec.genlcur.set(acmvpf.getGenlcur());
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		/* MOVE WSAA-TODAY                 TO LIFA-EFFDATE.             */
		lifacmvrec.effdate.set(acmvpf.getEffdate());
		/* COMPUTE LIFA-RCAMT = ACMVREV-RCAMT * -1.                     */
		/*    MOVE ACMVREV-RCAMT              TO LIFA-RCAMT.*/
		lifacmvrec.sacscode.set(acmvpf.getSacscode());
		lifacmvrec.sacstyp.set(acmvpf.getSacstyp());
		/* MOVE ACMVREV-FRCDATE            TO LIFA-FRCDATE.             */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}
	protected void postPendingInterest() {
		for(Loanpf loan : loanpfList)
		if (isLT(loan.getLstintbdte(),wsaaToday)) {
			totloanrec.totloanRec.set(SPACES);
			totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
			totloanrec.chdrnum.set(chdrpf.getChdrnum());
			totloanrec.principal.set(ZERO);
			totloanrec.interest.set(ZERO);
			totloanrec.loanCount.set(ZERO);
			totloanrec.effectiveDate.set(wsaaToday);
			totloanrec.tranno.set(wsaaTranno.toInt());
			totloanrec.batchkey.set(wsaaBatckey);
			totloanrec.tranTerm.set(varcom.vrcmTermid);
			totloanrec.tranDate.set(varcom.vrcmDate);
			totloanrec.tranTime.set(varcom.vrcmTime);
			totloanrec.tranUser.set(varcom.vrcmUser);
			totloanrec.language.set(wsspcomn.language);
			totloanrec.function.set("POST");
			callProgram(Totloan.class, totloanrec.totloanRec);
			if (isNE(totloanrec.statuz, varcom.oK)) {
				syserrrec.params.set(totloanrec.totloanRec);
				syserrrec.statuz.set(totloanrec.statuz);
				fatalError600();
			} 
		}
	}
	
	
	protected void lnrepayrecDetails(int increment){
				
		strSacscode = t5645rec.sacscode[wsaaBaseIdx + increment].toString();
		strSacsType = t5645rec.sacstype[wsaaBaseIdx + increment].toString();
		strglMap = t5645rec.glmap[wsaaBaseIdx + increment].toString();
		strglSign = t5645rec.sign[wsaaBaseIdx + increment].toString();
		
		lnrepayrec.function.set(SPACE);
		lnrepayrec.statuz.set(varcom.oK);
		lnrepayrec.trandate.set(wsaaToday);
		lnrepayrec.sacscode.set(strSacscode);
		lnrepayrec.sacstyp.set(strSacsType);
		lnrepayrec.trankey.set(wsaaTrankey);		
		lnrepayrec.loanNo.set(sv.loanNumber);
		
		lnrepayrec.genlCompany.set(wsspcomn.company);
		lnrepayrec.genlCurrency.set(wsaaSuspCurrency);
		lnrepayrec.genlAccount.set(strglMap);
		lnrepayrec.sign.set(strglSign);		
		lnrepayrec.transeq.set(wsaaSequenceNo+1);
		lnrepayrec.chdrpfx.set(wsaaTranpfx);
		lnrepayrec.chdrcoy.set(wsaaTrancoy);
		lnrepayrec.chdrnum.set(sv.chdrnum);
		lnrepayrec.tranno.set(wsaaTranno.toInt());		
		lnrepayrec.doctPrefix.set("");
		lnrepayrec.doctCompany.set(wsspcomn.company);
		lnrepayrec.doctNumber.set(chdrpf.getDocnum());
		lnrepayrec.batchkey.set(wsaaBatckey);
		lnrepayrec.tranid.set(chdrpf.getTranid());
		lnrepayrec.origamt.set(lnrepayrec.docamt);
		lnrepayrec.origccy.set(wsaaSuspCurrency);
		lnrepayrec.cnttype.set(chdrpf.getCnttype());
		 if (t1688Map.containsKey(wsaaBatckey.batcBatctrcde.toString())) {
	         descpfT1688 = t1688Map.get(wsaaBatckey.batcBatctrcde.toString());
	     } else {
	         descpfT1688 = new Descpf();
	         descpfT1688.setLongdesc("");
	     }
		lnrepayrec.trandesc.set(descpfT1688.getLongdesc());
		callProgram(LoanRepay.class, lnrepayrec.lnrepayRec);
		//lnrepayrec.acctccy.set(wsaaSuspCurrency);
		
	}
	
	protected void deleteRplnpf(){ // delete RPLNPF
		rplnpfDAO.deleteRploanpf(sv.chdrnum.toString(),rplnpf.getTranno());
	}
	protected void deleteRpdetpfRcd(){ // delete RPDETPF
		rpdetpfDAO.deleteRpdetpf(sv.chdrnum.toString(),rplnpf.getTranno());
	}
	
	protected void updatePtrnRcd(){
		ptrnUpdpf = new Ptrnpf();
		ptrnUpdpf.setChdrnum(sv.chdrnum.toString());
		ptrnUpdpf.setTranno(rplnpf.getTranno());
		ptrnUpdpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnUpdpf.setValidflag("2");
		
	}

	
	protected void processChdr(){
			
		chdrpf.setCurrto(wsaaToday.toInt());
		chdrBulkUpdtList.add(chdrpf);
		
		insChdrpf = new Chdrpf(chdrpf);
		insChdrpf.setValidflag('1');
		insChdrpf.setCurrfrom(wsaaToday.toInt());
		insChdrpf.setCurrto(varcom.maxdate.toInt());
		insChdrpf.setTranno(wsaaTranno.toInt());
		insChdrpf.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrBulkInstList.add(insChdrpf);
				
	}
	
	protected void updateTrnoChdrpf() {
		
		chdrpf.setTranno(wsaaTranno.toInt());
		chdrBulkUpdtList = new ArrayList<Chdrpf>();
		chdrBulkUpdtList.add(chdrpf);	

	}

	protected void insertupdateRplnpf() {

		if (isEQ(wsspcomn.sbmaction, "C")) {
			if(rplnpf == null)
				rplnpf = new Rplnpf();
			rplnpf.setChdrnum(sv.chdrnum.toString());
			rplnpf.setRepymop(sv.repymop.toString());
			rplnpf.setRepaymentamt(sv.repaymentamt.toDouble());
			rplnpf.setTotalplprncpl(sv.totalplprncpl.toDouble());
			rplnpf.setTotalplint(sv.totalplint.toDouble());
			rplnpf.setTotalaplprncpl(sv.totalaplprncpl.toDouble());
			rplnpf.setTotalaplint(sv.totalaplint.toDouble());
			rplnpf.setRemngplprncpl(sv.remngplprncpl.toDouble());
			rplnpf.setRemngplint(sv.remngplint.toDouble());
			rplnpf.setRemngaplprncpl(sv.remngaplprncpl.toDouble());
			rplnpf.setRemngaplint(sv.remngaplint.toDouble());
			rplnpf.setStatus(pendingStatus);
			rplnpf.setAprvdate(99999999);
			rplnpf.setTranno(wsaaTranno.toInt());
			rplnpf.setMandref(mandref);
		} else {
			if (isEQ(wsspcomn.sbmaction, "E")) {
				rplnpf.setStatus(approveStatus);//IJTI-1410
				rplnpf.setAprvdate(wsaaToday.toInt());
				rplnpf.setTranno(wsaaTranno.toInt());
				rplnpf.setChdrnum(sv.chdrnum.toString());	
				
			}
			
			
		}

	}

	protected void writePtrn() {	

		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		ptrnpf.setTranno(wsaaTranno.toInt());
		ptrnpf.setPtrneff(wsaaToday.toInt());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setValidflag("1");
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setDatesub(wsaaToday.toInt());

		ptrnpfList = new ArrayList<Ptrnpf>();
		ptrnpfList.add(ptrnpf);	
		boolean result;
		if(ptrnpfList != null && ptrnpfList.size() > 0){
			result = ptrnpfDAO.insertPtrnPF(ptrnpfList);
			if (!result) {
				syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum()));
				fatalError600();
			} else ptrnpfList.clear();
		}
	}

	
	 protected void writeBextRecord3060(){
		  
	  bextpf = new Bextpf();
	  bextpf.setInstfrom(wsaaToday.toInt());
	  bextpf.setInstto(wsaaToday.toInt());
	  bextpf.setBtdate(chdrpf.getBtdate());
	  bextpf.setBilldate(wsaaToday.toInt());
	  bextpf.setBillcd(wsaaToday.toInt());
	  bextpf.setInstamt01(sv.repaymentamt.toDouble());
	  bextpf.setInstamt02(0); bextpf.setInstamt03(0);
	  bextpf.setInstamt04(0); bextpf.setInstamt05(0);
	  bextpf.setInstamt06(sv.repaymentamt.toDouble());
	  bextpf.setInstjctl(SPACE);
	  bextpf.setChdrnum(chdrpf.getChdrnum());
	  bextpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	  bextpf.setChdrpfx(chdrpf.getChdrpfx());
	  bextpf.setServunit(chdrpf.getServunit());
	  bextpf.setCnttype(chdrpf.getCnttype());
	  bextpf.setOccdate(chdrpf.getOccdate());
	  bextpf.setCcdate(chdrpf.getCcdate());
	  bextpf.setCownpfx(chdrpf.getCownpfx());
	  bextpf.setCowncoy(chdrpf.getCowncoy().toString());
	  bextpf.setCownnum(chdrpf.getCownnum());
	  bextpf.setInstcchnl(chdrpf.getCollchnl());
	  bextpf.setCntbranch(chdrpf.getCntbranch());
	  bextpf.setAgntpfx(chdrpf.getAgntpfx());
	  bextpf.setAgntcoy(chdrpf.getAgntcoy().toString());
	  bextpf.setAgntnum(chdrpf.getAgntnum());
	  bextpf.setOutflag(SPACE);
	  bextpf.setPayflag(SPACE);
	  bextpf.setBilflag(SPACE);
	  bextpf.setGrupkey(SPACE);
	  bextpf.setMembsel(SPACE);
	  bextpf.setDdrsncde(SPACE);
	  bextpf.setCanflag(SPACE);
	  bextpf.setCompany(SPACE);
	  bextpf.setSupflag("N");
	  bextpf.setPayrpfx(clntpf.getClntpfx());//IJTI-1410
	  bextpf.setPayrcoy(clntpf.getClntcoy());//IJTI-1410
	  bextpf.setPayrnum(sv.cownnum.toString());
	  bextpf.setPtdate(chdrpf.getPtdate());
	  bextpf.setBillchnl(sv.repymop.toString());
	  bextpf.setInstbchnl(sv.repymop.toString());
	  bextpf.setInstfreq(chdrpf.getBillfreq());
	  bextpf.setCntcurr(chdrpf.getBillcurr());
	  
	  mandpf = mandpfDAO.searchMandpfRecordData(wsspcomn.fsuco.toString(),sv.cownnum.toString(),mandref,"MANDPF");
	  
	  bextpf.setMandref(mandref);	  
	  bextpf.setBankkey(mandpf.getBankkey());
	  bextpf.setBankacckey(mandpf.getBankacckey());
	  bextpf.setMandstat(mandpf.getMandstat());
	  
	
	  clbapf =clbapfDAO.searchClbapfRecordData(mandpf.getBankkey().trim(),mandpf.getBankacckey().trim(),//IJTI-1410
			  chdrpf.getCowncoy().toString().trim(),sv.cownnum.toString().trim(),"CN");
	  
	  bextpf.setFacthous(clbapf.getFacthous());
	  
	  readT3629(payrpf.getBillcurr());
	  bextpf.setBankcode(t3629rec.bankcode.toString());
	  bextpf.setNextdate(0);
	  getGLAcctCodes(sv.loanType.toString(), mandref);
	  bextpf.setGlmap(strglMap);
	  bextpf.setSacscode(strSacscode);
	  bextpf.setSacstyp(strSacsType); 
	  bextpf.setEffdatex(0);
	  bextpf.setDdderef(0);
	 	  
	  bextBulkInsList = new ArrayList<Bextpf>();
	  bextBulkInsList.add(bextpf);  
	 	
	 
	 }
	 
	 protected void getGLAcctCodes(String loanType, String mandRef){
			
			if (isEQ(loanType.toUpperCase(), "P")){
				intIndex = 0;
			}
			else if (isEQ(loanType.toUpperCase(), "A")){
				intIndex = 4;
			}
	
			if (isNE(mandRef, SPACES)) {
				strSacscode = t5645rec.sacscode13.toString();
				strSacsType = t5645rec.sacstype13.toString();
				strglMap = t5645rec.glmap13.toString();
				strglSign = t5645rec.sign13.toString();
			}
			else {
				strSacscode = "";
				strSacsType = "";
				strglMap = "";
				strglSign = "";
			}
		}
	 
	 protected void readT3629(String billCurr){
		 
	 	/* Read T3629 for bank code.*/
	 	String keyItemitem = billCurr.trim();
	 	List<Itempf> itempfList = new ArrayList<Itempf>();
	 	boolean itemFound = false;
	 	if (t3629ListMap.containsKey(keyItemitem)){	
	 		itempfList = t3629ListMap.get(keyItemitem);
	 		Iterator<Itempf> iterator = itempfList.iterator();
	 		while (iterator.hasNext()) {
	 			Itempf itempf = new Itempf();
	 			itempf = iterator.next();
	 			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
	 				if((wsaaToday.toInt() >= Integer.parseInt(itempf.getItmfrm().toString()) )
	 						&& wsaaToday.toInt() <= Integer.parseInt(itempf.getItmto().toString())){					
	 					t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	 					itemFound = true;
	 				}
	 			}else{
	 				t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	 				itemFound = true;					
	 			}				
	 		}		
	 	}
	 	if (!itemFound) {
	 		syserrrec.params.set(t3629rec.t3629Rec);
	 		syserrrec.statuz.set(hl63);
	 		fatalError600();		
	 	}		
	 }

	 
	/**
	 * <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */

	protected void preScreenEdit() {
		/* PRE-START */
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}
		return;
		/* PRE-EXIT */

	}

	protected void screenEdit2000() {
		screenIo2010();		
		if(isEQ(wsspcomn.flag, "I")){
			if(isEQ(wsspcomn.sbmaction, "E")){	
				if(isNE(wsaaSuspCurrency,sv.rpcurr)) {
					conlinkrec.clnk002Rec.set(SPACES);         
					conlinkrec.currOut.set(wsaaSuspCurrency);
					conlinkrec.currIn.set(sv.rpcurr);
					conlinkrec.amountIn.set(sv.repaymentamt);
					convertCurrency();
					wsaarepaymentamt.set(conlinkrec.amountOut);
				} else {
					wsaarepaymentamt.set(sv.repaymentamt);
				}
					if (isLT(sv.susbalnce, wsaarepaymentamt)) {
						sv.susbalnceErr.set(e961);	
						wsspcomn.edterror.set("Y");	
						return;
					}	

			}			
			if(isEQ(sv.crcind, "X") && isEQ(sv.repymop, "D")){
				sv.crcind.set(SPACES);
				sv.ddind.set("X");
			}
			if(isEQ(sv.ddind, "X") && isEQ(sv.repymop, "R")){
				sv.ddind.set(SPACES);
				sv.crcind.set("X");
			}
			if(isNE(sv.crcind, "X") && isNE(sv.ddind, "X")){
				if(isEQ(wsspcomn.sbmaction , "D"))
						sv.refcode.set("1");
				if(isEQ(wsspcomn.sbmaction , "E"))
						sv.refcode.set("2");
			}
						
			return;
		}
		rpdetBulkInsList = new ArrayList<Rpdetpf>();//rpdetpf
		sv.remngplprncpl.set(ZERO);
		sv.remngplint.set(ZERO);
		sv.remngaplprncpl.set(ZERO);
		sv.remngaplint.set(ZERO);
		sv.repayment.set(ZERO);
				
		subfileStart();
		if (isEQ(sv.repaymentamt, 0)) {
			sv.repaymentamtErr.set(rrel);
		}
		if (isEQ(sv.repymop, SPACES)) {
			sv.repymopErr.set(f020);			
		}
		
		if(isEQ(wsspcomn.flag, "P")){
			if(isEQ(sv.repymop, "D") && mandref.isEmpty()){		
				sv.crcind.set(SPACES);
				/*if(isEQ(sv.crcind, "X")){
					sv.crcind.set(SPACES);
					sv.crcindErr.set("E944");	
				}*/
					sv.ddind.set("X");
			}
			else if((isEQ(sv.ddind, "X") && isEQ(sv.repymop, "R") && !mandref.isEmpty())){
					sv.ddindErr.set("E944");	
					sv.ddind.set(SPACE);
				}
			if(isEQ(sv.repymop, "R") && mandref.isEmpty()){	
				sv.ddind.set(SPACES);
				/*if(isEQ(sv.ddind, "X")){
					sv.ddind.set(SPACES);
					sv.ddindErr.set("E944");
				}*/
					sv.crcind.set("X");
				}
			else if((isEQ(sv.crcind, "X") && isEQ(sv.repymop, "D") && !mandref.isEmpty())){
					sv.crcindErr.set("E944");	
					sv.crcind.set(SPACE);
				}
			if(isEQ(sv.ddind, "X") || isEQ(sv.crcind, "X") ){
				if(isEQ(sv.repymop, "C") &&  !mandref.isEmpty() ){
				sv.ddindErr.set("E944");	
				sv.crcindErr.set("E944");	
				sv.ddind.set(SPACE);
				sv.crcind.set(SPACE);
				}else if(isEQ(sv.repymop, "C") &&  mandref.isEmpty() ){
					sv.ddind.set(SPACE);
					sv.crcind.set(SPACE);
				}
			}
		}
		
				
		if(isGT(sv.repaymentamt,ZERO))
		if (isEQ(sv.repymop, "C")) {
			conlinkrec.clnk002Rec.set(SPACES);         
			conlinkrec.currOut.set(wsaaSuspCurrency);
			conlinkrec.currIn.set(sv.rpcurr);
			conlinkrec.amountIn.set(sv.repaymentamt);
			convertCurrency();
			if (isLT(sv.susbalnce, conlinkrec.amountOut)) {
				sv.susbalnceErr.set(e961);				
			}
		}		
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return;
		}
		
		recalcRemainingFields();

	}
	protected void recalcRemainingFields() {
	sv.remngplprncpl.set(totalPlPrinciple);
	sv.remngplint.set(totalPlint);
	sv.remngaplprncpl.set(totalAplPrinciple);
	sv.remngaplint.set(totalAplint);
		
	wsaarepaymentamt = new ZonedDecimalData(17, 2, sv.repaymentamt.toDouble());
	isFullyPaid = true;
	isFirstTime = true;
	while (!(isEQ(scrnparams.statuz, varcom.endp))) {
		
		validatePL();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			break;
		}
		scrnparams.function.set(varcom.srdn);
		subfileIo4400();
	}
	
	if (isEQ(sv.errorIndicators, SPACES)) {			
		subfileStart();
		while (!(isEQ(scrnparams.statuz, varcom.endp))) {
			
			validateAPL();			
			scrnparams.function.set(varcom.srdn);
			subfileIo4400();
		}
		
	}
}
	

	protected void subfileIo4400() {

		processScreen("SD5H1", sv);
		if (isNE(scrnparams.statuz, varcom.oK) && isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	protected void updateErrorIndicators2670() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*
		 * scrnparams.function.set(varcom.supd); screenIo2010();
		 * READ-NEXT-MODIFIED-RECORD scrnparams.function.set(varcom.srdn);
		 * screenIo2010();
		 */
	}

	protected void screenIo2010() {
		/* CALL 'SD5H1IO' USING SCRN-SCREEN-PARAMS */
		/* SH593-DATA-AREA */
		/* SH593-SUBFILE-AREA. */
		/* Screen errors are now handled in the calling program. */
		/* PERFORM 200-SCREEN-ERRORS. */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.rolu)) {			
			loadSubfile();			
			wsspcomn.edterror.set(SPACES);
			exit2020();
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");			
		}
	}

	protected void validatePL() {
		boolean isSuffRepmtAmt = false;
		conlinkrec.clnk002Rec.set(SPACES);         
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.currIn.set(sv.rpcurr);
		conlinkrec.amountIn.set(sv.repaymentamt);
		convertCurrency();
		checkTotal.set(ZERO);														//ILB-1339
		if (isSuffRepmtAmt == false && isFirstTime) {

			if (isEQ(sv.loanType, "P")) {
				checkTotal.set(ZERO);												//ILB-1339
				compute(checkTotal, 2).set((add(sv.hpndint, sv.hacrint)));			//ILB-1339
				isFirstTime = false;				
				if (isLT(conlinkrec.amountOut, checkTotal)) {
					isSuffRepmtAmt = true;
				} 
			}
			if(isEQ(loantypeStatus,false)){
				if (isEQ(sv.loanType, "A") && isSuffRepmtAmt == false) {
					checkTotal.set(ZERO);															//ILB-1339
					compute(checkTotal, 2).set(add(add(sv.hpndint, sv.hacrint),sv.hcurbal));		//ILB-1339
					isFirstTime = false;
					if (isLT(conlinkrec.amountOut, checkTotal)) {
						isSuffRepmtAmt = true;
					}
				}
			}

		}

		if (isSuffRepmtAmt == true ) {
			sv.repaymentamtErr.set(rrem);
			return;
		}
		
		if(totalPlList.contains(sv.loanNumber)){			
			calculatePLInt(add(sv.hacrint,sv.hpndint).getbigdata());
			rpdetpfSetdetails(); 
		}
	}
	protected void validateAPL() {
		if(totalAplList.contains(sv.loanNumber)){			
			calculateAPL(add(sv.hacrint,sv.hpndint,sv.hcurbal).getbigdata());
			rpdetpfSetdetails(); 
		}
	}
	
	protected void calculatePLInt(BigDecimal interest) {
		if (isGTE(wsaarepaymentamt , interest) && isFullyPaid) {
			compute(wsaarepaymentamt,2).set(sub(wsaarepaymentamt,interest));
			compute(sv.remngplint,2).set(sub(sv.remngplint.getbigdata(),interest));
			calculatePLPrin();			
		} else {
			isFullyPaid = false;
		}
	}
	
	
	protected void calculatePLPrin() {
		if (isGT(wsaarepaymentamt , 0)) {
			if (isLT(wsaarepaymentamt , sv.hcurbal)) {
				compute(sv.remngplprncpl,2).set(sub(sv.remngplprncpl,wsaarepaymentamt));
				//compute(sv.repayment,2).set(add(sv.repayment,wsaarepaymentamt));
				compute(wsaarepaymentamt,2).set(ZERO);
			} else {
				compute(sv.remngplprncpl,2).set(sub(sv.remngplprncpl,sv.hcurbal));
				//compute(sv.repayment,2).set(add(sv.repayment,sv.hcurbal));
				compute(wsaarepaymentamt,2).set(sub(wsaarepaymentamt,sv.hcurbal));
			}
			
		}else {
			isFullyPaid = false;
		}
		
	}

	
	protected void calculateAPL(BigDecimal APLintprin) {
		
		if (isGTE(wsaarepaymentamt , APLintprin) && isFullyPaid) {
			compute(wsaarepaymentamt,2).set(sub(wsaarepaymentamt,APLintprin));
			compute(sv.remngaplint,2).set(sub(sv.remngaplint,add(sv.hacrint,sv.hpndint)));
			compute(sv.remngaplprncpl,2).set(sub(sv.remngaplprncpl,sv.hcurbal));			
		} else {
			isFullyPaid = false;
		}
		
		
	}

	protected void rpdetpfSetdetails(){

		rpdetpf = new Rpdetpf();//rpdetpf
		rpdetpf.setLoannum(sv.loanNumber.toInt());
		rpdetpf.setChdrnum(sv.chdrnum.toString());		
		rpdetpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		rpdetpf.setChdrpfx(chdrpf.getChdrpfx());
		rpdetpf.setRepayamt(0);
		rpdetpf.setRepaystatus(" ");
		rpdetpf.setApprovaldate(99999999);
		rpdetpf.setRepaymethod(sv.repymop.toString());
		rpdetpf.setTranno(wsaaTranno.toInt());
		rpdetpf.setLoantype(sv.loanType.toString());
		rpdetpf.setEfftdate(sv.loanstdate.toInt());
		rpdetpf.setLoancurr(sv.cntcurr.toString());
		rpdetpf.setInterest(sv.intrstpercentage.toInt());
		rpdetpf.setPrinamnt(sv.hprincipal.toDouble());
		rpdetpf.setCurramnt(sv.hcurbal.toDouble());
		rpdetpf.setAccrdint(sv.hacrint.toDouble());
		rpdetpf.setPendint(sv.hpndint.toDouble());
		rpdetpf.setStatus(pendingStatus);
		
		rpdetBulkInsList.add(rpdetpf);
	}
	
	 protected void nextProgram4010(){
		
		 if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
					chdrlnbIO.setFunction(varcom.retrv);
					SmartFileCode.execute(appVars, chdrlnbIO);
					if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(chdrlnbIO.getParams());
						fatalError600();
					}
		 }
		 
		 	gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			wsspcomn.nextprog.set(wsaaProg);
		 /*  If first time into this section (stack action blank)*/
			/*  save next eight programs in stack*/
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
				wsaaX.set(wsspcomn.programPtr);
				wsaaY.set(1);
				for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
					saveProgram4100();
				}
			}
			if(isEQ(sv.crcind, "X")){
				gensswrec.function.set("A");
				sv.crcind.set("?");
				 ReturnIO4500();
				callGenssw4300();
				return ;
			}
			if (isEQ(sv.crcind, "?")){
				
				if (isNE(chdrlnbIO.getMandref(), SPACES) || isEQ(wsspcomn.flag, "I")) {
					mandref  = chdrlnbIO.getMandref().toString();
					sv.crcind.set("+");					
					}
				else{
					sv.crcind.set(SPACES);
					}
			}
		
			if(isEQ(sv.ddind, "X")){
				gensswrec.function.set("B");
				sv.ddind.set("?");
				ReturnIO4500();
				callGenssw4300();
				return ;
			}
			if (isEQ(sv.ddind, "?")){
				
				if (isNE(chdrlnbIO.getMandref(), SPACES) || isEQ(wsspcomn.flag, "I")) {
					mandref  = chdrlnbIO.getMandref().toString();
					sv.ddind.set("+");					
					}
				else{
					sv.ddind.set(SPACES);
					}
			}
			checkSelections4050();
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
				restoreProgram4200();
			}
			
			
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
			else {
				
				if(isEQ(wsspcomn.flag, "I")){
					if(isEQ(sv.ansrepy, "Y")){				
						
						if(isEQ(wsspcomn.sbmaction, "D")){
							chdrpfDAO.updateChdrTrannoByChdrnum(chdrBulkUpdtList);
							ptrnpfDAO.updatePtrnByTranNo(ptrnUpdpf);// update valid flag=2 in ptrn
							deleteRpdetpfRcd();	// delete from rpdetpf
							deleteRplnpf();// delete from rplnpf	
							reversalPosting();
							reversalLoan();															
							
						}else if(isEQ(wsspcomn.sbmaction, "E")){
							chdrpfDAO.updateInvalidChdrRecord(chdrBulkUpdtList);
							chdrpfDAO.insertChdrValidRecord(chdrBulkInstList);
							rplnpfDAO.updateRploanRecord(rplnpf);
							rpdetpfDAO.updateRpdetRecordList(rpdetBulkupList);
						}
						writePtrn();
						releaseSoftlock();
						wsspcomn.programPtr.add(1);						
						}
					else if(isEQ(sv.ansrepy, "N") || isEQ(sv.ansrepy, "X")){
						releaseSoftlock();
						wsspcomn.programPtr.add(1);						
						}
					else{
						wsspcomn.nextprog.set(scrnparams.scrname);
					}
					
				}else{
					chdrpfDAO.updateChdrTrannoByChdrnum(chdrBulkUpdtList);
					boolean result ;
					if(isEQ(wsspcomn.flag, "P")){					
						rplnpfDAO.insertRploanpf(rplnpf);
						rpdetpfDAO.insertRpdetRecord(rpdetBulkInsList); // insert in rpdetpf table
						
						if(isNE(sv.repymop,"C")){
							result = bextpfDAO.insertBextPF(bextBulkInsList);
							
							if(!result) { 
						 		fatalError600();
						 		}else bextBulkInsList.clear();
						}
						releaseSoftlock();					 	
					}
					writePtrn();
					
					wsspcomn.programPtr.add(1);
					rlseRecord();
			}
				
					

			}
		}
	 
	 protected void releaseSoftlock()
		{	
			sftlockrec.function.set("UNLK");
			sftlockrec.company.set(chdrpf.getChdrcoy());
			sftlockrec.enttyp.set("CH");
			sftlockrec.entity.set(chdrpf.getChdrnum());
			sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);			
			sftlockrec.user.set(varcom.vrcmUser);
			callProgram(Sftlock.class, sftlockrec.sftlockRec);
			if (isNE(sftlockrec.statuz,varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
	 
	protected void callGenssw4300(){
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, varcom.oK)
					&& isNE(gensswrec.statuz, varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			if (isEQ(gensswrec.statuz, varcom.mrnf)) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
				scrnparams.errorCode.set(h093);
				wsspcomn.nextprog.set(scrnparams.scrname);
				return ;
			}

			compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
			wsaaY.set(1);

			for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
				loadProgram4400();
			}

				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
				wsspcomn.programPtr.add(1);
			
		}
	 protected void ReturnIO4500()
		{
		 	chdrlnbIO.setChdrcoy(wsspcomn.company);
			chdrlnbIO.setChdrnum(sv.chdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			if (isNE(sv.chdrnum,SPACES)) {
				SmartFileCode.execute(appVars, chdrlnbIO);
			}
			else {
				chdrlnbIO.setStatuz(varcom.mrnf);
			} 
			chdrlnbIO.setFunction("KEEPS");
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			} 
			payrIO.setChdrcoy(wsspcomn.company);
			payrIO.setChdrnum(sv.chdrnum);
			payrIO.setPayrseqno(1);
			payrIO.setFunction(varcom.readr);
			if (isNE(sv.chdrnum,SPACES)) {
				SmartFileCode.execute(appVars, payrIO);
			}
			else {
				payrIO.setStatuz(varcom.mrnf);
			} 
			payrIO.setFunction("KEEPS");
			payrIO.setFormat(payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			} 
          
		}
	
	 protected void checkSelections4050()
		{
		 	if (isNE(sv.ddind, "X")
					&& isNE(sv.ddind, "+")
					&& isNE(sv.ddind, SPACES)) {
				sv.ddindErr.set(g620);
			}
			if (isNE(sv.crcind, "X")
					&& isNE(sv.crcind, "+")
					&& isNE(sv.crcind, SPACES)) {
				sv.crcindErr.set(g620); 
			}
			
		}
	protected void rlseRecord(){
		
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.set(SPACE);
		
		
		payrIO.setFunction(varcom.rlse);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		payrIO.set(SPACE);
	}
	
	protected void loadProgram4400()
	{
		/*RESTORE1*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT1*/
	}
	protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}
	
	protected void exit2020() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	protected void convertCurrency()
	{		
		/* call the convert subroutine*/
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("REAL");
		
		conlinkrec.cashdate.set(wsaaMaxDate);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(wsspcomn.company);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}
	protected void subfileStart()	
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("SD5H1", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	protected void suspencePost(){
		wsaaEntity.set(wsaaEntity.left(8));
		if(isEQ(wsaaBaseIdx, 0) && isNE(sv.repayment,ZERO)){
			
			lnrepayrec.docamt.set(sv.repayment);
			lnrepayrecDetails(13);
			if (isNE(lnrepayrec.statuz, varcom.oK)) {
				syserrrec.subrname.set("TRNCURBAL");
				syserrrec.statuz.set(lnrepayrec.statuz);
				fatalError600();
			}			
		}
		if(isEQ(wsaaBaseIdx, 4) && isNE(sv.repayment,ZERO)){
			lnrepayrec.docamt.set(sv.repayment);
			lnrepayrecDetails(9);
			if (isNE(lnrepayrec.statuz, varcom.oK)) {
				syserrrec.subrname.set("TRNCURBAL");
				syserrrec.statuz.set(lnrepayrec.statuz);
				fatalError600();
			}	
		}
	}
}
