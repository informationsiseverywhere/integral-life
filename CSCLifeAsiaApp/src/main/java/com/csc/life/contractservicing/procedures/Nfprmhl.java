/*
 * File: Nfprmhl.java
 * Date: 29 August 2009 23:00:33
 * Author: Quipoz Limited
 * 
 * Class transformed from NFPRMHL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.AcmvldgTableDAM;
import com.csc.life.contractservicing.dataaccess.PrmhenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.reports.Rr51qReport;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Tr51prec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.dataaccess.UtrnrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Auto Premium Holiday
*
*   Non forfeiture routine for Premium Holiday.
*   This subroutine will write record in PRMHPF file when process
*   is successful. For fund value is insufficient to cover number
*   of COI that required by TR51P's set up, Premium Holiday Exception
*   report will be printed out.
*
***********************************************************************
* </pre>
*/
public class Nfprmhl extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(Nfprmhl.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rr51qReport printerFile = new Rr51qReport();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private final String wsaaSubr = "NFPRMHL";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaPrmhenqFnd = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaFundval = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTempval = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaLastcoi = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaMinfundInsuflt = new PackedDecimalData(17, 2);
	private String wsaaOpenPrintFlag = "N";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private ZonedDecimalData wsaaPageCount = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*   Main, standard page heading*/
	private FixedLengthStringData rr51qH01 = new FixedLengthStringData(83);
	private FixedLengthStringData rr51qh01O = new FixedLengthStringData(83).isAPartOf(rr51qH01, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rr51qh01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr51qh01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr51qh01O, 11);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr51qh01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr51qh01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr51qh01O, 53);

		/*  Detail line*/
	private FixedLengthStringData rr51qD01 = new FixedLengthStringData(102);
	private FixedLengthStringData rr51qd01O = new FixedLengthStringData(102).isAPartOf(rr51qD01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rr51qd01O, 0);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(rr51qd01O, 8);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(rr51qd01O, 11);
	private ZonedDecimalData rd01Origamt = new ZonedDecimalData(17, 2).isAPartOf(rr51qd01O, 14);
	private ZonedDecimalData rd01Dura = new ZonedDecimalData(4, 0).isAPartOf(rr51qd01O, 31);
	private ZonedDecimalData rd01Zcurprmbal = new ZonedDecimalData(17, 2).isAPartOf(rr51qd01O, 35);
	private FixedLengthStringData rd01Resndesc = new FixedLengthStringData(50).isAPartOf(rr51qd01O, 52);
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String prmhenqrec = "PRMHENQREC";
	private static final String acmvldgrec = "ACMVLDGREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String tr51p = "TR51P";
	private static final String t5645 = "T5645";
	private AcmvldgTableDAM acmvldgIO = new AcmvldgTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PrmhenqTableDAM prmhenqIO = new PrmhenqTableDAM();
	private UtrnrnlTableDAM utrnrnlIO = new UtrnrnlTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr51prec tr51prec = new Tr51prec();
	private T5645rec t5645rec = new T5645rec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Ovrduerec ovrduerec = new Ovrduerec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		read1120, 
		read1220, 
		nextpRecord1350, 
		exit1390, 
		exit2090
	}

	public Nfprmhl() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		para110();
		exit190();
	}

protected void para110()
	{
		ovrduerec.newSumins.set(ovrduerec.sumins);
		ovrduerec.statuz.set(varcom.oK);
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		readTables1400();
		checkFundval1000();
		if (isLT(wsaaFundval, wsaaMinfundInsuflt)) {
			try {
				printReportLine1500();
				ovrduerec.statuz.set("SKIP");
			} catch (Exception ex) {
				LOGGER.error("Exception occured :-",ex);
			}
		}
		else {
			writePrmh2000();
			processReassurance3000();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void checkFundval1000()
	{
		/*START*/
		wsaaFundval.set(ZERO);
		utrsFundval1100();
		hitsFundval1200();
		getLastcoi1300();
		/*TRIGGER-INSUFF-LET*/
		wsaaMinfundInsuflt.set(ZERO);
		if (isNE(tr51prec.durationa, ZERO)) {
			compute(wsaaMinfundInsuflt, 2).set(mult(wsaaLastcoi, tr51prec.durationa));
			if (isLT(wsaaMinfundInsuflt, 0)) {
				compute(wsaaMinfundInsuflt, 2).set(mult(wsaaMinfundInsuflt, -1));
			}
		}
		/*EXIT*/
	}

protected void utrsFundval1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1110();
				case read1120: 
					read1120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1110()
	{
		utrsIO.setRecKeyData(SPACES);
		utrsIO.setChdrcoy(ovrduerec.chdrcoy);
		utrsIO.setChdrnum(ovrduerec.chdrnum);
		utrsIO.setLife(ovrduerec.life);
		utrsIO.setCoverage(ovrduerec.coverage);
		utrsIO.setRider(ovrduerec.rider);
		utrsIO.setPlanSuffix(ovrduerec.planSuffix);
		utrsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void read1120()
	{
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)
		&& isNE(utrsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(utrsIO.getStatuz());
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isNE(utrsIO.getStatuz(), varcom.oK)
		|| isNE(utrsIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(utrsIO.getChdrnum(), ovrduerec.chdrnum)
		|| isNE(utrsIO.getLife(), ovrduerec.life)
		|| isNE(utrsIO.getCoverage(), ovrduerec.coverage)
		|| isNE(utrsIO.getRider(), ovrduerec.rider)
		|| isNE(utrsIO.getPlanSuffix(), ovrduerec.planSuffix)) {
			return ;
		}
		vprnudlIO.setRecKeyData(SPACES);
		vprnudlIO.setCompany(utrsIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(utrsIO.getUnitType());
		vprnudlIO.setEffdate(ovrduerec.effdate);
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY", "VRTFND", "ULTYPE"); 
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(), varcom.oK)
		|| isNE(vprnudlIO.getCompany(), utrsIO.getChdrcoy())
		|| isNE(vprnudlIO.getUnitVirtualFund(), utrsIO.getUnitVirtualFund())
		|| isNE(vprnudlIO.getUnitType(), utrsIO.getUnitType())) {
			vprnudlIO.setUnitBidPrice(ZERO);
		}
		compute(wsaaTempval, 6).setRounded(mult(utrsIO.getCurrentUnitBal(), vprnudlIO.getUnitBidPrice()));
		wsaaFundval.add(wsaaTempval);
		utrsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read1120);
	}

protected void hitsFundval1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1210();
				case read1220: 
					read1220();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1210()
	{
		hitsIO.setRecKeyData(SPACES);
		hitsIO.setChdrcoy(ovrduerec.chdrcoy);
		hitsIO.setChdrnum(ovrduerec.chdrnum);
		hitsIO.setLife(ovrduerec.life);
		hitsIO.setCoverage(ovrduerec.coverage);
		hitsIO.setRider(ovrduerec.rider);
		hitsIO.setPlanSuffix(ovrduerec.planSuffix);
		hitsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void read1220()
	{
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)
		&& isNE(utrsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(utrsIO.getStatuz());
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		|| isNE(hitsIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(hitsIO.getChdrnum(), ovrduerec.chdrnum)
		|| isNE(hitsIO.getLife(), ovrduerec.life)
		|| isNE(hitsIO.getCoverage(), ovrduerec.coverage)
		|| isNE(hitsIO.getRider(), ovrduerec.rider)
		|| isNE(hitsIO.getPlanSuffix(), ovrduerec.planSuffix)) {
			return ;
		}
		wsaaFundval.add(hitsIO.getZcurprmbal());
		hitsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read1220);
	}

protected void getLastcoi1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1310();
				case nextpRecord1350: 
					nextpRecord1350();
				case exit1390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1310()
	{
		acmvldgIO.setRecKeyData(SPACES);
		acmvldgIO.setRldgcoy(ovrduerec.chdrcoy);
		acmvldgIO.setSacscode(t5645rec.sacscode01);
		acmvldgIO.setSacstyp(t5645rec.sacstype01);
		wsaaRldgChdrnum.set(ovrduerec.chdrnum);
		wsaaRldgLife.set(ovrduerec.life);
		wsaaRldgCoverage.set(ovrduerec.coverage);
		wsaaRldgRider.set(ovrduerec.rider);
		wsaaRldgPlanSuff.set("00");
		acmvldgIO.setRldgacct(wsaaRldgacct);
		acmvldgIO.setOrigcurr(ovrduerec.cntcurr);
		acmvldgIO.setTranno(99999);
		acmvldgIO.setFormat(acmvldgrec);
		acmvldgIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, acmvldgIO);
		if (isNE(acmvldgIO.getStatuz(), varcom.oK)
		&& isNE(acmvldgIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(acmvldgIO.getStatuz());
			syserrrec.params.set(acmvldgIO.getParams());
			fatalError600();
		}
		if (isNE(acmvldgIO.getRldgcoy(), ovrduerec.chdrcoy)
		|| isNE(acmvldgIO.getSacscode(), t5645rec.sacscode01)
		|| isNE(acmvldgIO.getSacstyp(), t5645rec.sacstype01)
		|| isNE(acmvldgIO.getRldgacct(), wsaaRldgacct)
		|| isNE(acmvldgIO.getOrigcurr(), ovrduerec.cntcurr)
		|| isEQ(acmvldgIO.getStatuz(), varcom.endp)) {
			wsaaLastcoi.set(0);
			goTo(GotoLabel.exit1390);
		}
		wsaaTranno.set(acmvldgIO.getTranno());
		wsaaEffdate.set(acmvldgIO.getEffdate());
		wsaaLastcoi.set(acmvldgIO.getOrigamt());
	}

protected void nextpRecord1350()
	{
		acmvldgIO.setFunction(varcom.nextp);
		SmartFileCode.execute(appVars, acmvldgIO);
		if (isNE(acmvldgIO.getStatuz(), varcom.oK)
		&& isNE(acmvldgIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(acmvldgIO.getStatuz());
			syserrrec.params.set(acmvldgIO.getParams());
			fatalError600();
		}
		if (isNE(acmvldgIO.getRldgcoy(), ovrduerec.chdrcoy)
		|| isNE(acmvldgIO.getSacscode(), t5645rec.sacscode01)
		|| isNE(acmvldgIO.getSacstyp(), t5645rec.sacstype01)
		|| isNE(acmvldgIO.getRldgacct(), wsaaRldgacct)
		|| isNE(acmvldgIO.getOrigcurr(), ovrduerec.cntcurr)
		|| isNE(acmvldgIO.getTranno(), wsaaTranno)
		|| isEQ(acmvldgIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(acmvldgIO.getEffdate(), wsaaEffdate)) {
			wsaaLastcoi.add(acmvldgIO.getOrigamt());
		}
		goTo(GotoLabel.nextpRecord1350);
	}

protected void readTables1400()
	{
		start1410();
	}

protected void start1410()
	{
		/*  Read TR51P to get the Premium Holiday Rule*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tr51p);
		itemIO.setItemitem(ovrduerec.cnttype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			tr51prec.tr51pRec.set(itemIO.getGenarea());
		}
		else {
			initialize(tr51prec.tr51pRec);
		}
		/*  Read T5645*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void printReportLine1500()
	{
		start1500();
		setUpHeadingCompany1510();
		setUpHeadingBranch1520();
		setUpHeadingDates1530();
		writeDetail1550();
	}

protected void start1500()
	{
		printerFile.openOutput();
		wsaaOverflow.set("Y");
	}

protected void setUpHeadingCompany1510()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(ovrduerec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		rh01Company.set(ovrduerec.company);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1520()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ovrduerec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(ovrduerec.batcbrn);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		rh01Branch.set(ovrduerec.batcbrn);
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1530()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ovrduerec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
	}

protected void writeDetail1550()
	{
		/* IF FIRST PAGE/OVERFLOW - WRITE STANDARD HEADINGS*/
		if (newPageReq.isTrue()) {
			printerFile.printRr51qh01(rr51qH01, indicArea);
			wsaaOverflow.set("N");
		}
		/* Set up details line*/
		rd01Chdrnum.set(ovrduerec.chdrnum);
		rd01Cnttype.set(ovrduerec.cnttype);
		rd01Cntcurr.set(ovrduerec.cntcurr);
		rd01Origamt.set(wsaaLastcoi);
		rd01Dura.set(tr51prec.durationa);
		rd01Zcurprmbal.set(wsaaFundval);
		rd01Resndesc.set("Insufficient Fund Value");
		/*  Write detail, checking for page overflow*/
		printerFile.printRr51qd01(rr51qD01, indicArea);
		printerFile.close();
	}

protected void writePrmh2000()
	{
		try {
			readPrmh2010();
			appendPrmh2020();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readPrmh2010()
	{
		prmhenqIO.setParams(SPACES);
		prmhenqIO.setChdrcoy(ovrduerec.chdrcoy);
		prmhenqIO.setChdrnum(ovrduerec.chdrnum);
		prmhenqIO.setFrmdate(varcom.vrcmMaxDate);
		prmhenqIO.setFormat(prmhenqrec);
		prmhenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		prmhenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		prmhenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, prmhenqIO);
		if (isNE(prmhenqIO.getStatuz(), varcom.oK)
		&& isNE(prmhenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(prmhenqIO.getParams());
			syserrrec.statuz.set(prmhenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(prmhenqIO.getStatuz(), varcom.oK)
		&& isEQ(prmhenqIO.getChdrcoy(), ovrduerec.chdrcoy)
		&& isEQ(prmhenqIO.getChdrnum(), ovrduerec.chdrnum)
		&& isEQ(prmhenqIO.getTranno(), ovrduerec.tranno)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void appendPrmh2020()
	{
		prmhenqIO.setRecKeyData(SPACES);
		prmhenqIO.setRecNonKeyData(SPACES);
		prmhenqIO.setChdrcoy(ovrduerec.chdrcoy);
		prmhenqIO.setChdrnum(ovrduerec.chdrnum);
		prmhenqIO.setValidflag("1");
		prmhenqIO.setActiveInd("A");
		prmhenqIO.setFrmdate(ovrduerec.ptdate);
		prmhenqIO.setTodate(ovrduerec.premCessDate);
		prmhenqIO.setTranno(ovrduerec.tranno);
		prmhenqIO.setEffdate(datcon1rec.intDate);
		prmhenqIO.setCrtuser(SPACES);
		prmhenqIO.setApind(SPACES);
		if (isEQ(ovrduerec.function, "OLPT")) {
			prmhenqIO.setLogtype("C");
		}
		else {
			prmhenqIO.setLogtype("A");
		}
		prmhenqIO.setBillfreq(SPACES);
		prmhenqIO.setFormat(prmhenqrec);
		prmhenqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, prmhenqIO);
		if (isNE(prmhenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(prmhenqIO.getParams());
			syserrrec.statuz.set(prmhenqIO.getStatuz());
			fatalError600();
		}
	}

protected void processReassurance3000()
	{
		trmreas3010();
	}

protected void trmreas3010()
	{
		wsaaBatckey.batcBatcpfx.set("BA");
		wsaaBatckey.batcBatccoy.set(ovrduerec.chdrcoy);
		wsaaBatckey.batcBatcbrn.set(ovrduerec.batcbrn);
		wsaaBatckey.batcBatcactyr.set(ovrduerec.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec.batcbatch);
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("CHGR");
		trmreasrec.chdrcoy.set(ovrduerec.chdrcoy);
		trmreasrec.chdrnum.set(ovrduerec.chdrnum);
		trmreasrec.life.set(ovrduerec.life);
		trmreasrec.coverage.set(ovrduerec.coverage);
		trmreasrec.rider.set(ovrduerec.rider);
		trmreasrec.planSuffix.set(ovrduerec.planSuffix);
		trmreasrec.cnttype.set(ovrduerec.cnttype);
		trmreasrec.crtable.set(ovrduerec.crtable);
		trmreasrec.polsum.set(ovrduerec.polsum);
		trmreasrec.effdate.set(ovrduerec.ptdate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(ovrduerec.tranno);
		trmreasrec.language.set(ovrduerec.language);
		trmreasrec.billfreq.set(ovrduerec.billfreq);
		trmreasrec.ptdate.set(ovrduerec.ptdate);
		trmreasrec.origcurr.set(ovrduerec.cntcurr);
		trmreasrec.acctcurr.set(ovrduerec.cntcurr);
		trmreasrec.crrcd.set(ovrduerec.crrcd);
		trmreasrec.convUnits.set(ZERO);
		trmreasrec.jlife.set(SPACES);
		trmreasrec.singp.set(ovrduerec.instprem);
		trmreasrec.pstatcode.set(ovrduerec.pstatcode);
		trmreasrec.oldSumins.set(ovrduerec.sumins);
		trmreasrec.newSumins.set(ovrduerec.newSumins);
		trmreasrec.clmPercent.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			fatalError600();
		}
	}

protected void fatalError600()
	{
		start610();
		errorBomb620();
	}

protected void start610()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorBomb620()
	{
		ovrduerec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
