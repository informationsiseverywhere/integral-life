package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:46
 * Description:
 * Copybook name: ACOMCALREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acomcalrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData acomcalRec = new FixedLengthStringData(198);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(acomcalRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(acomcalRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(acomcalRec, 5);
  	public FixedLengthStringData covrLife = new FixedLengthStringData(2).isAPartOf(acomcalRec, 13);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(acomcalRec, 15);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(acomcalRec, 17);
  	public PackedDecimalData covrPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(acomcalRec, 19);
  	public FixedLengthStringData covtLife = new FixedLengthStringData(2).isAPartOf(acomcalRec, 22);
  	public FixedLengthStringData covtCoverage = new FixedLengthStringData(2).isAPartOf(acomcalRec, 24);
  	public FixedLengthStringData covtRider = new FixedLengthStringData(2).isAPartOf(acomcalRec, 26);
  	public PackedDecimalData covtPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(acomcalRec, 28);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(acomcalRec, 31);
  	public PackedDecimalData transactionDate = new PackedDecimalData(8, 0).isAPartOf(acomcalRec, 34);
  	public PackedDecimalData transactionTime = new PackedDecimalData(8, 0).isAPartOf(acomcalRec, 39);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(acomcalRec, 44);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(acomcalRec, 48);
  	public PackedDecimalData payrBtdate = new PackedDecimalData(8, 0).isAPartOf(acomcalRec, 52);
  	public PackedDecimalData payrPtdate = new PackedDecimalData(8, 0).isAPartOf(acomcalRec, 57);
  	public FixedLengthStringData payrBillfreq = new FixedLengthStringData(2).isAPartOf(acomcalRec, 62);
  	public FixedLengthStringData payrCntcurr = new FixedLengthStringData(3).isAPartOf(acomcalRec, 64);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(acomcalRec, 67);
  	public PackedDecimalData chdrOccdate = new PackedDecimalData(8, 0).isAPartOf(acomcalRec, 70);
  	public PackedDecimalData chdrCurrfrom = new PackedDecimalData(8, 0).isAPartOf(acomcalRec, 75);
  	public FixedLengthStringData chdrAgntcoy = new FixedLengthStringData(1).isAPartOf(acomcalRec, 80);
  	public FixedLengthStringData covrJlife = new FixedLengthStringData(2).isAPartOf(acomcalRec, 81);
  	public FixedLengthStringData covrCrtable = new FixedLengthStringData(4).isAPartOf(acomcalRec, 83);
  	public FixedLengthStringData comlvlacc = new FixedLengthStringData(1).isAPartOf(acomcalRec, 87);
  	public FixedLengthStringData basicCommMeth = new FixedLengthStringData(4).isAPartOf(acomcalRec, 88);
  	public FixedLengthStringData bascpy = new FixedLengthStringData(4).isAPartOf(acomcalRec, 92);
  	public FixedLengthStringData rnwcpy = new FixedLengthStringData(4).isAPartOf(acomcalRec, 96);
  	public FixedLengthStringData srvcpy = new FixedLengthStringData(4).isAPartOf(acomcalRec, 100);
  	public PackedDecimalData covtAnnprem = new PackedDecimalData(17, 2).isAPartOf(acomcalRec, 104);
  	public PackedDecimalData covrAnnprem = new PackedDecimalData(17, 2).isAPartOf(acomcalRec, 113);
  	public FixedLengthStringData atmdLanguage = new FixedLengthStringData(1).isAPartOf(acomcalRec, 122);
  	public FixedLengthStringData atmdBatchKey = new FixedLengthStringData(22).isAPartOf(acomcalRec, 123);
  	public FixedLengthStringData singlePremInd = new FixedLengthStringData(1).isAPartOf(acomcalRec, 145);
  	public PackedDecimalData fpcoTargPrem = new PackedDecimalData(17, 2).isAPartOf(acomcalRec, 146);
  	public PackedDecimalData fpcoCurrto = new PackedDecimalData(8, 0).isAPartOf(acomcalRec, 155);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(acomcalRec, 160);
	public PackedDecimalData covtCommPrem = new PackedDecimalData(17, 2).isAPartOf(acomcalRec, 164);
	public PackedDecimalData covrCommPrem = new PackedDecimalData(17, 2).isAPartOf(acomcalRec, 181);


	public void initialize() {
		COBOLFunctions.initialize(acomcalRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acomcalRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}