/*
 * File: P6258at.java
 * Date: 30 August 2009 0:40:10
 * Author: Quipoz Limited
 *
 * Class transformed from P6258AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.util.ObjectUtils;

import com.csc.diary.procedures.Dryproces;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZrapTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdbilTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.dao.impl.CovrpfDAOImp;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.LextbrrTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   PAY-TO-DATE ADVANCE
*
*   This is a subsystem to be called after a WINDBACK. It allows
*   the user to advance forward to a specified point, make
*   alterations, advance forward again and make alterations,
*   before being passed through the renwals subsytem to bring
*   the contract up-to-date.
*
*  Almost a direct copy of the first three batch programs
*  within renewals.
*                    Re-Rate.
*                    Billing.
*                    Collection.
*
*  AT MODULE
*
*
*    Perform
*            Perform Re-Rate
*            Perform Billing
*            Perform Collection
*
*    Until frequency number of times
*       or the amount in suspense does not cover billing.
*
*   ! Within billing a bill to the punter should not be
*     created.
*     But, the suspense account should be checked to see
*     whether enough money exists to pay the bill - if
*     not then processing should stop.
*
*  Rerate of Instalment Premiums.
*
*  This is to recalculate any contract premiums due for revision
*  prior to billing. Lead times (billing in
*  advance periods) are established and generic subroutines are
*  called to recalculate premiums and components are rescheduled
*  for re-rating again if applicable.
*
*  The program flow is as follows:-
*
*              If WSAA-PREM-DIFF not = 0
*              (Total of difference of new and old premiums)
*                  Update CHDRLIF with CHDRLIF-VALIDFLAG = 2.
*
*         Read a coverage .
*         Validate the coverage/rider status against T5679.
*         If invalid status read the next related COVR.
*
*         Once found calculate
*              WSAA-BILL-DATE = Bill to date.
*
*          If COVR-RRTDAT > WSAA-BILL-DATE
*              Goto read next COVR record.
*
*          If COVR-PCESDTE = COVR-RRTDAT(Cessation = Re-rate)
*              Goto next section(Update COVR).
*
*          Read T5687 for Premium calculation method and Re-calc
*          frequency.
*
*          If no method found
*              Goto next section(Update COVR).
*
*          Read LIFERNL to see if there is a joint life.
*          (Joint life existed implies joint life contract)
*          If joint life indicator not = 'N' and
*             joint life existed
*              Use joint life premium calculation method to access
*              T5675 to get the Calculation subroutine
*          Else
*              Use single life premium calculation method to acces
*              T5675 to get the Calculation subroutine.
*
*          Call calculation subroutine.
*
*          Recalculate, reschedule next rerating dates and update  OVR.
*
*          Read and hold COVR record.
*
*          If COVR-PCESDTE = COVR-RRTDAT(Cessation = Re-rate)
*              Move 'PU' to PSTATCODE and calculate
*              WSAA-PREM-DIFF = Instalment premium * -1
*              Rewrite COVR record.
*
*          Set VALIDFLAG to '2' and rewrite with the current to da e
*          set to (WSAA-BILL-DATE - 1)
*
*          Set up new COVR record details.
*
*          Set VALIDFLAG to '1', add 1 to TRANNO and update the
*          current from date to WSAA-BILL-DATE and current to date
*          as VRCM-MAX-DATE.
*
*          Read LEXTBRR(Option/extra loading) for any unexpired
*          records so that rescheduling can be done if required.
*
*          If T5687-RECALC-FREQ not = 0
*              Calculate WSAA-RATE-FROM as rerate from date
*              incremented by this frequency.
*              If this new date is less than any current LEXT
*              expiry date
*                  Move this new date to COVR-RERATE-DATE and
*                  COVR-RERATE-FROM-DATE
*              Else
*                  Move the LEXT expiry date to COVR-RERATE-DATE.
*
*          Adjust the calculated premium for plan polices.
*
*          Calculate WSAA-INST-PREM = CPRM-CALC-PREM / POLINC.
*          If plan suffix = 0
*              Calculate WSAA-INST-PREM = WSAA-INST-PREM * POLSUM.
*          Calculate WSAA-PREM-DIFF = WSAA-PREM-DIFF +
*                                     WSAA-INST-PREM - COVR-SINGP
*          (where COVR-SINGP  is the old coverage/rider instalment
*           premium)
*
*          Set COVR-SINGP to WSAA-INST-PREM.
*          Write this new COVR record.
*
*       If WSAA-PREM-DIFF not = 0
*       (Total of the differences of new and old premiums)
*           Update CHDRLIF with CHDRLIF-VALIDFLAG = 2.
*
*       End of Re-Rate.
*
*  Billing.
*  -------
*  This bills  any premiums due for a contract. It also produces
*  instalment records for outstanding payments.
*
* Read  the transaction status codes from T5679.
*
* Calculate WSAA-BILL-DATE as the effective date. Move
* CHDR-BTDATE to WSAA-BTDATE.
*
*
* Read SACSPF with logical  view  SACSRNL  using subaccount and
* key from T5645 line  '01'  to  get  suspense  balance.  Using
* subaccount code as the item  access  T3695  to check the sign
* for this account, if  negative multiply the suspense value by
* -1. Move the suspense balance amount to WSAA-SUSP-AVAIL.
*
* NO MEDIA RECORDS SHOULD BE PRODUCED !.
* ONLY adjust the amount of suspense premium available.
*
* Zeroise WSAA-TOT-AMOUNT and perform Create LINS records.
*
*
* 3.) Create LINS records.
*
*
* Firstly check if  the contract record we are dealing with has
* the correct premium  for  the instalment date; if not we will
* </pre>
*/
public class P6258at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6258");
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaZbinstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaZlinstprem = new ZonedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaJanb = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaRecalcDate = new ZonedDecimalData(8, 0).init(0);
	private ZonedDecimalData wsaaStoreEffdate = new ZonedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaReplaceAnb = new FixedLengthStringData(1).init("N");
	private Validator replaceAnb = new Validator(wsaaReplaceAnb, "Y");
	private String wsaaAllCovExp = "Y";
	private String wsaaAllRidExp = "Y";

		/* WSAA-MISCELLANEOUS-FLAGS */
	private FixedLengthStringData wsaaCoverExit = new FixedLengthStringData(1);
	private Validator wsaaCoverEof = new Validator(wsaaCoverExit, "Y");
	private ZonedDecimalData wsaaRerateDate = new ZonedDecimalData(8, 0).init(0);
		/* WSAA-FIELDS */
	private FixedLengthStringData wsaaRtrnwfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRateFrom = new ZonedDecimalData(8, 0).init(0);

		/* WSAA-COVERAGE-PREMIUMS */
	private FixedLengthStringData[] wsaaLifeLevel = FLSInittedArray (10, 3366);
	private FixedLengthStringData[][] wsaaCoverageLevel = FLSDArrayPartOfArrayStructure(99, 34, wsaaLifeLevel, 0);
	private ZonedDecimalData[][] wsaaCovrInstprem = ZDArrayPartOfArrayStructure(17, 2, wsaaCoverageLevel, 0);
	private ZonedDecimalData[][] wsaaCovrSingp = ZDArrayPartOfArrayStructure(17, 2, wsaaCoverageLevel, 17);
	private ZonedDecimalData wsaaC = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaL = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCoverageNum = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaLifeNum = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaNetBillamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaNetCbillamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0).init(0);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
	private String wsaaNotEnoughCash = "N";

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private PackedDecimalData wsaaTaxEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWaiveAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfee = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaTr517Contitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaWopComponent = new FixedLengthStringData(4);
	private String wsaaCovrValid = "";
	private FixedLengthStringData wsaaWaiveCntf = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaWaiveItself = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaWaiveCrtables = new FixedLengthStringData(800);
	private FixedLengthStringData[] wsaaWaiveCrtable = FLSArrayPartOfStructure(200, 4, wsaaWaiveCrtables, 0);

		/* AT Request storage.*/
	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrimaryKey, 9, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(199);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 5);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 9);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 13);
	private PackedDecimalData wsaaAdvptdat = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 17);
	private PackedDecimalData wsaaNoOfFreqs = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 22).setUnsigned();
	private PackedDecimalData wsaaUnitEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 26);
	private PackedDecimalData wsaaApaRequired = new PackedDecimalData(15, 2).isAPartOf(wsaaTransArea, 31);
	private FixedLengthStringData filler2 = new FixedLengthStringData(160).isAPartOf(wsaaTransArea, 39, FILLER).init(SPACES);

	private FixedLengthStringData wsaaAtmdBatchKey = new FixedLengthStringData(19);
	private FixedLengthStringData atrtBatchKey = new FixedLengthStringData(19).isAPartOf(wsaaAtmdBatchKey, 0);
	private FixedLengthStringData atrtPrefix = new FixedLengthStringData(2).isAPartOf(atrtBatchKey, 0);
	private FixedLengthStringData atrtCompany = new FixedLengthStringData(1).isAPartOf(atrtBatchKey, 2);
	protected FixedLengthStringData atrtBranch = new FixedLengthStringData(2).isAPartOf(atrtBatchKey, 3);
	private FixedLengthStringData atrtAcctPerd = new FixedLengthStringData(5).isAPartOf(atrtBatchKey, 5);
	protected PackedDecimalData atrtAcctYear = new PackedDecimalData(4, 0).isAPartOf(atrtAcctPerd, 0);
	protected PackedDecimalData atrtAcctMonth = new PackedDecimalData(2, 0).isAPartOf(atrtAcctPerd, 3);
	protected FixedLengthStringData atrtTransCode = new FixedLengthStringData(4).isAPartOf(atrtBatchKey, 10);
	protected FixedLengthStringData atrtBatch = new FixedLengthStringData(5).isAPartOf(atrtBatchKey, 14);
	private PackedDecimalData wsaaCurrentPayuptoDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBtdate = new PackedDecimalData(8, 0);
	protected PackedDecimalData wsaaBillcd = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotAmount = new PackedDecimalData(17, 2);
	protected String wsaaOldPayr = "";
	private PackedDecimalData wsaaSequenceNo = new PackedDecimalData(3, 0);
	protected FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30);
		/* WSAA-BILLFREQ-VAR */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(filler3, 0).setUnsigned();

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaItemCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);
	private ZonedDecimalData wsaaFrequency = new ZonedDecimalData(3, 0);
		/* WSAA-DATES */
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaTerm, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermLeftInteger = new ZonedDecimalData(6, 0).isAPartOf(filler4, 0).setUnsigned();
	private ZonedDecimalData wsaaTermLeftRemain = new ZonedDecimalData(5, 0).isAPartOf(filler4, 6).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaCompayKept = new PackedDecimalData(13, 2);
	private ZonedDecimalData wsaaAgentKept = new ZonedDecimalData(8, 0).setPattern("ZZZZZZZZ");
	private ZonedDecimalData wsaaTermRan = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaBascpySubprog = new FixedLengthStringData(7).init(SPACES);
	private FixedLengthStringData wsaaSrvcpySubprog = new FixedLengthStringData(7).init(SPACES);
	private FixedLengthStringData wsaaRnwcpySubprog = new FixedLengthStringData(7).init(SPACES);
		/* WSAA-COMM-TOTALS */
	private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2).init(0);

	protected FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	protected FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	protected FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	protected FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	protected FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	protected ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	protected ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

		/* WSAA-T5645
		 03  WSAA-STORED-T5645           OCCURS 4.
		 03  WSAA-STORED-T5645           OCCURS 5.               <018>*/
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (15, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
		/* WSAA-T5645-32TH-VALUE */
	private ZonedDecimalData wsaaT5645Cnttot32 = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaT5645Glmap32 = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaT5645Sacscode32 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sacstype32 = new FixedLengthStringData(2);
	private static final String wsaaT5645Sign32 = "";
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTol2 = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaLinsBtdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLinsBtdateFmt = new FixedLengthStringData(8).isAPartOf(wsaaLinsBtdate, 0, REDEFINE);
	private ZonedDecimalData wsaaLinsDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLinsBtdateFmt, 6).setUnsigned();
	private ZonedDecimalData wsaaPuDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaPuDateFmt = new FixedLengthStringData(8).isAPartOf(wsaaPuDate, 0, REDEFINE);
	private ZonedDecimalData wsaaPuDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaPuDateFmt, 6).setUnsigned();
	private ZonedDecimalData wsaaBtdateNew = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEomDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaEomDateFmt = new FixedLengthStringData(8).isAPartOf(wsaaEomDate, 0, REDEFINE);
	private ZonedDecimalData wsaaEomYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEomDateFmt, 0).setUnsigned();
	private FixedLengthStringData wsaaEomYearFmt = new FixedLengthStringData(4).isAPartOf(wsaaEomYear, 0, REDEFINE);
	private ZonedDecimalData wsaaCenturyYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomYearFmt, 2).setUnsigned();
	private ZonedDecimalData wsaaEomMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 4).setUnsigned();
	private ZonedDecimalData wsaaEomDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 6).setUnsigned();
	private static final String wsaaDaysInMonthNorm = "312831303130313130313031";
	private static final String wsaaDaysInMonthLeap = "312931303130313130313031";
	private FixedLengthStringData wsaaDaysInMonth = new FixedLengthStringData(24);

	private FixedLengthStringData wsaaDaysInMonthRed = new FixedLengthStringData(24).isAPartOf(wsaaDaysInMonth, 0, REDEFINE);
	private ZonedDecimalData[] wsaaMonthDays = ZDArrayPartOfStructure(12, 2, 0, wsaaDaysInMonthRed, 0, UNSIGNED_TRUE);
	private String wsaaLeapYear = "";
	private String wsaaEndOfMonth = "";
	private FixedLengthStringData wsaaEom1stDate = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(11, 5);

	private FixedLengthStringData wsaaFreqFactorFmt = new FixedLengthStringData(11).isAPartOf(wsaaFreqFactor, 0, REDEFINE);
	private ZonedDecimalData wsaaFreqDecimal = new ZonedDecimalData(5, 5).isAPartOf(wsaaFreqFactorFmt, 6);
	private PackedDecimalData wsaaResult = new PackedDecimalData(5, 0).setUnsigned();
	private String wsaaUpdateFlag = "";
	private PackedDecimalData wsaaPtdate = new PackedDecimalData(8, 0);
	private String wsaaAgtTerminatedFlag = "";
	private String wsaaToleranceFlag = "";

		/*                                                         <DRYAPL>*/
	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfq, 0, REDEFINE).setUnsigned();
	private static final int wsaaAgcmIxSize = 100;
	private ZonedDecimalData wsaaAgcmIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIa = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private static final int wsaaAgcmIySize = 500;
	private ZonedDecimalData wsaaAgcmIy = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();

		/* WSAA-AGCM-SUMMARY */
	private FixedLengthStringData[] wsaaAgcmRec = FLSInittedArray (100, 8515);
	private FixedLengthStringData[] wsaaAgcmChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgcmRec, 0);
	private FixedLengthStringData[] wsaaAgcmChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAgcmRec, 1);
	private FixedLengthStringData[] wsaaAgcmLife = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 9);
	private FixedLengthStringData[] wsaaAgcmCoverage = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 11);
	private FixedLengthStringData[] wsaaAgcmRider = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 13);
	private FixedLengthStringData[][] wsaaAgcmPremRec = FLSDArrayPartOfArrayStructure(500, 17, wsaaAgcmRec, 15);
	private ZonedDecimalData[][] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
	private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator singPrem = new Validator(wsaaPremType, "S");
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String f294 = "F294";
	private static final String g029 = "G029";
	private static final String e103 = "E103";
	private static final String f109 = "F109";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextbrrTableDAM lextbrrIO = new LextbrrTableDAM();
	protected LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	protected PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private TaxdbilTableDAM taxdbilIO = new TaxdbilTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZrapTableDAM zrapIO = new ZrapTableDAM();
	private Rdockey wsaaRdockey = new Rdockey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itdmkey wsaaItdmkey = new Itdmkey();
	protected Varcom varcom = new Varcom();
	protected Syserrrec syserrrec = new Syserrrec();
	private Vflagcpy vflagcpy = new Vflagcpy();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Prasrec prasrec = new Prasrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private T3629rec t3629rec = new T3629rec();
	private T3688rec t3688rec = new T3688rec();
	private T3695rec t3695rec = new T3695rec();
	private T5679rec t5679rec = new T5679rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	protected T5645rec t5645rec = new T5645rec();
	private T5644rec t5644rec = new T5644rec();
	private T5671rec t5671rec = new T5671rec();
	private T6647rec t6647rec = new T6647rec();
	protected T5688rec t5688rec = new T5688rec();
	private T6687rec t6687rec = new T6687rec();
	private T5667rec t5667rec = new T5667rec();
	private T6659rec t6659rec = new T6659rec();
	private Th605rec th605rec = new Th605rec();
	private T7508rec t7508rec = new T7508rec();
	private Tr517rec tr517rec = new Tr517rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Annprocrec annprocrec = new Annprocrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	protected TablesInner tablesInner = new TablesInner();
	//ILIFE-1345 STARTS
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private ZonedDecimalData wsaaFpcoFreqFactor = new ZonedDecimalData(11, 5);
	private T5729rec t5729rec = new T5729rec();
	private PackedDecimalData wsaaT5729Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaMinOverdue = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
	//ILIFE-1345 ENDS
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private long uniqueNumber = 0;
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	
	private Itempf itempf = null;
	private Descpf descpf = null;
	protected Chdrpf chdrpf = null;
    private Payrpf payrpf = null;
    private Ptrnpf ptrnpf = null;
    private Clrfpf clrfpf = null;
    private Linspf linspf = null;
    protected Covrpf covrpf = null;
    private Lifepf lifepf = null;
    private Annypf annypf = null;
    
    private Chdrpf updtChdrpf = null;

    private List<Covrpf> instCovrList = new ArrayList<Covrpf>();
    List<Payrpf> payrInstlist  = null;
    
	private Iterator<Covrpf> iterator;
	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private boolean riskPremflag = false; //ILIFE-7845 
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845 
	//ILIFE-7845  start
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private List<Rskppf> addRiskPremList = new ArrayList<>();
	List<Rskppf> rskppfList = new ArrayList<Rskppf>();
	private Map<String, List<Itempf>> t5567Map;
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 3);
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private T5567rec t5567rec = new T5567rec();
	private ZonedDecimalData wsaapolFee = new ZonedDecimalData(8, 2);
	private Chdrpf chdrpf1 = null;
	private boolean allFrequencyCompleted = false;
	private PackedDecimalData totalPremiumAmount = new PackedDecimalData(17, 2).init(0);
	//ILIFE-7845  start
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		tableLoop1130,
		loopCheck1140,
		callPremiumMeth2102,
		reschedule2103,
		moveWsaaRateFrom2106,
		writeCovr2107,
		cont2108,
		exit2109,
		correctPayrRecord2213,
		totalBilled2214,
		skipInitializeAgcm2303,
		callCovr2304,
		skipBonusWorkbench2304,
		continue2305,
		callAgcmbchio2307,
		servComm2309,
		renlComm2310,
		endComm2311,
		exit2312,
		exit2329,
		fee2332,
		tolerance2333,
		stampDuty2334,
		spare2335,
		taxRelief2336,
		checkTax2337,
		bypassDate2340,
		initialCommEarned2353,
		initialCommPaid2354,
		servCommDue2355,
		servCommEarned2356,
		rnwCommDue2357,
		rnwCommEarned2358,
		initialOvrdCommDue2359,
		initialOvrdCommEarned2359,
		initialOvrdCommPaid2359,
		exit2379,
		b220Call,
		b280Next,
		b290Exit,
		b320Loop,
		b380Next,
		b390Exit,
		b600CalcComponentTax,
		b620ReadTr52e,
		b620ReadNextRecord,
		b620Exit,
		b670Exit
	}

	public P6258at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		for (wsaaFrequency.set(1); !(isGT(wsaaFrequency,wsaaNoOfFreqs)
		|| isEQ(wsaaNotEnoughCash,"Y")); wsaaFrequency.add(1)){
			if(wsaaFrequency.equals(wsaaNoOfFreqs)) {
				allFrequencyCompleted = true;
			}
			reRateBillCollect2000();
		}
		generalHousekeeping3000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxx1ErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxx1ErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case tableLoop1130:
					tableLoop1130();
				case loopCheck1140:
					loopCheck1140();
					transDescription1050();
					updateApa1060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaFirstTime.set("Y");
		comlinkrec.currto.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaAtmdBatchKey.set(atmodrec.batchKey);
		/* Read the contract header record.*/

		
		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsaaCompany.toString(), wsaaChdrnum.toString());
		if (chdrpf == null ) {
			syserrrec.params.set(wsaaCompany.toString().concat(wsaaChdrnum.toString()));
			xxxxFatalError();
		}
		ptrnpf = new Ptrnpf();
		ptrnpf.setPtrneff(chdrpf.getBtdate());

		payrpf = populatePayrpf1010();
				
		if (payrpf==null) {
			syserrrec.params.set(wsaaCompany.toString().concat(wsaaChdrnum.toString()));
			xxxxFatalError();
		}
		/* Read the client role file to get the payer number.              */
		clrfpf = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrpf.getChdrpfx(), payrpf.getChdrcoy(),payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1)), "PY");
		if (clrfpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(payrpf.getChdrcoy().concat(payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1))));
			xxxxFatalError();
		}
		wsaaIndex.set(0);
		wsaaPremDiff.set(0);
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(),1));
		/* Calculate the first date to pay-to date.*/
		/*MOVE SPACES                 TO DTC2-FUNCTION.                */
		/* MOVE CHDRLIF-BTDATE         TO DTC2-INT-DATE-1.              */
		/* MOVE CHDRLIF-BILLFREQ       TO DTC2-FREQUENCY.               */
		/*MOVE PAYR-BTDATE            TO DTC2-INT-DATE-1.         <018>*/
		/*MOVE PAYR-BILLFREQ          TO DTC2-FREQUENCY.          <018>*/
		/*MOVE 1                      TO DTC2-FREQ-FACTOR.             */
		/*CALL 'DATCON2'              USING DTC2-DATCON2-REC.          */
		/*IF DTC2-STATUZ              NOT = O-K                        */
		/*   MOVE DTC2-DATCON2-REC    TO SYSR-PARAMS                   */
		/*   PERFORM XXXX-FATAL-ERROR.                                 */
		/*MOVE DTC2-INT-DATE-2        TO WSAA-CURRENT-PAYUPTO-DATE.    */
		/* Read the Premium Billing file to find first billing date.       */
	
		linspf = linspfDAO.getLinsRecordByCoyAndNumAndflag(payrpf.getChdrcoy(), payrpf.getChdrnum(), payrpf.getValidflag());
		//performance improvement -- Anjali

		/* Call DATCON3 to work out if the first premium was a             */
		/* prorata premium.                                                */
		if (linspf!=null) {
			wsaaLinsBtdate.set(linspf.getInstfrom());
		}
		else {
			wsaaLinsBtdate.set(payrpf.getBtdate());
		}
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(chdrpf.getOccdate());
		datcon3rec.intDate2.set(wsaaLinsBtdate);
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		wsaaFreqFactor.set(datcon3rec.freqFactor);
		/* If the original commence date and the first billing             */
		/* date are a whole billing frequency apart, use the risk          */
		/* commence dates day for future billing, otherwise use            */
		/* the first billing dates day for the first renewal, and          */
		/* there after the first LINS install from dates day.              */
		/* The last two dates are moved previously, before calling         */
		/* DATCON3.                                                        */
		if (isEQ(wsaaFreqDecimal,ZERO)) {
			wsaaLinsBtdate.set(chdrpf.getOccdate());
		}
		/* Check to see if the commence date is at the end of the month.   */
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(chdrpf.getOccdate());
		checkEndOfMonth9000();
		wsaaEom1stDate.set(wsaaEndOfMonth);
		/* Check to see if the 1ST bill date is at the end of the month,   */
		/* and if the month is February.                                   */
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(wsaaLinsBtdate);
		if (isEQ(wsaaEomMonth,2)) {
			checkEndOfMonth9000();
		}
		/* If the commence date was not at the end of the month, and       */
		/* the 1st billing date was in February and at the end of the      */
		/* month, set the billing day to the original commence day.        */
		if (isEQ(wsaaEndOfMonth,"Y")
		&& isEQ(wsaaEom1stDate,"N")) {
			wsaaLinsBtdate.set(chdrpf.getOccdate());
		}
		wsaaCurrentPayuptoDate.set(payrpf.getBtdate());
		calculateNextDate2400();
		/* Get todays date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Read T5645 to get the financial accounting rules                */
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(tablesInner.t5645);
		itempf.setItemitem("P6258");
		itempf = itemDao.getItempfRecord(itempf);
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsaaCompany.toString()).concat(tablesInner.t5645).concat("P6258"));
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Read the contract definition  on T5688.                         */
		List<Itempf> list = itemDao.getItdmByFrmdate(wsaaCompany.toString(), tablesInner.t5688, chdrpf.getCnttype(), chdrpf.getOccdate());
	
		if (list == null || list.size() == 0 ) {
			/*        MOVE ITDM-STATUZ        TO SYSR-STATUZ                 */
			syserrrec.params.set(wsaaCompany.toString().concat(tablesInner.t5688).concat(chdrpf.getCnttype()));
			syserrrec.statuz.set(e308);
			xxxxFatalError();
		} else { //IJTI-462
			t5688rec.t5688Rec.set(StringUtil.rawToString(list.get(0).getGenarea())); //IJTI-462
		} //IJTI-462
		wsaaAllCovExp = "Y";
		wsaaAllRidExp = "Y";
		wsaaRerateDate.set(varcom.vrcmMaxDate);
		/* Zeroise all the commission totals.*/
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaJrnseq.set(0);
		wsaaRnwcpyDue.set(0);
		/* Read T5679 for validation of statii of future COVR's.*/
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(tablesInner.t5679);
		itempf.setItemitem(atrtTransCode.toString());
		itempf = itemDao.getItempfRecord(itempf);
		if (itempf == null ) {
			syserrrec.params.set(wsaaCompany.toString().concat(tablesInner.t5679).concat(atrtTransCode.toString()));
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		wsaaNotEnoughCash = "N";
		/*MOVE 1                      TO WSAA-FREQUENCY.               */
		/* Read T5645 for accounting details. This call will read the 2nd  */
		/* page of the table item(this item has more than one page of      */
		/* accounting details). This first page will be read in individual */
		/* section. The details read here will be stored away in working   */
		/* storage and to be used later on when calling UNITALOC(from      */
		/* T5671).                                                         */
		readT5645Page34200();
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(tablesInner.t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("01");
		itempf = itemDao.getItempfRecordBySeq(itempf);

		if (itempf != null ) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}else{
			t5645rec.t5645Rec.set(SPACES);
		}
		
		wsaaSub1.set(1);
	}

protected void tableLoop1130()
	{
		if (isEQ(t5645rec.sacscode[wsaaSub1.toInt()],SPACES)
		&& isEQ(t5645rec.sacstype[wsaaSub1.toInt()],SPACES)
		&& isEQ(t5645rec.glmap[wsaaSub1.toInt()],SPACES)) {
			wsaaSub1.set(16);
			goTo(GotoLabel.loopCheck1140);
		}
		wsaaT5645Cnttot[wsaaSub1.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
		wsaaT5645Glmap[wsaaSub1.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
		wsaaT5645Sacscode[wsaaSub1.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
		wsaaT5645Sacstype[wsaaSub1.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
		wsaaT5645Sign[wsaaSub1.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		wsaaSub1.add(1);
	}

protected void loopCheck1140()
	{
		if (isLT(wsaaSub1,16)) {
			goTo(GotoLabel.tableLoop1130);
		}
	}

	/**
	* <pre>
	*    MOVE T5645-CNTTOT-01        TO WSAA-T5645-CNTTOT (01). <013>>
	*    MOVE T5645-SACSCODE-01      TO WSAA-T5645-SACSCODE (01).<013>
	*    MOVE T5645-SACSTYPE-01      TO WSAA-T5645-SACSTYPE (01).<013>
	*    MOVE T5645-GLMAP-01         TO WSAA-T5645-GLMAP (01).  <013>>
	*    MOVE T5645-CNTTOT-02        TO WSAA-T5645-CNTTOT (02). <013>>
	*    MOVE T5645-SACSCODE-02      TO WSAA-T5645-SACSCODE (02).<013>
	*    MOVE T5645-SACSTYPE-02      TO WSAA-T5645-SACSTYPE (02).<013>
	*    MOVE T5645-GLMAP-02         TO WSAA-T5645-GLMAP (02).   <013>
	*    MOVE T5645-SIGN-02          TO WSAA-T5645-SIGN (02).    <013>
	*    MOVE T5645-CNTTOT-03        TO WSAA-T5645-CNTTOT (03).  <013>
	*    MOVE T5645-SACSCODE-03      TO WSAA-T5645-SACSCODE (03).<013>
	*    MOVE T5645-SACSTYPE-03      TO WSAA-T5645-SACSTYPE (03).<013>
	*    MOVE T5645-GLMAP-03         TO WSAA-T5645-GLMAP (03).   <013>
	*    MOVE T5645-SIGN-03          TO WSAA-T5645-SIGN (03).    <013>
	*    MOVE T5645-CNTTOT-04        TO WSAA-T5645-CNTTOT (04).  <013>
	*    MOVE T5645-SACSCODE-04      TO WSAA-T5645-SACSCODE (04).<013>
	*    MOVE T5645-SACSTYPE-04      TO WSAA-T5645-SACSTYPE (04).<013>
	*    MOVE T5645-GLMAP-04         TO WSAA-T5645-GLMAP (04).   <013>
	*    MOVE T5645-SIGN-04          TO WSAA-T5645-SIGN (04).    <013>
	*    MOVE T5645-CNTTOT-05        TO WSAA-T5645-CNTTOT (05).  <018>
	*    MOVE T5645-SACSCODE-05      TO WSAA-T5645-SACSCODE (05).<018>
	*    MOVE T5645-SACSTYPE-05      TO WSAA-T5645-SACSTYPE (05).<018>
	*    MOVE T5645-GLMAP-05         TO WSAA-T5645-GLMAP (05).   <018>
	*    MOVE T5645-SIGN-05          TO WSAA-T5645-SIGN (05).    <018>
	*****MOVE T5645-SACSCODE-01      TO WSAA-T5645-SACSCODE-16.
	*****MOVE T5645-SACSTYPE-01      TO WSAA-T5645-SACSTYPE-16.
	*****MOVE T5645-GLMAP-01         TO WSAA-T5645-GLMAP-16.
	*                                                         <V4L001>
	* </pre>
	*/
protected void transDescription1050()
	{
		/*                                                         <V4L001>*/
		/* Get transaction description. Clone from 2300-COLLECTION section.*/
		/*                                                         <V4L001>*/
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), tablesInner.t1688, atrtTransCode.toString(), wsaaCompany.toString(), atmodrec.language.toString());
		if (descpf == null) {
			descpf = new Descpf();
			descpf.setLongdesc("");
		}
		wsaaTransDesc.set(descpf.getLongdesc());
	}

	/**
	* <pre>
	*                                                         <V4L001>
	* </pre>
	*/
protected void updateApa1060()
	{
		/*                                                         <V4L001>*/
		/* This paragraph is required before the 2200-BILLING    so that   */
		/* suspense will have enough money for the amount due. If there    */
		/* has extract suspense for the amount due then NEXT SENTENCE. If  */
		/* Adv Prem Deposit amount (APA) is required to pay the amount due */
		/* then move DELT to withdraw money from APA and put into suspense.*/
		/* Otherwise, take the extra money from suspense and try to post it*/
		/* into APA.                                               <V4L001>*/
		/*                                                         <V4L001>*/
		initialize(rlpdlonrec.rec);
		if (isEQ(wsaaApaRequired,ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isGT(wsaaApaRequired,ZERO)) {
				rlpdlonrec.function.set(varcom.delt);
				rlpdlonrec.prmdepst.set(wsaaApaRequired);
			}
			else {
				rlpdlonrec.function.set(varcom.insr);
				compute(wsaaApaRequired, 2).set(mult(wsaaApaRequired,(-1)));
				rlpdlonrec.prmdepst.set(wsaaApaRequired);
			}
			updateApa1100();
		}
		/* READ TR52D.                                                     */
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrpf.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.tr52d);
		itempf.setItemitem(atrtTransCode.toString());
		itempf = itemDao.getItempfRecord(itempf);
		
		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(chdrpf.getChdrcoy().toString());
			itempf.setItemtabl(tablesInner.tr52d);
			itempf.setItemitem("***");
			itempf = itemDao.getItempfRecord(itempf);
			if (itempf == null) {
				syserrrec.statuz.set("MRNF");
				syserrrec.params.set("IT".concat(chdrpf.getChdrcoy().toString()).concat(tablesInner.tr52d).concat("***"));
				xxxxFatalError();
			}
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

	/**
	* <pre>
	*                                                         <V4L001>
	* </pre>
	*/
protected void updateApa1100()
	{
		
		rlpdlonrec.pstw.set("PSTW");
		rlpdlonrec.chdrcoy.set(chdrpf.getChdrcoy());
		rlpdlonrec.chdrnum.set(chdrpf.getChdrnum());
		wsaaRdockey.rdocRdocpfx.set("CH");
		wsaaRdockey.rdocRdoccoy.set(chdrpf.getChdrcoy());
		wsaaRdockey.rdocRdocnum.set(chdrpf.getChdrnum());
		wsaaRdockey.rdocTranseq.set(wsaaJrnseq);
		rlpdlonrec.doctkey.set(wsaaRdockey);
		rlpdlonrec.effdate.set(chdrpf.getOccdate());
		rlpdlonrec.currency.set(payrpf.getBillcurr());
		rlpdlonrec.tranno.set(wsaaTranno);
		rlpdlonrec.transeq.set(wsaaJrnseq);
		rlpdlonrec.longdesc.set(wsaaTransDesc);
		rlpdlonrec.language.set(atmodrec.language);
		rlpdlonrec.batchkey.set(atmodrec.batchKey);
		rlpdlonrec.authCode.set(atrtTransCode);
		rlpdlonrec.time.set(wsaaTransactionTime);
		rlpdlonrec.date_var.set(wsaaTransactionDate);
		rlpdlonrec.user.set(wsaaUser);
		rlpdlonrec.termid.set(wsaaTermid);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rlpdlonrec.statuz);
			syserrrec.params.set(rlpdlonrec.rec);
			xxxxFatalError();
		}
		wsaaJrnseq.set(rlpdlonrec.transeq);
	}

protected void reRateBillCollect2000()
	{
		/* Set COVR params so that we start at beginning of COVeRages      */
		/* associated with the specified Contract Number.                  */

		List<Covrpf> covrpflist = covrpfDAO.getCovrByComAndNum(wsaaCompany.toString(), wsaaChdrnum.toString());
		if(covrpflist != null && covrpflist.size()>0){
			for(int i=0; i<covrpflist.size(); i++){
				covrpf = covrpflist.get(i);
				reRate2100();
			}
		}
		
		if(instCovrList != null && instCovrList.size() > 0){
			covrpfDAO.insertCovrBulk(instCovrList);
		}

		updateChdrPayr4000();
		checkAgentTerminate4100();
		billing2200();
		if (isNE(wsaaNotEnoughCash,"Y")) {
			collection2300();
			updateCovrStatus2370();
			calculateNextDate2400();
		}
	}

	/**
	* <pre>
	* Instalment Premiums.
	* </pre>
	*/
protected void reRate2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					reRate2101();
				case callPremiumMeth2102:
					callPremiumMeth2102();
				case reschedule2103:
					reschedule2103();
					checkLext2104();
					readLext2105();
				case moveWsaaRateFrom2106:
					moveWsaaRateFrom2106();
				case writeCovr2107:
					writeCovr2107();
				case cont2108:
					cont2108();
				case exit2109:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void reRate2101()
	{
	    riskPremflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), RISKPREM_FEATURE_ID, appVars, "IT");//ILIFE-7845 //IJTI-1485
		/* Only process coverages for the selected payer.                  */
		/* *  IF COVR-PAYRSEQNO       NOT = PAYR-PAYRSEQNO                 */
		/* *     MOVE NEXTR           TO COVR-FUNCTION                     */
		/* *     GO TO 2109-EXIT.                                          */
		premiumrec.premiumRec.set(SPACES);
		/* Validate the new coverage status against T5679.*/
		wsaaValidStatus.set("N");
		if ((isNE(covrpf.getCoverage(),SPACES))
		&& ((isEQ(covrpf.getRider(),SPACES))
		|| (isEQ(covrpf.getRider(),"00")))) {
			/*    MOVE 1                  TO WSAA-INDEX                    */
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				validateCovrStatus2110();
			}
			/*        UNTIL WSAA-INDEX > 12.                               */
		}
		/* Validate the new rider status against T5679.*/
		if ((isNE(covrpf.getCoverage(),SPACES))
		&& ((isNE(covrpf.getRider(),SPACES))
		&& (isNE(covrpf.getRider(),"00")))) {
			/*    MOVE 1                  TO WSAA-INDEX                    */
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				validateRiderStatus2120();
			}
			/*        UNTIL WSAA-INDEX > 12.                               */
		}
		if (isEQ(wsaaValidStatus,"N")) {
			goTo(GotoLabel.exit2109);
		}
		wsaaBillDate.set(wsaaCurrentPayuptoDate);
		/* Calculate the month after by calling DATCON2 to compare the     */
		/* WSAA-BILL-DATE with the Rerate date because we really           */
		/* want the ANB to change the month after the rerate date.         */
		/* This is the same for the Joint life ANB calculation.            */
		datcon2rec.function.set(SPACES);
		datcon2rec.intDate1.set(covrpf.getRerateDate());
		datcon2rec.frequency.set(payrpf.getBillfreq());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			xxxxFatalError();
		}
		
		// ILIFE-7845 start
		if (riskPremflag) {
			updateRiskPrem();
		}
		// ILIFE-7845 end
		/* IF WSAA-RECALC-DATE         NOT =  WSAA-BILL-DATE       <025>*/
		/*    GO TO 2109-EXIT.                                     <025>*/
		/* IF COVR-RERATE-DATE > WSAA-BILL-DATE                         */
		/*     GO TO 2109-EXIT.                                         */
		/*IF COVR-RERATE-DATE > WSAA-BILL-DATE                    <027>*/
		/*    GO TO 2109-EXIT.                                    <027>*/
		/* If the premium cessation date = the re-rate date                */
		/*   then there is no re-rating on this contract, so               */
		/*   there is no need to re-calculate the premium.                 */
		/*IF COVR-PREM-CESS-DATE = COVR-RERATE-DATE                    */
		/*    GO TO 2103-RESCHEDULE.                                   */
		 	
		if (isGT(covrpf.getRerateDate(),wsaaBillDate)
		|| isEQ(covrpf.getRerateDate(),wsaaBillDate)) {
			goTo(GotoLabel.exit2109);
		}
		/* Use CRTABLE(coverage table) to access T5687 to get the premium*/
		/* calculation method and also the Re-calculation frequency.*/
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(tablesInner.t5687);
		itempf.setItemitem(covrpf.getCrtable());
		itempf = itemDao.getItempfRecord(itempf);

		if (itempf != null) {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	
		t5687rec.t5687Rec.set(SPACES);
			
		if (isEQ(covrpf.getPremCessDate(),covrpf.getRerateDate())) {
			goTo(GotoLabel.reschedule2103);
		}
		if (isEQ(t5687rec.premmeth,SPACES)) {
			premiumrec.calcPrem.set(0);
			premiumrec.calcBasPrem.set(0);
			premiumrec.calcLoaPrem.set(0);
			goTo(GotoLabel.reschedule2103);
		}
		/* Which calculation method to use depends on whether it is a*/
		/* single life contract or joint life contract.*/
		/*    IF T5687-JLIFE-PRESENT = 'N'                                 */
		/*        MOVE T5687-PREMMETH     TO WSKY-ITEM-ITEMITEM            */
		/*        GO TO 2102-CALL-PREMIUM-METH.                            */
		/* Read Life details to obtain sex & age for premium calc*/
		//ILIFE-7845  start
		if(riskPremflag) {
			List<Lifepf> lifepflist1 =  lifepfDAO.searchLifeRecordByLife(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
			if (lifepflist1 == null || lifepflist1.size() == 0) {
				syserrrec.params.set(covrpf.getChdrcoy().concat(covrpf.getChdrnum()).concat(covrpf.getLife()).concat("0"));
				xxxxFatalError();
			} else { 
				lifepf = lifepflist1.get(0); 
			} 
			premiumrec.lsex.set(lifepf.getCltsex());
			calculateAnb8200();
			premiumrec.lage.set(wsaaAnb);
			/* Read LIFERNL to see if a joint life exists.*/
			if (isEQ(t5687rec.jlifePresent,"N")) {
				wsaaItemkey.itemItemitem.set(t5687rec.premmeth);
				goTo(GotoLabel.callPremiumMeth2102);
			}
		
			lifepflist1 = null; //?
			lifepflist1 = lifepfDAO.searchLifeRecordByLife(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");
			if (lifepflist1!=null && lifepflist1.size()>0) {
				lifepf = lifepflist1.get(0);
				wsaaItemkey.itemItemitem.set(t5687rec.jlPremMeth);
				premiumrec.jlsex.set(lifepf.getCltsex());
				/*        MOVE LIFERNL-ANB-AT-CCD TO CPRM-JLAGE                    */
				calculateAnb8200();
				premiumrec.jlage.set(wsaaJanb);
			}
			else {
				wsaaItemkey.itemItemitem.set(t5687rec.premmeth);
				premiumrec.jlsex.set(SPACES);
				premiumrec.jlage.set(ZERO);
			}
			
			
		}//ILIFE-7845  end
		else {
		
		
		List<Lifepf> lifepflist =  lifepfDAO.searchLifeRecordByLife(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), ZERO.toString());
		if (lifepflist == null || lifepflist.size() == 0) {
			syserrrec.params.set(covrpf.getChdrcoy().concat(covrpf.getChdrnum()).concat(covrpf.getLife()).concat("0"));
			xxxxFatalError();
		} else { //IJTI-462
			lifepf = lifepflist.get(0); //IJTI-462
		} //IJTI-462
		/*    MOVE LIFERNL-ANB-AT-CCD     TO CPRM-LAGE.                    */
		premiumrec.lsex.set(lifepf.getCltsex());
		calculateAnb8200();
		premiumrec.lage.set(wsaaAnb);
		/* Read LIFERNL to see if a joint life exists.*/
		if (isEQ(t5687rec.jlifePresent,"N")) {
			wsaaItemkey.itemItemitem.set(t5687rec.premmeth);
			goTo(GotoLabel.callPremiumMeth2102);
		}
		
        lifepflist = null;
		lifepflist = lifepfDAO.searchLifeRecordByLife(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");
		if (lifepflist!=null && lifepflist.size()>0) {
			lifepf = lifepflist.get(0);
			wsaaItemkey.itemItemitem.set(t5687rec.jlPremMeth);
			premiumrec.jlsex.set(lifepf.getCltsex());
			/*        MOVE LIFERNL-ANB-AT-CCD TO CPRM-JLAGE                    */
			calculateAnb8200();
			premiumrec.jlage.set(wsaaJanb);
		}
		else {
			wsaaItemkey.itemItemitem.set(t5687rec.premmeth);
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	}
	}
protected void callPremiumMeth2102()
	{
		/* We use the method obtained from T5687 to access T5675 to*/
		/* get the corresponding subroutine.*/
	    itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(tablesInner.t5675);
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempf = itemDao.getItempfRecord(itempf);
		if (itempf == null ) {
			syserrrec.params.set(wsaaCompany.toString().concat(tablesInner.t5675).concat(wsaaItemkey.itemItemitem.toString()));
			xxxxFatalError();
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itempf.getItemitem());
		}
		/* ILIFE-3142 End*/
		t5675rec.t5675Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* We now have a Calculation subroutine called T5675-PREMSUBR.*/
		/* So just called this program to calculate the new instalment.*/
		/*   MOVE SPACE                  TO CPRM-PREMIUM-REC.*/
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.chdrChdrcoy.set(covrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrpf.getChdrnum());
		premiumrec.lifeLife.set(covrpf.getLife());
		premiumrec.lifeJlife.set(covrpf.getJlife());
		premiumrec.covrRider.set(covrpf.getRider());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.mop.set(chdrpf.getBillchnl());
		premiumrec.billfreq.set(chdrpf.getBillfreq());
		premiumrec.effectdt.set(covrpf.getCrrcd());
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.currcode.set(covrpf.getPremCurrency());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		premiumrec.ratingdate.set(covrpf.getRerateFromDate());
		premiumrec.reRateDate.set(covrpf.getRerateDate());
		/* MOVE 0                      TO CPRM-CALC-PREM.               */
		premiumrec.calcPrem.set(covrpf.getInstprem());
		premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
		premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Adjust instalment for plan processing.*/
		if (isEQ(covrpf.getPlanSuffix(),0)) {
			compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(),chdrpf.getPolsum()));
		}
		else {
			premiumrec.sumin.set(covrpf.getSumins());
		}
		compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin,chdrpf.getPolinc()));
		premiumrec.function.set("CALC");
		getAnny11000();
		premiumrec.language.set(atmodrec.language);
		//****Ticket #VE-155/263 TRM Calculations - Premium calculation start\
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/			
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{			
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(chdrpf.getCnttype());	
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			
			//ILIFE-7845 
		}
		/*Ticket #IVE-792 - End		*/	
		//****Ticket #VE-155/263 TRM Calculations - Premium calculation end
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			syserrrec.params.set(premiumrec.premiumRec);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(premiumrec.sumin);
		c000CallRounding();
		premiumrec.sumin.set(zrdecplrec.amountOut);
	}

	// ILIFE-7845 start
	protected void updateRiskPrem() {
		Rskppf rskppf = new Rskppf();
		rskppfList = rskppfDAO.fetchRskppfRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),
				covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getCrrcd());//IJTI-1485
		if (!ObjectUtils.isEmpty(rskppfList)) {
			for (int i = 0; i < rskppfList.size(); i++) {
				rskppf = new Rskppf();
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(rskppfList.get(i).getCnttype());
				premiumrec.crtable.set(rskppfList.get(i).getCrtable());
				premiumrec.calcTotPrem.set(rskppfList.get(i).getInstprem());
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
				rskppf.setUnique_Number(rskppfList.get(i).getUnique_Number());
				rskppf.setChdrcoy(rskppfList.get(i).getChdrcoy());
				rskppf.setChdrnum(rskppfList.get(i).getChdrnum());
				rskppf.setLife(rskppfList.get(i).getLife());
				rskppf.setCoverage(rskppfList.get(i).getCoverage());
				rskppf.setRider(rskppfList.get(i).getRider());
				rskppf.setCnttype(rskppfList.get(i).getCnttype());
				rskppf.setCrtable(rskppfList.get(i).getCrtable());
				rskppf.setRiskprem(rskppfList.get(i).getRiskprem().add(premiumrec.riskPrem.getbigdata()));
				readT5567();
				rskppf.setPolfee(rskppfList.get(i).getPolfee().add(wsaapolFee.getbigdata()));
				addRiskPremList.add(rskppf);
			}
		}
		if (addRiskPremList != null && !addRiskPremList.isEmpty())
			rskppfDAO.updateRiskPrem(addRiskPremList);
	}

	protected void readT5567() {
		chdrpf1 = chdrpfDAO.getchdrRecordData(covrpf.getChdrcoy(),covrpf.getChdrnum());//IJTI-1485
		t5567Map = itemDao.loadSmartTable("IT", covrpf.getChdrcoy(), "T5567");//IJTI-1485
		wsaaCnttype.set(chdrpf1.getCnttype());//IJTI-1485
		wsaaCntcurr.set(chdrpf1.getCntcurr());
		if(t5567Map.containsKey(wsaaItemitem.toString())) {
			List<Itempf> t5567List = t5567Map.get(wsaaItemitem.toString());
	        for(Itempf t5567Item: t5567List) {
	             	t5567rec.t5567Rec.set(StringUtil.rawToString(t5567Item.getGenarea()));           
	        }
		}else {
			t5567rec.t5567Rec.set(SPACES);
		}
		
		for (wsaaSub2.set(1); !(isGT(wsaaSub2,10)); wsaaSub2.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaSub2.toInt()],chdrpf1.getBillfreq())) {
				wsaapolFee.set(t5567rec.cntfee[wsaaSub2.toInt()]);
				wsaaSub2.set(11);
			}else {
				wsaapolFee.set(ZERO);
			}
		}
		
	}

	// ILIFE-7845 end	
protected void reschedule2103()
	{
		/* Update COVR.*/
		/* For premium cessation date = rerate date, update COVR with*/
		/* PSTATCODE = 'PU'.*/
		/* Check the coverage is a re-calculation coverage.                */
		if (isNE(t5687rec.rtrnwfreq,ZERO)) {
			if (isEQ(covrpf.getPremCessDate(),covrpf.getRerateDate())) {
				compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff,(mult(covrpf.getSingp(),-1))));
				/******       IF (COVR-RIDER = '00') OR                             */
				/******          (COVR-RIDER = '  ')                                */
				/******           IF T5679-SET-COV-PREM-STAT NOT = SPACES      <037>*/
				/******              MOVE T5679-SET-COV-PREM-STAT TO COVR-PSTATCODE */
				/******           END-IF                                       <037>*/
				/******       ELSE                                                  */
				/******           IF T5679-SET-RID-PREM-STAT NOT = SPACES      <037>*/
				/******              MOVE T5679-SET-RID-PREM-STAT TO COVR-PSTATCODE */
				/******           END-IF                                       <037>*/
			}
			else {
				datcon2rec.freqFactor.set(-1);
				datcon2rec.frequency.set("DY");
				datcon2rec.intDate1.set(wsaaBillDate);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				covrpf.setCurrto(datcon2rec.intDate2.toInt());
				covrpf.setValidflag("2");
			}
		}
		if (isLT(covrpf.getRerateDate(),wsaaRerateDate)) {
			wsaaRerateDate.set(covrpf.getRerateDate());
		}
		
		if(!covrpfDAO.updateCovrValidAndCurrtoFlag(covrpf)){
			syserrrec.params.set(covrpf.getChdrnum());
			xxxxFatalError();
		}
		

		if (isEQ(covrpf.getPremCessDate(),covrpf.getRerateDate())) {
			/*        GO TO 2109-EXIT.                                         */
			goTo(GotoLabel.exit2109);
		}
		
		uniqueNumber = covrpf.getUniqueNumber();
        covrpf = new Covrpf();
        covrpf = covrpfDAO.getCovrByUniqueNum(uniqueNumber);
        if(covrpf==null){
        	syserrrec.params.set("Covrpf is not exist");
			xxxxFatalError();
        }
	}

protected void checkLext2104()
	{
		/* Calculate WSAA-RATE-FROM as RRTFRM(rerate from date) incremented*/
		/* by the re-calculating frequency(T5687-RTRNWFREQ).*/
		if (isEQ(t5687rec.rtrnwfreq,SPACES)) {
			/*    MOVE '00'               TO T5687-RTRNWFREQ               */
			t5687rec.rtrnwfreq.set(ZERO);
			wsaaRateFrom.set(covrpf.getRerateFromDate());
		}
		else {
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set(t5687rec.rtrnwfreq);
			datcon2rec.intDate1.set(covrpf.getRerateFromDate());
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			wsaaRateFrom.set(datcon2rec.intDate2);
		}
		wsaaRtrnwfreq.set(t5687rec.rtrnwfreq);
		lextbrrIO.setDataArea(SPACES);
		lextbrrIO.setChdrcoy(covrpf.getChdrcoy());
		lextbrrIO.setChdrnum(covrpf.getChdrnum());
		lextbrrIO.setLife(covrpf.getLife());
		lextbrrIO.setCoverage(covrpf.getCoverage());
		lextbrrIO.setRider(covrpf.getRider());
		lextbrrIO.setExtCessDate(wsaaRateFrom);
		lextbrrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextbrrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextbrrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		lextbrrIO.setFormat(formatsInner.lextbrrrec);
	}

protected void readLext2105()
	{
		/* Read LEXTBRR for unexpired records.*/
		/* The first record encountered would have the first expiry date*/
		/* since LEXTBRR has the expiry date as key and in accending*/
		/* order.*/
		SmartFileCode.execute(appVars, lextbrrIO);
		if ((isNE(lextbrrIO.getStatuz(),varcom.oK))
		&& (isNE(lextbrrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lextbrrIO.getParams());
			xxxxFatalError();
		}
		/* No LEXT record.*/
		/* If re-calculate frequency = 00 (ie. not reratable)*/
		/*     Set re-rate date to premium cessation date.*/
		if (isEQ(lextbrrIO.getStatuz(),varcom.endp)) {
			if (isEQ(wsaaRtrnwfreq,"00")) {
				covrpf.setRerateDate(covrpf.getPremCessDate());
				goTo(GotoLabel.writeCovr2107);
			}
			else {
				goTo(GotoLabel.moveWsaaRateFrom2106);
			}
		}
		if ((isNE(lextbrrIO.getChdrcoy(),covrpf.getChdrcoy()))
		|| (isNE(lextbrrIO.getChdrnum(),covrpf.getChdrnum()))
		|| (isNE(lextbrrIO.getRider(),covrpf.getRider()))
		|| (isNE(lextbrrIO.getCoverage(),covrpf.getCoverage()))
		|| (isNE(lextbrrIO.getLife(),covrpf.getLife()))) {
			if (isEQ(wsaaRtrnwfreq,"00")) {
				covrpf.setRerateDate(covrpf.getPremCessDate());
				goTo(GotoLabel.writeCovr2107);
			}
			else {
				goTo(GotoLabel.moveWsaaRateFrom2106);
			}
		}
		/* LEXT record exists.*/
		/* If re-calculate frequency = 00 (ie. not reratable)*/
		/*     Set re-rate date to extras expiry date.*/
		if (isLT(wsaaRateFrom,lextbrrIO.getExtCessDate())) {
			if (isEQ(wsaaRtrnwfreq,"00")) {
				if (isGTE(covrpf.getRerateDate(),lextbrrIO.getExtCessDate())) {
					lextbrrIO.setFunction(varcom.nextr);
					readLext2105();
					return ;
				}
				else {
					covrpf.setRerateDate(lextbrrIO.getExtCessDate().toInt());
					goTo(GotoLabel.writeCovr2107);
				}
			}
			else {
				goTo(GotoLabel.moveWsaaRateFrom2106);
			}
		}
		else {
			if (isEQ(wsaaRateFrom,lextbrrIO.getExtCessDate())) {
				lextbrrIO.setFunction(varcom.nextr);
				readLext2105();
				return ;
			}
			else {
				covrpf.setRerateDate(lextbrrIO.getExtCessDate().toInt());
				goTo(GotoLabel.writeCovr2107);
			}
		}
	}

protected void moveWsaaRateFrom2106()
	{
		covrpf.setRerateDate(wsaaRateFrom.toInt());
		covrpf.setRerateFromDate(wsaaRateFrom.toInt());
	}

protected void writeCovr2107()
	{
		/* Rewite COVR with new details.*/
		/* Adjust calculated premiums for plan polices.*/
		/*  IF there was no prem method for recalculating premium          */
		/*     then there is no need to update the premium field on covr   */
		if (isEQ(t5687rec.premmeth,SPACES)) {
			goTo(GotoLabel.cont2108);
		}
		compute(wsaaInstPrem, 3).setRounded(div(premiumrec.calcPrem,chdrpf.getPolinc()));
		compute(wsaaZbinstprem, 3).setRounded(div(premiumrec.calcBasPrem,chdrpf.getPolinc()));
		compute(wsaaZlinstprem, 3).setRounded(div(premiumrec.calcLoaPrem,chdrpf.getPolinc()));
		if (isEQ(covrpf.getPlanSuffix(),0)) {
			compute(wsaaZbinstprem, 3).setRounded(mult(wsaaZbinstprem,chdrpf.getPolsum()));
			compute(wsaaZlinstprem, 3).setRounded(mult(wsaaZlinstprem,chdrpf.getPolsum()));
			compute(wsaaInstPrem, 3).setRounded(mult(wsaaInstPrem,chdrpf.getPolsum()));
		}
		zrdecplrec.amountIn.set(wsaaZbinstprem);
		c000CallRounding();
		wsaaZbinstprem.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaZlinstprem);
		c000CallRounding();
		wsaaZlinstprem.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaInstPrem);
		c000CallRounding();
		wsaaInstPrem.set(zrdecplrec.amountOut);
		compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff,(sub(wsaaInstPrem,covrpf.getInstprem()))));
		/*                                      COVR-SINGP).               */
		/*    MOVE WSAA-INST-PREM         TO COVR-SINGP.                   */
		covrpf.setInstprem(new BigDecimal(wsaaInstPrem.toString()));
		covrpf.setZbinstprem(new BigDecimal(wsaaZbinstprem.toString()));
		covrpf.setZlinstprem(new BigDecimal(wsaaZlinstprem.toString()));
		/* if we have just called a premium calc subroutine which sets the */
		/*  Sum assured depending on the Premium, then we must also update */
		/* the SUMINS on the COVRage record as well.                       */
		covrpf.setSumins(new BigDecimal(premiumrec.sumin.toString()));
	}

protected void cont2108()
	{
		covrpf.setValidflag(vflagcpy.inForceVal.toString());
		/*    ADD  1                      TO COVR-TRANNO.                  */
		covrpf.setTranno(wsaaTranno.toInt());
		covrpf.setCurrfrom(wsaaBillDate.toInt());
		covrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		covrpf.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845 
		instCovrList.add(covrpf);
		
	}


protected void validateCovrStatus2110()
	{
		/*VALIDATE-COVR-STATUS*/
		/*IF T5679-COV-RISK-STAT(WSAA-INDEX) NOT = SPACE               */
		/*    IF (T5679-COV-RISK-STAT(WSAA-INDEX) = COVR-STATCODE) AND */
		/*       (T5679-COV-PREM-STAT(WSAA-INDEX) = COVR-PSTATCODE)    */
		/*        MOVE 13             TO WSAA-INDEX                    */
		/*        MOVE 'Y'            TO WSAA-VALID-STATUS             */
		/*        GO TO 2119-EXIT.                                     */
		/*ADD 1                       TO WSAA-INDEX.                   */
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrpf.getPstatcode())) {
					wsaaValidStatus.set("Y");
				}
			}
		}
		/*EXIT*/
	}

protected void validateRiderStatus2120()
	{
		/*VALIDATE-RIDER-STATUS*/
		/*IF T5679-RID-RISK-STAT(WSAA-INDEX) NOT = SPACE               */
		/*    IF (T5679-RID-RISK-STAT(WSAA-INDEX) = COVR-STATCODE) AND */
		/*       (T5679-RID-PREM-STAT(WSAA-INDEX) = COVR-PSTATCODE)    */
		/*        MOVE 13             TO WSAA-INDEX                    */
		/*        MOVE 'Y'            TO WSAA-VALID-STATUS             */
		/*        GO TO 2129-EXIT.                                     */
		/*ADD 1                       TO WSAA-INDEX.                   */
		if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrpf.getPstatcode())) {
					wsaaValidStatus.set("Y");
				}
			}
		}
		/*EXIT*/
	}



	/**
	* <pre>
	* Billing.
	* </pre>
	*/
protected void billing2200()
	{
		/* Read T5645 for suspense balance.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T3695 to check the sign.*/
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/* MOVE CHDRLIF-BTDATE         TO WSAA-BTDATE.                  */
		wsaaBtdate.set(payrpf.getBtdate());
		wsaaBillcd.set(payrpf.getBillcd());
		/* Read ACBL*/
		acblIO.setParams(SPACES);
		acblIO.setRldgacct(chdrpf.getChdrnum());
		acblIO.setRldgcoy(chdrpf.getChdrcoy());
		/* MOVE CHDRLIF-CNTCURR        TO ACBL-ORIGCURR.           <022>*/
		acblIO.setOrigcurr(chdrpf.getBillcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK))
		&& (isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			xxxxFatalError();
		}
		/* Multiply the balance by the sign from T3695.                    */
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaSuspAvail.set(0);
		}
		else {
			if (isEQ(t3695rec.sign,"-")) {
				compute(wsaaSuspAvail, 2).set(mult(acblIO.getSacscurbal(),-1));
			}
			else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
		/*MOVE SPACE                  TO SACSRNL-PARAMS.               */
		/*MOVE CHDRLIF-CHDRNUM        TO SACSRNL-CHDRNUM.              */
		/*MOVE CHDRLIF-CHDRCOY        TO SACSRNL-CHDRCOY.              */
		/*MOVE CHDRLIF-CNTCURR        TO SACSRNL-CNTCURR.              */
		/*MOVE T5645-SACSCODE-01      TO SACSRNL-SACSCODE.             */
		/*MOVE T5645-SACSTYPE-01      TO SACSRNL-SACSTYP.              */
		/*MOVE READR                  TO SACSRNL-FUNCTION.             */
		/*MOVE SACSRNLREC             TO SACSRNL-FORMAT.               */
		/*CALL 'SACSRNLIO' USING SACSRNL-PARAMS.                       */
		/*IF (SACSRNL-STATUZ NOT = O-K) AND                            */
		/*   (SACSRNL-STATUZ NOT = MRNF)                               */
		/*    MOVE SACSRNL-PARAMS     TO SYSR-PARAMS                   */
		/*    PERFORM XXXX-FATAL-ERROR.                                */
		/*Multiply the balance by the sign from T3695.                    */
		/*IF SACSRNL-STATUZ = MRNF                                     */
		/*    MOVE 0                  TO WSAA-SUSP-AVAIL               */
		/*ELSE                                                         */
		/*    IF T3695-SIGN = '-'                                      */
		/*        MULTIPLY SACSRNL-SACSCURBAL BY -1                    */
		/*                             GIVING WSAA-SUSP-AVAIL          */
		/*    ELSE                                                     */
		/*        MOVE SACSRNL-SACSCURBAL TO WSAA-SUSP-AVAIL.          */
		/*    IF WSAA-SUSP-AVAIL          NOT > 0                         <*/
		/*       MOVE 'Y'                 TO WSAA-NOT-ENOUGH-CASH         <*/
		/*       GO TO 2209-EXIT.                                         <*/
		/* Create a LINS record for each new payment.*/
		wsaaTotAmount.set(0);
		produceLinsRecord2210();
		/* IF WSAA-SUSP-AVAIL          < LINSRNL-CBILLAMT               */
		/*    MOVE 'Y'                 TO WSAA-NOT-ENOUGH-CASH          */
		/*    GO TO 2209-EXIT.                                          */
		if (isLT(wsaaSuspAvail,wsaaNetCbillamt)) {
			/* Read the latest premium tollerance allowed.                     */
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(atrtCompany);
			itemIO.setItemtabl(tablesInner.t5667);
			wsbbT5667Trancd.set(atrtTransCode);
			wsbbT5667Curr.set(payrpf.getBillcurr());
			itemIO.setItemitem(wsbbT5667Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
			&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				xxxxFatalError();
			}
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				t5667rec.t5667Rec.set(SPACES);
			}
			else {
				t5667rec.t5667Rec.set(itemIO.getGenarea());
			}
			/*-Check the tolerance amount against T5667 table entry read       */
			/* above.                                                          */
			for (wsaaSub.set(1); true; wsaaSub.add(1)){
				if (isEQ(payrpf.getBillfreq(),t5667rec.freq[wsaaSub.toInt()])) {
					compute(wsaaTol, 3).setRounded(div((mult(wsaaNetCbillamt,t5667rec.prmtol[wsaaSub.toInt()])),100));
					zrdecplrec.amountIn.set(wsaaTol);
					c000CallRounding();
					wsaaTol.set(zrdecplrec.amountOut);
					if (isGT(wsaaTol,t5667rec.maxAmount[wsaaSub.toInt()])) {
						wsaaTol.set(t5667rec.maxAmount[wsaaSub.toInt()]);
					}
					wsaaTol2.set(0);
					if (isGT(t5667rec.maxamt[wsaaSub.toInt()],0)) {
						if (isEQ(wsaaAgtTerminatedFlag,"N")
						|| (isEQ(wsaaAgtTerminatedFlag,"Y")
						&& isEQ(t5667rec.sfind,"2"))) {
							compute(wsaaTol2, 3).setRounded(div((mult(wsaaNetCbillamt,t5667rec.prmtoln[wsaaSub.toInt()])),100));
							zrdecplrec.amountIn.set(wsaaTol2);
							c000CallRounding();
							wsaaTol2.set(zrdecplrec.amountOut);
							if (isGT(wsaaTol2,t5667rec.maxamt[wsaaSub.toInt()])) {
								wsaaTol2.set(t5667rec.maxamt[wsaaSub.toInt()]);
							}
						}
					}
				}
				if (isEQ(wsaaSub,11)) {
					break ;
				}
			}
			if ((setPrecision(wsaaSuspAvail, 2)
			&& isLT(wsaaSuspAvail,(sub(wsaaNetCbillamt,wsaaTol))))) {
				if ((setPrecision(wsaaSuspAvail, 2)
				&& isLT(wsaaSuspAvail,(sub(wsaaNetCbillamt,wsaaTol2))))) {
					wsaaNotEnoughCash = "Y";
					return ;
				}
				else {
					setPrecision(linsrnlIO.getInstamt03(), 2);
					linsrnlIO.setInstamt03(sub(wsaaNetCbillamt,wsaaSuspAvail));
					wsaaToleranceFlag = "2";
				}
			}
			else {
				setPrecision(linsrnlIO.getInstamt03(), 2);
				linsrnlIO.setInstamt03(sub(wsaaNetCbillamt,wsaaSuspAvail));
				wsaaToleranceFlag = "1";
			}
		}
		/*IF WSAA-SUSP-AVAIL          < WSAA-NET-CBILLAMT         <018>*/
		/*MOVE 'Y'                 TO WSAA-NOT-ENOUGH-CASH     <018>*/
		/*GO TO 2209-EXIT.                                     <018>*/
		/* Check premium status code.*/
		/* If it is paid-up, no more payments will be required.*/
		/* (No LINS records.)*/
		/* IF CHDRLIF-PSTATCODE = T5679-SET-CN-PREM-STAT                */
		if (isEQ(payrpf.getPstatcode(),t5679rec.setCnPremStat)) {
			return ;
		}
	}

protected void produceLinsRecord2210()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					produceLinsRecord2211();
					callPayrlif2212();
				case correctPayrRecord2213:
					correctPayrRecord2213();
				case totalBilled2214:
					totalBilled2214();
					otherFields2215();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void produceLinsRecord2211()
	{
		/* First we have to check if the contract(CHDRLIF) we are dealing*/
		/* with has the correct premium for the instalment date; if not we*/
		/* will have to get it from history records(CHDROLD).*/
		/* MOVE 'N'                    TO WSAA-OLD-CHDR.                */
		/* IF (WSAA-BTDATE NOT < CHDRLIF-CURRFROM) AND                  */
		/*    (WSAA-BTDATE NOT > CHDRLIF-CURRTO)                        */
		/*     GO TO 2213-CORRECT-CHDR-RECORD.                          */
		/*At here, we have to read CHDROLD to get the history record for  */
		/*the instalment amount. There could be more than one history     */
		/*record, so we have to keep checking the current from and        */
		/*current to dates to see if they are compatible with the billing */
		/*date.                                                           */
		/* MOVE SPACE                  TO CHDROLD-PARAMS.               */
		/* MOVE CHDRLIF-CHDRNUM        TO CHDROLD-CHDRNUM.              */
		/* MOVE CHDRLIF-CHDRCOY        TO CHDROLD-CHDRCOY.              */
		/* MOVE CHDROLDREC             TO CHDROLD-FORMAT.               */
		/* MOVE BEGN                   TO CHDROLD-FUNCTION.             */
		/*2212-CALL-CHDROLD.                                               */
		/* CALL 'CHDROLDIO' USING CHDROLD-PARAMS.                       */
		/* IF (CHDROLD-STATUZ NOT = O-K)                                */
		/*     MOVE CHDROLD-PARAMS     TO SYSR-PARAMS                   */
		/*     PERFORM XXXX-FATAL-ERROR.                                */
		/* IF (WSAA-BTDATE NOT < CHDROLD-CURRFROM) AND                  */
		/*    (WSAA-BTDATE NOT > CHDROLD-CURRTO)                        */
		/*     MOVE 'Y'                TO WSAA-OLD-CHDR                 */
		/*     GO TO 2213-CORRECT-CHDR-RECORD.                          */
		/* MOVE NEXTR                  TO CHDROLD-FUNCTION.             */
		/* GO TO 2212-CALL-CHDROLD.                                     */
		/*We now have the record we want.                                 */
		/*2213-CORRECT-CHDR-RECORD.                                        */
		/* IF WSAA-OLD-CHDR = 'Y'                                       */
		/*     COMPUTE WSAA-TOT-AMOUNT = WSAA-TOT-AMOUNT +              */
		/*                               CHDROLD-SINSTAMT06             */
		/*     COMPUTE CHDRLIF-OUTSTAMT = CHDRLIF-OUTSTAMT +            */
		/*                                CHDROLD-SINSTAMT06            */
		/* ELSE                                                         */
		/*     COMPUTE WSAA-TOT-AMOUNT = WSAA-TOT-AMOUNT +              */
		/*                               CHDRLIF-SINSTAMT06             */
		/*     COMPUTE CHDRLIF-OUTSTAMT = CHDRLIF-OUTSTAMT +            */
		/*                                CHDRLIF-SINSTAMT06.           */
		/* First we have to check if the payer we are dealing              */
		/* with has the correct premium for the instalment date; if not we */
		/* will have to get it from history records(PAYRLIF).              */
		wsaaOldPayr = "N";
		if (isGTE(wsaaBtdate,payrpf.getEffdate())) {
			goTo(GotoLabel.correctPayrRecord2213);
		}
		/* At here, we have to read PAYRLIF to get the history record for  */
		/* the instalment amount. There could be more than one history     */
		/* record, so we have to keep checking the PAYR-EFFDATE            */
		/* to see if it is compatible with the billing date                */
		/* date.                                                           */
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrnum(payrpf.getChdrnum());
		payrlifIO.setChdrcoy(payrpf.getChdrcoy());
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.begn);
	}

protected void callPayrlif2212()
	{
		SmartFileCode.execute(appVars, payrlifIO);
		if ((isNE(payrlifIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		if (isGTE(wsaaBtdate,payrlifIO.getEffdate())) {
			wsaaOldPayr = "Y";
			goTo(GotoLabel.correctPayrRecord2213);
		}
		payrlifIO.setFunction(varcom.nextr);
		callPayrlif2212();
		return ;
	}

	/**
	* <pre>
	* We now have the record we want.
	* </pre>
	*/
protected void correctPayrRecord2213()
	{
		/*      This change removed it should keep in line with          */
		/*     b6243 a new section has been created to update CHDR + PAYR*/
		/* If the Instalment premium has been recalculated, ie not = zero  */
		/* then update these amounts on the PAYR.                          */
		/*  IF WSAA-INST-PREM = ZERO                                     */
		/*      NEXT SENTENCE                                            */
		/*  ELSE                                                         */
		/*      MOVE WSAA-INST-PREM       TO PAYR-SINSTAMT01             */
		/*      COMPUTE PAYR-SINSTAMT06   = PAYR-SINSTAMT01              */
		/*                          + PAYR-SINSTAMT02 + PAYR-SINSTAMT03  */
		/*                          + PAYR-SINSTAMT04 + PAYR-SINSTAMT05. */
		if (isEQ(wsaaOldPayr,"Y")) {
			compute(wsaaTotAmount, 2).set(add(wsaaTotAmount,payrlifIO.getSinstamt06()));
			/**        COMPUTE PAYR-OUTSTAMT =   PAYR-OUTSTAMT +           <019>*/
			/**                                   PAYRLIF-SINSTAMT06       <019>*/
		}
		else {
			compute(wsaaTotAmount, 2).set(add(wsaaTotAmount,payrpf.getSinstamt06()));
			/**        COMPUTE PAYR-OUTSTAMT = PAYR-OUTSTAMT +             <019>*/
			/**                                 PAYR-SINSTAMT06.           <019>*/
		}
		payrpf.setOutstamt(BigDecimal.ZERO);
		/* A large section of code has been commented out as DATCON4 *     */
		/* is now called to correctly establish BILLCD and BTDATE    *     */
		/* Call 'DATCON2' to increment the WSAA-BTDATE by the billing*/
		/* frequency.*/
		/* MOVE 1                      TO DTC2-FREQ-FACTOR.             */
		/* MOVE CHDRLIF-BILLFREQ       TO DTC2-FREQUENCY.          <018>*/
		/* MOVE PAYR-BILLFREQ          TO DTC2-FREQUENCY.          <018>*/
		/* MOVE WSAA-BTDATE            TO DTC2-INT-DATE-1.              */
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC.                       */
		/* IF DTC2-STATUZ           NOT = O-K                      <036>*/
		/*    MOVE DTC2-DATCON2-REC    TO SYSR-STATUZ              <036>*/
		/*    PERFORM XXXX-FATAL-ERROR.                            <036>*/
		/*                                                         <036>*/
		/*MOVE DTC2-INT-DATE-2        TO CHDRLIF-BTDATE.          <036>*/
		/*MOVE DTC2-INT-DATE-2        TO PAYR-BTDATE.        <018><036>*/
		/* MOVE DTC2-INT-DATE-2        TO WSAA-BTDATE-NEW.         <036>*/
		/*                                                         <036>*/
		/* Check to see if the day is at the end of the month.             */
		/* MOVE WSAA-BTDATE-NEW        TO WSAA-EOM-DATE.           <036>*/
		/* MOVE WSAA-LINS-DAY          TO WSAA-EOM-DAY.            <036>*/
		/* MOVE 'N'                    TO WSAA-END-OF-MONTH.       <036>*/
		/*                                                         <036>*/
		/* PERFORM 9000-CHECK-END-OF-MONTH.                        <036>*/
		/* If the day of the DATCON2 returned date is different            */
		/*    to the oringinal LINS billing renewal day and the            */
		/*    original LINS date was not at the end of the month           */
		/*       Set the day to the original billing day.                  */
		/*                                                         <036>*/
		/* IF WSAA-BTNEW-DAY        NOT = WSAA-LINS-DAY  AND       <036>*/
		/*    WSAA-END-OF-MONTH     NOT = 'Y'                      <036>*/
		/*    MOVE WSAA-LINS-DAY       TO WSAA-BTNEW-DAY.          <036>*/
		/*                                                         <036>*/
		/* MOVE WSAA-BTDATE-NEW        TO PAYR-BTDATE.             <036>*/
		/* MOVE WSAA-BTDATE-NEW        TO CHDRLIF-BTDATE.          <036>*/
		/*                                                         <036>*/
		/* MOVE DTC2-INT-DATE-2        TO PAYR-BILLCD.             <019>*/
		/* MOVE DTC2-INT-DATE-2        TO CHDRLIF-BILLCD.          <019>*/
		/* Call 'DATCON2' to increment the PAYR-BILLCD  by the billing     */
		/* frequency.                                                      */
		/*                                                         <019>*/
		/* MOVE 1                      TO DTC2-FREQ-FACTOR.        <019>*/
		/* MOVE PAYR-BILLFREQ          TO DTC2-FREQUENCY.          <019>*/
		/* MOVE PAYR-BILLCD            TO DTC2-INT-DATE-1.         <019>*/
		/*                                                         <019>*/
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC.                  <019>*/
		/*                                                         <019>*/
		/* IF DTC2-STATUZ           NOT = O-K                      <036>*/
		/*    MOVE DTC2-DATCON2-REC    TO SYSR-STATUZ              <036>*/
		/*    PERFORM XXXX-FATAL-ERROR.                            <036>*/
		/*                                                         <036>*/
		/*MOVE DTC2-INT-DATE-2        TO PAYR-BILLCD.             <019>*/
		/*MOVE DTC2-INT-DATE-2        TO CHDRLIF-BILLCD.          <019>*/
		/* MOVE DTC2-INT-DATE-2        TO WSAA-BILLCD-NEW.         <036>*/
		/* Check to see if the day is at the end of the month.             */
		/* MOVE WSAA-BILLCD-NEW        TO WSAA-EOM-DATE.           <036>*/
		/* MOVE WSAA-LINS-DAY          TO WSAA-EOM-DAY.            <036>*/
		/* MOVE 'N'                    TO WSAA-END-OF-MONTH.       <036>*/
		/*                                                         <036>*/
		/* PERFORM 9000-CHECK-END-OF-MONTH.                        <036>*/
		/* If the day of the DATCON2 returned date is different            */
		/*    to the oringinal LINS billing renewal day and the            */
		/*    original LINS date was not at the end of the month           */
		/*       Set the day to the original billing day.                  */
		/* IF WSAA-BILLNEW-DAY      NOT = WSAA-LINS-DAY  AND       <036>*/
		/*    WSAA-END-OF-MONTH     NOT = 'Y'                      <036>*/
		/*    MOVE WSAA-LINS-DAY       TO WSAA-BILLNEW-DAY.        <036>*/
		/*                                                         <036>*/
		/* MOVE WSAA-BILLCD-NEW        TO PAYR-BILLCD.             <036>*/
		/* MOVE WSAA-BILLCD-NEW        TO CHDRLIF-BILLCD.          <036>*/
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(payrpf.getBillfreq());
		datcon4rec.intDate1.set(wsaaBtdate);
		datcon4rec.billday.set(payrpf.getDuedd());
		datcon4rec.billmonth.set(payrpf.getDuemm());
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		wsaaBtdateNew.set(datcon4rec.intDate2);
		payrpf.setBtdate(datcon4rec.intDate2.toInt());
		chdrpf.setBtdate(datcon4rec.intDate2.toInt());
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(payrpf.getBillfreq());
		datcon4rec.intDate1.set(payrpf.getBillcd());
		datcon4rec.billday.set(payrpf.getBillday());
		datcon4rec.billmonth.set(payrpf.getBillmonth());
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		payrpf.setBillcd(datcon4rec.intDate2.toInt());
		chdrpf.setBillcd(datcon4rec.intDate2.toInt());
		/* Call 'DATCON2' to increment the PAYR-NEXTDATE  by the billing   */
		/* frequency.                                                      */
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(payrpf.getBillfreq());
		datcon2rec.intDate1.set(payrpf.getNextdate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.datcon2Rec);
			xxxxFatalError();
		}
		payrpf.setNextdate(datcon2rec.intDate2.toInt());
		/* Store the waiver amount and the contract fee and calculate      */
		if (isNE(tr52drec.txcode, SPACES)) {
			wsaaWaiveAmt.set(payrpf.getSinstamt05());
			wsaaCntfee.set(payrpf.getSinstamt02());
			wsaaTaxEffdate.set(wsaaBtdate);
			b600CalcTax();
			wsaaTotAmount.add(wsaaTax);
		}
		/* IF CHDRLIF-BTDATE > CHDRLIF-SINSTTO                          */
		/*     MOVE T5679-SET-CN-PREM-STAT TO CHDRLIF-PSTATCODE         */
		/*     MOVE WSAA-BTDATE        TO CHDRLIF-BTDATE                */
		/*     GO TO 2219-EXIT.                                         */
		/*                                                         <018>*/
		/* IF PAYR-BTDATE  > CHDRLIF-SINSTTO                       <018>*/
		/*     MOVE T5679-SET-CN-PREM-STAT TO PAYR-PSTATCODE       <018>*/
		/*     MOVE WSAA-BTDATE        TO PAYR-BTDATE              <018>*/
		/*     GO TO 2219-EXIT.                                    <018>*/
		/*                                                         <018>*/
		/* Look up the tax relief subroutine on T6687.                     */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(t5688rec.taxrelmth);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6687rec.t6687Rec.set(itemIO.getGenarea());
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		prasrec.taxrelamt.set(ZERO);
		/* If the tax relief method is not spaces calculate the tax        */
		/* relief amount and deduct it from the premium.                   */
		if (isNE(t5688rec.taxrelmth,SPACES)) {
			prasrec.clntnum.set(clrfpf.getClntnum());
			prasrec.clntcoy.set(clrfpf.getClntcoy());
			prasrec.incomeSeqNo.set(payrpf.getIncomeSeqNo());
			prasrec.cnttype.set(chdrpf.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			/*     MOVE WSAA-UNIT-EFFDATE      TO PRAS-EFFDATE         <019>*/
			prasrec.effdate.set(wsaaBillcd);
			prasrec.company.set(chdrpf.getChdrcoy());
			if (isEQ(wsaaOldPayr,"Y")) {
				prasrec.grossprem.set(payrlifIO.getSinstamt06());
			}
			else {
				prasrec.grossprem.set(payrpf.getSinstamt06());
			}
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				xxxxFatalError();
			}
		}
		zrdecplrec.amountIn.set(prasrec.taxrelamt);
		c000CallRounding();
		prasrec.taxrelamt.set(zrdecplrec.amountOut);
		if (isEQ(wsaaOldPayr,"Y")) {
			compute(wsaaNetBillamt, 2).set(sub(payrlifIO.getSinstamt06(),prasrec.taxrelamt));
		}
		else {
			compute(wsaaNetBillamt, 2).set(sub(payrpf.getSinstamt06(),prasrec.taxrelamt));
		}
		totalPremiumAmount.add(wsaaNetBillamt);
		wsaaNetBillamt.add(wsaaTax);
		/* Convert the net bill amount  if the contract currency           */
		/* is not the same as billing currency.                            */
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(wsaaNetBillamt);
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/* MOVE WSAA-TRANSACTION-DATE  TO CLNK-CASHDATE.                */
		conlinkrec.cashdate.set(datcon1rec.intDate);
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(wsaaCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		if (isEQ(payrpf.getCntcurr(),payrpf.getBillcurr())) {
			wsaaNetCbillamt.set(wsaaNetBillamt);
		}
		else {
			wsaaNetCbillamt.set(conlinkrec.amountOut);
		}
		/* Set up LINS record.(LINS holds instalment amounts in contract*/
		/* currency and the total instalment in billing currency.)*/
		linsrnlIO.setParams(SPACES);
		linsrnlIO.setBillcd(ZERO);
		/* IF WSAA-OLD-CHDR = 'Y'                                       */
		/*     MOVE CHDROLD-SINSTAMT01 TO  LINSRNL-INSTAMT01            */
		/*     MOVE CHDROLD-SINSTAMT02 TO  LINSRNL-INSTAMT02            */
		/*     MOVE CHDROLD-SINSTAMT03 TO  LINSRNL-INSTAMT03            */
		/*     MOVE CHDROLD-SINSTAMT04 TO  LINSRNL-INSTAMT04            */
		/*     MOVE CHDROLD-SINSTAMT05 TO  LINSRNL-INSTAMT05            */
		/*     MOVE CHDROLD-SINSTAMT06 TO  LINSRNL-INSTAMT06            */
		/* ELSE                                                         */
		/*     MOVE CHDRLIF-SINSTAMT01 TO  LINSRNL-INSTAMT01            */
		/*     MOVE CHDRLIF-SINSTAMT02 TO  LINSRNL-INSTAMT02            */
		/*     MOVE CHDRLIF-SINSTAMT03 TO  LINSRNL-INSTAMT03            */
		/*     MOVE CHDRLIF-SINSTAMT04 TO  LINSRNL-INSTAMT04            */
		/*     MOVE CHDRLIF-SINSTAMT05 TO  LINSRNL-INSTAMT05            */
		/*     MOVE CHDRLIF-SINSTAMT06 TO  LINSRNL-INSTAMT06.           */
		/* MOVE PAYR-SINSTAMT01        TO  LINSRNL-INSTAMT01.           */
		/* MOVE PAYR-SINSTAMT02        TO  LINSRNL-INSTAMT02.           */
		/* MOVE PAYR-SINSTAMT06        TO  LINSRNL-INSTAMT06.           */
		if (isEQ(wsaaOldPayr,"Y")) {
			linsrnlIO.setInstamt01(payrlifIO.getSinstamt01());
			linsrnlIO.setInstamt02(payrlifIO.getSinstamt02());
			linsrnlIO.setInstamt03(payrlifIO.getSinstamt03());
			linsrnlIO.setInstamt04(payrlifIO.getSinstamt04());
			linsrnlIO.setInstamt05(payrlifIO.getSinstamt05());
			linsrnlIO.setInstamt06(payrlifIO.getSinstamt06());
		}
		else {
			linsrnlIO.setInstamt01(payrpf.getSinstamt01());
			linsrnlIO.setInstamt02(payrpf.getSinstamt02());
			linsrnlIO.setInstamt03(payrpf.getSinstamt03());
			linsrnlIO.setInstamt04(payrpf.getSinstamt04());
			linsrnlIO.setInstamt05(payrpf.getSinstamt05());
			linsrnlIO.setInstamt06(payrpf.getSinstamt06());
		}
		setPrecision(linsrnlIO.getInstamt06(), 2);
		linsrnlIO.setInstamt06(add(linsrnlIO.getInstamt06(), wsaaTax));
		/* Convert contract amount(total) to billing amount if contract*/
		/* currency is not the same as billing currency.*/
		/* IF CHDRLIF-CNTCURR = CHDRLIF-BILLCURR                        */
		if (isEQ(payrpf.getCntcurr(),payrpf.getBillcurr())) {
			conlinkrec.amountOut.set(linsrnlIO.getInstamt06());
			goTo(GotoLabel.totalBilled2214);
		}
		conlinkrec.clnk002Rec.set(SPACES);
		/* MOVE CHDRLIF-CNTCURR        TO CLNK-CURR-IN.                 */
		/* MOVE CHDRLIF-BILLCURR       TO CLNK-CURR-OUT.                */
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(linsrnlIO.getInstamt06());
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/* MOVE WSAA-TRANSACTION-DATE  TO CLNK-CASHDATE.                */
		conlinkrec.cashdate.set(datcon1rec.intDate);
		/* MOVE 'CVRT'                 TO CLNK-FUNCTION                 */
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(wsaaCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void totalBilled2214()
	{
		linsrnlIO.setCbillamt(conlinkrec.amountOut);
	}

protected void otherFields2215()
	{
		linsrnlIO.setChdrnum(chdrpf.getChdrnum());
		linsrnlIO.setChdrcoy(chdrpf.getChdrcoy());
		linsrnlIO.setBranch(atrtBranch);
		linsrnlIO.setInstfrom(wsaaBtdate);
		/* MOVE CHDRLIF-CNTCURR        TO LINSRNL-CNTCURR.              */
		/* MOVE CHDRLIF-BILLCURR       TO LINSRNL-BILLCURR.             */
		/* MOVE CHDRLIF-BTDATE         TO LINSRNL-INSTTO.               */
		/* MOVE CHDRLIF-BILLFREQ       TO LINSRNL-INSTFREQ.             */
		linsrnlIO.setCntcurr(payrpf.getCntcurr());
		linsrnlIO.setBillcurr(payrpf.getBillcurr());
		linsrnlIO.setInstto(payrpf.getBtdate());
		linsrnlIO.setInstfreq(payrpf.getBillfreq());
		/* MOVE '0'                    TO LINSRNL-PAYFLAG.              */
		linsrnlIO.setPayflag("O");
		linsrnlIO.setTranscode(atrtTransCode);
		linsrnlIO.setInstjctl(atrtBatchKey);
		linsrnlIO.setValidflag(vflagcpy.inForceVal);
		/* MOVE CHDRLIF-BILLCHNL       TO LINSRNL-BILLCHNL.             */
		linsrnlIO.setBillchnl(payrpf.getBillchnl());
		linsrnlIO.setPayrseqno(payrpf.getPayrseqno());
		if (isNE(prasrec.taxrelamt,0)) {
			linsrnlIO.setTaxrelmth(t5688rec.taxrelmth);
		}
		linsrnlIO.setBillcd(wsaaBillcd);
	}

protected void updateLedger2220()
	{
		/* Read T3629 for bank code.*/
		itemIO.setDataArea(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(tablesInner.t3629);
		wsaaItemkey.itemItemitem.set(chdrpf.getBillcurr());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		/* Read T3688 for bank accounting rules.*/
		itemIO.setDataArea(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(tablesInner.t3688);
		wsaaItemkey.itemItemitem.set(t3629rec.bankcode);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3688rec.t3688Rec.set(itemIO.getGenarea());
		/* Get item description.*/

		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), tablesInner.t3688, t3629rec.bankcode.toString(), wsaaCompany.toString(), atmodrec.language.toString());
	
		if (descpf == null) {
			descpf = new Descpf(); //IJTI-462
			descpf.setLongdesc(" ");
		}
		/* Set up LIFRTRN parameters and call 'LIFRTRN' to post the cash*/
		/* to the bank cash account.(In billing currency)*/
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.rdocnum.set(chdrpf.getChdrnum());
		lifrtrnrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifrtrnrec.batccoy.set(wsaaCompany);
		lifrtrnrec.rldgcoy.set(wsaaCompany);
		lifrtrnrec.genlcoy.set(wsaaCompany);
		lifrtrnrec.batcactyr.set(atrtAcctYear);
		lifrtrnrec.batctrcde.set(atrtTransCode);
		lifrtrnrec.batcactmn.set(atrtAcctMonth);
		lifrtrnrec.batcbatch.set(atrtBatch);
		lifrtrnrec.batcbrn.set(atrtBranch);
		lifrtrnrec.sacscode.set("CA");
		lifrtrnrec.sacstyp.set("1 ");
		lifrtrnrec.origcurr.set(chdrpf.getBillcurr());
		if (isEQ(chdrpf.getBillcurr(),chdrpf.getCntcurr())) {
			lifrtrnrec.origamt.set(wsaaTotAmount);
		}
		else {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.currIn.set(chdrpf.getCntcurr());
			conlinkrec.currOut.set(chdrpf.getBillcurr());
			conlinkrec.amountIn.set(wsaaTotAmount);
			conlinkrec.rateUsed.set(0);
			conlinkrec.amountOut.set(0);
			/*     MOVE WSAA-TRANSACTION-DATE   TO CLNK-CASHDATE            */
			conlinkrec.cashdate.set(datcon1rec.intDate);
			/*     MOVE 'CVRT'             TO CLNK-FUNCTION                 */
			conlinkrec.function.set("SURR");
			conlinkrec.company.set(wsaaCompany);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,varcom.oK)) {
				lifrtrnrec.origamt.set(0);
			}
			else {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				c000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				lifrtrnrec.origamt.set(conlinkrec.amountOut);
			}
		}
		lifrtrnrec.rcamt.set(0);
		lifrtrnrec.tranno.set(0);
		lifrtrnrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.effdate.set(wsaaBillDate);
		lifrtrnrec.tranref.set(chdrpf.getChdrnum());
		lifrtrnrec.glsign.set(t3688rec.sign01);
		lifrtrnrec.glcode.set(t3688rec.glmap01);
		lifrtrnrec.user.set(wsaaUser);
		lifrtrnrec.contot.set(t3688rec.cdcontot);
		lifrtrnrec.trandesc.set(descpf.getLongdesc());
		lifrtrnrec.rldgacct.set(t3688rec.bankacckey);
		lifrtrnrec.transactionDate.set(wsaaTransactionDate);
		lifrtrnrec.transactionTime.set(wsaaTransactionTime);
		lifrtrnrec.function.set("NPSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			xxxxFatalError();
		}
		/* Set up LIFRTRN parameters and call 'LIFRTRN' to post the cash*/
		/* to the contract suspense account.*/
		lifrtrnrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifrtrnrec.sacscode.set(t5645rec.sacscode01);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype01);
		lifrtrnrec.rldgacct.set(chdrpf.getChdrnum());
		lifrtrnrec.glcode.set(t5645rec.glmap01);
		lifrtrnrec.glsign.set(t5645rec.sign01);
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Collection
	* </pre>
	*/
protected void collection2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					collection2301();
					processLinsRecord2302();
					ledgerAccountingUpdates2303();
				case skipInitializeAgcm2303:
					skipInitializeAgcm2303();
				case callCovr2304:
					callCovr2304();
				case skipBonusWorkbench2304:
					skipBonusWorkbench2304();
				case continue2305:
					continue2305();
				case callAgcmbchio2307:
					callAgcmbchio2307();
					basicComm2308();
				case servComm2309:
					servComm2309();
				case renlComm2310:
					renlComm2310();
				case endComm2311:
					endComm2311();
				case exit2312:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void collection2301()
	{
		/* Read T5645 for suspense balance.*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(tablesInner.t5645);
		wsaaItemkey.itemItemitem.set(wsaaProg);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T3695 to check the sign.*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(tablesInner.t3695);
		wsaaItemkey.itemItemitem.set(t5645rec.sacstype01);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/* Get transaction description.*/
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), tablesInner.t1688, atrtTransCode.toString(), wsaaCompany.toString(), atmodrec.language.toString());

		if (descpf == null) {
			descpf = new Descpf(); //IJTI-462
			descpf.setLongdesc(" ");
		}
		wsaaTransDesc.set(descpf.getLongdesc());
	}

protected void processLinsRecord2302()
	{
		/* Now we know we have enough money in suspense to cover the*/
		/* instalment. LINS need to be updated.*/
		/* Update LINSRNL with PAYFLAG set to 'P' to denote the*/
		/* transaction has been paid.*/
		linsrnlIO.setPayflag("P");
		linsrnlIO.setFunction(varcom.writr);
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		SmartFileCode.execute(appVars, linsrnlIO);
		
		if ((isNE(linsrnlIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(linsrnlIO.getParams());
			xxxxFatalError();
		}
		
		updateLinsForOtherFee();
		/* Update certain fields in CHDRLIF to denote the instalment has*/
		/* been paid. Most importantly is the outstanding amount to be paid*/
		/* this field should less the payment received.*/
		/* THE ACTUAL RECORD MUST BE UPDATED AT THIS POINT, BECAUSE,*/
		/* THE COMMISSION ROUTINES CALLED RE-READ THE CONTRACT HEADER*/
		/* TO GET THE BILLING DATE !*/
		chdrpf.setPtdate(linsrnlIO.getInstto().toInt());
		chdrpf.setInstjctl(atrtBatchKey.toString());
		chdrpf.setInstfrom(linsrnlIO.getInstfrom().toInt());
		chdrpf.setInstto(linsrnlIO.getInstto().toInt());
		/*    ADD 1                       TO CHDRLIF-TRANNO.               */
		chdrpf.setTranno(wsaaTranno.toInt());
		/*     this is the wrong place to update chdr                   */
		/* MOVE LINSRNL-INSTAMT01      TO CHDRLIF-SINSTAMT01.      <025>*/
		/* MOVE LINSRNL-INSTAMT02      TO CHDRLIF-SINSTAMT02.      <025>*/
		/* MOVE LINSRNL-INSTAMT03      TO CHDRLIF-SINSTAMT03.      <025>*/
		/* MOVE LINSRNL-INSTAMT04      TO CHDRLIF-SINSTAMT04.      <025>*/
		/* MOVE LINSRNL-INSTAMT05      TO CHDRLIF-SINSTAMT05.      <025>*/
		/* MOVE LINSRNL-INSTAMT06      TO CHDRLIF-SINSTAMT06.      <025>*/
		setPrecision(chdrpf.getInsttot01(), 2);
		chdrpf.setInsttot01(chdrpf.getInsttot01().add(new BigDecimal(linsrnlIO.getInstamt01().toString())));
		setPrecision(chdrpf.getInsttot02(), 2);
		chdrpf.setInsttot02(chdrpf.getInsttot02().add(new BigDecimal(linsrnlIO.getInstamt02().toString())));
		setPrecision(chdrpf.getInsttot03(), 2);
		chdrpf.setInsttot03(chdrpf.getInsttot03().add(new BigDecimal(linsrnlIO.getInstamt03().toString())));
		setPrecision(chdrpf.getInsttot04(), 2);
		chdrpf.setInsttot04(chdrpf.getInsttot04().add(new BigDecimal(linsrnlIO.getInstamt04().toString())));
		setPrecision(chdrpf.getInsttot05(), 2);
		chdrpf.setInsttot05(chdrpf.getInsttot05().add(new BigDecimal(linsrnlIO.getInstamt05().toString())));
		setPrecision(chdrpf.getInsttot06(), 2);
		chdrpf.setInsttot06(chdrpf.getInsttot06().add(new BigDecimal(linsrnlIO.getInstamt06().toString())));
		/*    COMPUTE CHDRLIF-OUTSTAMT = CHDRLIF-OUTSTAMT -                */
		/*                               LINSRNL-INSTAMT06.                */
		chdrpf.setOutstamt(BigDecimal.ZERO);
		/* Rewrite the Contract Header Record CHDRLIF.*/

		if (!chdrpfDAO.updateChdrlifRecord(chdrpf)) {
			syserrrec.params.set(chdrpf.getChdrnum());
			xxxxFatalError();
		}
		/* Update the PTDATE on the PAYR record.                           */
		/* firstly, store the old PTDATE for us to use when doing          */
		/*   commission supression testing.                                */
		wsaaPtdate.set(payrpf.getPtdate());
		payrpf.setPtdate(linsrnlIO.getInstto().toInt());
		payrpfDAO.updatePayrpfForBilling(payrpf);
	}

protected void ledgerAccountingUpdates2303()
	{
		/* Once the tranaction has been paid out from the suspence, the*/
		/* suspence account need to be updated(less the amount paid).*/
		updateSuspenseAccount2320();
		/* Update individual ledger accounts(INSTAMT01 to INSTAMT05).*/
		updateIndividualLedger2330();
		/* Generic component processing.*/
		/* INSTAMT06 is the actual life premium which to be invested.      */
		/* MOVE LINSRNL-INSTAMT06              TO WSAA-TOT-AMT.      008*/
		/* Initialise coverage premium table.                              */
		wsaaC.set(1);
		wsaaL.set(1);
		while ( !((isGT(wsaaL,10)))) {
			initialiseCovrPrem8000();
		}

		/* Find the total premium for each coverage and load them into a   */
		/* table so that they can be referenced before calling UNITALOC.   */
		/* This amount is required for enhanced allocation.                */
		covrpf = new Covrpf();
		findCovrTotPrem8100();
		readTh6052345();
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.skipInitializeAgcm2303);
		}
		/* Initialize the AGCM arrays                                      */
		b100InitializeArrays();
		wsaaBillfq.set(payrpf.getBillfreq());
		wsaaAgcmIx.set(0);
		covrpf = new Covrpf();
	}

protected void skipInitializeAgcm2303()
	{
		/* Read in all the relevant COVR records which made up the*/
		/* instalment. Although we are not updating COVR, we need the*/
		/* precise key(life, joint life, coverage, rider, plan suffix) from*/
		/* it to call the generic subroutine to process unit linked*/
		/* investment.*/
	    covrpf = new Covrpf();
	    List<Covrpf> covrpflist = covrpfDAO.getCovrByComAndNumAllFlagRecord(linsrnlIO.getChdrcoy().toString(), linsrnlIO.getChdrnum().toString());
        if(covrpflist == null || covrpflist.size() == 0){
        	goTo(GotoLabel.exit2312);
        } else { //IJTI-462
        	iterator = covrpflist.iterator(); //IJTI-462
        } //IJTI-462
	}

	/**
	* <pre>
	*    MOVE 0                      TO WSAA-JRNSEQ.          <V4L001>
	* </pre>
	*/
protected void callCovr2304()
	{
		
	  if(iterator == null || !iterator.hasNext()){
		  goTo(GotoLabel.exit2312);
	  }
	   covrpf = iterator.next();

		/* Check valid flag 2 on COVR*/
		/*IF COVR-VALIDFLAG           = '2'                            */
		/*   MOVE NEXTR               TO COVR-FUNCTION                 */
		/*   GO TO 2304-CALL-COVR.                                     */
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
				wsaaValidStatus.set("Y");
			}
		}
		if (isNE(wsaaValidStatus, "Y")) {
			callCovr2304();
			return ;
		}
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode())) {
				wsaaValidStatus.set("Y");
			}
		}
		if (isNE(wsaaValidStatus, "Y")) {
			callCovr2304();
			return ;
		}
		/* Read table T5687 to see if this is a single premium          */
		/* Component. If it is then no need to generate Suspense        */
		/* Postings.                                                    */
		readT568711100();
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			goTo(GotoLabel.exit2312);
		}
		/*IF (LINSRNL-INSTFROM > COVR-CURRTO) OR                  <044>*/
		if ((isGTE(linsrnlIO.getInstfrom(),covrpf.getCurrto()))
		|| (isLT(linsrnlIO.getInstfrom(),covrpf.getCurrfrom()))) {
			callCovr2304();
			return ;
		}
		/* Only process coverages for the selected payer.                  */
		/*   IF COVR-PAYRSEQNO NOT = PAYR-PAYRSEQNO                       */
		/*      MOVE NEXTR               TO COVR-FUNCTION                 */
		/*      GO TO 2304-CALL-COVR.                                     */
		/*    IF (LINSRNL-INSTFROM > COVR-CURRTO) OR                       */
		/*       (LINSRNL-INSTFROM < COVR-CURRFROM)                        */
		/*        MOVE NEXTR              TO COVR-FUNCTION                 */
		/*        GO TO 2304-CALL-COVR.                                    */
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.skipBonusWorkbench2304);
		}
		/* Bonus Workbench *                                               */
		if (isNE(covrpf.getInstprem(),ZERO)) {
			wsaaAgcmIx.add(1);
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
			b200PremiumHistory();
			b300WriteArrays();
		}
		else {
			wsaaAgcmIx.add(1);
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
			b200PremiumHistory();
		}
	}

protected void skipBonusWorkbench2304()
	{
		/* COMPUTE WSAA-TOT-AMT = WSAA-TOT-AMT - COVR-INSTPREM.      008*/
		if (isNE(t5688rec.comlvlacc,"Y")
		|| isEQ(covrpf.getInstprem(),0)) { //IJS-584
			goTo(GotoLabel.continue2305);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[6]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[6]);
		lifacmvrec.glsign.set(wsaaT5645Sign[6]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[6]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[6]);
		wsaaRldgChdrnum.set(covrpf.getChdrnum());
		wsaaRldgLife.set(covrpf.getLife());
		wsaaRldgCoverage.set(covrpf.getCoverage());
		wsaaRldgRider.set(covrpf.getRider());
		wsaaPlan.set(covrpf.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		/*MOVE LINSRNL-INSTAMT01              TO LIFA-ORIGAMT.         */
		lifacmvrec.origamt.set(covrpf.getInstprem());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.*/
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.            <021>*/
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Set up LIFACMV parameters and call 'LIFACMV' to post account
	* movements.
	*    IF T5688-COMLVLACC          NOT = 'Y'
	*       GO TO                    2305-CONTINUE.
	*    IF PRAS-TAXRELAMT           NOT > 0
	*       GO TO                    2305-CONTINUE.
	*    MOVE SPACE                  TO LIFA-LIFACMV-REC.
	*    MOVE WSAA-CHDRNUM           TO LIFA-RDOCNUM.
	*    MOVE 0                      TO LIFA-JRNSEQ.
	*    MOVE WSAA-COMPANY           TO LIFA-BATCCOY
	*                                   LIFA-RLDGCOY
	*                                   LIFA-GENLCOY.
	*    MOVE ATRT-ACCT-YEAR         TO LIFA-BATCACTYR.
	*    MOVE ATRT-TRANS-CODE        TO LIFA-BATCTRCDE.
	*    MOVE ATRT-ACCT-MONTH        TO LIFA-BATCACTMN.
	*    MOVE ATRT-BATCH             TO LIFA-BATCBATCH.
	*    MOVE ATRT-BRANCH            TO LIFA-BATCBRN.
	*    MOVE WSAA-T5645-SACSCODE(14) TO LIFA-SACSCODE.
	*    MOVE WSAA-T5645-SACSTYPE(14) TO LIFA-SACSTYP.
	*    MOVE WSAA-T5645-SIGN(14)     TO LIFA-GLSIGN.
	*    MOVE WSAA-T5645-GLMAP(14)    TO LIFA-GLCODE.
	*    MOVE WSAA-T5645-CNTTOT(14)   TO LIFA-CONTOT.
	*    MOVE CHDRLIF-CNTCURR        TO LIFA-ORIGCURR.
	*    MOVE PRAS-TAXRELAMT         TO LIFA-ORIGAMT.
	*    MOVE 0                      TO LIFA-RCAMT
	*                                   LIFA-CRATE
	*                                   LIFA-ACCTAMT.
	*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.
	*    MOVE WSAA-TRANNO            TO LIFA-TRANNO.
	*    MOVE VRCM-MAX-DATE          TO LIFA-FRCDATE.
	*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.
	*    MOVE LINSRNL-CHDRNUM        TO LIFA-TRANREF.
	*    MOVE WSAA-USER              TO LIFA-USER.
	*    MOVE CHDRLIF-CNTTYPE        TO LIFA-SUBSTITUTE-CODE(1).
	*    MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).
	*    MOVE WSAA-TRANS-DESC        TO LIFA-TRANDESC.
	*    MOVE PRAS-INREVNUM          TO WSAA-RLDG-CHDRNUM.
	*    MOVE COVR-LIFE              TO WSAA-RLDG-LIFE.
	*    MOVE COVR-COVERAGE          TO WSAA-RLDG-COVERAGE.
	*    MOVE COVR-RIDER             TO WSAA-RLDG-RIDER.
	*    MOVE COVR-PLAN-SUFFIX       TO WSAA-PLAN.
	*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.
	*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.
	*    MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.
	*    MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.
	*    MOVE 'PSTW'                 TO LIFA-FUNCTION.
	*    CALL 'LIFACMV' USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ NOT = O-K
	*        MOVE LIFA-LIFACMV-REC   TO SYSR-PARAMS
	*        PERFORM XXXX-FATAL-ERROR.
	* </pre>
	*/
protected void continue2305()
	{
		/* Call the generic processing for each relevant COVR record read.*/
		if(allFrequencyCompleted) {
			genericProcessing2340();
		}
		/*COMMISSION-UPDATING*/
		/*MOVE  0 TO WSAA-JRNSEQ.                                      */
		agcmbchIO.setDataArea(SPACES);
		/*MOVE LINSRNL-CHDRNUM        TO AGCMBCH-CHDRNUM.      <LA2108>*/
		agcmbchIO.setChdrnum(covrpf.getChdrnum());
		/*MOVE LINSRNL-CHDRCOY        TO AGCMBCH-CHDRCOY.      <LA2108>*/
		agcmbchIO.setChdrcoy(covrpf.getChdrcoy());
		agcmbchIO.setLife(covrpf.getLife());
		agcmbchIO.setCoverage(covrpf.getCoverage());
		agcmbchIO.setRider(covrpf.getRider());
		agcmbchIO.setPlanSuffix(covrpf.getPlanSuffix());
		agcmbchIO.setFunction(varcom.begnh);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
	}

protected void callAgcmbchio2307()
	{
		SmartFileCode.execute(appVars, agcmbchIO);
		if ((isNE(agcmbchIO.getStatuz(),varcom.oK))
		&& (isNE(agcmbchIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(agcmbchIO.getParams());
			xxxxFatalError();
		}
		/*    IF AGCMBCH-CURRFROM          = ZEROES                <V4L001>*/
		/*    OR AGCMBCH-EFDATE          = ZEROS                   <V4L001>*/
		/*    OR AGCMBCH-CURRTO          = ZEROS                   <V4L001>*/
		/*       CONTINUE                                          <V4L001>*/
		/*    END-IF.                                              <V4L001>*/
		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.callCovr2304);
		}
		if (isNE(agcmbchIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(agcmbchIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(agcmbchIO.getLife(),covrpf.getLife())
		|| isNE(agcmbchIO.getCoverage(),covrpf.getCoverage())
		|| isNE(agcmbchIO.getRider(),covrpf.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(),covrpf.getPlanSuffix())) {
			goTo(GotoLabel.callCovr2304);
		}
		/*        GO TO 2312-EXIT.                                <044>*/
		/* Do not process Single Premium AGCMs                          */
		/* (paid-to date = zeroes).                                     */
		if (isEQ(agcmbchIO.getPtdate(),ZERO)
		|| isEQ(agcmbchIO.getDormantFlag(),"Y")) {
			/*     MOVE REWRT              TO AGCMBCH-FUNCTION      <LA4018>*/
			/*     MOVE AGCMBCHREC         TO AGCMBCH-FORMAT        <LA4018>*/
			/*     CALL 'AGCMBCHIO'        USING AGCMBCH-PARAMS     <LA4018>*/
			/*     IF AGCMBCH-STATUZ    NOT = O-K                   <LA4018>*/
			/*         MOVE AGCMBCH-STATUZ TO SYSR-STATUZ           <LA4018>*/
			/*         MOVE AGCMBCH-PARAMS TO SYSR-PARAMS           <LA4018>*/
			/*         PERFORM XXXX-FATAL-ERROR                     <LA4018>*/
			/*     END-IF                                           <LA4018>*/
			agcmbchIO.setFunction(varcom.nextr);
			callAgcmbchio2307();
			return ;
		}
		/*    IF (AGCMBCH-CHDRNUM NOT = LINSRNL-CHDRNUM) OR        <LA2108>*/
		/*       (AGCMBCH-CHDRCOY NOT = LINSRNL-CHDRCOY)           <LA2108>*/
		/*        MOVE REWRT              TO AGCMBCH-FUNCTION      <LA2108>*/
		/*        CALL 'AGCMBCHIO' USING AGCMBCH-PARAMS            <LA2108>*/
		/*        IF AGCMBCH-STATUZ NOT = O-K                      <LA2108>*/
		/*            MOVE AGCMBCH-PARAMS TO SYSR-PARAMS           <LA2108>*/
		/*            PERFORM XXXX-FATAL-ERROR                             */
		/*        ELSE                                                     */
		/*            GO TO 2312-EXIT.                                     */
		/*PERFORM 2360-GET-CRTABLE.                                    */
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
		comlinkrec.clnkallRec.set(SPACES);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		/* MOVE 0                      TO CLNK-ERNDAMT.                 */
		if (isNE(chdrpf.getBillfreq(),NUMERIC)) {
			chdrpf.setBillfreq("01");
		}
		wsaaBillfreq.set(chdrpf.getBillfreq());
		comlinkrec.billfreq.set(chdrpf.getBillfreq());
		compute(comlinkrec.instprem, 3).setRounded(div(agcmbchIO.getAnnprem(),wsaaBillfreqNum));
		/*MOVE LINSRNL-CHDRNUM        TO CLNK-CHDRNUM.                 */
		comlinkrec.chdrnum.set(agcmbchIO.getChdrnum());
		/*MOVE LINSRNL-CHDRCOY        TO CLNK-CHDRCOY.                 */
		comlinkrec.chdrcoy.set(agcmbchIO.getChdrcoy());
		comlinkrec.life.set(agcmbchIO.getLife());
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.coverage.set(agcmbchIO.getCoverage());
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.rider.set(agcmbchIO.getRider());
		comlinkrec.planSuffix.set(agcmbchIO.getPlanSuffix());
		//ILIFE-1345 STARTS
		compute(comlinkrec.targetPrem, 3).setRounded(mult(covrpf.getInstprem(),wsaaBillfreq));
		comlinkrec.seqno.set(agcmbchIO.getSeqno());		
		comlinkrec.currto.set(agcmbchIO.getPtdate());
		//ILIFE-1345 ENDS
		comlinkrec.crtable.set(covrpf.getCrtable());
		/*    MOVE CHDRLIF-OCCDATE        TO CLNK-OCCDATE.*/
		/*MOVE AGCMBCH-EFDATE         TO CLNK-OCCDATE.                 */
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.icommtot.set(agcmbchIO.getInitcom());
		comlinkrec.icommpd.set(agcmbchIO.getCompay());
		comlinkrec.icommernd.set(agcmbchIO.getComern());
		comlinkrec.ptdate.set(0);
		comlinkrec.language.set(atmodrec.language);
		/* Read T5644 to get generic subroutine for basic commission.*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(tablesInner.t5644);
		wsaaItemkey.itemItemitem.set(agcmbchIO.getBascpy());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaBascpySubprog.set(t5644rec.compysubr);
		/* Read T5644 to get generic subroutine for servicing commission.*/
		wsaaItemkey.itemItemitem.set(agcmbchIO.getSrvcpy());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaSrvcpySubprog.set(t5644rec.compysubr);
		/* Read T5644 to get generic subroutine for renewal commission.*/
		wsaaItemkey.itemItemitem.set(agcmbchIO.getRnwcpy());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaRnwcpySubprog.set(t5644rec.compysubr);
	}

protected void basicComm2308()
	{
		if (isEQ(wsaaBascpySubprog,SPACES)) {
			goTo(GotoLabel.servComm2309);
		}
		comlinkrec.method.set(agcmbchIO.getBascpy());
		/*  Read ZRAP to get ZRORCODE, this one is valid only for Overide  */
		/*  Agent                                                          */
		/*  PERFORM  A100-READ-ZRAP                                      */
		callProgram(wsaaBascpySubprog, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		if (isEQ(agcmbchIO.getOvrdcat(),"O")) {
			wsaaOvrdBascpyDue.add(comlinkrec.payamnt);
			wsaaOvrdBascpyErn.add(comlinkrec.erndamt);
			compute(wsaaOvrdBascpyPay, 2).set(add(wsaaOvrdBascpyPay,(sub(comlinkrec.payamnt,comlinkrec.erndamt))));
		}
		else {
			wsaaBascpyDue.add(comlinkrec.payamnt);
			wsaaBascpyErn.add(comlinkrec.erndamt);
			compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay,(sub(comlinkrec.payamnt,comlinkrec.erndamt))));
			/*****    IF   WSAA-COMPAY-KEPT   = ZEROES                          */
			/*****         MOVE AGCMBCH-AGNTNUM TO WSAA-AGENT-KEPT      <LA2108>*/
			/*****         MOVE CLNK-PAYAMNT    TO WSAA-COMPAY-KEPT             */
			/*****    END-IF                                                    */
		}
		setPrecision(agcmbchIO.getCompay(), 2);
		agcmbchIO.setCompay(add(agcmbchIO.getCompay(),comlinkrec.payamnt));
		setPrecision(agcmbchIO.getComern(), 2);
		agcmbchIO.setComern(add(agcmbchIO.getComern(),comlinkrec.erndamt));
		/*ADD CLNK-PAYAMNT            TO WSAA-BASCPY-DUE               */
		/*                               AGCMBCH-COMPAY.       <LA2108>*/
		/*ADD CLNK-ERNDAMT            TO WSAA-BASCPY-ERN               */
		/*                               AGCMBCH-COMERN.       <LA2108>*/
		/*COMPUTE WSAA-BASCPY-PAY = WSAA-BASCPY-PAY +                  */
		/*                          (CLNK-PAYAMNT - CLNK-ERNDAMT).     */
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.servComm2309);
		}
		/* Bonus Workbench *                                               */
		/* Initial commission                                              */
		if (isEQ(agcmbchIO.getOvrdcat(),"B")
		&& isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			b400LocatePremium();
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(div(wsaaAgcmPremium,wsaaBillfq9), true);
			zrdecplrec.amountIn.set(zctnIO.getPremium());
			c000CallRounding();
			zctnIO.setPremium(zrdecplrec.amountOut);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(),100),wsaaAgcmPremium), true);
			zctnIO.setZprflg("I");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(wsaaTranno);
			zctnIO.setTransCode(atrtTransCode);
			zctnIO.setEffdate(linsrnlIO.getInstfrom());
			zctnIO.setTrandate(datcon1rec.intDate);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void servComm2309()
	{
		if (isEQ(wsaaSrvcpySubprog,SPACES)) {
			goTo(GotoLabel.renlComm2310);
		}
		comlinkrec.method.set(agcmbchIO.getSrvcpy());
		callProgram(wsaaSrvcpySubprog, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaSrvcpyDue.add(comlinkrec.payamnt);
		setPrecision(agcmbchIO.getScmdue(), 2);
		agcmbchIO.setScmdue(add(agcmbchIO.getScmdue(),comlinkrec.payamnt));
		wsaaSrvcpyErn.add(comlinkrec.erndamt);
		setPrecision(agcmbchIO.getScmearn(), 2);
		agcmbchIO.setScmearn(add(agcmbchIO.getScmearn(),comlinkrec.erndamt));
	}

protected void renlComm2310()
	{
		/* IF WSAA-RNWCPY-SUBPROG = SPACE                               */
		/*     GO TO 2311-END-COMM.                                     */
		if (isEQ(wsaaRnwcpySubprog,SPACES)
		|| (isEQ(chdrpf.getRnwlsupr(),"Y")
		&& isGTE(wsaaPtdate,chdrpf.getRnwlspfrom())
		&& isLTE(wsaaPtdate,chdrpf.getRnwlspto()))) {
			goTo(GotoLabel.endComm2311);
		}
		comlinkrec.method.set(agcmbchIO.getRnwcpy());
		callProgram(wsaaRnwcpySubprog, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaRnwcpyDue.add(comlinkrec.payamnt);
		setPrecision(agcmbchIO.getRnlcdue(), 2);
		agcmbchIO.setRnlcdue(add(agcmbchIO.getRnlcdue(),comlinkrec.payamnt));
		wsaaRnwcpyErn.add(comlinkrec.erndamt);
		setPrecision(agcmbchIO.getRnlcearn(), 2);
		agcmbchIO.setRnlcearn(add(agcmbchIO.getRnlcearn(),comlinkrec.erndamt));
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.endComm2311);
		}
		/* Bonus Workbench *                                               */
		/* Initial commission                                              */
		if (isEQ(agcmbchIO.getOvrdcat(),"B")
		&& isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			b400LocatePremium();
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(div(wsaaAgcmPremium,wsaaBillfq9), true);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(),100),wsaaAgcmPremium), true);
			zctnIO.setZprflg("R");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(wsaaTranno);
			zctnIO.setTransCode(atrtTransCode);
			zctnIO.setEffdate(linsrnlIO.getInstfrom());
			zctnIO.setTrandate(datcon1rec.intDate);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void endComm2311()
	{
		//ILIFE-1345 STARTS
		datcon2rec.frequency.set(chdrpf.getBillfreq());
		datcon2rec.intDate1.set(agcmbchIO.getPtdate());
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcoIO.setTargfrom(datcon2rec.intDate2);
		//ILIFE-1345 ENDS
		agcmbchIO.setPtdate(chdrpf.getPtdate());
		agcmbchIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(wsaaBascpyDue,0))
		|| (isNE(wsaaBascpyPay,0))
		|| (isNE(wsaaBascpyErn,0))
		|| (isNE(wsaaSrvcpyDue,0))
		|| (isNE(wsaaSrvcpyErn,0))
		|| (isNE(wsaaRnwcpyDue,0))
		|| (isNE(wsaaRnwcpyErn,0))
		|| (isNE(wsaaOvrdBascpyPay,0))
		|| (isNE(wsaaOvrdBascpyDue,0))
		|| (isNE(wsaaOvrdBascpyErn,0))) {
			readTh6052345();
			updateCommLedger2350();
		}
		agcmbchIO.setFunction(varcom.nextr);
		//ILIFE-1345 STARTS
		readTableT57291150();
		if (flexiblePremiumContract.isTrue()) {
			
			fpcoIO.setFormat(formatsInner.fpcorec);
			fpcoIO.setFunction(varcom.readh);
			readFpco7200();
			if (isLT(fpcoIO.getPremRecPer(),fpcoIO.getTargetPremium())) {
				
			}
			else {
				if (isGTE(fpcoIO.getBilledInPeriod(),fpcoIO.getTargetPremium())) {
					fpcoIO.setActiveInd("N");
				}
			}
			fpcoIO.setAnnProcessInd("Y");
			fpcoIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, fpcoIO);
			if (isNE(fpcoIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(fpcoIO.getParams());
				xxxxFatalError();
			}
			writeFpco7000();
		}
		//ILIFE-1345 ENDS
		goTo(GotoLabel.callAgcmbchio2307);
	}
//ILIFE-1345 STARTS
protected void readFpco7200()
{
	fpcoIO.setChdrcoy(chdrpf.getChdrcoy());
	fpcoIO.setChdrnum(chdrpf.getChdrnum());
	fpcoIO.setLife(covrpf.getLife());
	fpcoIO.setJlife(covrpf.getJlife());
	fpcoIO.setCoverage(covrpf.getCoverage());
	fpcoIO.setRider(covrpf.getRider());
	fpcoIO.setPlanSuffix(covrpf.getPlanSuffix());
	SmartFileCode.execute(appVars, fpcoIO);
	if (isNE(fpcoIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(fpcoIO.getParams());
		xxxxFatalError();
	}
}
protected void readTableT57291150()
{
	
	notFlexiblePremiumContract.setTrue();
	/*  Read T5729.                                            <D9604>*/
	itdmIO.setStatuz(varcom.oK);
	itdmIO.setItemcoy(chdrpf.getChdrcoy());
	itdmIO.setItemtabl(tablesInner.t5729);
	itdmIO.setItemitem(chdrpf.getCnttype());
	itdmIO.setItmto(0);
	itdmIO.setItmfrm(chdrpf.getOccdate());
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(),varcom.oK)
	&& isNE(itdmIO.getStatuz(),varcom.endp)) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrpf.getChdrcoy(), SPACES);
		stringVariable1.addExpression(tablesInner.t5729, SPACES);
		stringVariable1.addExpression(chdrpf.getCnttype(), SPACES);
		stringVariable1.setStringInto(syserrrec.params);
		syserrrec.statuz.set(itdmIO.getStatuz());
		xxxxFatalError();
	}
	if (isEQ(itdmIO.getStatuz(),varcom.endp)
	|| isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
	|| isNE(itdmIO.getItemtabl(), tablesInner.t5729)
	|| isNE(itdmIO.getItemitem(),chdrpf.getCnttype())
	|| isGT(itdmIO.getItmfrm(),chdrpf.getOccdate())
	|| isLT(itdmIO.getItmto(),chdrpf.getOccdate())) {
		return ;
	}
	t5729rec.t5729Rec.set(itdmIO.getGenarea());
	flexiblePremiumContract.setTrue();
}
protected void writeFpco7000()
	{				
		
		fpcoIO.setChdrcoy(SPACES);
		fpcoIO.setChdrnum(SPACES);
		fpcoIO.setLife(SPACES);
		fpcoIO.setCoverage(SPACES);
		fpcoIO.setRider(SPACES);
		fpcoIO.setActiveInd(SPACES);
		fpcoIO.setPlanSuffix(ZERO);
		fpcoIO.setCurrfrom(ZERO);
		fpcoIO.setTargfrom(ZERO);		
		fpcoIO.setValidflag("1");
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(wsaaCurrentPayuptoDate);
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcoIO.setCurrfrom(datcon2rec.intDate2);
		fpcoIO.setTargfrom(datcon2rec.intDate2);
		fpcoIO.setEffdate(datcon2rec.intDate2);
		fpcoIO.setCurrto(varcom.vrcmMaxDate);		
		fpcoIO.setTargto(wsaaCurrentPayuptoDate);
		fpcoIO.setAnnivProcDate(wsaaCurrentPayuptoDate);
		fpcoIO.setActiveInd("Y");		
		setPrecision(fpcoIO.getTargetPremium(), 2);
		fpcoIO.setTargetPremium(mult(covrpf.getInstprem(),payrpf.getBillfreq()));
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(chdrpf.getOccdate());
		datcon3rec.intDate2.set(fpcoIO.getTargto());
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		wsaaFpcoFreqFactor.set(datcon3rec.freqFactor);
		
		if (isEQ(wsaaFpcoFreqFactor, ZERO)) {
			fpcoIO.setPremRecPer(ZERO);
			fpcoIO.setBilledInPeriod(ZERO);
		}
		else {
			setPrecision(fpcoIO.getPremRecPer(), 2);
			fpcoIO.setPremRecPer(mult(covrpf.getInstprem(), wsaaFpcoFreqFactor));
			setPrecision(fpcoIO.getBilledInPeriod(), 2);
			fpcoIO.setBilledInPeriod(mult(covrpf.getInstprem(), wsaaFpcoFreqFactor));
		}
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub,6)
		|| isEQ(t5729rec.frqcy[wsaaT5729Sub.toInt()], payrpf.getBillfreq())); wsaaT5729Sub.add(1))
	{
			/*CONTINUE_STMT*/
		}
		wsaaMinOverdue.set(ZERO);
		if (isEQ(wsaaT5729Sub,1)){
			wsaaMinOverdue.set(t5729rec.overdueMina01);
		}
		else if (isEQ(wsaaT5729Sub,2)){
			wsaaMinOverdue.set(t5729rec.overdueMinb01);
		}
		else if (isEQ(wsaaT5729Sub,3)){
			wsaaMinOverdue.set(t5729rec.overdueMinc01);
		}
		else if (isEQ(wsaaT5729Sub,4)){
			wsaaMinOverdue.set(t5729rec.overdueMind01);
		}
		else if (isEQ(wsaaT5729Sub,5)){
			wsaaMinOverdue.set(t5729rec.overdueMine01);
		}
		else if (isEQ(wsaaT5729Sub,6)){
			wsaaMinOverdue.set(t5729rec.overdueMinf01);
		}
		else{
			syserrrec.params.set(tablesInner.t5729);
			xxxxFatalError();
		}
		fpcoIO.setMinOverduePer(wsaaMinOverdue);
		setPrecision(fpcoIO.getOverdueMin(), 3);
		fpcoIO.setOverdueMin(mult(div(wsaaMinOverdue, 100), (mult(covrpf.getInstprem(), wsaaFpcoFreqFactor))), true);
		zrdecplrec.amountIn.set(fpcoIO.getOverdueMin());
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		c000CallRounding();
		fpcoIO.setOverdueMin(zrdecplrec.amountOut);
		fpcoIO.setAnnProcessInd(SPACES);
		fpcoIO.setTranno(chdrpf.getTranno());
		fpcoIO.setFormat(formatsInner.fpcorec);
		fpcoIO.setFunction(varcom.writr);
		readFpco7200();
	}
//ILIFE-1345 ENDS
protected void updateSuspenseAccount2320()
	{
		try {
			updateSuspenseAccount2321();
			conversionControl2322();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateSuspenseAccount2321()
	{
		/* Reduce amount in suspense account. This is to be done by*/
		/* calling the subroutine LIFRTRN.*/
		/* Set up LIFRTRN parameters and call 'LIFRTRN' to update the*/
		/* ledger.*/
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.rdocnum.set(linsrnlIO.getChdrnum());
		lifrtrnrec.jrnseq.set(0);
		lifrtrnrec.batccoy.set(wsaaCompany);
		lifrtrnrec.rldgcoy.set(wsaaCompany);
		lifrtrnrec.genlcoy.set(wsaaCompany);
		lifrtrnrec.batcactyr.set(atrtAcctYear);
		lifrtrnrec.batctrcde.set(atrtTransCode);
		lifrtrnrec.batcactmn.set(atrtAcctMonth);
		lifrtrnrec.batcbatch.set(atrtBatch);
		lifrtrnrec.batcbrn.set(atrtBranch);
		lifrtrnrec.contot.set(t5645rec.cnttot01);
		lifrtrnrec.sacscode.set(t5645rec.sacscode01);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype01);
		lifrtrnrec.glsign.set(t5645rec.sign01);
		lifrtrnrec.glcode.set(t5645rec.glmap01);
		/*    MOVE CHDRLIF-TRANNO         TO LIFR-TRANNO.*/
		lifrtrnrec.tranno.set(wsaaTranno);
		lifrtrnrec.origcurr.set(linsrnlIO.getBillcurr());
		/* MOVE LINSRNL-CBILLAMT       TO LIFR-ORIGAMT.                 */
		/* MOVE WSAA-NET-CBILLAMT      TO LIFR-ORIGAMT.            <018>*/
		compute(lifrtrnrec.origamt, 2).set(sub(wsaaNetCbillamt,linsrnlIO.getInstamt03()));
		lifrtrnrec.rcamt.set(0);
		lifrtrnrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		/*    MOVE DTC1-INT-DATE          TO LIFR-EFFDATE.                 */
		lifrtrnrec.effdate.set(linsrnlIO.getInstfrom());
		lifrtrnrec.tranref.set(linsrnlIO.getChdrnum());
		lifrtrnrec.user.set(wsaaUser);
		lifrtrnrec.substituteCode[1].set(chdrpf.getCnttype());
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.rldgacct.set(chdrpf.getChdrnum());
		lifrtrnrec.transactionDate.set(wsaaTransactionDate);
		lifrtrnrec.transactionTime.set(wsaaTransactionTime);
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			xxxxFatalError();
		}
		if (isEQ(linsrnlIO.getCntcurr(),linsrnlIO.getBillcurr())) {
			goTo(GotoLabel.exit2329);
		}
	}

protected void conversionControl2322()
	{
		/* Call LIFACMV to write 2 ACMV records for the two currencies.*/
		/* Contract currency.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linsrnlIO.getChdrnum());
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.tranno.set(chdrpf.getTranno());
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.origcurr.set(linsrnlIO.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt06());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.             021>*/
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Billing currency.                                               */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.                  */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.            <021>*/
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		wsaaRldgChdrnum.set(covrpf.getChdrnum());
		wsaaRldgLife.set(covrpf.getLife());
		wsaaRldgCoverage.set(covrpf.getCoverage());
		wsaaRldgRider.set(covrpf.getRider());
		wsaaPlan.set(covrpf.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.contot.set(t5645rec.cnttot15);
		lifacmvrec.sacscode.set(t5645rec.sacscode15);
		lifacmvrec.sacstyp.set(t5645rec.sacstype15);
		lifacmvrec.glsign.set(t5645rec.sign15);
		lifacmvrec.glcode.set(t5645rec.glmap15);
		lifacmvrec.origcurr.set(linsrnlIO.getBillcurr());
		lifacmvrec.origamt.set(linsrnlIO.getCbillamt());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void updateIndividualLedger2330()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					premium2331();
				case fee2332:
					fee2332();
				case tolerance2333:
					tolerance2333();
				case stampDuty2334:
					stampDuty2334();
				case spare2335:
					spare2335();
				case taxRelief2336:
					taxRelief2336();
				case checkTax2337:
					checkTax2337();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void premium2331()
	{
		/* Update ledger and accounting details for the instalment*/
		/* 'premium'.*/
		/* If the instalment premium has been updated ie not = zero        */
		/* move to the line record.                                        */
		/*IF WSAA-INST-PREM NOT = ZERO                           <025> */
		/*   MOVE WSAA-INST-PREM     TO LINSRNL-INSTAMT01.      <025> */
		if (isLTE(linsrnlIO.getInstamt01(),0)) {
			goTo(GotoLabel.fee2332);
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			goTo(GotoLabel.fee2332);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt01());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.*/
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.            <021>*/
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void fee2332()
	{
		/* Update ledger and accounting details for the instalment*/
		/* 'Fee'.*/
		if (isLTE(linsrnlIO.getInstamt02(),0)) {
			goTo(GotoLabel.tolerance2333);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account*/
		/* movements.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt02());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.*/
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linsrnlIO.getChdrnum());
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void tolerance2333()
	{
		/* Update ledger and accounting details for the instalment*/
		/* 'Tolerance'.*/
		if (isLTE(linsrnlIO.getInstamt03(),0)) {
			goTo(GotoLabel.stampDuty2334);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account*/
		/* movements.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		/*    MOVE T5645-SACSCODE-05      TO LIFA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE-05      TO LIFA-SACSTYP.                 */
		/*    MOVE T5645-SIGN-05          TO LIFA-GLSIGN.                  */
		/*    MOVE T5645-GLMAP-05         TO LIFA-GLCODE.                  */
		/*    MOVE T5645-CNTTOT-05        TO LIFA-CONTOT.                  */
		if (isEQ(wsaaToleranceFlag,"1")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode32);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype32);
			lifacmvrec.glsign.set(wsaaT5645Sign32);
			lifacmvrec.glcode.set(wsaaT5645Glmap32);
			lifacmvrec.contot.set(wsaaT5645Cnttot32);
			lifacmvrec.rldgacct.set(chdrpf.getAgntnum());
		}
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt03());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.*/
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/*   MOVE WSAA-CHDRNUM           TO LIFA-RLDGACCT.        <V42013>*/
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")
		&& isNE(wsaaToleranceFlag, "1")) {
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			zorlnkrec.annprem.set(lifacmvrec.origamt);
			b500CallZorcompy();
		}
	}

protected void stampDuty2334()
	{
		/* Update ledger and accounting details for the instalment*/
		/* 'Stamp Duty'.*/
		if (isLTE(linsrnlIO.getInstamt04(),0)) {
			goTo(GotoLabel.spare2335);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account*/
		/* movements.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt04());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.*/
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void spare2335()
	{
		/* Update ledger and accounting details for the instalment*/
		/* 'Spare'.*/
		if (isLTE(linsrnlIO.getInstamt05(),0)) {
			/* GO TO 2339-EXIT.                                         */
			goTo(GotoLabel.taxRelief2336);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account*/
		/* movements.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt05());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.*/
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void taxRelief2336()
	{
		/* Update ledger and accounting details for the instalment         */
		/* 'Tax Relief'.                                                   */
		/* IF T5688-COMLVLACC         = 'Y'                        <021>*/
		/*     GO TO                  2339-EXIT.                   <021>*/
		if (isLTE(prasrec.taxrelamt,0)) {
			/*      GO TO 2339-EXIT.                                 <V74L01>*/
			goTo(GotoLabel.checkTax2337);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[5]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[5]);
		lifacmvrec.glsign.set(wsaaT5645Sign[5]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[5]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[5]);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(prasrec.taxrelamt);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.                  */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.            <021>*/
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/*  MOVE PRAS-INREVNUM          TO LIFA-RLDGACCT.        <A06541>*/
		lifacmvrec.rldgacct.set(chdrpf.getChdrnum());
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void checkTax2337()
	{
		/*    IF WSAA-TAX              NOT = ZERO                          */
		b700PostTax();
		/*EXIT*/
	}

protected void genericProcessing2340()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readT56712341();
				case bypassDate2340:
					bypassDate2340();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT56712341()
	{
		/* Read T5671 to for the generic subroutines.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(tablesInner.t5671);
		wsaaItemCrtable.set(covrpf.getCrtable());
		wsaaItemTranCode.set(atrtTransCode);
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		rnlallrec.rnlallRec.set(SPACES);
		rnlallrec.company.set(covrpf.getChdrcoy());
		rnlallrec.chdrnum.set(covrpf.getChdrnum());
		rnlallrec.life.set(covrpf.getLife());
		wsaaLifeNum.set(covrpf.getLife());
		rnlallrec.coverage.set(covrpf.getCoverage());
		wsaaCoverageNum.set(covrpf.getCoverage());
		rnlallrec.rider.set(covrpf.getRider());
		rnlallrec.planSuffix.set(covrpf.getPlanSuffix());
		/* MOVE CHDRLIF-BILLCD         TO RNLA-BILLCD.                  */
		rnlallrec.billcd.set(payrpf.getBillcd());
		rnlallrec.duedate.set(chdrpf.getInstfrom());
		rnlallrec.user.set(wsaaUser);
		rnlallrec.crdate.set(covrpf.getCrrcd());
		rnlallrec.crtable.set(covrpf.getCrtable());
		if (isNE(totalPremiumAmount,covrpf.getInstprem())) {
			rnlallrec.totrecd.set(totalPremiumAmount);
			rnlallrec.covrInstprem.set(totalPremiumAmount);
		}
		else {
			rnlallrec.totrecd.set(ZERO);
			rnlallrec.covrInstprem.set(wsaaCovrInstprem[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()]);
		}
		//rnlallrec.totrecd.set(ZERO);
		rnlallrec.language.set(atmodrec.language);
		/* If an effective date has been passed, then use this !*/
		/* IF WSAA-UNIT-EFFDATE        NOT = VRCM-MAX-DATE*/
		/*                         AND NOT = 0*/
		/*    MOVE WSAA-UNIT-EFFDATE   TO RNLA-EFFDATE*/
		/*    GO TO 2348-BYPASS-DATE.*/
		/* Read T6647 to determine the sort of date to be used as*/
		/* the efeective date for unit allocation.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsaaCompany);
		itdmIO.setItemtabl(tablesInner.t6647);
		itdmIO.setItmfrm(chdrpf.getPtdate());
		wsaaT6647Batctrcde.set(atrtTransCode);
		wsaaT6647Cnttype.set(chdrpf.getCnttype());
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(itdmIO.getItemcoy(),wsaaCompany))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Item))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
		if (isNE(t6647rec.unitStatMethod,SPACES)) {
			a300ReadT6659();
		}
		if (isNE(wsaaUnitEffdate,varcom.vrcmMaxDate)
		&& isNE(wsaaUnitEffdate,0)) {
			rnlallrec.effdate.set(wsaaUnitEffdate);
			/*       GO TO 2348-BYPASS-DATE.                                   */
			goTo(GotoLabel.bypassDate2340);
		}
		rnlallrec.effdate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode,"DD")) {
			rnlallrec.effdate.set(covrpf.getCrrcd());
		}
		if (isEQ(t6647rec.efdcode,"LO")) {
			if (isGT(covrpf.getCrrcd(),datcon1rec.intDate)) {
				rnlallrec.effdate.set(covrpf.getCrrcd());
			}
		}
	}

protected void bypassDate2340()
	{
		/*    MOVE CHDRLIF-TRANNO         TO RNLA-TRANNO.*/
		rnlallrec.tranno.set(wsaaTranno);
		/* Use entries 15(invested premium) and 16(uninvested premium).    */
		/* RNLALLREC has been extended to cater for the extra linkage.     */
		/*MOVE T5645-SACSCODE-03      TO RNLA-SACSCODE.                */
		/*MOVE T5645-SACSTYPE-03      TO RNLA-SACSTYP.                 */
		/*MOVE T5645-GLMAP-03         TO RNLA-GENLCDE.                 */
		/*MOVE T5645-SACSCODE-15      TO RNLA-SACSCODE.         <002>  */
		/*MOVE T5645-SACSTYPE-15      TO RNLA-SACSTYP.          <002>  */
		/*MOVE T5645-GLMAP-15         TO RNLA-GENLCDE.          <002>  */
		/*MOVE WSAA-T5645-SACSCODE-16 TO RNLA-SACSCODE-02.      <013>  */
		/*MOVE WSAA-T5645-SACSTYPE-16 TO RNLA-SACSTYP-02.       <013>  */
		/*MOVE WSAA-T5645-GLMAP-16    TO RNLA-GENLCDE-02.       <013>  */
		/*MOVE WSAA-T5645-SACSCODE (01) TO RNLA-SACSCODE-02.    <013>  */
		/*MOVE WSAA-T5645-SACSTYPE (01) TO RNLA-SACSTYP-02.     <013>  */
		/*MOVE WSAA-T5645-GLMAP    (01) TO RNLA-GENLCDE-02.     <013>  */
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[11]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[11]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[11]);
		}
		else {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[1]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[1]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[1]);
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[12]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[12]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[12]);
		}
		else {
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[2]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[2]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[2]);
		}
		rnlallrec.cntcurr.set(chdrpf.getCntcurr());
		rnlallrec.cnttype.set(chdrpf.getCnttype());
		rnlallrec.batcactyr.set(atrtAcctYear);
		rnlallrec.batctrcde.set(atrtTransCode);
		rnlallrec.batcactmn.set(atrtAcctMonth);
		rnlallrec.batcbatch.set(atrtBatch);
		rnlallrec.batcbrn.set(atrtBranch);
		rnlallrec.batccoy.set(wsaaCompany);
		rnlallrec.billfreq.set(chdrpf.getBillfreq());
		rnlallrec.moniesDate.set(rnlallrec.effdate);
		/* Calculate the term left to run. It is needed in the generic*/
		/* processing routine to work out the initial unit discount factor.*/
		datcon3rec.intDate1.set(wsaaCurrentPayuptoDate);
		datcon3rec.intDate2.set(covrpf.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaTerm.set(datcon3rec.freqFactor);
		if (isNE(wsaaTermLeftRemain,0)) {
			wsaaTerm.add(1);
		}
		rnlallrec.termLeftToRun.set(wsaaTermLeftInteger);
		/* Calculate age next birthday at present.*/
		/* Age next birthday = (Age next birthday at contract commencement*/
		/*                      date) + (Term ran so far)*/
		compute(wsaaTermRan, 0).set(sub(covrpf.getPremCessTerm(),wsaaTermLeftInteger));
		compute(rnlallrec.anbAtCcd, 0).set(add(covrpf.getAnbAtCcd(),wsaaTermRan));
		rnlallrec.anbAtCcd.set(covrpf.getAnbAtCcd());
		/* Get the total premium from the temporary table for the coverage.*/
		//rnlallrec.covrInstprem.set(totalPremiumAmount);
		/* Call each non blank table entry. The table entry consists the*/
		/* required gereric subroutine.*/
		if (isNE(t5671rec.subprog01,SPACES)) {
			if (isEQ(wsaaFirstTime,"Y"))
			{
				callProgram(t5671rec.subprog01, rnlallrec.rnlallRec);
				if (isNE(rnlallrec.statuz,varcom.oK)) {
					syserrrec.params.set(rnlallrec.rnlallRec);
					xxxxFatalError();
				}
				wsaaFirstTime.set("N");
			}
		}
		if (isNE(t5671rec.subprog02,SPACES)) {
			callProgram(t5671rec.subprog02, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz,varcom.oK)) {
				syserrrec.params.set(rnlallrec.rnlallRec);
				xxxxFatalError();
			}
		}
		if (isNE(t5671rec.subprog03,SPACES)) {
			callProgram(t5671rec.subprog03, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz,varcom.oK)) {
				syserrrec.params.set(rnlallrec.rnlallRec);
				xxxxFatalError();
			}
		}
		if (isNE(t5671rec.subprog04,SPACES)) {
			callProgram(t5671rec.subprog04, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz,varcom.oK)) {
				syserrrec.params.set(rnlallrec.rnlallRec);
				xxxxFatalError();
			}
		}
	}

protected void readTh6052345()
	{
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(tablesInner.th605);
		itempf.setItemitem(wsaaCompany.toString());
		itempf = itemDao.getItempfRecord(itempf);

		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsaaCompany.toString().concat(tablesInner.th605).concat(wsaaCompany.toString()));
			xxxxFatalError();
		}else{
			th605rec.th605Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void updateCommLedger2350()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateCommLedger2351();
					initialCommDue2352();
				case initialCommEarned2353:
					initialCommEarned2353();
				case initialCommPaid2354:
					initialCommPaid2354();
				case servCommDue2355:
					servCommDue2355();
				case servCommEarned2356:
					servCommEarned2356();
				case rnwCommDue2357:
					rnwCommDue2357();
				case rnwCommEarned2358:
					rnwCommEarned2358();
				case initialOvrdCommDue2359:
					initialOvrdCommDue2359();
				case initialOvrdCommEarned2359:
					initialOvrdCommEarned2359();
				case initialOvrdCommPaid2359:
					initialOvrdCommPaid2359();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* This is to done by calling the subroutine LIFACMV for each type
	* of commission.
	* </pre>
	*/
protected void updateCommLedger2351()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.*/
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		/* MOVE LINSRNL-INSTFROM       TO LIFA-EFFDATE.         <LA3426>*/
		/*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.         <LA3426>*/
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
	}

protected void initialCommDue2352()
	{
		if (isEQ(wsaaBascpyDue,0)) {
			goTo(GotoLabel.initialCommEarned2353);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaBascpyDue);
		lifacmvrec.sacscode.set(t5645rec.sacscode08);
		lifacmvrec.sacstyp.set(t5645rec.sacstype08);
		lifacmvrec.glsign.set(t5645rec.sign08);
		lifacmvrec.glcode.set(t5645rec.glmap08);
		if (isEQ(th605rec.indic,"Y")) {
			wsaaStoreEffdate.set(lifacmvrec.effdate);
			lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		}
		lifacmvrec.contot.set(t5645rec.cnttot08);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
		wsaaRldgLife.set(agcmbchIO.getLife());
		wsaaRldgCoverage.set(agcmbchIO.getCoverage());
		wsaaRldgRider.set(agcmbchIO.getRider());
		wsaaPlan.set(agcmbchIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		if (isEQ(th605rec.indic,"Y")) {
			lifacmvrec.effdate.set(wsaaStoreEffdate);
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			b500CallZorcompy();
		}
	}

protected void initialCommEarned2353()
	{
		if (isEQ(wsaaBascpyErn,0)) {
			goTo(GotoLabel.initialCommPaid2354);
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[7]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[7]);
			lifacmvrec.glsign.set(wsaaT5645Sign[7]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[7]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[7]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.contot.set(t5645rec.cnttot09);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyErn);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void initialCommPaid2354()
	{
		if (isEQ(wsaaBascpyPay,0)) {
			goTo(GotoLabel.servCommDue2355);
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyPay);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[8]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[8]);
			lifacmvrec.glsign.set(wsaaT5645Sign[8]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[8]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[8]);
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyPay);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void servCommDue2355()
	{
		if (isEQ(wsaaSrvcpyDue,0)) {
			goTo(GotoLabel.servCommEarned2356);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaSrvcpyDue);
		lifacmvrec.sacscode.set(t5645rec.sacscode11);
		lifacmvrec.sacstyp.set(t5645rec.sacstype11);
		lifacmvrec.glsign.set(t5645rec.sign11);
		lifacmvrec.glcode.set(t5645rec.glmap11);
		lifacmvrec.contot.set(t5645rec.cnttot11);
		if (isEQ(th605rec.indic,"Y")) {
			wsaaStoreEffdate.set(lifacmvrec.effdate);
			lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		}
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
			zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			b500CallZorcompy();
		}
	}

protected void servCommEarned2356()
	{
		if (isEQ(wsaaSrvcpyErn,0)) {
			goTo(GotoLabel.rnwCommDue2357);
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[9]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[9]);
			lifacmvrec.glsign.set(wsaaT5645Sign[9]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[9]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[9]);
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			lifacmvrec.sacscode.set(t5645rec.sacscode12);
			lifacmvrec.sacstyp.set(t5645rec.sacstype12);
			lifacmvrec.glsign.set(t5645rec.sign12);
			lifacmvrec.glcode.set(t5645rec.glmap12);
			lifacmvrec.contot.set(t5645rec.cnttot12);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		/*MOVE AGCMBCH-AGNTNUM        TO LIFA-TRANREF.                 */
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void rnwCommDue2357()
	{
		if (isEQ(wsaaRnwcpyDue,0)) {
			goTo(GotoLabel.rnwCommEarned2358);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaRnwcpyDue);
		lifacmvrec.sacscode.set(t5645rec.sacscode13);
		lifacmvrec.sacstyp.set(t5645rec.sacstype13);
		lifacmvrec.glsign.set(t5645rec.sign13);
		lifacmvrec.glcode.set(t5645rec.glmap13);
		lifacmvrec.contot.set(t5645rec.cnttot13);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(wsaaChdrnum);
		if (isEQ(th605rec.indic,"Y")) {
			wsaaStoreEffdate.set(lifacmvrec.effdate);
			lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		}
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.function.set("PSTW");
		wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
		wsaaRldgLife.set(agcmbchIO.getLife());
		wsaaRldgCoverage.set(agcmbchIO.getCoverage());
		wsaaRldgRider.set(agcmbchIO.getRider());
		wsaaPlan.set(agcmbchIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			b500CallZorcompy();
		}
	}

protected void rnwCommEarned2358()
	{
		if (isEQ(wsaaRnwcpyErn,0)) {
			goTo(GotoLabel.initialOvrdCommDue2359);
		}
		/*GO TO 2359-EXIT.                                         */
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			/*       MOVE WSAA-RNWCPY-DUE        TO LIFA-ORIGAMT               */
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[10]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[10]);
			lifacmvrec.glsign.set(wsaaT5645Sign[10]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[10]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[10]);
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			/*       MOVE WSAA-RNWCPY-DUE        TO LIFA-ORIGAMT               */
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			lifacmvrec.sacscode.set(t5645rec.sacscode14);
			lifacmvrec.sacstyp.set(t5645rec.sacstype14);
			lifacmvrec.glsign.set(t5645rec.sign14);
			lifacmvrec.glcode.set(t5645rec.glmap14);
			lifacmvrec.contot.set(t5645rec.cnttot14);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		/*MOVE AGCMBCH-AGNTNUM        TO LIFA-TRANREF.                 */
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void initialOvrdCommDue2359()
	{
		if (isEQ(wsaaOvrdBascpyDue,0)) {
			goTo(GotoLabel.initialOvrdCommEarned2359);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaOvrdBascpyDue);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[15]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[15]);
		lifacmvrec.glsign.set(wsaaT5645Sign[15]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[15]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[15]);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(agcmbchIO.getCedagent());
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void initialOvrdCommEarned2359()
	{
		if (isEQ(wsaaOvrdBascpyErn,0)) {
			goTo(GotoLabel.initialOvrdCommPaid2359);
		}
		/*                                                         <013>*/
		wsaaJrnseq.add(1);
		/* MOVE WSAA-JRNSEQ               TO LIFA-JRNSEQ.          <013>*/
		/* MOVE WSAA-OVRD-BASCPY-ERN      TO LIFA-ORIGAMT.         <013>*/
		/* MOVE WSAA-T5645-SACSCODE (03)  TO LIFA-SACSCODE.        <013>*/
		/* MOVE WSAA-T5645-SACSTYPE (03)  TO LIFA-SACSTYP.         <013>*/
		/* MOVE WSAA-T5645-SIGN (03)      TO LIFA-GLSIGN.          <013>*/
		/* MOVE WSAA-T5645-GLMAP (03)     TO LIFA-GLCODE.          <013>*/
		/* MOVE WSAA-T5645-CNTTOT (03)    TO LIFA-CONTOT.          <013>*/
		/* MOVE AGCMBCH-AGNTNUM           TO LIFA-TRANREF.              */
		/*MOVE AGCMBCH-CEDAGENT          TO LIFA-RLDGACCT.             */
		/* MOVE AGCMBCH-CHDRNUM        TO WSAA-RLDG-CHDRNUM             */
		/* MOVE AGCMBCH-LIFE           TO WSAA-RLDG-LIFE.               */
		/* MOVE AGCMBCH-COVERAGE       TO WSAA-RLDG-COVERAGE.           */
		/* MOVE AGCMBCH-RIDER          TO WSAA-RLDG-RIDER.              */
		/* MOVE AGCMBCH-PLAN-SUFFIX    TO WSAA-PLAN.                    */
		/* MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.   <013>*/
		/* MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.           <013>*/
		/* MOVE CHDRLIF-CNTTYPE        TO LIFA-SUBSTITUTE-CODE(1). <013>*/
		/* MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <013>*/
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[14]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[14]);
			lifacmvrec.glsign.set(wsaaT5645Sign[14]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[14]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[14]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[3]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[3]);
			lifacmvrec.glsign.set(wsaaT5645Sign[3]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[3]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[3]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			lifacmvrec.rldgacct.set(agcmbchIO.getChdrnum());
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void initialOvrdCommPaid2359()
	{
		if (isEQ(wsaaOvrdBascpyPay,0)) {
			return ;
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			/*    MOVE WSAA-T5645-SACSCODE(13)   TO LIFA-SACSCODE      <021>*/
			/*    MOVE WSAA-T5645-SACSTYPE(13)   TO LIFA-SACSTYP       <021>*/
			/*    MOVE WSAA-T5645-SIGN(13)       TO LIFA-GLSIGN        <021>*/
			/*    MOVE WSAA-T5645-GLMAP(13)      TO LIFA-GLCODE        <021>*/
			/*    MOVE WSAA-T5645-CNTTOT(13)     TO LIFA-CONTOT        <021>*/
			/*    MOVE WSAA-T5645-SACSCODE(14)   TO LIFA-SACSCODE   <A06168>*/
			/*    MOVE WSAA-T5645-SACSTYPE(14)   TO LIFA-SACSTYP    <A06168>*/
			/*    MOVE WSAA-T5645-SIGN(14)       TO LIFA-GLSIGN     <A06168>*/
			/*    MOVE WSAA-T5645-GLMAP(14)      TO LIFA-GLCODE     <A06168>*/
			/*    MOVE WSAA-T5645-CNTTOT(14)     TO LIFA-CONTOT     <A06168>*/
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[13]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[13]);
			lifacmvrec.glsign.set(wsaaT5645Sign[13]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[13]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[13]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		else {
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			/*    MOVE WSAA-T5645-SACSCODE(4)    TO LIFA-SACSCODE      <013>*/
			/*    MOVE WSAA-T5645-SACSTYPE(4)    TO LIFA-SACSTYP       <013>*/
			/*    MOVE WSAA-T5645-SIGN(4)        TO LIFA-GLSIGN        <013>*/
			/*    MOVE WSAA-T5645-GLMAP(4)       TO LIFA-GLCODE        <013>*/
			/*    MOVE WSAA-T5645-CNTTOT(4)      TO LIFA-CONTOT        <013>*/
			/*    MOVE WSAA-T5645-SACSCODE(3)    TO LIFA-SACSCODE   <A06168>*/
			/*    MOVE WSAA-T5645-SACSTYPE(3)    TO LIFA-SACSTYP    <A06168>*/
			/*    MOVE WSAA-T5645-SIGN(3)        TO LIFA-GLSIGN     <A06168>*/
			/*    MOVE WSAA-T5645-GLMAP(3)       TO LIFA-GLCODE     <A06168>*/
			/*    MOVE WSAA-T5645-CNTTOT(3)      TO LIFA-CONTOT     <A06168>*/
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[4]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[4]);
			lifacmvrec.glsign.set(wsaaT5645Sign[4]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[4]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[4]);
			lifacmvrec.rldgacct.set(agcmbchIO.getCedagent());
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void getCrtable2360()
	{
		/*GET-CRTABLE*/
		/**    MOVE SPACE                  TO COVR-DATA-KEY.                */
		/**    MOVE AGCMBCH-CHDRCOY        TO COVR-CHDRCOY.         <LA2108>*/
		/**    MOVE AGCMBCH-CHDRNUM        TO COVR-CHDRNUM.         <LA2108>*/
		/**    MOVE AGCMBCH-LIFE           TO COVR-LIFE.            <LA2108>*/
		/**    MOVE AGCMBCH-COVERAGE       TO COVR-COVERAGE.        <LA2108>*/
		/**    MOVE AGCMBCH-RIDER          TO COVR-RIDER.           <LA2108>*/
		/**    MOVE 0                      TO COVR-PLAN-SUFFIX.             */
		/**    MOVE BEGN                   TO COVR-FUNCTION.                */
		/**    MOVE COVRREC                TO COVR-FORMAT.                  */
		/**    CALL 'COVRIO' USING COVR-PARAMS.                             */
		/**    IF (COVR-STATUZ NOT = O-K) AND                               */
		/**       (COVR-STATUZ NOT = ENDP)                                  */
		/**        MOVE COVR-PARAMS        TO SYSR-PARAMS                   */
		/**        PERFORM XXXX-FATAL-ERROR.                                */
		/**    IF (COVR-STATUZ = ENDP)                                      */
		/**        MOVE SPACE              TO COVR-CRTABLE                  */
		/**        GO TO 2369-EXIT.                                         */
		/**    IF (COVR-CHDRNUM NOT = AGCMBCH-CHDRNUM) OR           <LA2108>*/
		/**       (COVR-CHDRCOY NOT = AGCMBCH-CHDRCOY) OR           <LA2108>*/
		/**       (COVR-COVERAGE NOT = AGCMBCH-COVERAGE) OR         <LA2108>*/
		/**       (COVR-RIDER NOT = AGCMBCH-RIDER) OR               <LA2108>*/
		/**       (COVR-LIFE NOT = AGCMBCH-LIFE)                    <LA2108>*/
		/**        MOVE SPACE              TO COVR-CRTABLE                  */
		/**        GO TO 2369-EXIT.                                         */
		/*EXIT*/
	}

protected void updateCovrStatus2370()
	{
		
	   List<Covrpf> covrpflist = covrpfDAO.getCovrByComAndNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
     
	   if(covrpflist!=null&&covrpflist.size()>0){
    	 
    	   for(int i=0; i< covrpflist.size(); i++){
    	   covrpf = covrpflist.get(i);
    	   readT568711100();
    	   if (isEQ(t5687rec.singlePremInd,"Y")) {
   			continue;
   		   }
    	   /* Validate the coverage status against T5679.                     */
   		  wsaaValidStatus.set("N");
   		  
   		  if ((isNE(covrpf.getCoverage(),SPACES)) && ((isEQ(covrpf.getRider(),SPACES)) || (isEQ(covrpf.getRider(),"00")))) {
   		     	for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
   				validateCovrStatus2110();
   			   }
   		    }
   		/* Validate the new rider status against T5679.                    */
  		if ((isNE(covrpf.getCoverage(),SPACES)) && ((isNE(covrpf.getRider(),SPACES)) && (isNE(covrpf.getRider(),"00")))) {
  			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
  				validateRiderStatus2120();
  			}
  		}
  		if (isEQ(wsaaValidStatus,"N")) {
			continue;
		}
  		
		wsaaUpdateFlag = "N";
		if (isNE(t5687rec.rtrnwfreq,ZERO) && (isNE(covrpf.getPremCessDate(),covrpf.getRerateDate()))) {
				continue;
			}
		
		if (isGTE(chdrpf.getPtdate(),covrpf.getPremCessDate())) {
			if ((isEQ(covrpf.getRider(),"00"))|| (isEQ(covrpf.getRider(), "  "))) {
				if (isNE(t5679rec.setCovPremStat,SPACES)) {
					covrpf.setPstatcode(t5679rec.setCovPremStat.toString());
					wsaaUpdateFlag = "Y";
				}
			}
			else {
				if (isNE(t5679rec.setRidPremStat,SPACES)) {
					covrpf.setPstatcode(t5679rec.setRidPremStat.toString());
					wsaaUpdateFlag = "Y";
				}
			}
		}
		
		if (isNE(wsaaUpdateFlag,"Y")) {
			continue;
		}
		 if(!covrpfDAO.updateCovrPstatcode(covrpf)){
			 syserrrec.params.set(covrpf.getChdrnum());
			 xxxxFatalError();
		 }
		
		}
    	   
        }
       
	}




protected void calculateNextDate2400()
	{
		calculateNextDate2410();
	}

protected void calculateNextDate2410()
	{
		/* Calculate the next pay-to date.*/
		datcon2rec.function.set(SPACES);
		datcon2rec.intDate1.set(wsaaCurrentPayuptoDate);
		datcon2rec.frequency.set(chdrpf.getBillfreq());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			xxxxFatalError();
		}
		/*MOVE DTC2-INT-DATE-2        TO WSAA-CURRENT-PAYUPTO-DATE.    */
		wsaaPuDate.set(datcon2rec.intDate2);
		/* Check to see if the day is at the end of the month.             */
		wsaaEomDate.set(wsaaPuDate);
		wsaaEomDay.set(wsaaLinsDay);
		wsaaEndOfMonth = "N";
		checkEndOfMonth9000();
		/* If the day of the DATCON2 returned date is different            */
		/*    to the oringinal LINS billing renewal day and the            */
		/*    original LINS date was not at the end of the month           */
		/*       Set the day to the original billing day.                  */
		if (isNE(wsaaPuDay,wsaaLinsDay)
		&& isNE(wsaaEndOfMonth,"Y")) {
			wsaaPuDay.set(wsaaLinsDay);
		}
		wsaaCurrentPayuptoDate.set(wsaaPuDate);
	}

protected void generalHousekeeping3000()
	{
		/* Write PTRN record.*/
		/*MOVE SPACE                  TO PTRN-PARAMS.                  */
		ptrnpf.setChdrcoy(wsaaCompany.toString());
		ptrnpf.setChdrnum(wsaaChdrnum.toString());
		/*    MOVE CHDRLIF-TRANNO         TO PTRN-TRANNO.                  */
		ptrnpf.setTranno(wsaaTranno.toInt());
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		/*MOVE DTC1-INT-DATE          TO PTRN-PTRNEFF.                 */
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		ptrnpf.setUserT(wsaaUser.toInt());
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(wsaaCompany.toString());
		ptrnpf.setBatcbrn(atrtBranch.toString());
		ptrnpf.setBatcactyr(atrtAcctYear.toInt());
		ptrnpf.setBatctrcde(atrtTransCode.toString());
		ptrnpf.setBatcactmn(atrtAcctMonth.toInt());
		ptrnpf.setBatcbatch(atrtBatch.toString());
        ptrnpf.setValidflag(" ");
		List<Ptrnpf> ptrnList = new ArrayList();
		ptrnList.add(ptrnpf);
		boolean result = ptrnpfDAO.insertPtrnPF(ptrnList);

		if (!result) {
			syserrrec.params.set(ptrnpf.getChdrcoy().concat(ptrnpf.getChdrnum()));
			xxxxFatalError();
		}
		/* Update batch header.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(atrtPrefix);
		batcuprec.batccoy.set(wsaaCompany);
		batcuprec.batcbrn.set(atrtBranch);
		batcuprec.batcactyr.set(atrtAcctYear);
		batcuprec.batctrcde.set(atrtTransCode);
		batcuprec.batcactmn.set(atrtAcctMonth);
		batcuprec.batcbatch.set(atrtBatch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/* DIARY PROCESSING....                                           */
		/* Call the section for Diary Maintenance.                         */
		dryProcessing12000();
		/* RELEASE SOFTLOCK....*/
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(wsaaChdrnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void updateChdrPayr4000()
	{
		
		if (isEQ(wsaaPremDiff,0)) {
			return ;
		}
		chdrpf = null;
		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsaaCompany.toString(), wsaaChdrnum.toString());

		if (chdrpf == null) {
			syserrrec.params.set(wsaaCompany.toString().concat(wsaaChdrnum.toString()));
			syserrrec.statuz.set("MRNF");
			xxxxFatalError();
		}
		chdrpf.setValidflag("2".charAt(0));
		/* This CALL to DATCON2 is not necessary as the VFLAG 2 record     */
		/* should have a CURRTO equal to the BILL DATE and not 1 day       */
		/* before.                                                         */
		/* MOVE -1                     TO DTC2-FREQ-FACTOR.        <027>*/
		/* MOVE 'DY'                   TO DTC2-FREQUENCY.          <027>*/
		/* MOVE WSAA-BILL-DATE         TO DTC2-INT-DATE-1.         <027>*/
		/*                                                         <027>*/
		/* CALL 'DATCON2'              USING DTC2-DATCON2-REC.     <027>*/
		/*                                                         <027>*/
		/* IF DTC2-STATUZ           NOT = O-K                      <036>*/
		/*    MOVE DTC2-DATCON2-REC    TO SYSR-STATUZ              <036>*/
		/*    PERFORM XXXX-FATAL-ERROR.                            <036>*/
		/*                                                         <036>*/
		/* MOVE DTC2-INT-DATE-2        TO CHDRLIF-CURRTO.          <027>*/
		chdrpf.setCurrto(wsaaBillDate.toInt());
		updtChdrpf = new Chdrpf();
		updtChdrpf = chdrpf;
		List<Chdrpf> chdrBulkOpList = new ArrayList<Chdrpf>();
		chdrBulkOpList.add(updtChdrpf);
		chdrpfDAO.updateInvalidChdrRecord(chdrBulkOpList);
	
		/* Write a new CHDRLIF record with new details.                    */
		chdrpf.setValidflag("1".charAt(0));
		chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpf.setCurrfrom(wsaaBillDate.toInt());
		chdrpf.setTranno(wsaaTranno.toInt());
		setPrecision(chdrpf.getSinstamt01(), 2);
		chdrpf.setSinstamt01(chdrpf.getSinstamt01().add(new BigDecimal(wsaaPremDiff.toString())));
		setPrecision(chdrpf.getSinstamt06(), 2);
		chdrpf.setSinstamt06(chdrpf.getSinstamt06().add(new BigDecimal(wsaaPremDiff.toString())));
		/* Check if all coverages or riders have expired in order          */
		/* to update the chdr status codes.                                */
		
		List<Covrpf> covrpflist = covrpfDAO.getCovrByComAndNumAllFlagRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		
		wsaaCoverExit.set("N");
		wsaaAllCovExp = "Y";
		wsaaAllRidExp = "Y";
		if(covrpflist!=null&&covrpflist.size()>0){
			for(int i=0; i<covrpflist.size(); i++){
				covrpf = covrpflist.get(i);
				cessationProcess5700();
			}
		}
	
		if (isEQ(wsaaAllCovExp,"Y")
		&& isEQ(wsaaAllRidExp,"Y")
		&& isNE(t5679rec.setCnRiskStat,SPACES)) {
			chdrpf.setStatcode(t5679rec.setCnRiskStat.toString());
		}
		if (isEQ(wsaaAllCovExp,"Y")
		&& isNE(t5679rec.setCnRiskStat,SPACES)) {
			chdrpf.setPstcde(t5679rec.setCovPremStat.toString());
		}
		List<Chdrpf> instChdrpf = new ArrayList();
		instChdrpf.add(chdrpf);
		chdrpfDAO.insertChdrAmt(instChdrpf);
		
	
		payrpf =  new Payrpf();
		payrpf.setChdrcoy(wsaaCompany.toString());
		payrpf.setChdrnum(wsaaChdrnum.toString());
		payrpf.setValidflag("1");
		payrpf.setPayrseqno(1);
		
		payrpf.setValidflag("2");
		payrpfDAO.updateValidForPayr(payrpf);

		payrpf = new Payrpf();
		payrpf = payrpfDAO.getpayrRecord(wsaaCompany.toString(), wsaaChdrnum.toString(), 1);
		payrpf.setChdrcoy(wsaaCompany.toString());
		payrpf.setChdrnum(wsaaChdrnum.toString());
		payrpf.setPayrseqno(1);
		payrpf.setPstatcode(chdrpf.getPstcde());
		payrpf.setValidflag("1");
		payrpf.setEffdate(wsaaRerateDate.toInt());
		payrpf.setTranno(wsaaTranno.toInt());
		payrpf.setSinstamt01(chdrpf.getSinstamt01());
		payrpf.setSinstamt06(chdrpf.getSinstamt06());
		payrpf.setNextdate(chdrpf.getBillcd());
		
		payrInstlist = new ArrayList();
		payrInstlist.add(payrpf);
		payrpfDAO.insertPayrpfList(payrInstlist);

		wsaaPremDiff.set(0);
	}

protected void checkAgentTerminate4100()
	{
	
		/* Retrieve Today's date.                                          */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			xxxxFatalError();
		}
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrpf.getAgntcoy());
		aglfIO.setAgntnum(chdrpf.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		aglfIO.setFormat(formatsInner.aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			xxxxFatalError();
		}
		wsaaAgtTerminatedFlag = "N";
		if (isLT(aglfIO.getDtetrm(),datcon1rec.intDate)
		|| isLT(aglfIO.getDteexp(),datcon1rec.intDate)
		|| isGT(aglfIO.getDteapp(),datcon1rec.intDate)) {
			wsaaAgtTerminatedFlag = "Y";
		}
	}

protected void readT5645Page34200()
	{
		
		/* If more values are added onto the 3rd screen, then a more       */
		/* method should be used. (such as looping). But for now, this     */
		/* will suffice.                                                   */
	    itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(tablesInner.t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("02");
		itempf = itemDao.getItempfRecordBySeq(itempf);

		if (itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}else{
			t5645rec.t5645Rec.set(SPACES);
		}
		
		if (isEQ(t5645rec.sacscode[2],SPACES)
		&& isEQ(t5645rec.sacstype[2],SPACES)
		&& isEQ(t5645rec.glmap[2],SPACES)) {
			return ;
		}
		wsaaT5645Cnttot32.set(t5645rec.cnttot[2]);
		wsaaT5645Glmap32.set(t5645rec.glmap[2]);
		wsaaT5645Sacscode32.set(t5645rec.sacscode[2]);
		wsaaT5645Sacstype32.set(t5645rec.sacstype[2]);
	}

protected void cessationProcess5700()
	{
		
		/* Read the Coverage Rider file.                                   */
	
		if (isEQ(covrpf.getRider(),"00")
		|| isEQ(covrpf.getRider(), "  ")) {
			coverageStatus7000();
		}
		else {
			riderStatus6000();
		}

	}

protected void riderStatus6000()
	{
		if (isEQ(wsaaAllRidExp,"Y")) {
			if (isGT(covrpf.getPremCessDate(),wsaaBillDate)) {
				wsaaAllRidExp = "N";
			}
		}
		/*EXIT*/
	}

protected void coverageStatus7000()
	{
		if (isEQ(wsaaAllCovExp,"Y")) {
			if (isGT(covrpf.getPremCessDate(),wsaaBillDate)) {
				wsaaAllCovExp = "N";
			}
		}
		/*EXIT*/
	}

protected void initialiseCovrPrem8000()
	{
		/*PARA*/
		wsaaCovrInstprem[wsaaL.toInt()][wsaaC.toInt()].set(0);
		wsaaC.add(1);
		if (isGT(wsaaC,99)) {
			wsaaL.add(1);
			wsaaC.set(1);
		}
		/*EXIT*/
	}

protected void findCovrTotPrem8100()
	{
	    List<Covrpf> covrlist = covrpfDAO.getCovrByComAndNumAllFlagRecord(linsrnlIO.getChdrcoy().toString(), linsrnlIO.getChdrnum().toString());
	    if(covrlist == null || covrlist.size() == 0)
	    	return ;
	    for(int i=0; i<covrlist.size();i++){
	    	covrpf = covrlist.get(i);
	    	if ((isGTE(linsrnlIO.getInstfrom(),covrpf.getCurrto()))|| (isLT(linsrnlIO.getInstfrom(),covrpf.getCurrfrom()))) {
	    				continue;
	    	}
	    	if (isEQ(covrpf.getRider(),"00")|| isEQ(covrpf.getRider(), "  ")) {
	    				wsaaCoverageNum.set(covrpf.getCoverage());
	    				wsaaLifeNum.set(covrpf.getLife());
	    				wsaaCovrInstprem[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()].add(covrpf.getInstprem().doubleValue());
	    	}
	    }
	    

	}



protected void calculateAnb8200()
	{

		/* Call datcon3 to attain the age next birthday at billing date.   */
		/*    MOVE SPACES                 TO DTC3-FUNCTION.           <025>*/
		/*    MOVE LIFELNB-CLTDOB         TO DTC3-INT-DATE-1.         <025>*/
		/*    MOVE WSAA-BILL-DATE         TO DTC3-INT-DATE-2.         <025>*/
		/*    MOVE FREQ-YRLY              TO DTC3-FREQUENCY.          <025>*/
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.                  <025>*/
		/*    IF DTC3-STATUZ              NOT = O-K                   <025>*/
		/*        MOVE DTC3-STATUZ        TO SYSR-STATUZ              <025>*/
		/*        PERFORM XXXX-FATAL-ERROR.                           <025>*/
		/* Round up the age.                                               */
		/*    IF LIFERNL-JLIFE            = '01'                      <025>*/
		/*       ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-JANB         <025>*/
		/*    ELSE                                                    <025>*/
		/*       ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB.         <025>*/
		/* Routine to calculate Age next/nearest/last birthday             */
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(atmodrec.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(lifepf.getCltdob());
		agecalcrec.intDate2.set(wsaaBillDate);
		agecalcrec.company.set(wsaaFsuCoy);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			xxxxFatalError();
		}
		if (isEQ(lifepf.getJlife(),"01")) {
			wsaaJanb.set(agecalcrec.agerating);
		}
		else {
			wsaaAnb.set(agecalcrec.agerating);
		}
	}

protected void checkEndOfMonth9000()
	{
		/*PARA*/
		wsaaEndOfMonth = "N";
		wsaaLeapYear = "N";
		checkLeapYear10000();
		if (isEQ(wsaaLeapYear,"Y")) {
			wsaaDaysInMonth.set(wsaaDaysInMonthLeap);
		}
		else {
			wsaaDaysInMonth.set(wsaaDaysInMonthNorm);
		}
		if (isGTE(wsaaEomDay,wsaaMonthDays[wsaaEomMonth.toInt()])) {
			wsaaEndOfMonth = "Y";
		}
		/*EXIT*/
	}

protected void checkLeapYear10000()
	{
		/*CHK-LEAP-YEAR*/
		/* If the year is the begining of the century                      */
		/*    Divide the year by 400 and if result is an integer value,    */
		/*    this indicates that this is a Century leap year.             */
		if (isEQ(wsaaCenturyYear,ZERO)) {
			compute(wsaaResult, 0).set(div(wsaaEomYear,400));
			wsaaResult.multiply(400);
			if (isEQ(wsaaResult,wsaaEomYear)) {
				wsaaLeapYear = "Y";
			}
			return ;
		}
		/* Divide year by 4 and if result is an integer value, this        */
		/* indicates a yeap year.                                          */
		compute(wsaaResult, 0).set(div(wsaaEomYear,4));
		wsaaResult.multiply(4);
		if (isEQ(wsaaResult,wsaaEomYear)) {
			wsaaLeapYear = "Y";
		}
		/*EXIT*/
	}

protected void getAnny11000()
	{
	    annypf = annypfDAO.getAnnyRecordByAnnyKey(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix());

		if (annypf != null) {
			premiumrec.freqann.set(annypf.getFreqann());
			premiumrec.advance.set(annypf.getAdvance());
			premiumrec.arrears.set(annypf.getArrears());
			premiumrec.guarperd.set(annypf.getGuarperd());
			premiumrec.intanny.set(annypf.getIntanny());
			premiumrec.capcont.set(annypf.getCapcont());
			premiumrec.withprop.set(annypf.getWithprop());
			premiumrec.withoprop.set(annypf.getWithoprop());
			premiumrec.ppind.set(annypf.getPpind());
			premiumrec.nomlife.set(annypf.getNomlife());
			premiumrec.dthpercn.set(annypf.getDthpercn());
			premiumrec.dthperco.set(annypf.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

protected void a100ReadZrap()
	{
		/*A100-START*/
		zrapIO.setAgntcoy(wsaaCompany);
		zrapIO.setAgntnum(wsaaAgentKept);
		zrapIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zrapIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zrapIO.setFitKeysSearch("AGNTNUM");
		zrapIO.setStatuz(varcom.oK);
		while ( !(isEQ(zrapIO.getStatuz(),varcom.endp))) {
			a200SearchZrap();
		}

		comlinkrec.zcomcode.set(comlinkrec.method);
		comlinkrec.payamnt.set(wsaaCompayKept);
		/*A100-EXIT*/
	}

protected void a200SearchZrap()
	{
			a200Start();
		}

protected void a200Start()
	{
		SmartFileCode.execute(appVars, zrapIO);
		if (isNE(zrapIO.getStatuz(),varcom.oK)
		&& isNE(zrapIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zrapIO.getParams());
			syserrrec.statuz.set(zrapIO.getStatuz());
			comlinkrec.payamnt.set(ZERO);
			xxxxFatalError();
		}
		if (isNE(zrapIO.getAgntnum(),wsaaAgentKept)
		|| isEQ(zrapIO.getStatuz(),varcom.endp)) {
			comlinkrec.payamnt.set(ZERO);
			zrapIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(zrapIO.getStatuz(),varcom.oK)) {
			comlinkrec.payamnt.set(ZERO);
			zrapIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrapIO.getReportag(),agcmbchIO.getAgntnum())) {
			comlinkrec.zorcode.set(zrapIO.getZrorcode());
			comlinkrec.efdate.set(zrapIO.getEffdate());
			zrapIO.setStatuz(varcom.endp);
			return ;
		}
		zrapIO.setFunction(varcom.nextr);
	}

protected void a300ReadT6659()
	{
			a301Read();
			a350CheckT6659Details();
		}

protected void a301Read()
	{
		itdmIO.setItemcoy(wsaaCompany);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(wsaaUnitEffdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(t6647rec.unitStatMethod,itdmIO.getItemitem())
		|| isNE(wsaaCompany,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6659)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g029);
			xxxxFatalError();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void a350CheckT6659Details()
	{
		/* Check the details and call the generic subroutine from T6659.   */
		if (isEQ(t6659rec.subprog,SPACES)
		|| isNE(t6659rec.annOrPayInd,"P")
		|| isNE(t6659rec.osUtrnInd,"Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(wsaaCompany);
		annprocrec.chdrnum.set(wsaaChdrnum);
		annprocrec.effdate.set(wsaaUnitEffdate);
		annprocrec.batchkey.set(atrtBatchKey);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			xxxxFatalError();
		}
	}

protected void readT568711100()
	{
		
		/* Access the table to establish if this is a single premium       */
		/* compoenent.                                                     */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrpf.getCrtable());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void dryProcessing12000()
	{
		start12010();
	}

protected void start12010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrpf.getCnttype());
		wsaaT7508Batctrcde.set(atrtTransCode);
		readT750813000();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT750813000();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(atrtCompany);
		drypDryprcRecInner.drypBranch.set(atrtBranch);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(atrtBatchKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuCoy);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
		drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT750813000()
	{
		start13010();
	}

protected void start13010()
	{
	    itempf = new Itempf();
	    
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}             

protected void b100InitializeArrays()
	{
		/*B110-INIT*/
		for (wsaaAgcmIx.set(1); !(isGT(wsaaAgcmIx,wsaaAgcmIxSize)); wsaaAgcmIx.add(1)){
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(SPACES);
			for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy,wsaaAgcmIySize)); wsaaAgcmIy.add(1)){
				wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
			}
		}
		/*B190-EXIT*/
	}

protected void b200PremiumHistory()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b210Init();
				case b220Call:
					b220Call();
					b230Summary();
				case b280Next:
					b280Next();
				case b290Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b210Init()
	{
		/* Read the AGCM for differentiating the First & Renewal Year      */
		/* Premium                                                         */
		agcmseqIO.setDataArea(SPACES);
		agcmseqIO.setChdrcoy(covrpf.getChdrcoy());
		agcmseqIO.setChdrnum(covrpf.getChdrnum());
		agcmseqIO.setLife(covrpf.getLife());
		agcmseqIO.setCoverage(covrpf.getCoverage());
		agcmseqIO.setRider(covrpf.getRider());
		agcmseqIO.setPlanSuffix(covrpf.getPlanSuffix());
		agcmseqIO.setSeqno(ZERO);
		agcmseqIO.setFormat(formatsInner.agcmseqrec);
		agcmseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmseqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
	}

protected void b220Call()
	{
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(agcmseqIO.getStatuz(),varcom.endp)
		|| isNE(agcmseqIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(agcmseqIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(agcmseqIO.getLife(),covrpf.getLife())
		|| isNE(agcmseqIO.getCoverage(),covrpf.getCoverage())
		|| isNE(agcmseqIO.getRider(),covrpf.getRider())) {
			agcmseqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.b290Exit);
		}
		/* Skip those irrelevant records                                   */
		if (isNE(agcmseqIO.getOvrdcat(),"B")
		|| isEQ(agcmseqIO.getPtdate(),ZERO)
		|| isEQ(agcmseqIO.getEfdate(),ZERO)
		|| isEQ(agcmseqIO.getEfdate(),varcom.vrcmMaxDate)) {
			goTo(GotoLabel.b280Next);
		}
	}

protected void b230Summary()
	{
		wsaaAgcmFound.set("N");
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy,wsaaAgcmIySize)
		|| isEQ(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()],ZERO)
		|| agcmFound.isTrue()); wsaaAgcmIy.add(1)){
			if (isEQ(agcmseqIO.getSeqno(),wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()])) {
				wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].add(agcmseqIO.getAnnprem());
				agcmFound.setTrue();
			}
		}
		if (!agcmFound.isTrue()) {
			if (isGT(wsaaAgcmIy,wsaaAgcmIySize)) {
				syserrrec.statuz.set(e103);
				xxxxFatalError();
			}
			wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getSeqno());
			wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getEfdate());
			wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getAnnprem());
		}
	}

protected void b280Next()
	{
		agcmseqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.b220Call);
	}

protected void b300WriteArrays()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b310Init();
				case b320Loop:
					b320Loop();
					b330Writ();
				case b380Next:
					b380Next();
				case b390Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b310Init()
	{
		wsaaAgcmIy.set(1);
	}

protected void b320Loop()
	{
		if (isEQ(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()],ZERO)) {
			goTo(GotoLabel.b390Exit);
		}
		else {
			if (isGT(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()],linsrnlIO.getInstfrom())) {
				goTo(GotoLabel.b380Next);
			}
		}
	}

protected void b330Writ()
	{
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]);
		datcon3rec.intDate2.set(linsrnlIO.getInstfrom());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			xxxxFatalError();
		}
		if (isGTE(datcon3rec.freqFactor,1)) {
			renPrem.setTrue();
		}
		else {
			firstPrem.setTrue();
		}
		if (isNE(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()],ZERO)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(covrpf.getChdrcoy());
			zptnIO.setChdrnum(covrpf.getChdrnum());
			zptnIO.setLife(covrpf.getLife());
			zptnIO.setCoverage(covrpf.getCoverage());
			zptnIO.setRider(covrpf.getRider());
			zptnIO.setTranno(wsaaTranno);
			setPrecision(zptnIO.getOrigamt(), 3);
			zptnIO.setOrigamt(div(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()],wsaaBillfq9), true);
			zrdecplrec.amountIn.set(zptnIO.getOrigamt());
			c000CallRounding();
			zptnIO.setOrigamt(zrdecplrec.amountOut);
			zptnIO.setTransCode(atrtTransCode);
			zptnIO.setEffdate(linsrnlIO.getInstfrom());
			zptnIO.setInstfrom(linsrnlIO.getInstfrom());
			zptnIO.setBillcd(linsrnlIO.getBillcd());
			zptnIO.setInstto(linsrnlIO.getInstto());
			zptnIO.setTrandate(datcon1rec.intDate);
			zptnIO.setZprflg(wsaaPremType);
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void b380Next()
	{
		wsaaAgcmIy.add(1);
		goTo(GotoLabel.b320Loop);
	}

protected void b400LocatePremium()
	{
		/*B410-INIT*/
		wsaaAgcmFound.set("N");
		wsaaAgcmPremium.set(ZERO);
		for (wsaaAgcmIa.set(1); !(isGT(wsaaAgcmIa,wsaaAgcmIxSize)
		|| agcmFound.isTrue()); wsaaAgcmIa.add(1)){
			if (isEQ(agcmbchIO.getChdrcoy(),wsaaAgcmChdrcoy[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getChdrnum(),wsaaAgcmChdrnum[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getLife(),wsaaAgcmLife[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getCoverage(),wsaaAgcmCoverage[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getRider(),wsaaAgcmRider[wsaaAgcmIa.toInt()])) {
				for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb,wsaaAgcmIySize)
				|| agcmFound.isTrue()); wsaaAgcmIb.add(1)){
					if (isEQ(agcmbchIO.getSeqno(),wsaaAgcmSeqno[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()])) {
						agcmFound.setTrue();
						wsaaAgcmPremium.set(wsaaAgcmAnnprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
					}
				}
			}
		}
		/*B490-EXIT*/
	}

protected void b500CallZorcompy()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(chdrpf.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrpf.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(atmodrec.batchKey);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			xxxxFatalError();
		}
	}

protected void b600CalcTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b600Start();
				case b600CalcComponentTax:
					b600CalcComponentTax();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b600Start()
	{
		wsaaTax.set(0);
		if (isEQ(wsaaWaiveAmt, 0)) {
			goTo(GotoLabel.b600CalcComponentTax);
		}
		/*  waiver of premium has been activated. We need to               */
		/*  look for the wop type component by doing an existence check    */
		/*  on TR517.                                                      */
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		covrmjaIO.setChdrnum(chdrpf.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		covrmjaIO.setStatuz(varcom.oK);
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			b610GetWopComponent();
		}

	}

protected void b600CalcComponentTax()
	{
		/*  Calculate the tax on all components.                           */
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		covrmjaIO.setChdrnum(chdrpf.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		covrmjaIO.setStatuz(varcom.oK);
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			b620CalcCompTax();
		}

		if (isNE(wsaaCntfee, ZERO)) {
			/*       we do not need to calculate the tax on contract fee if    */
			/*       the WOP waives the contract fee.                          */
			if (isNE(wsaaWaiveAmt, ZERO)
			&& isEQ(wsaaWaiveCntf, "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				b630CalcContTax();
			}
		}
	}

protected void b610GetWopComponent()
	{
		b610Start();
		b610NextRecord();
	}

protected void b610Start()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		|| isNE(covrmjaIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(covrmjaIO.getChdrcoy(), chdrpf.getChdrcoy())) {
			covrmjaIO.setChdrnum(chdrpf.getChdrnum());
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			xxxxFatalError();
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		wsaaItdmkey.set(itdmIO.getDataKey());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setDataKey(wsaaItdmkey);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		/*  read the next component if the component is not found          */
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			return ;
		}
		/*  load the wop details to working storage .                      */
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		wsaaWaiveItself.set(tr517rec.zrwvflg01);
		wsaaWaiveCntf.set(tr517rec.zrwvflg03);
		wsaaWopComponent.set(covrmjaIO.getCrtable());
		wsaaWaiveCrtables.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 50)); wsaaSub.add(1)){
			wsaaSub1.add(1);
			wsaaWaiveCrtable[wsaaSub1.toInt()].set(tr517rec.ctable[wsaaSub.toInt()]);
		}
		wsaaTr517Contitem.set(tr517rec.contitem);
		/*  load continuation items is any.                                */
		while ( !(isEQ(tr517rec.contitem, SPACES))) {
			itdmIO.setItemcoy(atmodrec.company);
			itdmIO.setItemtabl(tablesInner.tr517);
			itdmIO.setItemitem(wsaaTr517Contitem);
			itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			itdmIO.setFormat(formatsInner.itdmrec);
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.oK);
			wsaaItdmkey.set(itdmIO.getDataKey());
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			|| isNE(itdmIO.getItemitem(), wsaaItdmkey.itdmItemitem)
			|| isNE(itdmIO.getItemtabl(), wsaaItdmkey.itdmItemtabl)) {
				itdmIO.setDataKey(wsaaItdmkey);
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				xxxxFatalError();
			}
			for (wsaaSub.set(1); !(isGT(wsaaSub, 50)); wsaaSub.add(1)){
				wsaaSub1.add(1);
				wsaaWaiveCrtable[wsaaSub1.toInt()].set(tr517rec.ctable[wsaaSub.toInt()]);
			}
			wsaaTr517Contitem.set(tr517rec.contitem);
		}

		/*  end of the loading process. We set endp to status to exit      */
		covrmjaIO.setStatuz(varcom.endp);
	}

protected void b610NextRecord()
	{
		/* prepare to read the next covr record.                           */
		covrmjaIO.setFunction(varcom.nextr);
		/*B610-EXIT*/
	}

protected void b620CalcCompTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b620Start();
				case b620ReadTr52e:
					b620ReadTr52e();
				case b620ReadNextRecord:
					b620ReadNextRecord();
				case b620Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b620Start()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)
		|| isNE(covrmjaIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(covrmjaIO.getChdrcoy(), chdrpf.getChdrcoy())) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.b620Exit);
		}
		if (isLTE(covrmjaIO.getCurrfrom(), wsaaTaxEffdate)
		&& isNE(covrmjaIO.getInstprem(), ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			/* not a valid component hence we skip.                            */
			goTo(GotoLabel.b620ReadNextRecord);
		}
		b660ReadT5679();
		b670StatusCheck();
		/* Exclude components that do not qualify.                         */
		if (isEQ(wsaaValidStatus, "N")) {
			goTo(GotoLabel.b620ReadNextRecord);
		}
		if (isNE(wsaaWaiveAmt, ZERO)) {
			if (isEQ(covrmjaIO.getCrtable(), wsaaWopComponent)) {
				if (isEQ(wsaaWaiveItself, "Y")) {
					goTo(GotoLabel.b620ReadNextRecord);
				}
				goTo(GotoLabel.b620ReadTr52e);
			}
			/*       Check if the covr/rider is ont of the component           */
			/*       being waived.                                             */
			wsaaCovrValid = "Y";
			for (wsaaSub.set(1); !(isGT(wsaaSub, 200)
			|| isEQ(wsaaWaiveCrtable[wsaaSub.toInt()], SPACES)
			|| isEQ(wsaaCovrValid, "N")); wsaaSub.add(1)){
				if (isEQ(wsaaWaiveCrtable[wsaaSub.toInt()], covrmjaIO.getCrtable())) {
					wsaaCovrValid = "N";
				}
			}
			/* Do not calculate the premium tax on the COVRMJA record if the   */
			/* component code exists in the TR517 list.                        */
			if (isEQ(wsaaCovrValid, "N")) {
				goTo(GotoLabel.b620ReadNextRecord);
			}
		}
	}

protected void b620ReadTr52e()
	{
		/* Read the tax control table.                                     */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		b690ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			b690ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b690ReadTr52e();
		}
		/* Check if premium tax applies or not.                            */
		if (isNE(tr52erec.taxind01, "Y")) {
			goTo(GotoLabel.b620ReadNextRecord);
		}
		/* Call the tax calculation subroutine.                            */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(covrmjaIO.getLife());
		txcalcrec.coverage.set(covrmjaIO.getCoverage());
		txcalcrec.rider.set(covrmjaIO.getRider());
		txcalcrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		txcalcrec.crtable.set(covrmjaIO.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set("PREM");
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.effdate.set(wsaaTaxEffdate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrpf.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(covrmjaIO.getZbinstprem());
		}
		else {
			txcalcrec.amountIn.set(covrmjaIO.getInstprem());
		}
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		/* CALL TR52D TAX SUBROUTINE                                       */
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			xxxxFatalError();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrpf.getChdrcoy());
		taxdIO.setChdrnum(chdrpf.getChdrnum());
		taxdIO.setTrantype(txcalcrec.transType);
		taxdIO.setLife(txcalcrec.life);
		taxdIO.setCoverage(txcalcrec.coverage);
		taxdIO.setRider(txcalcrec.rider);
		taxdIO.setPlansfx(txcalcrec.planSuffix);
		taxdIO.setEffdate(txcalcrec.effdate);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(txcalcrec.taxrule);
		stringVariable2.addExpression(txcalcrec.rateItem);
		stringVariable2.setStringInto(taxdIO.getTranref());
		taxdIO.setInstfrom(txcalcrec.effdate);
		taxdIO.setInstto(payrpf.getBtdate());
		taxdIO.setBillcd(wsaaBillcd);
		taxdIO.setTranno(wsaaTranno);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg(SPACES);
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void b620ReadNextRecord()
	{
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void b630CalcContTax()
	{
		/* Read the tax control table.                                     */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set("****");
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		b690ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b690ReadTr52e();
		}
		/* Check if premium tax applies to contract fee or not             */
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		/* Call the tax calculation subroutine.                            */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set("CNTF");
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.effdate.set(wsaaTaxEffdate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrpf.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		txcalcrec.amountIn.set(wsaaCntfee);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		/* CALL TR52D TAX SUBROUTINE                                       */
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			xxxxFatalError();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrpf.getChdrcoy());
		taxdIO.setChdrnum(chdrpf.getChdrnum());
		taxdIO.setTrantype(txcalcrec.transType);
		taxdIO.setLife(SPACES);
		taxdIO.setCoverage(SPACES);
		taxdIO.setRider(SPACES);
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(txcalcrec.effdate);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(txcalcrec.taxrule);
		stringVariable2.addExpression(txcalcrec.rateItem);
		stringVariable2.setStringInto(taxdIO.getTranref());
		taxdIO.setInstfrom(txcalcrec.effdate);
		taxdIO.setInstto(payrpf.getBtdate());
		taxdIO.setBillcd(wsaaBillcd);
		taxdIO.setTranno(wsaaTranno);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg(SPACES);
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void b660ReadT5679()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(atrtTransCode);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void b670StatusCheck()
	{
		try {
			b670CovrRiskStatus();
			b670CovrPremStatus();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void b670CovrRiskStatus()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.covRiskStat[wsaaSub.toInt()])) {
					wsaaValidStatus.set("Y");
					wsaaSub.set(13);
				}
			}
		}
		else {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.ridRiskStat[wsaaSub.toInt()])) {
					wsaaValidStatus.set("Y");
					wsaaSub.set(13);
				}
			}
		}
		if (isEQ(wsaaValidStatus, "N")) {
			goTo(GotoLabel.b670Exit);
		}
	}

protected void b670CovrPremStatus()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.covPremStat[wsaaSub.toInt()])) {
					wsaaValidStatus.set("Y");
					wsaaSub.set(13);
				}
			}
		}
		else {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.ridPremStat[wsaaSub.toInt()])) {
					wsaaValidStatus.set("Y");
					wsaaSub.set(13);
				}
			}
		}
	}

protected void b690ReadTr52e()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrpf.getBtdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (((isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (((isEQ(itdmIO.getItemcoy(), chdrpf.getChdrcoy()))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void b700PostTax()
	{
		/*B700-START*/
		/* Read TAXD records                                               */
		taxdbilIO.setStatuz(varcom.oK);
		taxdbilIO.setChdrcoy(chdrpf.getChdrcoy());
		taxdbilIO.setChdrnum(chdrpf.getChdrnum());
		taxdbilIO.setInstfrom(linsrnlIO.getInstfrom());
		taxdbilIO.setLife(SPACES);
		taxdbilIO.setCoverage(SPACES);
		taxdbilIO.setRider(SPACES);
		taxdbilIO.setTrantype(SPACES);
		taxdbilIO.setPlansfx(ZERO);
		taxdbilIO.setFunction(varcom.begn);
		taxdbilIO.setFormat(formatsInner.taxdbilrec);
		while ( !(isEQ(taxdbilIO.getStatuz(), varcom.endp))) {
			b800ProcessTax();
		}

		/*B700-EXIT*/
	}

protected void b800ProcessTax()
	{
		
		/* Read TAXD records                                               */
		SmartFileCode.execute(appVars, taxdbilIO);
		if (isNE(taxdbilIO.getStatuz(), varcom.oK)
		&& isNE(taxdbilIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(taxdbilIO.getStatuz());
			syserrrec.params.set(taxdbilIO.getParams());
			xxxxFatalError();
		}
		if (isNE(taxdbilIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(taxdbilIO.getChdrnum(), chdrpf.getChdrnum())
		|| isEQ(taxdbilIO.getStatuz(), varcom.endp)) {
			taxdbilIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(taxdbilIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(taxdbilIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(taxdbilIO.getEffdate(), linsrnlIO.getInstfrom())
		|| isNE(taxdbilIO.getPostflg(), SPACES)) {
			taxdbilIO.setFunction(varcom.nextr);
			return ;
		}
		/* Read table TR52E                                                */
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(subString(taxdbilIO.getTranref(), 1, 8));
		itdmIO.setItmfrm(taxdbilIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		wsaaItdmkey.set(itdmIO.getDataKey());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)
		|| isNE(subString(taxdbilIO.getTranref(), 1, 8), itdmIO.getItemitem())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr52e)
		|| isNE(subString(taxdbilIO.getTranref(), 1, 8), itdmIO.getItemitem())) {
			itdmIO.setDataKey(wsaaItdmkey);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f109);
			xxxxFatalError();
		}
		tr52erec.tr52eRec.set(itdmIO.getGenarea());
		/* Call tax subroutine                                             */
		txcalcrec.function.set("POST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(taxdbilIO.getChdrcoy());
		txcalcrec.chdrnum.set(taxdbilIO.getChdrnum());
		txcalcrec.life.set(taxdbilIO.getLife());
		txcalcrec.coverage.set(taxdbilIO.getCoverage());
		txcalcrec.rider.set(taxdbilIO.getRider());
		txcalcrec.planSuffix.set(taxdbilIO.getPlansfx());
		if (isNE(taxdbilIO.getCoverage(), SPACES)) {
			covrmjaIO.setRecKeyData(SPACES);
			covrmjaIO.setChdrcoy(chdrpf.getChdrcoy());
			covrmjaIO.setChdrnum(chdrpf.getChdrnum());
			covrmjaIO.setLife(taxdbilIO.getLife());
			covrmjaIO.setCoverage(taxdbilIO.getCoverage());
			covrmjaIO.setRider(taxdbilIO.getRider());
			covrmjaIO.setPlanSuffix(taxdbilIO.getPlansfx());
			covrmjaIO.setFunction(varcom.readr);
			covrmjaIO.setStatuz(varcom.oK);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				xxxxFatalError();
			}
			txcalcrec.crtable.set(covrmjaIO.getCrtable());
			txcalcrec.cntTaxInd.set("N");
		}
		else {
			txcalcrec.crtable.set(SPACES);
			txcalcrec.cntTaxInd.set("Y");
		}
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.taxrule.set(subString(taxdbilIO.getTranref(), 1, 8));
		txcalcrec.rateItem.set(subString(taxdbilIO.getTranref(), 9, 8));
		txcalcrec.amountIn.set(taxdbilIO.getBaseamt());
		txcalcrec.effdate.set(taxdbilIO.getEffdate());
		txcalcrec.taxType[1].set(taxdbilIO.getTxtype01());
		txcalcrec.taxType[2].set(taxdbilIO.getTxtype02());
		txcalcrec.taxAmt[1].set(taxdbilIO.getTaxamt01());
		txcalcrec.taxAmt[2].set(taxdbilIO.getTaxamt02());
		txcalcrec.taxAbsorb[1].set(taxdbilIO.getTxabsind01());
		txcalcrec.taxAbsorb[2].set(taxdbilIO.getTxabsind02());
		txcalcrec.batckey.set(atmodrec.batchKey);
		txcalcrec.language.set(atmodrec.language);
		txcalcrec.jrnseq.set(wsaaJrnseq);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.tranno.set(taxdbilIO.getTranno());
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			xxxxFatalError();
		}
		wsaaJrnseq.set(txcalcrec.jrnseq);
		/* Update TAXD record                                              */
		/*  IF TXCL-STATUZ          NOT = O-K                   <LA4758>*/
		/*     MOVE TXCL-LINK-REC      TO SYSR-PARAMS           <LA4758>*/
		/*     MOVE TXCL-STATUZ        TO SYSR-STATUZ           <LA4758>*/
		/*     PERFORM XXXX-FATAL-ERROR                         <LA4758>*/
		/*  END-IF.                                             <LA4758>*/
		taxdIO.setRrn(taxdbilIO.getRrn());
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(taxdIO.getStatuz());
			syserrrec.params.set(taxdIO.getParams());
			xxxxFatalError();
		}
		taxdIO.setPostflg("P");
		taxdIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(taxdIO.getStatuz());
			syserrrec.params.set(taxdIO.getParams());
			xxxxFatalError();
		}
		/* Read next TAXD                                                  */
		taxdbilIO.setFunction(varcom.nextr);
	}

protected void c000CallRounding()
	{
		/*C100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrpf.getBillcurr());
		zrdecplrec.batctrcde.set(atrtTransCode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/*C900-EXIT*/
	}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
public static final class TablesInner {
		/* TABLES */
	public final String t5645 = "T5645";
	private final String t5688 = "T5688";
	private final String t5679 = "T5679";
	private final String t1688 = "T1688";	
	private final String tr52d = "TR52D";
	private final String t5687 = "T5687";
	private final String t5675 = "T5675";
	private final String t5667 = "T5667";
	private final String t3695 = "T3695";
	private final String t3629 = "T3629";
	private final String t3688 = "T3688";
	private final String t5644 = "T5644";
	private final String t5671 = "T5671";
	private final String t6647 = "T6647";
	private final String t6687 = "T6687";
	private final String t6659 = "T6659";
	private final String th605 = "TH605";
	private final String t7508 = "T7508";
	private final String tr517 = "TR517";
	private final String tr52e = "TR52E";
	//ILIFE-1345 STARTS
	private final String t5729 = "T5729";
	//ILIFE-1345 ENDS
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lextbrrrec = new FixedLengthStringData(10).init("LEXTBRRREC");
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData agcmseqrec = new FixedLengthStringData(10).init("AGCMSEQREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
	private FixedLengthStringData taxdbilrec = new FixedLengthStringData(10).init("TAXDBILREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData fpcorec = new FixedLengthStringData(10).init("FPCOREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}

protected Payrpf populatePayrpf1010(){
	return payrpfDAO.getPayrRecordByCoyAndNumAndSeq(wsaaCompany.toString(), wsaaChdrnum.toString());
}

protected void updateLinsForOtherFee(){
}
}
