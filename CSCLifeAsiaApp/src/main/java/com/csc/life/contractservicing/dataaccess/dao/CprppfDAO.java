package com.csc.life.contractservicing.dataaccess.dao;

import com.csc.life.contractservicing.dataaccess.model.Cprppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CprppfDAO extends BaseDAO<Cprppf> {

	public int updateOrInstCprppf(Cprppf cprppf);
	public Cprppf getCprpRecord(Cprppf cprppf);
	public int updateCprppfForTrannoAndInstprem(Cprppf cprppf);
	//ILIFE-6809
	public void delCprpRecord(String chdrpfx,String chdrcoy,String chdrnum);
}
