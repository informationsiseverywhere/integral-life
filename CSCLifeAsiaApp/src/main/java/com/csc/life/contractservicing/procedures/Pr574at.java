/*
 * File: Pr574at.java
 * Date: 30 August 2009 1:44:25
 * Author: Quipoz Limited
 *
 * Class transformed from PR574AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.tablestructures.Th502rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CprpTableDAM;
import com.csc.life.contractservicing.dataaccess.CvuwTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.recordstructures.Acomcalrec;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcorevTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.procedures.Rlliadb;
import com.csc.life.productdefinition.procedures.Vpuubbl;
import com.csc.life.productdefinition.procedures.Vpxubbl;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Rlliadbrec;
import com.csc.life.productdefinition.recordstructures.Stdtallrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5676rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltmjaTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*REMARKS.
*
*
*
*  PR574AT - Component Change AT Processing.
*  -----------------------------------------
*
*  This program will be run  under  AT  and  will  perform  the
*  general  functions for the finalisation of Component Change.
*  It will then ascertain whether or not  there  is  a  generic
*  component  specific  subroutine  to complete the change and,
*  if so, it will call it.
*
*  The  general  processing  will  carry  out   the   following
*  functions:
*
*  1.  Increment the TRANNO field on the Contract Header.
*
*  2.  Write a PTRN record to mark the transaction.
*
*  3.  Call the Breakout routine if required.
*
*  4.  Create a new COVR record from the COVT transaction
*      records.
*
*  5.  Rewrite the old COVR record with its VALIDFLAG set
*      to '2'.
*
*  6.  Delete the COVT records.
*
*  7.  Create new ULNK records from the UNLT transaction
*      records if they exist.
*
*  8.  Rewrite the old corresponding ULNK records with their
*      VALIDFLAG's set to '2'.
*
*  9.  Delete the UNLT records.
*
*  10. If an increase or decrease has occurred it will create
*      or update the necessary commission records, (on AGCM),
*      by calling the ACOMCALC subroutine.
*
*  11. If commission clawback is required it will create the
*      necessary G/L postings, (ACMV records), again in the
*      ACOMCALC subroutine.
*
*  12. Call the generic subroutine if one exists.
*
*  Processing:
*  -----------
*
*
*  Contract Header.
*  ----------------
*
*  The  ATMOD-PRIMARY-KEY will contain the Company and Contract
*  number  of  the  contract  being  processed.  Use  these  to
*  perform  a READH on CHDRMJA. Increment the TRANNO field by 1
*  and rewrite the Contract Header.
*
*
*  PTRN Record.
*  ------------
*
*  Write a PTRN record with the new TRANNO.
*
*  See the Notes section at the end of this  specification  for
*  a description of the field contents.
*
*  Breakout Processing.
*  --------------------
*
*
*  A  physical  breakout  will  be  required if any COVT record
*  exists for the transaction that has a Plan  Suffix  that  is
*  less  than  or equal to CHDRMJA-POLSUM. All the COVT records
*  will have to be read first just to determine whether or  not
*  this is necessary.
*
*  Use  the  Contract  Company  and Contract Number to read all
*  the COVT records that match on  this  portion  of  the  key.
*  Make  a  note of the lowest COVT-PLAN-SUFFIX. At the end, if
*  this is not greater than CHDRMJA-POLSUM then the  subroutine
*  BRKOUT  must  be called. Set the OLD-SUMMARY to the value of
*  CHDRMJA-POLSUM and the  NEW-SUMMARY  to  the  value  of  the
*  lowest COVT-PLAN-SUFFIX, then call 'BRKOUT'.
*
*
*  Coverage Record Processing.
*  ---------------------------
*
*  The  COVT  records  will  drive the processing for Component
*  Change  records,  Fund  Direction  records,   Options/Extras
*  records  and  Commission and Clawback records. For each COVT
*  record found the program should perform the  processing  for
*  COVT,   UNLT,   LEXT   and,   if  necessary  Commission  and
*  Commission Clawback.
*
*  Use the Contract Company and Contract  Number  to  read  all
*  the  COVT records that match on this portion of the key. For
*  each one read the  corresponding  COVR  record,  change  its
*  VALIDFLAG  to  '2'  and  rewrite  it.  Then write a new COVR
*  record from the COVT record  and  delete  the  COVT  record.
*  Perform  the  UNLT, LEXT, Commission and Commission Clawback
*  processing for all records associated with the COVT record.
*
*  The program must determine whether or  not  an  increase  or
*  decrease  has  occurred.  The  increase  or decrease will be
*  relevant to individual components. An increase  or  decrease
*  will  be  deemed  to have occurred if the new premium varies
*  from the old.
*
*  If an increase  or  decrease  has  occurred  the  commission
*  records  will  have  to be processed. If there is a decrease
*  then Commission Clawback may also have to be  processed.  If
*  there  is  no difference then there will be no Commission or
*  Clawback processing.
*
*
*  Fund Direction Processing.
*  --------------------------
*
*  Use the COVT key to read UNLT. If no record  is  found  then
*  there  is no Fund Direction processing. If one is found then
*  use the key  to  read  the  corresponding  ULNK  record  and
*  rewrite  it with its VALIDFLAG set to '2'. Create a new ULNK
*  record from the UNLT record and delete the UNLT record.
*
*
*  Commission Processing.
*  ----------------------
*
*  See the remarks in the ACOMCALC subroutine for details on
*   how the AGCM records are processed.
*
*  Generic Processing.
*  -------------------
*
*  The  program  will  now read T5671 with a key of Transaction
*  Code concatenated with the Component Code, (CRTABLE). If  no
*  entry  is found then no generic processing will be required.
*  If a record is found then  call  the  program  held  therein
*  with   a   linkage   section  containing  Contract  Company,
*  Contract Number and TRANNO.
*
*
*  Policy Suspense will need to be reduced by the premium of the
*  component. To do this call LIFRTRN which will create an RTRN;
*  Notes.
*  ------
*
*  Tables:
*  -------
*
*   T5671 - Coverage/Rider Switching           Key: Tran Code||CRTABLE
*   T5687 - General Coverage/Rider Details     Key: CRTABLE
*   T5729 - Flexible Premium details           Key: CRTABLE
*
*
*  PTRN Record:
*  ------------
*
*  . PTRN-BATCTRCDE         -  Transaction Code
*  . PTRN-CHDRCOY           -  CHDRMJA-CHDRCOY
*  . PTRN-CHDRNUM           -  CHDRMJA-CHDRNUM
*  . PTRN-TRANNO            -  CHDRMJA-TRANNO
*  . PTRN-TRANSACTION-DATE  -  Current Date
*  . PTRN-TRANSACTION-TIME  -  Current Time
*  . PTRN-PTRNEFF           -  Current Date
*  . PTRN-TERMID            -  Passed in Linkage
*  . PTRN-USER              -  Passed in Linkage.
*
*
* The general processing is:
* 1. Increment the TRANNO field on the Contract Header.
*
* 2. Write a PTRN record to mark the transaction.
*
* 3. Call the Breakout routine if required, if it is called keep
*    the new updated CHDR which will have the latest details.
*
* 4. Create a new COVR record from the COVT transaction records.
*
* 5. Rewrite the old COVR record with its VALIDFLAG set
*    to '2'.
*
* 6  For Flexible Premiums, rewrite old FPCOs with validflag '2'.
*    Re-calculate FPCO details according to adjusted premium
*    from Component Modify.  Write new FPCO.
*
*    For a Component Add, calculate FPCO values,zeroise Premium
*    Rec'd, Premium Billed and Min Overdue fields and write new
*    record.
*
* 7. Delete the COVT records.
*
* 8. Create new ULNK records from the UNLT transaction
*    records if they exist.
*
* 9. Rewrite the old corresponding ULNK records with their
*    VALIDFLAG's set to '2'.
*
* 10.Delete the UNLT records.
*
* 11. If an increase or decrease has occurred it will create
*     or update the necessary commission records, (on AGCM),
*     by calling the ACOMCALC subroutine.
*
* 12. If commission clawback is required it will create the
*     necessary G/L Postings, (ACMV records), in the
*     ACOMCALC subroutine.
*
* 13. Call the generic subroutine if one exists.
*
* 14. Update the CHDR record with new instalment fields.
*
* X400-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name, the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, e.g. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, e.g. AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGCMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*
*****************************************************************
* </pre>
*/
public class Pr574at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PR574AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
		/* WSAA-WORKING-STORAGE */
	private PackedDecimalData wsaaBrkoutSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaLastSuffix = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaBillfreqX = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqX, 0).setUnsigned();

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransArea, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 24);
	private FixedLengthStringData filler1 = new FixedLengthStringData(175).isAPartOf(wsaaTransArea, 25, FILLER).init(SPACES);

	private FixedLengthStringData wsaaIfOvrdCommEnd = new FixedLengthStringData(1);
	private Validator wsaaEndOvrdComm = new Validator(wsaaIfOvrdCommEnd, "Y");
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30).init(SPACES);
		/* STORE-OLDCOVR-DATES */
	private PackedDecimalData wsaaUnitstmtDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaRerateDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaRerateFromDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaAnivprocDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBenbillDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaIucancDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(13, 2).init(ZERO);
		/* WSAA-VARIABLES */
	private ZonedDecimalData wsaaSuspAvail = new ZonedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaGlSub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCprpInstprem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8);
		/* WSAA-BILLFREQ-VAR */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0).setUnsigned();

	private FixedLengthStringData wsaaCovtKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovtChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovtKey, 0);
	private FixedLengthStringData wsaaCovtChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovtKey, 1);
	private FixedLengthStringData wsaaCovtLife = new FixedLengthStringData(2).isAPartOf(wsaaCovtKey, 9);
	private FixedLengthStringData wsaaCovtCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovtKey, 11);
	private FixedLengthStringData wsaaCovtRider = new FixedLengthStringData(2).isAPartOf(wsaaCovtKey, 13);
	private FixedLengthStringData filler3 = new FixedLengthStringData(49).isAPartOf(wsaaCovtKey, 15, FILLER).init(SPACES);
	private FixedLengthStringData wsaaBonusInd = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaCommethNo = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaLextExtDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaFreqFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaStampDutyAcc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtAnnprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovrAnnprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinstamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveZbinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtZbinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSuspPosting = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmountIn = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTaxableAmount = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaProrateFactor = new PackedDecimalData(9, 5).init(0);
	private PackedDecimalData wsaaTotalTax = new PackedDecimalData(9, 5).init(0);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBascpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSrvcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRnwcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasscmth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasscpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private PackedDecimalData wsaaCovtSuffix = new PackedDecimalData(4, 0);
	private String wsaaFirstUnltmja = "Y";
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaA = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaLextDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaUnltSeq = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaRldgagnt = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAgntChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgagnt, 0);
	private FixedLengthStringData wsaaAgntLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 8);
	private FixedLengthStringData wsaaAgntCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 10);
	private FixedLengthStringData wsaaAgntRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 12);
	private FixedLengthStringData wsaaAgntPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 14);
	private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2);
		/*    TO CALCULATE THE BALANCES IN ACBLBF                    **    */
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private PackedDecimalData wsaaT5645Sub = new PackedDecimalData(5, 0);
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2628 fixed--Array size increased
		private static final int wsaaT5645Size = 1000;
	

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
		/* WSAA-UALPRCS */
	private ZonedDecimalData[] wsaaUalprc = ZDInittedArray(10, 17, 2);

	private FixedLengthStringData wsaaSaveUalprcs = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaSaveUalprc = PDArrayPartOfStructure(10, 17, 2, wsaaSaveUalprcs, 0);

	private FixedLengthStringData wsaaUalprcAmounts = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaUalprcAmt = PDArrayPartOfStructure(10, 17, 2, wsaaUalprcAmounts, 0);
	private FixedLengthStringData wsaaStoreCovtKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaCompkey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompkeyTranno = new FixedLengthStringData(4).isAPartOf(wsaaCompkey, 0);
	private FixedLengthStringData wsaaCompkeyCrtable = new FixedLengthStringData(4).isAPartOf(wsaaCompkey, 4);

	private FixedLengthStringData wsaaProcessCompleteFlag = new FixedLengthStringData(1);
	private Validator processComplete = new Validator(wsaaProcessCompleteFlag, "Y");

	private FixedLengthStringData wsaaUlnkUpdate = new FixedLengthStringData(1);
	private Validator wsaaNoUlnkUpdate = new Validator(wsaaUlnkUpdate, "N");

	private FixedLengthStringData wsaaIfSplitReqd = new FixedLengthStringData(1);
	private Validator wsaaSplitNotReqd = new Validator(wsaaIfSplitReqd, "N");
	private Validator wsaaSplitReqd = new Validator(wsaaIfSplitReqd, "Y");

	private FixedLengthStringData wsaaEndProcessFlag = new FixedLengthStringData(1);
	private Validator processExit = new Validator(wsaaEndProcessFlag, "Y");

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");
	private PackedDecimalData wsaaOrigPtdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrPtdate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private ZonedDecimalData wsaaDayTemp = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaDayOld = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYyOld = new ZonedDecimalData(4, 0).isAPartOf(wsaaDayOld, 0).setUnsigned();
	private ZonedDecimalData wsaaMmOld = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayOld, 4).setUnsigned();
	private ZonedDecimalData wsaaDdOld = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayOld, 6).setUnsigned();

	private FixedLengthStringData wsaaDayNew = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYyNew = new ZonedDecimalData(4, 0).isAPartOf(wsaaDayNew, 0).setUnsigned();
	private ZonedDecimalData wsaaMmNew = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayNew, 4).setUnsigned();
	private ZonedDecimalData wsaaDdNew = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayNew, 6).setUnsigned();
		/*  Flexible Premiums totals                               <D9604>*/
	private ZonedDecimalData wsaaT5729Sub = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaNewTarget = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaNewBilled = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaNewBilledDecimal = new ZonedDecimalData(18, 7);
	private ZonedDecimalData wsaaNewOverdueMin = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOldBilled = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOldOverdueMin = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaCompChanged = new FixedLengthStringData(1).init(" ");
	private Validator compChanged = new Validator(wsaaCompChanged, "Y");

	private FixedLengthStringData wsaaCompAction = new FixedLengthStringData(1);
	private Validator wsaaCompAdd = new Validator(wsaaCompAction, "A");
	private Validator wsaaCompChg = new Validator(wsaaCompAction, "C");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaEarliestRerateDate = new PackedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCpiValid = new FixedLengthStringData(1);
	private Validator cpiValid = new Validator(wsaaCpiValid, "Y");

	private FixedLengthStringData wsaaWopMatch = new FixedLengthStringData(1);
	private Validator wopMatch = new Validator(wsaaWopMatch, "Y");
	private Validator wopNotMatch = new Validator(wsaaWopMatch, "N");

	private FixedLengthStringData wsaaCrtableMatch = new FixedLengthStringData(1).init("N");
	private Validator crtableMatch = new Validator(wsaaCrtableMatch, "Y");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 0);
	private ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOkeys, 1).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItem, 3);
	private String wsaaWaiveCont = "";

	private FixedLengthStringData wsaaZrwvflgs = new FixedLengthStringData(3);
	private FixedLengthStringData[] wsaaZrwvflg = FLSArrayPartOfStructure(3, 1, wsaaZrwvflgs, 0);
	private FixedLengthStringData wsaaTr517Rec = new FixedLengthStringData(250);

	private FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	private FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private PackedDecimalData wsaaRegPremFirst = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegPremRenewal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZctnPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegIntmDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCrrcdDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCrrcdDateX = new FixedLengthStringData(8).isAPartOf(wsaaCrrcdDate, 0, REDEFINE);
	private FixedLengthStringData wsaaCrrcdMm = new FixedLengthStringData(2).isAPartOf(wsaaCrrcdDateX, 4);
	private FixedLengthStringData wsaaCrrcdDd = new FixedLengthStringData(2).isAPartOf(wsaaCrrcdDateX, 6);

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator singPrem = new Validator(wsaaPremType, "S");
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
		/* ERRORS */
	private static final String e351 = "E351";
	private static final String e308 = "E308";
	private static final String f294 = "F294";
	private static final String g586 = "G586";
	private static final String hl49 = "HL49";
	private static final String h791 = "H791";
	private IntegerData wsaaT5644Ix = new IntegerData();
	private AcblTableDAM acblIO = new AcblTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	protected CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private CprpTableDAM cprpIO = new CprpTableDAM();
	private CvuwTableDAM cvuwIO = new CvuwTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcorevTableDAM fpcorevIO = new FpcorevTableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HitdTableDAM hitdIO = new HitdTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UnltmjaTableDAM unltmjaIO = new UnltmjaTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Acomcalrec acomcalrec = new Acomcalrec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Isuallrec isuallrec = new Isuallrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Stdtallrec stdtallrec = new Stdtallrec();
	private Actvresrec actvresrec = new Actvresrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private T3695rec t3695rec = new T3695rec();
	private T5534rec t5534rec = new T5534rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5644rec t5644rec = new T5644rec();
	private T5687rec t5687rec = new T5687rec();
	private T5676rec t5676rec = new T5676rec();
	private T5671rec t5671rec = new T5671rec();
	private T5688rec t5688rec = new T5688rec();
	private T5729rec t5729rec = new T5729rec();
	private T5448rec t5448rec = new T5448rec();
	private T5515rec t5515rec = new T5515rec();
	private Th510rec th510rec = new Th510rec();
	private Tr517rec tr517rec = new Tr517rec();
	private Th502rec th502rec = new Th502rec();
	private T6640rec t6640rec = new T6640rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Tr695rec tr695rec = new Tr695rec();
	private Rlliadbrec rlliadbrec = new Rlliadbrec();
	private Tr384rec tr384rec = new Tr384rec();
	private T7508rec t7508rec = new T7508rec();
	private Th605rec th605rec = new Th605rec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private ExternalisedRules er = new ExternalisedRules();
	private boolean stampDutyflag = false;
	private Exclpf exclpf=null; 
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class); 
	private List<Covrpf> covrlnbList = new ArrayList<Covrpf>();
	private CovrpfDAO covrpupDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
	private List<Agcmpf> agcmpfUpdateList = new ArrayList<Agcmpf>();
	private Agcmpf agcmpf = new Agcmpf();
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO",ItempfDAO.class); 
	private Itempf itempf = new Itempf();
	//ILIFE-8919 Starts
	private static final String SYS_COY = "0";
	//ILIFE-8919 End
	private boolean isDMSFlgOn = false;	//ILIFE-8880
	private static final String DMS_FEATURE = "SAFCF001";	//ILIFE-8880
	private boolean lincFlag = false;
	private static final String lincFeature = "NBPRP116";
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private PackedDecimalData wsaaCovtCommPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovrCommPrem = new PackedDecimalData(17, 2);
	//PINNACLE-2323 START
	private boolean adjuReasrFlag;
	private static final String ADJU_REASR_FEATURE = "CSLRI008";
	//PINNACLE-2323 END 

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		pass1030,
		exit1090,
		deleteCovt2650,
		nextCovt2670,
		exit2690,
		next3535,
		exit3539,
		exit3790,
		readNextLext4064,
		rerateDates4065,
		break8500,
		nextRecord20100,
		read20200,
		addUlnk21050,
		exit21090,
		e090Exit,
		a230Exit,
		a420Check,
		a480Next,
		a110Fund,
		a180NextFund,
		callCprp3950,
		exit3900
	}

	public Pr574at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		contractHeader1000();
		initialize1200();
		checkSuspAvail1500();
		brkoutProcessing2000();
		brkoutCovtUnlt2500();
		customerSpecificcalcFeeChrges();
		coverageProcessing3000();
		updateHeader3500();
		callLifrtrn3600();
		writeLetter3700();
		x100WritePtrn();
		lincFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), lincFeature, appVars, "IT");
		adjuReasrFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), ADJU_REASR_FEATURE, appVars, "IT");
		if(lincFlag)
			performLincProcessing();
		m800UpdateMlia();
		x200UpdateBatchHeader();
		dryProcessing8000();
		/*    PERFORM X300-RELEASE-SFTLCK.                                 */
		deleteCvuw3800();
		deleteCprp3900();
		x400Statistics();
		x300ReleaseSftlck();
	}

protected void exit0009()
	{
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* START                                                       *
	* </pre>
	*/
protected void contractHeader1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initial1010();
					readStatusT56791015();
					contractHeader1020();
				case pass1030:
					pass1030();
					readStatusCodes1030();
					callDatcon31040();
					readTh6051050();
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initial1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		wsaaToday.set(datcon1rec.intDate);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
	}

protected void readStatusT56791015()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void contractHeader1020()
	{
		/* Read & hold CHDRMJA record*/
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*  Read Flexible Premium Table.                           <D9604> */
		/*                                                         <D9604> */
		wsaaFlexiblePremium.set("N");
		itemIO.setStatuz(SPACES);
		itemIO.setDataKey(SPACES);
		itemIO.setItemtabl(tablesInner.t5729);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			t5729rec.t5729Rec.set(itemIO.getGenarea());
			wsaaFlexiblePremium.set("Y");
		}
		/* Read & hold PAYR record*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction("READH");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		/*  Rewrite the original contract header for historical purposes.*/
		/* When doing component approval after proposal just change the    */
		/* the CHDR record created by proposal .                           */
		/*    MOVE '2'                    TO CHDRMJA-VALIDFLAG.            */
		payrIO.setValidflag("2");
		/* For flex prem contracts use WSSA effective date                 */
		/* which will be either the target period start date               */
		/* in the event of modification or btdate otherwise                */
		if (flexiblePremium.isTrue()) {
			/*       MOVE WSAA-EFFDATE        TO CHDRMJA-CURRTO        <V4L004>*/
			/*                                   WSAA-ORIG-PTDATE      <V4L004>*/
			wsaaOrigPtdate.set(wsaaEffdate);
			wsaaPayrPtdate.set(payrIO.getPtdate());
			goTo(GotoLabel.pass1030);
		}
		/* Write the CURRTO date for valid flag '2'*/
		/* records on the CHDR File*/
		/*    MOVE PAYR-PTDATE            TO CHDRMJA-CURRTO,               */
		/*                                   WSAA-ORIG-PTDATE.             */
		wsaaOrigPtdate.set(payrIO.getPtdate());
		wsaaItemCnttype.set(chdrmjaIO.getCnttype());
	}

protected void pass1030()
	{
		/* Update CHDRMJA header with TRANNO + 1 AFTER REWRT OF OLD RECORD*/
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		chdrmjaIO.setCurrfrom(wsaaOrigPtdate);
		chdrmjaIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*    ADD 1                       TO CHDRMJA-TRANNO.               */
		/*    MOVE '1'                    TO CHDRMJA-VALIDFLAG.            */
		/*    MOVE 99999999               TO CHDRMJA-CURRTO.               */
		/*    MOVE T5679-SET-CN-RISK-STAT TO CHDRMJA-STATCODE.             */
		/*    MOVE WSAA-ORIG-PTDATE       TO CHDRMJA-CURRFROM.             */
		/*    MOVE WRITR                  TO CHDRMJA-FUNCTION.             */
		/*    MOVE CHDRMJAREC             TO CHDRMJA-FORMAT.               */
		/*    CALL 'CHDRMJAIO'            USING CHDRMJA-PARAMS.            */
		/*    IF CHDRMJA-STATUZ           NOT = O-K                        */
		/*       MOVE CHDRMJA-PARAMS      TO SYSR-PARAMS                   */
		/*       MOVE CHDRMJA-STATUZ      TO SYSR-STATUZ                   */
		/*       PERFORM XXXX-FATAL-ERROR.                                 */
		/* Update PAYR record with TRANNO + 1 AFTER REWRT OF OLD RECORD*/
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setValidflag("1");
		payrIO.setPtdate(wsaaOrigPtdate);
		payrIO.setEffdate(wsaaOrigPtdate);
		if (flexiblePremium.isTrue()) {
			payrIO.setPtdate(wsaaPayrPtdate);
		}
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readStatusCodes1030()
	{
		/*    MOVE ATMD-BATCH-KEY         TO WSAA-BATCKEY.                 */
		/*    MOVE SPACES                 TO ITEM-DATA-KEY.                */
		/*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/*    MOVE ATMD-COMPANY           TO ITEM-ITEMCOY.                 */
		/*    MOVE T5679                  TO ITEM-ITEMTABL.                */
		/*    MOVE WSKY-BATC-BATCTRCDE    TO ITEM-ITEMITEM.                */
		/*    MOVE 'READR'                TO ITEM-FUNCTION.                */
		/*    CALL 'ITEMIO' USING ITEM-PARAMS.                             */
		/*    IF ITEM-STATUZ              NOT = O-K                        */
		/*        MOVE ITEM-PARAMS        TO SYSR-PARAMS                   */
		/*        PERFORM XXXX-FATAL-ERROR.                                */
		/*    MOVE ITEM-GENAREA TO T5679-T5679-REC.                        */
		/* Read the contract definition  on T5688.*/
		/*    New - to read T5644                                          */
		/*  Load the commission methods.                                   */
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemitem(SPACES);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setFunction(varcom.begn);
		readT56441700();

		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.t5688.toString(),
				chdrmjaIO.getOccdate().toInt(),chdrmjaIO.getCnttype().toString());
		if (null == itempf) { //fix ILIFE-8777 defect
			syserrrec.params.set(tablesInner.t5688.toString() + chdrmjaIO.getOccdate().toString());
			syserrrec.statuz.set(e308);
			xxxxFatalError();
		}
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void callDatcon31040()
	{
		/* Get the frequency Factor from DATCON3 for Regular premiums.*/
		if (isEQ(payrIO.getBillfreq(), "00")) {
			wsaaFreqFactor.set(1);
			goTo(GotoLabel.exit1090);
		}
		wsaaBillfreqX.set(payrIO.getBillfreq());
		wsaaFreqFactor.set(wsaaBillfreq9);
	}

protected void readTh6051050()
	{
		/*  Read TH605 to see if bonus workbench extraction used.          */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(atmodrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/*    IF ITEM-STATUZ           NOT = O-K AND MRNF          <V70L01>*/
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void initialize1200()
	{
		begn1210();
	}

protected void begn1210()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("PR574");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		ptrnIO.setParams(SPACES);
		wsaaSuspAvail.set(ZERO);
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifacmvrec.lifacmvRec.set(SPACES);
		wsaaJrnseq.set(ZERO);
		lifrtrnrec.batccoy.set(atmodrec.company);
		lifrtrnrec.rldgcoy.set(atmodrec.company);
		lifrtrnrec.genlcoy.set(atmodrec.company);
		lifacmvrec.batccoy.set(atmodrec.company);
		lifacmvrec.rldgcoy.set(atmodrec.company);
		lifacmvrec.genlcoy.set(atmodrec.company);
		ptrnIO.setBatccoy(atmodrec.company);
		lifrtrnrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		lifrtrnrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		lifrtrnrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		lifrtrnrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		lifrtrnrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		lifrtrnrec.rcamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifrtrnrec.crate.set(0);
		lifacmvrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifacmvrec.acctamt.set(0);
		lifrtrnrec.user.set(0);
		lifacmvrec.user.set(0);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.transactionTime.set(varcom.vrcmTime);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifrtrnrec.transactionDate.set(varcom.vrcmDate);
		isDMSFlgOn = FeaConfg.isFeatureExist(SYS_COY, DMS_FEATURE, appVars, "IT"); //ILIFE-8880
	}

protected void checkSuspAvail1500()
	{
		begn1510();
	}

	/**
	* <pre>
	*  1. Check T5645, T3695         *
	*     ACBLPF - Amount in suspense*
	* </pre>
	*/
protected void begn1510()
	{
		itemIO.setStatuz(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("PR574");
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		readT56451600();
		/*READ THE SECOND PAGE OF T5645                                */
		itemIO.setStatuz(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("PR574");
		itemIO.setItemseq("01");
		itemIO.setFunction(varcom.readr);
		readT56451600();
		/*READ THE THIRD  PAGE OF T5645                                */
		itemIO.setStatuz(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("PR574");
		itemIO.setItemseq("02");
		itemIO.setFunction(varcom.readr);
		readT56451600();
		acblIO.setRldgacct(chdrmjaIO.getChdrnum());
		acblIO.setRldgcoy(chdrmjaIO.getChdrcoy());
		acblIO.setOrigcurr(chdrmjaIO.getBillcurr());
		acblIO.setSacscode(wsaaT5645Sacscode[1]);
		acblIO.setSacstyp(wsaaT5645Sacstype[1]);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			xxxxFatalError();
		}
		itemIO.setStatuz(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(wsaaT5645Sacstype[1]);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			wsaaSuspAvail.set(0);
		}
		else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(sub(0, acblIO.getSacscurbal()));
			}
			else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
	}

protected void readT56451600()
	{
		start1610();
	}

protected void start1610()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT5645Sub.set(1);
		if (isGT(wsaaT5645Sub, wsaaT5645Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5645);
			xxxxFatalError();
		}
		while ( !(isGT(wsaaT5645Sub, 15))) {
			wsaaT5645Cnttot[wsaaT5645Offset.toInt()].set(t5645rec.cnttot[wsaaT5645Sub.toInt()]);
			wsaaT5645Glmap[wsaaT5645Offset.toInt()].set(t5645rec.glmap[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacscode[wsaaT5645Offset.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacstype[wsaaT5645Offset.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
			wsaaT5645Sign[wsaaT5645Offset.toInt()].set(t5645rec.sign[wsaaT5645Sub.toInt()]);
			wsaaT5645Sub.add(1);
			wsaaT5645Offset.add(1);
		}

	}

protected void readT56441700()
	{
		start1710();
	}

protected void start1710()
	{
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemitem(comlinkrec.method);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
	}

protected void brkoutProcessing2000()
	{
		checkBreakout2010();
		callBreakout2020();
	}

protected void checkBreakout2010()
	{
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setStatuz(varcom.oK);
		covtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife("01");
		covtmjaIO.setJlife("00");
		covtmjaIO.setCoverage("01");
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(9999);
		wsaaBrkoutSuffix.set(ZERO);
		wsaaLastSuffix.set(chdrmjaIO.getPolsum());
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covtmjaIO.getStatuz(),varcom.endp))) {
			readAllCovts2100();
		}

	}

protected void callBreakout2020()
	{
		/* Call the BRKOUT subroutine to breakout COVR and AGCM*/
		if (isGT(wsaaBrkoutSuffix, ZERO)) {
			brkoutrec.outRec.set(SPACES);
			brkoutrec.brkChdrcoy.set(chdrmjaIO.getChdrcoy());
			brkoutrec.brkChdrnum.set(chdrmjaIO.getChdrnum());
			brkoutrec.brkOldSummary.set(chdrmjaIO.getPolsum());
			brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
			compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaBrkoutSuffix, 1));
			callProgram(Brkout.class, brkoutrec.outRec);
			if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
				/*       MOVE BRK-STATUZ       TO SYSR-PARAMS                   */
				syserrrec.params.set(brkoutrec.outRec);
				syserrrec.statuz.set(brkoutrec.brkStatuz);
				xxxxFatalError();
			}
		}
		/* If BRKOUT is called then the CHDR will have been updated,*/
		/* we therefore need to RLSE the kept CHDRMJA and then READR*/
		/* again to save the correct CHDR for updating further on in the*/
		/* program.*/
		/* This also applies to the PAYR record*/
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readAllCovts2100()
	{
		callCovt2110();
	}

protected void callCovt2110()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covtmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
			return ;
		}
		if (isLTE(covtmjaIO.getPlanSuffix(), chdrmjaIO.getPolsum())
		&& isLTE(covtmjaIO.getPlanSuffix(), wsaaLastSuffix)) {
			wsaaLastSuffix.set(covtmjaIO.getPlanSuffix());
			wsaaBrkoutSuffix.set(covtmjaIO.getPlanSuffix());
		}
		covtmjaIO.setFunction(varcom.nextr);
	}

protected void brkoutCovtUnlt2500()
	{
		start2510();
	}

	/**
	* <pre>
	* BREAKS OUT COVT AND UNLT RECORDS IF COVERAGE/RIDER IS
	*  ADDITIONAL BENEFIT.
	* </pre>
	*/
protected void start2510()
	{
		if (isEQ(chdrmjaIO.getPolinc(), chdrmjaIO.getPolsum())
		|| isEQ(chdrmjaIO.getPolinc(), 1)) {
			return ;
		}
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setStatuz(SPACES);
		covtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife("01");
		covtmjaIO.setJlife("00");
		covtmjaIO.setCoverage("01");
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(9999);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		while ( !(isEQ(covtmjaIO.getStatuz(),varcom.endp))) {
			processCovt2600();
		}

	}

protected void processCovt2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					callCovt2600();
				case deleteCovt2650:
					deleteCovt2650();
				case nextCovt2670:
					nextCovt2670();
				case exit2690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callCovt2600()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covtmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
		wsaaStoreCovtKey.set(covtmjaIO.getDataKey());
		covtmjaIO.setFunction(varcom.nextr);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(covtmjaIO.getLife());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		covrmjaIO.setCoverage(covtmjaIO.getCoverage());
		covrmjaIO.setRider(covtmjaIO.getRider());
		covrmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.exit2690);
		}
		/* PERFORM 2800-BRKOUT-UNTL-REC.                                */
		brkoutUnltRec2800();
		a400ReadPcdtRec();
		wsaaSaveSumins.set(covtmjaIO.getSumins());
		wsaaSaveSingp.set(covtmjaIO.getSingp());
		wsaaSaveInstprem.set(covtmjaIO.getInstprem());
		wsaaSaveZbinstprem.set(covtmjaIO.getZbinstprem());
		wsaaSaveZlinstprem.set(covtmjaIO.getZlinstprem());
		if (isNE(wsaaSaveSumins, 0)) {
			compute(wsaaCovtSumins, 3).setRounded(div(wsaaSaveSumins, chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtSumins.set(0);
		}
		if (isNE(wsaaSaveSingp, 0)) {
			compute(wsaaCovtSingp, 3).setRounded(div(wsaaSaveSingp, chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtSingp.set(0);
		}
		if (isNE(wsaaSaveInstprem, 0)) {
			compute(wsaaCovtInstprem, 3).setRounded(div(wsaaSaveInstprem, chdrmjaIO.getPolinc()));
		}
		else {
			/*    MOVE +0 TO WSAA-COVT-SINGP.                              */
			wsaaCovtInstprem.set(0);
		}
		if (isNE(wsaaSaveZbinstprem, 0)) {
			compute(wsaaCovtZbinstprem, 3).setRounded(div(wsaaSaveZbinstprem, chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtZbinstprem.set(0);
		}
		if (isNE(wsaaSaveZlinstprem, 0)) {
			compute(wsaaCovtZlinstprem, 3).setRounded(div(wsaaSaveZlinstprem, chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtZlinstprem.set(0);
		}
		wsaaCovtSuffix.set(chdrmjaIO.getPolsum());
		while ( !(isEQ(chdrmjaIO.getPolinc(), wsaaCovtSuffix)
		|| isLT(chdrmjaIO.getPolinc(), wsaaCovtSuffix))) {
			addCovtPcdt2700();
		}

		covtmjaIO.setDataKey(wsaaStoreCovtKey);
		covtmjaIO.setFunction(varcom.readh);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		/* if you have a contract with a polsum of 0 ,*/
		/* proccess 2700-add-covt-pcdt has created all the broke out*/
		/* policies , since you do not need a summarised record*/
		/* delete the covt record with a plan-suffix of '0'.*/
		if (isEQ(chdrmjaIO.getPolsum(), ZERO)) {
			goTo(GotoLabel.deleteCovt2650);
		}
		zrdecplrec.amountIn.set(wsaaSaveSumins);
		c000CallRounding();
		wsaaSaveSumins.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaSaveSingp);
		c000CallRounding();
		wsaaSaveSingp.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaSaveInstprem);
		c000CallRounding();
		wsaaSaveInstprem.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaSaveZbinstprem);
		c000CallRounding();
		wsaaSaveZbinstprem.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaSaveZlinstprem);
		c000CallRounding();
		wsaaSaveZlinstprem.set(zrdecplrec.amountOut);
		covtmjaIO.setSumins(wsaaSaveSumins);
		covtmjaIO.setSingp(wsaaSaveSingp);
		covtmjaIO.setInstprem(wsaaSaveInstprem);
		covtmjaIO.setZbinstprem(wsaaSaveZbinstprem);
		covtmjaIO.setZlinstprem(wsaaSaveZlinstprem);
		covtmjaIO.setFunction(varcom.rewrt);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		goTo(GotoLabel.nextCovt2670);
	}

protected void deleteCovt2650()
	{
		covtmjaIO.setFunction(varcom.delet);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void nextCovt2670()
	{
		covtmjaIO.setFunction(varcom.nextr);
	}

protected void addCovtPcdt2700()
	{
		start2700();
	}

protected void start2700()
	{
		wsaaCovtSuffix.add(1);
		covtmjaIO.setPlanSuffix(wsaaCovtSuffix);
		covtmjaIO.setSeqnbr(wsaaCovtSuffix);
		covtmjaIO.setSumins(wsaaCovtSumins);
		covtmjaIO.setSingp(wsaaCovtSingp);
		covtmjaIO.setInstprem(wsaaCovtInstprem);
		covtmjaIO.setZbinstprem(wsaaCovtZbinstprem);
		covtmjaIO.setZlinstprem(wsaaCovtZlinstprem);
		compute(wsaaSaveSumins, 2).set(sub(wsaaSaveSumins, wsaaCovtSumins));
		compute(wsaaSaveSingp, 2).set(sub(wsaaSaveSingp, wsaaCovtSingp));
		compute(wsaaSaveInstprem, 2).set(sub(wsaaSaveInstprem, wsaaCovtInstprem));
		compute(wsaaSaveZbinstprem, 2).set(sub(wsaaSaveZbinstprem, wsaaCovtZbinstprem));
		compute(wsaaSaveZlinstprem, 2).set(sub(wsaaSaveZlinstprem, wsaaCovtZlinstprem));
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		covtmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		a500AddPcdtRec();
	}

	/**
	* <pre>
	*2800-BRKOUT-UNTL-REC SECTION.
	* </pre>
	*/
protected void brkoutUnltRec2800()
	{
		start2800();
	}

protected void start2800()
	{
		unltmjaIO.setDataArea(SPACES);
		unltmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		unltmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		unltmjaIO.setLife(covtmjaIO.getLife());
		unltmjaIO.setCoverage(covtmjaIO.getCoverage());
		unltmjaIO.setRider(covtmjaIO.getRider());
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setFunction(varcom.readr);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(), varcom.oK)) {
			if (isNE(unltmjaIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(unltmjaIO.getParams());
				syserrrec.statuz.set(unltmjaIO.getStatuz());
				xxxxFatalError();
			}
		}
		if (isEQ(unltmjaIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		wsaaSaveUalprcs.set(unltmjaIO.getUalprcs());
		if (isEQ(unltmjaIO.getPercOrAmntInd(), "A")) {
			for (sub1.set(1); !(isGT(sub1, 10)); sub1.add(1)){
				calcUalprc2850();
			}
		}
		wsaaUnltSeq.set(chdrmjaIO.getPolsum());
		while ( !(isEQ(chdrmjaIO.getPolinc(), wsaaUnltSeq)
		|| isLT(chdrmjaIO.getPolinc(), wsaaUnltSeq))) {
			addUnlt2900();
		}

		unltmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		unltmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		unltmjaIO.setLife(covtmjaIO.getLife());
		unltmjaIO.setCoverage(covtmjaIO.getCoverage());
		unltmjaIO.setRider(covtmjaIO.getRider());
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setFunction(varcom.readh);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(), varcom.oK)) {
			if (isNE(unltmjaIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(unltmjaIO.getParams());
				syserrrec.statuz.set(unltmjaIO.getStatuz());
				xxxxFatalError();
			}
		}
		unltmjaIO.setUalprcs(wsaaSaveUalprcs);
		unltmjaIO.setFunction(varcom.rewrt);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(unltmjaIO.getParams());
			syserrrec.statuz.set(unltmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void calcUalprc2850()
	{
		/*START*/
		if (isNE(wsaaSaveUalprc[sub1.toInt()], 0)) {
			compute(wsaaUalprcAmt[sub1.toInt()], 2).set(div(wsaaSaveUalprc[sub1.toInt()], chdrmjaIO.getPolinc()));
		}
		else {
			wsaaUalprcAmt[sub1.toInt()].set(0);
		}
		zrdecplrec.amountIn.set(wsaaUalprcAmt[sub1.toInt()]);
		c000CallRounding();
		wsaaUalprcAmt[sub1.toInt()].set(zrdecplrec.amountOut);
		/*EXIT*/
	}

protected void addUnlt2900()
	{
		start2900();
		writeUnlt2910();
	}

protected void start2900()
	{
		wsaaUnltSeq.add(1);
		unltmjaIO.setSeqnbr(wsaaUnltSeq);
		if (isEQ(unltmjaIO.getPercOrAmntInd(), "P")) {
			return ;
		}
		unltmjaIO.setUalprcs(wsaaUalprcAmounts);
		for (sub1.set(1); !(isGT(sub1, 10)); sub1.add(1)){
			calcSummAmt2950();
		}
	}

protected void writeUnlt2910()
	{
		unltmjaIO.setFunction(varcom.writr);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(unltmjaIO.getParams());
			syserrrec.statuz.set(unltmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void calcSummAmt2950()
	{
		/*START*/
		if (isNE(wsaaSaveUalprc[sub1.toInt()], 0)) {
			compute(wsaaSaveUalprc[sub1.toInt()], 2).set(sub(wsaaSaveUalprc[sub1.toInt()], wsaaUalprcAmt[sub1.toInt()]));
		}
		/*EXIT*/
	}

protected void coverageProcessing3000()
	{
		begnCovt3010();
	}

protected void begnCovt3010()
	{
		wsaaSinstamt.set(0);
		wsaaSuspPosting.set(ZERO);
		wsaaTotalTax.set(0);
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setStatuz(SPACES);
		covtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife("01");
		covtmjaIO.setJlife("00");
		covtmjaIO.setCoverage("01");
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(9999);
		covtmjaIO.setFunction(varcom.begnh);
		while ( !(isEQ(covtmjaIO.getStatuz(), varcom.endp))) {
			processCovt3100();
		}

		if (flexiblePremium.isTrue()
		&& compChanged.isTrue()) {
			writeFprm3100();
		}
		/* To detect for newly added WOP component and update the          */
		/* re-rate and CPI dates                                           */
		/* to it if applicable.                                            */
		a100WopDates();
	}

protected void writeFprm3100()
	{
		start3110();
	}

	/**
	* <pre>
	************************                                  <D9604>
	* </pre>
	*/
protected void start3110()
	{
		fprmIO.setDataArea(SPACES);
		fprmIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		fprmIO.setChdrnum(chdrmjaIO.getChdrnum());
		fprmIO.setPayrseqno(payrIO.getPayrseqno());
		fprmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			xxxxFatalError();
		}
		fprmIO.setCurrto(wsaaOrigPtdate);
		fprmIO.setValidflag("2");
		fprmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			xxxxFatalError();
		}
		fprmIO.setCurrfrom(wsaaOrigPtdate);
		fprmIO.setCurrto(99999999);
		setPrecision(fprmIO.getTotalBilled(), 3);
		fprmIO.setTotalBilled(add(sub(fprmIO.getTotalBilled(), wsaaOldBilled), wsaaNewBilled), true);
		setPrecision(fprmIO.getMinPrmReqd(), 3);
		fprmIO.setMinPrmReqd(add(sub(fprmIO.getMinPrmReqd(), wsaaOldOverdueMin), wsaaNewOverdueMin), true);
		fprmIO.setTranno(chdrmjaIO.getTranno());
		fprmIO.setValidflag("1");
		fprmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processCovt3100()
	{
		callCovt3110();
	}

protected void callCovt3110()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covtmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
			return ;
		}

		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.t5687.toString(),
				covtmjaIO.getEffdate().toInt(),covtmjaIO.getCrtable().toString()); 
		if (null == itempf) { //fix ILIFE-8777 defect
		//performance improvement -- Anjali
			syserrrec.params.set(tablesInner.t5687+" "+covtmjaIO.getCrtable());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			if(isDMSFlgOn) {	//ILIFE-8880
				wsaaBasicCommMeth.set(SPACES);
			} else {
				wsaaBasicCommMeth.set(t5687rec.basicCommMeth);
			}
			wsaaBascpy.set(t5687rec.bascpy);
			wsaaSrvcpy.set(t5687rec.srvcpy);
			wsaaRnwcpy.set(t5687rec.rnwcpy);
			wsaaBasscmth.set(t5687rec.basscmth);
			wsaaBasscpy.set(t5687rec.basscpy);
		}
		if (isNE(covtmjaIO.getRider(), "00")) {
			readTr6953200();
		}
		if (isEQ(covtmjaIO.getInstprem(), ZERO)) {
			if (isNE(t5687rec.zrrcombas, SPACES)) {
				wsaaCovtAnnprem.set(covtmjaIO.getZbinstprem());
			}
			else {
				wsaaCovtAnnprem.set(covtmjaIO.getSingp());
			}
		}
		else {
			if (isNE(t5687rec.zrrcombas, SPACES)) {
				compute(wsaaCovtAnnprem, 5).set(mult(covtmjaIO.getZbinstprem(), wsaaFreqFactor));
			}
			else {
				compute(wsaaCovtAnnprem, 5).set(mult(covtmjaIO.getInstprem(), wsaaFreqFactor));
			}
		}
		
		if(isNE(covtmjaIO.getCommPrem(),ZERO)){
			wsaaCovtCommPrem.set(covtmjaIO.getCommPrem());  //IBPLIFE-5237
		}
		
		covtmjaIO.setFunction(varcom.nextr);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(covtmjaIO.getLife());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		covrmjaIO.setCoverage(covtmjaIO.getCoverage());
		covrmjaIO.setRider(covtmjaIO.getRider());
		covrmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.oK)) {
			if (isEQ(covrmjaIO.getInstprem(), ZERO)) {
				if (isNE(t5687rec.zrrcombas, SPACES)) {
					wsaaCovrAnnprem.set(covrmjaIO.getZbinstprem());
				}
				else {
					wsaaCovrAnnprem.set(covrmjaIO.getSingp());
				}
			}
			else {
				if (isNE(t5687rec.zrrcombas, SPACES)) {
					compute(wsaaCovrAnnprem, 5).set(mult(covrmjaIO.getZbinstprem(), wsaaFreqFactor));
				}
				else {
					compute(wsaaCovrAnnprem, 5).set(mult(covrmjaIO.getInstprem(), wsaaFreqFactor));
				}
			}
		}
		else {
			wsaaCovrAnnprem.set(ZERO);
		}
		
		if(isGT(covrmjaIO.getCommPrem(),ZERO)) {
			wsaaCovrCommPrem.set(covrmjaIO.getCommPrem());   //IBPLIFE-5237
		}
		
		if (isEQ(covrmjaIO.getStatuz(), varcom.oK)) {
			compute(wsaaSinstamt, 2).set(sub(add(wsaaSinstamt, covtmjaIO.getInstprem()), covrmjaIO.getInstprem()));
		}
		else {
			compute(wsaaSinstamt, 2).set(add(wsaaSinstamt, covtmjaIO.getInstprem()));
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.oK)) {
			wsaaCompAction.set("C");
			storeDates4000();
			compChange4000();
		}
		else {
			wsaaCompAction.set("A");
			postProratedPrem5600();
			compAdd5000();
		}
		/* If addition or modify then commission must be adjusted*/
		if (isNE(wsaaCovrAnnprem, wsaaCovtAnnprem)) {
			/*    PERFORM 7000-COMMISSION-PROCESS.                          */
			commissionProcess7000();
			if (wsaaCompAdd.isTrue()
			&& isNE(cprpIO.getStatuz(), varcom.mrnf)) {
				commissionProcess3500();
			}
			activateReassurance8000();
		}
		a300DeletePcdtRec();
		convertUntlToUlnk20000();
		d000StampDuty();
		e000T5671CompProcess();
		deleteCovt5500();
	}

protected void readTr6953200()
	{
		read3210();
	}

protected void read3210()
	{
		covrlnbIO.setDataArea(SPACES);
		covrlnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrlnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrlnbIO.setLife(covtmjaIO.getLife());
		covrlnbIO.setJlife(covtmjaIO.getJlife());
		covrlnbIO.setCoverage(covtmjaIO.getCoverage());
		covrlnbIO.setRider("00");
		covrlnbIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		covrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)
		&& isNE(covrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			xxxxFatalError();
		}
		wsaaTr695Coverage.set(covrlnbIO.getCrtable());
		wsaaTr695Rider.set(covtmjaIO.getCrtable());

		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.tr695.toString(),covtmjaIO.getEffdate().toInt(),wsaaTr695Key.toString()); 
		if (null == itempf) { //fix ILIFE-8777 defect
			// performance improvement -- Anjali
			wsaaTr695Rider.set("****");
			itempf = itempfDAO.readItdmpf("IT", atmodrec.company.toString(), tablesInner.tr695.toString(),
					covtmjaIO.getEffdate().toInt(), wsaaTr695Key.toString());
			if (null == itempf) { //fix ILIFE-8777 defect
				// performance improvement -- Anjali
				return;
			}
		}
		tr695rec.tr695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if(isDMSFlgOn) {	//ILIFE-8880
				wsaaBasicCommMeth.set(SPACES);
			} else {
				wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
			}	
		wsaaBascpy.set(tr695rec.bascpy);
		wsaaSrvcpy.set(tr695rec.srvcpy);
		wsaaRnwcpy.set(tr695rec.rnwcpy);
		wsaaBasscmth.set(tr695rec.basscmth);
		wsaaBasscpy.set(tr695rec.basscpy);
	}

protected void updateHeader3500()
	{
		chdr3510();
	}

protected void customerSpecificcalcFeeChrges(){
		
}

protected void chdr3510()
	{
		/* If reduction then WSAA-LESS-PREM will be a negative value.*/
		setPrecision(chdrmjaIO.getSinstamt01(), 2);
		chdrmjaIO.setSinstamt01(add(chdrmjaIO.getSinstamt01(), wsaaSinstamt));
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(chdrmjaIO.getSinstamt06(), wsaaSinstamt));

		setPrecision(payrIO.getSinstamt01(), 2);
		payrIO.setSinstamt01(add(payrIO.getSinstamt01(), wsaaSinstamt));
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(payrIO.getSinstamt06(), wsaaSinstamt));
		
		customerSpecificSinstamt06();
		chdrmjaIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* New codes for Commission Accounting                          *
	* </pre>
	*/
protected void commissionProcess3500()
	{
		/*BEGN*/
		wsaaRldgacct.set(SPACES);
		List<Agcmpf> agcmpfList = agcmpfDAO.searchAgcmstRecord(covtmjaIO.getChdrcoy().toString(),covtmjaIO.getChdrnum().toString(),
				covtmjaIO.getLife().toString(), covtmjaIO.getCoverage().toString(), covtmjaIO.getRider().toString(), covtmjaIO.getPlanSuffix().toInt(),"1");
		if(agcmpfList.size() > 0) {
			for(Agcmpf agcmpfTemp : agcmpfList){
				agcmpf = agcmpfTemp;
				if(agcmpf.getPtdate() == 0){
				continue; }	
				if(agcmpf.getTranno() == chdrmjaIO.getTranno().toInt()){
					updateAgcm3540();
				}
			}
			agcmpfDAO.updateAgcmrevdRecord(this.agcmpfUpdateList);
			agcmpfUpdateList.clear();
		}	

		/*EXIT*/
	}


	protected void updateAgcm3540()
	{
		start3541();
	}

	protected void start3541()
	{
		/* Add Component - Rounding to nearest integer in CMPY001        * */
		comlinkrec.function.set("ADDC");
		wsaaBillfreq.set(payrIO.getBillfreq());
		comlinkrec.billfreq.set(payrIO.getBillfreq());
		compute(comlinkrec.instprem, 3).setRounded(div(agcmpf.getAnnprem(), wsaaBillfreqNum));
		/* Pro-rate the commission to be paid out.                       * */
		compute(comlinkrec.icommtot, 2).set(div(mult(agcmpf.getInitcom(), cprpIO.getInstprem()), agcmpf.getAnnprem()));
		comlinkrec.chdrnum.set(covtmjaIO.getChdrnum());
		comlinkrec.chdrcoy.set(covtmjaIO.getChdrcoy());
		comlinkrec.life.set(agcmpf.getLife());
		comlinkrec.agent.set(agcmpf.getAgntnum());
		comlinkrec.coverage.set(agcmpf.getCoverage());
		comlinkrec.annprem.set(agcmpf.getAnnprem());
		comlinkrec.rider.set(agcmpf.getRider());
		comlinkrec.planSuffix.set(agcmpf.getPlnsfx());
		comlinkrec.crtable.set(covtmjaIO.getCrtable());
		comlinkrec.agentClass.set(agcmpf.getAgcls());
		comlinkrec.icommpd.set(agcmpf.getCompay());
		comlinkrec.icommernd.set(agcmpf.getComern());
		comlinkrec.effdate.set(agcmpf.getEfdate());
		comlinkrec.language.set(atmodrec.language);
		comlinkrec.currto.set(0);
		comlinkrec.ptdate.set(0);
		comlinkrec.targetPrem.set(0);
		/*  Call the three types of commission routine from AGCM           */
		for (wsaaCommethNo.set(1); !(isGT(wsaaCommethNo, 3)); wsaaCommethNo.add(1)){
			setUpCommMethod3550();
		}
		/*  Write an ACMV for each CHDRNUM/AGNTNUM/COVR/COMMISSION AMOUNT  */
		postCommission3570();
		/*CPW                                                            */
		agcmpf.setPtdate(chdrmjaIO.getPtdate().toInt());
		compute(comlinkrec.instprem, 3).setRounded(div(agcmpf.getAnnprem(), wsaaBillfreqNum));
		/* Pro-rate the commission to be paid out.                       * */
		comlinkrec.icommtot.set(agcmpf.getInitcom());
		comlinkrec.chdrnum.set(covtmjaIO.getChdrnum());
		comlinkrec.chdrcoy.set(covtmjaIO.getChdrcoy());
		comlinkrec.life.set(agcmpf.getLife());
		comlinkrec.agent.set(agcmpf.getAgntnum());
		comlinkrec.coverage.set(agcmpf.getCoverage());
		comlinkrec.annprem.set(agcmpf.getAnnprem());
		comlinkrec.rider.set(agcmpf.getRider());
		comlinkrec.planSuffix.set(agcmpf.getPlnsfx());
		comlinkrec.crtable.set(covtmjaIO.getCrtable());
		comlinkrec.agentClass.set(agcmpf.getAgcls());
		comlinkrec.icommpd.set(agcmpf.getCompay());
		comlinkrec.icommernd.set(agcmpf.getComern());
		comlinkrec.effdate.set(chdrmjaIO.getOccdate());
		comlinkrec.currto.set(0);
		comlinkrec.targetPrem.set(0);
		/*    MOVE CLNK-PAYAMNT           TO AGCMRNL-COMPAY.               */
		/*    MOVE CLNK-ERNDAMT           TO AGCMRNL-COMERN.               */
		agcmpfUpdateList.add(agcmpf);	
	}

	protected void setUpCommMethod3550()
	{
		start3551();
	}

	protected void start3551()
	{
		if (isEQ(wsaaCommethNo, 1)){
			comlinkrec.method.set(agcmpf.getBascpy());
		}
		else if (isEQ(wsaaCommethNo, 2)){
			comlinkrec.method.set(agcmpf.getSrvcpy());
		}
		else if (isEQ(wsaaCommethNo, 3)){
			comlinkrec.method.set(agcmpf.getRnwcpy());
		}
		/*  Check initial commission for BASCPY method only.               */
		if (isEQ(comlinkrec.method, SPACES)) {
			return ;
		}
		if (isEQ(wsaaCommethNo, 1)) {
			if (isNE(agcmpf.getCompay(), agcmpf.getInitcom())
					|| isNE(agcmpf.getComern(), agcmpf.getInitcom())) {
				callCommRoutine3560();
			}
		}
		else {
			if (isEQ(wsaaCommethNo, 3)
					&& isEQ(chdrmjaIO.getRnwlsupr(), "Y")
					&& isGTE(payrIO.getPtdate(), chdrmjaIO.getRnwlspfrom())
					&& isLTE(payrIO.getPtdate(), chdrmjaIO.getRnwlspto())) {
				/*NEXT_SENTENCE*/
			}
			else {
				callCommRoutine3560();
			}
		}
	}

	protected void callCommRoutine3560()
	{
		start3561();
	}

	protected void start3561()
	{
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		/*  Read ZRAP to get ZRORCODE, this one is valid only for Overide  */
		/*  Agent                                                          */
		/*  Call the subroutine relating to the CLNK-METHOD                */
		readT56441700();
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		/*  Basic Commission                                               */
		if (isEQ(wsaaCommethNo, 1)) {
			if (isEQ(agcmpf.getOvrdcat(), "O")) {
				wsaaOvrdBascpyDue.set(comlinkrec.payamnt);
				wsaaOvrdBascpyErn.set(comlinkrec.erndamt);
				compute(wsaaOvrdBascpyPay, 2).set(add(wsaaOvrdBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			}
			else {
				wsaaBascpyDue.set(comlinkrec.payamnt);
				wsaaBascpyErn.set(comlinkrec.erndamt);
				compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			}
			setPrecision(agcmpf.getCompay(), 2);
			agcmpf.setCompay(add(agcmpf.getCompay(), comlinkrec.payamnt).getbigdata());
			setPrecision(agcmpf.getComern(), 2);
			agcmpf.setComern(add(agcmpf.getComern(), comlinkrec.erndamt).getbigdata());
		}
		/*  Service Commission                                             */
		if (isEQ(wsaaCommethNo, 2)) {
			wsaaSrvcpyDue.set(comlinkrec.payamnt);
			wsaaSrvcpyErn.set(comlinkrec.erndamt);
			setPrecision(agcmpf.getScmearn(), 2);
			agcmpf.setScmearn(add(agcmpf.getScmearn(), comlinkrec.erndamt).getbigdata());
			setPrecision(agcmpf.getScmdue(), 2);
			agcmpf.setScmdue(add(agcmpf.getScmdue(), comlinkrec.payamnt).getbigdata());
		}
		/*  Renewal Commission                                             */
		if (isEQ(wsaaCommethNo, 3)) {
			wsaaRnwcpyDue.set(comlinkrec.payamnt);
			wsaaRnwcpyErn.set(comlinkrec.erndamt);
			setPrecision(agcmpf.getRnlcdue(), 2);
			agcmpf.setRnlcdue(add(agcmpf.getRnlcdue(), comlinkrec.payamnt).getbigdata());
			setPrecision(agcmpf.getRnlcearn(), 2);
			agcmpf.setRnlcearn(add(agcmpf.getRnlcearn(), comlinkrec.erndamt).getbigdata());
		}
		/* Skip bonus workbench                                            */
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench *                                               */
		/* Skip overriding commission                                      */
		if (isNE(agcmpf.getOvrdcat(), "B")) {
			return ;
		}
		if (isEQ(wsaaCommethNo, "1")){
			firstPrem.setTrue();
			wsaaZctnPremium.set(wsaaRegPremFirst);
		}
		else if (isEQ(wsaaCommethNo, "3")){
			renPrem.setTrue();
			wsaaZctnPremium.set(wsaaRegPremRenewal);
		}
		else{
			return ;
		}
		if (isNE(comlinkrec.payamnt, ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmpf.getChdrcoy());
			zctnIO.setAgntnum(agcmpf.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			zctnIO.setPremium(wsaaZctnPremium, true);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmpf.getAnnprem(), 100), covrmjaIO.getInstprem()), true);
			zctnIO.setZprflg(wsaaPremType);
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(chdrmjaIO.getTranno());
			zctnIO.setTransCode(wsaaBatckey.batcBatctrcde);
			zctnIO.setEffdate(agcmpf.getEfdate());
			zctnIO.setTrandate(datcon1rec.intDate);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void postCommission3570()
	{
		start3571();
	}

protected void start3571()
	{
		/*  Initialise the ACMV area and move the contract type to its     */
		/*  relevant field (same for all calls to LIFACMV).                */
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		wsaaAgntChdrnum.set(covtmjaIO.getChdrnum());
		wsaaAgntLife.set(covtmjaIO.getLife());
		wsaaAgntCoverage.set(covtmjaIO.getCoverage());
		wsaaAgntRider.set(covtmjaIO.getRider());
		wsaaPlan.set(covtmjaIO.getPlanSuffix());
		wsaaAgntPlanSuffix.set(wsaaPlansuff);
		/*CPW                                                            */
		postInitialCommission11000();
		postServiceCommission12000();
		postRenewalCommission13000();
		postOverridCommission14000();
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
	}

	/**
	* <pre>
	* New codes for Commission Accounting                          *
	* </pre>
	*/
protected void callLifrtrn3600()
	{
		readT56453610();
		lifrtrn3620();
	}

protected void readT56453610()
	{
		/* Read T5645 for transaction accounting rules.                    */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("PR574");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("PR574");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void lifrtrn3620()
	{
		/*  Post the Single Premium Amount.                             */
		if (isNE(wsaaSuspPosting, ZERO)) {
			/*    IF WSAA-SUSP-POSTING         <=  WSAA-SUSP-AVAIL   <V74L01>*/
			if ((setPrecision(wsaaSuspAvail, 5)
			&& isLTE(add(wsaaSuspPosting, wsaaTotalTax), wsaaSuspAvail))) {
				lifrtrnrec.function.set("PSTW");
				lifrtrnrec.batckey.set(atmodrec.batchKey);
				lifrtrnrec.rdocnum.set(chdrmjaIO.getChdrnum());
				lifrtrnrec.rldgacct.set(chdrmjaIO.getChdrnum());
				lifrtrnrec.tranno.set(chdrmjaIO.getTranno());
				lifrtrnrec.jrnseq.set(0);
				lifrtrnrec.rldgcoy.set(chdrmjaIO.getChdrcoy());
				/*        MOVE CHDRMJA-CNTCURR        TO LIFR-ORIGCURR     <V4L005>*/
				lifrtrnrec.origcurr.set(chdrmjaIO.getBillcurr());
				lifrtrnrec.sacscode.set(t5645rec.sacscode01);
				lifrtrnrec.sacstyp.set(t5645rec.sacstype01);
				lifrtrnrec.glcode.set(t5645rec.glmap01);
				lifrtrnrec.glsign.set(t5645rec.sign01);
				lifrtrnrec.contot.set(t5645rec.cnttot01);
				lifrtrnrec.tranref.set(chdrmjaIO.getChdrnum());
				lifrtrnrec.trandesc.set(descIO.getLongdesc());
				lifrtrnrec.crate.set(0);
				lifrtrnrec.acctamt.set(0);
				lifrtrnrec.rcamt.set(0);
				lifrtrnrec.user.set(wsaaUser);
				lifrtrnrec.termid.set(wsaaTermid);
				lifrtrnrec.transactionDate.set(wsaaTransactionDate);
				lifrtrnrec.transactionTime.set(wsaaTransactionTime);
				lifrtrnrec.origamt.set(wsaaSuspPosting);
				lifrtrnrec.origamt.add(wsaaTotalTax);
				lifrtrnrec.genlcur.set(SPACES);
				lifrtrnrec.genlcoy.set(chdrmjaIO.getChdrcoy());
				lifrtrnrec.postyear.set(SPACES);
				lifrtrnrec.postmonth.set(SPACES);
				if (wsaaCompAdd.isTrue()) {
					lifrtrnrec.effdate.set(wsaaEffdate);
				}
				else {
					lifrtrnrec.effdate.set(chdrmjaIO.getOccdate());
				}
				lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
				lifrtrnrec.substituteCode[1].set(chdrmjaIO.getCnttype());
				callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
				if (isNE(lifrtrnrec.statuz, varcom.oK)) {
					syserrrec.params.set(lifrtrnrec.lifrtrnRec);
					syserrrec.statuz.set(lifrtrnrec.statuz);
					xxxxFatalError();
				}
				if (isNE(chdrmjaIO.getCntcurr(), chdrmjaIO.getBillcurr())) {
					currencyConvert3700();
				}
				if (isNE(t5688rec.comlvlacc, "Y")
				&& isNE(wsaaCprpInstprem, 0)) {
					premiumBooking13800();
				}
			}
			else {
				/*-- Insufficient Fund Available                                   */
				syserrrec.dbparams.set("Insufficient Funds");
				xxxxFatalError();
			}
		}
	}

protected void currencyConvert3700()
	{
		/*BEGN*/
		lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
		lifacmvrec.tranref.set(chdrmjaIO.getChdrnum());
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.origamt.set(wsaaSuspPosting);
		wsaaGlSub.set(2);
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		callLifacmv6000();
		wsaaGlSub.set(15);
		lifacmvrec.origcurr.set(chdrmjaIO.getBillcurr());
		callLifacmv6000();
		/*EXIT*/
	}

	/**
	* <pre>
	* PREMIUM BOOKING                                              *
	* 3800-Premium-Booking1   - For Contract  Level Booking        *
	* 3850-Premium-Booking2   - For Component Level Booking        *
	* </pre>
	*/
protected void premiumBooking13800()
	{
		/*BEGN1*/
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaSuspPosting);
		wsaaGlSub.set(3);
		callLifacmv6000();
		/*EXIT1*/
	}

protected void premiumBooking23800()
	{
		begn23810();
	}

protected void begn23810()
	{
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaCprpInstprem);
		wsaaGlSub.set(22);
		lifrtrnrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec.substituteCode[6].set(covtmjaIO.getCrtable());
		/*RLDGACCT                                                         */
		wsaaRldgChdrnum.set(covtmjaIO.getChdrnum());
		wsaaRldgLife.set(covtmjaIO.getLife());
		wsaaRldgCoverage.set(covtmjaIO.getCoverage());
		wsaaRldgRider.set(covtmjaIO.getRider());
		wsaaPlan.set(covtmjaIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.effdate.set(wsaaEffdate);
		callLifacmv6000();
	}

protected void writeLetter3700()
	{
		try {
			readT66343710();
			setUpParm3730();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readT66343710()
	{
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/*     PERFORM  3800-READ-T6634-AGAIN                   <PCPPRT>*/
			readTr384Again3800();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		/* IF   T6634-LETTER-TYPE          = SPACE              <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			goTo(GotoLabel.exit3790);
		}
	}

protected void setUpParm3730()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		/* MOVE 'EN'                   TO LETRQST-RDOCPFX.      <PCPPRT>*/
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(atmodrec.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.      <PCPPRT>*/
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		/*   MOVE SPACE                  TO WSAA-OKEYS.                   */
		/* IF   WSAA-COMP-ADD                                   <PCPPRT>*/
		/*      MOVE ATMD-LANGUAGE     TO LETRQST-OTHER-KEYS    <PCPPRT>*/
		/* ELSE                                                 <PCPPRT>*/
		wsaaOkey1.set(atmodrec.language);
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		/* CALL 'TLETRQS' USING LETRQST-PARAMS.                   <P008>*/
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	*3800-READ-T6634-AGAIN SECTION.
	* </pre>
	*/
protected void readTr384Again3800()
	{
		start3810();
	}

protected void start3810()
	{
		wsaaItemCnttype.set("***");
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
	}

protected void storeDates4000()
	{
		/*STORE-DATES-PARA*/
		wsaaUnitstmtDate.set(varcom.vrcmMaxDate);
		wsaaRerateDate.set(varcom.vrcmMaxDate);
		wsaaAnivprocDate.set(varcom.vrcmMaxDate);
		wsaaIucancDate.set(varcom.vrcmMaxDate);
		wsaaBenbillDate.set(varcom.vrcmMaxDate);
		wsaaBonusInd.set(SPACES);
		wsaaBonusInd.set(covrmjaIO.getBonusInd());
		/*MOVE COVRMJA-BEN-CESS-DATE       TO WSAA-BENCESS-DATE.       */
		wsaaBenbillDate.set(covrmjaIO.getBenBillDate());
		wsaaAnivprocDate.set(covrmjaIO.getAnnivProcDate());
		wsaaRerateDate.set(covrmjaIO.getRerateDate());
		wsaaUnitstmtDate.set(covrmjaIO.getUnitStatementDate());
		wsaaIucancDate.set(covrmjaIO.getInitUnitCancDate());
		/*STORE-DATES-EXIT*/
	}
protected void updateExcl(){
	List<Exclpf> exclpflist = new LinkedList<Exclpf>(); 
	exclpflist=exclpfDAO.getExclpfRecord(chdrmjaIO.getChdrnum().toString(),covrmjaIO.getCrtable().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),covrmjaIO.getRider().toString()); 
	 for (int i = 0; i < exclpflist.size(); i++) 
		{
		 exclpf = exclpflist.get(i);
		 if(isEQ(exclpf.getValidflag(),"3")) { //PINNACLE-2855
			exclpf.setTranno(chdrmjaIO.getTranno().toInt());
			exclpfDAO.updateStatus(exclpf);	
		 }
			
		} 
	
}
protected void compChange4000()
	{
		t5687Table4010();
		validflag14030();
	}

protected void t5687Table4010()
	{
		/** Access the table to initialise the COVR fields.*/
		/***** Note: this table read has been moved to the 3100- section.   */
		/***** MOVE SPACES                 TO ITDM-DATA-AREA.               */
		/***** MOVE ATMD-COMPANY           TO ITDM-ITEMCOY.                 */
		/***** MOVE T5687                  TO ITDM-ITEMTABL.                */
		/***** MOVE COVTMJA-CRTABLE        TO ITDM-ITEMITEM.                */
		/***** MOVE COVTMJA-EFFDATE        TO ITDM-ITMFRM.                  */
		/***** MOVE BEGN                   TO ITDM-FUNCTION.                */
		/***** CALL 'ITDMIO' USING         ITDM-PARAMS.                     */
		/***** IF ITDM-STATUZ              NOT = O-K AND NOT = ENDP         */
		/*****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   */
		/*****    MOVE ITDM-STATUZ         TO SYSR-STATUZ                   */
		/*****    PERFORM XXXX-FATAL-ERROR.                                 */
		/***** IF ITDM-ITEMCOY             NOT = ATMD-COMPANY               */
		/***** OR ITDM-ITEMTABL            NOT = T5687                      */
		/***** OR ITDM-ITEMITEM            NOT = COVTMJA-CRTABLE            */
		/***** OR ITDM-STATUZ              = ENDP                           */
		/*****    MOVE COVTMJA-CRTABLE     TO ITDM-ITEMITEM                 */
		/*****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   */
		/*****    MOVE F294                TO SYSR-STATUZ                   */
		/*****    PERFORM XXXX-FATAL-ERROR                                  */
		/***** ELSE                                                         */
		/*****    MOVE ITDM-GENAREA        TO T5687-T5687-REC.              */
		/*VALIDFLAG-2*/
		wsaaPrevTranno.set(covrmjaIO.getTranno());
		covrmjaIO.setCurrto(covtmjaIO.getEffdate());
		covrmjaIO.setValidflag("2");
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		updateExcl();
	}

protected void validflag14030()
	{
		/* Here we need to store the SUMINS value from the old COVR        */
		/* since it is required by the subroutine ACTVRES later on to      */
		/* update company exposure. However, if the increase is on a       */
		/* proportionate basis, the company exposure should be updated     */
		/* with ACVR-OLD-SUMINS equal to zero.                             */
		tableT5448Read4100();
		if (isNE(t5448rec.proppr, SPACES)) {
			wsaaOldSumins.set(ZERO);
		}
		else {
			wsaaOldSumins.set(covrmjaIO.getSumins());
		}
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setCurrfrom(covtmjaIO.getEffdate());
		covrmjaIO.setCurrto(chdrmjaIO.getCurrto());
		if (isEQ(covtmjaIO.getJlife(), SPACES)
		|| isEQ(covtmjaIO.getJlife(), "00")) {
			covrmjaIO.setSex(covtmjaIO.getSex01());
		}
		else {
			covrmjaIO.setSex(covtmjaIO.getSex02());
		}
		covrmjaIO.setPremCurrency(payrIO.getCntcurr());
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setTransactionDate(wsaaTransactionDate);
		covrmjaIO.setTransactionTime(wsaaTransactionTime);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setPayrseqno(covtmjaIO.getPayrseqno());
		covrmjaIO.setRiskCessDate(covtmjaIO.getRiskCessDate());
		/*MOVE COVTMJA-PREM-CESS-DATE TO COVRMJA-PREM-CESS-DATE        */
		/*                               COVRMJA-BEN-CESS-DATE.        */
		covrmjaIO.setPremCessDate(covtmjaIO.getPremCessDate());
		covrmjaIO.setBenCessDate(covtmjaIO.getBenCessDate());
		covrmjaIO.setRiskCessAge(covtmjaIO.getRiskCessAge());
		covrmjaIO.setPremCessAge(covtmjaIO.getPremCessAge());
		covrmjaIO.setBenCessAge(covtmjaIO.getBenCessAge());
		covrmjaIO.setRiskCessTerm(covtmjaIO.getRiskCessTerm());
		covrmjaIO.setPremCessTerm(covtmjaIO.getPremCessTerm());
		covrmjaIO.setBenCessTerm(covtmjaIO.getBenCessTerm());
		covrmjaIO.setAnnivProcDate(ZERO);
		covrmjaIO.setBenBillDate(ZERO);
		covrmjaIO.setConvertInitialUnits(ZERO);
		covrmjaIO.setCpiDate(ZERO);
		covrmjaIO.setExtraAllocDate(ZERO);
		covrmjaIO.setInitUnitCancDate(ZERO);
		covrmjaIO.setInitUnitIncrsDate(ZERO);
		covrmjaIO.setRerateDate(ZERO);
		covrmjaIO.setRerateFromDate(ZERO);
		covrmjaIO.setReviewProcessing(ZERO);
		covrmjaIO.setUnitStatementDate(ZERO);
		/*MOVE WSAA-BENCESS-DATE      TO COVRMJA-BEN-CESS-DATE.        */
		covrmjaIO.setBenBillDate(wsaaBenbillDate);
		covrmjaIO.setUnitStatementDate(wsaaUnitstmtDate);
		covrmjaIO.setAnnivProcDate(wsaaAnivprocDate);
		covrmjaIO.setInitUnitCancDate(wsaaIucancDate);
		/* MOVE WSAA-CPI-DATE          TO COVRMJA-CPI-DATE.             */
		covrmjaIO.setCpiDate(varcom.vrcmMaxDate);
		covrmjaIO.setSumins(covtmjaIO.getSumins());
		covrmjaIO.setMortcls(covtmjaIO.getMortcls());
		covrmjaIO.setLiencd(covtmjaIO.getLiencd());
		covrmjaIO.setSingp(covtmjaIO.getSingp());
		covrmjaIO.setZbinstprem(covtmjaIO.getZbinstprem());
		covrmjaIO.setZlinstprem(covtmjaIO.getZlinstprem());
		covrmjaIO.setInstprem(covtmjaIO.getInstprem());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		covrmjaIO.setBonusInd(wsaaBonusInd);
		covrmjaIO.setBappmeth(covtmjaIO.getBappmeth());
		covrmjaIO.setTpdtype(covtmjaIO.getTpdtype());//ILIFE-7118
		computeRerateDate4060();
		/* We are now going to write our new Coverage/Rider COVR record*/
		/*  - first we write the validflag '2' for Reversals to match on*/
		/*    (if a Reversal should be requested in the future), then we*/
		/*    will write the validflag '1' COVR record.*/
		/* N.B. Validflag '2' record is no longer required.                */
		/*    MOVE '2'                    TO COVRMJA-VALIDFLAG.            */
		/*    MOVE COVRMJAREC             TO COVRMJA-FORMAT.               */
		/*    MOVE WRITR                  TO COVRMJA-FUNCTION.             */
		/*    CALL 'COVRMJAIO'            USING COVRMJA-PARAMS.            */
		/*    IF COVRMJA-STATUZ           NOT = O-K                        */
		/*       MOVE COVRMJA-PARAMS      TO SYSR-PARAMS                   */
		/*       MOVE COVRMJA-STATUZ      TO SYSR-STATUZ                   */
		/*       PERFORM XXXX-FATAL-ERROR.                                 */
		stampDutyflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if (stampDutyflag) {
			covrmjaIO.setZstpduty01(covtmjaIO.getZstpduty01());
			covrmjaIO.setZclstate(covtmjaIO.getZclstate());
		}
		covrmjaIO.setValidflag("1");
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		setPureLoadingCustomerspecific();
		if (flexiblePremium.isTrue()) {
			readFpco4200();
			calcFpcoAmounts4300();
			rewrtFpco4400();
			writeFpco4500();
		}
		cashDividendModify4600();
		/* PERFORM 5500-DELETE-COVT.                                    */
		wsaaCompChanged.set("Y");
	}

protected void readFpco4200()
	{
		start4210();
	}

	/**
	* <pre>
	***********************                                   <D9604>
	* </pre>
	*/
protected void start4210()
	{
		fpcorevIO.setDataArea(SPACES);
		fpcorevIO.setChdrcoy(covtmjaIO.getChdrcoy());
		fpcorevIO.setChdrnum(covtmjaIO.getChdrnum());
		fpcorevIO.setLife(covtmjaIO.getLife());
		fpcorevIO.setCoverage(covtmjaIO.getCoverage());
		fpcorevIO.setRider(covtmjaIO.getRider());
		fpcorevIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		fpcorevIO.setTargfrom(99999999);
		fpcorevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcorevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrmjaIO.getChdrcoy(), fpcorevIO.getChdrcoy())
		&& isNE(covrmjaIO.getChdrnum(), fpcorevIO.getChdrnum())
		&& isNE(covrmjaIO.getLife(), fpcorevIO.getLife())
		&& isNE(covrmjaIO.getCoverage(), fpcorevIO.getCoverage())
		&& isNE(covrmjaIO.getRider(), fpcorevIO.getRider())
		&& isNE(covrmjaIO.getPlanSuffix(), fpcorevIO.getPlanSuffix())) {
			syserrrec.params.set(fpcorevIO.getParams());
			fpcorevIO.setStatuz(varcom.endp);
			xxxxFatalError();
		}
	}

protected void calcFpcoAmounts4300()
	{
		/*START*/
		/*  Calculate new Target for the period.                   <D9604>*/
		compute(wsaaNewTarget, 3).setRounded(mult(covrmjaIO.getInstprem(), wsaaBillfreq9));
		/*  Calculate new Billed amount.                           <D9604>*/
		compute(wsaaNewBilledDecimal, 7).set(div(fpcorevIO.getBilledInPeriod(), fpcorevIO.getTargetPremium()));
		/*  Calculate the new billed amount                        <D9604>*/
		compute(wsaaNewBilled, 8).setRounded(mult(wsaaNewBilledDecimal, wsaaNewTarget));
		zrdecplrec.amountIn.set(wsaaNewBilled);
		c000CallRounding();
		wsaaNewBilled.set(zrdecplrec.amountOut);
		/*  Calculate the new minimum overdue amount               <D9604>*/
		compute(wsaaNewOverdueMin, 3).setRounded(mult(wsaaNewBilled, (div(fpcorevIO.getMinOverduePer(), 100))));
		zrdecplrec.amountIn.set(wsaaNewOverdueMin);
		c000CallRounding();
		wsaaNewOverdueMin.set(zrdecplrec.amountOut);
		/*EXIT*/
	}

protected void rewrtFpco4400()
	{
		start4410();
	}

	/**
	* <pre>
	************************                                  <D9604>
	* </pre>
	*/
protected void start4410()
	{
		fpcorevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			xxxxFatalError();
		}
		wsaaOldBilled.set(fpcorevIO.getBilledInPeriod());
		wsaaOldOverdueMin.set(fpcorevIO.getOverdueMin());
		fpcorevIO.setValidflag("2");
		fpcorevIO.setCurrto(wsaaEffdate);
		fpcorevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writeFpco4500()
	{
		start4510();
	}

	/**
	* <pre>
	************************                                  <D9604>
	* </pre>
	*/
protected void start4510()
	{
		fpcorevIO.setChdrcoy(covtmjaIO.getChdrcoy());
		fpcorevIO.setChdrnum(covtmjaIO.getChdrnum());
		fpcorevIO.setLife(covtmjaIO.getLife());
		fpcorevIO.setCoverage(covtmjaIO.getCoverage());
		fpcorevIO.setRider(covtmjaIO.getRider());
		fpcorevIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		fpcorevIO.setTranno(chdrmjaIO.getTranno());
		fpcorevIO.setTargfrom(wsaaEffdate);
		datcon2rec.intDate1.set(wsaaEffdate);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcorevIO.setTargto(datcon2rec.intDate2);
		fpcorevIO.setAnnivProcDate(datcon2rec.intDate2);
		fpcorevIO.setCurrfrom(wsaaEffdate);
		fpcorevIO.setEffdate(wsaaEffdate);
		fpcorevIO.setCurrto(99999999);
		fpcorevIO.setBilledInPeriod(wsaaNewBilled);
		fpcorevIO.setTargetPremium(wsaaNewTarget);
		fpcorevIO.setOverdueMin(wsaaNewOverdueMin);
		fpcorevIO.setValidflag("1");
		fpcorevIO.setActiveInd("Y");
		fpcorevIO.setAnnProcessInd("N");
		fpcorevIO.setFormat(formatsInner.fpcorevrec);
		fpcorevIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void computeRerateDate4060()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					optionalExtras4062();
				case readNextLext4064:
					readNextLext4064();
				case rerateDates4065:
					rerateDates4065();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void optionalExtras4062()
	{
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lextIO.setChdrnum(chdrmjaIO.getChdrnum());
		lextIO.setLife(covtmjaIO.getLife());
		lextIO.setCoverage(covtmjaIO.getCoverage());
		lextIO.setRider(covtmjaIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
		wsaaLextDate.set(varcom.vrcmMaxDate);
		while ( !(isEQ(lextIO.getStatuz(), varcom.endp)
		|| isNE(lextIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isNE(lextIO.getCoverage(), covtmjaIO.getCoverage())
		|| isNE(lextIO.getRider(), covtmjaIO.getRider()))) {
			optionalExtraProcessingThru4063();
		}

		goTo(GotoLabel.rerateDates4065);
	}

protected void optionalExtraProcessing4063()
	{
		if (isEQ(lextIO.getExtCessTerm(), 0)) {
			goTo(GotoLabel.readNextLext4064);
		}
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon2rec.freqFactor.set(lextIO.getExtCessTerm());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		wsaaLextExtDate.set(lextIO.getExtCessDate());
		if (isEQ(datcon2rec.intDate2, lextIO.getExtCessDate())) {
			if (isGT(covtmjaIO.getPremCessDate(), lextIO.getExtCessDate())) {
				goTo(GotoLabel.readNextLext4064);
			}
		}
		if (isLT(covtmjaIO.getPremCessDate(), lextIO.getExtCessDate())) {
			lextIO.setExtCessDate(covtmjaIO.getPremCessDate());
		}
		else {
			lextIO.setExtCessDate(datcon2rec.intDate2);
		}
		if (isNE(wsaaLextExtDate, lextIO.getExtCessDate())) {
			datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
			datcon3rec.intDate2.set(lextIO.getExtCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				xxxxFatalError();
			}
			else {
				lextIO.setExtCessTerm(datcon3rec.freqFactor);
			}
		}
		lextIO.setTermid(wsaaTermid);
		lextIO.setTransactionDate(wsaaTransactionDate);
		lextIO.setTransactionTime(wsaaTransactionTime);
		lextIO.setUser(wsaaUser);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readNextLext4064()
	{
		if (isLT(lextIO.getExtCessDate(), wsaaLextDate)) {
			wsaaLextDate.set(lextIO.getExtCessDate());
		}
		lextIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rerateDates4065()
	{
		/* MOVE CHDRMJA-OCCDATE        TO COVRMJA-RERATE-FROM-DATE.     */
		wsaaRerateDate.set(wsaaLextDate);
		if (isLT(covtmjaIO.getPremCessDate(), wsaaRerateDate)) {
			wsaaRerateDate.set(covtmjaIO.getPremCessDate());
		}
		covrmjaIO.setRerateFromDate(wsaaRerateDate);
		if (isNE(t5687rec.rtrnwfreq, 0)) {
			/*    MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               */
			/*       IF WSAA-COMP-ADD                                  <V4L005>*/
			/*          MOVE COVRMJA-CRRCD    TO DTC2-INT-DATE-1       <V4L005>*/
			/*       ELSE                                              <V4L005>*/
			/*          MOVE CHDRMJA-OCCDATE  TO DTC2-INT-DATE-1       <V4L005>*/
			/*       END-IF                                            <V4L005>*/
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t5687rec.rtrnwfreq);
			datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
			datcon2rec.intDate2.set(ZERO);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (wsaaCompAdd.isTrue()) {
				while ( !(isGT(datcon2rec.intDate2, covrmjaIO.getCrrcd()))) {
					callProgram(Datcon2.class, datcon2rec.datcon2Rec);
					if (isNE(datcon2rec.statuz, varcom.oK)) {
						syserrrec.params.set(datcon2rec.datcon2Rec);
						syserrrec.statuz.set(datcon2rec.statuz);
						xxxxFatalError();
					}
					datcon2rec.intDate1.set(datcon2rec.intDate2);
				}

			}
			else {
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					xxxxFatalError();
				}
			}
			if (isLT(datcon2rec.intDate2, wsaaRerateDate)) {
				wsaaRerateDate.set(datcon2rec.intDate2);
				calculateRerateFrom4900();
				/**                                   COVRMJA-RERATE-FROM-DATE      */
			}
		}
		/*       IF DTC2-STATUZ           NOT = O-K                        */
		/*          MOVE DTC2-DATCON2-REC TO SYSR-PARAMS                   */
		/*          MOVE DTC2-STATUZ      TO SYSR-STATUZ                   */
		/*          PERFORM XXXX-FATAL-ERROR                               */
		/*       ELSE                                                      */
		/*          IF DTC2-INT-DATE-2    < WSAA-RERATE-DATE               */
		/*             MOVE DTC2-INT-DATE-2 TO WSAA-RERATE-DATE,           */
		/*                                  COVRMJA-RERATE-FROM-DATE.      */
		covrmjaIO.setRerateDate(wsaaRerateDate);
	}

protected void optionalExtraProcessingThru4063()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					optionalExtraProcessing4063();
				case readNextLext4064:
					readNextLext4064();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void tableT5448Read4100()
	{
		t54484101();
	}

protected void t54484101()
	{

		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(covrmjaIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		itempf =itempfDAO.readItdmpf("IT",chdrmjaIO.getChdrcoy().toString(),tablesInner.t5448.toString(),covrmjaIO.getCrrcd().toInt(),wsaaT5448Item.toString()); 
		if (null == itempf) { //fix ILIFE-8777 defect
		//performance improvement -- Anjali
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString()+" "+wsaaT5448Item.toString());
			xxxxFatalError();
		}
		else {
			t5448rec.t5448Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
		}
	}

protected void cashDividendModify4600()
	{
		divMod4600();
	}

protected void divMod4600()
	{
		/* Check if HCSD record exists.                                    */
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(covtmjaIO.getChdrcoy());
		hcsdIO.setChdrnum(covtmjaIO.getChdrnum());
		hcsdIO.setLife(covtmjaIO.getLife());
		hcsdIO.setCoverage(covtmjaIO.getCoverage());
		hcsdIO.setRider(covtmjaIO.getRider());
		hcsdIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		hcsdIO.setValidflag("2");
		hcsdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
		hcsdIO.setValidflag("1");
		hcsdIO.setTranno(covrmjaIO.getTranno());
		hcsdIO.setEffdate(payrIO.getPtdate());
		hcsdIO.setZdivopt(covtmjaIO.getZdivopt());
		hcsdIO.setPayclt(covtmjaIO.getPayclt());
		hcsdIO.setPaymth(covtmjaIO.getPaymth());
		hcsdIO.setPaycurr(covtmjaIO.getPaycurr());
		hcsdIO.setFacthous(covtmjaIO.getFacthous());
		hcsdIO.setBankkey(covtmjaIO.getBankkey());
		hcsdIO.setBankacckey(covtmjaIO.getBankacckey());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void calculateRerateFrom4900()
	{
		calculate4900();
	}

protected void calculate4900()
	{
		if (isEQ(t5687rec.premGuarPeriod, 0)) {
			covrmjaIO.setRerateFromDate(wsaaRerateDate);
		}
		else {
			datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t5687rec.premGuarPeriod);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
		}
		/*  Check whether the re-rate date is within the minimum        */
		/*  guarantee period. If it is then use the rates from the      */
		/*  Contract Commencement date. If not use the rates from the   */
		/*  re-rate date.                                        ****   */
		if (isLT(wsaaRerateDate, datcon2rec.intDate2)) {
			covrmjaIO.setRerateFromDate(covrmjaIO.getCrrcd());
		}
		else {
			covrmjaIO.setRerateFromDate(wsaaRerateDate);
		}
	}

protected void compAdd5000()
	{
		t5687Table5010();
		setupCovr5020();
		skipBonusWorkbench5020();
	}

protected void t5687Table5010()
	{
		/* Access the table to initialise the COVR fields.*/

		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.t5687.toString(),
				covtmjaIO.getEffdate().toInt(),covtmjaIO.getCrtable().toString()); 
		if (null == itempf) { //fix ILIFE-8777 defect
			syserrrec.params.set(atmodrec.company.toString()+" "+ tablesInner.t5687 + covtmjaIO.getCrtable().toString());
		//performance improvement -- Anjali
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
		}
	}

protected void setupCovr5020()
	{
		covrmjaIO.setSumins(covtmjaIO.getSumins());
		covrmjaIO.setInstprem(covtmjaIO.getInstprem());
		covrmjaIO.setZbinstprem(covtmjaIO.getZbinstprem());
		covrmjaIO.setZlinstprem(covtmjaIO.getZlinstprem());
		covrmjaIO.setSingp(covtmjaIO.getSingp());
		covrmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		/* MOVE PAYR-BTDATE            TO COVRMJA-CURRFROM.             */
		covrmjaIO.setCurrto(chdrmjaIO.getCurrto());
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setTpdtype(covtmjaIO.getTpdtype());//ILIFE-7118
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			covrmjaIO.setCrrcd(covtmjaIO.getEffdate());
			covrmjaIO.setCurrfrom(covtmjaIO.getEffdate());
			wsaaSuspPosting.add(covrmjaIO.getSingp());
			covrmjaIO.setCpiDate(varcom.vrcmMaxDate);
			wsaaTaxableAmount.set(covrmjaIO.getSingp());
		}
		else {
			/*        MOVE PAYR-BTDATE        TO COVRMJA-CURRFROM      <V4L005>*/
			/*                                   COVRMJA-CRRCD         <V4L005>*/
			covrmjaIO.setCurrfrom(covtmjaIO.getEffdate());
			covrmjaIO.setCrrcd(covtmjaIO.getEffdate());
			/*        MOVE 0                  TO COVRMJA-CPI-DATE      <V4L005>*/
			covrmjaIO.setCpiDate(varcom.vrcmMaxDate);
			wsaaSuspPosting.add(wsaaCprpInstprem);
			wsaaTaxableAmount.set(wsaaCprpInstprem);
			if (isNE(cprpIO.getStatuz(), varcom.mrnf)
			&& isEQ(t5688rec.comlvlacc, "Y")
			&& isNE(wsaaSuspPosting, 0)) {
				premiumBooking23800();
			}
		}
		/* MOVE PAYR-BTDATE            TO COVRMJA-CRRCD.                */
		if (isEQ(covtmjaIO.getRider(), "00")
		|| isEQ(covtmjaIO.getRider(), SPACES)) {
			covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
		}
		else {
			covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
		}
		if (isNE(payrIO.getBillfreq(), "00")
		&& isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covtmjaIO.getRider(), SPACES)
			|| isEQ(covtmjaIO.getRider(), "00")) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
			else {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		else {
			if (isEQ(covtmjaIO.getRider(), SPACES)
			|| isEQ(covtmjaIO.getRider(), "00")) {
				covrmjaIO.setPstatcode(t5679rec.setSngpCovStat);
			}
			else {
				covrmjaIO.setPstatcode(t5679rec.setSngpCovStat);
			}
		}
		covrmjaIO.setReptcds(t5687rec.reptcds);
		covrmjaIO.setStatFund(t5687rec.statFund);
		covrmjaIO.setStatSect(t5687rec.statSect);
		covrmjaIO.setStatSubsect(t5687rec.statSubSect);
		/* Move relevent COVT fields and clear non-used numerics*/
		/* and dates.*/
		covrmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		covrmjaIO.setLife(covtmjaIO.getLife());
		covrmjaIO.setCoverage(covtmjaIO.getCoverage());
		covrmjaIO.setRider(covtmjaIO.getRider());
		covrmjaIO.setCrInstamt01(0);
		covrmjaIO.setCrInstamt02(0);
		covrmjaIO.setCrInstamt03(0);
		covrmjaIO.setCrInstamt04(0);
		covrmjaIO.setCrInstamt05(0);
		covrmjaIO.setEstMatValue01(0);
		covrmjaIO.setEstMatValue02(0);
		covrmjaIO.setEstMatDate01(0);
		covrmjaIO.setEstMatDate02(0);
		covrmjaIO.setEstMatInt01(0);
		covrmjaIO.setAnnivProcDate(0);
		covrmjaIO.setBenBillDate(0);
		covrmjaIO.setConvertInitialUnits(0);
		covrmjaIO.setExtraAllocDate(0);
		covrmjaIO.setInitUnitCancDate(0);
		covrmjaIO.setInitUnitIncrsDate(0);
		covrmjaIO.setRerateDate(0);
		covrmjaIO.setRerateFromDate(0);
		covrmjaIO.setReviewProcessing(0);
		covrmjaIO.setUnitStatementDate(0);
		covrmjaIO.setEstMatInt02(0);
		covrmjaIO.setPremCurrency(payrIO.getCntcurr());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		if (isEQ(covtmjaIO.getJlife(), SPACES)
		|| isEQ(covtmjaIO.getJlife(), "00")) {
			covrmjaIO.setAnbAtCcd(covtmjaIO.getAnbAtCcd01());
			covrmjaIO.setSex(covtmjaIO.getSex01());
		}
		else {
			covrmjaIO.setAnbAtCcd(covtmjaIO.getAnbAtCcd02());
			covrmjaIO.setSex(covtmjaIO.getSex02());
		}
		covrmjaIO.setCrtable(covtmjaIO.getCrtable());
		covrmjaIO.setRiskCessDate(covtmjaIO.getRiskCessDate());
		covrmjaIO.setPremCessDate(covtmjaIO.getPremCessDate());
		covrmjaIO.setBenCessDate(covtmjaIO.getBenCessDate());
		covrmjaIO.setNextActDate(0);
		covrmjaIO.setRiskCessAge(covtmjaIO.getRiskCessAge());
		covrmjaIO.setPremCessAge(covtmjaIO.getPremCessAge());
		covrmjaIO.setBenCessAge(covtmjaIO.getBenCessAge());
		covrmjaIO.setRiskCessTerm(covtmjaIO.getRiskCessTerm());
		covrmjaIO.setPremCessTerm(covtmjaIO.getPremCessTerm());
		covrmjaIO.setBenCessTerm(covtmjaIO.getBenCessTerm());
		covrmjaIO.setPayrseqno(covtmjaIO.getPayrseqno());
		covrmjaIO.setVarSumInsured(0);
		covrmjaIO.setMortcls(covtmjaIO.getMortcls());
		covrmjaIO.setLiencd(covtmjaIO.getLiencd());
		covrmjaIO.setBappmeth(covtmjaIO.getBappmeth());
		covrmjaIO.setDeferPerdAmt(0);
		covrmjaIO.setTotMthlyBenefit(0);
		covrmjaIO.setCoverageDebt(0);
		covrmjaIO.setStatSumins(0);
		covrmjaIO.setRtrnyrs(0);
		covrmjaIO.setPremCessAgeMth(0);
		covrmjaIO.setPremCessAgeDay(0);
		covrmjaIO.setPremCessTermMth(0);
		covrmjaIO.setPremCessTermDay(0);
		covrmjaIO.setRiskCessAgeMth(0);
		covrmjaIO.setRiskCessAgeDay(0);
		covrmjaIO.setRiskCessTermMth(0);
		covrmjaIO.setRiskCessTermDay(0);
		covrmjaIO.setTransactionDate(wsaaTransactionDate);
		covrmjaIO.setTransactionTime(wsaaTransactionTime);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setReserveUnitsInd(covtmjaIO.getReserveUnitsInd());
		covrmjaIO.setReserveUnitsDate(covtmjaIO.getReserveUnitsDate());
		if (isEQ(t5687rec.bbmeth, SPACES)) {
			/*   MOVE VRCM-MAX-DATE          TO COVRMJA-BEN-CESS-DATE      */
			covrmjaIO.setBenBillDate(ZERO);
		}
		else {
			/*   MOVE COVRMJA-PREM-CESS-DATE TO COVRMJA-BEN-CESS-DATE      */
			/*    MOVE PAYR-BTDATE            TO COVRMJA-BEN-BILL-DATE.     */
			covrmjaIO.setBenBillDate(covrmjaIO.getCrrcd());
		}
		updateExcl();
		computeRerateDate4060();
		/* If Single Premium, Curr From, Risk Commence Date & Benefit Bill */
		/* Date should be the current date (but the DD should be the same) */
		if (isEQ(chdrmjaIO.getBillfreq(), "00 ")) {
			y100SpDate();
		}
		/* Write the covr record.*/
		/* We are now going to write our new Coverage/Rider COVR record*/
		/*  - first we write the validflag '2' for Reversals to match on*/
		/*    (if a Reversal should be requested in the future), then we*/
		/*    will write the validflag '1' COVR record.*/
		/* N.B. Validflag '2' record is no longer required.                */
		/*    MOVE '2'                    TO COVRMJA-VALIDFLAG.            */
		/*    MOVE COVRMJAREC             TO COVRMJA-FORMAT.               */
		/*    MOVE WRITR                  TO COVRMJA-FUNCTION.             */
		/*    CALL 'COVRMJAIO'            USING COVRMJA-PARAMS.            */
		/*    IF COVRMJA-STATUZ           NOT = O-K                        */
		/*       MOVE COVRMJA-PARAMS      TO SYSR-PARAMS                   */
		/*       MOVE COVRMJA-STATUZ      TO SYSR-STATUZ                   */
		/*       PERFORM XXXX-FATAL-ERROR.                                 */
		/* For a Single Premium component Compute the Benefit Billing Date */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			f000BenefitBilling();
		}
		covrmjaIO.setValidflag("1");
		stampDutyflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if (stampDutyflag) {
			covrmjaIO.setZstpduty01(covtmjaIO.getZstpduty01());
			covrmjaIO.setZclstate(covtmjaIO.getZclstate());
		}
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		setPureLoadingCustomerspecific();
		b300CalcTax();
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench *                                               */
		/* Regular Premium                                                 */
		wsaaBillfq9.set(payrIO.getBillfreq());
		initialize(datcon4rec.datcon4Rec);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set("01");
		datcon4rec.intDate1.set(covrmjaIO.getCrrcd());
		wsaaCrrcdDate.set(covrmjaIO.getCrrcd());
		datcon4rec.billday.set(wsaaCrrcdDd);
		datcon4rec.billmonth.set(wsaaCrrcdMm);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon4rec.statuz);
			syserrrec.params.set(datcon4rec.datcon4Rec);
			xxxxFatalError();
		}
		wsaaRegIntmDate.set(datcon4rec.intDate2);
		if (isGT(payrIO.getPtdate(), wsaaRegIntmDate)) {
			compute(wsaaRegPremFirst, 2).set(mult(covrmjaIO.getInstprem(), wsaaBillfq9));
			compute(wsaaRegPremRenewal, 2).set(sub(wsaaCprpInstprem, wsaaRegPremFirst));
		}
		else {
			wsaaRegPremFirst.set(wsaaCprpInstprem);
			wsaaRegPremRenewal.set(ZERO);
			wsaaRegIntmDate.set(payrIO.getBtdate());
		}
		/* Regular Premium - First Year                                    */
		if (isNE(wsaaRegPremFirst, 0)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(covrmjaIO.getChdrcoy());
			zptnIO.setChdrnum(covrmjaIO.getChdrnum());
			zptnIO.setLife(covrmjaIO.getLife());
			zptnIO.setCoverage(covrmjaIO.getCoverage());
			zptnIO.setRider(covrmjaIO.getRider());
			zptnIO.setTranno(covrmjaIO.getTranno());
			zptnIO.setOrigamt(wsaaRegPremFirst);
			zptnIO.setTransCode(wsaaBatckey.batcBatctrcde);
			zptnIO.setEffdate(covrmjaIO.getCrrcd());
			zptnIO.setBillcd(covrmjaIO.getCrrcd());
			zptnIO.setInstfrom(covrmjaIO.getCrrcd());
			zptnIO.setInstto(wsaaRegIntmDate);
			zptnIO.setTrandate(datcon1rec.intDate);
			zptnIO.setZprflg("I");
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				xxxxFatalError();
			}
		}
		/* Regular Premium - Renewal Year                                  */
		if (isNE(wsaaRegPremRenewal, 0)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(covrmjaIO.getChdrcoy());
			zptnIO.setChdrnum(covrmjaIO.getChdrnum());
			zptnIO.setLife(covrmjaIO.getLife());
			zptnIO.setCoverage(covrmjaIO.getCoverage());
			zptnIO.setRider(covrmjaIO.getRider());
			zptnIO.setTranno(covrmjaIO.getTranno());
			zptnIO.setOrigamt(wsaaRegPremRenewal);
			zptnIO.setTransCode(wsaaBatckey.batcBatctrcde);
			zptnIO.setBillcd(covrmjaIO.getCrrcd());
			zptnIO.setEffdate(wsaaRegIntmDate);
			zptnIO.setInstfrom(wsaaRegIntmDate);
			zptnIO.setInstto(payrIO.getBtdate());
			zptnIO.setTrandate(datcon1rec.intDate);
			zptnIO.setZprflg("R");
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void skipBonusWorkbench5020()
	{
		/* For a Single Premium component After each COVR is written       */
		/* do benefit billing. Call the benefit billing routine.           */
		if (isEQ(t5687rec.singlePremInd, "Y")
		&& isNE(t5534rec.subprog, SPACES)) {
			
			/*IVE-795 RUL Product - Benefit Billing Calculation started*/
			//callProgram(t5534rec.subprog, ubblallpar.ubblallRec);
			//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5534rec.subprog.toString()) && er.isExternalized(ubblallpar.cnttype.toString(), ubblallpar.crtable.toString())))
			{
				callProgramX(t5534rec.subprog, ubblallpar.ubblallRec); //ILIFE-8320
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxubblrec vpxubblrec = new Vpxubblrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);		
				//ubblallpar.sumins.format(ubblallpar.sumins.toString());
				vpxubblrec.function.set("INIT");
				callProgram(Vpxubbl.class, vpmcalcrec.vpmcalcRec,vpxubblrec);	
				ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
				vpmfmtrec.initialize();
				vpmfmtrec.date01.set(ubblallpar.effdate);
				
				//callProgram(t5534rec.subprog, ubblallpar.ubblallRec);
				callProgram(t5534rec.subprog, vpmfmtrec,ubblallpar.ubblallRec,vpxubblrec);
				
				callProgram(Vpuubbl.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				
				if(isEQ(ubblallpar.statuz,SPACES))
					ubblallpar.statuz.set(Varcom.oK);
			}
			
			/*IVE-795 RUL Product - Benefit Billing Calculation end*/
			t5534rec.t5534Rec.set(SPACES);
			if (isNE(ubblallpar.statuz, varcom.oK)) {
				syserrrec.params.set(ubblallpar.ubblallRec);
				syserrrec.statuz.set(ubblallpar.statuz);
				xxxxFatalError();
			}
		}
		if (flexiblePremium.isTrue()) {
			writeNewFpco5300();
		}
		/*EXIT*/
	}

protected void cashDividendAdd5100()
	{
		divAdd5100();
	}

protected void divAdd5100()
	{

		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.t6640.toString(),covrmjaIO.getCrrcd().toInt(),covtmjaIO.getCrtable().toString()); 
		if (null == itempf) { //fix ILIFE-8777 defect
		//performance improvement -- Anjali
			/*     MOVE COVRMJA-CRTABLE    TO SYSR-PARAMS                   */
			/*     MOVE G588               TO SYSR-STATUZ                   */
			/*     PERFORM XXXX-FATAL-ERROR                                 */
			t6640rec.t6640Rec.set(SPACES);
			return ;
		}
		else {
			t6640rec.t6640Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
		}
		if (isNE(t6640rec.zbondivalc, SPACES)) {
			readTh5025200();
		}
		else {
			th502rec.th502Rec.set(SPACES);
			return ;
		}
		//if (isEQ(th502rec.zrevbonalc, "X")) { 
		if (isEQ(th502rec.zrevbonalc, "Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
			if (isEQ(t6640rec.revBonusMeth, SPACES)) {
				syserrrec.statuz.set(g586);
				xxxxFatalError();
			}
		}
		if (isNE(th502rec.zcshdivalc, "X")) {
			return ;
		}
		if (isEQ(t6640rec.zcshdivmth, SPACES)) {
			syserrrec.statuz.set(g586);
			xxxxFatalError();
		}
		/*  Write a HCSD record if cash dividend.                          */
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(atmodrec.company);
		hcsdIO.setChdrnum(covtmjaIO.getChdrnum());
		hcsdIO.setLife(covtmjaIO.getLife());
		hcsdIO.setCoverage(covtmjaIO.getCoverage());
		hcsdIO.setRider(covtmjaIO.getRider());
		hcsdIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		hcsdIO.setValidflag("1");
		hcsdIO.setTranno(covrmjaIO.getTranno());
		hcsdIO.setEffdate(covrmjaIO.getCrrcd());
		hcsdIO.setZdivopt(covtmjaIO.getZdivopt());
		hcsdIO.setZcshdivmth(t6640rec.zcshdivmth);
		hcsdIO.setPaycoy(covtmjaIO.getPaycoy());
		hcsdIO.setPayclt(covtmjaIO.getPayclt());
		hcsdIO.setPaymth(covtmjaIO.getPaymth());
		hcsdIO.setFacthous(covtmjaIO.getFacthous());
		hcsdIO.setBankkey(covtmjaIO.getBankkey());
		hcsdIO.setBankacckey(covtmjaIO.getBankacckey());
		hcsdIO.setPaycurr(covtmjaIO.getPaycurr());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readTh5025200()
	{
		start5200();
	}

protected void start5200()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.th502);
		itemIO.setItemitem(t6640rec.zbondivalc);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/*        MOVE HL20               TO SYSR-STATUZ                   */
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		th502rec.th502Rec.set(itemIO.getGenarea());
		if (isEQ(th502rec.zrevbonalc, SPACES)
		&& isEQ(th502rec.zcshdivalc, SPACES)) {
			/*        MOVE HL20               TO SYSR-STATUZ                   */
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
	}

protected void writeNewFpco5300()
	{
		start5310();
	}

	/**
	* <pre>
	****************************                              <D9604>
	* </pre>
	*/
protected void start5310()
	{
		fpcorevIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		fpcorevIO.setChdrnum(chdrmjaIO.getChdrnum());
		fpcorevIO.setLife(covrmjaIO.getLife());
		fpcorevIO.setJlife(covrmjaIO.getJlife());
		fpcorevIO.setCoverage(covrmjaIO.getCoverage());
		fpcorevIO.setRider(covrmjaIO.getRider());
		fpcorevIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		fpcorevIO.setValidflag("1");
		fpcorevIO.setTargfrom(payrIO.getBtdate());
		/* Calculate CURRTO as BTDATE  + 1 year                    <D9604> */
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(payrIO.getBtdate());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			xxxxFatalError();
		}
		fpcorevIO.setTargto(datcon2rec.intDate2);
		fpcorevIO.setAnnivProcDate(datcon2rec.intDate2);
		fpcorevIO.setCurrfrom(wsaaToday);
		fpcorevIO.setEffdate(wsaaToday);
		fpcorevIO.setCurrto(varcom.vrcmMaxDate);
		fpcorevIO.setActiveInd("Y");
		fpcorevIO.setAnnProcessInd("N");
		setPrecision(fpcorevIO.getTargetPremium(), 2);
		fpcorevIO.setTargetPremium(mult(covrmjaIO.getInstprem(), wsaaBillfreq9));
		fpcorevIO.setPremRecPer(ZERO);
		fpcorevIO.setBilledInPeriod(ZERO);
		fpcorevIO.setOverdueMin(ZERO);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 6)
		|| isEQ(t5729rec.frqcy[wsaaT5729Sub.toInt()], wsaaBillfreqX)); wsaaT5729Sub.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT5729Sub, 1)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMina01);
		}
		else if (isEQ(wsaaT5729Sub, 2)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMinb01);
		}
		else if (isEQ(wsaaT5729Sub, 3)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMinc01);
		}
		else if (isEQ(wsaaT5729Sub, 4)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMind01);
		}
		else if (isEQ(wsaaT5729Sub, 5)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMine01);
		}
		else if (isEQ(wsaaT5729Sub, 6)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMinf01);
		}
		else{
			syserrrec.params.set(tablesInner.t5729);
			xxxxFatalError();
		}
		fpcorevIO.setTranno(chdrmjaIO.getTranno());
		fpcorevIO.setFormat(formatsInner.fpcorevrec);
		fpcorevIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void deleteCovt5500()
	{
		/*COVT*/
		covtmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		covtmjaIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void postProratedPrem5600()
	{
		begn5610();
	}

	/**
	* <pre>
	*  Only for Add Component                                        *
	*  1. Call CPRP         -  prorated prem  & update               *
	*  2. Call LIFACMV      -  update conversion of currency         *
	*  3. Call                                                       *
	* </pre>
	*/
protected void begn5610()
	{
		wsaaCovtKey.set(covtmjaIO.getDataKey());
		wsaaCprpInstprem.set(ZERO);
		initialize(formatsInner.cprprec);
		cprpIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		cprpIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		cprpIO.setChdrnum(chdrmjaIO.getChdrnum());
		cprpIO.setLife(wsaaCovtLife);
		cprpIO.setCoverage(wsaaCovtCoverage);
		cprpIO.setRider(wsaaCovtRider);
		compute(wsaaTranno, 0).set(sub(chdrmjaIO.getTranno(), 1));
		cprpIO.setTranno(wsaaTranno);
		cprpIO.setFunction("READH ");
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)
		&& isNE(cprpIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cprpIO.getParams());
			syserrrec.statuz.set(cprpIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(cprpIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		cprpIO.setFunction("REWRT");
		cprpIO.setValidflag("2");
		cprpIO.setFormat(formatsInner.cprprec);
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cprpIO.getParams());
			syserrrec.statuz.set(cprpIO.getStatuz());
			xxxxFatalError();
		}
		wsaaCprpInstprem.set(cprpIO.getInstprem());
	}

protected void callLifacmv6000()
	{
		start6010();
	}

protected void start6010()
	{
		/* This is the only place LIFACMV parameters are referenced.  LIFA */
		/* fields that are changeable are set outside this section within  */
		/* a working storagwe variable.                                    */
		lifacmvrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec.tranno.set(chdrmjaIO.getTranno());
		/*  If the ACMV is being created for the COVT-INSTPREM, the        */
		/*  seventh table entries will be used.  Move the appropriate      */
		/*  value for the COVT posting.                                    */
		lifacmvrec.effdate.set(wsaaEffdate);
		/* MOVE VRCM-TERMID                TO LIFA-TERMID.      <V4L005>*/
		/* MOVE 0                          TO LIFA-USER.        <V4L005>*/
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.termid.set(varcom.vrcmCompTermid);
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		/* Nothing is moved to WSAA-TRANDESC.                           */
		/* MOVE WSAA-TRANDESC              TO LIFA-TRANDESC.    <V4L005>*/
		/*    MOVE WSKY-BATC-BATCBRN          TO LIFA-BATCBRN.             */
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaGlSub.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaGlSub.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaGlSub.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaGlSub.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[wsaaGlSub.toInt()]);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
		wsaaRldgacct.set(SPACES);
	}

protected void commissionProcess7000()
	{
		start7010();
	}

protected void start7010()
	{
		/* Call subroutine to re-validate all Commission records*/
		/* First, set up linkage*/
		acomcalrec.acomcalRec.set(SPACES);
		acomcalrec.statuz.set(varcom.oK);
		acomcalrec.company.set(chdrmjaIO.getChdrcoy());
		acomcalrec.chdrnum.set(chdrmjaIO.getChdrnum());
		acomcalrec.covrLife.set(covrmjaIO.getLife());
		acomcalrec.covrCoverage.set(covrmjaIO.getCoverage());
		acomcalrec.covrRider.set(covrmjaIO.getRider());
		acomcalrec.covrPlanSuffix.set(covrmjaIO.getPlanSuffix());
		acomcalrec.covtLife.set(covtmjaIO.getLife());
		acomcalrec.covtCoverage.set(covtmjaIO.getCoverage());
		acomcalrec.covtRider.set(covtmjaIO.getRider());
		acomcalrec.covtPlanSuffix.set(covtmjaIO.getPlanSuffix());
		acomcalrec.tranno.set(chdrmjaIO.getTranno());
		acomcalrec.transactionDate.set(wsaaTransactionDate);
		acomcalrec.transactionTime.set(wsaaTransactionTime);
		acomcalrec.user.set(wsaaUser);
		acomcalrec.termid.set(wsaaTermid);
		/*    MOVE PAYR-BTDATE            TO ACOM-PAYR-BTDATE.             */
		/*    MOVE PAYR-PTDATE            TO ACOM-PAYR-PTDATE.             */
		if (wsaaCompAdd.isTrue()) {
			acomcalrec.payrBtdate.set(payrIO.getBtdate());
			acomcalrec.payrPtdate.set(wsaaEffdate);
		}
		else {
			acomcalrec.payrBtdate.set(payrIO.getBtdate());
			acomcalrec.payrPtdate.set(payrIO.getPtdate());
		}
		/* MOVE PAYR-BILLFREQ          TO ACOM-PAYR-BILLFREQ.           */
		acomcalrec.payrCntcurr.set(payrIO.getCntcurr());
		acomcalrec.cnttype.set(chdrmjaIO.getCnttype());
		acomcalrec.chdrOccdate.set(chdrmjaIO.getOccdate());
		/* MOVE CHDRMJA-CURRFROM       TO ACOM-CHDR-CURRFROM.           */
		acomcalrec.chdrAgntcoy.set(chdrmjaIO.getAgntcoy());
		acomcalrec.singlePremInd.set(t5687rec.singlePremInd);
		acomcalrec.covrJlife.set(covrmjaIO.getJlife());
		acomcalrec.covrCrtable.set(covrmjaIO.getCrtable());
		acomcalrec.comlvlacc.set(t5688rec.comlvlacc);
		/* MOVE T5687-BASIC-COMM-METH  TO ACOM-BASIC-COMM-METH.         */
		/* MOVE T5687-BASCPY           TO ACOM-BASCPY.                  */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			acomcalrec.bascpy.set(wsaaBasscpy);
			acomcalrec.basicCommMeth.set(wsaaBasscmth);
			/*     MOVE T5687-BASSCPY      TO ACOM-BASCPY           <V42004>*/
			/*     MOVE T5687-BASSCMTH     TO ACOM-BASIC-COMM-METH  <V42004>*/
			acomcalrec.payrBillfreq.set("00");
			acomcalrec.chdrCurrfrom.set(covrmjaIO.getCurrfrom());
		}
		else {
			acomcalrec.basicCommMeth.set(wsaaBasicCommMeth);
			acomcalrec.bascpy.set(wsaaBascpy);
			/*     MOVE T5687-BASIC-COMM-METH TO ACOM-BASIC-COMM-METH<V42004*/
			/*     MOVE T5687-BASCPY          TO ACOM-BASCPY        <V42004>*/
			acomcalrec.payrBillfreq.set(payrIO.getBillfreq());
			acomcalrec.chdrCurrfrom.set(chdrmjaIO.getCurrfrom());
		}
		if (flexiblePremium.isTrue()) {
			acomcalrec.fpcoTargPrem.set(fpcorevIO.getTargetPremium());
			acomcalrec.fpcoCurrto.set(fpcorevIO.getTargto());
		}
		else {
			acomcalrec.fpcoTargPrem.set(0);
			acomcalrec.fpcoCurrto.set(0);
		}
		acomcalrec.rnwcpy.set(wsaaRnwcpy);
		acomcalrec.srvcpy.set(wsaaSrvcpy);
		/* MOVE T5687-RNWCPY           TO ACOM-RNWCPY.                  */
		/* MOVE T5687-SRVCPY           TO ACOM-SRVCPY.                  */
		acomcalrec.covtAnnprem.set(wsaaCovtAnnprem);
		acomcalrec.covrAnnprem.set(wsaaCovrAnnprem);
		/*IBPLIFE-5237 starts */
		acomcalrec.covrCommPrem.set(wsaaCovrCommPrem);
		acomcalrec.covtCommPrem.set(wsaaCovtCommPrem);
		/*IBPLIFE-5237 ends */
		acomcalrec.atmdLanguage.set(atmodrec.language);
		acomcalrec.atmdBatchKey.set(atmodrec.batchKey);
		acomcalrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Acomcalc.class, acomcalrec.acomcalRec);
		if (isNE(acomcalrec.statuz, varcom.oK)) {
			syserrrec.params.set(acomcalrec.acomcalRec);
			syserrrec.statuz.set(acomcalrec.statuz);
			xxxxFatalError();
		}
	}

protected void activateReassurance8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start8001();
				case break8500:
					break8500();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start8001()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrmjaIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)
		&& isNE(lifeenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			xxxxFatalError();
		}
		wsaaL1Clntnum.set(lifeenqIO.getLifcnum());
		if (isNE(covrmjaIO.getJlife(), "01")) {
			goTo(GotoLabel.break8500);
		}
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrmjaIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)
		&& isNE(lifeenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			wsaaL2Clntnum.set(lifeenqIO.getLifcnum());
		}
		else {
			wsaaL2Clntnum.set(SPACES);
		}
	}

protected void break8500()
	{
		/* Call to ACTVRES Subroutine.                                  */
		actvresrec.actvresRec.set(SPACES);
		actvresrec.function.set("ACT8");
		actvresrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(chdrmjaIO.getChdrnum());
		actvresrec.life.set(covrmjaIO.getLife());
		actvresrec.coverage.set(covrmjaIO.getCoverage());
		actvresrec.rider.set(covrmjaIO.getRider());
		actvresrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		actvresrec.crtable.set(covrmjaIO.getCrtable());
		actvresrec.clntcoy.set(wsaaFsuCoy);
		actvresrec.l1Clntnum.set(wsaaL1Clntnum);
		actvresrec.l2Clntnum.set(wsaaL2Clntnum);
		actvresrec.jlife.set(covrmjaIO.getJlife());
		actvresrec.currency.set(chdrmjaIO.getCntcurr());
		if(adjuReasrFlag) {
			actvresrec.oldSumins.set(ZERO);
		}else {
			actvresrec.oldSumins.set(wsaaOldSumins);
		}
		actvresrec.newSumins.set(covtmjaIO.getSumins());
		actvresrec.effdate.set(covrmjaIO.getCrrcd());
		actvresrec.cnttype.set(chdrmjaIO.getCnttype());
		actvresrec.tranno.set(chdrmjaIO.getTranno());
		actvresrec.crrcd.set(covrmjaIO.getCrrcd());
		actvresrec.language.set(atmodrec.language);
		actvresrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			xxxxFatalError();
		}
	}

protected void postInitialCommission11000()
	{
		start11010();
	}

protected void start11010()
	{
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		/*  Initial commission due                                         */
		if (isNE(wsaaBascpyDue, 0)) {
			/*    ADD +1                    TO LIFA-JRNSEQ                  */
			wsaaGlSub.set(8);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaBascpyDue);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				lifacmvrec.tranref.set(wsaaRldgagnt);
			}
			else {
				lifacmvrec.tranref.set(chdrmjaIO.getChdrnum());
			}
			callLifacmv6000();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmpf.getAnnprem());
				b100CallZorcompy();
			}
		}
		/*  Initial commission earned                                      */
		if (isNE(wsaaBascpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaBascpyErn);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(23);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covtmjaIO.getCrtable());
			}
			else {
				wsaaGlSub.set(9);
				lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv6000();
		}
		/*  Initial commission paid                                        */
		if (isNE(wsaaBascpyPay, 0)) {
			lifacmvrec.origamt.set(wsaaBascpyPay);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(24);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covtmjaIO.getCrtable());
			}
			else {
				wsaaGlSub.set(10);
				lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv6000();
		}
	}

	protected void postServiceCommission12000()
	{
		start12010();
	}

	protected void start12010()
	{
		/*  Service commission due                                         */
		if (isNE(wsaaSrvcpyDue, 0)) {
			lifacmvrec.origamt.set(wsaaSrvcpyDue);
			wsaaGlSub.set(11);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			/*       MOVE CHDRMJA-CHDRNUM        TO LIFA-TRANREF               */
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				lifacmvrec.tranref.set(wsaaRldgagnt);
			}
			else {
				lifacmvrec.tranref.set(chdrmjaIO.getChdrnum());
			}
			callLifacmv6000();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmpf.getAnnprem());
				b100CallZorcompy();
			}
		}
		/*  Service commission earned                                      */
		if (isNE(wsaaSrvcpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(25);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covtmjaIO.getCrtable());
			}
			else {
				wsaaGlSub.set(12);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv6000();
		}
	}

	protected void postRenewalCommission13000()
	{
		start13010();
	}

	protected void start13010()
	{
		/*  Renewal commission due                                         */
		if (isNE(wsaaRnwcpyDue, 0)) {
			/*    ADD  +1                        TO LIFA-JRNSEQ             */
			wsaaGlSub.set(13);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaRnwcpyDue);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			/*       MOVE CHDRMJA-CHDRNUM           TO LIFA-TRANREF            */
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				lifacmvrec.tranref.set(wsaaRldgagnt);
			}
			else {
				lifacmvrec.tranref.set(chdrmjaIO.getChdrnum());
			}
			callLifacmv6000();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmpf.getAnnprem());
				b100CallZorcompy();
			}
		}
		/*  Renewal commission earned                                      */
		if (isNE(wsaaRnwcpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(26);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covtmjaIO.getCrtable());
			}
			else {
				wsaaGlSub.set(14);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv6000();
		}
	}

	protected void postOverridCommission14000()
	{
		start14010();
	}

	protected void start14010()
	{
		/*  Initial over-riding commission due                             */
		if (isNE(wsaaOvrdBascpyDue, 0)) {
			/*    ADD   1                     TO LIFA-JRNSEQ                */
			wsaaGlSub.set(16);
			lifacmvrec.origamt.set(wsaaOvrdBascpyDue);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			lifacmvrec.substituteCode[6].set(SPACES);
			/*       MOVE AGCMRNL-CEDAGENT       TO LIFA-TRANREF               */
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				lifacmvrec.tranref.set(wsaaRldgagnt);
			}
			else {
				lifacmvrec.tranref.set(chdrmjaIO.getChdrnum());
			}
			callLifacmv6000();
		}
		/*  Initial over-riding commission earned                          */
		if (isNE(wsaaOvrdBascpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(27);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covtmjaIO.getCrtable());
			}
			else {
				wsaaGlSub.set(17);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			callLifacmv6000();
		}
		/*  Initial over-riding commission paid                            */
		if (isNE(wsaaOvrdBascpyPay, 0)) {
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(28);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covtmjaIO.getCrtable());
			}
			else {
				wsaaGlSub.set(18);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			}
			callLifacmv6000();
		}
	}

protected void convertUntlToUlnk20000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para20000();
				case nextRecord20100:
					nextRecord20100();
				case read20200:
					read20200();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para20000()
	{
		unltmjaIO.setDataArea(SPACES);
		unltmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		unltmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		unltmjaIO.setLife(covtmjaIO.getLife());
		unltmjaIO.setCoverage(covtmjaIO.getCoverage());
		unltmjaIO.setRider(covtmjaIO.getRider());
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setFunction(varcom.begnh);
		goTo(GotoLabel.read20200);
	}

protected void nextRecord20100()
	{
		unltmjaIO.setFunction(varcom.nextr);
	}

protected void read20200()
	{
		unltmjaio24000();
		if ((isEQ(unltmjaIO.getStatuz(), varcom.endp))) {
			return ;
		}
		if ((isNE(unltmjaIO.getChdrcoy(), covtmjaIO.getChdrcoy()))
		|| (isNE(unltmjaIO.getChdrnum(), covtmjaIO.getChdrnum()))
		|| (isNE(unltmjaIO.getLife(), covtmjaIO.getLife()))
		|| (isNE(unltmjaIO.getCoverage(), covtmjaIO.getCoverage()))
		|| (isNE(unltmjaIO.getRider(), covtmjaIO.getRider()))
		|| (isNE(unltmjaIO.getSeqnbr(), covtmjaIO.getPlanSuffix()))) {
			return ;
		}
		convertUnltmja21000();
		/* At this point delete the last UNLTMJA record read since all*/
		/* the informations have been fully used(converted into ULNK)*/
		/* and will no longer be useful.*/
		unltmjaIO.setFunction(varcom.delet);
		unltmjaio24000();
		goTo(GotoLabel.nextRecord20100);
	}

protected void convertUnltmja21000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					convert21000();
				case addUlnk21050:
					addUlnk21050();
				case exit21090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void convert21000()
	{
		storePrc22000();
		ulnkIO.setDataKey(SPACES);
		ulnkIO.setChdrcoy(unltmjaIO.getChdrcoy());
		ulnkIO.setChdrnum(unltmjaIO.getChdrnum());
		ulnkIO.setLife(unltmjaIO.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setCoverage(unltmjaIO.getCoverage());
		ulnkIO.setRider(unltmjaIO.getRider());
		ulnkIO.setPlanSuffix(unltmjaIO.getSeqnbr());
		ulnkIO.setFunction(varcom.readh);
		ulnkIO.setFormat(formatsInner.ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)
		&& isNE(ulnkIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.addUlnk21050);
		}
		wsaaUlnkUpdate.set("N");
		for (wsaaA.set(1); !(isGT(wsaaA, 10)); wsaaA.add(1)){
			updateUlnk27000();
		}
		if (wsaaNoUlnkUpdate.isTrue()) {
			goTo(GotoLabel.exit21090);
		}
		ulnkIO.setValidflag("2");
		ulnkIO.setFunction(varcom.rewrt);
		ulnkio26000();
	}

protected void addUlnk21050()
	{
		setupUlnk25000();
		ulnkIO.setFunction(varcom.writr);
		ulnkio26000();
		wsaaSub.set(1);
		a100CheckHitd();
	}

protected void storePrc22000()
	{
		/*PARA*/
		wsaaUalprc[1].set(unltmjaIO.getUalprc(1));
		wsaaUalprc[2].set(unltmjaIO.getUalprc(2));
		wsaaUalprc[3].set(unltmjaIO.getUalprc(3));
		wsaaUalprc[4].set(unltmjaIO.getUalprc(4));
		wsaaUalprc[5].set(unltmjaIO.getUalprc(5));
		wsaaUalprc[6].set(unltmjaIO.getUalprc(6));
		wsaaUalprc[7].set(unltmjaIO.getUalprc(7));
		wsaaUalprc[8].set(unltmjaIO.getUalprc(8));
		wsaaUalprc[9].set(unltmjaIO.getUalprc(9));
		wsaaUalprc[10].set(unltmjaIO.getUalprc(10));
		/*EXIT*/
	}

protected void unltmjaio24000()
	{
		/*READ*/
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(), varcom.oK)) {
			if (isNE(unltmjaIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(unltmjaIO.getParams());
				syserrrec.statuz.set(unltmjaIO.getStatuz());
				xxxxFatalError();
			}
		}
		/*EXIT*/
	}

protected void setupUlnk25000()
	{
		setup25010();
	}

protected void setup25010()
	{
		ulnkIO.setChdrcoy(unltmjaIO.getChdrcoy());
		ulnkIO.setChdrnum(unltmjaIO.getChdrnum());
		ulnkIO.setLife(unltmjaIO.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setCoverage(unltmjaIO.getCoverage());
		ulnkIO.setRider(unltmjaIO.getRider());
		ulnkIO.setPlanSuffix(unltmjaIO.getSeqnbr());
		ulnkIO.setUalfnds(unltmjaIO.getUalfnds());
		ulnkIO.setUalprc(1, wsaaUalprc[1]);
		ulnkIO.setUalprc(2, wsaaUalprc[2]);
		ulnkIO.setUalprc(3, wsaaUalprc[3]);
		ulnkIO.setUalprc(4, wsaaUalprc[4]);
		ulnkIO.setUalprc(5, wsaaUalprc[5]);
		ulnkIO.setUalprc(6, wsaaUalprc[6]);
		ulnkIO.setUalprc(7, wsaaUalprc[7]);
		ulnkIO.setUalprc(8, wsaaUalprc[8]);
		ulnkIO.setUalprc(9, wsaaUalprc[9]);
		ulnkIO.setUalprc(10, wsaaUalprc[10]);
		ulnkIO.setUspcprs(unltmjaIO.getUspcprs());
		ulnkIO.setTranno(unltmjaIO.getTranno());
		ulnkIO.setPercOrAmntInd(unltmjaIO.getPercOrAmntInd());
		ulnkIO.setCurrto(unltmjaIO.getCurrto());
		ulnkIO.setPremTopupInd(SPACES);
		ulnkIO.setValidflag("1");
		ulnkIO.setCurrfrom(0);
	}

protected void ulnkio26000()
	{
		/*CALL*/
		ulnkIO.setFormat(formatsInner.ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(ulnkIO.getChdrcoy(), covtmjaIO.getChdrcoy()))
		|| (isNE(ulnkIO.getChdrnum(), covtmjaIO.getChdrnum()))
		|| (isNE(ulnkIO.getLife(), covtmjaIO.getLife()))
		|| (isNE(ulnkIO.getCoverage(), covtmjaIO.getCoverage()))
		|| (isNE(ulnkIO.getRider(), covtmjaIO.getRider()))
		|| (isNE(ulnkIO.getPlanSuffix(), covtmjaIO.getPlanSuffix()))) {
			ulnkIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void updateUlnk27000()
	{
		/*ULNK*/
		if (isNE(ulnkIO.getPercOrAmntInd(), unltmjaIO.getPercOrAmntInd())) {
			wsaaUlnkUpdate.set("Y");
		}
		if (isNE(ulnkIO.getUalfnd(wsaaA), unltmjaIO.getUalfnd(wsaaA))) {
			wsaaUlnkUpdate.set("Y");
		}
		if (isNE(ulnkIO.getUalprc(wsaaA), unltmjaIO.getUalprc(wsaaA))) {
			wsaaUlnkUpdate.set("Y");
		}
		if (isNE(ulnkIO.getUspcpr(wsaaA), unltmjaIO.getUspcpr(wsaaA))) {
			wsaaUlnkUpdate.set("Y");
		}
		/*EXIT*/
	}

protected void a300DeletePcdtRec()
	{
		a300Start();
	}

protected void a300Start()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setLife(covtmjaIO.getLife());
		pcdtmjaIO.setCoverage(covtmjaIO.getCoverage());
		pcdtmjaIO.setRider(covtmjaIO.getRider());
		pcdtmjaIO.setTranno(chdrmjaIO.getTranno());
		pcdtmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
		pcdtmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a400ReadPcdtRec()
	{
		a400SetupKey();
	}

protected void a400SetupKey()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setLife(covtmjaIO.getLife());
		pcdtmjaIO.setCoverage(covtmjaIO.getCoverage());
		pcdtmjaIO.setRider(covtmjaIO.getRider());
		pcdtmjaIO.setTranno(chdrmjaIO.getTranno());
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		pcdtmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a500AddPcdtRec()
	{
		/*A500-SETUP-KEY*/
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		pcdtmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*A500-EXIT*/
	}

protected void d000StampDuty()
	{
		d010Duty();
	}

protected void d010Duty()
	{
		/* If no stamp duty then exit otherwise call the stamp duty*/
		/* calculation subroutine.*/
		if (isEQ(t5687rec.stampDutyMeth, SPACES)) {
			return ;
		}
		/* Fetch relevent stamp duty subroutine.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5676);
		itemIO.setItemitem(t5687rec.stampDutyMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5676rec.t5676Rec.set(itemIO.getGenarea());
		if (isEQ(t5676rec.subprog, SPACES)) {
			return ;
		}
		stdtallrec.company.set(chdrmjaIO.getChdrcoy());
		stdtallrec.chdrnum.set(chdrmjaIO.getChdrnum());
		stdtallrec.life.set(covrmjaIO.getLife());
		stdtallrec.coverage.set(covrmjaIO.getCoverage());
		stdtallrec.rider.set(covrmjaIO.getRider());
		stdtallrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		stdtallrec.cntcurr.set(payrIO.getCntcurr());
		stdtallrec.effdate.set(covrmjaIO.getCrrcd());
		stdtallrec.stampDuty.set(0);
		callProgram(t5676rec.subprog, stdtallrec.stdt001Rec);
		zrdecplrec.amountIn.set(stdtallrec.stampDuty);
		c000CallRounding();
		stdtallrec.stampDuty.set(zrdecplrec.amountOut);
	}

protected void e000T5671CompProcess()
	{
		try {
			e010ReadT5671();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void e010ReadT5671()
	{
		/* Lookup the generic component processing programs (T5671)*/
		/* and call each non blank subroutine.*/
		wsaaCompkeyTranno.set(wsaaBatckey.batcBatctrcde);
		wsaaCompkeyCrtable.set(covrmjaIO.getCrtable());
		/* Fetch coverage/rider generic component processing progs.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItemitem(wsaaCompkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.e090Exit);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		chdrlnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		chdrlnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		chdrlnbIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			xxxxFatalError();
		}
		isuallrec.company.set(chdrmjaIO.getChdrcoy());
		isuallrec.chdrnum.set(chdrmjaIO.getChdrnum());
		isuallrec.life.set(covrmjaIO.getLife());
		isuallrec.coverage.set(covrmjaIO.getCoverage());
		isuallrec.rider.set(covrmjaIO.getRider());
		isuallrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		isuallrec.covrSingp.set(covrmjaIO.getSingp());
		isuallrec.freqFactor.set(1);
		isuallrec.batchkey.set(atmodrec.batchKey);
		isuallrec.transactionDate.set(wsaaTransactionDate);
		isuallrec.transactionTime.set(wsaaTransactionTime);
		isuallrec.user.set(wsaaUser);
		isuallrec.termid.set(wsaaTermid);
		isuallrec.newTranno.set(ZERO);
		/* MOVE PAYR-PTDATE            TO ISUA-EFFDATE.                 */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			isuallrec.effdate.set(covrmjaIO.getCurrfrom());
			isuallrec.runDate.set(covrmjaIO.getCurrfrom());
		}
		else {
			isuallrec.effdate.set(payrIO.getPtdate());
			isuallrec.runDate.set(payrIO.getPtdate());
		}
		if (flexiblePremium.isTrue()
		&& isNE(t5687rec.singlePremInd, "Y")) {
			isuallrec.effdate.set(wsaaOrigPtdate);
			isuallrec.runDate.set(wsaaOrigPtdate);
		}
		isuallrec.language.set(atmodrec.language);
		isuallrec.function.set(SPACES);
		for (sub1.set(1); !(isGT(sub1, 3)); sub1.add(1)){
			e020SubCall();
		}
		goTo(GotoLabel.e090Exit);
	}

	/**
	* <pre>
	* Do the subroutine calls.
	* </pre>
	*/
protected void e020SubCall()
	{
		isuallrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.subprog[sub1.toInt()], SPACES)) {
			callProgram(t5671rec.subprog[sub1.toInt()], isuallrec.isuallRec);
		}
		if (isNE(isuallrec.statuz, varcom.oK)) {
			syserrrec.params.set(isuallrec.isuallRec);
			syserrrec.statuz.set(isuallrec.statuz);
			xxxxFatalError();
		}
	}

protected void f000BenefitBilling()
	{
		f010BenefitBilling();
	}

protected void f010BenefitBilling()
	{
		/* If the benefit billing method is blank there will be no         */
		/* benefit billing subroutine to call so we've finished.           */
		if (isEQ(t5687rec.bbmeth, SPACES)) {
			return ;
		}
		/* Fetch relevent billing subroutine.                              */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5534);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5534rec.t5534Rec.set(itemIO.getGenarea());
		if (isEQ(t5534rec.subprog, SPACES)) {
			return ;
		}
		ubblallpar.ubblallRec.set(SPACES);
		ubblallpar.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
		ubblallpar.chdrChdrnum.set(chdrmjaIO.getChdrnum());
		ubblallpar.lifeLife.set(covrmjaIO.getLife());
		ubblallpar.lifeJlife.set(covrmjaIO.getJlife());
		ubblallpar.covrCoverage.set(covrmjaIO.getCoverage());
		ubblallpar.covrRider.set(covrmjaIO.getRider());
		ubblallpar.planSuffix.set(covrmjaIO.getPlanSuffix());
		ubblallpar.billfreq.set(t5534rec.unitFreq);
		ubblallpar.cntcurr.set(chdrmjaIO.getCntcurr());
		ubblallpar.cnttype.set(chdrmjaIO.getCnttype());
		ubblallpar.tranno.set(chdrmjaIO.getTranno());
		ubblallpar.effdate.set(covrmjaIO.getCrrcd());
		ubblallpar.premMeth.set(t5534rec.premmeth);
		ubblallpar.jlifePremMeth.set(t5534rec.jlPremMeth);
		ubblallpar.sumins.set(covrmjaIO.getSumins());
		ubblallpar.premCessDate.set(covrmjaIO.getPremCessDate());
		ubblallpar.crtable.set(covrmjaIO.getCrtable());
		ubblallpar.billchnl.set(covtmjaIO.getBillchnl());
		ubblallpar.mortcls.set(covtmjaIO.getMortcls());
		ubblallpar.svMethod.set(t5534rec.svMethod);
		ubblallpar.language.set(atmodrec.language);
		ubblallpar.user.set(wsaaUser);
		ubblallpar.batccoy.set(wsaaBatckey.batcBatccoy);
		ubblallpar.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ubblallpar.batcactyr.set(wsaaBatckey.batcBatcactyr);
		ubblallpar.batcactmn.set(wsaaBatckey.batcBatcactmn);
		ubblallpar.batctrcde.set(wsaaBatckey.batcBatctrcde);
		ubblallpar.batch.set(wsaaBatckey.batcBatcbatch);
		ubblallpar.tranno.set(chdrmjaIO.getTranno());
		ubblallpar.adfeemth.set(t5534rec.adfeemth);
		ubblallpar.function.set("ISSUE");
		ubblallpar.polsum.set(chdrmjaIO.getPolsum());
		ubblallpar.ptdate.set(chdrmjaIO.getPtdate());
		ubblallpar.polinc.set(chdrmjaIO.getPolinc());
		ubblallpar.occdate.set(chdrmjaIO.getOccdate());
		ubblallpar.chdrRegister.set(chdrmjaIO.getRegister());
		/* Get the next benefit billing date.                              */
		/*    MOVE SPACE                  TO DTC2-DATCON2-REC.     <LA4351>*/
		/*    MOVE T5534-UNIT-FREQ        TO DTC2-FREQUENCY.       <LA4351>*/
		/*    MOVE 1                      TO DTC2-FREQ-FACTOR.     <LA4351>*/
		/*    MOVE COVRMJA-CRRCD          TO DTC2-INT-DATE-1.      <LA4351>*/
		/*    MOVE 0                      TO DTC2-INT-DATE-2.      <LA4351>*/
		/*    CALL 'DATCON2'           USING DTC2-DATCON2-REC.     <LA4351>*/
		/*    IF  DTC2-STATUZ        NOT  = O-K                    <LA4351>*/
		/*        MOVE DTC2-DATCON2-REC   TO SYSR-PARAMS           <LA4351>*/
		/*        MOVE DTC2-STATUZ        TO SYSR-STATUZ           <LA4351>*/
		/*        PERFORM XXXX-FATAL-ERROR                         <LA4351>*/
		/*    END-IF.                                              <LA4351>*/
		/*    MOVE DTC2-INT-DATE-2        TO COVRMJA-BEN-BILL-DATE.<LA4351>*/
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.frequency.set(t5534rec.unitFreq);
		datcon4rec.freqFactor.set(1);
		datcon4rec.intDate1.set(covrmjaIO.getCrrcd());
		wsaaCrrcdDate.set(covrmjaIO.getCrrcd());
		datcon4rec.billday.set(wsaaCrrcdDd);
		datcon4rec.billmonth.set(wsaaCrrcdMm);
		datcon4rec.intDate2.set(0);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		covrmjaIO.setBenBillDate(datcon4rec.intDate2);
	}

protected void a100WopDates()
	{
	covrlnbList = covrpupDAO.getCovrsurByComAndNum(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
	for (Covrpf covrmjaIO : covrlnbList) {
		a210Covrmja(covrmjaIO);
	}
	}


protected void a210Covrmja(Covrpf covrmjaIO)
	{
		a300ReadTr517(covrmjaIO);
		wsaaCpiValid.set("N");
		if (wopMatch.isTrue()) {
			wsaaTr517Rec.set(tr517rec.tr517Rec);
			if (isGT(covrmjaIO.getCpiDate(), covrmjaIO.getRerateDate())
			|| isEQ(covrmjaIO.getCpiDate(), 0)) {
				wsaaEarliestRerateDate.set(covrmjaIO.getRerateDate());	
			}
			else {
				wsaaEarliestRerateDate.set(covrmjaIO.getCpiDate());
			}
			/* Determine if the WOP is life insured specific.                  */
			if (isNE(tr517rec.zrwvflg02, "Y")) {
				covrlnbIO.setParams(SPACES);
				covrlnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
				covrlnbIO.setChdrnum(covrmjaIO.getChdrnum());
				covrlnbIO.setLife(covrmjaIO.getLife());
				covrlnbIO.setPlanSuffix(0);
				covrlnbIO.setFormat(formatsInner.covrlnbrec);
				covrlnbIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
				while ( !(isNE(covrlnbIO.getChdrcoy(),covrmjaIO.getChdrcoy())
				|| isNE(covrlnbIO.getChdrnum(),covrmjaIO.getChdrnum())
				|| isNE(covrlnbIO.getLife(),covrmjaIO.getLife())
				|| isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
					a400SetWopDate();
				}

				if (isNE(wsaaEarliestRerateDate, 0)) {
					a240RewriteCovrmja(covrmjaIO);
				}
			}
			else {
				covrlnbIO.setParams(SPACES);
				covrlnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
				covrlnbIO.setChdrnum(covrmjaIO.getChdrnum());
				covrlnbIO.setPlanSuffix(0);
				covrlnbIO.setFormat(formatsInner.covrlnbrec);
				covrlnbIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
				while ( !(isNE(covrlnbIO.getChdrcoy(),covrmjaIO.getChdrcoy())
				|| isNE(covrlnbIO.getChdrnum(),covrmjaIO.getChdrnum())
				|| isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
					a400SetWopDate();
				}

				if (isNE(wsaaEarliestRerateDate, 0)) {
					a240RewriteCovrmja(covrmjaIO);
				}
			}
		}
	}

protected void a240RewriteCovrmja(Covrpf covrmjaIO)
	{
	covrmjaIO.setRerateDate(wsaaEarliestRerateDate.toInt());
	covrpupDAO.updateCovrpf(covrmjaIO);
	if (cpiValid.isTrue()) {
	covrmjaIO.setCpiDate(wsaaEarliestRerateDate.toInt());
	covrpupDAO.updateCovrpfRecord(covrmjaIO);
	}
	}

protected void a270Covrmjaio()
	{
		/*A280-READ-COVRMJA*/
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			xxxxFatalError();
		}
		/*A290-EXIT*/
	}

protected void a300ReadTr517(Covrpf covrmjaIO)
	{
		a310Read(covrmjaIO);
	}

protected void a310Read(Covrpf covrmjaIO)
	{
		/* Read TR517 for Wavier of Premium Component.                     */
		wopNotMatch.setTrue();

		itempf =itempfDAO.readItdmpf("IT",covrmjaIO.getChdrcoy(),tablesInner.tr517.toString(),covrmjaIO.getCrrcd(),covrmjaIO.getCrtable()); 
		if (null != itempf) { //fix ILIFE-8777 defect
			wopMatch.setTrue();
			tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));	 
		}
	}

protected void a30cReadTr517()
	{
		a31cRead();
	}

protected void a31cRead()
	{
		/* Read TR517 for Wavier of Premium Component.                     */
		wsaaWaiveCont = "N";
		wsaaZrwvflgs.set(tr517rec.zrwvflgs);

		itempf =itempfDAO.readItdmpf("IT",covrmjaIO.getChdrcoy().toString(),tablesInner.tr517.toString(),
				covrmjaIO.getCrrcd().toInt(),tr517rec.contitem.toString()); 
		if (null != itempf) { //fix ILIFE-8777 defect
		//performance improvement -- Anjali
			wsaaWaiveCont = "Y";
			tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			tr517rec.zrwvflgs.set(wsaaZrwvflgs);
		}

	}

protected void a400SetWopDate()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a410SetWop();
				case a420Check:
					a420Check();
				case a480Next:
					a480Next();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410SetWop()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		tr517rec.tr517Rec.set(wsaaTr517Rec);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)
		&& isNE(covrlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covrlnbIO.getValidflag(), "1")) {
			goTo(GotoLabel.a480Next);
		}
		wsaaCrtableMatch.set("N");
	}

protected void a420Check()
	{
		for (wsaaCnt.set(1); !(isGT(wsaaCnt, 50)
		|| crtableMatch.isTrue()); wsaaCnt.add(1)){
			if (isEQ(covrlnbIO.getCrtable(), tr517rec.ctable[wsaaCnt.toInt()])) {
				crtableMatch.setTrue();
			}
		}
		if (!crtableMatch.isTrue()
		&& isNE(tr517rec.contitem, SPACES)) {
			a30cReadTr517();
			if (isEQ(wsaaWaiveCont, "Y")) {
				goTo(GotoLabel.a420Check);
			}
		}
		if (crtableMatch.isTrue()
		&& (isEQ(tr517rec.zrwvflg02, "Y")
		|| isEQ(covrlnbIO.getLife(), covrmjaIO.getLife()))) {
			if (isLT(covrlnbIO.getRerateDate(), wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getRerateDate(), 0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getRerateDate());
			}
			if (isNE(covrlnbIO.getCpiDate(), varcom.vrcmMaxDate)) {
				cpiValid.setTrue();
			}
			if (isLT(covrlnbIO.getCpiDate(), wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getCpiDate(), 0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getCpiDate());
			}
		}
	}

protected void a480Next()
	{
		covrlnbIO.setFunction(varcom.nextr);
		/*A490-EXIT*/
	}

protected void x100WritePtrn()
	{
		x110PtrnRecord();
	}

protected void x110PtrnRecord()
	{
		/* Write a transaction record to PTRN file*/
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcpfx(wsaaBatckey.batcBatcpfx);
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		/*    MOVE PAYR-PTDATE            TO PTRN-PTRNEFF.         <V4L005>*/
		if (wsaaCompAdd.isTrue()) {
			ptrnIO.setPtrneff(wsaaEffdate);
		}
		else {
			ptrnIO.setPtrneff(payrIO.getPtdate());
		}
		/* MOVE WSAA-TODAY             TO PTRN-PTRNEFF.                 */
		/* MOVE PAYR-PTDATE            TO PTRN-DATESUB.         <LA5184>*/
		if (flexiblePremium.isTrue()) {
			ptrnIO.setPtrneff(wsaaEffdate);
			/*****                                PTRN-DATESUB          <LA5184>*/
		}
		/* IF CHDRMJA-BILLFREQ          = '00 '                 <LA5184>*/
		/*    MOVE WSAA-TODAY          TO PTRN-DATESUB          <LA5184>*/
		/* END-IF.                                              <LA5184>*/
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setCrtuser(username); //PINNACLE-2931
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

protected void x200UpdateBatchHeader()
	{
		/*X210-UPDATE-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*X290-EXIT*/
	}

protected void x300ReleaseSftlck()
	{
		x310ReleaseSoftlock();
	}

protected void x310ReleaseSoftlock()
	{
		/* Release Soft locked record.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(atmodrec.primaryKey);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void x400Statistics()
	{
		x410Start();
	}

protected void x410Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		/*  MOVE COVRMJA-TRANNO         TO LIFS-TRANNO.                  */
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void y100SpDate()
	{
		y110Start();
	}

protected void y110Start()
	{
		/* To get the DD of the Main plan                                  */
		wsaaDayTemp.set(payrIO.getBtdate());
		wsaaDayOld.set(wsaaDayTemp);
		/* To get MM & YY of Todays Date                                   */
		wsaaDayTemp.set(wsaaToday);
		wsaaDayNew.set(wsaaDayTemp);
		if (isLT(wsaaDdNew, wsaaDdOld)) {
			compute(wsaaMmNew, 0).set(sub(wsaaMmNew, 1));
			if (isEQ(wsaaMmNew, 0)) {
				wsaaMmNew.set(12);
				compute(wsaaYyNew, 0).set(sub(wsaaYyNew, 1));
			}
		}
		wsaaDdNew.set(wsaaDdOld);
		wsaaDayTemp.set(wsaaDayNew);
		/*    MOVE WSAA-DAY-TEMP          TO COVRMJA-BEN-BILL-DATE <LA4477>*/
		/*                                   COVRMJA-CURRFROM      <LA4477>*/
		/*                                   COVRMJA-CRRCD.        <LA4477>*/
		covrmjaIO.setCurrfrom(wsaaDayTemp);
		covrmjaIO.setCrrcd(wsaaDayTemp);
		if (isEQ(t5687rec.bbmeth, SPACES)) {
			covrmjaIO.setBenBillDate(ZERO);
		}
		else {
			covrmjaIO.setBenBillDate(wsaaDayTemp);
		}
	}

protected void m800UpdateMlia()
	{
		/*M800-START*/
		rlliadbrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		rlliadbrec.chdrnum.set(chdrmjaIO.getChdrnum());
		rlliadbrec.fsucoy.set(wsaaFsuCoy);
		rlliadbrec.batctrcde.set(ptrnIO.getBatctrcde());
		rlliadbrec.language.set(atmodrec.language);
		callProgram(Rlliadb.class, rlliadbrec.rlliaRec);
		if (isNE(rlliadbrec.statuz, varcom.oK)) {
			syserrrec.params.set(rlliadbrec.rlliaRec);
			syserrrec.statuz.set(rlliadbrec.statuz);
			xxxxFatalError();
		}
		/*M800-EXIT*/
	}

protected void a100CheckHitd()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case a110Fund:
					a110Fund();
				case a180NextFund:
					a180NextFund();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Fund()
	{

		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.t5515.toString(),
				chdrmjaIO.getCurrfrom().toInt(),ulnkIO.getUalfnd(wsaaSub).toString()); 
		//performance improvement -- Anjali
		if (null != itempf) { //fix ILIFE-8777 defect
			t5515rec.t5515Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if (isNE(t5515rec.zfundtyp, "D")) {
			goTo(GotoLabel.a180NextFund);
		}
		hitdIO.setParams(SPACES);
		hitdIO.setChdrcoy(covrmjaIO.getChdrcoy());
		hitdIO.setChdrnum(covrmjaIO.getChdrnum());
		hitdIO.setLife(covrmjaIO.getLife());
		hitdIO.setCoverage(covrmjaIO.getCoverage());
		hitdIO.setRider(covrmjaIO.getRider());
		hitdIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		hitdIO.setZintbfnd(ulnkIO.getUalfnd(wsaaSub));
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)
		&& isNE(hitdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitdIO.getParams());
			syserrrec.statuz.set(hitdIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(hitdIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.a180NextFund);
		}
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			hitdIO.setEffdate(covrmjaIO.getCurrfrom());
		}
		else {
			if (flexiblePremium.isTrue()) {
				hitdIO.setEffdate(wsaaOrigPtdate);
			}
			else {
				hitdIO.setEffdate(payrIO.getPtdate());
			}
		}
		hitdIO.setZlstintdte(varcom.vrcmMaxDate);
		hitdIO.setZlstsmtdt(varcom.vrcmMaxDate);
		hitdIO.setZlstfndval(ZERO);
		hitdIO.setZlstsmtno(ZERO);
		hitdIO.setZlstsmtbal(ZERO);


		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.th510.toString(),
				hitdIO.getEffdate().toInt(),ulnkIO.getUalfnd(wsaaSub).toString()); 
		if (null == itempf) { //fix ILIFE-8777 defect
		//performance improvement -- Anjali
			syserrrec.params.set(tablesInner.th510+ ulnkIO.getUalfnd(wsaaSub).toString());
			xxxxFatalError();
		}

		th510rec.th510Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		a300GetNextFreq();
		hitdIO.setZnxtintdte(datcon2rec.intDate2);
		hitdIO.setTranno(ulnkIO.getTranno());
		hitdIO.setValidflag("1");
		hitdIO.setEffdate(hitdIO.getEffdate());
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			syserrrec.statuz.set(hitdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a180NextFund()
	{
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			t5515rec.t5515Rec.set(SPACES);
			goTo(GotoLabel.a110Fund);
		}
		/*A190-EXIT1*/
	}

protected void a300GetNextFreq()
	{
		a310Next();
	}

protected void a310Next()
	{
		/*    MOVE SPACES                 TO DTC2-DATCON2-REC.     <LA4351>*/
		/*    MOVE HITD-EFFDATE           TO DTC2-INT-DATE-1.      <LA4351>*/
		/*    MOVE 1                      TO DTC2-FREQ-FACTOR.     <LA4351>*/
		/*    MOVE TH510-ZINTALOFRQ       TO DTC2-FREQUENCY.       <LA4351>*/
		/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.  <LA4351>*/
		/*    IF DTC2-STATUZ              NOT = O-K                <LA4351>*/
		/*        MOVE DTC2-DATCON2-REC   TO SYSR-PARAMS           <LA4351>*/
		/*        MOVE DTC2-STATUZ        TO SYSR-STATUZ           <LA4351>*/
		/*        PERFORM XXXX-FATAL-ERROR                         <LA4351>*/
		/*    END-IF.                                              <LA4351>*/
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(hitdIO.getEffdate());
		wsaaCrrcdDate.set(hitdIO.getEffdate());
		datcon4rec.billday.set(wsaaCrrcdDd);
		datcon4rec.billmonth.set(wsaaCrrcdMm);
		datcon4rec.intDate2.set(0);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(th510rec.zintalofrq);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		datcon2rec.intDate2.set(datcon4rec.intDate2);
		if (isNE(th510rec.zintfixdd, ZERO)) {
			/*     MOVE TH510-ZINTFIXDD    TO DTC2-INT-DATE-2 (5:2) <LA4737>*/
			datcon2rec.intDate2.setSub1String(7, 2, th510rec.zintfixdd);
		}
	}

protected void deleteCvuw3800()
	{
		begin3810();
	}

protected void begin3810()
	{
		cvuwIO.setRecKeyData(SPACES);
		cvuwIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		cvuwIO.setChdrnum(chdrmjaIO.getChdrnum());
		cvuwIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)
		&& isNE(cvuwIO.getStatuz(), varcom.mrnf)
		&& isNE(cvuwIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(cvuwIO.getParams());
			syserrrec.statuz.set(cvuwIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), cvuwIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), cvuwIO.getChdrnum())
		|| isNE(cvuwIO.getStatuz(), varcom.oK)) {
			return ;
		}
		cvuwIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cvuwIO.getStatuz());
			syserrrec.params.set(cvuwIO.getParams());
			xxxxFatalError();
		}
	}

protected void deleteCprp3900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begin3900();
				case callCprp3950:
					callCprp3950();
					deleteCprp3960();
				case exit3900:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3900()
	{
		cprpIO.setRecKeyData(SPACES);
		cprpIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		cprpIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		cprpIO.setChdrnum(chdrmjaIO.getChdrnum());
		cprpIO.setFunction(varcom.begnh);
	}

protected void callCprp3950()
	{
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)
		&& isNE(cprpIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(cprpIO.getParams());
			syserrrec.statuz.set(cprpIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(cprpIO.getStatuz(), varcom.endp)
		|| isNE(cprpIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isNE(cprpIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(cprpIO.getChdrpfx(), chdrmjaIO.getChdrpfx())) {
			goTo(GotoLabel.exit3900);
		}
	}

protected void deleteCprp3960()
	{
		cprpIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cprpIO.getParams());
			syserrrec.statuz.set(cprpIO.getStatuz());
			xxxxFatalError();
		}
		cprpIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callCprp3950);
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuCoy);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrmjaIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypRcesdte.set(covrmjaIO.getRiskCessDate());
		if (isEQ(covrmjaIO.getUnitStatementDate(), 0)) {
			drypDryprcRecInner.drypCbunst.set(covrmjaIO.getCrrcd());
		}
		else {
			drypDryprcRecInner.drypCbunst.set(covrmjaIO.getUnitStatementDate());
		}
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void b100CallZorcompy()
	{
		b110Start();
	}

protected void b110Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.effdate.set(covtmjaIO.getEffdate());
		zorlnkrec.ptdate.set(chdrmjaIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrmjaIO.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			xxxxFatalError();
		}
	}

protected void b300CalcTax()
	{
		b310Start();
	}

protected void b310Start()
	{
		/* Read table TR52D using CHDRLNB-REGISTER as key                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister());
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItemcoy(atmodrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setItempfx("IT");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)
		|| isEQ(wsaaTaxableAmount, ZERO)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		b400ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			b400ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b400ReadTr52e();
			if (null == itempf) {
				syserrrec.params.set(tablesInner.tr52e + wsaaTr52eKey.toString());
				xxxxFatalError();
			}
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		/* Call Tax subroutine                                             */
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(covrmjaIO.getLife());
		txcalcrec.coverage.set(covrmjaIO.getCoverage());
		txcalcrec.rider.set(covrmjaIO.getRider());
		txcalcrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		txcalcrec.crtable.set(covrmjaIO.getCrtable());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		wsaaCntCurr.set(chdrmjaIO.getCntcurr());
		txcalcrec.tranno.set(chdrmjaIO.getTranno());
		txcalcrec.language.set(atmodrec.language);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.transType.set("PREM");
		txcalcrec.effdate.set(wsaaEffdate);
		txcalcrec.jrnseq.set(wsaaJrnseq);
		txcalcrec.batckey.set(atmodrec.batchKey);
		wsaaAmountIn.set(ZERO);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			txcalcrec.amountIn.set(wsaaTaxableAmount);
		}
		else {
			compute(wsaaProrateFactor, 6).setRounded(div(wsaaTaxableAmount, covrmjaIO.getInstprem()));
			if (isEQ(tr52erec.zbastyp, "Y")) {
				compute(wsaaAmountIn, 6).setRounded(mult(covrmjaIO.getZbinstprem(), wsaaProrateFactor));
			}
			else {
				compute(wsaaAmountIn, 6).setRounded(mult(covrmjaIO.getInstprem(), wsaaProrateFactor));
			}
			txcalcrec.amountIn.set(wsaaAmountIn);
		}
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			xxxxFatalError();
		}
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaTotalTax.add(txcalcrec.taxAmt[1]);
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaTotalTax.add(txcalcrec.taxAmt[2]);
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		taxdIO.setChdrnum(chdrmjaIO.getChdrnum());
		taxdIO.setLife(covrmjaIO.getLife());
		taxdIO.setCoverage(covrmjaIO.getCoverage());
		taxdIO.setRider(covrmjaIO.getRider());
		taxdIO.setPlansfx(covrmjaIO.getPlanSuffix());
		taxdIO.setEffdate(txcalcrec.effdate);
		taxdIO.setInstfrom(txcalcrec.effdate);
		taxdIO.setBillcd(wsaaEffdate);
		taxdIO.setInstto(payrIO.getPtdate());
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setTranno(chdrmjaIO.getTranno());
		taxdIO.setTrantype("PREM");
		taxdIO.setBaseamt(wsaaTaxableAmount);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void b400ReadTr52e()
	{
		b410Start();
	}

protected void b410Start()
	{

		tr52erec.tr52eRec.set(SPACES);

		itempf =itempfDAO.readItdmpf("IT",atmodrec.company.toString(),tablesInner.tr52e.toString(),
						wsaaEffdate.toInt(),wsaaTr52eKey.toString()); 
		if (null != itempf) {
			tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void c000CallRounding()
	{
		/*C100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getBillcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/*C900-EXIT*/
	}
	
	protected void performLincProcessing() {
		Tjl47rec tjl47rec = new Tjl47rec(); 
		itempf = null;
		itempf = itempfDAO.readItemItem("IT", atmodrec.company.toString(), 
				tablesInner.tjl47.toString(), wsaaBatckey.batcBatctrcde.toString());
		if(itempf != null) {
			tjl47rec.tjl47Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			LincCSService lincService = (LincCSService) getApplicationContext().getBean(tjl47rec.lincsubr.toString().toLowerCase());
			if(wsaaCompAdd.isTrue())
				lincService.addComponentApproval(setLincpfHelper(), tjl47rec);
			else
				lincService.modifyComponentApproval(setLincpfHelper(), tjl47rec);
		}
	}
	
	protected LincpfHelper setLincpfHelper() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		LincpfHelper lincpfHelper = new LincpfHelper();
		lincpfHelper.setChdrcoy(atmodrec.company.toString());
		lincpfHelper.setChdrnum(wsaaPrimaryChdrnum.toString());
		lincpfHelper.setOwncoy(wsaaFsuCoy.toString());
		lincpfHelper.setClntnum(chdrmjaIO.getCownnum().toString());
		lincpfHelper.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		lincpfHelper.setCownnum(chdrmjaIO.getCownnum().toString());
		lincpfHelper.setCnttype(chdrmjaIO.getCnttype().toString());
		lincpfHelper.setOccdate(chdrmjaIO.getOccdate().toInt());	
		lincpfHelper.setTransactionDate(wsaaToday.toInt());
		lincpfHelper.setTranscd(wsaaBatckey.batcBatctrcde.toString());
		lincpfHelper.setLanguage(atmodrec.language.toString());
		lincpfHelper.setTranno(chdrmjaIO.getTranno().toInt());
		lincpfHelper.setPtrnEff(wsaaEffdate.toInt());
		lincpfHelper.setTermId(varcom.vrcmTermid.toString());
		lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
		lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
		lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
		lincpfHelper.setJobnm(wsaaUser.toString());
		lincpfHelper = setClientDetails(lincpfHelper);
		lincpfHelper = getLifeDetails(lincpfHelper);
		return lincpfHelper;
	}
	
	protected LincpfHelper getLifeDetails(LincpfHelper lincpfHelper) {
		Lifepf lifepf = lifepfDAO.getLifeRecord(atmodrec.company.toString(), wsaaPrimaryChdrnum.toString(), "01", "00");
		Clntpf clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(lifepf.getLifcnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setLifcnum(lifepf.getLifcnum());
		lincpfHelper.setLcltdob(clntpf.getCltdob());
		lincpfHelper.setLcltsex(clntpf.getCltsex());
		lincpfHelper.setLzkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setLzkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setLzkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setLzkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setLzkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setLzkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setLzkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setLgivname(clntpf.getGivname());
		lincpfHelper.setLsurname(clntpf.getSurname());
		return lincpfHelper;
	}
	
	protected LincpfHelper setClientDetails(LincpfHelper lincpfHelper) {
		Clntpf clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(lincpfHelper.getClntnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setZkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setZkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setZkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setZkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setZkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setZkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setZkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setGivname(clntpf.getGivname());
		lincpfHelper.setSurname(clntpf.getSurname());
		lincpfHelper.setCltsex(clntpf.getCltsex());
		lincpfHelper.setCltdob(clntpf.getCltdob());
		lincpfHelper.setClttype(clntpf.getClttype());
		return lincpfHelper;
	}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3695 = new FixedLengthStringData(5).init("T3695");
	private FixedLengthStringData t5644 = new FixedLengthStringData(5).init("T5644");
	private FixedLengthStringData t5534 = new FixedLengthStringData(5).init("T5534");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5676 = new FixedLengthStringData(5).init("T5676");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData t5448 = new FixedLengthStringData(5).init("T5448");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData th510 = new FixedLengthStringData(5).init("TH510");
	private FixedLengthStringData th502 = new FixedLengthStringData(5).init("TH502");
	private FixedLengthStringData t6640 = new FixedLengthStringData(5).init("T6640");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData tr384 = new FixedLengthStringData(6).init("TR384");
	private FixedLengthStringData tr695 = new FixedLengthStringData(5).init("TR695");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData th605 = new FixedLengthStringData(5).init("TH605");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData tjl47 = new FixedLengthStringData(5).init("TJL47");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData cprprec = new FixedLengthStringData(10).init("CPRPREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
	private FixedLengthStringData unltmjarec = new FixedLengthStringData(10).init("UNLTMJAREC");
	private FixedLengthStringData ulnkrec = new FixedLengthStringData(10).init("ULNKREC   ");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC   ");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData fpcorevrec = new FixedLengthStringData(10).init("FPCOREVREC");
	private FixedLengthStringData lifeenqrec = new FixedLengthStringData(10).init("LIFEENQREC");
	private FixedLengthStringData hitdrec = new FixedLengthStringData(10).init("HITDREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}

protected void customerSpecificSinstamt06(){
	
}

protected void setPureLoadingCustomerspecific() {
	
}
}
