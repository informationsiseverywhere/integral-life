/*
 * File: P5099at.java
 * Date: 30 August 2009 0:07:04
 * Author: Quipoz Limited
 *
 * Class transformed from P5099AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.BjrnmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.dataaccess.BonsrvbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
* P5099AT - Bonus JOURNAL AT Module.
* ----------------------------------
*
*  This At module is used to process Bonus Journals.
*  The Contract number to be processed is passed in the AT
*  parameters "primary key".
*
*  Processing is driven by the BONUS JOURNAL file (BJRNPF). All
*  records for this contract for this CHDRNUM for this TRANNO
*  are processed sequentially.
*
*  PROCESSING.
*  ----------
*
*  - READH the CHDR record and store the TRANNO.
*
*  - BEGN the BJRN record to get the EFFDATE of the transaction.
*
*  - Increment the TRANNO.
*
*  - Update the CHDR file by making the current CHDR record
*    VALIDFLAG 2 and writing a new record with the new TRANNO
*    and VALIDFLAG 1. Set 'from' and 'to' dates accordingly.
*
*  - Read tables to establish whether component level
*    accounting or not, to get accounting rules and to get
*    transaction description.
*
*  - Write a Policy Transaction (PTRN) Record.
*
*  MAIN PROCESSING LOOP.
*  --------------------
*
*  PERFORM UNTIL CHANGE OF CHDRNUM OR TRANNO ON THE SURD FILE.
*
*  - If this is a NOTIONAL POLICY, then call BREAKOUT.
*
*  - Update the Coverage with the new Bonus Declaration Date.
*
*  - Write accounting entries :
*
*    1. Post Amount of BONUS to Reversionary Bonus Sub Account.
*       A single sided entry effecting ACBL balance for
*       reversionary Bonus. This is set up in T5645 without
*       a GLMAP which will cause GLPOST to exclude it from
*       the true general ledger postings.
*       Use Full Component Key in ACMV-RLDGACCT.
*
*    - NEXTR the BJRN file.
*
*  END OF MAIN PROCESSING LOOP.
*  ---------------------------
*
*    - Update the Batch Header
*
*    - Release the Softlock on the contract
*
*****************************************************
* </pre>
*/
public class P5099at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5099AT");
	private final String wsaaT5645Prog = "P5099";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	
	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaDayBefore = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private static final String h134 = "H134";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t1688 = "T1688";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String bjrnmjarec = "BJRNMJAREC";
	private static final String bonsrvbrec = "BONSRVBREC";
	private BjrnmjaTableDAM bjrnmjaIO = new BjrnmjaTableDAM();
	private BonsrvbTableDAM bonsrvbIO = new BonsrvbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Batcuprec batcuprec = new Batcuprec();
	private T5645rec t5645rec = new T5645rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Atmodrec atmodrec = new Atmodrec();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		writeRecord2530
	}

	public P5099at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*START*/
		initialise1000();
		/* Continue processing until contract break.*/
		while ( !(isEQ(bjrnmjaIO.getStatuz(), varcom.endp))) {
			processBjrnRecords2000();
		}

		updateBatchHeader3000();
		releaseSoftlock4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
		getTodaysDate1010();
	}

protected void start1000()
	{
		sysrSyserrRecInner.sysrSubrname.set(wsaaProg);
	}

protected void getTodaysDate1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon1rec.datcon1Rec);
			sysrSyserrRecInner.sysrStatuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		wsaaToday.set(datcon1rec.intDate);
		contractHeader1100();
		readTables1200();
		writePtrnRecord1300();
		/*EXIT*/
	}

protected void contractHeader1100()
	{
		/*START*/
		readContractHeader1110();
		begnBjrnFile1120();
		rewriteContractHeader1130();
		writeNewContractHeader1140();
		/*EXIT*/
	}

protected void readContractHeader1110()
	{
		start1110();
	}

protected void start1110()
	{
		/*  Read the contract header using the header number held in*/
		/*  ATMD-PRIMARY-KEY then store the transaction number.*/
		/*  This CHDR record wil be deleted later, hence READH.*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
		compute(wsaaTranno, 0).set(add(1, chdrmjaIO.getTranno()));
		/*  Store the policies summarised figure.*/
		wsaaPolsum.set(chdrmjaIO.getPolsum());
	}

protected void begnBjrnFile1120()
	{
		start1120();
		getDayBeforeEffdate1125();
	}

protected void start1120()
	{
		/*  BEGN on BJRN record to get EFFDATE.*/
		bjrnmjaIO.setParams(SPACES);
		bjrnmjaIO.setChdrcoy(atmodrec.company);
		bjrnmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		bjrnmjaIO.setTranno(wsaaTranno);
		bjrnmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bjrnmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		bjrnmjaIO.setFormat(bjrnmjarec);
		SmartFileCode.execute(appVars, bjrnmjaIO);
		if (isNE(bjrnmjaIO.getStatuz(), varcom.oK)
		&& isNE(bjrnmjaIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(bjrnmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(bjrnmjaIO.getStatuz());
			fatalError9000();
		}
		if (isNE(bjrnmjaIO.getChdrcoy(), atmodrec.company)
		|| isNE(bjrnmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isNE(bjrnmjaIO.getTranno(), wsaaTranno)
		|| isEQ(bjrnmjaIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(bjrnmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(bjrnmjaIO.getStatuz());
			fatalError9000();
		}
	}

protected void getDayBeforeEffdate1125()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(bjrnmjaIO.getEffdate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon2rec.datcon2Rec);
			fatalError9000();
		}
		wsaaDayBefore.set(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void rewriteContractHeader1130()
	{
		start1130();
	}

	/**
	* <pre>
	*  Rewrite old CHDR rec with VALIDFLAG = '2'. Set the
	* 'to' date on the record to one day before the EFFDATE of
	*  transaction.
	* </pre>
	*/
protected void start1130()
	{
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmjaIO.setTranid(varcom.vrcmCompTranid);
		chdrmjaIO.setCurrto(wsaaDayBefore);
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
	}

protected void writeNewContractHeader1140()
	{
		start1140();
	}

	/**
	* <pre>
	*  WRITR a new CHDR rec with VALIDFLAG = '1' (making it
	*  'current'). Set the 'from' date to the EFFDATE of the
	*  transaction and the 'to' date to all 9's.
	* </pre>
	*/
protected void start1140()
	{
		chdrmjaIO.setTranno(wsaaTranno);
		chdrmjaIO.setCurrfrom(bjrnmjaIO.getEffdate());
		chdrmjaIO.setCurrto(99999999);
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setValidflag("1");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
	}

protected void readTables1200()
	{
		/*START*/
		readAccountingRules1210();
		getTranDesc1220();
		/*EXIT*/
	}

protected void readAccountingRules1210()
	{
		start1210();
	}

protected void start1210()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaT5645Prog);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(h134);
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getTranDesc1220()
	{
		start1220();
	}

	/**
	* <pre>
	*  Read T1688 to get the transaction description of the
	*  number used in this transaction.
	* </pre>
	*/
protected void start1220()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(t1688);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(descIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(descIO.getStatuz());
			fatalError9000();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}

protected void writePtrnRecord1300()
	{
		start1300();
	}

protected void start1300()
	{
		/* MOVE '1'                        TO PTRN-VALIDFLAG.           */
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setValidflag("1");
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(wsaaTranno);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(ptrnIO.getStatuz());
			fatalError9000();
		}
	}

protected void processBjrnRecords2000()
	{
		/*START*/
		/*  If this is a notional policy (part of a summary record)*/
		/*  then perform the breakout of the ACBL and the ACMV*/
		/*  (this will only happen for single compononent surrenders).*/
		/*  Each time check to see whether BRKOUT has already been*/
		/*  performed down to this 'level' by a preceding BJRN which*/
		/*  was part of the current transaction.*/
		/*  Do not breakout Single Policy Plans.*/
		if (isLTE(bjrnmjaIO.getPlanSuffix(), wsaaPolsum)
		&& isNE(chdrmjaIO.getPolsum(), 1)) {
			breakout2300();
		}
		updateCovr2100();
		/*  Write accounting entries.*/
		writeAccountingEntries2200();
		readNextBjrn2400();
		/*EXIT*/
	}

protected void updateCovr2100()
	{
		start2100();
	}

protected void start2100()
	{
		/*  Update the Coverage file with the new Bonus Declaration Date.*/
		/*  First Read and Hold.*/
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(bjrnmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(bjrnmjaIO.getChdrnum());
		covrmjaIO.setLife(bjrnmjaIO.getLife());
		covrmjaIO.setCoverage(bjrnmjaIO.getCoverage());
		covrmjaIO.setRider(bjrnmjaIO.getRider());
		covrmjaIO.setPlanSuffix(bjrnmjaIO.getPlanSuffix());
		if (isEQ(chdrmjaIO.getPolsum(), 1)) {
			covrmjaIO.setPlanSuffix(0);
		}
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
		covrmjaIO.setUnitStatementDate(bjrnmjaIO.getAdjBonusDecDate());
		covrmjaIO.setTranno(wsaaTranno);
		/*  Rewrite.*/
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
	}

protected void writeAccountingEntries2200()
	{
		start2200();
	}

protected void start2200()
	{
		/* Set up LIFACMV parameters which are common to all postings.*/
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.rldgcoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec1.origcurr.set(chdrmjaIO.getCntcurr());
		lifacmvrec1.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec1.tranno.set(wsaaTranno);
		lifacmvrec1.tranref.set(chdrmjaIO.getChdrnum());
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(wsaaTransactionDate);
		lifacmvrec1.transactionTime.set(wsaaTransactionTime);
		lifacmvrec1.user.set(wsaaUser);
		lifacmvrec1.termid.set(wsaaTermid);
		lifacmvrec1.batccoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.genlcoy.set(bjrnmjaIO.getChdrcoy());
		lifacmvrec1.effdate.set(bjrnmjaIO.getEffdate());
		lifacmvrec1.trandesc.set(wsaaLongdesc);
		lifacmvrec1.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec1.substituteCode[6].set(bjrnmjaIO.getCrtable());
		/*  Write ACMV record to adjust the ACBL balance.*/
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		lifacmvrec1.sacscode.set(t5645rec.sacscode01);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec1.glcode.set(t5645rec.glmap01);
		lifacmvrec1.glsign.set(t5645rec.sign01);
		lifacmvrec1.contot.set(t5645rec.cnttot01);
		wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
		wsaaPlan.set(bjrnmjaIO.getPlanSuffix());
		wsaaRldgLife.set(bjrnmjaIO.getLife());
		wsaaRldgCoverage.set(bjrnmjaIO.getCoverage());
		wsaaRldgRider.set(bjrnmjaIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.origamt.set(bjrnmjaIO.getArbAdjustAmt());
		/*IF  LIFA-ORIGAMT > 0*/
		if (isNE(lifacmvrec1.origamt, ZERO)) {
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(lifacmvrec1.lifacmvRec);
				sysrSyserrRecInner.sysrStatuz.set(lifacmvrec1.statuz);
				fatalError9000();
			}
			updateBonsrvbRecord2500();
		}
	}

protected void breakout2300()
	{
		start2300();
	}

protected void start2300()
	{
		/*   Call the breakout routine in order to breakout the ACBL*/
		/*   and the ACMV records.*/
		if (isEQ(chdrmjaIO.getPolsum(), 1)
		|| isEQ(bjrnmjaIO.getPlanSuffix(), 0)) {
			return ;
		}
		brkoutrec.brkOldSummary.set(wsaaPolsum);
		compute(brkoutrec.brkNewSummary, 0).set(sub(bjrnmjaIO.getPlanSuffix(), 1));
		brkoutrec.brkChdrnum.set(chdrmjaIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrmjaIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(SPACES);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(brkoutrec.outRec);
			sysrSyserrRecInner.sysrStatuz.set(brkoutrec.brkStatuz);
			fatalError9000();
		}
		/*  Store the new number of policies summarised.*/
		wsaaPolsum.set(brkoutrec.brkNewSummary);
	}

protected void readNextBjrn2400()
	{
		/*START*/
		bjrnmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, bjrnmjaIO);
		if (isNE(bjrnmjaIO.getStatuz(), varcom.oK)
		&& isNE(bjrnmjaIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(bjrnmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(bjrnmjaIO.getStatuz());
			fatalError9000();
		}
		if (isNE(bjrnmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(bjrnmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isNE(bjrnmjaIO.getTranno(), wsaaTranno)) {
			bjrnmjaIO.setStatuz("ENDP");
		}
		/*EXIT*/
	}

protected void updateBonsrvbRecord2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2510();
					rewrt2520();
				case writeRecord2530:
					writeRecord2530();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2510()
	{
		/* first call DATCON2 to get date for when bonus is paid to*/
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(bjrnmjaIO.getEffdate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon2rec.datcon2Rec);
			fatalError9000();
		}
		bonsrvbIO.setDataArea(SPACES);
		bonsrvbIO.setChdrcoy(bjrnmjaIO.getChdrcoy());
		bonsrvbIO.setChdrnum(chdrmjaIO.getChdrnum());
		bonsrvbIO.setLife(bjrnmjaIO.getLife());
		bonsrvbIO.setCoverage(bjrnmjaIO.getCoverage());
		bonsrvbIO.setRider(bjrnmjaIO.getRider());
		bonsrvbIO.setPlanSuffix(bjrnmjaIO.getPlanSuffix());
		bonsrvbIO.setFormat(bonsrvbrec);
		bonsrvbIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, bonsrvbIO);
		if (isNE(bonsrvbIO.getStatuz(), varcom.oK)
		&& isNE(bonsrvbIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(bonsrvbIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(bonsrvbIO.getStatuz());
			fatalError9000();
		}
		if (isNE(bonsrvbIO.getChdrcoy(), bjrnmjaIO.getChdrcoy())
		|| isNE(bonsrvbIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isNE(bonsrvbIO.getLife(), bjrnmjaIO.getLife())
		|| isNE(bonsrvbIO.getCoverage(), bjrnmjaIO.getCoverage())
		|| isNE(bonsrvbIO.getRider(), bjrnmjaIO.getRider())
		|| isNE(bonsrvbIO.getPlanSuffix(), bjrnmjaIO.getPlanSuffix())
		|| isEQ(bonsrvbIO.getStatuz(), varcom.endp)) {
			/*    Release the record still held*/
			bonsrvbIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, bonsrvbIO);
			if (isNE(bonsrvbIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(bonsrvbIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(bonsrvbIO.getStatuz());
				fatalError9000();
			}
			bonsrvbIO.setDataArea(SPACES);
			bonsrvbIO.setBonPayLastYr(ZERO);
			bonsrvbIO.setBonPayThisYr(ZERO);
			bonsrvbIO.setTotalBonus(ZERO);
			bonsrvbIO.setChdrcoy(bjrnmjaIO.getChdrcoy());
			bonsrvbIO.setChdrnum(chdrmjaIO.getChdrnum());
			bonsrvbIO.setLife(bjrnmjaIO.getLife());
			bonsrvbIO.setCoverage(bjrnmjaIO.getCoverage());
			bonsrvbIO.setRider(bjrnmjaIO.getRider());
			bonsrvbIO.setPlanSuffix(bjrnmjaIO.getPlanSuffix());
			goTo(GotoLabel.writeRecord2530);
		}
	}

protected void rewrt2520()
	{
		/*  VALIDFLAG '2' old record, and write a new one.*/
		bonsrvbIO.setTermid(wsaaTermid);
		bonsrvbIO.setUser(wsaaUser);
		bonsrvbIO.setTransactionDate(wsaaTransactionDate);
		bonsrvbIO.setTransactionTime(wsaaTransactionTime);
		bonsrvbIO.setValidflag("2");
		bonsrvbIO.setCurrto(datcon2rec.intDate2);
		bonsrvbIO.setFormat(bonsrvbrec);
		bonsrvbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, bonsrvbIO);
		if (isNE(bonsrvbIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(bonsrvbIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(bonsrvbIO.getStatuz());
			fatalError9000();
		}
	}

protected void writeRecord2530()
	{
		bonsrvbIO.setValidflag("1");
		bonsrvbIO.setCurrfrom(bjrnmjaIO.getEffdate());
		bonsrvbIO.setCurrto(varcom.vrcmMaxDate);
		setPrecision(bonsrvbIO.getTotalBonus(), 2);
		bonsrvbIO.setTotalBonus(add(bonsrvbIO.getTotalBonus(), lifacmvrec1.origamt));
		bonsrvbIO.setTermid(wsaaTermid);
		bonsrvbIO.setUser(wsaaUser);
		bonsrvbIO.setTransactionDate(wsaaTransactionDate);
		bonsrvbIO.setTransactionTime(wsaaTransactionTime);
		bonsrvbIO.setFormat(bonsrvbrec);
		bonsrvbIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bonsrvbIO);
		if (isNE(bonsrvbIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(bonsrvbIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(bonsrvbIO.getStatuz());
			fatalError9000();
		}
	}

protected void updateBatchHeader3000()
	{
		/*START*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			sysrSyserrRecInner.sysrStatuz.set(batcuprec.statuz);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void releaseSoftlock4000()
	{
		start4000();
	}

protected void start4000()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(sftlockrec.sftlockRec);
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		fatalErrors9000();
		errorProg9000();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void fatalErrors9000()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			return ;
		}
		sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void errorProg9000()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/*  System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSubrname = new FixedLengthStringData(50).isAPartOf(sysrMsgarea, 0);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}
}
