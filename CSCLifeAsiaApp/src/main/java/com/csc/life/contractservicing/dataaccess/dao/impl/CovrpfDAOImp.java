package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovrpfDAOImp extends BaseDAOImpl<Covrpf> implements CovrpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CovrpfDAOImp.class);

	private static final String SQLFORUpdateIndexationInd = "Update Covrpf set INDXIN = ? where UNIQUE_NUMBER in ( ";
	private static final String SQLFORSelectIndexationInd = "select UNIQUE_NUMBER from Covrpf where CHDRNUM = ? and CHDRCOY = ?";
	@Override
	public int updateIndexationInd(String contractNum, String indexationInd, String compy) {
        PreparedStatement preStat = null;
        PreparedStatement preStatUpdate = null;
        ResultSet rs = null;
		try (Connection conn = getConnection()) {
		    preStat = conn.prepareStatement(SQLFORSelectIndexationInd);
			preStat.setString(1, contractNum);
			preStat.setString(2, compy);
			rs = preStat.executeQuery(); 
			List<String> ids = new ArrayList<>();
			StringBuilder updateSql = new StringBuilder(SQLFORUpdateIndexationInd);
			while (rs.next()) {
				String id = rs.getString("UNIQUE_NUMBER");
				updateSql.append("?,");
				ids.add(id);
			}
			if (!ids.isEmpty()) {
				updateSql = updateSql.replace(updateSql.length() - 1, updateSql.length(), ")");
				String sql = updateSql.toString();
				LOGGER.debug(sql);
				preStatUpdate = conn.prepareStatement(sql);
				preStatUpdate.setString(1, indexationInd);
				for (int i = 0; i < ids.size(); i++) {
					preStatUpdate.setString(i + 2, ids.get(i));
				}
				return preStatUpdate.executeUpdate();
			} else {
				return 0;
			}
		} catch (SQLException e) {
			LOGGER.error("updateIndexationInd()", e);
			throw new SQLRuntimeException(e);
        } finally {
			close(preStat,rs);
			close(preStatUpdate,rs);
		}
	}
	
	private static final String SQLFORGetCovrlnbPF = "select UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,"
			+ "RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,"
			+ "REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,"
			+ "CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,"
			+ "NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,"
			+ "RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,"
			+ "CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,"
			+ "RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,"
			+ "ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,USRPRF,JOBNM,DATIME,LOADPER,"
			+ "RATEADJ,FLTMORT,PREMADJ,AGEADJ,ZSTPDUTY01,ZCLSTATE,GMIB,GMDB,GMWB from Covrpf "
			+ "where CHDRNUM = ? and CHDRCOY = ? and LIFE = ? and COVERAGE = ? and RIDER = ? and PLNSFX = ? and VALIDFLAG = '1'";
	@Override
	public List<Covrpf> getCovrlnbPF(String chdrcoy, String chdrnum, String life,
			String coverage, String rider, Integer planSuffix) {
		List<Covrpf> result = new ArrayList<>();
		try (PreparedStatement prep = getPrepareStatement(SQLFORGetCovrlnbPF)) {
			prep.setString(1, chdrnum);
			prep.setString(2, chdrcoy);
			prep.setString(3, life);
			prep.setString(4, coverage);
			prep.setString(5, rider);
			prep.setInt(6, planSuffix);
			try(ResultSet rs = prep.executeQuery()){
			BeanPropertyRowMapper<Covrpf> rowMapper = BeanPropertyRowMapper.newInstance(Covrpf.class);
			int i = 1;
			while (rs.next()) {
				/*Covrpf covrpf = new Covrpf();
				covrpf.setChdrcoy(rs.getString("CHDRCOY"));
				covrpf.setChdrnum(rs.getString("CHDRNUM"));
				covrpf.setLife(rs.getString("LIFE"));
				covrpf.setCoverage(rs.getString("COVERAGE"));
				covrpf.setRider(rs.getString("RIDER"));
				covrpf.setPlanSuffix(rs.getInt("PLNSFX"));*/
				Covrpf covrpf = rowMapper.mapRow(rs, i);
				result.add(covrpf);
				i++;
			}
			}
		} catch (SQLException e) {
			LOGGER.error("getCovrlnbPF()", e);
			throw new SQLRuntimeException(e);
		}
		return result;
	}

	// ILIFE-7584

		public List<Covrpf> searchCovrRecordByCoyNumDescUniquNo(String chdrcoy, String chdrNum) throws SQLRuntimeException{

			StringBuilder sqlCovtSelect = new StringBuilder();

			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sqlCovtSelect.append("SELECT UNIQUE_NUMBER, ");
			sqlCovtSelect.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
			sqlCovtSelect.append("TERMID, TRDT, TRTM, USER_T, CRTABLE, ");
			sqlCovtSelect.append("RCESDTE, PCESDTE, RCESAGE, PCESAGE, RCESTRM, ");
			sqlCovtSelect.append("PCESTRM, SUMINS, LIENCD, MORTCLS, JLIFE, ");
			sqlCovtSelect.append("RSUNIN, RUNDTE, ");
			sqlCovtSelect.append("SINGP, INSTPREM, PLNSFX, ");
			sqlCovtSelect.append("PAYRSEQNO,  BCESAGE, BCESTRM, BCESDTE, ");
			sqlCovtSelect.append("BAPPMETH, ZBINSTPREM, ZLINSTPREM,");
			sqlCovtSelect.append("USRPRF, JOBNM, DATIME, LOADPER, ");
			sqlCovtSelect.append("RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZCLSTATE, ZSTPDUTY01, LNKGNO, TPDTYPE, LNKGSUBREFNO, LNKGIND, RISKPREM, CRRCD, CURRFROM, STATCODE, PSTATCODE ");
			sqlCovtSelect.append("FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = '1'");
			sqlCovtSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");

			PreparedStatement psCovtSelect = getPrepareStatement(sqlCovtSelect.toString());
			ResultSet sqlpCovt1Rs = null;
			List<Covrpf> covtSearchResult = new ArrayList<Covrpf>();
			try {

				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				psCovtSelect.setString(1, chdrcoy);
				psCovtSelect.setString(2, chdrNum);

				// ---------------------------------
				// Execute Query
				// ---------------------------------
				sqlpCovt1Rs = executeQuery(psCovtSelect);

				// ---------------------------------
				// Iterate result set
				// ---------------------------------
				while (sqlpCovt1Rs.next()) {

					Covrpf covrpf = new Covrpf();
					covrpf.setUniqueNumber(sqlpCovt1Rs.getInt("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(sqlpCovt1Rs.getString("CHDRCOY"));
					covrpf.setChdrnum(sqlpCovt1Rs.getString("CHDRNUM"));
					covrpf.setLife(sqlpCovt1Rs.getString("LIFE"));
					covrpf.setCoverage(sqlpCovt1Rs.getString("COVERAGE"));
					covrpf.setRider(sqlpCovt1Rs.getString("RIDER"));
					covrpf.setTermid(sqlpCovt1Rs.getString("TERMID"));
					covrpf.setTransactionDate(sqlpCovt1Rs.getInt("TRDT"));
					covrpf.setTransactionTime(sqlpCovt1Rs.getInt("TRTM"));
					covrpf.setUser(sqlpCovt1Rs.getInt("USER_T"));
					covrpf.setCrtable(sqlpCovt1Rs.getString("CRTABLE"));
					covrpf.setRcesDte(sqlpCovt1Rs.getInt("RCESDTE"));
					covrpf.setPcesDte(sqlpCovt1Rs.getInt("PCESDTE"));
					covrpf.setRcesAge(sqlpCovt1Rs.getInt("RCESAGE"));
					covrpf.setPcesAge(sqlpCovt1Rs.getInt("PCESAGE"));
					covrpf.setRcesTrm(sqlpCovt1Rs.getInt("RCESTRM"));
					covrpf.setPcesTrm(sqlpCovt1Rs.getInt("PCESTRM"));
					covrpf.setSumins(sqlpCovt1Rs.getBigDecimal("SUMINS"));
					covrpf.setLiencd(sqlpCovt1Rs.getString("LIENCD"));
					covrpf.setMortcls(sqlpCovt1Rs.getString("MORTCLS"));
					covrpf.setJlife(sqlpCovt1Rs.getString("JLIFE"));
					covrpf.setReserveUnitsInd(sqlpCovt1Rs.getString("RSUNIN"));
					covrpf.setReserveUnitsDate(sqlpCovt1Rs.getInt("RUNDTE"));
					covrpf.setSingp(sqlpCovt1Rs.getBigDecimal("SINGP"));
					covrpf.setInstprem(sqlpCovt1Rs.getBigDecimal("INSTPREM"));
					covrpf.setPlanSuffix(sqlpCovt1Rs.getInt("PLNSFX"));
					covrpf.setPayrseqno(sqlpCovt1Rs.getInt("PAYRSEQNO"));
					covrpf.setBappmeth(sqlpCovt1Rs.getString("BAPPMETH"));
					covrpf.setZbinstprem(sqlpCovt1Rs.getBigDecimal("ZBINSTPREM"));
					covrpf.setZlinstprem(sqlpCovt1Rs.getBigDecimal("ZLINSTPREM"));
					covrpf.setUserProfile(sqlpCovt1Rs.getString("USRPRF"));
					covrpf.setJobName(sqlpCovt1Rs.getString("JOBNM"));
					covrpf.setDatime(sqlpCovt1Rs.getString("DATIME"));
					covrpf.setLoadper(sqlpCovt1Rs.getBigDecimal("LOADPER"));
					covrpf.setRateadj(sqlpCovt1Rs.getBigDecimal("RATEADJ"));
					covrpf.setFltmort(sqlpCovt1Rs.getBigDecimal("FLTMORT"));
					covrpf.setPremadj(sqlpCovt1Rs.getBigDecimal("PREMADJ"));
					covrpf.setAgeadj(sqlpCovt1Rs.getBigDecimal("AGEADJ"));
					covrpf.setLnkgno(sqlpCovt1Rs.getString("LNKGNO"));
					covrpf.setTpdtype(sqlpCovt1Rs.getString("TPDTYPE"));
					covrpf.setLnkgsubrefno(sqlpCovt1Rs.getString("LNKGSUBREFNO"));
					covrpf.setLnkgind(sqlpCovt1Rs.getString("LNKGIND"));
					covrpf.setRiskprem(sqlpCovt1Rs.getBigDecimal("RISKPREM"));
					covrpf.setCrrcd(sqlpCovt1Rs.getInt("CRRCD"));
					covrpf.setCurrfrom(sqlpCovt1Rs.getInt("CURRFROM"));
					covrpf.setStatcode(sqlpCovt1Rs.getString("STATCODE"));
					covrpf.setPstatcode(sqlpCovt1Rs.getString("PSTATCODE"));
					covtSearchResult.add(covrpf);
				}

				
			} catch (SQLException e) {
				LOGGER.error("searchCovtRecordByCoyNum()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovtSelect, sqlpCovt1Rs);
			}
			return covtSearchResult;
		}
		

		
		
		public int bulkUpdateCovrData(Map<String, Covtpf> covtUpdateMap){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE COVRPF SET INSTPREM = ?, ZBINSTPREM = ?, ZLINSTPREM = ?, ZSTPDUTY01 = ?, RISKPREM = ?, COMMPREM = ? ");
		sb.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ?");
		PreparedStatement ps=null;
		int[] count;
		try{
			ps=getConnection().prepareStatement(sb.toString());
			for(Map.Entry<String, Covtpf> covtpf: covtUpdateMap.entrySet()){
				ps.setBigDecimal(1, covtpf.getValue().getInstprem());
				ps.setBigDecimal(2, covtpf.getValue().getZbinstprem());
				ps.setBigDecimal(3, covtpf.getValue().getZlinstprem());
				ps.setBigDecimal(4, covtpf.getValue().getZstpduty01());
				ps.setBigDecimal(5, covtpf.getValue().getRiskprem());
				ps.setBigDecimal(6, covtpf.getValue().getCommPrem());
				ps.setString(7, covtpf.getValue().getChdrcoy());
				ps.setString(8, covtpf.getKey().substring(0, 8));
				ps.setString(9, covtpf.getKey().substring(8, 10));
				ps.setString(10, covtpf.getKey().substring(10, 12));
				ps.setString(11, covtpf.getKey().substring(12, 14));
				
				ps.addBatch();
			}
			count = ps.executeBatch();
			if(count.length > 0)
			return count.length;
		}
		catch(SQLException e){
			LOGGER.error("bulkUpdateCovtData()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, null);
        }
		return 0;
	}
// ILIFE-7584

//	ILIFE-7845 Start 
		public  int updateRiskprem(String chdrcoy, String chdrnum, String life, String coverage, String rider, BigDecimal riskPrem) {
			// TODO Auto-generated method stub

			StringBuilder sb = new StringBuilder();
			sb.append(" UPDATE COVRPF SET RISKPREM = ? ");
			
			
			sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? ");
			PreparedStatement ps=null;
			int count;
			
			try{
				ps=getConnection().prepareStatement(sb.toString());
			
					ps.setBigDecimal(1, riskPrem);
					ps.setString(2,chdrcoy);
					ps.setString(3, chdrnum);
					ps.setString(4, life);
					ps.setString(5, coverage);
					ps.setString(6,rider);
					
				count = ps.executeUpdate();
				if(count > 0)
				return count;
			}
			catch(SQLException e){
				LOGGER.error("bulkUpdateCovtData()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
			}
			finally {
	            close(ps, null);
	        }
			return 0;
		}//ILIFE-7845 end
		
		public List<Covrpf> getCovrpfRecord(String chdrcoy, String chdrNum) {

			StringBuilder sqlCovtSelect = new StringBuilder();
			sqlCovtSelect.append("SELECT UNIQUE_NUMBER, ");
			sqlCovtSelect.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
			sqlCovtSelect.append("CRTABLE, CRRCD, INSTPREM, CURRFROM ");
			sqlCovtSelect.append("FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = '1' AND STATCODE = 'IF' ");
			sqlCovtSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");

			PreparedStatement psCovtSelect = getPrepareStatement(sqlCovtSelect.toString());
			ResultSet sqlpCovt1Rs = null;
			List<Covrpf> covtSearchResult = new ArrayList<Covrpf>();
			try {
				psCovtSelect.setString(1, chdrcoy);
				psCovtSelect.setString(2, chdrNum);

				sqlpCovt1Rs = executeQuery(psCovtSelect);

				while (sqlpCovt1Rs.next()) {

					Covrpf covrpf = new Covrpf();
					covrpf.setUniqueNumber(sqlpCovt1Rs.getInt("UNIQUE_NUMBER"));
					covrpf.setChdrcoy(sqlpCovt1Rs.getString("CHDRCOY"));
					covrpf.setChdrnum(sqlpCovt1Rs.getString("CHDRNUM"));
					covrpf.setLife(sqlpCovt1Rs.getString("LIFE"));
					covrpf.setCoverage(sqlpCovt1Rs.getString("COVERAGE"));
					covrpf.setRider(sqlpCovt1Rs.getString("RIDER"));
					covrpf.setCrtable(sqlpCovt1Rs.getString("CRTABLE"));
					covrpf.setCrrcd(sqlpCovt1Rs.getInt("CRRCD"));
					covrpf.setInstprem(sqlpCovt1Rs.getBigDecimal("INSTPREM"));
					covrpf.setCurrfrom(sqlpCovt1Rs.getInt("CURRFROM"));
					covtSearchResult.add(covrpf);
				}

				
			} catch (SQLException e) {
				LOGGER.error("searchCovtRecordByCoyNum()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovtSelect, sqlpCovt1Rs);
			}
			return covtSearchResult;
		}
		
		public BigDecimal getPreviousInstprem(String chdrcoy, String chdrNum, String life, String coverage, String rider, String crtable, String currfrom) {

			StringBuilder sqlCovtSelect = new StringBuilder();
			sqlCovtSelect.append(" SELECT INSTPREM FROM COVRPF "
					+ " WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND CRTABLE = ? AND CURRFROM = ? AND "
					+ " TRANNO = (SELECT MAX(TRANNO) FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND "
					+ " COVERAGE = ? AND RIDER = ? AND CRTABLE = ? AND CURRFROM = ? AND VALIDFLAG = '2')");

			PreparedStatement psCovtSelect = getPrepareStatement(sqlCovtSelect.toString());
			ResultSet sqlpCovt1Rs = null;
			BigDecimal instprem = new BigDecimal(0);
			try {
				psCovtSelect.setString(1, chdrcoy);
				psCovtSelect.setString(2, chdrNum);
				psCovtSelect.setString(3, life);
				psCovtSelect.setString(4, coverage);
				psCovtSelect.setString(5, rider);
				psCovtSelect.setString(6, crtable);
				psCovtSelect.setString(7, currfrom);
				psCovtSelect.setString(8, chdrcoy);
				psCovtSelect.setString(9, chdrNum);
				psCovtSelect.setString(10, life);
				psCovtSelect.setString(11, coverage);
				psCovtSelect.setString(12, rider);
				psCovtSelect.setString(13, crtable);
				psCovtSelect.setString(14, currfrom);

				sqlpCovt1Rs = executeQuery(psCovtSelect);

				while (sqlpCovt1Rs.next()) {

					instprem = sqlpCovt1Rs.getBigDecimal("INSTPREM");
				}
			} catch (SQLException e) {
				LOGGER.error("getPreviousInstprem()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psCovtSelect, sqlpCovt1Rs);
			}
			return instprem;
		}
}