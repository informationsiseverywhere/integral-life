package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:34
 * Description:
 * Copybook name: BFRQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bfrqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bfrqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData bfrqKey = new FixedLengthStringData(64).isAPartOf(bfrqFileKey, 0, REDEFINE);
  	public FixedLengthStringData bfrqChdrcoy = new FixedLengthStringData(1).isAPartOf(bfrqKey, 0);
  	public FixedLengthStringData bfrqChdrnum = new FixedLengthStringData(8).isAPartOf(bfrqKey, 1);
  	public PackedDecimalData bfrqEffdate = new PackedDecimalData(8, 0).isAPartOf(bfrqKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(bfrqKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bfrqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bfrqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}