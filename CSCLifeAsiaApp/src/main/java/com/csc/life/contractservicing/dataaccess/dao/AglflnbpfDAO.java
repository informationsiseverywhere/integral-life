package com.csc.life.contractservicing.dataaccess.dao;

import com.csc.life.contractservicing.dataaccess.model.Aglflnbpf;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AglflnbpfDAO extends BaseDAO<Aglflnbpf> {

	public Aglflnbpf getAgentInformByNum(String agntNum, String compy);
	public Aglflnbpf getAglflnb(String agntNum, String compy);
}
