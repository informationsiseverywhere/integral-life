package com.csc.life.contractservicing.dataaccess.dao;
import java.util.List;

import com.csc.life.contractservicing.dataaccess.model.Tpdbpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface TpdbpfDAO extends BaseDAO<Tpdbpf> {
	
	public  List<Tpdbpf> getTpdbtamt(String chdrcoy,String chdrnum);
	public  List<Tpdbpf> getTpoldbt(String chdrcoy,String chdrnum);
	public  List<Tpdbpf> getTpoldbtAll(String chdrcoy,String chdrnum);
}
