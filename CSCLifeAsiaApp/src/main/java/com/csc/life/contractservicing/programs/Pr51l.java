/*
 * File: Pr51l.java
 * Date: 30 August 2009 1:36:55
 * Author: Quipoz Limited
 * 
 * Class transformed from PR51L.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PhrtTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.screens.Sr51lScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        Premium Holiday Reinstatement Submenu
*        -------------------------------------
*
* Overview.
* ---------
*
*  This is the PH Reinstatement Submenun program. This screen
*  (SR51L) allows the user to select a Contract, an Effective
*  date and an Action.
*  The Contract validation is described below, the date must be
*   a valid date and the  Action  must also be valid - see  the
*   PR51L entry in T1690 for allowable Actions.
*
* Validation.
* -----------
*
*  Key 1 - Contract number:
*  -----
*  The Contract must exist.
*  The Contract must have a valid status for transaction that is
*   being selected - check this via the table T5679.
*
*  Key 2 - Reinstatement Date
*  -----
*   The Reinstatement Date inputed must be in line with the contract
*   PTDATE, i.e. must be the additive multiple of the billing frequency
*   from PTDATE. It also must > PTDATE and <= Today's date. Only the
*   Qutation allows the Reinstatement Date set up as future date.
*
*  The ACTION must be a valid selection.
*     Currently, the Actions are 'A' - Pending PH Reinstatement
*                                'B' - Modify Pending PH Reinstatement
*                                'C' - Pending PH Reinstatement Inquiry
*                                'D' - PH Reinstatement Issue
*                                'E' - Delete Pending PH Reinstatement
*                                'F' - PH Reinstatement Quotation
*
* Updating.
* ---------
*
* Softlock the Contract Header - If already locked, display
*  an error message.
*
* Store CHDRMJA/CHDRLIF/CHDRLNB for use by next program in
* stack - do KEEPS
* Update the Batch Transaction Code.
* Commence Batching if required.
* The remarks section should detail the main processes of the
* program, and any special functions.
*
***********************************************************************
* </pre>
*/
public class Pr51l extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR51L");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(6, 0).setUnsigned();

	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(1);
	private Validator trcdeMatch = new Validator(wsaaTrcde, "Y");
	private Validator trcdeNotMatch = new Validator(wsaaTrcde, "N");
	private PackedDecimalData wsaaPhrtEffdate = new PackedDecimalData(8, 0).setUnsigned();
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String itemrec = "ITEMREC";
	private static final String phrtrec = "PHRTREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PhrtTableDAM phrtIO = new PhrtTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5679rec t5679rec = new T5679rec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sr51lScreenVars sv = ScreenProgram.getScreenVars( Sr51lScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private Ta524rec ta524rec = new Ta524rec();
	private static final String ta524 = "TA524";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private boolean prmhldtrad = false;
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO",PrmhpfDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2290, 
		exit2990, 
		setUp3500, 
		exit3090
	}

	public Pr51l() {
		super();
		screenVars = sv;
		new ScreenModel("Sr51l", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		sv.effdate.set(varcom.vrcmMaxDate);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen*/
		/*    before the screen is painted.*/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		try {
			validateKey12210();
			validateKey22250();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateKey12210()
	{
		/*    Validate Contract with correct branch, contract statii*/
		/*    for selected action.*/
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(errorsInner.g667);
			goTo(GotoLabel.exit2290);
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.f259);
			goTo(GotoLabel.exit2290);
		}
		if (isNE(chdrmjaIO.getCntbranch(), wsspcomn.branch)) {
			sv.chdrselErr.set(errorsInner.e455);
			goTo(GotoLabel.exit2290);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		/*   Validate Contract Status Code*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.f321);
			goTo(GotoLabel.exit2290);
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		trcdeNotMatch.setTrue();
		/*    Verify the Contract Risk Statuz*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| trcdeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(chdrmjaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				trcdeMatch.setTrue();
			}
		}
		if (trcdeNotMatch.isTrue()) {
			sv.chdrselErr.set(errorsInner.h137);
			goTo(GotoLabel.exit2290);
		}
		/*    Verify the Contract Premium Statuz*/
		trcdeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| trcdeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(chdrmjaIO.getPstatcode(), t5679rec.cnPremStat[wsaaSub.toInt()])) {
				trcdeMatch.setTrue();
			}
		}
		if (trcdeNotMatch.isTrue()) {
			sv.chdrselErr.set(errorsInner.h137);
			goTo(GotoLabel.exit2290);
		}
		if (isNE(sv.action, "F")) {
			checkPhrt2300();
		}
		if (isEQ(sv.action, "A")
		&& isNE(phrtIO.getStatuz(), varcom.endp)) {
			sv.chdrselErr.set(errorsInner.e170);
			goTo(GotoLabel.exit2290);
		}
	}

protected void validateKey22250()
	{
		wsaaPhrtEffdate.set(varcom.vrcmMaxDate);
		if (isNE(subprogrec.key2, "Y")) {
			if (isNE(sv.effdate, varcom.vrcmMaxDate)) {
				sv.effdateErr.set(errorsInner.g570);
				return ;
			}
			else {
				return ;
			}
		}
		else {
			if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
				sv.effdateErr.set(errorsInner.i032);
				return ;
			}
		}
		if (isNE(sv.action, "F")
		&& isGT(sv.effdate, wsaaToday)) {
			sv.effdateErr.set(errorsInner.f073);
			return ;
		}
		//ILIFE-8179
		prmhldtrad = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString(), "CSOTH010", appVars, "IT");
		if(prmhldtrad) {
			readTa524();
		}
		else {
			ta524rec.ta524Rec.set(SPACES);
		}
		if(isEQ(ta524rec.ta524Rec, SPACES)) {
			/*    IF SR51L-EFFDATE        NOT > CHDRMJA-PTDATE                 */
			if (isLT(sv.effdate, chdrmjaIO.getPtdate())) {
				sv.effdateErr.set(errorsInner.rl32);
				return ;
			}
		}
		datcon3rec.datcon3Rec.set(SPACES);
		wsaaFreqFactor.set(ZERO);
		datcon3rec.intDate1.set(chdrmjaIO.getPtdate());
		datcon3rec.intDate2.set(sv.effdate);
		datcon3rec.frequency.set(chdrmjaIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		//ILIFE-8179
		if(isEQ(ta524rec.ta524Rec, SPACES)) {
			wsaaFreqFactor.set(datcon3rec.freqFactor);
			if (isLT(wsaaFreqFactor, datcon3rec.freqFactor)) {
				sv.effdateErr.set(errorsInner.f065);
				return ;
			}
		}
	}

protected void checkPhrt2300()
	{
		begn2310();
	}

protected void begn2310()
	{
		phrtIO.setParams(SPACES);
		phrtIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		phrtIO.setChdrnum(chdrmjaIO.getChdrnum());
		phrtIO.setTranno(99999);
		phrtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		phrtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		phrtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		phrtIO.setFormat(phrtrec);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)
		&& isNE(phrtIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
		if (isEQ(phrtIO.getStatuz(), varcom.endp)
		|| isNE(phrtIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(phrtIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			phrtIO.setStatuz(varcom.endp);
			if (isNE(sv.action, "A")) {
				sv.chdrselErr.set(errorsInner.rlbt);
				return ;
			}
			else {
				return ;
			}
		}
		wsaaPhrtEffdate.set(phrtIO.getEffdate());
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			goTo(GotoLabel.exit2990);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case setUp3500: 
					setUp3500();
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		if (isNE(sv.effdate, varcom.vrcmMaxDate)) {
			wsspcomn.currfrom.set(sv.effdate);
		}
		else {
			wsspcomn.currfrom.set(wsaaPhrtEffdate);
		}
		/*  Perform KEEPS and SFTLOCKs here that required by the next progr*/
		chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getParams());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.action, "A")
		|| isEQ(sv.action, "F")) {
			goTo(GotoLabel.setUp3500);
		}
		phrtIO.setFunction(varcom.keeps);
		phrtIO.setFormat(phrtrec);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
	}

protected void setUp3500()
	{
		if (isEQ(sv.action, "A")){
			wsspcomn.flag.set("P");
		}
		else if (isEQ(sv.action, "B")){
			wsspcomn.flag.set("M");
		}
		else if (isEQ(sv.action, "C")){
			wsspcomn.flag.set("I");
		}
		else if (isEQ(sv.action, "D")){
			wsspcomn.flag.set("C");
		}
		else if (isEQ(sv.action, "E")){
			wsspcomn.flag.set("D");
		}
		else if (isEQ(sv.action, "F")){
			wsspcomn.flag.set("Q");
		}
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "Q")) {
			softlockContract3200();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void softlockContract3200()
	{
		softlockContract3210();
	}

protected void softlockContract3210()
	{
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.entity.set(sv.chdrsel);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

//ILIFE-8179
protected void readTa524() {
	/*  Read TA524*/
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrmjaIO.getChdrcoy().toString());
	itempf.setItemtabl(ta524);/* IJTI-1523 */
	itempf.setItemitem(chdrmjaIO.getCnttype().toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	if(itempf != null && itempf.getGenarea() != null) {
		ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else {
		ta524rec.ta524Rec.set(SPACES);
	}
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e170 = new FixedLengthStringData(4).init("E170");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData f065 = new FixedLengthStringData(4).init("F065");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData g570 = new FixedLengthStringData(4).init("G570");
	private FixedLengthStringData g667 = new FixedLengthStringData(4).init("G667");
	private FixedLengthStringData h137 = new FixedLengthStringData(4).init("H137");
	private FixedLengthStringData i032 = new FixedLengthStringData(4).init("I032");
	private FixedLengthStringData rl32 = new FixedLengthStringData(4).init("RL32");
	private FixedLengthStringData rlbt = new FixedLengthStringData(4).init("RLBT");
}
}
