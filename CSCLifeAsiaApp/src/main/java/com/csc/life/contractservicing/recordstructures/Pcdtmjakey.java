package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:38
 * Description:
 * Copybook name: PCDTMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pcdtmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData pcdtmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData pcdtmjaKey = new FixedLengthStringData(64).isAPartOf(pcdtmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData pcdtmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(pcdtmjaKey, 0);
  	public FixedLengthStringData pcdtmjaChdrnum = new FixedLengthStringData(8).isAPartOf(pcdtmjaKey, 1);
  	public PackedDecimalData pcdtmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(pcdtmjaKey, 9);
  	public FixedLengthStringData pcdtmjaLife = new FixedLengthStringData(2).isAPartOf(pcdtmjaKey, 12);
  	public FixedLengthStringData pcdtmjaCoverage = new FixedLengthStringData(2).isAPartOf(pcdtmjaKey, 14);
  	public FixedLengthStringData pcdtmjaRider = new FixedLengthStringData(2).isAPartOf(pcdtmjaKey, 16);
  	public PackedDecimalData pcdtmjaTranno = new PackedDecimalData(5, 0).isAPartOf(pcdtmjaKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(pcdtmjaKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(pcdtmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		pcdtmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}