/*
 * File: P5052at.java
 * Date: 30 August 2009 0:01:15
 * Author: Quipoz Limited
 *
 * Class transformed from P5052AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LhdrTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        POLICY LOAN REGISTRATION AT MODULE.
*        -----------------------------------
*
* This AT module is called from the Policy Loan Registration
*  program P5052. The Contract number is passed in the AT
*  parameters area.
*
* Overview.
* ---------
*
* This AT module reads the LHDR loan header file using the
*  given Contract number as the key. It will increment the
*  TRANNO on the contract header record, write a PTRN record,
*  write a LOAN record, do a BATCUP, post ACMVs detailing the
*  fact that a Loan has been taken out and then Delete the
*  LHDR loan header record we have just been processing.
*
* Processing.
* -----------
*
* Read the ATREQ written by the calling process.
*
* Read the Loan header LHDR record using the Contract number
*  passed in the AT parameters.
*
* UPDATE CONTRACT HEADER RECORD
*
* Read & lock the CHDR contract header record using the Contract
*  number from the AT parameters.
* Validflag 2 the CHDR Contract header record and REWRiTe.
* Increment the TRANNO by 1 and WRITE a new CHDR contract header
*  record.
*
* Do Batch update BATCUP
*
* Write PTRN for this transaction
*
* CREATE LOAN RECORD
*
* Using the Contract number we are processing as part of the key,
*  Read through the LOAN file ( using the LOANENQ logical view )
*  to determine how many Loans there are/have been already on the
*  Contract so that we can obtain the Next Loan number.
*
* Write new LOAN record, using data from the LHDR loan header
*  record where necessary and using the table T6633 Loan
*  Interest rules ( read on Contract Type ) to calculate the
*  Next Interest and Next Capitalisation dates that are held
*  on the Loan record
*
* ACMV Postings
*
*  Write 2 ACMV records, one putting the amount of the Loan into
*   a Sub-account that can be drawn upon to provide the Contract
*   owner with his Loan and the other putting the Loan amount
*   into the Loan Principal Sub-account.
*   See the T5645 table entry P5052 for more details.
*
* TIDYUP Processing
*
* Delete the LHDR Loan Header record - this record is just a
*  temporary one until the LOAN record is written and then it
*  gets deleted from the system.
*
* Release the Softlock on the CHDR contract Header record
*
*****************************************************************
* </pre>
*/
public class P5052at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("P5052AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final String wsaaT5645Item = "P5052";

	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaTranData = new FixedLengthStringData(25).isAPartOf(wsaaTransArea, 0);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranData, 0);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTranData, 4);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTranData, 8);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTranData, 12);
	private PackedDecimalData wsaaStamp = new PackedDecimalData(17, 2).isAPartOf(wsaaTranData, 16);
	private FixedLengthStringData filler2 = new FixedLengthStringData(175).isAPartOf(wsaaTransArea, 25, FILLER).init(SPACES);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaExistingLoan = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-TRANID */
	private PackedDecimalData wsaaTranDate = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaTranTime = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaTranUser = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaTranTerm = new FixedLengthStringData(4);
	
	private PackedDecimalData wsaanofDay = new PackedDecimalData(3, 0); //ICIL-549
	private FixedLengthStringData wsaaCalflag = new FixedLengthStringData(1); //ICIL-549
		/* WSAA-C-DATE */
	protected ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler4, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler4, 6).setUnsigned();
		/* ERRORS */
	private static final String e723 = "E723";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String itdmrec = "ITEMREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String lhdrrec = "LHDRREC   ";
	private static final String loanrec = "LOANREC   ";
	private static final String loanenqrec = "LOANENQREC";
	private static final String ptrnrec = "PTRNREC   ";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t6633 = "T6633";
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected LhdrTableDAM lhdrIO = new LhdrTableDAM();
	protected LoanTableDAM loanIO = new LoanTableDAM();
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Varcom varcom = new Varcom();
	private Batcuprec batcuprec = new Batcuprec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	protected T6633rec t6633rec = new T6633rec();
	protected Atmodrec atmodrec = new Atmodrec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	boolean cslnd004Permission = false;  //ICIL-549
	

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextIntBillDate5250
	}

	public P5052at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*CONTROL*/
		initialise1000();
		updateChdr2000();
		readTables2500();
		batchUpdate3000();
		writePtrn4000();
		processLoan5000();
		loanRequestLetter3020();
		releaseSoftlock6000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaProg);
		cslnd004Permission = FeaConfg.isFeatureExist("2", "CSLND004", appVars, "IT");	//ICIL-549 
		wsaaToday.set(ZERO);
		wsaaTranno.set(ZERO);
		wsaaContractDate.set(ZERO);
		wsaaLoanDate.set(ZERO);
		wsaaNewDate.set(ZERO);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		readLhdr1100();
	}

protected void readLhdr1100()
	{
		start1100();
	}

protected void start1100()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaTranTerm.set(wsaaTermid);
		wsaaTranDate.set(wsaaTransactionDate);
		wsaaTranTime.set(wsaaTransactionTime);
		wsaaTranUser.set(wsaaUser);
		lhdrIO.setParams(SPACES);
		lhdrIO.setChdrnum(wsaaPrimaryChdrnum);
		lhdrIO.setChdrcoy(atmodrec.company);
		lhdrIO.setFormat(lhdrrec);
		lhdrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lhdrIO);
		if (isNE(lhdrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lhdrIO.getParams());
			syserrrec.params.set(lhdrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void updateChdr2000()
	{
		start2000();
	}

protected void start2000()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		wsaaTranno.set(chdrmjaIO.getTranno());
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(lhdrIO.getLoanStartDate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		callDatcon25300();
		chdrmjaIO.setCurrto(datcon2rec.intDate2);
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setTranid(wsaaTranData);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(1,wsaaTranno));
		chdrmjaIO.setCurrfrom(lhdrIO.getLoanStartDate());
		chdrmjaIO.setCurrto(99999999);
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readTables2500()
	{
		start2500();
	}

protected void start2500()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lhdrIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaT5645Item);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(lhdrIO.getChdrcoy());
		itdmIO.setItemtabl(t6633);
		wsaaT6633Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT6633Type.set("P");
		itdmIO.setItemitem(wsaaT6633Key);
		itdmIO.setItmfrm(lhdrIO.getLoanStartDate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),lhdrIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t6633)
		|| isNE(itdmIO.getItemitem(),wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT6633Key);
			itdmIO.setItemtabl(t6633);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			xxxxFatalError();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
		//ICIL-549 start
		if ((cslnd004Permission)) {
			wsaanofDay.set(t6633rec.nofDay); 
		}
		//ICIL-549 end
	}

protected void batchUpdate3000()
	{
		/*START*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void writePtrn4000()
	{
		start4000();
	}

protected void start4000()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(lhdrIO.getLoanStartDate());
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processLoan5000()
	{
		start5000();
	}

protected void start5000()
	{
		wsaaExistingLoan.set(ZERO);
		loanenqIO.setParams(SPACES);
		loanenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		loanenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		loanenqIO.setLoanNumber(ZERO);
		loanenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loanenqIO.setFormat(loanenqrec);
		while ( !(isEQ(loanenqIO.getStatuz(),varcom.endp))) {
			readLoans5100();
		}

		loanIO.setParams(SPACES);
		loanIO.setFormat(loanrec);
		loanIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		loanIO.setChdrnum(chdrmjaIO.getChdrnum());
		setPrecision(loanIO.getLoanNumber(), 0);
		loanIO.setLoanNumber(add(1,wsaaExistingLoan));
		loanIO.setLoanType(lhdrIO.getLoanType());
		loanIO.setLoanCurrency(lhdrIO.getLoanCurrency());
		loanIO.setLoanOriginalAmount(lhdrIO.getLoanOriginalAmount());
		loanIO.setLoanStartDate(lhdrIO.getLoanStartDate());
		loanIO.setLastCapnLoanAmt(lhdrIO.getLoanOriginalAmount());
		loanIO.setLastCapnDate(lhdrIO.getLoanStartDate());
		loanIO.setLastIntBillDate(lhdrIO.getLoanStartDate());
		//ICIL-549 start
		if(cslnd004Permission) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(lhdrIO.getLoanStartDate());
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(wsaanofDay);
			callDatcon25300();
			loanIO.setLastCapnDate(datcon2rec.intDate2);
			loanIO.setLastIntBillDate(datcon2rec.intDate2);
		}
		//ICIL-549 end
		loanIO.setTermid(wsaaTranTerm);
		loanIO.setTransactionDate(wsaaTranDate);
		loanIO.setTransactionTime(wsaaTranTime);
		loanIO.setUser(wsaaTranUser);
		loanIO.setTplstmdty(wsaaStamp);
		loanIO.setValidflag("1");
		loanIO.setFirstTranno(chdrmjaIO.getTranno());
		loanIO.setLastTranno(ZERO);
		setNextDates5200();
		loanIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			xxxxFatalError();
		}
		postAcmvs5400();
		deleteLhdr5500();
	}

protected void readLoans5100()
	{
		/*START*/
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(),varcom.oK)
		&& isNE(loanenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(loanenqIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(loanenqIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(loanenqIO.getStatuz(),varcom.endp)) {
			loanenqIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaExistingLoan.add(1);
		loanenqIO.setFunction(varcom.nextr);
	}

protected void setNextDates5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start5200();
				case nextIntBillDate5250:
					nextIntBillDate5250();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5200()
	{
		wsaaCalflag.set("N"); //ICIL-549
		wsaaLoanDate.set(lhdrIO.getLoanStartDate());
		wsaaContractDate.set(chdrmjaIO.getOccdate());
		if (isEQ(t6633rec.annloan,"Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(lhdrIO.getLoanStartDate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon25300();
			loanIO.setNextCapnDate(datcon2rec.intDate2);
			goTo(GotoLabel.nextIntBillDate5250);
		}
		if (isEQ(t6633rec.annpoly,"Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(wsaaContractDate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2,lhdrIO.getLoanStartDate()))) {
				callDatcon25300();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}

			loanIO.setNextCapnDate(datcon2rec.intDate2);
			goTo(GotoLabel.nextIntBillDate5250);
		}
		//ICIL-549 start
		if (cslnd004Permission) {   
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(wsaaLoanDate);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(wsaanofDay);
			callDatcon25300();
			wsaaLoanDate.set(datcon2rec.intDate2);
			wsaaCalflag.set("Y");
		}                              
		//ICIL-549 end
		wsaaNewDate.set(ZERO);
		wsaaDayCheck.set(t6633rec.day);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet5600();
		}
		else {
			if (isNE(t6633rec.day,ZERO)) {
				wsaaNewDay.set(t6633rec.day);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewDate);
		datcon2rec.freqFactor.set(1);
		if (isEQ(t6633rec.compfreq,SPACES)) {
			datcon2rec.frequency.set("01");
			callDatcon25300();
		}
		else {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.frequency.set(t6633rec.compfreq);
			datcon4rec.intDate1.set(wsaaNewDate);
			wsaaOccdate.set(wsaaNewDate);
			datcon4rec.freqFactor.set(1);
			datcon4rec.intDate2.set(0);
			datcon4rec.billdayNum.set(wsaaOccDd);
			datcon4rec.billmonthNum.set(wsaaOccMm);
			callDatcon45350();
			datcon2rec.intDate2.set(datcon4rec.intDate2);
		}
		wsaaNewDate.set(datcon2rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet5600();
			loanIO.setNextCapnDate(wsaaNewDate);
		}
		else {
			loanIO.setNextCapnDate(datcon2rec.intDate2);
		}
	}

protected void nextIntBillDate5250()
	{
	if (isEQ(t6633rec.loanAnnivInterest,"Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(lhdrIO.getLoanStartDate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon25300();
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			return ;
		}
		if (isEQ(t6633rec.policyAnnivInterest,"Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(wsaaContractDate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2,lhdrIO.getLoanStartDate()))) {
				callDatcon25300();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}

			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			return ;
		}
		//ICIL-549 start
		if ((cslnd004Permission) 
		&& (isEQ(wsaaCalflag,"N"))){   
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(wsaaLoanDate);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(wsaanofDay);
			callDatcon25300();
			wsaaLoanDate.set(datcon2rec.intDate2);
		}                              
		//ICIL-549 end
		wsaaNewDate.set(ZERO);
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet5600();
		}
		else {
			if (isNE(t6633rec.interestDay,ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewDate);
		datcon2rec.freqFactor.set(1);
		if (isEQ(t6633rec.interestFrequency,SPACES)) {
			datcon2rec.frequency.set("01");
			callDatcon25300();
		}
		else {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.frequency.set(t6633rec.interestFrequency);
			datcon4rec.intDate1.set(wsaaNewDate);
			wsaaOccdate.set(wsaaNewDate);
			datcon4rec.freqFactor.set(1);
			datcon4rec.intDate2.set(0);
			datcon4rec.billdayNum.set(wsaaOccDd);
			datcon4rec.billmonthNum.set(wsaaOccMm);
			callDatcon45350();
			datcon2rec.intDate2.set(datcon4rec.intDate2);
		}
		wsaaNewDate.set(datcon2rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet5600();
			loanIO.setNextIntBillDate(wsaaNewDate);
		}
		else {
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
		}
	}

protected void callDatcon25300()
	{
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void callDatcon45350()
	{
		/*START*/
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void postAcmvs5400()
	{
		start5400();
	}

protected void start5400()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.rdocnum.set(lhdrIO.getChdrnum());
		lifacmvrec1.jrnseq.set(0);
		lifacmvrec1.batccoy.set(lhdrIO.getChdrcoy());
		lifacmvrec1.rldgcoy.set(lhdrIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(lhdrIO.getChdrcoy());
		lifacmvrec1.batckey.set(wsaaBatckey);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.origcurr.set(lhdrIO.getLoanCurrency());
		lifacmvrec1.origamt.set(lhdrIO.getLoanOriginalAmount());
		lifacmvrec1.origamt.subtract(wsaaStamp);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.tranno.set(chdrmjaIO.getTranno());
		lifacmvrec1.frcdate.set(99999999);
		if(cslnd004Permission) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(lhdrIO.getLoanStartDate());
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(wsaanofDay);
			callDatcon25300();
			lifacmvrec1.effdate.set(datcon2rec.intDate2);
		}
		else{
		lifacmvrec1.effdate.set(lhdrIO.getLoanStartDate());
		}
		lifacmvrec1.tranref.set(lhdrIO.getChdrnum());
		lifacmvrec1.substituteCode[1].set(chdrmjaIO.getCnttype());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(lhdrIO.getChdrcoy());
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaT5645Item);
		descIO.setLanguage(atmodrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(lhdrIO.getChdrnum());
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.transactionDate.set(wsaaTranDate);
		lifacmvrec1.transactionTime.set(wsaaTranTime);
		lifacmvrec1.user.set(wsaaTranUser);
		lifacmvrec1.termid.set(wsaaTranTerm);
		lifacmvrec1.sacscode.set(t5645rec.sacscode01);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec1.glcode.set(t5645rec.glmap01);
		lifacmvrec1.glsign.set(t5645rec.sign01);
		lifacmvrec1.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			xxxxFatalError();
		}
		if (isNE(wsaaStamp,ZERO)) {
			lifacmvrec1.origamt.set(wsaaStamp);
			lifacmvrec1.sacscode.set(t5645rec.sacscode03);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec1.glcode.set(t5645rec.glmap03);
			lifacmvrec1.glsign.set(t5645rec.sign03);
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec1.statuz);
				xxxxFatalError();
			}
		}
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(lhdrIO.getChdrnum());
		wsaaRldgLoanno.set(loanIO.getLoanNumber());
		lifacmvrec1.origamt.set(lhdrIO.getLoanOriginalAmount());
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			xxxxFatalError();
		}
	}

protected void deleteLhdr5500()
	{
		/*START*/
		lhdrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, lhdrIO);
		if (isNE(lhdrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lhdrIO.getParams());
			syserrrec.params.set(lhdrIO.getStatuz());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void dateSet5600()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon2*/
		/*  with is a valid from-date... ie IF the interest/capn day in*/
		/*  T6633 is > 28, we have to make sure the from-date isn't*/
		/*  something like 31/02/nnnn or 31/06/nnnn*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
	}

protected void releaseSoftlock6000()
	{
		start6000();
	}

protected void start6000()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}

protected void loanRequestLetter3020(){
	
}
}
