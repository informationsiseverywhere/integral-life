package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Livclmrec extends ExternalData{
	// *******************************
	// Attribute Declarations
	// *******************************
	public FixedLengthStringData statuz = new FixedLengthStringData(8);
	public FixedLengthStringData transEffdate = new FixedLengthStringData(8);
  	public FixedLengthStringData system = new FixedLengthStringData(4);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3);
  	public FixedLengthStringData covcde = new FixedLengthStringData(4);//Input Coverage Code
  	public FixedLengthStringData region = new FixedLengthStringData(2);
  	public FixedLengthStringData locale = new FixedLengthStringData(2);
  	public FixedLengthStringData language = new FixedLengthStringData(1);
  	public FixedLengthStringData trancde = new FixedLengthStringData(4);//Input Transaction Code
  	public FixedLengthStringData waiver  = new FixedLengthStringData(11);
	public FixedLengthStringData lastpaydate  = new FixedLengthStringData(1);
	public PackedDecimalData benefitvalue = new PackedDecimalData(3);
	public FixedLengthStringData setriskstat  = new FixedLengthStringData(2);
	public FixedLengthStringData setpremstat  = new FixedLengthStringData(2);
	public FixedLengthStringData checkriskstat  = new FixedLengthStringData(2);
	public FixedLengthStringData checkpremstat  = new FixedLengthStringData(2);
	public FixedLengthStringData setriskstatall  = new FixedLengthStringData(2);
	public FixedLengthStringData setpremstatall  = new FixedLengthStringData(2);
	public FixedLengthStringData percent  = new FixedLengthStringData(5);
	//IGI - 913, Adding a new field Maxvalid.
	
	public FixedLengthStringData Maxvalid  = new FixedLengthStringData(1);
	public void initialize() {
		COBOLFunctions.initialize(statuz);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());			
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}
