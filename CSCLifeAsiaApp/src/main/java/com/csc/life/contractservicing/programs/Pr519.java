/*
 * File: Pr519.java
 * Date: 30 August 2009 1:35:50
 * Author: Quipoz Limited
 *
 * Class transformed from PR519.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.Sr519ScreenVars;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LfcllnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*            Add Additional Life Assured Details
*
*Note : This module is based on P5005. Much of the code has
*       remained except for that which relates to joint lives.
*
*Initialise
*----------
*
*   Initialise the screen values.
*
*   Retrieve the contract  header  (CHDRMJA) and get the latest
*   life number by reading LIFEMJA.
*
*   Add one to the life number and set up the standard values on
*   the screen.
*
* Validation
* ----------
*
*   Check the client entered against CLTS.
*
*   Validate all fields according to the help except:
*             Sex
*             ANB at RCD
*             Date of birth
*             Occupation
*
*   If CALC pressed (and the client number is valid),
*
*        - overwrite  the   following  from  the  client  details
*             previously read:
*
*             Sex
*             ANB at RCD  - note *
*             Date of birth
*             Occupation
*
*        * only overwrite if  not  mandatory (ANB at RCD required
*             indicator from T5688 no 'R')
*
*   Otherwise CALC was not pressed,
*
*        - validate the remaining fields:
*             Sex - note *
*             ANB at RCD - calculate if not 'required'
*             Date of birth - note *
*             Occupation - default if not entered
*
*        * cannot be different  from  the  client  details if the
*             life is assured  on  any  other  contract. To check
*             this, read the  client  roles  file  (CLRR) for the
*             client number  and  role of life assured (CLRF-LIFE
*             from CLNTRLSREC).  If  there  are any for contracts
*             other than the  current one, this means sex and DoB
*             must be the  same  as  already  held  on the client
*             record.
*
*        * cannot be a client that already exists on the contract
*
* Updating
* --------
*
*   Update the TRANNO on the contract header and rewrite the
*   record CHDRMJA.
*
*   Write the required life.
*
*   Maintain the client roles file (CLRR) as follows:
*
*        - if the client number on  the screen is not the same as
*             the client number  on  the  life record, delete the
*             life role for  the  client  number on the life file
*             and add a new  one  for  the  client  number on the
*             screen.
*
*        - if it is a new life, add a new client role record.
*
*   Add a PTRN record.
*
*   Release the softlock on the contract.
*
* Next
* ----
*
*   Exit to submenu.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr519 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR519");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";

	private FixedLengthStringData wsaa1stTimeIn = new FixedLengthStringData(1).init("Y");
	private Validator n1stTimeIn = new Validator(wsaa1stTimeIn, "Y");
	private String wsaaUpdateClts = "N";

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaForenum, 8).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(wsaaForenum, 10, FILLER).init(SPACES);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);
	private String wsaaNewLife = "N";
	private FixedLengthStringData wsaaClient = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaLifeselMrnf = new FixedLengthStringData(1).init(SPACES);
	private String wsaaOptBlankscr = "N";
	private FixedLengthStringData wsaaDuprCheck = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaJlduprCheck = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaAgeadm = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaLifeNumber = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaLifeAlpha = new FixedLengthStringData(2).isAPartOf(wsaaLifeNumber, 0, REDEFINE);

	private FixedLengthStringData wsaaIfDuplicateCl = new FixedLengthStringData(1).init("N");
	private Validator wsaaDuplicateCl = new Validator(wsaaIfDuplicateCl, "Y");
	private static final String t5688 = "T5688";
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String clrrrec = "CLRRREC";
	private static final String cltsrec = "CLTSREC";
	private static final String ptrnrec = "PTRNREC   ";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private LfcllnbTableDAM lfcllnbIO = new LfcllnbTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5688rec t5688rec = new T5688rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Wssplife wssplife = new Wssplife();
	private Sr519ScreenVars sv = ScreenProgram.getScreenVars( Sr519ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	//ILB-456 start 
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2190,
		readNext2720,
		nextRecord2730,
		exit2790
	}

	public Pr519() {
		super();
		screenVars = sv;
		new ScreenModel("Sr519", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaLifeselMrnf.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.dob.set(varcom.vrcmMaxDate);
		sv.anbage.set(ZERO);
		sv.dummyOut[varcom.nd.toInt()].set("Y");
		/*  Retrieve contract header details*/
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		
		chdrpfDAO.deleteCacheObject(chdrpf);
		chdrpf = chdrpfDAO.getchdrRecordservunit(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (chdrpf == null) {
			fatalError600();
		}
		
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		
		/*  Get the latest Life Record*/
		lifemjaIO.setParams(SPACES);
		lifemjaIO.setChdrcoy(chdrpf.getChdrcoy());
		lifemjaIO.setChdrnum(chdrpf.getChdrnum());
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRNUM");
		callLifemjaio6000();
		while ( !(isEQ(lifemjaIO.getStatuz(), varcom.endp))) {
			callLifemjaio6000();
			if (isNE(lifemjaIO.getChdrnum(), chdrpf.getChdrnum())) {
				lifemjaIO.setStatuz(varcom.endp);
			}
			else {
				wsaaLifeAlpha.set(lifemjaIO.getLife());
				lifemjaIO.setFunction(varcom.nextr);
			}
		}

		/*  Read Contract Definition Table.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrpf.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.e268);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		/*  Assume that joint lives are not applicable.*/
		sv.jlife.set("00");
		sv.rollitOut[varcom.nd.toInt()].set("Y");
		sv.jlifeOut[varcom.nd.toInt()].set("Y");
		sv.relationOut[varcom.nd.toInt()].set("Y");
		/*  Display standard fields*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		wsaaLifeNumber.add(1);
		sv.life.set(wsaaLifeAlpha);
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void cltsio1900()
	{
		/*CLTSIO*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			sv.lifeselErr.set(errorsInner.e339);
		}
		else {
			if (isLTE(cltsIO.getCltdod(), chdrpf.getOccdate())) {
				sv.lifeselErr.set(errorsInner.f782);
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
	}

protected void screenIo2010()
	{
		/*    CALL 'SR519IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SR519-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    Catering for F11.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/*VALIDATE*/
		/*    Validate SECTION.*/
		validateAll2100();
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateAll2100()
	{
		try {
			validateStart2110();
			cltsCheck2120();
			readLfcllnb2125();
			screenValidate2140();
			clientValidate2150();
			clientRoleVal2160();
			screenDefault2170();
			screenVsClnt2180();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateStart2110()
	{
		/* Retrieve client details when the client has been selected       */
		/* via Client Scroll (ie Function key F4 used).                    */
		if (isNE(wsspcomn.clntkey, SPACES)) {
			sv.lifesel.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			sv.lifeselOut[varcom.chg.toInt()].set("Y");
		}
		/* If adding a Life & no Client has been input, redisplay          */
		/* screen with message.                                            */
		if (isEQ(sv.lifesel, SPACES)) {
			sv.lifeselErr.set(errorsInner.e186);
			goTo(GotoLabel.exit2190);
		}
		/* If the client has changed, set the indicator to ensure          */
		/* an attempt is made to read the new client.                      */
		if (isNE(sv.lifesel, wsaaClient)) {
			wsaaClient.set(sv.lifesel);
			sv.lifeselOut[varcom.chg.toInt()].set("Y");
		}
		else {
			sv.lifeselOut[varcom.chg.toInt()].set("N");
		}
		/* The date of birth field on screen SR519 is optional.*/
		/* If the date of birth is entered on the screen and it is*/
		/* different from what is on the client file (CLTS-CLTDOB) -*/
		/* an error message is produced until the correct date of*/
		/* birth has been entered.*/
		/* Before checking the CLTDOB, make sure a client exists.          */
		if (isEQ(sv.lifesel, SPACES)) {
			sv.lifeselErr.set(errorsInner.e186);
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(sv.lifeselOut[varcom.chg.toInt()], "Y")) {
			findClient2300();
			if (isEQ(wsaaLifeselMrnf, "Y")) {
				sv.lifeselErr.set(errorsInner.e339);
				goTo(GotoLabel.exit2190);
			}
		}
		if (isNE(cltsIO.getCltdob(), ZERO)
		&& isNE(cltsIO.getCltdob(), varcom.vrcmMaxDate)) {
			if ((isNE(sv.dob, cltsIO.getCltdob()))
			&& (isNE(sv.dob, SPACES))
			&& (isNE(sv.dob, varcom.vrcmMaxDate))) {
				sv.dobErr.set(errorsInner.g819);
			}
		}
		/*    Validate client number (if changed)*/
		if (isEQ(sv.lifesel, SPACES)
		&& isEQ(sv.jlife, "00")
		&& isNE(sv.life, "00")) {
			sv.lifeselErr.set(errorsInner.e186);
			goTo(GotoLabel.exit2190);
		}
		else {
			if (isNE(cltsIO.getOccpcode(), SPACES)) {
				sv.occup.set(cltsIO.getOccpcode());
			}
		}
		if (isNE(sv.lifeselOut[varcom.chg.toInt()], "Y")) {
			findClient2300();
			if ((isNE(cltsIO.getCltdob(), ZERO)
			&& isNE(cltsIO.getCltdob(), varcom.vrcmMaxDate))
			&& isNE(cltsIO.getCltdob(), sv.dob)
			&& isEQ(cltsIO.getStatuz(), varcom.oK)
			&& !n1stTimeIn.isTrue()) {
				/*   Check if life already has a contract.*/
				readLfcllnb2125();
				if (isEQ(sv.dob, ZERO)
				|| isEQ(sv.dob, varcom.vrcmMaxDate)) {
					sv.dob.set(cltsIO.getCltdob());
				}
				else {
					if (isNE(lfcllnbIO.getStatuz(), varcom.mrnf)) {
						if (isNE(cltsIO.getCltdob(), sv.dob)) {
							sv.dobErr.set(errorsInner.h367);
						}
					}
					else {
						wsaa1stTimeIn.set("N");
						sv.dob.set(cltsIO.getCltdob());
					}
				}
			}
			else {
				if (isNE(cltsIO.getOccpcode(), SPACES)
				&& isEQ(cltsIO.getStatuz(), varcom.oK)) {
					sv.occup.set(cltsIO.getOccpcode());
				}
			}
		}
		wsaa1stTimeIn.set("N");
	}

protected void cltsCheck2120()
	{
		if (isNE(cltsIO.getClttype(), "P")) {
			sv.lifeselErr.set(errorsInner.g844);
			goTo(GotoLabel.exit2190);
		}
		else {
			if (isNE(cltsIO.getCltind(), "C")) {
				sv.lifeselErr.set(errorsInner.g499);
				goTo(GotoLabel.exit2190);
			}
		}
	}

protected void readLfcllnb2125()
	{
		lfcllnbIO.setParams(SPACES);
		lfcllnbIO.setFunction(varcom.readr);
		lfcllnbIO.setChdrcoy(wsspcomn.fsuco);
		lfcllnbIO.setLifcnum(sv.lifesel);
		SmartFileCode.execute(appVars, lfcllnbIO);
		if (isEQ(sv.lifeselErr, errorsInner.h075)
		|| isEQ(sv.lifeselErr, errorsInner.h076)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
	}

protected void screenValidate2140()
	{
		/*    Validate all screen fields for correctness.*/
		validateFields2500();
		if (isEQ(sv.sex, SPACES)) {
			sv.sex.set(cltsIO.getCltsex());
		}
		if (((isEQ(sv.anbage, ZERO)
		|| isEQ(sv.anbage, SPACES))
		&& isEQ(t5688rec.anbrqd, "R"))) {
			sv.anbageErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		/*    Due to windowing the Client must be found if no change in*/
		/*       life number.*/
		/*    Confused? well this is due to the fact that windowing does*/
		/*       not set the change (chg) flag. So we must check just in*/
		/*       case.*/
		if (isNE(sv.lifeselOut[varcom.chg.toInt()], "Y")) {
			findClient2300();
		}
	}

protected void clientValidate2150()
	{
		/*    If client not found then exit*/
		/*    else default the set client details to the screen.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit2190);
		}
		else {
			if (isNE(sv.lifesel, SPACES)
			&& (isNE(sv.lifeselErr, errorsInner.h075)
			|| isNE(sv.lifeselErr, errorsInner.h076))) {
				screenDefault2170();
			}
		}
		/*    Check Age next birthday.*/
		if (((isEQ(sv.anbage, ZERO)
		|| isEQ(sv.anbage, SPACES))
		&& isEQ(t5688rec.anbrqd, "R"))) {
			sv.anbageErr.set(errorsInner.e186);
		}
	}

protected void clientRoleVal2160()
	{
		/*    Find the Life's Client Role if it exists.*/
		wsaaIfDuplicateCl.set("N");
		checkRoles2700();
		/*    Make sure that it is not a duplicate client on the*/
		/*    same contract.*/
		if (wsaaDuplicateCl.isTrue()) {
			sv.lifeselErr.set(errorsInner.rl07);
			goTo(GotoLabel.exit2190);
		}
		wsaaForenum.set(clrrIO.getForenum());
		if (isEQ(wsaaChdrnum, lifemjaIO.getChdrnum())) {
			goTo(GotoLabel.exit2190);
		}
		else {
			screenVsClnt2180();
		}
	}

protected void screenDefault2170()
	{
		/*    Move Client details to the screen provided there is a life*/
		/*    to which the defaults can be assigned.*/
		if (isEQ(sv.sex, SPACES)) {
			sv.sex.set(cltsIO.getCltsex());
		}
		if (isEQ(sv.occup, SPACES)) {
			sv.occup.set(cltsIO.getOccpcode());
		}
		if (isNE(cltsIO.getCltdob(), varcom.vrcmMaxDate)
		&& isEQ(sv.dobErr, SPACES)
		&& isEQ(sv.dob, ZERO)
		|| isEQ(sv.dob, varcom.vrcmMaxDate)) {
			sv.dob.set(cltsIO.getCltdob());
		}
		sv.anbageErr.set(SPACES);
		calculateAnb2600();
	}

protected void screenVsClnt2180()
	{
		/*    Screen Fields validated against the clients details.*/
		if (isNE(sv.sex, cltsIO.getCltsex())
		&& isNE(cltsIO.getCltsex(), SPACES)
		&& isNE(sv.sex, SPACES)) {
			sv.sexErr.set(errorsInner.g983);
			sv.sex.set(SPACES);
		}
	}

protected void findClient2300()
	{
		checkClient2310();
	}

protected void checkClient2310()
	{
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.lifesel);
		cltsio1900();
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			sv.lifename.set(SPACES);
			wsaaLifeselMrnf.set("Y");
		}
		else {
			wsaaLifeselMrnf.set("N");
			plainname();
			sv.lifename.set(wsspcomn.longconfname);
		}
	}

protected void validateFields2500()
	{
		validateFields2510();
	}

protected void validateFields2510()
	{
		/* if SOE & CLTDOB exist display 'Age admitted'*/
		if (isEQ(cltsIO.getSoe(), SPACES)
		|| isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)
		|| isEQ(cltsIO.getCltdob(), ZERO)) {
			sv.dummyOut[varcom.nd.toInt()].set("Y");
			wsaaAgeadm.set(SPACES);
		}
		else {
			sv.dummyOut[varcom.nd.toInt()].set(SPACES);
			if (isEQ(wsaaAgeadm, SPACES)) {
				wsaaAgeadm.set("X");
			}
		}
		if (isEQ(sv.dob, varcom.vrcmMaxDate)) {
			if (isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)) {
				sv.dobErr.set(errorsInner.e186);
			}
			else {
				sv.dob.set(cltsIO.getCltdob());
			}
		}
		if (isEQ(sv.selection, SPACES)) {
			sv.selectionErr.set(errorsInner.e186);
		}
		if (isEQ(sv.occup, SPACES)
		&& isEQ(t5688rec.occrqd, "R")) {
			if (isNE(cltsIO.getOccpcode(), SPACES)) {
				sv.occup.set(cltsIO.getOccpcode());
			}
			else {
				sv.occupErr.set(errorsInner.e186);
			}
		}
		if (isEQ(sv.smoking, SPACES)
		&& isEQ(t5688rec.smkrqd, "R")) {
			sv.smokingErr.set(errorsInner.e186);
		}
		/*    CHECK RELATIONSHIP  (FOR JOINT LIFE ONLY)*/
		/* check age next birthday.*/
		if (isNE(sv.anbage, ZERO)) {
			wsaaAnb.set(sv.anbage);
			calculateAnb2600();
			if (isNE(sv.anbage, wsaaAnb)) {
				sv.anbage.set(wsaaAnb);
				sv.anbageErr.set(errorsInner.h015);
			}
		}
	}

protected void calculateAnb2600()
	{
		calculateAnb2610();
	}

protected void calculateAnb2610()
	{
		if (isEQ(sv.dob, varcom.vrcmMaxDate)) {
			return ;
		}
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(sv.dob);
		agecalcrec.intDate2.set(chdrpf.getOccdate());
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isEQ(agecalcrec.statuz, "IVFD")) {
			sv.dobErr.set(errorsInner.f401);
			return ;
		}
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			syserrrec.params.set(agecalcrec.agecalcRec);
			fatalError600();
		}
		sv.anbage.set(agecalcrec.agerating);
	}

protected void checkRoles2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					checkRoles2710();
				case readNext2720:
					readNext2720();
				case nextRecord2730:
					nextRecord2730();
				case exit2790:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkRoles2710()
	{
		clrrIO.setParams(SPACES);
		clrrIO.setClntpfx("CN");
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClntnum(sv.lifesel);
		clrrIO.setClrrrole("LF");
		clrrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		clrrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clrrIO.setFitKeysSearch("CLNTNUM", "CLRRROLE");
		clrrIO.setFormat(clrrrec);
	}

protected void readNext2720()
	{
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)
		&& isNE(clrrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clrrIO.getParams());
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
		if (isNE(sv.lifesel, clrrIO.getClntnum())
		|| isNE(clrrIO.getClrrrole(), "LF")) {
			clrrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2790);
		}
		/* If CLRR record read 'U'sed to be valid, read the next CLRR rec.*/
		if (isNE(clrrIO.getUsedToBe(), SPACES)) {
			goTo(GotoLabel.nextRecord2730);
		}
		if (isEQ(sv.lifesel, clrrIO.getClntnum())) {
			if (isEQ(chdrpf.getChdrnum(), subString(clrrIO.getForenum(), 1, 8))) {
				wsaaIfDuplicateCl.set("Y");
				clrrIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit2790);
			}
		}
		if (isEQ(clrrIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2790);
		}
	}

protected void nextRecord2730()
	{
		clrrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readNext2720);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, "****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/*    Catering for F11.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		wsaaUpdateClts = "N";
		compute(wsaaNewTranno, 0).set(add(1,chdrpf.getTranno()));
		//ILB-456
		boolean flag = chdrpfDAO.updateChdrValidflag(chdrpf, "2");
		if (flag == false) {
			fatalError600();
		}
		chdrpf.setStatcode(chdrpf.getStatcode());
		chdrpf.setCurrfrom(datcon1rec.intDate.toInt());
		chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpf.setValidflag("1".charAt(0));
		chdrpf.setTranlused(chdrpf.getTranno());
		chdrpf.setTranno(wsaaNewTranno.toInt());
		//ILB-456
		chdrpfDAO.insertChdrRecords(chdrpf);
		/*chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		writeLifemja3100();
		maintainRoles3200();
		checkInitialValues3300();
		checkChanges3400();
		updates3500();
		writePtrn3600();
		callBldenrl3700();
		/* Release Soft locked record.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void writeLifemja3100()
	{
		para3101();
		continue3120();
	}

protected void para3101()
	{
		/*    IF THE LIFEMJA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		/*      SET UP THE KEY*/
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(sv.chdrnum);
		lifemjaIO.setLife(sv.life);
		lifemjaIO.setJlife(sv.jlife);
		lifemjaIO.setTranno(wsaaNewTranno);
		lifemjaIO.setCurrfrom(chdrpf.getCurrfrom());
		lifemjaIO.setCurrto(chdrpf.getCurrto());
		lifemjaIO.setValidflag("1");
		lifemjaIO.setStatcode("IF");
		lifemjaIO.setLifeCommDate(chdrpf.getOccdate());
		lifemjaIO.setTransactionDate(varcom.vrcmDate);
		lifemjaIO.setTransactionTime(varcom.vrcmTime);
		lifemjaIO.setUser(varcom.vrcmUser);
		lifemjaIO.setTermid(varcom.vrcmTermid);
	}

protected void continue3120()
	{
		lifemjaIO.setLifcnum(sv.lifesel);
		lifemjaIO.setCltsex(sv.sex);
		lifemjaIO.setCltdob(sv.dob);
		lifemjaIO.setAnbAtCcd(sv.anbage);
		lifemjaIO.setSelection(sv.selection);
		lifemjaIO.setLiferel(sv.relation);
		lifemjaIO.setAgeadm(wsaaAgeadm);
		lifemjaIO.setSmoking(sv.smoking);
		if (isNE(sv.occup, SPACES)) {
			if (isNE(sv.occup, lifemjaIO.getOccup())) {
				if (isEQ(lifemjaIO.getOccup(), SPACES)) {
					lifemjaIO.setOccup(sv.occup);
				}
			}
		}
		lifemjaIO.setPursuit01(sv.pursuit01);
		lifemjaIO.setPursuit02(sv.pursuit02);
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.writr);
		callLifemjaio6000();
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void maintainRoles3200()
	{
		addRole3240();
	}

protected void addRole3240()
	{
		cltrelnrec.data.set(SPACES);
		cltrelnrec.clrrrole.set("LF");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(wsspcomn.company);
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(lifemjaIO.getChdrnum());
		wsaaLife.set(lifemjaIO.getLife());
		cltrelnrec.forenum.set(wsaaForenum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.lifesel);
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			fatalError600();
		}
	}

protected void checkInitialValues3300()
	{
		para3301();
	}

protected void para3301()
	{
		lifemjaIO.setTranno(wsaaNewTranno);
		lifemjaIO.setCurrfrom(chdrpf.getCurrfrom());
		lifemjaIO.setCurrto(chdrpf.getCurrto());
		lifemjaIO.setValidflag("1");
		lifemjaIO.setStatcode("IF");
		lifemjaIO.setLifeCommDate(chdrpf.getOccdate());
		lifemjaIO.setTransactionDate(varcom.vrcmDate);
		lifemjaIO.setTransactionTime(varcom.vrcmTime);
		lifemjaIO.setUser(varcom.vrcmUser);
		lifemjaIO.setTermid(varcom.vrcmTermid);
		lifemjaIO.setLifcnum(sv.lifesel);
		lifemjaIO.setCltsex(sv.sex);
		lifemjaIO.setCltdob(sv.dob);
		lifemjaIO.setAnbAtCcd(sv.anbage);
		lifemjaIO.setSelection(sv.selection);
		lifemjaIO.setLiferel(sv.relation);
		lifemjaIO.setAgeadm(wsaaAgeadm);
		lifemjaIO.setSmoking(sv.smoking);
		if (isNE(sv.occup, SPACES)) {
			if (isNE(sv.occup, lifemjaIO.getOccup())) {
				if (isEQ(lifemjaIO.getOccup(), SPACES)) {
					lifemjaIO.setOccup(sv.occup);
				}
			}
		}
		lifemjaIO.setPursuit01(sv.pursuit01);
		lifemjaIO.setPursuit02(sv.pursuit02);
	}

protected void checkChanges3400()
	{
		para3401();
	}

protected void para3401()
	{
		if (isNE(sv.sex, cltsIO.getCltsex())) {
			cltsIO.setCltsex(sv.sex);
			wsaaUpdateClts = "Y";
		}
		if (isNE(sv.occup, cltsIO.getOccpcode())
		&& isNE(sv.occup, SPACES)) {
			if (isEQ(cltsIO.getOccpcode(), SPACES)) {
				cltsIO.setOccpcode(sv.occup);
				wsaaUpdateClts = "Y";
			}
		}
		if (isNE(sv.dob, cltsIO.getCltdob())) {
			if (isEQ(cltsIO.getCltdob(), ZERO)
			|| isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)) {
				cltsIO.setCltdob(sv.dob);
				wsaaUpdateClts = "Y";
			}
		}
	}

protected void updates3500()
	{
		updates3520();
	}

protected void updates3520()
	{
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(sv.chdrnum);
		lifemjaIO.setLife(sv.life);
		lifemjaIO.setJlife(sv.jlife);
		lifemjaIO.setFunction(varcom.readh);
		callLifemjaio6000();
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/* CALL FOR UPDAT                                                  */
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.updat);
		callLifemjaio6000();
		if (isEQ(wsaaUpdateClts, "Y")) {
			cltsIO.setFunction(varcom.updat);
			callCltsio3900();
		}
	}

protected void writePtrn3600()
	{
		ptrnRecord3610();
	}

protected void ptrnRecord3610()
	{
		/* Write a transaction record to PTRN file*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcpfx(wsaaBatckey.batcBatcpfx);
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setChdrnum(chdrpf.getChdrnum());
		ptrnIO.setTranno(wsaaNewTranno);
		/* Get Todays Date.*/
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void callBldenrl3700()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrpf.getChdrpfx());
		bldenrlrec.company.set(chdrpf.getChdrcoy());
		bldenrlrec.uentity.set(chdrpf.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callCltsio3900()
	{
		/*PARA*/
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.lifesel);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		/*    Catering for F11.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callLifemjaio6000()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.endp)) {
			syserrrec.dbparams.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e268 = new FixedLengthStringData(4).init("E268");
	private FixedLengthStringData e339 = new FixedLengthStringData(4).init("E339");
	private FixedLengthStringData g844 = new FixedLengthStringData(4).init("G844");
	private FixedLengthStringData g819 = new FixedLengthStringData(4).init("G819");
	private FixedLengthStringData g983 = new FixedLengthStringData(4).init("G983");
	private FixedLengthStringData h015 = new FixedLengthStringData(4).init("H015");
	private FixedLengthStringData f782 = new FixedLengthStringData(4).init("F782");
	private FixedLengthStringData h367 = new FixedLengthStringData(4).init("H367");
	private FixedLengthStringData g499 = new FixedLengthStringData(4).init("G499");
	private FixedLengthStringData h075 = new FixedLengthStringData(4).init("H075");
	private FixedLengthStringData h076 = new FixedLengthStringData(4).init("H076");
	private FixedLengthStringData f401 = new FixedLengthStringData(4).init("F401");
	private FixedLengthStringData rl07 = new FixedLengthStringData(4).init("RL07");
}
}