package com.csc.life.contractservicing.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class TaxdpfDAOImpl extends BaseDAOImpl<Taxdpf>implements TaxdpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(TaxdpfDAOImpl.class);
	
	 /*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/    
	public List<Taxdpf> readTaxdbilData(Taxdpf taxdbilpfModel){
			
		Taxdpf taxdbilpf = null;
			List<Taxdpf> taxdbilpfReadResult = new LinkedList<Taxdpf>();
			PreparedStatement stmn = null;
			ResultSet rs = null;
			StringBuilder sqlStringBuilder = new StringBuilder();
			
			sqlStringBuilder.append("SELECT TXABSIND01, TXABSIND02, TXABSIND03 TAXAMT01 ,TAXAMT02, TAXAMT03 FROM TAXDBIL ");
			//sqlStringBuilder.append("TAXDBIL");
			sqlStringBuilder.append("WHERE TRANNO =? AND CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND RIDER=? AND COVERAGE=? AND PLANSFX=? AND TRANTYPE=? AND INSTFROM=?");
			stmn = getPrepareStatement(sqlStringBuilder.toString());
			try {
				stmn.setInt(1, taxdbilpfModel.getTranno());
				stmn.setString(2, taxdbilpfModel.getChdrcoy());
				stmn.setString(3, taxdbilpfModel.getChdrnum());
				stmn.setString(4, taxdbilpfModel.getLife());
				stmn.setString(5, taxdbilpfModel.getRider());
				stmn.setString(6, taxdbilpfModel.getCoverage());
				stmn.setInt(7, taxdbilpfModel.getPlansfx());
				stmn.setString(8, taxdbilpfModel.getTrantype());
				stmn.setInt(9, taxdbilpfModel.getInstfrom());
				rs = executeQuery(stmn);
				while (rs.next()) {
					taxdbilpf = new Taxdpf();
					taxdbilpfReadResult.add(taxdbilpf);
				}

			} catch (SQLException e) {
				LOGGER.error("readTaxdbilData()", e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(stmn, rs);
			}
			return taxdbilpfReadResult;
		
				
		}
	
	public List<Taxdpf> searchTaxdpfRecord(Taxdpf taxdpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
		sqlSelect.append("PLANSFX, EFFDATE, TRANREF, INSTFROM, INSTTO, ");
		sqlSelect.append("BILLCD, TRANNO, BASEAMT, TRANTYPE, TAXAMT01, ");
		sqlSelect.append("TAXAMT02, TAXAMT03, TXABSIND01, TXABSIND02, TXABSIND03, ");
		sqlSelect.append("TXTYPE01, TXTYPE02, TXTYPE03, POSTFLG, USRPRF, ");
		sqlSelect.append("JOBNM, DATIME ");
		sqlSelect.append("FROM TAXDPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND INSTFROM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLANSFX = ? ");
		if(taxdpf.getTrantype() != null)
		{
			sqlSelect.append("AND TRANTYPE = ? ");
		}
		if(taxdpf.getTranno() != 0)
		{
			sqlSelect.append("AND TRANNO = ? ");
		}
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, INSTFROM DESC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLANSFX ASC, TRANTYPE ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psTaxdpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlTaxdpfRs = null;
		List<Taxdpf> outputList = new ArrayList<Taxdpf>();

		try {
            int i =0;  
			psTaxdpfSelect.setString(++i, taxdpf.getChdrcoy());
			psTaxdpfSelect.setString(++i, taxdpf.getChdrnum());
			psTaxdpfSelect.setInt(++i, taxdpf.getInstfrom());
			psTaxdpfSelect.setString(++i, taxdpf.getLife());
			psTaxdpfSelect.setString(++i, taxdpf.getCoverage());
			psTaxdpfSelect.setString(++i, taxdpf.getRider());
			psTaxdpfSelect.setInt(++i, taxdpf.getPlansfx());
			if(taxdpf.getTrantype() != null)
			{
				psTaxdpfSelect.setString(++i, taxdpf.getTrantype());
			}
			
			if(taxdpf.getTranno() != 0)
			{
				psTaxdpfSelect.setInt(++i, taxdpf.getTranno());

			}
			sqlTaxdpfRs = executeQuery(psTaxdpfSelect);

			while (sqlTaxdpfRs.next()) {

				taxdpf = new Taxdpf();

				taxdpf.setUnique_number(sqlTaxdpfRs.getInt("UNIQUE_NUMBER"));
				taxdpf.setChdrcoy(sqlTaxdpfRs.getString("CHDRCOY"));
				taxdpf.setChdrnum(sqlTaxdpfRs.getString("CHDRNUM"));
				taxdpf.setLife(sqlTaxdpfRs.getString("LIFE"));
				taxdpf.setCoverage(sqlTaxdpfRs.getString("COVERAGE"));
				taxdpf.setRider(sqlTaxdpfRs.getString("RIDER"));
				taxdpf.setPlansfx(sqlTaxdpfRs.getInt("PLANSFX"));
				taxdpf.setEffdate(sqlTaxdpfRs.getInt("EFFDATE"));
				taxdpf.setTranref(sqlTaxdpfRs.getString("TRANREF"));
				taxdpf.setInstfrom(sqlTaxdpfRs.getInt("INSTFROM"));
				taxdpf.setInstto(sqlTaxdpfRs.getInt("INSTTO"));
				taxdpf.setBillcd(sqlTaxdpfRs.getInt("BILLCD"));
				taxdpf.setTranno(sqlTaxdpfRs.getInt("TRANNO"));
				taxdpf.setBaseamt(sqlTaxdpfRs.getBigDecimal("BASEAMT"));
				taxdpf.setTrantype(sqlTaxdpfRs.getString("TRANTYPE"));
				taxdpf.setTaxamt01(sqlTaxdpfRs.getBigDecimal("TAXAMT01"));
				taxdpf.setTaxamt02(sqlTaxdpfRs.getBigDecimal("TAXAMT02"));
				taxdpf.setTaxamt03(sqlTaxdpfRs.getBigDecimal("TAXAMT03"));
				taxdpf.setTxabsind01(sqlTaxdpfRs.getString("TXABSIND01"));
				taxdpf.setTxabsind02(sqlTaxdpfRs.getString("TXABSIND02"));
				taxdpf.setTxabsind03(sqlTaxdpfRs.getString("TXABSIND03"));
				taxdpf.setTxtype01(sqlTaxdpfRs.getString("TXTYPE01"));
				taxdpf.setTxtype02(sqlTaxdpfRs.getString("TXTYPE02"));
				taxdpf.setTxtype03(sqlTaxdpfRs.getString("TXTYPE03"));
				taxdpf.setPostflg(sqlTaxdpfRs.getString("POSTFLG"));
				taxdpf.setUsrprf(sqlTaxdpfRs.getString("USRPRF"));
				taxdpf.setJobnm(sqlTaxdpfRs.getString("JOBNM"));
				taxdpf.setDatime(sqlTaxdpfRs.getDate("DATIME"));

				outputList.add(taxdpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchTaxdpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psTaxdpfSelect, sqlTaxdpfRs);
		}

		return outputList;
	}
	
	public Map<String, List<Taxdpf>> searchTaxdRecord(String coy,List<String> chdrnumlist) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLANSFX,EFFDATE,TRANREF,INSTFROM,INSTTO,BILLCD,TRANNO,BASEAMT,TRANTYPE,TAXAMT01,TAXAMT02,TAXAMT03,TXABSIND01,TXABSIND02,TXABSIND03,TXTYPE01,TXTYPE02,TXTYPE03,POSTFLG FROM TAXDPF WHERE CHDRCOY=? AND ")
				.append(getSqlInStr("CHDRNUM", chdrnumlist))
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, INSTFROM DESC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLANSFX ASC, TRANTYPE ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		Map<String, List<Taxdpf>> taxdMap = new HashMap<String, List<Taxdpf>>();
		try {
			ps.setString(1, coy);
			rs = executeQuery(ps);

			while (rs.next()) {
				Taxdpf taxdpf = new Taxdpf();
				taxdpf.setUnique_number(rs.getLong(1));
				taxdpf.setChdrcoy(rs.getString(2));
				taxdpf.setChdrnum(rs.getString(3));
				taxdpf.setLife(rs.getString(4) != null ? rs.getString(4) : "");
				taxdpf.setCoverage(rs.getString(5) != null ? rs.getString(5) : "");
				taxdpf.setRider(rs.getString(6) != null ? rs.getString(6) : "");
				taxdpf.setPlansfx(rs.getInt(7));
				taxdpf.setEffdate(rs.getInt(8));
				taxdpf.setTranref(rs.getString(9));
				taxdpf.setInstfrom(rs.getInt(10));
				taxdpf.setInstto(rs.getInt(11));
				taxdpf.setBillcd(rs.getInt(12));
				taxdpf.setTranno(rs.getInt(13));
				taxdpf.setBaseamt(rs.getBigDecimal(14));
				taxdpf.setTrantype(rs.getString(15));
				taxdpf.setTaxamt01(rs.getBigDecimal(16));
				taxdpf.setTaxamt02(rs.getBigDecimal(17));
				taxdpf.setTaxamt03(rs.getBigDecimal(18));

				taxdpf.setTxabsind01(rs.getString(19));
				taxdpf.setTxabsind02(rs.getString(20));
				taxdpf.setTxabsind03(rs.getString(21) != null ? rs.getString(21) : "");
				taxdpf.setTxtype01(rs.getString(22));
				taxdpf.setTxtype02(rs.getString(23));
				taxdpf.setTxtype03(rs.getString(24) != null ? rs.getString(24) : "");
				taxdpf.setPostflg(rs.getString(25) != null ? rs.getString(25) : "");


				String chdrnum = taxdpf.getChdrnum();
				if (taxdMap.containsKey(chdrnum)) {
					taxdMap.get(chdrnum).add(taxdpf);
				} else {
					List<Taxdpf> taxdfSearchResult = new LinkedList<Taxdpf>();
					taxdfSearchResult.add(taxdpf);
					taxdMap.put(chdrnum, taxdfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchTaxdRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return taxdMap;
	}   
	
    public void updateTaxdpf(List<Taxdpf> taxdpfList) {
        if (taxdpfList != null && taxdpfList.size() > 0) {
            String SQL_UPDATE = "UPDATE TAXDPF SET POSTFLG='P',JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement pstaxdUpdate = getPrepareStatement(SQL_UPDATE);
            try {
                for (Taxdpf c : taxdpfList) {
                    pstaxdUpdate.setString(1, getJobnm());
                    pstaxdUpdate.setString(2, getUsrprf());
                    pstaxdUpdate.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                    pstaxdUpdate.setLong(4, c.getUnique_number());
                    pstaxdUpdate.addBatch();
                }
                pstaxdUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateTaxdpf()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(pstaxdUpdate, null);
            }
        }
    }	
    
	public boolean insertTaxdPF(List<Taxdpf> taxdList){
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.TAXDPF(CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLANSFX, EFFDATE, TRANREF, INSTFROM, INSTTO, BILLCD, TRANNO, BASEAMT, TRANTYPE, TAXAMT01, TAXAMT02, TAXAMT03, TXABSIND01, TXABSIND02, TXABSIND03, TXTYPE01, TXTYPE02, TXTYPE03, POSTFLG, USRPRF, JOBNM, DATIME)  "); 
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			int seq;
            for (Taxdpf taxd : taxdList) {
            	seq=0;
				ps.setString(++seq, taxd.getChdrcoy());
				ps.setString(++seq, taxd.getChdrnum());
			    ps.setString(++seq, taxd.getLife());
			    ps.setString(++seq, taxd.getCoverage());
			    ps.setString(++seq, taxd.getRider());	
			    ps.setInt(++seq, taxd.getPlansfx());	
			    ps.setInt(++seq, taxd.getEffdate());
			    ps.setString(++seq, taxd.getTranref());	
				ps.setInt(++seq, taxd.getInstfrom());
				ps.setInt(++seq, taxd.getInstto());
				ps.setInt(++seq, taxd.getBillcd());
				ps.setInt(++seq, taxd.getTranno());
			    ps.setBigDecimal(++seq, taxd.getBaseamt()); 	
			    ps.setString(++seq, taxd.getTrantype());	
			    ps.setBigDecimal(++seq, taxd.getTaxamt01()); 
			    ps.setBigDecimal(++seq, taxd.getTaxamt02());
			    ps.setBigDecimal(++seq, taxd.getTaxamt03());
				ps.setString(++seq, taxd.getTxabsind01());
				ps.setString(++seq, taxd.getTxabsind02());
				ps.setString(++seq, taxd.getTxabsind03());		
				ps.setString(++seq, taxd.getTxtype01());
				ps.setString(++seq, taxd.getTxtype02());
				ps.setString(++seq, taxd.getTxtype03());	
				ps.setString(++seq, taxd.getPostflg());
			    ps.setString(++seq, this.getUsrprf());
			    ps.setString(++seq, this.getJobnm());
			    ps.setTimestamp(++seq, new Timestamp(System.currentTimeMillis()));								   		    			    		    		   
			    ps.addBatch();
            }		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("insertTaxdPF()",e);//IJTI-1485
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return isInsertSuccessful;
	}	
	
	// Ticket#ILIFE-4404
	public Map<String, List<Taxdpf>> searchTaxdbilRecord(List<String> chdrnumlist) {
		
		if(chdrnumlist == null || chdrnumlist.isEmpty()){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLANSFX,EFFDATE,TRANREF,INSTFROM,INSTTO,BILLCD,TRANNO,BASEAMT,TRANTYPE,TAXAMT01,TAXAMT02,TAXAMT03,TXABSIND01,TXABSIND02,TXABSIND03,TXTYPE01,TXTYPE02,TXTYPE03,POSTFLG ");
		sql.append(" FROM TAXDPF WHERE INSTTO <> 99999999 AND " );
		sql.append(getSqlInStr("CHDRNUM", chdrnumlist));
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, INSTFROM DESC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLANSFX ASC, TRANTYPE ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		Map<String, List<Taxdpf>> taxdMap = new HashMap<String, List<Taxdpf>>();
		try {
			rs = executeQuery(ps);

			while (rs.next()) {
				Taxdpf taxdpf = new Taxdpf();
				taxdpf.setUnique_number(rs.getLong(1));
				taxdpf.setChdrcoy(rs.getString(2));
				taxdpf.setChdrnum(rs.getString(3));
				taxdpf.setLife(rs.getString(4) != null ? rs.getString(4) : "");
				taxdpf.setCoverage(rs.getString(5) != null ? rs.getString(5) : "");
				taxdpf.setRider(rs.getString(6) != null ? rs.getString(6) : "");
				taxdpf.setPlansfx(rs.getInt(7));
				taxdpf.setEffdate(rs.getInt(8));
				taxdpf.setTranref(rs.getString(9));
				taxdpf.setInstfrom(rs.getInt(10));
				taxdpf.setInstto(rs.getInt(11));
				taxdpf.setBillcd(rs.getInt(12));
				taxdpf.setTranno(rs.getInt(13));
				taxdpf.setBaseamt(rs.getBigDecimal(14));
				taxdpf.setTrantype(rs.getString(15));
				taxdpf.setTaxamt01(rs.getBigDecimal(16));
				taxdpf.setTaxamt02(rs.getBigDecimal(17));
				taxdpf.setTaxamt03(rs.getBigDecimal(18));

				taxdpf.setTxabsind01(rs.getString(19));
				taxdpf.setTxabsind02(rs.getString(20));
				taxdpf.setTxabsind03(rs.getString(21) != null ? rs.getString(21) : "");
				taxdpf.setTxtype01(rs.getString(22));
				taxdpf.setTxtype02(rs.getString(23));
				taxdpf.setTxtype03(rs.getString(24) != null ? rs.getString(24) : "");
				taxdpf.setPostflg(rs.getString(25) != null ? rs.getString(25) : "");


				String chdrnum = taxdpf.getChdrnum();
				if (taxdMap.containsKey(chdrnum)) {
					taxdMap.get(chdrnum).add(taxdpf);
				} else {
					List<Taxdpf> taxdfSearchResult = new LinkedList<Taxdpf>();
					taxdfSearchResult.add(taxdpf);
					taxdMap.put(chdrnum, taxdfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchTaxdbilRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return taxdMap;
	}   
	
	@Override
	public List<Taxdpf> searchTaxdbilRecordByChdr(String coy, String chdrnum, Integer instFrom) {
		
		Taxdpf taxdbilpf = null;
		List<Taxdpf> taxdbilpfReadResult = new LinkedList<Taxdpf>();
		PreparedStatement stmn = null;
		ResultSet sqltaxdbilpfRs = null;
		StringBuilder sqlStringBuilder = new StringBuilder();
		
		sqlStringBuilder.append("SELECT UNIQUE_NUMBER, ");
		sqlStringBuilder.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
		sqlStringBuilder.append("PLANSFX, EFFDATE, TRANREF, INSTFROM, INSTTO, ");
		sqlStringBuilder.append("BILLCD, TRANNO, BASEAMT, TRANTYPE, TAXAMT01, ");
		sqlStringBuilder.append("TAXAMT02, TAXAMT03, TXABSIND01, TXABSIND02, TXABSIND03, ");
		sqlStringBuilder.append("TXTYPE01, TXTYPE02, TXTYPE03, POSTFLG, USRPRF, ");
		sqlStringBuilder.append("JOBNM, DATIME  FROM TAXDBIL ");
		//sqlStringBuilder.append("TAXDBIL");
		sqlStringBuilder.append("WHERE CHDRCOY=? AND CHDRNUM=? AND TRANTYPE IN ('PREM','CNTF') AND INSTFROM=?");
		stmn = getPrepareStatement(sqlStringBuilder.toString());
		try {
			stmn.setString(1, coy);
			stmn.setString(2, chdrnum);
			stmn.setInt(3, instFrom);
			sqltaxdbilpfRs = executeQuery(stmn);
			while (sqltaxdbilpfRs.next()) {
				taxdbilpf = new Taxdpf();
				taxdbilpf.setUnique_number(sqltaxdbilpfRs.getInt("UNIQUE_NUMBER"));
				taxdbilpf.setChdrcoy(sqltaxdbilpfRs.getString("CHDRCOY"));
				taxdbilpf.setChdrnum(sqltaxdbilpfRs.getString("CHDRNUM"));
				taxdbilpf.setLife(sqltaxdbilpfRs.getString("LIFE"));
				taxdbilpf.setCoverage(sqltaxdbilpfRs.getString("COVERAGE"));
				taxdbilpf.setRider(sqltaxdbilpfRs.getString("RIDER"));
				taxdbilpf.setPlansfx(sqltaxdbilpfRs.getInt("PLANSFX"));
				taxdbilpf.setEffdate(sqltaxdbilpfRs.getInt("EFFDATE"));
				taxdbilpf.setTranref(sqltaxdbilpfRs.getString("TRANREF"));
				taxdbilpf.setInstfrom(sqltaxdbilpfRs.getInt("INSTFROM"));
				taxdbilpf.setInstto(sqltaxdbilpfRs.getInt("INSTTO"));
				taxdbilpf.setBillcd(sqltaxdbilpfRs.getInt("BILLCD"));
				taxdbilpf.setTranno(sqltaxdbilpfRs.getInt("TRANNO"));
				taxdbilpf.setBaseamt(sqltaxdbilpfRs.getBigDecimal("BASEAMT"));
				taxdbilpf.setTrantype(sqltaxdbilpfRs.getString("TRANTYPE"));
				taxdbilpf.setTaxamt01(sqltaxdbilpfRs.getBigDecimal("TAXAMT01"));
				taxdbilpf.setTaxamt02(sqltaxdbilpfRs.getBigDecimal("TAXAMT02"));
				taxdbilpf.setTaxamt03(sqltaxdbilpfRs.getBigDecimal("TAXAMT03"));
				taxdbilpf.setTxabsind01(sqltaxdbilpfRs.getString("TXABSIND01"));
				taxdbilpf.setTxabsind02(sqltaxdbilpfRs.getString("TXABSIND02"));
				taxdbilpf.setTxabsind03(com.quipoz.COBOLFramework.util.StringUtil.nullToTrimString(sqltaxdbilpfRs.getString("TXABSIND03"))); //ILIFE-7134 by dpuhawan
				taxdbilpf.setTxtype01(sqltaxdbilpfRs.getString("TXTYPE01"));
				taxdbilpf.setTxtype02(sqltaxdbilpfRs.getString("TXTYPE02"));
				taxdbilpf.setTxtype03(sqltaxdbilpfRs.getString("TXTYPE03"));
				taxdbilpf.setPostflg(sqltaxdbilpfRs.getString("POSTFLG"));
				taxdbilpf.setUsrprf(sqltaxdbilpfRs.getString("USRPRF"));
				taxdbilpf.setJobnm(sqltaxdbilpfRs.getString("JOBNM"));
				taxdbilpf.setDatime(sqltaxdbilpfRs.getDate("DATIME"));
				taxdbilpfReadResult.add(taxdbilpf);
			}

		} catch (SQLException e) {
			LOGGER.error("readTaxdbilData()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(stmn, sqltaxdbilpfRs);
		}
		return taxdbilpfReadResult;
	}   
	public List<Taxdpf> searchTaxdrevRecord(String coy, String chdrnum) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT UNIQUE_NUMBER FROM TAXDREV WHERE CHDRCOY=? AND CHDRNUM=? ")
				.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, EFFDATE ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		List<Taxdpf> taxdfSearchResult = new LinkedList<Taxdpf>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = executeQuery(ps);

			while (rs.next()) {
				Taxdpf taxdpf = new Taxdpf();
				taxdpf.setUnique_number(rs.getLong(1));
				taxdfSearchResult.add(taxdpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchTaxdrevRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return taxdfSearchResult;
	}   
	
	public void deleteTaxdpfRecord(List<Taxdpf> taxdpfList){
		String sql = "DELETE FROM TAXDPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Taxdpf l:taxdpfList){
				ps.setLong(1, l.getUnique_number());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteTaxdpfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
}
