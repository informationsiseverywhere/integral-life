/*
 * File: Pr52g.java
 * Date: December 3, 2013 3:26:23 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR52G.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.contractservicing.screens.Sr52gScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
****************************************************************** ****
* </pre>
*/
public class Pr52g extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR52G");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private FixedLengthStringData wsaaTableToRead = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaItemToRead = new FixedLengthStringData(8);
	private PackedDecimalData wsaaGndtotal = new PackedDecimalData(17, 2);
		/* TABLES */
	private static final String tr52b = "TR52B";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private static final String itemrec = "ITEMREC";
	private static final String descrec = "DESCREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Sr52gScreenVars sv = ScreenProgram.getScreenVars( Sr52gScreenVars.class);

	public Pr52g() {
		super();
		screenVars = sv;
		new ScreenModel("Sr52g", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaGndtotal.set(ZERO);
		/*    Dummy field initilisation for prototype version.*/
		sv.aprem.set(ZERO);
		sv.gndtotal.set(ZERO);
		sv.taxamt01.set(ZERO);
		sv.taxamt02.set(ZERO);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/*    Retrieve the key of the coverage/rider being processed*/
		/*    from the COVTLNB logical view.*/
		covtlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		/* Read TR52D table .*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		/* Read table TR52E.*/
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		readTr52e1100();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e1100();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e1100();
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.transType.set("PREM");
		txcalcrec.function.set("CALC");
		txcalcrec.effdate.set(covtlnbIO.getEffdate());
		txcalcrec.tranno.set(chdrlnbIO.getTranno());
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covtlnbIO.getLife());
		txcalcrec.coverage.set(covtlnbIO.getCoverage());
		txcalcrec.rider.set(covtlnbIO.getRider());
		txcalcrec.planSuffix.set(0);
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.crtable.set(covtlnbIO.getCrtable());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		if (isNE(chdrlnbIO.getBillfreq(), "00")) {
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(covtlnbIO.getZbinstprem());
			}
			else {
				txcalcrec.amountIn.set(covtlnbIO.getInstprem());
			}
			txcalcrec.amountIn.add(covtlnbIO.getSingp());
		}
		else {
			txcalcrec.amountIn.set(covtlnbIO.getSingp());
		}
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		/*    Set screen fields*/
		sv.aprem.set(txcalcrec.amountIn);
		sv.taxamt01.set(txcalcrec.taxAmt[1]);
		sv.taxamt02.set(txcalcrec.taxAmt[2]);
		wsaaItemToRead.set(txcalcrec.taxType[1]);
		wsaaTableToRead.set(tr52b);
		readDesc1200();
		sv.descript01.set(descIO.getLongdesc());
		if (sv.descript01.containsOnly("?")) {
			sv.descript01.set(SPACES);
		}
		wsaaItemToRead.set(txcalcrec.taxType[2]);
		readDesc1200();
		sv.descript02.set(descIO.getLongdesc());
		if (sv.descript02.containsOnly("?")) {
			sv.descript02.set(SPACES);
		}
		compute(wsaaGndtotal, 3).setRounded(add(txcalcrec.taxAmt[1], txcalcrec.taxAmt[2]));
		sv.gndtotal.set(wsaaGndtotal);
	}

protected void readTr52e1100()
	{
		start1110();
	}

protected void start1110()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void readDesc1200()
	{
		start1200();
	}

protected void start1200()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaTableToRead);
		descIO.setDescitem(wsaaItemToRead);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/**    Validate fields*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required / WSSP*/
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}
}
