/*
 * File: Revphrtp.java
 * Date: 30 August 2009 2:10:35
 * Author: Quipoz Limited
 * 
 * Class transformed from REVPHRTP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.FluprevTableDAM;
import com.csc.life.contractservicing.dataaccess.FuperevTableDAM;
import com.csc.life.contractservicing.dataaccess.PhrtTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*REMARKS.
*
*        Pending Prem Holiday Reinstatement Reverse
*        ------------------------------------------
*
* Overview.
* ---------
*
*  This subroutine can be called by:
*  - the Full Contract Reversal program
*  - Action E "Delete Pending PH Reinstatement" (PR51M) at
*    Premium Holiday Reinstatement Submenu, via Table T6661
*
*  The parameters passed to this program are set up in the
*  copybook REVESEREC.
*
* Reverse Contract Header ( CHDRS ):
* ----------------------------------
*  Read the Contract Header file (CHDRPF) using the logical
*  view CHDRMJA with a key of company and contract number
*  from linkage.
*
*  For the record found, check it is validflag '1'.
*    If not, then ERROR.
*  Delete validflag '1' CHDR record.
*  Read Next CHDR record which is validflag '2'.
*  Reset validflag to '1', and then Rewrite CHDR record.
*
* Follow-ups
* ----------
*  Read and hold on the follow up code for the relevant contract.
*  Delete this record.
*
* Reverse Coverage & Rider records ( COVRs ):
* -------------------------------------------
*  Read and hold on the Coverage/Riders record for the relevant
*  contract.
*  Delete this record. Read next on the Coverage/Riders record
*  for the same contract.
*  This should be valid flag of '2'.
*  If not, then this is an error.  Overwrite this record after
*  altering the valid flag from '2' to '1'.
*
*
*
*****************************************************************
*
* </pre>
*/
public class Revphrtp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REVPHRTP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(256);
		/* FORMATS */
	private String covrrec = "COVRREC   ";
	private String chdrmjarec = "CHDRMJAREC";
	private String fluprevrec = "FLUPREVREC";
	private String fuperevrec = "FUPEREVREC";
	private String phrtrec = "PHRTREC";
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Follow Up File for Reversals*/
	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
		/*FollowUp Extended Text File for Reversal*/
	private FuperevTableDAM fuperevIO = new FuperevTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*PremHoliday Reinstatement Pending Detail*/
	private PhrtTableDAM phrtIO = new PhrtTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1490, 
		processComponent1550, 
		exit1590, 
		exit1890, 
		fupeIo1950, 
		exit1990, 
		exit9040, 
		exit9590
	}

	public Revphrtp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise1100();
		processChdrs1200();
		processFlups1300();
		processCovrs1500();
		deletePhrt1800();
		processFupes1900();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise1100()
	{
		start1100();
	}

protected void start1100()
	{
		wsaaBatckey.set(reverserec.batchkey);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError9000();
		}
	}

protected void processChdrs1200()
	{
		start1210();
	}

protected void start1210()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(),"1")
		|| isEQ(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		chdrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(),"2")
		|| isEQ(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
	}

protected void processFlups1300()
	{
		/*START*/
		fluprevIO.setParams(SPACES);
		fluprevIO.setChdrcoy(reverserec.company);
		fluprevIO.setChdrnum(reverserec.chdrnum);
		fluprevIO.setTranno(reverserec.tranno);
		fluprevIO.setFormat(fluprevrec);
		fluprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluprevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(fluprevIO.getStatuz(),varcom.endp))) {
			callFluprevio1400();
		}
		
		/*EXIT*/
	}

protected void callFluprevio1400()
	{
		try {
			start1410();
			nextr1480();
		}
		catch (GOTOException e){
		}
	}

protected void start1410()
	{
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(),varcom.oK)
		&& isNE(fluprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(fluprevIO.getChdrcoy(),reverserec.company)
		|| isNE(fluprevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(fluprevIO.getTranno(),reverserec.tranno)
		|| isEQ(fluprevIO.getStatuz(),varcom.endp)) {
			fluprevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1490);
		}
		fluprevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError9500();
		}
	}

protected void nextr1480()
	{
		fluprevIO.setFunction(varcom.nextr);
	}

protected void processCovrs1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					covers1510();
				}
				case processComponent1550: {
					processComponent1550();
				}
				case exit1590: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covers1510()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
		if (isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1590);
		}
	}

protected void processComponent1550()
	{
		wsaaCovrKey.set(covrIO.getDataKey());
		if (isEQ(covrIO.getTranno(),reverserec.tranno)) {
			deleteAndUpdatComp1600();
		}
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getDataKey(),wsaaCovrKey))) {
			getNextCovr1700();
		}
		
		if (isNE(covrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.processComponent1550);
		}
	}

protected void deleteAndUpdatComp1600()
	{
		start1610();
	}

protected void start1610()
	{
		if (isNE(covrIO.getValidflag(),"1")) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
		covrIO.setFunction(varcom.delet);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
		if (isNE(covrIO.getValidflag(),"2")
		|| isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
	}

protected void getNextCovr1700()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError9500();
		}
		if (isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void deletePhrt1800()
	{
		try {
			start1810();
			callPhrtio1820();
		}
		catch (GOTOException e){
		}
	}

protected void start1810()
	{
		phrtIO.setParams(SPACES);
		phrtIO.setChdrcoy(reverserec.company);
		phrtIO.setChdrnum(reverserec.chdrnum);
		phrtIO.setTranno(reverserec.tranno);
		phrtIO.setFormat(phrtrec);
		phrtIO.setFunction(varcom.readh);
	}

protected void callPhrtio1820()
	{
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(),varcom.oK)
		&& isNE(phrtIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			systemError9000();
		}
		if (isEQ(phrtIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit1890);
		}
		phrtIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(phrtIO.getParams());
			syserrrec.statuz.set(phrtIO.getStatuz());
			databaseError9500();
		}
	}

protected void processFupes1900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1910();
				}
				case fupeIo1950: {
					fupeIo1950();
					nextr1980();
				}
				case exit1990: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1910()
	{
		fuperevIO.setParams(SPACES);
		fuperevIO.setChdrcoy(reverserec.company);
		fuperevIO.setChdrnum(reverserec.chdrnum);
		fuperevIO.setTranno(reverserec.tranno);
		fuperevIO.setFormat(fuperevrec);
		fuperevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fuperevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void fupeIo1950()
	{
		SmartFileCode.execute(appVars, fuperevIO);
		if (isNE(fuperevIO.getStatuz(),varcom.oK)
		&& isNE(fuperevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fuperevIO.getParams());
			syserrrec.statuz.set(fuperevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(fuperevIO.getChdrcoy(),reverserec.company)
		|| isNE(fuperevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(fuperevIO.getTranno(),reverserec.tranno)
		|| isEQ(fuperevIO.getStatuz(),varcom.endp)) {
			fuperevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1990);
		}
		fuperevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fuperevIO);
		if (isNE(fuperevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fuperevIO.getParams());
			syserrrec.statuz.set(fuperevIO.getStatuz());
			databaseError9500();
		}
	}

protected void nextr1980()
	{
		fuperevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.fupeIo1950);
	}

protected void systemError9000()
	{
		try {
			start9010();
		}
		catch (GOTOException e){
		}
		finally{
			exit9040();
		}
	}

protected void start9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9040);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9040()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		try {
			start9510();
		}
		catch (GOTOException e){
		}
		finally{
			exit9590();
		}
	}

protected void start9510()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9590()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
