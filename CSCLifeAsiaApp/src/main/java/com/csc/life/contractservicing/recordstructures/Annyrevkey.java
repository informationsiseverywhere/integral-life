package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:17
 * Description:
 * Copybook name: ANNYREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Annyrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData annyrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData annyrevKey = new FixedLengthStringData(64).isAPartOf(annyrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData annyrevChdrcoy = new FixedLengthStringData(1).isAPartOf(annyrevKey, 0);
  	public FixedLengthStringData annyrevChdrnum = new FixedLengthStringData(8).isAPartOf(annyrevKey, 1);
  	public FixedLengthStringData annyrevLife = new FixedLengthStringData(2).isAPartOf(annyrevKey, 9);
  	public FixedLengthStringData annyrevCoverage = new FixedLengthStringData(2).isAPartOf(annyrevKey, 11);
  	public FixedLengthStringData annyrevRider = new FixedLengthStringData(2).isAPartOf(annyrevKey, 13);
  	public PackedDecimalData annyrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(annyrevKey, 15);
  	public PackedDecimalData annyrevTranno = new PackedDecimalData(5, 0).isAPartOf(annyrevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(annyrevKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(annyrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		annyrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}