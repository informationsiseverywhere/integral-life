package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr579screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr579ScreenVars sv = (Sr579ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr579screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr579ScreenVars screenVars = (Sr579ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.zbinstprem.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.polinc.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.susamt.setClassString("");
		screenVars.matage.setClassString("");
		screenVars.mattrm.setClassString("");
		screenVars.mattcessDisp.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.premcessDisp.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.singlePremium.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.comind.setClassString("");
		screenVars.optextind.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.taxind.setClassString("");
	}

/**
 * Clear all the variables in Sr579screen
 */
	public static void clear(VarModel pv) {
		Sr579ScreenVars screenVars = (Sr579ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.zbinstprem.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifenum.clear();
		screenVars.linsname.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.polinc.clear();
		screenVars.planSuffix.clear();
		screenVars.sumin.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.currcd.clear();
		screenVars.susamt.clear();
		screenVars.matage.clear();
		screenVars.mattrm.clear();
		screenVars.mattcessDisp.clear();
		screenVars.mattcess.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		screenVars.premcessDisp.clear();
		screenVars.premcess.clear();
		screenVars.liencd.clear();
		screenVars.singlePremium.clear();
		screenVars.mortcls.clear();
		screenVars.comind.clear();
		screenVars.optextind.clear();
		screenVars.zagelit.clear();
		screenVars.zlinstprem.clear();
		screenVars.taxamt.clear();
		screenVars.taxind.clear();
	}
}
