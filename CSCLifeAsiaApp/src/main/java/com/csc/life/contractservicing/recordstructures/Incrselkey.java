package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:00
 * Description:
 * Copybook name: INCRSELKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrselkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrselFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrselKey = new FixedLengthStringData(64).isAPartOf(incrselFileKey, 0, REDEFINE);
  	public FixedLengthStringData incrselChdrcoy = new FixedLengthStringData(1).isAPartOf(incrselKey, 0);
  	public FixedLengthStringData incrselChdrnum = new FixedLengthStringData(8).isAPartOf(incrselKey, 1);
  	public FixedLengthStringData incrselLife = new FixedLengthStringData(2).isAPartOf(incrselKey, 9);
  	public FixedLengthStringData incrselCoverage = new FixedLengthStringData(2).isAPartOf(incrselKey, 11);
  	public FixedLengthStringData incrselRider = new FixedLengthStringData(2).isAPartOf(incrselKey, 13);
  	public PackedDecimalData incrselPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrselKey, 15);
  	public PackedDecimalData incrselTranno = new PackedDecimalData(5, 0).isAPartOf(incrselKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(incrselKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrselFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrselFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}