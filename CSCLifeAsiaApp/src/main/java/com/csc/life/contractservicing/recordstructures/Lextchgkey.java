package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:03
 * Description:
 * Copybook name: LEXTCHGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lextchgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifergpFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifergpKey = new FixedLengthStringData(256).isAPartOf(lifergpFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifergpChdrcoy = new FixedLengthStringData(1).isAPartOf(lifergpKey, 0);
  	public FixedLengthStringData lifergpChdrnum = new FixedLengthStringData(8).isAPartOf(lifergpKey, 1);
  	public FixedLengthStringData lifergpLife = new FixedLengthStringData(2).isAPartOf(lifergpKey, 9);
  	public FixedLengthStringData lifergpJlife = new FixedLengthStringData(2).isAPartOf(lifergpKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifergpKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifergpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifergpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}