package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50V
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50vScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(706);
	public FixedLengthStringData dataFields = new FixedLengthStringData(402).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnames = new FixedLengthStringData(200).isAPartOf(dataFields, 8);
	public FixedLengthStringData[] clntname = FLSArrayPartOfStructure(4, 50, clntnames, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(200).isAPartOf(clntnames, 0, FILLER_REDEFINE);
	public FixedLengthStringData clntname01 = DD.clntname.copy().isAPartOf(filler,0);
	public FixedLengthStringData clntname02 = DD.clntname.copy().isAPartOf(filler,50);
	public FixedLengthStringData clntname03 = DD.clntname.copy().isAPartOf(filler,100);
	public FixedLengthStringData clntname04 = DD.clntname.copy().isAPartOf(filler,150);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,208);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,211);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(dataFields,219);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(dataFields,227);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,237);
	public FixedLengthStringData fupcode = DD.fupcde.copy().isAPartOf(dataFields,267);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,270);
	public FixedLengthStringData jownnum = DD.jownnum.copy().isAPartOf(dataFields,278);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,286);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,294);
	public FixedLengthStringData lstupuser = DD.lstupuser.copy().isAPartOf(dataFields,324);
	public FixedLengthStringData names = new FixedLengthStringData(60).isAPartOf(dataFields, 334);
	public FixedLengthStringData[] name = FLSArrayPartOfStructure(2, 30, names, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(names, 0, FILLER_REDEFINE);
	public FixedLengthStringData name01 = DD.name.copy().isAPartOf(filler1,0);
	public FixedLengthStringData name02 = DD.name.copy().isAPartOf(filler1,30);
	public ZonedDecimalData zlstupdt = DD.zlstupdt.copyToZonedDecimal().isAPartOf(dataFields,394);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 402);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnamesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] clntnameErr = FLSArrayPartOfStructure(4, 4, clntnamesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(clntnamesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData clntname01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData clntname02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData clntname03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData clntname04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData fupcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lstupuserErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData namesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] nameErr = FLSArrayPartOfStructure(2, 4, namesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(namesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData name01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData name02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData zlstupdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 478);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData clntnamesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(4, 12, clntnamesOut, 0);
	public FixedLengthStringData[][] clntnameO = FLSDArrayPartOfArrayStructure(12, 1, clntnameOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(48).isAPartOf(clntnamesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] clntname01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] clntname02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] clntname03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] clntname04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] fupcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lstupuserOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData namesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] nameOut = FLSArrayPartOfStructure(2, 12, namesOut, 0);
	public FixedLengthStringData[][] nameO = FLSDArrayPartOfArrayStructure(12, 1, nameOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(namesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] name01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] name02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] zlstupdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(96);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(78).isAPartOf(subfileArea, 0);
	public FixedLengthStringData message = DD.message.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(4).isAPartOf(subfileArea, 78);
	public FixedLengthStringData messageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(12).isAPartOf(subfileArea, 82);
	public FixedLengthStringData[] messageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 94);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData zlstupdtDisp = new FixedLengthStringData(10);

	public LongData Sr50vscreensflWritten = new LongData(0);
	public LongData Sr50vscreenctlWritten = new LongData(0);
	public LongData Sr50vscreenWritten = new LongData(0);
	public LongData Sr50vprotectWritten = new LongData(0);
	public GeneralTable sr50vscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50vscreensfl;
	}

	public Sr50vScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
//		ILIFE-697 start-sgadkari
		fieldIndMap.put(messageOut,new String[] {"01","11","-01",null, null, null, null, null, null, null, null, null});
//		ILIFE-697 end
		screenSflFields = new BaseData[] {message};
		screenSflOutFields = new BaseData[][] {messageOut};
		screenSflErrFields = new BaseData[] {messageErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cownnum, jownnum, lifcnum, jlifcnum, fupcode, crtuser, lstupuser, cnttype, ctypdesc, clntname01, clntname02, clntname03, clntname04, longdesc, name01, name02, crtdate, zlstupdt};
		screenOutFields = new BaseData[][] {chdrnumOut, cownnumOut, jownnumOut, lifcnumOut, jlifcnumOut, fupcdeOut, crtuserOut, lstupuserOut, cnttypeOut, ctypdescOut, clntname01Out, clntname02Out, clntname03Out, clntname04Out, longdescOut, name01Out, name02Out, crtdateOut, zlstupdtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cownnumErr, jownnumErr, lifcnumErr, jlifcnumErr, fupcdeErr, crtuserErr, lstupuserErr, cnttypeErr, ctypdescErr, clntname01Err, clntname02Err, clntname03Err, clntname04Err, longdescErr, name01Err, name02Err, crtdateErr, zlstupdtErr};
		screenDateFields = new BaseData[] {crtdate, zlstupdt};
		screenDateErrFields = new BaseData[] {crtdateErr, zlstupdtErr};
		screenDateDispFields = new BaseData[] {crtdateDisp, zlstupdtDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50vscreen.class;
		screenSflRecord = Sr50vscreensfl.class;
		screenCtlRecord = Sr50vscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50vprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50vscreenctl.lrec.pageSubfile);
	}
}
