package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:15
 * Description:
 * Copybook name: PTRNWFDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptrnwfdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptrnwfdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptrnwfdKey = new FixedLengthStringData(64).isAPartOf(ptrnwfdFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptrnwfdChdrcoy = new FixedLengthStringData(1).isAPartOf(ptrnwfdKey, 0);
  	public FixedLengthStringData ptrnwfdChdrnum = new FixedLengthStringData(8).isAPartOf(ptrnwfdKey, 1);
  	public PackedDecimalData ptrnwfdTranno = new PackedDecimalData(5, 0).isAPartOf(ptrnwfdKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(ptrnwfdKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptrnwfdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptrnwfdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}