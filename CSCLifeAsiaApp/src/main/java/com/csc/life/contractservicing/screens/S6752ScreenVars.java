package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6752
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6752ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(449);
	public FixedLengthStringData dataFields = new FixedLengthStringData(209).isAPartOf(dataArea, 0);
	public FixedLengthStringData hflags = new FixedLengthStringData(2).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] hflag = FLSArrayPartOfStructure(2, 1, hflags, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(hflags, 0, FILLER_REDEFINE);
	public FixedLengthStringData hflag01 = DD.hflag.copy().isAPartOf(filler,0);
	public FixedLengthStringData hflag02 = DD.hflag.copy().isAPartOf(filler,1);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData jownnum = DD.jownnum.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,111);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,158);
	public FixedLengthStringData optdscs = new FixedLengthStringData(30).isAPartOf(dataFields, 166);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(2, 15, optdscs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler1,15);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,206);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 209);
	public FixedLengthStringData hflagsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] hflagErr = FLSArrayPartOfStructure(2, 4, hflagsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(hflagsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hflag01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData hflag02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(2, 4, optdscsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 269);
	public FixedLengthStringData hflagsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(2, 12, hflagsOut, 0);
	public FixedLengthStringData[][] hflagO = FLSDArrayPartOfArrayStructure(12, 1, hflagOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(hflagsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hflag01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] hflag02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(2, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(171);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(57).isAPartOf(subfileArea, 0);
	public ZonedDecimalData datesub = DD.datesub.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,38);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(subfileFields,46);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,47);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,48);
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(subfileFields,53);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 57);
	public FixedLengthStringData datesubErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 85);
	public FixedLengthStringData[] datesubOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 169);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData datesubDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S6752screensflWritten = new LongData(0);
	public LongData S6752screenctlWritten = new LongData(0);
	public LongData S6752screenWritten = new LongData(0);
	public LongData S6752protectWritten = new LongData(0);
	public GeneralTable s6752screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6752screensfl;
	}

	public S6752ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc01Out,new String[] {null, null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc02Out,new String[] {null, null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hflag01Out,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hflag02Out,new String[] {null, null, null, "11",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, tranno, effdate, trcode, datesub, ind, descrip};
		screenSflOutFields = new BaseData[][] {selectOut, trannoOut, effdateOut, trcodeOut, datesubOut, indOut, descripOut};
		screenSflErrFields = new BaseData[] {selectErr, trannoErr, effdateErr, trcodeErr, datesubErr, indErr, descripErr};
		screenSflDateFields = new BaseData[] {effdate, datesub};
		screenSflDateErrFields = new BaseData[] {effdateErr, datesubErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp, datesubDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, jownnum, optdsc01, optdsc02, lifename, jlinsname, hflag01, hflag02};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, jownnumOut, optdsc01Out, optdsc02Out, lifenameOut, jlinsnameOut, hflag01Out, hflag02Out};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, jownnumErr, optdsc01Err, optdsc02Err, lifenameErr, jlinsnameErr, hflag01Err, hflag02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6752screen.class;
		screenSflRecord = S6752screensfl.class;
		screenCtlRecord = S6752screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6752protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6752screenctl.lrec.pageSubfile);
	}
}
