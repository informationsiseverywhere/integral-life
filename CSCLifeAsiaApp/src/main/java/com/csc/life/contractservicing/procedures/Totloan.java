/*
 * File: Totloan.java
 * Date: 30 August 2009 2:38:54
 * Author: Quipoz Limited
 * 
 * Class transformed from TOTLOAN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;  //ILIFE-9041
import java.util.List; //ILIFE-9041

import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO; //ILIFE-9041
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf; //ILIFE-9041
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*             TOTAL LOANS SUBROUTINE.
*             -----------------------
* Overview.
* ---------
*
* This Subroutine is called to calculate just how many Loans
*  a Contract has associated with it and what the total
*  Loan Principal & Loan Interest debt is. I.E. How much of the
*  Loan Principal hasn't been paid off by the Client, plus the
*  Interest the Client owes us for giving him/her the Loan(s).
* IF this subroutine is called by a Claims related program,
*  such as Death, Maturities or Surrenders, then a Function
*  of 'POST' is specified - this means that in addition to
*  calculating Loan Principal & Interest owed up to the
*  specified date, any Interest which hasn't been Posted yet
*  WILL be posted by this routine so that later in the
*  processing of the chosen Claim the Total Loan debt up to
*  the Effective date can be paid-off.
* The Principal & Interest values returned by this subroutine
*  are in the CONTRACT CURRENCY, although if any Interest has
*  to be posted, ( as in Function = 'POST' ), then the postings
*  will be made in the LOAN Currency
*
*
*Given:
*      Function, Company, Contract number, Effective date,
*       Batchkey, Tranno, Tranid, Language
*
* NOTE.... Function is either SPACES or 'POST'
*  where
*        SPACES => just calculate interest & principal
*
*        'POST' => this routine is being called from somewhere
*                   like surrenders and we must post interest
*                   up to today so that surrenders will be able
*                   to pay off ALL Outstanding Loans before
*                   passing on any cash to clients etc..
*                   I.E. write interest ACMVs and update dates
*                   on Loan record
*
*Gives:
*      Status, Total Principal, Total Interest, Number of Loans
*       on contract
*
*Functionality
*-------------
*
* Read Contract to get contract type CNTTYPE and contract
*  currency CNTCURR.
*
* BEGN on LOAN file & get 1st record
* Store principal amount WSAA-PRINCIPAL
*
* Add up all existing ACMVs for loan interest which relate
*  to this particular LOAN record and are dated since the
*  last capitalisation date. ( value in CoNTract CURRency )
*
* IF effective-date > last interest date on Loan record
*
*   Set interest to date = effective date
*   Set interest from date = last interest date on loan record
*
*   calculate interest earned since last interest date for this
*    LOAN record:
*
*    CALL INTCALC giving:
*           Loan no, Company, contract no, interest from date,
*           interest to date, loan prin at last capn date,
*           last loan capn date, loan commencement date
*       getting:
*          interest amount
*
*    Store interest amount ( in contract currency CNTCURR )
*    IF Function = 'POST',then write interest ACMVs
*    write interest ACMVs
*    REWRiTe LOAN record with last & next interest billing dates
*     amended.
* END IF
*
*READ NEXT LOAN record and repeat above process
*
*WHEN no more LOAN records,
* we have total interest amount
* Get current principal amount by adding/subtracting all Loan
* principal ACMVs since start of loan. (in CoNTract CURRency )
*
* NOTE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*------
*      ALL VALUES ARE CALCULATED & RETURNED IN CONTRACT CURRENCY
*                                              --------
*      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*
*RETURN Loan principal and Loan interest to calling routine
*
*****************************************************************
* </pre>
*/
public class Totloan extends SMARTCodeModel {
	private static final String wsaaSubr = "TOTLOAN ";
	public static final String ROUTINE = QPUtilities.getThisClass();
	private ZonedDecimalData wsaaLoanCount = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private int wsaaMaxDate;
	private PackedDecimalData wsaaPostedAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInterest = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrincipal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIntcInterest = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(3, 0).setUnsigned();
	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	

	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Intcalcrec intcalcrec = new Intcalcrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	private Totloanrec totloanrec = new Totloanrec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	//ILIFE-9041 start
	private LoanpfDAO loanpfDAO = getApplicationContext().getBean("loanpfDAO", LoanpfDAO.class);
	private List<Loanpf> loanList = new ArrayList<Loanpf>();
	private Loanpf loanpf = new Loanpf();
	
	private SMARTAppVars av = (SMARTAppVars) AppVars.getInstance();
	private ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private DescpfDAO descpfDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private List<Acmvpf> acmvpfList;
	private Chdrpf chdrpf;
	private String rldgacct;
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ZrdecplcPojo zrdecplcPojo;
	private Datcon2Pojo datcon2Pojo;
	private String descstr;
	//ILIFE-9041 end

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNextAmv2450, 
		exit2490, 
		datcon2Loop3550, 
		updateLoan3550
	}

	public Totloan() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		totloanrec.totloanRec = convertAndSetParam(totloanrec.totloanRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		control100();
		exit190();
	}

protected void control100()
	{
		initialise1000();
		processLoans2000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		totloanrec.statuz.set(varcom.oK);
		wsaaMaxDate = 99999999;
		wsaaLoanCount.set(ZERO);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		wsaaPostedAmount.set(ZERO);
		wsaaInterest.set(ZERO);
		wsaaPrincipal.set(ZERO);
		wsaaIntcInterest.set(ZERO);
		wsaaSacscode.set(SPACES);
		wsaaSacstype.set(SPACES);
		wsaaSequenceNo.set(ZERO);
		wsaaBatckey.set(totloanrec.batchkey);
		/* Get contract type to pass to interest calculation routine*/
		/*  later on and get contract currency.*/
		readContract1100();
	}

protected void readContract1100(){
	chdrpf = chdrpfDAO.getChdrenqRecord(totloanrec.chdrcoy.toString().trim(),totloanrec.chdrnum.toString().trim());
	if (chdrpf == null) {
		databaseError99500();
	}
}

protected void processLoans2000()
	{
		start2000();
	}

protected void start2000(){
	/* Do a BEGN on LOAN file to get 1st record*/
	 //ILIFE-9041 start
	 List<Loanpf> loanpfList = loanpfDAO.loadDataByLoanNum(totloanrec.chdrcoy.toString(), 
			 totloanrec.chdrnum.toString(), "0");
	 /* If no loans for this contract, exit subroutine*/
	 if(loanpfList.isEmpty() && isEQ(wsaaLoanCount, ZERO)) {
		return ;
	}
	readT5645Table2200();
    for(Loanpf loan : loanpfList) {
		loanpf = loan;
		processLoan();
    }
	 
	//ILIFE-9041 end
	/* We have at least 1 loan for this contract*/
	totloanrec.loanCount.set(wsaaLoanCount);
	totloanrec.interest.set(wsaaInterest);
	totloanrec.principal.set(wsaaPrincipal);
}

	protected void processLoan(){
		wsaaLoanCount.add(1);
//		readT5645Table2200();
		/* Sum all existing Loan interest ACMVs which exist for this*/
		/*  Loan and are dated since the last capitalisation date on*/
		/*  the loan.*/
		sumInterestAcmvs2300();
		wsaaInterest.add(wsaaPostedAmount);
 		//ILIFE-9041 start
		if (isLT(loanpf.getLstintbdte(), totloanrec.effectiveDate)) {
			/*        Set up INTCALC linkage and call...*/
			intcalcrec.intcalcRec.set(SPACES);
			intcalcrec.loanNumber.set(loanpf.getLoannumber());
			intcalcrec.chdrcoy.set(loanpf.getChdrcoy());
			intcalcrec.chdrnum.set(loanpf.getChdrnum());
			intcalcrec.cnttype.set(chdrpf.getCnttype());
			intcalcrec.interestTo.set(totloanrec.effectiveDate);
			intcalcrec.interestFrom.set(loanpf.getLstintbdte());
			intcalcrec.loanorigam.set(loanpf.getLstcaplamt());
			intcalcrec.lastCaplsnDate.set(loanpf.getLstcapdate());
			intcalcrec.loanStartDate.set(loanpf.getLoansdate());
			intcalcrec.interestAmount.set(ZERO);
			intcalcrec.loanCurrency.set(loanpf.getLoancurr());
			intcalcrec.loanType.set(loanpf.getLoantype());
			Intcalc intcalc = new Intcalc();
			intcalc.setGlobalVars(av);
			intcalc.mainline( new Object[] {intcalcrec.intcalcRec });
    		
			if (isNE(intcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(intcalcrec.intcalcRec);
				syserrrec.statuz.set(intcalcrec.statuz);
				systemError99000();
			}
 			//ILIFE-9041 end
			/*        Get here and we want to add wsaa-posted-amount to result*/
			/*        from intcalc and add into wsaa-total-interest - then*/
			/*        clear out wsaa-interest for next loan record to use.*/
			/*        after all LOAN records processed, return values to*/
			/*        calling program.*/
			wsaaIntcInterest.set(intcalcrec.interestAmount);
			/*        make sure interest returned from INTCALC is*/
			/*        in ctrt currency*/
			if (isNE(loanpf.getLoancurr(), chdrpf.getCntcurr())) {
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountIn.set(intcalcrec.interestAmount);
				conlinkrec.statuz.set(SPACES);
				conlinkrec.function.set("REAL");
				/*        MOVE 'SURR'               TO CLNK-FUNCTION           */
				conlinkrec.currIn.set(loanpf.getLoancurr());
				conlinkrec.cashdate.set(wsaaMaxDate);
				conlinkrec.currOut.set(chdrpf.getCntcurr());
				conlinkrec.amountOut.set(ZERO);
				conlinkrec.company.set(loanpf.getChdrcoy());
				Xcvrt.getInstance().mainline(conlinkrec.clnk002Rec);
				if (isNE(conlinkrec.statuz, varcom.oK)) {
					syserrrec.params.set(conlinkrec.clnk002Rec);
					syserrrec.statuz.set(conlinkrec.statuz);
					systemError99000();
				}
				else {
					intcalcrec.interestAmount.set(conlinkrec.amountOut);
				}
			}
			zrdecplcPojo = new ZrdecplcPojo();
			zrdecplcPojo.setAmountIn(intcalcrec.interestAmount.getbigdata());
			a000CallRounding();
			intcalcrec.interestAmount.set(PackedDecimalData.parseObject(zrdecplcPojo.getAmountOut()));
			wsaaInterest.add(intcalcrec.interestAmount);
		}
		/* need to sum all loan principal ACMVs since last capitalisation*/
		/*  date to get current principal value*/
		sumLoanPrincipal2500();
		wsaaPrincipal.add(wsaaPostedAmount);
		/* if loan is not in contract currency, then convert last loan*/
		/*  capitalisation amount to the contract currency.*/
		/* NOTE... we will not add the principal to the sum of the ACMVs*/
		/*  IF the LOAN-START-DATE is the same as the TOTL-EFFECTIVE-DATE*/
		/*   other-wise we will get double the principal amount*/
		if (isNE(totloanrec.effectiveDate, loanpf.getLoansdate())) {
			if (isNE(loanpf.getLoancurr(), chdrpf.getCntcurr())) {
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountIn.set(loanpf.getLstcaplamt());
				conlinkrec.statuz.set(SPACES);
				conlinkrec.function.set("REAL");
				/*         MOVE 'SURR'                  TO CLNK-FUNCTION        */
				conlinkrec.currIn.set(loanpf.getLoancurr());
				conlinkrec.cashdate.set(wsaaMaxDate);
				conlinkrec.currOut.set(chdrpf.getCntcurr());
				conlinkrec.amountOut.set(ZERO);
				conlinkrec.company.set(loanpf.getChdrcoy());
				Xcvrt.getInstance().mainline(conlinkrec.clnk002Rec);
				if (isNE(conlinkrec.statuz, varcom.oK)) {
					syserrrec.params.set(conlinkrec.clnk002Rec);
					syserrrec.statuz.set(conlinkrec.statuz);
					systemError99000();
				}
				else {
					zrdecplcPojo = new ZrdecplcPojo();
					zrdecplcPojo.setAmountIn(conlinkrec.amountIn.getbigdata());
					a000CallRounding();
					conlinkrec.amountOut.set(PackedDecimalData.parseObject(zrdecplcPojo.getAmountOut()));
					wsaaPrincipal.add(conlinkrec.amountOut);
				}
			}
			else {
				wsaaPrincipal.add(loanpf.getLstcaplamt());
			}
		}
		/* if Function = 'POST', we must post an interest ACMV*/
		if (isEQ(totloanrec.function, "POST")) {
			postInterestAcmv3000();
			updateLoan3500();
		}

	}

protected void readT5645Table2200(){
	Itempf t5645 = itempfDAO.findItemByItem(totloanrec.chdrcoy.toString(), "T5645","TOTLOAN");
	if (t5645 == null) {
		databaseError99500();
	}else{
		t5645rec.t5645Rec.set(StringUtil.rawToString(t5645.getGenarea()));
	}
	
}

protected void sumInterestAcmvs2300()
	{
		start2300();
	}

protected void start2300()
	{
		/* Use entry 2 on the T5645 record read to get Loan interest*/
		/*  ACMVs for this loan number.*/
		if (isEQ(loanpf.getLoantype(), "P")) {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstype.set(t5645rec.sacstype02);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode06);
			wsaaSacstype.set(t5645rec.sacstype06);
		}
		wsaaPostedAmount.set(ZERO);
		rldgacct = totloanrec.chdrnum.toString().trim().concat(String.valueOf(loanpf.getLoannumber()));
		acmvpfList = acmvpfDAO.loadDataByBatch(rldgacct, wsaaSacscode.toString(), wsaaSacstype.toString(), 
				loanpf.getLstcapdate(), totloanrec.chdrcoy.toString());
		if(!acmvpfList.isEmpty()){
			for(Acmvpf acmvpf : acmvpfList){
				processAcmv(acmvpf);
			}
		}
		
	}

protected void processAcmv(Acmvpf acmvpf){
	if(isNE(totloanrec.effectiveDate, loanpf.getLoansdate())) {
		if(isEQ(acmvpf.getEffdate(), loanpf.getLstcapdate())) {
			if(isEQ(loanpf.getLoansdate(), loanpf.getLstcapdate())
			&& isEQ(acmvpf.getGlsign(), "-")) {
				/*CONTINUE_STMT*/
			}
			else {
				return;
			}
		}
	}
	
	if(isNE(acmvpf.getOrigcurr(), chdrpf.getCntcurr())) {
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(acmvpf.getOrigamt());
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("REAL");
		/*     MOVE 'SURR'             TO CLNK-FUNCTION                 */
		conlinkrec.currIn.set(acmvpf.getOrigcurr());
		conlinkrec.cashdate.set(wsaaMaxDate);
		conlinkrec.currOut.set(chdrpf.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(loanpf.getChdrcoy());
		Xcvrt.getInstance().mainline(conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError99000();
		}
		else {
			if (isEQ(acmvpf.getGlsign(), "-")) {
				compute(conlinkrec.amountOut, 2).set(mult(conlinkrec.amountOut, -1));
			}
			zrdecplcPojo = new ZrdecplcPojo();
			zrdecplcPojo.setAmountIn(conlinkrec.amountOut.getbigdata());
			a000CallRounding();
			conlinkrec.amountOut.set(PackedDecimalData.parseObject(zrdecplcPojo.getAmountOut()));
			wsaaPostedAmount.add(conlinkrec.amountOut);
			/**            ADD CLNK-AMOUNT-OUT TO WSAA-POSTED-AMOUNT            */
		}
	}
	else {
		if (isEQ(acmvpf.getGlsign(), "-")) {
			setPrecision(acmvpf.getOrigamt(), 2);
			acmvpf.setOrigamt(acmvpf.getOrigamt().multiply(BigDecimal.valueOf(-1)));
		}
		wsaaPostedAmount.add(PackedDecimalData.parseObject(acmvpf.getOrigamt()));
		/**        ADD ACMVLON-ORIGAMT     TO WSAA-POSTED-AMOUNT            */
	}
}

protected void sumLoanPrincipal2500(){
		/* Use entry 1 on the T5645 record read to get Loan principal*/
		/*  ACMVs for this loan number.*/
		if (isEQ(loanpf.getLoantype(), "P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode05);
			wsaaSacstype.set(t5645rec.sacstype05);
		}
		wsaaPostedAmount.set(ZERO);
		rldgacct = totloanrec.chdrnum.toString().trim().concat(String.valueOf(loanpf.getLoannumber()));
		acmvpfList = acmvpfDAO.loadDataByBatch(rldgacct, wsaaSacscode.toString(), wsaaSacstype.toString(), 
				loanpf.getLstcapdate(), totloanrec.chdrcoy.toString());
		if(!acmvpfList.isEmpty()){
			for(Acmvpf acmvpf : acmvpfList){
				processAcmv(acmvpf);
			}
		}
	}

protected void postInterestAcmv3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* If no interest accrued, then just update LOAN record - don't*/
		/*  write a Zero ACMV*/
		if (isEQ(wsaaIntcInterest, ZERO)) {
			return ;
		}
		/* Use entries 3 & 4 on the T5645 record read earlier for our*/
		/*  interest postings if it is a policy loan being processed*/
		/* or Use entries 7 & 8 on the T5645 record read earlier for our*/
		/*  interest postings if it is an APL loan.*/
		/* Set up lifacmv fields.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(totloanrec.chdrnum);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.batccoy.set(totloanrec.chdrcoy);
		lifacmvrec.rldgcoy.set(totloanrec.chdrcoy);
		lifacmvrec.genlcoy.set(totloanrec.chdrcoy);
		lifacmvrec.batckey.set(totloanrec.batchkey);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(loanpf.getLoancurr());
		lifacmvrec.origamt.set(wsaaIntcInterest);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(totloanrec.tranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(totloanrec.effectiveDate);
		lifacmvrec.tranref.set(totloanrec.chdrnum);
		/* Get item description.*/
		descstr = descpfDAO.getLongDesc("IT", totloanrec.chdrcoy.toString(), "T5645", wsaaSubr, totloanrec.language.toString());
		
		if (descstr == null) {
			descstr = "";
		}
		lifacmvrec.trandesc.set(descstr);
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(totloanrec.chdrnum);
		wsaaLoanNumber.set(loanpf.getLoannumber());
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.transactionDate.set(totloanrec.tranDate);
		lifacmvrec.transactionTime.set(totloanrec.tranTime);
		lifacmvrec.user.set(totloanrec.tranUser);
		lifacmvrec.termid.set(totloanrec.tranTerm);
		/* post the interest to the Loan debit account*/
		if (isEQ(loanpf.getLoantype(), "P")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
		/* post the interest to the interest income account*/
		if (isEQ(loanpf.getLoantype(), "P")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
		}
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void updateLoan3500(){
		/* Read T6633 so that we can work out what the next interest*/
		/*  billing date should be set to.*/
		readT66333600();
		/* Check the interest  details on T6633 in the following*/
		/*  order: i) Calculate interest on Loan anniv ... Y/N*/
		/*        ii) Calculate interest on Policy anniv.. Y/N*/
		/*       iii) Check Int freq & whether a specific Day is chosen*/
		loanpf.setLstintbdte(totloanrec.effectiveDate.toInt());
		wsaaLoanDate.set(loanpf.getLoansdate());
		wsaaEffdate.set(totloanrec.effectiveDate);
		wsaaContractDate.set(chdrpf.getOccdate());
		/* Check for loan anniversary flag set*/
		/* IF set,*/
		/*    set next interest billing date to be on the next loan anniv*/
		/*     date after the Effective date we are using now.*/
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {

			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setIntDate1(String.valueOf(loanpf.getLoansdate()));
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			
			while ( !(isGT(datcon2Pojo.getIntDate2(), totloanrec.effectiveDate))) {
				datcon2Utils.calDatcon2(datcon2Pojo);
				datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			}
			
			loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
			updateLoan3550();
			return;
		}
		/* IF T6633-LOAN-ANNIV-INTEREST = 'Y'                           */
		/*    AND ( WSAA-EFFDATE-MD     > WSAA-LOAN-MD OR               */
		/*          WSAA-EFFDATE-MD     = WSAA-LOAN-MD )                */
		/*     MOVE ZEROS                TO WSAA-NEW-DATE               */
		/*     MOVE WSAA-LOAN-MD         TO WSAA-NEW-MD                 */
		/*     MOVE WSAA-EFFDATE-YEAR    TO WSAA-NEW-YEAR               */
		/*now add 1 year to 'new' date for next interest billing date     */
		/*     MOVE SPACES               TO DTC2-DATCON2-REC            */
		/*     MOVE WSAA-NEW-DATE        TO DTC2-INT-DATE-1             */
		/*     MOVE '01'                 TO DTC2-FREQUENCY              */
		/*     MOVE 1                    TO DTC2-FREQ-FACTOR            */
		/*     PERFORM 3700-CALL-DATCON2                                */
		/*     MOVE DTC2-INT-DATE-2    TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3550-UPDATE-LOAN                                   */
		/* END-IF.                                                      */
		/* IF T6633-LOAN-ANNIV-INTEREST = 'Y'                           */
		/*    AND WSAA-EFFDATE-MD       < WSAA-LOAN-MD                  */
		/* set next interest billing date to be on the next loan anniv  */
		/*  date after the Effective date we are using now.             */
		/*     MOVE ZEROS              TO WSAA-NEW-DATE                 */
		/*     MOVE WSAA-LOAN-MD       TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE WSAA-NEW-DATE      TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3550-UPDATE-LOAN                                   */
		/* END-IF.                                                      */
		/* Check for contract anniversary flag set                         */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next contract    */
		/*     anniversary date after the Effective date we are using now. */
		if (isEQ(t6633rec.policyAnnivInterest, "Y")) {
			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setIntDate1(chdrpf.getOccdate().toString());
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			
			while ( !(isGT(datcon2Pojo.getIntDate2(), totloanrec.effectiveDate))) {
				datcon2Utils.calDatcon2(datcon2Pojo);
				datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			}
			
			loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
			
			updateLoan3550();
			return;
		}
		/* IF T6633-POLICY-ANNIV-INTEREST = 'Y'                         */
		/*    AND ( WSAA-EFFDATE-MD       > WSAA-CONTRACT-MD OR         */
		/*          WSAA-EFFDATE-MD       = WSAA-CONTRACT-MD )          */
		/*     MOVE ZEROS                TO WSAA-NEW-DATE               */
		/*     MOVE WSAA-CONTRACT-MD     TO WSAA-NEW-MD                 */
		/*     MOVE WSAA-EFFDATE-YEAR    TO WSAA-NEW-YEAR               */
		/*now add 1 year to 'new' date for next interest billing date     */
		/*     MOVE SPACES               TO DTC2-DATCON2-REC            */
		/*     MOVE WSAA-NEW-DATE        TO DTC2-INT-DATE-1             */
		/*     MOVE '01'                 TO DTC2-FREQUENCY              */
		/*     MOVE 1                    TO DTC2-FREQ-FACTOR            */
		/*     PERFORM 3700-CALL-DATCON2                                */
		/*     MOVE DTC2-INT-DATE-2    TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3550-UPDATE-LOAN                                   */
		/* END-IF.                                                      */
		/* IF T6633-POLICY-ANNIV-INTEREST = 'Y'                         */
		/*    AND WSAA-EFFDATE-MD         < WSAA-CONTRACT-MD            */
		/* set next interest billing date to be on the next loan anniv  */
		/*  date after the Effective date we are using now.             */
		/*     MOVE ZEROS              TO WSAA-NEW-DATE                 */
		/*     MOVE WSAA-CONTRACT-MD   TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE WSAA-NEW-DATE      TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3550-UPDATE-LOAN                                   */
		/* END-IF.                                                      */
		/* Get here so the next interest calc. date isn't based on loan*/
		/*  or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified*/
		/* ...if not, use the Loan day*/
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet();
		}
		else {
			if (isNE(t6633rec.interestDay, ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setIntDate1(wsaaNewDate.toString());
		datcon2Pojo.setFreqFactor(1);
		/* check if table T6633 has a fixed frequency for interest calcs,*/
		/* ...if not, use 1 year as the default interest calc. frequency.*/
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon2Pojo.setFrequency("01");
		}
		else {
			datcon2Pojo.setFrequency(t6633rec.interestFrequency.toString());
		}
		datcon2Loop3550();
		updateLoan3550();
	}

protected void datcon2Loop3550()
	{
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!"****".equals(datcon2Pojo.getStatuz())) {
			syserrrec.params.set(datcon2Pojo);
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			databaseError99500();
		}
		
		if (isLTE(datcon2Pojo.getIntDate2(), totloanrec.effectiveDate)) {
			datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			datcon2Loop3550();
		}
		if (!daysLessThan28.isTrue()) {
			if (wsaaMonthCheckInner.february.isTrue()) {
				wsaaNewDate.set(datcon2Pojo.getIntDate2());
				wsaaNewDay.set(t6633rec.interestDay);
				loanpf.setNxtintbdte(wsaaNewDate.toInt());
			}
			else {
				if (wsaaMonthCheckInner.april.isTrue()
				|| wsaaMonthCheckInner.june.isTrue()
				|| wsaaMonthCheckInner.september.isTrue()
				|| wsaaMonthCheckInner.november.isTrue()) {
					if (daysInJan.isTrue()) {
						wsaaNewDate.set(datcon2Pojo.getIntDate2());
						wsaaNewDay.set(t6633rec.interestDay);
						loanpf.setNxtintbdte(wsaaNewDate.toInt());
					}
					else {
						loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
					}
				}
				else {
					loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
				}
			}
		}
		else {
			loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
		}
	}

	/**
	* <pre>
	* Having now got the next interest billing date, REWRiTe the
	*  LOAN record
	* </pre>
	*/
protected void updateLoan3550()
	{
		//ILIFE-9041 start
		loanList.add(loanpf);
		loanpfDAO.updateLoanRecords(loanList);
		loanList.clear();
		//ILIFE-9041 end
		/*EXIT*/
	}

protected void readT66333600(){
	List<Itempf> t6633List = itempfDAO.getItdmByFrmdate(totloanrec.chdrcoy.toString(), "T6633", 
			chdrpf.getCnttype().toString(), totloanrec.effectiveDate.toInt());
	if(t6633List.isEmpty()) {
		syserrrec.statuz.set("E723");
		databaseError99500();
	}
	t6633rec.t6633Rec.set(StringUtil.rawToString(t6633List.get(0).getGenarea()));
}


protected void dateSet()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon2*/
		/*  with is a valid from-date... ie IF the interest/capn day in*/
		/*  T6633 is > 28, we have to make sure the from-date isn't*/
		/*  something like 31/02/nnnn or 31/06/nnnn*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(30);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(28);
		}
		/*EXIT*/
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		totloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		start99500();
		exit99590();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		totloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void a000CallRounding(){
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setCompany(totloanrec.chdrcoy.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setCurrency(chdrpf.getCntcurr());
		if (isEQ(wsaaBatckey.batcBatctrcde, SPACES)) {
			zrdecplcPojo.setBatctrcde("****");
		}
		else {
			zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		}
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			systemError99000();
		}
		
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner { 

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}
}
