/*
 * File: P5049.java
 * Date: 30 August 2009 0:00:20
 * Author: Quipoz Limited
 * 
 * Class transformed from P5049.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.List;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.S5049ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.CtrspfDAO;
import com.csc.life.newbusiness.dataaccess.model.Ctrspf;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*                  Contract Minor Alterations Submenu
*                  ==================================
*
*
* Initialisation
* --------------
*
*  Retrieve todays date (DATCON1) and store for later use.
*
*
* Validation
* ----------
*
*  Key 1 - Contract proposal number (CHDRMNA)
*
*       Y = mandatory, must exist on file.
*            - CHDRMNA  -  Life  new  business  contract  header
*                 logical view  (LIFO,  keyed  on  company   and
*                 contract number, select service unit = LP).
*            - must be correct status for transaction (T5679).
*            - for maintenANCE  transactions,   servicing  branch
*              must be sign-on branch
*
*
*       Blank = irrelevant.
*
*  Key 3 - Contract number.
*
*       Y = mandatory, must exist on file (CHDRMNA).
*
*       N = irrelevant.
*
*       Blank = irrelevant.
*
*
* Updating
* --------
*
*  Set up WSSP-FLAG:
*       - "I" for L action, otherwise "M".
*  For enqury transactions ("I"), nothing else is required.
*
*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header minor alterations. If the contract
*  is already locked, display an error message.
*
*
*
*
*
*****************************************************************
* </pre>
*/
public class P5049 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5049");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrmnarec = "CHDRMNAREC";
	private static final String chdrmjarec = "CHDRMJAREC"; //ILIFE-2472
		/*01  WSAA-T5677-KEY.                                              */
	private int wsaaToday = 0;
	private int wsaaSub1 = 0;

	private FixedLengthStringData wsaaChdrselFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator chdrselFirstTime = new Validator(wsaaChdrselFirstTime, "Y");
	private String wsaaPreviousChdrsel = "";
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();//bugai
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM(); //ILIFE-2472
	private Alocnorec alocnorec = new Alocnorec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S5049ScreenVars sv = ScreenProgram.getScreenVars( S5049ScreenVars.class);
	private Th506rec th506rec = new Th506rec();
	boolean isFatcaConfig = false;
	
	private CtrspfDAO ctrspfDAO = getApplicationContext().getBean("ctrspfDAO", CtrspfDAO.class);
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO", HcsdpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private Hcsdpf hcsdpf = null;

    boolean CSMIN003Permission  = false;

	boolean CSMIN004Permission = false;  //ICIL-658

	private boolean nonSuper=false;
	
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190,  
		search2510, 
		exit12090, 
		continue3060, 
		keeps3070, 
		batching3080, 
		exit3090
	}

	public P5049() {
		super();
		screenVars = sv;
		new ScreenModel("S5049", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set("E070");
		}
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday = datcon1rec.intDate==null?0:datcon1rec.intDate.toInt();
		}
		/*EXIT*/
		//ILIFE-3974 STARTS smalchi2	
		isFatcaConfig = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "CLMTN008", appVars, "IT");
		if(isFatcaConfig)
		{
			sv.actionflag.set("Y");
		}
		else
		{
			sv.actionflag.set("N");
		}
        CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
        if(CSMIN003Permission) {
            sv.fuflag.set("Y");
        } else {
            sv.fuflag.set("N");
        }
		//ICIL-658 start
        CSMIN004Permission  = FeaConfg.isFeatureExist("2", "CSMIN004", appVars, "IT");
        if(!CSMIN004Permission) {
        	sv.rfundflgOut[varcom.nd.toInt()].set("Y");
        }
        //ICIL-658 end 
		//ENDS
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5049IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5049-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200("DEFAULT");
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*  Only cash dividend contract is allowed for change of           */
		/*  dividend option.                                               */
		if (isEQ(sv.errorIndicators, SPACES)
		&& isEQ(sv.action, "M")) {
			x100CheckContract();
		}
		/*                                                         <V76L01>*/
		/*  If contract already have Trustee, display an warning           */
		/*  message when user try to do change on the contract             */
		/*  beneficiary                                                    */
		if (isEQ(sv.errorIndicators, SPACES)
		&& (isEQ(sv.action, "A")
		|| isEQ(sv.action, "N"))) {
			x200CheckTrustee();
		}
		if (isNE(wsaaPreviousChdrsel, sv.chdrsel)) {
			wsaaPreviousChdrsel = sv.chdrsel.toString();
			wsaaChdrselFirstTime.set("Y");
		}
		if (isEQ(sv.chdrselErr, "RFK7")) {
			if (chdrselFirstTime.isTrue()) {
				wsspcomn.edterror.set("Y");
				wsaaChdrselFirstTime.set("N");
			}
			else {
				sv.chdrselErr.set(SPACES);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200(String nextMethod)
	{

		switch (nextMethod) {
				case "DEFAULT": 
					validateKey12210();
				case "validateKey22220": 
					validateKey22220();
				case "exit2290": 
				}

	}


protected void validateKey12210()
	{
	if (isEQ(subprogrec.key1, SPACES)) {
		validateKeys2200("validateKey22220");
		return ;
	}
		chdrmnaIO.setChdrcoy(wsspcomn.company);
		chdrmnaIO.setChdrnum(sv.chdrsel);
		chdrmnaIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrmnaIO);
			chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), sv.chdrsel.toString());
		}
		else {
			chdrmnaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmnaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmnaIO.getStatuz(), varcom.mrnf)
		&& isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set("E544");
		}
		if (isEQ(chdrmnaIO.getStatuz(), varcom.oK)
		&& isEQ(subprogrec.key1, "N")) {
			sv.chdrselErr.set("F918");
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				 validateKeys2200("exit2290");
				 return ;
			}
		}
		/*    For transactions on existing proposals,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branck.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrmnaIO.getCntbranch())) {
			sv.chdrselErr.set("E455");
		}
		if ((isEQ(subprogrec.key1,"Y")) &&isEQ(sv.action,"T")){
			wsspcomn.currfrom.set(chdrmnaIO.getPtdate());
			chdrmjaIO.setParams(SPACES);
			chdrmjaIO.setChdrcoy(wsspcomn.company);
			chdrmjaIO.setChdrnum(sv.chdrsel);
			chdrmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
			&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)) {
				sv.chdrselErr.set("E544");
				return;
			}
		}
			
		//ILIFE-7277
		if (readTR59X() && sv.action.toString().trim().equals("E")) {
			sv.actionErr.set("RRH4");
		}
		//ILIFE-7792
		nonSuper= FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),"NBPRP095", appVars, "IT");
		if(nonSuper){
		if( !readTR59X() && isEQ(sv.action, "A")){
			sv.actionErr.set("RRMR");
		}
		}
	}




protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2, SPACES)
		|| isEQ(subprogrec.key2, "N")) {
			return ;
		}
		chdrmnaIO.setChdrcoy(wsspcomn.company);
		chdrmnaIO.setChdrnum(sv.chdrsel);
		chdrmnaIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrmnaIO);
		}
		else {
			chdrmnaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmnaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmnaIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set("E544");
		}
	}

protected void checkStatus2400()
	{
		readStatusTable2410();
		readStatusTable2420();
		//ICIL-658 start
		if((CSMIN004Permission)
        && (isEQ(sv.action, "U"))
		&& isEQ(sv.chdrselErr, SPACES)){
        	CheckBtdatePtdate2430();
        }
		//ICIL-658 end
	}

protected void readStatusTable2410()
	{
	    itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(subprogrec.transcd.toString());
	    itempf = itemDAO.getItempfRecord(itempf);
		
		
		/* MOVE ITEM-GENAREA           TO T5679-T5679-REC.              */
		/* MOVE ZERO TO WSAA-SUB.                                       */
		/* PERFORM 2500-LOOK-FOR-STAT.                                  */
		if (itempf != null) {
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			wsaaSub1=0;
			lookForStat2500();
		}
		else {
			t5679rec.t5679Rec.set(SPACES);
			scrnparams.errorCode.set("F321");
			wsspcomn.edterror.set("Y");
		}
	}
/*FATCA CNT Type */

protected void readStatusTable2420(){
	
    itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("TH506");
	itempf.setItemitem(chdrmnaIO.getCnttype().toString());
	itempf = itemDAO.getItempfRecord(itempf);
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TH506");
		itempf.setItemitem("***");
        itempf = itemDAO.getItempfRecord(itempf);
		
	}
	if ((isEQ(sv.action, "S")) ||  (isEQ(sv.action, "R"))){
		

	if (itempf != null) {
		th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if(isEQ(th506rec.fatcaFlag,"Y")){
				//chdrmnaIO.setChdrnum(sv.chdrsel);
			}
		else{
			sv.chdrselErr.set("rfyz");
		}
	}

 }
}

//ICIL-658 start
private void CheckBtdatePtdate2430() 
	{
		if (isNE(chdrmnaIO.btdate, chdrmnaIO.ptdate)) {
			sv.chdrselErr.set("H947");
			wsspcomn.edterror.set("Y");
		}
	}
//ICIL-658 end

/*FATCA CNT Type 
*/protected void lookForStat2500()
	{
		wsaaSub1++;
		if (isGT(wsaaSub1, 12)) {
			sv.chdrselErr.set("E767");
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(chdrmnaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub1])) {
				lookForStat2500();
			}
		}
		/*EXIT*/
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set("E073");
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2990-EXIT.                                         */
		goTo(GotoLabel.exit12090);
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case continue3060: 
					continue3060();
				case keeps3070: 
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
				
				
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		
		//ILIFE-2472 Starts
		if (isEQ(scrnparams.action, "A")
				|| isEQ(scrnparams.action, "B")
				|| isEQ(scrnparams.action, "Q")) {
					chdrmjaIO.setChdrcoy(chdrmnaIO.getChdrcoy());
					chdrmjaIO.setChdrnum(chdrmnaIO.getChdrnum());
					chdrmjaIO.setFunction(varcom.readr);
					chdrmjaIO.setFormat(chdrmjarec);
					SmartFileCode.execute(appVars, chdrmjaIO);
					if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
						syserrrec.statuz.set(chdrmjaIO.getStatuz());
						syserrrec.params.set(chdrmjaIO.getParams());
						fatalError600();
					}
					chdrmjaIO.setFormat(chdrmjarec);
					chdrmjaIO.setFunction(varcom.keeps);
					SmartFileCode.execute(appVars, chdrmjaIO);
					if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
						syserrrec.statuz.set(chdrmjaIO.getStatuz());
						syserrrec.params.set(chdrmjaIO.getParams());
						fatalError600();
					}
					chdrpfDAO.setCacheObject(chdrpf);
				}
		//ILIFE-2472 Ends
		/*  Update WSSP Key details*/
		if (isEQ(scrnparams.action, "L")) {
			wsspcomn.flag.set("I");
			goTo(GotoLabel.keeps3070);
		}
		else {
			wsspcomn.flag.set("M");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		/* If accessing this program remotely from Business Objects,       */
		/* skip the returning of error F910 - 'contract in use' as         */
		/* only a enquiry is performed.                                    */
		if (isEQ(scrnparams.deviceInd, "*RMT")) {
			goTo(GotoLabel.continue3060);
		}
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		if (isNE(sv.chdrsel, SPACES)) {
			/*     MOVE S5049-CHDRSEL       TO SFTL-ENTITY*/
			sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		}
		else {
			sftlockrec.entity.set(alocnorec.alocNo);
		}
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set("F910");
		}
	}

protected void continue3060()
	{
		/*    For existing proposals, add one to the transaction number.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			setPrecision(chdrmnaIO.getTranno(), 0);
			chdrmnaIO.setTranno(add(chdrmnaIO.getTranno(), 1));
		}
	}

protected void keeps3070(){
	if(isEQ(sv.action,"Q")){
		chdrpfDAO.setCacheObject(chdrpf);
	}else{
		/*   Store the contract header for use by the transaction programs*/
		chdrmnaIO.setFunction("KEEPS");
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if(isEQ(sv.action,"T")) {
			chdrmjaIO.setFunction("KEEPS");
			chdrmjaIO.setFormat("CHDRMJAREC");
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
		}
	}	
}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

protected void x100CheckContract()
	{
		hcsdpf = hcsdpfDAO.getHcsdRecordByCoyAndNum(chdrmnaIO.getChdrcoy().toString(), chdrmnaIO.getChdrnum().toString());
	
		if (hcsdpf == null) {
			sv.chdrselErr.set("HL65");
		}
		/*X190-EXIT*/
	}

protected void x200CheckTrustee()
	{
	    List<Ctrspf> list = ctrspfDAO.searchCtrspfRecord(chdrmnaIO.getChdrcoy().toString(), chdrmnaIO.getChdrnum().toString());
	    
	
		if (list == null || list.size() == 0) {
			return ;
		}
		sv.chdrselErr.set("RFK7");
	}
//ILIFE-7277
protected boolean readTR59X(){

    itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("TR59X");
	itempf.setItemitem(chdrmnaIO.getCnttype().toString());
	itempf.setValidflag("1");
	itempf = itemDAO.getItempfRecord(itempf);
	if (itempf != null) {
		return true;
	}else{
		return false;
	}
 }

}
