package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:04
 * Description:
 * Copybook name: LEXTMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lextmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lextmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lextmjaKey = new FixedLengthStringData(64).isAPartOf(lextmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData lextmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(lextmjaKey, 0);
  	public FixedLengthStringData lextmjaChdrnum = new FixedLengthStringData(8).isAPartOf(lextmjaKey, 1);
  	public FixedLengthStringData lextmjaLife = new FixedLengthStringData(2).isAPartOf(lextmjaKey, 9);
  	public FixedLengthStringData lextmjaCoverage = new FixedLengthStringData(2).isAPartOf(lextmjaKey, 11);
  	public FixedLengthStringData lextmjaRider = new FixedLengthStringData(2).isAPartOf(lextmjaKey, 13);
  	public PackedDecimalData lextmjaSeqnbr = new PackedDecimalData(3, 0).isAPartOf(lextmjaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(lextmjaKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lextmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lextmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}