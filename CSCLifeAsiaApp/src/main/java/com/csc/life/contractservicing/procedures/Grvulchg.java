/*
 * File: Grvulchg.java
 * Date: 29 August 2009 22:51:23
 * Author: Quipoz Limited
 * 
 * Class transformed from GRVULCHG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.IncitrdTableDAM;
import com.csc.life.unitlinkedprocessing.procedures.Unlchg;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Reversal of INCIs affected by Component Modify
* ----------------------------------------------
*
* Overview.
* ---------
*
* This subroutine is called via the generic processing table,
*  T5671. It is called to reverse the effects that a Component
*  Modify transaction has had on the INCI records associated
*  with the specified Component (Coverage/Rider).
*
*
* Processing.
* -----------
*
* This subroutine is called via an entry in T5671.
*  All parameters required by this subroutine are passed in
*   the GREVERSREC copybook.
*
* To avoid code duplication, this subroutine is going to call
*  UNLCHG, the subroutine that is called when the forward
*  Component Modify transaction is executed. The purpose of
*  UNLCHG is to re-evaluate all the INCI records for the
*  specified Component and take into account whether the
*  premium amount has increased or decreased. Thus, to reverse
*  the effects of the forward transaction, we call UNLCHG
*  again and it will again re-evaluate the INCIs ( we
*  obviously pass the UNLCHG subroutine the Component details
*  that we in effect prior to the Component Change ).
*
*
* NOTE.
*    The records to be processed should be specified right down
*     to PLAN-SUFFIX level in the Linkage passed to this
*     subroutine.
*
*****************************************************************
* </pre>
*/
public class Grvulchg extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GRVULCHG";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String incitrdrec = "INCITRDREC";

	private FixedLengthStringData wsaaBatchkey = new FixedLengthStringData(69);
	private FixedLengthStringData wsaaContkey = new FixedLengthStringData(19).isAPartOf(wsaaBatchkey, 0);
	private FixedLengthStringData wsaaBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 0);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaContkey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaContkey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaContkey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaContkey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaContkey, 14);
	private FixedLengthStringData wsaaOldBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatchkey, 19);
	private Greversrec greversrec = new Greversrec();
		/*Logical File Layout - Version 9305*/
	private IncitrdTableDAM incitrdIO = new IncitrdTableDAM();
	private Isuallrec isuallrec = new Isuallrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		searchDestroyExit2699, 
		exit9490, 
		exit9990
	}

	public Grvulchg() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		processIncis2000();
		removeInactiveIncis2500();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processIncis2000()
	{
		start2000();
	}

protected void start2000()
	{
		isuallrec.isuallRec.set(SPACES);
		isuallrec.company.set(greversrec.chdrcoy);
		isuallrec.chdrnum.set(greversrec.chdrnum);
		isuallrec.life.set(greversrec.life);
		isuallrec.coverage.set(greversrec.coverage);
		isuallrec.rider.set(greversrec.rider);
		isuallrec.planSuffix.set(greversrec.planSuffix);
		isuallrec.freqFactor.set(1);
		isuallrec.transactionDate.set(greversrec.transDate);
		isuallrec.transactionTime.set(greversrec.transTime);
		isuallrec.user.set(greversrec.user);
		isuallrec.termid.set(greversrec.termid);
		wsaaBatchkey.set(greversrec.batckey);
		isuallrec.batcpfx.set(wsaaBatcpfx);
		isuallrec.batccoy.set(wsaaBatccoy);
		isuallrec.batcbrn.set(wsaaBatcbrn);
		isuallrec.batcactyr.set(wsaaBatcactyr);
		isuallrec.batcactmn.set(wsaaBatcactmn);
		isuallrec.batctrcde.set(wsaaOldBatctrcde);
		isuallrec.batcbatch.set(wsaaBatcbatch);
		isuallrec.convertUnlt.set(SPACES);
		isuallrec.covrInstprem.set(ZERO);
		isuallrec.covrSingp.set(ZERO);
		isuallrec.newTranno.set(greversrec.newTranno);
		isuallrec.effdate.set(greversrec.effdate);
		isuallrec.language.set(greversrec.language);
		callProgram(Unlchg.class, isuallrec.isuallRec);
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.params.set(isuallrec.isuallRec);
			syserrrec.statuz.set(isuallrec.statuz);
			systemError9000();
		}
	}

protected void removeInactiveIncis2500()
	{
		readFirstInciRecord2510();
	}

protected void readFirstInciRecord2510()
	{
		incitrdIO.setChdrcoy(greversrec.chdrcoy);
		incitrdIO.setChdrnum(greversrec.chdrnum);
		incitrdIO.setLife(greversrec.life);
		incitrdIO.setCoverage(greversrec.coverage);
		incitrdIO.setRider(greversrec.rider);
		incitrdIO.setPlanSuffix(greversrec.planSuffix);
		incitrdIO.setValidflag(SPACES);
		incitrdIO.setTranno(99);
		incitrdIO.setSeqno(ZERO);
		incitrdIO.setInciNum(ZERO);
		incitrdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incitrdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incitrdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		incitrdIO.setFormat(incitrdrec);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)
		&& isNE(incitrdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			systemError9000();
		}
		while ( !(isEQ(incitrdIO.getStatuz(),varcom.endp))) {
			inciSearchNDestroy2600();
		}
		
	}

protected void inciSearchNDestroy2600()
	{
		try {
			trannoMatchAndDel2610();
		}
		catch (GOTOException e){
		}
	}

protected void trannoMatchAndDel2610()
	{
		if (isNE(greversrec.chdrcoy,incitrdIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,incitrdIO.getChdrnum())
		|| isNE(greversrec.life,incitrdIO.getLife())
		|| isNE(greversrec.coverage,incitrdIO.getCoverage())
		|| isNE(greversrec.rider,incitrdIO.getRider())
		|| isNE(greversrec.planSuffix,incitrdIO.getPlanSuffix())
		|| (isEQ(incitrdIO.getStatuz(),varcom.endp))
				) {
			incitrdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.searchDestroyExit2699);
		}
		if (isNE(incitrdIO.getValidflag(),"1")
		|| isLT(incitrdIO.getTranno(),greversrec.tranno)) {
			incitrdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.searchDestroyExit2699);
		}
		if (isEQ(incitrdIO.getTranno(),greversrec.tranno)) {
			incitrdIO.setFunction(varcom.deltd);
			incitrdIO.setFormat(incitrdrec);
			SmartFileCode.execute(appVars, incitrdIO);
			if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(incitrdIO.getParams());
				syserrrec.statuz.set(incitrdIO.getStatuz());
				systemError9000();
			}
		}
		incitrdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)
		&& isNE(incitrdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			systemError9000();
		}
	}

protected void systemError9000()
	{
		try {
			start9000();
		}
		catch (GOTOException e){
		}
		finally{
			exit9490();
		}
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		try {
			start9500();
		}
		catch (GOTOException e){
		}
		finally{
			exit9990();
		}
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9990);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}
}
