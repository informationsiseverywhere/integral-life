package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5134
 * @version 1.0 generated on 30/08/09 06:35
 * @author Quipoz
 */
public class S5134ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(425);
	public FixedLengthStringData dataFields = new FixedLengthStringData(201).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData entity = DD.entity.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,172);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,180);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,184);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 201);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 257);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(335);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(93).isAPartOf(subfileArea, 0);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData cmpntnum = DD.cmpntnum.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData component = DD.component.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData deit = DD.deit.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData hcoverage = DD.hcoverage.copy().isAPartOf(subfileFields,48);
	public FixedLengthStringData hcovt = DD.hcovt.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,51);
	public FixedLengthStringData hlifcnum = DD.hlifcnum.copy().isAPartOf(subfileFields,55);
	public FixedLengthStringData hlifeno = DD.hlifeno.copy().isAPartOf(subfileFields,63);
	public FixedLengthStringData hrider = DD.hrider.copy().isAPartOf(subfileFields,65);
	public ZonedDecimalData hsuffix = DD.hsuffix.copyToZonedDecimal().isAPartOf(subfileFields,67);
	public FixedLengthStringData linetype = DD.linetype.copy().isAPartOf(subfileFields,71);
	public FixedLengthStringData pstatcode = DD.premdesc.copy().isAPartOf(subfileFields,72);
	public FixedLengthStringData statcode = DD.riskdesc.copy().isAPartOf(subfileFields,82);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,92);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 93);
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cmpntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData componentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData deitErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hcoverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcovtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hlifenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData hriderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData hsuffixErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData linetypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData premdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData riskdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(180).isAPartOf(subfileArea, 153);
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cmpntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] componentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] deitOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hcoverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcovtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hlifcnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hlifenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] hriderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] hsuffixOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] linetypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] premdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] riskdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 333);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5134screensflWritten = new LongData(0);
	public LongData S5134screenctlWritten = new LongData(0);
	public LongData S5134screenWritten = new LongData(0);
	public LongData S5134protectWritten = new LongData(0);
	public GeneralTable s5134screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5134screensfl;
	}

	public S5134ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"02","04","-01","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "06",null, null, null, null, null, null, null, null});
		fieldIndMap.put(entityOut,new String[] {null, null, "06",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hlifcnum, hlifeno, hsuffix, hcoverage, hrider, hcrtable, linetype, statcode, pstatcode, select, component, cmpntnum, deit, asterisk, hcovt};
		screenSflOutFields = new BaseData[][] {hlifcnumOut, hlifenoOut, hsuffixOut, hcoverageOut, hriderOut, hcrtableOut, linetypeOut, riskdescOut, premdescOut, selectOut, componentOut, cmpntnumOut, deitOut, asteriskOut, hcovtOut};
		screenSflErrFields = new BaseData[] {hlifcnumErr, hlifenoErr, hsuffixErr, hcoverageErr, hriderErr, hcrtableErr, linetypeErr, riskdescErr, premdescErr, selectErr, componentErr, cmpntnumErr, deitErr, asteriskErr, hcovtErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, numpols, planSuffix, entity};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, numpolsOut, plnsfxOut, entityOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, numpolsErr, plnsfxErr, entityErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5134screen.class;
		screenSflRecord = S5134screensfl.class;
		screenCtlRecord = S5134screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5134protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5134screenctl.lrec.pageSubfile);
	}
}
