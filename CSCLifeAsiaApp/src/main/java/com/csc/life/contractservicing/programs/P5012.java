/*
 * File: P5012.java
 * Date: 29 August 2009 23:55:02
 * Author: Quipoz Limited
 * 
 * Class transformed from P5012.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.S5012ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Component Change - Life Selection.
*
*  This transaction, Life Selection, is selected from the
*  Component Change Sub Menu.   It is entered into first, in
*  order that the correct life may be  selected, if there is
*  only one life, then it will default to this life.
*
*
*     Initialise
*     ----------
*
*  Clear the subfile ready for loading.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the relevant life  (i.e.  that  lucky  one  to
*  die) BEGN using LIFEMJA  for  this  contract and only lives
*  with a valid flag of '1' are selected (defined in logical
*  view).
*
*  - one line is written to  the subfile for each life read
*    from the LIFEMJA file.
*
*  - if there is only  one life which can be selected, then
*    return this life  as  the  selected life and do not
*    display the life selection screen S5012.
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Only read the  Contract header (function RETRV) if the
*  screen is going to be returned. Read the relevant data
*  necessary for obtaining   the   status   description,
*  the   Owner,   CCD, Paid-to-date and the Bill-to-date.
*
*  Read the client  details record and use the relevant
*  copybook in order to format the required names.
*
*  In all cases, load all  pages required in the subfile and
*  set the subfile indicator to no.
*
*  The above details are returned to the user.
*
*
*     Validation
*     ----------
*
*  If  CF11  (KILL)  was entered, then skip the remainder of
*  the validation and blank out the correct program on the
*  stack and exit from the program.
*
*  Read all modified  records and if more than one selection
*  was made return the  screen  to the user requesting that
*  only one life be selected.  Likewise, return a message if
*  no selection was made, as one life must be selected.
*
*
*     Updating
*     --------
*
*  If the  CF11  (KILL)  function  key  was  pressed,  skip
*  the updating  and  blank out the correct program on the
*  stack and exit from the program.
*
*  For the life selected do a KEEPS on this life.
*
*  Next Program
*  ------------
*
*  For CF11 or 'Enter', add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5012 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5012");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaActualWritten = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaNoOfSelect = new PackedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaRrn = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
		/* ERRORS */
	private String e304 = "E304";
	private String h070 = "H070";
	private String h071 = "H071";
		/* TABLES */
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String lifemjarec = "LIFEMJAREC";
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5012ScreenVars sv = ScreenProgram.getScreenVars( S5012ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P5012() {
		super();
		screenVars = sv;
		new ScreenModel("S5012", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			beginLifeFile1020();
			loadSubfile1030();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		wsaaActualWritten.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5012", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrmjaIO.setFunction(varcom.retrv);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void beginLifeFile1020()
	{
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK))
		&& (isNE(lifemjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifemjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),lifemjaIO.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
	}

protected void loadSubfile1030()
	{
		while ( !(isEQ(lifemjaIO.getStatuz(),varcom.endp))) {
			loadSubfile1100();
		}
		
		if (isGT(wsaaActualWritten,1)) {
			fillScreen1200();
		}
	}

protected void loadSubfile1100()
	{
		lifeDetails1110();
	}

protected void lifeDetails1110()
	{
		if (isEQ(lifemjaIO.getJlife(),"00")) {
			sv.jlife.set(SPACES);
		}
		else {
			sv.jlife.set(lifemjaIO.getJlife());
		}
		sv.life.set(lifemjaIO.getLife());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		descIO.setDescitem(lifemjaIO.getStatcode());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5012", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaActualWritten.add(1);
		}
		lifemjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK))
		&& (isNE(lifemjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifemjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),lifemjaIO.getChdrnum())) {
			lifemjaIO.setStatuz(varcom.endp);
		}
	}

protected void fillScreen1200()
	{
		headings1210();
	}

protected void headings1210()
	{
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrmjaIO.getCownnum());
		getClientDetails1700();
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1700()
	{
		read1710();
	}

protected void read1710()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernumErr.set(e304);
			sv.ownernum.set(SPACES);
		}
		else {
			plainname();
			sv.ownernum.set(wsspcomn.longconfname);
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsaaNoOfSelect.set(0);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsaaActualWritten,1)) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			startSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void startSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5012", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaRrn.set(scrnparams.subfileRrn);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isGT(wsaaNoOfSelect,1)) {
			scrnparams.errorCode.set(h070);
			wsspcomn.edterror.set("Y");
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit2090);
		}
		if (isLT(wsaaNoOfSelect,1)) {
			scrnparams.errorCode.set(h071);
			scrnparams.subfileRrn.set(1);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5012", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void validateSubfile2600()
	{
		/*VALIDATION*/
		if (isNE(sv.select,SPACES)) {
			wsaaNoOfSelect.add(1);
			wsaaRrn.set(scrnparams.subfileRrn);
		}
		/*READ-NEXT-RECORD*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5012", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setLife(sv.life);
		if (isEQ(sv.jlife,SPACES)) {
			lifemjaIO.setJlife("00");
		}
		else {
			lifemjaIO.setJlife(sv.jlife);
		}
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRNUM");
		SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK))
		&& (isNE(lifemjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifemjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),lifemjaIO.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
		lifemjaIO.setFunction(varcom.keeps);
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
