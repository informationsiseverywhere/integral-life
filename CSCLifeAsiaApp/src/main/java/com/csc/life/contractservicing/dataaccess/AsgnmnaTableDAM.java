package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AsgnmnaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:29:21
 * Class transformed from ASGNMNA.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AsgnmnaTableDAM extends AsgnpfTableDAM {

	public AsgnmnaTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ASGNMNA");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", SEQNO"
		             + ", ASGNNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "ASGNPFX, " +
		            "ASGNNUM, " +
		            "REASONCD, " +
		            "COMMFROM, " +
		            "COMMTO, " +
		            "TRANNO, " +
		            "SEQNO, " +
		            "TRANCDE, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "SEQNO ASC, " +
		            "ASGNNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "SEQNO DESC, " +
		            "ASGNNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               asgnpfx,
                               asgnnum,
                               reasoncd,
                               commfrom,
                               commto,
                               tranno,
                               seqno,
                               trancde,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(237);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getSeqno().toInternal()
					+ getAsgnnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, asgnnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller9 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller4.setInternal(asgnnum.toInternal());
	nonKeyFiller9.setInternal(seqno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(58);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getAsgnpfx().toInternal()
					+ nonKeyFiller4.toInternal()
					+ getReasoncd().toInternal()
					+ getCommfrom().toInternal()
					+ getCommto().toInternal()
					+ getTranno().toInternal()
					+ nonKeyFiller9.toInternal()
					+ getTrancde().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, asgnpfx);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, reasoncd);
			what = ExternalData.chop(what, commfrom);
			what = ExternalData.chop(what, commto);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, nonKeyFiller9);
			what = ExternalData.chop(what, trancde);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}
	public FixedLengthStringData getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(Object what) {
		asgnnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getAsgnpfx() {
		return asgnpfx;
	}
	public void setAsgnpfx(Object what) {
		asgnpfx.set(what);
	}	
	public FixedLengthStringData getReasoncd() {
		return reasoncd;
	}
	public void setReasoncd(Object what) {
		reasoncd.set(what);
	}	
	public PackedDecimalData getCommfrom() {
		return commfrom;
	}
	public void setCommfrom(Object what) {
		setCommfrom(what, false);
	}
	public void setCommfrom(Object what, boolean rounded) {
		if (rounded)
			commfrom.setRounded(what);
		else
			commfrom.set(what);
	}	
	public PackedDecimalData getCommto() {
		return commto;
	}
	public void setCommto(Object what) {
		setCommto(what, false);
	}
	public void setCommto(Object what, boolean rounded) {
		if (rounded)
			commto.setRounded(what);
		else
			commto.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTrancde() {
		return trancde;
	}
	public void setTrancde(Object what) {
		trancde.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		seqno.clear();
		asgnnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		asgnpfx.clear();
		nonKeyFiller4.clear();
		reasoncd.clear();
		commfrom.clear();
		commto.clear();
		tranno.clear();
		nonKeyFiller9.clear();
		trancde.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();		
	}


}