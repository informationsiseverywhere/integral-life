package com.csc.life.contractservicing.dataaccess.model;

/**
 * Asgnpf For Table
 */
public class Asgnpf implements java.io.Serializable {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String asgnpfx;
	private String asgnnum;
	private String assigname;
	private String reasoncd;
	private Integer commfrom;
	private Integer commto;
	private Integer tranno;
	private Integer seqno;
	private String trancde;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer user_T;

	public Asgnpf() {
	}

	public Asgnpf(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public Asgnpf(long uniqueNumber, String chdrcoy,
			String chdrnum, String asgnpfx, String asgnnum,
			String assigname, String reasoncd, Integer commfrom,
			Integer commto, Integer tranno, Integer seqno,
			String trancde, String termid, Integer trdt,
			Integer trtm, Integer userT) {
		this.uniqueNumber = uniqueNumber;
		this.chdrcoy = chdrcoy;
		this.chdrnum = chdrnum;
		this.asgnpfx = asgnpfx;
		this.asgnnum = asgnnum;
		this.assigname = assigname;
		this.reasoncd = reasoncd;
		this.commfrom = commfrom;
		this.commto = commto;
		this.tranno = tranno;
		this.seqno = seqno;
		this.trancde = trancde;
		this.termid = termid;
		this.trdt = trdt;
		this.trtm = trtm;
		this.user_T = userT;
	}

	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return this.chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return this.chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getAsgnpfx() {
		return this.asgnpfx;
	}

	public void setAsgnpfx(String asgnpfx) {
		this.asgnpfx = asgnpfx;
	}

	public String getAsgnnum() {
		return this.asgnnum;
	}

	public void setAsgnnum(String asgnnum) {
		this.asgnnum = asgnnum;
	}

	public String getAssigname() {
		return this.assigname;
	}

	public void setAssigname(String assigname) {
		this.assigname = assigname;
	}

	public String getReasoncd() {
		return this.reasoncd;
	}

	public void setReasoncd(String reasoncd) {
		this.reasoncd = reasoncd;
	}

	public Integer getCommfrom() {
		return this.commfrom;
	}

	public void setCommfrom(Integer commfrom) {
		this.commfrom = commfrom;
	}

	public Integer getCommto() {
		return this.commto;
	}

	public void setCommto(Integer commto) {
		this.commto = commto;
	}

	public Integer getTranno() {
		return this.tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public Integer getSeqno() {
		return this.seqno;
	}

	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}

	public String getTrancde() {
		return this.trancde;
	}

	public void setTrancde(String trancde) {
		this.trancde = trancde;
	}

	public String getTermid() {
		return this.termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public Integer getTrdt() {
		return this.trdt;
	}

	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}

	public Integer getTrtm() {
		return this.trtm;
	}

	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}

	public Integer getUser_T() {
		return this.user_T;
	}

	public void setUser_T(Integer userT) {
		this.user_T = userT;
	}

}
