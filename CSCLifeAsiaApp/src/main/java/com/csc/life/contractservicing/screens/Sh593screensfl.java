package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh593screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 17;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh593ScreenVars sv = (Sh593ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sh593screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sh593screensfl, 
			sv.Sh593screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sh593ScreenVars sv = (Sh593ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sh593screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sh593ScreenVars sv = (Sh593ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sh593screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sh593screensflWritten.gt(0))
		{
			sv.sh593screensfl.setCurrentIndex(0);
			sv.Sh593screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sh593ScreenVars sv = (Sh593ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sh593screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh593ScreenVars screenVars = (Sh593ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.loanNumber.setFieldName("loanNumber");
				screenVars.loanType.setFieldName("loanType");
				screenVars.loanStartDateDisp.setFieldName("loanStartDateDisp");
				screenVars.cntcurr.setFieldName("cntcurr");
				screenVars.hprincipal.setFieldName("hprincipal");
				screenVars.hcurbal.setFieldName("hcurbal");
				screenVars.hacrint.setFieldName("hacrint");
				screenVars.hpndint.setFieldName("hpndint");
				screenVars.hpltot.setFieldName("hpltot");
				screenVars.numcon.setFieldName("numcon");
				screenVars.totrepd.setFieldName("totrepd");
				screenVars.repdstat.setFieldName("repdstat");
				screenVars.repymop.setFieldName("repymop");
				screenVars.repdateDisp.setFieldName("repdateDisp");
				// CML-072
				screenVars.nxtintbdteDisp.setFieldName("nxtintbdteDisp");
				screenVars.nxtcapdateDisp.setFieldName("nxtcapdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.loanNumber.set(dm.getField("loanNumber"));
			screenVars.loanType.set(dm.getField("loanType"));
			screenVars.loanStartDateDisp.set(dm.getField("loanStartDateDisp"));
			screenVars.cntcurr.set(dm.getField("cntcurr"));
			screenVars.hprincipal.set(dm.getField("hprincipal"));
			screenVars.hcurbal.set(dm.getField("hcurbal"));
			screenVars.hacrint.set(dm.getField("hacrint"));
			screenVars.hpndint.set(dm.getField("hpndint"));
			screenVars.hpltot.set(dm.getField("hpltot"));
			screenVars.numcon.set(dm.getField("numcon"));
			screenVars.totrepd.set(dm.getField("totrepd"));
			screenVars.repdstat.set(dm.getField("repdstat"));
			screenVars.repymop.set(dm.getField("repymop"));
			screenVars.repdateDisp.set(dm.getField("repdateDisp"));
			// CML-072
			screenVars.nxtintbdteDisp.set(dm.getField("nxtintbdteDisp"));
			screenVars.nxtcapdateDisp.set(dm.getField("nxtcapdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh593ScreenVars screenVars = (Sh593ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.loanNumber.setFieldName("loanNumber");
				screenVars.loanType.setFieldName("loanType");
				screenVars.loanStartDateDisp.setFieldName("loanStartDateDisp");
				screenVars.cntcurr.setFieldName("cntcurr");
				screenVars.hprincipal.setFieldName("hprincipal");
				screenVars.hcurbal.setFieldName("hcurbal");
				screenVars.hacrint.setFieldName("hacrint");
				screenVars.hpndint.setFieldName("hpndint");
				screenVars.hpltot.setFieldName("hpltot");
				screenVars.numcon.setFieldName("numcon");
				screenVars.totrepd.setFieldName("totrepd");
				screenVars.repdstat.setFieldName("repdstat");
				screenVars.repymop.setFieldName("repymop");
				screenVars.repdateDisp.setFieldName("repdateDisp");
				// CML-072
				screenVars.nxtintbdteDisp.setFieldName("nxtintbdteDisp");
				screenVars.nxtcapdateDisp.setFieldName("nxtcapdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("loanNumber").set(screenVars.loanNumber);
			dm.getField("loanType").set(screenVars.loanType);
			dm.getField("loanStartDateDisp").set(screenVars.loanStartDateDisp);
			dm.getField("cntcurr").set(screenVars.cntcurr);
			dm.getField("hprincipal").set(screenVars.hprincipal);
			dm.getField("hcurbal").set(screenVars.hcurbal);
			dm.getField("hacrint").set(screenVars.hacrint);
			dm.getField("hpndint").set(screenVars.hpndint);
			dm.getField("hpltot").set(screenVars.hpltot);
			dm.getField("numcon").set(screenVars.numcon);
			dm.getField("totrepd").set(screenVars.totrepd);
			dm.getField("repdstat").set(screenVars.repdstat);
			dm.getField("repymop").set(screenVars.repymop);
			dm.getField("repdateDisp").set(screenVars.repdateDisp);
			// CML-072
			dm.getField("nxtintbdteDisp").set(screenVars.nxtintbdteDisp);
			dm.getField("nxtcapdateDisp").set(screenVars.nxtcapdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sh593screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sh593ScreenVars screenVars = (Sh593ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.loanNumber.clearFormatting();
		screenVars.loanType.clearFormatting();
		screenVars.loanStartDateDisp.clearFormatting();
		screenVars.cntcurr.clearFormatting();
		screenVars.hprincipal.clearFormatting();
		screenVars.hcurbal.clearFormatting();
		screenVars.hacrint.clearFormatting();
		screenVars.hpndint.clearFormatting();
		screenVars.hpltot.clearFormatting();
		screenVars.numcon.clearFormatting();
		screenVars.totrepd.clearFormatting();
		screenVars.repdstat.clearFormatting();
		screenVars.repymop.clearFormatting();
		screenVars.repdateDisp.clearFormatting();
		// CML-072
		screenVars.nxtintbdteDisp.clearFormatting();
		screenVars.nxtcapdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sh593ScreenVars screenVars = (Sh593ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.loanNumber.setClassString("");
		screenVars.loanType.setClassString("");
		screenVars.loanStartDateDisp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.hprincipal.setClassString("");
		screenVars.hcurbal.setClassString("");
		screenVars.hacrint.setClassString("");
		screenVars.hpndint.setClassString("");
		screenVars.hpltot.setClassString("");
		screenVars.numcon.setClassString("");
		screenVars.totrepd.setClassString("");
		screenVars.repdstat.setClassString("");
		screenVars.repymop.setClassString("");
		screenVars.repdateDisp.setClassString("");
		// CML-072
		screenVars.nxtintbdteDisp.setClassString("");
		screenVars.nxtcapdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sh593screensfl
 */
	public static void clear(VarModel pv) {
		Sh593ScreenVars screenVars = (Sh593ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.loanNumber.clear();
		screenVars.loanType.clear();
		screenVars.loanStartDateDisp.clear();
		screenVars.loanStartDate.clear();
		screenVars.cntcurr.clear();
		screenVars.hprincipal.clear();
		screenVars.hcurbal.clear();
		screenVars.hacrint.clear();
		screenVars.hpndint.clear();
		screenVars.hpltot.clear();
		screenVars.numcon.clear();
		screenVars.totrepd.clear();
		screenVars.repdstat.clear();
		screenVars.repymop.clear();
		screenVars.repdateDisp.clear();
		screenVars.repdate.clear();
		// CML-072
		screenVars.nxtintbdteDisp.clear();
		screenVars.nxtcapdateDisp.clear();
		
	}
}
