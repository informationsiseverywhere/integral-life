/*
 * File: Pt527.java
 * Date: 30 August 2009 2:01:32
 * Author: Quipoz Limited
 * 
 * Class transformed from PT527.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.St527ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*             PT527 - INSURED/BENEFICIARY SELECTION
*             -------------------------------------
* Overview.
* =========
*
* This screen provides a list of all the active life insured and
* beneficiary of a selected contract.
*
* Further enquiries and minor alteration of listed beneficiary/
* insured are possible via making subsequent selections on those
* items in turn. When all have been processed, this screen is
* redisplayed.
*
* Processing.
* ===========
*
*Initialise
*----------
*
*  Skip  this  section  if  returning from an optional selection
*  (current stack position action flag = '*').
*
*  Clear the subfile ready for loading.
*
*  The  details  of  the  contract  being  enquired upon will be
*  stored  in  the  CHDRMNA I/O module. Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       Contract's Bill to Date and Paid to Date.
*
*       Use a key of CHDRCOY  and CHDRNUM, to perform a BEGN on
*       client relation file CLRF.
*
*  Load the subfile as follows:
*
*       Locate the records of FORENUM from CLRF as same contract
*       number (CHDRNUM) with client roles of beneficiary or
*       life insured.
*
*       Look up the following description and name:
*
*       Client Name,  (CLNTNUM) - using subroutine 'NAMADRS,
*
*       Client Role, (CLRROLE) - short description from T3639
*
*       Then the client number must be written to the subfile
*       along  with  the  description of the client name, client
*       role and its short code descriptions.
*
*       Load all pages  required  in  the subfile and set the
*       subfile more indicator to no.
*
*Validation
*----------
*
*  There is no specified validation in this program.
*
*Updating
*--------
*
*  There is no specified updating in this program.
*
*Next Program
*------------
*
*  If "KILL" was requested move spaces to  the  current  program
*  position and action field.
*
*  The system will start from the first record of the subfile,
*  otherwise it will start from the previous selected record
*
*  If the record is not  selected, read the next one and so on,
*  until a selected  record is found, or the end of the subfile
*  is reached.
*
*  If a subfile record has been selected, the selection will be
*  blanked out to denote processed. And the system will store
*  up the key details of the client relation file by using the
*  following: -
*           Company - from CHDRMNA
*           Client Number  - from display client number field
*           Client Role - from display client role field
*           Foreign Number -  from display contract number field
*
*  If this action is OK, then set the current stack action
*  to '*'(so that the system will return to this program to
*  process the next one).
*
*  Add one to  the  program  pointer  and  exit  to  process the
*  required generic action.
*
*  If nothing was selected or there are no  more  selections  to
*  process , continue  by  moving blank to the current program
*  pointer field  and exit with 'unlock' of the selected
*  contract.
*
*Notes.
*------
*
*  Tables Used:
*
*T3588 - Contract Premium Status         Key: PSTATCODE
*T3623 - Contract Risk Status            Key: STATCODE
*T3639 - Client Roles & Relations        Key: CLRRROLE
*T5688 - Contract Structure              Key: CNTTYPE
*
*Examples.
*---------
*
*  In this  example  the Client Role by Foreign Key, (CLRF), has the
*  following records, then it will show the same number of records
*  on the screen subfile.
*
*  CLRF-FORENUM  like 00001107
*
*<-- File Details -->          <--------  Screen Details -------->
*
* Foreign     Role Client      Client No   Role
* Number           No
*
* 0000110701  LF   50001024    50001014    BN  Benefic'y
* 00001107    BN   50001014    50001024    LF  Life Assrd
*
*REMARKS.
*
*****************************************************************
* </pre>
*/
public class Pt527 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PT527");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaNoOfSelect = new PackedDecimalData(3, 0).init(ZERO);
		/* TABLES */
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t3639 = "T3639";
		/* FORMATS */
	private String chdrmnarec = "CHDRMNAREC";
	private String clrrrec = "CLRRREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Minor Alterations Contract Header*/
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
		/*Client role and relationships logical fi*/
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatckey = new Batckey();
	private St527ScreenVars sv = ScreenProgram.getScreenVars( St527ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		clrfNextr1480, 
		preExit, 
		exit2090, 
		bypassStart4010, 
		exit4090
	}

	public Pt527() {
		super();
		screenVars = sv;
		new ScreenModel("St527", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End 
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("ST527", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrmnaIO.setFunction(varcom.retrv);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		fillScreen1100();
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(chdrmnaIO.getChdrcoy());
		clrfIO.setForenum(chdrmnaIO.getChdrnum());
		clrfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		clrfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clrfIO.setFitKeysSearch("FOREPFX", "FORECOY");
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)
		&& isNE(clrfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		wsaaForenum.set(clrfIO.getForenum());
		while ( !(isEQ(clrfIO.getStatuz(),varcom.endp)
		|| isNE(clrfIO.getForepfx(),"CH")
		|| isNE(clrfIO.getForecoy(),chdrmnaIO.getChdrcoy())
		|| isNE(wsaaForenum,chdrmnaIO.getChdrnum()))) {
			loadSubfile1400();
		}
		
	}

protected void fillScreen1100()
	{
		start1110();
	}

protected void start1110()
	{
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		sv.btdate.set(chdrmnaIO.getBtdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		descIO.setParams(SPACES);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1200();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setParams(SPACES);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setDesctabl(t3623);
		findDesc1200();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setParams(SPACES);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setDesctabl(t3588);
		findDesc1200();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		sv.cownnum.set(chdrmnaIO.getCownnum());
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(chdrmnaIO.getCownpfx());
		namadrsrec.clntCompany.set(chdrmnaIO.getCowncoy());
		namadrsrec.clntNumber.set(chdrmnaIO.getCownnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(cltsIO.getDataKey());
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.ownernum.set(namadrsrec.name);
	}

protected void findDesc1200()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadSubfile1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1410();
				}
				case clrfNextr1480: {
					clrfNextr1480();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1410()
	{
		if (isEQ(clrfIO.getUsedToBe(),SPACES)
		&& (isEQ(clrfIO.getClrrrole(),"LF")
		|| isEQ(clrfIO.getClrrrole(),"BN"))) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.clrfNextr1480);
		}
		sv.subfileFields.set(SPACES);
		sv.forenum.set(clrfIO.getForenum());
		sv.clntnum.set(clrfIO.getClntnum());
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(clrfIO.getClntpfx());
		namadrsrec.clntCompany.set(clrfIO.getClntcoy());
		namadrsrec.clntNumber.set(clrfIO.getClntnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(cltsIO.getDataKey());
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.clntname.set(namadrsrec.name);
		sv.clrrrole.set(clrfIO.getClrrrole());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3639);
		descIO.setDescitem(clrfIO.getClrrrole());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.clrole.fill("?");
		}
		else {
			sv.clrole.set(descIO.getShortdesc());
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("ST527", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void clrfNextr1480()
	{
		clrfIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)
		&& isNE(clrfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		wsaaForenum.set(clrfIO.getForenum());
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
	//ILIFE-1274
//		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
//			wsspcomn.edterror.set(varcom.oK);
//			wsspcomn.sectionno.set("3000");
//			goTo(GotoLabel.preExit);
//		}
		// ILIFE-1986 Starts
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		// ILIFE-1986 ends
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("ST527", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("ST527", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("ST527", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case bypassStart4010: {
					bypassStart4010();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
//ILIFE-1274
//		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
//			goTo(GotoLabel.bypassStart4010);
//		}
		//ILIFE-1986 starts 
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.bypassStart4010);
		}
		//ILIFE-1986 ends
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("ST527", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void bypassStart4010()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4100();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			releaseSflock4300();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		sv.select.set(SPACES);
		keepsClrr4200();
//ILIFE-1274
//		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*"); //ILIFE-1986
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4100()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("ST527", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void keepsClrr4200()
	{
		start4210();
	}

protected void start4210()
	{
		clrrIO.setParams(SPACES);
		clrrIO.setClntpfx("CN");
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClntnum(sv.clntnum);
		clrrIO.setClrrrole(sv.clrrrole);
		clrrIO.setForepfx("CH");
		clrrIO.setForecoy(chdrmnaIO.getChdrcoy());
		clrrIO.setForenum(sv.forenum);
		clrrIO.setFunction(varcom.readr);
		clrrIO.setFormat(clrrrec);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		clrrIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
	}

protected void releaseSflock4300()
	{
		start4310();
	}

protected void start4310()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}
}
