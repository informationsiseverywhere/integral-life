package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:41
 * Description:
 * Copybook name: ACMVBK3KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvbk3key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvbk3FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvbk3Key = new FixedLengthStringData(64).isAPartOf(acmvbk3FileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvbk3Rldgcoy = new FixedLengthStringData(1).isAPartOf(acmvbk3Key, 0);
  	public FixedLengthStringData acmvbk3Rldgacct = new FixedLengthStringData(16).isAPartOf(acmvbk3Key, 1);
  	public FixedLengthStringData acmvbk3Sacscode = new FixedLengthStringData(2).isAPartOf(acmvbk3Key, 17);
  	public FixedLengthStringData acmvbk3Sacstyp = new FixedLengthStringData(2).isAPartOf(acmvbk3Key, 19);
  	public FixedLengthStringData acmvbk3Origcurr = new FixedLengthStringData(3).isAPartOf(acmvbk3Key, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(acmvbk3Key, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvbk3FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvbk3FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}