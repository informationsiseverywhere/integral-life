/*
 * File: Br50t.java
 * Date: December 3, 2013 2:08:24 AM ICT
 * Author: CSC
 * 
 * Class transformed from BR50T.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.agents.dataaccess.AgpbpfTableDAM;
import com.csc.life.agents.dataaccess.AgptpfTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*         New Agent Batch Payments Splitter
*
* This program is used to select records from AGPBPF and writes
* to multiple members of the Temporary Extract File AGPTPF for
* subsequent processing. The selection criteria are PROCFLG
* = 'Y' and TAGSUSIND = SPACE.
*
* This program is to be run before the Anniversary Batch
* Program BR50U.
*
* It is run in single-thread, and writes the selected AGPB
* records to multiple files . Each of the members will be read
* by a copy of BR50U run in multi-thread mode.
*
* Embedded SQL statement is used to access the AGPBPF.
*
* Control totals used in this program:
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
****************************************************************** ****
* </pre>
*/
public class Br50t extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlagpbCursorrs = null;
	private java.sql.PreparedStatement sqlagpbCursorps = null;
	private java.sql.Connection sqlagpbCursorconn = null;
	private String sqlagpbCursor = "";
	private int agpbCursorLoopIndex = 0;
	private AgptpfTableDAM agptpf = new AgptpfTableDAM();
	private DiskFileDAM agpt01 = new DiskFileDAM("AGPT01");
	private DiskFileDAM agpt02 = new DiskFileDAM("AGPT02");
	private DiskFileDAM agpt03 = new DiskFileDAM("AGPT03");
	private DiskFileDAM agpt04 = new DiskFileDAM("AGPT04");
	private DiskFileDAM agpt05 = new DiskFileDAM("AGPT05");
	private DiskFileDAM agpt06 = new DiskFileDAM("AGPT06");
	private DiskFileDAM agpt07 = new DiskFileDAM("AGPT07");
	private DiskFileDAM agpt08 = new DiskFileDAM("AGPT08");
	private DiskFileDAM agpt09 = new DiskFileDAM("AGPT09");
	private DiskFileDAM agpt10 = new DiskFileDAM("AGPT10");
	private DiskFileDAM agpt11 = new DiskFileDAM("AGPT11");
	private DiskFileDAM agpt12 = new DiskFileDAM("AGPT12");
	private DiskFileDAM agpt13 = new DiskFileDAM("AGPT13");
	private DiskFileDAM agpt14 = new DiskFileDAM("AGPT14");
	private DiskFileDAM agpt15 = new DiskFileDAM("AGPT15");
	private DiskFileDAM agpt16 = new DiskFileDAM("AGPT16");
	private DiskFileDAM agpt17 = new DiskFileDAM("AGPT17");
	private DiskFileDAM agpt18 = new DiskFileDAM("AGPT18");
	private DiskFileDAM agpt19 = new DiskFileDAM("AGPT19");
	private DiskFileDAM agpt20 = new DiskFileDAM("AGPT20");
	private AgptpfTableDAM agptpfData = new AgptpfTableDAM();
		/* Change the record length to that of the temporary file. This
		 can be found by doing a DSPFD of the file being duplicated
		 by the CRTTMPF process.*/
	private FixedLengthStringData agpt01Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt02Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt03Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt04Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt05Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt06Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt07Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt08Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt09Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt10Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt11Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt12Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt13Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt14Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt15Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt16Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt17Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt18Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt19Rec = new FixedLengthStringData(106);
	private FixedLengthStringData agpt20Rec = new FixedLengthStringData(106);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR50T");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaAgptFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAgptFn, 0, FILLER).init("AGPT");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaAgptFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAgptFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/*    SQL error message formatting.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaAgptInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaAgptInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init("2");
	private FixedLengthStringData wsaaY = new FixedLengthStringData(1).init("Y");
	private FixedLengthStringData wsaaPrevRldgacct = new FixedLengthStringData(16).init(SPACES);
	private FixedLengthStringData wsaaSpace = new FixedLengthStringData(1).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private AgpbpfTableDAM agpbpfData = new AgpbpfTableDAM();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();

	public Br50t() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart|*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		sqlagpbCursor = " SELECT  RLDGCOY, RLDGACCT, SACSCURBAL, PROCFLG, TAGSUSIND, TAXCDE, CURRCODE, AGCCQIND, PAYCLT, FACTHOUS, BANKKEY, BANKACCKEY, WHTAX, TCOLBAL, TDEDUCT, TDCCHRG, PAYMTH" +
" FROM   " + getAppVars().getTableNameOverriden("AGPBPF") + " " +
" WHERE RLDGCOY = ?" +
" AND PROCFLG = ?" +
" AND TAGSUSIND = ?" +
" ORDER BY RLDGCOY, RLDGACCT, CURRCODE";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialize(wsaaFetchArrayInner.wsaaAgptData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlagpbCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.agents.dataaccess.AgpbpfTableDAM());
			sqlagpbCursorps = getAppVars().prepareStatementEmbeded(sqlagpbCursorconn, sqlagpbCursor, "AGPBPF");
			getAppVars().setDBString(sqlagpbCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlagpbCursorps, 2, wsaaY);
			getAppVars().setDBString(sqlagpbCursorps, 3, wsaaSpace);
			sqlagpbCursorrs = getAppVars().executeQuery(sqlagpbCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaAgptFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(AGPT");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaAgptFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			agpt01.openOutput();
		}
		if (isEQ(iz, 2)) {
			agpt02.openOutput();
		}
		if (isEQ(iz, 3)) {
			agpt03.openOutput();
		}
		if (isEQ(iz, 4)) {
			agpt04.openOutput();
		}
		if (isEQ(iz, 5)) {
			agpt05.openOutput();
		}
		if (isEQ(iz, 6)) {
			agpt06.openOutput();
		}
		if (isEQ(iz, 7)) {
			agpt07.openOutput();
		}
		if (isEQ(iz, 8)) {
			agpt08.openOutput();
		}
		if (isEQ(iz, 9)) {
			agpt09.openOutput();
		}
		if (isEQ(iz, 10)) {
			agpt10.openOutput();
		}
		if (isEQ(iz, 11)) {
			agpt11.openOutput();
		}
		if (isEQ(iz, 12)) {
			agpt12.openOutput();
		}
		if (isEQ(iz, 13)) {
			agpt13.openOutput();
		}
		if (isEQ(iz, 14)) {
			agpt14.openOutput();
		}
		if (isEQ(iz, 15)) {
			agpt15.openOutput();
		}
		if (isEQ(iz, 16)) {
			agpt16.openOutput();
		}
		if (isEQ(iz, 17)) {
			agpt17.openOutput();
		}
		if (isEQ(iz, 18)) {
			agpt18.openOutput();
		}
		if (isEQ(iz, 19)) {
			agpt19.openOutput();
		}
		if (isEQ(iz, 20)) {
			agpt20.openOutput();
		}
	}

protected void readFile2000()
	{
		readFiles2010();
	}

protected void readFiles2010()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-RLDGACCT = the present AGPBnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (agpbCursorLoopIndex = 1; isLT(agpbCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlagpbCursorrs); agpbCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlagpbCursorrs, 1, wsaaFetchArrayInner.wsaaRldgcoy[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 2, wsaaFetchArrayInner.wsaaRldgacct[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 3, wsaaFetchArrayInner.wsaaSacscurbal[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 4, wsaaFetchArrayInner.wsaaProcflg[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 5, wsaaFetchArrayInner.wsaaTagsusind[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 6, wsaaFetchArrayInner.wsaaTaxcde[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 7, wsaaFetchArrayInner.wsaaCurrcode[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 8, wsaaFetchArrayInner.wsaaAgccqind[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 9, wsaaFetchArrayInner.wsaaPayclt[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 10, wsaaFetchArrayInner.wsaaFacthous[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 11, wsaaFetchArrayInner.wsaaBankkey[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 12, wsaaFetchArrayInner.wsaaBankacckey[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 13, wsaaFetchArrayInner.wsaaWhtax[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 14, wsaaFetchArrayInner.wsaaTcolbal[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 15, wsaaFetchArrayInner.wsaaTdeduct[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 16, wsaaFetchArrayInner.wsaaTdcchrg[agpbCursorLoopIndex]);
				getAppVars().getDBObject(sqlagpbCursorrs, 17, wsaaFetchArrayInner.wsaaPaymth[agpbCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  IF Either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			if (firstTime.isTrue()) {
				wsaaPrevRldgacct.set(wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		/*  Check record is required for processsing but do not do any othe*/
		/*  reads to any other file. The exception to this rule are reads t*/
		/*  ITEM and these should be kept to a mimimum. Do not do any soft*/
		/*  locking in Splitter programs. The soft lock should be done by*/
		/*  the subsequent program which reads the temporary file.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/* In the update section we write a record to the temporary file*/
		/* member identified by the value of IY. If it is possible to*/
		/* to include the RRN from the primary file we should pass*/
		/* this data as the subsequent program can then use it to do*/
		/* a direct read which is faster than a normal read.*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialize(wsaaFetchArrayInner.wsaaAgptData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the RLDGACCT being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last RLDGACCT*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next PAYX member to write to if they have changed.*/
		if (isNE(wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()], wsaaPrevRldgacct)) {
			wsaaPrevRldgacct.set(wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all AGPT data for the same contract until*/
		/*  the AGPBNUM on AGPT has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()], wsaaPrevRldgacct)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
 {	// Add by yy for ILIFE-763
		if (wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()] != null
				&& (!wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()].isEmptyOrNull())) { // Add End
			agptpfData.rldgcoy.set(wsaaFetchArrayInner.wsaaRldgcoy[wsaaInd.toInt()]);
			agptpfData.rldgacct.set(wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()]);
			agptpfData.sacscurbal.set(wsaaFetchArrayInner.wsaaSacscurbal[wsaaInd.toInt()]);
			agptpfData.procflg.set(wsaaFetchArrayInner.wsaaProcflg[wsaaInd.toInt()]);
			agptpfData.tagsusind.set(wsaaFetchArrayInner.wsaaTagsusind[wsaaInd.toInt()]);
			agptpfData.taxcde.set(wsaaFetchArrayInner.wsaaTaxcde[wsaaInd.toInt()]);
			agptpfData.currcode.set(wsaaFetchArrayInner.wsaaCurrcode[wsaaInd.toInt()]);
			agptpfData.agccqind.set(wsaaFetchArrayInner.wsaaAgccqind[wsaaInd.toInt()]);
			agptpfData.payclt.set(wsaaFetchArrayInner.wsaaPayclt[wsaaInd.toInt()]);
			agptpfData.facthous.set(wsaaFetchArrayInner.wsaaFacthous[wsaaInd.toInt()]);
			agptpfData.bankkey.set(wsaaFetchArrayInner.wsaaBankkey[wsaaInd.toInt()]);
			agptpfData.bankacckey.set(wsaaFetchArrayInner.wsaaBankacckey[wsaaInd.toInt()]);
			agptpfData.whtax.set(wsaaFetchArrayInner.wsaaWhtax[wsaaInd.toInt()]);
			agptpfData.tcolbal.set(wsaaFetchArrayInner.wsaaTcolbal[wsaaInd.toInt()]);
			agptpfData.tdeduct.set(wsaaFetchArrayInner.wsaaTdeduct[wsaaInd.toInt()]);
			agptpfData.tdcchrg.set(wsaaFetchArrayInner.wsaaTdcchrg[wsaaInd.toInt()]);
			agptpfData.paymth.set(wsaaFetchArrayInner.wsaaPaymth[wsaaInd.toInt()]);
			if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
				iy.set(1);
			}
			/* Write records to their corresponding temporary file */
			/* members, determined by the working storage count, IY. */
			/* This spreads the transaction members evenly across */
			/* the thread members. */
			if (isEQ(iy, 1)) {
				agpt01.write(agptpfData);
			}
			if (isEQ(iy, 2)) {
				agpt02.write(agptpfData);
			}
			if (isEQ(iy, 3)) {
				agpt03.write(agptpfData);
			}
			if (isEQ(iy, 4)) {
				agpt04.write(agptpfData);
			}
			if (isEQ(iy, 5)) {
				agpt05.write(agptpfData);
			}
			if (isEQ(iy, 6)) {
				agpt06.write(agptpfData);
			}
			if (isEQ(iy, 7)) {
				agpt07.write(agptpfData);
			}
			if (isEQ(iy, 8)) {
				agpt08.write(agptpfData);
			}
			if (isEQ(iy, 9)) {
				agpt09.write(agptpfData);
			}
			if (isEQ(iy, 10)) {
				agpt10.write(agptpfData);
			}
			if (isEQ(iy, 11)) {
				agpt11.write(agptpfData);
			}
			if (isEQ(iy, 12)) {
				agpt12.write(agptpfData);
			}
			if (isEQ(iy, 13)) {
				agpt13.write(agptpfData);
			}
			if (isEQ(iy, 14)) {
				agpt14.write(agptpfData);
			}
			if (isEQ(iy, 15)) {
				agpt15.write(agptpfData);
			}
			if (isEQ(iy, 16)) {
				agpt16.write(agptpfData);
			}
			if (isEQ(iy, 17)) {
				agpt17.write(agptpfData);
			}
			if (isEQ(iy, 18)) {
				agpt18.write(agptpfData);
			}
			if (isEQ(iy, 19)) {
				agpt19.write(agptpfData);
			}
			if (isEQ(iy, 20)) {
				agpt20.write(agptpfData);
			}
			/* Log the number of extacted records. */
			contotrec.totval.set(1);
			contotrec.totno.set(ct02);
			callContot001();
			/* Set up the array for the next block of records. */
			wsaaInd.add(1);
		}
		/* Check for an incomplete block retrieved. */
		if (isEQ(wsaaFetchArrayInner.wsaaRldgacct[wsaaInd.toInt()], SPACES) && isLTE(wsaaInd, wsaaRowsInBlock)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlagpbCursorconn, sqlagpbCursorps, sqlagpbCursorrs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			agpt01.close();
		}
		if (isEQ(iz, 2)) {
			agpt02.close();
		}
		if (isEQ(iz, 3)) {
			agpt03.close();
		}
		if (isEQ(iz, 4)) {
			agpt04.close();
		}
		if (isEQ(iz, 5)) {
			agpt05.close();
		}
		if (isEQ(iz, 6)) {
			agpt06.close();
		}
		if (isEQ(iz, 7)) {
			agpt07.close();
		}
		if (isEQ(iz, 8)) {
			agpt08.close();
		}
		if (isEQ(iz, 9)) {
			agpt09.close();
		}
		if (isEQ(iz, 10)) {
			agpt10.close();
		}
		if (isEQ(iz, 11)) {
			agpt11.close();
		}
		if (isEQ(iz, 12)) {
			agpt12.close();
		}
		if (isEQ(iz, 13)) {
			agpt13.close();
		}
		if (isEQ(iz, 14)) {
			agpt14.close();
		}
		if (isEQ(iz, 15)) {
			agpt15.close();
		}
		if (isEQ(iz, 16)) {
			agpt16.close();
		}
		if (isEQ(iz, 17)) {
			agpt17.close();
		}
		if (isEQ(iz, 18)) {
			agpt18.close();
		}
		if (isEQ(iz, 19)) {
			agpt19.close();
		}
		if (isEQ(iz, 20)) {
			agpt20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaAgptData = FLSInittedArray (1000, 106);
	private FixedLengthStringData[] wsaaRldgcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgptData, 0);
	private FixedLengthStringData[] wsaaRldgacct = FLSDArrayPartOfArrayStructure(16, wsaaAgptData, 1);
	private PackedDecimalData[] wsaaSacscurbal = PDArrayPartOfArrayStructure(17, 2, wsaaAgptData, 17);
	private FixedLengthStringData[] wsaaProcflg = FLSDArrayPartOfArrayStructure(1, wsaaAgptData, 26);
	private FixedLengthStringData[] wsaaTagsusind = FLSDArrayPartOfArrayStructure(1, wsaaAgptData, 27);
	private FixedLengthStringData[] wsaaTaxcde = FLSDArrayPartOfArrayStructure(2, wsaaAgptData, 28);
	private FixedLengthStringData[] wsaaCurrcode = FLSDArrayPartOfArrayStructure(3, wsaaAgptData, 30);
	private FixedLengthStringData[] wsaaAgccqind = FLSDArrayPartOfArrayStructure(1, wsaaAgptData, 33);
	private FixedLengthStringData[] wsaaPayclt = FLSDArrayPartOfArrayStructure(8, wsaaAgptData, 34);
	private FixedLengthStringData[] wsaaFacthous = FLSDArrayPartOfArrayStructure(2, wsaaAgptData, 42);
	private FixedLengthStringData[] wsaaBankkey = FLSDArrayPartOfArrayStructure(10, wsaaAgptData, 44);
	private FixedLengthStringData[] wsaaBankacckey = FLSDArrayPartOfArrayStructure(20, wsaaAgptData, 54);
	private PackedDecimalData[] wsaaWhtax = PDArrayPartOfArrayStructure(13, 2, wsaaAgptData, 74);
	private PackedDecimalData[] wsaaTcolbal = PDArrayPartOfArrayStructure(14, 2, wsaaAgptData, 81);
	private PackedDecimalData[] wsaaTdeduct = PDArrayPartOfArrayStructure(12, 2, wsaaAgptData, 89);
	private PackedDecimalData[] wsaaTdcchrg = PDArrayPartOfArrayStructure(14, 2, wsaaAgptData, 96);
	private FixedLengthStringData[] wsaaPaymth = FLSDArrayPartOfArrayStructure(2, wsaaAgptData, 104);
}
}
