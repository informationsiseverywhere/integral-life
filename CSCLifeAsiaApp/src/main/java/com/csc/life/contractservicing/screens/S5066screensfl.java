package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5066screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 8;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 17, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5066ScreenVars sv = (S5066ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5066screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5066screensfl, 
			sv.S5066screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5066ScreenVars sv = (S5066ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5066screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5066ScreenVars sv = (S5066ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5066screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5066screensflWritten.gt(0))
		{
			sv.s5066screensfl.setCurrentIndex(0);
			sv.S5066screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5066ScreenVars sv = (S5066ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5066screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5066ScreenVars screenVars = (S5066ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fund.setFieldName("fund");
				screenVars.fundtype.setFieldName("fundtype");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.fund.set(dm.getField("fund"));
			screenVars.fundtype.set(dm.getField("fundtype"));
			screenVars.shortds.set(dm.getField("shortds"));
			screenVars.cnstcur.set(dm.getField("cnstcur"));
			screenVars.estMatValue.set(dm.getField("estMatValue"));
			screenVars.actvalue.set(dm.getField("actvalue"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5066ScreenVars screenVars = (S5066ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.fund.setFieldName("fund");
				screenVars.fundtype.setFieldName("fundtype");
				screenVars.shortds.setFieldName("shortds");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.actvalue.setFieldName("actvalue");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("fund").set(screenVars.fund);
			dm.getField("fundtype").set(screenVars.fundtype);
			dm.getField("shortds").set(screenVars.shortds);
			dm.getField("cnstcur").set(screenVars.cnstcur);
			dm.getField("estMatValue").set(screenVars.estMatValue);
			dm.getField("actvalue").set(screenVars.actvalue);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5066screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5066ScreenVars screenVars = (S5066ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.fund.clearFormatting();
		screenVars.fundtype.clearFormatting();
		screenVars.shortds.clearFormatting();
		screenVars.cnstcur.clearFormatting();
		screenVars.estMatValue.clearFormatting();
		screenVars.actvalue.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5066ScreenVars screenVars = (S5066ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.fund.setClassString("");
		screenVars.fundtype.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.cnstcur.setClassString("");
		screenVars.estMatValue.setClassString("");
		screenVars.actvalue.setClassString("");
	}

/**
 * Clear all the variables in S5066screensfl
 */
	public static void clear(VarModel pv) {
		S5066ScreenVars screenVars = (S5066ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hcrtable.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.fund.clear();
		screenVars.fundtype.clear();
		screenVars.shortds.clear();
		screenVars.cnstcur.clear();
		screenVars.estMatValue.clear();
		screenVars.actvalue.clear();
	}
}
