package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:55
 * Description:
 * Copybook name: GENOUTREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Genoutrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData outRec = new FixedLengthStringData(26);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(outRec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(outRec, 1);
  	public PackedDecimalData oldSummary = new PackedDecimalData(4, 0).isAPartOf(outRec, 9);
  	public PackedDecimalData newSummary = new PackedDecimalData(4, 0).isAPartOf(outRec, 12);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(outRec, 15);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(outRec, 19);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(outRec, 23);


	public void initialize() {
		COBOLFunctions.initialize(outRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		outRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}