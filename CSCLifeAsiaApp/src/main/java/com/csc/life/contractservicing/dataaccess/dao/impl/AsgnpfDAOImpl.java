package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.csc.life.contractservicing.dataaccess.dao.AsgnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Asgnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AsgnpfDAOImpl extends BaseDAOImpl<Asgnpf> implements AsgnpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AsgnpfDAOImpl.class);
	
	private static final String SQLForGetAsgnpfByChdrNum = "select UNIQUE_NUMBER, CHDRCOY, CHDRNUM, ASGNPFX, ASGNNUM, ASSIGNAME,"
			+ " REASONCD, COMMFROM, COMMTO, TRANNO, SEQNO, TRANCDE, TERMID, TRDT, TRTM, USER_T"
			+ " from Asgnpf where chdrnum = ?"
			+ " and chdrcoy = ?";
	@Override
	public List<Asgnpf> getAsgnpfByChdrNum(String chdrcoy, String chdrnum,
			String seqno) {
		List<Asgnpf> result = new ArrayList<>();
		String sql = SQLForGetAsgnpfByChdrNum;
		if (seqno != null && !"".equals(seqno.trim())) {
			sql += " and seqno = ?";
		}
		sql += " order by seqno";
		try (PreparedStatement prep = getPrepareStatement(sql)) {
			prep.setString(1, chdrnum);
			prep.setString(2, chdrcoy);
			if (seqno != null && !"".equals(seqno.trim())) {
				prep.setString(3, seqno);
			}
			ResultSet rs = prep.executeQuery();
			int i = 1;
			BeanPropertyRowMapper<Asgnpf> rowMapper = BeanPropertyRowMapper.newInstance(Asgnpf.class);
			while (rs.next()) {
				Asgnpf asgnpf = rowMapper.mapRow(rs, i);
				result.add(asgnpf);
				i++;
			}
		} catch (SQLException e) {
			LOGGER.error("getAsgnpfByChdrNum()", e);
			throw new SQLRuntimeException(e);
		}
		return result;
	}

}
