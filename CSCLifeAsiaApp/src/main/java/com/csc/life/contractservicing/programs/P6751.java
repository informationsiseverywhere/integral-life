/*
 * File: P6751.java
 * Date: 30 August 2009 0:55:34
 * Author: Quipoz Limited
 * 
 * Class transformed from P6751.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.CwfdTableDAM;
import com.csc.life.contractservicing.screens.S6751ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*           P6751 - WINDFORWARD SUB MENU PROGRAM
*           ====================================
*
* This submenu provides the user with a selection of Windforward
*  after Reversal transactions. Depending on the transaction
*  selected, the program stores a copy of the current Contract
*  Header record to pass onto the next program in the processing
*  sequence.
*
*  Available windforward transactions are...
*
*    A - Contract Enquiry
*    B - Windforward Registration
*    C - Insert Batch Transactions
*    D - Windforward Confirmation
*    E - Display Windforward List
*    F - Cancel Windforward
*
* Initialisation
* --------------
*
* Contract number and effective date are initialised.
* Default action is set up from WSSP-SBMACTION.
*
* Validation
* ----------
*
* Action.
*     Must be A, B, C, D, E or F.
*
*  Validate contract number entered by performing a READR on the
*  Logical file CHDRENQ.
*
*  Validate Key - 1
*
*       If S6751-CHDRSEL not  equal  to  spaces call the CHDRENQ
*       I/O module for  the  contract.  If  any  combination  of
*       CHDRSEL and SUBP-KEY1 other than O-K and 'Y' then error.
*
*       Check STATCODE to validate  status of the contract
*       against T5679.
*
*       If NOT valid then Error.
*
*       If action D, E or F must have Windforward record.
*
*
* Update
* ------
*
* Set up WSSP-FLAG:
*     When  A       set to I (Inquire).
*     When  D       Set to C (Create).
*     When  F       set to D (Delete).
*     When  other   set to M (Modify).
*
*
* For transactions other than enquiry, soft lock the Contract
* Header. If the contract is already locked, display an error
* message.
*
* Store the Contract header in the I/O module for the transaction
* programs called.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6751 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6751");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private String wsaaValidStatuz = "";
		/* ERRORS */
	private static final String d005 = "D005";
	private static final String d031 = "D031";
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e767 = "E767";
	private static final String f910 = "F910";
	private static final String f917 = "F917";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String cwfdrec = "CWFDREC";
	private static final String itemrec = "ITEMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CwfdTableDAM cwfdIO = new CwfdTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private S6751ScreenVars sv = ScreenProgram.getScreenVars( S6751ScreenVars.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2290, 
		exit12090, 
		keeps3070, 
		exit3090
	}

	public P6751() {
		super();
		screenVars = sv;
		new ScreenModel("S6751", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		try {
			validateKey12210();
			continue2280();
		}
		catch (GOTOException e){
		}
	}

protected void validateKey12210()
	{
		if (isEQ(sv.chdrsel,SPACES)) {
			sv.chdrselErr.set(d031);
			goTo(GotoLabel.exit2290);
		}
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrsel);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim()); //ILIFE-6816
		chdrpfDAO.setCacheObject(chdrpf); //ILIFE-6816
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(f917);
			goTo(GotoLabel.exit2290);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			goTo(GotoLabel.exit2290);
		}
	}

protected void continue2280()
	{
		checkStatus2300();
		if (isEQ(sv.action,"D")
		|| isEQ(sv.action,"E")
		|| isEQ(sv.action,"F")) {
			validateWindforward2500();
		}
	}

protected void checkStatus2300()
	{
		readStatusTable2310();
	}

protected void readStatusTable2310()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatuz = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
			lookForStat2400();
		}
		if (isNE(wsaaValidStatuz,"Y")) {
			sv.chdrselErr.set(e767);
			wsspcomn.edterror.set("Y");
		}
	}

protected void lookForStat2400()
	{
		/*SEARCH*/
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()],chdrenqIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()],chdrenqIO.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void validateWindforward2500()
	{
		read2510();
	}

protected void read2510()
	{
		cwfdIO.setDataArea(SPACES);
		cwfdIO.setChdrcoy(wsspcomn.company);
		cwfdIO.setChdrnum(sv.chdrsel);
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		cwfdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		cwfdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		cwfdIO.setFormat(cwfdrec);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)
		&& isNE(cwfdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(cwfdIO.getParams());
			syserrrec.statuz.set(cwfdIO.getStatuz());
			fatalError600();
		}
		if (isNE(cwfdIO.getChdrcoy(),wsspcomn.company)
		|| isNE(cwfdIO.getChdrnum(),sv.chdrsel)) {
			cwfdIO.setStatuz(varcom.endp);
		}
		if (isEQ(cwfdIO.getStatuz(),varcom.endp)) {
			sv.chdrselErr.set(d005);
			wsspcomn.edterror.set("Y");
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateWssp3010();
					softlock3020();
				}
				case keeps3070: {
					keeps3070();
					batching3080();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(sv.action,"A")){
			wsspcomn.flag.set("I");
		}
		else if (isEQ(sv.action,"F")){
			wsspcomn.flag.set("D");
		}
		else if (isEQ(sv.action,"D")){
			wsspcomn.flag.set("C");
		}
		else if (isEQ(sv.action,"E")){
			wsspcomn.flag.set("E");
		}
		else{
			wsspcomn.flag.set("M");
		}
		if (isEQ(sv.action,"A")) {
			goTo(GotoLabel.keeps3070);
		}
	}

protected void softlock3020()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrenqIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("LOCK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void keeps3070()
	{
		chdrenqIO.setFunction(varcom.keeps);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
