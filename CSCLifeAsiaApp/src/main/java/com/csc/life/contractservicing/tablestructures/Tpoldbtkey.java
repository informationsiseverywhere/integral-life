package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:57
 * Description:
 * Copybook name: TPOLDBTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tpoldbtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData tpoldbtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData tpoldbtKey = new FixedLengthStringData(64).isAPartOf(tpoldbtFileKey, 0, REDEFINE);
  	public FixedLengthStringData tpoldbtChdrcoy = new FixedLengthStringData(1).isAPartOf(tpoldbtKey, 0);
  	public FixedLengthStringData tpoldbtChdrnum = new FixedLengthStringData(8).isAPartOf(tpoldbtKey, 1);
  	public PackedDecimalData tpoldbtTranno = new PackedDecimalData(5, 0).isAPartOf(tpoldbtKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(tpoldbtKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tpoldbtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tpoldbtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}