package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:43
 * Description:
 * Copybook name: REGPBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regpbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regpbrkFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData regpbrkKey = new FixedLengthStringData(256).isAPartOf(regpbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData regpbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(regpbrkKey, 0);
  	public FixedLengthStringData regpbrkChdrnum = new FixedLengthStringData(8).isAPartOf(regpbrkKey, 1);
  	public PackedDecimalData regpbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(regpbrkKey, 9);
  	public PackedDecimalData regpbrkTranno = new PackedDecimalData(5, 0).isAPartOf(regpbrkKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(regpbrkKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regpbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regpbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}