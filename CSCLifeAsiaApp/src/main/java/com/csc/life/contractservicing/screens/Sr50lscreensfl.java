package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50lscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 7, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 13;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 20, 3, 62}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50lScreenVars sv = (Sr50lScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr50lscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr50lscreensfl, 
			sv.Sr50lscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr50lScreenVars sv = (Sr50lScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr50lscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr50lScreenVars sv = (Sr50lScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr50lscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr50lscreensflWritten.gt(0))
		{
			sv.sr50lscreensfl.setCurrentIndex(0);
			sv.Sr50lscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr50lScreenVars sv = (Sr50lScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr50lscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50lScreenVars screenVars = (Sr50lScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.anbage.setFieldName("anbage");
				screenVars.tranno.setFieldName("tranno");
				screenVars.ind.setFieldName("ind");
				screenVars.select.setFieldName("select");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.status.setFieldName("status");
				screenVars.ccdateDisp.setFieldName("ccdateDisp");
				screenVars.ptdateDisp.setFieldName("ptdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.life.set(dm.getField("life"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.anbage.set(dm.getField("anbage"));
			screenVars.tranno.set(dm.getField("tranno"));
			screenVars.ind.set(dm.getField("ind"));
			screenVars.select.set(dm.getField("select"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.status.set(dm.getField("status"));
			screenVars.ccdateDisp.set(dm.getField("ccdateDisp"));
			screenVars.ptdateDisp.set(dm.getField("ptdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50lScreenVars screenVars = (Sr50lScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.anbage.setFieldName("anbage");
				screenVars.tranno.setFieldName("tranno");
				screenVars.ind.setFieldName("ind");
				screenVars.select.setFieldName("select");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.status.setFieldName("status");
				screenVars.ccdateDisp.setFieldName("ccdateDisp");
				screenVars.ptdateDisp.setFieldName("ptdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("life").set(screenVars.life);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("anbage").set(screenVars.anbage);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("ind").set(screenVars.ind);
			dm.getField("select").set(screenVars.select);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("status").set(screenVars.status);
			dm.getField("ccdateDisp").set(screenVars.ccdateDisp);
			dm.getField("ptdateDisp").set(screenVars.ptdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr50lscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr50lScreenVars screenVars = (Sr50lScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.anbage.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.ind.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.status.clearFormatting();
		screenVars.ccdateDisp.clearFormatting();
		screenVars.ptdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr50lScreenVars screenVars = (Sr50lScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.anbage.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.select.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.status.setClassString("");
		screenVars.ccdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr50lscreensfl
 */
	public static void clear(VarModel pv) {
		Sr50lScreenVars screenVars = (Sr50lScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.anbage.clear();
		screenVars.tranno.clear();
		screenVars.ind.clear();
		screenVars.select.clear();
		screenVars.chdrnum.clear();
		screenVars.status.clear();
		screenVars.ccdateDisp.clear();
		screenVars.ccdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
	}
}
