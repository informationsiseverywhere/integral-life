package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5047screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 6;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 1, 2, 10, 7, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 16, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5047ScreenVars sv = (S5047ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5047screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5047screensfl, 
			sv.S5047screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5047ScreenVars sv = (S5047ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5047screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5047ScreenVars sv = (S5047ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5047screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5047screensflWritten.gt(0))
		{
			sv.s5047screensfl.setCurrentIndex(0);
			sv.S5047screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5047ScreenVars sv = (S5047ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5047screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5047ScreenVars screenVars = (S5047ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.newinst.setFieldName("newinst");
				screenVars.lastInst.setFieldName("lastInst");
				screenVars.lastSumi.setFieldName("lastSumi");
				screenVars.newsumi.setFieldName("newsumi");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.select.setFieldName("select");
				screenVars.refusalFlag.setFieldName("refusalFlag");
				screenVars.hrrn.setFieldName("hrrn");
				screenVars.hselect.setFieldName("hselect");
				screenVars.hiddenAnnvryMethod.setFieldName("hiddenAnnvryMethod");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.newinst.set(dm.getField("newinst"));
			screenVars.lastInst.set(dm.getField("lastInst"));
			screenVars.lastSumi.set(dm.getField("lastSumi"));
			screenVars.newsumi.set(dm.getField("newsumi"));
			screenVars.crrcdDisp.set(dm.getField("crrcdDisp"));
			screenVars.select.set(dm.getField("select"));
			screenVars.refusalFlag.set(dm.getField("refusalFlag"));
			screenVars.hrrn.set(dm.getField("hrrn"));
			screenVars.hselect.set(dm.getField("hselect"));
			screenVars.hiddenAnnvryMethod.set(dm.getField("hiddenAnnvryMethod"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5047ScreenVars screenVars = (S5047ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.newinst.setFieldName("newinst");
				screenVars.lastInst.setFieldName("lastInst");
				screenVars.lastSumi.setFieldName("lastSumi");
				screenVars.newsumi.setFieldName("newsumi");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.select.setFieldName("select");
				screenVars.refusalFlag.setFieldName("refusalFlag");
				screenVars.hrrn.setFieldName("hrrn");
				screenVars.hselect.setFieldName("hselect");
				screenVars.hiddenAnnvryMethod.setFieldName("hiddenAnnvryMethod");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("newinst").set(screenVars.newinst);
			dm.getField("lastInst").set(screenVars.lastInst);
			dm.getField("lastSumi").set(screenVars.lastSumi);
			dm.getField("newsumi").set(screenVars.newsumi);
			dm.getField("crrcdDisp").set(screenVars.crrcdDisp);
			dm.getField("select").set(screenVars.select);
			dm.getField("refusalFlag").set(screenVars.refusalFlag);
			dm.getField("hrrn").set(screenVars.hrrn);
			dm.getField("hselect").set(screenVars.hselect);
			dm.getField("hiddenAnnvryMethod").set(screenVars.hiddenAnnvryMethod);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5047screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5047ScreenVars screenVars = (S5047ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.newinst.clearFormatting();
		screenVars.lastInst.clearFormatting();
		screenVars.lastSumi.clearFormatting();
		screenVars.newsumi.clearFormatting();
		screenVars.crrcdDisp.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.refusalFlag.clearFormatting();
		screenVars.hrrn.clearFormatting();
		screenVars.hselect.clearFormatting();
		screenVars.hiddenAnnvryMethod.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5047ScreenVars screenVars = (S5047ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.newinst.setClassString("");
		screenVars.lastInst.setClassString("");
		screenVars.lastSumi.setClassString("");
		screenVars.newsumi.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.select.setClassString("");
		screenVars.refusalFlag.setClassString("");
		screenVars.hrrn.setClassString("");
		screenVars.hselect.setClassString("");
		screenVars.hiddenAnnvryMethod.setClassString("");
	}

/**
 * Clear all the variables in S5047screensfl
 */
	public static void clear(VarModel pv) {
		S5047ScreenVars screenVars = (S5047ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.newinst.clear();
		screenVars.lastInst.clear();
		screenVars.lastSumi.clear();
		screenVars.newsumi.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.select.clear();
		screenVars.refusalFlag.clear();
		screenVars.hrrn.clear();
		screenVars.hselect.clear();
		screenVars.hiddenAnnvryMethod.clear();
	}
}
