package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PrmhactTableDAM.java
 * Date: Sun, 30 Aug 2009 03:44:49
 * Class transformed from PRMHACT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PrmhactTableDAM extends PrmhpfTableDAM {

	public PrmhactTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("PRMHACT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "VALIDFLAG, " +
		            "ACTIND, " +
		            "FRMDATE, " +
		            "TODATE, " +
		            "TRANNO, " +
		            "EFFDATE, " +
		            "APIND, " +
		            "LOGTYPE, " +
		            "CRTUSER, " +
		            "LTRANNO, " +
		            "BILLFREQ, " +
		            "ZMTHOS, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               validflag,
                               activeInd,
                               frmdate,
                               todate,
                               tranno,
                               effdate,
                               apind,
                               logtype,
                               crtuser,
                               lastTranno,
                               billfreq,
                               zmthos,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(244);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller70.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(94);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getValidflag().toInternal()
					+ getActiveInd().toInternal()
					+ getFrmdate().toInternal()
					+ getTodate().toInternal()
					+ nonKeyFiller70.toInternal()
					+ getEffdate().toInternal()
					+ getApind().toInternal()
					+ getLogtype().toInternal()
					+ getCrtuser().toInternal()
					+ getLastTranno().toInternal()
					+ getBillfreq().toInternal()
					+ getZmthos().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, activeInd);
			what = ExternalData.chop(what, frmdate);
			what = ExternalData.chop(what, todate);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, apind);
			what = ExternalData.chop(what, logtype);
			what = ExternalData.chop(what, crtuser);
			what = ExternalData.chop(what, lastTranno);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, zmthos);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(Object what) {
		activeInd.set(what);
	}	
	public PackedDecimalData getFrmdate() {
		return frmdate;
	}
	public void setFrmdate(Object what) {
		setFrmdate(what, false);
	}
	public void setFrmdate(Object what, boolean rounded) {
		if (rounded)
			frmdate.setRounded(what);
		else
			frmdate.set(what);
	}	
	public PackedDecimalData getTodate() {
		return todate;
	}
	public void setTodate(Object what) {
		setTodate(what, false);
	}
	public void setTodate(Object what, boolean rounded) {
		if (rounded)
			todate.setRounded(what);
		else
			todate.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getApind() {
		return apind;
	}
	public void setApind(Object what) {
		apind.set(what);
	}	
	public FixedLengthStringData getLogtype() {
		return logtype;
	}
	public void setLogtype(Object what) {
		logtype.set(what);
	}	
	public FixedLengthStringData getCrtuser() {
		return crtuser;
	}
	public void setCrtuser(Object what) {
		crtuser.set(what);
	}	
	public PackedDecimalData getLastTranno() {
		return lastTranno;
	}
	public void setLastTranno(Object what) {
		setLastTranno(what, false);
	}
	public void setLastTranno(Object what, boolean rounded) {
		if (rounded)
			lastTranno.setRounded(what);
		else
			lastTranno.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public PackedDecimalData getZmthos() {
		return zmthos;
	}
	public void setZmthos(Object what) {
		setZmthos(what, false);
	}
	public void setZmthos(Object what, boolean rounded) {
		if (rounded)
			zmthos.setRounded(what);
		else
			zmthos.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		validflag.clear();
		activeInd.clear();
		frmdate.clear();
		todate.clear();
		nonKeyFiller70.clear();
		effdate.clear();
		apind.clear();
		logtype.clear();
		crtuser.clear();
		lastTranno.clear();
		billfreq.clear();
		zmthos.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}