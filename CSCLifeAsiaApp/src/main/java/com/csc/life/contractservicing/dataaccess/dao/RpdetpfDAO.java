package com.csc.life.contractservicing.dataaccess.dao;


import com.csc.life.contractservicing.dataaccess.model.Rpdetpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;
import java.util.List;

public interface RpdetpfDAO {
	
	public List<Rpdetpf> getRpdetRecord(String chdrnum, int loannum);
	public void updateRpdetRecordList(List<Rpdetpf> rpdetpfUpList);
	public void insertRpdetRecord(List<Rpdetpf> rpdetpfList)throws SQLRuntimeException;	
	public List<Rpdetpf> getRpdetRecordList( String chdrnum, int tranno);
	public void deleteRpdetpf(String chdrnum , int tranno);
	public void updateRpdetpftrannoRecord(Rpdetpf rpdetpf);
}
