package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import java.util.HashMap;
import java.util.Map;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR572
 * @version 1.0 generated on 30/08/09 07:20
 * @author Quipoz
 */
public class Sr572ScreenVars extends SmartVarModel { 

//ILIFE-1403 START by nnazeer
	public FixedLengthStringData dataArea = new FixedLengthStringData(502);
	public FixedLengthStringData dataFields = new FixedLengthStringData(214).isAPartOf(dataArea, 0);
//ILIFE-1403 END
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public ZonedDecimalData huwdcdte = DD.huwdcdte.copyToZonedDecimal().isAPartOf(dataFields,41);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData lifeorcov = DD.lifeorcov.copy().isAPartOf(dataFields,116);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,117);
	//ILIFE-1403 START by nnazeer
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,167);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,177);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,187);
	public FixedLengthStringData entity = DD.entity.copy().isAPartOf(dataFields,190);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,206);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,210);
	//ILIFE-1403 END
	

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 214);    	//ILIFE-1403 START by nnazeer
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData huwdcdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifeorcovErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	//ILIFE-1403 START by nnazeer
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	//ILIFE-1403 END
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 286); 	//ILIFE-1403 START by nnazeer
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] huwdcdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifeorcovOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	//ILIFE-1403 START by nnazeer
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	//ILIFE-1403 END
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(212);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(50).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clcode = DD.clcode.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData crcode = DD.crcode.copy().isAPartOf(subfileFields,6);
	public FixedLengthStringData hrequired = DD.hrequired.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData rreq = DD.rreq.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData rtable = DD.rtable.copy().isAPartOf(subfileFields,45);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,49);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 50);
	public FixedLengthStringData clcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData crcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hrequiredErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData rreqErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData rtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 90);
	public FixedLengthStringData[] clcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] crcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hrequiredOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] rreqOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] rtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 210);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData huwdcdteDisp = new FixedLengthStringData(10);

	public LongData Sr572screensflWritten = new LongData(0);
	public LongData Sr572screenctlWritten = new LongData(0);
	public LongData Sr572screenWritten = new LongData(0);
	public LongData Sr572protectWritten = new LongData(0);
	public GeneralTable sr572screensfl = new GeneralTable(AppVars.getInstance());
	
	public Map<String, Integer> uwResultMap = new HashMap<>();
	public int uwFlag = 0;
	public int mainCovrDisPos = -1;
	public int tableCovrDisPos = -1;
	public int mainCovrDisFlag = -1;
	public int tableCovrDisFlag = -1;
	
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr572screensfl;
	}

	public Sr572ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"12","01","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifcnumOut,new String[] {"13","02","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifcnumOut,new String[] {"14","03","-14",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {rreq, clcode, coverage, rider, indic, select, crcode, rtable, longdesc, hrequired};
		screenSflOutFields = new BaseData[][] {rreqOut, clcodeOut, coverageOut, riderOut, indicOut, selectOut, crcodeOut, rtableOut, longdescOut, hrequiredOut};
		screenSflErrFields = new BaseData[] {rreqErr, clcodeErr, coverageErr, riderErr, indicErr, selectErr, crcodeErr, rtableErr, longdescErr, hrequiredErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, life, lifcnum, linsname, jlife, jlifcnum, jlinsname, lifeorcov, huwdcdte,register,chdrstatus,premstatus,cntcurr,entity,numpols,planSuffix}; 	//ILIFE-1403 START by nnazeer
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifeOut, lifcnumOut, linsnameOut, jlifeOut, jlifcnumOut, jlinsnameOut, lifeorcovOut, huwdcdteOut,registerOut,chdrstatusOut,premstatusOut,cntcurrOut,entityOut,numpolsOut,plnsfxOut};	//ILIFE-1403 START by nnazeer
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifeErr, lifcnumErr, linsnameErr, jlifeErr, jlifcnumErr, jlinsnameErr, lifeorcovErr, huwdcdteErr,registerErr,chdrstatusErr,premstatusErr,cntcurrErr,entityErr,numpolsErr,plnsfxErr};	//ILIFE-1403 START by nnazeer
		screenDateFields = new BaseData[] {huwdcdte};
		screenDateErrFields = new BaseData[] {huwdcdteErr};
		screenDateDispFields = new BaseData[] {huwdcdteDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr572screen.class;
		screenSflRecord = Sr572screensfl.class;
		screenCtlRecord = Sr572screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr572protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr572screenctl.lrec.pageSubfile);
	}
}
