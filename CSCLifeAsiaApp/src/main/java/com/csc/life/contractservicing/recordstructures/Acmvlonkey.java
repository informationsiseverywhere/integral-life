package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:42
 * Description:
 * Copybook name: ACMVLONKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvlonkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvlonFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvlonKey = new FixedLengthStringData(64).isAPartOf(acmvlonFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvlonRldgcoy = new FixedLengthStringData(1).isAPartOf(acmvlonKey, 0);
  	public FixedLengthStringData acmvlonSacscode = new FixedLengthStringData(2).isAPartOf(acmvlonKey, 1);
  	public FixedLengthStringData acmvlonSacstyp = new FixedLengthStringData(2).isAPartOf(acmvlonKey, 3);
  	public FixedLengthStringData acmvlonRldgacct = new FixedLengthStringData(16).isAPartOf(acmvlonKey, 5);
  	public PackedDecimalData acmvlonEffdate = new PackedDecimalData(8, 0).isAPartOf(acmvlonKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(acmvlonKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvlonFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvlonFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}