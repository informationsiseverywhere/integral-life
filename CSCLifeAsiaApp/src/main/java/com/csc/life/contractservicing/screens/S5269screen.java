package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5269screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {17, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5269ScreenVars sv = (S5269ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5269screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5269ScreenVars screenVars = (S5269ScreenVars)pv;
		screenVars.policyloan.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.causeofdth.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.fupflg.setClassString("");
		screenVars.estimateTotalValue.setClassString("");
		screenVars.clamamt.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.dtofdeathDisp.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.zrcshamt.setClassString("");
		screenVars.tdbtamt.setClassString("");
	}

/**
 * Clear all the variables in S5269screen
 */
	public static void clear(VarModel pv) {
		S5269ScreenVars screenVars = (S5269ScreenVars) pv;
		screenVars.policyloan.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.causeofdth.clear();
		screenVars.otheradjst.clear();
		screenVars.fupflg.clear();
		screenVars.estimateTotalValue.clear();
		screenVars.clamamt.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
		screenVars.dtofdeathDisp.clear();
		screenVars.dtofdeath.clear();
		screenVars.currcd.clear();
		screenVars.zrcshamt.clear();
		screenVars.tdbtamt.clear();
	}
}
