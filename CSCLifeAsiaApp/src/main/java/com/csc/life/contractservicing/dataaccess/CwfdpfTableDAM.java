package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CwfdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:38
 * Class transformed from CWFDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CwfdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 104;
	public FixedLengthStringData cwfdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData cwfdpfRecord = cwfdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(cwfdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(cwfdrec);
	public PackedDecimalData efdate = DD.efdate.copy().isAPartOf(cwfdrec);
	public PackedDecimalData prcSeqNbr = DD.prcseqnbr.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData jobnameUser = DD.bsjobuser.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData jobnameJob = DD.bsjobjob.copy().isAPartOf(cwfdrec);
	public PackedDecimalData jobnameNumber = DD.bsjobno.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(cwfdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(cwfdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CwfdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CwfdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CwfdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CwfdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CwfdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CwfdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CwfdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CWFDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANCD, " +
							"TRANNO, " +
							"EFDATE, " +
							"PRCSEQNBR, " +
							"BSCHEDNAM, " +
							"BSJOBUSER, " +
							"BSJOBJOB, " +
							"BSJOBNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     trancd,
                                     tranno,
                                     efdate,
                                     prcSeqNbr,
                                     scheduleName,
                                     jobnameUser,
                                     jobnameJob,
                                     jobnameNumber,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		trancd.clear();
  		tranno.clear();
  		efdate.clear();
  		prcSeqNbr.clear();
  		scheduleName.clear();
  		jobnameUser.clear();
  		jobnameJob.clear();
  		jobnameNumber.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCwfdrec() {
  		return cwfdrec;
	}

	public FixedLengthStringData getCwfdpfRecord() {
  		return cwfdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCwfdrec(what);
	}

	public void setCwfdrec(Object what) {
  		this.cwfdrec.set(what);
	}

	public void setCwfdpfRecord(Object what) {
  		this.cwfdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(cwfdrec.getLength());
		result.set(cwfdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}