package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5081
 * @version 1.0 generated on 30/08/09 06:33
 * @author Quipoz
 */
public class S5081ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1496);
	public FixedLengthStringData dataFields = new FixedLengthStringData(872).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData clientname = DD.clientname.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData clientnum = DD.clientnum.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData clntwin = DD.clntwin.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,181);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,189);
	public FixedLengthStringData jownername = DD.jownername.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData jownnum = DD.jownnum.copy().isAPartOf(dataFields,266);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,274);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,275);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,283);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,330);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,340);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,348);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,352);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,402);
	/* ILIFE-7277-start  */
	public FixedLengthStringData clntwin2 = DD.clntwin.copy().isAPartOf(dataFields,412);
	public FixedLengthStringData clntwin3 = DD.clntwin.copy().isAPartOf(dataFields,420);
	public FixedLengthStringData clntwin4 = DD.clntwin.copy().isAPartOf(dataFields,428);
	public FixedLengthStringData clntwin5 = DD.clntwin.copy().isAPartOf(dataFields,436);
	public FixedLengthStringData clntname2 = DD.clntname.copy().isAPartOf(dataFields,444);
	public FixedLengthStringData clntname3 = DD.clntname.copy().isAPartOf(dataFields,494);
	public FixedLengthStringData clntname4 = DD.clntname.copy().isAPartOf(dataFields,544);
	public FixedLengthStringData clntname5 = DD.clntname.copy().isAPartOf(dataFields,594);
	public FixedLengthStringData cownnum2 = DD.cownnum.copy().isAPartOf(dataFields,644);
	public FixedLengthStringData cownnum3 = DD.cownnum.copy().isAPartOf(dataFields,652);
	public FixedLengthStringData cownnum4 = DD.cownnum.copy().isAPartOf(dataFields,660);
	public FixedLengthStringData cownnum5 = DD.cownnum.copy().isAPartOf(dataFields,668);
	public FixedLengthStringData ownername2 = DD.ownername.copy().isAPartOf(dataFields,676);
	public FixedLengthStringData ownername3 = DD.ownername.copy().isAPartOf(dataFields,723);
	public FixedLengthStringData ownername4 = DD.ownername.copy().isAPartOf(dataFields,770);
	public FixedLengthStringData ownername5 = DD.ownername.copy().isAPartOf(dataFields,817);
	public FixedLengthStringData relationwithlife = DD.relwLife.copy().isAPartOf(dataFields,864);
	public FixedLengthStringData relationwithlifejoint = DD.relwLife.copy().isAPartOf(dataFields,868);
	/* ILIFE-7277-end  */
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 872);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData clientnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData clientnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData clntwinErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	/* ILIFE-7277-start  */
	public FixedLengthStringData clntwin2Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData clntwin3Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData clntwin4Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData clntwin5Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData clntname2Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData clntname3Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData clntname4Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData clntname5Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData cownnum2Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData cownnum3Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData cownnum4Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData cownnum5Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData ownername2Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData ownername3Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData ownername4Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData ownername5Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	
	public FixedLengthStringData relationwithlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData relationwithlifejointErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	/* ILIFE-7277-end  */
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(468).isAPartOf(dataArea, 1028);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] clientnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] clientnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] clntwinOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	/* ILIFE-7277-start  */
	public FixedLengthStringData[] clntwin2Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] clntwin3Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] clntwin4Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] clntwin5Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] clntname2Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] clntname3Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] clntname4Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] clntname5Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] cownnum2Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] cownnum3Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] cownnum4Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] cownnum5Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] ownername2Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] ownername3Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] ownername4Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] ownername5Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] relationwithlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] relationwithlifejointOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456); 
	/* ILIFE-7277-end  */
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData multipleOwnerFlag = new FixedLengthStringData(1);//ILIFE-7277
	

	public LongData S5081screenWritten = new LongData(0);
	public LongData S5081protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5081ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"04", null, "-04", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntwinOut,new String[] {"01", null, "-01", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clientnumOut,new String[] {"02", null, "-02", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"37", null, "-37", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntwin2Out,new String[] {null, "41", null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntwin3Out,new String[] {null, "42", null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntwin4Out,new String[] {null, "43", null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntwin5Out,new String[] {null, "44", null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntname2Out,new String[] {null, null, null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntname3Out,new String[] {null, null, null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntname4Out,new String[] {null, null, null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntname5Out,new String[] {null, null, null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(relationwithlifeOut,new String[] {"30","31","-30","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(relationwithlifejointOut,new String[] {"33","34","-33","35",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, jownnum, jownername, agntnum, agentname, occdate, ptdate, billfreq, mop, clntwin,clntwin2,clntwin3,clntwin4,clntwin5, clientnum, clientname, reasoncd, resndesc, clntname,ownername2,ownername3,ownername4,ownername5,relationwithlife,relationwithlifejoint};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, jownnumOut, jownernameOut, agntnumOut, agentnameOut, occdateOut, ptdateOut, billfreqOut, mopOut, clntwinOut, clntwin2Out, clntwin3Out, clntwin4Out, clntwin5Out, clientnumOut, clientnameOut, reasoncdOut, resndescOut, clntnameOut,ownername2Out,ownername3Out,ownername4Out,ownername5Out,relationwithlifeOut,relationwithlifejointOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, jownnumErr, jownernameErr, agntnumErr, agentnameErr, occdateErr, ptdateErr, billfreqErr, mopErr, clntwinErr,clntwin2Err,clntwin3Err,clntwin4Err,clntwin5Err, clientnumErr, clientnameErr, reasoncdErr, resndescErr, clntnameErr,ownername2Err,ownername3Err,ownername4Err,ownername5Err,relationwithlifeErr,relationwithlifejointErr};
		screenDateFields = new BaseData[] {occdate, ptdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5081screen.class;
		protectRecord = S5081protect.class;
	}

}
