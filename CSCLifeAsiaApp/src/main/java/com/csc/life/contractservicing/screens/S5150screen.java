package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5150screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5150ScreenVars sv = (S5150ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5150screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5150ScreenVars screenVars = (S5150ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.cntinst.setClassString("");
		screenVars.todateDisp.setClassString("");
		screenVars.supflag.setClassString("");
		screenVars.suppressToDisp.setClassString("");
		screenVars.osbal.setClassString("");
	}

/**
 * Clear all the variables in S5150screen
 */
	public static void clear(VarModel pv) {
		S5150ScreenVars screenVars = (S5150ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.payrnum.clear();
		screenVars.payorname.clear();
		screenVars.agntnum.clear();
		screenVars.agentname.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.cntcurr.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.cntinst.clear();
		screenVars.todateDisp.clear();
		screenVars.todate.clear();
		screenVars.supflag.clear();
		screenVars.suppressToDisp.clear();
		screenVars.suppressTo.clear();
		screenVars.osbal.clear();
	}
}
