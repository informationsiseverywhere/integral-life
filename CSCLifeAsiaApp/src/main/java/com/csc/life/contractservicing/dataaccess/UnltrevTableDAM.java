package com.csc.life.contractservicing.dataaccess;

import com.csc.life.newbusiness.dataaccess.UnltpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UnltrevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:51:17
 * Class transformed from UNLTREV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UnltrevTableDAM extends UnltpfTableDAM {

	public UnltrevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UNLTREV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", SEQNBR";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "SEQNBR, " +
		            "CURRTO, " +
		            "CURRFROM, " +
		            "TRANNO, " +
		            "PTOPUP, " +
		            "NUMAPP, " +
		            "VALIDFLAG, " +
		            "UALFND01, " +
		            "UALFND02, " +
		            "UALFND03, " +
		            "UALFND04, " +
		            "UALFND05, " +
		            "UALFND06, " +
		            "UALFND07, " +
		            "UALFND08, " +
		            "UALFND09, " +
		            "UALFND10, " +
		            "UALPRC01, " +
		            "UALPRC02, " +
		            "UALPRC03, " +
		            "UALPRC04, " +
		            "UALPRC05, " +
		            "UALPRC06, " +
		            "UALPRC07, " +
		            "UALPRC08, " +
		            "UALPRC09, " +
		            "UALPRC10, " +
		            "USPCPR01, " +
		            "USPCPR02, " +
		            "USPCPR03, " +
		            "USPCPR04, " +
		            "USPCPR05, " +
		            "USPCPR06, " +
		            "USPCPR07, " +
		            "USPCPR08, " +
		            "USPCPR09, " +
		            "USPCPR10, " +
		            "PRCAMTIND, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "SEQNBR ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "SEQNBR DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               seqnbr,
                               currto,
                               currfrom,
                               tranno,
                               premTopupInd,
                               numapp,
                               validflag,
                               unitAllocFund01,
                               unitAllocFund02,
                               unitAllocFund03,
                               unitAllocFund04,
                               unitAllocFund05,
                               unitAllocFund06,
                               unitAllocFund07,
                               unitAllocFund08,
                               unitAllocFund09,
                               unitAllocFund10,
                               unitAllocPercAmt01,
                               unitAllocPercAmt02,
                               unitAllocPercAmt03,
                               unitAllocPercAmt04,
                               unitAllocPercAmt05,
                               unitAllocPercAmt06,
                               unitAllocPercAmt07,
                               unitAllocPercAmt08,
                               unitAllocPercAmt09,
                               unitAllocPercAmt10,
                               unitSpecPrice01,
                               unitSpecPrice02,
                               unitSpecPrice03,
                               unitSpecPrice04,
                               unitSpecPrice05,
                               unitSpecPrice06,
                               unitSpecPrice07,
                               unitSpecPrice08,
                               unitSpecPrice09,
                               unitSpecPrice10,
                               percOrAmntInd,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(47);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getSeqnbr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, seqnbr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(seqnbr.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(262);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getCurrto().toInternal()
					+ getCurrfrom().toInternal()
					+ getTranno().toInternal()
					+ getPremTopupInd().toInternal()
					+ getNumapp().toInternal()
					+ getValidflag().toInternal()
					+ getUnitAllocFund01().toInternal()
					+ getUnitAllocFund02().toInternal()
					+ getUnitAllocFund03().toInternal()
					+ getUnitAllocFund04().toInternal()
					+ getUnitAllocFund05().toInternal()
					+ getUnitAllocFund06().toInternal()
					+ getUnitAllocFund07().toInternal()
					+ getUnitAllocFund08().toInternal()
					+ getUnitAllocFund09().toInternal()
					+ getUnitAllocFund10().toInternal()
					+ getUnitAllocPercAmt01().toInternal()
					+ getUnitAllocPercAmt02().toInternal()
					+ getUnitAllocPercAmt03().toInternal()
					+ getUnitAllocPercAmt04().toInternal()
					+ getUnitAllocPercAmt05().toInternal()
					+ getUnitAllocPercAmt06().toInternal()
					+ getUnitAllocPercAmt07().toInternal()
					+ getUnitAllocPercAmt08().toInternal()
					+ getUnitAllocPercAmt09().toInternal()
					+ getUnitAllocPercAmt10().toInternal()
					+ getUnitSpecPrice01().toInternal()
					+ getUnitSpecPrice02().toInternal()
					+ getUnitSpecPrice03().toInternal()
					+ getUnitSpecPrice04().toInternal()
					+ getUnitSpecPrice05().toInternal()
					+ getUnitSpecPrice06().toInternal()
					+ getUnitSpecPrice07().toInternal()
					+ getUnitSpecPrice08().toInternal()
					+ getUnitSpecPrice09().toInternal()
					+ getUnitSpecPrice10().toInternal()
					+ getPercOrAmntInd().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, premTopupInd);
			what = ExternalData.chop(what, numapp);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, unitAllocFund01);
			what = ExternalData.chop(what, unitAllocFund02);
			what = ExternalData.chop(what, unitAllocFund03);
			what = ExternalData.chop(what, unitAllocFund04);
			what = ExternalData.chop(what, unitAllocFund05);
			what = ExternalData.chop(what, unitAllocFund06);
			what = ExternalData.chop(what, unitAllocFund07);
			what = ExternalData.chop(what, unitAllocFund08);
			what = ExternalData.chop(what, unitAllocFund09);
			what = ExternalData.chop(what, unitAllocFund10);
			what = ExternalData.chop(what, unitAllocPercAmt01);
			what = ExternalData.chop(what, unitAllocPercAmt02);
			what = ExternalData.chop(what, unitAllocPercAmt03);
			what = ExternalData.chop(what, unitAllocPercAmt04);
			what = ExternalData.chop(what, unitAllocPercAmt05);
			what = ExternalData.chop(what, unitAllocPercAmt06);
			what = ExternalData.chop(what, unitAllocPercAmt07);
			what = ExternalData.chop(what, unitAllocPercAmt08);
			what = ExternalData.chop(what, unitAllocPercAmt09);
			what = ExternalData.chop(what, unitAllocPercAmt10);
			what = ExternalData.chop(what, unitSpecPrice01);
			what = ExternalData.chop(what, unitSpecPrice02);
			what = ExternalData.chop(what, unitSpecPrice03);
			what = ExternalData.chop(what, unitSpecPrice04);
			what = ExternalData.chop(what, unitSpecPrice05);
			what = ExternalData.chop(what, unitSpecPrice06);
			what = ExternalData.chop(what, unitSpecPrice07);
			what = ExternalData.chop(what, unitSpecPrice08);
			what = ExternalData.chop(what, unitSpecPrice09);
			what = ExternalData.chop(what, unitSpecPrice10);
			what = ExternalData.chop(what, percOrAmntInd);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getSeqnbr() {
		return seqnbr;
	}
	public void setSeqnbr(Object what) {
		setSeqnbr(what, false);
	}
	public void setSeqnbr(Object what, boolean rounded) {
		if (rounded)
			seqnbr.setRounded(what);
		else
			seqnbr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getPremTopupInd() {
		return premTopupInd;
	}
	public void setPremTopupInd(Object what) {
		premTopupInd.set(what);
	}	
	public PackedDecimalData getNumapp() {
		return numapp;
	}
	public void setNumapp(Object what) {
		setNumapp(what, false);
	}
	public void setNumapp(Object what, boolean rounded) {
		if (rounded)
			numapp.setRounded(what);
		else
			numapp.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund01() {
		return unitAllocFund01;
	}
	public void setUnitAllocFund01(Object what) {
		unitAllocFund01.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund02() {
		return unitAllocFund02;
	}
	public void setUnitAllocFund02(Object what) {
		unitAllocFund02.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund03() {
		return unitAllocFund03;
	}
	public void setUnitAllocFund03(Object what) {
		unitAllocFund03.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund04() {
		return unitAllocFund04;
	}
	public void setUnitAllocFund04(Object what) {
		unitAllocFund04.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund05() {
		return unitAllocFund05;
	}
	public void setUnitAllocFund05(Object what) {
		unitAllocFund05.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund06() {
		return unitAllocFund06;
	}
	public void setUnitAllocFund06(Object what) {
		unitAllocFund06.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund07() {
		return unitAllocFund07;
	}
	public void setUnitAllocFund07(Object what) {
		unitAllocFund07.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund08() {
		return unitAllocFund08;
	}
	public void setUnitAllocFund08(Object what) {
		unitAllocFund08.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund09() {
		return unitAllocFund09;
	}
	public void setUnitAllocFund09(Object what) {
		unitAllocFund09.set(what);
	}	
	public FixedLengthStringData getUnitAllocFund10() {
		return unitAllocFund10;
	}
	public void setUnitAllocFund10(Object what) {
		unitAllocFund10.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt01() {
		return unitAllocPercAmt01;
	}
	public void setUnitAllocPercAmt01(Object what) {
		setUnitAllocPercAmt01(what, false);
	}
	public void setUnitAllocPercAmt01(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt01.setRounded(what);
		else
			unitAllocPercAmt01.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt02() {
		return unitAllocPercAmt02;
	}
	public void setUnitAllocPercAmt02(Object what) {
		setUnitAllocPercAmt02(what, false);
	}
	public void setUnitAllocPercAmt02(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt02.setRounded(what);
		else
			unitAllocPercAmt02.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt03() {
		return unitAllocPercAmt03;
	}
	public void setUnitAllocPercAmt03(Object what) {
		setUnitAllocPercAmt03(what, false);
	}
	public void setUnitAllocPercAmt03(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt03.setRounded(what);
		else
			unitAllocPercAmt03.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt04() {
		return unitAllocPercAmt04;
	}
	public void setUnitAllocPercAmt04(Object what) {
		setUnitAllocPercAmt04(what, false);
	}
	public void setUnitAllocPercAmt04(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt04.setRounded(what);
		else
			unitAllocPercAmt04.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt05() {
		return unitAllocPercAmt05;
	}
	public void setUnitAllocPercAmt05(Object what) {
		setUnitAllocPercAmt05(what, false);
	}
	public void setUnitAllocPercAmt05(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt05.setRounded(what);
		else
			unitAllocPercAmt05.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt06() {
		return unitAllocPercAmt06;
	}
	public void setUnitAllocPercAmt06(Object what) {
		setUnitAllocPercAmt06(what, false);
	}
	public void setUnitAllocPercAmt06(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt06.setRounded(what);
		else
			unitAllocPercAmt06.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt07() {
		return unitAllocPercAmt07;
	}
	public void setUnitAllocPercAmt07(Object what) {
		setUnitAllocPercAmt07(what, false);
	}
	public void setUnitAllocPercAmt07(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt07.setRounded(what);
		else
			unitAllocPercAmt07.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt08() {
		return unitAllocPercAmt08;
	}
	public void setUnitAllocPercAmt08(Object what) {
		setUnitAllocPercAmt08(what, false);
	}
	public void setUnitAllocPercAmt08(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt08.setRounded(what);
		else
			unitAllocPercAmt08.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt09() {
		return unitAllocPercAmt09;
	}
	public void setUnitAllocPercAmt09(Object what) {
		setUnitAllocPercAmt09(what, false);
	}
	public void setUnitAllocPercAmt09(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt09.setRounded(what);
		else
			unitAllocPercAmt09.set(what);
	}	
	public PackedDecimalData getUnitAllocPercAmt10() {
		return unitAllocPercAmt10;
	}
	public void setUnitAllocPercAmt10(Object what) {
		setUnitAllocPercAmt10(what, false);
	}
	public void setUnitAllocPercAmt10(Object what, boolean rounded) {
		if (rounded)
			unitAllocPercAmt10.setRounded(what);
		else
			unitAllocPercAmt10.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice01() {
		return unitSpecPrice01;
	}
	public void setUnitSpecPrice01(Object what) {
		setUnitSpecPrice01(what, false);
	}
	public void setUnitSpecPrice01(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice01.setRounded(what);
		else
			unitSpecPrice01.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice02() {
		return unitSpecPrice02;
	}
	public void setUnitSpecPrice02(Object what) {
		setUnitSpecPrice02(what, false);
	}
	public void setUnitSpecPrice02(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice02.setRounded(what);
		else
			unitSpecPrice02.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice03() {
		return unitSpecPrice03;
	}
	public void setUnitSpecPrice03(Object what) {
		setUnitSpecPrice03(what, false);
	}
	public void setUnitSpecPrice03(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice03.setRounded(what);
		else
			unitSpecPrice03.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice04() {
		return unitSpecPrice04;
	}
	public void setUnitSpecPrice04(Object what) {
		setUnitSpecPrice04(what, false);
	}
	public void setUnitSpecPrice04(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice04.setRounded(what);
		else
			unitSpecPrice04.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice05() {
		return unitSpecPrice05;
	}
	public void setUnitSpecPrice05(Object what) {
		setUnitSpecPrice05(what, false);
	}
	public void setUnitSpecPrice05(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice05.setRounded(what);
		else
			unitSpecPrice05.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice06() {
		return unitSpecPrice06;
	}
	public void setUnitSpecPrice06(Object what) {
		setUnitSpecPrice06(what, false);
	}
	public void setUnitSpecPrice06(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice06.setRounded(what);
		else
			unitSpecPrice06.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice07() {
		return unitSpecPrice07;
	}
	public void setUnitSpecPrice07(Object what) {
		setUnitSpecPrice07(what, false);
	}
	public void setUnitSpecPrice07(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice07.setRounded(what);
		else
			unitSpecPrice07.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice08() {
		return unitSpecPrice08;
	}
	public void setUnitSpecPrice08(Object what) {
		setUnitSpecPrice08(what, false);
	}
	public void setUnitSpecPrice08(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice08.setRounded(what);
		else
			unitSpecPrice08.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice09() {
		return unitSpecPrice09;
	}
	public void setUnitSpecPrice09(Object what) {
		setUnitSpecPrice09(what, false);
	}
	public void setUnitSpecPrice09(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice09.setRounded(what);
		else
			unitSpecPrice09.set(what);
	}	
	public PackedDecimalData getUnitSpecPrice10() {
		return unitSpecPrice10;
	}
	public void setUnitSpecPrice10(Object what) {
		setUnitSpecPrice10(what, false);
	}
	public void setUnitSpecPrice10(Object what, boolean rounded) {
		if (rounded)
			unitSpecPrice10.setRounded(what);
		else
			unitSpecPrice10.set(what);
	}	
	public FixedLengthStringData getPercOrAmntInd() {
		return percOrAmntInd;
	}
	public void setPercOrAmntInd(Object what) {
		percOrAmntInd.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getUspcprs() {
		return new FixedLengthStringData(unitSpecPrice01.toInternal()
										+ unitSpecPrice02.toInternal()
										+ unitSpecPrice03.toInternal()
										+ unitSpecPrice04.toInternal()
										+ unitSpecPrice05.toInternal()
										+ unitSpecPrice06.toInternal()
										+ unitSpecPrice07.toInternal()
										+ unitSpecPrice08.toInternal()
										+ unitSpecPrice09.toInternal()
										+ unitSpecPrice10.toInternal());
	}
	public void setUspcprs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getUspcprs().getLength()).init(obj);
	
		what = ExternalData.chop(what, unitSpecPrice01);
		what = ExternalData.chop(what, unitSpecPrice02);
		what = ExternalData.chop(what, unitSpecPrice03);
		what = ExternalData.chop(what, unitSpecPrice04);
		what = ExternalData.chop(what, unitSpecPrice05);
		what = ExternalData.chop(what, unitSpecPrice06);
		what = ExternalData.chop(what, unitSpecPrice07);
		what = ExternalData.chop(what, unitSpecPrice08);
		what = ExternalData.chop(what, unitSpecPrice09);
		what = ExternalData.chop(what, unitSpecPrice10);
	}
	public PackedDecimalData getUspcpr(BaseData indx) {
		return getUspcpr(indx.toInt());
	}
	public PackedDecimalData getUspcpr(int indx) {

		switch (indx) {
			case 1 : return unitSpecPrice01;
			case 2 : return unitSpecPrice02;
			case 3 : return unitSpecPrice03;
			case 4 : return unitSpecPrice04;
			case 5 : return unitSpecPrice05;
			case 6 : return unitSpecPrice06;
			case 7 : return unitSpecPrice07;
			case 8 : return unitSpecPrice08;
			case 9 : return unitSpecPrice09;
			case 10 : return unitSpecPrice10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setUspcpr(BaseData indx, Object what) {
		setUspcpr(indx, what, false);
	}
	public void setUspcpr(BaseData indx, Object what, boolean rounded) {
		setUspcpr(indx.toInt(), what, rounded);
	}
	public void setUspcpr(int indx, Object what) {
		setUspcpr(indx, what, false);
	}
	public void setUspcpr(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setUnitSpecPrice01(what, rounded);
					 break;
			case 2 : setUnitSpecPrice02(what, rounded);
					 break;
			case 3 : setUnitSpecPrice03(what, rounded);
					 break;
			case 4 : setUnitSpecPrice04(what, rounded);
					 break;
			case 5 : setUnitSpecPrice05(what, rounded);
					 break;
			case 6 : setUnitSpecPrice06(what, rounded);
					 break;
			case 7 : setUnitSpecPrice07(what, rounded);
					 break;
			case 8 : setUnitSpecPrice08(what, rounded);
					 break;
			case 9 : setUnitSpecPrice09(what, rounded);
					 break;
			case 10 : setUnitSpecPrice10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getUalprcs() {
		return new FixedLengthStringData(unitAllocPercAmt01.toInternal()
										+ unitAllocPercAmt02.toInternal()
										+ unitAllocPercAmt03.toInternal()
										+ unitAllocPercAmt04.toInternal()
										+ unitAllocPercAmt05.toInternal()
										+ unitAllocPercAmt06.toInternal()
										+ unitAllocPercAmt07.toInternal()
										+ unitAllocPercAmt08.toInternal()
										+ unitAllocPercAmt09.toInternal()
										+ unitAllocPercAmt10.toInternal());
	}
	public void setUalprcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getUalprcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, unitAllocPercAmt01);
		what = ExternalData.chop(what, unitAllocPercAmt02);
		what = ExternalData.chop(what, unitAllocPercAmt03);
		what = ExternalData.chop(what, unitAllocPercAmt04);
		what = ExternalData.chop(what, unitAllocPercAmt05);
		what = ExternalData.chop(what, unitAllocPercAmt06);
		what = ExternalData.chop(what, unitAllocPercAmt07);
		what = ExternalData.chop(what, unitAllocPercAmt08);
		what = ExternalData.chop(what, unitAllocPercAmt09);
		what = ExternalData.chop(what, unitAllocPercAmt10);
	}
	public PackedDecimalData getUalprc(BaseData indx) {
		return getUalprc(indx.toInt());
	}
	public PackedDecimalData getUalprc(int indx) {

		switch (indx) {
			case 1 : return unitAllocPercAmt01;
			case 2 : return unitAllocPercAmt02;
			case 3 : return unitAllocPercAmt03;
			case 4 : return unitAllocPercAmt04;
			case 5 : return unitAllocPercAmt05;
			case 6 : return unitAllocPercAmt06;
			case 7 : return unitAllocPercAmt07;
			case 8 : return unitAllocPercAmt08;
			case 9 : return unitAllocPercAmt09;
			case 10 : return unitAllocPercAmt10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setUalprc(BaseData indx, Object what) {
		setUalprc(indx, what, false);
	}
	public void setUalprc(BaseData indx, Object what, boolean rounded) {
		setUalprc(indx.toInt(), what, rounded);
	}
	public void setUalprc(int indx, Object what) {
		setUalprc(indx, what, false);
	}
	public void setUalprc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setUnitAllocPercAmt01(what, rounded);
					 break;
			case 2 : setUnitAllocPercAmt02(what, rounded);
					 break;
			case 3 : setUnitAllocPercAmt03(what, rounded);
					 break;
			case 4 : setUnitAllocPercAmt04(what, rounded);
					 break;
			case 5 : setUnitAllocPercAmt05(what, rounded);
					 break;
			case 6 : setUnitAllocPercAmt06(what, rounded);
					 break;
			case 7 : setUnitAllocPercAmt07(what, rounded);
					 break;
			case 8 : setUnitAllocPercAmt08(what, rounded);
					 break;
			case 9 : setUnitAllocPercAmt09(what, rounded);
					 break;
			case 10 : setUnitAllocPercAmt10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getUalfnds() {
		return new FixedLengthStringData(unitAllocFund01.toInternal()
										+ unitAllocFund02.toInternal()
										+ unitAllocFund03.toInternal()
										+ unitAllocFund04.toInternal()
										+ unitAllocFund05.toInternal()
										+ unitAllocFund06.toInternal()
										+ unitAllocFund07.toInternal()
										+ unitAllocFund08.toInternal()
										+ unitAllocFund09.toInternal()
										+ unitAllocFund10.toInternal());
	}
	public void setUalfnds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getUalfnds().getLength()).init(obj);
	
		what = ExternalData.chop(what, unitAllocFund01);
		what = ExternalData.chop(what, unitAllocFund02);
		what = ExternalData.chop(what, unitAllocFund03);
		what = ExternalData.chop(what, unitAllocFund04);
		what = ExternalData.chop(what, unitAllocFund05);
		what = ExternalData.chop(what, unitAllocFund06);
		what = ExternalData.chop(what, unitAllocFund07);
		what = ExternalData.chop(what, unitAllocFund08);
		what = ExternalData.chop(what, unitAllocFund09);
		what = ExternalData.chop(what, unitAllocFund10);
	}
	public FixedLengthStringData getUalfnd(BaseData indx) {
		return getUalfnd(indx.toInt());
	}
	public FixedLengthStringData getUalfnd(int indx) {

		switch (indx) {
			case 1 : return unitAllocFund01;
			case 2 : return unitAllocFund02;
			case 3 : return unitAllocFund03;
			case 4 : return unitAllocFund04;
			case 5 : return unitAllocFund05;
			case 6 : return unitAllocFund06;
			case 7 : return unitAllocFund07;
			case 8 : return unitAllocFund08;
			case 9 : return unitAllocFund09;
			case 10 : return unitAllocFund10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setUalfnd(BaseData indx, Object what) {
		setUalfnd(indx.toInt(), what);
	}
	public void setUalfnd(int indx, Object what) {

		switch (indx) {
			case 1 : setUnitAllocFund01(what);
					 break;
			case 2 : setUnitAllocFund02(what);
					 break;
			case 3 : setUnitAllocFund03(what);
					 break;
			case 4 : setUnitAllocFund04(what);
					 break;
			case 5 : setUnitAllocFund05(what);
					 break;
			case 6 : setUnitAllocFund06(what);
					 break;
			case 7 : setUnitAllocFund07(what);
					 break;
			case 8 : setUnitAllocFund08(what);
					 break;
			case 9 : setUnitAllocFund09(what);
					 break;
			case 10 : setUnitAllocFund10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		seqnbr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		currto.clear();
		currfrom.clear();
		tranno.clear();
		premTopupInd.clear();
		numapp.clear();
		validflag.clear();
		unitAllocFund01.clear();
		unitAllocFund02.clear();
		unitAllocFund03.clear();
		unitAllocFund04.clear();
		unitAllocFund05.clear();
		unitAllocFund06.clear();
		unitAllocFund07.clear();
		unitAllocFund08.clear();
		unitAllocFund09.clear();
		unitAllocFund10.clear();
		unitAllocPercAmt01.clear();
		unitAllocPercAmt02.clear();
		unitAllocPercAmt03.clear();
		unitAllocPercAmt04.clear();
		unitAllocPercAmt05.clear();
		unitAllocPercAmt06.clear();
		unitAllocPercAmt07.clear();
		unitAllocPercAmt08.clear();
		unitAllocPercAmt09.clear();
		unitAllocPercAmt10.clear();
		unitSpecPrice01.clear();
		unitSpecPrice02.clear();
		unitSpecPrice03.clear();
		unitSpecPrice04.clear();
		unitSpecPrice05.clear();
		unitSpecPrice06.clear();
		unitSpecPrice07.clear();
		unitSpecPrice08.clear();
		unitSpecPrice09.clear();
		unitSpecPrice10.clear();
		percOrAmntInd.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}