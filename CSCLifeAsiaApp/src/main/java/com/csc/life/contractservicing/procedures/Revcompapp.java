/*
 * File: Revlaps.java
 * Date: 30 August 2009 2:09:37
 * Author: Quipoz Limited
 * 
 * Class transformed from REVLAPS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List;

import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.dao.CpsupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cpsupf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*  REVCOMPAPP - Component Surrender Approval Reversal
* ---------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  The following transactions are reversed :
*
*  - CHDR
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract.
*       DELET this record.
*       NEXTR on  the  Contract  Header record (CHDRLIF) for the
*            same contract. This should read a record with
*            valid flag of '2'. If not, then this is an error.
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1'.
*
*  - COVRs
*
*       Only reverse COVR records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  COVR  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  COVR  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       REWRT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' . This will reinstate the
*            coverage prior to the LAPSE/PUP.
*
*
* All of these records are reversed when REVGENAT, the AT module,
*  calls the statistical subroutine LIFSTTR.
*
* NO processing is therefore required in this subroutine.
*
*
****************************************************************
* </pre>
*/
public class Revcompapp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVCOMPAPP";


	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO", PyoupfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);	
	private CpsupfDAO cpsupfDAO = getApplicationContext().getBean("cpsupfDAO", CpsupfDAO.class);
	private CovrpfDAO covrpupDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Payrpf payrpf = new Payrpf();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Covrpf covrpup = new Covrpf();
	protected Chdrpf chdrpf = new Chdrpf();
	private Descpf descpf = null;
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Reverserec reverserec = new Reverserec();
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);
	private static final String t1688 = "T1688";
	private List<Acmvpf> acmvpfList = null;
	private List<Cpsupf> cpsupfList = null;
	


	public Revcompapp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		descpf = descDAO.getdescData("IT", t1688, reverserec.oldBatctrcde.toString(), reverserec.company.toString(), reverserec.language.toString());
		if(descpf == null) {
			syserrrec.statuz.set(varcom.bomb);
			xxxxFatalError();
		}else {
			wsaaRevTrandesc.set(descpf.getLongdesc());
		}

	}

protected void process2000()
	{
		/*PROCESS*/
		processChdrs2100();
		processCpsupf3000();
		processPyoupf6000();
		processAcmv8000();
		processPayrpf9000();
	}

protected void processChdrs2100()
{
	
	chdrpfDAO.deleteRcdByFlag("1",reverserec.chdrnum.toString());
	
	chdrpf.setChdrnum(reverserec.chdrnum.toString());
	chdrpf.setValidflag('1');
	chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	chdrpfDAO.updateChdrbyUniqueno(chdrpf);
}


protected void processCpsupf3000()
{
	
	cpsupfList = cpsupfDAO.getCpsubyTranno(reverserec.chdrnum.toString(),reverserec.tranno.toString());
	
	 if (cpsupfList.size() == 0 )
		{
		syserrrec.statuz.set(varcom.bomb);
		xxxxFatalError();
 	}
	 
	for (Cpsupf cpsupf : cpsupfList) {
		{
			covrpup = covrpupDAO.getCovrpfData(reverserec.company.toString(), reverserec.chdrnum.toString(),cpsupf.getCrtable().trim());/* IJTI-1523 */
			if(covrpup == null){
				syserrrec.statuz.set(varcom.bomb);
				xxxxFatalError();
				}
			
			covrpupDAO.deleteByFlag("1", reverserec.chdrnum.trim(), cpsupf.getCrtable());/* IJTI-1523 */
			
			covrpup.setValidflag("1");
			covrpup.setCurrto(varcom.vrcmMaxDate.toInt());
			covrpupDAO.updatebyUniqueNo(covrpup);	  //update
			
			cpsupf.setChdrnum(reverserec.chdrnum.trim());
			cpsupf.setCrtable(cpsupf.getCrtable().trim());
			cpsupf.setStatus("A");
			cpsupfDAO.deleteCpsupfRecord(cpsupf);  //Reversal
			

			cpsupf.setChdrnum(reverserec.chdrnum.toString());
			cpsupf.setCrtable(cpsupf.getCrtable());
			cpsupf.setValidflag("1");
			cpsupf.setStatus("R"); 
			cpsupfDAO.updateCpsupf(cpsupf);    //Update record
			
		}
	}
	
}



protected void processPyoupf6000()
{
	pyoupfDAO.deleteCashDividendRecord(reverserec.chdrnum.toString(),reverserec.tranno.toString(),reverserec.oldBatctrcde.toString(),reverserec.company.toString());
}


protected void processAcmv8000(){
	
	acmvpfList = acmvpfDAO.getAcmvpfData(reverserec.company.toString(),reverserec.chdrnum.toString(),reverserec.tranno.toInt(),reverserec.oldBatctrcde.toString());
	for (Acmvpf acmvpf : acmvpfList) {
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvpf.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvpf.getSacscode());
		lifacmvrec.sacstyp.set(acmvpf.getSacstyp());
		lifacmvrec.glcode.set(acmvpf.getGlcode());
		lifacmvrec.glsign.set(acmvpf.getGlsign());
		lifacmvrec.jrnseq.set(acmvpf.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvpf.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvpf.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvpf.getRldgacct());
		lifacmvrec.origcurr.set(acmvpf.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvpf.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvpf.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvpf.getGenlcur());
		lifacmvrec.crate.set(acmvpf.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvpf.getTranref());
		lifacmvrec.trandesc.set(wsaaRevTrandesc);
		/* MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvpf.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
	  }
	
	}
		
}

protected void processPayrpf9000(){
	
	 /* read record with latest Valid flag=2 to get Sinstamt01 and Sinstamt06  */
	payrpf = payrpfDAO.getpayrpfRecord(reverserec.company.toString(), reverserec.chdrnum.toString()); 
	
	/*update Sinstamt01 and Sinstamt06 where valid flag=1, so while reversal,  */
	/* old values of Sinstamt01 and Sinstamt06 (where valid flag=2) will be used by revgenat. */
	/* Revgenat will then delete this record and insert a new record with old values of Sinstamt01 and Sinstamt06  */
	payrpfDAO.updatePayrbyUniqueno(payrpf.getSinstamt01(), payrpf.getSinstamt06() ,reverserec.chdrnum.toString(),reverserec.tranno.toInt()); 

}

protected void xxxxFatalError()
{
				xxxxFatalErrors();
				xxxxErrorProg();
			}

protected void xxxxFatalErrors()
{
	if (isEQ(syserrrec.statuz,varcom.bomb)) {
		return ;
	}
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	if (isNE(syserrrec.syserrType,"2")) {
		syserrrec.syserrType.set("1");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
}

protected void xxxxErrorProg()
{
	reverserec.statuz.set(varcom.bomb);
	/*XXXX-EXIT*/
	exitProgram();
}

}
