package com.csc.life.contractservicing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.dao.YctxpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.contractservicing.dataaccess.model.Yctxpf;
import com.csc.life.contractservicing.procedures.BonusDtn;
import com.csc.life.contractservicing.procedures.TaxComputeDtn;
import com.csc.life.contractservicing.procedures.UnitDtn;
import com.csc.life.contractservicing.recordstructures.Pd5klrec;
import com.csc.life.contractservicing.recordstructures.TaxDeductionRec;
import com.csc.life.contractservicing.tablestructures.Td5jsrec;
import com.csc.life.contractservicing.tablestructures.Tr5lxrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr59xrec;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *REMARKS
 *			CONTRIBUTIONS TAX PROCESSING
 *-----------------------------------------------------------------------------------------
 *
 * This program is used to calculate tax applicable on superannuation 
 * products being read from TR59X and processed upon policies in IF status. 
 * The contribution details are being read from ZCTXPF.
 * 
 * 1. Read all the superannuation products available in TR59X.
 * 2. Read the status codes from T5679 corresponding to transaction code.
 * 3. Read all recovery types present in TD5JS.
 * 4. Find the products from TR59X for which recovery type is set.
 * 5. All the policies that satisfy 2 & 4, read contract header details for those policies.
 * 6. Read all data from COVRPF for all policies fetched from CHDRPF.
 * 7. Read TR5LX to get tax period.
 * 8. Read ZCTXPF for all valid policies fetched from CHDRPF valid for tax period fetched  
 * 	  from TR5LX and where procflag in ZCTXPF is not equal to Y.
 * 9. Iterate over data fetched form ZCTXPF.
 * 10. Setup Tax Deduction Rec.
 * 11. Following products shall be processed by this batch program.
 * 		Products	Recovery Types
 * 		----------------------------
 * 		SLS:		No Recovery
 * 		SIS:		Unit Deduction
 * 		SIR:		Unit Deduction
 * 		SEN:		Bonus Deduction
 * 		SWL:		Bonus Deduction
 * 12. Call VPMS subroutine CONTTAX to get total applicable tax on a policy.
 * 13. If tax amount is grater than zero check:
 * 			If recovery type is unit deduction redirect to UnitDtn subroutine.
 * 			If recovery type is bonus deduction redirect to BonusDtn subroutine.
 * 14. If tax amount is lesser than zero set update flag as false but rewrite ZCTX as 
 * 	   policy has been processed for current tax year.
 * 15. Write record to Tax Contributions File (YCTXPF).
 * 16. If the tax is processed first time insert a record.
 * 17. If the tax processing recurs, update previous record for valid flag 2
 * 	   and write a new record with valid flag 1
 * 18. Update contract header tranno and increment it by 1.
 * 19. Write PTRN.
 * 20. Write ZCTXPF, set dateFrm and dateTo to next financial year dates and set
 *     all contribution amounts to zero.
 * 21. Unlock policies.
 * 
 * Smart tables used:
 * 		Table		Item
 * 		----------------------
 * 		T5679		BAOJ
 * 		TR59X		SIS, SLS, SIR, SEN, SWL
 * 		TD5JS		B, N, U
 * 		T5645		BD5JR
 * 		TR5LX		CTAX
 * 		TR60O		All contribution types
 * </pre>
 * 
 * @author gsaluja2
 * @version 1.0
 * @since 05 February 2019
 *
 */

public class Bd5jr extends Mainb{
	/*	Batch params	*/
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BD5JR");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5jr.class);
	
	/*	DAO UTIL used	*/
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private YctxpfDAO yctxpfDAO = getApplicationContext().getBean("yctxpfDAO", YctxpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private DescpfDAO descpfDAO = getApplicationContext().getBean("descpfDAO", DescpfDAO.class);
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private SftlockUtil sftlockUtil = new SftlockUtil();
	
	/*	Rec's used	*/
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private Pd5klrec pd5klrec = new Pd5klrec();
	private T5679rec t5679rec = new T5679rec();
	private Tr59xrec tr59xrec = new Tr59xrec();
	private Td5jsrec td5jsrec = new Td5jsrec();
	private Tr5lxrec tr5lxrec = new Tr5lxrec();
	private T5645rec t5645rec = new T5645rec();
	private TaxDeductionRec taxdrec = new TaxDeductionRec();
	
	/*	Objects used	*/
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private ExternalisedRules er = new ExternalisedRules();
	private Yctxpf yctxpf;
	private Chdrpf chdrpf;
	private Covrpf covrpf;
	private Rskppf rskppf;

	private List<Itempf> t5679List;
	private List<String> tr59xItemList;
	private List<String> t5679StatcodeList;
	private Map<String, Itempf> tr59xMap;
	private Map<String, Itempf> td5jsMap;
	private Map<String, Itempf> t5645Map;
	private List<Itempf> tr5lxList;
	private List <Itempf> tr60oList;
	
	private Map<String, Chdrpf> chdrpfMap;
	private Map<String, List<Chdrpf>> chdrpfListMap;
	private Map<String, List<Covrpf>> covrpfMap;
	private Map<String, List<Rskppf>> rskppfMap;
	private List<String> chdrnumList;
	private Map<String, Zctxpf> zctxpfMap;

	/*	update insert Lists used	*/
	private List<Yctxpf> yctxpfUpdateList = new ArrayList<>();
	private List<Yctxpf> yctxpfInsertList = new ArrayList<>();
	private List<Chdrpf> chdrpfUpdateList = new ArrayList<>();
	private List<Ptrnpf> ptrnpfInsertList = new ArrayList<>();
	private List<Zctxpf> zctxpfUpdateList = new ArrayList<>();
	private List<Zctxpf> zctxpfInsertList = new ArrayList<>();
	
	/*	variables used	*/
	private String coyPfx;
	private String coy;
	private FixedLengthStringData wsaaChdrnumFrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8);
	private ZonedDecimalData idx = new ZonedDecimalData(2, 0).setUnsigned();
	private Iterator<Entry<String, Zctxpf>> itr;
	private Map.Entry<String, Zctxpf> entry;
	private String procflag = SPACE;
	private boolean updateFlag = false;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private Calendar taxToDate = Calendar.getInstance();
	private String trancodeDesc;
	
	/*	Smart Tables Used	*/
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData tr59x = new FixedLengthStringData(5).init("TR59X");
	private FixedLengthStringData td5js = new FixedLengthStringData(5).init("TD5JS");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData tr5lx = new FixedLengthStringData(5).init("TR5LX");
	private FixedLengthStringData tr60o = new FixedLengthStringData(5).init("TR60O");
	
	/*	Control Totals Used	*/
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private int ct06Value = 0;
	private int ct07Value = 0;
	private int ct08Value = 0;
	private int ct09Value = 0;

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	@Override
	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	@Override
	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}
	
	@Override
	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	@Override
	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	@Override
	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	@Override
	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	@Override
	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	@Override
	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	@Override
	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	@Override
	protected void restart0900() {
		/*	RESTART	*/
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	@Override
	protected void initialise1000() {
		wsspEdterror.set(varcom.oK);
		coyPfx = smtpfxcpy.item.toString();
		coy = bsprIO.getCompany().toString();
		readSmartTables();
		pd5klrec.parmRecord.set(bupaIO.getParmarea());
		validateDate();
		if(isEQ(wsspEdterror, varcom.oK)) {
			if(isNE(pd5klrec.chdrnum, SPACES)) {
				wsaaChdrnumFrm.set(pd5klrec.chdrnum);
			}
			if(isNE(pd5klrec.chdrnum1, SPACES)) {
				wsaaChdrnumTo.set(pd5klrec.chdrnum1);
			}
			loadData();
		}
	}
	
	protected void readSmartTables() {
		t5679List = itemDAO.getAllItemitem(coyPfx, coy, t5679.toString(), bprdIO.getAuthCode().toString());
		if (null == t5679List || t5679List.isEmpty()) {
			syserrrec.params.set("T5679");
			fatalError600();
		} else {
			t5679rec.t5679Rec.set(StringUtil.rawToString(t5679List.get(0).getGenarea()));
		}
		tr59xMap = itemDAO.getItemMap(coyPfx, coy, tr59x.toString());
		td5jsMap = itemDAO.getItemMap(coyPfx, coy, td5js.toString());
		t5645Map = itemDAO.getItemMap(coyPfx, coy, t5645.toString());
		tr60oList = itemDAO.getAllitems(coyPfx, coy, tr60o.toString());
		tr5lxList = itemDAO.loadItdmTable(coyPfx,  coy, tr5lx.toString());
		tr59xItemList = new ArrayList<>();
		for(Map.Entry<String, Itempf> tr59xKV : tr59xMap.entrySet()) {
			if(tr59xKV.getValue() != null){
				tr59xrec.tr59xrec.set(StringUtil.rawToString(tr59xKV.getValue().getGenarea()));
				if(td5jsMap.keySet().contains(tr59xrec.recoveryType.toString())) {
					tr59xItemList.add(tr59xKV.getKey());
				}
			}
		}
		t5679StatcodeList = new ArrayList<>();
		idx.set(1);
		while(isLT(idx, 12) && isNE(t5679rec.cnRiskStat[idx.toInt()], SPACES)) {
			t5679StatcodeList.add(t5679rec.cnRiskStat[idx.toInt()].toString());
			idx.add(1);
		}
		if(t5645Map.get(wsaaProg.toString()) == null)
		{
			syserrrec.params.set(coyPfx+ coy+ t5645.toString()+wsaaProg);
			syserrrec.function.set(Varcom.mrnf);
			syserrrec.statuz.set("H134");
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(t5645Map.get(wsaaProg.toString()).getGenarea()));
	
		Descpf descpf = new Descpf();
		descpfDAO.setSession(appVars.getHibernateSession());
		descpf = descpfDAO.getItemTblItemByLang("IT","T1688", batcdorrec.trcde.toString(), "E", coy);
		trancodeDesc = descpf.getLongdesc();
	}
	
	protected void validateDate() {
		String keyItem = "CTAX";
		BigDecimal itmfrmDate = new BigDecimal("0");
		for(Itempf item: tr5lxList) {
			if(keyItem.equals(item.getItemitem())){
				tr5lxrec.tr5lxRec.set(StringUtil.rawToString(item.getGenarea()));
				if(pd5klrec.taxFrmYear.toString().trim().equals(tr5lxrec.taxPeriodFrm.toString().trim())) {
					itmfrmDate = item.getItmfrm();
					break;
				}
			}
		}
		Calendar toDate = Calendar.getInstance();		
		try {
			toDate.setTime(dateFormat.parse(itmfrmDate.toString()));/*tr5lx from date*/
			taxToDate.set(toDate.get(Calendar.YEAR)+1, 5, 30);
		}catch(ParseException e) {
			LOGGER.error("BD5JR error", e);
		}
		if(bsscIO.getEffectiveDate().toInt() < Integer.parseInt(dateFormat.format(taxToDate.getTime()))) {
			wsspEdterror.set(varcom.endp);
	}
	}
	
	protected void loadData() {
		chdrpfMap = chdrpfDAO.validateChdrList("CH", coy, wsaaChdrnumFrm.toString(), wsaaChdrnumTo.toString(), tr59xItemList, t5679StatcodeList, null); //ILIFE-8179
		if(chdrpfMap != null && !chdrpfMap.isEmpty()) {
			chdrnumList = new ArrayList<>();
			for(Map.Entry<String, ?> chdrEntry : chdrpfMap.entrySet()) {
				chdrnumList.add(((Chdrpf) chdrEntry.getValue()).getChdrnum());
			}
		}
		covrpfMap = covrpfDAO.searchCovrMap(coy, chdrnumList);
		rskppfMap = rskppfDAO.searchRskpMap(coy, chdrnumList);
		fetchContributionDetails();
	}
	
	private void fetchContributionDetails() {
		zctxpfMap = zctxpfDAO.readContributionDetails(coy, chdrnumList, Integer.parseInt(dateFormat.format(taxToDate.getTime())), procflag);
		if(zctxpfMap.isEmpty()) {
			wsspEdterror.set(varcom.endp);
		}
		else{
			itr = zctxpfMap.entrySet().iterator();
		}
	}
	
	@Override
	protected void readFile2000() {
		if(!itr.hasNext()){
			wsspEdterror.set(varcom.endp);
			return;
		}
		ct01Value++;
	}

	@Override
	protected void edit2500() {
		entry = itr.next();
		initTaxDeductionRec();
		if(tr59xMap.get(entry.getValue().getCnttype().trim()) != null) {
			tr59xrec.tr59xrec.set(StringUtil.rawToString(tr59xMap.get(entry.getValue().getCnttype().trim()).getGenarea()));
			if(td5jsMap.get(tr59xrec.recoveryType.toString()) != null)
				td5jsrec.td5jsRec.set(StringUtil.rawToString(td5jsMap.get(tr59xrec.recoveryType.toString()).getGenarea()));
		}
		chdrpfDAO.setCacheObject(chdrpfMap.get(taxdrec.getChdrnum()));
		sftlockRecBean.setFunction("LOCK");
		performLockOperation();
		ct02Value++;
		taxdrec.setRiskPremium(getRiskPremium());
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("CONTTAX")){
			callProgram("CONTTAX", taxdrec);
			if(isNE(taxdrec.statuz, Varcom.oK)) {
				fatalError600();
			}
			if(taxdrec.getTotalTaxAmt().compareTo(BigDecimal.ZERO) > 0) {
				callSubroutines();
			}
			else {
				updateFlag = false;
			}
			procflag = "Y";
		}
	}
	
	protected void callSubroutines() {	
		if(("B").equals(tr59xrec.recoveryType.toString()) && ("Y").equals(td5jsrec.taxCalcReqd.toString())) {
			callProgram(BonusDtn.class, batcdorrec.batcdorRec, taxdrec, t5645rec, covrpf,trancodeDesc);
			if(isNE(taxdrec.statuz, Varcom.oK)) {
				fatalError600();
			}
			updateFlag = true;
		}
		else if (("U").equals(tr59xrec.recoveryType.toString()) && ("Y").equals(td5jsrec.taxCalcReqd.toString())) {
			callProgram(UnitDtn.class, batcdorrec.batcdorRec, taxdrec, t5645rec, covrpf,trancodeDesc);
			if(isNE(taxdrec.statuz, Varcom.oK)) {
				fatalError600();
			}
			updateFlag = true;
		}
		else{
			if(("Y").equals(td5jsrec.taxCalcReqd.toString())) {
				updateFlag = true;
				TaxComputeDtn taxcompute = new TaxComputeDtn();
				taxcompute.setBaseModel(this.getBaseModel());
				taxcompute.setGlobalVars(appVars);
				taxcompute.setJTAWrapper(this.getJTAWrapper());
				taxcompute.postAcmv(batcdorrec,taxdrec,varcom,t5645rec,trancodeDesc,chdrpfMap.get(taxdrec.getChdrnum()),covrpf);
			}
		}
	}
	
	protected BigDecimal getRiskPremium() {
		BigDecimal wsaaTotRiskPrem = BigDecimal.ZERO;
		covrpf = new Covrpf();
		for(Covrpf covr: covrpfMap.get(taxdrec.getChdrnum())) {
			//wsaaTotRiskPrem = wsaaTotRiskPrem.add(covr.getRiskprem() == null ? BigDecimal.ZERO : covr.getRiskprem());
			if(("01").equals(covr.getLife()) && ("01").equals(covr.getCoverage()) && ("00").equals(covr.getRider()))
				covrpf = covr;
		}
		rskppf = new Rskppf();
		for(Rskppf rskp: rskppfMap.get(taxdrec.getChdrnum())) {
			int i = 0;
			wsaaTotRiskPrem = wsaaTotRiskPrem.add(rskp.getRiskprem() == null ? BigDecimal.ZERO : rskp.getRiskprem());
			if(i <= 0 && isGT(rskp.getPolfee(), ZERO) && isNE(tr59xrec.polfee, SPACE)) {
				wsaaTotRiskPrem = wsaaTotRiskPrem.add(rskp.getPolfee());
				i++;
			}
		}
		return wsaaTotRiskPrem;
	}

	protected void initTaxDeductionRec() {
		taxdrec.setCoyPfx(coyPfx);
		taxdrec.setChdrcoy(entry.getValue().getChdrcoy());
		taxdrec.setChdrnum(entry.getValue().getChdrnum());
		taxdrec.setCnttype(entry.getValue().getCnttype());
		taxdrec.setDatefrm(entry.getValue().getDatefrm());
		taxdrec.setDateto(entry.getValue().getDateto());
		for(int i=0; i<tr60oList.size(); i++) {
			taxdrec.getContributionType().add(tr60oList.get(i).getItemitem());
		}
		taxdrec.getContributionAmt().add(entry.getValue().getZoeramt() == null ? BigDecimal.ZERO.toString() : entry.getValue().getZoeramt().toString());
		taxdrec.getContributionAmt().add(entry.getValue().getZsgtamt() == null ? BigDecimal.ZERO.toString() : entry.getValue().getZsgtamt().toString());
		taxdrec.getContributionAmt().add(entry.getValue().getZdedamt() == null ? BigDecimal.ZERO.toString() : entry.getValue().getZdedamt().toString());
		taxdrec.getContributionAmt().add(entry.getValue().getZundamt() == null ? BigDecimal.ZERO.toString() : entry.getValue().getZundamt().toString());
		taxdrec.getContributionAmt().add(entry.getValue().getZslrysamt() == null ? BigDecimal.ZERO.toString() : entry.getValue().getZslrysamt().toString());
		taxdrec.getContributionAmt().add(entry.getValue().getZspsamt() == null ? BigDecimal.ZERO.toString() : entry.getValue().getZspsamt().toString());
		taxdrec.setEffdate(bsscIO.getEffectiveDate().toInt());
		taxdrec.setCount(String.valueOf(taxdrec.getContributionType().size()));
	}

	@Override
	protected void update3000() {
		if(updateFlag) {
			updateTaxContribution();
			updateChdr();
			writePtrn();
		}
		rewriteZctx();
		sftlockRecBean.setFunction("UNLK");
		performLockOperation();
		ct09Value++;
		taxdrec.getContributionAmt().clear();
		taxdrec.getContributionType().clear();
	}
	
	protected void updateTaxContribution() {
		yctxpf = yctxpfDAO.fetchRecord(taxdrec.getChdrcoy(), taxdrec.getChdrnum());
		if(yctxpf != null) {
			Yctxpf yctx = yctxpf.copyObject();
			yctx.setValidFlag("2");
			yctx.setTranno(yctxpf.getTranno()+1);
			yctxpfUpdateList.add(yctx);
			ct03Value++;
		}
		else {
			yctxpf = new Yctxpf();
			yctxpf.setTranno(1);
			ct04Value++;
		}
		setYctxpf(yctxpf);
		yctxpf.setValidFlag("1");
		yctxpfInsertList.add(yctxpf);
	}
	
	protected void setYctxpf(Yctxpf yctxpf) {
		yctxpf.setChdrcoy(taxdrec.getChdrcoy());
		yctxpf.setChdrnum(taxdrec.getChdrnum());
		yctxpf.setCnttype(taxdrec.getCnttype());
		yctxpf.setStaxdt(taxdrec.getDatefrm());	//check from date
		yctxpf.setEtaxdt(taxdrec.getDateto());	//check to date
		yctxpf.setBatctrcde(batcdorrec.trcde.toString());
		yctxpf.setEffDate(bsscIO.getEffectiveDate().toInt());
		yctxpf.setTotalTaxAmt(taxdrec.getTotalTaxAmt());
		yctxpf.setRpoffset(taxdrec.getRiskPremium());
	}
	
	protected void updateChdr() {
		if(chdrpfMap.containsKey(taxdrec.getChdrnum())) {
			chdrpf = chdrpfMap.get(taxdrec.getChdrnum());
			chdrpf.setTranno(chdrpf.getTranno()+1);
			chdrpfUpdateList.add(chdrpf);
			ct05Value++;
		}
	}
	
	protected void writePtrn() {
		Ptrnpf ptrnpf = new Ptrnpf();
		ptrnpf.setChdrcoy(taxdrec.getChdrcoy());
		ptrnpf.setChdrnum(taxdrec.getChdrnum());
		ptrnpf.setTranno(chdrpf.getTranno());
		ptrnpf.setPtrneff(taxdrec.getEffdate());
		ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setUserT(0);
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnpfInsertList.add(ptrnpf);
		ct06Value++;
	}
	
	protected void rewriteZctx() {
		Zctxpf zctxpf = entry.getValue();
		zctxpf.setProcflag(procflag);
		zctxpfUpdateList.add(zctxpf.copyObject());
		ct07Value++;
		Zctxpf zctx = entry.getValue();
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(1);
		datcon2Pojo.setIntDate1(Integer.toString(zctx.getDatefrm()));
		datcon2Pojo.setFrequency("01");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		zctx.setDatefrm(Integer.parseInt(datcon2Pojo.getIntDate2()));
	
		datcon2Pojo.setIntDate1(Integer.toString(zctx.getDateto()));
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		zctx.setDateto(Integer.parseInt(datcon2Pojo.getIntDate2()));
		
		zctx.setZsgtamt(BigDecimal.ZERO);
		zctx.setZoeramt(BigDecimal.ZERO);
		zctx.setZdedamt(BigDecimal.ZERO);
		zctx.setZundamt(BigDecimal.ZERO);
		zctx.setZspsamt(BigDecimal.ZERO);
		zctx.setZslrysamt(BigDecimal.ZERO);
		zctx.setAmount(BigDecimal.ZERO);
		zctx.setProcflag(SPACE);
		zctx.setEffdate(bsscIO.getEffectiveDate().toInt());
		zctxpfInsertList.add(zctx);
		ct08Value++;
	}
	
	protected void performLockOperation() {
    	sftlockRecBean.setCompany(bsprIO.getCompany().toString().trim());
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(taxdrec.getChdrnum().trim());
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
    		&& !sftlockRecBean.getFunction().equals(sftlockRecBean.getStatuz())) {
    			syserrrec.statuz.set(sftlockRecBean.getStatuz());
    			fatalError600();
    	}
	}

	@Override
	protected void commit3500() {
		if(yctxpfUpdateList != null && !yctxpfUpdateList.isEmpty()) {
			if(!yctxpfDAO.updateRecord(yctxpfUpdateList)) {
				fatalError600();
			}
			yctxpfUpdateList.clear();
		}
		if(yctxpfInsertList != null && !yctxpfInsertList.isEmpty()) {
			yctxpfDAO.insertRecord(yctxpfInsertList);
			yctxpfInsertList.clear();
		}
		if(chdrpfUpdateList != null && !chdrpfUpdateList.isEmpty()) {
			chdrpfDAO.updateChdrTrannoByUniqueNo(chdrpfUpdateList);
			chdrpfUpdateList.clear();
		}
		if(ptrnpfInsertList != null && !ptrnpfInsertList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(ptrnpfInsertList);
			ptrnpfInsertList.clear();
		}
		if(zctxpfUpdateList != null && !zctxpfUpdateList.isEmpty()) {
			if(!zctxpfDAO.updateRecords(zctxpfUpdateList)) {
				fatalError600();
			}
			zctxpfUpdateList.clear();
		}
		if(zctxpfInsertList != null && !zctxpfInsertList.isEmpty()) {
			if(!zctxpfDAO.insertRecord(zctxpfInsertList)) {
				fatalError600();
			}
			zctxpfInsertList.clear();
		}
		commitControlTotals();
	}
	
	protected void commitControlTotals() {
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callContot001();
		ct01Value = 0;

		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callContot001();
		ct02Value = 0;
		
		contotrec.totno.set(ct03);
		contotrec.totval.set(ct03Value);
		callContot001();
		ct03Value = 0;
		
		contotrec.totno.set(ct04);
		contotrec.totval.set(ct04Value);
		callContot001();
		ct04Value = 0;
		
		contotrec.totno.set(ct05);
		contotrec.totval.set(ct05Value);
		callContot001();
		ct05Value = 0;
		
		contotrec.totno.set(ct06);
		contotrec.totval.set(ct06Value);
		callContot001();
		ct06Value = 0;
		
		contotrec.totno.set(ct07);
		contotrec.totval.set(ct07Value);
		callContot001();
		ct07Value = 0;
		
		contotrec.totno.set(ct08);
		contotrec.totval.set(ct08Value);
		callContot001();
		ct08Value = 0;
		
		contotrec.totno.set(ct09);
		contotrec.totval.set(ct09Value);
		callContot001();
		ct09Value = 0;
	}

	@Override
	protected void close4000() {
		tr59xMap.clear();
		t5679List.clear();
		tr59xItemList.clear();
		t5679StatcodeList.clear();
		td5jsMap.clear();
		t5645Map.clear();
		tr5lxList.clear();
		tr60oList.clear();
		if(chdrpfMap != null)
			chdrpfMap.clear();
		if(covrpfMap != null)
			covrpfMap.clear();
		if(chdrnumList != null)
			chdrnumList.clear();
		if(zctxpfMap != null)
			zctxpfMap.clear();
		itr = null;
		lsaaStatuz.set(varcom.oK);
	}

	@Override
	protected void rollback3600() {
		/*ROLLBACK*/
	}

}
