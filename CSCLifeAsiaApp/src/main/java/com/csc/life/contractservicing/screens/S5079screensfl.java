package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5079screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 6;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] { 1,  4,  5, 11,  12,  36,  37}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {16, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5079ScreenVars sv = (S5079ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5079screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5079screensfl, 
			sv.S5079screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5079ScreenVars sv = (S5079ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5079screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5079ScreenVars sv = (S5079ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5079screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5079screensflWritten.gt(0))
		{
			sv.s5079screensfl.setCurrentIndex(0);
			sv.S5079screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5079ScreenVars sv = (S5079ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5079screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5079ScreenVars screenVars = (S5079ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.hasgnnum.setFieldName("hasgnnum");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.asgnsel.setFieldName("asgnsel");
				screenVars.reasoncd.setFieldName("reasoncd");
				screenVars.commfromDisp.setFieldName("commfromDisp");
				screenVars.commtoDisp.setFieldName("commtoDisp");
				screenVars.assigneeName.setFieldName("assigneeName");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.updteflag.set(dm.getField("updteflag"));
			screenVars.hasgnnum.set(dm.getField("hasgnnum"));
			screenVars.hseqno.set(dm.getField("hseqno"));
			screenVars.asgnsel.set(dm.getField("asgnsel"));
			screenVars.reasoncd.set(dm.getField("reasoncd"));
			screenVars.commfromDisp.set(dm.getField("commfromDisp"));
			screenVars.commtoDisp.set(dm.getField("commtoDisp"));
			screenVars.assigneeName.set(dm.getField("assigneeName"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5079ScreenVars screenVars = (S5079ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.hasgnnum.setFieldName("hasgnnum");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.asgnsel.setFieldName("asgnsel");
				screenVars.reasoncd.setFieldName("reasoncd");
				screenVars.commfromDisp.setFieldName("commfromDisp");
				screenVars.commtoDisp.setFieldName("commtoDisp");
				screenVars.assigneeName.setFieldName("assigneeName");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("updteflag").set(screenVars.updteflag);
			dm.getField("hasgnnum").set(screenVars.hasgnnum);
			dm.getField("hseqno").set(screenVars.hseqno);
			dm.getField("asgnsel").set(screenVars.asgnsel);
			dm.getField("reasoncd").set(screenVars.reasoncd);
			dm.getField("commfromDisp").set(screenVars.commfromDisp);
			dm.getField("commtoDisp").set(screenVars.commtoDisp);
			dm.getField("assigneeName").set(screenVars.assigneeName);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5079screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5079ScreenVars screenVars = (S5079ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.updteflag.clearFormatting();
		screenVars.hasgnnum.clearFormatting();
		screenVars.hseqno.clearFormatting();
		screenVars.asgnsel.clearFormatting();
		screenVars.reasoncd.clearFormatting();
		screenVars.commfromDisp.clearFormatting();
		screenVars.commtoDisp.clearFormatting();
		screenVars.assigneeName.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5079ScreenVars screenVars = (S5079ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.updteflag.setClassString("");
		screenVars.hasgnnum.setClassString("");
		screenVars.hseqno.setClassString("");
		screenVars.asgnsel.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.commfromDisp.setClassString("");
		screenVars.commtoDisp.setClassString("");
		screenVars.assigneeName.setClassString("");
	}

/**
 * Clear all the variables in S5079screensfl
 */
	public static void clear(VarModel pv) {
		S5079ScreenVars screenVars = (S5079ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.updteflag.clear();
		screenVars.hasgnnum.clear();
		screenVars.hseqno.clear();
		screenVars.asgnsel.clear();
		screenVars.reasoncd.clear();
		screenVars.commfromDisp.clear();
		screenVars.commfrom.clear();
		screenVars.commtoDisp.clear();
		screenVars.commto.clear();
		screenVars.assigneeName.clear();
	}
}
