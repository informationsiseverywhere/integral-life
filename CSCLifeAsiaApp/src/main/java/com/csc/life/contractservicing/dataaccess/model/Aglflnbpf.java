package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Aglflnbpf
 */
public class Aglflnbpf implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private long uniqueNumber;
	
	private String clntnum;
	private String givname;
	private String surname;
	private String lsurname;
	private String lgivname;
	private String clttype;
	private String agntbr;
	private Integer dteapp;
	private Integer dtetrm;
	private Integer dteexp;
	private String bcmtab;
	private String rcmtab;
	private String scmtab;
	private String agcls;
	private String reportag;
	private BigDecimal ovcpc;
	private String agtype;
	private String aracde;
	private String zrorcode;
	private Integer effdate;
	private String agntcoy;
	private String agntnum;
	private String tlaglicno;
	private Integer tlicexpdt;
	private String prdagent;
	private String agccqind;
	private String validflag;
	private String jobnm;
	private String usrprf;
	private Date datime;
	private String zrecruit;

	public Aglflnbpf() {
	}

	public Aglflnbpf(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public Aglflnbpf(String clntnum, String agntbr, Integer dteapp,
			Integer dtetrm, Integer dteexp, String bcmtab,
			String rcmtab, String scmtab, String agcls,
			String reportag, BigDecimal ovcpc, String agtype,
			String aracde, String zrorcode, Integer effdate,
			long uniqueNumber, String agntcoy, String agntnum,
			String tlaglicno, Integer tlicexpdt, String prdagent,
			String agccqind, String validflag, String jobnm,
			String usrprf, Date datime, String zrecruit) {
		this.clntnum = clntnum;
		this.agntbr = agntbr;
		this.dteapp = dteapp;
		this.dtetrm = dtetrm;
		this.dteexp = dteexp;
		this.bcmtab = bcmtab;
		this.rcmtab = rcmtab;
		this.scmtab = scmtab;
		this.agcls = agcls;
		this.reportag = reportag;
		this.ovcpc = ovcpc;
		this.agtype = agtype;
		this.aracde = aracde;
		this.zrorcode = zrorcode;
		this.effdate = effdate;
		this.uniqueNumber = uniqueNumber;
		this.agntcoy = agntcoy;
		this.agntnum = agntnum;
		this.tlaglicno = tlaglicno;
		this.tlicexpdt = tlicexpdt;
		this.prdagent = prdagent;
		this.agccqind = agccqind;
		this.validflag = validflag;
		this.jobnm = jobnm;
		this.usrprf = usrprf;
		this.datime = (Date) datime.clone(); //IJTI-460
		this.zrecruit = zrecruit;
	}

	public String getClntnum() {
		return this.clntnum;
	}

	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}

	public String getGivname() {
		return givname;
	}

	public void setGivname(String givname) {
		this.givname = givname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLsurname() {
		return lsurname;
	}

	public void setLsurname(String lsurname) {
		this.lsurname = lsurname;
	}

	public String getLgivname() {
		return lgivname;
	}

	public void setLgivname(String lgivname) {
		this.lgivname = lgivname;
	}

	public String getClttype() {
		return clttype;
	}

	public void setClttype(String clttype) {
		this.clttype = clttype;
	}

	public String getAgntbr() {
		return this.agntbr;
	}

	public void setAgntbr(String agntbr) {
		this.agntbr = agntbr;
	}

	public Integer getDteapp() {
		return this.dteapp;
	}

	public void setDteapp(Integer dteapp) {
		this.dteapp = dteapp;
	}

	public Integer getDtetrm() {
		return this.dtetrm;
	}

	public void setDtetrm(Integer dtetrm) {
		this.dtetrm = dtetrm;
	}

	public Integer getDteexp() {
		return this.dteexp;
	}

	public void setDteexp(Integer dteexp) {
		this.dteexp = dteexp;
	}

	public String getBcmtab() {
		return this.bcmtab;
	}

	public void setBcmtab(String bcmtab) {
		this.bcmtab = bcmtab;
	}

	public String getRcmtab() {
		return this.rcmtab;
	}

	public void setRcmtab(String rcmtab) {
		this.rcmtab = rcmtab;
	}

	public String getScmtab() {
		return this.scmtab;
	}

	public void setScmtab(String scmtab) {
		this.scmtab = scmtab;
	}

	public String getAgcls() {
		return this.agcls;
	}

	public void setAgcls(String agcls) {
		this.agcls = agcls;
	}

	public String getReportag() {
		return this.reportag;
	}

	public void setReportag(String reportag) {
		this.reportag = reportag;
	}

	public BigDecimal getOvcpc() {
		return this.ovcpc;
	}

	public void setOvcpc(BigDecimal ovcpc) {
		this.ovcpc = ovcpc;
	}

	public String getAgtype() {
		return this.agtype;
	}

	public void setAgtype(String agtype) {
		this.agtype = agtype;
	}

	public String getAracde() {
		return this.aracde;
	}

	public void setAracde(String aracde) {
		this.aracde = aracde;
	}

	public String getZrorcode() {
		return this.zrorcode;
	}

	public void setZrorcode(String zrorcode) {
		this.zrorcode = zrorcode;
	}

	public Integer getEffdate() {
		return this.effdate;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getAgntcoy() {
		return this.agntcoy;
	}

	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}

	public String getAgntnum() {
		return this.agntnum;
	}

	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}

	public String getTlaglicno() {
		return this.tlaglicno;
	}

	public void setTlaglicno(String tlaglicno) {
		this.tlaglicno = tlaglicno;
	}

	public Integer getTlicexpdt() {
		return this.tlicexpdt;
	}

	public void setTlicexpdt(Integer tlicexpdt) {
		this.tlicexpdt = tlicexpdt;
	}

	public String getPrdagent() {
		return this.prdagent;
	}

	public void setPrdagent(String prdagent) {
		this.prdagent = prdagent;
	}

	public String getAgccqind() {
		return this.agccqind;
	}

	public void setAgccqind(String agccqind) {
		this.agccqind = agccqind;
	}

	public String getValidflag() {
		return this.validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getJobnm() {
		return this.jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public String getUsrprf() {
		return this.usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	//IJTI-461 START
	public Date getDatime() {
		if (this.datime==null){
			return null;
		} else {
			return (Date) this.datime.clone();
		}
	} //IJTI-461 END

	public void setDatime(Date datime) {
		this.datime = (Date) datime.clone(); //IJTI-460
	}

	public String getZrecruit() {
		return this.zrecruit;
	}

	public void setZrecruit(String zrecruit) {
		this.zrecruit = zrecruit;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof Aglflnbpf))
			return false;
		Aglflnbpf castOther = (Aglflnbpf) other;

		return this.agntnum.equals(castOther.agntnum)
				&& this.clntnum.equals(castOther.clntnum);
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getClntnum() == null ? 0 : this.getClntnum().hashCode());
		result = 37 * result
				+ (getAgntbr() == null ? 0 : this.getAgntbr().hashCode());
		result = 37 * result
				+ (getDteapp() == null ? 0 : this.getDteapp().hashCode());
		result = 37 * result
				+ (getDtetrm() == null ? 0 : this.getDtetrm().hashCode());
		result = 37 * result
				+ (getDteexp() == null ? 0 : this.getDteexp().hashCode());
		result = 37 * result
				+ (getBcmtab() == null ? 0 : this.getBcmtab().hashCode());
		result = 37 * result
				+ (getRcmtab() == null ? 0 : this.getRcmtab().hashCode());
		result = 37 * result
				+ (getScmtab() == null ? 0 : this.getScmtab().hashCode());
		result = 37 * result
				+ (getAgcls() == null ? 0 : this.getAgcls().hashCode());
		result = 37 * result
				+ (getReportag() == null ? 0 : this.getReportag().hashCode());
		result = 37 * result
				+ (getOvcpc() == null ? 0 : this.getOvcpc().hashCode());
		result = 37 * result
				+ (getAgtype() == null ? 0 : this.getAgtype().hashCode());
		result = 37 * result
				+ (getAracde() == null ? 0 : this.getAracde().hashCode());
		result = 37 * result
				+ (getZrorcode() == null ? 0 : this.getZrorcode().hashCode());
		result = 37 * result
				+ (getEffdate() == null ? 0 : this.getEffdate().hashCode());
		result = 37 * result + (int) this.getUniqueNumber();
		result = 37 * result
				+ (getAgntcoy() == null ? 0 : this.getAgntcoy().hashCode());
		result = 37 * result
				+ (getAgntnum() == null ? 0 : this.getAgntnum().hashCode());
		result = 37 * result
				+ (getTlaglicno() == null ? 0 : this.getTlaglicno().hashCode());
		result = 37 * result
				+ (getTlicexpdt() == null ? 0 : this.getTlicexpdt().hashCode());
		result = 37 * result
				+ (getPrdagent() == null ? 0 : this.getPrdagent().hashCode());
		result = 37 * result
				+ (getAgccqind() == null ? 0 : this.getAgccqind().hashCode());
		result = 37 * result
				+ (getValidflag() == null ? 0 : this.getValidflag().hashCode());
		result = 37 * result
				+ (getJobnm() == null ? 0 : this.getJobnm().hashCode());
		result = 37 * result
				+ (getUsrprf() == null ? 0 : this.getUsrprf().hashCode());
		result = 37 * result
				+ (getDatime() == null ? 0 : this.getDatime().hashCode());
		result = 37 * result
				+ (getZrecruit() == null ? 0 : this.getZrecruit().hashCode());
		return result;
	}

}
