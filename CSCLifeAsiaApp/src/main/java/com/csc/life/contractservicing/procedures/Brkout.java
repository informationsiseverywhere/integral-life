/*
 * File: Brkout.java
 * Date: 29 August 2009 22:36:39
 * Author: Quipoz Limited
 * 
 * Class transformed from BRKOUT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.dataaccess.AgcmbrkTableDAM;
import com.csc.life.contractservicing.dataaccess.Acblbk3TableDAM;
import com.csc.life.contractservicing.dataaccess.Acmvbk3TableDAM;
import com.csc.life.contractservicing.dataaccess.CovrbrkTableDAM;
import com.csc.life.contractservicing.dataaccess.Incrbk1TableDAM;
import com.csc.life.contractservicing.dataaccess.Incrbk2TableDAM;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.contractservicing.recordstructures.Genoutrec;
import com.csc.life.productdefinition.dataaccess.Acblbk2TableDAM;
import com.csc.life.productdefinition.dataaccess.Acmvbk2TableDAM;
import com.csc.life.productdefinition.dataaccess.AcmvbrkTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* BREAKOUT Processing occurs when the policy selected from the
* calling program belongs to the summarised policy record which
* is denoted by a plan suffix of 00. For whatever reason this
* policy must become a record in its own right for further
* processing.
*
* In this program, records are 'broken out' in one of two possible
* ways depending on the file. If the file holds the lowest possible
* values in the system then use processing 'A'. If the file is
* built from values on another file use processing 'B' (ACBL are
* built from the values on the ACMVs).
*
* PROCESSING A
* ------------
*
* Get the summarised record (plan suffix 00).
* Store the values of the summarised record in an array
* (WSAA-AMOUNT-IN).
* Write a new record until the last policy that requires to be
* broken out is reached. The values on the new record are
* calculated and stored in an array (WSAA-AMOUNT-OUT) as follows -
*
*    COMPUTE WSAA-AMOUNT-OUT (WSAA-SUB) ROUNDED =
*                 WSAA-AMOUNT-IN (WSAA-SUB) / BRK-OLD-SUMMARY.
*
* These new values are totalled for later use in an array;
*
*    COMPUTE WSAA-TOT-AMOUNT (WSAA-SUB) =
*                 WSAA-TOT-AMOUNT (WSAA-SUB) +
*                 WSAA-AMOUNT-OUT (WSAA-SUB).
*
* When the last policy that requires to be broken out is reached,
* calculate the values on the record as the following example for
* ACMVs;
*
*    COMPUTE ACMVBRK-ORIGAMT =
*                 WSAA-AMOUNT-IN (1) - WSAD-TOT-AMOUNT (1).
*
* These values are either written as a new record or rewritten on
* an old record depending on the following;
*
*  1.The policy to be broken out is the last policy of the plan.
*    In this case it is necessary to delete the summarised record
*    (plan-suffix 00) and write a new record using the
*    calculation above, with a plan suffix of 1.
*
*  2.The policy to be broken out is not the last policy on the
*    plan so calculate the new values as above and rewrite the
*    summarised record (plan-suffix 00).
*
* PROCESSING B
* ------------
*
* This processing is applied to master files that hold the totals
* of values held on a transaction file. eg. ACBLs hold the total
* of ACMVs.
*
* The transaction record must be broken out before the master
* file processing.
*
* Get the summarised record of the master file.
* eg ACBL-PLAN-SUFFIX 00.
*
* There are two methods of processing depending on whether the
* last policy of the plan is to be broken out or not.
*
* If the policy to be broken out is the last policy of the plan,
* write a new record until all the policies nave been broken out.
* The values on each new record is calculated by the accumulation
* of the values on the transaction records for that policy
* (ACMV-PLAN-SUFFIX).
*
* If the policy to be broken out is not the last policy of the
* plan, write a new record until the last policy that requires to
* be broken out is reached. The values on each new record is
* calculated by the accumulation of the values on the transaction
* records for that policy (ACMV-PLAN-SUFFIX).
*
* When the last policy required to be broken out is reached and
* as its not the last policy in the plan, rewrite the summarised
* record (plan-suffix 00) and calculate the values as the
* following example for ACBL;
*
*    COMPUTE ACBLBR3-SACSCURBAL =
*                         ACBLBK3-SACSCURBAL -
*                         WSAA-TOT-SACSCURBAL.
*
* NOTE
* ----
* If the contract is a unit-linked product the table T5671 is
* read with a concaternated key of 'BRK' + CRTABLE for the first
* coverage only. Table T5671 holds the unit linked generic
* subroutine.
*
* NOTE
* ----
* The files that use PROCESSING A are COVR, AGCM, ACMV, INCR,
*                                 and ACMC.
* The files that use PROCESSING B are ACBL.
*
*
*****************************************************************
* </pre>
*/
public class Brkout extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "BRKOUT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaAgcmNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCovrNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaAcblNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaAcmvNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaIncrNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* TABLES */
	private static final String t5671 = "T5671";

		/* WSAA-AMOUNT-TABLE */
	private FixedLengthStringData[] wsaaAmountsIn = FLSInittedArray (17, 17);
	private ZonedDecimalData[] wsaaAmountIn = ZDArrayPartOfArrayStructure(17, 2, wsaaAmountsIn, 0);

	private FixedLengthStringData[] wsaaAmountsOut = FLSInittedArray (17, 17);
	private ZonedDecimalData[] wsaaAmountOut = ZDArrayPartOfArrayStructure(17, 2, wsaaAmountsOut, 0);
		/* WSAA-TOT-TABLE */
	private PackedDecimalData[] wsaaTotAmount = PDInittedArray(17, 17, 2);
	private PackedDecimalData wsaaSacscurbal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotSacscurbal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBrkPlnSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaGenericSub = new PackedDecimalData(4, 0).init(ZERO);

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0).init("BRKO");
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);
	private Acblbk2TableDAM acblbk2IO = new Acblbk2TableDAM();
	private Acblbk3TableDAM acblbk3IO = new Acblbk3TableDAM();
	private Acmvbk2TableDAM acmvbk2IO = new Acmvbk2TableDAM();
	private Acmvbk3TableDAM acmvbk3IO = new Acmvbk3TableDAM();
	private AcmvbrkTableDAM acmvbrkIO = new AcmvbrkTableDAM();
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmbrkTableDAM agcmbrkIO = new AgcmbrkTableDAM();
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrbrkTableDAM covrbrkIO = new CovrbrkTableDAM();
	private Incrbk1TableDAM incrbk1IO = new Incrbk1TableDAM();
	private Incrbk2TableDAM incrbk2IO = new Incrbk2TableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5671rec t5671rec = new T5671rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Genoutrec genoutrec = new Genoutrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextCovr205, 
		exit209, 
		exit309, 
		nextr405, 
		exit409, 
		nextr505, 
		exit509, 
		nextr605, 
		exit609
	}

	public Brkout() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		brkoutrec.outRec = convertAndSetParam(brkoutrec.outRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		if (isEQ(brkoutrec.brkNewSummary, 0)) {
			brkoutrec.brkNewSummary.set(1);
		}
		brkoutrec.brkStatuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaAcmvNoRecs.set(ZERO);
		wsaaIncrNoRecs.set(ZERO);
		wsaaCovrNoRecs.set(ZERO);
		wsaaAgcmNoRecs.set(ZERO);
		initialize100();
		while ( !(isEQ(covrbrkIO.getStatuz(), varcom.endp))) {
			mainProcessingCovr200();
		}
		
		while ( !(isEQ(incrbk1IO.getStatuz(), varcom.endp))) {
			mainProcessingIncr300();
		}
		
		while ( !(isEQ(acmvbrkIO.getStatuz(), varcom.endp))) {
			mainProcessingAcmv400();
		}
		
		while ( !(isEQ(acblbk3IO.getStatuz(), varcom.endp))) {
			mainProcessingAcbl500();
		}
		
		while ( !(isEQ(agcmbrkIO.getStatuz(), varcom.endp))) {
			mainProcessingAgcm600();
		}
		
		/* PERFORM 700-MAIN-PROCESSING-ACMC UNTIL               <LA4407>*/
		/*     ACMCBRK-STATUZ = ENDP.                           <LA4407>*/
		for (wsaaGenericSub.set(1); !(isGT(wsaaGenericSub, 4)); wsaaGenericSub.add(1)){
			callSubprog990();
		}
		updateContractHeader1000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		para101();
	}

protected void para101()
	{
		brkoutrec.brkStatuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		t5671rec.t5671Rec.set(SPACES);
		/*  first read of COVRBRK file*/
		covrbrkIO.setDataArea(SPACES);
		covrbrkIO.setChdrnum(brkoutrec.brkChdrnum);
		covrbrkIO.setChdrcoy(brkoutrec.brkChdrcoy);
		covrbrkIO.setPlanSuffix(ZERO);
		covrbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, covrbrkIO);
		if (isNE(covrbrkIO.getStatuz(), varcom.oK)
		&& isNE(covrbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrbrkIO.getParams());
			fatalError9000();
		}
		if (isNE(covrbrkIO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(covrbrkIO.getChdrcoy(), brkoutrec.brkChdrcoy)
		|| isNE(covrbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(covrbrkIO.getStatuz(), varcom.endp)) {
			wsaaCovrNoRecs.set(1);
		}
		/*  first read of AGCM file*/
		agcmbrkIO.setDataArea(SPACES);
		agcmbrkIO.setChdrnum(brkoutrec.brkChdrnum);
		agcmbrkIO.setChdrcoy(brkoutrec.brkChdrcoy);
		agcmbrkIO.setPlanSuffix(ZERO);
		agcmbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, agcmbrkIO);
		if (isNE(agcmbrkIO.getStatuz(), varcom.oK)
		&& isNE(agcmbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbrkIO.getParams());
			fatalError9000();
		}
		if (isNE(agcmbrkIO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(agcmbrkIO.getChdrcoy(), brkoutrec.brkChdrcoy)
		|| isNE(agcmbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(agcmbrkIO.getStatuz(), varcom.endp)) {
			wsaaAgcmNoRecs.set(1);
		}
		/* The Plan Suffix on the ACMV record is stored in the RLDGACCT*/
		/* (Entity) field. This field is broken down as follows, CHDRNUM*/
		/* (08) Life (02) Coverage (02) Rider (02) and Plan Suffix (02),*/
		/* making up the 16 character string.*/
		/* As ACMVBRK is keyed on Co. and RLDGACCT, if we give the full*/
		/* key of Co. and CHDRNUM in RLDGACCT together with a '00' value*/
		/* for LIFE this will enable the process to   miss any records*/
		/* that have a blank in Plan-Suffix. It will retrieve only those*/
		/* records that have a valid Plan-Suffix value starting from 00.*/
		acmvbrkIO.setDataArea(SPACES);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(brkoutrec.brkChdrnum);
		wsaaRldgLife.set("00");
		acmvbrkIO.setRldgacct(wsaaRldgacct);
		acmvbrkIO.setRldgcoy(brkoutrec.brkChdrcoy);
		acmvbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, acmvbrkIO);
		if (isNE(acmvbrkIO.getStatuz(), varcom.oK)
		&& isNE(acmvbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvbrkIO.getParams());
			fatalError9000();
		}
		wsaaRldgacct.set(acmvbrkIO.getRldgacct());
		/*    Check we have an ACMV for the correct contract.              */
		if (isNE(acmvbrkIO.getRldgcoy(), brkoutrec.brkChdrcoy)
		|| isNE(wsaaRldgChdrnum, brkoutrec.brkChdrnum)
		|| isEQ(acmvbrkIO.getStatuz(), varcom.endp)) {
			wsaaAcmvNoRecs.set(1);
		}
		/*  Continue reading around the ACMVs until a plan suffix*/
		/*  is found which is not SPACES.*/
		while ( !(isNE(wsaaRldgPlnsfx, "  ")
		|| isEQ(wsaaAcmvNoRecs, 1))) {
			acmvbrkIO.setFunction(varcom.rlse);
			FixedLengthStringData groupTEMP = acmvbrkIO.getParams();
			callProgram(acmvbrkIO.getIo(), groupTEMP);
			acmvbrkIO.setParams(groupTEMP);
			if (isNE(acmvbrkIO.getStatuz(), varcom.oK)
			&& isNE(acmvbrkIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvbrkIO.getParams());
				fatalError9000();
			}
			/*  Read the next record.*/
			acmvbrkIO.setFunction(varcom.nextr);
			FixedLengthStringData groupTEMP2 = acmvbrkIO.getParams();
			callProgram(acmvbrkIO.getIo(), groupTEMP2);
			acmvbrkIO.setParams(groupTEMP2);
			if (isNE(acmvbrkIO.getStatuz(), varcom.oK)
			&& isNE(acmvbrkIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvbrkIO.getParams());
				fatalError9000();
			}
			/*  Store the plan suffix.*/
			wsaaRldgacct.set(acmvbrkIO.getRldgacct());
			/*  Check for change of contract.*/
			if (isNE(acmvbrkIO.getRldgcoy(), brkoutrec.brkChdrcoy)
			|| isNE(wsaaRldgChdrnum, brkoutrec.brkChdrnum)
			|| isNE(wsaaRldgPlnsfx, ZERO)
			|| isEQ(acmvbrkIO.getStatuz(), varcom.endp)) {
				wsaaAcmvNoRecs.set(1);
			}
		}
		
		acblbk3IO.setDataArea(SPACES);
		acblbk3IO.setRldgcoy(brkoutrec.brkChdrcoy);
		wsaaRldgacct.set(SPACES);
		wsaaRldgLife.set("00");
		wsaaRldgChdrnum.set(brkoutrec.brkChdrnum);
		acblbk3IO.setRldgacct(wsaaRldgacct);
		acblbk3IO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, acblbk3IO);
		if (isNE(acblbk3IO.getStatuz(), varcom.oK)
		&& isNE(acblbk3IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acblbk3IO.getParams());
			fatalError9000();
		}
		wsaaRldgacct.set(acblbk3IO.getRldgacct());
		/*  Continue reading around the ACBLs until a plan suffix*/
		/*  is found which is not SPACES.*/
		while ( !(isNE(wsaaRldgPlnsfx, "  ")
		|| isEQ(wsaaAcblNoRecs, 1))) {
			acblbk3IO.setFunction(varcom.rlse);
			FixedLengthStringData groupTEMP3 = acblbk3IO.getParams();
			callProgram(acblbk3IO.getIo(), groupTEMP3);
			acblbk3IO.setParams(groupTEMP3);
			if (isNE(acblbk3IO.getStatuz(), varcom.oK)
			&& isNE(acblbk3IO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acblbk3IO.getParams());
				fatalError9000();
			}
			/*  Read the next record.*/
			acblbk3IO.setFunction(varcom.nextr);
			FixedLengthStringData groupTEMP4 = acblbk3IO.getParams();
			callProgram(acblbk3IO.getIo(), groupTEMP4);
			acblbk3IO.setParams(groupTEMP4);
			if (isNE(acblbk3IO.getStatuz(), varcom.oK)
			&& isNE(acblbk3IO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acblbk3IO.getParams());
				fatalError9000();
			}
			/*  Store the plan suffix.*/
			wsaaRldgacct.set(acblbk3IO.getRldgacct());
			/*  Check for change of contract.*/
			if (isNE(acblbk3IO.getRldgcoy(), brkoutrec.brkChdrcoy)
			|| isNE(wsaaRldgChdrnum, brkoutrec.brkChdrnum)
			|| isNE(wsaaRldgPlnsfx, ZERO)
			|| isEQ(acblbk3IO.getStatuz(), varcom.endp)) {
				wsaaAcblNoRecs.set(1);
			}
		}
		
		/*  first read of INCRBK1 file                                     */
		incrbk1IO.setDataArea(SPACES);
		incrbk1IO.setChdrnum(brkoutrec.brkChdrnum);
		incrbk1IO.setChdrcoy(brkoutrec.brkChdrcoy);
		incrbk1IO.setPlanSuffix(ZERO);
		incrbk1IO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, incrbk1IO);
		if (isNE(incrbk1IO.getStatuz(), varcom.oK)
		&& isNE(incrbk1IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incrbk1IO.getParams());
			fatalError9000();
		}
		if (isNE(incrbk1IO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(incrbk1IO.getChdrcoy(), brkoutrec.brkChdrcoy)
		|| isNE(incrbk1IO.getPlanSuffix(), ZERO)
		|| isEQ(incrbk1IO.getStatuz(), varcom.endp)) {
			wsaaIncrNoRecs.set(1);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			initializeTable150();
		}
	}

protected void initializeTable150()
	{
		/*PARA*/
		wsaaAmountIn[wsaaSub.toInt()].set(ZERO);
		wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void mainProcessingCovr200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para201();
				case nextCovr205: 
					nextCovr205();
				case exit209: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para201()
	{
		if (isEQ(wsaaCovrNoRecs, 1)) {
			covrbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit209);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			wsaaTotAmount[wsaaSub.toInt()].set(ZERO);
		}
		moveCovrValsToTable210();
		for (wsaaPlanSuffix.set(brkoutrec.brkOldSummary); !(isEQ(wsaaPlanSuffix, brkoutrec.brkNewSummary)); wsaaPlanSuffix.add(-1)){
			for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
				calcAmts2000();
			}
			moveCovrValsFromTable220();
			writeBrkCovr230();
		}
		calcNewSummaryRecCovr240();
		/* If it is a unit linked coverage, it should have an entry in*/
		/* T5671. The routine in T5671 are for all coverages, so we only*/
		/* need to read T5671 once even if there are multiple coverages.*/
		if (isNE(t5671rec.t5671Rec, SPACES)) {
			goTo(GotoLabel.nextCovr205);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(brkoutrec.brkChdrcoy);
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrbrkIO.getCrtable());
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
	}

protected void nextCovr205()
	{
		genoutrec.chdrcoy.set(brkoutrec.brkChdrcoy);
		genoutrec.chdrnum.set(brkoutrec.brkChdrnum);
		genoutrec.oldSummary.set(brkoutrec.brkOldSummary);
		genoutrec.newSummary.set(brkoutrec.brkNewSummary);
		genoutrec.batctrcde.set(brkoutrec.brkBatctrcde);
		genoutrec.cntcurr.set(covrbrkIO.getSicurr());
		covrbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrbrkIO);
		if (isNE(covrbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrbrkIO.getParams());
			fatalError9000();
		}
		/*   In the absence of a proper release function rewrite the*/
		/*   record at end of file.  This will enable the calling program*/
		/*   to update the record if required*/
		if (isNE(covrbrkIO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(covrbrkIO.getChdrcoy(), brkoutrec.brkChdrcoy)
		|| isNE(covrbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(covrbrkIO.getStatuz(), varcom.endp)) {
			covrbrkIO.setFunction(varcom.rewrt);
			covrbrkIO.setFormat(formatsInner.covrbrkrec);
			SmartFileCode.execute(appVars, covrbrkIO);
			if (isNE(covrbrkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrbrkIO.getParams());
				fatalError9000();
			}
			else {
				covrbrkIO.setStatuz(varcom.endp);
			}
		}
	}

protected void moveCovrValsToTable210()
	{
		entry211();
	}

protected void entry211()
	{
		wsaaAmountIn[1].set(covrbrkIO.getCrinst(1));
		wsaaAmountIn[2].set(covrbrkIO.getCrinst(2));
		wsaaAmountIn[3].set(covrbrkIO.getCrinst(3));
		wsaaAmountIn[4].set(covrbrkIO.getCrinst(4));
		wsaaAmountIn[5].set(covrbrkIO.getCrinst(5));
		wsaaAmountIn[6].set(covrbrkIO.getEmv(1));
		wsaaAmountIn[7].set(covrbrkIO.getEmv(2));
		wsaaAmountIn[8].set(covrbrkIO.getEmvint(1));
		wsaaAmountIn[9].set(covrbrkIO.getEmvint(2));
		wsaaAmountIn[10].set(covrbrkIO.getSumins());
		wsaaAmountIn[11].set(covrbrkIO.getVarSumInsured());
		wsaaAmountIn[12].set(covrbrkIO.getDeferPerdAmt());
		wsaaAmountIn[13].set(covrbrkIO.getTotMthlyBenefit());
		wsaaAmountIn[14].set(covrbrkIO.getSingp());
		wsaaAmountIn[15].set(covrbrkIO.getInstprem());
		wsaaAmountIn[16].set(covrbrkIO.getStatSumins());
		wsaaAmountIn[17].set(covrbrkIO.getCoverageDebt());
	}

protected void moveCovrValsFromTable220()
	{
		entry221();
	}

protected void entry221()
	{
		covrbrkIO.setCrinst(1, wsaaAmountOut[1]);
		covrbrkIO.setCrinst(2, wsaaAmountOut[2]);
		covrbrkIO.setCrinst(3, wsaaAmountOut[3]);
		covrbrkIO.setCrinst(4, wsaaAmountOut[4]);
		covrbrkIO.setCrinst(5, wsaaAmountOut[5]);
		covrbrkIO.setEmv(1, wsaaAmountOut[6]);
		covrbrkIO.setEmv(2, wsaaAmountOut[7]);
		covrbrkIO.setEmvint(1, wsaaAmountOut[8]);
		covrbrkIO.setEmvint(2, wsaaAmountOut[9]);
		covrbrkIO.setSumins(wsaaAmountOut[10]);
		covrbrkIO.setVarSumInsured(wsaaAmountOut[11]);
		covrbrkIO.setDeferPerdAmt(wsaaAmountOut[12]);
		covrbrkIO.setTotMthlyBenefit(wsaaAmountOut[13]);
		covrbrkIO.setSingp(wsaaAmountOut[14]);
		covrbrkIO.setInstprem(wsaaAmountOut[15]);
		covrbrkIO.setStatSumins(wsaaAmountOut[16]);
		covrbrkIO.setCoverageDebt(wsaaAmountOut[17]);
	}

protected void writeBrkCovr230()
	{
		entry231();
	}

protected void entry231()
	{
		covrIO.setChdrcoy(covrbrkIO.getChdrcoy());
		covrIO.setChdrnum(covrbrkIO.getChdrnum());
		covrIO.setPlanSuffix(covrbrkIO.getPlanSuffix());
		covrIO.setLife(covrbrkIO.getLife());
		covrIO.setCoverage(covrbrkIO.getCoverage());
		covrIO.setRider(covrbrkIO.getRider());
		covrIO.setPlanSuffix(wsaaPlanSuffix);
		covrIO.setNonKey(covrbrkIO.getNonKey());
		covrIO.setPayrseqno(1);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecCovr240()
	{
		entry241();
	}

protected void entry241()
	{
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			deleteSummaryCovr250();
		}
		/*  Calculate the values of the summarised record or the last polic*/
		/*  record. Calculate the new values by subtracting the accumulated*/
		/*  amounts from the previous summarised values.*/
		setPrecision(covrbrkIO.getCrinst(1), 2);
		covrbrkIO.setCrinst(1, sub(wsaaAmountIn[1], wsaaTotAmount[1]));
		setPrecision(covrbrkIO.getCrinst(2), 2);
		covrbrkIO.setCrinst(2, sub(wsaaAmountIn[2], wsaaTotAmount[2]));
		setPrecision(covrbrkIO.getCrinst(3), 2);
		covrbrkIO.setCrinst(3, sub(wsaaAmountIn[3], wsaaTotAmount[3]));
		setPrecision(covrbrkIO.getCrinst(4), 2);
		covrbrkIO.setCrinst(4, sub(wsaaAmountIn[4], wsaaTotAmount[4]));
		setPrecision(covrbrkIO.getCrinst(5), 2);
		covrbrkIO.setCrinst(5, sub(wsaaAmountIn[5], wsaaTotAmount[5]));
		setPrecision(covrbrkIO.getEmv(1), 2);
		covrbrkIO.setEmv(1, sub(wsaaAmountIn[6], wsaaTotAmount[6]));
		setPrecision(covrbrkIO.getEmv(2), 2);
		covrbrkIO.setEmv(2, sub(wsaaAmountIn[7], wsaaTotAmount[7]));
		setPrecision(covrbrkIO.getEmvint(1), 2);
		covrbrkIO.setEmvint(1, sub(wsaaAmountIn[8], wsaaTotAmount[8]));
		setPrecision(covrbrkIO.getEmvint(2), 2);
		covrbrkIO.setEmvint(2, sub(wsaaAmountIn[9], wsaaTotAmount[9]));
		setPrecision(covrbrkIO.getSumins(), 2);
		covrbrkIO.setSumins(sub(wsaaAmountIn[10], wsaaTotAmount[10]));
		setPrecision(covrbrkIO.getVarSumInsured(), 2);
		covrbrkIO.setVarSumInsured(sub(wsaaAmountIn[11], wsaaTotAmount[11]));
		setPrecision(covrbrkIO.getDeferPerdAmt(), 2);
		covrbrkIO.setDeferPerdAmt(sub(wsaaAmountIn[12], wsaaTotAmount[12]));
		setPrecision(covrbrkIO.getTotMthlyBenefit(), 2);
		covrbrkIO.setTotMthlyBenefit(sub(wsaaAmountIn[13], wsaaTotAmount[13]));
		setPrecision(covrbrkIO.getSingp(), 2);
		covrbrkIO.setSingp(sub(wsaaAmountIn[14], wsaaTotAmount[14]));
		setPrecision(covrbrkIO.getInstprem(), 2);
		covrbrkIO.setInstprem(sub(wsaaAmountIn[15], wsaaTotAmount[15]));
		setPrecision(covrbrkIO.getStatSumins(), 2);
		covrbrkIO.setStatSumins(sub(wsaaAmountIn[16], wsaaTotAmount[16]));
		setPrecision(covrbrkIO.getCoverageDebt(), 2);
		covrbrkIO.setCoverageDebt(sub(wsaaAmountIn[17], wsaaTotAmount[17]));
		/* Write the last policy.*/
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkCovr230();
			return ;
		}
		/* To get here there are still records summarised so rewrite the*/
		/* the record.*/
		covrbrkIO.setFormat(formatsInner.covrbrkrec);
		covrbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrbrkIO);
		if (isNE(covrbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrbrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryCovr250()
	{
		/*ENTRY*/
		covrbrkIO.setFunction(varcom.delet);
		covrbrkIO.setFormat(formatsInner.covrbrkrec);
		SmartFileCode.execute(appVars, covrbrkIO);
		if (isNE(covrbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrbrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void mainProcessingIncr300()
	{
		try {
			para301();
			nextIncr305();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para301()
	{
		if (isEQ(wsaaIncrNoRecs, 1)) {
			incrbk1IO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit309);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			wsaaTotAmount[wsaaSub.toInt()].set(ZERO);
		}
		moveIncrValsToTable310();
		for (wsaaPlanSuffix.set(brkoutrec.brkOldSummary); !(isEQ(wsaaPlanSuffix, brkoutrec.brkNewSummary)); wsaaPlanSuffix.add(-1)){
			for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
				calcAmts2000();
			}
			moveIncrValsFromTable320();
			writeBrkIncr330();
		}
		calcNewSummaryRecIncr340();
	}

protected void nextIncr305()
	{
		incrbk1IO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incrbk1IO);
		if (isNE(incrbk1IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrbk1IO.getParams());
			fatalError9000();
		}
		/*   In the absence of a proper release function rewrite the       */
		/*   record at end of file.  This will enable the calling program  */
		/*   to update the record if required                              */
		if (isNE(incrbk1IO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(incrbk1IO.getChdrcoy(), brkoutrec.brkChdrcoy)
		|| isNE(incrbk1IO.getPlanSuffix(), ZERO)
		|| isEQ(incrbk1IO.getStatuz(), varcom.endp)) {
			incrbk1IO.setFunction(varcom.rewrt);
			incrbk1IO.setFormat(formatsInner.incrbk1rec);
			SmartFileCode.execute(appVars, incrbk1IO);
			if (isNE(incrbk1IO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(incrbk1IO.getParams());
				fatalError9000();
			}
			else {
				incrbk1IO.setStatuz(varcom.endp);
			}
		}
	}

protected void moveIncrValsToTable310()
	{
		/*READ*/
		wsaaAmountIn[1].set(incrbk1IO.getNewinst());
		wsaaAmountIn[2].set(incrbk1IO.getLastInst());
		wsaaAmountIn[3].set(incrbk1IO.getOrigInst());
		wsaaAmountIn[4].set(incrbk1IO.getNewsum());
		wsaaAmountIn[5].set(incrbk1IO.getLastSum());
		wsaaAmountIn[6].set(incrbk1IO.getOrigSum());
		/*EXIT*/
	}

protected void moveIncrValsFromTable320()
	{
		/*GO*/
		incrbk1IO.setNewinst(wsaaAmountOut[1]);
		incrbk1IO.setLastInst(wsaaAmountOut[2]);
		incrbk1IO.setOrigInst(wsaaAmountOut[3]);
		incrbk1IO.setNewsum(wsaaAmountOut[4]);
		incrbk1IO.setLastSum(wsaaAmountOut[5]);
		incrbk1IO.setOrigSum(wsaaAmountOut[6]);
		/*EXIT*/
	}

protected void writeBrkIncr330()
	{
		go331();
	}

protected void go331()
	{
		incrbk2IO.setChdrcoy(incrbk1IO.getChdrcoy());
		incrbk2IO.setChdrnum(incrbk1IO.getChdrnum());
		incrbk2IO.setPlanSuffix(incrbk1IO.getPlanSuffix());
		incrbk2IO.setLife(incrbk1IO.getLife());
		incrbk2IO.setCoverage(incrbk1IO.getCoverage());
		incrbk2IO.setRider(incrbk1IO.getRider());
		incrbk2IO.setPlanSuffix(wsaaPlanSuffix);
		incrbk2IO.setNonKey(incrbk1IO.getNonKey());
		incrbk2IO.setFormat(formatsInner.incrbk2rec);
		incrbk2IO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrbk2IO);
		if (isNE(incrbk2IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrbk2IO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecIncr340()
	{
		entry341();
	}

protected void entry341()
	{
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			deleteSummaryIncr350();
		}
		/*  Calculate the values of the summarised record or the last polic*/
		/*  record. Calculate the new values by subtracting the accumulated*/
		/*  amounts from the previous summarised values.                   */
		setPrecision(incrbk1IO.getNewinst(), 2);
		incrbk1IO.setNewinst(sub(wsaaAmountIn[1], wsaaTotAmount[1]));
		setPrecision(incrbk1IO.getLastInst(), 2);
		incrbk1IO.setLastInst(sub(wsaaAmountIn[2], wsaaTotAmount[2]));
		setPrecision(incrbk1IO.getOrigInst(), 2);
		incrbk1IO.setOrigInst(sub(wsaaAmountIn[3], wsaaTotAmount[3]));
		setPrecision(incrbk1IO.getNewsum(), 2);
		incrbk1IO.setNewsum(sub(wsaaAmountIn[4], wsaaTotAmount[4]));
		setPrecision(incrbk1IO.getLastSum(), 2);
		incrbk1IO.setLastSum(sub(wsaaAmountIn[5], wsaaTotAmount[5]));
		setPrecision(incrbk1IO.getOrigSum(), 2);
		incrbk1IO.setOrigSum(sub(wsaaAmountIn[6], wsaaTotAmount[6]));
		/* Write the last policy.                                          */
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkIncr330();
			return ;
		}
		/* To get here there are still records summarised so rewrite the   */
		/* the record.                                                     */
		incrbk1IO.setFormat(formatsInner.incrbk1rec);
		incrbk1IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, incrbk1IO);
		if (isNE(incrbk1IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrbk1IO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryIncr350()
	{
		/*ENTRY*/
		incrbk1IO.setFunction(varcom.delet);
		incrbk1IO.setFormat(formatsInner.incrbk1rec);
		SmartFileCode.execute(appVars, incrbk1IO);
		if (isNE(incrbk1IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrbk1IO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void mainProcessingAcmv400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go401();
				case nextr405: 
					nextr405();
				case exit409: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go401()
	{
		if (isEQ(wsaaAcmvNoRecs, 1)) {
			acmvbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit409);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			wsaaTotAmount[wsaaSub.toInt()].set(ZERO);
		}
		moveAcmvValsToTable410();
		for (wsaaPlanSuffix.set(brkoutrec.brkOldSummary); !(isEQ(wsaaPlanSuffix, brkoutrec.brkNewSummary)); wsaaPlanSuffix.add(-1)){
			for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
				calcAmts2000();
			}
			moveAcmvValsFromTable420();
			writeBrkAcmv430();
		}
		calcNewSummaryRecAcmv440();
	}

protected void nextr405()
	{
		acmvbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, acmvbrkIO);
		if (isEQ(acmvbrkIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(acmvbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvbrkIO.getParams());
			fatalError9000();
		}
		wsaaRldgacct.set(acmvbrkIO.getRldgacct());
		if (isNE(acmvbrkIO.getRldgcoy(), brkoutrec.brkChdrcoy)
		|| isNE(wsaaRldgChdrnum, brkoutrec.brkChdrnum)
		|| isNE(wsaaRldgPlnsfx, ZERO)) {
			acmvbrkIO.setFunction(varcom.rewrt);
			acmvbrkIO.setFormat(formatsInner.acmvbrkrec);
			SmartFileCode.execute(appVars, acmvbrkIO);
			if (isNE(acmvbrkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(acmvbrkIO.getParams());
				fatalError9000();
			}
		}
		if (isNE(acmvbrkIO.getRldgcoy(), brkoutrec.brkChdrcoy)
		|| isNE(wsaaRldgChdrnum, brkoutrec.brkChdrnum)) {
			acmvbrkIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(acmvbrkIO.getRldgcoy(), brkoutrec.brkChdrcoy)
		&& isEQ(wsaaRldgChdrnum, brkoutrec.brkChdrnum)
		&& isNE(wsaaRldgPlnsfx, ZERO)) {
			goTo(GotoLabel.nextr405);
		}
	}

protected void moveAcmvValsToTable410()
	{
		/*GO*/
		wsaaAmountIn[1].set(acmvbrkIO.getOrigamt());
		wsaaAmountIn[2].set(acmvbrkIO.getAcctamt());
		wsaaAmountIn[3].set(acmvbrkIO.getRcamt());
		/*EXIT*/
	}

protected void moveAcmvValsFromTable420()
	{
		/*ENTRY*/
		acmvbrkIO.setOrigamt(wsaaAmountOut[1]);
		acmvbrkIO.setAcctamt(wsaaAmountOut[2]);
		acmvbrkIO.setRcamt(wsaaAmountOut[3]);
		/*EXIT*/
	}

protected void writeBrkAcmv430()
	{
		entry431();
	}

protected void entry431()
	{
		/*  move key values from ACMVBRK to ACMV in order to write the*/
		/*  detail ACMV records*/
		acmvbk2IO.setRldgcoy(acmvbrkIO.getRldgcoy());
		acmvbk2IO.setRldgacct(acmvbrkIO.getRldgacct());
		wsaaRldgacct.set(acmvbk2IO.getRldgacct());
		wsaaPlan.set(wsaaPlanSuffix);
		wsaaRldgPlnsfx.set(wsaaPlansuff);
		acmvbk2IO.setRldgacct(wsaaRldgacct);
		acmvbk2IO.setNonKey(acmvbrkIO.getNonKey());
		acmvbk2IO.setFormat(formatsInner.acmvbk2rec);
		acmvbk2IO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, acmvbk2IO);
		if (isNE(acmvbk2IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvbk2IO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecAcmv440()
	{
		entry441();
	}

protected void entry441()
	{
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			deleteSummaryAcmv450();
		}
		/*  Calculate the values of the summarised record or the last polic*/
		/*  record. Calculate the new values by subtracting the accumulated*/
		/*  amounts from the previous summarised values.*/
		setPrecision(acmvbrkIO.getOrigamt(), 2);
		acmvbrkIO.setOrigamt(sub(wsaaAmountIn[1], wsaaTotAmount[1]));
		setPrecision(acmvbrkIO.getAcctamt(), 2);
		acmvbrkIO.setAcctamt(sub(wsaaAmountIn[2], wsaaTotAmount[2]));
		setPrecision(acmvbrkIO.getRcamt(), 2);
		acmvbrkIO.setRcamt(sub(wsaaAmountIn[3], wsaaTotAmount[3]));
		/* Write the last policy.*/
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkAcmv430();
			return ;
		}
		/* To get here there are still records summarised so rewrite the*/
		/* the record.*/
		acmvbrkIO.setFormat(formatsInner.acmvbrkrec);
		acmvbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, acmvbrkIO);
		if (isNE(acmvbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvbrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryAcmv450()
	{
		/*ENTRY*/
		acmvbrkIO.setFunction(varcom.delet);
		acmvbrkIO.setFormat(formatsInner.acmvbrkrec);
		SmartFileCode.execute(appVars, acmvbrkIO);
		if (isNE(acmvbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvbrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void mainProcessingAcbl500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go501();
				case nextr505: 
					nextr505();
				case exit509: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go501()
	{
		if (isEQ(wsaaAcblNoRecs, 1)) {
			acblbk3IO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit509);
		}
		wsaaTotSacscurbal.set(ZERO);
		compute(wsaaBrkPlnSuffix, 0).set(add(brkoutrec.brkNewSummary, 1));
		for (wsaaPlanSuffix.set(wsaaBrkPlnSuffix); !(isGT(wsaaPlanSuffix, brkoutrec.brkOldSummary)); wsaaPlanSuffix.add(1)){
			calcWriteAcbl510();
		}
		calcNewSummaryRecAcbl520();
	}

protected void nextr505()
	{
		acblbk3IO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, acblbk3IO);
		if (isEQ(acblbk3IO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(acblbk3IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acblbk3IO.getParams());
			fatalError9000();
		}
		wsaaRldgacct.set(acblbk3IO.getRldgacct());
		if (isNE(acblbk3IO.getRldgcoy(), brkoutrec.brkChdrcoy)
		|| isNE(wsaaRldgChdrnum, brkoutrec.brkChdrnum)
		|| isNE(wsaaRldgPlnsfx, ZERO)) {
			acblbk3IO.setFunction(varcom.rewrt);
			acblbk3IO.setFormat(formatsInner.acblbk3rec);
			SmartFileCode.execute(appVars, acblbk3IO);
			if (isNE(acblbk3IO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(acblbk3IO.getParams());
				fatalError9000();
			}
		}
		if (isNE(acblbk3IO.getRldgcoy(), brkoutrec.brkChdrcoy)
		|| isNE(wsaaRldgChdrnum, brkoutrec.brkChdrnum)) {
			acblbk3IO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(acblbk3IO.getRldgcoy(), brkoutrec.brkChdrcoy)
		&& isEQ(wsaaRldgChdrnum, brkoutrec.brkChdrnum)
		&& isNE(wsaaRldgPlnsfx, ZERO)) {
			goTo(GotoLabel.nextr505);
		}
	}

protected void calcWriteAcbl510()
	{
		entry511();
	}

protected void entry511()
	{
		wsaaRldgacct.set(SPACES);
		wsaaRldgacct.set(acblbk3IO.getRldgacct());
		wsaaPlan.set(wsaaPlanSuffix);
		wsaaRldgPlnsfx.set(wsaaPlansuff);
		acmvbk3IO.setDataKey(SPACES);
		acmvbk3IO.setRldgcoy(acblbk3IO.getRldgcoy());
		acmvbk3IO.setRldgacct(wsaaRldgacct);
		acmvbk3IO.setSacscode(acblbk3IO.getSacscode());
		acmvbk3IO.setSacstyp(acblbk3IO.getSacstyp());
		acmvbk3IO.setOrigcurr(acblbk3IO.getOrigcurr());
		acmvbk3IO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, acmvbk3IO);
		if (isNE(acmvbk3IO.getStatuz(), varcom.oK)
		&& isNE(acmvbk3IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvbk3IO.getParams());
			syserrrec.statuz.set(acmvbk3IO.getStatuz());
			fatalError9000();
		}
		wsaaSacscurbal.set(ZERO);
		while ( !(isNE(acmvbk3IO.getRldgcoy(), acblbk3IO.getRldgcoy())
		|| isNE(acmvbk3IO.getRldgacct(), wsaaRldgacct)
		|| isNE(acmvbk3IO.getSacscode(), acblbk3IO.getSacscode())
		|| isNE(acmvbk3IO.getSacstyp(), acblbk3IO.getSacstyp())
		|| isNE(acmvbk3IO.getOrigcurr(), acblbk3IO.getOrigcurr())
		|| isEQ(acmvbk3IO.getStatuz(), varcom.endp))) {
			calcSacscurbal530();
		}
		
		if (isNE(wsaaSacscurbal, ZERO)) {
			writeBrkAcbl540();
		}
	}

protected void calcNewSummaryRecAcbl520()
	{
		entry521();
	}

protected void entry521()
	{
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			deleteSummaryAcbl550();
			wsaaPlanSuffix.set(1);
			calcWriteAcbl510();
			return ;
		}
		if (isNE(wsaaTotSacscurbal, ZERO)) {
			setPrecision(acblbk3IO.getSacscurbal(), 2);
			acblbk3IO.setSacscurbal(sub(acblbk3IO.getSacscurbal(), wsaaTotSacscurbal));
		}
		acblbk3IO.setFormat(formatsInner.acblbk3rec);
		acblbk3IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, acblbk3IO);
		if (isNE(acblbk3IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acblbk3IO.getParams());
			fatalError9000();
		}
	}

protected void calcSacscurbal530()
	{
		entry531();
	}

protected void entry531()
	{
		if (isEQ(acmvbk3IO.getGlsign(), "-")) {
			setPrecision(acmvbk3IO.getOrigamt(), 2);
			acmvbk3IO.setOrigamt(mult(acmvbk3IO.getOrigamt(), -1));
		}
		compute(wsaaSacscurbal, 2).set(add(wsaaSacscurbal, acmvbk3IO.getOrigamt()));
		/*  Its not possible to create a logical view on the components*/
		/*  values of RLDGACCT. To overcome this read the next record that*/
		/*  matches the original key.*/
		acmvbk3IO.setRldgcoy(acblbk3IO.getRldgcoy());
		acmvbk3IO.setRldgacct(wsaaRldgacct);
		acmvbk3IO.setSacscode(acblbk3IO.getSacscode());
		acmvbk3IO.setSacstyp(acblbk3IO.getSacstyp());
		acmvbk3IO.setOrigcurr(acblbk3IO.getOrigcurr());
		acmvbk3IO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, acmvbk3IO);
		if (isNE(acmvbk3IO.getStatuz(), varcom.oK)
		&& isNE(acmvbk3IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvbk3IO.getParams());
			syserrrec.statuz.set(acmvbk3IO.getStatuz());
			fatalError9000();
		}
	}

protected void writeBrkAcbl540()
	{
		entry541();
	}

protected void entry541()
	{
		compute(wsaaTotSacscurbal, 2).set(add(wsaaTotSacscurbal, wsaaSacscurbal));
		acblbk2IO.setSacscurbal(wsaaSacscurbal);
		acblbk2IO.setRldgcoy(acblbk3IO.getRldgcoy());
		acblbk2IO.setRldgacct(wsaaRldgacct);
		acblbk2IO.setSacscode(acblbk3IO.getSacscode());
		acblbk2IO.setSacstyp(acblbk3IO.getSacstyp());
		acblbk2IO.setOrigcurr(acblbk3IO.getOrigcurr());
		acblbk2IO.setFunction(varcom.writr);
		acblbk2IO.setFormat(formatsInner.acblbk2rec);
		SmartFileCode.execute(appVars, acblbk2IO);
		if (isNE(acblbk2IO.getStatuz(), varcom.oK)
		&& isNE(acblbk2IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acblbk2IO.getParams());
			syserrrec.statuz.set(acblbk2IO.getStatuz());
			fatalError9000();
		}
	}

protected void deleteSummaryAcbl550()
	{
		/*ENTRY*/
		acblbk3IO.setFunction(varcom.delet);
		acblbk3IO.setFormat(formatsInner.acblbk3rec);
		SmartFileCode.execute(appVars, acblbk3IO);
		if (isNE(acblbk3IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acblbk3IO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void mainProcessingAgcm600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go601();
				case nextr605: 
					nextr605();
				case exit609: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go601()
	{
		if (isEQ(wsaaAgcmNoRecs, 1)) {
			agcmbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit609);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			wsaaTotAmount[wsaaSub.toInt()].set(ZERO);
		}
		moveAgcmValsToTable610();
		for (wsaaPlanSuffix.set(brkoutrec.brkOldSummary); !(isEQ(wsaaPlanSuffix, brkoutrec.brkNewSummary)); wsaaPlanSuffix.add(-1)){
			for (wsaaSub.set(1); !(isGT(wsaaSub, 8)); wsaaSub.add(1)){
				calcAmts2000();
			}
			moveAgcmValsFromTable620();
			writeBrkAgcm630();
		}
		calcNewSummaryRecAgcm640();
	}

protected void nextr605()
	{
		agcmbrkIO.setFunction(varcom.nextr);
		FixedLengthStringData groupTEMP = agcmbrkIO.getParams();
		callProgram(agcmbrkIO.getIo(), groupTEMP);
		agcmbrkIO.setParams(groupTEMP);
		/* IF AGCMBRK-STATUZ      NOT = O-K                             */
		if (isNE(agcmbrkIO.getStatuz(), varcom.oK)
		&& isNE(agcmbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbrkIO.getParams());
			fatalError9000();
		}
		if (isEQ(agcmbrkIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(agcmbrkIO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(agcmbrkIO.getChdrcoy(), brkoutrec.brkChdrcoy)
		|| isNE(agcmbrkIO.getPlanSuffix(), ZERO)) {
			agcmbrkIO.setFunction(varcom.rewrt);
			agcmbrkIO.setFormat(formatsInner.agcmbrkrec);
			SmartFileCode.execute(appVars, agcmbrkIO);
			if (isNE(agcmbrkIO.getStatuz(), "****")) {
				syserrrec.params.set(agcmbrkIO.getParams());
				fatalError9000();
			}
		}
		if (isNE(agcmbrkIO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(agcmbrkIO.getChdrcoy(), brkoutrec.brkChdrcoy)) {
			agcmbrkIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(agcmbrkIO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(agcmbrkIO.getChdrcoy(), brkoutrec.brkChdrcoy)
		|| isNE(agcmbrkIO.getPlanSuffix(), ZERO)) {
			goTo(GotoLabel.nextr605);
		}
	}

protected void moveAgcmValsToTable610()
	{
		/*GO*/
		wsaaAmountIn[1].set(agcmbrkIO.getAnnprem());
		wsaaAmountIn[2].set(agcmbrkIO.getInitcom());
		wsaaAmountIn[3].set(agcmbrkIO.getCompay());
		wsaaAmountIn[4].set(agcmbrkIO.getComern());
		wsaaAmountIn[5].set(agcmbrkIO.getScmdue());
		wsaaAmountIn[6].set(agcmbrkIO.getScmearn());
		wsaaAmountIn[7].set(agcmbrkIO.getRnlcdue());
		wsaaAmountIn[8].set(agcmbrkIO.getRnlcearn());
		/*EXIT*/
	}

protected void moveAgcmValsFromTable620()
	{
		/*GO*/
		agcmbrkIO.setAnnprem(wsaaAmountOut[1]);
		agcmbrkIO.setInitcom(wsaaAmountOut[2]);
		agcmbrkIO.setCompay(wsaaAmountOut[3]);
		agcmbrkIO.setComern(wsaaAmountOut[4]);
		agcmbrkIO.setScmdue(wsaaAmountOut[5]);
		agcmbrkIO.setScmearn(wsaaAmountOut[6]);
		agcmbrkIO.setRnlcdue(wsaaAmountOut[7]);
		agcmbrkIO.setRnlcearn(wsaaAmountOut[8]);
		/*EXIT*/
	}

protected void writeBrkAgcm630()
	{
		go631();
	}

protected void go631()
	{
		agcmIO.setParams(SPACES);
		agcmIO.setChdrcoy(agcmbrkIO.getChdrcoy());
		agcmIO.setChdrnum(agcmbrkIO.getChdrnum());
		agcmIO.setAgntnum(agcmbrkIO.getAgntnum());
		agcmIO.setPlanSuffix(agcmbrkIO.getPlanSuffix());
		agcmIO.setLife(agcmbrkIO.getLife());
		agcmIO.setCoverage(agcmbrkIO.getCoverage());
		agcmIO.setRider(agcmbrkIO.getRider());
		agcmIO.setPlanSuffix(wsaaPlanSuffix);
		agcmIO.setNonKey(agcmbrkIO.getNonKey());
		agcmIO.setFormat(formatsInner.agcmrec);
		agcmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmIO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecAgcm640()
	{
		go641();
	}

protected void go641()
	{
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			deleteSummaryAgcm650();
		}
		/*  Calculate the values of the summarised record or the last polic*/
		/*  record. Calculate the new values by subtracting the accumulated*/
		/*  amounts from the previous summarised values.*/
		setPrecision(agcmbrkIO.getAnnprem(), 2);
		agcmbrkIO.setAnnprem(sub(wsaaAmountIn[1], wsaaTotAmount[1]));
		setPrecision(agcmbrkIO.getInitcom(), 2);
		agcmbrkIO.setInitcom(sub(wsaaAmountIn[2], wsaaTotAmount[2]));
		setPrecision(agcmbrkIO.getCompay(), 2);
		agcmbrkIO.setCompay(sub(wsaaAmountIn[3], wsaaTotAmount[3]));
		setPrecision(agcmbrkIO.getComern(), 2);
		agcmbrkIO.setComern(sub(wsaaAmountIn[4], wsaaTotAmount[4]));
		setPrecision(agcmbrkIO.getScmdue(), 2);
		agcmbrkIO.setScmdue(sub(wsaaAmountIn[5], wsaaTotAmount[5]));
		setPrecision(agcmbrkIO.getScmearn(), 2);
		agcmbrkIO.setScmearn(sub(wsaaAmountIn[6], wsaaTotAmount[6]));
		setPrecision(agcmbrkIO.getRnlcdue(), 2);
		agcmbrkIO.setRnlcdue(sub(wsaaAmountIn[7], wsaaTotAmount[7]));
		setPrecision(agcmbrkIO.getRnlcearn(), 2);
		agcmbrkIO.setRnlcearn(sub(wsaaAmountIn[8], wsaaTotAmount[8]));
		/* Write the last policy.*/
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkAgcm630();
			return ;
		}
		/* To get here there are still records summarised so rewrite the*/
		/* the record.*/
		agcmbrkIO.setFormat(formatsInner.agcmbrkrec);
		agcmbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agcmbrkIO);
		if (isNE(agcmbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryAgcm650()
	{
		/*GO*/
		agcmbrkIO.setFunction(varcom.delet);
		agcmbrkIO.setFormat(formatsInner.agcmbrkrec);
		SmartFileCode.execute(appVars, agcmbrkIO);
		if (isNE(agcmbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	****                                                      <LA4407>
	*************************************                     <LA4407>
	*700-MAIN-PROCESSING-ACMC    SECTION.                     <LA4407>
	*************************************                     <LA4407>
	*701-GO.                                                  <LA4407>
	****                                                      <LA4407>
	**** IF WSAA-ACMC-NO-RECS = 1                             <LA4407>
	****    MOVE ENDP   TO ACMCBRK-STATUZ                     <LA4407>
	****    GO TO 709-EXIT.                                   <LA4407>
	****                                                      <LA4407>
	**** PERFORM VARYING WSAA-SUB FROM 1 BY 1 UNTIL WSAA-SUB > 17     
	****    MOVE ZEROES                 TO WSAA-TOT-AMOUNT(WSAA-SUB)  
	**** END-PERFORM.                                         <LA4407>
	****                                                      <LA4407>
	**** PERFORM 710-MOVE-ACMC-VALS-TO-TABLE.                 <LA4407>
	****                                                      <LA4407>
	**** PERFORM VARYING WSAA-PLAN-SUFFIX FROM BRK-OLD-SUMMARY BY -1  
	****    UNTIL WSAA-PLAN-SUFFIX = BRK-NEW-SUMMARY          <LA4407>
	****                                                      <LA4407>
	****     PERFORM 2000-CALC-AMTS VARYING WSAA-SUB          <LA4407>
	****        FROM 1 BY 1 UNTIL WSAA-SUB > 3                <LA4407>
	****     PERFORM 720-MOVE-ACMC-VALS-FROM-TABLE            <LA4407>
	****     PERFORM 730-WRITE-BRK-ACMC                       <LA4407>
	****                                                      <LA4407>
	**** END-PERFORM.                                         <LA4407>
	****                                                      <LA4407>
	**** PERFORM 740-CALC-NEW-SUMMARY-REC-ACMC.               <LA4407>
	****                                                      <LA4407>
	*705-NEXTR.                                               <LA4407>
	****                                                      <LA4407>
	**** MOVE NEXTR             TO ACMCBRK-FUNCTION.          <LA4407>
	**** CALL 'ACMCBRKIO'       USING ACMCBRK-PARAMS.         <LA4407>
	****                                                      <LA4407>
	**** IF ACMCBRK-STATUZ          = ENDP                    <LA4407>
	****    GO TO 709-EXIT.                                   <LA4407>
	****                                                      <LA4407>
	**** IF ACMCBRK-STATUZ      NOT = O-K                     <LA4407>
	****     MOVE ACMCBRK-PARAMS TO SYSR-PARAMS               <LA4407>
	****     PERFORM 9000-FATAL-ERROR.                        <LA4407>
	****                                                      <LA4407>
	**** MOVE ACMCBRK-RLDGACCT          TO WSAA-RLDGACCT.     <LA4407>
	**** IF ACMCBRK-RLDGCOY             NOT =  BRK-CHDRCOY OR <LA4407>
	****    WSAA-RLDG-CHDRNUM           NOT =  BRK-CHDRNUM OR <LA4407>
	****    WSAA-RLDG-PLNSFX            NOT =  ZERO           <LA4407>
	****    MOVE REWRT                  TO ACMCBRK-FUNCTION   <LA4407>
	****    MOVE ACMCBRKREC             TO ACMCBRK-FORMAT     <LA4407>
	****    CALL 'ACMCBRKIO'            USING ACMCBRK-PARAMS  <LA4407>
	****    IF ACMCBRK-STATUZ           NOT = O-K             <LA4407>
	****        MOVE ACMCBRK-PARAMS         TO SYSR-PARAMS    <LA4407>
	****        PERFORM 9000-FATAL-ERROR.                     <LA4407>
	****                                                      <LA4407>
	**** IF ACMCBRK-RLDGCOY             NOT =  BRK-CHDRCOY OR <LA4407>
	****    WSAA-RLDG-CHDRNUM           NOT =  BRK-CHDRNUM    <LA4407>
	****    MOVE ENDP                   TO ACMCBRK-STATUZ     <LA4407>
	****    GO TO 709-EXIT.                                   <LA4407>
	****                                                      <LA4407>
	**** IF  ACMCBRK-RLDGCOY            =  BRK-CHDRCOY        <LA4407>
	**** AND WSAA-RLDG-CHDRNUM          =  BRK-CHDRNUM        <LA4407>
	**** AND WSAA-RLDG-PLNSFX           NOT =  ZERO           <LA4407>
	****     GO TO 705-NEXTR.                                 <LA4407>
	****                                                      <LA4407>
	*709-EXIT.                                                <LA4407>
	**** EXIT.                                                <LA4407>
	****                                                      <LA4407>
	**************************************                    <LA4407>
	*710-MOVE-ACMC-VALS-TO-TABLE   SECTION.                   <LA4407>
	**************************************                    <LA4407>
	*711-GO.                                                  <LA4407>
	****                                                      <LA4407>
	**** MOVE ACMCBRK-ORIGAMT        TO WSAA-AMOUNT-IN(1).    <LA4407>
	**** MOVE ACMCBRK-ACCTAMT        TO WSAA-AMOUNT-IN(2).    <LA4407>
	****                                                      <LA4407>
	*719-EXIT.                                                <LA4407>
	**** EXIT.                                                <LA4407>
	****                                                      <LA4407>
	****                                                      <LA4407>
	**************************************                    <LA4407>
	*720-MOVE-ACMC-VALS-FROM-TABLE SECTION.                   <LA4407>
	**************************************                    <LA4407>
	*721-ENTRY.                                               <LA4407>
	****                                                      <LA4407>
	**** MOVE WSAA-AMOUNT-OUT(1)     TO  ACMCBRK-ORIGAMT.     <LA4407>
	**** MOVE WSAA-AMOUNT-OUT(2)     TO  ACMCBRK-ACCTAMT.     <LA4407>
	****                                                      <LA4407>
	*729-EXIT.                                                <LA4407>
	**** EXIT.                                                <LA4407>
	****                                                      <LA4407>
	**************************************                    <LA4407>
	*730-WRITE-BRK-ACMC           SECTION.                    <LA4407>
	**************************************                    <LA4407>
	*731-ENTRY.                                               <LA4407>
	****                                                      <LA4407>
	***move key values from ACMCBRK to ACMC in order to write the     
	***detail ACMC records                                    <LA4407>
	****                                                      <LA4407>
	**** MOVE ACMCBRK-RLDGCOY        TO ACMCBK2-RLDGCOY.      <LA4407>
	**** MOVE ACMCBRK-RLDGACCT       TO ACMCBK2-RLDGACCT.     <LA4407>
	****                                                      <LA4407>
	**** MOVE ACMCBK2-RLDGACCT       TO WSAA-RLDGACCT.        <LA4407>
	**** MOVE WSAA-PLAN-SUFFIX       TO WSAA-PLAN.            <LA4407>
	**** MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLNSFX.     <LA4407>
	**** MOVE WSAA-RLDGACCT          TO ACMCBK2-RLDGACCT.     <LA4407>
	**** MOVE ACMCBRK-NON-KEY        TO ACMCBK2-NON-KEY.      <LA4407>
	****                                                      <LA4407>
	**** MOVE ACMCBK2REC             TO ACMCBK2-FORMAT.       <LA4407>
	****                                                      <LA4407>
	**** MOVE WRITR                  TO ACMCBK2-FUNCTION.     <LA4407>
	**** CALL 'ACMCBK2IO'               USING ACMCBK2-PARAMS. <LA4407>
	**** IF ACMCBK2-STATUZ           NOT = O-K                <LA4407>
	****     MOVE ACMCBK2-PARAMS     TO SYSR-PARAMS           <LA4407>
	****     PERFORM                 9000-FATAL-ERROR.        <LA4407>
	****                                                      <LA4407>
	*739-EXIT.                                                <LA4407>
	**** EXIT.                                                <LA4407>
	****                                                      <LA4407>
	**************************************                    <LA4407>
	*740-CALC-NEW-SUMMARY-REC-ACMC SECTION.                   <LA4407>
	**************************************                    <LA4407>
	*741-ENTRY.                                               <LA4407>
	****                                                      <LA4407>
	**** IF BRK-NEW-SUMMARY          < 2                      <LA4407>
	****     PERFORM 750-DELETE-SUMMARY-ACMC                  <LA4407>
	**** END-IF.                                              <LA4407>
	****                                                      <LA4407>
	***Calculate the values of the summarised record or the last polic
	***record. Calculate the new values by subtracting the accumulated
	***amounts from the previous summarised values.           <LA4407>
	****                                                      <LA4407>
	**** COMPUTE ACMCBRK-ORIGAMT     = WSAA-AMOUNT-IN(1) -    <LA4407>
	****                               WSAA-TOT-AMOUNT(1).    <LA4407>
	**** COMPUTE ACMCBRK-ACCTAMT     = WSAA-AMOUNT-IN(2) -    <LA4407>
	****                               WSAA-TOT-AMOUNT(2).    <LA4407>
	****                                                      <LA4407>
	**Write the last policy.                                  <LA4407>
	****                                                      <LA4407>
	**** IF BRK-NEW-SUMMARY          < 2                      <LA4407>
	****    MOVE 1                   TO WSAA-PLAN-SUFFIX      <LA4407>
	****    PERFORM 730-WRITE-BRK-ACMC                        <LA4407>
	****    GO TO 749-EXIT                                    <LA4407>
	**** END-IF.                                              <LA4407>
	****                                                      <LA4407>
	**To get here there are still records summarised so rewrite the   
	**the record.                                             <LA4407>
	****                                                      <LA4407>
	**** MOVE ACMCBRKREC             TO ACMCBRK-FORMAT.       <LA4407>
	**** MOVE REWRT                  TO ACMCBRK-FUNCTION.     <LA4407>
	**** CALL 'ACMCBRKIO'            USING ACMCBRK-PARAMS.    <LA4407>
	**** IF ACMCBRK-STATUZ           NOT = O-K                <LA4407>
	****     MOVE ACMCBRK-PARAMS     TO SYSR-PARAMS           <LA4407>
	****     PERFORM                 9000-FATAL-ERROR.        <LA4407>
	****                                                      <LA4407>
	*749-EXIT.                                                <LA4407>
	**** EXIT.                                                <LA4407>
	****                                                      <LA4407>
	********************************                          <LA4407>
	*750-DELETE-SUMMARY-ACMC SECTION.                         <LA4407>
	********************************                          <LA4407>
	*751-ENTRY.                                               <LA4407>
	****                                                      <LA4407>
	**** MOVE DELET             TO ACMCBRK-FUNCTION.          <LA4407>
	**** MOVE ACMCBRKREC        TO ACMCBRK-FORMAT.            <LA4407>
	**** CALL 'ACMCBRKIO'       USING ACMCBRK-PARAMS.         <LA4407>
	**** IF ACMCBRK-STATUZ      NOT = O-K                     <LA4407>
	****     MOVE ACMCBRK-PARAMS TO SYSR-PARAMS               <LA4407>
	****     PERFORM 9000-FATAL-ERROR.                        <LA4407>
	****                                                      <LA4407>
	*759-EXIT.                                                <LA4407>
	**** EXIT.                                                <LA4407>
	* </pre>
	*/
protected void callSubprog990()
	{
		/*GO*/
		genoutrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.subprog[wsaaGenericSub.toInt()], SPACES)) {
			callProgram(t5671rec.subprog[wsaaGenericSub.toInt()], genoutrec.outRec);
		}
		if (isNE(genoutrec.statuz, varcom.oK)) {
			syserrrec.params.set(genoutrec.outRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void updateContractHeader1000()
	{
		go1010();
	}

protected void go1010()
	{
		chdrsurIO.setDataArea(SPACES);
		chdrsurIO.setChdrnum(brkoutrec.brkChdrnum);
		chdrsurIO.setChdrcoy(brkoutrec.brkChdrcoy);
		chdrsurIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)
		&& isNE(chdrsurIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError9000();
		}
		if (isNE(chdrsurIO.getChdrnum(), brkoutrec.brkChdrnum)
		|| isNE(chdrsurIO.getChdrcoy(), brkoutrec.brkChdrcoy)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError9000();
		}
		chdrsurIO.setFormat(formatsInner.chdrsurrec);
		chdrsurIO.setFunction(varcom.rewrt);
		if (isLT(brkoutrec.brkNewSummary, 2)) {
			chdrsurIO.setPolsum(ZERO);
		}
		else {
			chdrsurIO.setPolsum(brkoutrec.brkNewSummary);
		}
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError9000();
		}
	}

protected void calcAmts2000()
	{
		/*ENTRY*/
		if (isEQ(wsaaAmountIn[wsaaSub.toInt()], ZERO)) {
			wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
			return ;
		}
		compute(wsaaAmountOut[wsaaSub.toInt()], 3).setRounded((div(wsaaAmountIn[wsaaSub.toInt()], brkoutrec.brkOldSummary)));
		zrdecplrec.amountIn.set(wsaaAmountOut[wsaaSub.toInt()]);
		callRounding8000();
		wsaaAmountOut[wsaaSub.toInt()].set(zrdecplrec.amountOut);
		compute(wsaaTotAmount[wsaaSub.toInt()], 2).set(add(wsaaTotAmount[wsaaSub.toInt()], wsaaAmountOut[wsaaSub.toInt()]));
		/*EXIT*/
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(brkoutrec.brkChdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(covrbrkIO.getSicurr());
		zrdecplrec.batctrcde.set(brkoutrec.brkBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		brkoutrec.brkStatuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData agcmrec = new FixedLengthStringData(10).init("AGCMREC");
	private FixedLengthStringData covrbrkrec = new FixedLengthStringData(10).init("COVRBRKREC");
	private FixedLengthStringData agcmbrkrec = new FixedLengthStringData(10).init("AGCMBRKREC");
	private FixedLengthStringData chdrsurrec = new FixedLengthStringData(10).init("CHDRSURREC");
	private FixedLengthStringData acmvbrkrec = new FixedLengthStringData(10).init("ACMVBRKREC");
	private FixedLengthStringData acmvbk2rec = new FixedLengthStringData(10).init("ACMVBK2REC");
	private FixedLengthStringData acblbk2rec = new FixedLengthStringData(10).init("ACBLBK2REC");
	private FixedLengthStringData acblbk3rec = new FixedLengthStringData(10).init("ACBLBK3REC");
	private FixedLengthStringData incrbk1rec = new FixedLengthStringData(10).init("INCRBK1REC");
	private FixedLengthStringData incrbk2rec = new FixedLengthStringData(10).init("INCRBK2REC");
}
}
