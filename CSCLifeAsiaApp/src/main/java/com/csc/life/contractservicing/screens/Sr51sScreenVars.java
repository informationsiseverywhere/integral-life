package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR51S
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr51sScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(506);
	public FixedLengthStringData dataFields = new FixedLengthStringData(234).isAPartOf(dataArea, 0);
	public FixedLengthStringData acdes = DD.acdes.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,30);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,32);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,72);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,149);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,157);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,158);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,166);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,213);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,223);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,231);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 234);
	public FixedLengthStringData acdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 302);
	public FixedLengthStringData[] acdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(459);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(121).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData cpstats = new FixedLengthStringData(4).isAPartOf(subfileFields, 2);
	public FixedLengthStringData[] cpstat = FLSArrayPartOfStructure(2, 2, cpstats, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(cpstats, 0, FILLER_REDEFINE);
	public FixedLengthStringData cpstat01 = DD.cpstat.copy().isAPartOf(filler,0);
	public FixedLengthStringData cpstat04 = DD.cpstat.copy().isAPartOf(filler,2);
	public FixedLengthStringData crstats = new FixedLengthStringData(4).isAPartOf(subfileFields, 6);
	public FixedLengthStringData[] crstat = FLSArrayPartOfStructure(2, 2, crstats, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(crstats, 0, FILLER_REDEFINE);
	public FixedLengthStringData crstat01 = DD.crstat.copy().isAPartOf(filler1,0);
	public FixedLengthStringData crstat04 = DD.crstat.copy().isAPartOf(filler1,2);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData crtabled = DD.crtabled.copy().isAPartOf(subfileFields,14);
	public FixedLengthStringData eror = DD.eror.copy().isAPartOf(subfileFields,44);
	public ZonedDecimalData instprem = DD.instprem.copyToZonedDecimal().isAPartOf(subfileFields,48);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData nonForfeitMethod = DD.nffeit.copy().isAPartOf(subfileFields,69);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(subfileFields,73);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(subfileFields,77);
	public FixedLengthStringData pumeth = DD.pumeth.copy().isAPartOf(subfileFields,79);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,83);
	public FixedLengthStringData sel = DD.sel.copy().isAPartOf(subfileFields,85);
	public FixedLengthStringData statcde = DD.statcde.copy().isAPartOf(subfileFields,86);
	public FixedLengthStringData premsubrs = new FixedLengthStringData(16).isAPartOf(subfileFields, 88);
	public FixedLengthStringData[] premsubr = FLSArrayPartOfStructure(2, 8, premsubrs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(premsubrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData premsubr01 = DD.subr.copy().isAPartOf(filler2,0);
	public FixedLengthStringData premsubr04 = DD.subr.copy().isAPartOf(filler2,8);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,104);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 121);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cpstatsErr = new FixedLengthStringData(8).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData[] cpstatErr = FLSArrayPartOfStructure(2, 4, cpstatsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(cpstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cpstat01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData cpstat04Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData crstatsErr = new FixedLengthStringData(8).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData[] crstatErr = FLSArrayPartOfStructure(2, 4, crstatsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(crstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crstat01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData crstat04Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData crtabledErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData erorErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData instpremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData nffeitErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData pumethErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData selErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData statcdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 68);
	public FixedLengthStringData subrsErr = new FixedLengthStringData(8).isAPartOf(errorSubfile, 72);
	public FixedLengthStringData[] subrErr = FLSArrayPartOfStructure(2, 4, subrsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(subrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData subr01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData subr04Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 80);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(252).isAPartOf(subfileArea, 205);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData cpstatsOut = new FixedLengthStringData(24).isAPartOf(outputSubfile, 12);
	public FixedLengthStringData[] cpstatOut = FLSArrayPartOfStructure(2, 12, cpstatsOut, 0);
	public FixedLengthStringData[][] cpstatO = FLSDArrayPartOfArrayStructure(12, 1, cpstatOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(cpstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cpstat01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] cpstat04Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData crstatsOut = new FixedLengthStringData(24).isAPartOf(outputSubfile, 36);
	public FixedLengthStringData[] crstatOut = FLSArrayPartOfStructure(2, 12, crstatsOut, 0);
	public FixedLengthStringData[][] crstatO = FLSDArrayPartOfArrayStructure(12, 1, crstatOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(24).isAPartOf(crstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crstat01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] crstat04Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] crtabledOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] erorOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] nffeitOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] pumethOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] selOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public FixedLengthStringData[] statcdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 204);
	public FixedLengthStringData subrsOut = new FixedLengthStringData(24).isAPartOf(outputSubfile, 216);
	public FixedLengthStringData[] subrOut = FLSArrayPartOfStructure(2, 12, subrsOut, 0);
	public FixedLengthStringData[][] subrO = FLSDArrayPartOfArrayStructure(12, 1, subrOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(subrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] subr01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] subr04Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 240);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 457);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr51sscreensflWritten = new LongData(0);
	public LongData Sr51sscreenctlWritten = new LongData(0);
	public LongData Sr51sscreenWritten = new LongData(0);
	public LongData Sr51sprotectWritten = new LongData(0);
	public GeneralTable sr51sscreensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr51sscreensfl;
	}

	public Sr51sScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(erorOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {nonForfeitMethod, pumeth, premsubr01, crstat01, cpstat01, premsubr04, planSuffix, crstat04, cpstat04, sel, life, jlife, coverage, rider, crtabled, crtable, statcde, pstatcode, eror, sumins, instprem};
		screenSflOutFields = new BaseData[][] {nffeitOut, pumethOut, subr01Out, crstat01Out, cpstat01Out, subr04Out, plnsfxOut, crstat04Out, cpstat04Out, selOut, lifeOut, jlifeOut, coverageOut, riderOut, crtabledOut, crtableOut, statcdeOut, pstatcodeOut, erorOut, suminsOut, instpremOut};
		screenSflErrFields = new BaseData[] {nffeitErr, pumethErr, subr01Err, crstat01Err, cpstat01Err, subr04Err, plnsfxErr, crstat04Err, cpstat04Err, selErr, lifeErr, jlifeErr, coverageErr, riderErr, crtabledErr, crtableErr, statcdeErr, pstatcodeErr, erorErr, suminsErr, instpremErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, register, chdrstatus, premstatus, cownnum, ownername, lifenum, lifename, occdate, billfreq, mop, ptdate, btdate, acdes};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, registerOut, chdrstatusOut, premstatusOut, cownnumOut, ownernameOut, lifenumOut, lifenameOut, occdateOut, billfreqOut, mopOut, ptdateOut, btdateOut, acdesOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, registerErr, chdrstatusErr, premstatusErr, cownnumErr, ownernameErr, lifenumErr, lifenameErr, occdateErr, billfreqErr, mopErr, ptdateErr, btdateErr, acdesErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr51sscreen.class;
		screenSflRecord = Sr51sscreensfl.class;
		screenCtlRecord = Sr51sscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr51sprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr51sscreenctl.lrec.pageSubfile);
	}
}
