/*
 * File: Grevunit.java
 * Date: 29 August 2009 22:50:59
 * Author: Quipoz Limited
 * 
 * Class transformed from GREVUNIT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.contractservicing.dataaccess.IncigrvTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnsurTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   Generalised Reversal Subroutine for UTRNs and INCIs
*   ---------------------------------------------------
*
*   This subroutine is normally called by REVCOLL reversal subroutine
*   via Table T5671. The parameters passed to this subroutine are
*   contained in the GREVERSREC copybook.
*
*   UTRN and INCI records are reversed by this process.
*
*   All the UTRN records with the transaction number of the transaction
*   being reversed are read and the values of premiums to be reversed
*   are totalled.
*   Each time that a UTRN is read for a different Component on
*   this Contract, all the relevant INCIs for that component are then
*   read. It is possible to have 'active' INCIs which correspond to
*   Components which are no longer having UTRNs written for them and
*   looping in this manner precludes such INCIs from being included in
*   the INCI total premium.
*
*   If the UTRN total does not equal a whole multiple of the INCI value
*   the program abends.
*
*   If the totals match then the INCI records and UTRN records are
*   reversed as follows:
*
*   INCI records.
*
*   The value of the current premium (CURR-PREM) for each INCI record
*   processed is added to the current amount of premium required for the
*   most recent period able to absorb the premium (CURR-PREM0n).
*   If only part of the premium can be absorbed by a given period, the
*   remainder is applied to the next most recent period.
*
*   If any premium is outstanding at the end of this process, the program
*   ends with an error.
*
*   UTRN records.
*
*   The processing for UTRN records depends on whether they have been
*   processed by UNITDEAL or not.
*
*   If the Feedback Indicator is set, a new negative copy of the UTRN
*   is written, otherwise the UTRN record is deleted.
*
*****************************************************************
* </pre>
*/
public class Grevunit extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("03");
	private String utrnrevrec = "UTRNREVREC";
	private String incigrvrec = "INCIGRVREC";
		/* ERRORS */
	private String t743 = "T743";
	private String t744 = "T744";
	private ZonedDecimalData wsaaTotalUtrnValue = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalInciValue = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaStoredValueToAdd = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaStoredDifference = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaNoOfPremiums = new ZonedDecimalData(11, 0).init(ZERO);
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(11, 0).init(ZERO);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private Greversrec greversrec = new Greversrec();
		/*Logical File Layout - Version 9305*/
	private IncigrvTableDAM incigrvIO = new IncigrvTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
		/*Unit transaction logical view for revers*/
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
		/*Utrn logical used for full surr reverse*/
	private UtrnsurTableDAM utrnsurIO = new UtrnsurTableDAM();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit000, 
		rewriteInci315, 
		end950
	}

	public Grevunit() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control000()
	{
		try {
			start000();
		}
		catch (GOTOException e){
		}
		finally{
			exit000();
		}
	}

protected void start000()
	{
		wsaaTotalUtrnValue.set(ZERO);
		wsaaTotalInciValue.set(ZERO);
		getWsaaTotalUtrnValue100();
		compute(wsaaNoOfPremiums, 2).setDivide(wsaaTotalUtrnValue, (wsaaTotalInciValue));
		wsaaRemainder.setRemainder(wsaaNoOfPremiums);
		if (isNE(wsaaRemainder,ZERO)) {
			syserrrec.statuz.set(t743);
			fatalError900();
		}
		else {
			if (isEQ(wsaaTotalUtrnValue,ZERO)) {
				goTo(GotoLabel.exit000);
			}
			else {
				reverseInciPrems300();
				reverseUtrns400();
			}
		}
	}

protected void exit000()
	{
		exitProgram();
	}

protected void getWsaaTotalUtrnValue100()
	{
		start100();
	}

protected void start100()
	{
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		utrnsurIO.setParams(SPACES);
		utrnsurIO.setChdrcoy(greversrec.chdrcoy);
		utrnsurIO.setChdrnum(greversrec.chdrnum);
		utrnsurIO.setTranno(greversrec.tranno);
		utrnsurIO.setPlanSuffix(ZERO);
		utrnsurIO.setCoverage(greversrec.coverage);
		utrnsurIO.setRider(greversrec.rider);
		utrnsurIO.setLife(greversrec.life);
		utrnsurIO.setUnitVirtualFund(SPACES);
		utrnsurIO.setUnitType(SPACES);
		utrnsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, utrnsurIO);
		if ((isNE(utrnsurIO.getStatuz(),varcom.oK))
		&& (isNE(utrnsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrnsurIO.getParams());
			syserrrec.statuz.set(utrnsurIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,utrnsurIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,utrnsurIO.getChdrnum()))
		|| (isNE(greversrec.tranno,utrnsurIO.getTranno()))) {
			utrnsurIO.setStatuz(varcom.endp);
		}
		else {
			while ( !(isEQ(utrnsurIO.getStatuz(),varcom.endp))) {
				addUpUtrnValue110();
			}
			
		}
	}

protected void addUpUtrnValue110()
	{
		start110();
	}

protected void start110()
	{
		wsaaTotalUtrnValue.add(utrnsurIO.getContractAmount());
		if (isNE(utrnsurIO.getPlanSuffix(),wsaaPlanSuffix)
		|| isNE(utrnsurIO.getCoverage(),wsaaCoverage)
		|| isNE(utrnsurIO.getRider(),wsaaRider)
		|| isNE(utrnsurIO.getLife(),wsaaLife)) {
			wsaaPlanSuffix.set(utrnsurIO.getPlanSuffix());
			wsaaCoverage.set(utrnsurIO.getCoverage());
			wsaaRider.set(utrnsurIO.getRider());
			wsaaLife.set(utrnsurIO.getLife());
			getWsaaTotalInciValue200();
		}
		utrnsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnsurIO);
		if ((isNE(utrnsurIO.getStatuz(),varcom.oK))
		&& (isNE(utrnsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrnsurIO.getParams());
			syserrrec.statuz.set(utrnsurIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,utrnsurIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,utrnsurIO.getChdrnum()))
		|| (isNE(greversrec.tranno,utrnsurIO.getTranno()))) {
			utrnsurIO.setStatuz(varcom.endp);
		}
	}

protected void getWsaaTotalInciValue200()
	{
		start200();
	}

protected void start200()
	{
		incigrvIO.setDataArea(SPACES);
		incigrvIO.setChdrcoy(greversrec.chdrcoy);
		incigrvIO.setChdrnum(greversrec.chdrnum);
		incigrvIO.setLife(utrnsurIO.getLife());
		incigrvIO.setRider(utrnsurIO.getRider());
		incigrvIO.setCoverage(utrnsurIO.getCoverage());
		incigrvIO.setPlanSuffix(utrnsurIO.getPlanSuffix());
		incigrvIO.setInciNum(ZERO);
		incigrvIO.setSeqno(ZERO);
		incigrvIO.setFormat(incigrvrec);
		incigrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incigrvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incigrvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(incigrvIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(incigrvIO.getChdrnum(),greversrec.chdrnum))
		|| (isNE(incigrvIO.getLife(),utrnsurIO.getLife()))
		|| (isNE(incigrvIO.getRider(),utrnsurIO.getRider()))
		|| (isNE(incigrvIO.getCoverage(),utrnsurIO.getCoverage()))
		|| (isNE(incigrvIO.getPlanSuffix(),utrnsurIO.getPlanSuffix()))) {
			incigrvIO.setStatuz(varcom.endp);
		}
		else {
			while ( !(isEQ(incigrvIO.getStatuz(),varcom.endp))) {
				addUpInciValue210();
			}
			
		}
	}

protected void addUpInciValue210()
	{
		/*START*/
		wsaaTotalInciValue.add(incigrvIO.getCurrPrem());
		incigrvIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incigrvIO);
		if ((isNE(incigrvIO.getStatuz(),varcom.oK))
		&& (isNE(incigrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,incigrvIO.getChdrcoy()))
		|| (isNE(incigrvIO.getChdrnum(),greversrec.chdrnum))
		|| (isNE(incigrvIO.getLife(),utrnsurIO.getLife()))
		|| (isNE(incigrvIO.getRider(),utrnsurIO.getRider()))
		|| (isNE(incigrvIO.getCoverage(),utrnsurIO.getCoverage()))
		|| (isNE(incigrvIO.getPlanSuffix(),utrnsurIO.getPlanSuffix()))) {
			incigrvIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void reverseInciPrems300()
	{
		start300();
	}

protected void start300()
	{
		incigrvIO.setDataArea(SPACES);
		incigrvIO.setPlanSuffix(ZERO);
		incigrvIO.setInciNum(ZERO);
		incigrvIO.setSeqno(ZERO);
		incigrvIO.setChdrcoy(greversrec.chdrcoy);
		incigrvIO.setChdrnum(greversrec.chdrnum);
		incigrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incigrvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incigrvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(incigrvIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(incigrvIO.getChdrnum(),greversrec.chdrnum))) {
			incigrvIO.setStatuz(varcom.endp);
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		else {
			while ( !(isEQ(incigrvIO.getStatuz(),varcom.endp))) {
				updateCurrPremFields310();
			}
			
		}
	}

protected void updateCurrPremFields310()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start310();
				}
				case rewriteInci315: {
					rewriteInci315();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start310()
	{
		compute(wsaaStoredValueToAdd, 2).set(mult(incigrvIO.getCurrPrem(),wsaaNoOfPremiums));
		if (isNE(incigrvIO.getPremCurr04(),incigrvIO.getPremStart04())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart04(),incigrvIO.getPremCurr04()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr04(), 2);
				incigrvIO.setPremCurr04(add(incigrvIO.getPremCurr04(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci315);
			}
			else {
				incigrvIO.setPremCurr04(incigrvIO.getPremStart04());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(incigrvIO.getPremCurr03(),incigrvIO.getPremStart03())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart03(),incigrvIO.getPremCurr03()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr03(), 2);
				incigrvIO.setPremCurr03(add(incigrvIO.getPremCurr03(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci315);
			}
			else {
				incigrvIO.setPremCurr03(incigrvIO.getPremStart03());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(incigrvIO.getPremCurr02(),incigrvIO.getPremStart02())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart02(),incigrvIO.getPremCurr02()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr02(), 2);
				incigrvIO.setPremCurr02(add(incigrvIO.getPremCurr02(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci315);
			}
			else {
				incigrvIO.setPremCurr02(incigrvIO.getPremStart02());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(incigrvIO.getPremCurr01(),incigrvIO.getPremStart01())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart01(),incigrvIO.getPremCurr01()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr01(), 2);
				incigrvIO.setPremCurr01(add(incigrvIO.getPremCurr01(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci315);
			}
			else {
				incigrvIO.setPremCurr01(incigrvIO.getPremStart01());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(wsaaStoredValueToAdd,ZERO)) {
			syserrrec.statuz.set(t744);
			fatalError900();
		}
	}

protected void rewriteInci315()
	{
		incigrvIO.setFunction(varcom.writd);
		incigrvIO.setTransactionDate(greversrec.transDate);
		incigrvIO.setTransactionTime(greversrec.transTime);
		incigrvIO.setUser(greversrec.user);
		incigrvIO.setTermid(greversrec.termid);
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		incigrvIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incigrvIO);
		if ((isNE(incigrvIO.getStatuz(),varcom.oK))
		&& (isNE(incigrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,incigrvIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,incigrvIO.getChdrnum()))) {
			incigrvIO.setStatuz(varcom.endp);
		}
	}

protected void reverseUtrns400()
	{
		start400();
	}

protected void start400()
	{
		utrnrevIO.setParams(SPACES);
		utrnrevIO.setChdrcoy(greversrec.chdrcoy);
		utrnrevIO.setChdrnum(greversrec.chdrnum);
		utrnrevIO.setTranno(greversrec.tranno);
		utrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, utrnrevIO);
		if ((isNE(utrnrevIO.getStatuz(),varcom.oK))
		&& (isNE(utrnrevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,utrnrevIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,utrnrevIO.getChdrnum()))
		|| (isNE(greversrec.tranno,utrnrevIO.getTranno()))) {
			utrnrevIO.setStatuz(varcom.endp);
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError900();
		}
		while ( !(isEQ(utrnrevIO.getStatuz(),varcom.endp))) {
			updateUtrns410();
		}
		
	}

protected void updateUtrns410()
	{
		start410();
	}

protected void start410()
	{
		if (isEQ(utrnrevIO.getFeedbackInd(),SPACES)) {
			deleteUtrn411();
		}
		else {
			writeNegativeUtrn412();
		}
		utrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnrevIO);
		if ((isNE(utrnrevIO.getStatuz(),varcom.oK))
		&& (isNE(utrnrevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,utrnrevIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,utrnrevIO.getChdrnum()))
		|| (isNE(greversrec.tranno,utrnrevIO.getTranno()))) {
			utrnrevIO.setStatuz(varcom.endp);
		}
	}

protected void deleteUtrn411()
	{
		start411();
	}

protected void start411()
	{
		utrnrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError900();
		}
		utrnrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError900();
		}
	}

protected void writeNegativeUtrn412()
	{
		start412();
	}

protected void start412()
	{
		utrnrevIO.setFormat(utrnrevrec);
		utrnrevIO.setFeedbackInd(SPACES);
		utrnrevIO.setSwitchIndicator(SPACES);
		utrnrevIO.setSurrenderPercent(ZERO);
		setPrecision(utrnrevIO.getProcSeqNo(), 0);
		utrnrevIO.setProcSeqNo(mult(utrnrevIO.getProcSeqNo(),-1));
		setPrecision(utrnrevIO.getContractAmount(), 2);
		utrnrevIO.setContractAmount(mult(utrnrevIO.getContractAmount(),-1));
		setPrecision(utrnrevIO.getFundAmount(), 2);
		utrnrevIO.setFundAmount(mult(utrnrevIO.getFundAmount(),-1));
		setPrecision(utrnrevIO.getNofDunits(), 5);
		utrnrevIO.setNofDunits(mult(utrnrevIO.getNofDunits(),-1));
		setPrecision(utrnrevIO.getNofUnits(), 5);
		utrnrevIO.setNofUnits(mult(utrnrevIO.getNofUnits(),-1));
		setPrecision(utrnrevIO.getInciprm01(), 2);
		utrnrevIO.setInciprm01(mult(utrnrevIO.getInciprm01(),-1));
		setPrecision(utrnrevIO.getInciprm02(), 2);
		utrnrevIO.setInciprm02(mult(utrnrevIO.getInciprm02(),-1));
		utrnrevIO.setTransactionDate(greversrec.transDate);
		utrnrevIO.setTransactionTime(greversrec.transTime);
		utrnrevIO.setTranno(greversrec.newTranno);
		utrnrevIO.setTermid(greversrec.termid);
		utrnrevIO.setUser(greversrec.user);
		wsaaBatckey.set(greversrec.batckey);
		utrnrevIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnrevIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrnrevIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrnrevIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnrevIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnrevIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		utrnrevIO.setUstmno(ZERO);
		utrnrevIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError900();
		}
	}

protected void fatalError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start900();
				}
				case end950: {
					end950();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start900()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.end950);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void end950()
	{
		greversrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
