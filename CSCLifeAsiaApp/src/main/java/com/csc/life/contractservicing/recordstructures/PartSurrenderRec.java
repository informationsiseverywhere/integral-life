package com.csc.life.contractservicing.recordstructures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class PartSurrenderRec extends ExternalData  {
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
	private String curr;
	private String partsurrMeth;
	private BigDecimal estTotalValue;
	private BigDecimal totalWithdrawAmt;
	private String contractFreq;
	private String contractType;
	private List<String> fundPer = new ArrayList<>();
	private List<String> fundWithdrawAmt = new ArrayList<>();
	private List<String> fundEstValue = new ArrayList<>();
	private BigDecimal totalWithdrawPer;
	private int nFunds;  
	private BigDecimal outWithdrawalAmt;
	private BigDecimal outWithdrawalPer;
	private BigDecimal outFundPerAmt;
	private BigDecimal outFundWithAmt;
	
	public BigDecimal getOutWithdrawalPer() {
		return outWithdrawalPer;
	}

	public BigDecimal getOutWithdrawalAmt() {
		return outWithdrawalAmt;
	}

	public void setOutWithdrawalAmt(BigDecimal outWithdrawalAmt) {
		this.outWithdrawalAmt = outWithdrawalAmt;
	}

	public void setOutWithdrawalPer(BigDecimal outWithdrawalPer) {
		this.outWithdrawalPer = outWithdrawalPer;
	}

	public BigDecimal getOutFundPerAmt() {
		return outFundPerAmt;
	}

	public void setOutFundPerAmt(BigDecimal outFundPerAmt) {
		this.outFundPerAmt = outFundPerAmt;
	}

	public BigDecimal getOutFundWithAmt() {
		return outFundWithAmt;
	}

	public void setOutFundWithAmt(BigDecimal outFundWithAmt) {
		this.outFundWithAmt = outFundWithAmt;
	}

	public String getCurr() {
		return curr;
	}
  
	public void setCurr(String curr) {
		this.curr = curr;
	}

	public String getPartsurrMeth() {
		return partsurrMeth;
	}

	public void setPartsurrMeth(String partsurrMeth) {
		this.partsurrMeth = partsurrMeth;
	}

	public BigDecimal getEstTotalValue() {
		return estTotalValue;
	}

	public void setEstTotalValue(BigDecimal estTotalValue) { 
		this.estTotalValue = estTotalValue;
	}

	public BigDecimal getTotalWithdrawAmt() {
		return totalWithdrawAmt;
	}

	public void setTotalWithdrawAmt(BigDecimal totalWithdrawAmt) {
		this.totalWithdrawAmt = totalWithdrawAmt;
	}

	public String getContractFreq() {
		return contractFreq;
	}

	public void setContractFreq(String contractFreq) {
		this.contractFreq = contractFreq;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public List<String> getFundPer() {
		return fundPer;
	}

	public void setFundPer(List<String> fundPer) {
		this.fundPer = fundPer;
	}

	public List<String> getFundWithdrawAmt() {
		return fundWithdrawAmt;
	}

	public void setFundWithdrawAmt(List<String> fundWithdrawAmt) {
		this.fundWithdrawAmt = fundWithdrawAmt;
	}

	public List<String> getFundEstValue() {
		return fundEstValue;
	}

	public void setFundEstValue(List<String> fundEstValue) {
		this.fundEstValue = fundEstValue;
	}

	public BigDecimal getTotalWithdrawPer() {
		return totalWithdrawPer;
	}

	public void setTotalWithdrawPer(BigDecimal totalWithdrawPer) {
		this.totalWithdrawPer = totalWithdrawPer;
	}

	public int getnFunds() {
		return nFunds;
	}

	public void setnFunds(int nFunds) {
		this.nFunds = nFunds;
	}


	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
