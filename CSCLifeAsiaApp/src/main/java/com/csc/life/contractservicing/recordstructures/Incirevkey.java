package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:57
 * Description:
 * Copybook name: INCIREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incirevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incirevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incirevKey = new FixedLengthStringData(64).isAPartOf(incirevFileKey, 0, REDEFINE);
  	public FixedLengthStringData incirevChdrcoy = new FixedLengthStringData(1).isAPartOf(incirevKey, 0);
  	public FixedLengthStringData incirevChdrnum = new FixedLengthStringData(8).isAPartOf(incirevKey, 1);
  	public FixedLengthStringData incirevLife = new FixedLengthStringData(2).isAPartOf(incirevKey, 9);
  	public FixedLengthStringData incirevCoverage = new FixedLengthStringData(2).isAPartOf(incirevKey, 11);
  	public FixedLengthStringData incirevRider = new FixedLengthStringData(2).isAPartOf(incirevKey, 13);
  	public PackedDecimalData incirevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incirevKey, 15);
  	public PackedDecimalData incirevTranno = new PackedDecimalData(5, 0).isAPartOf(incirevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(incirevKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incirevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incirevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}