package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:44
 * Description:
 * Copybook name: BJRNENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bjrnenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bjrnenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData bjrnenqKey = new FixedLengthStringData(64).isAPartOf(bjrnenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData bjrnenqChdrcoy = new FixedLengthStringData(1).isAPartOf(bjrnenqKey, 0);
  	public FixedLengthStringData bjrnenqChdrnum = new FixedLengthStringData(8).isAPartOf(bjrnenqKey, 1);
  	public PackedDecimalData bjrnenqTranno = new PackedDecimalData(5, 0).isAPartOf(bjrnenqKey, 9);
  	public FixedLengthStringData bjrnenqLife = new FixedLengthStringData(2).isAPartOf(bjrnenqKey, 12);
  	public FixedLengthStringData bjrnenqCoverage = new FixedLengthStringData(2).isAPartOf(bjrnenqKey, 14);
  	public FixedLengthStringData bjrnenqRider = new FixedLengthStringData(2).isAPartOf(bjrnenqKey, 16);
  	public PackedDecimalData bjrnenqPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(bjrnenqKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(bjrnenqKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bjrnenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bjrnenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}