package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PhrtpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:05
 * Class transformed from PHRTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PhrtpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 89;
	public FixedLengthStringData phrtrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData phrtpfRecord = phrtrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(phrtrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(phrtrec);
	public PackedDecimalData ptdate = DD.ptdate.copy().isAPartOf(phrtrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(phrtrec);
	public PackedDecimalData dteiss = DD.dteiss.copy().isAPartOf(phrtrec);
	public FixedLengthStringData apind = DD.apind.copy().isAPartOf(phrtrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(phrtrec);
	public PackedDecimalData hprjptdate = DD.hprjptdate.copy().isAPartOf(phrtrec);
	public PackedDecimalData manadj = DD.manadj.copy().isAPartOf(phrtrec);
	public PackedDecimalData lastTranno = DD.ltranno.copy().isAPartOf(phrtrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(phrtrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(phrtrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(phrtrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PhrtpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PhrtpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PhrtpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PhrtpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PhrtpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PhrtpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PhrtpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PHRTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PTDATE, " +
							"EFFDATE, " +
							"DTEISS, " +
							"APIND, " +
							"TRANNO, " +
							"HPRJPTDATE, " +
							"MANADJ, " +
							"LTRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     ptdate,
                                     effdate,
                                     dteiss,
                                     apind,
                                     tranno,
                                     hprjptdate,
                                     manadj,
                                     lastTranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		ptdate.clear();
  		effdate.clear();
  		dteiss.clear();
  		apind.clear();
  		tranno.clear();
  		hprjptdate.clear();
  		manadj.clear();
  		lastTranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPhrtrec() {
  		return phrtrec;
	}

	public FixedLengthStringData getPhrtpfRecord() {
  		return phrtpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPhrtrec(what);
	}

	public void setPhrtrec(Object what) {
  		this.phrtrec.set(what);
	}

	public void setPhrtpfRecord(Object what) {
  		this.phrtpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(phrtrec.getLength());
		result.set(phrtrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}