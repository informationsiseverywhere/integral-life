package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR50K
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50kScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(329);
	public FixedLengthStringData dataFields = new FixedLengthStringData(105).isAPartOf(dataArea, 0);
	public FixedLengthStringData cltdobs = new FixedLengthStringData(16).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] cltdob = ZDArrayPartOfStructure(2, 8, 0, cltdobs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(cltdobs, 0, FILLER_REDEFINE);
	public ZonedDecimalData cltdob01 = DD.cltdob.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData cltdob02 = DD.cltdob.copyToZonedDecimal().isAPartOf(filler,8);
	public FixedLengthStringData cltsexs = new FixedLengthStringData(2).isAPartOf(dataFields, 16);
	public FixedLengthStringData[] cltsex = FLSArrayPartOfStructure(2, 1, cltsexs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(cltsexs, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltsex01 = DD.cltsex.copy().isAPartOf(filler1,0);
	public FixedLengthStringData cltsex02 = DD.cltsex.copy().isAPartOf(filler1,1);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData occpcodes = new FixedLengthStringData(8).isAPartOf(dataFields, 73);
	public FixedLengthStringData[] occpcode = FLSArrayPartOfStructure(2, 4, occpcodes, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(occpcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData occpcode01 = DD.occpcode.copy().isAPartOf(filler2,0);
	public FixedLengthStringData occpcode02 = DD.occpcode.copy().isAPartOf(filler2,4);
	public FixedLengthStringData salutls = new FixedLengthStringData(16).isAPartOf(dataFields, 81);
	public FixedLengthStringData[] salutl = FLSArrayPartOfStructure(2, 8, salutls, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(salutls, 0, FILLER_REDEFINE);
	public FixedLengthStringData salutl01 = DD.salutl.copy().isAPartOf(filler3,0);
	public FixedLengthStringData salutl02 = DD.salutl.copy().isAPartOf(filler3,8);
	public FixedLengthStringData smokings = new FixedLengthStringData(4).isAPartOf(dataFields, 97);
	public FixedLengthStringData[] smoking = FLSArrayPartOfStructure(2, 2, smokings, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(4).isAPartOf(smokings, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01 = DD.smoking.copy().isAPartOf(filler4,0);
	public FixedLengthStringData smoking02 = DD.smoking.copy().isAPartOf(filler4,2);
	public FixedLengthStringData statcodes = new FixedLengthStringData(4).isAPartOf(dataFields, 101);
	public FixedLengthStringData[] statcode = FLSArrayPartOfStructure(2, 2, statcodes, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(4).isAPartOf(statcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData statcode01 = DD.statcode.copy().isAPartOf(filler5,0);
	public FixedLengthStringData statcode02 = DD.statcode.copy().isAPartOf(filler5,2);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 105);
	public FixedLengthStringData cltdobsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] cltdobErr = FLSArrayPartOfStructure(2, 4, cltdobsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(8).isAPartOf(cltdobsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltdob01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData cltdob02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData cltsexsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] cltsexErr = FLSArrayPartOfStructure(2, 4, cltsexsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(cltsexsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltsex01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData cltsex02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData occpcodesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] occpcodeErr = FLSArrayPartOfStructure(2, 4, occpcodesErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(8).isAPartOf(occpcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData occpcode01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData occpcode02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData salutlsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] salutlErr = FLSArrayPartOfStructure(2, 4, salutlsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(8).isAPartOf(salutlsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData salutl01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData salutl02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData smokingsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] smokingErr = FLSArrayPartOfStructure(2, 4, smokingsErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(smokingsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData smoking02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData statcodesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData[] statcodeErr = FLSArrayPartOfStructure(2, 4, statcodesErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(8).isAPartOf(statcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData statcode01Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData statcode02Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 161);
	public FixedLengthStringData cltdobsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] cltdobOut = FLSArrayPartOfStructure(2, 12, cltdobsOut, 0);
	public FixedLengthStringData[][] cltdobO = FLSDArrayPartOfArrayStructure(12, 1, cltdobOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(cltdobsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cltdob01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] cltdob02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData cltsexsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] cltsexOut = FLSArrayPartOfStructure(2, 12, cltsexsOut, 0);
	public FixedLengthStringData[][] cltsexO = FLSDArrayPartOfArrayStructure(12, 1, cltsexOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(24).isAPartOf(cltsexsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cltsex01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] cltsex02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData occpcodesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] occpcodeOut = FLSArrayPartOfStructure(2, 12, occpcodesOut, 0);
	public FixedLengthStringData[][] occpcodeO = FLSDArrayPartOfArrayStructure(12, 1, occpcodeOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(24).isAPartOf(occpcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] occpcode01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] occpcode02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData salutlsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] salutlOut = FLSArrayPartOfStructure(2, 12, salutlsOut, 0);
	public FixedLengthStringData[][] salutlO = FLSDArrayPartOfArrayStructure(12, 1, salutlOut, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(24).isAPartOf(salutlsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] salutl01Out = FLSArrayPartOfStructure(12, 1, filler15, 0);
	public FixedLengthStringData[] salutl02Out = FLSArrayPartOfStructure(12, 1, filler15, 12);
	public FixedLengthStringData smokingsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(2, 12, smokingsOut, 0);
	public FixedLengthStringData[][] smokingO = FLSDArrayPartOfArrayStructure(12, 1, smokingOut, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(24).isAPartOf(smokingsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] smoking01Out = FLSArrayPartOfStructure(12, 1, filler16, 0);
	public FixedLengthStringData[] smoking02Out = FLSArrayPartOfStructure(12, 1, filler16, 12);
	public FixedLengthStringData statcodesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 144);
	public FixedLengthStringData[] statcodeOut = FLSArrayPartOfStructure(2, 12, statcodesOut, 0);
	public FixedLengthStringData[][] statcodeO = FLSDArrayPartOfArrayStructure(12, 1, statcodeOut, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(24).isAPartOf(statcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] statcode01Out = FLSArrayPartOfStructure(12, 1, filler17, 0);
	public FixedLengthStringData[] statcode02Out = FLSArrayPartOfStructure(12, 1, filler17, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData cltdob01Disp = new FixedLengthStringData(10);
	public FixedLengthStringData cltdob02Disp = new FixedLengthStringData(10);

	public LongData Sr50kscreenWritten = new LongData(0);
	public LongData Sr50kprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr50kScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(salutl02Out,new String[] {"01","12","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltsex02Out,new String[] {"02","13","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltdob02Out,new String[] {"03","14","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occpcode02Out,new String[] {"04","15","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(statcode02Out,new String[] {"05","16","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(smoking02Out,new String[] {"06","17","-06",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {lifcnum, lifename, salutl01, salutl02, cltsex01, cltsex02, cltdob01, cltdob02, occpcode01, occpcode02, statcode01, statcode02, smoking01, smoking02};
		screenOutFields = new BaseData[][] {lifcnumOut, lifenameOut, salutl01Out, salutl02Out, cltsex01Out, cltsex02Out, cltdob01Out, cltdob02Out, occpcode01Out, occpcode02Out, statcode01Out, statcode02Out, smoking01Out, smoking02Out};
		screenErrFields = new BaseData[] {lifcnumErr, lifenameErr, salutl01Err, salutl02Err, cltsex01Err, cltsex02Err, cltdob01Err, cltdob02Err, occpcode01Err, occpcode02Err, statcode01Err, statcode02Err, smoking01Err, smoking02Err};
		screenDateFields = new BaseData[] {cltdob01, cltdob02};
		screenDateErrFields = new BaseData[] {cltdob01Err, cltdob02Err};
		screenDateDispFields = new BaseData[] {cltdob01Disp, cltdob02Disp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr50kscreen.class;
		protectRecord = Sr50kprotect.class;
	}

}
