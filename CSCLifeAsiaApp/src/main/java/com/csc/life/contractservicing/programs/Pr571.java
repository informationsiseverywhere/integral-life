/*
 * File: Pr571.java
 * Date: 30 August 2009 1:43:05
 * Author: Quipoz Limited
 * 
 * Class transformed from PR571.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CprppfDAO;
import com.csc.life.contractservicing.dataaccess.dao.CvuwpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.PcdtpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Cprppf;
import com.csc.life.contractservicing.dataaccess.model.Cvuwpf;
import com.csc.life.contractservicing.dataaccess.model.Pcdtpf;
import com.csc.life.contractservicing.screens.Sr571ScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.dataaccess.dao.PovrpfDAO;
import com.csc.life.general.dataaccess.model.Povrpf;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.terminationclaims.dataaccess.dao.CovppfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Covppf;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  PR571 - Component Change - Term Components.
*  -------------------------------------------
*  ----
*  This screen/program PR571 is  used  to  capture any  changes
*  to  the    coverage   and   rider  details  for  traditional
*  generic components.
*
*  Initialise.
*  -----------
*
*  Skip this section  if    returning    from    an    optional
*  selection (current stack position action flag = '*').
*
*  Read  CHDRLIF  (RETRV)  in    order  to  obtain the contract
*  header information.
*
*  The key for Coverage/rider  to    be    worked  on  will  be
*  available  from  COVR.   This is obtained by using the RETRV
*  function.
*
*  Firstly  we  need  to check  if  there  has  already    been
*  an   alteration   to   this     coverage/rider  within  this
*  transaction. If this  is   the  case  there  will    be    a
*  record      present     on    the  temporary  coverage/rider
*  transaction  file    COVTPF.    Read  this  file  using  the
*  logical  view    COVTMAJ  and  the  key  from  the retrieved
*  coverage/rider record. If  a  record  is  found  go  to  the
*  SET-UP-SCREEN section.
*
*  Compare  the  plan  suffix  of  the  retrieved  record  with
*  the contract header no of policies  in  plan    field.    If
*  the  plan  suffix  is '00' go to SET-UP-COVT.  If it is less
*  than or equal to this field the we need  to  'breakout'  the
*  amount fields as follows:
*
*  Divide the COVR sum assured  field  by  the CHDR no of
*                policies in plan.
*  Divide  the COVR premium  field  by  the  CHDR  no  of
*                policies in plan.
*
*  ******* SET-UP-COVT.
*
*  Move    the  appropriate fields  from  the  COVR  record  to
*  the COVTMAJ record.
*
*  ******* SET-UP-SCREEN.
*
*  Read  the  contract definition  details  from   T5688    for
*  the  contract  type  held on  CHDRLIF.  Access  the  version
*  of this item for the original commencement date of the risk.
*
*  Read  the  general  coverage/rider  details from  T5687  and
*  the  traditional/term    edit    rules  from  T5608  for the
*  coverage type held  on  COVTMAJ.  Access  the    version  of
*  these item for the original commencement date of the risk.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain the life assured  and joint-life details (if any)
*  do the following;-
*
*       - read the life  details  using  LIFEMAJ  (life  number
*       from  COVTMAJ,  joint  life number  '00').  Look up the
*       name from the client details (CLTS)  and  format  as  a
*       "confirmation name".
*
*       -  read  the  joint    life details using LIFEMAJ (life
*       number from COVTMAJ,  joint  life  number  '01').    If
*       found,  look  up  the  name  from  the  client  details
*       (CLTS) and format as a "confirmation name".
*
*  To determine which premium  calculation    method    to  use
*  decide  whether  or  not  it  is  a   Single  or  Joint-life
*  case  (it is a joint life case, the  joint    life    record
*  was  found  above). Then:
*
*       -  if  it  is a single-life case use, the single-method
*       from T5687. The age to be used  for validation will  be
*       the age of the main life.
*
*       -  if  the joint-life  indicator (from T5687) is blank,
*       and  if  it  is  a    Joint-life    case,    use    the
*       joint-method  from  T5687.  The  age  to  be  used  for
*       validation will be the age of the main life.
*
*       -  if the Joint-life  indicator  is  'N',   then    use
*       the  Single-method.    But,  if  there  is a joint-life
*       (this must be a rider to have got  this  far)    prompt
*       for  the  joint  life  indicator  to  determine   which
*       life  the rider is to attach to.  In all  other  cases,
*       the  joint  life  indicator should be non-displayed and
*       protected.  The  age  to  be used for  validation  will
*       be  the    age  of the main or joint life, depending on
*       the one selected.
*
*       - use the premium-method  selected  from   T5687,    if
*       not  blank, to access T5675.  This gives the subroutine
*       to use for the calculation.
*
*      COVERAGE/RIDER DETAILS
*
*  The fields to be displayed  on  the  screen  are  determined
*  from the entry in table T5608 read earlier as follows:
*
*       -  if  the  maximum and minimum sum insured amounts are
*       both zero, non-display  and  protect  the  sum  insured
*       amount.    If  the  minimum and maximum  are  both  the
*       same, display the  amount  protected.  Otherwise  allow
*       input.
*
*       -    if    all    the   valid   mortality  classes  are
*       blank, non-display and protect this   field.  If  there
*       is  only one mortality class, display  and  protect  it
*       (no validation will be required).
*
*       - if all the valid  lien codes are  blank,  non-display
*       and  protect this field. Otherwise, this is an optional
*       field.
*
*       - if the cessation  AGE  section  on  T5608  is  blank,
*       protect the two age related fields.
*
*       -    if  the  cessation    TERM  section  on  T5608  is
*       blank, protect the two term related fields.
*
*       - using the age next  birthday  (ANB  at   RCD)    from
*       the  applicable  life (see above), look up Issue Age on
*       the AGE and TERM sections.  If the  age fits into  only
*       one  "slot"  in  one  of these  sections,  and the risk
*       cessation limits are the same, default   and    protect
*       the    risk  cessation fields.  Also  do the  same  for
*       the  premium   cessation  details.      In  this  case,
*       also    calculate    the    risk  and premium cessation
*       dates.
*
*       -  please note that it  is  possible  for   the    risk
*       and  premium  cessation  dates    to   be  overwritten,
*       i.e.  even after they are calculated.
*
*      OPTIONS AND EXTRAS
*
*  If options and extras are  not   allowed    (as  defined  by
*  T5608) non-display and protect the fields.
*
*  Otherwise,  read  the  options  and  extras  details for the
*  current coverage/rider.   If any records  exist,    put    a
*  '+'    in   the Options/Extras indicator (to show that there
*  are some).
*
*  If options and extras  are  not  allowed,  then  non-display
*  and  protect  the  special  terms    narrative  clause  code
*  fields. If special terms are present,    then    obtain  the
*  clause  codes  from  the  coverage  record and output to the
*  screen.
*
* REASSURANCE
*
* If Reassurance is not allowed ( as defined by T5687 ), then
* non-display and protect the field ( SR571-RATYPIND ).
*
* Otherwise,  read  the  Reassurance  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the  Reassurance  indicator (to show that there are some).
*
*  Finally after setting up  all  of  the    screen  attributes
*  move over the relevant fields from COVTMAJ where applicable.
*
*  Validation.
*  -----------
*
*  Skip  this  section  if    returning    from    an  optional
*  selection (current stack position action flag = '*').
*
*  If CF11 (KILL) is requested then skip the validation.
*
*  Before the premium amount  is  calculated,  the screen  must
*  be  valid.    So  all  editing    is  completed  before  the
*  premium is calculated.
*
*  Table T5608 (previously read)  is    used    to  obtain  the
*  editing  rules.    Edit  the  screen according  to the rules
*  defined by the help. In particular:-
*
*       1) Check the  sum-assured,  if   applicable,    against
*       the limits.
*
*       2)  Check  the  consistency  of   the risk age and term
*       fields and premium age and term  fields.  Either,  risk
*       age  and  and  premium age must be used  or  risk  term
*       and premium term must  be  used.    They  must  not  be
*       combined.  Note that these only need validating if they
*       were not defaulted.
*
*       3) Mortality-Class, if the  mortality    class  appears
*       on  a  coverage/rider  screen  it is a compulsory field
*       because  it  will    be  used  in    calculating    the
*       premium    amount.    The  mortality class entered must
*       one  of the ones in the edit rules table.
*
*  Calculate the following:-
*
*       - risk cessation date
*
*       - premium cessation date
*
*       - note, the risk and premium  cessation  dates  may  be
*       overwritten by the user.
*
*  OPTIONS AND EXTRAS
*
*  If  options/extras  already  exist, there  will  be a '+' in
*  this field.  A request to access  the  details  is  made  by
*  entering  'X'.    No other values  (other  than  blank)  are
*  allowed.  If  options  and  extras  are  requested,  DO  NOT
*  CALCULATE THE PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance  already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
*  PREMIUM CALCULATION.
*  --------------------
*
*  The    premium   amount is  required  on  all  products  and
*  all validating  must  be   successfully  completed    before
*  it    is  calculated.  If  there  is    no   premium  method
*  defined (i.e the relevant code  was  blank),    the  premium
*  amount  must  be  entered.  Otherwise,  it  is  optional and
*  always calculated.
*
*  To  calculate  it,    call    the    relevant    calculation
*  subroutine worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       -  Joint life number (if the screen indicator is set to
*       'J' ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. term cessation date)
*       - Currency
*       - Sum insured
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date of transaction)
*
*  Subroutine may look up:
*
*       - Life and Joint-life details
*       - Options/Extras
*
*  Having calculated it, the    entered    value,  if  any,  is
*  compared  with  it  to check that it  is  within  acceptable
*  limits of the automatically  calculated  figure.    If    it
*  is    less    than    the  amount    calculated  and  within
*  tolerance  then  the  manually entered  amount  is  allowed.
*  If    the    entered  value  exceeds the calculated one, the
*  calculated value is used.
*
*  To check the tolerance  amount,  read  the  tolerance  limit
*  from  T5667  (item  is  transaction  and  coverage  code, if
*  not found, item and '****'). Although this  is    a    dated
*  table, just read the latest one (using ITEM).
*
*      If 'CALC' was entered then re-display the screen.
*
*  Updating.
*  ---------
*
*  Updating  occurs  if  any of the  fields  on the screen have
*  been amended from the original COVTMAJ details.
*
*  If the 'KILL' function key was pressed skip the updating.
*
*  Update the COVTMAJ record with the details from  the  screen
*  and write/rewrite the record to the database.
*
*  IF no Coverage/rider details have been changed, but
*    Reassurance or Options/Extras details have been amended,
*    we force the program to produce a COVT record so that the
*    Component change AT module is executed, thus writing a PTRN
*    indicating that something has happened to the Coverage/Rider
*    details.
*
*  Next Program.
*  -------------
*
*  The    first  thing  to   consider   is   the   handling  of
*  an options/extras request. If  the    indicator  is  'X',  a
*  request  to  visit options and extras has been made. In this
*  case:
*
*       - change the options/extras request indicator to '?',
*
*       - save the next 8 programs from the program stack,
*
*       - call GENSSWCH with and  action  of  'A'  to  retrieve
*       the  program  switching  required, and move them to the
*       stack,
*
*       - set the current stack "action" to '*',
*
*       - add one to the program pointer and exit.
*
*  On return from this  request,  the  current  stack  "action"
*  will  be    '*' and the  options/extras  indicator  will  be
*  '?'.  To handle the return from options and extras:
*
*       -  calculate  the  premium  as  described   above    in
*       the  'Validation'  section,  and  check    that  it  is
*       within the tolerance limit,
*
*       -  blank  out  the  stack   "action",    restore    the
*       saved programs to the program stack,
*
*       -  if  the  tolerance   checking  results  in  an error
*       being detected, set WSSP-NEXTPROG  to    the    current
*       screen name (thus returning to re-display the screen).
*
* If Reassurance has been requested (the indicator = 'X'), then..
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with and  action  of 'B' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance  indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
*  If  control  is passed to this  part  of the 4000 section on
*  the way  out  of the program,  ie.   after    screen    I/O,
*  then    the  current  stack  position  action  flag  will be
*  blank.  If the 4000 section  is  being    performed    after
*  returning      from  processing  another  program  then  the
*  current  stack  position action flag will be '*'.
*
*  If 'Enter' has been pressed  add 1 to  the  program  pointer
*  and exit.
*
*  If  'KILL'  has  been requested, (CF11), then move spaces to
*  the current program entry in the program stack and exit.
*****************************************************************
* </pre>
*/
public class Pr571 extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
//	public int numberOfParameters = 0; //to fix findbug commented 
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR571");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTempPrev = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaPrevDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaDay1 = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaDay2 = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaProrate = new ZonedDecimalData(11, 6).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaWorkingAnb = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaPovrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPovrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaPcestermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcestermStore = new ZonedDecimalData(3, 0);
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private PackedDecimalData wsaaCalcPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaProtax = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaProtaxRate = new ZonedDecimalData(10, 5);
	private ZonedDecimalData wsaaStoreZbinstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreSingp = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreInstprem = new ZonedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private String wsaaPovrModified = "N";

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");

		/* WSAA-TMP1 */
	private ZonedDecimalData wsaaTmpprem = new ZonedDecimalData(18, 3).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(wsaaTmpprem, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaDecimal3 = new ZonedDecimalData(1, 0).isAPartOf(filler, 17).setUnsigned();

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator policyLevel = new Validator(wsaaPlanproc, "N");
	protected Validator planLevel = new Validator(wsaaPlanproc, "Y");

		/*    88 FIRST-TAX-CALC                      VALUE 'Y'.            
		    88 NOT-FIRST-TAX-CALC                  VALUE 'N'.            */
	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	private Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	protected Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
	private Validator compApp = new Validator(wsaaFlag, "S", "P");
	private Validator addApp = new Validator(wsaaFlag, "P");
	private Validator revComp = new Validator(wsaaFlag, "R");
	private Validator addLife = new Validator(wsaaFlag, "D");

	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);

	protected PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);


	private ZonedDecimalData wsaaProprem = new ZonedDecimalData(15, 5);
	private PackedDecimalData wsaaNextSeqnbr = new PackedDecimalData(3, 0).init(999);

	private ZonedDecimalData wsaaOldPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(17, 0);

	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSuminsDiff = new PackedDecimalData(17, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaAnb = new PackedDecimalData(3, 0);


	private FixedLengthStringData wsaaIfSuminsChanged = new FixedLengthStringData(1);
	private Validator wsaaSuminsChanged = new Validator(wsaaIfSuminsChanged, "I", "D");
	private Validator wsaaSuminsIncreased = new Validator(wsaaIfSuminsChanged, "I");
	private Validator wsaaSuminsDecreased = new Validator(wsaaIfSuminsChanged, "D");

	private FixedLengthStringData wsaaIfPremChanged = new FixedLengthStringData(1);
	private Validator wsaaPremChanged = new Validator(wsaaIfPremChanged, "I", "D");
	private Validator wsaaPremIncreased = new Validator(wsaaIfPremChanged, "I");
	private Validator wsaaPremDecreased = new Validator(wsaaIfPremChanged, "D");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
		/* WSAA-MAIN-LIFE-DETS */
	protected PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsaaSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaOptext = new FixedLengthStringData(1);
	private Validator optextYes = new Validator(wsaaOptext, "Y");
	private Validator optextNo = new Validator(wsaaOptext, "N");
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler3, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler5, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);

		/* Plan selection.*/
	protected FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	protected FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	protected FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	protected FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	protected FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	protected FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	protected FixedLengthStringData filler7 = new FixedLengthStringData(49).isAPartOf(wsaaCovrKey, 15, FILLER).init(SPACES);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
		/* WSBB-JOINT-LIFE-DETS */
	protected PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsccSingPremFlag = new FixedLengthStringData(1);
	private Validator singlePremium = new Validator(wsccSingPremFlag, "Y");
	private FixedLengthStringData wsaaLextUpdates = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
			/*Coverage/Rider details - Major Alts*/
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Cover/Rider Logical View*/
//	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
		/*Coverage transaction Record - Major Alts*/
		/*Life Details File - Major Alts*/

		/*Premium breakdown additions to the covR.*/
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected Datcon3rec datcon3rec = new Datcon3rec();
	private Gensswrec gensswrec = new Gensswrec();
	protected Premiumrec premiumrec = new Premiumrec();
	private T2240rec t2240rec = new T2240rec();
	protected T5608rec t5608rec = getT5608rec();
	
	public T5608rec getT5608rec(){
		return new T5608rec();
	}
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	protected T5675rec t5675rec = new T5675rec();
	protected T5687rec t5687rec = new T5687rec();
	private T6005rec t6005rec = new T6005rec();
	private Th506rec th506rec = new Th506rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Th528rec th528rec = new Th528rec();// fwang3 ICIL-560
	private Wssplife wssplife = new Wssplife();
	protected Sr571ScreenVars sv = getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	/*TSD-308 START*/
    private Datcon4rec datcon4rec = new Datcon4rec();	// TSD-308
	private ZonedDecimalData wsaaMnthVers = new ZonedDecimalData(8, 0);	// TSD-308
	private ZonedDecimalData wsaaLastAnnv = new ZonedDecimalData(8, 0);	// TSD-308
//	private CovrTableDAM covrIO = new CovrTableDAM();	// TSD-308
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	boolean isFeaAllowed = false;
	/* TSD-308 END */
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	protected ExternalisedRules er = new ExternalisedRules();
	private boolean loadingFlag = false;/* BRD-306 */
	//ILIFE-3519:Start
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	//ILIFE-3519:End
	private boolean prmbasisFlag=false;
	private boolean dialdownFlag = false;
	
	
//ILIFE-3310-add by liwei
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private CvuwpfDAO cvuwpfDAO = getApplicationContext().getBean("cvuwpfDAO", CvuwpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO .class);
	private PovrpfDAO povrpfDAO = getApplicationContext().getBean("povrpfDAO", PovrpfDAO .class);
	
//	private Map<String,Descpf> t5688Map =  new HashMap<String, Descpf>(); //ILIFE-8723
//	private Map<String,Descpf> t5687Map =  new HashMap<String, Descpf>(); //ILIFE-8723
	boolean itemFlag = false;
	private String t5675Itemitem = ""; 
	private Cvuwpf cvuwpf = null;
	private Lifepf lifepf = null;
	private Clntpf clntpf = null;
	private List<Lifepf> lifeList = null;
	private Payrpf payrpf = null;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
	//ILIFE-6586
	protected CprppfDAO cprppfDAO = getApplicationContext().getBean("cprppfDAO",CprppfDAO.class);
	/*Temporary Commission Split*/
	private PcdtpfDAO pcdtpfDAO = getApplicationContext().getBean("pcdtpfDAO",PcdtpfDAO.class);

	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	boolean CSMIN003Permission  = false;

	private static final String t6a7 = "T6A7";
	private static final String t6a8 = "T6A8";

	//ILJ-46
	private boolean contDtCalcFlag = false;
	private String cntDteFeature = "NBPRP113";
	//end

	private PackedDecimalData wsaaCashValArr = new PackedDecimalData(17, 2);
	/*
	 * fwang3 ICIL-560
	 */
	private boolean hasCashValue = false;
	private PackedDecimalData wsaaHoldSumin = new PackedDecimalData(15, 0);
	private boolean chinaLocalPermission;
//next
/*Life Extras & Options Reversals logical*/
	protected Cprppf cprppf = null;
	private Pcdtpf pcdtpf = null;	
	//private Covrpf covrpfIO = null;
	private Lextpf firstLext = null;
	
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",  HcsdpfDAO.class);
	private List<Hcsdpf> hsdpfList = null;
	private	Hcsdpf hcsdpf = null;
	
	//ILB-456 start 
    protected Chdrpf chdrpf = new Chdrpf();
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
	private int covrpfCount = 0;
	private Clntpf clts;//ILIFE-8187
	//ICIL-1311:Start
	boolean NBPRP056Permission  = false;
	boolean isFollowUpRequired  = false;
	private Itempf itempf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private T3644rec t3644rec = new T3644rec();
	private Ta610rec ta610rec = new Ta610rec();
	private T5661rec t5661rec = new T5661rec();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	private Fluppf fluppf;
	private int fupno = 0;
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();	//ICIL-1494
	private List<Fluppf> fluppfList = new ArrayList<Fluppf>();
	private Descpf fupDescpf;
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private static final String NBPRP056 = "NBPRP056";
	private static final String t5661 = "T5661";
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> itempfList = new ArrayList<Itempf>();	//ICIL-1494
	//ICIL-1311:End
	
	private Descpf descpf = new Descpf(); //ILIFE-8723
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Covtpf covtpf = new Covtpf();
	//IBPLIFE-2134
	private boolean contnewBFlag = false;
	private String cntnewBFeature = "NBPRP126";
	Covppf covppf=null;
	private CovppfDAO covppfDAO = getApplicationContext().getBean("covppfDAO", CovppfDAO.class);
	//end
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		cont2030, 
		beforeExit2080, 
		exit2090, 
		checkSuminCont2120, 
		checkRcessFields2120, 
		validDate2140, 
		datesCont2140, 
		ageAnniversary2140, 
		check2140, 
		checkOccurance2140, 
		checkTermFields2150, 
		checkComplete2160, 
		checkMortcls2170, 
		loop2175, 
		checkLiencd2180, 
		loop2185, 
		checkMore2190, 
		cont, 
		exit2290, 
		updatePcdt3060, 
		exit3090, 
		exit3290, 
		exit50z0, 
		exit5290, 
		exit5390, 
		covtExist6025, 
		itemCall7023, 
		a200Exit
	}

	protected Sr571ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sr571ScreenVars.class);
	}
	
	public Pr571() {
		super();
		screenVars = sv;
		new ScreenModel("Sr571", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

//ILIFE-8187 start
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clts.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clts.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  "); //ILB-474
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  "); //ILB-474
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
//ILIFE-8187 end
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		if(initialise1010()){
			contractHeader1020();
			contractCoverage1030();
			headerToScreen1040();
			lifeDetails1050();
			setScreen1060();
			commisionSplit1070();
			initializeScreenCustomerSpecific();
		}
	}

protected boolean initialise1010()
	{
		//ILJ-46
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP113", appVars, "IT"); 
		
		NBPRP056Permission  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), NBPRP056, appVars, "IT");
		
		contnewBFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntnewBFeature, appVars, "IT");//IBPLIFE-2134
		/* If returning from a multiple selection then go to exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return false;
		}
		/* Move WSSP key details to working storage for later updates*/
		/*   and table reads.*/
		/* Move WSSP FLAG to working storage for later program condition*/
		/*   checks.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaFlag.set(wsspcomn.flag);
		CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
		isFeaAllowed = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL30801", appVars,smtpfxcpy.item.toString());
		/* Intialise the Screen.*/
		sv.dataArea.set(SPACES);
		// IBPLIFE-2134
		if (!contnewBFlag) {
			sv.contnewBScreenflag.set("N");
		}
		else
		{
			sv.contnewBScreenflag.set("Y");	
		}
		// end
		// ILJ-46
		if (!contDtCalcFlag) {
			sv.riskCessAgeOut[varcom.nd.toInt()].set("Y");
		}
		// end
		sv.anbAtCcd.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		prmbasisFlag=false;
		sv.dialdownoption.set(SPACES);
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		sv.adjustageamt.set(0);
		/*BRD-306 END */
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.linstamt.set(ZERO);
		sv.taxamt01.set(ZERO);
		sv.taxamt02.set(ZERO);
		sv.sumin.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.comind.set(SPACES);
		wsaaSuspAvail.set(ZERO);
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		wsaaPlanSuffix.set(ZERO);
		wsaaSumin.set(ZERO);
		wsaaOldPrem.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaPremDiff.set(ZERO);
		wsaaSuminsDiff.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaProtax.set(ZERO);
		wsaaIndex.set(ZERO);
		sv.prmbasis.set(SPACES);//ILIFE-3519
		sv.cashvalarer.set(ZERO);//ICIL-299
		wsaaCashValArr.set(ZERO);//ICIL-299
		
		String coy = wsspcomn.company.toString();
	//	t5688Map = descDAO.getItems("IT", coy, "T5688", wsspcomn.language.toString()); //ILIFE-8723
	//	t5687Map = descDAO.getItems("IT", coy, "T5687", wsspcomn.language.toString()); //ILIFE-8723
		
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
		//IBPLIFE-2132
//		if(contnewBFlag){
//			sv.trcode.set(wsaaBatckey.batcBatctrcde);
//			descpf=descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
//		    if (null!=descpf) {
//		        sv.trcdedesc.set(descpf.getLongdesc());
//		    }
//		    else{
//		    	 sv.trcdedesc.set("?");
//		    }
//		  
//		    if (isEQ(wsaaToday, 0)) {
//		        datcon1rec.function.set(varcom.tday);
//		        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
//		        wsaaToday.set(datcon1rec.intDate);
//		    }
//		    sv.dateeff.set(wsaaToday);
			//sv.dateeff.set(wsspcomn.effdate.toInt());
//		}
		//end
		return true;
	}

	/**
	* <pre>
	*    MOVE 'Y'                    TO  WSAA-FIRST-TIME.             
	*    MOVE 'Y'                    TO  WSAA-FIRST-TAX-CALC.         
	* </pre>
	*/
protected void contractHeader1020()
	{
		/* Retrieve contract header information.*/
	//ILB-456
	chdrpf = chdrpfDAO.getCacheObject(chdrpf);
	if(null==chdrpf) {
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		else {
			chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
			if(null==chdrpf) {
				fatalError600();
			}
			else {
				chdrpfDAO.setCacheObject(chdrpf);
			}
		}
	}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/*BRD-306 START */
		sv.cnttype.set(chdrpf.getCnttype());
		/*BRD-306 END */
		
 //ILIFE-8723 start
		descpf=descDAO.getdescData("IT", "T5688" ,chdrpf.getCnttype(),wsspcomn.company.toString(), wsspcomn.language.toString());/* IJTI-1386 */
		if (descpf == null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		
/*if(t5688Map != null){
			if(t5688Map.get(chdrpf.getCnttype()) != null){ IJTI-1386 
				sv.ctypedes.set(t5688Map.get(chdrpf.getCnttype()).getLongdesc()); IJTI-1386 
			}else{
				sv.ctypedes.fill("?");
			}
		}*/
 //ILIFE-8723 end
loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		datcon2rec.intDate1.set(chdrpf.getOccdate());
		datcon2rec.intDate2.set(chdrpf.getOccdate());
		if (addComp.isTrue()) {
			while ( !(isGT(datcon2rec.intDate2,wsspcomn.currfrom))) {
				getNextEffdate1400();
			}
			
			wsaaPrevDue.set(wsaaTempPrev);
			wsaaNextDue.set(datcon2rec.intDate2);
		}
		if (addComp.isTrue() && !contnewBFlag) {
			sv.effdate.set(wsspcomn.currfrom);
		}
		else {
			//sv.effdate.set(chdrmjaIO.getOccdate());
			sv.effdate.set(chdrpf.getPtdate());//ICIL-560
		}
	}

/**
 * fwang3 ICIL-560
 */
private void processCashValue() {
	chinaLocalPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM007", appVars, "IT");
	if(chinaLocalPermission) {
		List<Itempf> items = itemDAO.getItemsbyItemcodeLikeOperator("IT", wsspcomn.company.toString(), "TH528", covrpf.getCrtable());/* IJTI-1386 */
		if (items.isEmpty()) {
			sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
			hasCashValue = false;
		} else {
			loadTh528(items);
			hasCashValue = true;
		}
	} else {
		sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
	}
}

	/**
	 * @author fwang3
	 * ICIL-560
	 * @param items
	 */
	private void loadTh528(List<Itempf> items) {
		String item4parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "" + covrpf.getMortcls()	+ "" + covrpf.getSex();
		String item2parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "**";
		for (Itempf item : items) {
			if ((new Integer(covrpf.getCrrcd()).compareTo(new Integer(item.getItmfrm().toString()))) >= 0) {
				if (item.getItemitem().equals(item4parts)) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
				if (item.getItemitem().equals(item2parts)) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
				if (item.getItemitem().equals(covrpf.getCrtable() + "****")) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
			}
		}
	}
	
protected void contractCoverage1030()
	{
		/* Retrieve the coverage details saved from either p5131 (modify/*/
		/*   enquiry) or p5128 (add).*/
		//ILB-456 
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
	
		processCashValue();//fwang3 ICIL-560
		/* TSD-308 START */
		if (isFeaAllowed) {
			covrpfList = covrpfDAO.searchCovrRecordForContract(covrpf);
			if(covrpfList==null){
				fatalError600();
			}
			caluclateTransactionDate();	// TSD-308
			int crrcd = 0;
			if(covrpfList !=null && !covrpfList.isEmpty()){
				covrpf = covrpfList.get(0);
				crrcd = covrpf.getCrrcd();
			}
			calculateMonthiversary(crrcd);	// TSD-308
		}
		/* TSD-308 END */
		if (addComp.isTrue() && !contnewBFlag) {            
			/*if (isFeaAllowed) {
				wsspcomn.currfrom.set(wsaaMnthVers);
			}*/
			sv.effdate.set(wsspcomn.currfrom);
		}
		else {
//			sv.effdate.set(covrmjaIO.getCurrfrom());
			sv.effdate.set(chdrpf.getPtdate());//ICIL-560
		}
		/* Read T2240 for age definition.                                  */
		/* MOVE CHDRMJA-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		boolean itemfound = readT2240(wsaaT2240Key.toString().trim());
		if(!itemfound){
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemfound = readT2240(wsaaT2240Key.toString().trim());
			if(!itemfound){
				syserrrec.params.set(t2240rec.t2240Rec);
				fatalError600();
			}
		}
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}//MTL002
			}
		}
		/* Read TR52D for Taxcode.                                         */
		sv.taxamt01Out[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamt01Out[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemfound = readTr52d(chdrpf.getReg().trim());
		if(!itemfound){
			itemfound = readTr52d("***");
			if(!itemfound){
				syserrrec.params.set(tr52drec.tr52dRec);
				fatalError600();
			}
		}
		/*                                                         <V74L01>*/
		itemfound = readTh5061600(chdrpf.getCnttype().trim());/* IJTI-1386 */
		/*    Read table TH506 to determine, whether the component         */
		/*    add anytime has been enabled for this contract type          */
		if(!itemfound){
			itemfound = readTh5061600("***");
			if(!itemfound){
				th506rec.th506Rec.set(SPACES);
			}
		}
		
		/* Read the PAYR record to get the Billing Details.                */

		//Ilife-3310 add by liwei
		payrpf = payrpfDAO.getpayrRecord(wsspcomn.company.toString(),chdrpf.getChdrnum(),covrpf.getPayrseqno());/* IJTI-1386 */
		if(payrpf == null){
			fatalError600();
		}
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		wsaaCrtable.set(covrpf.getCrtable());
		/*MOVE COVRMJA-SUMINS         TO WSAA-PREVIOUS-SUMINS.         */
		/* Release the coverage details for processing of Whole Plan.*/
		/*   i.e. If entering on an action of Component ADD then the*/
		/*        Component to be added must exist for all policies*/
		/*        within the plan.*/
		/*   i.e. If entering on an action of Component Modify/Enquiry*/
		/*        and Whole Plan selected then we will process the*/
		/*        component for all policies within the plan.*/
		if (isEQ(wsaaPlanSuffix,ZERO)) {
			covrpfDAO.deleteCacheObject(covrpf);
		
		}
		/*    If modify of Component requested and the component           */
		/*    in question is a Single premium Component then we move 'y'   */
		/*    to comp enquiry and spaces to comp modify and an error       */
		/*    informing the user that single premium component cannot      */
		/*    be modified.                                                 */
		if (modifyComp.isTrue()
		&& isEQ(payrpf.getBillfreq(),ZERO)) {
			wsaaFlag.set("I");
			wsccSingPremFlag.set("Y");
		}
		if (addComp.isTrue()
		&& isEQ(th506rec.indic,"Y")) {
			loadSusp1500();
		}
		
		dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011 /* IJTI-1386 */
		//ILIFE-3519:Start
			premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars, "IT");/* IJTI-1386 */
			if(premiumflag || dialdownFlag){
				callReadRCVDPF();
			}
			if(!premiumflag){
				sv.prmbasisOut[varcom.nd.toInt()].set("Y");
			}else{
				sv.prmbasisOut[varcom.nd.toInt()].set("N");
				if(rcvdPFObject!=null){
					if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
						sv.prmbasis.set(rcvdPFObject.getPrmbasis());
					}
				}
			}
				
			//ILIFE-3519:End
			//BRD-NBP-011 starts				
			if(!dialdownFlag)
			{	sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");			
			}
		
			else
			{
				sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
				if(rcvdPFObject != null){
					if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
						sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
					}
				}
			}
			//BRD-NBP-011 ends
			exclFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP020", appVars, "IT");/* IJTI-1386 */
			if(!exclFlag){
				sv.exclindOut[varcom.nd.toInt()].set("Y");
			}
			else{
				checkExcl();
			}
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getCrtable(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());/* IJTI-1386 */
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
	}
protected boolean readT2240(String keyItemitem) {
	boolean itemFound = false;
	List<Itempf> t2240List = itemDAO.getAllitemsbyCurrency("IT", wsspcomn.fsuco.toString(), "T2240", keyItemitem);
	if (t2240List != null && !t2240List.isEmpty()) {
		t2240rec.t2240Rec.set(StringUtil.rawToString(t2240List.get(0).getGenarea()));
		itemFound = true;
	}
	return itemFound;
}
protected boolean readTr52d(String keyItemitem) {
	boolean itemFound = false;
	List<Itempf> tr52dList = itemDAO.getAllitemsbyCurrency("IT", wsspcomn.company.toString(), "TR52D", keyItemitem);
	if (tr52dList != null && !tr52dList.isEmpty()) {
		tr52drec.tr52dRec.set(StringUtil.rawToString(tr52dList.get(0).getGenarea()));
		itemFound = true;
	}
	return itemFound;
}
protected void headerToScreen1040()
	{
		/* Load the screen header title.*/
 //ILIFE-8723 start	
		descpf=descDAO.getdescData("IT", "T5687", covrpf.getCrtable(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		if (descpf == null) {
			wsaaHedline.fill("?");
		}
		else {
		wsaaHedline.set(descpf.getLongdesc());
		}
	
		/*if(t5687Map != null){
			if(t5687Map.get(covrpf.getCrtable()) != null){ IJTI-1386 
				wsaaHedline.set(t5687Map.get(covrpf.getCrtable()).getLongdesc()); IJTI-1386 
			}else{
				wsaaHedline.fill("?");
			}
		}*/
 //ILIFE-8723 end
		/* The following will centre the title.*/
		loadHeading1100();
	}

protected void lifeDetails1050()
	{
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the life assured and joint-life details (if any) do*/
		/* the following;-*/
		/*  - Read  the life details using LIFEMJA (life number from*/
		/*    COVTMJA, joint life number '00').  Look up the name*/
		/*    from the  client  details  (CLTS)  and  format as a*/
		/*    "confirmation name".*/
		/*lifeList = lifepfDAO.getLifeData(covrpf.getChdrcoy().toString(), covrpf.getChdrnum().toString(), covrpf.getLife().toString(), "00");
		if (lifeList == null) {
			fatalError600();
		}
		lifepf = lifeList.get(0);*/
		lifepf = lifepfDAO.getLifeRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),"00");/* IJTI-1386 */
		if (lifepf== null ) {
			fatalError600();
		}
		/* Save Main Life details within Working Storage for later use.*/
		/*Calculation of age of life assured is incorrect...            */
		/* IF  ADD-COMP                                                 */
		/*     MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1           */
		/*     MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2           */
		/*     MOVE '01'                   TO DTC3-FREQUENCY            */
		/*     CALL 'DATCON3' USING DTC3-DATCON3-REC                    */
		/*     ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD       */
		/* ELSE                                                         */
		/*     MOVE LIFEMJA-ANB-AT-CCD     TO WSAA-ANB-AT-CCD.          */
		/*    Chdrmja-btdate is always used to calculate the age of life   */
		/*    assured.                                                     */
		/*    MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1          <022>*/
		/*MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2          <022>*/
		/*    MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2          <041>*/
		/*    MOVE '01'                   TO DTC3-FREQUENCY           <022>*/
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC                   <022>*/
		/*    ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD      <022>*/
		calcAge1200();
		wsaaAnbAtCcd.set(wsaaWorkingAnb);
		if (!addComp.isTrue()) {
			wsaaAnbAtCcd.set(lifepf.getAnbAtCcd());
		}
		wsaaCltdob.set(lifepf.getCltdob());
		wsaaSex.set(lifepf.getCltsex());
//ILIFE-8187 start
		clts=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifepf.getLifcnum());/* IJTI-1386 */
		if (clts == null) {
			fatalError600();
		}
		sv.lifcnum.set(lifepf.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/*  - read the joint life details using LIFEMJA (life number*/
		/*    from COVTMJA,  joint  life number '01').  If found,*/
		/*    look up the name from the client details (CLTS) and*/
		/*    format as a "confirmation name".*/
		
		lifeList = lifepfDAO.getLifeData(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");/* IJTI-1386 */
//ILIFE-8187 end
		if (lifeList != null && lifeList.size() == 0) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbAnbAtCcd.set(0);
			wsaaLifeind.set("S");
			wsbbSex.set(SPACES);
			return;
		}
		else {
			//IJTI-320 START
			if(lifeList != null){
				lifepf = lifeList.get(0);
			}
			//IJTI-320 END
			wsaaLifeind.set("J");
		}
		if (addComp.isTrue()) {
			/*        MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1           */
			/*   MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2           */
			/*        MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2      <041>*/
			/*        MOVE '01'                   TO DTC3-FREQUENCY            */
			/*        CALL 'DATCON3' USING DTC3-DATCON3-REC                    */
			/*        ADD .99999 DTC3-FREQ-FACTOR GIVING WSBB-ANB-AT-CCD       */
			calcAge1200();
			wsbbAnbAtCcd.set(wsaaWorkingAnb);
		}
		else {
			wsbbAnbAtCcd.set(lifepf.getAnbAtCcd());
		}
		wsbbCltdob.set(lifepf.getCltdob());
		wsbbSex.set(lifepf.getCltsex());
		clts=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifepf.getLifcnum());//ILIFE-8187
		sv.jlifcnum.set(lifepf.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void setScreen1060()
	{
		/* Set up the screen header details which are constant for all*/
		/*   policies within the plan.*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.numpols.set(chdrpf.getPolinc());
		sv.life.set(lifepf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		/*MOVE CHDRMJA-CNTCURR        TO SR571-CURRCD.*/
		sv.currcd.set(payrpf.getCntcurr());
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		covtpf = new Covtpf();
		covtpf.setChdrcoy(covrpf.getChdrcoy());
		covtpf.setChdrnum(covrpf.getChdrnum());
		covtpf.setLife(covrpf.getLife());
		covtpf.setCoverage(covrpf.getCoverage());
		covtpf.setRider(covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)
		&& !addComp.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			covrpf.setPlanSuffix(9999);
			covtpf.setPlnsfx(9999);
		
			covrpfList=covrpfDAO.searchCovrRecordForContract(covrpf);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanproc.set("Y");
			if (addComp.isTrue()) {
				/*NEXT_SENTENCE*/
				/**          MOVE ZEROES           TO COVRMJA-COVERAGE*/
				/**                                   COVRMJA-RIDER*/
			}
		}
		else {
			wsaaPlanproc.set("N");
			covtpf.setChdrcoy(covrpf.getChdrcoy());
			covtpf.setChdrnum(covrpf.getChdrnum());
			covtpf.setLife(covrpf.getLife().trim());
			covtpf.setCoverage(covrpf.getCoverage().trim());
			covtpf.setRider(covrpf.getRider());
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		/* If this is a component modify, check whether this component  */
		/* is subject to Automatic Increases (CPI-DATE holds a valid    */
		/* date).  If so, display a message stating that future         */
		/* increases will not be offered.                               */
		if (modifyComp.isTrue()
		&& isNE(covrpf.getCpiDate(),ZERO)
		&& isNE(covrpf.getCpiDate(),varcom.vrcmMaxDate)) {
			scrnparams.errorCode.set(errorsInner.f862);
		}
		/* The following 5000-section relates to the main coverage details*/
		/*   for each individual policy and as such will initially be load*/
		/*   -ed from here. But, if the Whole Plan has been selected or*/
		/*   Adding the Component then the 5000-section will be repeated*/
		/*   in the !000-section to load each policy.*/
		policyLoad5000();
		sv.taxamt02Out[varcom.pr.toInt()].set("Y");
		sv.taxamt01Out[varcom.pr.toInt()].set("Y");
		if(contnewBFlag){
		    if (modifyComp.isTrue() || compEnquiry.isTrue() || compApp.isTrue()) {
		    	covppf=covppfDAO.getCovppfCrtable(covtpf.getChdrcoy(), covtpf.getChdrnum(), covtpf.getCrtable(),
		    			covtpf.getCoverage(),covtpf.getRider());
		    	
		    	if(covppf != null){
		    		sv.covrprpse.set(covppf.getCovrprpse());
		    		sv.trcode.set(covppf.getTranCode());
		    		sv.dateeff.set(wsaaToday);
					descpf=descDAO.getdescData("IT", "T1688", sv.trcode.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
				    if (null!=descpf) {
				        sv.trcdedesc.set(descpf.getLongdesc());
				    }
				    else{
				    	sv.trcdedesc.set(SPACE);
				    }
		    	}
		    }
		}
	}

protected void commisionSplit1070()
	{
		/* Check pcdtmja file for commission split details                 */
		/* If they exist '+' needed in check-box.                          */
		checkPcdt5700();
		if (isNE(tr52drec.txcode, SPACES)
		&& isEQ(t5687rec.bbmeth, SPACES)) {
			sv.taxamt01Out[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamt01Out[varcom.pr.toInt()].set("N");
			sv.taxindOut[varcom.pr.toInt()].set("N");
			a200CheckCalcTax();
		}
	}

protected void loadHeading1100()
	{
		/* Count the number of spaces at the end of the line*/
		/*   (this is assuming there are none at the beginning)*/
		wsaaHeading.set(SPACES);
		for (wsaaX.set(30); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
	{
			/* No processing required. */
		}
		/*VARYING WSAA-X           FROM 24 BY -1                    */
		/*SUBTRACT WSAA-X             FROM 24 GIVING WSAA-Y.           */
		compute(wsaaY, 0).set(sub(30,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/* WSAA-X is the size of the heading string*/
		/* WSAA-Y is the number of spaces in the front*/
		/* WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			wsaaZ.add(1);
			compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
			wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
		}
		/* WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
	}

protected void calcAge1200()
	{
		/* New routine to calculate Age next/nearest/last birthday         */
		AgecalcPojo agecalcPojo = new AgecalcPojo();
		agecalcPojo.setFunction("CALCP");
		agecalcPojo.setLanguage(wsspcomn.language.toString());
		agecalcPojo.setCnttype(chdrpf.getCnttype());//ILIFE-8187
		agecalcPojo.setIntDate1(Integer.toString(lifepf.getCltdob()));
		agecalcPojo.setIntDate2(Integer.toString(payrpf.getBtdate()));
		agecalcPojo.setCompany(wsspcomn.fsuco.toString());
		agecalcUtils.calcAge(agecalcPojo);
	
		/* TSD-308 START */
		//TODO:
		if (isFeaAllowed) {
			caluculateAnniversarydate(); // TSD-308
			
			agecalcPojo.setIntDate2(wsaaLastAnnv.toString());	// TSD-308
			if (addComp.isTrue()) {	
				agecalcPojo.setIntDate2(wsaaMnthVers.toString());	
			}
			caluclateTransactionDate(); //ILIFE-2366
			agecalcPojo.setIntDate2(datcon1rec.intDate.toString()); //ILIFE-2366
		}
		/* TSD-308 END */
		wsaaAnb.set(lifepf.getAnbAtCcd());
		if (isNE(agecalcPojo.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agecalcPojo.toString());
			syserrrec.statuz.set(agecalcPojo.getStatuz());
			fatalError600();
		}
		wsaaWorkingAnb.set(agecalcPojo.getAgerating());
		/*LOGGER.info("=====> Component Add Proposal | RCD Date = " + chdrmjaIO.getOccdate()); // TSD-308
		LOGGER.info("=====> Component Add Proposal | Transaction Date = " + datcon1rec.intDate); // TSD-308
		LOGGER.info("=====> Component Add Proposal | Client Birth Date = " + lifemjaIO.getCltdob());	// TSD-308
		LOGGER.info("=====> Component Add Proposal | Monthiversary  = " + wsaaMnthVers); // TSD-308
		LOGGER.info("=====> Component Add Proposal | Last Anniversary  = " + wsaaLastAnnv); // TSD-308
		LOGGER.info("=====> Component Add Proposal | Attained Age  = " + agecalcrec.agerating); */// TSD-308
	}

/** TSD-308 Start **/
protected void caluclateTransactionDate(){
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	if (isNE(datcon1rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon1rec.datcon1Rec);
		syserrrec.statuz.set(datcon1rec.statuz);
		fatalError600();
	}
	
}

protected void calculateMonthiversary(int crrcd){
	datcon3rec.intDate1.set(chdrpf.getOccdate());
	datcon3rec.intDate2.set(datcon1rec.intDate);
	datcon3rec.frequency.set("12");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	ZonedDecimalData var = new ZonedDecimalData(11, 5).init(0);
	compute(var).set(add(datcon3rec.freqFactor, .99999));

	datcon2rec.intDate1.set(crrcd);
	datcon2rec.frequency.set("12");
	datcon2rec.freqFactor.set(var);
	
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	wsaaMnthVers.set(datcon2rec.intDate2);

}
protected void caluculateAnniversarydate(){
	
	datcon4rec.datcon4Rec.set(SPACES);
	datcon4rec.frequency.set("01");
	datcon4rec.freqFactor.set(001); 
	//Modified for ILIFE-2181 Start 
	/*datcon4rec.intDate1.set(covrIO.getCurrfrom());                       
	datcon4rec.billmonth.set(covrIO.getCurrfrom().toStringRaw().substring(4, 6));
	datcon4rec.billday.set(covrIO.getCurrfrom().toStringRaw().substring(6, 8));   */             
	if (addComp.isTrue()) {            
		datcon4rec.intDate1.set(wsspcomn.currfrom);
		datcon4rec.billmonth.set(wsspcomn.currfrom.toStringRaw().substring(4, 6));
		datcon4rec.billday.set(wsspcomn.currfrom.toStringRaw().substring(6, 8));
	}
	else {
		//ILIFE-3949 STARTS
		String currFrom = "00000000";
		if(covrpf != null){
			currFrom = covrpf.getCurrfrom() + "";
		}
		datcon4rec.intDate1.set(currFrom);
		datcon4rec.billmonth.set(currFrom.substring(4, 6));
		datcon4rec.billday.set(currFrom.substring(6, 8));
		//ENDS
	}
	//Modified for ILIFE-2181 End 
	while ( !(isGT(datcon4rec.intDate2,wsaaMnthVers))) {
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		wsaaLastAnnv.set(datcon4rec.intDate1);
		datcon4rec.intDate1.set(datcon4rec.intDate2);
	}	
}
/** TSD-308 End **/
protected void setupBonus1300()
	{
		/* Check if Coverage/Rider is a SUM product. If not SUM product    */
		/* protect field.                                                  */
		/* If SUM product and default exists setup BAPPMETH and protect    */
		/* field. If SUM product and default not setup allow entry.        */
		sv.bappmethOut[varcom.pr.toInt()].set("N");
		sv.bappmethOut[varcom.nd.toInt()].set("N");
	//ILIFE-3310 by liwei
		Itempf itemReturn = readT6005(covtpf.getCrtable(),covtpf.getChdrcoy());//IJTI-1793
		if(itemReturn == null){
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(StringUtil.rawToString(itemReturn.getGenarea()));
		
		if (isEQ(t6005rec.ind,"1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
	}


//ILIFE-3310 by liwei
protected Itempf readT6005(String itemitme,String itemcoy){
	Itempf itempf = null;
	List<Itempf> t6005List = itemDAO.getAllitemsbyCurrency("IT", itemcoy, "T6005", itemitme);
	if (t6005List != null && !t6005List.isEmpty()) {
		itempf = t6005List.get(0);
	}
	return itempf;
}
protected void getNextEffdate1400()
	{
		/*GET-EFFDATE*/
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(chdrpf.getBillfreq());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaTempPrev.set(datcon2rec.intDate1);
		datcon2rec.intDate1.set(datcon2rec.intDate2);
		wsaaFreq.add(1);
		/*EXIT*/
	}

protected void loadSusp1500()
 {
		cvuwpf = cvuwpfDAO.getCvuwpfRecord(chdrpf.getChdrcoy().toString(),
				chdrpf.getChdrnum());/* IJTI-1386 */
		if (cvuwpf != null) {
			sv.effdate.set(cvuwpf.getEffdate());
		}
		cprppf = new Cprppf();
		cprppf.setChdrpfx(chdrpf.getChdrpfx());/* IJTI-1386 */
		cprppf.setChdrcoy(chdrpf.getChdrcoy().toString());
		cprppf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		cprppf.setLife("99");
		cprppf.setCoverage("00");
		cprppf.setRider("00");
		cprppf = cprppfDAO.getCprpRecord(cprppf);
		if (cprppf == null ) {
			syserrrec.params.set(chdrpf.getChdrpfx().concat(chdrpf.getChdrcoy().toString())
					.concat(chdrpf.getChdrnum()));/* IJTI-1386 */
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
		wsaaSuspAvail.set(cprppf.getInstprem());
	}


protected boolean readTh5061600(String keyItemitem) {
		boolean itemFound = false;
		List<Itempf> th506List = itemDAO.getAllitemsbyCurrency("IT", wsspcomn.company.toString(), "TH506", keyItemitem);
		if (th506List != null && !th506List.isEmpty()) {
			th506rec.th506Rec.set(StringUtil.rawToString(th506List.get(0).getGenarea()));
			itemFound = true;
		}
		return itemFound;
	}

protected void proratePremium1600()
	{
		/*    CALCULATE THE FRACTION IN THE DUE BETWEEN PREV & NEXT DUE.   */
		datcon3rec.intDate1.set(wsspcomn.currfrom);
		datcon3rec.intDate2.set(wsaaNextDue);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaDay1.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(wsaaPrevDue);
		datcon3rec.intDate2.set(wsaaNextDue);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaDay2.set(datcon3rec.freqFactor);
		compute(wsaaProrate, 6).set(div(wsaaDay1,wsaaDay2));
		/*    MAY BE MORE THAN 1 DUE INVOLVED WHEN YOU CONSIDER FROM       */
		/*    MAY BE MORE THAN 1 DUE INVOLVED                              */
		if (isLT(wsaaNextDue,chdrpf.getBtdate())) {
			wsaaFreq.set(ZERO);
			datcon2rec.intDate1.set(wsaaNextDue);
			datcon2rec.intDate2.set(wsaaNextDue);
			while ( !(isGT(datcon2rec.intDate2,chdrpf.getBtdate()))) {
				getNextEffdate1400();
			}
			compute(wsaaProrate, 6).set(sub(add(wsaaProrate,wsaaFreq),1));
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preStart()
	{
		/* Skip this section  if  returning  from  an optional selection   */
		/* (current stack position action flag = '*').                     */
		/*   i.e. returning from Options and Extras data screen.           */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (singlePremium.isTrue()) {
			scrnparams.errorCode.set(errorsInner.f008);
		}
		/*MOVE 'SUMIN   '          TO SCRN-POSITION-CURSOR.            */
		wsaaOldSumins.set(sv.sumin);
		wsaaOldPrem.set(sv.instPrem);
		/* Test the POVR file for the existance of a Premium Breakdown     */
		/* record. If one exists then allow the option to display the      */
		/* select window for enquiry....if not then protect.               */
		/* Test here so that the latest POVR is retreived when either      */
		/* returning from a selection or having calculated new values.     */
		
		Povrpf povrIO = new Povrpf();
		povrIO.setChdrcoy(wsaaPovrChdrcoy.toString());
		povrIO.setChdrnum(wsaaPovrChdrnum.toString());
		povrIO.setLife(wsaaPovrLife.toString());
		povrIO.setCoverage(wsaaPovrCoverage.toString());
		povrIO.setRider(wsaaPovrRider.toString());
		povrIO = povrpfDAO.getPovrRecord(povrIO);
		if (povrIO == null) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
		/* Check the date terms for error and enable correction by         */
		/* re-setting the date fields. The dates are then re-set           */
		/* based on chanded age or term fields...                          */
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()],"Y")
		&& !compEnquiry.isTrue()
		&& (isNE(sv.pcesdteErr,SPACES)
		|| isNE(sv.rcesdteErr,SPACES))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
		}
		if (CSMIN003Permission &&
				((isEQ(wsaaBatckey.batcBatctrcde, t6a7) ||isEQ(wsaaBatckey.batcBatctrcde, t6a8)))) {
			sv.fuindOut[varcom.nd.toInt()].set("N");
			sv.fuindOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.fuind.set("+");
			sv.fuindOut[varcom.nd.toInt()].set("Y");
			sv.fuindOut[varcom.pr.toInt()].set("Y");
		}
		if(compEnquiry.isTrue()){
			scrnparams.function.set(varcom.prot);//IBPLIFE-2134
		}
	}

protected void callScreenIo2010()
	{
		/* Display the screen.                                             */
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
					validate2020();
				case cont2030: 
					cont2030();
				case beforeExit2080: 
					beforeExit2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'SR571IO' USING        SCRN-SCREEN-PARAMS               */
		/*                                SR571-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested*/
		/* then skip the remainder of the validation.*/
		wsaaIfSuminsChanged.set("N");
		wsaaIfPremChanged.set("N");
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* If Component Enquiry then skip validation.                      */
		/*    IF  COMP-ENQUIRY                                     <V4L004>*/
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.cont2030);
		}
		/* Check the situation where the date fields are protected but     */
		/* age and term fields differ from the last call to the premium    */
		/* calculation routine. Blank the dates if ages or terms have      */
		/* changed hence ensuring a re-calculation of dates & premium.     */
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()],"Y")
		&& (isNE(sv.premCessAge,wsaaPcesageStore)
		|| isNE(sv.riskCessAge,wsaaRcesageStore)
		|| isNE(sv.premCessTerm,wsaaPcestermStore)
		|| isNE(sv.riskCessTerm,wsaaRcestermStore))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
		}
	}

protected void validate2020()
	{
		/* If  in  enquiry  mode,  skip  all field validation EXCEPT the*/
		/* options/extras indicator.*/
		/*    IF NOT COMP-ENQUIRY                                          */
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			editCoverage2100();
			validateAllowedCovergaeDuration();
			if(NBPRP056Permission) {
				validateOccupationOrOccupationClass();
			}
		}
	}

protected void cont2030()
	{
		/* If  options/extras already exist, there will be a '+' in this*/
		/*   field. A request  to access the details is made by entering*/
		/*   'X'. No other  values  (other  than  blank) are allowed. If*/
		/*   options and extras  are  requested,  DO  NOT  CALCULATE THE*/
		/*   PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind,"+")
		&& isNE(sv.optextind,"X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		if (isNE(sv.comind, " ")
		&& isNE(sv.comind,"+")
		&& isNE(sv.comind,"X")) {
			sv.comindErr.set(errorsInner.g620);
		}
		/* Check the Taxcode indicator.                                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/* Check the premium breakdown indicator.                          */
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind,"+")
		&& isNE(sv.pbind,"X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
		&& isNE(sv.exclind, "+")
		&& isNE(sv.exclind, "X")) {
			sv.exclindErr.set(errorsInner.g620);
		}
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		/* If  reassurance   already exists, there will be a '+' in this   */
		/* field.  A  request  to access the details is made by entering   */
		/* 'X'.  No  other  values  (other  than  blank) are allowed.      */
		/* IF SR571-RATYPIND           NOT = ' ' AND            <R96REA>*/
		/*                             NOT = '+' AND            <R96REA>*/
		/*                             NOT = 'X'                <R96REA>*/
		/*    MOVE G620                TO SR571-RATYPIND-ERR.   <R96REA>*/
		/*                                                      <R96REA>*/
		/* If Component Enquiry then skip validation.                      */
		/*    IF  COMP-ENQUIRY                                     <V4L004>*/
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.beforeExit2080);
		}
		/*If sum assured is changed and reassurance details exist <R96REA>*/
		/*to go onto the reassuranc screen, to check if sum assure<R96REA>*/
		/*still greater then the amount to be reassured ..        <R96REA>*/
		/*                                                      <R96REA>*/
		/* IF MODIFY-COMP                                       <R96REA>*/
		/*  IF SR571-RATYPIND               = '+'               <R96REA>*/
		/*    IF SR571-SUMIN           NOT = WSAA-PREVIOUS-SUMIN<R96REA>*/
		/*       IF SR571-SUMIN        NOT = WSAA-OLD-SUMINS    <R96REA>*/
		/*       MOVE 'X'              TO SR571-RATYPIND        <R96REA>*/
		/*       MOVE SR571-SUMIN      TO WSAA-PREVIOUS-SUMINS.    <020>*/
		/*       MOVE SR571-SUMIN      TO WSAA-PREVIOUS-SUMINS  <R96REA>*/
		/* END-IF.                                              <R96REA>*/
		if (isNE(sv.sumin,wsaaOldSumins)) {
			compute(wsaaSuminsDiff, 0).set(sub(sv.sumin,wsaaOldSumins));
			if (isLT(wsaaSuminsDiff,0)) {
				compute(wsaaSuminsDiff, 0).set(mult(wsaaSuminsDiff,-1));
				wsaaIfSuminsChanged.set("D");
			}
			else {
				wsaaIfSuminsChanged.set("I");
			}
		}
		/*    IF WSAA-FIRST-TIME       = 'N'                               */
		if (isNE(sv.instPrem,wsaaOldPrem)) {
			compute(wsaaPremDiff, 2).set(sub(sv.instPrem,wsaaOldPrem));
			if (isLT(wsaaPremDiff,0)) {
				compute(wsaaPremDiff, 2).set(mult(wsaaPremDiff,-1));
				wsaaIfPremChanged.set("D");
			}
			else {
				wsaaIfPremChanged.set("I");
			}
		}
		processCashValueInArrears();// fwang3 ICIL-560
		/* If everything else is O-K calculate the Installment Premium.*/
		if (isNE(sv.optextind,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& addComp.isTrue()) {
			calcPremium2200();
		}
		if (isNE(sv.optextind,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& isNE(sv.comind,"X")
		&& modifyComp.isTrue()) {
			/*AND (WSAA-PREM-CHANGED OR WSAA-SUMINS-CHANGED)               */
			calcPremium2200();
		}
	}

/**
 * fwang3 ICIL-560
 */
private void processCashValueInArrears() {
	// fwang3 ICIL-560 judge if the component has cash value and than code is come from submenu
	if (chinaLocalPermission && isEQ(wsaaFlag, "M") && hasCashValue && isEQ("T6A8", wsaaBatckey.batcBatctrcde)) {
		if (isLT(sv.sumin, wsaaCashValArr)) {
			sv.suminErr.set(errorsInner.rrj4);// ICIL-299
		}
		sv.cashvalarer.set(ZERO);
		if (isGT(sv.sumin, wsaaHoldSumin)) {
			int years = DateUtils.calYears(chdrpf.getOccdate().toString(), chdrpf.getPtdate().toString());
			PackedDecimalData data = new PackedDecimalData(11, 5).init(100000);
			compute(sv.cashvalarer,2).setRounded(mult(div(th528rec.insprm[years + 1], data), sub(sv.sumin, wsaaHoldSumin)));//fix ICIL-706
		}
	}
}

protected void beforeExit2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* If 'CALC' was entered then re-display the screen.*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void editCoverage2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkSumin2110();
				case checkSuminCont2120: 
					checkSuminCont2120();
				case checkRcessFields2120: 
					checkRcessFields2120();
					checkPcessFields2130();
					checkAgeTerm2140();
				case validDate2140: 
					validDate2140();
				case datesCont2140: 
					datesCont2140();
				case ageAnniversary2140: 
					ageAnniversary2140();
				case check2140: 
					check2140();
				case checkOccurance2140: 
					checkOccurance2140();
				case checkTermFields2150: 
					checkTermFields2150();
				case checkComplete2160: 
					checkComplete2160();
				case checkMortcls2170: 
					checkMortcls2170();
				case loop2175: 
					loop2175();
				case checkLiencd2180: 
					checkLiencd2180();
				case loop2185: 
					loop2185();
				case checkMore2190: 
					checkMore2190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkSumin2110()
	{
		a100CheckLimit();
		/* Check sum insured ranges.*/
		if (isEQ(t5608rec.sumInsMax,t5608rec.sumInsMin)) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkRcessFields2120);
		}
		/*                                                        <044>*/
		/*IF ADD-COMP                                             <044>*/
		/*   IF SR571-SUMIN              = 0                      <044>*/
		/*      AND                                               <044>*/
		/*      SR571-INST-PREM          = 0                      <044>*/
		/*      MOVE  H933              TO SR571-SUMIN-ERR        <044>*/
		/*                              SR571-INSTPRM-ERR         <044>*/
		/*      GO TO 2120-CHECK-SUMIN-CONT                       <044>*/
		/*   END-IF                                               <044>*/
		/*END-IF.                                                 <044>*/
		if (isEQ(sv.sumin,covrpf.getSumins())) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkSuminCont2120);
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin,chdrpf.getPolinc()),chdrpf.getPolsum())));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
	}

protected void checkSuminCont2120()
	{
		if (isLT(wsaaSumin,t5608rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin,t5608rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

	/**
	* <pre>
	***  IF WSAA-SUMIN              < T5608-SUM-INS-MIN
	***     MOVE E417                TO SR571-SUMIN-ERR.
	* </pre>
	*/
protected void checkRcessFields2120()
	{
		/* Check the consistency of the risk age and term fields and*/
		/*   premium age and term fields. Either, risk age and premium*/
		/*   age must be used or risk term and premium term must be used.*/
		/*   They must not be combined.*/
		/* Note that these only need validating if they were not defaulted*/
		/*     NOTE: Age and Term fields may now be mixed.                 */
		if (isEQ(sv.select,"J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent,SPACES)
		&& jointlif.isTrue()) {
			checkDefaults5300();
		}
		if (isGT(sv.riskCessAge,0)
		&& isGT(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(t5608rec.eaage,SPACES)
		&& isEQ(sv.riskCessAge,0)
		&& isEQ(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2130()
	{
		if (isGT(sv.premCessAge,0)
		&& isGT(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)
		&& isEQ(sv.rcessageErr,SPACES)
		&& isEQ(sv.rcesstrmErr,SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5608rec.eaage,SPACES)
		&& isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2140()
	{
		if ((isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))
		|| (isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		/*    IF SR571-RISK-CESS-AGE      > 0 AND                          */
		/*       SR571-PREM-CESS-AGE      = 0                              */
		/*       MOVE F224                TO SR571-PCESSAGE-ERR            */
		/*       IF NOT DEFAULT-RA                                         */
		/*          MOVE F224             TO SR571-RCESSAGE-ERR.           */
		/*    IF SR571-RISK-CESS-TERM     > 0 AND                          */
		/*       SR571-PREM-CESS-TERM     = 0                              */
		/*       MOVE F225                TO SR571-PCESSTRM-ERR            */
		/*       IF NOT DEFAULT-RT                                         */
		/*          MOVE F225             TO SR571-RCESSTRM-ERR.           */
		if ((isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))
		|| (isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		/* To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (T5608).*/
		/*    IF T5608-EAAGE              NOT = SPACES                     */
		/*    OR SR571-RISK-CESS-DATE     = VRCM-MAX-DATE                  */
		/*       PERFORM 5400-RISK-CESS-DATE                               */
		/*    ELSE                                                         */
		/*       IF SR571-RCESDTE-ERR     = SPACES                         */
		/*          IF SR571-RISK-CESS-AGE NOT = 0                         */
		/*             MOVE WSZZ-CLTDOB   TO DTC2-INT-DATE-1               */
		/*             MOVE SR571-RISK-CESS-AGE                            */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*             PERFORM 5500-CALL-DATCON2                           */
		/*                IF DTC2-INT-DATE-2 < SR571-RISK-CESS-DATE        */
		/*                   MOVE U029    TO SR571-RCESSAGE-ERR            */
		/*                                   SR571-RCESDTE-ERR             */
		/*                ELSE                                             */
		/*                   SUBTRACT 1   FROM DTC2-FREQ-FACTOR            */
		/*                   PERFORM 5500-CALL-DATCON2                     */
		/*                   IF DTC2-INT-DATE-2 NOT < SR571-RISK-CESS-DATE */
		/*                      MOVE U029 TO SR571-RCESSAGE-ERR            */
		/*                                   SR571-RCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      NEXT SENTENCE                              */
		/*          ELSE                                                   */
		/*             IF SR571-RISK-CESS-TERM NOT = 0                     */
		/*                IF NOT ADD-COMP                                  */
		/*                    MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1  */
		/*                ELSE                                             */
		/*                    MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1  */
		/*                MOVE SR571-RISK-CESS-TERM                        */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*                PERFORM 5500-CALL-DATCON2                        */
		/*                   IF DTC2-INT-DATE-2 < SR571-RISK-CESS-DATE     */
		/*                      MOVE U029 TO SR571-RCESSTRM-ERR            */
		/*                                   SR571-RCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      SUBTRACT 1 FROM DTC2-FREQ-FACTOR           */
		/*                      PERFORM 5500-CALL-DATCON2                  */
		/*                      IF DTC2-INT-DATE-2                         */
		/*                                   NOT < SR571-RISK-CESS-DATE    */
		/*                         MOVE U029 TO SR571-RCESSTRM-ERR         */
		/*                                      SR571-RCESDTE-ERR.         */
		/*    IF T5608-EAAGE              NOT = SPACES                     */
		/*    OR SR571-PREM-CESS-DATE     = VRCM-MAX-DATE                  */
		/*       PERFORM 5450-PREM-CESS-DATE                               */
		/*    ELSE                                                         */
		/*       IF SR571-PCESDTE-ERR     = SPACES                         */
		/*          IF SR571-PREM-CESS-AGE NOT = 0                         */
		/*             MOVE WSZZ-CLTDOB   TO DTC2-INT-DATE-1               */
		/*             MOVE SR571-PREM-CESS-AGE                            */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*             PERFORM 5500-CALL-DATCON2                           */
		/*                IF DTC2-INT-DATE-2 < SR571-PREM-CESS-DATE        */
		/*                   MOVE U029    TO SR571-PCESSAGE-ERR            */
		/*                                   SR571-PCESDTE-ERR             */
		/*                ELSE                                             */
		/*                   SUBTRACT 1   FROM DTC2-FREQ-FACTOR            */
		/*                   PERFORM 5500-CALL-DATCON2                     */
		/*                   IF DTC2-INT-DATE-2 NOT < SR571-PREM-CESS-DATE */
		/*                      MOVE U029 TO SR571-PCESSAGE-ERR            */
		/*                                   SR571-PCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      NEXT SENTENCE                              */
		/*          ELSE                                                   */
		/*             IF SR571-PREM-CESS-TERM NOT = 0                     */
		/*                IF NOT ADD-COMP                                  */
		/*                   MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1   */
		/*                ELSE                                             */
		/*                   MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1   */
		/*                MOVE SR571-PREM-CESS-TERM                        */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*                PERFORM 5500-CALL-DATCON2                        */
		/*                   IF DTC2-INT-DATE-2 < SR571-PREM-CESS-DATE     */
		/*                      MOVE U029 TO SR571-PCESSTRM-ERR            */
		/*                                   SR571-PCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      SUBTRACT 1   FROM DTC2-FREQ-FACTOR         */
		/*                      PERFORM 5500-CALL-DATCON2                  */
		/*                      IF DTC2-INT-DATE-2                         */
		/*                                NOT < SR571-PREM-CESS-DATE       */
		/*                         MOVE U029 TO SR571-PCESSTRM-ERR         */
		/*                                      SR571-PCESDTE-ERR.         */
		if (isEQ(sv.riskCessDate,varcom.vrcmMaxDate)) {
			riskCessDate5400();
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			premCessDate5450();
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate,sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isEQ(sv.rider,"00")) {
			goTo(GotoLabel.datesCont2140);
		}
		wsaaStorePlanSuffix.set(covrpf.getPlanSuffix());
		/* Read the Temporary COVT file for the Coverage record which must */
		/* have been written before a rider can be selected.               */

		List<Covtpf> covtList = covtpfDAO.getCovtpfData(covrpf.getChdrcoy(),
			chdrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(),"00");
		if(!covtList.isEmpty()){
			covtpf = covtList.get(0);
			if(covtpf!=null){//ILIFE-9505
				goTo(GotoLabel.validDate2140);
			}
		}
		
		/* Read the Temporary COVT file for the Coverage record which must */
		/* have been written before a rider can be selected.               */
		/* if a policy that was summarised was selected a                  */
		/* the plan-suffix was in correct (fell over because               */
		/* could not find a record with that plan-suffix)                  */
		/* need to access the summaried reccord (plan-suffix of 0)         */
		if (modifyComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(),ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGT(chdrpf.getPolsum(),covrpf.getPlanSuffix())
				|| isEQ(chdrpf.getPolsum(),covrpf.getPlanSuffix())) {
					covrpf.setPlanSuffix(0);
				}
			}
		}
		/* IF ADD-COMP                                                  */
		/*    IF CHDRMJA-POLSUM              = ZEROS               <032>*/
		/*       MOVE 1                      TO COVRMJA-PLAN-SUFFIX<032>*/
		/*    ELSE                                                 <032>*/
		/*       MOVE 0                      TO COVRMJA-PLAN-SUFFIX.<032*/
		if (addComp.isTrue()) {
			covrpf.setPlanSuffix(0);
			if (isEQ(chdrpf.getPolsum(),ZERO)) {
				if (isEQ(chdrpf.getPolinc(),1)) {
					covrpf.setPlanSuffix(0);
				}
				else {
					covrpf.setPlanSuffix(1);
				}
			}
			else {
				covrpf.setPlanSuffix(0);
			}
		}
		covrpf.setRider("00");
		//ILB-456
//ILIFE-8187 start
		/*covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),"1");
		if (covrpf == null) {
			fatalError600();
		}*/
		covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),covrpf.getValidflag());
		if (covrpf == null) {
			fatalError600();
		}
//ILIFE-8187 end
		if(covtpf!=null){
			covtpf.setRcesdte(covrpf.getRiskCessDate()); 
			covtpf.setPcesdte(covrpf.getPremCessDate());
		}
	}

protected void validDate2140()
	{
		/* Validate that the riders Risk and Premium Cessation dates are   */
		/* not greater than the Coverages to which it is attached. A rider */
		/* can not mature after its driving coverage.                      */
		if (isGT(sv.riskCessDate,covtpf.getRcesdte())) {
			sv.rcesdteErr.set(errorsInner.h033);
		}
		if (isGT(sv.premCessDate,covtpf.getPcesdte())) {
			sv.pcesdteErr.set(errorsInner.h044);
		}
		/* Re-Read the Rider record in order to position the file and data */
		/* in the correct position for the information to be processed.    */
		/* (in regard to the covt file)                                    */
		/*    MOVE COVRMJA-LIFE           TO COVTMJA-LIFE.                 */
		/*    MOVE COVRMJA-COVERAGE       TO COVTMJA-COVERAGE.             */
		/*    MOVE COVRMJA-RIDER          TO COVTMJA-RIDER.                */
		/*    MOVE COVRMJA-SEQNBR         TO COVTMJA-SEQNBR.               */
		List<Covtpf> covtList = covtpfDAO.getCovtpfData(covrpf.getChdrcoy(),chdrpf.getChdrnum(),
				sv.life.toString().trim(),sv.coverage.toString().trim(),sv.rider.toString().trim(),covtpf.getPlnsfx());
		if(!covtList.isEmpty()){
			covtpf = covtList.get(0);
		}	
		/* Re-Read the Rider record in order to position the file and data */
		/* in the correct position for the information to be processed.    */
		/* (in regard to the  covr file).....                              */
		covrpf.setChdrcoy(covtpf.getChdrcoy());
		covrpf.setChdrnum(covtpf.getChdrnum());
		covrpf.setLife(sv.life.toString());
		covrpf.setCoverage(sv.coverage.toString());
		covrpf.setRider(sv.rider.toString());
		covrpf.setPlanSuffix(wsaaStorePlanSuffix.toInt());
		//ILB-456 starts
		covrpfList = covrpfDAO.searchCovrpfRecord(covrpf);
		if (covrpfList.isEmpty()) {
			goTo(GotoLabel.datesCont2140);
		}
	
	}

protected void datesCont2140()
	{
		/* If modify mode and the record being modified is the coverage    */
		/* check that the cessation dates are not less than those on the   */
		/* riders.                                                         */
		/* Once the check has been completed re read the COVR to ensure    */
		/* the correct record details are retained                         */
		if (isEQ(sv.rider,"00")
		&& modifyComp.isTrue()) {
			Covrpf covrrgwIO = new Covrpf();
			/* IJTI-1386 START*/
			covrrgwIO.setChdrcoy(covrpf.getChdrcoy());
			covrrgwIO.setChdrnum(covrpf.getChdrnum());
			covrrgwIO.setLife(covrpf.getLife());
			covrrgwIO.setCoverage(covrpf.getCoverage());
			covrrgwIO.setRider(covrpf.getRider());
			covrrgwIO.setPlanSuffix(covrpf.getPlanSuffix());
			/* IJTI-1386 END*/
			List<Covrpf> covrppfList = covrpfDAO.searchCovrpfRecord(covrrgwIO);
			for(Covrpf c:covrppfList){
				/*  Check the riders for cessation dates greater than the          */
				/*  coverage cessation date. As soon as an invalid date is         */
				/*  found display error. On finding an error the user will         */
				/*  have to exit th coverage screen and change the riders          */
				/*  cessation dates before changing the coverage cessation dates   */
				if (isNE(covrrgwIO.getChdrcoy(),wsaaCovrChdrcoy)
				|| isNE(covrrgwIO.getChdrnum(),wsaaCovrChdrnum)
				|| isNE(covrrgwIO.getLife(),wsaaCovrLife)
				|| isNE(covrrgwIO.getCoverage(),wsaaCovrCoverage)) {
					break;
				}
				if (isGT(covrrgwIO.getRiskCessDate(),sv.riskCessDate)) {
					sv.rcesdteErr.set(errorsInner.h033);
					break;
				}
				if (isGT(covrrgwIO.getPremCessDate(),sv.premCessDate)) {
					sv.pcesdteErr.set(errorsInner.h044);
					break;
				}
			}
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		/*  Calculate cessasion age and term.*/
		/* MOVE WSZZ-CLTDOB            TO DTC3-INT-DATE-1.              */
		/* MOVE SR571-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-AGE.           */
		/* MOVE SR571-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-AGE.           */
		/* MOVE CHDRMJA-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE SR571-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-TERM.          */
		/* MOVE SR571-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-TERM.          */
		if (isEQ(t5608rec.eaage,"A")) {
			goTo(GotoLabel.ageAnniversary2140);
		}
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		goTo(GotoLabel.check2140);
	}

protected void ageAnniversary2140()
	{
		if (isEQ(sv.riskCessAge,ZERO)) {
			/*       MOVE PAYR-BTDATE           TO DTC3-INT-DATE-1             */
			datcon3rec.intDate1.set(chdrpf.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			compute(wszzRiskCessAge, 5).set(add(datcon3rec.freqFactor,wsaaAnb));
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			/*       MOVE PAYR-BTDATE           TO DTC3-INT-DATE-1             */
			datcon3rec.intDate1.set(chdrpf.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			compute(wszzPremCessAge, 5).set(add(datcon3rec.freqFactor,wsaaAnb));
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
	}

protected void check2140()
	{
		/* When calculating SR571-RISK-CESS-DATE, it checks to see if the  */
		/* transaction is add or modify and use different dates            */
		/* accordingly. We must do the same here.                          */
		if (addComp.isTrue()) {
			/*        MOVE PAYR-BTDATE        TO DTC3-INT-DATE-1               */
			datcon3rec.intDate1.set(chdrpf.getOccdate());
		}
		else {
			datcon3rec.intDate1.set(covrpf.getCrrcd());
		}
		if (isEQ(sv.riskCessTerm,ZERO)
		|| addComp.isTrue()) {
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm,ZERO)
		|| addComp.isTrue()) {
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		/* Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
	}

protected void checkOccurance2140()
	{
		/* Check each possible option.*/
		x.add(1);
		if (isGT(x,wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2160);
		}
		if ((isEQ(t5608rec.ageIssageFrm[x.toInt()],0)
		&& isEQ(t5608rec.ageIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,t5608rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,t5608rec.ageIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkTermFields2150);
		}
		if (isGTE(wszzRiskCessAge,t5608rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge,t5608rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge,t5608rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge,t5608rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2150()
	{
		if ((isEQ(t5608rec.termIssageFrm[x.toInt()],0)
		&& isEQ(t5608rec.termIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,t5608rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,t5608rec.termIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkOccurance2140);
		}
		if (isGTE(wszzRiskCessTerm,t5608rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm,t5608rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm,t5608rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm,t5608rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2140);
	}

protected void checkComplete2160()
	{
		if (isNE(sv.rcesstrmErr,SPACES)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr,SPACES)
		&& isEQ(sv.premCessTerm,ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr,SPACES)
		&& isEQ(sv.riskCessAge,ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr,SPACES)
		&& isEQ(sv.premCessAge,ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2170()
	{
		/* Mortality-Class, if the mortality class appears on a coverage/*/
		/*   rider screen it is a compulsory field because it will be used*/
		/*   in calculating the premium amount. The mortality class*/
		/*   entered must one of the ones in the edit rules table.*/
		if (isEQ(sv.mortclsOut[varcom.pr.toInt()],"Y")) {
			goTo(GotoLabel.checkLiencd2180);
		}
		x.set(0);
	}

protected void loop2175()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2180);
		}
		if (/*isEQ(t5608rec.mortcls[x.toInt()],SPACES)
		|| */isNE(t5608rec.mortcls[x.toInt()],sv.mortcls)) {
			goTo(GotoLabel.loop2175);
		}
	}

protected void checkLiencd2180()
	{
		/* Lien Code - Validate against the maximum.*/
		if (isEQ(sv.liencdOut[varcom.pr.toInt()],"Y")
		|| isEQ(sv.liencd,SPACES)) {
			goTo(GotoLabel.checkMore2190);
		}
		x.set(0);
	}

protected void loop2185()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2190);
		}
		if (isEQ(t5608rec.liencd[x.toInt()],SPACES)
		|| isNE(t5608rec.liencd[x.toInt()],sv.liencd)) {
			goTo(GotoLabel.loop2185);
		}
	}

protected void checkMore2190()
	{
		/* Check to see if BONUS APPLICATION METHOD is valid for           */
		/* coverage/rider.                                                 */
		if (isNE(sv.bappmeth,SPACES)
		&& isNE(sv.bappmeth,t6005rec.bappmeth01)
		&& isNE(sv.bappmeth,t6005rec.bappmeth02)
		&& isNE(sv.bappmeth,t6005rec.bappmeth03)
		&& isNE(sv.bappmeth,t6005rec.bappmeth04)
		&& isNE(sv.bappmeth,t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
			wsspcomn.edterror.set("Y");
		}
		/* Check joint life selection indicator (if applicable).*/
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			if (isNE(sv.select,SPACES)
			&& isNE(sv.select,"J")
			&& isNE(sv.select,"L")) {
				/*       MOVE U028             TO SR571-SELECT-ERR.             */
				sv.selectErr.set(errorsInner.h039);
			}
		}
		/*Z0-EXIT*/
		
		//ILIFE-3519:Start
		if(premiumflag)
		{
			checkIPPmandatory();
			boolean t5608Flag=false;
			for(int counter=1; counter<t5608rec.prmbasis.length;counter++){
				if(isNE(sv.prmbasis,SPACES)){
					if (isEQ(t5608rec.prmbasis[counter], sv.prmbasis)){
						t5608Flag=true;
						break;
					}
				}
			}
			if(!t5608Flag && isNE(sv.prmbasis,SPACES)){
				sv.prmbasisErr.set("RFV1");
			}
		}
		
		//ILIFE-3519:End
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
			sv.prmbasisErr.set(errorsInner.e186);
	}
}

/*protected void para2140()
	{
		/*  Check the riders for cessation dates greater than the          */
		/*  coverage cessation date. As soon as an invalid date is         */
		/*  found display error. On finding an error the user will         */
		/*  have to exit th coverage screen and change the riders          */
		/*  cessation dates before changing the coverage cessation dates   */
/*		if (isNE(covrrgwIO.getChdrcoy(),wsaaCovrChdrcoy)
		|| isNE(covrrgwIO.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrrgwIO.getLife(),wsaaCovrLife)
		|| isNE(covrrgwIO.getCoverage(),wsaaCovrCoverage)) {
			covrrgwIO.setStatuz(varcom.endp);
			return ;
		}

		CessDateValidation();

		if (isEQ(covrrgwIO.getStatuz(),varcom.endp)) {
			return ;
		}
		covrrgwIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrrgwIO);
		if (isNE(covrrgwIO.getStatuz(),varcom.oK)
		&& isNE(covrrgwIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrrgwIO.getParams());
			syserrrec.statuz.set(covrrgwIO.getStatuz());
			fatalError600();
		}
	}*/

protected void calcPremium2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2210();
					callSubr2220();
					adjustPrem2225();
				case cont: 
					cont();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2210()
	{
		/* If benefit billed, do not calculate premium*/
		if (isNE(t5687rec.bbmeth,SPACES)) {
			goTo(GotoLabel.exit2290);
		}
		/* If there is no premium calculation method, set premium          */
		/* required field to YES.                                          */
		if (isEQ(t5675rec.premsubr,SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the sum insured OR the sum insured from the*/
		/* premium.*/
		/*    IF PREM-REQD            NOT = 'N'                            */
		/*       IF SR571-INST-PREM       = 0                              */
		/*          MOVE U019             TO SR571-INSTPRM-ERR             */
		/*          GO TO 2290-EXIT.                                       */
		/* If a premium is required check if this premium > 0              */
		if (premReqd.isTrue()) {
			if (isEQ(sv.instPrem,0)) {
				/*       MOVE U019             TO SR571-INSTPRM-ERR        <031>*/
				sv.instprmErr.set(errorsInner.g818);
			}
			goTo(GotoLabel.exit2290);
		}
		/* If the premium is greater than zero and the user is modifying   */
		/* a component then skip premium calculation.                      */
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (wsaaPremChanged.isTrue()
			&& modifyComp.isTrue()) {
				wsaaOldPrem.set(sv.instPrem);
				wsaaPremStatuz.set("U");
				sv.instprmErr.set(errorsInner.f404);
				goTo(GotoLabel.exit2290);
			}
		}
		/* If the premium is greater than zero and the user has modified   */
		/* a premium then skip premium calculation.                        */
		/* This check was before the one above, but this way it always     */
		/* checks if the user entered an amount, reversed it only          */
		/* checked the first time.                                         */
		if (modifyComp.isTrue()
		&& userPremEntered.isTrue()
		&& premReqd.isTrue()) {
			/*   MOVE 'N'                 TO WSAA-PREM-STATUZ      <031036>*/
			if (isGT(sv.instPrem,0)) {
				goTo(GotoLabel.exit2290);
			}
		}
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (!wsaaPremChanged.isTrue()) {
				goTo(GotoLabel.exit2290);
			}
		}
	}

protected void callSubr2220()
	{
		/* To calculate  it,  call  the  relevant calculation subroutine*/
		/*   worked out above passing:*/
		/*    MOVE COVTMJA-CRTABLE        TO CPRM-CRTABLE.*/
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select,"J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		/*    MOVE CHDRMJA-OCCDATE        TO CPRM-EFFECTDT.*/
		/*MOVE CHDRMJA-BTDATE         TO CPRM-EFFECTDT.                */
		if (addComp.isTrue()) {
			premiumrec.effectdt.set(wsspcomn.currfrom);
		}
		premiumrec.effectdt.set(payrpf.getBtdate());
		premiumrec.termdate.set(sv.premCessDate);
		/*MOVE CHDRMJA-CNTCURR        TO CPRM-CURRCODE.                */
		premiumrec.currcode.set(payrpf.getCntcurr());
		/*  (wsaa-sumin already adjusted for plan processing)*/
		if (isNE(wsaaSumin,ZERO)) {
			/* Nothing to do. */
		}
		/*IF   ADD-COMP                                                */
		/*     MOVE WSAA-SUMIN             TO CPRM-SUMIN               */
		/*ELSE                                                         */
		/*     IF  PLAN-LEVEL  AND                                     */
		/*         SR571-PLAN-SUFFIX NOT > CHDRMJA-POLSUM              */
		/*         COMPUTE WSAA-SUMIN ROUNDED                          */
		/*               = SR571-SUMIN * CHDRMJA-POLINC                */
		/*                 / CHDRMJA-POLSUM                            */
		/*           COMPUTE WSAA-SUMIN ROUNDED                          */
		/*                 = WSAA-SUMINS-DIFF * CHDRMJA-POLINC           */
		/*                   / CHDRMJA-POLSUM                            */
		/*         MOVE WSAA-SUMIN             TO CPRM-SUMIN           */
		/*     ELSE                                                    */
		/*         MOVE SR571-SUMIN            TO CPRM-SUMIN.          */
		/*         MOVE WSAA-SUMINS-DIFF       TO CPRM-SUMIN.          */
		premiumrec.sumin.set(wsaaSumin);
		/*IF MODIFY-COMP                                          <036>*/
		/*   IF WSAA-SUMINS-DIFF               = ZERO             <036>*/
		/*      IF WSAA-SUMIN                  = ZERO             <036>*/
		/*         MOVE WSAA-STORE-SUMIN       TO CPRM-SUMIN      <036>*/
		/*      ELSE                                              <036>*/
		/*         MOVE WSAA-SUMIN             TO CPRM-SUMIN.     <036>*/
		/*IF CPRM-SUMIN               = ZERO                      <024>*/
		/*     IF OPTEXT-YES                                      <024>*/
		/*         MOVE WSAA-STORE-SUMIN        TO CPRM-SUMIN.    <024>*/
		premiumrec.mortcls.set(sv.mortcls);
		/*MOVE CHDRMJA-BILLFREQ       TO CPRM-BILLFREQ.                */
		/*MOVE CHDRMJA-BILLCHNL       TO CPRM-MOP                      */
		premiumrec.billfreq.set(payrpf.getBillfreq());
		premiumrec.mop.set(payrpf.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.calcBasPrem.set(sv.instPrem);
		premiumrec.calcLoaPrem.set(ZERO);
		/*   IF   ADD-COMP                                                */
		/*        MOVE SR571-INST-PREM        TO CPRM-CALC-PREM           */
		/*   ELSE                                                         */
		/*        MOVE WSAA-PREM-DIFF         TO CPRM-CALC-PREM.          */
		/*    MOVE COVRMJA-ANB-AT-CCD     TO CPRM-LAGE.*/
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		setCustomerSpecificFirstDate();
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.cnttype.set(chdrpf.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		setCustomerSpecificFirstDate();
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.benCessTerm.set(ZERO);
		/*IF PLAN-LEVEL                                                */
		/*    AND                                                      */
		/*   SR571-PLAN-SUFFIX  NOT > CHDRMJA-POLSUM                   */
		/*   COMPUTE CPRM-CALC-PREM ROUNDED = (CPRM-CALC-PREM          */
		/*                                    * CHDRMJA-POLINC         */
		/*                                    / CHDRMJA-POLSUM).       */
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		/* If a component has a premium breakdown record (indicates also   */
		/* it is a SUM product) and it is being modified, then set the     */
		/* valid flag on the premium breakdown record to '2'. Note this    */
		/* must be done only the once & so set and check a flag.           */
		/* Note that as of 18/4/95 a function of 'MALT' will only apply    */
		/* to coverage's using SUM.                                        */
		if (modifyComp.isTrue()
		&& isEQ(sv.pbindOut[varcom.nd.toInt()],"N")) {
			premiumrec.function.set("MALT");
			if (isNE(wsaaPovrModified,"Y")) {
				vf2MaltPovr8300();
				wsaaPovrModified = "Y";
			}
		}
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{	
			if(isNE(sv.dialdownoption,SPACES)){
				if(sv.dialdownoption.toString().startsWith("0"))
					premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
				else
					premiumrec.dialdownoption.set(sv.dialdownoption);
			}
			else
				premiumrec.dialdownoption.set("100");
		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//****Ticket #ILIFE-2005 end
		if (isEQ(premiumrec.statuz,varcom.bomb)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		/* Store age and term fields used in the calculation to ensure     */
		/* any values stored on file match calculation parameters.         */
		wsaaPcesageStore.set(sv.premCessAge);
		wsaaRcesageStore.set(sv.riskCessAge);
		wsaaPcestermStore.set(sv.premCessTerm);
		wsaaRcestermStore.set(sv.riskCessTerm);
	}

	/**
	* <pre>
	*    IF CPRM-STATUZ NOT = O-K
	*       MOVE CPRM-STATUZ         TO SR571-INSTPRM-ERR
	*       IF CPRM-STATUZ = 'F265'
	*       MOVE CPRM-MESSAGE        TO SCRN-ERRORLINE
	*       GO TO 2290-EXIT
	*       ELSE
	*       MOVE CPRM-STATUZ         TO SR571-INSTPRM-ERR
	*       GO TO 2290-EXIT.
	* Adjust premium calculated for plan processing.
	*****IF  NOT ADD-COMP                                        <024>
	*****    IF WSAA-SUMINS-DIFF    = ZERO                       <024>
	*****      IF OPTEXT-YES                                     <024>
	*****         GO TO 2225-ADJUST-PREM.                        <024>
	*****IF  MODIFY-COMP                                              
	*****    PERFORM 2800-ADJUST-SUMINS-PREM                          
	*****    GO TO 2290-EXIT.                                         
	* </pre>
	*/
protected void adjustPrem2225()
	{
		/* Adjust premium calculated for plan processing.*/
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		/* Put possibly calculated sum insured back on the screen.*/
		/*    THIS SECTION CALCULATES THE NEW PREMIUM PAYMENT AND*/
		/*    COMPARES THE MANDATE AMOUT WITH IT.*/
		/*    THE FORMULA USED IS AS FOLLOWS.*/
		/*    NP = OP - (CP - SP).*/
		/*  WHERE :*/
		/*    NP = NEW PREMIUM (TOTAL)*/
		/*    OP = OLD PREMIUM (TOTAL)*/
		/*    CP = OLD SINGLE PREMIUM*/
		/*    SP = NEW SINGLE PREMIUM*/
		/*IF CHDRMJA-BILLCHNL         NOT = 'B'                   <025>*/
		/*   IF PAYR-BILLCHNL            NOT = 'B'                   <042>*/
		/*      GO TO CONT.                                          <042>*/
		if (isLTE(payrpf.getMandref(),SPACES)) {
			goTo(GotoLabel.cont);
		}
		/* IF CHDRMJA-PAYRNUM          NOT = SPACES             <CAS1.0>*/
		/*    MOVE CHDRMJA-PAYRNUM     TO MAND-PAYRNUM          <CAS1.0>*/
		/*    MOVE CHDRMJA-PAYRCOY     TO MAND-PAYRCOY          <CAS1.0>*/
		/*MOVE CHDRMJA-MANDREF     TO MAND-MANDREF             <025>*/
		/* ELSE                                                 <CAS1.0>*/
		/*    MOVE CHDRMJA-COWNNUM     TO MAND-PAYRNUM          <CAS1.0>*/
		/*    MOVE CHDRMJA-COWNCOY     TO MAND-PAYRCOY          <CAS1.0>*/
		/*MOVE CHDRMJA-MANDREF     TO MAND-MANDREF.            <025>*/
		/* END-IF.                                              <CAS1.0>*/
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(payrpf.getChdrnum());
		wsaaPayrseqno.set(payrpf.getPayrseqno());

//ILIFE-3310 by liwei
		Mandpf mand = mandpfDAO.getMandDataByClrf("CH",payrpf.getChdrcoy(),wsaaForenum.toString(),"PY",payrpf.getMandref());

		if(mand == null){
			fatalError600();
		} //IJTI-462 START
		else {
			//ILIFE-6661
			
			if (isEQ(mand.getMandamt(),ZERO) || mand.getMandamt()==0) {
				goTo(GotoLabel.cont);
			}
			/*IF   CHDRMJA-BILLFREQ > 0                               <025>*/
			if (isGT(payrpf.getBillfreq(),0)) {
				compute(wsaaCalcPrem, 2).set(sub(covrpf.getInstprem(),(sub(payrpf.getSinstamt06(),sv.instPrem))));
			}
			if (isNE(mand.getMandamt(),wsaaCalcPrem)) {
				sv.instprmErr.set(errorsInner.ev01);
				wsspcomn.edterror.set("Y");
			}
		} //IJTI-462 END
	}

protected void cont()
	{
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		//smalchi2 for ILIFE-1510 STARTS
//		sv.instPrem.set(premiumrec.calcPrem);
		//ENDS
		if (isEQ(sv.effdate, chdrpf.getPtdate())) {
			sv.instPrem.set(ZERO);
			sv.linstamt.set(premiumrec.calcPrem);
		} else {
			sv.instPrem.set(premiumrec.calcPrem);
			sv.linstamt.set(ZERO);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		/*BRD-306 END */
		/*    To apply pro-rating on cprm-calc-prem                        */
		/*    rounding to the nearest cent.                                */
		
		calcFirstPrem();
		if (isNE(tr52drec.txcode, SPACES)) {
			a200CheckCalcTax();
		}
		if (addComp.isTrue()) {
			proratePremium1600();
			/*       PERFORM 1700-READ-T5541                                   */
			compute(wsaaProprem, 6).set(mult(premiumrec.calcPrem,wsaaProrate));
			/*                                * WSAA-T5541-FACTOR              */
			wsaaTmpprem.set(wsaaProprem);
			if (isGTE(wsaaDecimal3,5)) {
				wsaaTmpprem.add(0.01);
			}
			wsaaDecimal3.set(0);
			wsaaProprem.set(wsaaTmpprem);
			/*       COMPUTE CPRM-CALC-PREM = WSAA-PROPREM + 0.009999          */
			premiumrec.calcPrem.set(wsaaProprem);
			if (isEQ(wsspcomn.currfrom,chdrpf.getBtdate())) {
				sv.instPrem.set(ZERO);
				premiumrec.calcPrem.set(ZERO);
			}
			else {
				sv.instPrem.set(premiumrec.calcPrem);
			}
			a250ProrateTax();
			/*     IF SR571-INST-PREM > WSAA-SUSP-AVAIL              <V74L01>*/
			if ((setPrecision(wsaaSuspAvail, 2)
			&& isGT((add(sv.instPrem, wsaaProtax)), wsaaSuspAvail))) {
				/*          MOVE F702         TO   SCRN-ERROR-CODE                 */
				sv.instprmErr.set(errorsInner.rl33);
			}
		}
		else {
			if (isEQ(sv.instPrem,0)) {
				//sv.instPrem.set(premiumrec.calcPrem);
				goTo(GotoLabel.exit2290);
			}
		}
		/* Having calculated it, the  entered value, if any, is compared*/
		/*   with it to check that it is within acceptable limits of the*/
		/*   automatically calculated figure. If it  is  less  than  the*/
		/*   amount calculated and within tolerance  then  the  manually*/
		/*   entered amount is allowed. If the entered value exceeds the*/
		/*   calculated one, the calculated value is used.*/
		if (isEQ(sv.instPrem,0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			/*    MOVE SR571-INST-PREM    TO WSAA-OLD-PREM            <036>*/
			goTo(GotoLabel.exit2290);
		}
		if (isGTE(sv.instPrem,premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			/*    MOVE SR571-INST-PREM    TO WSAA-OLD-PREM            <036>*/
			goTo(GotoLabel.exit2290);
		}
		/* check the tolerance amount against the table entry read above.*/
		/*    SUBTRACT SR571-INST-PREM    FROM CPRM-CALC-PREM              */
		/*                                GIVING WSAA-DIFF.                */
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, (add(sv.instPrem, wsaaProtax))));
		sv.instprmErr.set(errorsInner.f254);
		/*    MOVE CPRM-CALC-PREM         TO WSAA-OLD-PREM.                */
		/*MOVE WSAA-OLD-PREM          TO SR571-INST-PREM.         <036>*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,11)
		|| isEQ(sv.instprmErr,SPACES)); wsaaSub.add(1)){
			searchForTolerance2240();
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.instprmErr,SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem,sv.zlinstprem));
		}
		goTo(GotoLabel.exit2290);
	}

protected void searchForTolerance2240()
	{
		/* Calculate tolerance Limit and Check whether it is greater*/
		/*   than the maximum tolerance amount in table T5667.*/
		/*IF CHDRMJA-BILLFREQ         = T5667-FREQ (WSAA-SUB)          */
		if (isEQ(payrpf.getBillfreq(),t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtol[wsaaSub.toInt()])),100));
			if (isLTE(wsaaDiff,wsaaTol)
			&& isLTE(wsaaDiff,t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()],ZERO)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtoln[wsaaSub.toInt()])),100));
				if (isLTE(wsaaDiff,wsaaTol)
				&& isLTE(wsaaDiff,t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.instprmErr.set(SPACES);
				}
			}
		}
	}
protected void calcFirstPrem() {
	
}
protected void callDatcon32300()
	{
		/*CALL*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void adjustSuminsPrem2800()
	{
		/*START*/
		/* Adjust premium calculated for plan processing.*/
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		/* Put possibly calculated sum insured back on the screen.*/
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(premiumrec.sumin, 3).setRounded((div(mult(premiumrec.sumin,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		/*  IF  WSAA-SUMINS-INCREASED                                    */
		/*      COMPUTE SR571-INST-PREM                                  */
		/*              = WSAA-OLD-PREM + CPRM-CALC-PREM                 */
		/*      COMPUTE SR571-SUMIN                                      */
		/*              = WSAA-OLD-SUMINS      + CPRM-SUMIN              */
		/*      MOVE SR571-INST-PREM    TO WSAA-OLD-PREM                 */
		/*  ELSE                                                         */
		/*  IF WSAA-SUMINS-DECREASED                                     */
		/*      COMPUTE SR571-INST-PREM                                  */
		/*              = WSAA-OLD-PREM  - CPRM-CALC-PREM                */
		/*      COMPUTE SR571-SUMIN                                      */
		/*              = WSAA-OLD-SUMINS  - CPRM-SUMIN                  */
		/*      MOVE SR571-INST-PREM    TO WSAA-OLD-PREM                 */
		/*  ELSE                                                         */
		sv.sumin.set(premiumrec.sumin);
		sv.instPrem.set(premiumrec.calcPrem);
		wsaaOldPrem.set(sv.instPrem);
		wsaaOldSumins.set(sv.sumin);
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
				case updatePcdt3060: 
					updatePcdt3060();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		/* Skip this section  if  returning  from  an optional selection   */
		/* (current stack position action flag = '*').                     */
		/*   i.e. returning from Options and Extras data screen.           */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		/* If termination of processing or enquiry go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3090);
		}
		
		//IBPLIFE-2134
		if(contnewBFlag){
			if (modifyComp.isTrue()) {
				covppf=covppfDAO.getCovppfCrtable(covtpf.getChdrcoy(), covtpf.getChdrnum(), covtpf.getCrtable(),
						covtpf.getCoverage().toString(),covtpf.getRider().toString());
				if(covppf != null){
				if (isNE(sv.covrprpse, covppf.getCovrprpse())){
					covppf.setCovrprpse(sv.covrprpse.toString());
					if (isEQ(wsaaToday, 0)) {
				        datcon1rec.function.set(varcom.tday);
				        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				        wsaaToday.set(datcon1rec.intDate);
				    }
				    covppf.setEffdate(wsaaToday.toInt());
				    covppf.setDatime(new Timestamp(new Date().getTime()));
				    covppf.setCoverage(sv.coverage.toString());
					covppf.setRider(sv.rider.toString());
				    covppfDAO.updateCovppfCrtable(covppf);
				}
			}
			}
			if (compEnquiry.isTrue()) {
				covppf=covppfDAO.getCovppfCrtable(covtpf.getChdrcoy(), covtpf.getChdrnum(),
						covtpf.getCrtable(),covtpf.getCoverage(),covtpf.getRider());
			}
			if (addComp.isTrue()){
				setCovppf();
				Covppf covppftemp=covppfDAO.getCovppfCrtable(covtpf.getChdrcoy(), covtpf.getChdrnum(), covtpf.getCrtable()
						,covtpf.getCoverage(),covtpf.getRider());
				if(null==covppftemp)
					covppfDAO.insertCovppfRecord(covppf);
				else {
					covppf.setUniqueNumber(covppftemp.getUniqueNumber());
					covppfDAO.updateCovppfCrtable(covppf); //IBPLIFE-6053
				}
				}
		}
		
		/*    IF  COMP-ENQUIRY                                             */
		if (compEnquiry.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		if (compApp.isTrue()) {
			goTo(GotoLabel.updatePcdt3060);
		}
		/*                                                         <V4L004>*/
		/* If premium breakdown selected then exit....                     */
		if (isEQ(sv.pbind,"X")) {
			goTo(GotoLabel.exit3090);
		}
		/* Perform updates for all subfile records up to a maximum of*/
		/* one page.*/
		setupCovtmja3100();
		/*    Extra check in here to see if RACT or                        */
		/*    or LEXT updates have occurred - if so, we want to            */
		/*    set update flag to 'Y'.                                      */
		if (isEQ(wsaaLextUpdates,"Y")) {
			/*   OR WSAA-RACT-UPDATES     = 'Y'                     <R96REA>*/
			wsaaUpdateFlag.set("Y");
		}
		updateCovtmja3200();
		updatrecrdsCustomerSpecific33();

		//ILIFE-3519:Start
		if(premiumflag || dialdownFlag){
			insertAndUpdateRcvdpf();
		}
		//ILIFE-3519:End
		if(isFollowUpRequired){
			createFollowUps3400();
		}
		goTo(GotoLabel.exit3090);
	}

protected void updatePcdt3060()
	{
		if (isEQ(wsaaUpdateFlag,"N")) {
			return ;
		}
		pcdtpf = new Pcdtpf();
		pcdtpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		pcdtpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		pcdtpf.setTranno(add(chdrpf.getTranno(),1).toInt());
		pcdtpf.setLife(sv.life.toString());
		pcdtpf.setCoverage(sv.coverage.toString());
		pcdtpf.setRider(sv.rider.toString());
		pcdtpf.setPlnsfx(covtpf.getPlnsfx());
		pcdtpf = pcdtpfDAO.getPcdtmjaByKey(pcdtpf);
		
		if (pcdtpf == null) {
			defaultPcdt3800();
		}
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void setupCovtmja3100()
	{
		/* Check for changes in the COVTMJA Record.*/
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate,covtpf.getRcesdte())) {
			covtpf.setRcesdte(sv.riskCessDate.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate,covtpf.getPcesdte())) {
			covtpf.setPcesdte(sv.premCessDate.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge,covtpf.getRcesage())) {
			covtpf.setRcesage(sv.riskCessAge.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge,covtpf.getPcesage())) {
			covtpf.setPcesage(sv.premCessAge.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm,covtpf.getRcestrm())) {
			covtpf.setRcestrm(sv.riskCessTerm.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm,covtpf.getPcestrm())) {
			covtpf.setPcestrm(sv.premCessTerm.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin,covtpf.getSumins())) {
			covtpf.setSumins(sv.sumin.getbigdata());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zbinstprem,covtpf.getZbinstprem())) {
			covtpf.setZbinstprem(sv.zbinstprem.getbigdata());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem,covtpf.getZlinstprem())) {
			covtpf.setZlinstprem(sv.zlinstprem.getbigdata());
			/*BRD-306 START */			
			covtpf.setLoadper(sv.loadper.getbigdata());
			covtpf.setRateadj(sv.rateadj.getbigdata());
			covtpf.setFltmort(sv.fltmort.getbigdata());
			covtpf.setPremadj(sv.premadj.getbigdata());
			covtpf.setAgeadj(sv.adjustageamt.getbigdata());
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRMJA-BILLFREQ         = '00'                      <001>*/
		if (isEQ(payrpf.getBillfreq(),"00")) {
			/*       IF SR571-INST-PREM       NOT = COVTMJA-SINGP*/
			/*          MOVE SR571-INST-PREM  TO COVTMJA-SINGP*/
			if (isNE(sv.linstamt,covtpf.getSingp())) {
				covtpf.setSingp(sv.instPrem.getbigdata());
				covtpf.setInstprem(BigDecimal.ZERO);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			/*       IF SR571-INST-PREM       NOT = COVTMJA-INSTPREM           */
			/*          MOVE SR571-INST-PREM  TO COVTMJA-INSTPREM              */
			if (isNE(sv.linstamt,covtpf.getInstprem())) {
				covtpf.setInstprem(sv.linstamt.getbigdata());	//ILIFE-9496
				covtpf.setSingp(BigDecimal.ZERO);
				wsaaUpdateFlag.set("Y");
			}
			else if (isNE(sv.instPrem,covtpf.getInstprem())) {
				covtpf.setInstprem(sv.instPrem.getbigdata());
				covtpf.setSingp(BigDecimal.ZERO);
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.mortcls,covtpf.getMortcls()==null?"":covtpf.getMortcls())) {
			covtpf.setMortcls(sv.mortcls.toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd,covtpf.getLiencd()==null?"":covtpf.getLiencd())) {
			covtpf.setLiencd(sv.liencd.toString());
			wsaaUpdateFlag.set("Y");
		}
		if(covtpf.getJlife() == null) {
			covtpf.setJlife("  ");
		}
		if ((isEQ(sv.select,"J")
		&& (isEQ(covtpf.getJlife(),"00")
		|| isEQ(covtpf.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select,"L"))
		&& isEQ(covtpf.getJlife(),"01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select,"J")) {
				covtpf.setJlife("01");
			}
			else {
				covtpf.setJlife("00");
			}
		}
		/*IF CHDRMJA-BILLFREQ         NOT = COVTMJA-BILLFREQ           */
		/*   MOVE CHDRMJA-BILLFREQ    TO COVTMJA-BILLFREQ              */
		if(covtpf.getBillfreq()==null) {
			covtpf.setBillfreq("");
		}
		if(covtpf.getBillchnl()==null) {
			covtpf.setBillchnl("");
		}
		if(covtpf.getSex01()==null) {
			covtpf.setSex01("");
		}
		if(covtpf.getSex02()==null) {
			covtpf.setSex02("");
		}
		if(covtpf.getBappmeth()==null) {
			covtpf.setBappmeth("");
		}
	
		if (isNE(payrpf.getBillfreq(),covtpf.getBillfreq())) {
			covtpf.setBillfreq(payrpf.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRMJA-BILLCHNL         NOT = COVTMJA-BILLCHNL           */
		/*   MOVE CHDRMJA-BILLCHNL    TO COVTMJA-BILLCHNL              */
		if (isNE(payrpf.getBillchnl(),covtpf.getBillchnl())) {
			covtpf.setBillchnl(payrpf.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		/*    IF CHDRMJA-OCCDATE          NOT = COVTMJA-EFFDATE*/
		/*       MOVE CHDRMJA-OCCDATE     TO COVTMJA-EFFDATE*/
		/*       MOVE 'Y'                 TO WSAA-UPDATE-FLAG.*/
		if (isNE(wsaaAnbAtCcd,covtpf.getAnbccd01())) {
			covtpf.setAnbccd01(wsaaAnbAtCcd.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtpf.getSex01())) {
			covtpf.setSex01(wsaaSex.toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd,covtpf.getAnbccd02())) {
			covtpf.setAnbccd02(wsbbAnbAtCcd.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtpf.getSex02())) {
			covtpf.setSex02(wsbbSex.toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth,covtpf.getBappmeth())) {
			covtpf.setBappmeth(sv.bappmeth.toString());
			wsaaUpdateFlag.set("Y");
		}
	}

//IBPLIFE-2134
public void setCovppf(){
	covppf= new Covppf();
	covppf.setChdrcoy(covtpf.getChdrcoy());
	covppf.setChdrnum(covtpf.getChdrnum());
	covppf.setChdrpfx(chdrpf.getChdrpfx());
	covppf.setCovrprpse(sv.covrprpse.toString());
	covppf.setDatime(new Timestamp(new Date().getTime()));
	covppf.setEffdate(sv.dateeff.toString().trim().equals("")?wsaaToday.toInt():sv.dateeff.toInt());
	covppf.setCoverage(covtpf.getCoverage());
	covppf.setRider(covtpf.getRider());
	covppf.setJobnm(appVars.getLoggedOnUser());
	covppf.setUsrprf(wsspcomn.username.toString());
	covppf.setTranCode(sv.trcode.toString().trim().equals("")?wsaaBatckey.batcBatctrcde.toString():sv.trcode.toString());
	covppf.setValidflag("1");
	covppf.setCrtable(covtpf.getCrtable());
	
	
}

protected void updateCovtmja3200()
	{
		if(noUpdate3210()){
			update3220();
		}
	}

protected boolean noUpdate3210()
	{
		/* If there is no change of detail then skip the UPDAT.*/
		/* If Options and extras selected and no change of detail then we*/
		/*   must have a COVTMJA record to pass on.*/
		if (isEQ(wsaaUpdateFlag,"N")
		&& isNE(sv.optextind,"X")) {
			return false;
		}
		return true;
	}

protected void update3220()
	{
		/* Update the COVRMJA record if it exists or write a new one if it*/
		/*   doesn't.*/
		/* Release the COVTMJA record.*/
		covtpfDAO.deleteCacheObject(covtpf);
		
		covtpf.setTermid(varcom.vrcmTermid.toString());
		covtpf.setUserT(varcom.vrcmUser.toInt());
		covtpf.setTrdt(varcom.vrcmDate.toInt());
		covtpf.setTrtm(varcom.vrcmTime.toInt());
		covtpf.setChdrcoy(covrpf.getChdrcoy());
		covtpf.setChdrnum(covrpf.getChdrnum());
		covtpf.setLife(covrpf.getLife());
		covtpf.setCoverage(wsaaCovrCoverage.toString());
		/*MOVE CHDRMJA-BTDATE         TO COVTMJA-EFFDATE.              */
		covtpf.setEffdate(payrpf.getBtdate());
		if (addComp.isTrue()) {
			covtpf.setEffdate(wsspcomn.currfrom.toInt());
		}
		covtpf.setRider(wsaaCovrRider.toString());
		covtpf.setPayrseqno(1);
		covtpf.setBcesage(0);
		covtpf.setBcestrm(0);
		covtpf.setBcesdte(varcom.vrcmMaxDate.toInt());
		if (addComp.isTrue()) {
			covtpf.setPlnsfx(0);
			covtpf.setSeqnbr(0);
		}
		else {
			covtpf.setPlnsfx(covrpf.getPlanSuffix());
			covtpf.setSeqnbr(wsaaNextSeqnbr.toInt());
		}
		wsaaNextSeqnbr.subtract(1);
		/*      MOVE COVRMJA-PLAN-SUFFIX    TO COVTMJA-PLAN-SUFFIX,     */
		/*                                COVTMJA-SEQNBR.               */
		covtpf.setCrtable(wsaaCrtable.toString());
		covtpf.setRsunin(" ");
		covtpf.setRundte(0);
		covtpf.setPolinc(0);
		if (chinaLocalPermission)
		{
			covtpf.setCashvalarer(sv.cashvalarer.getbigdata());
		}
		
		/* IF   ADD-COMP                                                */
		/*      MOVE WRITR             TO COVTMJA-FUNCTION              */
		/* ELSE   
		 *
		 *                                                      */
		//ILIFE-9355--Start
		Covtpf covt = covtpfDAO.getCovtlnbData(covtpf.getChdrcoy(),covtpf.getChdrnum(),covtpf.getLife(),covtpf.getCoverage(),covtpf.getRider());
		if(covt != null)
			//if(covtmjaIO.getUniqueNumber()>0)
		//ILIFE-9355--End
			covtpfDAO.updateCovtData(covtpf);
		else
			covtpfDAO.insertCovtData(covtpf);
			
		
		if (addComp.isTrue()) {
			writeCprp3300();
		}
	}

protected void writeCprp3300()
	{
	    cprppf = new Cprppf();
		cprppf.setChdrpfx(chdrpf.getChdrpfx());/* IJTI-1386 */
		cprppf.setChdrcoy(chdrpf.getChdrcoy().toString());
		cprppf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		cprppf.setLife(wsaaCovrLife.toString());
		cprppf.setCoverage(wsaaCovrCoverage.toString());
		cprppf.setRider(wsaaCovrRider.toString());
		setPrecision(cprppf.getTranno(), 0);
		cprppf.setTranno(add(chdrpf.getTranno(),1).toInt());
		cprppf.setInstprem(sv.instPrem.toDouble());
		cprppfDAO.updateOrInstCprppf(cprppf);
		/*-- CODE TO DEDUCT CPRP-99-APA.                                   */
		if (isEQ(sv.optextind,"X")) {
			return ;
		}
		/*  COMPUTE WSAA-SUSP-AVAIL = WSAA-SUSP-AVAIL - SR571-INST-PREM.<*/
		compute(wsaaSuspAvail, 2).set(sub(sub(wsaaSuspAvail, sv.instPrem), wsaaProtax));
		
		
		cprppf.setChdrpfx(chdrpf.getChdrpfx());/* IJTI-1386 */
		cprppf.setChdrcoy(chdrpf.getChdrcoy().toString());
		cprppf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		cprppf.setLife("99");
		cprppf.setCoverage("00");
		cprppf.setRider("00");
		cprppf.setValidflag("1");
		setPrecision(cprppf.getTranno(), 0);
		cprppf.setTranno(add(chdrpf.getTranno(),1).toInt());
		cprppf.setInstprem(Double.parseDouble(wsaaSuspAvail.toString()));
		if (cprppfDAO.updateCprppfForTrannoAndInstprem(cprppf)<=0) {
			syserrrec.params.set(chdrpf.getChdrpfx().concat(chdrpf.getChdrcoy().toString()).concat(chdrpf.getChdrnum()));/* IJTI-1386 */
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
	}

protected void defaultPcdt3800()
	{
		pcdtpf = new Pcdtpf();

	    pcdtpf.setInstprem(BigDecimal.ZERO);
	    pcdtpf.setPlnsfx(0);
	    pcdtpf.setSplitc01(BigDecimal.ZERO);
	    pcdtpf.setSplitc02(BigDecimal.ZERO);
	    pcdtpf.setSplitc03(BigDecimal.ZERO);
	    pcdtpf.setSplitc04(BigDecimal.ZERO);
	    pcdtpf.setSplitc05(BigDecimal.ZERO);
	    pcdtpf.setSplitc06(BigDecimal.ZERO);
	    pcdtpf.setSplitc07(BigDecimal.ZERO);
	    pcdtpf.setSplitc08(BigDecimal.ZERO);
	    pcdtpf.setSplitc09(BigDecimal.ZERO);
	    pcdtpf.setSplitc10(BigDecimal.ZERO);
	    pcdtpf.setTranno(0);
		pcdtpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		pcdtpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		pcdtpf.setTranno(add(chdrpf.getTranno(),1).toInt());
		pcdtpf.setLife(sv.life.toString());
		pcdtpf.setCoverage(sv.coverage.toString());
		pcdtpf.setRider(sv.rider.toString());
		pcdtpf.setPlnsfx(covtpf.getPlnsfx());
		pcdtpf.setInstprem(BigDecimal.ZERO);
		wsaaIndex.set(1);
		pcdtpf.setAgntnum01(chdrpf.getAgntnum());/* IJTI-1386 */
		pcdtpf.setSplitc01(new BigDecimal(100));
        pcdtpfDAO.insertPcdtpfPcdtpf(pcdtpf);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/* As the program can be forced back into the 2000-section using*/
		/*   SCRN-SCRNAME in WSSP-NEXTPROG, in order to exit we must*/
		/*   ensure that the Program name has been moved to WSSP-NEXTPROG.*/
		wsspcomn.nextprog.set(wsaaProg);
		/* Determine processing according to whether we are selectin*/
		/*   or returning from the Options and Extras data screen.*/
		if (isEQ(sv.pbind,"X")) {
			pbindExe8100();
			return ;
		}
		else {
			if (isEQ(sv.pbind,"?")) {
				pbindRet8200();
				return ;
			}
		}
		if (isEQ(sv.optextind,"X")) {
			optionsExe4500();
			return ;
		}
		else {
			if (isEQ(sv.optextind,"?")) {
				optionsRet4600();
				return ;
			}
		}
		if (isEQ(sv.comind,"X")) {
			commissionExe4900();
			return ;
		}
		else {
			if (isEQ(sv.comind,"?")) {
				commissionRet4a00();
				return ;
			}
			}
		if (isEQ(sv.taxind, "X")) {
			taxExe4b00();
			return ;
		}
		else {
			if (isEQ(sv.taxind, "?")) {
				taxRet4b50();
				return ;
			}
		}
		if (isEQ(sv.exclind, "X")) {
			exclExe6000();
			return;
		}
		if (isEQ(sv.exclind, "?")) {
			exclRet6100();
			return;
		}
		if (isEQ(sv.fuind, "X")) {
			fuindExe7100();
			return;
		}else {
			if (isEQ(sv.fuind,"?")) {
				fuindRec7100();
				return ;
			}
		}
		/* IF SR571-RATYPIND           = 'X'                    <R96REA>*/
		/*    PERFORM 4200-REASSURANCE-EXE                      <R96REA>*/
		/*    GO TO 4090-EXIT                                   <R96REA>*/
		/* ELSE                                                 <R96REA>*/
		/* IF SR571-RATYPIND           = '?'                    <R96REA>*/
		/*    PERFORM 4300-REASSURANCE-RET                      <R96REA>*/
		/*       GO TO 4090-EXIT.                               <R96REA>*/
		/*                                                      <R96REA>*/
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (planLevel.isTrue()) {
			covrpfCount++; 
			clearScreen4700();
			policyLoad5000();
			if (covrpf!=null) { //ILB-456 //to resolve findbug changed
				wsspcomn.nextprog.set(wsaaProg);
				wayout4800();
			}
			
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4800();
		}
	}

	/**
	* <pre>
	*4200-REASSURANCE-EXE SECTION.                            <R96REA>
	******************************                            <R96REA>
	****                                                      <R96REA>
	*4200-PARA.                                               <R96REA>
	****                                                      <R96REA>
	*****                                                     <R96REA>
	**Keep the COVRMJA record if Whole Plan selected or Add co<R96REA>
	****as this will have been released within the 1000-sectio<R96REA>
	****enable all policies to be processed.                  <R96REA>
	*****                                                     <R96REA>
	**** MOVE KEEPS               TO COVRMJA-FUNCTION.        <R96REA>
	****                                                      <R96REA>
	**** CALL 'COVRMJAIO' USING COVRMJA-PARAMS.               <R96REA>
	****                                                      <R96REA>
	**** IF COVRMJA-STATUZ        NOT = O-K                   <R96REA>
	****    MOVE COVRMJA-PARAMS   TO SYSR-PARAMS              <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** If the reassurance details have been selected,(value <R96REA>
	**** then set an asterisk in the program stack action fiel<R96REA>
	**** ensure that control returns here, set the parameters <R96REA>
	**** generalised secondary switching and save the original<R96REA>
	**** programs from the program stack.                     <R96REA>
	****                                                      <R96REA>
	**** MOVE '?'               TO SR571-RATYPIND.            <R96REA>
	**** COMPUTE SUB1           =  WSSP-PROGRAM-PTR + 1.      <R96REA>
	**** MOVE 1                 TO SUB2.                      <R96REA>
	**** PERFORM 4510-SAVE      8 TIMES.                      <R96REA>
	**** MOVE 'C'               TO GENS-FUNCTION.             <R96REA>
	**** PERFORM                4210-GENSWW.                  <R96REA>
	****                                                      <R96REA>
	**** COMPUTE SUB1           =  WSSP-PROGRAM-PTR + 1.      <R96REA>
	**** MOVE 1                 TO SUB2.                      <R96REA>
	**** PERFORM 4530-LOAD               8 TIMES.             <R96REA>
	****                                                      <R96REA>
	**** MOVE '*'                 TO WSSP-SEC-ACTN(WSSP-PROGRA<R96REA>
	**** ADD 1                    TO WSSP-PROGRAM-PTR.        <R96REA>
	****                                                      <R96REA>
	*4209-EXIT.                                               <R96REA>
	****  EXIT.                                               <R96REA>
	****                                                      <R96REA>
	*   If a value has been placed in the GENS-FUNCTION then call     
	*   the generalised secondary switching module to obtain the      
	*   next 8 programs and load them into the program stack.         
	* </pre>
	*/
protected void exclExe6000(){
	covrpfDAO.setCacheObject(covrpf);
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
		wsspcomn.crtable.set(wsaaCrtable);
		
		
		if(isNE(wsspcomn.flag,"I")){
		wsspcomn.flag.set("X");
		wsspcomn.cmode.set("IF");}
		else
			wsspcomn.cmode.set("IFE");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),wsaaCrtable.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());//ILIFE-8187
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}
protected void gensww4210()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.params.set(gensswrec.gensswRec);
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*    MOVE V045                TO SCRN-ERROR-CODE          (018)*/
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			/*    IF LEXT-STATUZ            = ENDP                     <018>*/
			if (firstLext == null) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

	/**
	* <pre>
	*4300-REASSURANCE-RET SECTION.                            <R96REA>
	******************************                            <R96REA>
	****                                                      <R96REA>
	*4310-PARA.                                               <R96REA>
	****                                                      <R96REA>
	**** MOVE '+'            TO SR571-RATYPIND.               <R96REA>
	**** COMPUTE SUB1        = WSSP-PROGRAM-PTR + 1.          <R96REA>
	**** MOVE 1              TO SUB2.                         <R96REA>
	**** PERFORM 4610-RESTORE 8 TIMES.                        <R96REA>
	**** MOVE ' '            TO WSSP-SEC-ACTN(WSSP-PROGRAM-PTR<R96REA>
	**** PERFORM             5650-CHECK-RACT.                 <R96REA>
	*****ADD 1               TO WSSP-PROGRAM-PTR.             <R96REA>
	**** MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.           <R96REA>
	****                                                      <R96REA>
	*4319-EXIT.                                               <R96REA>
	****  EXIT.                                               <R96REA>
	* </pre>
	*/
protected void optionsExe4500()
	{
		/* Keep the COVRMJA record if Whole Plan selected or Add component*/
		/*   as this will have been released within the 1000-section to*/
		/*   enable all policies to be processed.*/
		covrpfDAO.setCacheObject(covrpf);
	
		/* The first thing to consider is the handling of an Options/*/
		/*   Extras request. If the indicator is 'X', a request to visit*/
		/*   options and extras has been made. In this case:*/
		/*    - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/* Save the next 8 programs from the program stack.*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		/* Call GENSSWCH with and action of 'A' to retrieve the program*/
		/*   switching required, and move them to the stack.*/
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.*/
		/* Add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		/* On return from this  request, the current stack "action" will*/
		/*   be '*' and the options/extras indicator  will  be  '?'.  To*/
		/*   handle the return from options and extras:*/
		/* - calculate the  premium  as  described  above  in  the*/
		/*   'Validation' section, and check that it is within the*/
		/*   tolerance limit,*/
		/*    PERFORM 5200-CALC-PREMIUM.*/
		/* MOVE 'Y'                    TO WSAA-OPTEXT.          <RA9606>*/
		/* PERFORM 2200-CALC-PREMIUM.                           <RA9606>*/
		/* If enquiry on a component then do not recalculate the Premium.  */
		/*    IF  NOT COMP-ENQUIRY                                 <V4L004>*/
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			wsaaOptext.set("Y");
			calcPremium2200();
		}
		wsaaOptext.set("N");
		/* Restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		/* Blank out the stack "action".*/
		/*    Only blank out the action and set the screen name if         */
		/*    no other check boxes have been selected.                     */
		/* MOVE ' '                 TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR).*/
		checkLext5600();
		/* Store the same values in the POVR key as used in 5600- for use  */
		/* in returning .........                                          */
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(covtpf.getLife());
		wsaaPovrCoverage.set(covtpf.getCoverage());
		wsaaPovrRider.set(covtpf.getRider());
		/* Set WSSP-NEXTPROG to the current screen name (thus returning*/
		/*    to re-display the screen).*/
		/* MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.                   */
		if (isNE(sv.comind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}

	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void clearScreen4700()
	{
		/*PARA*/
		/* Clear all screen fields which are variable NOT header details.*/
		sv.premCessDate.set(ZERO);
		sv.riskCessDate.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.liencd.set(SPACES);
		sv.mortcls.set(SPACES);
		sv.optextind.set(SPACES);
		sv.select.set(SPACES);
		sv.comind.set(SPACES);
		sv.statFund.set(SPACES);
		sv.statSect.set(SPACES);
		sv.bappmeth.set(SPACES);
		sv.statSubsect.set(SPACES);
		/*EXIT*/
	}

protected void wayout4800()
	{
		/*PROGRAM-EXIT*/
		/* If control is passed to this  part of the 4000 section on the*/
		/*   way out of the program, i.e. after  screen  I/O,  then  the*/
		/*   current stack position action  flag will be  blank.  If the*/
		/*   4000-section  is  being   performed  after  returning  from*/
		/*   processing another program then  the current stack position*/
		/*   action flag will be '*'.*/
		/* If 'KILL' has been requested, (CF11), then move spaces to the*/
		/*   current program entry in the program stack and exit.*/
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/* Add 1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void commissionExe4900()
	{
		/* Keep the COVRMJA record if Whole Plan selected or Add component*/
		/*   as this will have been released within the 1000-section to*/
		/*   enable all policies to be processed.*/
		wsspcomn.tranrate.set(sv.instPrem);
		/*MOVE CHDRMJA-BTDATE         TO WSSP-EFFDATE.                 */
		wssplife.effdate.set(payrpf.getBtdate());
		/*    IF ADD-COMP OR                                               */
		/*      PLAN-LEVEL*/
		covrpfDAO.setCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* The first thing to consider is the handling of an Options/*/
		/*   Extras request. If the indicator is 'X', a request to visit*/
		/*   options and extras has been made. In this case:*/
		/*    - change the options/extras request indicator to '?',*/
		sv.comind.set("?");
		/* Save the next 8 programs from the program stack.*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		/* Call GENSSWCH with and action of 'A' to retrieve the program*/
		/*   switching required, and move them to the stack.*/
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.*/
		/* Add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);

	}

protected void commissionRet4a00()
	{
		/*A00-PARA*/
		/* On return from this  request, the current stack "action" will*/
		/*   be '*' and the options/extras indicator  will  be  '?'.  To*/
		/*   handle the return from options and extras:*/
		/* - calculate the  premium  as  described  above  in  the*/
		/*   'Validation' section, and check that it is within the*/
		/*   tolerance limit,*/
		calcPremium5200();
		/* Restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restore4610();
		}
		/* Blank out the stack "action".*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		checkPcdt5700();
		/* Set WSSP-NEXTPROG to the current screen name (thus returning*/
		/*   returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*A90-EXIT*/
	}

protected void taxExe4b00()
	{
		/* Keep the CHDR/COVT record                                       */
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		/*chdrmjaIO.setFunction("KEEPS");
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		wsaaStoreZbinstprem.set(covtpf.getZbinstprem());
		wsaaStoreSingp.set(covtpf.getSingp());
		wsaaStoreInstprem.set(covtpf.getInstprem());
		covtpf.setZbinstprem(sv.zbinstprem.getbigdata());
		if (isEQ(payrpf.getBillfreq(), "00")) {
			covtpf.setSingp(sv.linstamt.getbigdata());
			covtpf.setInstprem(BigDecimal.ZERO);
		}
		else {
			covtpf.setInstprem(sv.instPrem.getbigdata());
			covtpf.setSingp(BigDecimal.ZERO);
		}
		covtpfDAO.setCacheObject(covtpf);
		
		wssplife.effdate.set(chdrpf.getBtdate());
		/* The first thing to consider is the handling of an Options/      */
		/*   Extras request. If the indicator is 'X', a request to visit   */
		/*   options and extras has been made. In this case:               */
		/*    - change the options/extras request indicator to '?',        */
		sv.taxind.set("?");
		/* Save the next 8 programs from the program stack.                */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			save4510();
		}
		/* Call GENSSWCH with and action of 'A' to retrieve the program    */
		/*   switching required, and move them to the stack.               */
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.                          */
		/* Add one to the program pointer and exit.                        */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);

	}

protected void taxRet4b50()
	{
		/*B50-START*/
		sv.taxind.set("+");
		covtpf.setZbinstprem(wsaaStoreZbinstprem.getbigdata());
		covtpf.setSingp(wsaaStoreSingp.getbigdata());
		covtpf.setInstprem(wsaaStoreInstprem.getbigdata());
		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		/* Blank out the stack  action".                                   */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/* Set WSSP-NEXTPROG to the current screen name (thus returning    */
		/*   returning to re-display the screen).                          */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*B59-EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			loadPolicy5010();
			readCovt5020();
			fieldsToScreen5080();
			mortalityLien5090();
			ageProcessing50a0();			
			optionsExtras50b0();
			enquiryProtect50c0();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadPolicy5010()
	{
		/* If Whole plan selected or component ADD, then we need to read*/
		/*   the COVRMJA file to find either the record to be changed or*/
		/*   the policies to which the component must be added.*/
		/* If COVRMJA statuz = ENDP then all policies have been processed*/
		/*   so exit this section.*/
		/* Else attempt a search of the COVTMJA temporary file for detail*/
		/*   changes.*/
		wsaaOldPrem.set(0);
		wsaaOldSumins.set(0);
		/*    MOVE 'Y' TO WSAA-FIRST-TIME.*/
		if (planLevel.isTrue()) {
			readCovr5100();
			for (Covrpf covr : covrpfList) {
					if (covr == null) {
						goTo(GotoLabel.exit50z0);
					}
					else {
						if (addComp.isTrue()) {
							covtpf.setCoverage(wsaaCovrCoverage.toString());
							covtpf.setRider(wsaaCovrRider.toString());
							covtpf.setPlnsfx(covr.getPlanSuffix());
						}
						else {
							/*             MOVE COVRMJA-DATA-KEY    TO COVTMJA-DATA-KEY*/
							covtpf.setChdrcoy(wsaaCovrChdrcoy.toString());
							covtpf.setChdrnum(wsaaCovrChdrnum.toString());
							covtpf.setLife(wsaaCovrLife.toString());
							covtpf.setCoverage(wsaaCovrCoverage.toString());
							covtpf.setRider(wsaaCovrRider.toString());
							if (isNE(covr.getPlanSuffix(),ZERO)) {
								covtpf.setPlnsfx(covr.getPlanSuffix());
							}
					}
				}
					covrpf.setPlanSuffix(covr.getPlanSuffix());
				}
				
			/*if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
				goTo(GotoLabel.exit50z0);
			}*/
			
		}
		wsaaOldSumins.set(covrpf.getSumins());
		wsaaOldPrem.set(covrpf.getInstprem());
		/* The header is set to show which the policy is being actioned.*/
		/* If all policies are within the summarised record then display*/
		/*   the number of policies summarised and set the Hi-light on*/
		/*   Plan suffix to display the literal -*/
		/*                  'Policy Number : 10 to 1'*/
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			sv.planSuffix.set(chdrpf.getPolsum());
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
	}

protected void readCovt5020()
	{
		List<Covtpf> covtList = covtpfDAO.getCovtpfData(covtpf.getChdrcoy()
				.trim(), covtpf.getChdrnum().trim(), covtpf.getLife()
				.trim(), covtpf.getCoverage().trim(), covtpf.getRider()
				.trim(), covtpf.getPlnsfx());
		
		if(!covtList.isEmpty()){
			covtpf = covtList.get(0);
		}
		/* Read the Temporary COVT record which will contain any previous*/
		/*   changes before AT submission creates the COVRMJA record.*/
		/* If entering on Component Change then just Display the details*/
		/*   as no defaults are necessary.*/
		/* Else we must load the Tables required for defaults.*/
		/*   note - Both statuz's are checked as we may previously have*/
		/*          read the file directly or sequentially.*/
		if (!addComp.isTrue()) {
			recordToScreen6000();
			return;
		}
		else {
			if(covtpf.getUniqueNumber()==0) {
				/*          MOVE COVRMJA-DATA-KEY TO COVTMJA-DATA-KEY*/
				covtpf.setAnbccd01(0);
				covtpf.setAnbccd02(0);
				covtpf.setSingp(BigDecimal.ZERO);
				covtpf.setPlnsfx(0);
				covtpf.setInstprem(BigDecimal.ZERO);
				covtpf.setZbinstprem(BigDecimal.ZERO);
				covtpf.setZlinstprem(BigDecimal.ZERO);
				/*BRD-306 START */
				covtpf.setLoadper(BigDecimal.ZERO);
				covtpf.setRateadj(BigDecimal.ZERO);
				covtpf.setFltmort(BigDecimal.ZERO);
				covtpf.setPremadj(BigDecimal.ZERO);
				covtpf.setAgeadj(BigDecimal.ZERO);
				/*BRD-306 END */
				covtpf.setPcesage(0);
				covtpf.setPcestrm(0);
				covtpf.setRcesage(0);
				covtpf.setRcestrm(0);
				covtpf.setBcesage(0);
				covtpf.setBcestrm(0);
				covtpf.setRundte(0);
				covtpf.setSumins(BigDecimal.ZERO);
				/*     MOVE CHDRMJA-BTDATE TO   COVTMJA-EFFDATE               */
				covtpf.setEffdate(payrpf.getBtdate());
				if (addComp.isTrue()) {
					covtpf.setEffdate(wsspcomn.currfrom.toInt());
				}
				covtpf.setRcesdte(varcom.vrcmMaxDate.toInt());
				covtpf.setPcesdte(varcom.vrcmMaxDate.toInt());
				covtpf.setBcesdte(varcom.vrcmMaxDate.toInt());
				covtpf.setPolinc(chdrpf.getPolinc());
				covtpf.setCrtable(wsaaCrtable.toString());
				covtpf.setJlife("  ");
				tableLoads7000();
			}
			else {
				tableLoads7000();
			}
		}
		if(covtpf.getJlife()==null) {
			covtpf.setJlife("  ");
		}
		if (isEQ(covtpf.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
	}

protected void fieldsToScreen5080()
	{
		/* Move fields to screen.*/
		sv.planSuffix.set(chdrpf.getPolinc());
		sv.liencd.set(covtpf.getLiencd());
		sv.mortcls.set(covtpf.getMortcls());
		sv.premCessDate.set(covtpf.getPcesdte());
		sv.premCessAge.set(covtpf.getPcesage());
		sv.premCessTerm.set(covtpf.getPcestrm());
		sv.riskCessDate.set(covtpf.getRcesdte());
		sv.riskCessAge.set(covtpf.getRcesage());
		sv.riskCessTerm.set(covtpf.getRcestrm());
		/*    MOVE COVTMJA-INSTPREM       TO SR571-INST-PREM.*/
		if (addComp.isTrue()
		|| addApp.isTrue()
		|| revComp.isTrue()) {
			r200ReadCprp();
			if (cprppf!=null) {
				sv.instPrem.set(cprppf.getInstprem());
			}
			else {
				sv.instPrem.set(covtpf.getInstprem());
			}
		}
		else {
			if (isEQ(sv.effdate, chdrpf.getPtdate())) {
				sv.instPrem.set(ZERO);
				sv.linstamt.set(covtpf.getInstprem());
			} else {
				sv.instPrem.set(covtpf.getInstprem());
				sv.linstamt.set(ZERO);
			}
		}
	
		sv.zbinstprem.set(covtpf.getZbinstprem());
		sv.zlinstprem.set(covtpf.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtpf.getLoadper());
		sv.rateadj.set(covtpf.getRateadj());
		sv.fltmort.set(covtpf.getFltmort());
		sv.premadj.set(covtpf.getPremadj());
		sv.adjustageamt.set(covtpf.getAgeadj());
		/*BRD-306 END */

		sv.sumin.set(covtpf.getSumins());
		setupBonus1300();
		sv.bappmeth.set(covtpf.getBappmeth());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/* The fields to  be displayed on the screen are determined from*/
		/*   the entry in table T5608 read earlier as follows:*/
		/*  - if the  maximum  and  minimum  sum insured amounts are*/
		/*       both  zero, non-display and protect the sum insured*/
		/*       amount.  If  the  minimum  and maximum are both the*/
		/*       same, display the amount protected. Otherwise allow*/
		/*       input.*/
		/* NOTE - the sum insured applies to the PLAN, so if it is to*/
		/*        be defaulted, scale it down according to the number*/
		/*        of policies applicable.*/
		if (isEQ(t5608rec.sumInsMax,0)
		&& isEQ(t5608rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5608rec.sumInsMax,t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5608rec.sumInsMax);
		}
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560 
		}
	}

protected void mortalityLien5090()
	{
		/* If all the valid mortality classes are blank, non-display and*/
		/*   protect this field. If there is only one mortality class,*/
		/*   display and protect it (no validation will be required).*/
		if (isEQ(t5608rec.mortclss,SPACES)) {
			sv.mortcls.set(SPACES);
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5608rec.mortcls01,SPACES)
		&& isEQ(t5608rec.mortcls02,SPACES)
		&& isEQ(t5608rec.mortcls03,SPACES)
		&& isEQ(t5608rec.mortcls04,SPACES)
		&& isEQ(t5608rec.mortcls05,SPACES)
		&& isEQ(t5608rec.mortcls06,SPACES)) {
			sv.mortcls.set(t5608rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/* If all the valid lien codes are blank, non-display and protect*/
		/*   this field. Otherwise, this is an optional field.*/
		if (isEQ(t5608rec.liencds,SPACES)) {
			sv.liencd.set(SPACES);
			/*sv.liencdOut[varcom.nd.toInt()].set("Y");*/
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5608rec.liencd01,SPACES)
		&& isEQ(t5608rec.liencd02,SPACES)
		&& isEQ(t5608rec.liencd03,SPACES)
		&& isEQ(t5608rec.liencd04,SPACES)
		&& isEQ(t5608rec.liencd05,SPACES)
		&& isEQ(t5608rec.liencd06,SPACES)) {
			sv.liencd.set(t5608rec.liencd01);
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void ageProcessing50a0()
	{
		/* Using the age next birthday (ANB at RCD) from the applicable*/
		/*   life (see above), look up Issue Age on the AGE and TERM*/
		/*   sections. If the age fits into a "slot" in one of these*/
		/*   sections, and the risk cessation limits are the same,*/
		/*   default and protect the risk cessation fields. Also do the*/
		/*   same for the premium cessation details. In this case, also*/
		/*   calculate the risk and premium cessation dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (isEQ(covtpf.getJlife(),"01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults5300();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& isNE(t5608rec.eaage,SPACES)) {
			riskCessDate5400();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& isNE(t5608rec.eaage,SPACES)) {
			premCessDate5450();
		}
		if (isNE(t5608rec.eaage,SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void optionsExtras50b0()
	{
		/* If options and extras are  not  allowed (as defined by T5608)*/
		/*   non-display and protect the fields.*/
		/* Otherwise,  read the  options and extras  details  for  the*/
		/*   current coverage/rider. If any records  exist, put a '+' in*/
		/*   the Options/Extras indicator (to show that there are some).*/
		if (isEQ(t5608rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
		/*B2-PREMIUM-BREAKDOWN*/
		/* As per other 'window box' checks the premium breakdown file     */
		/* needs to be checked for the existance of a record. Store the    */
		/* values here so that a general check can be made prior to        */
		/* displaying the screen in all cases ......                       */
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
	}

protected void enquiryProtect50c0()
	{
		/* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to*/
		/*   protect  all input capable  fields  except  the  indicators*/
		/*   controlling where to  switch  to  next  (options and extras*/
		/*   indicator).*/
		/*    IF COMP-ENQUIRY                                              */
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");//ILIFE-3519
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void readCovr5100()
	{
		/*READ-COVRMJA*/
		/* Read the Coverage file.*/
		/*SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(),wsaaCovrChdrcoy)
		|| isNE(covrmjaIO.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrmjaIO.getLife(),wsaaCovrLife)
		|| isNE(covrmjaIO.getCoverage(),wsaaCovrCoverage)
		|| isNE(covrmjaIO.getRider(),wsaaCovrRider)
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
			if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
					|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
					|| isNE(covr.getLife(),wsaaCovrLife)
					|| isNE(covr.getCoverage(),wsaaCovrCoverage)
					|| isNE(covr.getRider(),wsaaCovrRider)
					|| (!covrpfList.isEmpty())) {
						return;
					}
		}
		
		/*if (isEQ(covrmjaIO.getFunction(),varcom.nextr)
		&& isEQ(covrmjaIO.getPlanSuffix(),wsaaPlanSuffix)) {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
	}

protected void calcPremium5200()
	{
		if(calcPremium5210()){
			methodTable5220();
		}
	}

protected boolean calcPremium5210()
	{
		/* Use the premium-method selected from T5687, if not blank THEN*/
		/*   access T5675. This gives the subroutine for the calculation.*/
		/* If the benefit billing method is not blank, non-display and*/
		/*   protect the premium field (so skip the following).*/
		/*MOVE 'N'                    TO PREM-REQD.                    */
		wsaaPremStatuz.set("N");
		if (isNE(t5687rec.bbmeth,SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			return false;
		}
		return true;
	}

protected void methodTable5220()
	{
		boolean itemFound = false;
		List<Itempf> t5675List = itemDAO.getAllitemsbyCurrency("IT", chdrpf.getChdrcoy().toString(), "T5675", t5675Itemitem);
		if (t5675List != null && !t5675List.isEmpty()) {
			Itempf itempf = t5675List.get(0);
			//ILIFE-4001 start
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itempf.getItemitem());
			}
			//ILIFE-4001 end 
			t5675rec.t5675Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound = true;
		}
		if(!itemFound){
			wsaaPremStatuz.set("Y");
		}
	}

/**
* <pre>
* Using the age next birthday (ANB at RCD) from the applicable
*   life (see above), look up Issue Age on the AGE and TERM
*   sections. If the age fits into a "slot" in one of these
*   sections, and the risk cessation limits are the same,
*   default and protect the risk cessation fields. Also do the
*   same for the premium cessation details. In this case, also
*   calculate the risk and premium cessation dates.
* </pre>
*/
protected void checkDefaults5300()
	{
		sub1.set(1);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
		while ( !(isGT(sub1,wsaaMaxOcc))) {
			nextColumn5320();
		}
		
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void nextColumn5320()
	{
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.riskCessageFrom[sub1.toInt()],t5608rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(t5608rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.premCessageFrom[sub1.toInt()],t5608rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5608rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.riskCesstermFrom[sub1.toInt()],t5608rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(t5608rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.premCesstermFrom[sub1.toInt()],t5608rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5608rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		sub1.add(1);
	}

	/**
	* <pre>
	*5400-RISK-CESS-DATE SECTION.                                     
	*5400-PARA.                                                       
	*    IF SR571-RISK-CESS-AGE      = 0                              
	*       PERFORM 5410-RISK-CESS-TERM                               
	*       GO TO 5440-EXIT.                                          
	*    IF T5608-EAAGE              = 'A'                            
	*      IF NOT ADD-COMP                                            
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR571-RISK-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5608-EAAGE              = 'A'                            
	*      IF ADD-COMP                                                
	*       MOVE CHDRMJA-BTDATE     TO DTC2-INT-DATE-1                
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR571-RISK-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5608-EAAGE              = 'E' OR SPACE                   
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       MOVE SR571-RISK-CESS-AGE TO DTC2-FREQ-FACTOR.             
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR571-RCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR571-RISK-CESS-DATE.         
	*    GO TO 5440-EXIT.                                             
	*5410-RISK-CESS-TERM.                                             
	*    IF SR571-RISK-CESS-TERM     = 0                              
	*        MOVE E186               TO SR571-RCESDTE-ERR             
	*        GO TO 5440-EXIT.                                         
	*    IF T5608-EAAGE              = 'A' OR SPACE                   
	*       MOVE SR571-RISK-CESS-TERM TO DTC2-FREQ-FACTOR             
	*       IF ADD-COMP                                               
	*          MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1            
	*       ELSE                                                      
	*          MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1.           
	*    IF T5608-EAAGE              = 'E'                            
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       ADD SR571-RISK-CESS-TERM, WSZZ-ANB-AT-CCD                 
	*                                GIVING DTC2-FREQ-FACTOR          
	*    IF NOT ADD-COMP                                              
	*       IF CHDRMJA-OCCDATE NOT = WSZZ-CLTDOB                      
	*          SUBTRACT 1          FROM DTC2-FREQ-FACTOR.             
	*    IF T5608-EAAGE              = 'E'                            
	*     IF ADD-COMP                                                 
	*       IF CHDRMJA-BTDATE      NOT = WSZZ-CLTDOB                  
	*          SUBTRACT 1          FROM DTC2-FREQ-FACTOR.             
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR571-RCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR571-RISK-CESS-DATE.         
	*5440-EXIT.                                                       
	*    EXIT.                                                        
	*5450-PREM-CESS-DATE SECTION.                                     
	*5450-PARA.                                                       
	*    IF SR571-PREM-CESS-AGE      = 0                              
	*       PERFORM 5460-PREM-CESS-TERM                               
	*       GO TO 5490-EXIT.                                          
	*    IF T5608-EAAGE              = 'A'                            
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR571-PREM-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5608-EAAGE              = 'A'                            
	*    IF ADD-COMP                                                  
	*       MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR571-PREM-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5608-EAAGE              = 'E' OR SPACE                   
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       MOVE SR571-PREM-CESS-AGE TO DTC2-FREQ-FACTOR.             
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR571-PCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR571-PREM-CESS-DATE.         
	*    GO TO 5490-EXIT.                                             
	*5460-PREM-CESS-TERM.                                             
	*    IF SR571-PREM-CESS-TERM     = 0                              
	*        GO TO 5490-EXIT.                                         
	*    IF T5608-EAAGE              = 'A' OR SPACE                   
	*       MOVE SR571-PREM-CESS-TERM TO DTC2-FREQ-FACTOR             
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*    ELSE                                                         
	*       MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1.              
	*    IF T5608-EAAGE              = 'E'                            
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       ADD SR571-PREM-CESS-TERM, WSZZ-ANB-AT-CCD                 
	*                                GIVING DTC2-FREQ-FACTOR          
	*    IF NOT ADD-COMP                                              
	*       IF CHDRMJA-OCCDATE       NOT = WSZZ-CLTDOB                
	*          SUBTRACT 1            FROM DTC2-FREQ-FACTOR.           
	*    IF T5608-EAAGE              = 'E'                            
	*    IF  ADD-COMP                                                 
	*       IF CHDRMJA-BTDATE        NOT = WSZZ-CLTDOB                
	*          SUBTRACT 1            FROM DTC2-FREQ-FACTOR.           
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR571-PCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR571-PREM-CESS-DATE.         
	*5490-EXIT.                                                       
	*     EXIT.                                                       
	* </pre>
	*/
protected void riskCessDate5400()
	{
		if (isEQ(sv.riskCessAge,ZERO)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.riskCessAge,0),true)
		&& isEQ(isGT(sv.riskCessTerm,0),false)){
			if (isEQ(t5608rec.eaage,"A")) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <034>*/
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1          */
					datcon2rec.intDate1.set(chdrpf.getOccdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge,wsaaAnb));
			}
			if (isEQ(t5608rec.eaage,"E")
			|| isEQ(t5608rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.riskCessAge);
			}
		}
		else if (isEQ(isGT(sv.riskCessAge,0),false)
		&& isEQ(isGT(sv.riskCessTerm,0),true)){
			if (isEQ(t5608rec.eaage,"A")
			|| isEQ(t5608rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <034>*/
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1          */
					datcon2rec.intDate1.set(chdrpf.getOccdate());
					wsaaTempPrev.set(chdrpf.getOccdate());
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.freqFactor.set(1);
					while ( !(isGT(datcon2rec.intDate2,sv.effdate))) {
						callDatcon25500();
						wsaaTempPrev.set(datcon2rec.intDate1);
						datcon2rec.intDate1.set(datcon2rec.intDate2);
					}
					
					datcon2rec.intDate1.set(wsaaTempPrev);
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.riskCessTerm);
			}
			if (isEQ(t5608rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(add(sv.riskCessTerm,wsaaAnb));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.riskCessDate.set(datcon2rec.intDate2);
		}
	}

protected void premCessDate5450()
	{
		if (isEQ(sv.premCessAge,ZERO)
		&& isEQ(sv.premCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.premCessAge,0),true)
		&& isEQ(isGT(sv.premCessTerm,0),false)){
			if (isEQ(t5608rec.eaage,"A")) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <034>*/
					datcon2rec.intDate1.set(payrpf.getBtdate());
					datcon2rec.intDate1.set(chdrpf.getOccdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge,wsaaAnb));
			}
			if (isEQ(t5608rec.eaage,"E")
			|| isEQ(t5608rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.premCessAge);
			}
		}
		else if (isEQ(isGT(sv.premCessAge,0),false)
		&& isEQ(isGT(sv.premCessTerm,0),true)){
			if (isEQ(t5608rec.eaage,"A")
			|| isEQ(t5608rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <034>*/
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1          */
					datcon2rec.intDate1.set(chdrpf.getOccdate());
					wsaaTempPrev.set(chdrpf.getOccdate());
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.freqFactor.set(1);
					while ( !(isGT(datcon2rec.intDate2,sv.effdate))) {
						callDatcon25500();
						wsaaTempPrev.set(datcon2rec.intDate1);
						datcon2rec.intDate1.set(datcon2rec.intDate2);
					}
					
					datcon2rec.intDate1.set(wsaaTempPrev);
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.premCessTerm);
			}
			if (isEQ(t5608rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(add(sv.premCessTerm,wsaaAnb));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.premCessDate.set(datcon2rec.intDate2);
		}
	}

protected void callDatcon25500()
	{
		/*CALL*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkLext5600()
	{
		/* Read the Options and Extras record for the Component.*/
		/* MOVE COVTMJA-CHDRCOY        TO LEXT-CHDRCOY.                 */
		/* MOVE COVTMJA-CHDRNUM        TO LEXT-CHDRNUM.                 */
		/* MOVE COVTMJA-LIFE           TO LEXT-LIFE.                    */
		/* MOVE COVTMJA-COVERAGE       TO LEXT-COVERAGE.                */
		/* MOVE COVTMJA-RIDER          TO LEXT-RIDER.                   */
		/* MOVE 0                      TO LEXT-SEQNBR.                  */
		/* MOVE LEXTREC                TO LEXT-FORMAT.                  */
		/* MOVE BEGN                   TO LEXT-FUNCTION.                */
		/* CALL 'LEXTIO' USING LEXT-PARAMS.                             */
		/* IF LEXT-STATUZ              NOT = O-K AND                    */
		/*                             NOT = ENDP                       */
		/*    MOVE LEXT-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* IF COVTMJA-CHDRCOY          NOT = LEXT-CHDRCOY               */
		/* OR COVTMJA-CHDRNUM          NOT = LEXT-CHDRNUM               */
		/* OR COVTMJA-LIFE             NOT = LEXT-LIFE                  */
		/* OR COVTMJA-COVERAGE         NOT = LEXT-COVERAGE              */
		/* OR COVTMJA-RIDER            NOT = LEXT-RIDER                 */
		/*    MOVE ENDP                TO LEXT-STATUZ.                  */
		/* IF LEXT-STATUZ              = ENDP                           */
		/*    MOVE ' '                 TO SR571-OPTEXTIND               */
		/* ELSE                                                         */
		/*    MOVE '+'                 TO SR571-OPTEXTIND.              */
		wsaaLextUpdates.set("N");
		Lextpf lextrevIO = new Lextpf();
		lextrevIO.setChdrcoy(chdrpf.getChdrcoy().toString());
		lextrevIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		lextrevIO.setLife(covtpf.getLife());//IJTI-1793
		lextrevIO.setCoverage(covtpf.getCoverage());//IJTI-1793
		lextrevIO.setRider(covtpf.getRider());//IJTI-1793
		
		List<Lextpf> lextList = lextpfDAO.getLextpfListRecord(lextrevIO);
		if (lextList.isEmpty()) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
			firstLext = lextList.remove(0);
		}
		/* Extra check in here to see if any LEXT records associated with  */
		/*  this component have been amended whilst we have been here in   */
		/*  component Add/Modify - check TRANNOs on LEXTREV records        */
		/*  against the current TRANNO held on the Contract Header we      */
		/*  are currently using in the CHDRMJA logical.                    */
		if (firstLext != null) {
			if (isGT(firstLext.getTranno(),chdrpf.getTranno())) {
				wsaaLextUpdates.set("Y");
				lextList.clear();
				firstLext = null;
			}
		}
		for(Lextpf lextpf:lextList){
			if (isGT(lextpf.getTranno(),chdrpf.getTranno())) {
				wsaaLextUpdates.set("Y");
				firstLext = null;
				break;
			}
		}
		readFlupal1500();
	}

	/**
	* <pre>
	*5650-CHECK-RACT SECTION.                                 <R96REA>
	*************************                                 <R96REA>
	*5650-READ-RACT.                                          <R96REA>
	****                                                      <R96REA>
	**** MOVE CHDRMJA-CHDRCOY        TO RACT-CHDRCOY.         <R96REA>
	**** MOVE CHDRMJA-CHDRNUM        TO RACT-CHDRNUM.         <R96REA>
	**** MOVE SR571-LIFE             TO RACT-LIFE.            <R96REA>
	**** MOVE SR571-COVERAGE         TO RACT-COVERAGE.        <R96REA>
	**** MOVE SR571-RIDER            TO RACT-RIDER.           <R96REA>
	**** MOVE RACTREC                TO RACT-FORMAT.          <R96REA>
	**** MOVE BEGN                   TO RACT-FUNCTION.        <R96REA>
	**** CALL 'RACTIO' USING RACT-PARAMS.                     <R96REA>
	**** IF RACT-STATUZ              NOT = O-K AND            <R96REA>
	****                             NOT = ENDP               <R96REA>
	****    MOVE RACT-PARAMS         TO SYSR-PARAMS           <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** IF CHDRMJA-CHDRCOY   NOT = RACT-CHDRCOY              <R96REA>
	****  OR CHDRMJA-CHDRNUM  NOT = RACT-CHDRNUM              <R96REA>
	****  OR SR571-LIFE       NOT = RACT-LIFE                 <R96REA>
	****  OR SR571-COVERAGE   NOT = RACT-COVERAGE             <R96REA>
	****  OR SR571-RIDER      NOT = RACT-RIDER                <R96REA>
	****    MOVE ENDP                TO RACT-STATUZ.          <R96REA>
	**** IF RACT-STATUZ              = ENDP                   <R96REA>
	****    MOVE ' '                 TO SR571-RATYPIND        <R96REA>
	**** ELSE                                                 <R96REA>
	****    MOVE '+'                 TO SR571-RATYPIND.       <R96REA>
	****                                                      <R96REA>
	****                                                      <R96REA>
	**Extra check in here to see if any RACT records associate<R96REA>
	***this component have been amended whilst we have been he<R96REA>
	***component Add/Modify - check TRANNOs on RACTRCC records<R96REA>
	***against the current TRANNO held on the Contract Header <R96REA>
	***are currently using in the CHDRMJA logical.            <R96REA>
	****                                                      <R96REA>
	**** IF RACT-STATUZ              NOT = ENDP               <R96REA>
	****     IF RACT-TRANNO          > CHDRMJA-TRANNO         <R96REA>
	****         MOVE 'Y'            TO WSAA-RACT-UPDATES     <R96REA>
	****         MOVE ENDP           TO RACT-STATUZ           <R96REA>
	****         GO TO 5690-EXIT                              <R96REA>
	****     END-IF                                           <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	**** MOVE SPACES                 TO RACTRCC-PARAMS.       <R96REA>
	**** MOVE CHDRMJA-CHDRCOY        TO RACTRCC-CHDRCOY.      <R96REA>
	**** MOVE CHDRMJA-CHDRNUM        TO RACTRCC-CHDRNUM.      <R96REA>
	**** MOVE SR571-LIFE             TO RACTRCC-LIFE.         <R96REA>
	**** MOVE SR571-COVERAGE         TO RACTRCC-COVERAGE.     <R96REA>
	**** MOVE SR571-RIDER            TO RACTRCC-RIDER.        <R96REA>
	**** MOVE SPACES                 TO RACTRCC-RASNUM.       <R96REA>
	**** MOVE SPACES                 TO RACTRCC-RATYPE.       <R96REA>
	**** MOVE SPACES                 TO RACTRCC-VALIDFLAG.    <R96REA>
	**** MOVE RACTRCCREC             TO RACTRCC-FORMAT.       <R96REA>
	**** MOVE BEGN                   TO RACTRCC-FUNCTION.     <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTRCC-STATUZ           NOT = O-K                <R96REA>
	****    AND RACTRCC-STATUZ       NOT = ENDP               <R96REA>
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS           <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** IF CHDRMJA-CHDRCOY          NOT = RACTRCC-CHDRCOY    <R96REA>
	****  OR CHDRMJA-CHDRNUM         NOT = RACTRCC-CHDRNUM    <R96REA>
	****  OR SR571-LIFE              NOT = RACTRCC-LIFE       <R96REA>
	****  OR SR571-COVERAGE          NOT = RACTRCC-COVERAGE   <R96REA>
	****  OR SR571-RIDER             NOT = RACTRCC-RIDER      <R96REA>
	****     MOVE ENDP               TO RACTRCC-STATUZ        <R96REA>
	****  END-IF.                                             <R96REA>
	****                                                      <R96REA>
	**** PERFORM                     UNTIL RACTRCC-STATUZ = EN<R96REA>
	****                                                      <R96REA>
	****     MOVE NEXTR              TO RACTRCC-FUNCTION      <R96REA>
	****                                                      <R96REA>
	****     CALL 'RACTRCCIO'        USING RACTRCC-PARAMS     <R96REA>
	****                                                      <R96REA>
	****     IF RACTRCC-STATUZ       NOT = O-K                <R96REA>
	****        AND RACTRCC-STATUZ   NOT = ENDP               <R96REA>
	****         MOVE RACTRCC-PARAMS TO SYSR-PARAMS           <R96REA>
	****         MOVE RACTRCC-STATUZ TO SYSR-STATUZ           <R96REA>
	****         PERFORM 600-FATAL-ERROR                      <R96REA>
	****     END-IF                                           <R96REA>
	****                                                      <R96REA>
	****     IF CHDRMJA-CHDRCOY      NOT = RACTRCC-CHDRCOY    <R96REA>
	****      OR CHDRMJA-CHDRNUM     NOT = RACTRCC-CHDRNUM    <R96REA>
	****      OR SR571-LIFE          NOT = RACTRCC-LIFE       <R96REA>
	****      OR SR571-COVERAGE      NOT = RACTRCC-COVERAGE   <R96REA>
	****      OR SR571-RIDER         NOT = RACTRCC-RIDER      <R96REA>
	****        MOVE ENDP            TO RACTRCC-STATUZ        <R96REA>
	****     ELSE                                             <R96REA>
	****         IF RACTRCC-TRANNO   > CHDRMJA-TRANNO         <R96REA>
	****             MOVE 'Y'        TO WSAA-RACT-UPDATES     <R96REA>
	****             MOVE ENDP       TO RACTRCC-STATUZ        <R96REA>
	****         END-IF                                       <R96REA>
	****     END-IF                                           <R96REA>
	****                                                      <R96REA>
	**** END-PERFORM.                                         <R96REA>
	****                                                      <R96REA>
	*5690-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	* </pre>
	*/
protected void checkPcdt5700()
	{
		/* COMMISSION SPLIT*/
		/* MOVE READR                  TO PCDTMJA-FUNCTION.             */
	    pcdtpf = new Pcdtpf();
	
	    pcdtpf.setChdrcoy(covtpf.getChdrcoy());
	    pcdtpf.setChdrnum(covtpf.getChdrnum());
	    pcdtpf.setLife(covtpf.getLife());
	    pcdtpf.setCoverage(covtpf.getCoverage());
	    pcdtpf.setRider(covtpf.getRider());
	    pcdtpf.setPlnsfx(covtpf.getPlnsfx());
		pcdtpf.setTranno(add(chdrpf.getTranno(),1).toInt());

		pcdtpf = pcdtpfDAO.getPcdtmjaByKey(pcdtpf);
		
		if (pcdtpf == null) {
			sv.comind.set(" ");
		}
		else {
			sv.comind.set("+");
		}
	}

protected void recordToScreen6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covrRecord6010();
					fieldsToScreen6020();
				case covtExist6025: 
					covtExist6025();
					enquiryProtect6030();
					optionsExtras603a();
					premiumBreakdown603c();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covrRecord6010()
	{
		/* Move the COVR Details to the COVT record as the Coding moves*/
		/*   the COVT fields to the screen.*/
		if(covtpf.getUniqueNumber()==0)  
		{
			covtpf.setSex01(wsaaSex.toString());
			covtpf.setSex02(wsbbSex.toString());
			covtpf.setAnbccd01(wsaaAnbAtCcd.toInt());
			covtpf.setAnbccd02(wsbbAnbAtCcd.toInt());
			covtpf.setCrtable(covrpf.getCrtable());
			covtpf.setRcesdte(covrpf.getRiskCessDate());
			covtpf.setPcesdte(covrpf.getPremCessDate());
			covtpf.setRcesage(covrpf.getRiskCessAge());
			covtpf.setPcesage(covrpf.getPremCessAge());
			covtpf.setRcestrm(covrpf.getRiskCessTerm());
			covtpf.setPcestrm(covrpf.getPremCessTerm());
			covtpf.setRundte(covrpf.getReserveUnitsDate());
			covtpf.setRsunin(covrpf.getReserveUnitsInd());
			covtpf.setSumins(covrpf.getSumins());
			covtpf.setInstprem(covrpf.getInstprem());
			covtpf.setZbinstprem(covrpf.getZbinstprem());
			covtpf.setZlinstprem(covrpf.getZlinstprem());
			/*BRD-306 START */
			covtpf.setLoadper(covrpf.getLoadper());
			covtpf.setRateadj(covrpf.getRateadj());
			covtpf.setFltmort(covrpf.getFltmort());
			covtpf.setPremadj(covrpf.getPremadj());
			covtpf.setAgeadj(covrpf.getAgeadj());
			/*BRD-306 END */
			covtpf.setMortcls(covrpf.getMortcls());
			covtpf.setLiencd(covrpf.getLiencd());
			covtpf.setJlife(covrpf.getJlife());
			covtpf.setEffdate(covrpf.getCrrcd());
			/*  MOVE CHDRMJA-BILLFREQ       TO COVTMJA-BILLFREQ           */
			/*  MOVE CHDRMJA-BILLCHNL       TO COVTMJA-BILLCHNL           */
			covtpf.setBillfreq(payrpf.getBillfreq());
			covtpf.setBillchnl(payrpf.getBillchnl());
			covtpf.setSingp(covrpf.getSingp());
			sv.statFund.set(covrpf.getStatFund());
			sv.statSect.set(covrpf.getStatSect());
			covtpf.setPayrseqno(covrpf.getPayrseqno());
			covtpf.setBappmeth(covrpf.getBappmeth());
			sv.statSubsect.set(covrpf.getStatSubsect());
			hcsdpf = new Hcsdpf();
			hcsdpf.setChdrcoy(covrpf.getChdrcoy());/* IJTI-1386 */
			hcsdpf.setChdrnum(covrpf.getChdrnum());/* IJTI-1386 */
			hcsdpf.setLife(covrpf.getLife());/* IJTI-1386 */
			hcsdpf.setCoverage(wsaaCovrCoverage.toString());
			hcsdpf.setRider(wsaaCovrRider.toString());
			hcsdpf.setPlnsfx(0);		
			hsdpfList = hcsdpfDAO.searchHcsdpfRecord(hcsdpf);
			if(hsdpfList != null && !hsdpfList.isEmpty()){
				covtpf.setZdivopt(hsdpfList.get(0).getZdivopt());
				covtpf.setPaycurr(hsdpfList.get(0).getPaycurr());
				covtpf.setPayclt(hsdpfList.get(0).getPayclt());
				covtpf.setPaycoy(hsdpfList.get(0).getPaycoy());
				covtpf.setPaymth(hsdpfList.get(0).getPaymth());
				covtpf.setBankkey(hsdpfList.get(0).getBankkey());
				covtpf.setBankacckey(hsdpfList.get(0).getBankacckey());
				covtpf.setFacthous(hsdpfList.get(0).getFacthous());
			}	
		}
		covtpf.setPolinc(chdrpf.getPolinc());
	}

protected void fieldsToScreen6020()
	{
	    if(covtpf.getJlife()==null) {
	    	covtpf.setJlife("  ");
	    }
		if (isEQ(covtpf.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		if (covtpf.getUniqueNumber() !=0) {
			goTo(GotoLabel.covtExist6025);
		}
		/* If a summarised record then breakout the Inst.Premium & Sumin*/
		/*   for the number of summarised policies within the Plan, this*/
		/*   is only applicable if a COVT does not already exist in which*/
		/*   case it has already been broken down before.*/
		/* Note - the 1st policy in the summarised record will contain*/
		/*        any division remainder.*/
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(sv.sumin, 2).set((sub(covtpf.getSumins(),(div(mult(covtpf.getSumins(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				/*          COMPUTE SR571-INST-PREM =*/
				compute(sv.linstamt, 2).set((sub(covtpf.getInstprem(),(div(mult(covtpf.getInstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zbinstprem, 2).set((sub(covtpf.getZbinstprem(),(div(mult(covtpf.getZbinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zlinstprem, 2).set((sub(covtpf.getZlinstprem(),(div(mult(covtpf.getZlinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(sv.sumin, 3).setRounded(div(covtpf.getSumins(),chdrpf.getPolsum()));
				compute(sv.linstamt, 3).setRounded(div(covtpf.getInstprem(),chdrpf.getPolsum()));
				compute(sv.zbinstprem, 3).setRounded(div(covtpf.getZbinstprem(),chdrpf.getPolsum()));
				compute(sv.zlinstprem, 3).setRounded(div(covtpf.getZlinstprem(),chdrpf.getPolsum()));
			}
		}
		else {
			sv.sumin.set(covtpf.getSumins());
			sv.zbinstprem.set(covtpf.getZbinstprem());
			sv.zlinstprem.set(covtpf.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtpf.getLoadper());
			sv.rateadj.set(covtpf.getRateadj());
			sv.fltmort.set(covtpf.getFltmort());
			sv.premadj.set(covtpf.getPremadj());
			sv.adjustageamt.set(covtpf.getAgeadj());
			/*BRD-306 END */

			/*       MOVE COVTMJA-INSTPREM TO SR571-INST-PREM.*/
			sv.linstamt.set(covtpf.getInstprem());
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(wsaaOldSumins, 0).set((sub(wsaaOldSumins,(div(mult(wsaaOldSumins,(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(wsaaOldPrem, 2).set((sub(wsaaOldPrem,(div(mult(wsaaOldPrem,(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(wsaaOldSumins, 1).setRounded(div(wsaaOldSumins,chdrpf.getPolsum()));
				compute(wsaaOldPrem, 3).setRounded(div(wsaaOldPrem,chdrpf.getPolsum()));
			}
		}
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560 
		}
	}

protected void covtExist6025()
	{
		if (covtpf.getUniqueNumber()>0) {
			sv.sumin.set(covtpf.getSumins());
			sv.zbinstprem.set(covtpf.getZbinstprem());
			sv.zlinstprem.set(covtpf.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtpf.getLoadper());
			sv.rateadj.set(covtpf.getRateadj());
			sv.fltmort.set(covtpf.getFltmort());
			sv.premadj.set(covtpf.getPremadj());
			sv.adjustageamt.set(covtpf.getAgeadj());
			/*BRD-306 END */

			/*       MOVE COVTMJA-INSTPREM TO SR571-INST-PREM.*/
			sv.linstamt.set(covtpf.getInstprem());
			if (addComp.isTrue()
			|| addApp.isTrue()
			|| revComp.isTrue()) {
				r200ReadCprp();
				if (cprppf != null) {
					sv.instPrem.set(cprppf.getInstprem());
				}
				else {
					sv.instPrem.set(covtpf.getInstprem());
				}
			}
			else {
				sv.instPrem.set(covtpf.getInstprem());
			}
		}
		sv.liencd.set(covtpf.getLiencd());
		sv.mortcls.set(covtpf.getMortcls());
		sv.premCessDate.set(covtpf.getPcesdte());
		sv.premCessAge.set(covtpf.getPcesage());
		sv.premCessTerm.set(covtpf.getPcestrm());
		sv.riskCessDate.set(covtpf.getRcesdte());
		sv.riskCessAge.set(covtpf.getRcesage());
		sv.riskCessTerm.set(covtpf.getRcestrm());
		setupBonus1300();
		sv.bappmeth.set(covtpf.getBappmeth());
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560
		}
	}

protected void enquiryProtect6030()
	{
		/* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to*/
		/*   protect all input  capable  fields  except  the  indicators*/
		/*   controlling where to  switch  to  next  (options and extras*/
		/*   indicator).*/
		/*    IF WSSP-FLAG                = 'I'                            */
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");//ILIFE-3519
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
		/* If we have a record to display then we must load all the tables*/
		/*   required for Validation in the 2000-section.*/
		if (!addComp.isTrue()) {
			tableLoads7000();
		}
		/* If the  maximum  and  minimum  sum insured amounts are both     */
		/* zero, non-display and protect the sum insured field. If the     */
		/* minimum and maximum are both the same, display the amount       */
		/* protected. Otherwise allow input.                               */
		if (isEQ(t5608rec.sumInsMax,0)
		&& isEQ(t5608rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5608rec.sumInsMax,t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5608rec.sumInsMax);
		}
	}

protected void optionsExtras603a()
	{
		/* If options and extras are  not  allowed (as defined by T5608)   */
		/*   non-display and protect the fields.                           */
		/* Otherwise,  read the  options and extras  details  for  the     */
		/*   current coverage/rider. If any records  exist, put a '+' in   */
		/*   the Options/Extras indicator (to show that there are some).   */
		if (isEQ(t5608rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
	}

	/**
	* <pre>
	*603B-REASSURANCE.                                        <R96REA>
	******************                                        <R96REA>
	****                                                      <R96REA>
	**REASSURANCE                                             <R96REA>
	****                                                      <R96REA>
	**If reassurance is not allowed                           <R96REA>
	**non-display and protect the field.                      <R96REA>
	****                                                      <R96REA>
	**Otherwise,  read the  reassurance details  for  the  cur<R96REA>
	**coverage/rider.  If any  records  exist, put a '+' in  t<R96REA>
	**reassurance indicator (to show that there is one).      <R96REA>
	****                                                      <R96REA>
	**** IF T5687-RIIND              = 'N' OR SPACES          <R96REA>
	****    MOVE 'Y'                 TO SR571-RATYPIND-OUT (ND<R96REA>
	**** ELSE                                                 <R96REA>
	****    PERFORM 5650-CHECK-RACT.                          <R96REA>
	****                                                      <R96REA>
	****                                                      <R96REA>
	* </pre>
	*/
protected void premiumBreakdown603c()
	{
		/* As per other 'window box' checks the premium breakdown file     */
		/* needs to be checked for the existance of a record. Store the    */
		/* values here so that a general check can be made prior to        */
		/* displaying the screen in all cases .....                        */
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/*EXIT*/
	}

protected void r200ReadCprp()
 {
		cprppf = new Cprppf();	
		cprppf.setChdrpfx(chdrpf.getChdrpfx());//ILIFE-8187
		cprppf.setChdrcoy(chdrpf.getChdrcoy().toString());
		cprppf.setChdrnum(chdrpf.getChdrnum());//ILIFE-8187
		cprppf.setLife(covtpf.getLife());//IJTI-1793
		cprppf.setCoverage(covtpf.getCoverage());//IJTI-1793
		cprppf.setRider(covtpf.getRider());//IJTI-1793

		cprppf = cprppfDAO.getCprpRecord(cprppf);
}

protected void tableLoads7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t5687Load7010();
					t5671Load7020();
				case itemCall7023: 
					itemCall7023();
					editRules7030();
					t5608Load7040();
					t5667Load7045();
					t5675Load7050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t5687Load7010()
	{
		/* General Coverage/Rider details.*/
		boolean itemFound = false;
		List<Itempf> t5687List = itemDAO.getAllitemsbyCurrency("IT", chdrpf.getChdrcoy().toString(), "T5687", wsaaCrtable.toString());
		if (t5687List != null && !t5687List.isEmpty()) {
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687List.get(0).getGenarea()));
			itemFound = true;
		}
		if(!itemFound){
			scrnparams.errorCode.set(errorsInner.f294);
			fatalError600();
		}
	}

	/**
	* <pre>
	**** IF T5687-RIIND              = 'N' OR SPACES          <R96REA>
	****    MOVE SPACES              TO SR571-RATYPIND        <R96REA>
	****    MOVE 'Y'                 TO SR571-RATYPIND-OUT(ND)<R96REA>
	* </pre>
	*/
protected void t5671Load7020()
	{
		/* Coverage/Rider Switching.*/
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		/*    MOVE COVTMJA-CRTABLE        TO WSBB-CRTABLE.                 */
		wsbbCrtable.set(wsaaCrtable);
	}

protected void itemCall7023()
	{
		boolean itemFound = false;
		List<Itempf> t5671List = itemDAO.getAllitemsbyCurrency("IT", chdrpf.getChdrcoy().toString(), "T5671", wsbbTranCrtable.toString());
		if (t5671List != null && !t5671List.isEmpty()) {
			t5671rec.t5671Rec.set(StringUtil.rawToString(t5671List.get(0).getGenarea()));
			itemFound = true;
		}
		/* IF ITEM-STATUZ NOT = O-K */
		/* MOVE ITEM-PARAMS TO SYSR-PARAMS */
		/* PERFORM 600-FATAL-ERROR */
		if (!itemFound) {
			wsbbCrtable.set("****");
			goTo(GotoLabel.itemCall7023);
		}

	}

protected void editRules7030()
	{
		/* Find the Edit rules associated with the program.*/
		if (isEQ(t5671rec.pgm[1],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
	}

protected void t5608Load7040()
	{
		/* Term Component edit rules.*/
	
		boolean itemFound = false;
		wsbbCurrency.set(payrpf.getCntcurr());
		//List<Itempf> t5608List = itemDAO.getAllitemsbyCurrency("IT", chdrmjaIO.getChdrcoy().toString(), "T5608", wsbbTranCurrency.toString().trim());
		List<Itempf> t5608List = itemDAO.getAllItemitemByDateFrm("IT", chdrpf.getChdrcoy().toString(), "T5608", wsbbTranCurrency.toString().trim(), chdrpf.getCcdate()); //ILFE-7560
		if (t5608List != null && !t5608List.isEmpty()) {
			Iterator<Itempf> iterator = t5608List.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if(itempf.getItmfrm().compareTo(new BigDecimal(chdrpf.getOccdate().toString())) <=0 ){
					t5608rec.t5608Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
					break;
				}
			}
		}
		if(!itemFound){
			t5608rec.t5608Rec.set(SPACES);
			t5608rec.ageIssageFrms.fill("0");
			t5608rec.ageIssageTos.fill("0");
			t5608rec.termIssageFrms.fill("0");
			t5608rec.termIssageTos.fill("0");
			t5608rec.premCessageFroms.fill("0");
			t5608rec.premCessageTos.fill("0");
			t5608rec.premCesstermFroms.fill("0");
			t5608rec.premCesstermTos.fill("0");
			t5608rec.riskCessageFroms.fill("0");
			t5608rec.riskCessageTos.fill("0");
			t5608rec.riskCesstermFroms.fill("0");
			t5608rec.riskCesstermTos.fill("0");
			t5608rec.sumInsMax.set(ZERO);
			t5608rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode,SPACES)) {
				scrnparams.errorCode.set(errorsInner.f335);
			}
		}
		//ILIFE-3519:Start
		if(isEQ(t5608rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-3519:End
		else{
			prmbasisFlag=true;
		}
		
	}

protected void t5667Load7045()
	{
		/* Read the latest premium tollerance allowed.                     */
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		boolean itemFound = false;
		
		List<Itempf> t5667List = itemDAO.getAllitemsbyCurrency("IT", chdrpf.getChdrcoy().toString(), "T5667", wsbbT5667Key.toString());
		if (t5667List != null && !t5667List.isEmpty()) {
			t5667rec.t5667Rec.set(StringUtil.rawToString(t5667List.get(0).getGenarea()));
			itemFound = true;
		}
		if(!itemFound){
			t5667rec.t5667Rec.set(SPACES);
		}
	}

protected void t5675Load7050()
	{
		/* Premium Calculation Methods.*/
		/* To determine  which premium calculation method to use decide*/
		/*   whether or not it  is  a Single or Joint-life case (it is a*/
		/*   joint life case,  the  joint  life record was found above).*/
		/* Then:*/
		/*  - if it  is  a  single-life  case use, the single-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the joint-life indicator (from T5687) is blank, and*/
		/*       if  it  is  a Joint-life case, use the joint-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the  Joint-life  indicator  is  'N',  then  use the*/
		/*       Single-method.  But, if there is a joint-life (this*/
		/*       must be  a  rider  to have got this far) prompt for*/
		/*       the joint  life  indicator  to determine which life*/
		/*       the rider is to attach to.  In all other cases, the*/
		/*       joint life  indicator  should  be non-displayed and*/
		/*       protected.  The  age to be used for validation will*/
		/*       be the age  of the main or joint life, depending on*/
		/*       the one selected.*/
		if (singlif.isTrue()) {
//			itemIO.setItemitem(t5687rec.premmeth);
			t5675Itemitem = t5687rec.premmeth.toString();
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
//			itemIO.setItemitem(t5687rec.jlPremMeth);
			t5675Itemitem = t5687rec.jlPremMeth.toString();
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5675Itemitem,SPACES)) {
			/* MOVE 'Y'                 TO PREM-REQD                     */
			wsaaPremStatuz.set("Y");
		}
		else {
			calcPremium5200();
		}
		/*EXIT*/
	}

protected void readPovr8000()
	{
	
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)
		&& isNE(povrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}

	}

protected void fuindExe7100()
{
	start7110();
}

protected void start7110()
{
	/* Keep the COVRMJA record if Whole Plan selected or Add component*/
	/*   as this will have been released within the 1000-section to*/
	/*   enable all policies to be processed.*/
	chdrpfDAO.setCacheObject(chdrpf);
	/*chdrmjaIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, chdrmjaIO);
	if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrmjaIO.getParams());
		syserrrec.statuz.set(chdrmjaIO.getStatuz());
		fatalError600();
	}*/
	/* The first thing to consider is the handling of an Options/*/
	/*   Extras request. If the indicator is 'X', a request to visit*/
	/*   options and extras has been made. In this case:*/
	/*    - change the options/extras request indicator to '?',*/
	sv.fuind.set("?");
	/* Save the next 8 programs from the program stack.*/
	compute(sub1, 0).set(add(1,wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		save4510();
	}
	/* Call GENSSWCH with and action of 'H' to retrieve the program*/
	/*   switching required, and move them to the stack.*/
	gensswrec.function.set("J");
	gensww4210();
	compute(sub1, 0).set(add(1,wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		load4530();
	}
	/* Set the current stack "action" to '*'.*/
	/* Add one to the program pointer and exit.*/
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}

protected void fuindRec7100(){

	/* Restore the saved programs to the program stack                 */
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
		restore4610();
	}
	chdrpfDAO.setCacheObject(chdrpf);
	/*chdrmjaIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, chdrmjaIO);
	if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrmjaIO.getParams());
		syserrrec.statuz.set(chdrmjaIO.getStatuz());
		fatalError600();
	}*/
	readFlupal1500();
	/* Blank out the stack  action".                                   */
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	/* Set WSSP-NEXTPROG to the current screen name (thus returning    */
	/*   returning to re-display the screen).                          */
	wsspcomn.nextprog.set(scrnparams.scrname);
}

protected void readFlupal1500()
{
	readFlupal1510();
}

protected void readFlupal1510()
{
	List<Fluppf> fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());/* IJTI-1386 */
	if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
		sv.fuind.set("+");
	}else{
		sv.fuind.set(" ");
	}
}

protected void pbindExe8100()
	{
		/*  - Keep the POVR/CHDR record for Premium Breakdown enquiry.     */
		/*    Ensure we get the latest one.                                */
		readPovr8000();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(),wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(),wsaaPovrLife)
		|| isNE(povrIO.getCoverage(),wsaaPovrCoverage)
		|| isNE(povrIO.getRider(),wsaaPovrRider)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.pbind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);

	}

protected void pbindRet8200()
	{
		/* Release the POVR records as no longer required.                 */
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.pbind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);

	}

protected void vf2MaltPovr8300()
	{
		/* Get the premium breakdown record .......                        */
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* Update the valid flag to '2' ......                             */
		povrIO.setValidflag("2");
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}

	}

protected void a100CheckLimit()
	{
		chkrlrec.clntnum.set(sv.lifcnum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrpf.getCnttype());
		chkrlrec.crtable.set(covtpf.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtpf.getLife());
		chkrlrec.chdrnum.set(chdrpf.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz,varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CheckCalcTax()
	{
		if(a200Start()){
			a200CallTaxSubr();
		}
	}

protected boolean a200Start()
	{
		wsaaProtax.set(0);
		if (isNE(t5687rec.bbmeth, SPACES)) {
			return false;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covtpf.getCrtable());
		a300ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		return true;
	}

	/**
	* <pre>
	*    MOVE 'N'                    TO WSAA-FIRST-TAX-CALC.          
	* </pre>
	*/
protected void a200CallTaxSubr()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covtpf.getLife());
			txcalcrec.coverage.set(covtpf.getCoverage());
			txcalcrec.rider.set(covtpf.getRider());
			txcalcrec.planSuffix.set(covtpf.getPlnsfx());
			txcalcrec.crtable.set(covtpf.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				if(isEQ(sv.instPrem,ZERO)) {
					txcalcrec.amountIn.set(sv.linstamt);
				}
				else {
					txcalcrec.amountIn.set(sv.instPrem);
				}
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrpf.getBtdate());
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(0);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				compute(sv.taxamt01, 2).set(add(add(wsaaTaxamt, sv.linstamt),sv.instPrem));
				if (isEQ(sv.taxind, SPACES)) {
					sv.taxind.set("+");
				}
			}
		}
		sv.taxamt02Out[varcom.pr.toInt()].set("Y");
		sv.taxamt01Out[varcom.pr.toInt()].set("Y");
	}

protected void a250ProrateTax()
	{
		/*A250-START*/
		wsaaProtax.set(0);
		compute(wsaaProtaxRate, 6).setRounded((div(sv.instPrem, sv.linstamt)));
		compute(wsaaProtax, 6).setRounded(mult(wsaaTaxamt, wsaaProtaxRate));
		compute(sv.taxamt02, 2).set(add(wsaaProtax, sv.instPrem));
		/*A250-EXIT*/
	}

protected void a300ReadTr52e() {
	//	List<Itempf> tr52eList = itemDAO.getAllitemsbyCurrency("IT", wsspcomn.company.toString(), "TR52E", wsaaTr52eKey.toString());
		List<Itempf> tr52eList = itemDAO.getAllItemitemByDateFrm("IT", wsspcomn.company.toString(), "TR52E", wsaaTr52eKey.toString().trim(), chdrpf.getCcdate()); //ILFE-7560
		if (tr52eList != null && !tr52eList.isEmpty()) {
			Iterator<Itempf> iterator = tr52eList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (itempf.getItmfrm().compareTo(new BigDecimal(chdrpf.getOccdate().toString())) <= 0) {
					tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
					break;
				}
			}
		}else{
			if (isEQ(subString(wsaaTr52eKey, 2, 7), "*******")) {
				syserrrec.params.set(wsaaTr52eKey);
				fatalError600();
			}
		}
	}
/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f008 = new FixedLengthStringData(4).init("F008");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f335 = new FixedLengthStringData(4).init("F335");
	private FixedLengthStringData f404 = new FixedLengthStringData(4).init("F404");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033");
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");
	private FixedLengthStringData ev01 = new FixedLengthStringData(4).init("EV01");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData f862 = new FixedLengthStringData(4).init("F862");
	private FixedLengthStringData rl33 = new FixedLengthStringData(4).init("RL33");
	private FixedLengthStringData rrj4 = new FixedLengthStringData(4).init("RRJ4");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
}

//ILIFE-3519:Start
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	/* IJTI-1386 START*/
	rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
	rcvdPFObject.setChdrnum(covrpf.getChdrnum());
	rcvdPFObject.setLife(covrpf.getLife());
	rcvdPFObject.setCoverage(covrpf.getCoverage());
	rcvdPFObject.setRider(covrpf.getRider());
	rcvdPFObject.setCrtable(covrpf.getCrtable());
	/* IJTI-1386 END*/
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}

protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
	if(rcvdPFObject!=null){
		/* IJTI-1386 START*/
		rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
		rcvdPFObject.setChdrnum(covrpf.getChdrnum());
		rcvdPFObject.setCoverage(covrpf.getCoverage());
		rcvdPFObject.setCrtable(covrpf.getCrtable());
		rcvdPFObject.setLife(covrpf.getLife());
		rcvdPFObject.setRider(covrpf.getRider());
		/* IJTI-1386 END*/
		if(rcvdPFObject.getPrmbasis() != null){
		if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			rcvdUpdateFlag=true;
		}	
		}
		if(rcvdPFObject.getDialdownoption() != null){
		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			rcvdUpdateFlag=true;
		}
		}
		if(rcvdUpdateFlag)
		{
			rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
		}	

	}else{ 
		rcvdPFObject=new Rcvdpf();
		/* IJTI-1386 START*/
		rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
		rcvdPFObject.setChdrnum(covrpf.getChdrnum());
		rcvdPFObject.setCoverage(covrpf.getCoverage());
		rcvdPFObject.setCrtable(covrpf.getCrtable());
		rcvdPFObject.setLife(covrpf.getLife());
		rcvdPFObject.setRider(covrpf.getRider());
		/* IJTI-1386 END*/
		if(isNE(sv.prmbasis,SPACES)){
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		}
		//BRD-NBP-011 starts
		if(isNE(sv.dialdownoption,SPACES)){
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
		}
		//BRD-NBP-011 ends
		rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
	}


}
//ILIFE-3519:End

/*protected void CessDateValidation() {
	if (isGT(covrrgwIO.getRiskCessDate(),sv.riskCessDate)) {
		sv.rcesdteErr.set(errorsInner.h033);
		covrrgwIO.setStatuz(varcom.endp);
	}
	if (isGT(covrrgwIO.getPremCessDate(),sv.premCessDate)) {
		sv.pcesdteErr.set(errorsInner.h044);
		covrrgwIO.setStatuz(varcom.endp);
	}

}*/

protected void setCustomerSpecificFirstDate() {

}

protected void validateAllowedCovergaeDuration(){
	
}

//ICIL-1311:Start
protected void validateOccupationOrOccupationClass() 
	{
		if(lifepf != null) {
			readTa610();
			String occupation = lifepf.getOccup();
			if( occupation != null && !occupation.trim().isEmpty()) {
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu);
						break;
					}
				}
				
				if(!isFollowUpRequired){
					getOccupationClass2900(occupation);
					for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
							&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
						if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
							isFollowUpRequired = true;
							scrnparams.errorCode.set(errorsInner.rrsu);
							break;
						}
					}	
				}
			}
		}
	}

protected void getOccupationClass2900(String occupation) 
	{
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
	}

protected void readTa610() 
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tA610);
		itempf.setItemitem(covtpf.getCrtable());//IJTI-1793
		itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
		itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
		itempfList  = itempfDAO.findByItemDates(itempf);	//ICIL-1494
		if (itempfList.size()>0 && itempfList.get(0).getGenarea()!=null) {
			ta610rec.tA610Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

protected void createFollowUps3400()
	{
		try {
			fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1386 */
			fupno = fluppfAvaList.size();	//ICIL-1494
			writeFollowUps3410();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void writeFollowUps3410()
	{
		for (wsaaCount.set(1); !(isGT(wsaaCount, 5))
				&& isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
			writeFollowUp3500();
		}
		fluppfDAO.insertFlupRecord(fluppfList);
	}

protected void writeFollowUp3500()
	{
		try {
			fluppf = new Fluppf();
			lookUpStatus3510();
			lookUpDescription3520();
			writeRecord3530();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void lookUpStatus3510()
	{
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}
protected void lookUpDescription3520()
	{
		fupDescpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	}
private void writeRecord3530() 
	{
		fupno++;
		fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
		fluppf.setChdrnum(chdrpf.getChdrnum());
		setPrecision(fupno, 0);
		fluppf.setFupNo(fupno);
		fluppf.setLife("01");
		fluppf.setTranNo(chdrpf.getTranno());
		fluppf.setFupDt(wsaaToday.toInt());
		fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString().trim());
		fluppf.setFupTyp('P');
		fluppf.setFupRmk(fupDescpf.getLongdesc());
		fluppf.setjLife("00");
		fluppf.setTrdt(varcom.vrcmDate.toInt());
		fluppf.setTrtm(varcom.vrcmTime.toInt());
		fluppf.setUserT(varcom.vrcmUser.toInt());
		fluppf.setEffDate(wsaaToday.toInt());
		fluppf.setCrtUser(varcom.vrcmUser.toString());
		fluppf.setCrtDate(wsaaToday.toInt());
		fluppf.setLstUpUser(varcom.vrcmUser.toString());
		fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		fluppfList.add(fluppf);
	}
//ICIL-1311:End
protected void initializeScreenCustomerSpecific(){
	
}

protected void  updatrecrdsCustomerSpecific33(){
	
}


}