/*
 * File: Nfloan.java
 * Date: 29 August 2009 23:00:25
 * Author: Quipoz Limited
 * 
 * Class transformed from NFLOAN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.recordstructures.Nfloanrec;
import com.csc.life.contractservicing.reports.R6244Report;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*REMARKS.
*
*        NON-FORFEITURES - LOANS SUBROUTINE.
*        ===================================
*
* Overview.
* ---------
*
* This program is to be called from Overdue processing, for
*  Traditional contracts that are Overdue and in need of transfer
*  to Non-forfeiture ( APL ), via T6654, and from the Loans
*  Capitalisation batch job B5031.
* This subroutine will basically decide whether the contract
*  being processed is to be allowed a Non-forfeiture Loan (APL).
* It will calculate the Current Surrender value of the Contract,
*  total all outstanding Loans against the contract and then
*  check the difference between the two. If the Surrender value
*  is greater than the debt then processing for this contract
*  can continue, otherwise this routine will write a line to a
*  report saying that the Contract Surrender value can no longer
*  support the debts against it.
*
* Processing.
* -----------
*
* Given: NFLOANREC copybook, containing.....
*          Company, Contract Number, Open-printer-flag,
*          Language, Branch, Effective date,
*
*
* Gives: Statuz:   'O-K'   ..... Policy can have an APL
*                  'NAPL'  ..... Policy cannot have an APL
*             anything else is an ERROR.
*
*
*    i) Calculate current Surrender value of Contract:
*       For each Coverage/Rider attached to the Contract, use
*        the Surrender method specified on T5687 to access the
*        table T6598 we get the Surrender value calculation
*        subroutine which can then be called to calculate the
*        S/Value of the Coverage/Rider being processed.
*       Total the Surrender values of all the Coverages/Riders
*        to get the Contract Surrender value.
*
*   ii) Call the 'TOTLOAN' subroutine with a Function of SPACES
*        to calculate the current total of any Loans + associated
*        interest owed which are associated with this contract
*        we are processing.
*
*  iii) Check the difference between the Surrender value of the
*        contract and the current Loan total:
*
*       If the Surrender value is > current Loan total,
*        Exit the subroutine with a status of OK.
*
*       If the Surrender value is <= current Loan total,
*        write a line to the R6244 Overdue Processing report,
*        detailing the contract number, Surrender value and
*        Current Loan total.
*        Exit the subroutine with a status of 'NAPL' i.e. no APL
*         allowed.
*
*
* TABLES USED
* -----------
*
*   T1692  -   Branch table
*   T1693  -   Company table
*   T5679  -   Transaction Status requirements
*   T5687  -   Coverage/Rider details table
*   T6598  -   Calculation methods table
*   T6633  -   Loan Interest Rules table
*
*****************************************************************
* </pre>
*/
public class Nfloan extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R6244Report printerFile = new R6244Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRiskStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaTotalCash = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSurrValTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldSurrVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalDebt = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaYesToLoan = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrintALine = new FixedLengthStringData(1);
	private String wsaaOpenPrintFlag = "N";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaPolsumCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);
		/* ERRORS */
	private static final String f294 = "F294";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	private static final String t6633 = "T6633";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*   Main, standard page heading*/
	private FixedLengthStringData r6244H01 = new FixedLengthStringData(83);
	private FixedLengthStringData r6244h01O = new FixedLengthStringData(83).isAPartOf(r6244H01, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(r6244h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r6244h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r6244h01O, 11);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r6244h01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r6244h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r6244h01O, 53);

		/*  Detail line*/
	private FixedLengthStringData r6244D01 = new FixedLengthStringData(67);
	private FixedLengthStringData r6244d01O = new FixedLengthStringData(67).isAPartOf(r6244D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r6244d01O, 0);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(r6244d01O, 8);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(r6244d01O, 11);
	private FixedLengthStringData rd01Totnox = new FixedLengthStringData(2).isAPartOf(r6244d01O, 14);
	private ZonedDecimalData rd01Surrval = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 16);
	private ZonedDecimalData rd01Loansum = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 33);
	private ZonedDecimalData rd01Outstamt = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 50);

	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6633rec t6633rec = new T6633rec();
	private Nfloanrec nfloanrec = new Nfloanrec();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ExternalisedRules er = new ExternalisedRules();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Map<String,Descpf> descMap = new HashMap<>();
	private Covrpf covrpf = new Covrpf();
	private Itempf itempf = new Itempf();
	
	public Nfloan() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		nfloanrec.nfloanRec = convertAndSetParam(nfloanrec.nfloanRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		start100();
		exit190();
	}

protected void start100()
	{
		initialise1000();
		if (isNE(wsaaYesToLoan, "Y")) {
			return ;
		}
		getSurrValues2000();
		sumLoans3000();
		surrvalLoansumCalc4000();
		if (isEQ(wsaaPrintALine, "Y")) {
			printReportLine10000();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
	
		wsaaYesToLoan.set(SPACES);
		wsaaPrintALine.set(SPACES);
		wsaaSurrValTot.set(ZERO);
		wsaaHeldSurrVal.set(ZERO);
		wsaaHeldCurrLoans.set(ZERO);
		wsaaTotalDebt.set(ZERO);
		nfloanrec.statuz.set(varcom.oK);
		/* Open print file only if not already open*/
		if (isEQ(wsaaOpenPrintFlag, "N")) {
			setUpPrintFile1100();
		}
		/* check contract type has an entry in T6633 Loan Interest rules*/
		/*  table before we try to give contract an APL.*/
		readT66331200();
		/* if no T6633 record found, set status to NAPL and exit*/
		if (isEQ(t6633rec.t6633Rec, SPACES)) {
			wsaaYesToLoan.set("N");
			nfloanrec.statuz.set("NAPL");
		}
		else {
			wsaaYesToLoan.set("Y");
		}
		/* Read T5679 transaction statii table*/
		readT56791300();
	}

protected void setUpPrintFile1100()
	{
	/* this section will only be performed once whilst the subroutine*/
	/* itself may be called many times from the same program - we only*/
	/* need to set up the report header details & open the print file*/
	/* once*/
	    printerFile.openOutput();
	    wsaaOpenPrintFlag = "Y";
	    
		descMap = descDAO.getMultiDescByCoyAItem("IT", new String[]{"0",nfloanrec.company.toString()}, new String[]{t1693,t1692}, nfloanrec.language.toString(), 
				  new String[]{nfloanrec.company.toString(),nfloanrec.batcbrn.toString()}, new String[]{"  ","  "});
		if(descMap == null || descMap.size() <=0 || descMap.get(t1693)==null || descMap.get(t1692)==null){
			syserrrec.params.set("0t1693"+nfloanrec.company.toString());
			syserrrec.statuz.set("MRNF");
			databaseError99500();
		}
		rh01Company.set(nfloanrec.company);
		rh01Companynm.set(descMap.get(t1693).getLongdesc());
		rh01Branch.set(nfloanrec.batcbrn);
		rh01Branchnm.set(descMap.get(t1692).getLongdesc());
		
		setUpHeadingDates1130();
	
	}


protected void setUpHeadingDates1130()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(nfloanrec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void readT66331200()
	{
		
		/* Read T6633 Loan interest rules to see if Contract type is*/
		/*  allowed to have a loan held against it.*/
	    wsaaT6633Cnttype.set(nfloanrec.cnttype);
	    wsaaT6633Type.set("A");
	    
	    List<Itempf> list = itemDAO.getItdmByFrmdate(nfloanrec.chdrcoy.toString(), t6633, wsaaT6633Key.toString(), nfloanrec.effdate.toInt());
	
	    if(list == null || list.size() == 0){
	    	t6633rec.t6633Rec.set(SPACES);
	    }else{
	    	t6633rec.t6633Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
	    }

	}

protected void readT56791300()
	{		
		/* Read T5679*/
	    itempf = new Itempf();
	    itempf.setItempfx("IT");
	    itempf.setItemcoy(nfloanrec.chdrcoy.toString());
	    itempf.setItemtabl(t5679);
	    itempf.setItemitem(nfloanrec.batcBatctrcde.toString());
	    itempf = itemDAO.getItempfRecord(itempf);
		if (itempf == null) {
			syserrrec.params.set(t5679+nfloanrec.batcBatctrcde.toString());
			syserrrec.statuz.set("MRNF");
			systemError99000();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void getSurrValues2000()
	{
		/* Start reading on Coverage file to see what components this*/
		/*  contract has ... if none, then we will just exit this section,*/
		/*  Otherwise we will calculate the total surrender value of the*/
		/*   contract.*/
		
		List<Covrpf> list = covrpfDAO.getCovrsurByComAndNum(nfloanrec.chdrcoy.toString(), nfloanrec.chdrnum.toString());
		if (list == null || list.size() == 0) {
			syserrrec.params.set(nfloanrec.chdrcoy.toString()+nfloanrec.chdrcoy.toString());
			syserrrec.statuz.set("MRNF");
			systemError99000();
		} else {//IJTI-320 START
		Iterator<Covrpf> iterator = list.iterator();
	
		/* We have at least 1 Coverage/rider on the contract.*/
		/* Now process all components ( ie Coverage/Riders ) on contract*/
	
		boolean first = true;
		while (iterator.hasNext()) {
			covrpf = iterator.next();
			if(first){
				wsaaLife.set(covrpf.getLife());
				first = false;
			}
			if(isNE(covrpf.getLife(), wsaaLife)){
				break;
			}
			processComponents2100();
		}
		

		/* Store values calculated in the Contracts' own currency in case*/
		/*  user decides he wants the loan in a different currency later.*/
		wsaaHeldSurrVal.set(wsaaSurrValTot);
		}
		//IJTI-320 END
	}

protected void processComponents2100()
	{
		/* Read T5687 for Surrender method of component being processed.*/
		/* IF Surrender method not spaces, Read T6598, the Calculation*/
		/*  methods table to get the name of the Surrender value*/
		/*  subroutine we are going to call.*/
		surrSubroutine2200();
		/* Set up linkage area to Call Surrender value calculation*/
		/*  subroutines.*/
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
		srcalcpy.planSuffix.set(covrpf.getPlanSuffix());
		srcalcpy.polsum.set(nfloanrec.polsum);
		srcalcpy.lifeLife.set(covrpf.getLife());
		srcalcpy.lifeJlife.set(covrpf.getJlife());
		srcalcpy.covrCoverage.set(covrpf.getCoverage());
		srcalcpy.covrRider.set(covrpf.getRider());
		srcalcpy.crtable.set(covrpf.getCrtable());
		srcalcpy.crrcd.set(covrpf.getCrrcd());
		srcalcpy.ptdate.set(nfloanrec.ptdate);
		srcalcpy.effdate.set(nfloanrec.effdate);
		srcalcpy.convUnits.set(covrpf.getConvertInitialUnits());
		srcalcpy.language.set(nfloanrec.language);
		srcalcpy.currcode.set(covrpf.getPremCurrency());
		srcalcpy.chdrCurr.set(nfloanrec.cntcurr);
		srcalcpy.pstatcode.set(covrpf.getPstatcode());
		if (isGT(covrpf.getInstprem(), ZERO)) {
			srcalcpy.singp.set(covrpf.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrpf.getSingp());
		}
		srcalcpy.billfreq.set(nfloanrec.billfreq);
		srcalcpy.status.set(SPACES);
		/* Call T6598 Surrender value calculation subroutine....*/
		/* BUT FIRST..... We may have problems with contracts which are*/
		/*  part broken out i.e. ...10 policies in the plan, only 5 are*/
		/*  broken out and therefore there are still 5 summarised under*/
		/*  a plan suffix of 0.... IF PRESERV ( the surrender val calc*/
		/*  subroutine ) is given a plan suffix of zero, it thinks we*/
		/*  want to do a Whole Plan Surrender value calculation when in*/
		/*  fact in our part broken out case we want the Surrender value*/
		/*  of the summarised policies and ALSO the surrender values of*/
		/*  the individually broken out policies.*/
		/*  So, when we have a part broken out case, we will pretend*/
		/*   that the summarised record is broken out and call the*/
		/*   Surrender value calculation subroutiens for each of the*/
		/*   summarised policies within the plan.*/
		if (isEQ(covrpf.getPlanSuffix(), ZERO)
		&& isNE(nfloanrec.polsum, nfloanrec.polinc)
		&& isNE(nfloanrec.polinc, 1)) {
			for (wsaaPolsumCount.set(1); !(isGT(wsaaPolsumCount, nfloanrec.polsum)); wsaaPolsumCount.add(1)){
				sumpolSvalCalc2150();
			}
		}
		else {
			while ( !(isEQ(srcalcpy.status, varcom.endp))) {
				calculateSurrValue2300();
			}
			
		}

		
	}



protected void sumpolSvalCalc2150()
	{
		/*START*/
		/* Calculate the surrender value of a summarised policy in a*/
		/*  part broken out plan*/
		srcalcpy.planSuffix.set(wsaaPolsumCount);
		srcalcpy.status.set(varcom.oK);
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			calculateSurrValue2300();
		}
		
		/*EXIT*/
	}

protected void surrSubroutine2200()
	{
		
		/* Read T5687*/
	    List<Itempf> list = itemDAO.getItdmByFrmdate(covrpf.getChdrcoy(), t5687, covrpf.getCrtable(), covrpf.getCrrcd());

		if (list==null||list.size()==0) {
			syserrrec.params.set("t5687"+covrpf.getCrtable());
			syserrrec.statuz.set(f294);
			systemError99000();
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
		}
		/* Now read T6598 using T5687-SV-METHOD*/
		 itempf = new Itempf();
		 itempf.setItempfx("IT");
		 itempf.setItemcoy(covrpf.getChdrcoy());
		 itempf.setItemtabl(t6598);
		 itempf.setItemitem(t5687rec.svMethod.toString());
		 itempf = itemDAO.getItempfRecord(itempf);

		if (itempf == null ) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void calculateSurrValue2300()
	{
		
		if (isEQ(t6598rec.calcprog, SPACES)) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		/* We have a subroutine to call so let's do it*/
		/* But first we must check that the chosen COVRSUR record has a*/
		/*  valid premium & risk status by checking in T5679*/
		checkCovrsurStatii2350();
		/* if either the Premium or Risk status of the Coverage is*/
		/*  invalid, then we won't calculate a Surrender value for it*/
		if (isEQ(wsaaPremStatus, "N")
		|| isEQ(wsaaRiskStatus, "N")) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) 
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			systemError99000();
		}
		/* Add values returned to our running Surrender value total*/
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		a000CallRounding();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		wsaaSurrValTot.add(srcalcpy.actualVal);
	}

protected void checkCovrsurStatii2350()
	{
		/*START*/
		wsaaPremStatus.set("N");
		wsaaRiskStatus.set("N");
		/* check Coverage premium status*/
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaPremStatus, "Y")); wsaaIndex.add(1)){
			premiumStatus2370();
		}
		/* check Coverage Risk status*/
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaRiskStatus, "Y")); wsaaIndex.add(1)){
			riskStatus2390();
		}
		/*EXIT*/
	}

protected void premiumStatus2370()
	{
		/*START*/
		if (isEQ(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void riskStatus2390()
	{
		/*START*/
		if (isEQ(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
		/*EXIT*/
	}



protected void sumLoans3000()
	{
		
		/* Having got the current surrender value of the contract and the*/
		/*  maximum loan amount available figures, we need to check on*/
		/*  whether there are any existing loans and if so what the value*/
		/*  of those loans is at this moment in time.*/
		/* NOTE..... values returned from TOTLOAN are in the Contract*/
		/*            currency.*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		totloanrec.chdrnum.set(nfloanrec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(nfloanrec.effdate);
		totloanrec.language.set(nfloanrec.language);
		/* Pass 'LOAN' as the Function to HRTOTLON to calculate only    */
		/* total Loans related to that contract.                        */
		totloanrec.function.set("LOAN");
		/* CALL 'TOTLOAN'              USING TOTL-TOTLOAN-REC.          */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError99000();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		/* Now, we should calculate the Total Cash that the Contract    */
		/* has by passing 'CASH' function to HRTOTLON.          <LA2103>*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		totloanrec.chdrnum.set(nfloanrec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(nfloanrec.effdate);
		totloanrec.function.set("CASH");
		totloanrec.language.set(nfloanrec.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError99000();
		}
		compute(wsaaTotalCash, 2).set(add(totloanrec.principal, totloanrec.interest));
	}

protected void surrvalLoansumCalc4000()
	{
		/*START*/
		compute(wsaaTotalDebt, 2).set(add(wsaaHeldCurrLoans, nfloanrec.outstamt));
		/* IF WSAA-HELD-SURR-VAL       > WSAA-TOTAL-DEBT                */
		if ((setPrecision(wsaaTotalDebt, 2)
		&& isGT((add(wsaaHeldSurrVal, wsaaTotalCash)), wsaaTotalDebt))) {
			/* we can let the contract have an APL ( Automatic Premium Loan )*/
			wsaaPrintALine.set("N");
		}
		else {
			/* we cannot allow an APL for this contract because the Loan debt*/
			/*  is already higher than the Surrender value of the contract*/
			wsaaPrintALine.set("Y");
			nfloanrec.statuz.set("NAPL");
		}
		/*EXIT*/
	}

protected void printReportLine10000()
	{
		/* If first page/overflow - write standard headings*/
		if (newPageReq.isTrue()) {
			printerFile.printR6244h01(r6244H01, indicArea);
			wsaaOverflow.set("N");
		}
		/* set up details line fields*/
		rd01Chdrnum.set(nfloanrec.chdrnum);
		rd01Cnttype.set(nfloanrec.cnttype);
		rd01Cntcurr.set(nfloanrec.cntcurr);
		rd01Surrval.set(wsaaHeldSurrVal);
		rd01Loansum.set(wsaaHeldCurrLoans);
		rd01Totnox.set(totloanrec.loanCount);
		rd01Outstamt.set(nfloanrec.outstamt);
		/*  Write detail, checking for page overflow*/
		printerFile.printR6244d01(r6244D01, indicArea);
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		nfloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		start99500();
		exit99590();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		nfloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(nfloanrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(nfloanrec.cntcurr);
		zrdecplrec.batctrcde.set(nfloanrec.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError99000();
		}
		/*A900-EXIT*/
	}
}
